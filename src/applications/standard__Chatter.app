<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-OtherUserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>Access_Card_Application__c</tab>
    <tab>standard-Knowledge</tab>
    <tab>Knowledge__kav</tab>
    <tab>Over_Time_Request__c</tab>
    <tab>I_Link__c</tab>
</CustomApplication>
