<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-Campaign</tab>
    <tab>Recruit__c</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>CS__c</tab>
    <tab>Access_Card_Application__c</tab>
    <tab>Over_Time_Request__c</tab>
    <tab>standard-Quote</tab>
</CustomApplication>
