<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Lead Management</description>
    <label>Lead Management</label>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>LMT_EmailAddressManagement__c</tab>
    <tab>Access_Card_Application__c</tab>
    <tab>Over_Time_Request__c</tab>
</CustomApplication>
