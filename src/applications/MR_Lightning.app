<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Contact_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Contact</pageOrSobjectType>
    </actionOverrides>
    <formFactors>Large</formFactors>
    <label>MR</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Feed</tab>
    <tab>standard-Opportunity</tab>
    <tab>MNT_OppBulkUpdate</tab>
    <tab>standard-report</tab>
    <tab>AgentSelfServiceTab</tab>
    <tab>DailyActivityReport__c</tab>
    <tab>WeeklyActivityReports__c</tab>
    <tab>DPA_Recruit_AH__c</tab>
    <tab>DefectInfoHeader__c</tab>
    <tab>phoneappli__SharePhoneBookView</tab>
    <tab>phoneappli__ChatSettingsTab</tab>
    <tab>standard-Case</tab>
    <tab>Customer__c</tab>
    <tab>standard-Account</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
