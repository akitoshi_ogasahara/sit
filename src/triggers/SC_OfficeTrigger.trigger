trigger SC_OfficeTrigger on SC_Office__c (before insert, after insert) {

	SC_OfficeTriggerHandler handler = new SC_OfficeTriggerHandler();

	// before insert
	if(Trigger.isBefore && Trigger.isInsert){
		handler.onBeforeInsert(Trigger.new);
	}
	
	// after insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new);
	}
}