trigger AGC_DairitenKihonJoho_Trg on AGC_DairitenKihonJoho__c (before insert, before update, after insert,after update) {
	
	/**------APIユーザ以外のユーザによる実行の場合、後続の処理を行わないようチェックする------*/
	AGC_Config__c conf = AGC_Config__c.getInstance('Default');	
	User u = [Select Id, Username from User where Id = :UserInfo.getUserId()];
	if(!u.Username.equals(conf.APIUserName__c)) return;
	
	if(Trigger.isBefore){
		/**------取込日を設定する------*/
		for(AGC_DairitenKihonJoho__c obj : Trigger.new){
			if(!obj.DelRecord__c) obj.DataInputDate__c = Date.today();
		}
	}
	
	else if(Trigger.isAfter){
		/**------代理店補足情報を作成する。------*/
		List<AGC_DairitenJimushoHosokuJoho__c> newHosoku = new List<AGC_DairitenJimushoHosokuJoho__c>();
		if(Trigger.isInsert){
			for(AGC_DairitenKihonJoho__c kihon:Trigger.new){
				AGC_DairitenJimushoHosokuJoho__c hosoku = new AGC_DairitenJimushoHosokuJoho__c();
				hosoku.AgencyInfoRef__c = kihon.Id;
				hosoku.BranchID__c = kihon.BranchID__c;
				newHosoku.add(hosoku);
			}
			if(!newHosoku.isEmpty()){
				insert newHosoku;
			}
		}
	}
}