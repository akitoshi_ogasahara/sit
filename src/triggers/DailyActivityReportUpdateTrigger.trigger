trigger DailyActivityReportUpdateTrigger on DailyActivityReport__c (before insert) {

    if (Trigger.isBefore) {

        /* 登録前の処理(日次活動報告_週単位)
         * 下記項目データを自動セット
         * ・エリア：所有者情報を元にユーザオブジェクトから該当する拠点名をセットする
         * ・営業部：所有者情報を元にユーザオブジェクトから該当する営業部をセットする
         */
        if (Trigger.isInsert) {
            System.debug('-------------------------------------------------------(DailyActivityReportUpdateTrigger)isBefore.isInsert：START');
            // 所有者情報取得
            Set<String> ownerIdList = new Set<String>();
            for (DailyActivityReport__c darObj : Trigger.new) {
                ownerIdList.add(darObj.OwnerId);
            }

            // ユーザーオブジェクト作成
            Map<Id,User> userIdMap = new Map<Id,User>();
            for (User userSelect : [SELECT Id,
                                           Unit__c,
                                           UnitName__c
                                    FROM User
                                         WHERE Id IN: ownerIdList]) {
                userIdMap.put(userSelect.Id,userSelect);
            }

            for (DailyActivityReport__c darObj : Trigger.new) {
                // エリア、営業部をセット
                if (userIdMap.get(darObj.OwnerId) != null) {
                    User userIdList = userIdMap.get(darObj.OwnerId);
                    darObj.Area__c = userIdList.UnitName__c;
                    darObj.SalesUnit__c = userIdList.Unit__c;
                }
            }
            System.debug('-------------------------------------------------------(DailyActivityReportUpdateTrigger)isBefore.isInsert：END');
        } // END Trigger.isBefore.isInsert

    } else {

    }  // End isBefore

}