trigger ContactTrigger on Contact (before insert, before update, before delete
                                        ,after delete, after insert, after undelete, after update) {

    E_ContactTriggerHandler handler = new E_ContactTriggerHandler();

    if(E_ContactTriggerHandler.isSkipTriggerActions == false) {
        //before insert
        if(Trigger.isBefore && Trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        }

        //after insert
        if(Trigger.isAfter && Trigger.isInsert){
            handler.onAfterInsert(Trigger.new, Trigger.newMap);
        }

        //before update
        if(Trigger.isBefore && Trigger.isUpdate){
            handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }

        //after update
        if(Trigger.isAfter && Trigger.isUpdate){
            handler.onAfterUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
        }
    }

}