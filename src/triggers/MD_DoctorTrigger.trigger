trigger MD_DoctorTrigger on MD_Doctor__c (before delete, before insert, before update,
                                            after delete, after insert, after undelete, after update) {

    MD_DoctorTriggerHandler handler = new MD_DoctorTriggerHandler();

    //before insert
    if (Trigger.isBefore && Trigger.isInsert) {
        //Trigger.new, Trigger.oldMap
        handler.onBeforeInsert(Trigger.new, Trigger.oldMap);
    }

    //before update
    if (Trigger.isBefore && Trigger.isUpdate) {
        //Trigger.new, Trigger.oldMap
        handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
}