trigger E_NewPolicyTrigger on E_NewPolicy__c (before delete, before insert, before update
												, after delete, after insert, after update) {

	E_NewPolicyTriggerHandler handler = new E_NewPolicyTriggerHandler();

	//before Insert
	if(Trigger.isBefore && Trigger.isInsert){
		handler.onBeforeInsert(Trigger.new);
	}
	//After Insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new);
	}
	//before update
	if(Trigger.isBefore && Trigger.isUpdate){
		handler.onBeforeUpdate(Trigger.newMap, Trigger.oldMap);
	}
	//After update
	if(Trigger.isAfter && Trigger.isUpdate){
		handler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
	}

}