/**
 * E_BizDataSyncLog trigger
 */
trigger E_BizDataSyncLogTrigger on E_BizDataSyncLog__c (before insert, after insert) {

	//E_BizDataSyncLogTriggerHandler instance
	E_BizDataSyncLogTriggerHandler handler = new E_BizDataSyncLogTriggerHandler();
	
	//before insert
	if(Trigger.isBefore && Trigger.isInsert){
		handler.onBeforeInsert(Trigger.new, Trigger.newMap);
	}
	
	//after insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new, Trigger.newMap);
	}
}