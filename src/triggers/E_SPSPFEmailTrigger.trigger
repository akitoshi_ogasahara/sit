trigger E_SPSPFEmailTrigger on E_SPSPFEmail__c (after insert, after update) {
	
	E_SPSPFEmailTriggerHandler handler = new E_SPSPFEmailTriggerHandler();
	
	//after insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new);
	}
	
	//after update
	if(Trigger.isAfter && Trigger.isUpdate){
		handler.onAfterUpdaet(Trigger.new);
	}


}