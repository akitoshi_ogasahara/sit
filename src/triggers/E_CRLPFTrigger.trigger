trigger E_CRLPFTrigger on E_CRLPF__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	
	E_CRLPFTriggerHandler handler = new E_CRLPFTriggerHandler();
	
	//after insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new);
	}
	
	//after update
	if(Trigger.isAfter && Trigger.isUpdate){
		handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
	}
	
}