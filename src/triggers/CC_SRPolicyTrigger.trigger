trigger CC_SRPolicyTrigger on CC_SRPolicy__c (before insert, after insert, before update, after update) {

	// カスタム表示ラベルでTriggerの実行を制御
	if(System.Label.CMN_CaseTriggerOff.toLowerCase() == 'true'){
		return;
	}

	//サービスリクエストをフィルタリング
	List<CC_SRPolicy__c> newList = new List<CC_SRPolicy__c>();
	List<CC_SRPolicy__c> oldList = new List<CC_SRPolicy__c>();
	Map<Id,CC_SRPolicy__c> newMap = new Map<Id,CC_SRPolicy__c>();
	Map<Id,CC_SRPolicy__c> oldMap = new Map<Id,CC_SRPolicy__c>();
	if(Trigger.new != null){
		List<Id> srIdList = new List<Id>();
		Map<Id, Case> srMap = new Map<Id, Case>();
		for(CC_SRPolicy__c newObj : Trigger.new){
			if(newObj.CC_CaseId__c != null) srIdList.add(newObj.CC_CaseId__c);
		}
		if(srIdList != null) srMap = new Map<Id, Case>([SELECT Id FROM Case where Id IN: srIdList AND RecordType.Name = 'サービスリクエスト']);
		if(srMap != null){
			for(CC_SRPolicy__c newObj : Trigger.new){
				if(newObj.CC_CaseId__c != null && srMap.get(newObj.CC_CaseId__c) != null){
					newList.add(newObj);
					if(Trigger.newMap != null) newMap.put(newObj.Id, Trigger.newMap.get(newObj.Id));
					if(Trigger.oldMap != null){
						oldList.add(Trigger.oldMap.get(newObj.Id));
						oldMap.put(newObj.Id, Trigger.oldMap.get(newObj.Id));
					}
				}
			}
		}
	}

	// Before Insert
	if(Trigger.isInsert && Trigger.isBefore){
		if(newList != null) CC_SRPolicyValidTgrHdl.onBeforeInsert(newList);

	// After Insert
	}else if(Trigger.isInsert && Trigger.isAfter){
		if(newList != null) CC_SRPolicyValidTgrHdl.onAfterInsert(newList, newMap);

	// Before Update
	}else if(Trigger.isUpdate && Trigger.isBefore){
		if(newList != null) CC_SRPolicyValidTgrHdl.onBeforeUpdate(newList, oldList, newMap, oldMap);

	// After Update
	}else if(Trigger.isUpdate && Trigger.isAfter){
		if(newList != null) CC_SRPolicyValidTgrHdl.onAfterUpdate(newList, oldList, newMap, oldMap);
	}
}