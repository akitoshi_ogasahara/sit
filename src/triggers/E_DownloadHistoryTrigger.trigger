trigger E_DownloadHistoryTrigger on E_DownloadHistorry__c (before insert, before update, before delete
										,after delete, after insert, after undelete, after update ) {

	E_DownloadHistoryTriggerHandler handler = new E_DownloadHistoryTriggerHandler();

	//after insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new);
	}

}