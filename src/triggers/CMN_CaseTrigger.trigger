trigger CMN_CaseTrigger on Case (before insert, after insert, before update, after update, before delete, after delete) {

	// カスタム表示ラベルでTriggerの実行を制御
	if(System.Label.CMN_CaseTriggerOff.toLowerCase() == 'true'){
		return;
	}

	//サービスリクエストをフィルタリング
	List<Case> srNewList = new List<Case>();
	List<Case> srOldList = new List<Case>();
	Map<Id,Case> srNewMap = new Map<Id,Case>();
	Map<Id,Case> srOldMap = new Map<Id,Case>();
	RecordType recordTypeOfSR = [SELECT Id FROM RecordType where SobjectType = 'Case' and Name = 'サービスリクエスト' limit 1];
	if(Trigger.new != null){
		for(Case newCase : Trigger.new){
			if(newCase.RecordTypeId == recordTypeOfSR.Id){
				srNewList.add(newCase);
				if(Trigger.newMap != null) srNewMap.put(newCase.Id, Trigger.newMap.get(newCase.Id));
				if(Trigger.oldMap != null) srOldMap.put(newCase.Id, Trigger.oldMap.get(newCase.Id));
			}
		}
	}
	if(Trigger.old != null){
		for(Case oldCase : Trigger.old){
			if(oldCase.RecordTypeId == recordTypeOfSR.Id){
				srOldList.add(oldCase);
				if(Trigger.oldMap != null) srOldMap.put(oldCase.Id, Trigger.oldMap.get(oldCase.Id));
			}
		}
	}

	// Before Insert
	if(Trigger.isBefore && Trigger.isInsert){
		if(srNewList != null){
			CC_CaseManageNonDeliveryTgrHdl.onBeforeInsert(srNewList);
			CC_CaseValidTgrHdl.onBeforeInsert(srNewList);
			CC_CaseCommentHistoryTgrHdl.onBeforeInsert(srNewList);
			CC_CaseSRActivityTgrHdl.onBeforeInsert(srNewList);
			CC_CaseAccountContactTgrHdl.onBeforeInsert(srNewList);
			CC_CaseSetSharedMailBoxAddTgrHdl.onBeforeInsert(srNewList);
		}

	// After Insert
	}else if(Trigger.isAfter && Trigger.isInsert){
		if(srNewList != null){
			CC_CaseManageNonDeliveryTgrHdl.onAfterInsert(srNewList, srNewMap);
			CC_CaseValidTgrHdl.onAfterInsert(srNewList, srNewMap);
			CC_CaseCommentHistoryTgrHdl.onAfterInsert(srNewList, srNewMap);
			CC_CaseSRActivityTgrHdl.onAfterInsert(srNewList, srNewMap);
			CC_CaseAccountContactTgrHdl.onAfterInsert(srNewList, srNewMap);
			CC_CaseSetSharedMailBoxAddTgrHdl.onAfterInsert(srNewList, srNewMap);
		}

	// Before Update
	}else if(Trigger.isBefore && Trigger.isUpdate){
		if(srNewList != null){
			CC_CaseManageNonDeliveryTgrHdl.onBeforeUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseValidTgrHdl.onBeforeUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseCommentHistoryTgrHdl.onBeforeUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseSRActivityTgrHdl.onBeforeUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseAccountContactTgrHdl.onBeforeUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseSetSharedMailBoxAddTgrHdl.onBeforeUpdate(srNewList, srNewMap, srOldMap);
		}

	// After Update
	}else if(Trigger.isAfter && Trigger.isUpdate){
		if(srNewList != null){
			CC_CaseManageNonDeliveryTgrHdl.onAfterUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseValidTgrHdl.onAfterUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseCommentHistoryTgrHdl.onAfterUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseSRActivityTgrHdl.onAfterUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseAccountContactTgrHdl.onAfterUpdate(srNewList, srNewMap, srOldMap);
			CC_CaseSetSharedMailBoxAddTgrHdl.onAfterUpdate(srNewList, srNewMap, srOldMap);
		}

	// Before Delete
	}else if(Trigger.isBefore && Trigger.isDelete){
		if(srOldList != null) CC_CaseAccountContactTgrHdl.onBeforeDelete(srOldList, srOldMap);

	// After Delete
	}else if(Trigger.isAfter && Trigger.isDelete){
		if(srOldList != null) CC_CaseAccountContactTgrHdl.onAfterDelete(srOldList, srOldMap);
	}

}