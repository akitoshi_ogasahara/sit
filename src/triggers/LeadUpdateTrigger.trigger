/**
 *@Update date 2017-12-05
 *@author PwC
 *@Note BatonプロジェクトのトリガをLMT導入時に無効化
 */
trigger LeadUpdateTrigger on Lead (after update) {

    if (Trigger.isAfter) {

        /* 登録前の処理(リクルート)
         * 下記項目データを自動セット
         * ・エリア：所有者情報を元にユーザオブジェクトから該当する拠点名をセットする
         * ・営業部：所有者情報を元にユーザオブジェクトから該当する営業部をセットする
         */
        if (Trigger.isUpdate) {

            for (Lead leadObj : Trigger.new) {
                system.debug('更新処理判定');
                //レコードタイプがバトンの際のみ処理を行う
                if(leadObj.RecordTypeId <> E_RecordTypeDao.getRecordTypeBySobjectType(SObjectType.Lead.Name).get(MK_Const.RECTYPE_DEV_NAME_BATON)) continue;
                if(leadObj.ContractantCheckStatus__c <> MK_Const.PICKLIST_VALUE_NEW_LEAD) continue;
                MK_leadTMP__c upd = E_MK_leadTMPDao.getRecById(leadObj.LeadTMP__c);
                //参照先がな場合は処理を行わない
                system.debug('更新処理判定');
                if(upd == null) continue;
                upd.NewLeadFlag__c = true;
                update upd;
                system.debug('更新終了');

            }
        } // END Trigger.isBefore.isInsert

    } else {

    }  // End isBefore

}