/***********************************************************
 *
 * Application: CRM SYSTEM
 * File Name  : EventTrigger.trigger
 * 
 * created  : 2013/11/26 keizu)　曹文傑
 * modified :
 ************************************************************/
/**
 *
 * 行動トリガ
 *
 */
trigger EventTrigger on Event (before insert, before update) {
	// EventTriggerHandlerハンドル
	EventTriggerHandler eventTriggerHandler = new EventTriggerHandler();

	if(Trigger.isBefore){
		// 行動トリガのonBeforeInsertUpdate関数を呼び出す
		if(Trigger.isInsert){
			eventTriggerHandler.onBeforeInsertUpdate(trigger.new);
		}

		// 行動トリガのonBeforeInsertUpdate関数を呼び出す
		if(Trigger.isUpdate){
			eventTriggerHandler.onBeforeInsertUpdate(trigger.new);				
		}
	}
}