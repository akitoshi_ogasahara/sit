/**
 * history
 * modified on 2014/04/03 any vulnerable codes improved for Code Scanner by Keizu D.Hatada
 */
trigger AgencyForINGAgencyTrigger on AgencyForINGAgency__c (before delete, before update) {

	if (Trigger.isBefore) {

		try {

			List<Customer__c> customs = new List<Customer__c>();

			if (Trigger.isupdate) {

					List<AgencyForINGAgency__c> AgencyForINGAgency_list = Trigger.old;

					List<AgencyForINGAgency__c> AgencyForINGAgency_list_n = Trigger.new;

					///2014/04/03: Add: Begin
					///estimate amount of records for processing
					List<String> idList = new List<String>();
					for(Integer i=0; i<AgencyForINGAgency_list_n.size(); i++) {

						idList.add(AgencyForINGAgency_list_n[i].Id);
					}

					List<Customer__c> cstList = new List<Customer__c>();
					cstList = [
									Select
												Id,
												Name,
												AgencyForINGAgency__c,
												OwnerId
									From
												Customer__c
									Where
												AgencyForINGAgency__c IN :idList
					];

					if((idList.size() * cstList.size()) > 80) {
					///put error message on the page and return
					Trigger.new[0].addError('一度に処理出来る最大件数を超えております。¥n恐れ入りますが、処理する件数を減らして再度実行願います。');
					return;
				}
					///2014/04/03: Add: Finish

					for(Integer i = 0; i < AgencyForINGAgency_list.size(); i++) {

						if (AgencyForINGAgency_list[i].OwnerId != AgencyForINGAgency_list_n[i].OwnerId) {
							customs = [Select  Id,
																Name,
																AgencyForINGAgency__c,
																OwnerId
									From Customer__c
									Where AgencyForINGAgency__c = :AgencyForINGAgency_list[i].Id
						];

						for (Integer j = 0; j < customs.size(); j++) {
							customs[j].OwnerId = AgencyForINGAgency_list_n[i].OwnerId;
						}
						Update customs;
					}
				}
			}

			if (trigger.isDelete) {

				List<AgencyForINGAgency__c> AgencyForINGAgency_list = Trigger.old;

				///2014/04/03: Comment out: Begin
//				for(Integer i = 0; i < AgencyForINGAgency_list.size(); i++) {
//
//					customs = [Select  Id,
//															Name,
//															AgencyForINGAgency__c,
//															OwnerId
//											From Customer__c
//											Where AgencyForINGAgency__c = :AgencyForINGAgency_list[i].Id
//					];
//
//					delete customs;
//				}
				///2014/04/03: Comment out: Finish

				///2014/04/03: Replace above code: Begin
				List<String> idList = new List<String>();
				for(Integer i=0; i<AgencyForINGAgency_list.size(); i++) {
					idList.add(AgencyForINGAgency_list[i].Id);
				}

				//fetch records all at once by idList
				customs = [
								Select
											Id,
											Name,
											AgencyForINGAgency__c,
											OwnerId
								From
											Customer__c
								Where
											AgencyForINGAgency__c IN :idList
				];

				delete customs;
				///2014/04/03: Replace above code: Finish
			}

		} catch(exception e) {
			// エラー
		}

	}

}