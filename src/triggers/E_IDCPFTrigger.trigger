trigger E_IDCPFTrigger on E_IDCPF__c (before insert, before update, before delete
										,after delete, after insert, after undelete, after update ) {


	//E_IDCPFTriggerHandler handler = new E_IDCPFTriggerHandler();
	
	//スキップフラグがFalseの場合のみ処理実行
	if(E_IDCPFTriggerHandler.isSkipTriggerActions==false){
	
        //before update
        if(Trigger.isBefore && Trigger.isUpdate){
            E_IDCPFTriggerHandler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }

        //before insert
        if(Trigger.isBefore && Trigger.isInsert){
            E_IDCPFTriggerHandler.onBeforeInsert(Trigger.new);
        }

		//after insert
		if(Trigger.isAfter && Trigger.isInsert){
            E_IDCPFTriggerHandler.onAfterInsert(Trigger.new);
			E_IDCPFTriggerHandler.OnAfterModifiedAsync(Trigger.newMap.keyset());
		}
		
		//after update
		if(Trigger.isAfter && Trigger.isUpdate){
            E_IDCPFTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap);
			E_IDCPFTriggerHandler.OnAfterModifiedAsync(Trigger.newMap.keyset());
		}
		
		//after delete
		if(Trigger.isAfter && Trigger.isDelete){
			E_IDCPFTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keyset());		//deleteのみoldMap利用
		}
		
		//after undelete
		if(Trigger.isAfter && Trigger.isUndelete){
			E_IDCPFTriggerHandler.OnAfterModifiedAsync(Trigger.newMap.keyset());
		}
	}	
	//P16-0003 Atria対応開発
	//after update
	if(Trigger.isAfter && Trigger.isUpdate){
		E_IDCPFTriggerHandler.OnAfterUpdateLog(Trigger.oldMap, Trigger.newMap);
	}
}