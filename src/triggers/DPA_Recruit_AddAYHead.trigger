trigger DPA_Recruit_AddAYHead on DPA_Recruit_AH__c (after insert, before insert, before update) {
	    List<DPA_Recruit_AY__c> ayList = new List<DPA_Recruit_AY__c>();
    	List<DPA_Recruit_AH__c> ahUpdateList = new List<DPA_Recruit_AH__c>();

    if (Trigger.isBefore) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            for(DPA_Recruit_AH__c ah : Trigger.New) {
                if (ah.OwnerToUpdate__c != NULL) {
                    ah.OwnerId = ah.OwnerToUpdate__c;
					ah.OwnerToUpdate__c = NULL;                    
                }
            }       
        }
    }
    
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            for(DPA_Recruit_AH__c ah : Trigger.New) {
                    ayList.add(new DPA_Recruit_AY__c(	BotenFlag__c = '母店かつ本店、または個人代理店',
                                                        Name = ah.Name,
                                                        PostalCode__c = ah.PostalCode__c,
                                                        JimusyoAddress__c = ah.BotenAddress__c,
                                                        Phone__c = ah.Phone__c,
                                                        RecruitAgency__c = ah.Id));
                           
            }
            if (ayList.size() > 0) {
                insert ayList;
            }
        }
    }
}