trigger E_AGSPFTrigger on E_AGSPF__c (before insert, before update, before delete
                                        ,after delete, after insert, after undelete, after update) {
    E_AGSPFTriggerHandler handler = new E_AGSPFTriggerHandler();

    if(Trigger.isBefore){
        //インサート時
        if (Trigger.isInsert){
            handler.onBeforeInsert(Trigger.new);
        }
        //update
        if (Trigger.isUpdate){
            handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }
    }
}