/**
@version 1.0
@author PwC
リード所有者更新トリガー
*/
trigger LMT_LeadUpdateTriger on Lead (before update, after update) {

	LMT_LeadUpdateTriggerHandler handler = new LMT_LeadUpdateTriggerHandler(Trigger.old, Trigger.new);

	if (LMT_LeadUpdateTrigerHelper.isFirstRun) {
		if (Trigger.isUpdate){
			if(Trigger.isBefore){
				handler.checkLeadAssignAuthority(Trigger.old, Trigger.new);
	            handler.setAssignmentStatus(Trigger.old, Trigger.new);
			}
		    if (Trigger.isAfter){
		    	if(Trigger.Old[0].OwnerId != Trigger.New[0].OwnerId){
		    		handler.sendNotificationMail(Trigger.old, Trigger.new);
		    	}
		    	LMT_LeadUpdateTrigerHelper.isFirstRun = false;
		    }
	 	}
	}
}