trigger UserTrigger on User (before insert, before update, before delete
										,after delete, after insert, after undelete, after update ) {

	UserTriggerHandler handler = new UserTriggerHandler();

	if(UserTriggerHandler.isSkipTriggerActions == false) {
		//before insert
		if(Trigger.isBefore && Trigger.isInsert){
			handler.onBeforeInsert(Trigger.new);
		}

		//before update
		if(Trigger.isBefore && Trigger.isUpdate){
			handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
		}

		//after update
		if(Trigger.isAfter && Trigger.isUpdate){
			handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
		}
	}
}