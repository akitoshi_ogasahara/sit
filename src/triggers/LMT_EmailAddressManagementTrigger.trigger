/**
@version 1.0
@author PwC
LMTメール通知管理のUser1重複チェックトリガー
*/
trigger LMT_EmailAddressManagementTrigger on LMT_EmailAddressManagement__c (before insert, before update) {
    LMT_EmailAddressManagementTriggerHandler  handler = new LMT_EmailAddressManagementTriggerHandler ();
    if(Trigger.isBefore){
        if (Trigger.isInsert){
            handler.checkUser1existenceForInsert(Trigger.new);
        }
        if (Trigger.isUpdate){
            handler.checkUser1existenceForUpdate(Trigger.old, Trigger.new);
        }


    }
}