trigger E_AGPOTrigger on E_AGPO__c (before insert, before update, before delete
                                        ,after delete, after insert, after undelete, after update) {

	E_AGPOTriggerHandler handler = new E_AGPOTriggerHandler();
	//before
	if(Trigger.isBefore){
		//insert
		if (Trigger.isInsert){
			handler.onBeforeInsert(Trigger.new);
		}
		//update
		if (Trigger.isUpdate){
			handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
		}
	}
}