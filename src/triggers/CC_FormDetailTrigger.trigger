/**
 * コールセンター帳票情報トリガ
 */
trigger CC_FormDetailTrigger on CC_FormDetail__c (after update) {

    CC_FormDetailTgrHdl handler = new CC_FormDetailTgrHdl();

    if(!handler.isTriggerRun) {
        return;
    }

    if(Trigger.isAfter) {

        if(Trigger.isUpdate) {

            handler.onAfterUpdate(Trigger.new);

            if(handler.isCreateFollowUpSRActivity) {

                CC_FormDetailSRActivityTgrHdl.onAfterUpdate(handler.detailsToCreateActivity, null, null);

                handler.updateFormInfo(handler.detailsToCreateActivity, true);
            }
        }
    }
}