trigger E_NewPolicyDefectTrigger on E_NewPolicyDefect__c (before delete, before insert, before update
												, after delete, after insert, after update) {

	E_NewPolicyDefectTriggerHandler handler = new E_NewPolicyDefectTriggerHandler();

	//After insert
	if(Trigger.isAfter && Trigger.isInsert){
		handler.onAfterInsert(Trigger.new);
	}
	//After update
	if(Trigger.isAfter && Trigger.isUpdate){
		handler.onAfterUpdate(Trigger.newMap, Trigger.oldMap);
	}

}