trigger E_NCOLTrigger on E_NCOL__c (before insert, before update, before delete
                                        ,after delete, after insert, after undelete, after update) {

	E_NCOLTriggerHandler handler = new E_NCOLTriggerHandler();
	//before
	if(Trigger.isBefore){
		//insert
		if (Trigger.isInsert){
			handler.onBeforeInsert(Trigger.new);
		}
		//update
		if (Trigger.isUpdate){
			handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
		}
	}
}