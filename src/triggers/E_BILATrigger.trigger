trigger E_BILATrigger on E_BILA__c (before insert, before update, before delete
                                        ,after delete, after insert, after undelete, after update) {

	E_BILATriggerHandler handler = new E_BILATriggerHandler();
	//before
	if(Trigger.isBefore){
		//insert
		if (Trigger.isInsert){
			handler.onBeforeInsert(Trigger.new);
		}
		//update
		if (Trigger.isUpdate){
			handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
		}
	}
}