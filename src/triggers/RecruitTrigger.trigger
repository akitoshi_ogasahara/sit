trigger RecruitTrigger on Recruit__c (before insert) {
	if (Trigger.isBefore) {
		/* 登録前の処理(リクルート)
		 * 下記項目データを自動セット
		 * ・エリア：所有者情報を元にユーザオブジェクトから該当する拠点名をセットする
		 * ・営業部：所有者情報を元にユーザオブジェクトから該当する営業部をセットする
		 */
		if (Trigger.isInsert) {
			System.debug('-------------------------------------------------------(RecruitTrigger)isBefore.isInsert：START');
			// 所有者情報取得
			Set<String> ownerIdList = new Set<String>();
			for (Recruit__c recruitObj : Trigger.new) {
				ownerIdList.add(recruitObj.OwnerId);
			}

			// ユーザーオブジェクト作成
			Map<Id,User> userIdMap = new Map<Id,User>();
			for (User userSelect : [SELECT Id,
										   Unit__c,
										   UnitName__c
									FROM User
										 WHERE Id IN: ownerIdList]) {
				userIdMap.put(userSelect.Id,userSelect);
			}

			for (Recruit__c recruitObj : Trigger.new) {
				// エリア、営業部をセット
				if (userIdMap.get(recruitObj.OwnerId) != null) {
					User userIdList = userIdMap.get(recruitObj.OwnerId);
					recruitObj.Area__c = userIdList.UnitName__c;
					recruitObj.SalesUnit__c = userIdList.Unit__c;
				}
			}
			System.debug('-------------------------------------------------------(RecruitTrigger)isBefore.isInsert：END');
		} // END Trigger.isBefore.isInsert
	} else {

	}  // End isBefore
}