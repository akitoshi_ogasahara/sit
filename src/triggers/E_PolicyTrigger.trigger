trigger E_PolicyTrigger on E_Policy__c (before delete, before insert, before update
											,after delete, after insert, after undelete, after update) {
	
	E_PolicyTriggerHandler handler = new E_PolicyTriggerHandler();
	
	//before insert
	if(Trigger.isBefore && Trigger.isInsert){
		handler.onBeforeInsert(Trigger.new);
	}
	
	//Before update
	if(Trigger.isBefore && Trigger.isUpdate){
		handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
	}
	
}