trigger EventUpdateTrigger on Event (before insert) {

    if (Trigger.isBefore) {

        /* 登録前の処理(行動)
         * 下記項目データを自動セット
         * ・エリア：所有者情報を元にユーザオブジェクトから該当する拠点名をセットする
         * ・営業部：所有者情報を元にユーザオブジェクトから該当する営業部をセットする
         */
        if (Trigger.isInsert) {
            System.debug('-------------------------------------------------------(Event)isBefore.isInsert：START');
            // 所有者情報取得
            Set<String> ownerIdList = new Set<String>();
            for (Event enevtObj : Trigger.new) {
                ownerIdList.add(enevtObj.OwnerId);
            }

            // ユーザーオブジェクト作成
            Map<Id,User> userIdMap = new Map<Id,User>();
            for (User userSelect : [SELECT Id,
                                           Unit__c,
                                           UnitName__c
                                    FROM User
                                         WHERE Id IN: ownerIdList]) {
                userIdMap.put(userSelect.Id,userSelect);
            }

            for (Event enevtObj : Trigger.new) {
                // エリア、営業部をセット
                if (userIdMap.get(enevtObj.OwnerId) != null) {
                    User userIdList = userIdMap.get(enevtObj.OwnerId);
                    enevtObj.Area__c = userIdList.UnitName__c;
                    enevtObj.SalesUnit__c = userIdList.Unit__c;
                }
            }
            System.debug('-------------------------------------------------------(Event)isBefore.isInsert：END');
        } // END Trigger.isBefore.isInsert

    } else {

    }  // End isBefore

}