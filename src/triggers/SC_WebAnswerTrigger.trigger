trigger SC_WebAnswerTrigger on SC_WebAnswer__c (before insert, before update, before delete, after delete, after insert, after undelete, after update) {
	SC_WebAnswerTriggerHandler handler = new SC_WebAnswerTriggerHandler();

	if (Trigger.isAfter && Trigger.isInsert) {
		//after insert

		// 別紙レコード作成
		handler.createExhibit(Trigger.newMap);

	} else if(Trigger.isAfter && Trigger.isUpdate){
		//after update

		// 変更履歴取得
		handler.historyAcquisition(Trigger.oldMap, Trigger.newMap);
		
		// 別紙レコード作成
		handler.createExhibit(Trigger.newMap);
	}
}