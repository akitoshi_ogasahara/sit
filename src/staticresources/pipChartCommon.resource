/**	--------------------------------------------------------------------------------
 * Chart全体
 */
/*
jQuery(document).ready(function(){
	Chart.plugins.register({
		// データ描写後
		afterDatasetsDraw: function(chart, easing) {
			if(chart.canvas.className == 'sideBarGraph100P'){
				// 横棒グラフ(100%)の場合
				var ctx = chart.ctx;
				chart.data.datasets.forEach(function (dataset, i) {
					var meta = chart.getDatasetMeta(i);
					if (!meta.hidden) {
						meta.data.forEach(function(element, index) {
							// データ文字の描写
							ctx.fillStyle = 'rgba(255, 255, 255, 1)';
							ctx.font = Chart.helpers.fontString(14, 'bold', 'Helvetica Neue');
							ctx.textAlign = 'center';
							ctx.textBaseline = 'middle';
							var dataString = chartCommon.toFixed(dataset.data[index], 1) + '%';
							ctx.fillText(dataString, 55, 12);
						});
					}
				});
			}
		}
	});
});
*/

/** --------------------------------------------------------------------------------
 * Chart用共通処理
 */
var chartCommon = chartCommon || {};

/**
 * 3桁カンマ編集
 */
chartCommon.comma = function(_number){
	var txt = '';
	if(_number == 0){
		txt = '0';
	}else{
		txt = String(Math.floor(_number));
		while(txt != (txt = txt.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
	}
	return txt;
}
/**
 * 小数点編集
 */
chartCommon.toFixed = function(_number, _decimal){
	return _number.toFixed(_decimal);
}
/**
 * 特殊文字変換
 */
chartCommon.unescapeHTML = function(_string){
	return _string
	.replace(/(&quot;)/g,	'"')
	.replace(/(&amp;)/g,	'&')
	.replace(/(&lt;)/g,		'<')
	.replace(/(&gt;)/g,		'>')
	.replace(/(&nbsp;)/g,	' ')
	.replace(/(&#39;)/g,	"'");
}
/**
 * 再描画
 * @param	Object:グラフオブジェクト
 */
chartCommon.refresh = function(_obj){
	try {
		var param = {
			type: _obj.config.type,
			data: _obj.data,
			options: _obj.options
		};
		var canvas	= _obj.legend.ctx.canvas;
		_obj.destroy();
		_obj = new Chart(canvas.getContext("2d"), param);
	}catch(e){
		alert(e);
	}
}


/**
 * グラフ色
 */
/* 指定色 */
chartCommon.colors = [
'rgba(156,145,198,1)','rgba(230,68,21,1)','rgba(83,137,194,1)','rgba(164,204,234,1)','rgba(91,152,83,1)','rgba(255,165,0,1)'];

/**
 * グラフ色(50%)
 */
/* 指定色 */
chartCommon.colorhalfs = [
'rgba(234, 101, 13, 0.5)','rgba(234, 201, 142, 0.5)','rgba(230, 68, 21, 0.5)',
'rgba(124, 170, 214, 0.5)','rgba(164, 204, 234, 0.5)','rgba(83, 137, 194, 0.5)',
'rgba(150, 180, 57, 0.5)','rgba(200, 205, 46, 0.5)','rgba(91, 152, 83, 0.5)',
'rgba(156, 145, 198, 0.5)','rgba(198, 191, 224, 0.5)','rgba(112, 108, 176, 0.5)',
'rgba(102, 102, 102, 0.5)','rgba(202, 199, 199, 0.5)','rgba(153, 153, 153, 0.5)'];
/* 調整色
chartCommon.colorhalfs = [
'rgba(234, 101, 13, 0.5)', 'rgba(241, 153, 51, 0.5)', 'rgba(230, 68, 21, 0.5)',
'rgba(124, 170, 214, 0.5)', 'rgba(164, 204, 234, 0.5)', 'rgba(83, 137, 194, 0.5)',
'rgba(251, 218, 30, 0.5)', 'rgba(246, 231, 123, 0.5)', 'rgba(225, 205, 74, 0.5)',
'rgba(150, 180, 57, 0.5)', 'rgba(200, 205, 46, 0.5)', 'rgba(91, 152, 83, 0.5)',
'rgba(247, 211, 204, 0.5)', 'rgba(251, 234, 231, 0.5)', 'rgba(248, 178, 164, 0.5)',
'rgba(156, 145, 198, 0.5)', 'rgba(198, 191, 224, 0.5)', 'rgba(112, 108, 176, 0.5)',
'rgba(153, 153, 153, 0.5)', 'rgba(202, 199, 199, 0.5)', 'rgba(102, 102, 102, 0.5)'];
*/


/**
 * 文字・線色
 */
chartCommon.basecolors = [
'rgba(0, 0, 0, 1)',
'rgba(102, 102, 102, 1)', 'rgba(153, 153, 153, 1)', 'rgba(202, 199, 199, 1)',
'rgba(255, 255, 255, 1)',
'rgba(234, 201, 142, 1)', 'rgba(238, 127, 0, 1)', 'rgba(234, 101, 13, 1)', 'rgba(230, 68, 21, 1)'];


/** --------------------------------------------------------------------------------
 * 推移表折れ線グラフ
 */
chartCommon.barGraph = {
	/**
	 * 初期化
	 * @param	String：グラフ描写対象ID
	 * @param	json：jsonデータ
	 * @return Object:グラフオブジェクト
	 */
	init: function(_id, _json, currentYear, selectedYear ){
		var element	= document.getElementById(_id);
		if(!element) return;
		var jsonData = JSON.parse(_json);
		var labels		= this.getLabels(jsonData);
		var datasets	= this.getDatasets(jsonData);
		var isInit = selectedYear.length == 0 ? true : false; //初期表示か判定するフラグ

		var myChart = new Chart(document.getElementById(_id).getContext("2d"), {
			type: 'line',
			data: {
				labels: labels,
				datasets: datasets
			},
			options: {
/*
				legend:{
					display:true,
					position:'right',
				},
*/
				legend:{
					display:false,
				},
				animation : false ,

				legendCallback: function(chart){
					var tableStr = [];
					tableStr.push('<table id="pip-merit-selected-table" class="table iris-table iris-bg-white">');
					//console.log(chart.legend);
					tableStr.push('<tr>');
					tableStr.push('<th class="text-xs-center" colspan="2">');
					tableStr.push('経過年数 ');
					tableStr.push('<select id="selectedYearList" onChange="selectYearFromList(this)" >' );
					for( var i = 1; i <= chart.data.datasets[0].data.length; i++ ){
						tableStr.push('<option value="year' + i + '">' +  i + '年 </option>'  );
					}
					tableStr.push('</select>' );
					tableStr.push('</th>');
					tableStr.push('</tr>');
					for (var i=0; i<chart.data.datasets.length; i++) {
						tableStr.push('<tr>');
						//表示ラベル
						tableStr.push('<td><label for="' + chart.data.datasets[i].id + '_check">');
						tableStr.push('<div class="chart-legend" style="color:' + chart.data.datasets[i].backgroundColor + '">');
						tableStr.push('<input type="checkbox" checked="checked" id="' + chart.data.datasets[i].id + '_check" onclick="onClickLegend()">' );
						tableStr.push( chart.data.datasets[i].label );
						tableStr.push('</label></div></td>');
						//値
						tableStr.push('<td class="text-xs-right"><label for="' + chart.data.datasets[i].id + '_check">');
						var unit = chart.data.datasets[i].label.indexOf('率') == -1 ? '円' : '％';
						tableStr.push('0' + unit);
						tableStr.push('</label></td>');
						tableStr.push('</tr>');
					}
					tableStr.push('</table>');
					//console.log('tableStr = '  + tableStr.join(""));
					return tableStr.join("");
				},
				scales:{
					xAxes: [{
						stacked: true,
						display: true,
						gridLines: {
							LineWidth: 1,
							color: chartCommon.basecolors[4],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[3]
						},
						ticks: {
							fontColor: chartCommon.basecolors[0],
							maxRotation: 0,
							minRotation: 0,
						}
					}],
					yAxes: [{
						id: 'Amount',
						type: 'linear',
						position: 'left',
						gridLines: {
/*
							lineWidth: 1,
							drawBorder: false,
							color: chartCommon.basecolors[3],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[2]
*/
							display:false
						},
						ticks: {
							suggestedMax: 10000,
							min: 0,
							callback: function(value, index, values) {
								return chartCommon.comma(value) + '円';
							}
						}
					},{
						id: 'ratio',
						type: 'linear',
						position: 'right',
						gridLines: {
							lineWidth: 1,
							drawBorder: false,
							color: chartCommon.basecolors[3],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[2]
						},
						ticks: {
							suggestedMax: 100,
							min: 0,
							callback: function(value) {
								return value + '％';
							}
						}
					}]
				},
				tooltips: {
					mode: 'x' ,
					intersect: false,
					position: 'nearest',
					callbacks: {
						title: function(tooltipItems, data ){
							var titleList = [];
							titleList.push('経過年数：' + tooltipItems[0].xLabel[0]);
							titleList.push('年齢:' + tooltipItems[0].xLabel[1]);
							return titleList;
						},
						label: function(tooltipItems, data) {
							var regex = new RegExp(/^([1-9]\d*|0)$/);
							if(regex.test(tooltipItems.yLabel)){
								return data.datasets[tooltipItems.datasetIndex].label + ': ' + chartCommon.comma(tooltipItems.yLabel);
								// return data.datasets[tooltipItems.datasetIndex].label + ': ' + chartCommon.comma(tooltipItems.yLabel) + '円';
							} else {
								return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.yLabel;
								// return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.yLabel + '％';
							}
						}
					}
				}
			},
			plugins: [{
				afterDraw: function( chart, ease ){
					// カスタム描画を追加でおこなうため、データセットのメタデータを取得
					var meta = chart.getDatasetMeta(0);
					var data = meta.data;
					// X座標
					var x_current = data[currentYear]._model.x; //現在年
					// Y座標
					var y_top = data[currentYear]._yScale.top;
					var y_bottom = data[currentYear]._yScale.bottom;

					// 垂直線の描画
					var context = chart.chart.ctx;
					context.beginPath();
					context.lineWidth = 2;
					context.strokeStyle = '#bfbfbf';
					context.moveTo(x_current, y_top);
					context.lineTo(x_current, y_bottom);
					context.stroke();
					if( !isInit ){
						var x_selected = data[selectedYear]._model.x; //選択年
						var y_top_selected = data[selectedYear]._yScale.top;
						var y_bottom_selected = data[selectedYear]._yScale.bottom;
						context.beginPath();
						context.lineWidth = 2;
						context.strokeStyle = '#CC6600';
						context.moveTo(x_selected, y_top_selected);
						context.lineTo(x_selected, y_bottom_selected);
						context.stroke();
					}
				}
			}]
		});
		return myChart;
	},

	/**
	 * JSONデータよりChart用データセットへ変換(内部処理)
	 * @param	json：jsonデータ
	 * @return json：Chart用データセット
	 */
	getDatasets: function(_json){
		var dataset 	= [];
		var data 		= {};
		if(_json.s[0] != ''){
			data					={};
			data.label				='保険金額';
			data.yAxisID			='Amount';
			data.data				=_json.s;
			data.backgroundColor	=chartCommon.colors[0];
			data.borderColor		=chartCommon.colors[0];
			data.borderWidth		= 2;
			data.pointRadius		= 1;
			data.lineTension		= 0;
			data.fill				= false;
			data.id					='s';
			dataset.push(data);
		}
		

		data					={};
		data.label				='解約返戻金額';
		data.yAxisID			='Amount';
		data.data				=_json.cv;
		data.backgroundColor	=chartCommon.colors[1];
		data.borderColor		=chartCommon.colors[1];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.id					='cv';
		dataset.push(data);

		data					={};
		data.label				='保険料累計';
		data.yAxisID			='Amount';
		data.data				=_json.pRuikei;
		data.backgroundColor	=chartCommon.colors[2];
		data.borderColor		=chartCommon.colors[2];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.id					='pRuikei';
		dataset.push(data);

		data					={};
		data.label				='実質負担額';
		data.yAxisID			='Amount';
		data.data				=_json.jisshitsuFutanGaku;
		data.backgroundColor	=chartCommon.colors[3];
		data.borderColor		=chartCommon.colors[3];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.id					='jisshitsuFutanGaku';
		dataset.push(data);

		data					={};
		data.label				='単純返戻率';
		data.yAxisID			='ratio';
		data.data				=_json.simpleHenreiRt;
		data.backgroundColor	=chartCommon.colors[4];
		data.borderColor		=chartCommon.colors[4];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.id					='simpleHenreiRt';
		dataset.push(data);

		data					={};
		data.label				='実質返戻率';
		data.yAxisID			='ratio';
		data.data				=_json.jisshitsuHenreiRt;
		data.backgroundColor	=chartCommon.colors[5];
		data.borderColor		=chartCommon.colors[5];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.data				=_json.jisshitsuHenreiRt;
		data.id					='jisshitsuHenreiRt';
		dataset.push(data);

		return dataset;
	},

	/**
	 * JSONデータよりChart用データセットへ変換(内部処理)
	 * @param	json：jsonデータ
	 * @return json：Chart用データセット
	 */
	getLabels: function(_json){
		var nensulabels = _json.keikanensuList;
		var ageLabels = _json.ageList;
		var labels = [];
		var label	= [];
		for( var i = 0; i < nensulabels.length; i++ ){
			label = [];
			label.push(nensulabels[i] + '年');
			label.push(ageLabels[i] + '歳');
			labels.push(label);
		}
		return labels;
	}
}