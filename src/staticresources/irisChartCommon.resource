/**  --------------------------------------------------------------------------------
 * Chart全体
 */
/*
jQuery(document).ready(function(){
	Chart.plugins.register({
		// データ描写後
		afterDatasetsDraw: function(chart, easing) {
			if(chart.canvas.className == 'sideBarGraph100P'){
				// 横棒グラフ(100%)の場合
				var ctx = chart.ctx;
				chart.data.datasets.forEach(function (dataset, i) {
					var meta = chart.getDatasetMeta(i);
					if (!meta.hidden) {
						meta.data.forEach(function(element, index) {
							// データ文字の描写
							ctx.fillStyle = 'rgba(255, 255, 255, 1)';
							ctx.font = Chart.helpers.fontString(14, 'bold', 'Helvetica Neue');
							ctx.textAlign = 'center';
							ctx.textBaseline = 'middle';
							var dataString = chartCommon.toFixed(dataset.data[index], 1) + '%';
							ctx.fillText(dataString, 55, 12);
						});
					}
				});
			}
		}
	});
});
*/

/** --------------------------------------------------------------------------------
 * Chart用共通処理
 */
var chartCommon = chartCommon || {};

/**
 * 3桁カンマ編集
 */
chartCommon.comma = function(_number){
	var txt = '';
	if(_number == 0){
		txt = '0';
	}else{
		txt = String(Math.floor(_number));
		while(txt != (txt = txt.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
	}
	return txt;
}
/**
 * 小数点編集
 */
chartCommon.toFixed = function(_number, _decimal){
	return _number.toFixed(_decimal);
}
/**
 * 特殊文字変換
 */
chartCommon.unescapeHTML = function(_string){
	return _string
	.replace(/(&quot;)/g,	'"')
	.replace(/(&amp;)/g,	'&')
	.replace(/(&lt;)/g,		'<')
	.replace(/(&gt;)/g,		'>')
	.replace(/(&nbsp;)/g,	' ')
	.replace(/(&#39;)/g,	"'");
}
/**
 * 再描画
 * @param  Object:グラフオブジェクト
 */
chartCommon.refresh = function(_obj){
	try {
		var param = {
			type: _obj.config.type,
			data: _obj.data,
			options: _obj.options
		};
		var canvas	= _obj.legend.ctx.canvas;
		_obj.destroy();
		_obj = new Chart(canvas.getContext("2d"), param);
	}catch(e){
		alert(e);
	}
}


/**
 * グラフ色
 */
/* 指定色 */
chartCommon.colors = [
'rgba(234, 101, 13, 1)','rgba(234, 201, 142, 1)','rgba(230, 68, 21, 1)',
'rgba(124, 170, 214, 1)','rgba(164, 204, 234, 1)','rgba(83, 137, 194, 1)',
'rgba(150, 180, 57, 1)','rgba(200, 205, 46, 1)','rgba(91, 152, 83, 1)',
'rgba(156, 145, 198, 1)','rgba(198, 191, 224, 1)','rgba(112, 108, 176, 1)',
'rgba(102, 102, 102, 1)','rgba(202, 199, 199, 1)','rgba(153, 153, 153, 1)'];
/* 調整色
chartCommon.colors = [
'rgba(234, 101, 13, 1)', 'rgba(241, 153, 51, 1)', 'rgba(230, 68, 21, 1)',
'rgba(124, 170, 214, 1)', 'rgba(164, 204, 234, 1)', 'rgba(83, 137, 194, 1)',
'rgba(251, 218, 30, 1)', 'rgba(246, 231, 123, 1)', 'rgba(225, 205, 74, 1)',
'rgba(150, 180, 57, 1)', 'rgba(200, 205, 46, 1)', 'rgba(91, 152, 83, 1)',
'rgba(247, 211, 204, 1)', 'rgba(251, 234, 231, 1)', 'rgba(248, 178, 164, 1)',
'rgba(156, 145, 198, 1)', 'rgba(198, 191, 224, 1)', 'rgba(112, 108, 176, 1)',
'rgba(153, 153, 153, 1)', 'rgba(202, 199, 199, 1)', 'rgba(102, 102, 102, 1)'];
 */
/**
 * グラフ色(50%)
 */
/* 指定色 */
chartCommon.colorhalfs = [
'rgba(234, 101, 13, 0.5)','rgba(234, 201, 142, 0.5)','rgba(230, 68, 21, 0.5)',
'rgba(124, 170, 214, 0.5)','rgba(164, 204, 234, 0.5)','rgba(83, 137, 194, 0.5)',
'rgba(150, 180, 57, 0.5)','rgba(200, 205, 46, 0.5)','rgba(91, 152, 83, 0.5)',
'rgba(156, 145, 198, 0.5)','rgba(198, 191, 224, 0.5)','rgba(112, 108, 176, 0.5)',
'rgba(102, 102, 102, 0.5)','rgba(202, 199, 199, 0.5)','rgba(153, 153, 153, 0.5)'];
/* 調整色
chartCommon.colorhalfs = [
'rgba(234, 101, 13, 0.5)', 'rgba(241, 153, 51, 0.5)', 'rgba(230, 68, 21, 0.5)',
'rgba(124, 170, 214, 0.5)', 'rgba(164, 204, 234, 0.5)', 'rgba(83, 137, 194, 0.5)',
'rgba(251, 218, 30, 0.5)', 'rgba(246, 231, 123, 0.5)', 'rgba(225, 205, 74, 0.5)',
'rgba(150, 180, 57, 0.5)', 'rgba(200, 205, 46, 0.5)', 'rgba(91, 152, 83, 0.5)',
'rgba(247, 211, 204, 0.5)', 'rgba(251, 234, 231, 0.5)', 'rgba(248, 178, 164, 0.5)',
'rgba(156, 145, 198, 0.5)', 'rgba(198, 191, 224, 0.5)', 'rgba(112, 108, 176, 0.5)',
'rgba(153, 153, 153, 0.5)', 'rgba(202, 199, 199, 0.5)', 'rgba(102, 102, 102, 0.5)'];
*/


/**
 * 文字・線色
 */
chartCommon.basecolors = [
'rgba(0, 0, 0, 1)',
'rgba(102, 102, 102, 1)', 'rgba(153, 153, 153, 1)', 'rgba(202, 199, 199, 1)',
'rgba(255, 255, 255, 1)',
'rgba(234, 201, 142, 1)', 'rgba(238, 127, 0, 1)', 'rgba(234, 101, 13, 1)', 'rgba(230, 68, 21, 1)'];

/** --------------------------------------------------------------------------------
 * 横棒グラフ(%表示用)
 */
chartCommon.sideBarGraph100P = {
	/**
	 * 初期化
	 * @param  String：グラフ描写対象ID
	 * @param  Double：値
	 * @return Object:グラフオブジェクト
	 */
	init: function(_id, _val){
		var $element = jQuery("#" + _id);
		if($element.length == 0) return;

		// デザイン変更
		$element.addClass('sideBarGraph100P');
		$element.attr('width','100');
		$element.attr('height','25');
		$element.css("background-color", chartCommon.basecolors[2]);
		$element.css("border", "solid 1px " + chartCommon.basecolors[1]);

		$element.wrap('<div style="position: relative;"></div>');
		var style = '';
		style += 'position: absolute; ';
		style += 'width: 100px; ';
		style += 'height: 25px; ';
		style += 'top: 1px; ';
		style += 'left: 1px; ';
		style += 'padding-top: 8.5px; ';
		style += 'color: rgb(255, 255, 255); ';
		style += 'font-size: 14px; ';
		style += 'text-align: center; ';
		style += 'cursor: default; ';
		$element.after('<div id="' + _id + '_percent" style="' + style + '">' + chartCommon.toFixed(_val, 1) + '%</div>');

		return new Chart(document.getElementById(_id).getContext("2d"), {
			type: 'horizontalBar',
			data: {
				labels: [""],
				datasets: [{
					backgroundColor: chartCommon.colors[0],
					borderColor: chartCommon.colors[0],
					data: [_val]
				}]
			},
			options: {
				maintainAspectRatio: false,
				layout: {
					padding: {
						left: 0,
						right: 0,
						top: 0,
						bottom: 0
					}
				},
				scales: {
					xAxes: [{
						display: false,
						barPercentage: 1,
						categoryPercentage: 1,
						gridLines: {
							offsetGridLines: false
						},
						ticks: {
							beginAtZero: true,
							min: 0,
							max: 100
						}
					}],
					yAxes: [{
						display: false,
						barPercentage: 1,
						categoryPercentage: 1,
						gridLines: {
							offsetGridLines: false
						}
					}]
				},
				elements: {
					rectangle: {
						borderWidth: 0,
					}
				},
				responsive: false,
				legend: {
		            display: false
				},
				tooltips: {
					enabled: false
				}
			}
		});
	},
	/**
	 * 値再設定
	 * @param  Object:グラフオブジェクト
	 * @param  Double：値
	 */
	setValue: function(_obj, _val){
		try {
			_obj.data.datasets[0].data[0] = _val;
			_obj.update();
		}catch(e){
			alert(e);
		}
	}
}
/** --------------------------------------------------------------------------------
 * ANP系縦棒グラフ
 */
chartCommon.barGraph = {
	/**
	 * 初期化
	 * @param  String：グラフ描写対象ID
	 * @param  json：jsonデータ
	 * @return Object:グラフオブジェクト
	 */
	init: function(_id, _json){
		var element		= document.getElementById(_id);
		if(!element) return;
		var datasets	= this.getDatasets(_json);
		var ticks		= this.getTicksSize(_json);

		return new Chart(document.getElementById(_id).getContext("2d"), {
			type: 'bar',
			data: {
				labels: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
				datasets: datasets
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					display: true,
					labels: {
						fontSize: 10,
						fontColor: chartCommon.basecolors[0]
					},
					onClick: function(e, legendItem){}
				},
				scales: {
					xAxes: [{
						gridLines: {
							color: chartCommon.basecolors[4],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[2]
						},
						ticks: {
							fontSize: 10,
							fontColor: chartCommon.basecolors[0]
						}
					}],
					yAxes: [{
						gridLines: {
							color: chartCommon.basecolors[3],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[2]
						},
						ticks: {
							fontSize: 10,
							fontColor: chartCommon.basecolors[0],
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step,
							callback: function(value, index, values) {
								return chartCommon.comma(value);
							}
						}
					}]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItems, data) {
							return data.datasets[tooltipItems.datasetIndex].label + ': ' + chartCommon.comma(tooltipItems.yLabel);
						}
					}
				}
			}
		});
	},
	/**
	 * JSONデータよりChart用データセットへ変換(内部処理)
	 * @param  json：jsonデータ
	 * @return json：Chart用データセット
	 */
	getDatasets: function(_json){
		var dataset		= [];
		var data		= {};

		for(var y = 0; y < _json.length; y++){
			data					= {};
			data.backgroundColor	= chartCommon.colors[y * 3];
			data.label				= _json[y].year + '年';
			data.data				= _json[y].val;
			dataset.push(data);
		}
		return dataset;
	},
	/**
	 * JSONデータより目盛りサイズを取得(内部処理)
	 * @param  json：jsonデータ
	 * @return json：目盛り情報
	 */
	getTicksSize: function(_json){
		var max			= Math.pow(-2,53);
		var gridCount	= 7;
		var gridSpan	= 10;
		var step		= 0;
		var num			= 0;
		var fix			= 0;
		var mod			= 0;

		for(var y = 0; y < _json.length; y++){
			max = Math.max(max, Math.max.apply(null, _json[y].val));
		}
		if(max == Math.pow(-2,53)) max = 0;

		// グリット計算
		mod = max % gridCount;
		num = (max - mod + gridCount) / gridCount;
		fix = Math.pow(10, String(num).length - 2) * gridSpan;
		mod = num % fix;
		step = num + fix - mod;

		max = step * gridCount;
		return {min: 0, max: max, step: step};
	}
}
/** --------------------------------------------------------------------------------
 * 商品内訳 縦棒積算グラフ
 */
chartCommon.sideBarIntegrationGraph = {
	/**
	 * 初期化
	 * @param  String：グラフ描写対象ID
	 * @param  json：jsonデータ
	 * @return Object:グラフオブジェクト
	 */
	init: function(_id, _json){
		var element		= document.getElementById(_id);
		if(!element) return;
		var datasets	= this.getDatasets(_json);
		var ticks		= this.getTicksSize(_json);
		var labels		= this.getLabels(_json);

		return new Chart(document.getElementById(_id).getContext("2d"), {
			type: 'horizontalBar',
			data: {
				labels: labels,
				datasets: datasets
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					display: true,
					position: 'right',
					labels: {
						boxWidth: 10,
						fontSize: 10,
						fontColor: chartCommon.basecolors[0]
					},
					onClick: function(e, legendItem){}
				},
				scales: {
					xAxes: [{
						stacked: true,
						gridLines: {
							LineWidth: 1,
							color: chartCommon.basecolors[3],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[3]
						},
						ticks: {
							fontSize: 10,
							fontColor: chartCommon.basecolors[0],
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step,
							callback: function(value, index, values) {
								return value + '%';
							}
						},
						afterCalculateTickRotation: function(self){
							self.labelRotation = 0;
						}

					}],
					yAxes: [{
						stacked: true,
						categoryPercentage: 0.5,
						gridLines: {
							color: chartCommon.basecolors[4]
						},
						ticks: {
							fontSize: 10,
							fontColor: chartCommon.basecolors[0]
						},
					}]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItems, data) {
							return data.datasets[tooltipItems.datasetIndex].label + ': ' + chartCommon.toFixed(tooltipItems.xLabel, 1) + '%';
						}
					}
				}
			}
		});
	},
	/**
	 * JSONデータよりChart用データセットへ変換(内部処理)
	 * @param  json：jsonデータ
	 * @return json：Chart用データセット
	 */
	getDatasets: function(_json){
		var dataset		= [];
		var data		= {};
		var sums		= [];

		for(var p = 0; p < _json.length; p++){
			for(var y = 0; y < _json[p].vals.length; y++){
				if(p == 0) sums.push(0);
				sums[y] += _json[p].vals[y].val;
			}
		}

		for(var p = 0; p < _json.length; p++){
			data					= {};
			data.label				= _json[p].product;
			data.borderWidth		= 1;
			data.backgroundColor	= chartCommon.colors[p];
			data.borderColor		= chartCommon.colors[p];
			data.data				= [];

			for(var y = 0; y < _json[p].vals.length; y++){
				data.data.push(_json[p].vals[y].val * 100 / sums[y]);
			}
			dataset.push(data);
		}
		return dataset;
	},
	/**
	 * JSONデータより目盛りサイズを取得(内部処理)
	 * @param  json：jsonデータ
	 * @return json：目盛り情報
	 */
	getTicksSize: function(_json){
/* 割合の場合 */
		return {min: 0, max: 100, step: 10};

/* 金額積算の場合
		var max			= Math.pow(-2,53);
		var step		= 100000;
		var nums		= [];

		for(var p = 0; p < _json.length; p++){
			for(var y = 0; y < _json[p].vals.length; y++){
				if(nums.length <= y) nums.push(0);
				nums[y] += _json[p].vals[y].val;
			}
		}
		max = Math.max(max, Math.max.apply(null, nums));
		max = Math.floor((max + step) / step) * step;
		return {min: 0, max: max, step: step};
*/
	},
	/**
	 * JSONデータよりラベルを(内部処理)
	 * @param  json：jsonデータ
	 * @return String[]：ラベル用配列
	 */
	getLabels: function(_json){
		var labels		= [];
		for(var y = 0; y < _json[0].vals.length; y++){
			labels.push(_json[0].vals[y].year + '年');
		}
		return labels;
	}

}
/** --------------------------------------------------------------------------------
 * AH 複合グラフ
 */
chartCommon.compositeAHGraph = {
	/**
	 * 初期化
	 * @param  String：グラフ描写対象ID
	 * @param  json：jsonデータ
	 * @return Object:グラフオブジェクト
	 */
	init: function(_id, _json){
		var element = document.getElementById(_id);
		if(!element) return;
		var datasets	= this.getDatasets(_json);
		var ticks		= this.getTicksSize(_json);
		var labels		= this.getLabels(_json);

		return new Chart(document.getElementById(_id).getContext("2d"), {
			type: 'bar',
			data: {
				labels: labels,
				datasets: datasets
			},
			options: {
				maintainAspectRatio: false,
				layout: {
					padding: {
						left: 5,
						right: 5,
						top: 15,
						bottom: 0,
					}
				},
				legend: {
					display: true,
					position: 'top',
					labels: {
						boxWidth: 10,
						fontSize: 10,
						fontColor: chartCommon.basecolors[0]
					},
					onClick: function(e, legendItem){}
				},
				scales: {
					xAxes: [{
						stacked: true,
						display: true,
						gridLines: {
							LineWidth: 1,
							color: chartCommon.basecolors[4],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[3]
						},
						ticks: {
							fontSize: 10,
							fontColor: chartCommon.basecolors[0]
						},
						afterCalculateTickRotation: function(self){
							//self.labelRotation = 45;
						}
					}],
					yAxes: [{
						stacked: true,
						gridLines: {
							lineWidth: 1,
							drawBorder: false,
							color: chartCommon.basecolors[3],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[2]
						},
						ticks: {
							/*fontSize: 10,
							fontColor: chartCommon.basecolors[0],
							beginAtZero: true,
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step
							callback: function(value, index, values) {
								return chartCommon.comma(value);
							}*/
							fontSize: 10,
							fontColor: chartCommon.basecolors[0],
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step,
							callback: function(value, index, values) {
								return chartCommon.comma(value);
							}
						}
					},{
						id: 'yline',
						stacked: false,
						display: false,
						ticks: {
							beginAtZero: true,
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step
						}
					}]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItems, data) {
							return data.datasets[tooltipItems.datasetIndex].label + ': ' + chartCommon.comma(tooltipItems.yLabel) + '円';
						}
					}
				}
			}
		});
	},
	/**
	 * JSONデータよりChart用データセットへ変換(内部処理)
	 * @param  json：jsonデータ
	 * @return json：Chart用データセット
	 */
	getDatasets: function(_json){
		var dataset		= [];
		var data		= {};

		data					= {};
		data.yAxisID			= 'yline';
		data.type				= 'line';
		data.label				= '振込金額(税込)';
		data.backgroundColor	= 'rgba(255, 255, 255, 0)';
		data.borderColor		= chartCommon.colors[8];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.data				= _json.transfer.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= '初年度手数料';
		data.backgroundColor	= chartCommon.colors[0];
		data.borderColor		= chartCommon.colors[0];
		data.borderWidth		= 1;
		data.data				= _json.fy.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= '次年度以降手数料';
		data.backgroundColor	= chartCommon.colors[1];
		data.borderColor		= chartCommon.colors[1];
		data.borderWidth		= 1;
		data.data				= _json.asy.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= 'その他(調整等)';
		data.backgroundColor	= chartCommon.colors[2];
		data.borderColor		= chartCommon.colors[2];
		data.borderWidth		= 1;
		data.data				= _json.other.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= '戻入手数料';
		data.backgroundColor	= chartCommon.colors[9];
		data.borderColor		= chartCommon.colors[9];
		data.borderWidth		= 1;
		data.data				= _json.reversion.val;
		dataset.push(data);

		return dataset;
	},
	/**
	 * JSONデータより目盛りサイズを取得(内部処理)
	 * @param  json：jsonデータ
	 * @return json：目盛り情報
	 */
	getTicksSize: function(_json){
		var min			= Math.pow(2,53);
		var max			= Math.pow(-2,53);
		var gridCount	= 7;
		var gridSpan	= 10;
		var step		= 0;
		var nums		= [];
		var num			= 0;
		var fix			= 0;
		var mod			= 0;

		for(var m = 0; m < _json.fy.val.length; m++){
			nums.push(_json.fy.val[m] + _json.asy.val[m] + _json.other.val[m]);
		}
		max = Math.max(max, Math.max.apply(null, _json.transfer.val));
		max = Math.max(max, Math.max.apply(null, nums));
		max = Math.max(max, Math.max.apply(null, _json.reversion.val));

		min = Math.min(min, Math.min.apply(null, _json.transfer.val));
		min = Math.min(min, Math.min.apply(null, nums));
		min = Math.min(min, Math.min.apply(null, _json.reversion.val));

		// グリット計算
		mod = max % gridCount;
		num = (max - mod + gridCount) / gridCount;
		fix = Math.pow(10, String(num).length - 2) * gridSpan;
		mod = num % fix;
		step = num + fix - mod;
		max = step * gridCount;

		mod = -min % step;
		min = min + mod - step;
		if(min >= 0) min = -step;
		return {min: min, max: max, step: step};

	},
	/**
	 * JSONデータよりラベルを(内部処理)
	 * @param  json：jsonデータ
	 * @return String[]：ラベル用配列
	 */
	getLabels: function(_json){
		var labels		= ['1月', '2月', '3月', '4月', '5月', '6月',
							'7月', '8月', '9月', '10月', '11月', '12月'];
//		var labels		= ['/1', '/2', '/3', '/4', '/5', '/6',
//							'/7', '/8', '/9', '/10', '/11', '/12'];
//		for(var m = 0; m < labels.length; m++){
//			labels[m] =  _json.transfer.year + labels[m];
//		}
		return labels;
	}
}
/** --------------------------------------------------------------------------------
 * AY 複合グラフ
 */
chartCommon.compositeAYGraph = {
	/**
	 * 初期化
	 * @param  String：グラフ描写対象ID
	 * @param  json：jsonデータ
	 * @return Object:グラフオブジェクト
	 */
	init: function(_id, _json){
		var element = document.getElementById(_id);
		if(!element) return;
		var datasets	= this.getDatasets(_json);
		var ticks		= this.getTicksSize(_json);
		var labels		= this.getLabels(_json);

		return new Chart(document.getElementById(_id).getContext("2d"), {
			type: 'bar',
			data: {
				labels: labels,
				datasets: datasets
			},
			options: {
				maintainAspectRatio: false,
				layout: {
					padding: {
						left: 5,
						right: 5,
						top: 15,
						bottom: 0,
					}
				},
				legend: {
					display: true,
					position: 'top',
					labels: {
						boxWidth: 10,
						fontSize: 10,
						fontColor: chartCommon.basecolors[0]
					},
					onClick: function(e, legendItem){}
				},
				scales: {
					xAxes: [{
						stacked: true,
						display: true,
						gridLines: {
							LineWidth: 1,
							color: chartCommon.basecolors[4],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[3]
						},
						ticks: {
							fontSize: 10,
							fontColor: chartCommon.basecolors[0]
						},
						afterCalculateTickRotation: function(self){
							//self.labelRotation = 45;
						}
					}],
					yAxes: [{
						stacked: true,
						gridLines: {
							lineWidth: 1,
							drawBorder: false,
							color: chartCommon.basecolors[3],
							zeroLineWidth: 2,
							zeroLineColor: chartCommon.basecolors[2]
						},
						ticks: {
							/*fontSize: 10,
							fontColor: chartCommon.basecolors[0],
							beginAtZero: true,
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step
							*/
							fontSize: 10,
							fontColor: chartCommon.basecolors[0],
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step,
							callback: function(value, index, values) {
								return chartCommon.comma(value);
							}
						}
					},{
						id: 'yline',
						stacked: false,
						display: false,
						ticks: {
							beginAtZero: true,
							min: ticks.min,
							max: ticks.max,
							stepSize: ticks.step
						}
					}]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItems, data) {
							return data.datasets[tooltipItems.datasetIndex].label + ': ' + chartCommon.comma(tooltipItems.yLabel) + '円';
						}
					}
				}
			}
		});
	},
	/**
	 * JSONデータよりChart用データセットへ変換(内部処理)
	 * @param  json：jsonデータ
	 * @return json：Chart用データセット
	 */
	getDatasets: function(_json){
		var dataset		= [];
		var data		= {};
//		var nums		= [];

/*		for(var m = 0; m < _json.fy.val.length; m++){
			nums.push(_json.fy.val[m] + _json.asy.val[m] + _json.other.val[m] + _json.reversion.val[m]);
		}*/

		data					= {};
		data.yAxisID			= 'yline';
		data.type				= 'line';
		data.label				= '手数料合計';
		data.backgroundColor	= 'rgba(255, 255, 255, 0)';
		data.borderColor		= chartCommon.colors[8];
		data.borderWidth		= 2;
		data.pointRadius		= 1;
		data.lineTension		= 0;
		data.fill				= false;
		data.data				= _json.transfer.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= '初年度手数料';
		data.backgroundColor	= chartCommon.colors[0];
		data.borderColor		= chartCommon.colors[0];
		data.borderWidth		= 1;
		data.data				= _json.fy.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= '次年度以降手数料';
		data.backgroundColor	= chartCommon.colors[1];
		data.borderColor		= chartCommon.colors[1];
		data.borderWidth		= 1;
		data.data				= _json.asy.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= 'その他(調整等)';
		data.backgroundColor	= chartCommon.colors[2];
		data.borderColor		= chartCommon.colors[2];
		data.borderWidth		= 1;
		data.data				= _json.other.val;
		dataset.push(data);

		data					= {};
		data.type				= 'bar';
		data.label				= '戻入手数料';
		data.backgroundColor	= chartCommon.colors[9];
		data.borderColor		= chartCommon.colors[9];
		data.borderWidth		= 1;
		data.data				= _json.reversion.val;
		dataset.push(data);

		return dataset;
	},
	/**
	 * JSONデータより目盛りサイズを取得(内部処理)
	 * @param  json：jsonデータ
	 * @return json：目盛り情報
	 */
	getTicksSize: function(_json){
		var min			= Math.pow(2,53);
		var max			= Math.pow(-2,53);
		var gridCount	= 7;
		var gridSpan	= 10;
		var step		= 0;
		var nums		= [];
		var num			= 0;
		var fix			= 0;
		var mod			= 0;

		for(var m = 0; m < _json.fy.val.length; m++){
			nums.push(_json.fy.val[m] + _json.asy.val[m] + _json.other.val[m]);
		}
		max = Math.max(max, Math.max.apply(null, _json.transfer.val));
		max = Math.max(max, Math.max.apply(null, nums));
		max = Math.max(max, Math.max.apply(null, _json.reversion.val));

		min = Math.min(min, Math.min.apply(null, _json.transfer.val));
		min = Math.min(min, Math.min.apply(null, nums));
		min = Math.min(min, Math.min.apply(null, _json.reversion.val));

		// グリット計算
		mod = max % gridCount;
		num = (max - mod + gridCount) / gridCount;
		fix = Math.pow(10, String(num).length - 2) * gridSpan;
		mod = num % fix;
		step = num + fix - mod;
		max = step * gridCount;

		mod = -min % step;
		min = min + mod - step;
		if(min >= 0) min = -step;
		return {min: min, max: max, step: step};

	},
	/**
	 * JSONデータよりラベルを(内部処理)
	 * @param  json：jsonデータ
	 * @return String[]：ラベル用配列
	 */
	getLabels: function(_json){
		var labels		= ['1月', '2月', '3月', '4月', '5月', '6月',
							'7月', '8月', '9月', '10月', '11月', '12月'];
//		var labels		= ['/1', '/2', '/3', '/4', '/5', '/6',
//							'/7', '/8', '/9', '/10', '/11', '/12'];
//		for(var m = 0; m < labels.length; m++){
//			labels[m] =  _json.transfer.year + labels[m];
//		}
		return labels;
	}
}