<apex:component controller="I_MRController" allowDML="true">

<apex:attribute name="MRFlag" type="Boolean" assignTo="{!isDispSR}" description="挙績情報表示フラグ"/>
<apex:attribute name="p_brNo" type="String" assignTo="{!brNo}" description="照会者コード"/>
<!--
	<header class="iris-owner-list-header">
		<h1 class="iris-owner-list-title">
			<apex:outputText value="■MR一覧" />
		</h1>

		<div class="iris-split-right">
			<apex:outputText value="{!TotalRows}件"/>
		</div>
	</header>
-->
	<apex:actionFunction action="{!sortRows}" name="apex_sortRow" rerender="MRList" status="sortCallStatus">
		<apex:param name="st" value="" />
	</apex:actionFunction>

	<br/>
	<h2 style="margin-bottom: 8px;">MR一覧</h2>
	<!-- データ基準日 -->
	<apex:outputText value="データ基準日：{!bizDate} 時点" style="font-size: 1rem; float: right; margin-bottom: 22px;"/>
	<apex:outputPanel layout="block" id="MRList">
	<div>
		<div class="d-inline-block container-flued hidden-lg-up" >
			<div class="form-group">
				<apex:selectList value="{!sortType}" readOnly="true" styleClass="custom-select iris-custom-select" multiselect="false" size="1" onchange="apex_sortRow(this.value);">
					<apex:selectOption itemValue="mrName" itemLabel="MR名"/>
					<apex:selectOption itemValue="mrCode" itemLabel="社員番号"/>

					<apex:selectOption itemValue="fs" itemLabel="FS(YTD)" rendered="{!isDispSR}"/>
					<apex:selectOption itemValue="pro" itemLabel="Protection(YTD)" rendered="{!isDispSR}"/>
					<apex:selectOption itemValue="coli" itemLabel="BA－COLI(YTD)" rendered="{!isDispSR}"/>
					<apex:selectOption itemValue="total" itemLabel="Total(YTD)" rendered="{!isDispSR}"/>
					<apex:selectOption itemValue="agencyCount" itemLabel="稼動事務所数" rendered="{!isDispSR}"/>
					<apex:selectOption itemValue="agentCount" itemLabel="稼動募集人数" rendered="{!isDispSR}"/>
				</apex:selectList>
				<button class="btn {!IF(sortIsAsc, 'btn-info', 'btn-warning')}" type="button" onclick="apex_sortRow('{!sortType}');">
					<apex:outputText value="{!IF(sortIsAsc, '昇順', '降順')}" />
				</button>
			</div>
		</div>
		<apex:actionStatus startStyleClass="p-l-1" id="sortCallStatus" layout="inline">
			<apex:facet name="start">
				<img height="34px" src="{!URLFOR($Resource.irisResources, 'images/icon-loading.gif')}" />
				<span>通信中...</span>
			</apex:facet>
		</apex:actionStatus>
	</div>

		<table id="table" class="table iris-table" data-type="iris-sortable" data-trigger=".iris-action-sort" data-function="apex_sortRow" style="word-wrap: break-word;">
			<thead>
				<tr>
					<th>
						<apex:variable var="MRNameClass" value="{!IF(sortType == 'mrName' , IF(sortIsAsc, 'asc', 'desc'), '')}" />
						<a href="?st=mrName" data-sort="mrName" class="iris-action-sort {!MRNameClass}">MR名</a>
					</th>
					<th>
						<apex:variable var="mrCodeClass" value="{!IF(sortType == 'mrCode' , IF(sortIsAsc, 'asc', 'desc'), '')}" />
						<a href="?st=mrCode" data-sort="mrCode" class="iris-action-sort {!mrCodeClass}">社員番号</a>
					</th>
					<apex:outputPanel rendered="{!isDispSR}" layout="none">
						<th>
							<apex:variable var="fsClass" value="{!IF(sortType == 'fs' , IF(sortIsAsc, 'asc', 'desc'), '')}"/>
							<a href="?st=fs" data-sort="fs" class="iris-action-sort {!fsClass}" >FS<br/>(YTD)</a>
						</th>
						<th>
							<apex:variable var="proClass" value="{!IF(sortType == 'pro' , IF(sortIsAsc, 'asc', 'desc'), '')}"/>
							<a href="?st=pro" data-sort="pro" class="iris-action-sort {!proClass}">Protection<br/>(YTD)</a>
						</th>
						<th>
							<apex:variable var="coliClass" value="{!IF(sortType == 'coli' , IF(sortIsAsc, 'asc', 'desc'), '')}" />
							<a href="?st=coli" data-sort="coli" class="iris-action-sort {!coliClass}">BA－COLI<br/>(YTD)</a>
						</th>
						<th>
							<apex:variable var="totalClass" value="{!IF(sortType == 'total' , IF(sortIsAsc, 'asc', 'desc'), '')}" />
							<a href="?st=total" data-sort="total" class="iris-action-sort {!totalClass}">Total<br/>(YTD)</a>
						</th>
						<th>
							<apex:variable var="agencyClass" value="{!IF(sortType == 'agencyCount' , IF(sortIsAsc, 'asc', 'desc'), '')}"/>
							<a href="?st=agencyCount" data-sort="agencyCount" class="iris-action-sort {!agencyClass}">稼動事務所数<br/>(月平均)</a>
						</th>
						<th>
							<apex:variable var="agentClass" value="{!IF(sortType == 'agentCount' , IF(sortIsAsc, 'asc', 'desc'), '')}"/>
							<a href="?st=agentCount" data-sort="agentCount" class="iris-action-sort {!agentClass}">稼動募集人数<br/>(月平均)</a>
						</th>
					</apex:outputPanel>
                    <th></th>
				</tr>
			</thead>
			<tbody>
				<apex:repeat value="{!MRRows}" var="mr" rows="{!rowCount}">
				<tr>
					<td title="MR名" style="vertical-align: middle;">
						<apex:outputPanel rendered="{!brNo<>'00'}">
						<a href="IRIS_SalesResults?mrid={!mr.mrId}&irismn={!$CurrentPage.parameters.irismn}">{!mr.mrName}</a>
						</apex:outputPanel>
						<apex:outputPanel rendered="{!brNo=='00'}">
							{!mr.mrName}
						</apex:outputPanel>
					</td>
					<td title="社員番号" style="vertical-align: middle;">
						<apex:outputText value="{!mr.mrCode}" />
					</td>
					<apex:outputPanel rendered="{!isDispSR}" layout="none">
						<td title="FS(YTD)" style="vertical-align: middle;" >
							<apex:outputText value="{0, number, ###,###}">
								<apex:param value="{!mr.fs}"/>
							</apex:outputText><br/>
							<apex:outputText value="({!mr.fsYTD}%)"/>
						</td>
						<td title="Protection(YTD)" style="vertical-align: middle;">
							<apex:outputText value="{0, number, ###,###}">
								<apex:param value="{!mr.pro}"/>
							</apex:outputText><br/>
							<apex:outputText value="({!mr.proYTD}%)"/>
						</td>
						<td title="BA－COLI(YTD)" style="vertical-align: middle;">
							<apex:outputText value="{0, number, ###,###}">
								<apex:param value="{!mr.coli}" />
							</apex:outputText><br/>
							<apex:outputText value="({!mr.coliYTD}%)"/>
						</td>
						<td title="Total(YTD)" style="vertical-align: middle;">
							<apex:outputText value="{0, number, ###,###}">
								<apex:param value="{!mr.total}"/>
							</apex:outputText><br/>
							<apex:outputText value="({!mr.totalYTD}%)" />
						</td>
						<td title="稼動事務所数(月平均)" style="vertical-align: middle;">
							<apex:outputText value="{0, number, ###,###}店">
								<apex:param value="{!mr.agencyCount}"/>
							</apex:outputText>
						</td>
						<td title="稼動募集人数(月平均)" style="vertical-align: middle;">
							<apex:outputText value="{0, number, ###,###}人">
								<apex:param value="{!mr.agentCount}"/>
							</apex:outputText>
						</td>
					</apex:outputPanel>
                    <td style="vertical-align: middle;padding: 0px;" class="iris-title-disable">
                        <apex:outputPanel rendered="{!AND(mr.eidcid<>null ,mr.mrId<>$User.Id)}">
                            <!--Changed by Michael Dorrian 2018/02/26:Changed commandLink value from Atria to 設計書作成-->
                            <apex:commandLink value="設計書作成>" styleClass="btn" onclick="Javascript:window.open('IRIS_AtriaRedirect?userid={!mr.mrId}','atria','toolbar=no,location=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes');return false;" action="#" style="border: 3px solid; border-radius: 7px;" target="_blank">
                                <!--<apex:param name="fid" value="{!agent.Id}" />-->
                            </apex:commandLink>
                        </apex:outputPanel>
                    </td>
				</tr>
				</apex:repeat>
			</tbody>
		</table>
	</apex:outputPanel>

	<apex:outputPanel rendered="{!rowCount < listMaxRows && rowCount < totalRows}" >
		<div class="iris-btn-panel">
			<apex:actionStatus id="moreCallStatus" layout="inline">
				<apex:facet name="stop">
					<apex:commandbutton styleClass="btn iris-lg-btn" action="{!addRows}" value="もっと見る" rerender="MRList" status="moreCallStatus"/>
				</apex:facet>
				<apex:facet name="start">
					<img src="{!URLFOR($Resource.irisResources, 'images/icon-loading.gif')}" />
				</apex:facet>
			</apex:actionStatus>
		</div>
	</apex:outputPanel>
<!--	<apex:actionRegion >
		<script type="text/javascript">
			//<!-- 初期処理 
			jQuery(document).ready(function(){
				createMRList();
			});
		</script>
		<apex:actionFunction action="{!readyAction}" name="createMRList" reRender="MRList">
		</apex:actionFunction>
	</apex:actionRegion>-->
</apex:component>