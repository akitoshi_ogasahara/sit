<apex:component id="iris_main_tube" layout="block">
	<apex:attribute type="I_TubeConnectHelper" name="helper" description=""/>
	<apex:attribute type="boolean" name="showPagenation" description="ページネーションを表示する場合TRUE" default="true"/>
	<apex:attribute type="boolean" name="showController" description="前後ボタンを表示する場合TRUE" default="true"/>
	<apex:attribute type="integer" name="size" description="カルーセルに表示するアイテムの数" default="3"/>

	<div class="iris-caroucel iris-sky-caroucel iris-tube">
		<apex:outputPanel id="iris_swiper_pagenation" layout="block" styleClass="iris-tube-swiper-pagenation iris-swiper-pagination" rendered="{!showPagenation}"></apex:outputPanel>
		<apex:outputPanel id="iris_swiper" layout="block" styleClass="iris-tube-container swiper-container iris-swiper-container">
			<apex:outputPanel id="iris_swiper_body" layout="block" styleClass="swiper-wrapper iris-swiper-wrapper"></apex:outputPanel>
		</apex:outputPanel>
		<apex:outputPanel id="iris_swiper_prev" layout="block" styleClass="iris-tube-swiper-prev iris-swiper-prev" rendered="{!showController}">
			<span class="iris-swiper-control-icon iris-icon iris-icon-caroucel-prev"></span>
		</apex:outputPanel>
		<apex:outputPanel id="iris_swiper_next" layout="block" styleClass="iris-tube-swiper-next iris-swiper-next" rendered="{!showController}">
			<span class="iris-swiper-control-icon iris-icon iris-icon-caroucel-next"></span>
		</apex:outputPanel>
	</div>


	<script>
		jQuery(function(){
			var swiperId = irisUtils.normalizeVFID('#{!$Component.iris_main_tube.iris_swiper}');
			var swiperWrapperId = irisUtils.normalizeVFID('#{!$Component.iris_main_tube.iris_swiper_body}');
			var pagenationId = irisUtils.normalizeVFID('#{!$Component.iris_main_tube.iris_swiper_pagenation}');
			var ctrlPrevId = irisUtils.normalizeVFID('#{!$Component.iris_main_tube.iris_swiper_prev}');
			var ctrlNextId = irisUtils.normalizeVFID('#{!$Component.iris_main_tube.iris_swiper_next}');
			var endpoint = '{!helper.TubeMetaUrl}';
			var $dom = irisUtils.createDom;
			var categorySwiper;
			(function() {
				var deferred = new jQuery.Deferred()
				try {
					var swiperOptions = {
						speed: 400,
						loop: true,
						autoplay: 4000,
						// centeredSlides: true,
						pagination: pagenationId,
						paginationClickable: true,
						excludedElements: 'button, input, select, textarea, a, .noSwipe',
						threshold: 10,
						// mousewheelControl: true,
						nextButton: ctrlNextId,
						prevButton: ctrlPrevId,
						buttonDisabledClass: 'iris-swiper-button-disable',
					};
					// 画面表示数の初期化
					var viewSize = parseInt('{!size}', 10);
					if (viewSize < 1 || !Number.isFinite(viewSize)) {
						viewSize = 3; // default
					}
					swiperOptions.spaceBetween = 10*viewSize;
					swiperOptions.slidesPerView = 1*viewSize;
					swiperOptions.slidesPerGroup = 1*viewSize;
					
					// レスポンシブ対応
					var minScreenWidth = 543;
					var padded = 224;
					var screenWidth = minScreenWidth + padded * (viewSize - 1)
					for (var index = viewSize; index > 0; index--, screenWidth -= padded) {
						var breakOption = {
							spaceBetween: 10*index,
							slidesPerView: 1*index,
							slidesPerGroup: 1*index,
						};
						// initialize
						if (!swiperOptions['breakpoints']) swiperOptions['breakpoints'] = {};
						swiperOptions['breakpoints'][screenWidth] = breakOption;
					}
					categorySwiper = new Swiper(swiperId, swiperOptions);
	
					if (!endpoint) {
						throw new Error(endpoint);
					}
					deferred.resolve(endpoint);
				} catch(exception) {
					deferred.reject(exception);
				}
				return deferred.promise();
			})().then(function retrieveMainMovieInfo() {
				return jQuery.ajax({
					type: 'GET',
					url: endpoint,
					data: {
						SessionId: '{!helper.SessionId}',
						ccd: '{!helper.ChannelCode}',
						sortorder: 'desc',
						pagesize: 9,
						//ecd: undefined,
						//ccode: undefined,
						//sword: undefined,
						recommend: 'on',
						type: '{!helper.TubeEnvType}',
						callback: 'callMainTube'
					},
					dataType: 'jsonp',
					jsonpCallback: 'callMainTube'
				});
			}).then(function(response) {
				var deferred = new jQuery.Deferred();
				try {
					var medias = response.meta;
					if (!medias) medias = [];
					if (!(medias instanceof Array)) medias = [medias];
					//動画リストが0件の場合のメッセージ
					if (medias.length < 1) {
						jQuery(swiperWrapperId).append($dom('div', '現在視聴可能な動画はございません。'))
					}
					var baseMovieUrl = '{!helper.TubeTopUrl}?{!helper.Params_MovieType}&mid=';
					jQuery.each(medias, function(index, media) {
						var element =
						$dom('div', {class: 'card iris-card swiper-slide iris-library-item'}, function() {
							$dom('div', {class: 'iris-card-thumbnail'}, function() {
								if (media.mid) {
									$dom('a', {href: baseMovieUrl+media.mid, target: '_blank',title: media.title, class: 'iris-nav-link'}, function() {
										$dom('img', {src: media.thumbnail_url_ssl, alt: media.title});
									});
								} else {
									$dom('img', {src: media.thumbnail_url_ssl, alt: media.title});
								}
							});
							$dom('div', {class: 'card-block iris-card-block'}, function() {
								$dom('div', {class: 'iris-library-item-content'}, function() {
									$dom('div', {class: 'card-title iris-card-title'}, function() {
										if (media.mid) {
											$dom('a', media.title, {href: baseMovieUrl+media.mid, target: '_blank',title: media.title, class: 'iris-nav-link'});
										} else {
											$dom('span', media.title);
										}
									});
									$dom('div', (media.long_description || '').replace(/\<br\/?\>/igm, '\r\n'), {class: 'iris-card-description'});
								});
							});
						});
						categorySwiper.appendSlide(element);
					});
					if (categorySwiper.params.loop) {
						// カルーセルの表示を先頭に移動させる。
						// ループ用のダミー要素が先頭に追加されているため
						// 0から数えた数を先頭のIndex値とする。
						categorySwiper.slideTo(categorySwiper.params.slidesPerView, 0);
					}
					deferred.resolve();
				} catch(err) {
					deferred.reject(err);
				}
				return deferred.promise();
			}).catch(function(err) {
				var $wrapepr = jQuery(swiperWrapperId)
				$wrapepr.append($dom('div', 'NN Tubeに接続できませんでした。'))
			});
		});
	</script>

</apex:component>