<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IRIS_CMS_DomId</fullName>
        <field>DOM_Id__c</field>
        <formula>FormNo__c</formula>
        <name>【IRIS CMSコンテンツ】DomId値の設定</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IRIS_CMS_NEW_VALID_TO</fullName>
        <field>NewValidTo__c</field>
        <formula>DisplayFrom__c + 60</formula>
        <name>【IRIS CMSコンテンツ】New表示期限</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>I_UPDATE_DOCUMENTNO</fullName>
        <field>DocumentAndBranchNo__c</field>
        <formula>DocumentNo__c</formula>
        <name>【IRIS CMSコンテンツ】資料管理番号+枝番値の設定</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>I_UPDATE_DOMID</fullName>
        <field>DOM_Id__c</field>
        <formula>DocumentNo__c</formula>
        <name>【IRIS CMSコンテンツ】DomId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>I_UPDATE_KEY</fullName>
        <field>UpsertKey__c</field>
        <formula>DispAs__c &amp;&quot;|&quot;&amp;Name&amp;&quot;|&quot;&amp;DOM_Id__c</formula>
        <name>【IRIS CMSコンテンツ】外部ID値の設定</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>I_UPDATE_KEY_ST</fullName>
        <description>販売促進資料用</description>
        <field>UpsertKey__c</field>
        <formula>DispAs__c &amp; &quot;|&quot; &amp; Name &amp; &quot;|&quot; &amp; DocumentNo__c</formula>
        <name>【IRIS CMSコンテンツ】外部ID値の設定(販売促進資料)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>I_CMSContentsDocumentNo</fullName>
        <actions>
            <name>I_UPDATE_DOCUMENTNO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>暫定対応：資料管理番号+枝番をセットする</description>
        <formula>ParentContent__r.Name = &apos;セールスツール&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>I_CMSContentsDomIdSet</fullName>
        <actions>
            <name>IRIS_CMS_DomId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>パンフレット等の資料・動画掲載、パンフレット等発送依頼用</description>
        <formula>OR(ParentContent__r.Name == &apos;ご契約のしおり・約款&apos;,ParentContent__r.Name == &apos;ニード喚起資料等&apos;,ParentContent__r.Name == &apos;パンフレット／特に重要なお知らせ&apos;,!ISPICKVAL(DocumentCategory__c, &apos;&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>I_CMSContentsSetDocumentRequest</fullName>
        <actions>
            <name>IRIS_CMS_NEW_VALID_TO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>パンフレット等発送依頼用
New掲示期限が空の時、掲載開始+60をセット</description>
        <formula>AND(   !ISPICKVAL(DocumentCategory__c, &apos;&apos;),   ISBLANK( NewValidTo__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>I_CMSContentsSetSalesTool</fullName>
        <actions>
            <name>IRIS_CMS_NEW_VALID_TO</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>I_UPDATE_DOMID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>I_UPDATE_KEY_ST</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DomIdに資料管理番号をセット
New掲示期限に掲載開始+60をセット</description>
        <formula>ParentContent__r.Name = &apos;販売促進資料&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>I_CMSContentsUpsertKeySet</fullName>
        <actions>
            <name>I_UPDATE_KEY</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ParentContent__r.Name != &apos;販売促進資料&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
