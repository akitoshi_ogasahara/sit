<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>I_UpsertKeySet</fullName>
        <description>IRISメニューの「UpsertKey」を更新する</description>
        <field>UpsertKey__c</field>
        <formula>Text( MainCategory__c ) &amp; &apos;|&apos; &amp; Text( SubCategory__c ) &amp; &apos;|&apos; &amp; ParentMenu__r.Name &amp; &apos;|&apos; &amp;  Name &amp; &apos;|&apos; &amp; DestURL__c</formula>
        <name>【IRISメニュー】UpsertKey更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>I_MenuMasterUpsertKeySet</fullName>
        <actions>
            <name>I_UpsertKeySet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>IRISメニューマスタのUpsertKeyをセットする</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
