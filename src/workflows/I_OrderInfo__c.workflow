<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>DocumentOrderMA</fullName>
        <description>パンフレット等発送依頼</description>
        <protected>false</protected>
        <recipients>
            <field>EMail__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>iris-info@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Folder_Notification/DocumentOrderMT</template>
    </alerts>
    <rules>
        <fullName>パンフレット等発送依頼受付メール送信</fullName>
        <actions>
            <name>DocumentOrderMA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>I_OrderInfo__c.EMail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
