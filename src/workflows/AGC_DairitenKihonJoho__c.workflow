<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AGC_AgentSearchKeyUpdate</fullName>
        <field>KihonSearchKey__c</field>
        <formula>BranchID__c &amp;&apos; &apos;&amp; AgencyID__c &amp;&apos; &apos;&amp; AgencyName__c &amp;&apos; &apos;&amp; AgencyKanaName__c &amp;&apos; &apos;&amp;
 AgencyTel1__c &amp;&apos; &apos;&amp; AgencyFax__c &amp;&apos; &apos;&amp; BotenID__c &amp;&apos; &apos;&amp;  AgentName__c  &amp;&apos; &apos;&amp;  AgentKanaName__c  &amp;&apos; &apos;&amp; IF(CONTAINS(AgencyTel1__c,&apos;-&apos;), SUBSTITUTE(AgencyTel1__c,&apos;-&apos;,&apos;&apos;), &apos;&apos;) &amp;&apos; &apos;&amp; IF(CONTAINS(AgencyFax__c,&apos;-&apos;), SUBSTITUTE(AgencyFax__c,&apos;-&apos;,&apos;&apos;), &apos;&apos;)</formula>
        <name>代理店基本情報検索キー更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AGC_AgentKihonSearchkeyUpdate</fullName>
        <actions>
            <name>AGC_AgentSearchKeyUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>AG代理店事務所情報が新規登録、更新された場合に実行する。</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
