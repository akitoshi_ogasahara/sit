<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LMT_CloseReport_Visit_Date_EmailAlert</fullName>
        <description>【LMT】CloseReport_Visit_Date_EmailAlert</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <senderAddress>nn-project@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CRP_LMT/LMT_CloseReport_Visit_Date_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Unsubmitted_Report_Alert</fullName>
        <description>【LMT】Unsubmitted Report Alert</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <senderAddress>nn-project@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CRP_LMT/LMT_FirstVisitReportUnsubmittedMail</template>
    </alerts>
    <fieldUpdates>
        <fullName>LMT_Keep_Sent_Document</fullName>
        <description>資料送付済みに一度なったレコードは「資料送付」を他のステータスに更新できない</description>
        <field>SentDocument__c</field>
        <literalValue>資料送付済み</literalValue>
        <name>【LMT】Keep Sent Document</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_Keep_Status_Sent_EN</fullName>
        <description>状況をSentから更新させない</description>
        <field>Status</field>
        <literalValue>Sent</literalValue>
        <name>【LMT】Keep Status Sent_EN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_Keep_Status_Sent_JPN</fullName>
        <description>状況を送信から更新させない</description>
        <field>Status</field>
        <literalValue>送信</literalValue>
        <name>【LMT】Keep Status Sent_JPN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_Set_Toss_Up_Date</fullName>
        <field>Tosss_Up_Date__c</field>
        <formula>LastModifiedDate +   30</formula>
        <name>【LMT】Set Toss Up Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_UpdateMailAlertDate</fullName>
        <field>MailAlertField__c</field>
        <formula>IF(NOT( ISBLANK(MailAlertField__c) )  , IF( MONTH( MailAlertField__c ) = 11, DATE( YEAR( MailAlertField__c ), 12, 31 ), IF( MONTH( MailAlertField__c ) = 12, DATE( YEAR( MailAlertField__c )+1, 01, 31 ), DATE( YEAR( MailAlertField__c ), MONTH( MailAlertField__c ) + 2, 1) - 1)), MailAlertField__c)</formula>
        <name>【LMT】UpdateMailAlertDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_UpdateMailAlertFieldOnDateChange</fullName>
        <field>MailAlertField__c</field>
        <formula>IF( MONTH( DateForCloseReportWorkFlow__c ) = 12, 
DATE( YEAR( DateForCloseReportWorkFlow__c ), 12, 31 ), 
DATE( YEAR( DateForCloseReportWorkFlow__c ), MONTH( DateForCloseReportWorkFlow__c ) + 1, 1) - 1)</formula>
        <name>【LMT】UpdateMailAlertFieldOnDateChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LMT_Update_Management_No</fullName>
        <description>施策メンバー「管理番号」を見込顧客管理番号1の値で記録する</description>
        <field>ManagementNo__c</field>
        <formula>Lead.ManagementNo1__c</formula>
        <name>【LMT】 Update Management No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LMT CHECK UNSUBMITTED LEAD FIRST VISIT REPORT ONLINE_ORPHAN</fullName>
        <active>true</active>
        <description>帝国テレマ以外の場合、トスアップ日を一ヶ月経過し、商談状況が「対応完了」以外の場合、にアラートメールをリード所有者に送信する。</description>
        <formula>IF( ( ISPICKVAL(Campaign.Type ,&quot;オンライン&quot;) ||  ISPICKVAL(Campaign.Type ,&quot;クロスセル・アップセル&quot;) ) &amp;&amp; ISPICKVAL(checkVisited__c ,&quot;&quot;) &amp;&amp;   NOT(ISPICKVAL(Opportunity_Status__c ,&quot;99&quot;)) ,true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Unsubmitted_Report_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>CampaignMember.Tosss_Up_Date__c</offsetFromField>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LMT CHECK UNSUBMITTED LEAD FIRST VISIT REPORT TELEMARKETING</fullName>
        <active>true</active>
        <description>帝国テレマの場合、募集人の訪問後、訪問希望日の7日後、かつ商談状況が「対応完了」ではない場合にアラートメールを見込顧客の担当者に送信する</description>
        <formula>IF( ( ISPICKVAL(Campaign.Type ,&quot;テレマーケティング&quot;) ) &amp;&amp;  ISPICKVAL(checkVisited__c ,&quot;&quot;) &amp;&amp;  NOT(ISPICKVAL(Opportunity_Status__c ,&quot;99&quot;)),true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Unsubmitted_Report_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>CampaignMember.Check_Report_Date__c</offsetFromField>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LMT CHECK UPDATE CAMPAIGNMEMBERSTATUS_EN</fullName>
        <actions>
            <name>LMT_Keep_Status_Sent_EN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>反社チェック「有り」のレコードは状況を「Responded」に更新できない</description>
        <formula>ISCHANGED(Status )  &amp;&amp;  ISPICKVAL(PRIORVALUE(Status),&quot;Sent&quot; )   &amp;&amp; ISPICKVAL(AntiCompany_Check__c, &quot;有り&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMT CHECK UPDATE CAMPAIGNMEMBERSTATUS_JPN</fullName>
        <actions>
            <name>LMT_Keep_Status_Sent_JPN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>反社チェック「有り」のレコードは状況を「レスポンスあり」に更新できない</description>
        <formula>ISCHANGED(Status )  &amp;&amp;  (ISPICKVAL(PRIORVALUE(Status),&quot;送信&quot; )  ) &amp;&amp;  ISPICKVAL(AntiCompany_Check__c, &quot;有り&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMT CHECK UPDATE DOCUMENT SENT</fullName>
        <actions>
            <name>LMT_Keep_Sent_Document</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>資料送付済みになった後は他のステータスに変更できない。</description>
        <formula>ISCHANGED(SentDocument__c ) &amp;&amp;  ISPICKVAL(PRIORVALUE(SentDocument__c ), &quot;資料送付済み&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMT RECORD MANAGEMENT NO</fullName>
        <actions>
            <name>LMT_Update_Management_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>管理番号をリードから取得し施策時の履歴として保持する。</description>
        <formula>IF( Lead.ManagementNo1__c != &quot;&quot;,TRUE,FALSE)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LMT RECORD TOSSUP DATE</fullName>
        <actions>
            <name>LMT_Set_Toss_Up_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>トスアップされた（状況がRespondedになった）日付を記録する。</description>
        <formula>IF(  NOT( ISPICKVAL(Campaign.Type , &quot;テレマーケティング&quot;) ) &amp;&amp; ISCHANGED( Status ) , IF(ISPICKVAL(Status , &quot;Responded&quot;) || ISPICKVAL(Status , &quot;レスポンスあり&quot;)  ,True,False), False)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LMT_CLOSEREPORT_VISIT_DATE_REMAINDER</fullName>
        <active>true</active>
        <description>成約レポートリマインド</description>
        <formula>IF( ISPICKVAL(checkVisited__c, &quot;01&quot;) &amp;&amp; NOT(ISPICKVAL(Opportunity_Status__c , &quot;99&quot;))   ,true,false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>LMT_CloseReport_Visit_Date_EmailAlert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>LMT_UpdateMailAlertDate</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>CampaignMember.MailAlertField__c</offsetFromField>
            <timeLength>33</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>LMT_MAILALERT_FIELD_UPDATE</fullName>
        <actions>
            <name>LMT_UpdateMailAlertFieldOnDateChange</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>成約レポートリマインダー初回日付設定用。</description>
        <formula>ISCHANGED(  DateForCloseReportWorkFlow__c  )  &amp;&amp;  NOT( ISBLANK(DateForCloseReportWorkFlow__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
