<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ActionEmail</fullName>
        <description>【不備明細】承認メール</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/KeiyakuSyounin</template>
    </alerts>
    <alerts>
        <fullName>KyakkaEmail</fullName>
        <description>【不備明細】却下メール</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/KeiyakuKyakka</template>
    </alerts>
    <fieldUpdates>
        <fullName>ActionFieldUpdate</fullName>
        <field>Status__c</field>
        <literalValue>回答済み</literalValue>
        <name>【不備】ステータス更新（回答済み）</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ActionFieldUpdate2</fullName>
        <field>MRAnswerUser__c</field>
        <formula>$User.LastName &amp;&quot; &quot;&amp; $User.FirstName</formula>
        <name>【明細】入力者更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ActionFieldUpdateDate</fullName>
        <field>ResponseDate__c</field>
        <formula>TODAY()</formula>
        <name>【不備明細】回答日入力</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed</fullName>
        <field>Status__c</field>
        <literalValue>不備回答受領済</literalValue>
        <name>【契約】ステータス更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FLG0</fullName>
        <field>Temp__c</field>
        <formula>0</formula>
        <name>【不備明細】回答可能不備FLG　０更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FLGUpdate</fullName>
        <field>Temp__c</field>
        <formula>1</formula>
        <name>【不備明細】回答可能不備FLG更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>KaigyouUpdate</fullName>
        <field>ResponseMatter__c</field>
        <formula>IF(ISBLANK(PRIORVALUE(ResponseMatter__c)),
IF(LEN(ResponseMatter__c) &gt;120, 
LEFT( ResponseMatter__c,40 ) &amp; BR() &amp; 
MID(ResponseMatter__c, 41, 40) &amp; BR() &amp; 
MID(ResponseMatter__c, 81, 40) &amp; BR() &amp; 
MID(ResponseMatter__c, 121, 40) 
, 
IF(LEN(ResponseMatter__c) &gt;80, 
LEFT( ResponseMatter__c,40 ) &amp; BR() &amp; 
MID(ResponseMatter__c, 41, 40) &amp; BR() &amp; 
MID(ResponseMatter__c, 81, 40) 
, 
IF(LEN(ResponseMatter__c) &gt;40, 
LEFT( ResponseMatter__c,40 ) &amp; BR() &amp; 
MID(ResponseMatter__c, 41, 40) 
,ResponseMatter__c
) 
) 
)
,PRIORVALUE(ResponseMatter__c))</formula>
        <name>【不備明細】改行更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No</fullName>
        <field>Name</field>
        <formula>PRIORVALUE(Name)</formula>
        <name>【不備明細】No更新</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SHOUNIN_DATE_SET</fullName>
        <field>Shounin_DATE__c</field>
        <formula>TODAY()</formula>
        <name>【不備明細】承認日セット</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Shounin_Set</fullName>
        <field>Shounin__c</field>
        <formula>$User.LastName &amp;&quot; &quot; &amp;  $User.FirstName</formula>
        <name>【不備明細】承認者セット</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>%E3%80%90明細%E3%80%91No更新</fullName>
        <actions>
            <name>No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK( PRIORVALUE( Name ))), ISCHANGED(Name ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%E3%80%90明細%E3%80%91回答可能不備FLG更新</fullName>
        <actions>
            <name>FLGUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KaigyouUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(CASE(DefectCode__c,  &quot;AB6&quot;,&quot;true&quot;, &quot;BA1&quot;,&quot;true&quot;, &quot;BA2&quot;,&quot;true&quot;, &quot;BA3&quot;,&quot;true&quot;, &quot;BA4&quot;,&quot;true&quot;, &quot;BA5&quot;,&quot;true&quot;, &quot;BA6&quot;,&quot;true&quot;, &quot;BA7&quot;,&quot;true&quot;, &quot;BA8&quot;,&quot;true&quot;, &quot;BA9&quot;,&quot;true&quot;, &quot;BB1&quot;,&quot;true&quot;, &quot;BB2&quot;,&quot;true&quot;, &quot;BB4&quot;,&quot;true&quot;, &quot;BB5&quot;,&quot;true&quot;, &quot;BB6&quot;,&quot;true&quot;, &quot;BB7&quot;,&quot;true&quot;, &quot;BB9&quot;,&quot;true&quot;, &quot;BC1&quot;,&quot;true&quot;, &quot;BC2&quot;,&quot;true&quot;, &quot;BC3&quot;,&quot;true&quot;, &quot;BC4&quot;,&quot;true&quot;, &quot;BC5&quot;,&quot;true&quot;, &quot;BC6&quot;,&quot;true&quot;, &quot;BC7&quot;,&quot;true&quot;, &quot;BC9&quot;,&quot;true&quot;, &quot;BD1&quot;,&quot;true&quot;, &quot;BD2&quot;,&quot;true&quot;, &quot;BD3&quot;,&quot;true&quot;, &quot;BD4&quot;,&quot;true&quot;, &quot;BD5&quot;,&quot;true&quot;, &quot;BD6&quot;,&quot;true&quot;, &quot;BD7&quot;,&quot;true&quot;, &quot;BD8&quot;,&quot;true&quot;, &quot;BD9&quot;,&quot;true&quot;, &quot;BE1&quot;,&quot;true&quot;, &quot;BE2&quot;,&quot;true&quot;, &quot;BE3&quot;,&quot;true&quot;, &quot;BE4&quot;,&quot;true&quot;, &quot;BE5&quot;,&quot;true&quot;, &quot;BE8&quot;,&quot;true&quot;, &quot;BE9&quot;,&quot;true&quot;, &quot;KA1&quot;,&quot;true&quot;, &quot;KA2&quot;,&quot;true&quot;, &quot;KA3&quot;,&quot;true&quot;, &quot;KA4&quot;,&quot;true&quot;, &quot;KA5&quot;,&quot;true&quot;, &quot;KA6&quot;,&quot;true&quot;, &quot;KA7&quot;,&quot;true&quot;, &quot;KA8&quot;,&quot;true&quot;, &quot;KA9&quot;,&quot;true&quot;, &quot;KB1&quot;,&quot;true&quot;, &quot;KB2&quot;,&quot;true&quot;, &quot;KB4&quot;,&quot;true&quot;, &quot;KB5&quot;,&quot;true&quot;, &quot;KB6&quot;,&quot;true&quot;, &quot;KB7&quot;,&quot;true&quot;, &quot;KB8&quot;,&quot;true&quot;, &quot;KB9&quot;,&quot;true&quot;, &quot;KC1&quot;,&quot;true&quot;, &quot;KC2&quot;,&quot;true&quot;, &quot;KC3&quot;,&quot;true&quot;, &quot;KC4&quot;,&quot;true&quot;, &quot;KC5&quot;,&quot;true&quot;, &quot;KC6&quot;,&quot;true&quot;, &quot;KC7&quot;,&quot;true&quot;, &quot;KC8&quot;,&quot;true&quot;, &quot;KC9&quot;,&quot;true&quot;, &quot;KD1&quot;,&quot;true&quot;, &quot;KD2&quot;,&quot;true&quot;, &quot;KD5&quot;,&quot;true&quot;, &quot;KD6&quot;,&quot;true&quot;, &quot;KE4&quot;,&quot;true&quot;, &quot;SA3&quot;,&quot;true&quot;, &quot;SA5&quot;,&quot;true&quot;,&quot;DA3&quot;,&quot;true&quot;, &quot;EA4&quot;,&quot;true&quot;, &quot;OA2&quot;,&quot;true&quot;, &quot;WA1&quot;,&quot;true&quot;, &quot;WA2&quot;,&quot;true&quot;, &quot;WA3&quot;,&quot;true&quot;, &quot;WA4&quot;,&quot;true&quot;, &quot;WA5&quot;,&quot;true&quot;, &quot;WA6&quot;,&quot;true&quot;, &quot;WA7&quot;,&quot;true&quot;, &quot;WA8&quot;,&quot;true&quot;, &quot;WA9&quot;,&quot;true&quot;, &quot;WB1&quot;,&quot;true&quot;, &quot;WB2&quot;,&quot;true&quot;, &quot;WB3&quot;,&quot;true&quot;, &quot;WB4&quot;,&quot;true&quot;, &quot;WB5&quot;,&quot;true&quot;, &quot;WB6&quot;,&quot;true&quot;, &quot;WB7&quot;,&quot;true&quot;, &quot;WB8&quot;,&quot;true&quot;, &quot;WC1&quot;,&quot;true&quot;, &quot;AF4&quot;,&quot;true&quot;, &quot;AF5&quot;,&quot;true&quot;, &quot;AF6&quot;,&quot;true&quot;, &quot;AF7&quot;,&quot;true&quot;, &quot;AF8&quot;,&quot;true&quot;, &quot;AF9&quot;,&quot;true&quot;, &quot;AG1&quot;,&quot;true&quot;, &quot;AG2&quot;,&quot;true&quot;, &quot;AG3&quot;,&quot;true&quot;, &quot;JA2&quot;,&quot;true&quot;, &quot;BF1&quot;,&quot;true&quot;, &quot;BF2&quot;,&quot;true&quot;, &quot;KD7&quot;,&quot;true&quot;, &quot;KD8&quot;,&quot;true&quot;, &quot;AA2&quot;,&quot;true&quot;, &quot;BF8&quot;,&quot;true&quot;, &quot;BG2&quot;,&quot;true&quot;, &quot;IA3&quot;,&quot;true&quot;, &quot;IA6&quot;,&quot;true&quot;, &quot;XA1&quot;,&quot;true&quot;, &quot;false&quot;)  = &quot;true&quot;,true,false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%E3%80%90明細%E3%80%91回答可能不備FLG更新2</fullName>
        <actions>
            <name>FLG0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>KaigyouUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>変更になった場合フラグを落とす。</description>
        <formula>IF(CASE(DefectCode__c,  &quot;AB6&quot;,&quot;true&quot;, &quot;BA1&quot;,&quot;true&quot;, &quot;BA2&quot;,&quot;true&quot;, &quot;BA3&quot;,&quot;true&quot;, &quot;BA4&quot;,&quot;true&quot;, &quot;BA5&quot;,&quot;true&quot;, &quot;BA6&quot;,&quot;true&quot;, &quot;BA7&quot;,&quot;true&quot;, &quot;BA8&quot;,&quot;true&quot;, &quot;BA9&quot;,&quot;true&quot;, &quot;BB1&quot;,&quot;true&quot;, &quot;BB2&quot;,&quot;true&quot;, &quot;BB4&quot;,&quot;true&quot;, &quot;BB5&quot;,&quot;true&quot;, &quot;BB6&quot;,&quot;true&quot;, &quot;BB7&quot;,&quot;true&quot;, &quot;BB9&quot;,&quot;true&quot;, &quot;BC1&quot;,&quot;true&quot;, &quot;BC2&quot;,&quot;true&quot;, &quot;BC3&quot;,&quot;true&quot;, &quot;BC4&quot;,&quot;true&quot;, &quot;BC5&quot;,&quot;true&quot;, &quot;BC6&quot;,&quot;true&quot;, &quot;BC7&quot;,&quot;true&quot;, &quot;BC9&quot;,&quot;true&quot;, &quot;BD1&quot;,&quot;true&quot;, &quot;BD2&quot;,&quot;true&quot;, &quot;BD3&quot;,&quot;true&quot;, &quot;BD4&quot;,&quot;true&quot;, &quot;BD5&quot;,&quot;true&quot;, &quot;BD6&quot;,&quot;true&quot;, &quot;BD7&quot;,&quot;true&quot;, &quot;BD8&quot;,&quot;true&quot;, &quot;BD9&quot;,&quot;true&quot;, &quot;BE1&quot;,&quot;true&quot;, &quot;BE2&quot;,&quot;true&quot;, &quot;BE3&quot;,&quot;true&quot;, &quot;BE4&quot;,&quot;true&quot;, &quot;BE5&quot;,&quot;true&quot;, &quot;BE8&quot;,&quot;true&quot;, &quot;BE9&quot;,&quot;true&quot;, &quot;KA1&quot;,&quot;true&quot;, &quot;KA2&quot;,&quot;true&quot;, &quot;KA3&quot;,&quot;true&quot;, &quot;KA4&quot;,&quot;true&quot;, &quot;KA5&quot;,&quot;true&quot;, &quot;KA6&quot;,&quot;true&quot;, &quot;KA7&quot;,&quot;true&quot;, &quot;KA8&quot;,&quot;true&quot;, &quot;KA9&quot;,&quot;true&quot;, &quot;KB1&quot;,&quot;true&quot;, &quot;KB2&quot;,&quot;true&quot;, &quot;KB4&quot;,&quot;true&quot;, &quot;KB5&quot;,&quot;true&quot;, &quot;KB6&quot;,&quot;true&quot;, &quot;KB7&quot;,&quot;true&quot;, &quot;KB8&quot;,&quot;true&quot;, &quot;KB9&quot;,&quot;true&quot;, &quot;KC1&quot;,&quot;true&quot;, &quot;KC2&quot;,&quot;true&quot;, &quot;KC3&quot;,&quot;true&quot;, &quot;KC4&quot;,&quot;true&quot;, &quot;KC5&quot;,&quot;true&quot;, &quot;KC6&quot;,&quot;true&quot;, &quot;KC7&quot;,&quot;true&quot;, &quot;KC8&quot;,&quot;true&quot;, &quot;KC9&quot;,&quot;true&quot;, &quot;KD1&quot;,&quot;true&quot;, &quot;KD2&quot;,&quot;true&quot;, &quot;KD5&quot;,&quot;true&quot;, &quot;KD6&quot;,&quot;true&quot;, &quot;KE4&quot;,&quot;true&quot;, &quot;SA3&quot;,&quot;true&quot;, &quot;SA5&quot;,&quot;true&quot;, &quot;DA3&quot;,&quot;true&quot;, &quot;EA4&quot;,&quot;true&quot;, &quot;OA2&quot;,&quot;true&quot;, &quot;WA1&quot;,&quot;true&quot;, &quot;WA2&quot;,&quot;true&quot;, &quot;WA3&quot;,&quot;true&quot;, &quot;WA4&quot;,&quot;true&quot;, &quot;WA5&quot;,&quot;true&quot;, &quot;WA6&quot;,&quot;true&quot;, &quot;WA7&quot;,&quot;true&quot;, &quot;WA8&quot;,&quot;true&quot;, &quot;WA9&quot;,&quot;true&quot;, &quot;WB1&quot;,&quot;true&quot;, &quot;WB2&quot;,&quot;true&quot;, &quot;WB3&quot;,&quot;true&quot;, &quot;WB4&quot;,&quot;true&quot;, &quot;WB5&quot;,&quot;true&quot;, &quot;WB6&quot;,&quot;true&quot;, &quot;WB7&quot;,&quot;true&quot;, &quot;WB8&quot;,&quot;true&quot;, &quot;WC1&quot;,&quot;true&quot;, &quot;AF4&quot;,&quot;true&quot;, &quot;AF5&quot;,&quot;true&quot;, &quot;AF6&quot;,&quot;true&quot;, &quot;AF7&quot;,&quot;true&quot;, &quot;AF8&quot;,&quot;true&quot;, &quot;AF9&quot;,&quot;true&quot;, &quot;AG1&quot;,&quot;true&quot;, &quot;AG2&quot;,&quot;true&quot;, &quot;AG3&quot;,&quot;true&quot;, &quot;JA2&quot;,&quot;true&quot;, &quot;BF1&quot;,&quot;true&quot;, &quot;BF2&quot;,&quot;true&quot;, &quot;KD7&quot;,&quot;true&quot;, &quot;KD8&quot;,&quot;true&quot;, &quot;AA2&quot;,&quot;true&quot;, &quot;BF8&quot;,&quot;true&quot;, &quot;BG2&quot;,&quot;true&quot;, &quot;IA3&quot;,&quot;true&quot;, &quot;IA6&quot;,&quot;true&quot;, &quot;XA1&quot;,&quot;true&quot;, &quot;false&quot;)  = &quot;true&quot;,false,true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>%E3%80%90明細%E3%80%91回答日更新</fullName>
        <actions>
            <name>ActionFieldUpdate2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ActionFieldUpdateDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DefectInfo__c.ResponseMatter__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>DefectInfo__c.DefectCode__c</field>
            <operation>notEqual</operation>
            <value>BF1,BF2,JA2,KD7,KD8,TF2,TF3,WB6</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>%E3%80%90明細%E3%80%91回答日更新%E3%80%80承認</fullName>
        <actions>
            <name>ActionFieldUpdate2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DefectInfo__c.DefectCode__c</field>
            <operation>equals</operation>
            <value>BF1,BF2,JA2,KD7,KD8,TF2,TF3,WB6</value>
        </criteriaItems>
        <criteriaItems>
            <field>DefectInfo__c.ResponseMatter__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
