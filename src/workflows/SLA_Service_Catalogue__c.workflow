<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SLA_COO_Approved</fullName>
        <field>SLA_Review_Status__c</field>
        <literalValue>COO Approved</literalValue>
        <name>SLA COO Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Check</fullName>
        <field>SLA_Review_Status__c</field>
        <literalValue>SLM Checked</literalValue>
        <name>SLA Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_MT_Approved</fullName>
        <field>SLA_Review_Status__c</field>
        <literalValue>MT Approval</literalValue>
        <name>SLA MT Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SLA_Review_Status</fullName>
        <description>承認のステータス</description>
        <field>SLA_Review_Status__c</field>
        <literalValue>User Approved</literalValue>
        <name>SLA Review Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
