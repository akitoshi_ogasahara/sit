<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_SRPolicy_UpdateNumOfForms_Action</fullName>
        <field>CC_NumberOfForms__c</field>
        <formula>CC_SYSNumberOfForms__c</formula>
        <name>CC_SRPolicy_UpdateNumOfForms_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_SRPolicy_UpdateNumOfPolicies_Action</fullName>
        <field>CC_NumberOfPolicies__c</field>
        <formula>CC_SYSNumberOfPolicies__c</formula>
        <name>CC_SRPolicy_UpdateNumOfPolicies_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC_SRPolicy_Changed_Workflow</fullName>
        <actions>
            <name>CC_SRPolicy_UpdateNumOfForms_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CC_SRPolicy_UpdateNumOfPolicies_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SR契約連結オブジェクトが更新された際に起動する。
帳票数か証券数がブランク以外の時に起動。</description>
        <formula>AND( LOWER($Label.CMN_CaseTriggerOff) &lt;&gt; &apos;true&apos;,  OR ( NOT(ISBLANK( CC_SYSNumberOfPolicies__c )) , NOT( ISBLANK( CC_SYSNumberOfForms__c )) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
