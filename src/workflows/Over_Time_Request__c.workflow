<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>StatusToApproved</fullName>
        <description>Status will be changed to Approved</description>
        <field>Status__c</field>
        <literalValue>承認済み Approved</literalValue>
        <name>StatusToApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusToNew</fullName>
        <description>Status will be changed to New</description>
        <field>Status__c</field>
        <literalValue>新規 New</literalValue>
        <name>StatusToNew</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusToRecalled</fullName>
        <field>Status__c</field>
        <literalValue>取り消し済み Recalled</literalValue>
        <name>StatusToRecalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StatusToReject</fullName>
        <description>Status will be changed to rejected</description>
        <field>Status__c</field>
        <literalValue>却下 Rejected</literalValue>
        <name>StatusToReject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
