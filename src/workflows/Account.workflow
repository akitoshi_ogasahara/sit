<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_Account_UpdateAddressField_Action</fullName>
        <description>Account: 顧客住所最終更新日をnow()で更新</description>
        <field>CC_LastUpdateDateForAddress__c</field>
        <formula>now()</formula>
        <name>CC_Account_UpdateAddressField_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Account_UpdateEditableLUPD_Action</fullName>
        <description>Account: 顧客連絡先最終更新日をnow()で更新</description>
        <field>CC_LastUpdateDateForAccountInfo__c</field>
        <formula>now()</formula>
        <name>CC_Account_UpdateEditableLUPD_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC_Account_AddressFieldChanged_Workflow</fullName>
        <actions>
            <name>CC_Account_UpdateAddressField_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Accountの以下の項目が更新された際に起動する。
・漢字住所
・住所3
・住所4
・郵便番号</description>
        <formula>OR(ISCHANGED( E_COMM_ZCLADDR__c ), ISCHANGED( E_CLTPF_ZNKJADDR01__c ), ISCHANGED( E_CLTPF_ZNKJADDR02__c ) , ISCHANGED( CLTPCODE__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CC_Account_EditableFieldChanged_Workflow</fullName>
        <actions>
            <name>CC_Account_UpdateEditableLUPD_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Accountの以下の項目が更新された際に起動する。
・CMN_Phone3__c
・CMN_Phone3PIC__c
・CMN_Email2__c</description>
        <formula>ISCHANGED( CMN_Phone3__c ) ||  ISCHANGED( CMN_Phone3PIC__c ) ||  ISCHANGED( CMN_Email2__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
