<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateKenmei</fullName>
        <description>件名に活動内容をセットする</description>
        <field>Subject</field>
        <formula>IF(INCLUDES( Action2__c , &quot;研修&quot;) ,&quot;研修 &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;法定研修・委託業務説明会&quot;) ,&quot;法定研修・委託業務説明会 &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;商談・情報提供&quot;) ,&quot;商談・情報提供 &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;同行&quot;) ,&quot;同行 &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;診査&quot;) ,&quot;診査 &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;事務処理&quot;) ,&quot;事務処理 &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;リクルート&quot;) ,&quot;リクルート &quot;,&quot;&quot;)&amp;
IF(INCLUDES( Action2__c , &quot;セミナー&quot;) ,&quot;セミナー &quot;,&quot;&quot;)</formula>
        <name>件名に活動内容をセットする。</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>行動作成時編集時に件名をセットする%E3%80%82</fullName>
        <actions>
            <name>UpdateKenmei</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.RecordTypeId</field>
            <operation>equals</operation>
            <value>BA,IA</value>
        </criteriaItems>
        <description>レコードタイプIA BAの場合、行動の件名に　活動内容をセットする。</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
