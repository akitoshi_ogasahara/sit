<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>E_CMSFile_FLDUPD01</fullName>
        <field>UpsertKey__c</field>
        <formula>MessageMaster__r.UpsertKey__c &amp; &quot;|&quot; &amp;   LPAD(TEXT( sortOrder__c ), 2, &apos;0&apos;)</formula>
        <name>ファイル情報UpsertKey構築</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>E_CMSFile_WFR_True</fullName>
        <actions>
            <name>E_CMSFile_FLDUPD01</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
