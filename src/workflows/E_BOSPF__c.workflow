<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>E_BOSPF_FLDUPD01</fullName>
        <description>代理店手数料ボーナス明細CSVダウンロードデータ用インデックスキー構築
[代理店格コード|年|月]</description>
        <field>index__c</field>
        <formula>ParentAccount__r.E_CL1PF_ZHEADAY__c  &amp; &apos;|&apos; &amp;   TEXT(ACCTYEAR__c)  &amp; &apos;|&apos; &amp;   TEXT(ACCTMONTH__c)</formula>
        <name>E_BOSPFインデックスキー構築</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>E_BOSPF_WFR_True</fullName>
        <actions>
            <name>E_BOSPF_FLDUPD01</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>代理店手数料ボーナス明細CSVダウンロードデータ用の作成及び編集時</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
