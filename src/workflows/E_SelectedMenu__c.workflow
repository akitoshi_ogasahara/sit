<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>E_SelectedMenu_FLDUPD01</fullName>
        <field>ExternalId__c</field>
        <formula>MenuKind__r.ExternalId__c &amp; &quot;|&quot; &amp;  MenuMaster__r.MenuMasterKey__c</formula>
        <name>選択メニュー外部ID構築</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>E_SelectedMenu_WFR_True</fullName>
        <actions>
            <name>E_SelectedMenu_FLDUPD01</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
