<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EmailIFCompleteCharge</fullName>
        <description>【繰入比率変更申込完了】担当者</description>
        <protected>false</protected>
        <recipients>
            <field>EmailCharge__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs_fundsw@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EmailTemplate_ITCompleteCharge</template>
    </alerts>
    <alerts>
        <fullName>EmailITCompleteCostomer</fullName>
        <description>【繰入比率変更申込完了】顧客</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>cs_fundsw@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EmailTemplate_ITCompleteCustomer</template>
    </alerts>
    <alerts>
        <fullName>EmailTFCompleteCharge</fullName>
        <description>【積立金移転申込完了】担当者</description>
        <protected>false</protected>
        <recipients>
            <field>EmailCharge__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>cs_fundsw@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EmailTemplate_FTComplete_Charge</template>
    </alerts>
    <alerts>
        <fullName>EmailTFCompleteContact</fullName>
        <description>【積立金移転申込完了】顧客</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>cs_fundsw@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EmailTemplate_FTComplete_Contact</template>
    </alerts>
    <rules>
        <fullName>E_SPSPFEmail%5F%5FWFR_FTComplete</fullName>
        <actions>
            <name>EmailTFCompleteCharge</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EmailTFCompleteContact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>E_SPSPFEmail__c.Kbn__c</field>
            <operation>equals</operation>
            <value>積立金</value>
        </criteriaItems>
        <criteriaItems>
            <field>E_SPSPFEmail__c.SendMail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>積立金移転の申込完了</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>E_SPSPFEmail%5F%5FWFR_ITComplete</fullName>
        <actions>
            <name>EmailIFCompleteCharge</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>EmailITCompleteCostomer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>E_SPSPFEmail__c.Kbn__c</field>
            <operation>equals</operation>
            <value>繰入</value>
        </criteriaItems>
        <criteriaItems>
            <field>E_SPSPFEmail__c.SendMail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>繰入比率変更の申込完了</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
