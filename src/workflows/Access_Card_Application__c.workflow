<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Access_Card_Notice_of_expiration_date</fullName>
        <description>Access Card Notice of expiration date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>ml-il-jp-accesscard@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>InternalProcess/Access_Card_Expiration_Notice</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_SSD</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Shared Service部確認中</literalValue>
        <name>Assign to SSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>カード処理中</literalValue>
        <name>Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>General_Area_Step1</fullName>
        <field>Approval_Status__c</field>
        <literalValue>関連部署確認中</literalValue>
        <name>General Area Step1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Declined</fullName>
        <field>Approval_Status__c</field>
        <literalValue>差し戻し</literalValue>
        <name>Request Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSD_Assign_to_GA</fullName>
        <field>Approval_Status__c</field>
        <literalValue>承認待ち_Pending Approval</literalValue>
        <name>SSD Assign to GA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSD_Final_Approval</fullName>
        <field>Approval_Status__c</field>
        <literalValue>承認済み_Approved</literalValue>
        <name>SSD Final Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSD_GA_Approval</fullName>
        <description>一時貸出、TCの場合はGA承認のみで完了</description>
        <field>Approval_Status__c</field>
        <literalValue>承認済み_Approved</literalValue>
        <name>SSD GA Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSD_General_Area_Step1</fullName>
        <description>リクエスターが承認依頼を実施後、このステータスになる（所属部長と、セキュリティエリアの当該部署長の確認待ち）</description>
        <field>Approval_Status__c</field>
        <literalValue>承認待ち_Pending Approval</literalValue>
        <name>SSD General Area Step1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SSD_Request_Declined</fullName>
        <field>Approval_Status__c</field>
        <literalValue>差し戻し_Denied</literalValue>
        <name>SSD Request Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
