<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ThanksMailToAgent</fullName>
        <description>御礼メールTo募集人</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>iris-info@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/I_Survey_Thank</template>
    </alerts>
    <rules>
        <fullName>代理店アンケート 御礼メール送信</fullName>
        <actions>
            <name>ThanksMailToAgent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>レコードが作成されたとき</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
