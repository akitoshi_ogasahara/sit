<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BatonMailToFriends</fullName>
        <description>バトンメールToフレンド</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>nn-project@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/MK_VFTemp_Fiends_Invite</template>
    </alerts>
    <rules>
        <fullName>バトンメール送信Toフレンド</fullName>
        <actions>
            <name>BatonMailToFriends</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>既契約者に登録されたお友達へ招待メールを送信するためのルール</description>
        <formula>AND(NOT(ISBLANK(Email__c)),NOT(ISBLANK( DomainURL__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
