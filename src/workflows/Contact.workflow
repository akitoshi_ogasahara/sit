<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CC_Contact_UpdateAddressField_Action</fullName>
        <description>Contact: 顧客住所最終更新日をnow()で更新</description>
        <field>CC_LastUpdateDateForAddress__c</field>
        <formula>now()</formula>
        <name>CC_Contact_UpdateAddressField_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Contact_UpdateEditableLUPD_Action</fullName>
        <description>Contact: 顧客連絡先最終更新日をnow()で更新</description>
        <field>CC_LastUpdateDateForEditableField__c</field>
        <formula>now()</formula>
        <name>CC_Contact_UpdateEditableLUPD_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CC_Contact_AddressFieldChanged_Workflow</fullName>
        <actions>
            <name>CC_Contact_UpdateAddressField_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Contactの以下の項目が更新された際に起動する。
・漢字住所
・住所3
・住所4
・郵便番号</description>
        <formula>OR(ISCHANGED( E_COMM_ZCLADDR__c ), ISCHANGED( E_CLTPF_ZNKJADDR01__c ), ISCHANGED( E_CLTPF_ZNKJADDR02__c ) , ISCHANGED( E_CLTPF_CLTPCODE__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CC_Contact_EditableFieldChanged_Workflow</fullName>
        <actions>
            <name>CC_Contact_UpdateEditableLUPD_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Contactの以下の項目が更新された際に起動する。
・MobilePhone
・OtherPhone
・Email
・CC_AttachmentPassword__c</description>
        <formula>ISCHANGED(  OtherPhone )  || ISCHANGED(  MobilePhone  ) || ISCHANGED(  Email ) || ISCHANGED(  CC_AttachmentPassword__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
