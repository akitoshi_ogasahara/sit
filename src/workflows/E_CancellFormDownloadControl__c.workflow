<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>E_CancellFormDownloadControl_FLDUPD01</fullName>
        <field>UpsertKey__c</field>
        <formula>IF( ISBLANK( Account__c ) , &quot;ALL&quot;, Account__r.E_UpsertKey__c)  &amp; 
&quot;|&quot; &amp; ProductCode__c</formula>
        <name>解約申込書DL設定UpsertKey構築</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>E_CancellFormDownloadControl_WFR_True</fullName>
        <actions>
            <name>E_CancellFormDownloadControl_FLDUPD01</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
