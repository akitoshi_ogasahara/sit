<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BRM_Case_Delayed</fullName>
        <ccEmails>ML-IL-JP-IT-BRM@nnlife.co.jp</ccEmails>
        <description>[BRM 開発・導入相談] 期限超過</description>
        <protected>false</protected>
        <senderAddress>il-jp-brm-consultant@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/BRM_Case_Delayed</template>
    </alerts>
    <alerts>
        <fullName>BRM_Case_Not_Closed</fullName>
        <ccEmails>ML-IL-JP-IT-BRM@nnlife.co.jp</ccEmails>
        <description>[BRM 開発・導入相談] 未完了</description>
        <protected>false</protected>
        <senderAddress>il-jp-brm-consultant@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/BRM_Case_Not_Closed</template>
    </alerts>
    <alerts>
        <fullName>BRM_Case_Open</fullName>
        <ccEmails>ML-IL-JP-IT-BRM@nnlife.co.jp</ccEmails>
        <description>[BRM 開発・導入相談] オープン</description>
        <protected>false</protected>
        <senderAddress>il-jp-brm-consultant@nnlife.co.jp</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/BRM_Case_Open</template>
    </alerts>
</Workflow>
