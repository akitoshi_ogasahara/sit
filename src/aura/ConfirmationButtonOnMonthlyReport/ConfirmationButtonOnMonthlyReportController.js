({
    getStats: function(component, event, helper){
        var action = component.get("c.callCalcMethod");
        action.setParams({"strRecordId": component.get("v.recordId")});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.result", response.getReturnValue());
                     
                //Close the action panel 2018/08/21 Tao added
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.get("e.force:closeQuickAction").fire(); 
                    }), 2000
                );
                
            } else {
                console.log('Problem getting in call apex, response state: ' + state);
            }
        }); 
	 $A.enqueueAction(action);
    }
})