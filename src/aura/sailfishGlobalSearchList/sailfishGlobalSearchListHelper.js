({
    getRecord: function(component, accessId) {
        console.log("start helper");
       console.log("Here helper with " + accessId + " at controller");
        var getRecord = component.get('c.getOnboardingRecordFromAccessId'); 
        getRecord.setParams({'accessId': accessId});
        getRecord.setCallback(this, function(b){
            console.log("Back again ! with ", b.getState());
            var newRecord = b.getReturnValue() ; 
            console.log(newRecord[0]) ; // It's an Array, be careful (but don't worry too much, there is a LIMIT 1 in the APEX class)
            console.log('The name is ', newRecord[0].Name); 
            //alert('The record ID is ' + newRecord[0].Id + ' and the bon dieu de Name is ' + newRecord[0].Name) ;
            
            var appEvent = $A.get("e.c:sailfishGlobalRecordEvent");
            appEvent.setParams({'recordId': newRecord[0].Id, 'recordName': newRecord[0].Name}); 
            appEvent.fire();  
        });
        $A.enqueueAction(getRecord); 
        
        console.log("finish helper");
    }
})