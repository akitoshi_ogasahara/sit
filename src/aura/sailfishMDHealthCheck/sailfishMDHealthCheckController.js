({

    handleApplicationEvent: function(component, event, helper) {
        component.set("v.recordId", event.getParam('recordId'));
        component.set("v.recordName", event.getParam('recordName'));
        var errorMessage = component.get("v.errorMessage");
        var currentIdwrong = component.get("v.recordIdWrong"); // this contains the record Id of a record that has generated an error 
        var requestedId = component.get("v.recordId"); // this is the record Id changed on the server or requested by the user
        console.log("Before Reload");
        if ((errorMessage == '') || (currentIdwrong != requestedId)) {
            console.log("No error or the recordId has changed, then reload record, reset the error message");
            component.find("sailfishMD").reloadRecord();
            component.set("v.errorMessage", '');
            component.set("v.generalDecision", '');
        } else {
            console.log("There is an error, I don't want to reload the record now");
        }
        helper.disclaimer(component);
    },
    
    set14monthsDate: function(component, event, helper) {
        var d1 = new Date(); // current date
        var d1year = d1.getFullYear();
        var d1month = d1.getMonth() + 1; // Month are from 0 to 11
        var d1day = d1.getDate(); // NB: Day if retrieved with getDate() and not getDay() 
        // first we remove 14 months 
        var d2year = d1year - 1; // we remove 1 year
        var d2month = d1month - 2; // and then 2 months 
        if (d2month < 1) {
            d2month = 12 + d2month;
            d2year--;
        };
        // determine if it is a leap year for processing correctly february         
        var isLeapYear = false;
        if ((d2year % 4 == 0) || (d2year % 100 == 0) || (d2year % 400 == 0)) {
            isLeapYear = true
        };
        // Finally remove a day 
        var d2day = d1day - 1;
        if (d2day == 0) {
            d2month = d2month - 1;
            d2month = (d2month > 1 ? (d2month - 1) : 12);
            if (d2month == 2) {
                d2day = (isLeapYear ? 29 : 28);
            } else if (d2month == 12) {
                d2year--;
                d2day = 31;
            } else if ((d2month == 1) || (d2month == 3) || (d2month == 5) || (d2month == 7) || (d2month == 8) || (d2month == 10)) {
                d2day = 31;
            } else {
                d2day = 30;
            };
        };
        var d2 = d2year + '-' + d2month + '-' + d2day;
        component.set("v.date14months", d2);
        helper.disclaimer(component);
    },    
    
    setFuture14monthsDate: function(component, event, helper) {
        var date1temp = new Date(component.get("v.fieldValues.Medical_Report_Date__c"));
        var date1 = new Date(date1temp);
        var date1Plus14Months = new Date(date1temp.setMonth(date1temp.getMonth()+14));
        if(date1Plus14Months.getDate() != date1.getDate()) {
                var date1Plus14Months = new Date(date1temp.setDate(0));
        }
        var date1Plus14MonthsMinus1Day = new Date(date1Plus14Months.setDate(date1Plus14Months.getDate()-1));
        component.set("v.dateFuture14months", date1Plus14MonthsMinus1Day);
// new Aug 9, 2018
        component.set("v.fieldValues.Expiry_Date_Future_14_Months__c", date1Plus14MonthsMinus1Day);
//
        helper.disclaimer(component);
    },
    
    recordUpdated: function(component, event, helper) {
// new Aug 9, 2018
        var expiryPastDate = component.get("v.date14months");
        console.log('Record Updated Expiry Past Date:', expiryPastDate);
        component.set("v.fieldValues.Expiry_Date_Past_14_Months__c", expiryPastDate);
        var fieldValuesExpiryPastDate = component.get("v.fieldValues.Expiry_Date_Past_14_Months__c");
        console.log('Record Updated Expiry Past Date Field Value:', fieldValuesExpiryPastDate);
//
        component.set("v.lifeAssuredGender_values", component.get("v.fieldValues.Life_Assured_Gender__c"));
        component.set("v.lifeAssuredTobaccoUse_values", component.get("v.fieldValues.Tobacco_Use__c"));
        component.set("v.lifeAssuredUrinaryGlucoseRating_values", component.get("v.fieldValues.Urinary_Glucose_Rating__c"));
        component.set("v.lifeAssuredUrinaryProteinCurrentRating_values", component.get("v.fieldValues.Urinary_Protein_Current_Rating__c"));
        component.set("v.lifeAssuredUrinaryProteinPreviousRating_values", component.get("v.fieldValues.Urinary_Protein_Previous_Rating__c"));
        component.set("v.lifeAssuredUrinaryOccultBloodRating_values", component.get("v.fieldValues.Urinary_Occult_Blood_Rating__c"));
        component.set("v.diabetesDuration_values", component.get("v.fieldValues.Duration_of_Diabetes__c"));
        component.set("v.doctorAssessment_values", component.get("v.fieldValues.Doctor_Assessment__c")); // 20180530 JIRA SAIL-12
        component.set("v.examType_values", component.get("v.fieldValues.ExamType__c")); // 20180530 JIRA SAIL-11
        var reportCheck = component.get("v.fieldValues.Medical_Report_Date__c");
            //alert('Test0');
        if (reportCheck == null) {
            component.set('v.medicalReportYear', reportCheck);
            component.set('v.medicalReportMonth', reportCheck);
            component.set('v.medicalReportDay', reportCheck);
            component.set("v.dateFuture14months", reportCheck);
            //alert('Test1');
        } else {
            var report = new Date(component.get("v.fieldValues.Medical_Report_Date__c"));
            var reportEra = component.get('v.medicalReportEra_values');
            var reportYear = report.getFullYear();
            var reportEraYear = parseInt(reportYear) - parseInt(reportEra);
            component.set("v.medicalReportYear", reportEraYear);
            var reportMonth = report.getMonth() + 1;
            component.set("v.medicalReportMonth", reportMonth);
            var reportDay = report.getDate();
            component.set("v.medicalReportDay", reportDay);
            var a = component.get('c.setFuture14monthsDate');
            //alert('Test2');
            $A.enqueueAction(a);
        }
        var dobCheck = component.get("v.fieldValues.Life_Assured_Date_of_Birth__c");
        if (dobCheck == null) {
            component.set('v.lifeAssuredBirthYear', dobCheck);
            component.set('v.lifeAssuredBirthMonth', dobCheck);
            component.set('v.lifeAssuredBirthDay', dobCheck);
        } else {
            var dob = new Date(component.get("v.fieldValues.Life_Assured_Date_of_Birth__c"));
            var dobEra = component.get('v.lifeAssuredBirthEra_values');
            var dobYear = dob.getFullYear();
            var dobEraYear = parseInt(dobYear) - parseInt(dobEra);
            component.set("v.lifeAssuredBirthYear", dobEraYear);
            var dobMonth = dob.getMonth() + 1;
            component.set("v.lifeAssuredBirthMonth", dobMonth);
            var dobDay = dob.getDate();
            component.set("v.lifeAssuredBirthDay", dobDay);
        }
        var bp2 = component.get("v.fieldValues.Blood_Pressure_Systolic_2__c");
        if (bp2 == null) {
            component.set('v.bloodPressure2_visible', false);
        } else {
            component.set('v.bloodPressure2_visible', true);
        }
        var bp3 = component.get("v.fieldValues.Blood_Pressure_Systolic_3__c");
        if (bp3 == null) {
            component.set('v.bloodPressure3_visible', false);
        } else {
            component.set('v.bloodPressure3_visible', true);
        }
        helper.compute_avgBP(component);
        var height = component.get('v.fieldValues.Height_cm__c');
        var weight = component.get('v.fieldValues.Weight_kg__c');
        var BMI = (weight / Math.pow(height / 100, 2)).toFixed(2);
        if (BMI == "" || BMI == null || height == "" || height == null || weight == "" || weight == null) {
            component.set('v.BMI', null);
        } else {
            component.set('v.BMI', BMI);
        }
        helper.disclaimer(component);
    },

    handleSaveRecord: function(component, event, helper) {
        
        helper.disclaimer(component);
        var accessId = component.get("v.fieldValues.Form_Id__c")
        var dob = component.get("v.fieldValues.Life_Assured_Date_of_Birth__c");
        var gender = component.get("v.fieldValues.Life_Assured_Gender__c");
        var tobacco = component.get("v.fieldValues.Tobacco_Use__c");
// new Aug 9, 2018
        var expiryPastDate = component.get("v.date14months");
        console.log('Expiry Past Date:', expiryPastDate);
        component.set("v.fieldValues.Expiry_Date_Past_14_Months__c", expiryPastDate);
//        
        if (accessId == "" || accessId == null || dob == "" || dob == null || gender == "" || gender == null || tobacco == "" || tobacco == null) {
            alert('必要な項目が完了していません');
        } else {
            //if(helper.validateContactForm(component)) {
            component.set("v.fieldValues.Id", component.get("v.recordId"));
            component.set("v.errorMessage", 'Saving...');
            
            // Check error 
            component.set("v.errorCheck","");  
            helper.checkError(component);
            var errorMessage=component.get("v.errorCheck") ;
            var d1 = new Date(Date.parse(component.get('v.fieldValues.Medical_Report_Date__c')));
            var d2 = new Date(Date.parse(component.get('v.date14months')));
            console.log('d1',d1,'d2',d2); 
            if (errorMessage !== '') {
                console.log('Error has occurred.'); 
                component.set('v.errorMessage', errorMessage);
                component.set("v.recordIdWrong", component.get("v.recordId")); 
            } else {
                console.log('Date is correct, we shall proceed.');
                component.set("v.errorCheck","");  
                helper.checkError(component);
                var truc=component.get("v.errorCheck") ; 
                console.log("after execution of checkError ", truc) ;  
                component.find("sailfishMD").saveRecord(function(saveResult) {
                    if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                        // record is saved successfully
                        component.set("v.errorMessage", '');
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "title": "Saved",
                            "message": "The record was saved."
                        });
                        resultsToast.fire();
                    } else if (saveResult.state === "INCOMPLETE") {
                        console.log("User is offline, device doesn't support drafts.");
                    } else if (saveResult.state === "ERROR") {
                        var errorInfo = JSON.stringify(saveResult.error);
                        var errors = JSON.parse(errorInfo);
                        console.log('Server error:', errors);
                        var message = '';
                        if (errors) {
                            for (var i = 0; i < errors.length; i++) {
                                for (var j = 0; errors[i].pageErrors && j < errors[i].pageErrors.length; j++) {
                                    message += (message.length > 0 ? '\n' : '') + errors[i].pageErrors[j].message;
                                }
                                if (errors[i].fieldErrors) {
                                    for (var fieldError in errors[i].fieldErrors) {
                                        var thisFieldError = errors[i].fieldErrors[fieldError];
                                        for (var j = 0; j < thisFieldError.length; j++) {
                                            message += (message.length > 0 ? '\n' : '') + thisFieldError[j].fieldApiName + ' : ' + thisFieldError[j].message;
                                        }
                                    }
                                }
                                if (errors[i].message) {
                                    message += (message.length > 0 ? '\n' : '') + errors[i].message;
                                }
                            }
                        } else {
                            message += (message.length > 0 ? '\n' : '') + 'Unknown error';
                        }
                        console.log('Message', message);
                        component.set("v.recordIdWrong", component.get("v.recordId")); 
                        component.set("v.errorMessage", message);
                    }
                });
                var recordName = component.get("v.recordName");
                console.log("Here so far with " + recordName + " at controller");
                helper.getRecord(component, recordName);
            }
        }        
        
    },


    handleClearLiver: function(component, event, helper) {
        component.set('v.fieldValues.GOT_Count_AST__c', null);
        component.set('v.fieldValues.GPT_Count_ALT__c', null);
        component.set('v.fieldValues.yGTP__c', null);
        component.set('v.fieldValues.GOT_Count_AST_Base__c', 40);
        component.set('v.fieldValues.GPT_Count_ALT_Base__c', 40);
        component.set('v.fieldValues.yGTP_Base__c', 70);
        helper.disclaimer(component);
    },

    handleClearBlood: function(component, event, helper) {
        component.set('v.fieldValues.Red_Blood_Cell_Count__c', null);
        component.set('v.fieldValues.White_Blood_Cell_Count__c', null);
        component.set('v.fieldValues.Hemoglobin_Level__c', null);
        component.set('v.fieldValues.Creatinine_Count__c', null);
        helper.disclaimer(component);
    },

    handleClearCholesterol: function(component, event, helper) {
        component.set('v.fieldValues.Cholesterol_Total__c', null);
        component.set('v.fieldValues.Cholesterol_LDL__c', null);
        component.set('v.fieldValues.Cholesterol_HDL__c', null);
        component.set('v.fieldValues.Triglyceride_Count__c', null);
        helper.disclaimer(component);
    },

    handleClearSugar: function(component, event, helper) {
        component.set('v.fieldValues.HbA1c_Count__c', null);
        component.set('v.fieldValues.Fasting_Blood_Glucose_Count__c', null);
        component.set('v.fieldValues.Standard_Blood_Glucose_Count__c', null);
        component.set('v.lifeAssuredUrinaryGlucoseRating_values', null);
        component.set('v.fieldValues.Urinary_Glucose_Rating__c', null);
        //20180814 TerraSky 場所移動対応
        component.set('v.diabetesDuration_values', null);
        component.set('v.fieldValues.Duration_of_Diabetes__c', null);
        helper.disclaimer(component);
    },

    handleClearOther: function(component, event, helper) {
        component.set('v.lifeAssuredUrinaryProteinCurrentRating_values', null);
        component.set('v.fieldValues.Urinary_Protein_Current_Rating__c', null);
        component.set('v.lifeAssuredUrinaryProteinPreviousRating_values', null);
        component.set('v.fieldValues.Urinary_Protein_Previous_Rating__c', null);
        component.set('v.lifeAssuredUrinaryOccultBloodRating_values', null);
        component.set('v.fieldValues.Urinary_Occult_Blood_Rating__c', null);
        //20180814 TerraSky 場所移動対応
        //component.set('v.diabetesDuration_values', null);
        //component.set('v.fieldValues.Duration_of_Diabetes__c', null);
        helper.disclaimer(component);
    },

    handleClearBP: function(component, event, helper) {
        component.set('v.fieldValues.Blood_Pressure_Systolic_1__c', null);
        component.set('v.fieldValues.Blood_Pressure_Diastolic_1__c', null);
        component.set('v.fieldValues.Blood_Pressure_Systolic_2__c', null);
        component.set('v.fieldValues.Blood_Pressure_Diastolic_2__c', null);
        component.set('v.fieldValues.Blood_Pressure_Systolic_3__c', null);
        component.set('v.fieldValues.Blood_Pressure_Diastolic_3__c', null);
        component.set('v.bloodPressure2_visible', false);
        component.set('v.bloodPressure3_visible', false);
        component.set('v.bpAverage','');
        helper.disclaimer(component);
    },

    average_BP: function(component, event, helper) {
        console.log("Call helper"); 
        helper.disclaimer(component);
        helper.compute_avgBP(component) ; 
    },
    
    handleAddBP2: function(component, event, helper) {
        var bp1s = component.get("v.fieldValues.Blood_Pressure_Systolic_1__c");
        var bp1d = component.get("v.fieldValues.Blood_Pressure_Diastolic_1__c");
        if (bp1s == null || bp1d == null || bp1s == "" || bp1d == "") {} else {
            component.set('v.bloodPressure2_visible', true);
        }
        helper.disclaimer(component);
    },

    handleDeleteBP2: function(component, event, helper) {
        var bp2 = component.get("v.bloodPressure2_visible");
        var bp3 = component.get("v.bloodPressure3_visible");
        var systolic3 = component.get("v.fieldValues.Blood_Pressure_Systolic_3__c");
        var diastolic3 = component.get("v.fieldValues.Blood_Pressure_Diastolic_3__c");
        //alert('bp2:'+ bp2);
        //alert('bp3:'+ bp3);
        if (bp3 == true) {
            //alert('true is selected');
            component.set('v.bloodPressure3_visible', false);
            component.set('v.fieldValues.Blood_Pressure_Systolic_2__c', systolic3);
            component.set('v.fieldValues.Blood_Pressure_Diastolic_2__c', diastolic3);
            component.set('v.fieldValues.Blood_Pressure_Systolic_3__c', null);
            component.set('v.fieldValues.Blood_Pressure_Diastolic_3__c', null);
        } else {
            //alert('false is selected');
            component.set('v.bloodPressure2_visible', false);
            component.set('v.fieldValues.Blood_Pressure_Systolic_2__c', null);
            component.set('v.fieldValues.Blood_Pressure_Diastolic_2__c', null);
        }
        helper.compute_avgBP(component) ; 
        helper.disclaimer(component);
    },

    handleAddBP3: function(component, event, helper) {
        var bp2s = component.get("v.fieldValues.Blood_Pressure_Systolic_2__c");
        var bp2d = component.get("v.fieldValues.Blood_Pressure_Diastolic_2__c");
        if (bp2s == null || bp2d == null || bp2s == "" || bp2d == "") {} else {
            component.set('v.bloodPressure3_visible', true);
        }
        helper.disclaimer(component);
    },

    handleDeleteBP3: function(component, event, helper) {
        component.set('v.bloodPressure3_visible', false);
        component.set('v.fieldValues.Blood_Pressure_Systolic_3__c', null);
        component.set('v.fieldValues.Blood_Pressure_Diastolic_3__c', null);
        helper.compute_avgBP(component) ; 
        helper.disclaimer(component);
    },

    insertOnboardingRecord: function(component, event, helper) {
        var onboarding = component.get("v.onboarding");
        console.log("Create Onboarding Record: " + JSON.stringify(onboarding));
        helper.insertOnboardingRecord(component, onboarding);
        helper.disclaimer(component);
    },

    liverBase_function: function(component, event, helper) {
        var GOT_Count_AST_Base__c = component.get("v.fieldValues.GOT_Count_AST_Base__c");
        var GPT_Count_ALT_Base__c = component.get("v.fieldValues.GPT_Count_ALT_Base__c");
        var yGTP_Base__c = component.get("v.fieldValues.yGTP_Base__c");
        if (GOT_Count_AST_Base__c <= 40) {
                                                component.set('v.fieldValues.GOT_Count_AST_Base__c', 40);            
            //alert("Base was too low");
        }
        if (GPT_Count_ALT_Base__c <= 40) {
                                                component.set('v.fieldValues.GPT_Count_ALT_Base__c', 40);            
            //alert("Base was too low");
        }
        if (yGTP_Base__c <= 70) {
                                                component.set('v.fieldValues.yGTP_Base__c', 70); 
            //alert("Base was too low");
        }
        helper.disclaimer(component);
    },

    ageNull_function: function(component, event, helper) {
        var ageNull = component.get("v.fieldValues.Life_Assured_Age__c");
        if (ageNull == "" || ageNull == null) {
            var a = component.get('c.ageDate_function');
            $A.enqueueAction(a);
        };
        helper.disclaimer(component);
    },

    ageDate_function: function(component, event, helper) {
        //Report Date
        var reportEra = component.get("v.medicalReportEra_values");
        var reportYear = component.get("v.medicalReportYear");
        var reportMonth = component.get("v.medicalReportMonth");
        var reportDay = component.get("v.medicalReportDay");
        if (reportEra == "" || reportYear == "" || reportMonth == "" || reportDay == "" || reportEra == null || reportYear == null || reportMonth == null || reportDay == null) {
            component.set('v.fieldValues.Medical_Report_Date__c', null);
        } else {
            var reportMonth = reportMonth - 1;
            var reportEraYear = parseInt(reportEra) + parseInt(reportYear);
            var reportDate = new Date(Date.UTC(reportEraYear, reportMonth, reportDay, 0, 0, 0, 0));
            component.set('v.fieldValues.Medical_Report_Date__c', reportDate);
                var a = component.get('c.setFuture14monthsDate');
                $A.enqueueAction(a);
        }
        //Birth Date
        var dobEra = component.get("v.lifeAssuredBirthEra_values");
        var dobYear = component.get("v.lifeAssuredBirthYear");
        var dobMonth = component.get("v.lifeAssuredBirthMonth");
        var dobDay = component.get("v.lifeAssuredBirthDay");
        if (dobEra == "" || dobYear == "" || dobMonth == "" || dobDay == "" || dobEra == null || dobYear == null || dobMonth == null || dobDay == null) {
            component.set('v.fieldValues.Life_Assured_Date_of_Birth__c', null);
            component.set('v.fieldValues.Life_Assured_Age__c', null);
        } else {
            var dobMonth = dobMonth - 1;
            var dobEraYear = parseInt(dobEra) + parseInt(dobYear);
            var birthDate = new Date(Date.UTC(dobEraYear, dobMonth, dobDay, 0, 0, 0, 0));
            component.set('v.fieldValues.Life_Assured_Date_of_Birth__c', birthDate);

            //if (reportEra == "" || reportYear == "" || reportMonth == "" || reportDay == "" || reportEra == null || reportYear == null || reportMonth == null || reportDay == null) {
                var ageDifMs = Date.now() - birthDate.getTime();
            //} else {
            //    var reportMonth = reportMonth - 1;
            //    var reportEraYear = parseInt(reportEra) + parseInt(reportYear);
            //    var reportDate = new Date(Date.UTC(reportEraYear, reportMonth, reportDay, 0, 0, 0, 0));
            //    var ageDifMs = reportDate.getTime() - birthDate.getTime();
            //    var a = component.get('c.setFuture14monthsDate');
            //    $A.enqueueAction(a);
            //}
            var ageDate = new Date(ageDifMs); // miliseconds from epoch 
            var ageFinal = Math.abs(ageDate.getUTCFullYear() - 1970);
            component.set('v.fieldValues.Life_Assured_Age__c', ageFinal);
        }
        helper.disclaimer(component);
    },

    //    lifeAssuredDate_function:  function(component, event, helper){
    //        var dobEra = component.get("v.lifeAssuredBirthEra_values");
    //        var dobYear = component.get("v.lifeAssuredBirthYear");
    //        var dobMonth = component.get("v.lifeAssuredBirthMonth");
    //        var dobDay = component.get("v.lifeAssuredBirthDay");
    //        
    //        if(dobEra == "" || dobYear == "" || dobMonth == "" || dobDay == "" || dobEra == null || dobYear == null || dobMonth == null || dobDay == null){
    //            component.set('v.fieldValues.Life_Assured_Date_of_Birth__c', null);       
    //            component.set('v.fieldValues.Life_Assured_Age__c', null);             
    //        } else {
    //            var dobMonth = dobMonth - 1;
    //            var dobEraYear = parseInt(dobEra) + parseInt(dobYear);
    //            var birthDate = new Date(Date.UTC(dobEraYear, dobMonth, dobDay, 0, 0, 0, 0));
    //            component.set('v.fieldValues.Life_Assured_Date_of_Birth__c', birthDate);       
    //            
    //            var reportEra = component.get("v.medicalReportEra_values");
    //            var reportYear = component.get("v.medicalReportYear");
    //            var reportMonth = component.get("v.medicalReportMonth");
    //            var reportDay = component.get("v.medicalReportDay");
    //           
    //            if(reportEra == "" || reportYear == "" || reportMonth == "" || reportDay == "" || reportEra == null || reportYear == null || reportMonth == null || reportDay == null){
    //                var ageDifMs = Date.now() - birthDate.getTime();
    //            } else {
    //                var reportMonth = reportMonth - 1;
    //                var reportEraYear = parseInt(reportEra) + parseInt(reportYear);
    //                var reportDate = new Date(Date.UTC(reportEraYear, reportMonth, reportDay, 0, 0, 0, 0));
    //                var ageDifMs = reportDate.getTime() - birthDate.getTime();
    //            }
    //            var ageDate = new Date(ageDifMs); // miliseconds from epoch 
    //            var ageFinal = Math.abs(ageDate.getUTCFullYear() - 1970); 
    //            console.log('ageFinal: ' + ageFinal);
    //            component.set('v.fieldValues.Life_Assured_Age__c', ageFinal); 
    //        }
    //    },

    medicalReportEra_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.medicalReportEra_values', selectedButtonLabel);
        console.log("selectedButtonLabel", selectedButtonLabel);
        var reportCheck = component.get("v.fieldValues.Medical_Report_Date__c");
        if (reportCheck == null) {
            component.set('v.medicalReportYear', reportCheck);
            component.set('v.medicalReportMonth', reportCheck);
            component.set('v.medicalReportDay', reportCheck);
        } else {
            var report = new Date(component.get("v.fieldValues.Medical_Report_Date__c"));
            var reportEra = component.get('v.medicalReportEra_values');
            var reportYear = report.getFullYear();
            var reportEraYear = parseInt(reportYear) - parseInt(reportEra);
            component.set("v.medicalReportYear", reportEraYear);
            var reportMonth = report.getMonth() + 1;
            component.set("v.medicalReportMonth", reportMonth);
            var reportDay = report.getDate();
            component.set("v.medicalReportDay", reportDay);
        }
        helper.disclaimer(component);
    },

    lifeAssuredBirthEra_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.lifeAssuredBirthEra_values', selectedButtonLabel)
        var dobCheck = component.get("v.fieldValues.Life_Assured_Date_of_Birth__c");
        if (dobCheck == null) {
            component.set('v.lifeAssuredBirthYear', dobCheck);
            component.set('v.lifeAssuredBirthMonth', dobCheck);
            component.set('v.lifeAssuredBirthDay', dobCheck);
        } else {
            var dob = new Date(component.get("v.fieldValues.Life_Assured_Date_of_Birth__c"));
            var dobEra = component.get('v.lifeAssuredBirthEra_values');
            var dobYear = dob.getFullYear();
            var dobEraYear = parseInt(dobYear) - parseInt(dobEra);
            component.set("v.lifeAssuredBirthYear", dobEraYear);
            var dobMonth = dob.getMonth() + 1;
            component.set("v.lifeAssuredBirthMonth", dobMonth);
            var dobDay = dob.getDate();
            component.set("v.lifeAssuredBirthDay", dobDay);
        }
        helper.disclaimer(component);
    },

    lifeAssuredGender_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Life_Assured_Gender__c', selectedButtonLabel)
        helper.disclaimer(component);
    },
    doctorAssessment_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Doctor_Assessment__c', selectedButtonLabel);
        helper.disclaimer(component);
    },
    resetDoctorAssessment_button: function(component, event, helper) {
        component.set('v.fieldValues.Doctor_Assessment__c', null);
        component.set('v.doctorAssessment_values', null);
        helper.disclaimer(component);
   },

    examType_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.ExamType__c', selectedButtonLabel);
        helper.disclaimer(component);
    },
    
    lifeAssuredTobaccoUse_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Tobacco_Use__c', selectedButtonLabel)
        helper.disclaimer(component);
    },
    
    lifeAssuredUrinaryGlucoseRating_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Urinary_Glucose_Rating__c', selectedButtonLabel)
        helper.disclaimer(component);
    },

    lifeAssuredUrinaryProteinCurrentRating_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Urinary_Protein_Current_Rating__c', selectedButtonLabel)
        helper.disclaimer(component);
    },

    lifeAssuredUrinaryProteinPreviousRating_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Urinary_Protein_Previous_Rating__c', selectedButtonLabel)
        helper.disclaimer(component);
    },

    lifeAssuredUrinaryOccultBloodRating_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Urinary_Occult_Blood_Rating__c', selectedButtonLabel)
        helper.disclaimer(component);
    },

    diabetesDuration_button: function(component, event, helper) {
        var selectedButtonLabel = event.getSource().get("v.value");
        component.set('v.fieldValues.Duration_of_Diabetes__c', selectedButtonLabel)
        helper.disclaimer(component);
    },

    sectionOne: function(component, event, helper) {
        var prevValue = component.get('v.prevArticleOne');
        var newValue = component.get('v.fieldValues.CheckCompletion__c');
        if (prevValue == newValue) { // User has not checked the 入力確認済み checkbox 
            helper.helperFun(component, event, 'articleOne');
        } else {
            component.set('v.prevArticleOne', newValue); // The checkbox has been clicked, save the new value 
        };
    },

    sectionTwo: function(component, event, helper) {
        helper.helperFun(component, event, 'articleTwo');
    },
    
    sectionThree: function(component, event, helper) {
        helper.helperFun(component, event, 'articleThree');
    },
    
    sectionFour: function(component, event, helper) {
        helper.helperFun(component, event, 'articleFour');
    },
    BMI: function(component, event, helper) {
        var height = component.get('v.fieldValues.Height_cm__c');
        var weight = component.get('v.fieldValues.Weight_kg__c');
        var BMI = (weight / Math.pow(height / 100, 2)).toFixed(2);
        if (BMI == "" || BMI == null || height == "" || height == null || weight == "" || weight == null) {
            component.set('v.BMI', null)
        } else {
            component.set('v.BMI', BMI)
        };
        helper.disclaimer(component);
    },

    checkDisclaimer: function(component, event, helper) {
        helper.disclaimer(component);
    },
	createPDF : function(component, event, helper) {
        //20180815 TerraSky 新規ウィンドウで開くように修正

        var id = component.get("v.recordId");
        console.log('id='+id);
        var url = '/apex/svfcloud__PreviewPage?id='+id+'&buttonFullName=SVFSailfishOnboardingcButton201806280411504839hZ';
        window.open(url, '_blank', 'width=800,height=600');
        // URLを直接指定して呼び出します。
//        urlEvent.setParams({"url":'/apex/svfcloud__PreviewPage?id='+id+'&buttonFullName=SVFSailfishOnboardingcButton201806280411504839hZ'});
//        urlEvent.fire();
      
    },
    //DirectPrint対応 start
    directPrint : function(component, event, helper) {
        var id = component.get("v.recordId");
        console.log('id='+id);
        var getPrintURL = component.get('c.getDownloadURL');
        
        getPrintURL.setParams({
            'targetId': id
        });
        getPrintURL.setCallback(this, function(responce) {
            var printURL = responce.getReturnValue();
            alert("プリンター（PPJP05044）を確認してください。");
            //alert(printURL);
            //var urlEvent = $A.get("e.force:navigateToURL");
            //urlEvent.setParams({
            //    "url": printURL
            //});
            //urlEvent.fire();            
        });
        $A.enqueueAction(getPrintURL); 
    }
 
})