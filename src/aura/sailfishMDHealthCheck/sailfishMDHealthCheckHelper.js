({
    getRecord: function(component, recordName) {
        
        var getSelectedAccount = component.get("v.recordId");
        var getRecord = component.get('c.getOnboardingRecordFromId');
        getRecord.setParams({
            'Id': getSelectedAccount
        });
        getRecord.setCallback(this, function(b) {
            //console.log("Back again ! with ", b.getState());
            var newRecord = b.getReturnValue();
            //console.log(newRecord[0]); // It's an Array, be careful (but don't worry too much, there is a LIMIT 1 in the APEX class)
            // console.log('The name is ', newRecord[0].Name);
            //alert('The record ID is ' + newRecord[0].Id + ' and the bon dieu de Name is ' + newRecord[0].Name) ;

            var appEvent = $A.get("e.c:sailfishGlobalRecordEvent");
            appEvent.setParams({
                'recordId': newRecord[0].Id,
                'recordName': newRecord[0].Name
            });
            appEvent.fire();
        });
        $A.enqueueAction(getRecord);

        
    },

    insertOnboardingRecord: function(component, onboarding) {

        var action = component.get("c.insertOnboardingRecordApex");
        action.setParams({
            "onboardingRecord": onboarding
        });
        action.setCallback(this, function(response, event) {
            var state = response.getState();
            // console.log("I'm back with " + state);
            if (state == 'SUCCESS') {
                var a = response.getReturnValue();
                var getRecord = component.get('c.getOnboardingRecordFromId');
                getRecord.setParams({
                    'Id': a.Id
                });
                getRecord.setCallback(this, function(b) {
                    console.log("Back again ! with ", b.getState());
                    var newRecord = b.getReturnValue();
               //     console.log(newRecord[0]); // It's an Array, be careful (but don't worry too much, there is a LIMIT 1 in the APEX class)
               //     console.log('The name is ', newRecord[0].Name);
                    
                    var appEvent = $A.get("e.c:sailfishGlobalRecordEvent");
                    appEvent.setParams({
                        'recordId': newRecord[0].Id,
                        'recordName': newRecord[0].Name
                    });
                    appEvent.fire();
                });
                $A.enqueueAction(getRecord);

            } else {
                console.log("Not success...");
                // TO-DO: add exception management
            }
        });
        $A.enqueueAction(action);
    },

    compute_avgBP: function(component) {

        var bps1 = component.get('v.fieldValues.Blood_Pressure_Systolic_1__c');
        var bpd1 = component.get('v.fieldValues.Blood_Pressure_Diastolic_1__c');
        var bps2 = component.get('v.fieldValues.Blood_Pressure_Systolic_2__c');
        var bpd2 = component.get('v.fieldValues.Blood_Pressure_Diastolic_2__c');
        var bps3 = component.get('v.fieldValues.Blood_Pressure_Systolic_3__c');
        var bpd3 = component.get('v.fieldValues.Blood_Pressure_Diastolic_3__c');

        var sumBps = 0;
        var sumBpd = 0;
        var sumNbr = 0;

        if (((bps1 !== null) && (bps1 > 0)) && ((bpd1 !== null) && (bpd1 > 0))) {
            sumBps += parseInt(bps1);
            sumBpd += parseInt(bpd1);
            sumNbr++;
        };
        if (((bps2 !== null) && (bps2 > 0)) && ((bpd2 !== null) && (bpd2 > 0))) {
            sumBps += parseInt(bps2);
            sumBpd += parseInt(bpd2);
            sumNbr++;
        };
        if (((bps3 !== null) && (bps3 > 0)) && ((bpd3 !== null) && (bpd3 > 0))) {
            sumBps += parseInt(bps3);
            sumBpd += parseInt(bpd3);
            sumNbr++;
        };

        if (sumNbr > 0) {
            //var avgBps = sumBps / sumNbr * 10;
            //var avgBpd = sumBpd / sumNbr * 10;
            ////var message = '[平均: ' + Math.trunc(avgBps)/10 + '/' + Math.trunc(avgBpd)/10 + "]";
            //var message = '平均: ' + Math.trunc(avgBps)/10 + '/' + Math.trunc(avgBpd)/10;
            //component.set('v.bpAverage', message);
            var avgBps = sumBps / sumNbr ;
            var avgBpd = sumBpd / sumNbr ;
            //var message = '[平均: ' + Math.trunc(avgBps)/10 + '/' + Math.trunc(avgBpd)/10 + "]";
            var message = '平均: ' + Math.round(avgBps) + '/' + Math.round(avgBpd);
            component.set('v.bpAverage', message);
 
        } else {
            component.set('v.bpAverage', '');
        }

    },

    checkError : function(component) {
        var limitNum = 255;
        // Updates to include both the essential error checking (including server error) + allow for "saving with notifications".
        // Original checkError is commented below for documentation.
        
        var messageData = '';
        
        // if it is not a 健診ドック, no error check 
        var examType = component.get('v.examType_values');
        if (examType !== '健診ドック') //----------------- start if (examType == '健診ドック')    
        {
            component.set('v.fieldValues.Current_UW_Status__c', null);
            return null;
        };
        
        // Check assessment date  
        var d1 = new Date(Date.parse(component.get('v.fieldValues.Medical_Report_Date__c')));
        var d2 = new Date(Date.parse(component.get('v.date14months')));
        console.log('d1', d1, 'd2', d2);
        if (d1 < d2) {
            var vOptions = {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour12: false
            };
            var vLocale = 'ja-JP';
            //messageData = '無効受診日 (有効日付:' + d2.toLocaleTimeString(vLocale, vOptions) + '～） \n';
        };

        var ob = component.get('v.fieldValues'); // ob is the placeholder for the record to be created 

        if (d1 == null || d1 == 'Invalid Date') {
            //messageData += '(1) 実施年月日\n';
        }
        
        component.set("v.errorCheck", messageData);
        
        //////////////////////////////////////////////////
        //
        // New underwriting status logic
        //
        //////////////////////////////////////////////////
        
        var fieldValues = component.get('v.fieldValues'); // fieldValue var changed from ob --> fieldValues for new logic
		var missingFields = '項目漏れ：\n';
        
        // Original Certificate // 原本証明
        var originalCertificate = false;
        var originalCertificateLable = '原本証明';
        if(fieldValues.CheckOriginal__c != fieldValues.CheckInsuredSignature__c) {
            originalCertificate = true;
        } else {
            missingFields += '原本証明, ';
        }
        console.log('originalCertificate: ' + originalCertificate);
        
        // Identification // 本人確認
        var identification = false;
        var identificationLabel = '本人確認';
        if(fieldValues.CheckNameDOB__c == true || fieldValues.CheckNameWork__c == true || fieldValues.CheckNameAgeGender__c == true) {
            identification = true;
        } else {
            missingFields += '本人確認, ';
        }
        console.log('identification: ' + identification);
        
        // Date of Implementation // 実施年月日
        var implementationDate = false;
        var implementationDateLabel = '実施年月日';
        if(d1 != null && d1 != 'Invalid Date') {
        	implementationDate = true;
        } else {
            missingFields += '実施年月日, ';
        }
        console.log('implementationDate: ' + implementationDate);
        
        // Medical Institution or Doctor // 医療機関名/医師名
        var doctor = false;
        var doctorLabel = '医療機関名/医師名';
        if(fieldValues.CheckDoctorInfo__c == true) {
            doctor = true;
        } else {
            missingFields += '医療機関名/医師名, ';
        }
        console.log('doctor: ' + doctor);
        
        // Height // 身長
        var height = false;
        var heightLabel = '身長';
        if(fieldValues.Height_cm__c > 0) {
            height = true;
        } else {
            missingFields += '身長, ';
        }
        console.log('height: ' + height);
      	
        // Weight // 体重
        var weight = false;
        var weightLabel = '体重';
        if(fieldValues.Weight_kg__c > 0) {
            weight = true;
        } else {
            missingFields += '体重, ';
        }
        console.log('weight: ' + weight);
      	
        // Blood Pressure // 血圧
        var bloodPressure = false;
        var bloodPressureLabel = '血圧';
        if(fieldValues.Blood_Pressure_Systolic_1__c > 0 && fieldValues.Blood_Pressure_Diastolic_1__c > 0) {
            bloodPressure = true;
        } else {
            missingFields += '血圧, ';
        }
        console.log('bloodPressure: ' + bloodPressure);
        
        // Urine Sugar // 尿糖
        var urineSugar = false;
        var urineSugarLabel = '尿糖';
        if(fieldValues.Urinary_Glucose_Rating__c != null || fieldValues.HbA1c_Count__c > 0 || fieldValues.Fasting_Blood_Glucose_Count__c > 0 || fieldValues.Standard_Blood_Glucose_Count__c > 0) {
            urineSugar = true;
        } else {
            missingFields += '尿糖, ';
        }
        console.log('urineSugar: ' + urineSugar);
        
        // Urine Protein // 尿蛋白
        var urineProtein = false;
        var urineProteinLabel = '尿蛋白';
        if(fieldValues.Urinary_Protein_Current_Rating__c != null) {
            urineProtein = true;
        } else {
            missingFields += '尿蛋白, ';
        }
        console.log('urineProtein: ' + urineProtein);
		
        var missingFields39 = missingFields.substring(0, missingFields.length - 2);
        missingFields39 = missingFields39.substring(0,limitNum);
        console.log('missingFields39 Length: ' + missingFields39.length);
        if(missingFields.length > limitNum) {
            missingFields39 += '...';
        }
        
        // Xrays and CTs // 胸部X線/胸部CT/内科診察
        var xray = false;
        var xrayLabel = '胸部X線/胸部CT/内科診察';
        if(fieldValues.CheckChestXam__c == true) {
            xray = true;
        } else {
            missingFields += '胸部X線/胸部CT/内科診察, ';
        }
        console.log('xray: ' + xray);

        // Electrocardiogram // 心電図
        var electrocardiogram = false;
        var electrocardiogramLabel = '心電図';
        if(fieldValues.CheckECG__c == true) {
            electrocardiogram = true;
        } else {
            missingFields += '心電図, ';
        }
        console.log('electrocardiogram: ' + electrocardiogram);

        // GOT // GOT
        var got = false;
        var gotLabel = 'GOT';
        if(fieldValues.GOT_Count_AST__c > 0) {
            got = true;
        } else {
            missingFields += 'GOT, ';
        }
        console.log('got: ' + got);

        // GPT // GPT
        var gpt = false;
        var gptLabel = 'GPT';
        if(fieldValues.GPT_Count_ALT__c > 0) {
            gpt = true;
        } else {
            missingFields += 'GPT, ';
        }
        console.log('gpt: ' + gpt);

        // γ-GTP // γ-GTP
        var yGtp = false;
        var yGtpLabel = 'γ-GTP';
        if(fieldValues.yGTP__c > 0) {
            yGtp = true;
        } else {
            missingFields += 'γ-GTP, ';
        }
        console.log('yGtp: ' + yGtp);

        // Hemoglobin // 血色素
        var hemoglobin = false;
        var hemoglobinLabel = '血色素';
        if(fieldValues.Hemoglobin_Level__c > 0) {
            hemoglobin = true;
        } else {
            missingFields += '血色素, ';
        }
        console.log('hemoglobin: ' + hemoglobin);

        // RBC // 赤血球
        var rbc = false;
        var rbcLabel = '赤血球';
        if(fieldValues.Red_Blood_Cell_Count__c > 0) {
            rbc = true;
        } else {
            missingFields += '赤血球, ';
        }
        console.log('rbc: ' + rbc);

        // Total Cholesterol // 総コレステロール/LDLコレステロール
        var cholestrol = false;
        var cholestrolLabel = '総コレステロール/LDLコレステロール';
        if(fieldValues.Cholesterol_Total__c > 0 || fieldValues.Cholesterol_LDL__c > 0) {
            cholestrol = true;
        } else {
            missingFields += '総コレステロール/LDLコレステロール, ';
        }
        console.log('cholestrol: ' + cholestrol);

        // Blood Sugar // 空腹時血糖/随時血糖/HbA1c
        var bloodSugar = false;
        var bloodSugarLabel = '空腹時血糖/随時血糖/HbA1c';
        if(fieldValues.HbA1c_Count__c > 0 || fieldValues.Fasting_Blood_Glucose_Count__c > 0 || fieldValues.Standard_Blood_Glucose_Count__c > 0) {
            bloodSugar = true;
        } else {
            missingFields += '空腹時血糖/随時血糖/HbA1c, ';
        }
        console.log('bloodSugar: ' + bloodSugar);

        var missingFields40 = missingFields.substring(0, missingFields.length - 2);
        missingFields40 = missingFields40.substring(0,limitNum);
        console.log('missingFields40 Length: ' + missingFields40.length);
        
        if(missingFields.length > limitNum) {
            missingFields40 += '...';
        }

        // WBC // 白血球
        var wbc = false;
        var wbcLabel = '白血球';
        if(fieldValues.White_Blood_Cell_Count__c > 0) {
            wbc = true;
        } else {
            missingFields += '白血球\n';
        }
        console.log('wbc: ' + wbc);

        // Creatinine // クレアチニン
        var creatinine = false;
        var creatinineLabel = 'クレアチニン';
        if(fieldValues.Creatinine_Count__c > 0) {
            creatinine = true;
        } else {
            missingFields += 'クレアチニン\n';
        }
        console.log('creatinine: ' + creatinine);

        // Stomach Xray // 胃部X線/胃部内視鏡
        var stomachXray = false;
        var stomachXrayLabel = '胃部X線/胃部内視鏡';
        if(fieldValues.CheckStomachXam__c == true) {
            stomachXray = true;
        } else {
            missingFields += '胃部X線/胃部内視鏡\n';
        }
        console.log('stomachXray: ' + stomachXray);

        // Abdomin CT // 腹部エコー/腹部CT
        var abdominCT = false;
        var abdominCTLabel = '腹部エコー/腹部CT';
        if(fieldValues.CheckAbdomenXam__c == true) {
            abdominCT = true;
        } else {
            missingFields += '腹部エコー/腹部CT\n';
        }
        console.log('abdominCT: ' + abdominCT);

        var generalDecision = 'Error';
        
        if(originalCertificate && identification && implementationDate && doctor && height && weight && bloodPressure && urineSugar && urineProtein && 
           xray && electrocardiogram && got && gpt && yGtp && hemoglobin && rbc && cholestrol && bloodSugar && 
           wbc && creatinine && stomachXray && abdominCT) {
            generalDecision = '人間ドック';
        } else {
            /* ara before_change
            if(fieldValues.Life_Assured_Age__c < 40) {
                if(originalCertificate && identification && implementationDate && doctor && height && weight && bloodPressure && urineSugar && urineProtein) {
                    generalDecision = '健診39歳';
                } else {
                    generalDecision = missingFields39;
                }
            }
            */
            //ara fixed
            if(fieldValues.Life_Assured_Age__c < 40) {
                 if(originalCertificate && identification && implementationDate && doctor && height && weight && bloodPressure && urineSugar && urineProtein && 
                    xray && electrocardiogram && got && gpt && yGtp && hemoglobin && rbc && cholestrol && bloodSugar)
                 {
                 generalDecision = '健診40歳';
                  }else{
                      if(originalCertificate && identification && implementationDate && doctor && height && weight && bloodPressure && urineSugar && urineProtein) {
                    	generalDecision = '健診39歳'; 	
                      }else{
                         generalDecision = missingFields39;   
                      }
                 }
                    
            }
            
            if(fieldValues.Life_Assured_Age__c >= 40) {
                if(originalCertificate && identification && implementationDate && doctor && height && weight && bloodPressure && urineSugar && urineProtein && 
                   xray && electrocardiogram && got && gpt && yGtp && hemoglobin && rbc && cholestrol && bloodSugar) {
                    generalDecision = '健診40歳';
                } else {
                    generalDecision = missingFields40;
                }
            }
        }
        
        component.set('v.fieldValues.Current_UW_Status__c', generalDecision); // save the result of the evaluation on the record 
        
    },
    
/*    
    checkError: function(component) {

        var messageData = '';
        var ningenDoc = '';
        var kenshin39 = '';
        var kenshin40 = '';

        component.set('v.fieldValues.Current_UW_Status__c', ''); // reset the value of the decision before evaluation

        // if it is not a 健診ドック, no error check 
        var examType = component.get('v.examType_values');
        if (examType !== '健診ドック') //----------------- start if (examType == '健診ドック')    
        {
            return null;
        };

        // Check assessment date  
        var d1 = new Date(Date.parse(component.get('v.fieldValues.Medical_Report_Date__c')));
        var d2 = new Date(Date.parse(component.get('v.date14months')));
        console.log('d1', d1, 'd2', d2);
        if (d1 < d2) {
            var vOptions = {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour12: false
            };
            var vLocale = 'ja-JP';
            messageData = '無効受診日 (有効日付:' + d2.toLocaleTimeString(vLocale, vOptions) + '～） \n';
        };

        // 健診ドック is selected, we start the evaluation process 

        var ob = component.get('v.fieldValues'); // ob is the placeholder for the record to be created 

        if ( ((ob.CheckOriginal__c) && ( ob.CheckOriginal__c == ob.CheckInsuredSignature__c))
          || ((! ob.CheckOriginal__c) && ( ob.CheckOriginal__c == ob.CheckInsuredSignature__c)) )  {
            messageData += '(0) 原本証明\n';
        }

        if (  (ob.CheckNameDOB__c && ob.CheckNameWork__c && ob.CheckNameAgeGender__c)
           || (ob.CheckNameDOB__c && ( ob.CheckNameWork__c || ob.CheckNameAgeGender__c))
           || (!ob.CheckNameDOB__c && !ob.CheckNameWork__c && !ob.CheckNameAgeGender__c)  
           || (!ob.CheckNameDOB__c && ( ob.CheckNameWork__c && ob.CheckNameAgeGender__c )) ) {
            messageData += '(0) 本人確認\n';
        }
        if (ob.CheckDoctorInfo__c == false) {
            messageData += '(0) 医療機関名・医師名\n';
        }
        if (d1 == null || d1 == 'Invalid Date') {
            messageData += '(1) 実施年月日\n';
        }
        if ((ob.Weight_kg__c == null) || (ob.Weight_kg_c == '') || (ob.Height_cm__c == null) || (ob.Height_cm__c == '')) {
            messageData += '(2) 身長 /体重\n';
        }
        if ((ob.Blood_Pressure_Diastolic_1__c == null || ob.Blood_Pressure_Diastolic_1__c == '') || (ob.Blood_Pressure_Systolic_1__c == null || ob.Blood_Pressure_Systolic_1__c == '')) {
            messageData += '(3) 血圧\n';
        }
        if ((ob.Urinary_Glucose_Rating__c == null) &&
            (ob.HbA1c_Count__c == null || ob.HbA1c_Count__c == '') &&
            (ob.Fasting_Blood_Glucose_Count__c == null || ob.Fasting_Blood_Glucose_Count__c == '') &&
            (ob.Standard_Blood_Glucose_Count__c == null || ob.Standard_Blood_Glucose_Count__c == '')
        ) {
            messageData += '(8) 尿糖\n';
        }

        var urinaryProtein = component.get('v.lifeAssuredUrinaryProteinCurrentRating_values');
        console.log('urinaryProtein:', urinaryProtein);
        if (urinaryProtein == null) {
            messageData += '(9) 尿蛋白\n';
        }

        if (messageData == '' && ob.Life_Assured_Age__c !== null && ob.Life_Assured_Age__c < 40) {
            kenshin39 = 'OK';
        }

        if (ob.CheckChestXam__c == false) {
            messageData += '(4) 胸部X線/胸部CT/内科診察\n';
        }
        if (ob.CheckECG__c == false) {
            messageData += '(4) 心電図\n';
        }
        if (ob.GOT_Count_AST__c == null) {
            messageData += '(5) GOT\n';
        }
        if (ob.GPT_Count_ALT__c == null) {
            messageData += '(5) GPT\n';
        }
        if (ob.yGTP__c == null) {
            messageData += '(5) γ-GTP\n';
        }
        if (ob.Hemoglobin_Level__c == null) {
            messageData += '(6) 血色素\n';
        }
        if (ob.Red_Blood_Cell_Count__c == null) {
            messageData += '(6) 赤血球\n';
        }
        if (ob.Cholesterol_Total__c == null && ob.Cholesterol_LDL__c == null ) {
            messageData += '(7) 総コレステロール\n';
        }
//        if (ob.Cholesterol_HDL__c == null) {
//            messageData += '(7) HDLコレステロール(HDL-C)　(健診39歳以外）\n';
//        }
//        if (ob.Triglyceride_Count__c == null) {
//            messageData += '(7) 中性脂肪(TG、トリグリセライド)　(健診39歳以外）\n';
//        } 
        if (ob.Fasting_Blood_Glucose_Count__c == null && ob.Standard_Blood_Glucose_Count__c == null && ob.HbA1c_Count__c == null) {
            messageData += '(7) 空腹時血糖/随時血糖/HbA1c\n';
        }

        if ((ob.Life_Assured_Age__c !== null && ob.Life_Assured_Age__c >= 40) && (messageData == '') ) {
            kenshin40 = 'OK';
        }

        if ( ((ob.White_Blood_Cell_Count__c !== null) && (ob.White_Blood_Cell_Count__c != '')) 
        && ((ob.Creatinine_Count__c !== null && ob.Creatinine_Count__c != '')) 
        && (ob.CheckStomachXam__c != false) 
        && (ob.CheckAbdomenXam__c != false) 
        && (messageData == '') ) {
            ningenDoc = 'OK';
        }

        component.set("v.errorCheck", messageData);

        var generalDecision = '';
        if (ningenDoc == 'OK') {
            generalDecision = '人間ドック';
        } else if (kenshin40 == 'OK') {
            generalDecision += '健診40';
        } else if (kenshin39 == 'OK') {
            generalDecision += '健診39';
        };

　　　　　　  console.log('>> generalDecision: ', generalDecision); 
        component.set('v.fieldValues.Current_UW_Status__c', generalDecision); // save the result of the evaluation on the record 

    },
*/
    
    helperFun: function(component, event, secId) {
        var acc = component.find(secId);
        for (var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');
            $A.util.toggleClass(acc[cmp], 'slds-hide');
        }
    },
    
    disclaimer: function(component){
/*
        var disclaimer = '項目漏れ: \n';
        var examType = component.get('v.fieldValues.ExamType__c');
        var age = component.get('v.fieldValues.Life_Assured_Date_of_Birth__c');
        var height = component.get('v.fieldValues.Height_cm__c');
        var weight = component.get('v.fieldValues.Weight_kg__c');
        var bps = component.get('v.fieldValues.Blood_Pressure_Systolic_1__c');
        var bpd = component.get('v.fieldValues.Blood_Pressure_Diastolic_1__c');
       
        var hba1c = component.get('v.fieldValues.HbA1c_Count__c');
        var fastingBloodGlucose = component.get('v.fieldValues.Fasting_Blood_Glucose_Count__c');
        var standardBloodGlucose = component.get('v.fieldValues.Standard_Blood_Glucose_Count__c');
        var urineGlucose = component.get('v.fieldValues.Urinary_Glucose_Rating__c');
        var urineProtein = component.get('v.fieldValues.Urinary_Protein_Current_Rating__c');
        
        var ecg = component.get('v.fieldValues.CheckECG__c');
        var chestXray = component.get('v.fieldValues.CheckChestXam__c');
        var stomachXray = component.get('v.fieldValues.CheckStomachXam__c');
        var abdomenXray = component.get('v.fieldValues.CheckAbdomenXam__c');
        
        var got = component.get('v.fieldValues.GOT_Count_AST__c');
        var gpt = component.get('v.fieldValues.GPT_Count_ALT__c');
        var ygtp = component.get('v.fieldValues.yGTP__c');
        
        var hemoglobin = component.get('v.fieldValues.Hemoglobin_Level__c');
        var rbc = component.get('v.fieldValues.Red_Blood_Cell_Count__c');
        var wbc = component.get('v.fieldValues.White_Blood_Cell_Count__c');
        var ldl = component.get('v.fieldValues.Cholesterol_LDL__c');
        var creatinine = component.get('v.fieldValues.Creatinine_Count__c');
        
        // if blood glucose is taken via urine or blood
        if (hba1c == null && fastingBloodGlucose == null && standardBloodGlucose == null && urineGlucose == null) {
            var glucoseBasic = null;
        } else {
            var glucoseBasic = 'ok';
        }
        // if blood Glucose is taken via Blood
        if (hba1c == null && fastingBloodGlucose == null && standardBloodGlucose == null) {  
            var glucoseAdv = null;
        } else {
            var glucoseAdv = 'ok';
        }
        
        
        if (age == "" || age == null) {
            component.set('v.disclaimer', 'Age is null');
        }   else {
            if (height == null) {
                disclaimer = disclaimer + ' -height \n'
            }
            if (weight == null) {
                disclaimer = disclaimer + ' -weight \n'
            }
            if (bps == null || bpd == null) {
                disclaimer = disclaimer + ' -bloodpressure \n'
            }
            if (glucoseBasic == null) {
                disclaimer = disclaimer + ' -bloodGlucoseBasic \n'
            }
            if (glucoseAdv == null) {
                disclaimer = disclaimer + ' -bloodGlucoseAdv \n'
            }
            if (urineProtein == null) {
                disclaimer = disclaimer + ' -urineProtein \n'
            }
            if(age >=40 || examType == '健診ドック') {
                //disclaimer = disclaimer + examType + '\n'
                if (ecg == false) {
                    disclaimer = disclaimer + ' -ecg \n'
                }
                if (chestXray == false || stomachXray == false || abdomenXray == false) {
                    disclaimer = disclaimer + ' -xray \n'
                }
                if (got == null || gpt == null || ygtp == null) {
                    disclaimer = disclaimer + ' -liver \n'
                }
                if (hemoglobin == null) {
                    disclaimer = disclaimer + ' -hemoglobin \n'
                } 
                if (rbc == null) {
                    disclaimer = disclaimer + ' -rbc \n'
                }
            }
            if(examType == '健診ドック') {
                if (wbc == null) {
                    disclaimer = disclaimer + ' -wbc \n'
                }
                if (ldl == null) {
                    disclaimer = disclaimer + ' -ldl \n'
                }
                if (creatinine == null) {
                    disclaimer = disclaimer + ' -creatinine \n'
                }
            }
        }
        component.set('v.disclaimer', disclaimer);
*/
    	}
    /*ara 
        closeFocusedTab : function(component, event, helper) {
        var workspaceAPI = component.find('workspace');
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
  
    }
          */
    
})