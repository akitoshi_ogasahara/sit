({
    handleApplicationEvent: function(component, event, helper) {
        component.set("v.recordName", event.getParam('recordName'));
        component.set("v.statusUW", ''); 
        
        // Run Ruleset
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        //Search Results
        var action = component.get("c.getSearchRulesetResults");
        action.setParams({
            'recordName': component.get("v.recordName")
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // v.results refers to the aura:attribute name="results" in the component (cmp), v standing for value provider 
                component.set("v.resultRuleset", response.getReturnValue());
                console.log('Here is the response');
                console.log(response.getReturnValue());
                var listResults = response.getReturnValue();
                for (let i of listResults) {
                    //console.log('new i.Result_Id__c:', i.Result_Id__c) ; 
                    if ( i.Result_Id__c == 0) {
                        component.set("v.statusUW", i.Result_Rating__c) ; 
                    } 
                }; 
                var displayRulesetResults = [];
                displayRulesetResults.push({
                    title: '評点', r1: '普通死亡', r1v: '', r2: '重大疾病', r2v: '' 
                });
                console.log(listResults);
                for (let i of listResults) {
                    var searchKey = i.Result_Definition_Jp__c;
                    var index = displayRulesetResults.findIndex(displayRulesetResults => displayRulesetResults.r1 === searchKey);
                    if (index >= 0) {
                        if (i.Result_Score__c >= 0) {
                            displayRulesetResults[index].r1v = i.Result_Score__c;
                        } else {
                            displayRulesetResults[index].r1v = i.Result_Rating__c;
                        }
                    } else {
                        index = displayRulesetResults.findIndex(displayRulesetResults => displayRulesetResults.r2 === searchKey);
                        if (index >= 0) {
                            if (i.Result_Score__c >= 0) {
                                displayRulesetResults[index].r2v = i.Result_Score__c;
                            } else {
                                displayRulesetResults[index].r2v = i.Result_Rating__c;
                            }
                        }
                    };
                }           
                component.set("v.displayRulesetResults", displayRulesetResults);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
        
        
        
        // Run Lien
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        //Search Results
        var action = component.get("c.getSearchLienResults");
        action.setParams({
            'recordName': component.get("v.recordName")
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // v.results refers to the aura:attribute name="results" in the component (cmp), v standing for value provider 
                component.set("v.resultRuleset", response.getReturnValue());
                console.log('Here is the response');
                console.log(response.getReturnValue());
                var listResults = response.getReturnValue();
                for (let i of listResults) {
                    //console.log('i.Result_Id__c:', i.Result_Id__c) ; 
                    if ( i.Result_Id__c == 0) {
                        component.set("v.StatusUW", i.Result_Rating__c) ; 
                    } 
                }; 
                var displayRulesetResults = component.get("v.displayRulesetResults");
                displayRulesetResults.push({
                    title: '削減', r1: '普通死亡 - 尿蛋白潜血削減', r1v: '', r2: '重大疾病 - 尿蛋白潜血削減', r2v: '' 
                });
                console.log(listResults);
                for (let i of listResults) {
                    var searchKey = i.Result_Definition_Jp__c;
                    var index = displayRulesetResults.findIndex(displayRulesetResults => displayRulesetResults.r1 === searchKey);
                    if (index >= 0) {
                        if (i.Result_Score__c >= 0) {
                            displayRulesetResults[index].r1v = i.Result_Score__c;
                        } else {
                            displayRulesetResults[index].r1v = i.Result_Rating__c;
                        }
                    } else {
                        index = displayRulesetResults.findIndex(displayRulesetResults => displayRulesetResults.r2 === searchKey);
                        if (index >= 0) {
                            if (i.Result_Score__c >= 0) {
                                displayRulesetResults[index].r2v = i.Result_Score__c;
                            } else {
                                displayRulesetResults[index].r2v = i.Result_Rating__c;
                            }
                        }
                    };
                }           
                component.set("v.displayRulesetResults", displayRulesetResults);
            } else {
                console.log("Failed with state: " + state);
            }
        });        
        // Send action off to be executed
        $A.enqueueAction(action);





        // Run Rules
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        //Search Results
        var action = component.get("c.getSearchRuleResults");
        action.setParams({
            'recordName': component.get("v.recordName")
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // v.results refers to the aura:attribute name="results" in the component (cmp), v standing for value provider 
                component.set("v.resultRule", response.getReturnValue());
                console.log('Here is the response');
                console.log(response.getReturnValue());
                var listResults = response.getReturnValue();
                for (let i of listResults) {
                    //console.log('i.Result_Id__c:', i.Result_Id__c) ; 
                    if ( i.Result_Id__c == 0) {
                        component.set("v.StatusUW", i.Result_Rating__c) ; 
                    } 
                }; 
                var displayRuleResults = [];
                displayRuleResults.push({
                    title: 'BMI', r1: '普通死亡 - BMI', r1v: '', r2: '重大疾病 - BMI', r2v: '' 
                });
                displayRuleResults.push({
                    title: '血圧', r1: '普通死亡 - 血圧',r1v: '', r2: '重大疾病 - 血圧', r2v: ''
                });
                displayRuleResults.push({
                    title: '尿糖', r1: '普通死亡 - 尿糖',r1v: '', r2: '重大疾病 - 尿糖', r2v: ''
                });
                displayRuleResults.push({
                    title: '尿糖・尿蛋白', r1: '',r1v: '', r2: '重大疾病 - 尿糖・尿蛋白', r2v: ''
                });
                displayRuleResults.push({
                    title: '尿蛋白・尿潜血', r1: '普通死亡 - 尿蛋白・尿潜血',r1v: '', r2: '重大疾病 - 尿蛋白・尿潜血', r2v: ''
                });
                displayRuleResults.push({
                    title: '尿蛋白潜血削減', r1: '普通死亡 - 尿蛋白潜血削減',r1v: '', r2: '重大疾病 - 尿蛋白潜血削減', r2v: ''
                });
                displayRuleResults.push({
                    title: '糖尿病', r1: '普通死亡 - 糖尿',r1v: '', r2: '重大疾病 - 糖尿', r2v: ''
                });
                displayRuleResults.push({
                    title: 'クレアチニン', r1: '普通死亡 - クレアチニン',r1v: '', r2: '重大疾病 - クレアチニン', r2v: ''
                });
                displayRuleResults.push({
                    title: '白血球', r1: '普通死亡 - 白血球',r1v: '', r2: '重大疾病 - 白血球', r2v: ''
                });
                displayRuleResults.push({
                    title: '肝機能', r1: '普通死亡 - 肝機能',r1v: '', r2: '重大疾病 - 肝機能', r2v: ''
                });
                displayRuleResults.push({
                    title: '赤血球', r1: '普通死亡 - 赤血球',r1v: '', r2: '重大疾病 - 赤血球', r2v: ''
                });
                displayRuleResults.push({
                    title: 'ヘモグロビン', r1: '普通死亡 - ヘモグロビン',r1v: '', r2: '重大疾病 - ヘモグロビン', r2v: ''
                });
                displayRuleResults.push({
                    title: 'TO-cho', r1: '普通死亡 - 総コレステロール',r1v: '', r2: '重大疾病 - 総コレステロール', r2v: ''
                });
                displayRuleResults.push({
                    title: 'LDL-cho', r1: '普通死亡 - LDLコレステロール',r1v: '', r2: '重大疾病 - LDLコレステロール', r2v: ''
                });
                displayRuleResults.push({
                    title: 'HbA1c', r1: '普通死亡 - HbA1c',r1v: '', r2: '重大疾病 - HbA1c', r2v: ''
                });
                displayRuleResults.push({
                    title: '空腹時血糖', r1: '普通死亡 - 空腹時血糖',r1v: '', r2: '重大疾病 - 空腹時血糖', r2v: ''
                });
                displayRuleResults.push({
                    title: '随時血糖', r1: '普通死亡 - 随時血糖',r1v: '', r2: '重大疾病 - 随時血糖', r2v: ''
                });
                console.log(listResults);
                for (let i of listResults) {
                    var searchKey = i.Result_Definition_Jp__c;
                    var index = displayRuleResults.findIndex(displayRuleResults => displayRuleResults.r1 === searchKey);
                    if (index >= 0) {
                        if (i.Result_Score__c >= 0) {
                            displayRuleResults[index].r1v = i.Result_Score__c;
                        } else {
                            displayRuleResults[index].r1v = i.Result_Rating__c;
                        }
                    } else {
                        index = displayRuleResults.findIndex(displayRuleResults => displayRuleResults.r2 === searchKey);
                        if (index >= 0) {
                            if (i.Result_Score__c >= 0) {
                                displayRuleResults[index].r2v = i.Result_Score__c;
                            } else {
                                displayRuleResults[index].r2v = i.Result_Rating__c;
                            }
                        }
                    };
                }           
                component.set("v.displayRuleResults", displayRuleResults);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
        
        
        
    }
})