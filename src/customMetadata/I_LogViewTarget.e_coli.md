<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>契約内容個別照会（個人保険）</label>
    <protected>false</protected>
    <values>
        <field>ActionType__c</field>
        <value xsi:type="xsd:string">ページ</value>
    </values>
    <values>
        <field>ApiName__c</field>
        <value xsi:type="xsd:string">e_coli</value>
    </values>
    <values>
        <field>SelectCategory__c</field>
        <value xsi:type="xsd:string">policy</value>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
