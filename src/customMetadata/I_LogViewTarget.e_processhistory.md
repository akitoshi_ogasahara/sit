<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>手続履歴</label>
    <protected>false</protected>
    <values>
        <field>ActionType__c</field>
        <value xsi:type="xsd:string">ページ</value>
    </values>
    <values>
        <field>ApiName__c</field>
        <value xsi:type="xsd:string">e_processhistory</value>
    </values>
    <values>
        <field>SelectCategory__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
