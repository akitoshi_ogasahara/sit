<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SailfishURL</label>
    <protected>false</protected>
    <values>
        <field>SVFProcedureURL__c</field>
        <value xsi:type="xsd:string">https://a0483wvu.secure.svfcloud.com/api/v1/procedures/Sailfish_DirectPrint</value>
    </values>
</CustomMetadata>
