<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>保有契約_個人保険（事業保険）</label>
    <protected>false</protected>
    <values>
        <field>ActionType__c</field>
        <value xsi:type="xsd:string">帳票ダウンロード</value>
    </values>
    <values>
        <field>ApiName__c</field>
        <value xsi:type="xsd:string">hoyuukeiyaku_kojin</value>
    </values>
    <values>
        <field>SelectCategory__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>isActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
