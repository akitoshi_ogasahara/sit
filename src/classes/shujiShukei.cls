global class shujiShukei {
    /** メッセージ */
    public static final String STATUS_NOMAL = '見込案件の金額情報を集計を作成しました。';
    public static final String STATUS_NODATA = '処理対象データがありませんでした。';
    public static final String STATUS_ERROR = '処理中下記のエラーが発生しました。';

    public static final String HONSHAEIGYOBU = '本社営業部';
    public static final String TOTAL = 'TOTAL';
    public static final String REPLACE_AREA = '###Area###';
    public static final String REPLACE_EIGY = '###Eigyo###';

    public static final String SQL_COUNT = 'count';                 //集計データ存在確認用
    public static final String SQL_H1COUNT = 'h1_count';            //集計データ存在確認用：本社集計１
    public static final String SQL_H2COUNT = 'h2_count';            //集計データ存在確認用：本社集計２

    public static final String SQL_KUBUN1 = 'kubun1';               //保険種類：区分①集計
    public static final String SQL_CKUBUN1 = 'c_kubun1';            //保険種類：区分①(C)集計用
    public static final String SQL_H1KUBUN1 = 'h1_kubun1';          //保険種類：区分①本社集計１
    public static final String SQL_H2KUBUN1 = 'h2_kubun1';          //保険種類：区分①本社集計２

    public static final String SQL_KUBUN2 = 'kubun2';               //保険種類：区分②集計
    public static final String SQL_CKUBUN2 = 'c_kubun2';            //保険種類：区分②(C)集計用
    public static final String SQL_H1KUBUN2 = 'h1_kubun2';          //保険種類：区分②本社集計１
    public static final String SQL_H2KUBUN2 = 'h2_kubun2';          //保険種類：区分②本社集計２

    public static final String SQL_TOTAL = 'total';                 //保険種類：TOTAL集計
    public static final String SQL_CTOTAL = 'c_total';              //保険種類：TOTAL(C)集計用
    public static final String SQL_H1TOTAL = 'h1_total';            //保険種類：TOTAL本社集計１
    public static final String SQL_H2TOTAL = 'h2_total';            //保険種類：TOTAL本社集計２

    public static final String SQL_SHOUHIN = 'shouhin';             //商品種別：集計
    public static final String SQL_CSHOUHIN = 'c_shouhin';          //商品種別：(C)集計用
    public static final String SQL_H1SHOUHIN = 'h1_shouhin';        //商品種別：本社集計１
    public static final String SQL_H2SHOUHIN = 'h2_shouhin';        //商品種別：本社集計２

    public static final String SQL_CHANNEL = 'channel';             //チャネル：集計
    public static final String SQL_CCHANNEL = 'c_channel';          //チャネル：(C)集計用
    public static final String SQL_H1CHANNEL = 'h1_channel';        //チャネル：本社集計１
    public static final String SQL_H2CHANNEL = 'h2_channel';        //チャネル：本社集計２

    public static final String SQL_ETC = 'etc';                     //予算3：集計
    public static final String SQL_CETC = 'c_etc';                  //予算3：(C)集計用
    public static final String SQL_H1ETC = 'h1_etc';                //予算3：本社集計１
    public static final String SQL_H2ETC = 'h2_etc';                //予算3：本社集計２

    public static final String SQL_ETC4 = 'etc4';                   //予算4：集計
    public static final String SQL_CETC4 = 'c_etc4';                //予算4：(C)集計用
    public static final String SQL_H1ETC4 = 'h1_etc4';              //予算4：本社集計１
    public static final String SQL_H2ETC4 = 'h2_etc4';              //予算4：本社集計２

    public static final String SQL_ETC5 = 'etc5';                   //予算5：集計
    public static final String SQL_CETC5 = 'c_etc5';                //予算5：(C)集計用
    public static final String SQL_H1ETC5 = 'h1_etc5';              //予算5：本社集計１
    public static final String SQL_H2ETC5 = 'h2_etc5';              //予算5：本社集計２

    public static final String SQL_YOSAN = 'yosan';                 //予算：集計
    public static final String SQL_H1YOSAN = 'h1_yosan';            //予算：本社集計１
    public static final String SQL_H2YOSAN = 'h2_yosan';            //予算：本社集計２

    public static final String SQL_SMALLCHANNEL = 's_channel';      //チャネル 小分類：集計
    public static final String SQL_CSMALLCHANNEL = 'c_s_channel';   //チャネル 小分類：(C)集計用
    public static final String SQL_H1SMALLCHANNEL = 'h1_s_channel'; //チャネル 小分類：本社集計１
    public static final String SQL_H2SMALLCHANNEL = 'h2_s_channel'; //チャネル 小分類：本社集計２

    public static String strID {get;set;}                           //週次活動報告ID
    public static Date dtDate {get;set;}                            //週次活動報告の日付
    public static String strUser {get;set;}                         //所有者
    public static Boolean blnLastMFlg {get;set;}                    //月末フラグ
    public static String strArea {get;set;}                         //所有者エリア
    public static String strEigy {get;set;}                         //所有者営業部
    public static String tmpDtKey1 {get;set;}                       //月初
    public static String tmpDtKey2 {get;set;}                       //月末

    public static Map<String,String> map_sql {get;set;}             //クエリ発行用マップ
    public static WeeklyTotalBudget__c tmp_wtb {get;set;}           //週次予算集計
    public static List<WeeklyTotalBudget__c> ins_wtb {get;set;}     //週次予算集計登録用リスト
    public static List<WeeklyTotalBudget__c> del_wtb {get;set;}     //週次予算集計削除用リスト
    public static Map<String,WeeklyTotalBudget__c> map1 {get;set;}  //週次予算集計リスト

    /*------------------------------------------------------------------------
    * 週次予算集計を作成する。
    *
    * @param ：週次活動報告ID、日付、所有者、月末フラグ
    * @return：処理メッセージ
    *------------------------------------------------------------------------
    */
    webService static String execute(String id, String strDate, String strU, Boolean blnLastM) {
        strID = id;                                     //週次活動報告ID
        strArea = '';                                   //所有者エリア
        strEigy = '';                                   //所有者営業部
        strUser = strU;                                 //所有者
        blnLastMFlg = blnLastM;                         //月末フラグ
        dtDate = Util.cnvStrDate(strDate);              //日付
        tmpDtKey1 = Util.getFirstDate(strDate);         //月初
        tmpDtKey2 = Util.getLastDate(strDate);          //月末

        //トランザクション
        Savepoint sp = Database.setSavepoint();
        try{
            map1 = new Map<String,WeeklyTotalBudget__c>();
            getAreaEigyoubu();      //所有者の「エリア」「営業部」を取得
            setSQL();               //SQL発行

            //集計種類取得
            List<Schema.PicklistEntry> pList = Util.getPicklist(new WeeklyTotalBudget__c(), 'Type__c');
            String hokenShubetu = '';       //1:保険種類
            String shouhinShubetu = '';     //2:商品種別
            String channelShubetu = '';     //3:チャネル
            String etcShubetu = '';         //4:予算3
            String etcShubetu4 = '';        //5:予算4
            String etcShubetu5 = '';        //6:予算5
            String smallChannelShubetu = '';//7:チャネル 小分類
            for (Schema.PicklistEntry p : pList) {
                if (String.valueOf(p.getValue()).startsWith('1')) hokenShubetu = p.getValue();
                if (String.valueOf(p.getValue()).startsWith('2')) shouhinShubetu = p.getValue();
                if (String.valueOf(p.getValue()).startsWith('3')) channelShubetu = p.getValue();
                if (String.valueOf(p.getValue()).startsWith('4')) etcShubetu = p.getValue();
                if (String.valueOf(p.getValue()).startsWith('5')) etcShubetu4 = p.getValue();
                if (String.valueOf(p.getValue()).startsWith('6')) etcShubetu5 = p.getValue();
                if (String.valueOf(p.getValue()).startsWith('7')) smallChannelShubetu = p.getValue();
            }

            //本社営業部担当者確認
            if (check_HonshaTanto()) {
                //集計データ存在確認
                if (check_noData(map_sql.get(SQL_H1COUNT)) && check_noData(map_sql.get(SQL_H2COUNT))) return STATUS_NODATA;

                //【本社集計１】
                strArea = HONSHAEIGYOBU;
                strEigy = '支援有';
                putMap(map_sql.get(SQL_H1KUBUN1), hokenShubetu);              //保険種類：区分①集計
                putMap(map_sql.get(SQL_H1KUBUN2), hokenShubetu);              //保険種類：区分②集計
                putMap(map_sql.get(SQL_H1SHOUHIN), shouhinShubetu);           //商品種別：集計
                putMap(map_sql.get(SQL_H1CHANNEL), channelShubetu);           //チャネル：集計
                putMap(map_sql.get(SQL_H1ETC), etcShubetu);                   //予算3   ：集計
                putMap(map_sql.get(SQL_H1ETC4), etcShubetu4);                 //予算4   ：集計
                putMap(map_sql.get(SQL_H1ETC5), etcShubetu5);                 //予算5   ：集計
                putMap(map_sql.get(SQL_H1SMALLCHANNEL), smallChannelShubetu); //チャネル 小分類：集計
                totalPutMap(map_sql.get(SQL_H1TOTAL), hokenShubetu);          //TOTAL   ：集計
                yoSanPutMap(map_sql.get(SQL_H1YOSAN).replace(REPLACE_AREA,strArea).replace(REPLACE_EIGY,strEigy));                  //予算    ：集計
                //既存データ削除・登録
                insertWTB();

                //【本社集計２】
                strArea = HONSHAEIGYOBU;
                strEigy = '直扱い';
                putMap(map_sql.get(SQL_H2KUBUN1), hokenShubetu);               //保険種類：区分①集計
                putMap(map_sql.get(SQL_H2KUBUN2), hokenShubetu);               //保険種類：区分②集計
                putMap(map_sql.get(SQL_H2SHOUHIN), shouhinShubetu);            //商品種別：集計
                putMap(map_sql.get(SQL_H2CHANNEL), channelShubetu);            //チャネル：集計
                putMap(map_sql.get(SQL_H2ETC), etcShubetu);                    //予算3   ：集計
                putMap(map_sql.get(SQL_H2ETC4), etcShubetu4);                  //予算4   ：集計
                putMap(map_sql.get(SQL_H2ETC5), etcShubetu5);                  //予算5   ：集計
                putMap(map_sql.get(SQL_H2SMALLCHANNEL), smallChannelShubetu);  //チャネル 小分類：集計
                totalPutMap(map_sql.get(SQL_H2TOTAL), hokenShubetu);    //TOTAL   ：集計
                yoSanPutMap(map_sql.get(SQL_H2YOSAN).replace(REPLACE_AREA,strArea).replace(REPLACE_EIGY,strEigy));                  //予算    ：集計
                //既存データ削除・登録
                insertWTB();

            } else {
                //集計データ存在確認
                if (check_noData(map_sql.get(SQL_COUNT))) return STATUS_NODATA;

                putMap(map_sql.get(SQL_KUBUN1), hokenShubetu);               //保険種類：区分①集計
                putMap(map_sql.get(SQL_KUBUN2), hokenShubetu);               //保険種類：区分②集計
                putMap(map_sql.get(SQL_SHOUHIN), shouhinShubetu);            //商品種別：集計
                putMap(map_sql.get(SQL_CHANNEL), channelShubetu);            //チャネル：集計
                putMap(map_sql.get(SQL_ETC), etcShubetu);                    //予算3   ：集計
                putMap(map_sql.get(SQL_ETC4), etcShubetu4);                  //予算4   ：集計
                putMap(map_sql.get(SQL_ETC5), etcShubetu5);                  //予算5   ：集計
                putMap(map_sql.get(SQL_SMALLCHANNEL), smallChannelShubetu);  //チャネル 小分類：集計
                totalPutMap(map_sql.get(SQL_TOTAL), hokenShubetu);           //TOTAL   ：集計
                yoSanPutMap(map_sql.get(SQL_YOSAN));                         //予算    ：集計

                cPutMap(map_sql.get(SQL_CKUBUN1),hokenShubetu);                //保険種類：区分①(C)集計
                cPutMap(map_sql.get(SQL_CKUBUN2),hokenShubetu);                //保険種類：区分②(C)集計
                cPutMap(map_sql.get(SQL_CSHOUHIN),shouhinShubetu);             //商品種別：(C)集計
                cPutMap(map_sql.get(SQL_CCHANNEL),channelShubetu);             //チャネル：(C)集計
                cPutMap(map_sql.get(SQL_CETC),etcShubetu);                     //予算3   ：(C)集計
                cPutMap(map_sql.get(SQL_CETC4),etcShubetu4);                   //予算4   ：(C)集計
                cPutMap(map_sql.get(SQL_CETC5),etcShubetu5);                   //予算5   ：(C)集計
                cPutMap(map_sql.get(SQL_CSMALLCHANNEL), smallChannelShubetu);  //チャネル 小分類：集計
                totalcPutMap(map_sql.get(SQL_CTOTAL), hokenShubetu);           //TOTAL   ：(C)集計
                //既存データ削除・登録
                insertWTB();
            }
            return STATUS_NOMAL;
        } catch (Exception e) {
            Database.rollback(sp);
            return STATUS_ERROR + '\n' + e.getMessage();
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：週次予算集計削除・登録
    * 戻り値              ：
    * 機能概要            ：週次予算集計削除・登録
    *------------------------------------------------------------------------
    */
    private static void insertWTB() {
        //既存データ削除
        del_wtb = new List<WeeklyTotalBudget__c>();
        for (WeeklyTotalBudget__c wtb : [SELECT Id FROM WeeklyTotalBudget__c
                                         WHERE WeeklyActivityReports__c =: strID    //週次活動報告ID
                                         AND Area__c =: strArea                     //エリア
                                         AND SalesUnit__c =: strEigy                //営業部
                                         AND WeekStartDay__c =: dtDate]) {          //週開始日
            del_wtb.add(wtb);
            if (del_wtb.size() == 200) {
                delete del_wtb;
                del_wtb = new List<WeeklyTotalBudget__c>();
            }
        }
        if (del_wtb.size() > 0) { delete del_wtb; }

        //登録
        ins_wtb = new List<WeeklyTotalBudget__c>();
        for(String key : map1.keySet()){
            ins_wtb.add(map1.get(key));
            if(ins_wtb.size() == 200){
                insert ins_wtb;
                ins_wtb = new List<WeeklyTotalBudget__c>();
            }
        }
        if (ins_wtb.size() > 0) { insert ins_wtb; }
        //マップクリア
        map1.clear();
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：GAPE,WAPE,件数の集計
    * 戻り値              ：
    * 機能概要            ：GAPE,WAPE,件数の集計
    *------------------------------------------------------------------------
    */
    private static void putMap(String sql, String strKubun) {
system.debug('=================【SQL：GAPE,WAPE,件数の集計】' + sql);
        String strKey = '';
        for (sObject opp : Database.query(sql)) {
            strKey = strArea + strEigy + strKubun.substring(0,2) + opp.get('a');    //キー設定:エリア＋営業部＋予算区分(頭2桁)＋分類名
            tmp_wtb = new WeeklyTotalBudget__c();
            tmp_wtb.Area__c = strArea;                                  //エリア
            tmp_wtb.SalesUnit__c = strEigy;                             //営業部
            tmp_wtb.FAINAL_FLG__c = blnLastMFlg;                        //月末フラグ
            tmp_wtb.WeekStartDay__c = dtDate;                           //週開始日
            tmp_wtb.WeeklyActivityReports__c = strID;                   //週間活動報告
            tmp_wtb.Type__c = strKubun;                                 //予算区分
            tmp_wtb.GroupName__c = String.valueOf(opp.get('a'));        //分類名
            tmp_wtb.GAPE_Max__c = (Decimal)opp.get('b');                //GAPE
            tmp_wtb.WAPE_Max__c = (Decimal)opp.get('c');                //WAPE
            tmp_wtb.Count__c = (Decimal)opp.get('d');                   //件数
            map1.put(strKey,tmp_wtb);
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：保険種類のTOTAL集計
    * 戻り値              ：
    * 機能概要            ：保険種類のTOTAL集計
    *------------------------------------------------------------------------
    */
    private static void totalPutMap(String sql, String strKubun) {
system.debug('=================【SQL：TOTAL集計】' + sql);
        String strKey = strArea + strEigy + strKubun.substring(0,2) + TOTAL;            //キー設定:エリア＋営業部＋予算区分(頭2桁)＋TOTAL
        Decimal tot_GAPE = 0;
        Decimal tot_WAPE = 0;
        Decimal tot_CONT = 0;
        for (Opportunity opp : Database.query(sql)) {
            tot_GAPE += (Decimal)opp.get('GAPE__c');                //GAPE合計
            tot_WAPE += (Decimal)opp.get('WAPE__c');                //WAPE合計
            tot_CONT += (Decimal)opp.get('Count__c');               //件数合計
        }
        tmp_wtb = new WeeklyTotalBudget__c();
        tmp_wtb.Area__c = strArea;                                  //エリア
        tmp_wtb.SalesUnit__c = strEigy;                             //営業部
        tmp_wtb.FAINAL_FLG__c = blnLastMFlg;                        //月末フラグ
        tmp_wtb.WeekStartDay__c = dtDate;                           //週開始日
        tmp_wtb.WeeklyActivityReports__c = strID;                   //週間活動報告
        tmp_wtb.Type__c = strKubun;                                 //予算区分
        tmp_wtb.GroupName__c = TOTAL;                               //分類名
        tmp_wtb.GAPE_Max__c = tot_GAPE;                             //GAPE
        tmp_wtb.WAPE_Max__c = tot_WAPE;                             //WAPE
        tmp_wtb.Count__c = tot_CONT;                                //件数
        map1.put(strKey,tmp_wtb);
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：予算集計
    * 戻り値              ：
    * 機能概要            ：予算集計
    *------------------------------------------------------------------------
    */
    private static void yoSanPutMap(String sql){
system.debug('=================【SQL：予算集計】' + sql);
        String strKey = '';
        Map<String,sObject> map_yosan = new Map<String,sObject>();
        for (sObject mtb : Database.query(sql)) {
            strKey = strArea + strEigy + String.valueOf(mtb.get('a')).substring(0,2) + String.valueOf(mtb.get('b'));    //キー設定:エリア＋営業部＋予算区分(頭2桁)＋分類名
            map_yosan.put(strKey,mtb);
        }

        for(String key : map1.keySet()){
            if (map_yosan.containsKey(key)) {
                map1.get(key).GAPEBudget__c = (Decimal)map_yosan.get(key).get('c');
                map1.get(key).WAPEBudget__c = (Decimal)map_yosan.get(key).get('d');
            }
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：GAPE(C)、WAPE(C)集計
    * 戻り値              ：
    * 機能概要            ：GAPE(C)、WAPE(C)集計
    *------------------------------------------------------------------------
    */
    private static void cPutMap(String sql, String strKubun){
system.debug('=================【SQL：(C)集計】' + sql);
        String strKey = '';
        Map<String,sObject> map_c = new Map<String,sObject>();
        for (sObject cOpp : Database.query(sql)) {
            strKey = strArea + strEigy + strKubun.substring(0,2) + String.valueOf(cOpp.get('a'));       //キー設定:エリア＋営業部＋予算区分(頭2桁)＋分類名
            map_c.put(strKey,cOpp);
        }

        for(String key : map1.keySet()){
            if (map_c.containsKey(key)) {
                map1.get(key).GAPE_Min__c = map1.get(key).GAPE_Max__c + (Decimal)map_c.get(key).get('b');   //GAPE(C)
                map1.get(key).WAPE_Min__c = map1.get(key).WAPE_Max__c + (Decimal)map_c.get(key).get('c');   //WAPE(C)
            } else {
                if (map1.get(key).GAPE_Min__c == null) {
                    map1.get(key).GAPE_Min__c = map1.get(key).GAPE_Max__c;
                }
                if (map1.get(key).WAPE_Min__c == null) {
                    map1.get(key).WAPE_Min__c = map1.get(key).WAPE_Max__c;
                }
            }
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：GAPE(C)、WAPE(C)のTOTAL集計
    * 戻り値              ：
    * 機能概要            ：GAPE(C)、WAPE(C)のTOTAL集計
    *------------------------------------------------------------------------
    */
    private static void totalcPutMap(String sql, String strKubun) {
system.debug('=================【SQL：(C)のTOTAL集計】' + sql);
        String strKey = strArea + strEigy + strKubun.substring(0,2) + TOTAL;            //キー設定:エリア＋営業部＋予算区分(頭2桁)＋TOTAL
        Decimal tot_cGAPE = 0;
        Decimal tot_cWAPE = 0;
        for (Opportunity opp : Database.query(sql)) {
            tot_cGAPE += (Decimal)opp.get('GAPE__c');                                   //GAPE合計
            tot_cWAPE += (Decimal)opp.get('WAPE__c');                                   //WAPE合計
        }
        if (map1.containsKey(strKey)) {
            map1.get(strKey).GAPE_Min__c = map1.get(strKey).GAPE_Max__c + tot_cGAPE;    //GAPE(C)
            map1.get(strKey).WAPE_Min__c = map1.get(strKey).WAPE_Max__c + tot_cWAPE;    //WAPE(C)
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：集計データ存在確認
    * 戻り値              ：Boolean(True：データ存在ない、False：データ存在)
    * 機能概要            ：集計データ存在確認
    *------------------------------------------------------------------------
    */
    private static Boolean check_noData(String sql) {
        List<Opportunity> cntOpp = Database.query(sql);
        if (cntOpp.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：所有者の「エリア」「営業部」を取得
    * 戻り値              ：
    * 機能概要            ：所有者の「エリア」「営業部」を取得
    *------------------------------------------------------------------------
    */
    private static void getAreaEigyoubu() {
        for (User usr : [SELECT UnitName__c, Unit__c FROM User WHERE Id =: strUser]) {
            strArea = Util.cnvNullStr(usr.UnitName__c);
            strEigy = Util.cnvNullStr(usr.Unit__c);
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：ログインユーザが本社担当者か確認
    * 戻り値              ：Boolean(True：本社担当者、False：本社担当者以外)
    * 機能概要            ：ログインユーザが本社担当者か確認
    *------------------------------------------------------------------------
    */
    private static Boolean check_HonshaTanto(){
        for (User usr : [SELECT Unit__c FROM User WHERE Id =: UserInfo.getUserId()]) {
            return (Util.cnvNullStr(usr.Unit__c).equals(HONSHAEIGYOBU) ? true : false);
        }
        return false;
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：集計用SQL返却
    * 戻り値              ：Map<集計対象,クエリ>
    * 機能概要            ：集計用SQL返却
    *------------------------------------------------------------------------
    */
    private static void setSQL() {
        String s_all = '';
        String s_head = '';
        String s_wh_head = '';
        String s_wh_AreaEigyo = '';
        String s_wh_Date = '';
        String s_wh_Date2 = '';
        String s_wh_Stage = '';
        String s_wh_RecordType = '';
        String s_wh_c = '';
        String s_wh_h1 = '';
        String s_wh_h2 = '';
        String s_wh_h3 = '';
        String s_wh_h4 = '';
        String s_wh_y = '';
        String s_wh_comm = '';
        String s_group = '';
        map_sql = new Map<String,String>();

        //【条件設定】
        s_wh_head = 'WHERE IsDeleted = false ';

        //①エリア、営業部
        s_wh_AreaEigyo = 'AND Area__c = \'' + strArea + '\' AND SalesUnit__c = \'' + strEigy + '\' ';

        //②月初、月末
        s_wh_Date = 'AND CloseDate >= ' + tmpDtKey1 + ' AND CloseDate <= ' + tmpDtKey2 + ' ';
        s_wh_Date2 = 'AND Month__c >= ' + tmpDtKey1 + ' AND Month__c <= ' + tmpDtKey2 + ' ';

        //③フェーズ
        if (blnLastMFlg) {
            s_wh_Stage = 'AND StageName = \'S\' ';
        } else {
            s_wh_Stage = 'AND StageName IN (\'S\',\'A\',\'B\',\'C\') ';
        }

        //④レコードタイプ
        s_wh_RecordType = 'AND RecordType.Name = \'実績\' ';

        //【共通条件】
        s_wh_comm = s_wh_head + s_wh_AreaEigyo + s_wh_Date + s_wh_Stage + s_wh_RecordType;

        //【(C)条件】
        s_wh_c = s_wh_head + 'AND AssistanceUnit__c <> \'\' AND AssistanceUnit__c = \'' + strEigy + '\' ' + s_wh_RecordType + s_wh_Stage + s_wh_Date;

        //【本社条件】
        s_wh_h1 = s_wh_head + 'AND AssistanceUnit__c <> \'\' ' + s_wh_RecordType + s_wh_Stage + s_wh_Date;      //集計１用
// *** 2011.02.05 DEL BY J.Sugimori ***
//      s_wh_h2 = s_wh_head + 'AND SupportFlg__c = TRUE ' + s_wh_RecordType + s_wh_Stage + s_wh_Date;           //集計２用
// *** 2011.02.05 ADD BY J.Sugimori ***
        s_wh_h2 = s_wh_head + 'AND SupportFlg__c = FALSE ' + s_wh_RecordType + s_wh_Stage + s_wh_Date;          //集計２用
        s_wh_h2 = s_wh_h2 + 'AND SalesUnit__c = \'' + HONSHAEIGYOBU + '\' ';                                    //集計２用
// ************************************
        s_wh_h3 = s_wh_head + 'AND Area__c = \'' + REPLACE_AREA + '\' AND SalesUnit__c = \'' + REPLACE_EIGY + '\' ' + s_wh_Date2;   //集計１：予算用

        //【予算条件】
        s_wh_y = s_wh_head + s_wh_AreaEigyo + s_wh_Date2;


        //【集計データ存在確認】
        s_head = 'SELECT Id FROM Opportunity ';
        s_all = s_head + s_wh_comm;
        map_sql.put(SQL_COUNT,s_all);
        s_all = s_head + s_wh_h1;
        map_sql.put(SQL_H1COUNT,s_all);
        s_all = s_head + s_wh_h2;
        map_sql.put(SQL_H2COUNT,s_all);


        //【保険種類】
        //①区分1
        s_head = 'SELECT Category1TXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY Category1TXT__c';
        s_all = s_head + s_wh_comm + 'AND Category1TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_KUBUN1,s_all);
        //区分1(C)の集計
        s_all = s_head + s_wh_c + 'AND Category1TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CKUBUN1,s_all);
        //区分1(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND Category1TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1KUBUN1,s_all);
        //区分1(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND Category1TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2KUBUN1,s_all);

        //②区分2
        s_head = 'SELECT Category2TXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY Category2TXT__c';
        s_all = s_head + s_wh_comm + 'AND Category2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_KUBUN2,s_all);
        //区分2(C)の集計
        s_all = s_head + s_wh_c + 'AND Category2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CKUBUN2,s_all);
        //区分2(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND Category2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1KUBUN2,s_all);
        //区分2(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND Category2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2KUBUN2,s_all);


        //【TOTAL】
        s_all = 'SELECT GAPE__c, WAPE__c, Count__c FROM Opportunity ' + s_wh_comm;
        map_sql.put(SQL_TOTAL,s_all);
        //TOTAL(C)の集計
        s_all = 'SELECT GAPE__c, WAPE__c, Count__c FROM Opportunity ' + s_wh_c;
        map_sql.put(SQL_CTOTAL,s_all);
        //TOTAL(本社1)の集計
        s_all = 'SELECT GAPE__c, WAPE__c, Count__c FROM Opportunity ' + s_wh_h1;
        map_sql.put(SQL_H1TOTAL,s_all);
        //TOTAL(本社2)の集計
        s_all = 'SELECT GAPE__c, WAPE__c, Count__c FROM Opportunity ' + s_wh_h2;
        map_sql.put(SQL_H2TOTAL,s_all);


        //【商品種別】
        s_head = 'SELECT ProductType2TXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY ProductType2TXT__c';
        s_all = s_head + s_wh_comm + 'AND ProductType2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_SHOUHIN,s_all);
        //商品種別(C)の集計
        s_all = s_head + s_wh_c + 'AND ProductType2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CSHOUHIN,s_all);
        //商品種別(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND ProductType2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1SHOUHIN,s_all);
        //商品種別(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND ProductType2TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2SHOUHIN,s_all);


        //【チャネル】
        s_head = 'SELECT Channel__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY Channel__c';
        s_all = s_head + s_wh_comm + 'AND Channel__c <> \'\' ' + s_group;
        map_sql.put(SQL_CHANNEL,s_all);
        //チャネル(C)の集計
        s_all = s_head + s_wh_c + 'AND Channel__c <> \'\' ' + s_group;
        map_sql.put(SQL_CCHANNEL,s_all);
        //チャネル(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND Channel__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1CHANNEL,s_all);
        //チャネル(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND Channel__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2CHANNEL,s_all);

        //【チャネル 小分類】
        s_head = 'SELECT ChannelSmallTXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY ChannelSmallTXT__c';
        s_all = s_head + s_wh_comm + 'AND ChannelSmallTXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_SMALLCHANNEL,s_all);
        //チャネル(C)の集計
        s_all = s_head + s_wh_c + 'AND ChannelSmallTXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CSMALLCHANNEL,s_all);
        //チャネル(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND ChannelSmallTXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1SMALLCHANNEL,s_all);
        //チャネル(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND ChannelSmallTXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2SMALLCHANNEL,s_all);

        //【予算3】
        s_head = 'SELECT Budget3TXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY Budget3TXT__c';
        s_all = s_head + s_wh_comm + 'AND Budget3TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_ETC,s_all);
        //予算3(C)の集計
        s_all = s_head + s_wh_c + 'AND Budget3TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CETC,s_all);
        //予算3(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND Budget3TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1ETC,s_all);
        //予算3(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND Budget3TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2ETC,s_all);


        //【予算4】
        s_head = 'SELECT Budget4TXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY Budget4TXT__c';
        s_all = s_head + s_wh_comm + 'AND Budget4TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_ETC4,s_all);
        //予算4(C)の集計
        s_all = s_head + s_wh_c + 'AND Budget4TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CETC4,s_all);
        //予算4(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND Budget4TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1ETC4,s_all);
        //予算4(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND Budget4TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2ETC4,s_all);


        //【予算5】
        s_head = 'SELECT Budget5TXT__c a, SUM(GAPE__c) b, SUM(WAPE__c) c, SUM(Count__c) d FROM Opportunity ';
        s_group = 'GROUP BY Budget5TXT__c';
        s_all = s_head + s_wh_comm + 'AND Budget5TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_ETC5,s_all);
        //予算5(C)の集計
        s_all = s_head + s_wh_c + 'AND Budget5TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_CETC5,s_all);
        //予算5(本社1)の集計
        s_all = s_head + s_wh_h1 + 'AND Budget5TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H1ETC5,s_all);
        //予算5(本社2)の集計
        s_all = s_head + s_wh_h2 + 'AND Budget5TXT__c <> \'\' ' + s_group;
        map_sql.put(SQL_H2ETC5,s_all);


        //【予算】
        s_head = 'SELECT Type__c a, GroupName__c b, SUM(GAPEBudget__c) c, SUM(WAPEBudget__c) d FROM MonthlyTotalBudget__c ';
        s_group = 'GROUP BY Type__c, GroupName__c';
        s_all = s_head + s_wh_y + 'AND Type__c <> \'\' AND GroupName__c <> \'\' '  + s_group;
        map_sql.put(SQL_YOSAN,s_all);
        //予算(本社1)の集計
        s_all = s_head + + s_wh_h3 + 'AND Type__c <> \'\' AND GroupName__c <> \'\' '  + s_group;
        map_sql.put(SQL_H1YOSAN,s_all);
        //予算(本社2)の集計
        s_all = s_head + + s_wh_h3 + 'AND Type__c <> \'\' AND GroupName__c <> \'\' '  + s_group;
        map_sql.put(SQL_H2YOSAN,s_all);
    }
}