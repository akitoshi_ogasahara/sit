public with sharing class MNT_PamhletSearchCSVController extends E_CSVExportController{

    public override String mainSObjectAPIName(){
        return Schema.SObjectType.I_ContentMaster__c.Name;
    }

    public MNT_PamhletSearchCSVController(){
        super();
        
        // 項目の追加
        addColumn(new E_CSVColumnDescribe.BaseField('Name'));   // CMSコンテンツタイトル  Name(String)
        addColumn(new E_CSVColumnDescribe.ParentStringField('ParentContent__r.Name', 'I_ContentMaster__c', '資料名'));  // 親コンポーネント   ParentContent__c(String)
        addColumn(new E_CSVColumnDescribe.BaseField('FormNo__c','フォームNo')); // フォームNo   FormNo__c(String)
        addColumn(new E_CSVColumnDescribe.BaseField('ApprovalNo__c'));  // 承認番号 ApprovalNo__c(String)
        addColumn(new E_CSVColumnDescribe.DateField('DisplayFrom__c','使用開始日')); // 使用開始日 DisplayFrom__c(Date)
        addColumn(new E_CSVColumnDescribe.DateField('ValidTo__c')); // 有効期限 ValidTo__c(Date)
        addColumn(new E_CSVColumnDescribe.BaseField('Jurisdiction__c'));  // 所管部署 Jurisdiction__c(Picklists)
        addColumn(new E_CSVColumnDescribe.BaseField('CreatedDepartment__c'));    // 作成部署 CreatedDepartment__c(Picklists)
        addColumn(new E_CSVColumnDescribe.BaseField('Reason__c')); // 作成理由 Reason__c(LongTextarea)
    }

}