/**
 * 保険料逓減推移画面拡張クラス テストクラス
 * CreatedDate 2015/8/4
 * @Author Terrasky
 */
@isTest
private class TestE_TotalDPTExtender {

	static testMethod void initialdisp_1() {
		String CHDRNUM = 'xxxxxxxx';
		integer y = 100000000;
		User u = TestE_TestUtil.createUser(true, 'kanrisha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(false);
		log.InquiryDate__c = date.newInstance(2020,2,1);
		insert log;
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		E_Policy__c policy = TestE_TestUtil.createPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, CHDRNUM);
		E_DPTPF__c[] dpts = new List<E_DPTPF__c>();
		for(integer i = 1; i <= 50; i++){
			dpts.add(
				new E_DPTPF__c(
					E_Policy__r = new E_Policy__c(COMM_CHDRNUM__c = CHDRNUM),
					CHDRNUM__c = CHDRNUM,
					ZPOLYEAR__c = String.valueOf(i),
					DCPDATE__c = (2010 + i) + '0201',
					INSTPREM__c = y,
					SINSTAMT__c = y - (i - 1) * 1000000,
					UpsertKey__c = CHDRNUM + '|' + String.valueOf(i + 1)
				)
			);
		}
		upsert dpts UpsertKey__c;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_TotalDPT;
			pref.getParameters().put('id', policy.Id);
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_TotalDPTController controller = new E_TotalDPTController(standardcontroller);
			E_TotalDPTExtender extender = controller.getExtender();
			extender.init();

			resultPage = extender.pageAction();

			Test.stopTest();
			
			system.assertEquals(41, controller.dataTableDPT.items.size());
			system.assertEquals(null, resultPage);
		}
	}
/*
	static testMethod void initialdisp_2() {
		String CHDRNUM = 'xxxxxxxx';
		integer y = 100000000;
		User u = TestE_TestUtil.createUser(true, 'kanrisha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(false);
		log.InquiryDate__c = date.newInstance(2020,2,1);
		insert log;
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		E_Policy__c policy = TestE_TestUtil.createPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, CHDRNUM);

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_TotalDPT;
			pref.getParameters().put('id', policy.Id);
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_TotalDPTController controller = new E_TotalDPTController(standardcontroller);
			E_TotalDPTExtender extender = controller.getExtender();
			extender.init();

			resultPage = extender.pageAction();

			Test.stopTest();
			
			system.assertEquals('/apex/e_errorpage?code=' + EncodingUtil.urlEncode(E_Const.ERROR_MSG_NODATA, 'UTF-8'), resultPage.getUrl());
		}
	}
*/
}