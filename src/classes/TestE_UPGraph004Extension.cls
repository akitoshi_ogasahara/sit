/**
 * 
 */
@isTest
private class TestE_UPGraph004Extension {
    
    // testData
    private static E_UPBPF__c upbpf;

    /**
     * init004 
     * dlCondi != null
     */
    static testMethod void init01() {
        // create Data
        createData();
        
        // Set test page
        Pagereference pref = Page.up003;
        pref.getParameters().put('id',upbpf.Id);
        Test.setCurrentPage(pref);
        
        Test.starttest();
        
        // StandardController
        E_UPTerm003Controller controller = new E_UPTerm003Controller();
        controller.init();
        controller.doTransition004();
        
        // Extension
        E_UPGraph004Extension extension = new E_UPGraph004Extension(controller);
        Pagereference resultPref = extension.init004();
        
        Test.stoptest();
        
        system.assertEquals(upbpf.Id, extension.getUpbpf().Id);
        
        E_DownloadHistorry__c dh = [Select outputType__c, Conditions__c From E_DownloadHistorry__c Where Id = :extension.downloadHistoryId];
        system.assert(extension.downloadHistoryId != null);
        system.assertEquals(E_Const.DH_OUTPUTTYPE_GRAPH, dh.outputType__c);
        system.assertEquals(controller.dlCondi.toJSON(), dh.Conditions__c);
    }
   
    /**
     * init004 
     * dlCondi == null
     */
    static testMethod void init02() {
        // create Data
        createData();
        
        // Set test page
        Pagereference pref = Page.up003;
        pref.getParameters().put('id',upbpf.Id);
        Test.setCurrentPage(pref);
        
        Test.starttest();
        
        // StandardController
        E_UPTerm003Controller controller = new E_UPTerm003Controller();
        controller.init();
        //controller.doTransition004();
        
        // Extension
        E_UPGraph004Extension extension = new E_UPGraph004Extension(controller);
        Pagereference resultPref = extension.init004();
        
        Test.stoptest();
        
        system.assert(extension.downloadHistoryId == null);

    }
    
    // testDataCreate
    static void createData(){
        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        
        // ファンド群マスタ(E_FundMaster__c)
        E_FundMaster__c fundMaster = new E_FundMaster__c(
            CURRFROM__c = 0,
            CURRTO__c = 99999999,
            ZGFUNDCD__c = 'G04',
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZGRPFND01__c = 'アイエヌジーマイプラン年金',
            ZGRPFND02__c = '',
            ZGRPFND03__c = ''
        );
        insert fundMaster;
        
        // 特別勘定(E_SVCPF__c)
        E_SVCPF__c svcpf  = new E_SVCPF__c(
            ZFSDFROM__c = '20150101',
            ZFSHFROM__c = '10',
            ZFSMFROM__c = '10',
            ZFSDTO__c = '99999999',
            ZFSHTO__c = '99',
            ZFSMTO__c = '99',
            ZEFUNDCD__c = 'A052',
            ZCATFLAG__c = '20',
            ZFUNDNAM01__c = '世界債券型（TS1）',
            ZFUNDNAM02__c = 'ＶＡ外国債券ファンド',
            ZFUNDNAM03__c = '（適格機関投資家専用）',
            ZFUNDNAM04__c = '',
            CURRFROM__c = '20150101',
            CURRTO__c = '99999999'
        );
        insert svcpf;
        
        // 選択可能ファンド(E_SFGPF__c)
        E_SFGPF__c sfgpf = new E_SFGPF__c(
            FundMaster__c = fundMaster.Id,
            E_SVCPF__c = svcpf.Id,
            E_SFGPFCode__c = 'G04|A052'
        );
        insert sfgpf;
        
        // ユニットプライス基本情報(E_UPBPF__c)
        upbpf = new E_UPBPF__c(
            E_FundMaster__c = fundMaster.Id,
            ZPETNAME__c = 'アイエヌジーマイプラン年金',
            ZGFUNDCD__c = 'G04',
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZIDXDATE01__c = '20150116',
            ZIDXDATE02__c = '20150115',
            ZIDXDATE03__c = '20150114',
            ZIDXDATE04__c = '20150113',
            ZIDXDATE05__c = '20150112',
            ZPRDCD__c = 'S0013',
            ZQLFLG__c = true,
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '080000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        );
        insert upbpf;
        
        // ユニットプライス表示用ファンド(E_UPFPF__c)
        E_UPFPF__c upfpf = new E_UPFPF__c(
            SVCPF__c = svcpf.Id,
            ZFUNDFLG__c = false,
            ZSFUNDCD__c = 'A052',
            ZFNDNAME01__c = '世界債券型',
            ZFNDNAME02__c = '（TS1）',
            ZIVTNAME01__c = 'ＶＡ外国債券ファンド',
            ZIVTNAME02__c = '（適格機関投資家専用）',
            ZIVTNAME03__c = '',
            ZIVTNAME04__c = '',
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '000000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        );
        insert upfpf;
        
        // ユニットプライス(E_UPXPF__c)
        List<E_UPXPF__c> upxpfList = new List<E_UPXPF__c>();
        for(Integer i = 0; i < 9; i++){
            E_UPXPF__c upxpf = new E_UPXPF__c(
                E_UPFPF__c = upfpf.Id,
                ZSFUNDCD__c = 'A052',
                ZVINDEX__c = 100.000000000,
                ZFUNDDTE__c = '2015020' + String.valueOf(i + 1)
            );
            upxpfList.add(upxpf);
        }
        insert upxpfList;
    }
}