// [IRIS]保有契約共通クラス
public abstract class I_AbstractPolicyController extends I_AbstractController{

// 画面表示用
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// リスト行数
	public Integer rowCount {get; set;}

// ソート用
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}
	// ソートキー：項目変換Map
	protected Map<String, String> sortMap;

	// 表示区分
	public String searchType{get; set;}
	// 拠点長が表示しているかどうか
	public Boolean hasViewedByManager {get {
		return E_AccessController.getInstance().isAuthManager();
	}}

// 表示制御
	//募集人表示フラグ
	public Boolean contactFrag{get; set;}
	//代理店名表示フラグ
	public Boolean agencyFlag{get; set;}
	//事務所名表示フラグ
	public Boolean officeFlag{get; set;}
	//担当ＭＲ名表示フラグ
	public Boolean manegeMRFlag{get; set;}
	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}

	// コンストラクタ
	public I_AbstractPolicyController(){
	}

	/**
	 * フィルタ条件を元にWHERE句を生成する
	 */
	public String createPolicyCondition(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result = ' AND ( MainAgentAccountParentId__c = \'' + conditionValue + '\' '
						   + ' OR SubAgentAccountParent__c = \''  + conditionValue + '\' ) ';
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result = ' AND ( MainAgentAccount__c = \'' + conditionValue + '\' '
						   + ' OR SubAgentAccount__c = \''  + conditionValue + '\' ) ';
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					//修正　山田
					String agentId = E_Util.getMainAccountByAgentId(conditionValue);
						result = ' AND ( MainAgent__c = \'' + agentId + '\' '
							   + ' OR SubAgent__c = \''  + agentId + '\' ) ';
				// 営業部フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_UNIT){
					result = ' AND ( MainAgentAccount__r.E_CL2PF_BRANCH__c = \'' + conditionValue + '\''
						   + 'OR MainAgentAccount__r.E_CL2PF_ZBUSBR__c = \'' + conditionValue + '\''
						   + 'OR SubAgentAccount__r.E_CL2PF_BRANCH__c = \'' + conditionValue + '\''
						   + 'OR SubAgentAccount__r.E_CL2PF_ZBUSBR__c = \'' + conditionValue + '\' ) ';
				// MRフィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_MR){
					result = ' AND ( MainAgentAccount__r.OwnerId = \'' + conditionValue + '\' '
						   + ' OR SubAgentAccount__r.OwnerId = \'' + conditionValue + '\' )';
				}
			}
		// フィルタ無しの場合
		}else{
			// MR または 拠点長 または 代理店ユーザの場合 ⇒代理店ユーザまたは 社員(BR**)以外の時
			if(access.isEmployee() && access.ZINQUIRR_is_BRAll()==false){
				result = ' AND PolicyInCharge__c = True ';
			}
		}

		return result;
	}

	/**
	 * フィルタ条件を元に個人保険特約から保険契約ヘッダレコードを取得するとき用のWHERE句を生成する(失効契約用)
	 */
	public String createCovpfCondition(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result = ' AND ( E_Policy__r.MainAgentAccountParentId__c = \'' + conditionValue + '\' '
						   + ' OR E_Policy__r.SubAgentAccountParent__c = \''  + conditionValue + '\' ) ';
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result = ' AND ( E_Policy__r.MainAgentAccount__c = \'' + conditionValue + '\' '
						   + ' OR E_Policy__r.SubAgentAccount__c = \''  + conditionValue + '\' ) ';
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					//修正　山田
					String agentId = E_Util.getMainAccountByAgentId(conditionValue);
						result = ' AND ( E_Policy__r.MainAgent__c = \'' + agentId + '\' '
						   + ' OR E_Policy__r.SubAgent__c = \''  + agentId + '\' ) ';
				//営業部フィルタ
				}else if(conditionField == I_Const.CONDITION_FIELD_UNIT){
					result = ' AND ( E_Policy__r.MainAgentAccount__r.E_CL2PF_BRANCH__c = \'' + conditionValue + '\' '
						   + 'OR E_Policy__r.MainAgentAccount__r.E_CL2PF_ZBUSBR__c = \'' + conditionValue + '\' '
						   + 'OR E_Policy__r.SubAgentAccount__r.E_CL2PF_BRANCH__c = \'' + conditionValue + '\' '
						   + 'OR E_Policy__r.SubAgentAccount__r.E_CL2PF_ZBUSBR__c = \'' + conditionValue + '\' ) ';
				//MRフィルタ
				}else if(conditionField == I_Const.CONDITION_FIELD_MR){
					result = ' AND ( E_Policy__r.MainAgentAccount__r.OwnerId = \'' + conditionValue + '\' '
						   + ' OR E_Policy__r.SubAgentAccount__r.OwnerId = \'' + conditionValue + '\' )';
				}
			}
		// フィルタ無しの場合
		}else{
			// MR または 拠点長 または 代理店ユーザの場合
			if(access.isAuthMR() || access.isAuthManager()){
				result = ' AND E_Policy__r.PolicyInCharge__c = True ';
			}
		}

		return result;
	}

	/**
	 * フィルタ条件を元に証券一覧へのリンクを作成する
	 */
	public String crateCustomerInfoParameter(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result = '&disid=' + conditionValue + '&offId=&ageId=';
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result = '&disid=&offId=' + conditionValue + '&ageId=';
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					//修正　山田
					String agentId = E_Util.getMainAccountByAgentId(conditionValue);
					result = '&disid=&offId=&ageId=' + agentId;
				}
			}
		// フィルタ無しの場合
		} else {
			// 代理店ユーザの場合
			if(access.isAgent()){
				result = '&disid=&offId=&ageId=' + access.user.ContactId;
			} else {
				result += '&disid=&offId=&ageId=';
			}
		}

		return result;
	}

	/**
	 * アクティブメニュー制御用のパラメータを取得する
	 */
	public String getIRISMenuParameter(){
		return 'irismn=' + ApexPages.currentPage().getParameters().get('irismn');
	}

	/**
	 * 解約返戻金取得メソッド
	 */
	public Decimal getCancellationRefund(E_Policy__c pol){
		//解約返戻金
		Decimal cancelref = 0;
		//保険料振替貸付合計
		Decimal policyTransferLoan = 0;
		//契約者貸付合計
		Decimal contractorLoan = 0;
		//前納未経過保険料
		Decimal prePolicyLoan = 0;
		//未経過保険料
		Decimal unearnedPremiums = 0;

		if(pol.COLI_ZCSHVAL__c != null) cancelref = pol.COLI_ZCSHVAL__c;
		if(pol.COLI_ZEAPLTOT__c != null) policyTransferLoan = pol.COLI_ZEAPLTOT__c;
		if(pol.COLI_ZEPLTOT__c != null) contractorLoan = pol.COLI_ZEPLTOT__c;
		if(pol.COLI_ZEADVPRM__c != null) prePolicyLoan = pol.COLI_ZEADVPRM__c;
		if(pol.COLI_ZUNPREM__c != null) unearnedPremiums = pol.COLI_ZUNPREM__c;

		Decimal cancelrefSum = 0;
		//解約返戻金計算不能ﾌﾗｸまたはﾞ未経過保険料計算不能ﾌﾗｸﾞがtrueのとき
		if(pol.COLI_ZCVDCF__c == true || pol.COLI_ZUNPCF__c == true){
			return -1;
		//未経過保険料計算不能ﾌﾗｸﾞがfalseかつ未経過保険料表示ﾌﾗｸﾞがfalseのとき
		}else if(pol.COLI_ZUNPCF__c == false && pol.COLI_ZUNPDCF__c == false){
							//解約返戻金 - (保険料振替貸付合計 + 契約者貸付合計) + 前納未経過保険料
			cancelrefSum = cancelref - (policyTransferLoan + contractorLoan) + prePolicyLoan;
		}else{
							//解約返戻金 - (保険料振替貸付合計 + 契約者貸付合計) + 前納未経過保険料 + 未経過保険料
			cancelrefSum = cancelref - (policyTransferLoan + contractorLoan) + prePolicyLoan + unearnedPremiums;
		}
		return cancelrefSum;
	}

	public List<SelectOption> getSearchTypeOptions() {
		return new List<SelectOption>{
			new SelectOption(I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_ALL, '自身の担当分'),
			new SelectOption(I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE, '本社営業部分')
		};
	}

	//public String createCovpfConditionByManager() {
	//	String condition = '';
	//	// 本社営業分
	//	if (searchType == I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE) {
	//		condition += ' AND (E_Policy__r.MainAgent__r.Account.KSECTION__c == \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ';
	//	} 
	//	// 自拠点分
	//	else {
	//		condition += ' AND E_Policy__r.MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ';
	//	}
	//	return condition;
	//}

	//public String createPolicyConditionByManager() {
	//	String condition = '';
	//	// 本社営業分
	//	if (searchType == I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE) {
	//		condition += ' AND (MainAgent__r.Account.KSECTION__c == \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ';
	//	} 
	//	// 自拠点分
	//	else {
	//		condition += ' AND MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ';
	//	}
	//	return condition;
	//}

	public String createCovpfConditionByManager() {
		String condition = '';
		// 本社営業分
		if (searchType == I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE) {
		  condition += ' AND (E_Policy__r.MainAgent__r.Account.KSECTION__c = \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ' +
			  ' OR (E_Policy__r.MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ' +
			  ' AND E_Policy__r.MainAgent__r.Account.E_CL2PF_BRANCH__c != \'' + access.idcpf.ZINQUIRR__c.right(2) + '\')) ';
		} 
		// 自拠点分
		else {
		  condition += ' AND E_Policy__r.MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\''
			  + ' AND E_Policy__r.MainAgent__r.Account.E_CL2PF_BRANCH__c = \'' + access.idcpf.ZINQUIRR__c.right(2) + '\'';
		}
		return condition;
	}

	public String createPolicyConditionByManager() {
		String condition = '';
		// 本社営業分
		if (searchType == I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE) {
		  condition += ' AND (MainAgent__r.Account.KSECTION__c = \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ' +
			  ' OR (MainAgent__r.Account.KSECTION__c !=  \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' ' +
			  ' AND MainAgent__r.Account.E_CL2PF_BRANCH__c != \'' + access.idcpf.ZINQUIRR__c.right(2) + '\'))';
		}
		// 自拠点分
		else {
		  condition += ' AND MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\''
			  + ' AND MainAgent__r.Account.E_CL2PF_BRANCH__c = \'' + access.idcpf.ZINQUIRR__c.right(2) + '\'';
		}
		return condition;
	}
}