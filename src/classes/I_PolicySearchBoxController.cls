public with sharing class I_PolicySearchBoxController extends I_AbstractSearchBoxController{
// 出力
	// 検索結果リスト
	public List<I_PolicySearchBoxRow> policyRows {get; set;}

	//IRISエンハンス IRIS-749 IRIS-750 ここから
	//ラジオボタン取得変数
	public String selection{get; set;}
	//IRISエンハンス IRIS-749 IRIS-750 ここまで

// 制御
	// 表示制御 - 契約者リストを表示
	public Boolean getIsDisplayOwnerList() {
		return exeSearchType == I_Const.SEARCH_TYPE_OWNER;
	}

	// 表示制御 - 被保険者リストを表示
	public Boolean getIsDisplayInsuredList() {
		return exeSearchType == I_Const.SEARCH_TYPE_INSURED;
	}

	// 表示制御 - 証券番号リストを表示
	public Boolean getIsDisplayPolicyNoList() {
		return exeSearchType == I_Const.SEARCH_TYPE_POLICY_NO;
	}

	// 表示制御 - 顧客番号リストを表示
	public Boolean getIsDisplayCustomerNoList() {
		return exeSearchType == I_Const.SEARCH_TYPE_CUSTOMER_NO;
	}

	//IRISエンハンス IRIS-749 IRIS-750 ここから
	// AND OR検索制御
	// 表示制御 - 契約者リストを表示
	public Boolean getIsDisplaySearch() {
		return searchType == I_Const.SEARCH_TYPE_OWNER || searchType == I_Const.SEARCH_TYPE_INSURED;
	}
	//IRISエンハンス IRIS-749 IRIS-750 ここまで


// ソート用
	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

// ローカル定数
	private static final String SORT_TYPE_CUSTOMER_NO = 'customerNo';
	private static final String SORT_TYPE_OWNER_NAME = 'ownerName';
	private static final String SORT_TYPE_ADDRESS = 'address';
	private static final String SORT_TYPE_INSURED_NAME = 'insuredName';
	private static final String SORT_TYPE_BIRTH_DAY = 'birthDay';
	private static final String SORT_TYPE_AGE = 'age';
	private static final String SORT_TYPE_POLICY_NO = 'policyNo';
	private static final String SORT_TYPE_SEX = 'sex';
	private static final String SORT_TYPE_CUSTOMER_TYPE = 'customerType';



	public String getSearchCategoryLabel(){
		return I_Const.SEARCH_LABEL_CATEGORY_POLICY;
	}

	//IRISエンハンス IRIS-749 IRIS-750 ここから
	//ラジオボタン作成
	public List<SelectOption> getSearchOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('and', 'and検索'));
		options.add(new SelectOption('or', 'or検索'));
		return options;
	}

	//IRISエンハンス IRIS-749 IRIS-750 ここまで


	// コンストラクタ
	public I_PolicySearchBoxController(){
		super();
		policyRows = new List<I_PolicySearchBoxRow>();

		// 初期値をセット
		searchCategory = I_Const.SEARCH_LABEL_CATEGORY_POLICY;
		rowCount = I_Const.LIST_DEFAULT_ROWS;

		searchTypeItems = new List<SelectOption>();
		searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_OWNER, I_Const.SEARCH_TYPE_LABEL_OWNER));
		searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_INSURED, I_Const.SEARCH_TYPE_LABEL_INSURED));
		searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_POLICY_NO, I_Const.SEARCH_TYPE_LABELPOLICY_NO));
		searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_CUSTOMER_NO, I_Const.SEARCH_TYPE_LABEL_CUSTOMER_NO));

		//IRISエンハンス IRIS-749 IRIS-750 ここから
		//ラジオボタン初期値
		selection = 'and';
		//IRISエンハンス IRIS-749 IRIS-750 ここまで

		flush();
	}

	public void flush() {
		keyword = '';
		searchType = I_Const.SEARCH_TYPE_OWNER;
		searchTypeName = I_Const.SEARCH_TYPE_LABEL_OWNER;
		isSearch = false;
		policyRows.clear();
		searchMessages.clear();
		//JIRA_#63 170331
//		getSeachBoxLogRecord(searchType);
		getSeachBoxLogRecord(searchTypeName);
	}
	

	// 検索処理
	public void doSearch(){
		searchMessages.clear();

		// 入力チェック
		if(!isInputValidate()){
			return;
		}

		// 変数初期化
		policyRows = new List<I_PolicySearchBoxRow>();
		exeSearchType = searchType;
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		isSearch = false;
		// 検索ログを再取得
		//JIRA_#63 170331
//		getSeachBoxLogRecord(exeSearchType);
		getSeachBoxLogRecord(searchTypeName);



		// 証券番号検索
		if(exeSearchType == I_Const.SEARCH_TYPE_POLICY_NO){
			doSearchPolicy();

		// 顧客番号検索
		} else if(exeSearchType == I_Const.SEARCH_TYPE_CUSTOMER_NO){
			doSearchCustomerNo();

		// 上記以外（社員ユーザ）
		} else if(access.isEmployee()){
			doSearchContact();

		// 上記以外（代理店ユーザ）
		} else {
			doSearchPolicy();
		}

		return;
	}

	/**
	 * Contact検索
	 * 社員ユーザが契約者、被保険者で検索するときに使用
	 */
	private void doSearchContact(){
		List<Contact> cons = new List<Contact>();
		//IRISエンハンス IRIS-749 IRIS-750 ここから
		cons = E_ContactDao.getRecsIRISPolicySearchBox(keyword, exeSearchType, selection);
		//IRISエンハンス IRIS-749 IRIS-750 ここまで

		// 検索結果が上限の場合は処理終了
		if(cons != null && cons.size() == I_Const.SEARCH_CONTACT_SOQL_LIMIT){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return;
		}

		// 契約者名で検索
		if(exeSearchType == I_Const.SEARCH_TYPE_OWNER){
			for(Contact con : cons){
				if(con.E_PolicyContractors__r != null && con.E_PolicyContractors__r.size() > 0){
					I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

					// 顧客番号
					row.customerNo = con.E_CLTPF_CLNTNUM__c;
					// 契約者名
					row.ownerName = con.Name;
					// 契約者名カナ
					row.ownerNameKana = con.E_CLTPF_ZCLKNAME__c;
					// 住所
					row.address = con.E_COMM_ZCLADDR__c;
					// URL
					row.url = 'E_CustomerInfo?'
							+ 'cid='    + con.Id
							+ '&cNo='   + con.E_CLTPF_CLNTNUM__c
							+ '&cName=' + urlEncodeForUTF8(con.Name)
							+ '&disid=&offId=&ageId='
							+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;

					policyRows.add(row);
					if(policyRows.size() == I_Const.LIST_MAX_ROWS){
						searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
						return;
					}
				}
			}

			// ソート順序設定
			sortType = SORT_TYPE_OWNER_NAME;
			sortIsAsc = true;

		// 被保険者名で検索
		} else if(exeSearchType == I_Const.SEARCH_TYPE_INSURED){
			for(Contact con : cons){
				if(con.E_PolicyInsureds__r != null && con.E_PolicyInsureds__r.size() > 0){
					I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

					// 被保険者名
					row.insuredName = con.Name;
					// 被保険者名カナ
					row.insuredNameKana = con.E_CLTPF_ZCLKNAME__c;
					// 住所
					row.address = con.E_COMM_ZCLADDR__c;
					// 生年月日
					row.birthDay = E_Util.stringToDate(con.E_CLTPF_DOB__c);
					// 年齢
					row.age = con.Age__c;
					// 性別
					row.sex = con.E_CLTPF_ZKNJSEX__c;
					// URL
					row.url = 'E_InsuredInfo?'
							+ 'cid='    + con.Id
							+ '&cNo='   + con.E_CLTPF_CLNTNUM__c
							+ '&cName=' + urlEncodeForUTF8(con.Name)
							+ '&disid=&offId=&ageId='
							+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;
	
					policyRows.add(row);
					if(policyRows.size() == I_Const.LIST_MAX_ROWS){
						searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
						return;
					}
				}
			}

			// ソート順序設定
			sortType = SORT_TYPE_INSURED_NAME;
			sortIsAsc = true;
		}

		// 集計結果がゼロ件の場合はメッセージを表示
		if(policyRows == null || policyRows.isEmpty()){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return;
		}

		// ソート実行
		list_sortType = sortType;
		list_sortIsAsc = sortIsAsc;
		policyRows.sort();

		// 戻るボタンの遷移先をセット（IRIS_Top）
		setRefererContactSearch();

		isSearch = true;

		return;
	}

	/**
	 * 保険契約ヘッダ検索
	 *  ・代理店ユーザが契約者、被保険者で検索する場合に使用
	 *  ・証券番号で検索する場合に使用（社員、代理店共通）
	 */
	private void doSearchPolicy(){
		// SOQL実行
		List<E_Policy__c> policys = new List<E_Policy__c>();
		//IRISエンハンス IRIS-749 IRIS-750 ここから
		policys = E_PolicyDao.getRecsIRISPolicySearchBox(keyword, exeSearchType,selection);
		//IRISエンハンス IRIS-749 IRIS-750 ここまで

		//JIRA_#64 0406
		/*policys.addAll(E_PolicyDao.getRecsIRISVanishPolicySearchBox(keyword, exeSearchType, createPolicyCondition()));

		List<AggregateResult> covList = new List<AggregateResult>();
		covList = E_COVPFDao.getRecsIRISCOVPFSearchBox(keyword, exeSearchType, createCovpfCondition());

		Set<Id> polIds = new Set<Id>();
		for(AggregateResult covResult : covList)
		{
			String polId = String.valueOf(covResult.get('E_Policy__c'));
			if(String.isNotBlank(polId)){
				polIds.add(Id.valueOf(polId));
			}
		}

		policys.addAll(E_PolicyDao.getRecsIRISLapsedPolicySearchBox(polIds));
		*/
		//JIRA_#64 0406 ここまで

		// 検索結果がゼロ件の場合は処理終了
		if(policys == null || policys.isEmpty()){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return;
		}

		// 検索結果が上限の場合は処理終了
		if(policys.size() == I_Const.LIST_MAX_ROWS_AGGREGATE){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return;
		}

		// 検索結果の集計
		// 契約者、顧客番号
		if(exeSearchType == I_Const.SEARCH_TYPE_OWNER){
			policyRows = aggregateOwner(policys);
			// デフォルトソート
			sortType = SORT_TYPE_OWNER_NAME;
			sortIsAsc = true;

			list_sortType = sortType;
			list_sortIsAsc = sortIsAsc;
			policyRows.sort();

			// 戻るボタンの遷移先をセット（IRIS_Top）
			setRefererContactSearch();

		// 被保険者
		} else if(exeSearchType == I_Const.SEARCH_TYPE_INSURED){
			policyRows = aggregateInsured(policys);
			// デフォルトソート
			sortType = SORT_TYPE_INSURED_NAME;
			sortIsAsc = true;

			list_sortType = sortType;
			list_sortIsAsc = sortIsAsc;
			policyRows.sort();

			// 戻るボタンの遷移先をセット（IRIS_Top）
			setRefererContactSearch();

		// 証券番号
		} else if(exeSearchType == I_Const.SEARCH_TYPE_POLICY_NO){
			policyRows = setRowTypePolicyNo(policys);
			// デフォルトソート
			sortType = SORT_TYPE_POLICY_NO;
			sortIsAsc = true;

			list_sortType = sortType;
			list_sortIsAsc = sortIsAsc;
			policyRows.sort();

			setRefererPolicySearch();
		}

		// 集計結果が上限の場合は処理終了
		if(policyRows.size() == I_Const.LIST_MAX_ROWS){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return;
		}

		isSearch = true;

		return;		
	}

	/**
	 * 顧客番号検索
	 */
	private void doSearchCustomerNo(){
		// Contact検索
		Contact con = E_ContactDaoWithout.getRecCustomerNo(keyword);

		if(con == null || E_PolicyDao.getOwnerOrInsuredCount(con.Id) == 0){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return;
		}

		// 契約者の行を作成
		if(con.E_CLTPF_FLAG01__c == I_Const.CONTACT_OWNER_FLG_TRUE){
			I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

			// 顧客番号
			row.customerNo = con.E_CLTPF_CLNTNUM__c;
			// 顧客名（契約者名）
			row.ownerName = con.Name;
			// 顧客名カナ（契約者名カナ）
			row.ownerNameKana = con.E_CLTPF_ZCLKNAME__c;
			// 住所
			row.address = con.E_COMM_ZCLADDR__c;
			// 顧客種別
			row.customerType = I_Const.CUSTOMER_TYPE_OWNER;
			// URL
			row.url = 'E_CustomerInfo?'
					+ 'cid='    + con.Id
					+ '&cNo='   + con.E_CLTPF_CLNTNUM__c
					+ '&cName=' + urlEncodeForUTF8(con.Name)
					+ '&disid=&offId=&ageId=';
			if(access.isAgent()){
				row.url += access.user.ContactId;
			}
			row.url += '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;

			policyRows.add(row);
		}

		// 被保険者の行を作成
		if(con.E_CLTPF_FLAG02__c == I_Const.CONTACT_INSURED_FLG_TRUE){
			I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

			// 顧客番号
			row.customerNo = con.E_CLTPF_CLNTNUM__c;
			// 顧客名（被保険者名）
			row.ownerName = con.Name;
			// 顧客名カナ（被保険者名カナ）
			row.ownerNameKana = con.E_CLTPF_ZCLKNAME__c;
			// 住所
			row.address = con.E_COMM_ZCLADDR__c;
			// 顧客種別
			row.customerType = I_Const.CUSTOMER_TYPE_INSURED;
			// URL
			row.url = 'E_InsuredInfo?'
					+ 'cid='    + con.Id
					+ '&cNo='   + con.E_CLTPF_CLNTNUM__c
					+ '&cName=' + urlEncodeForUTF8(con.Name)
					+ '&disid=&offId=&ageId=';
			if(access.isAgent()){
				row.url += access.user.ContactId;
			}
			row.url += '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;

			policyRows.add(row);
		}

		// 戻るボタンの遷移先をセット（IRIS_Top）
		setRefererContactSearch();

		sortType = SORT_TYPE_CUSTOMER_TYPE;
		sortIsAsc = true;

		isSearch = true;

		return;
	}

	/**
	 * 契約者検索の結果リストを作成
	 */
	private List<I_PolicySearchBoxRow> aggregateOwner(List<E_Policy__c> policys){
		Map<String, I_PolicySearchBoxRow> rowMap = new Map<String, I_PolicySearchBoxRow>();
		for(E_policy__c policy : policys){
			if(!rowMap.containskey(policy.ContractorCLTPF_CLNTNUM__c)){
				I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

				// 顧客番号
				row.customerNo = policy.ContractorCLTPF_CLNTNUM__c;
				// 契約者名
				row.ownerName = policy.ContractorName__c;
				// 契約者名カナ
				row.ownerNameKana = policy.ContractorE_CLTPF_ZCLKNAME__c;
				// 住所
				row.address = policy.ContractorAddress__c;
				// URL
				row.url = 'E_CustomerInfo?'
						+ 'cid='    + policy.Contractor__c
						+ '&cNo='   + policy.ContractorCLTPF_CLNTNUM__c
						+ '&cName=' + urlEncodeForUTF8(policy.ContractorName__c)
						+ '&disid=&offId=&ageId=' + access.user.ContactId
						+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;

				rowMap.put(policy.ContractorCLTPF_CLNTNUM__c, row);
				if(rowMap.size() == I_Const.LIST_MAX_ROWS){
					break;
				}
			}
		}

		return rowMap.values();
	}

	/**
	 * 被保険者検索の検索結果リストを作成
	 */
	private List<I_PolicySearchBoxRow> aggregateInsured(List<E_Policy__c> policys){
		Map<String, I_PolicySearchBoxRow> rowMap = new Map<String, I_PolicySearchBoxRow>();
		for(E_policy__c policy : policys){
			if(!rowMap.containskey(policy.InsuredCLTPF_CLNTNUM__c)){
				I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

				// 被保険者名
				row.insuredName = policy.InsuredName__c;
				// 被保険者名カナ
				row.insuredNameKana = policy.InsuredE_CLTPF_ZCLKNAME__c;
				// 住所
				row.address = policy.InsuredE_COMM_ZCLADDR__c;
				// 生年月日
				row.birthDay = E_Util.stringToDate(policy.InsuredCLTPF_DOB__c);
				// 年齢
				row.age = policy.InsuredAge__c;
				// 性別
				row.sex = policy.InsuredCLTPF_ZKNJSEX__c;
				// URL
				row.url = 'E_InsuredInfo?'
						+ 'cid='    + policy.Insured__c
						+ '&cNo='   + policy.InsuredCLTPF_CLNTNUM__c
						+ '&cName=' + urlEncodeForUTF8(policy.InsuredName__c)
						+ '&disid=&offId=&ageId=' + access.user.ContactId
						+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;

				rowMap.put(policy.InsuredCLTPF_CLNTNUM__c, row);
				if(rowMap.size() == I_Const.LIST_MAX_ROWS){
					break;
				}
			}
		}

		return rowMap.values();
	}

	/**
	 * 証券番号検索の結果リストを作成
	 */
	private List<I_PolicySearchBoxRow> setRowTypePolicyNo(List<E_Policy__c> policys){
		List<I_PolicySearchBoxRow> result = new List<I_PolicySearchBoxRow>();
		for(E_Policy__c policy : policys){
			I_PolicySearchBoxRow row = new I_PolicySearchBoxRow();

			// 証券番号
			row.policyNo = policy.COMM_CHDRNUM__c;
			// 個別照会ページリンク
			row.url = policy.PolicyDetailLink__c
					+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_POLICY;
			// 個別照会リンク（ターゲット）
			row.urlTarget = policy.PolicyDetailLinkTarget__c;

			// 個人保険ヘッダ, SPVAヘッダ
			if(isEnablePolicy(policy)){
				// 契約者名
				row.ownerName = policy.ContractorName__c;
				// 被保険者名
				row.insuredName = policy.InsuredName__c;

			// 個人保険消滅, SPVA消滅
			} else {
				// 契約者名
				row.ownerName = policy.COMM_ZCLNAME__c;
				// 被保険者名
				row.insuredName = policy.COMM_ZINSNAM__c;
			}

			result.add(row);
			if(result.size() >= I_Const.LIST_MAX_ROWS){
				break;
			}
		}

		return result;
	}

	/**
	 * ソート
	 */
	public void sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		if(sortType != sType){
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;
	
		policyRows.sort();

		return;
	}
	/**
	 * URLエンコード
	 * IEで日本語が文字化けするため対応
	 */
	private String urlEncodeForUTF8(String baseStr){
		return EncodingUtil.urlEncode(baseStr,'UTF-8');
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return policyRows == null ? 0 : policyRows.size();
	}

	/**
	 * 「最近検索したキーワード」から検索
	 */
	public void doSearchLog(){
		searchType = ApexPages.currentPage().getParameters().get('searchType');
		keyword = ApexPages.currentPage().getParameters().get('keyword');

		doSearch();
	}

	public class I_PolicySearchBoxRow implements Comparable{
		// 顧客番号
		public String customerNo {get; set;}
		// 証券番号
		public String policyNo {get; set;}
		// 契約者名
		public String ownerName {get; set;}
		// 契約者名カナ
		public String ownerNameKana {get; set;}
		// 住所
		public String address {get; set;}
		// 被保険者名
		public String insuredName {get; set;}
		// 被保険者名カナ
		public String insuredNameKana {get; set;}
		// 生年月日
		public Date birthDay {get; set;}
		// 年齢
		public Decimal age {get; set;}
		// 性別
		public String sex {get; set;}
		// 顧客種別
		public String customerType {get; set;}
		// リンクURL
		public String url {get; set;}
		// リンクURL_Target
		public String urlTarget {get; set;}

		// コンストラクタ
		public I_PolicySearchBoxRow(){
			customerNo = '';
			policyNo = '';
			ownerName = '';
			ownerNameKana = '';
			address = '';
			insuredName = '';
			insuredNameKana = '';
			birthDay = null;
			age = 0;
			sex = '';
			customerType = '';
			url = '';
			urlTarget = '';
		}

		// ソート
		public Integer compareTo(Object compareTo){
			I_policySearchBoxRow compareToRow = (I_policySearchBoxRow)compareTo;
			Integer result = 0;

			// 顧客番号
			if(list_sortType == SORT_TYPE_CUSTOMER_NO){
				result = I_Util.compareToString(customerNo, compareToRow.customerNo);
			// 証券番号
			} else if(list_sortType == SORT_TYPE_POLICY_NO){
				result = I_Util.compareToString(policyNo, compareToRow.policyNo);
			// 契約者名
			} else if(list_sortType == SORT_TYPE_OWNER_NAME){
				result = I_Util.compareToString(ownerNameKana, compareToRow.ownerNameKana);
			// 住所
			} else if(list_sortType == SORT_TYPE_ADDRESS){
				result = I_Util.compareToString(address, compareToRow.address);
			// 被保険者名
			} else if(list_sortType == SORT_TYPE_INSURED_NAME){
				result = I_Util.compareToString(insuredNameKana, compareToRow.insuredNameKana);
			// 生年月日
			} else if(list_sortType == SORT_TYPE_BIRTH_DAY){
				result = I_Util.compareToDate(birthDay, compareToRow.birthDay);
			// 年齢
			} else if(list_sortType == SORT_TYPE_AGE){
				result = I_Util.compareToDecimal(age, compareToRow.age);
			// 性別
			} else if(list_sortType == SORT_TYPE_SEX){
				result = I_Util.compareToString(sex, compareToRow.sex);
			// 顧客種別
			} else if(list_sortType == SORT_TYPE_CUSTOMER_TYPE){
				result = I_Util.compareToString(customerType, compareToRow.customerType);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	//JIRA_#63 170331
	public void getSeachBoxLog(){
		getSeachBoxLogRecord(searchTypeName);
		return;
	}

	/**
	 * JIRA_#64
	 * フィルタ条件を元にWHERE句を生成する
	 */
	/*public String createPolicyCondition(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result = ' AND ( MainAgentAccountParentId__c = \'' + conditionValue + '\' '
						   + ' OR SubAgentAccountParent__c = \''  + conditionValue + '\' ) ';
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result = ' AND ( MainAgentAccount__c = \'' + conditionValue + '\' '
						   + ' OR SubAgentAccount__c = \''  + conditionValue + '\' ) ';
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					result = ' AND ( MainAgent__c = \'' + conditionValue + '\' '
						   + ' OR SubAgent__c = \''  + conditionValue + '\' ) ';
				}
			}
		// フィルタ無しの場合
		} else {
			// MR または 拠点長 または 代理店ユーザの場合 ⇒代理店ユーザまたは 社員(BR**)以外の時
			if(access.isAgent() || (access.isEmployee() && access.ZINQUIRR_is_BRAll()==false)){
				result = ' AND PolicyInCharge__c = True ';
			}
		}

		return result;
	}
	*/

	/**
	 * JIRA_#64
	 * フィルタ条件を元に個人保険特約から保険契約ヘッダレコードを取得するとき用のWHERE句を生成する(失効契約用)
	 */
	/*public String createCovpfCondition(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result = ' AND ( E_Policy__r.MainAgentAccountParentId__c = \'' + conditionValue + '\' '
						   + ' OR E_Policy__r.SubAgentAccountParent__c = \''  + conditionValue + '\' ) ';
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result = ' AND ( E_Policy__r.MainAgentAccount__c = \'' + conditionValue + '\' '
						   + ' OR E_Policy__r.SubAgentAccount__c = \''  + conditionValue + '\' ) ';
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					result = ' AND ( E_Policy__r.MainAgent__c = \'' + conditionValue + '\' '
						   + ' OR E_Policy__r.SubAgent__c = \''  + conditionValue + '\' ) ';
				}
			}
		// フィルタ無しの場合
		} else {
			// MR または 拠点長 または 代理店ユーザの場合
			if(access.isAgent() || access.isAuthMR() || access.isAuthManager()){
				result = ' AND E_Policy__r.PolicyInCharge__c = True ';
			}
		}

		return result;
	}
	*/
}