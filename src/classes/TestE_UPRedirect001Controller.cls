@isTest(SeeAllData=false)
public with sharing class TestE_UPRedirect001Controller {
	// testData
	private static E_UPBPF__c upbpf;
    private static String urlZprdCd;
    private static String urlZfndCd;
    private static String ZGFUNDCD;
    private static List<String> ZSFUNDCD;
    private static String ZPRDCD;
    private static Date baseDate;
    private static boolean ZFUNDFLG;
    private static integer EUPFPFSize = 1;
    
    private static void init(){
        createdata();
        insertMessage();
        
    }

//up001
    //ダイレクト型商品は遷移しないことを確認
    private static testMethod void testDirect() {
        //インスタンス
        ZPRDCD = 'S0105';
        ZSFUNDCD = new LIST<String>{'A098'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = E_const.UP_GFNDCD_DIRECT;
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        Pagereference  testpage = Page.up001;
        testpage.getParameters().put('zprdcd', urlZprdCd);
        Test.setCurrentPage(testpage);
        E_UPRedirect001Controller controller = new  E_UPRedirect001Controller();
        system.assertequals(null,controller.init());
        
    }
    
    //たしかなおくりもの70以下は遷移しないことを確認
    private static testMethod void testTashikana70() {
        //インスタンス
         ZPRDCD = 'S0213';
        ZSFUNDCD = new LIST<String>{'A098'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = E_const.UP_GFNDCD_PRESENT_UNDER70;
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
       Pagereference  testpage = Page.up001;
        testpage.getParameters().put('zprdcd', urlZprdCd);
        Test.setCurrentPage(testpage);
        E_UPRedirect001Controller controller = new  E_UPRedirect001Controller();
        system.assertequals(null,controller.init());
        
    }
    
    //たしかなおくりもの71以上は遷移しないことを確認
    private static testMethod void testTashikana71() {
        //インスタンス
         ZPRDCD = 'S0226';
        ZSFUNDCD = new LIST<String>{'A098'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = E_Const.UP_GFNDCD_PRESENT_OVER71;
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        Pagereference  testpage = Page.up001;
        testpage.getParameters().put('zprdcd', urlZprdCd);
        Test.setCurrentPage(testpage);
        E_UPRedirect001Controller controller = new  E_UPRedirect001Controller();
        system.assertequals(null,controller.init());
        
    }
    
    //通常の商品は遷移することを確認
    private static testMethod void testa() {
        //インスタンス
         ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A098'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        Pagereference  testpage = Page.up001;
        testpage.getParameters().put('zprdcd', urlZprdCd);
        Test.setCurrentPage(testpage);
        E_UPRedirect001Controller controller = new  E_UPRedirect001Controller();
        system.assert(controller.init().getURL().contains(Page.up002.getUrl()));
        
    }

    //URLパラメータエラー
    private static testMethod void testErrorURL() {
        //インスタンス
         ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A098'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        Pagereference testpage = Page.up001;
        Test.setCurrentPage(testpage);
        E_UPRedirect001Controller controller = new  E_UPRedirect001Controller();
        system.assert(controller.init().getURL().contains(Page.up999.getURL()));
        
    }

    //URLパラメータエラー
    private static testMethod void testNonRecord() {
        //インスタンス
         ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A098'};
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        urlZprdCd = E_EncryptUtil.prdCodeConversion('S0001', true);
        Pagereference  testpage = Page.up001;
        testpage.getParameters().put('zprdcd', urlZprdCd);
        Test.setCurrentPage(testpage);
        E_UPRedirect001Controller controller = new  E_UPRedirect001Controller();
        system.assert(controller.init().getURL().contains(Page.up999.getURL()));
        
    }

    // testDataCreate
    static void createData(){
        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        
        // ファンド群マスタ(E_FundMaster__c)
        E_FundMaster__c fundMaster = new E_FundMaster__c(
            CURRFROM__c = 0,
            CURRTO__c = 99999999,
            ZGFUNDCD__c = ZGFUNDCD,
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZGRPFND01__c = 'アイエヌジーマイプラン年金',
            ZGRPFND02__c = '',
            ZGRPFND03__c = ''
        );
        insert fundMaster;
        
        // 特別勘定(E_SVCPF__c)
        LIST<E_SVCPF__c> svcpfList = new LIST<E_SVCPF__c>();
        for(integer i = 0 ; i < EUPFPFSize;i++){
        E_SVCPF__c svcpf  = new E_SVCPF__c(
            ZFSDFROM__c = '20150101',
            ZFSHFROM__c = '10',
            ZFSMFROM__c = '10',
            ZFSDTO__c = '99999999',
            ZFSHTO__c = '99',
            ZFSMTO__c = '99',
            ZEFUNDCD__c = ZSFUNDCD[i],
            ZCATFLAG__c = '20',
            ZFUNDNAM01__c = '世界債券型（TS1）',
            ZFUNDNAM02__c = 'ＶＡ外国債券ファンド',
            ZFUNDNAM03__c = '（適格機関投資家専用）',
            ZFUNDNAM04__c = '',
            CURRFROM__c = '20150101',
            CURRTO__c = '99999999'
        );
            svcpfList.add(svcpf);
        }
        insert svcpfList;
        
        // 選択可能ファンド(E_SFGPF__c)
        LIST<E_SFGPF__c> sfgpfList = new LIST<E_SFGPF__c>();
        for(integer i = 0 ; i < svcpfList.size();i++){
        E_SFGPF__c sfgpf = new E_SFGPF__c(
            FundMaster__c = fundMaster.Id,
            E_SVCPF__c = svcpfList[i].Id,
            E_SFGPFCode__c = ZGFUNDCD + '|' + ZSFUNDCD[i]
        );
            sfgpfList.add(sfgpf);
        }
        insert sfgpfList;
        
        String[] indexDate = new String[9];
        
        for(integer i = 0;i<9;i++){
            indexDate[i] = string.valueOf(baseDate.addDays(-1 * i)).remove('-');
        }
        system.debug(indexDate);
        
        // ユニットプライス基本情報(E_UPBPF__c)
        upbpf = new E_UPBPF__c(
            E_FundMaster__c = fundMaster.Id,
            ZPETNAME__c = 'アイエヌジーマイプラン年金',
            ZGFUNDCD__c = ZGFUNDCD,
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZIDXDATE01__c = indexDate[0],
            ZIDXDATE02__c = indexDate[1],
            ZIDXDATE03__c = indexDate[2],
            ZIDXDATE04__c = indexDate[3],
            ZIDXDATE05__c = indexDate[4],
            ZPRDCD__c = ZprdCd,
            ZQLFLG__c = true,
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '080000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        );
        insert upbpf;
        
        LIST<E_UPFPF__c> upfpfList = new LIST<E_UPFPF__c>();
        for(integer i = 0 ; i < svcpfList.size();i++){
            // ユニットプライス表示用ファンド(E_UPFPF__c)
            E_UPFPF__c upfpf = new E_UPFPF__c(
                SVCPF__c = svcpfList[i].Id,
                ZFUNDFLG__c = ZFUNDFLG,
                ZSFUNDCD__c = ZSFUNDCD[i],
                ZFNDNAME01__c = '世界債券型',
                ZFNDNAME02__c = '（TS1）',
                ZIVTNAME01__c = 'ＶＡ外国債券ファンド',
                ZIVTNAME02__c = '（適格機関投資家専用）',
                ZIVTNAME03__c = '',
                ZIVTNAME04__c = '',
                ZDSPFLAG__c = '1',
                ZSTDTFRM__c = '00000000',
                ZSTTMFRM__c = '000000',
                ZSTDTTO__c = '99999999',
                ZSTTMTO__c = '999999'
            );
            upfpfList.add(upfpf);
        }
        insert upfpfList;
        
        // ユニットプライス(E_UPXPF__c)
        List<E_UPXPF__c> upxpfList = new List<E_UPXPF__c>();
        string fndcd;
        for(E_UPFPF__c item: upfpfList){
            fndcd = item.ZSFUNDCD__c;
            for(Integer i = 0; i < 9; i++){
                E_UPXPF__c upxpf = new E_UPXPF__c(
                    E_UPFPF__c = item.Id,
                    ZSFUNDCD__c = fndcd,
                    ZVINDEX__c = 100.000000000,
                    ZFUNDDTE__c = indexDate[i]
                );
                upxpfList.add(upxpf);
            }
        }
        insert upxpfList;
    }
    	/**
	 * メニューマスタ作成
	 * @param isInsert: whether to insert
	 * @param name: メニューマスタ名
	 * @param Key: メニューマスタキー
	 * @param recTypeDevName: レコードタイプ名
	 * @param showCloseBtn: 閉じるボタンを表示
	 * @return E_MenuMaster__c: メニューマスタレコード
	 */
	public static E_MenuMaster__c createMenuMaster(Boolean isInsert, String name, String Key, String recTypeDevName, Boolean showCloseBtn) {
		Id recTypeId = getRecTypeIdMap().get('E_MenuMaster__c').get(recTypeDevName);
		E_MenuMaster__c src = new E_MenuMaster__c(
					Name = name,
					RecordTypeId = recTypeId,
					MenuMasterKey__c = Key,
					IsShowCloseBtn__c = showCloseBtn
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
	
	/**
	 * メッセージマスタ作成
	 * @param isInsert: whether to insert
	 * @param name: メッセージマスタ名
	 * @param mId: メニューマスタのレコードID
	 * @param recTypeDevName: レコードタイプ名
	 * @param type: 種別
	 * @param key: Key/並び順
	 * @return E_MessageMaster__c: メッセージマスタレコード
	 */
	public static E_MessageMaster__c createMessageMaster(Boolean isInsert, String name, Id mId, String recTypeDevName, String type, String key) {
		Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get(recTypeDevName);
		E_MessageMaster__c src = new E_MessageMaster__c(
					Name = name,
					RecordTypeId = recTypeId,
					Menu__c = mId,
					Type__c = type,
					Key__c = key
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
    
    
    /**
	 * RecordTypeマップ取得
	 * @return Map<String, Map<String, Id>>: Map(sObjectType, Map(RecTypeDevName,RecTypeID)
	 */
	private static Map<String, Map<String, Id>> rMap;
	private static Map<String, Map<String, Id>> getRecTypeIdMap(){
		if(rMap != null) return rMap;
		rMap = new Map<String, Map<String, Id>>();
		for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
			if(rMap.containsKey(rt.sObjectType)){
				rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
			}else{
				rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
			}
		}
		return rMap;
	}

    private static void insertMessage(){
        LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
        String type = 'ディスクレイマー';
        String param = 'up2|001';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|002';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|003';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|004';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|005';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|006';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        insert insMessage;
    }


}