@isTest
private class TestI_ExportSpRelationCorpController {
	
	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static Contact agent;
	private static E_BizDataSyncLog__c ebizLog;
	private static E_AgencySalesResults__c asr;
	private static E_SpRelationCorp__c src;

	/**
	 * String mainSObjectAPIName()
	 */
	static testMethod void mainSObjectAPIName_test01(){
		// テストデータ作成
		createData('AH');

		Test.StartTest();
		
		I_ExportSpRelationCorpController controller = new I_ExportSpRelationCorpController();
		
		Test.stopTest();
		
		// assertion
		System.assertEquals('E_SpRelationCorp__c', controller.mainSObjectAPIName());
	}

	/**
	 * getCSVHeader
	 */
	private static testMethod void getCSVHeader_test01(){

		// テストデータ作成
		createData('AH');

		PageReference pr = Page.IRIS_ExportSpRelationCorp;
		Test.setCurrentPage(pr);
		pr.getParameters().put('agency', String.valueOf(distribute.id));

		Test.StartTest();
		
		I_ExportSpRelationCorpController controller = new I_ExportSpRelationCorpController();
		
		Test.stopTest();
		system.assert(String.isNotBlank(controller.getCSVTitle()));
		system.assert(String.isNotBlank(controller.getCSVHeader()));
		system.assert(!controller.getLines().isEmpty());
	}
	
	private static void createData(String hierarchy){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';		// 営業日

		//連携ログ作成
		Date d = E_Util.string2DateyyyyMMdd(BUSINESS_DATE);
		ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = d,BatchEndDate__c = d);
		insert ebizLog;

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;
		
		// Account 事務所
		office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
		insert office;
		suboffice = new Account(Name = 'テスト事務所SUB',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
		insert suboffice;
		
		// Contact 募集人作成
		agent = new Contact(AccountId = office.id, LastName = 'テスト募集人');
		insert agent;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = hierarchy,
			QualifSim__c = '優績S'
		);
		if(hierarchy == 'AY'){
			asr.Account__c = office.id;
		}else if(hierarchy == 'AT'){
			asr.Account__c = office.id;
			asr.Agent__c = agent.id;
		}
		insert asr;
		src = new E_SpRelationCorp__c(E_AgencySalesResults__c = asr.id,BusinessDate__c = BUSINESS_DATE);
		insert src;
	}
}