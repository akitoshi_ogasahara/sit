global with sharing class MNT_SalesToolViewController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public MNT_SalesToolViewController(ApexPages.StandardController controller) {
		super(controller);
		deleteTransitionControl='/apex/MNT_SalesToolMainteSVE';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DocumentNo__c;
		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.DisplayOrderKbn__c;
		f = I_ContentMaster__c.fields.DisplayOrderNo__c;
		f = I_ContentMaster__c.fields.ForCustomers__c;
		f = I_ContentMaster__c.fields.isRecommend__c;
		f = I_ContentMaster__c.fields.CompanyLimited__c;
		f = I_ContentMaster__c.fields.ForAgency__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.NewValidTo__c;
		f = I_ContentMaster__c.fields.Content__c;
		f = I_ContentMaster__c.fields.ContentEmployee__c;
		f = I_ContentMaster__c.fields.isHTML__c;
		f = I_ContentMaster__c.fields.Category__c;
		f = I_ContentMaster__c.fields.Management__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.LinkURL__c;
		f = I_ContentMaster__c.fields.filePath__c;
		f = I_ContentMaster__c.fields.WindowTarget__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_SalesToolViewController';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('DocumentNo__c');
			mainQuery.addFieldAsOutput('ParentContent__c');
			mainQuery.addFieldAsOutput('ApprovalNo__c');
			mainQuery.addFieldAsOutput('DisplayOrderKbn__c');
			mainQuery.addFieldAsOutput('DisplayOrderNo__c');
			mainQuery.addFieldAsOutput('ForCustomers__c');
			mainQuery.addFieldAsOutput('isRecommend__c');
			mainQuery.addFieldAsOutput('CompanyLimited__c');
			mainQuery.addFieldAsOutput('ForAgency__c');
			mainQuery.addFieldAsOutput('DisplayFrom__c');
			mainQuery.addFieldAsOutput('ValidTo__c');
			mainQuery.addFieldAsOutput('NewValidTo__c');
			mainQuery.addFieldAsOutput('Content__c');
			mainQuery.addFieldAsOutput('ContentEmployee__c');
			mainQuery.addFieldAsOutput('isHTML__c');
			mainQuery.addFieldAsOutput('Category__c');
			mainQuery.addFieldAsOutput('Management__c');
			mainQuery.addFieldAsOutput('ClickAction__c');
			mainQuery.addFieldAsOutput('LinkURL__c');
			mainQuery.addFieldAsOutput('filePath__c');
			mainQuery.addFieldAsOutput('WindowTarget__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = false;
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}