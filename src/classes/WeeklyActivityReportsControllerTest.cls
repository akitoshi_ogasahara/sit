/**
 * @Created Jia Hu on 2018/4/17
 *
 * @ModifiedBy Jia Hu on 2018/05/01
 * @version 1.0
 * @Changes add apex comment header
 * 
 */
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class WeeklyActivityReportsControllerTest {

    static testMethod void WeeklyReportUnitTest() {
        // TO DO: implement unit test
        
		WeeklyActivityReports__c weeklyreport = new WeeklyActivityReports__c();
		weeklyreport.Date__c = System.Today(); 
		weeklyreport.FAINAL_FLG__c = true;
		
		insert weeklyreport;
		
		String result = WeeklyActivityReportsController.callCalcMethod((String)weeklyreport.Id);
        
    }
}