global with sharing class CR_AgencySearchExtender extends SkyEditor2.Extender{
    
    private CR_AgencySearchSVEController controller;
    
    public CR_AgencySearchExtender(CR_AgencySearchSVEController extension){
        this.controller = extension;
    }
    
    public PageReference ExportCSV(){
        PageReference pr = Page.E_CRExportAgency;
        String soql = controller.queryMap.get('resultTable').toSoql();
        String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
        sWhere = E_SoqlUtil.trimLimitClause(sWhere);
        pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
                                E_EncryptUtil.getEncryptedString(sWhere));  
        return pr;
    }
}