/**
@version 1.0
@author PwC
リード所有者更新トリガーHelperクラス
*/
public with sharing class LMT_LeadUpdateTrigerHelper {
    public static boolean isFirstRun = true;
}