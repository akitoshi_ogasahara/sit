/***********************************************************
 *
 * Application: CRM SYSTEM
 * File Name  : EventTriggerHandler.cls
 *
 * created  : 2013/11/26 keizu)　曹文傑
 * modified :
 ************************************************************/
/**
 *
 * 行動トリガHandler
 *
 */
public with sharing class EventTriggerHandler {
	// カスタム設定取得
	EventCheck__c eventCheck = EventCheck__c.getOrgDefaults();
    //代理店事務所.ID一覧
    Set<String> setAccountID = new Set<String>();
    //レコードタイプ.ID一覧
    Set<String> setRecordTypeID = new Set<String>();
    //CustumRecordTypeName一覧
    Set<String> setCustumRecordTypeName = new Set<String>();
    //レコードタイプ.Name一覧
    Set<String> setRecordTypeName = new Set<String>();

	/**
	* onBeforeInsertUpdate
	* @param eventList 行動
	* @return なし
	* @created: 2013/11/26 keizu)　曹文傑
	* @modified: 
	*/
	public void onBeforeInsertUpdate(List<Event> eventList){
		try{
			// カスタム設定値取得チェック
			if(String.isBlank(eventCheck.Id)){
				// カスタム設定が取得できない場合
				eventList[0].addError(Label.Msg_GetCustumError);
				return;
			}

			List<EventCheck__c> eventCheckList = [select Recordtypename__c from EventCheck__c];
			if(eventCheckList.isEmpty()){
				//カスタム設定.行動チェック.レコードタイプ名が行動（Event)のレコードタイプ名に存在しない場合。
				eventList[0].addError(Label.Msg_GetCustumRecordtypenameError);
				return;
			}
			
			for(EventCheck__c ec : eventCheckList){
				setCustumRecordTypeName.add(ec.Recordtypename__c);
			}

			List<RecordType> rTList = [SELECT DeveloperName FROM RecordType where SobjectType = 'Event'];
			if(!rTList.isEmpty()){
				for(RecordType rt : rTList){
					setRecordTypeName.add(rt.DeveloperName);
				}
			}
			
			//カスタム設定.行動チェック.レコードタイプ名がレコードタイプOBJに存在しない場合。
			for(String crt : setCustumRecordTypeName){
				if(!setRecordTypeName.contains(crt)){
					eventList[0].addError(Label.Msg_GetCustumRecordtypenameError);
					return;					
				}
			}
			
			//代理店事務所.ID一覧のデータを作成する
			createSetEventWhatID(eventList);

			Map<String, Boolean> mapCommentsRequired = new Map<String, Boolean>();
			Map<String, String> mapRecordTypeName = new Map<String, String>();
				
			//代理店obj.入力チェック対象フラグをセット
			if(!setAccountID.isEmpty()){
				//代理店obj.入力チェック対象フラグを取得
				List<Account> accountList = [SELECT Id,Agency__r.CommentsRequired__c FROM Account where Id IN: setAccountID and Agency__c != null];
				
				if(!accountList.isEmpty()){
					for(Account acc : accountList){
						mapCommentsRequired.put(acc.Id,acc.Agency__r.CommentsRequired__c);
					}
				}					
			}
				
			//レコードタイプ名をセット
			if(!setRecordTypeID.isEmpty()){
				List<RecordType> recordTypeList = [SELECT Id, DeveloperName FROM RecordType where Id IN: setRecordTypeID];

				if(!recordTypeList.isEmpty()){
					for(RecordType rt : recordTypeList){
						mapRecordTypeName.put(rt.Id,rt.DeveloperName);
					}
				}
			}

			for(Event event : eventList){
				if(!mapCommentsRequired.isEmpty() && !mapRecordTypeName.isEmpty()){					
					//①行動obj.レコードタイプ名 = カスタム設定.行動チェック.レコードタイプ名
					if(mapRecordTypeName.containsKey(event.RecordTypeId) && setCustumRecordTypeName.contains(mapRecordTypeName.get(event.RecordTypeId))){
						//　②行動obj.WhatId ≠ NULL　③行動obj.WhatId  = 代理店事務所obj.Id　④　③で取得した代理店事務所obj.代理店->.->代理店obj.入力チェック対象フラグ=True
						if(mapCommentsRequired.containsKey(event.WhatId) && mapCommentsRequired.get(event.WhatId)){
							//⑤行動obj.実施後コメント入力チェック＝ＮＵＬＬ
							if(event.Comment__c == null){
								event.Comment__c.addError(Label.Msg_AfterperformingCommentsError);
							}
						}
					}						
				}
			}
		}catch(Exception ex){
			// システムエラー
			eventList[0].addError(Label.Msg_SystemError);
		}
	}

	/**
	* getSObjectName
	* オプジェクト名を取得
	* @param WhatID
	* @return String
	* @created: 2013/11/26 keizu)　曹文傑
	* @modified:
	*/
	private String getSObjectName(String WhatID){
		String keyCode = WhatID.subString(0,3);
		String sObjectName = '';
								
		//whatIｄでオブジェクトの名前を取得
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		for(Schema.SObjectType objectInstance : gd.values())
		{
			if(objectInstance.getDescribe().getKeyPrefix() == keyCode){
				sObjectName = objectInstance.getDescribe().getName();
			}
		}
		return sObjectName;
	}

    /**
    * createSetEventWhatID
    * whatIDセット作成
    * @param List<Event> eventList
    * @return なし
    * @created: 2013/11/26 keizu)　曹文傑
    * @modified: 
    */
    private void createSetEventWhatID(List<Event> eventList){
        for(Event et : eventList){
            // 代理店事務所
            if(et.WhatID != null){
            	String sObjectName = getSObjectName(et.whatId);
				
				//代理店事務所の場合、代理店事務所IDをセット
				if(sObjectName.equals('Account')){
					setAccountID.add(et.WhatID);
				}
            }
            
            if(et.RecordTypeId != null){
            	setRecordTypeID.add(et.RecordTypeId);
            }
        }
    }
}