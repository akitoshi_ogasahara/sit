/**
 * 新契約申込進捗照会(JP) 申込番号検索 Extenderクラス
 */
global with sharing class E_ProgressSearchAppNumberExtender extends E_AbstractSearchExtender {
	
	private String iProductCd1;
	private String iProductCd2;
	private String iProductCd3;
	private String iProductNum1;
	private String iProductNum2;
	private String iProductNum3;
	
	//extension
	E_ProgressSearchAppNumberController extension;
	
	public E_ProgressSearchAppNumberExtender(E_ProgressSearchAppNumberController extension) {
		super();
		this.extension = extension;
		extension.iProductCd1_val.SkyEditor2__Text__C = E_Const.PROGRESS_JP_PRODUCTCD;
		extension.iProductCd2_val.SkyEditor2__Text__C = E_Const.PROGRESS_JP_PRODUCTCD;
		extension.iProductCd3_val.SkyEditor2__Text__C = E_Const.PROGRESS_JP_PRODUCTCD;
	}
	
	global override void init() {
		pageRef = doAuth(E_Const.ID_KIND.SPECIAL_FEATURES, null);
	}
	
	public Pagereference pageAction (){
		return E_Util.toErrorPage(pageRef, null);
	}
	
	global override void preSearch() {
		pageMessages.clearMessages();
		if (extension != null) {
			iProductCd1 = extension.iProductCd1_val.SkyEditor2__Text__C;
			iProductCd2 = extension.iProductCd2_val.SkyEditor2__Text__C;
			iProductCd3 = extension.iProductCd3_val.SkyEditor2__Text__C;
			iProductNum1 = extension.iProductNum1_val.SkyEditor2__Text__C;
			iProductNum2 = extension.iProductNum2_val.SkyEditor2__Text__C;
			iProductNum3 = extension.iProductNum3_val.SkyEditor2__Text__C;
		
			//#1 『申込番号検索』がクリックされ、すべての申込番号項目が未入力だった場合
			if (!E_Util.isNotNull(iProductNum1) &&
			 	!E_Util.isNotNull(iProductNum2) && 
				!E_Util.isNotNull(iProductNum3)) {
				pageMessages.addErrorMessage(getMSG().get('PGJ|001'));
				throw new SkyEditor2.ExtenderException('');			//後続の処理を実施させないため、空メッセージで例外を生成
			}
			//#2 
			notIProductNum(iProductNum1);
			notIProductNum(iProductNum2);
			notIProductNum(iProductNum3);
			
			//#3 
			productNumAndNotIProductCd(iProductCd1, iProductNum1);
			productNumAndNotIProductCd(iProductCd2, iProductNum2);
			productNumAndNotIProductCd(iProductCd3, iProductNum3);
			
			//#5
			notIProductCd(iProductCd1);
			notIProductCd(iProductCd2);
			notIProductCd(iProductCd3);
			
			if (E_Util.isNotNull(iProductCd1) && E_Util.isNotNull(iProductNum1)) {
				/*
				extension.iProductNum1_val.SkyEditor2__Text__C = 
						extension.iProductCd1_val.SkyEditor2__Text__C 
						+ extension.iProductNum1_val.SkyEditor2__Text__C;
				*/
			} else {
				extension.iProductCd1_val.SkyEditor2__Text__C = null;
			}
			if (E_Util.isNotNull(iProductCd2) && E_Util.isNotNull(iProductNum2)) {
				/*
				extension.iProductNum2_val.SkyEditor2__Text__C =
						extension.iProductCd2_val.SkyEditor2__Text__C 
						+ extension.iProductNum2_val.SkyEditor2__Text__C; 
				*/
			} else {
				extension.iProductCd2_val.SkyEditor2__Text__C = null;
			}
			if (E_Util.isNotNull(iProductCd3) && E_Util.isNotNull(iProductNum3)) {
				/*
				extension.iProductNum3_val.SkyEditor2__Text__C =
						extension.iProductCd3_val.SkyEditor2__Text__C 
						+ extension.iProductNum3_val.SkyEditor2__Text__C;
				*/
			} else {
				extension.iProductCd3_val.SkyEditor2__Text__C = null;
			}
		}
	}
	
	global override void afterSearch() {
		extension.iProductNum1_val.SkyEditor2__Text__C = iProductNum1;
		extension.iProductNum2_val.SkyEditor2__Text__C = iProductNum2;
		extension.iProductNum3_val.SkyEditor2__Text__C = iProductNum3;
		extension.iProductCd1_val.SkyEditor2__Text__C = iProductCd1;
		extension.iProductCd2_val.SkyEditor2__Text__C = iProductCd2;
		extension.iProductCd3_val.SkyEditor2__Text__C = iProductCd3;
	}

	/**
	 * #2 『申込番号検索』がクリックされ、申込番号の入力値が下記の条件にて不正だった場合
	 * ・半角英数字でない
	 * ・９文字で入力されていない
	 */
	private void notIProductNum(String iProductNum) {
		System.debug(iProductNum);
		if (E_Util.isNotNull(iProductNum) && (!E_Util.isEnAlphaNum(iProductNum) || iProductNum.length() != 9)) {
			//throw new SkyEditor2.ExtenderException(getMSG().get('PGJ|002'));
			pageMessages.addErrorMessage(getMSG().get('PGJ|002'));
			throw new SkyEditor2.ExtenderException('');			//後続の処理を実施させないため、空メッセージで例外を生成
		}
	}
	
	/**
	 * #3 『申込番号検索』がクリックされ、申込番号項目が入力されているが、商品コードが未入力だった場合
	 */
	private void productNumAndNotIProductCd(String iProductCd, String iProductNum) {
		if (E_Util.isNotNull(iProductNum) && !E_Util.isNotNull(iProductCd)) {
			//throw new SkyEditor2.ExtenderException(getMSG().get('PGJ|003'));
			pageMessages.addErrorMessage(getMSG().get('PGJ|003'));
			throw new SkyEditor2.ExtenderException('');			//後続の処理を実施させないため、空メッセージで例外を生成
		}
	}
	
	/**
	 * #5 『申込番号検索』がクリックされ、商品コードの入力値が下記の条件にて不正だった場合
	 * ・半角英数字でない
	 * ・２文字で入力されていない
	 */
	private void notIProductCd(String iProductCd) {
		if (E_Util.isNotNull(iProductCd) && (!E_Util.isEnAlphaNum(iProductCd) || iProductCd.length() != 2)) {
			//throw new SkyEditor2.ExtenderException(getMSG().get('PGJ|004'));
			pageMessages.addErrorMessage(getMSG().get('PGJ|004'));
			throw new SkyEditor2.ExtenderException('');			//後続の処理を実施させないため、空メッセージで例外を生成
		}
	}
}