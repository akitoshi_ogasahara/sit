global with sharing class CR_BizBooksExtender extends CR_AbstractExtender{

	private CR_BizBooksController controller;
	private Map<String, String> branchMap;
    
	// 代理店事務所情報取得(AY権限ユーザ)
	public Contact officeInfoByAY {get; set;}
	// 代理店格レコードID
	public Id distributorId {get; set;}
	// 選択された代理店事務所
	public String[] selOfficeList {get; set;}
	// 出力基準年月
	public String outputRecordYm {get; set;}
	        
	// 代理店事務所選択リスト作成
	public List<SelectOption> getOfficeOptions(){
		// 代理店格レコードID
		distributorId = this.controller.record.Account__c;

		// 代理店事務所取得
		branchMap = new Map<String, String>();
		List<SelectOption> ops = new List<SelectOption>();
		List<Account> offices;
		if(access.isAgent()){
			//代理店の場合は全事務所を取得する。（AHのみしか事務所選択を表示しないため）
			offices = E_AccountDaoWithout.getRecsByParentId(distributorId);
		}else{
			//NN社員の場合は、照会者コード（支社コードによる抽出を行う）
			offices = E_AccountDaoWithout.getOfficesByParentId_BRCD(distributorId
																		, access.idcpf.ZINQUIRR__c
																		, new Set<String>{'1','2'});	//有効フラグ1,2を指定
		}
		
		for(Account office: offices){		//getRecsByParentIdIsActive		無効事務所も含めて選択可能とする　20160426
			ops.add(new SelectOption(office.E_CL2PF_ZAGCYNUM__c,office.E_CL2PF_ZAGCYNUM__c + '　' + office.Name));
			branchMap.put(office.E_CL2PF_ZAGCYNUM__c, office.E_CL2PF_BRANCH__c);
		}
		return ops;
	}
    
	/**
	 * Constructor
	 */
	public CR_BizBooksExtender(CR_BizBooksController extension){
		super();
		this.controller = extension;
		this.helpMenukey = CR_Const.MENU_HELP_BOOKS;
	} 
    
	/**
	 * init
	 */
	public override void init(){
		isSuccessOfInitValidate = false;
		
		// URLパラメータチェック
		if(this.controller.record == null 
				|| String.isBlank(this.controller.record.AgencyCode__c)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);
		// 権限チェック(大規模代理店=True、帳簿書類権限セット)
		}else if(!(getHasBooksPermission() == true && this.controller.record.LargeAgency__c)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG05_PH2_REQUIRED_PERMISSION_BOOKS); 
		// エラーなし
		}else{
			isSuccessOfInitValidate = true;
		}

		// 代理店事務所情報設定(AY権限ユーザ)
		if(isAuthAY){
			User u = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
			officeInfoByAY = E_ContactDao.getRecById(u.ContactId);
			
			// 事務所コード設定
			selOfficeList = new List<String>();
			selOfficeList.add(officeInfoByAY.Account.E_CL2PF_ZAGCYNUM__c);	
		}

		// 出力基準年月の初期値取得
		outputRecordYm = E_Util.getFormatDateStr(Date.today(), CR_Const.OUTPUT_RECORD_YM_FORMAT);
	}
    
	/**
	 * ダウンロード履歴一覧取得
	 */
	public List<E_DownloadHistorry__c> getHistoryList(){
		return E_DownloadHistoryDao.getRecsByAgencyAndFormNo(this.controller.record.Id, CR_Const.TYPE_BIZBOOKS, E_Util.getDayAgo(CR_Const.CR_ATTACHMENTS_EXPIRE_DAYS));
	}
    
	/**
	 * データ作成依頼
	 */
	public PageReference doRequestCreateData(){

		// Savepoint
		Savepoint sp = Database.setSavepoint();
		
		try{
			// 検索条件(JSON)作成
			String dlCondition = getDownloadCondition();
			
			// ダウンロード履歴作成
			E_DownloadHistorry__c dh = E_DownloadHistoryDao.createDlHistory(
						this.controller.record.Id, CR_Const.TYPE_BIZBOOKS, outputRecordYm, dlCondition);
			
			// エラーチェック
			if(validate(dlCondition, dh) == false){
				return null;
			}

			// ダウンロード履歴登録
			insert dh;
		
			// ダウンロード履歴Shareレコード取得
			List<E_DownloadHistorry__Share> dlShareList = getDownloadHistoryShareList(dh.Id);
			insert dlShareList;	
			
			// 代理店提出更新(最新依頼日時)
			CR_Agency__c agency = CR_AgencyDao.getAgencyLastRequestById(this.controller.record.Id);
			agency.LastRequestBook__c = Datetime.now();
			update agency;

			PageReference pr = Page.E_CRBizBooks;
			pr.getParameters().put('id', controller.record.Id);
			return pr;
			
		}catch(exception e){
			// rollback
			Database.rollback(sp);
			pageMessages.addErrorMessage(e.getMessage());		// + e.getStackTraceString()
			return null;
		}
	}
	
	/**
	 * エラーチェック
	 */
	private Boolean validate(String dlCondition,E_DownloadHistorry__c dh){   
		// 事務所選択チェック
		if(selOfficeList.isEmpty()){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG06_PH2_REQUIRED_OFFICES);
			return false;
		}
			
		// 出力基準年月チェック
		if(CR_Validate.hasErrOutputRecordYm(outputRecordYm)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG01_PH2_FRAUD_OUTPUTYM);
			return false;
		}
		 
		// 出力基準年月期間チェック
		if(CR_Validate.hasErrOutputRecordYmFrom(outputRecordYm, CR_Const.OUTPUT_RECORD_YM_BOOKS_FROM, CR_Const.OUTPUT_RECORD_YM_BOOKS_TO)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG07_PH2_FROM_OUTPUTYM);
			return false;
		}
		
		/* 2016/05/06 matsuzaki ETL実行予定日が同じデータを取得するように変更 */
		// 数式項目値算出
		dh.recalculateFormulas();

		// ステータスが「処理待ち」、かつETL実行予定日が同じダウンロード履歴取得
		List<E_DownloadHistorry__c> downloadHistroyList = E_DownloadHistoryDaoWithout.getRecsByAgencyAndFormNoAndStatusAndEtlPlanDate(
					this.controller.record.Id,	 // 代理店提出レコードID 
					CR_Const.TYPE_BIZBOOKS, 
					CR_Const.DL_STATUS_WAIT,
					dh.EtlPlanDate__c);
		/*
		// ステータスが「処理待ち」のダウンロード履歴取得
		List<E_DownloadHistorry__c> downloadHistroyList = E_DownloadHistoryDaoWithout.getRecsByAgencyAndFormNoAndStatus(
					this.controller.record.Id, 
					CR_Const.TYPE_BIZBOOKS, 
					CR_Const.DL_STATUS_WAIT,
					CR_Validate.getRequestFromDatetime());
		*/
		
		// リクエスト同条件チェック
		if(CR_Validate.hasErrSameCondition(outputRecordYm, dlCondition, downloadHistroyList)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG02_PH2_SAME_CONDITION);
			return false;
		}
		
		// リクエスト回数チェック
		if(CR_Validate.hasErrRequestCount(downloadHistroyList)){ 
			pageMessages.addErrorMessage(CR_Const.ERR_MSG03_PH2_LIMIT_REQUEST);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 検索条件(JSON)作成
	 */
	private String getDownloadCondition(){
		E_DownloadCondition.BizBooksDownloadCondi dlCondi = new E_DownloadCondition.BizBooksDownloadCondi();
		dlCondi.ZHEADAY = controller.record.AgencyCode__c;
		dlCondi.officeCodes = selOfficeList;
		return dlCondi.toJSON();
	}
	
	/**
	 * ダウンロード履歴Share取得
	 */
	private List<E_DownloadHistorry__Share> getDownloadHistoryShareList(Id recId){
		List<E_DownloadHistorry__Share> dhShareList = new List<E_DownloadHistorry__Share>();		
		
		// 1件のみ代理店事務所を選択
		if(selOfficeList.size() == 1){
			// 支社コードの設定
			String branchCode = isAuthAY ? officeInfoByAY.Account.E_CL2PF_BRANCH__c : branchMap.get(selOfficeList[0]);
						
			// 「L2+事務所コード 公開グループ」,「BR+支社コード(募集人の代理店事務所) 公開グループ」をShareに登録
			List<E_DownloadHistorry__Share> dhShareL2List = E_ShareUtil.getDownloadHistoryShareL2(recId, new Set<String>{selOfficeList[0]}, new Set<String>{branchCode});
			dhShareList.addAll(dhShareL2List);
		}
		
		//支社社員が出力依頼の場合、支社公開グループをShareに登録
		if(access.isEmployee() && access.idcpf.ZINQUIRR__c!='BR**'){
			List<E_DownloadHistorry__Share> dhShareBRList = E_ShareUtil.getDownloadHistoryShareBR(recId, new Set<String>{access.idcpf.ZINQUIRR__c.right(2)});
			dhShareList.addAll(dhShareBRList);
		}
		
		// 「L1+代理店コード 公開グループ」をShareに登録
		List<E_DownloadHistorry__Share> dhShareL1List = E_ShareUtil.getDownloadHistoryShareL1(recId, new Set<String>{this.controller.record.AgencyCode__c});	
		dhShareList.addAll(dhShareL1List);
		return dhShareList;
	}
}