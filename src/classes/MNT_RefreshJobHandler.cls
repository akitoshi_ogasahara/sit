public class MNT_RefreshJobHandler {
	public class RefreshJobHandlerException extends Exception{}

	private String sObjectName;
	private List<MNT_Masking.maskField> maskfields{get;set;}
	
	public MNT_RefreshJobHandler(String sObjNm){
		this.sObjectName = sObjNm;
		this.maskFields = new List<MNT_Masking.maskField>();
		for(String fldNm:MNT_SandBoxRefresh.REFRESH_JOBs.get(sObjectName).keyset()){
			this.maskFields.add(MNT_Masking.createMaskField(fldNm
															, MNT_SandBoxRefresh.REFRESH_JOBs.get(sObjectName).get(fldNm)));
		}
	}
	
	
	public Database.QueryLocator getQueryLocator(){
		List<String> fldNms = new List<String>();
		for(MNT_Masking.maskField mf:maskfields){
			fldNms.add(mf.fieldName);
		}
		
		String soql = 'SELECT Id ';
		if(fldNms.size()>0){
			soql += ', ' + String.join(fldNms, ',');
		}

		soql += ' FROM ' + sObjectName;
		
		//ユーザオブジェクトの場合のみの特別対応
		if(this.sObjectName == 'User'){
			Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'システム管理者'];
			
			soql += ' WHERE ProfileId != \'' + adminProfile.Id + '\' ';
		}
			
		return  Database.getQueryLocator(soql);			
	}


	public void maskRecords(List<SObject> records){

		if(this.maskFields.size()>0){
			for(SObject rec:records){
				for(MNT_Masking.maskField mf:maskfields){
					mf.mask(rec);
				}		
			}
			
			//Database.SaveResult result = database.update(records, false);
			// 部分的なUpdateを許可する。
			Map<Id, String> results = E_GenericDao.updatePartial(records);
			if(!results.isEmpty()){
				String s = '=== ■' + this.SObjectName + '■ '+ String.valueOf(results.size()) + '/' + String.valueOf(records.size()) + '  Error ===';
				for(Id errId:results.keyset()){
					s += String.valueOf(errId) + ':' + results.get(errId) + '   ';
				}
				throw new RefreshJobHandlerException(s);
			}
		}else{
			//項目リストが0件の場合、削除処理を実施
			delete records;
		}	
	}
	
	
	public String getNextSObjectName(){
		String wkNm = null;
		for(String nm:MNT_SandBoxRefresh.REFRESH_JOBs.keyset()){
			if(wkNm == this.sObjectName){		//ひとつ前のオブジェクト名が一致する場合に、次のオブジェクト名を返す
				return nm;
			}
			wkNm = nm;		//WK変数に確保
		}
		
		return null;
	}
}