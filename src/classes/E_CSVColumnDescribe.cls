public class E_CSVColumnDescribe {
	public class E_CSVColumnDescribeException extends Exception{}

	/**
	 *	String型の項目定義クラス
	 */
	public virtual class BaseField{
		// 項目API参照名
		public String fieldAPI{get;set;}
		// 項目ラベル名 未設定の場合はDescribeから取得
		protected String headerLabel{get;set;}

		//Constructor
		public BaseField(){}
		public BaseField(String nm){
			this.fieldAPI = nm;
		}
		
		public BaseField(String nm, String headerLbl){
			this(nm);
			this.headerLabel = headerLbl;
		}
		
		// 項目ラベル名取得
		public virtual String getFieldLabel(String sobjNm){
			if(String.isNotBlank(headerLabel)){
				return E_Util.getEscapedCSVString(headerLabel);
			}
			String lbl = E_Util.getFieldLabel(sobjNm, fieldAPI);
			return E_Util.getEscapedCSVString(lbl);
		}
	
		//　項目値取得
		public virtual String getStringValue(SObject rec){
			if(rec == null) return '';
			
			return String.valueOf(rec.get(fieldAPI));
		}
		
		//　項目値（エスケープ後）取得
		public String getEscapedString(SObject rec){
			String val = getStringValue(rec);
			return E_Util.getEscapedCSVString(val);
		}
	}

	/**
	 *	Decimal型の項目定義クラス
	 */
	public virtual class DecimalField extends BaseField{
	
		//Constructor
		public DecimalField(String nm){
			this.fieldAPI = nm;
		}
	}

	/**
	 *	Boolean型の項目定義クラス
	 */
	public virtual class BooleanField extends BaseField{
	
		//Constructor
		public BooleanField(String nm){
			this.fieldAPI = nm;
		}
		public BooleanField(String nm, String headerLbl){
			this.fieldAPI = nm;
			this.headerLabel = headerLbl;
		}

		public override String getStringValue(SObject rec){
			if(rec == null) return '';
			if(Boolean.valueOf(rec.get(fieldAPI))){
				return '1';
			}else{
				return '0';
			}
		}
	}

	/**
	 *	DateTime型の項目定義クラス
	 */
	public virtual class DateTimeField extends BaseField{
		//Constructor
		public DateTimeField(String nm){
			this.fieldAPI = nm;
		}
		public DateTimeField(String nm, String headerLbl){
			this(nm);
			this.headerLabel = headerLbl;
		}
		
		// 日付フォーマット  yyyy/MM/dd H:mm形式					その他のフォーマットの時には別クラスを作成
		public virtual String getFormatString(){
			return 'yyyy/MM/dd H:mm';			//時刻部分は24時間形式の0埋めなし
		}
		
		// 日付値へ変換
		public override String getStringValue(SObject rec){
			if(rec == null) return '';
			Object val = rec.get(fieldAPI);
			if(val == null){
				return '';
			}else{
				return Datetime.valueOf(val).format(getFormatString());
			}
		}
	}
	
	/**
	 *	Date型の項目定義クラス
	 */
	public virtual class DateField extends BaseField{
		//Constructor
		public DateField(String nm){
			this.fieldAPI = nm;
		}
		public DateField(String nm, String headerLbl){
			this(nm);
			this.headerLabel = headerLbl;
		}

		// 日付値へ変換 yyyy/MM/dd
		public override String getStringValue(SObject rec){
			if(rec == null) return '';
			Object val = rec.get(fieldAPI);
			if(val == null){
				return '';
			}else{
				return Date.valueOf(val).format();
			}
		}
	}

	/**
	 *	親オブジェクトのテキスト項目定義クラス
	 *			標準オブジェクトを参照する場合は、要見直し
	 */
	public virtual class ParentStringField extends BaseField{
		//参照先オブジェクトAPI
		protected String referToAPI;
		//項目API名リスト
		protected List<String> flds;
	
		//Constructor
		public ParentStringField(String nm, String parentObjNm){
			this.fieldAPI = nm;
			this.referToAPI = parentObjNm;

			//ピリオドで分割した内容を格納
			this.flds = fieldAPI.split('\\.',-1);
			system.assert(flds.size()==2, 'getStringValueメソッドは親オブジェクトの値まで取得可能です。親の親は対応してません。');
		}
		public ParentStringField(String nm, String parentObjNm, String headerLbl){
			this(nm, parentObjNm);
			this.headerLabel = headerLbl;
		}
		
		public virtual override String getStringValue(SObject rec){
			if(rec == null) return '';
			String ret = '';
			try{
				SObject parent = rec.getSObject(flds[0]);
				if(parent != null 
					&& parent.get(flds[1]) != null){
					ret = String.valueOf(parent.get(flds[1]));
				}
				return ret;
			}catch(Exception e){
				throw new E_CSVColumnDescribeException('親オブジェクト項目値の取得に失敗[' + fieldAPI + '/' + referToAPI + ':' + e.getMessage() + ']');
			}
		}
		
		public override String getFieldLabel(String sobjNm){
			if(String.isNotBlank(headerLabel)){
				return E_Util.getEscapedCSVString(headerLabel);
			}
			String referFieldAPI = flds[0].replace('__r','__c');
			String parentlbl = E_Util.getFieldLabel(sobjNm, referFieldAPI);
			String lbl = E_Util.getFieldLabel(referToAPI, flds[1]);
			
			//参照関係の項目は「:（半角コロン）」で結合される。
			return E_Util.getEscapedCSVString(parentlbl + ': '+ lbl);
		}
		
	}

	/**
	 *	親オブジェクトのチェックボックス項目定義クラス
	public virtual class ParentBooleanField extends ParentStringField{
		public ParentBooleanField(String nm, String parentObjNm){
			super(nm, parentObjNm);
		}
		public override String getStringValue(SObject rec){
			if(rec == null) return '';
			String ret = '0';
			try{
				SObject parent = rec.getSObject(flds[0]);
				if(parent != null){ 
					Object val = parent.get(flds[1]);
					if(val!=null && Boolean.valueOf(val)){
						ret = '1';
					}	
				}
				return ret;
			}catch(Exception e){
				throw new FSKCMRBUtils.RoboException('親オブジェクト項目値の取得に失敗[' + fieldAPI + '/' + referToAPI + ':' + e.getMessage() + ']');
			}
		}
	}
	 */





}