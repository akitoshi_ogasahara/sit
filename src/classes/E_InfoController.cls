public virtual with sharing class E_InfoController extends E_AbstractController{
	
	public List<E_MessageMaster__c> cmsContents{get{
													if(cmsContents==null){
														isValidate();
													}
													return cmsContents;
												}
												set;}
	public E_MenuMaster__c menu{get; protected set;}
	private List<String> errorParameterOptions;

	/**
	 *		Constructor
	 */
	public E_InfoController(){
		super();
		this.menu = null;
	}

	public virtual PageReference init(){
		pageRef = doAuth(E_Const.ID_KIND.INFO, userinfo.getUserId());
		System.debug('**********************' + pageRef);
		return E_Util.toErrorPage(pageRef, errorParameterOptions);

	}

	public override Boolean isValidate(){
		try{
			/*	メニューレコードの取得チェック	*/
			String mId = ApexPages.currentPage().getParameters().get('menuid');
			String mkey = getMenuKey();
			
			//	メニューId　→　メニューKeyの優先順序でメニューレコードを取得
			if(String.isNotBlank(mId)){
				this.menu = E_MessageMasterDao.getMenuMasterRecById(mId);
			}else{
				this.menu = E_MessageMasterDao.getMenuMasterRecByKey(mkey);
			}		
	
			if(this.menu==null){
				this.errorMsgCode = 'ERR|004';			//ページが存在しません
				this.errorParameterOptions = new List<String>{mId, mkey};
				return false;
			}else{
				this.pgTitle = this.menu.Name;			//pageTitleをprotected変数に設定する
				cmsContents = E_MessageMasterDao.getMessagesByMenuId(this.menu.Id);
			}
	
			/*	メニューレコードの参照権限チェック	*/
			if(canAccessMenu(this.menu)==false){
				this.errorMsgCode = 'ERR|006';			//ページが参照できません。
				this.errorParameterOptions = new List<String>{mId, mkey};
	
				return false;
			}
		}catch(Exception e){
			this.errorMsgCode = 'ERR|999';
			this.errorParameterOptions = new List<String>{e.getMessage()};
			return false;
		}	

		return true;
	}

	//MenuKeyの取得 通常はUrlから取得
	protected virtual String getMenuKey(){
		return ApexPages.currentPage().getParameters().get('menu');
	}
	

	/** 
	 *	canAccessMenu
	 *		メニューへの参照権限をチェック
	 *			E_MenuMasterDaoのクエリ条件と同期をとること
	 */
	public Boolean canAccessMenu(E_MenuMaster__c menu){
		E_AccessController accCon = E_AccessController.getInstance();
		//システム管理者はすべてアクセス可
		if(accCon.isSystemAdministrator()) return true;

		//社員ユーザのみメニュー　かつ　実行ユーザが社員ユーザ以外の場合にNG
		if(menu.IsStandardUserOnly__c && !accCon.isEmployee()) return false;

		//代理店ユーザのみメニュー　かつ　実行ユーザが代理店ユーザ以外の場合にNG
		if(menu.IsPartnerUserOnly__c && !accCon.isAgent()) return false;

		//共同GW利用フラグFalse　かつ　共同GW経由の場合にNG
		if(!menu.UseCommonGW__c && accCon.isCommonGateway() && !accCon.isEmployee()) return false;

		//インターネット利用フラグFalse　かつ　インターネット経由の場合にNG
		if(!menu.UseInternet__c && accCon.isViaInternet() && !accCon.isEmployee()) return false;
		
		return true;
    }


}