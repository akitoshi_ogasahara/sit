public with sharing class E_BizDataSyncBatchOperation_SalesPlan extends E_BizDataSyncBatchOperation {

	private E_BizDataSyncLog__c ebizLog;
	private Map<Id,Id> asrUpsertKeyMap;
	public class SalesPlanBatchException extends Exception{}

	/**
	 * getQueryLocator for Batch start method
	 */
	public override Database.QueryLocator getQueryLocator(){
//		errList = new List<String>();
		return Database.getQueryLocator(
					[SELECT 
						Id
						,ParentAccount__c
					From 
						E_SalesPlan__c 
					Where 
						ParentAccount__c != null
					And
						SalesResult__c != null
					]);
	}
	
	/**
	 * execute
	 * 		batchのExecuteの本体ロジック
	 */
	public override void execute(List<Sobject> records){
		ebizLog = [Select id,InquiryDate__c From E_BizDataSyncLog__c Where kind__c =: 'F2' ORDER BY CreatedDate desc limit 1];
		//AH用代理店挙積情報UpsertKeyMapの作成
		makeAsrUpsertKeyMap(ebizlog.InquiryDate__c);
		//代理店挙積情報の付替え
		for(SObject spRec : records){
			spRec.put('SalesResult__c', 
				asrUpsertKeyMap.get((ID)spRec.get('ParentAccount__c')));
		}

		//営業計画のUpdate
		Database.SaveResult[] srList = Database.update(records,false);

		// エラーレコードのエラー内容をセット
		String errMsg = '';
		for(Integer i = 0; i< records.size(); i++){
			if(srList != null && !srList.get(i).isSuccess()){
				Database.Error err = srList.get(i).getErrors().get(0);
				errMsg += '[' + records.get(i).get('Id') + '/' +err.getmessage() + ']\n';
			}				
		}
		if(String.isNotBlank(errMsg)){
			throw new SalesPlanBatchException(errMsg+records);
		}

	}

	/**
	 * UpsertKeyの営業日部分を指定された日付で置換する
	 *
	private String convUpsertkey(String beforeKey, Date updDate){
		//UpsertKeyの末尾が8桁数字の日付データである前提
//		String bizDate = beforeKey.right(8);
		String bizDate = beforeKey.split('|').get(7);
		system.debug(bizDate);
		//日付部分を置換し返却		
		return beforeKey.replace(bizDate,E_Util.date2YYYYMMDD(updDate));
	}*/

	/**
	 * AH用代理店挙積情報UpsertKeyMapの作成
	 */
	private void makeAsrUpsertKeyMap(Date updDate){
		//階層がAH、営業日が指定日付（基本は連携日）の代理店挙積情報を取得し、
		//UpsertKeyをkey,IDをValueとしたMapへ詰める
		List<E_AgencySalesResults__c> asrList = E_AgencySalesResultsDao.getRecordsByHierarchyAndBizDate('AH',E_Util.date2YYYYMMDD(updDate));

		asrUpsertKeyMap = new Map<Id,Id>();
		for(E_AgencySalesResults__c asr : asrList){
			asrUpsertKeyMap.put(asr.ParentAccount__c, asr.Id);
		}
	}

}