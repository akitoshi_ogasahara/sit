/**
 *          E_UPTable005Service 
 *              ユニットプライス表示データ保持クラス
 */
public with sharing class E_UPTable005Service {
    //1ページあたりのユニットプライス数
    private static final Integer COUNT_PER_PAGE = 4;
    
    public class UPIndexNotExistsException extends Exception{}

    public class UPTablePage implements Comparable{
        //ページ番号
        public Integer pageNo{get;set;}
        //特別勘定名表示区分
        private String NAMEKBN;
        //ユニットプライス表示用ファンドリスト
        //  ViewState対策としてIdのリストのみをViewStateに保持する。レコードは都度取得
        private List<Id> upIndexIds;
        transient private List<E_UPFPF__c> upIndexes;
        
        public UPTablePage(Integer pgNo, String nmKbn){
            this.pageNo = pgNo;
            this.NAMEKbn = nmKbn;
            this.upIndexIds = new List<Id>();
            this.upIndexes = new List<E_UPFPF__c>();        //追加は呼出しクラスにて行う
        }
        
        /*  ユニット表示用ファンドレコードの追加  */
        public void add(E_UPFPF__c rec){
            this.upIndexIds.add(rec.Id);
            this.upIndexes.add(rec);
        }
    
        /*  表示用Table    */
        public String getIndexTableHTML(String fromDtStr, String toDtStr){
            Date fromDt = E_Util.string2DateyyyyMMdd(fromDtStr);
            Date toDt = E_Util.string2DateyyyyMMdd(toDtStr);
            System.assert(fromDt<=toDt, '指定されている日付値が正しくありません。[' + fromDt.format() + '-' + toDt.format() + ']');
            
            //ページ切替時にIdのリストからIndex値を再取得
            //if(upIndexes == null){
                upIndexes = E_UPFPFDao.getIndexesBySvcpfIds(this.upIndexIds, fromDtStr, toDtStr);
            //}
            
            /*  HTML組立  */
            //カラム数をそろえる
            while(upIndexes.size()<COUNT_PER_PAGE){
                upIndexes.add(null);
            }

            String html = '<table class="greyTableStyle upIndexTable col-md-12">';
            //ヘッダーの作成
            html += '<tr><th>日付</th>';
            for(Integer i=0;i<COUNT_PER_PAGE;i++){
                html += '<th>'+getFundLabel(upIndexes[i])+'</th>';          
            }
            html += '</tr>';
            
            //Bodyの作成
            Map<Date, List<Decimal>> idxMap = getIndexMapByDate(upIndexes);
            for(Date dt=fromDt;dt<=toDt;dt=dt.addDays(1)){
                html += '<tr>';
                html += createHTMLTag('td', 'headCol', dt.format());            //<td style="headCol">日付</td>
                for(Integer i=0;i<COUNT_PER_PAGE;i++){
                    if(idxMap.containsKey(dt)){
                        html += createHTMLTag('td', toFormattedString(idxMap.get(dt)[i]));          //<td>インデックス値</td>
                    }else{
                        html += createHTMLTag('td', null);          //<td>インデックス値</td>
                    }
                }
                html += '</tr>';
            }
            
            html += '</table>';

            return html;
        }
        
        private String createHTMLTag(String tag, String val){
            String html = '<{0}>{1}</{2}>';
            val = (val==null?'-':val);
            return String.format(html, new List<String>{tag, val, tag});
        }
        private String createHTMLTag(String tag, String styleCls, String val){
            String html = '<{0} class="{1} ">{2}</{3}>';
            val = (val==null?'-':val);
            styleCls = (styleCls==null?'':styleCls);
            return String.format(html, new List<String>{tag, styleCls, val, tag});
        }
        
        /**
         *  toFormattedString
         *      //小数点以下3桁目を切り捨て
         */
        private String toFormattedString(Decimal val){
            //値がないまたは0の場合に、"-"に変換する（変換ロジックは createHTMLTagにて実装）
            if(val==null||val==0) return null;
            return E_Util.getRoundingDownString(val, 2);
        }
        
        /**
         *  getFundLabel
         *      ファンド表示名の切り替え
         */
        private String getFundLabel(E_UPFPF__c rec){
            if(rec==null) return '&nbsp;';
            
            String fundLabel;
            // 名称取得区分『0』 かつ コンポジットファンドフラグ『false』 ：ファンド名＋投資信託名
            if(NAMEKBN == '0' && !rec.ZFUNDFLG__c){
                fundLabel = rec.ZNAMEUnion__c;
                
            // 名称取得区分『0』 かつ コンポジットファンドフラグ『true』 ：投資信託名
            }else if(NAMEKBN == '0' && rec.ZFUNDFLG__c){
                fundLabel = rec.ZIVTNAMEUnion__c;
                
            // 名称取得区分『1』  ：ファンド名
            }else{
                fundLabel = rec.ZFNDNAMEUnion__c;
            }
            return fundLabel;
            
            /*
            return NAMEKBN == '0'   ?   rec.ZIVTNAMEUnion__c
                                        :rec.ZFNDNAMEUnion__c;
            */
        }
        
        /**
         *      getIndexMapByDate
         *          日付、ファンド毎インデックス　のMapを作成する。
         */
        private Map<Date, List<Decimal>> getIndexMapByDate(List<E_UPFPF__c> records){
            Map<Date, List<Decimal>> ret = new Map<Date, List<Decimal>>();
            for(Integer i=0;i<records.size();i++){
                E_UPFPF__c rec = records[i];
                if(rec!=null){
                    for(E_UPXPF__c upxpf: rec.E_UPXPFs__r){
                        Date dt = E_Util.string2DateyyyyMMdd(upxpf.ZFUNDDTE__c);
                        if(ret.containsKey(dt)==false){
                            ret.put(dt, new List<Decimal>(COUNT_PER_PAGE));     //Fund数分リスト領域を確保
                        }
                        ret.get(dt)[i] = upxpf.ZVINDEX__c;
                    }
                }
            }
            return ret;
        }
        
        
        /**
         * ソート処理：Page番号順
         */
        public Integer compareTo(Object rec){
            UPTablePage compareRec = (UPTablePage)rec;
            return this.pageNo - compareRec.pageNo;
        }
        
    }

    //Paging
    private Map<Integer, UPTablePage> tablePages;

    /**
     *      Constructor
     */
    public E_UPTable005Service(List<E_UPFPF__c> funds, String nmKbn){
        tablePages = new Map<Integer, UPTablePage>();
        Integer pgNo = 1;
        Boolean hasIndex = false;
        for(E_UPFPF__c rec:funds){
            if(rec.E_UPXPFs__r.size()>0){
                hasIndex = true;
            }
            if(tablePages.get(pgNo)==null){
                tablePages.put(pgNo, new UPTablePage(pgNo, nmKbn));
            }   
            UPTablePage uptblPg = tablePages.get(pgNo);
            uptblPg.add(rec);
            if(uptblPg.upIndexes.size()>=COUNT_PER_PAGE){
                pgNo++;
            }
        }
        
        if(!hasIndex){
            //例外を生成
            throw new UPIndexNotExistsException();
        }
    }

    /**
     *      getUPTablePages
     *          ページ単位のリスト
     */
    public List<UPTablePage> getUPTablegetUPTablePagesPages(){
        List<UPTablePage> ret = tablePages.values();
        ret.sort();
        return ret;
    }

    /**
     *      ユニットプライス表示用HTML
     */
    public String getIndexTableHTML(Integer pgNo, String sfromDt, String stoDt){
        UPTablePage tblPg = tablePages.get(pgNo);
        if(tblPg!=null){
            return tblPg.getIndexTableHTML(sfromDt, stoDt);
        }else{
            return null;
        }
    }
}