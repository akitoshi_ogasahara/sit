public with sharing class E_LogoDownloadController extends E_InfoController{
	
	public static final String MENUMASTERKEY_LOGO_MARK = 'logo_mark';

	/**
	 *		Constructor
	 */
	public E_LogoDownloadController(){
		super();
	}
	
	protected override String getMenuKey(){
		return MENUMASTERKEY_LOGO_MARK;
	}

}