public class MD_Const {
	/* 選択リスト */
	public static final String SELECTED_NONE_LABEL = '指定なし';
	public static final String SELECTED_NONE_VALUE = 'None';

	public static final String SELECT_LABEL_SORT_LOCATION = '指定位置から近い順';
	public static final String SELECT_LABEL_SORT_EXAMINATIONDATE = '直近の診査日が新しい順';

	/* ドクターオブジェクト 項目API名 */
	// 嘱託医名
	public static final String DOCTOR_COL_API_NAME = 'Name';
	// 病院名
	public static final String DOCTOR_COL_API_HOSPITALNAME = 'HospitalName__c';
	// 性別
	public static final String DOCTOR_COL_API_GENDER = 'Gender__c';
	// 年齢
	public static final String DOCTOR_COL_API_AGE = 'Age__c';
	// 血液検査
	public static final String DOCTOR_COL_API_BLOOD = 'Blood__c';
	// 特定医
	// public static final String DOCTOR_COL_API_PARTICULARDOCTOR = 'ParticularDoctor__c';
	// 心電図
	public static final String DOCTOR_COL_API_ELECTROCARDIOGRAM = 'Electrocardiogram__c';
	// 緯度経度
	public static final String DOCTOR_COL_API_LOCATION = 'Location__c';
	// 直近の診査日
	public static final String DOCTOR_COL_API_EXAMINATIONDATE = 'ExaminationDate__c';
	// 県ｺｰﾄﾞ
	public static final String DOCTOR_COL_API_STATECODE = 'StateCode__c';
	// 嘱託医名（検索用）
	public static final String DOCTOR_COL_API_SEARCHNAME = 'SearchName__c';
	// 病院名（検索用）
	public static final String DOCTOR_COL_API_SEARCHHOSPITALNAME = 'SearchHospitalName__c';

	/* 画面名 */
	// エリアから探す画面
	public static final String VF_NAME_MD_AREA = 'E_MDArea';
	// 住所・駅名から探す画面
	public static final String VF_NAME_MD_ADDRESS = 'E_MDAddress';
	// 画面pageReferenceと論理名のMap
	public static map<String, String> screenNameMap = new map<String, String> {
		'System.PageReference[/apex/E_MDLocation]' => '現在地から検索',
		'System.PageReference[/apex/E_MDAddress]' => '住所・駅から検索',
		'System.PageReference[/apex/E_MDArea]' => 'エリア地図から検索',
		'System.PageReference[/apex/E_MDDoctor]' => '病院名から検索'
	};

	/* Top画面 */
	// メニューキー（お知らせ）
	public static final String MD_MENU_KEY_INFO = 'MD_Information';
	// メニューキー（掲示ファイル）
	public static final String MD_MENU_KEY_FILE = 'MD_File';
	// 検索結果最大件数（お知らせ）
	public static final Integer MD_LIMIT_INFO = 100;

	/* 検索条件 ラベル */
	public static final String MD_LABEL_CONDITION_NAME = '嘱託医';
	public static final String MD_LABEL_CONDITION_HOSPTALNAME = '病院';
	public static final String MD_LABEL_CONDITION_GENDER = '性別';
	public static final String MD_LABEL_CONDITION_BLOOD = '年齢';
	public static final String MD_LABEL_CONDITION_PARTICULARDOCTOR = '血液検査';
	// public static final String MD_LABEL_CONDITION_ELECTROCARDIOGRAM = '特定医';
	public static final String MD_LABEL_CONDITION_AGE = '心電図';
	public static final String MD_LABEL_CONDITION_DICTANCE_LOCATION = '現在地からの距離';
	public static final String MD_LABEL_CONDITION_DICTANCE_ADDRESS = '住所・駅からの距離';
	public static final String MD_LABEL_CONDITION_DICTANCE_AREA = '住所・駅からの距離';

	/* 検索結果最大件数 */
	public static final Integer MD_LIMIT_SEARCH_RESULT = 30;
}