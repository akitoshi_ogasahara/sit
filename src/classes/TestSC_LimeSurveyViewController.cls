@isTest
private class TestSC_LimeSurveyViewController {
	
	@isTest static void contracterTest01() {
		PageReference pageRef = Page.IRIS_SCLimeSurveyView;
		pageRef.getParameters().put('token','testToken');
		pageRef.getParameters().put('surveyid','999999');
		Test.setCurrentPage(pageRef);

		Test.startTest();
			SC_LimeSurveyViewController controller = new SC_LimeSurveyViewController();
		Test.stopTest();
		System.assertEquals(controller.isError,false);
	}
	
	/*
	コンストラクタテスト 異常系1
	 */
	@isTest static void contracterTest02() {
		PageReference pageRef = Page.IRIS_SCLimeSurveyView;
		pageRef.getParameters().put('surveyid','999999');
		Test.setCurrentPage(pageRef);

		Test.startTest();
			SC_LimeSurveyViewController controller = new SC_LimeSurveyViewController();
		Test.stopTest();
		System.assertEquals(controller.isError,true);
	}

	/*
	コンストラクタテスト 異常系2
	 */
	@isTest static void contracterTest03() {
		PageReference pageRef = Page.IRIS_SCLimeSurveyView;
		pageRef.getParameters().put('token','testToken');
		Test.setCurrentPage(pageRef);

		Test.startTest();
			SC_LimeSurveyViewController controller = new SC_LimeSurveyViewController();
		Test.stopTest();
		System.assertEquals(controller.isError,true);
	}

}