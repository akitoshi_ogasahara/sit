public with sharing class E_PieGraphPolicyController extends E_AbstractPieGraph {

    public void createGraph() {

        //レコードタイプがNULLの場合は処理終了
        if (recordTypeName == null){
            return;
        }
        //更新データマップ
        if (updateData != null && !String.isEmpty(updateData)) {
            updateDataMap = (Map<Id, Decimal>)Json.deserialize(updateData, Map<Id, Decimal>.class);
        }
        //グラフ表示データ作成
        graphRecs = new List<GraphData>();
        //グラフ表示データ作成（更新）
        upGraphRecs = new List<GraphData>();
        //グラフデータテーブル作成
        graphTableRecs = new List<GraphDataTable>();

        if (executeKind.equals(E_Const.GRAPH_POLICY_FUNDTRANSFER)) {
            graphPolicyFundTransfer();
        } else if (executeKind.equals(E_Const.GRAPH_POLICY_FUNDRATIO)) {
            graphPolicyFundRatio();
        } else if (executeKind.equals(E_Const.GRAPH_POLICY_PORTFOLIO)) {
            graphPolicyPortfolio();
        }

    }

    public String getInwardHeader(){
        String ret = '';
        if(getIsEntry()){
            ret = '割合(%)';
        } else if(getIsComplete()){
            ret = '新しい繰入比率(%)';
        }
        return ret;
    }

    private void graphPolicyFundTransfer () {

        //COLI
        if (recordTypeName.equals (E_Const.POLICY_RECORDTYPE_COLI)) {
            // V002の時、表示する。
            E_Policy__c policy = E_PolicyDao.getRecByIdChildSVFPFFundTransfer(policyId);

            for (E_SVFPF__c rec : policy.E_SVFPFs__r) {
                //申込 or 参照 or 確認
                if (getIsEntry() || getIsConfirm()) {
                    //変額保険ファンド.保有割合!=null && 変額保険ファンド.保有割合 > 0
                    if (rec.ZHLDPERC__c != null && rec.ZHLDPERC__c > 0) {
                        GraphData graph = new GraphData(rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZHLDPERC__c);
                        graphRecs.add(graph);
                        colorList.add(rec.E_SVCPF__r.ZCOLORCD__c);
                        GraphDataTable graphTab = new GraphDataTable(rec.E_SVCPF__r.ZCOLORCD__c, rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZREVAMT__c, rec.ZHLDPERC__c);
                        graphTableRecs.add(graphTab);
                    }
                }
            }
        //SPVA
        } else if (recordTypeName.equals (E_Const.POLICY_RECORDTYPE_SPVA)) {
            //保険契約ヘッダ取得（id）
            E_Policy__c policy = E_PolicyDao.getRecById(policyId);
            //Integer count = 0;
            if (policy.E_FundMaster__c != null) {
                //ファンド群マスタ取得（ファンド群id）
                E_FundMaster__c fundMaster = E_FundMasterDao.getRecByIdChildSFGPF(policy.E_FundMaster__c);
                //key:特別勘定.id value:選択可能ファンド
                Map<Id, E_SFGPF__c> sfgpfMap = new Map<Id, E_SFGPF__c>();
                for (E_SFGPF__c rec : fundMaster.E_SFGPFs__r) {
                    sfgpfMap.put(rec.E_SVCPF__c, rec);
                }
                if (sfgpfMap.size() > 0) {
                    //変額契約ファンド取得（保険契約ヘッダidと対象の特別勘定ID）
                    List<E_SVFPF__c> svfpfList = E_SVFPFDao.getRecsBySVCPFIds(policyId, sfgpfMap.keySet());
                    for (E_SVFPF__c rec : svfpfList) {
                        //申込 or 確認
                        if (getIsEntry() || getIsConfirm()) {
                            //変額保険ファンドに存在する かつ (変額保険ファンド.保有割合!=null && 変額保険ファンド.保有割合 > 0)
                            if (sfgpfMap.containsKey(rec.E_SVCPF__c) && (rec.ZHLDPERC__c != null && rec.ZHLDPERC__c > 0)) {
                                GraphData graph = new GraphData(rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZHLDPERC__c);
                                graphRecs.add(graph);
                                colorList.add(rec.E_SVCPF__r.ZCOLORCD__c);
                                GraphDataTable graphTab = new GraphDataTable(rec.E_SVCPF__r.ZCOLORCD__c, rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZREVAMT__c, rec.ZHLDPERC__c);
                                graphTableRecs.add(graphTab);
                            }
                        }
                    }
                }
            }
        }

        //確認 or 完了
        if (updateDataMap != null) {
            List<E_SVCPF__c> svcpfList = E_SVCPFDao.getRecsByPolicyId(updateDataMap.keySet());
            Decimal sum = 0;
            for (E_SVCPF__c rec : svcpfList) {
                //確認
                if (getIsConfirm()) {
                    GraphData upGraph = new GraphData(rec.ZFUNDNAMEUNION__c, updateDataMap.get(rec.Id));
                    upGraphRecs.add(upGraph);
                    upColorList.add(rec.ZCOLORCD__c);
                }
                //完了
                if (getIsComplete()) {
                    if (updateDataMap.get(rec.Id) != null && updateDataMap.get(rec.Id) > 0) {
                        GraphData graph = new GraphData(rec.ZFUNDNAMEUNION__c, updateDataMap.get(rec.Id));
                        graphRecs.add(graph);
                        colorList.add(rec.ZCOLORCD__c);
                        GraphDataTable graphTab = new GraphDataTable(rec.ZCOLORCD__c, rec.ZFUNDNAMEUNION__c, 0, updateDataMap.get(rec.Id));
                        graphTableRecs.add(graphTab);
                        sum += updateDataMap.get(rec.Id);
                    }
                }
            }
            if (getIsComplete()) {
                GraphDataTable graphTab = new GraphDataTable('', '合計', 0, Integer.valueOf(sum));
                graphTableRecs.add(graphTab);
            }
        }
        colors = String.join(colorList, ',');
        upColors =  String.join(upColorList, ',');
        /* //　時計回りにする場合
        graphRecs = sortDescGraphRecord(graphRecs);
        upGraphRecs = sortDescGraphRecord(upGraphRecs);
        colors = sortDescColor(colorList);
        upColors =  sortDescColor(upColorList);
        */
    }
    
    /*繰入比率に使用*/
    private void graphPolicyFundRatio () {
        E_Policy__c policy = E_PolicyDao.getRecByIdChildSVFPF(policyId);
        for (E_SVFPF__c rec : policy.E_SVFPFs__r) {
            //申込 or 参照 or 確認
            if (getIsEntry() || getIsConfirm()) {
                //変額保険ファンド.繰入比率!=null && 変額保険ファンド.繰入比率 > 0
                if (rec.ZINVPERC__c != null && rec.ZINVPERC__c > 0) {
                    GraphData graph = new GraphData(rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZINVPERC__c);
                    graphRecs.add(graph);
                    colorList.add(rec.E_SVCPF__r.ZCOLORCD__c);
                    GraphDataTable graphTab = new GraphDataTable(rec.E_SVCPF__r.ZCOLORCD__c, rec.E_SVCPF__r.ZFUNDNAMEUNION__c,rec.ZINVPERC__c);
                    graphTableRecs.add(graphTab);
                }
            }
        }
        //確認 or 完了
        if (updateDataMap != null) {
            List<E_SVCPF__c> svcpfList = E_SVCPFDao.getRecsByPolicyId(updateDataMap.keySet());
            Decimal sum = 0;
            for (E_SVCPF__c rec : svcpfList) {
                //確認
                if (getIsConfirm()) {
                    GraphData upGraph = new GraphData(rec.ZFUNDNAMEUNION__c, updateDataMap.get(rec.Id));
                    upGraphRecs.add(upGraph);
                    upColorList.add(rec.ZCOLORCD__c);
                }
                //完了
                if (getIsComplete()) {
                    GraphData graph = new GraphData(rec.ZFUNDNAMEUNION__c, updateDataMap.get(rec.Id));
                    graphRecs.add(graph);
                    colorList.add(rec.ZCOLORCD__c);
                    GraphDataTable graphTab = new GraphDataTable(rec.ZCOLORCD__c, rec.ZFUNDNAMEUNION__c, 0, updateDataMap.get(rec.Id));
                    graphTableRecs.add(graphTab);
                    sum += updateDataMap.get(rec.Id);
                }
            }
            if (getIsComplete()) {
                GraphDataTable graphTab = new GraphDataTable('', '合計', 0, Integer.valueOf(sum));
                graphTableRecs.add(graphTab);
            }
        }
        colors = String.join(colorList, ',');
        upColors =  String.join(upColorList, ',');
        /* //　時計回りにする場合
        graphRecs = sortDescGraphRecord(graphRecs);
        upGraphRecs = sortDescGraphRecord(upGraphRecs);
        colors = sortDescColor(colorList);
        upColors =  sortDescColor(upColorList);
        */
    }

    //ポートフォリオ
    private void graphPolicyPortfolio () {
        //SPVA
        if (recordTypeName.equals (E_Const.POLICY_RECORDTYPE_SPVA)) {
            //保険契約ヘッダ取得（id）
            E_Policy__c policy = E_PolicyDao.getRecById(policyId);
            //Integer count = 0;
            if (policy.E_FundMaster__c != null) {
                //ファンド群マスタ取得（ファンド群id）
                E_FundMaster__c fundMaster = E_FundMasterDao.getRecByIdChildSFGPF(policy.E_FundMaster__c);
                //key:特別勘定.id value:選択可能ファンド
                Map<Id, E_SFGPF__c> sfgpfMap = new Map<Id, E_SFGPF__c>();
                for (E_SFGPF__c rec : fundMaster.E_SFGPFs__r) {
                    sfgpfMap.put(rec.E_SVCPF__c, rec);
                }
                if (sfgpfMap.size() > 0) {
                    //変額契約ファンド取得（保険契約ヘッダidと対象の特別勘定ID）
                    List<E_SVFPF__c> svfpfList = E_SVFPFDao.getRecsBySVCPFIds(policyId, sfgpfMap.keySet());
                    for (E_SVFPF__c rec : svfpfList) {
                        if (getIsEntry()) {
                            //変額保険ファンドに存在する かつ (変額保険ファンド.保有割合!=null && 変額保険ファンド.保有割合 > 0)
                            if (sfgpfMap.containsKey(rec.E_SVCPF__c) && (rec.ZHLDPERC__c != null && rec.ZHLDPERC__c > 0)) {
                                GraphData graph = new GraphData(rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZHLDPERC__c);
                                graphRecs.add(graph);
                                colorList.add(rec.E_SVCPF__r.ZCOLORCD__c);
                                GraphDataTable graphTab = new GraphDataTable(rec.E_SVCPF__r.ZCOLORCD__c, rec.E_SVCPF__r.ZFUNDNAMEUNION__c, rec.ZREVAMT__c, rec.ZHLDPERC__c);
                                graphTableRecs.add(graphTab);
                            }
                            //更新データが存在する場合
                            if (updateDataMap != null && updateDataMap.containsKey(rec.id)) {
                                GraphData upGraph = new GraphData(rec.E_SVCPF__r.ZFUNDNAMEUNION__c, updateDataMap.get(rec.id));
                                upGraphRecs.add(upGraph);
                                upColorList.add(rec.E_SVCPF__r.ZCOLORCD__c);
                            }
                        }
                    }
                }
            }
        colors = String.join(colorList, ',');
        upColors =  String.join(upColorList, ',');
        /* //　時計回りにする場合
        graphRecs = sortDescGraphRecord(graphRecs);
        upGraphRecs = sortDescGraphRecord(upGraphRecs);
        colors = sortDescColor(colorList);
        upColors =  sortDescColor(upColorList);
        */
        }
    }
}