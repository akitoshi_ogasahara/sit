public with sharing class E_ContactTriggerBizLogic {
	//contactレコードタイプのMap
	Map<String,Id> conRecTypeMap;

	public E_ContactTriggerBizLogic() {
		//contactレコードタイプのMap作成
		conRecTypeMap = new Map<String,Id> {
			 E_Const.CONTACT_RECORDTYPE_CLIENT => E_RecordTypeDao.getRecIdBySobjectTypeAndDevName(E_Const.CONTACT_RECORDTYPE_CLIENT, SObjectType.Contact.Name)
			,E_Const.CONTACT_RECORDTYPE_AGENT => E_RecordTypeDao.getRecIdBySobjectTypeAndDevName(E_Const.CONTACT_RECORDTYPE_AGENT, SObjectType.Contact.Name)
		};
	}

	/**
	 * 募集人コードの有無によりレコードタイプを設定
	　*	@param List<Contact> newList Contactリスト
	 *	@return void
	 */
	public void updateRecordType(List<Contact> newList){
		//レコードタイプはデフォルト設定されてしまうため全レコード対象でレコードタイプを更新する
		for(Contact rec : newList){
			//募集人コードがNullは顧客レコードタイプ、非Nullは募集人レコードタイプを設定
			if(String.isBlank(rec.E_CL3PF_AGNTNUM__c)){
				rec.RecordTypeId = conRecTypeMap.get(E_Const.CONTACT_RECORDTYPE_CLIENT);
			} else if(!String.isBlank(rec.E_CL3PF_AGNTNUM__c)){
				rec.RecordTypeId = conRecTypeMap.get(E_Const.CONTACT_RECORDTYPE_AGENT);
			}
		}
	}

	/**
	 * 募集人レコードの関連顧客項目へ募集人顧客コードと同じ値を顧客番号に持つ顧客レコードを設定する
	　*	@param List<Contact> newList ContactNewリスト
	　*	@param Map<Id,Contact> oldMap ContactOldマップ
	 *	@return void
	 */
	public void updateAgentToClientRelation(List<Contact> newList, Map<Id,Contact> oldMap){
		//追加・更新された顧客又は募集人に関連するレコードを更新対象とするため顧客番号、募集人顧客コードをSetに格納
		Set<String> candidateCLTNUMs = new Set<String>();
		for(Contact rec : newList){
			//after update処理対象
			//下記after updateの場合の処理条件以外はSetに格納しない
			//　・顧客番号が変更された場合、又は、募集人顧客コードが変更された場合、主ECLTがNULLの場合
			if(oldMap != null){
				if(!((rec.E_CLTPF_CLNTNUM__c != oldMap.get(rec.Id).E_CLTPF_CLNTNUM__c) ||
					 (rec.E_CL3PF_CLNTNUM__c != oldMap.get(rec.Id).E_CL3PF_CLNTNUM__c) ||
					 (rec.ParentECLT__c == null))){
				}
 				//Oldの顧客番号をセットにadd
 				//※Oldの顧客番号の顧客レコードに紐づいていた募集人レコードを更新するため
				if(!String.isBlank(oldMap.get(rec.Id).E_CLTPF_CLNTNUM__c)){
					candidateCLTNUMs.add(oldMap.get(rec.Id).E_CLTPF_CLNTNUM__c);
				}
			}

			//Newの顧客番号、又は、募集人顧客コードをセットにadd
			if(!String.isBlank(rec.E_CLTPF_CLNTNUM__c)){
				candidateCLTNUMs.add(rec.E_CLTPF_CLNTNUM__c);
			} else if(!String.isBlank(rec.E_CL3PF_CLNTNUM__c)){
				candidateCLTNUMs.add(rec.E_CL3PF_CLNTNUM__c);
			}
		}

		//候補となる顧客番号セットが空の場合は処理しない
		if(!candidateCLTNUMs.isEmpty()){
			//関連顧客取得用の募集人顧客コードセット、更新用の募集人リスト
			Set<String> updateCLTNUMs = new Set<String>();
			List<Contact> updateAgents = new List<Contact>();

			//候補顧客番号セットから更新対象のレコード取得
			for(Contact candidateRec : E_ContactDaoWithout.getAgentRecsByCltnums(conRecTypeMap.get(E_Const.CONTACT_RECORDTYPE_AGENT),candidateCLTNUMs)){
				updateCLTNUMs.add(candidateRec.E_CL3PF_CLNTNUM__c);
				updateAgents.add(candidateRec);
			}

			//更新用の募集人リストが空でない場合
			if(!updateAgents.isEmpty()){
				//募集人顧客コードと同じ値が顧客番号に設定された紐付先の既存顧客レコードMap作成
				Map<String,Contact> relatedClientsMap = new Map<String,Contact>();
				for(Contact clientRec : E_ContactDaoWithout.getClientRecsByCltnums(updateCLTNUMs)){
					relatedClientsMap.put(clientRec.E_CLTPF_CLNTNUM__c, clientRec);
				}
				
				//更新用の募集人レコード毎に主ECLT項目をNull⇒紐付先が存在する場合再設定
				for(Contact agentRec : updateAgents){
					agentRec.ParentECLT__c = null;
					//紐付先の既存顧客レコードMapが空でない場合
					if(!relatedClientsMap.isEmpty()){
						if(relatedClientsMap.containsKey(agentRec.E_CL3PF_CLNTNUM__c)){
							//同一顧客番号の顧客レコードへの参照を募集人レコードへ設定
							//※IDが異なる場合のみ。顧客番号と募集人顧客コードに同じ値が設定されている場合に循環参照を防ぐため
							if(agentRec.Id != relatedClientsMap.get(agentRec.E_CL3PF_CLNTNUM__c).Id){
								agentRec.ParentECLT__c = relatedClientsMap.get(agentRec.E_CL3PF_CLNTNUM__c).Id;
							}
						}
					}
				}
				
				//トリガの再帰呼び出しを防ぐため、update前後でスキップフラグをON/OFFする
				E_ContactTriggerHandler.isSkipTriggerActions = true;
				update updateAgents;
				E_ContactTriggerHandler.isSkipTriggerActions = false;
			}
		}
	}

    //R1.1対応
    //管理責任者が項目変更された時にId管理を更新する
    //管理責任者＝Yであれば手数料試算フラグはtrue
    public void commissionSetByContact( List<Contact> newList, Map<Id,Contact> oldMap ){
		List<Id> contactIds = new List<id>();
		for(Contact c : newList ){
			Contact oldRec = oldMap.get(c.Id);
			if(oldRec.E_CL3PF_ZAGMANGR__c != c.E_CL3PF_ZAGMANGR__c ){ //管理責任者が更新されていれば対象
				contactIds.add( c.Id );
			}
		}
        if( !contactIds.isEmpty() ){
            List<E_IDCPF__c> idcpfs = new List<E_IDCPF__c>();
		    idcpfs = E_IDCPFDaoWithout.getRecsByContactIds(contactIds);
            for( E_IDCPF__c rec : idcpfs ){
                if(rec.User__r.Contact.E_CL3PF_ZAGMANGR__c == 'Y'){
                    rec.AtriaCommissionAuthorityEditable__c = true;
                }
            }
            update idcpfs;
		}
	}

	/* AtriaR1.1対応により以下は不要 管理責任者更新時にはEIDCの更新を行う */
	// /**
	//  * 権限セットの再設定
	//  * 		管理責任者（E_CL3PF_ZAGMANGR__c）が変更されている場合、自主点検権限セットを再設定
	//  */
	// public void permissionSetAssign(List<Contact> newList, Map<Id,Contact> oldMap){
	// 	Set<Id> targetContactIds = new Set<Id>();

	// 	// 対象セット
	// 	for(Contact newRec : newList){
	// 		// 管理責任者（E_CL3PF_ZAGMANGR__c）が変更されている場合、自主点検権限セットを再設定
	// 		if(newRec.E_CL3PF_ZAGMANGR__c != oldMap.get(newRec.Id).E_CL3PF_ZAGMANGR__c){
	// 			targetContactIds.add(newRec.Id);
	// 		}
	// 	}

	// 	if(targetContactIds.isEmpty()){
	// 		return;
	// 	}

	// 	doAssing(targetContactIds);
	// }

	// /**
	//  * 権限セットの再設定実行
	//  * 		MIXED_DML_OPERATION回避のため、Futureで実行
	//  */
	// @future
	// public static void doAssing(Set<Id> targetContactIds){
	// 	// ID管理取得
	// 	List<E_IDCPF__c> idcpfs = E_IDCPFDao.getRecsByContactIds(targetContactIds);

	// 	if(!idcpfs.isEmpty()){
	// 		E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
	// 		bizLogic.addTargetUsers(idcpfs);
	// 		bizLogic.PermissionSetAssign();
	// 	}
	// }
}