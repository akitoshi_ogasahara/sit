/**
 * 基底となるExtenderの抽象クラスです
 */
public abstract class E_AbstractSearchExtender extends E_AbstractExtender {
	
	/**
	 * 特定代理店用機能判定
	 */
	protected override virtual boolean doAuthSpecialFeatures () {
		//オブジェクトアクセス制限
		//JP
		if (this instanceof E_ProgressSearchAgentExtender || this instanceof E_ProgressSearchAppNumberExtender) {
			if (!access.isAccessibleSpecialFeaturesJp()) {
				errorMsgCode = E_Const.ERROR_MSG_OBJECT;
				return false;
			}
		} else if (this instanceof E_ProgressSearchBankSVEExtender) {
			if (!access.isAccessibleSpecialFeaturesBank()) {
				errorMsgCode = E_Const.ERROR_MSG_OBJECT;
				return false;
			}
		}

		return true;
	}
	
	/**
	 * 契約関連機能判定
	 */
	protected override virtual boolean doAuthContact (String contactId) {
		//機能制限確認
		if (this instanceof E_ColiSearchExtender) {
			if (!access.canColi()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			} 
		} else if (this instanceof E_SpvaSearchExtender) {
			if (!access.canSpva()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}
		} else if (this instanceof E_AnnuitySearchExtender) {
			if (!access.canSpva()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}
		} 
		
		//オブジェクトアクセス制限
		if (!access.isAccessiblePolicy()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		
		//パラ―メータ確認
		if (!E_Util.isValidateId(contactId, Schema.Contact.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		
		if(this instanceOf E_ColiSearchExtender
		|| this instanceOf E_SpvaSearchExtender
		|| this instanceOf E_AnnuitySearchExtender) {
			access.setReferer(E_Const.ID_KIND.CONTACT, new List<PageReference>{Page.E_CustomerInfo}, E_CookieHandler.REFERER_SEARCH);
		}
		return true;
	}
	
	/**
	 * 投信信託関連機能判定
	 * ※2015年、投資信託サービス終了に伴い投資信託機能をコメントアウト
	 */
	/*
	protected override virtual boolean doAuthInvestment (String investmentId) {
		//機能制限確認
		if (!access.canInvestment()) {
			errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
			return false;
		}
		
		//オブジェクトアクセス制限
		if (!access.isAccessibleInvestment()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		return true;
	}
	*/
}