@isTest
private class TestI_LogViewController {
	static testMethod void pageActionTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
			Test.stopTest();

			system.assertEquals(lcon.rowCount, I_Const.LIST_DEFAULT_ROWS);
			system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_ACCESS_DATETIME);
			system.assertEquals(lcon.sortIsAsc, false);
		}
	}

	static testMethod void rowCountTest01() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '5');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
			Test.stopTest();

			system.assertEquals(lcon.rowCount, 5);
		}
	}

	static testMethod void rowCountTest02() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '1001');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
			Test.stopTest();

			system.assertEquals(lcon.rowCount, I_Const.LIST_MAX_ROWS);
		}
	}

	static testMethod void addRowTest01() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);

		Test.startTest();
			List<E_Log__c> testLog = new List<E_Log__c>();
			for (Integer i = 0, j = 30; i < j; i++) {
				testLog.add(new E_Log__c(Name = 'testLog' + i, AccessUser__c = actUser.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage',isDisplay__c = true));
			}
			insert testLog;
			pr.getParameters().put('id', actUser.id);

			system.runAs(actUser) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);

				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.addRows();

				system.assertEquals(lcon.rowCount, I_Const.LIST_DEFAULT_ROWS + I_Const.LIST_ADD_MORE_ADD_ROWS);
				system.assertEquals(lcon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
				//system.assertEquals(lcon.getLogCount(), 30);
				system.assertEquals(lcon.getTotalRows(), 30);
			}
		Test.stopTest();
	}

	static testMethod void addRowTest02() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '1001');

		Test.startTest();
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
			insert testAgency;

			List<Contact> testAgents = new List<Contact>();
			for (Integer i = 0, j = 1000; i < j; i++) {
				testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
			}
			insert testAgents;

			List<User> testUser = new List<User>();
			String userName;
			Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
			for (Contact testAgent : testAgents) {
				userName = testAgent.LastName + '@terrasky.ingtesting';
				User src = new User(
						  Lastname = testAgent.LastName
						, Username = userName
						, Email = userName
						, ProfileId = p.Id
						, Alias = testAgent.LastName.left(8)
						, TimeZoneSidKey = UserInfo.getTimeZone().getID()
						, LocaleSidKey = UserInfo.getLocale()
						, EmailEncodingKey = 'UTF-8'
						, LanguageLocaleKey = UserInfo.getLanguage()
						, ContactId = testAgent.Id
				);
				testUser.add(src);
			}
			insert testUser;

			List<E_Log__c> testLog = new List<E_Log__c>();
			for (Integer i = 0, j = 1000; i < j; i++) {
				testLog.add(new E_Log__c(Name = 'testLog_' + testUser[0].Lastname, AccessUser__c = testUser[0].Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage',isDisplay__c = true));
			}
			insert testLog;
			pr.getParameters().put('id', testUser[0].id);
			system.runAs(actUser) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);

				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.addRows();

				system.assertEquals(lcon.rowCount, I_Const.LIST_MAX_ROWS);
				system.assertEquals(lcon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
				//system.assertEquals(lcon.getLogCount(), I_Const.LIST_MAX_ROWS);
				system.assertEquals(lcon.getTotalRows(), I_Const.LIST_MAX_ROWS);
				system.assertEquals(lcon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
				
			}
		Test.stopTest();
	}

	static testMethod void sortRowsTest01() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_LOG_NAME);
		
		
		Test.startTest();
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
			insert testAgency;

			List<Contact> testAgents = new List<Contact>();
			for (Integer i = 0, j = 5; i < j; i++) {
				testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
			}
			insert testAgents;

			List<User> testUser = new List<User>();
			String userName;
			Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
			for (Contact testAgent : testAgents) {
				userName = testAgent.LastName + '@terrasky.ingtesting';
				User src = new User(
						  Lastname = testAgent.LastName
						, Username = userName
						, Email = userName
						, ProfileId = p.Id
						, Alias = testAgent.LastName.left(8)
						, TimeZoneSidKey = UserInfo.getTimeZone().getID()
						, LocaleSidKey = UserInfo.getLocale()
						, EmailEncodingKey = 'UTF-8'
						, LanguageLocaleKey = UserInfo.getLanguage()
						, ContactId = testAgent.Id
				);
				testUser.add(src);
			}
			insert testUser;

			List<E_Log__c> testLog = new List<E_Log__c>();
			for (Integer i = 0, j = 5; i < j; i++) {
				testLog.add(new E_Log__c(Name = 'testLog_' + testUser[0].Lastname,SummaryName__c ='testLog_' + testUser[0].Lastname, AccessUser__c = testUser[0].Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage', ActionType__c = 'ページ',isDisplay__c = true));
			}
			insert testLog;

			system.runAs(actUser) {
			//system.runAs(testUser[0]) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);
				pr.getParameters().put('id', testUser[0].id);
				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.sortRows();


				system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_LOG_NAME);
				system.assertEquals(lcon.sortIsAsc, true);
				system.assertEquals(testAgents.size(), 5);
				system.assertEquals(testUser.size(), 5);
				system.assertEquals(testLog.size(), 5);
				system.assertEquals(lcon.logRows.size(), 5);
				system.assertEquals(lcon.logRows[0].logName, testLog[0].SummaryName__c);
			}
		Test.stopTest();
	}

	static testMethod void sortRowsTest02() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_LOG_NAME);

		Test.startTest();
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
			insert testAgency;

			List<Contact> testAgents = new List<Contact>();
			for (Integer i = 0, j = 5; i < j; i++) {
				testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
			}
			insert testAgents;

			List<User> testUser = new List<User>();
			String userName;
			Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
			for (Contact testAgent : testAgents) {
				userName = testAgent.LastName + '@terrasky.ingtesting';
				User src = new User(
						  Lastname = testAgent.LastName
						, Username = userName
						, Email = userName
						, ProfileId = p.Id
						, Alias = testAgent.LastName.left(8)
						, TimeZoneSidKey = UserInfo.getTimeZone().getID()
						, LocaleSidKey = UserInfo.getLocale()
						, EmailEncodingKey = 'UTF-8'
						, LanguageLocaleKey = UserInfo.getLanguage()
						, ContactId = testAgent.Id
				);
				testUser.add(src);
			}
			insert testUser;

			List<E_Log__c> testLog = new List<E_Log__c>();
			for (User user : testUser) {
				testLog.add(new E_Log__c(Name = 'testLog_' + user.Lastname,SummaryName__c ='testLog_' + testUser[0].Lastname, AccessUser__c = user.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage', ActionType__c = 'ダウンロード',isDisplay__c = true));
			}
			insert testLog;
			pr.getParameters().put('id', testUser[0].id);

			system.runAs(actUser) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);

				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.sortRows();

				system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_LOG_NAME);
				system.assertEquals(lcon.sortIsAsc, true);
				system.assertEquals(lcon.logRows[0].logName, testLog[0].SummaryName__c);
				pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_LOG_TYPE);
				system.assertEquals(lcon.logRows[0].logType, testLog[0].ActionType__c);
			}
		Test.stopTest();
	}

	static testMethod void sortRowsTest03() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_LOG_TYPE);

		Test.startTest();
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
			insert testAgency;

			List<Contact> testAgents = new List<Contact>();
			for (Integer i = 0, j = 5; i < j; i++) {
				testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
			}
			insert testAgents;

			List<User> testUser = new List<User>();
			String userName;
			Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
			for (Contact testAgent : testAgents) {
				userName = testAgent.LastName + '@terrasky.ingtesting';
				User src = new User(
						  Lastname = testAgent.LastName
						, Username = userName
						, Email = userName
						, ProfileId = p.Id
						, Alias = testAgent.LastName.left(8)
						, TimeZoneSidKey = UserInfo.getTimeZone().getID()
						, LocaleSidKey = UserInfo.getLocale()
						, EmailEncodingKey = 'UTF-8'
						, LanguageLocaleKey = UserInfo.getLanguage()
						, ContactId = testAgent.Id
				);
				testUser.add(src);
			}
			insert testUser;

			List<E_Log__c> testLog = new List<E_Log__c>();
			for (User user : testUser) {
				testLog.add(new E_Log__c(Name = 'testLog_' + user.Lastname, AccessUser__c = user.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage', ActionType__c = 'ダウンロード'));
			}
			insert testLog;

			system.runAs(actUser) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);

				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.sortRows();

				system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_LOG_TYPE);
			}
		Test.stopTest();
	}

	static testMethod void sortRowsTest04() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_ACCESS_DATETIME);

		Test.startTest();
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
			insert testAgency;

			List<Contact> testAgents = new List<Contact>();
			for (Integer i = 0, j = 5; i < j; i++) {
				testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
			}
			insert testAgents;

			List<User> testUser = new List<User>();
			String userName;
			Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
			for (Contact testAgent : testAgents) {
				userName = testAgent.LastName + '@terrasky.ingtesting';
				User src = new User(
						  Lastname = testAgent.LastName
						, Username = userName
						, Email = userName
						, ProfileId = p.Id
						, Alias = testAgent.LastName.left(8)
						, TimeZoneSidKey = UserInfo.getTimeZone().getID()
						, LocaleSidKey = UserInfo.getLocale()
						, EmailEncodingKey = 'UTF-8'
						, LanguageLocaleKey = UserInfo.getLanguage()
						, ContactId = testAgent.Id
				);
				testUser.add(src);
			}
			insert testUser;

			List<E_Log__c> testLog = new List<E_Log__c>();
			for (User user : testUser) {
				testLog.add(new E_Log__c(Name = 'testLog_' + user.Lastname, AccessUser__c = user.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage', ActionType__c = 'ダウンロード'));
			}
			insert testLog;

			system.runAs(actUser) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);

				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.sortRows();

				system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_ACCESS_DATETIME);
			}
		Test.stopTest();
	}

	static testMethod void sortRowsTest05() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS_LogView;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_ACCESS_DATETIME);

		Test.startTest();
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
			insert testAgency;

			List<Contact> testAgents = new List<Contact>();
			for (Integer i = 0, j = 5; i < j; i++) {
				testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
			}
			insert testAgents;

			List<User> testUser = new List<User>();
			String userName;
			Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
			for (Contact testAgent : testAgents) {
				userName = testAgent.LastName + '@terrasky.ingtesting';
				User src = new User(
						  Lastname = testAgent.LastName
						, Username = userName
						, Email = userName
						, ProfileId = p.Id
						, Alias = testAgent.LastName.left(8)
						, TimeZoneSidKey = UserInfo.getTimeZone().getID()
						, LocaleSidKey = UserInfo.getLocale()
						, EmailEncodingKey = 'UTF-8'
						, LanguageLocaleKey = UserInfo.getLanguage()
						, ContactId = testAgent.Id
				);
				testUser.add(src);
			}
			insert testUser;

			List<E_Log__c> testLog = new List<E_Log__c>();
			for (User user : testUser) {
				testLog.add(new E_Log__c(Name = 'testLog_' + user.Lastname, AccessUser__c = user.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage', ActionType__c = 'ダウンロード'));
			}
			insert testLog;

			system.runAs(actUser) {
				// 権限セットを付与
				TestI_TestUtil.createBasePermissions(actUser.Id);

				I_LogViewController lcon = new I_LogViewController();
				lcon.pageAction();
				lcon.sortRows();
				lcon.sortIsAsc = false;

				system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_ACCESS_DATETIME);
			}
		Test.stopTest();
	}
	//MR列廃止のためテスト除外
	//static testMethod void sortRowsTest06() {
	//	// 実行ユーザ作成（MR）
	//	User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
	//	TestI_TestUtil.createIDCPF(true, actUser.Id, null);

	//	// ページ表示
	//	PageReference pr = Page.IRIS_LogView;
	//	Test.setCurrentPage(pr);
	//	pr.getParameters().put(I_LogViewController.URL_PARAM_SORT_TYPE, I_LogViewController.SORT_TYPE_MR);

	//	Test.startTest();
	//		// テスト用レコード作成
	//		Account testParentAgency = new Account(Name = 'testParentAgency');
	//		insert testParentAgency;

	//		Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
	//		insert testAgency;

	//		List<Contact> testAgents = new List<Contact>();
	//		for (Integer i = 0, j = 5; i < j; i++) {
	//			testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id));
	//		}
	//		insert testAgents;

	//		List<User> testUser = new List<User>();
	//		String userName;
	//		Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
	//		for (Contact testAgent : testAgents) {
	//			userName = testAgent.LastName + '@terrasky.ingtesting';
	//			User src = new User(
	//					  Lastname = testAgent.LastName
	//					, Username = userName
	//					, Email = userName
	//					, ProfileId = p.Id
	//					, Alias = testAgent.LastName.left(8)
	//					, TimeZoneSidKey = UserInfo.getTimeZone().getID()
	//					, LocaleSidKey = UserInfo.getLocale()
	//					, EmailEncodingKey = 'UTF-8'
	//					, LanguageLocaleKey = UserInfo.getLanguage()
	//					, ContactId = testAgent.Id
	//			);
	//			testUser.add(src);
	//		}
	//		insert testUser;

	//		List<E_Log__c> testLog = new List<E_Log__c>();
	//		for (User user : testUser) {
	//			testLog.add(new E_Log__c(Name = 'testLog_' + user.Lastname, AccessUser__c = user.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage', ActionType__c = 'ダウンロード'));
	//		}
	//		insert testLog;

	//		system.runAs(actUser) {
	//			// 権限セットを付与
	//			TestI_TestUtil.createBasePermissions(actUser.Id);

	//			I_LogViewController lcon = new I_LogViewController();
	//			lcon.pageAction();
	//			lcon.sortRows();
	//			lcon.sortIsAsc = false;

	//			system.assertEquals(lcon.sortType, I_LogViewController.SORT_TYPE_MR);
	//		}
	//	Test.stopTest();
	//}
}