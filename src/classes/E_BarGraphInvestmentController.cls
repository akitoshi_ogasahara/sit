public with sharing class E_BarGraphInvestmentController extends E_AbstractBarGraph {

    
    //ボタンラベル
    private static final String BUTTON_LABEL_HALF_YEAR = '半年';
    private static final String BUTTON_LABEL_2_YEAR = '2年';
    private static final String BUTTON_LABEL_ALL = '初回買付日以来';
    
    //どのボタンが押されたか？
    private String period;
    
    //グラフ生成
    public void createGraph() {
        
        //グラフ表示データ作成
        graphRecs = new List<GraphData>();
        
        //レコード取得条件設定

        //取得開始日計算
        String startDate = null;
        if(period == BUTTON_LABEL_HALF_YEAR){
            //startDate = System.now().addMonths(-6).format('yyyyMMdd');
            List<E_ITBPF__c> recs = [select Id,ZPRMSDTE__c from E_ITBPF__c where E_ITHPF__c = :parentId and FLAG01__c = true order by ZPRMSDTE__c desc];
            if(recs.isEmpty()){
				return;
			}
			Date enddate = E_Util.stringToDate(recs.get(0).ZPRMSDTE__c);
			if (enddate == null) {
				return;
			}
			startDate = E_Util.date2YYYYMMDD(enddate.addMonths(-6));

        } else if(period == BUTTON_LABEL_2_YEAR){
            //startDate = System.now().addYears(-2).format('yyyyMMdd');
            List<E_ITBPF__c> recs = [select Id,ZPRMSDTE__c from E_ITBPF__c where E_ITHPF__c = :parentId and FLAG02__c = true order by ZPRMSDTE__c desc];
            if(recs.isEmpty()){
				return;
			}
			Date enddate = E_Util.stringToDate(recs.get(0).ZPRMSDTE__c);
			if (enddate == null) {
				return;
			}
			startDate = E_Util.date2YYYYMMDD(enddate.addYears(-2).addMonths(+1));
        }
        //String endDate = System.now().format('yyyyMMdd');
        
        //String endDate = rec.ZPRMSDTE__c;
        //String endDate = dateToStrFormat(Date.today());

        //Weekly・Monthlyフラグ設定
        Boolean weekly = (period == BUTTON_LABEL_HALF_YEAR);
        Boolean monthly = (period != BUTTON_LABEL_HALF_YEAR);
        
        //レコード取得
        String query = '';
        query += ' select Id ';
        query += ' ,ZPRMSDTE__c ';
        query += ' ,ZINVAMT__c ';
        query += ' ,ZCURRPRC__c ';
        query += ' from E_ITBPF__c ';
        
        List<String> strWhere = new List<String>();
		strWhere.add(' E_ITHPF__c = :parentId ');
        //strWhere.add(' ZPRMSDTE__c <= :endDate ');
        if(String.isNotBlank(startDate)){
            strWhere.add(' ZPRMSDTE__c >= :startDate ');
        }
        if(weekly){
            strWhere.add(' FLAG01__c = true ');
        }
        if(monthly){
            strWhere.add(' FLAG02__c = true ');
        }
        
        if(!strWhere.isEmpty()){
           query += ' where ';
            query += String.join(strWhere, ' and ');
        }        
        query += ' order by ZPRMSDTE__c asc ';
        
        List<E_ITBPF__c> itbpfList = Database.query(query);
        
        //グラフ表示データ作成
        for(E_ITBPF__c itbpf : itbpfList){
            String year = itbpf.ZPRMSDTE__c.substring(0, 4);
            String month = itbpf.ZPRMSDTE__c.substring(4, 6);
            String day = itbpf.ZPRMSDTE__c.substring(6, 8);
            String promiseDate = year + '/' + month + '/' + day;
            GraphData graphData = new GraphData(promiseDate, itbpf.ZCURRPRC__c, itbpf.ZINVAMT__c, null, null);
            graphRecs.add(graphData);
        }
    }
    
    //半年ボタンを押下
    public PageReference doHalfYearButton(){
        period = BUTTON_LABEL_HALF_YEAR;
        isSelectedPeriod = true;
        createGraph();
        return null;
    }
    
    //2年ボタンを押下
    public PageReference do2YearButton(){
        period = BUTTON_LABEL_2_YEAR;
        isSelectedPeriod = true;
        createGraph();
        return null;
    }
    
    //以来ボタンを押下
    public PageReference doAllButton(){
        period = BUTTON_LABEL_ALL;
        isSelectedPeriod = true;
        createGraph();
        return null;
    }
}