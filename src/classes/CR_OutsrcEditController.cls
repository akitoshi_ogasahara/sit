global with sharing class CR_OutsrcEditController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CR_Outsrc__c record {get{return (CR_Outsrc__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public String recordTypeRecordsJSON_CR_Outsrc_c {get; private set;}
	public String defaultRecordTypeId_CR_Outsrc_c {get; private set;}
	public String metadataJSON_CR_Outsrc_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm01_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm02_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm03_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm04_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm05_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm06_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm06_1_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm06_2_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm06_3_c {get; private set;}
	public String picklistValuesJSON_CR_Outsrc_c_Confirm06_4_c {get; private set;}
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public CR_OutsrcEditExtender getExtender() {return (CR_OutsrcEditExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public CR_OutsrcEditController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','upload_outsrc');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','キャンセル');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!NOT(ISBLANK(Extender.urlForCancel))}');
        tmpPropMap.put('p_value','{!Extender.urlForCancel}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2558');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2558',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.urlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2559');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2559',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2532');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2532',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_targetClassName','nnDatePicker');
		tmpPropMap.put('Component__Width','20');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component2557');
		tmpPropMap.put('Component__Name','E_DatePicker');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2557',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = CR_Outsrc__c.fields.Status__c;
		f = CR_Outsrc__c.fields.Name;
		f = CR_Outsrc__c.fields.Representative__c;
		f = CR_Outsrc__c.fields.Address__c;
		f = CR_Outsrc__c.fields.Business__c;
		f = CR_Outsrc__c.fields.OutsrcFrom__c;
		f = CR_Outsrc__c.fields.Confirm01__c;
		f = CR_Outsrc__c.fields.Confirm02__c;
		f = CR_Outsrc__c.fields.Confirm03__c;
		f = CR_Outsrc__c.fields.ReConsignor__c;
		f = CR_Outsrc__c.fields.Confirm04__c;
		f = CR_Outsrc__c.fields.Confirm05__c;
		f = CR_Outsrc__c.fields.Confirm06__c;
		f = CR_Outsrc__c.fields.SharedInfo__c;
		f = CR_Outsrc__c.fields.Confirm06_1__c;
		f = CR_Outsrc__c.fields.Confirm06_2__c;
		f = CR_Outsrc__c.fields.Confirm06_3__c;
		f = CR_Outsrc__c.fields.Confirm06_4__c;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_CR_Outsrc_c = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'CR_Outsrc__c'];
		Map<Id, RecordType> recordTypeMap_CR_Outsrc_c = new Map<Id, RecordType>(recordTypeRecords_CR_Outsrc_c);
		List<RecordType> availableRecordTypes_CR_Outsrc_c = new List<RecordType>();
		recordTypes = SObjectType.CR_Outsrc__c.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_CR_Outsrc_c = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_CR_Outsrc_c.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_CR_Outsrc_c.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_CR_Outsrc_c = System.JSON.serialize(availableRecordTypes_CR_Outsrc_c);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[]}}'),
			recordTypeFullNames(availableRecordTypes_CR_Outsrc_c),
			CR_Outsrc__c.SObjectType
		);
		metadataJSON_CR_Outsrc_c = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_CR_Outsrc_c_Confirm01_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm01__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm02_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm02__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm03_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm03__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm04_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm04__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm05_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm05__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm06_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm06__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm06_1_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm06_1__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm06_2_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm06_2__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm06_3_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm06_3__c.getDescribe(), filterResult));
		picklistValuesJSON_CR_Outsrc_c_Confirm06_4_c = System.JSON.serialize(filterPricklistEntries(CR_Outsrc__c.SObjectType.Confirm06_4__c.getDescribe(), filterResult));
		try {
			mainSObjectType = CR_Outsrc__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('CR_Outsrc__c');
			mainQuery.addField('Name');
			mainQuery.addField('Representative__c');
			mainQuery.addField('Address__c');
			mainQuery.addField('Business__c');
			mainQuery.addField('OutsrcFrom__c');
			mainQuery.addField('Confirm01__c');
			mainQuery.addField('Confirm02__c');
			mainQuery.addField('Confirm03__c');
			mainQuery.addField('ReConsignor__c');
			mainQuery.addField('Confirm04__c');
			mainQuery.addField('Confirm05__c');
			mainQuery.addField('Confirm06__c');
			mainQuery.addField('SharedInfo__c');
			mainQuery.addField('Confirm06_1__c');
			mainQuery.addField('Confirm06_2__c');
			mainQuery.addField('Confirm06_3__c');
			mainQuery.addField('Confirm06_4__c');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('CR_Agency__c');
			mainQuery.addFieldAsOutput('CR_Agency__r.AgencyCode__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_OutsrcEditExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	public String getComponent2293OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm01__c.getSObjectField()
		));
		}
	public String getComponent2296OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm02__c.getSObjectField()
		));
		}
	public String getComponent2299OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm03__c.getSObjectField()
		));
		}
	public String getComponent2305OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm04__c.getSObjectField()
		));
		}
	public String getComponent2308OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm05__c.getSObjectField()
		));
		}
	public String getchkpersonalinfoOptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm06__c.getSObjectField()
		));
		}
	public String getComponent2317OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm06_1__c.getSObjectField()
		));
		}
	public String getComponent2320OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm06_2__c.getSObjectField()
		));
		}
	public String getComponent2323OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm06_3__c.getSObjectField()
		));
		}
	public String getComponent2326OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Outsrc__c.getSObjectType(),
		SObjectType.CR_Outsrc__c.fields.Confirm06_4__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}