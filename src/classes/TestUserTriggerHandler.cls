@isTest
private class TestUserTriggerHandler {

	/**
	 * 有効ユーザを無効化
	 */
	static testMethod void userIsActiveFalseTest() {
		//ユーザ作成
		User testUser = createUser();

		Test.startTest();
		//ユーザ無効化
		testUser.isActive = false;
		update testUser;
		Test.stopTest();

		User result = [SELECT Id, Email, UserName, isActive FROM User WHERE Id = :testUser.Id];
		System.assertEquals(false, result.isActive);
		System.assertEquals(testUser.Id, result.Id);
		String todayStr = E_Util.getSysDate(null);
		System.assertEquals(todayStr +'_'+ testUser.Email, result.Email);
		System.assertEquals(testUser.UserName +'_'+ todayStr, result.UserName);

		//権限セットが付与されていない
		result = E_UserDao.getUserWithPermissionsByUserId(testUser.Id);
		List<PermissionSetAssignment> psList = result.PermissionSetAssignments;
		System.assertEquals(0, psList.size());
	}

	/**
	 * 無効ユーザを有効化
	 */
	static testMethod void userIsActiveTrueTest() {
		//ユーザ作成
		User testUser = createUser();

		Test.startTest();
		//ユーザ無効化
		testUser.isActive = false;
		update testUser;

		//ユーザ有効化
		User testUs = [SELECT Id, Email, Username, isActive FROM User WHERE Id = :testUser.Id];
		testUs.isActive = true;
		update testUs;
		Test.stopTest();

		User result = [SELECT Id, Email, UserName, isActive FROM User WHERE Id = :testUser.Id];
		System.assertEquals(true, result.isActive);
		System.assertEquals(testUser.Id, result.Id);
		System.assertEquals(testUser.Email, result.Email);
		System.assertEquals(testUser.UserName, result.UserName);

		//権限セットが付与されている
		result = E_UserDao.getUserWithPermissionsByUserId(testUser.Id);
		List<PermissionSetAssignment> psList = result.PermissionSetAssignments;
		System.assertNotEquals(0, psList.size());
	}

	/**
	 * 一般代理店ユーザを作成
	 */
	static testMethod void normalAgentUserInsertTest() {
		//ユーザ作成
		User testUser = createNormalAgentUser(false);

		Test.startTest();
		//ユーザ作成
		insert testUser;
		Test.stopTest();

		User result = [SELECT Id, ProfileId FROM User WHERE Id = :testUser.Id];
		Profile resultProf = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);
//		Profile resultProf = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY);
		System.assertEquals(resultProf.Id, result.ProfileId);
	}

	/**
	 * 一般代理店ユーザをUpdate
	 */
	static testMethod void normalAgentUserUpdateTest() {
		//ユーザ作成
		User testUser = createNormalAgentUser(true);

		Test.startTest();
		//ユーザUpdate
		testUser.Username = 'ts.nn.iris.test02'+ System.Label.E_USERNAME_SUFFIX;
		update testUser;
		Test.stopTest();

		User result = [SELECT Id, ProfileId FROM User WHERE Id = :testUser.Id];
		Profile resultProf = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);
		System.assertEquals(resultProf.Id, result.ProfileId);
	}

	/**
	 * 往生ユーザをUpdate
	 */
	static testMethod void sumiseiUserUpdateTest() {
		//ユーザ作成
		User testUser = createSumiseiUser(true, null);

		Test.startTest();
		//ユーザUpdate
		testUser.Username = 'ts.nn.iris.test02'+ System.Label.E_USERNAME_SUFFIX;
		update testUser;
		Test.stopTest();

		User result = [SELECT Id, ProfileId FROM User WHERE Id = :testUser.Id];
		Profile resultProf = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_Sumisei);
		System.assertEquals(resultProf.Id, result.ProfileId);
	}

	/**
	 * 往生IRISユーザをUpdate
	 */
	static testMethod void sumiseiIRISUserUpdateTest() {
		//ユーザ作成
		User testUser = createSumiseiUser(true, '02');

		Test.startTest();
		//ユーザUpdate
		testUser.Username = 'ts.nn.iris.test02'+ System.Label.E_USERNAME_SUFFIX;
		update testUser;
		Test.stopTest();

		User result = [SELECT Id, ProfileId FROM User WHERE Id = :testUser.Id];
		String profileName;
		if(System.Label.E_SUMISEI_LICENSE == '0'){
			profileName = E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS;
		}else{
			profileName = E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2;
		}
		Profile resultProf = E_ProfileDaoWithout.getRecByName(profileName);
		System.assertEquals(resultProf.Id, result.ProfileId);
	}

	/**
	 * Insert : プロファイルIDがNullの場合
	 * IA
	 */
	static testMethod void changeProfileId_test01(){
		// Contact生成
		Contact con = createContact();

		Test.startTest();
		User u = createCommunityUser(false, con.Id);
		u.ProfileId = null;
		insert u;
		Test.stopTest();

		User result = [Select Profile.Name From User Where Id = :u.Id];
		System.assertEquals(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA, result.Profile.Name);
	}

	/**
	 * Insert : プロファイルIDがNullの場合
	 * Sumisei
	 */
	static testMethod void changeProfileId_test02(){
		// Contact生成
		User u = createSumiseiUser(false, null);

		Test.startTest();
		u.ProfileId = null;
		insert u;
		Test.stopTest();

		User result = [Select Profile.Name From User Where Id = :u.Id];
		System.assertEquals(E_Const.PROFILE_E_PARTNERCOMMUNITY_SUMISEI, result.Profile.Name);
	}

	/**
	 * Insert : プロファイルIDがNullの場合
	 * Sumisei
	 */
	static testMethod void changeProfileId_test03(){
		// Contact生成
		User u = createSumiseiUser(false, '02');

		Test.startTest();
		u.ProfileId = null;
		insert u;
		Test.stopTest();

		User result = [Select Profile.Name From User Where Id = :u.Id];
		String profileName;
		if(System.Label.E_SUMISEI_LICENSE == '0'){
			profileName = E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS;
		}else{
			profileName = E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2;
		}
		System.assertEquals(profileName, result.Profile.Name);
	}

	/**
	 * Update : プロファイルIDが住生IRISの場合
	 * Sumisei
	 */
	static testMethod void changeProfileId_test04(){
		//ユーザ作成
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User u = createSumiseiUser(true, '02');
		User normalUser = createNormalAgentUser(true);

		// Contact生成
		System.runAs(thisUser){
			Id newaccId = [Select AccountId from user where id = :normalUser.Id].accountid;
			Contact con = [select id, Account.Id from contact where id = :u.ContactId];
			con.AccountId = newaccId;
			update con;

			User result1 = [Select Profile.Name From User Where Id = :u.Id];
			String profileName;
			if(System.Label.E_SUMISEI_LICENSE == '0'){
				profileName = E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS;
			}else{
				profileName = E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2;
			}
			System.assertEquals(profileName, result1.Profile.Name);

			Test.startTest();
			u.Username = 'ts.nn.iris.sumisei.test02'+ System.Label.E_USERNAME_SUFFIX;
			u.profileId = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS).Id;
			update u;
			Test.stopTest();
		}

		User result2 = [Select Profile.Name From User Where Id = :u.Id];
		System.assertEquals(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA, result2.Profile.Name);
	}

/* TestData **************************************************************************************************** */
	//ユーザデータ作成
	static User createUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		Profile pro = E_ProfileDaoWithout.getRecByName('E_EmployeeStandard');
		system.runAs(thisUser){
			us = new User(
				  Username = 'ts.nn.iris.test01'+ System.Label.E_USERNAME_SUFFIX
				 ,Alias = 'テストユーザー'
				 ,Email = 'test01@test.com'
				 ,EmailEncodingKey = 'UTF-8'
				 ,LanguageLocaleKey = 'ja'
				 ,LastName = 'テスト'
				 ,LocaleSidKey = 'ja_JP'
				 ,ProfileId = pro.Id
				 ,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
			);
			insert us;

			E_IDCPF__c idcpf = new E_IDCPF__c(
				  User__c = us.Id
				 ,OwnerId = us.Id
				 ,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE
			);
			insert idcpf;
		}
		return us;
	}

	//一般代理店ユーザデータ作成
	static User createNormalAgentUser(Boolean isInsert){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		Profile pro = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY);
		system.runAs(thisUser){
			Account acc = new Account(
				  Name = 'TestAcc'
			);
			insert acc;

			Contact con = new Contact(
				  FirstName = '花子'
				 ,LastName = 'テストテスト'
				 ,E_CL3PF_AGNTNUM__c = 'test01'
				 ,E_CL3PF_KMOFCODE__c = 'test01'
				 ,E_CLTPF_DOB__c = '20161017'
				 ,E_CL3PF_VALIDFLAG__c = '1'
				 ,AccountId = acc.Id
			);
			insert con;

			us = new User(
				  Username = 'ts.nn.iris.ag.test01'+ System.Label.E_USERNAME_SUFFIX
				 ,Alias = 'テストユーザー'
				 ,Email = 'ag.test01@test.com'
				 ,EmailEncodingKey = 'UTF-8'
				 ,LanguageLocaleKey = 'ja'
				 ,LastName = 'テスト'
				 ,LocaleSidKey = 'ja_JP'
				 ,ProfileId = pro.Id
				 ,ContactId = con.Id
				 ,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
			);

			if(isInsert){
				insert us;

				E_IDCPF__c idcpf = new E_IDCPF__c(
					  User__c = us.Id
					 ,OwnerId = us.Id
					 ,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE
				);
				insert idcpf;
			}
		}
		return us;
	}

	//住生ユーザデータ作成
	static User createSumiseiUser(Boolean isInsert, String agType){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		Profile pro = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY);
		system.runAs(thisUser){
			Account acc = new Account(
				  Name = 'TestAcc'
				 ,E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI
			);
			insert acc;

			Contact con = new Contact(
				  FirstName = '花子'
				 ,LastName = 'テストテスト'
				 ,E_CL3PF_AGNTNUM__c = 'test02'
				 ,E_CL3PF_KMOFCODE__c = 'test02'
				 ,E_CLTPF_DOB__c = '20161017'
				 ,E_CL3PF_VALIDFLAG__c = '1'
				 ,E_CL3PF_AGTYPE__c = agType
				 ,AccountId = acc.Id
			);
			insert con;

			us = new User(
				  Username = 'ts.nn.sumisei.test01'+ System.Label.E_USERNAME_SUFFIX
				 ,Alias = 'テストユーザー'
				 ,Email = 'sumisei.test01@test.com'
				 ,EmailEncodingKey = 'UTF-8'
				 ,LanguageLocaleKey = 'ja'
				 ,LastName = 'テスト'
				 ,LocaleSidKey = 'ja_JP'
				 ,ProfileId = pro.Id
				 ,ContactId = con.Id
				 ,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
			);

			if(isInsert){
				insert us;

				E_IDCPF__c idcpf = new E_IDCPF__c(
					  User__c = us.Id
					 ,OwnerId = us.Id
					 ,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE
				);
				insert idcpf;
			}
		}
		return us;
	}

	/**
	 * Contact生成
	 */
	static Contact createContact(){
		Contact con;

		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Account acc = new Account(
				  Name = 'TestAcc'
			);
			insert acc;

			con = new Contact(
				  FirstName = '花子'
				 ,LastName = 'テストテスト'
				 ,E_CL3PF_AGNTNUM__c = 'test01'
				 ,E_CL3PF_KMOFCODE__c = 'test01'
				 ,E_CLTPF_DOB__c = '20161017'
				 ,E_CL3PF_VALIDFLAG__c = '1'
				 ,AccountId = acc.Id
			);
			insert con;
		}
		return con;
	}

	/**
	 * User生成
	 */
	static User createCommunityUser(Boolean isInsert, Id conId){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		Profile pro = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY);

		us = new User(
			  Username = 'ts.nn.iris.test01'+ System.Label.E_USERNAME_SUFFIX
			 ,Alias = 'テストユーザー'
			 ,Email = 'test01@test.com'
			 ,EmailEncodingKey = 'UTF-8'
			 ,LanguageLocaleKey = 'ja'
			 ,LastName = 'テスト'
			 ,LocaleSidKey = 'ja_JP'
			 ,ProfileId = pro.Id
			 ,ContactId = conId
			 ,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
		);

		if(isInsert){
			insert us;
		}
		return us;
	}
}