@isTest(SeeAllData=false)
public class TestE_PieGraphPolicyController {
    private static Map<ID, String> updateDataMap;
    
    private static testmethod void recordTypeNameNull(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = null;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.getUpDataList();
        piePolicy.getUpColorSet();
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDRATIO;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_ENTRY;
        piePolicy.createGraph();
    }

    private static testmethod void graphPolicyFundRatioEntry(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDRATIO;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_ENTRY;
        piePolicy.createGraph();
    }

    private static testmethod void graphPolicyFundRatioConfirm(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDRATIO;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_CONFIRM;
        piePolicy.createGraph();
    }
    
    private static testmethod void graphPolicyFundRatioComplete(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDRATIO;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_COMPLETE;
        piePolicy.createGraph();
    }
    
    private static testmethod void graphPolicyFundTransferEntry_coli(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDTRANSFER;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_ENTRY;
        piePolicy.createGraph();
    }
    
    private static testmethod void graphPolicyFundTransferEntry_spva(){
        string rectype = E_Const.POLICY_RECORDTYPE_SPVA;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDTRANSFER;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_ENTRY;
        piePolicy.createGraph();
    }

    
    private static testmethod void graphPolicyFundTransferoConfirm_coli(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDTRANSFER;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_CONFIRM;
        piePolicy.createGraph();
    }
    
    private static testmethod void graphPolicyFundTransferoConfirm_spva(){
        string rectype = E_Const.POLICY_RECORDTYPE_SPVA;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDTRANSFER;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_CONFIRM;
        piePolicy.createGraph();
    }
    
    
    private static testmethod void graphPolicyFundTransferComplete_coli(){
        string rectype = E_Const.POLICY_RECORDTYPE_COLI;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDTRANSFER;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_COMPLETE;
        piePolicy.createGraph();
    }
    
    private static testmethod void graphPolicyFundTransferComplete_spva(){
        string rectype = E_Const.POLICY_RECORDTYPE_SPVA;
        createDataCommon(recType,false);
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_FUNDTRANSFER;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_COMPLETE;
        piePolicy.createGraph();
    }
    
    private static testmethod void graphPolicyPortfolioEntry_spva(){
        string rectype = E_Const.POLICY_RECORDTYPE_SPVA;
        createDataCommon(recType,false);
        updateDataMap = new MAP<ID,String>();
        for(Integer i = 0; i < 4; i++){
            updateDataMap.put(esvfpfList[i].id, esvfpfList[i].InputZINVPERC__c);
        }
        E_PieGraphPolicyController piePolicy = new E_PieGraphPolicyController();
        piePolicy.recordTypeName = rectype;
        piePolicy.updateData = JSON.serialize(updateDataMap);
        piePolicy.policyId = epolicy.ID;
        piePolicy.executekind = E_CONST.GRAPH_POLICY_PORTFOLIO;
        piePolicy.executeScreen = E_const.GRAPH_POLICY_ENTRY;
        piePolicy.createGraph();
    }
    
    //test data */
    private static User user;
    private static E_IDCPF__c idcpf;
    private static Account account;
    private static Contact contact;
    private static RecordType recordType; 
    private static E_Policy__c epolicy;             // E_Policy_c(保険契約ヘッダ)
    private static List<E_SVCPF__c> esvcpfList;     // E_SVCPF__c(特別勘定)
    private static List<E_SVFPF__c> esvfpfList;     // E_SVFPF__c(変額保険ファンド)
    private static List<E_MessageMaster__c> msgList;
    private static List<E_SFGPF__c> esfgpfList;
    
    private static void createDataCommon(String recType, Boolean isNullZGRPFND){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            //User
            Profile profile = [Select Id, Name From Profile Where Name = 'システム管理者' Limit 1];
            user = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
            );
            insert user;
            user = TestE_TestUtil.createUser(true, 'testuser001', 'システム管理者');
            
            idcpf = new E_IDCPF__c(
                FLAG01__c = '1',
                FLAG02__c = '1',
                TRCDE01__c = 'TD06',
                TRCDE02__c = 'TD06',
                User__c = user.Id,
                ZSTATUS02__c = '1',
                ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
            );
            insert idcpf;
            idcpf = TestE_TestUtil.createIDCPF(false,user.Id);
            /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
*※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
*※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
*※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
*/
            idcpf.FLAG01__c = '1';
            idcpf.FLAG02__c = '1';
            idcpf.FLAG03__c = '1';
            idcpf.FLAG04__c = '1';
            idcpf.FLAG05__c = '1';
            idcpf.FLAG06__c = '1';        
            idcpf.TRCDE01__c = 'TD05';
            insert idcpf;
            
            // Account
            account = new Account(Name = 'testAccount');
            insert account;
            
            // Contact
            contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id);
            insert contact;
            
            // レコードタイプ
            recordType = [Select Id From RecordType Where DeveloperName = :recType LIMIT 1];
            
            // E_FundMaster__c(ファンド群マスタ)
            E_FundMaster__c fundMaster = new E_FundMaster__c(
                ZEQTYLMT__c = 50
            );
            insert fundMaster;
            
            // E_Policy_c(保険契約ヘッダ)
            epolicy = new E_Policy__c(
                COMM_ZTFRDCF__c = true,
                SPVA_ZTFRDCFA__c = true,
                COMM_CLNTNUM__c = '顧客番号0001',   // 顧客番号
                Contractor__c = contact.Id,         // 契約者
                COMM_CHDRNUM__c = '証券番号0001',   // 証券番号
                SPVA_ZGRPFND01__c = '販売名称01',       // 販売名称01
                SPVA_ZGRPFND02__c = '販売名称02',       // 販売名称02
                SPVA_ZGRPFND03__c = '販売名称03',       // 販売名称03
                SPVA_ZTFRCNT__c = 1,                // 積立金移転回数
                RecordTypeId = recordType.Id,       // レコードタイプ
                E_FundMaster__c = fundMaster.Id,     // ファンド群マスタ
                SPVA_ZANNFROM__c = '',
                dataSyncDate__c = Datetime.now()
            );
            if(isNullZGRPFND){
                epolicy.SPVA_ZGRPFND01__c = '';
                epolicy.SPVA_ZGRPFND02__c = '';
                epolicy.SPVA_ZGRPFND03__c = '';
            }
            insert epolicy;
            
            // E_SVCPF__c(特別勘定)
            String zcatflag;
            if(recType == E_Const.POLICY_RECORDTYPE_COLI){
                zcatflag = E_Const.FUND_KIND_COLI;
                
            }else{
                zcatflag = E_Const.FUND_KIND_SPVA;
            }
            
            esvcpfList = new List<E_SVCPF__c>();
            for(Integer i = 0; i < 4; i++){
                esvcpfList.add(new E_SVCPF__c(
                    FLAG01__c = true,                       // FUND表示フラグ
                    ZEFUNDCD__c = 'V00' + String.valueOf(i),// ファンドコード
                    ZCATFLAG__c = zcatflag,                 // ファンド区分
                    ZFUNDNAM01__c = 'ファンド名101',           // ファンド名０１
                    ZFUNDNAM02__c = 'ファンド名102',           // ファンド名０２
                    ZFUNDNAM03__c = 'ファンド名103',           // ファンド名０３
                    ZFUNDNAM04__c = 'ファンド名104',           // ファンド名０４
                    ZEQTYRTO__c = 80,                       // 株式割合
                    CURRFROM__c = '20141201'                // 設定日
                ));	
            }
            insert esvcpfList;
            
            // E_SFGPF__c(選択可能ファンド)
            esfgpfList = new List<E_SFGPF__c>();
            for(E_SVCPF__c esvcpf : esvcpfList){
                esfgpfList.add(new E_SFGPF__c(
                    FundMaster__c = fundMaster.Id,      // ファンド群マスタ
                    E_SVCPF__c = esvcpf.Id       // 特別勘定
                ));
            }
            insert esfgpfList;
            
            
            // E_SVFPF__c(変額保険ファンド)
            esvfpfList = new List<E_SVFPF__c>();
            esvfpfList.add(new E_SVFPF__c(
                E_Policy__c = epolicy.Id,           // 保険契約ヘッダー
                E_SVCPF__c = esvcpfList[0].Id,      // 特別勘定
                ZREVAMT__c = 11100,                 // 積立金
                SVFPFCode__c = 'hfund0001',         // 変額保険ファンドコード
                ZHLDPERC__c = 11.1,                  // 保有割合
                InputZINVPERC__c = '25.0',
                ZINVPERC__c = 25.0
            ));
            esvfpfList.add(new E_SVFPF__c(
                E_Policy__c = epolicy.Id,           // 保険契約ヘッダー
                E_SVCPF__c = esvcpfList[1].Id,      // 特別勘定
                ZREVAMT__c = 22200,                 // 積立金
                SVFPFCode__c = 'hfund0002',         // 変額保険ファンドコード
                ZHLDPERC__c = 22.2,                  // 保有割合
                InputZINVPERC__c = '25.0',
                ZINVPERC__c = 25.0
            ));
            esvfpfList.add(new E_SVFPF__c(
                E_Policy__c = epolicy.Id,           // 保険契約ヘッダー
                E_SVCPF__c = esvcpfList[2].Id,      // 特別勘定
                ZREVAMT__c = 33300,                 // 積立金
                SVFPFCode__c = 'hfund0003',         // 変額保険ファンドコード
                ZHLDPERC__c = 33.3,                  // 保有割合
                InputZINVPERC__c = '25.0',
                ZINVPERC__c = 25.0
            ));
            esvfpfList.add(new E_SVFPF__c(
                E_Policy__c = epolicy.Id,           // 保険契約ヘッダー
                E_SVCPF__c = esvcpfList[3].Id,      // 特別勘定
                ZREVAMT__c = 44400,                 // 積立金
                SVFPFCode__c = 'hfund0004',         // 変額保険ファンドコード
                ZHLDPERC__c = 60.6,                  // 保有割合
                InputZINVPERC__c = '25.0',
                ZINVPERC__c = 25.0
            ));
            insert esvfpfList;    
            
            // E_MessageMaster__c
            RecordType rtype = [Select Id From RecordType Where DeveloperName = 'MSG_DISCLAIMER' Limit 1];
            msgList = new List<E_MessageMaster__c>();
            for(Integer i = 0; i < 6; i++){
                // COLI
                msgList.add(new E_MessageMaster__c(
                    Name = 'COLIErr' + String.valueOf(i),
                    RecordTypeId = rtype.Id,
                    Type__c = 'メッセージ',
                    Key__c = 'FEC|00' + String.valueOf(i),
                    Value__c = 'FEC|00' + String.valueOf(i)
                ));
                // SPVA
                msgList.add(new E_MessageMaster__c(
                    Name = 'SPVAErr' + String.valueOf(i),
                    RecordTypeId = rtype.Id,
                    Type__c = 'メッセージ',
                    Key__c = 'FES|00' + String.valueOf(i),
                    Value__c = 'FES|00' + String.valueOf(i)
                ));
                // ディスクレイマー
                msgList.add(new E_MessageMaster__c(
                    Name = 'COLIErr' + String.valueOf(i),
                    RecordTypeId = rtype.Id,
                    Type__c = 'ディスクレイマー',
                    Key__c = 'DIS|00' + String.valueOf(i),
                    Value__c = 'DIS|00' + String.valueOf(i)
                ));
                
            }
            insert msgList;
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
            updateDataMap = new Map<ID, String>();
            for(Integer i = 0; i < 4; i++){
                updateDataMap.put(esvcpfList[i].id, esvfpfList[i].InputZINVPERC__c);
            }

        }        
    }    
}