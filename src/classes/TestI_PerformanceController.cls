@isTest
private class TestI_PerformanceController {

	/**
	 * TypeAのテスト
	 */
	@isTest static void typeATest() {

		// 社内挙積情報の作成
		E_EmployeeSalesResults__c employeeSalesResults = TestE_EmployeeSalesResultsDao.createEmployeeSalesResults();
		insert employeeSalesResults;

		Test.startTest();

		I_PerformanceController performanceController	= new I_PerformanceController();
		performanceController.paramSalesResultsId		= employeeSalesResults.Id;	// 社内挙積情報ID

		// JS用値設定の取得
		performanceController.getJSParameter();
		System.assertNotEquals(0, performanceController.json.length());

		// JS用値設定の取得(リモートアクション)
		String json = I_PerformanceController.getJSParam(performanceController.paramSalesResultsId);
		System.assertNotEquals(0, json.length());

	}

}