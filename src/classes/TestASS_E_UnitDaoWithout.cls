@isTest
private class TestASS_E_UnitDaoWithout {
	//エリア部長で実行
	private static testMethod void getUnitsTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<E_Unit__c> retunits = ASS_E_UnitDaoWithout.getUnits();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retunits.size()>0,true);
		}
	
	}

	private static testMethod void getUnitByIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			E_Unit__c retunits = ASS_E_UnitDaoWithout.getUnitById(unit.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retunits.Id,unit.Id);
		}
	
	}

	private static testMethod void getUnitByNameTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			E_Unit__c retunits = ASS_E_UnitDaoWithout.getUnitByName('テスト1営業部');
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retunits.Id,unit.Id);
		}
	
	}


	//MRで実行
	private static testMethod void getUnitsMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<E_Unit__c> retunits = ASS_E_UnitDaoWithout.getUnits();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retunits.size()>0,true);
		}
	
	}

	private static testMethod void getUnitByIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			E_Unit__c retunits = ASS_E_UnitDaoWithout.getUnitById(unit.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retunits.Id,unit.Id);
		}
	
	}

	private static testMethod void getUnitByNameMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			E_Unit__c retunits = ASS_E_UnitDaoWithout.getUnitByName('テスト1営業部');
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retunits.Id,unit.Id);
		}
	}
}