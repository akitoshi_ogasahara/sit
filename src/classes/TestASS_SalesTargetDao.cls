@isTest
private class TestASS_SalesTargetDao {
	//エリア部長で実行
	private static testMethod void getThisMonthSalesTargetByOffIdSetTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//営業目標
		List<SalesTarget__c> stList = new List<SalesTarget__c>();
		stList.add(new SalesTarget__c(Account__c = offList[0].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3));
		stList.add(new SalesTarget__c(Account__c = offList[1].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3));
		stList.add(new SalesTarget__c(Account__c = offList[2].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3));
		insert stList;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<SalesTarget__c> retStList = ASS_SalesTargetDao.getThisMonthSalesTargetByOffIdSet(officeMap.keySet());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retStList.size(),3);
		}
	
	}

	//MRで実行
	private static testMethod void getThisMonthSalesTargetByOffIdSetMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//営業目標
		List<SalesTarget__c> stList = new List<SalesTarget__c>();
		stList.add(new SalesTarget__c(Account__c = offList[0].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3));
		stList.add(new SalesTarget__c(Account__c = offList[1].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3));
		stList.add(new SalesTarget__c(Account__c = offList[2].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3));
		insert stList;
		
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<SalesTarget__c> retStList = ASS_SalesTargetDao.getThisMonthSalesTargetByOffIdSet(officeMap.keySet());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retStList.size(),3);
		}
	
	}
}