public with sharing class OppBudgetTotalizationController {
    private static final String KUBUN = '_';	// キーの区分記号
    public String inpDate {get; set;}	// 集計年月
    public List<OppTotalBudgetInfo> errOppList {get; set;}	// エラーリスト（該当する「営業部予算」データがない場合）
    public List<CustomObject2__c> errCustomList {get; set;}	// エラーリスト（該当する集計データがない場合）
    public Boolean isErrMsg {get; set;}	// エラー有無フラグ
    public Boolean isOppErr {get; set;}	// 見込案件の未処理有無
    public Boolean isCustomErr {get; set;}	// 営業部予算の未処理有無

	/* コンストラクタ */
    public OppBudgetTotalizationController() {
        inpDate = '';
        isErrMsg = false;
        errOppList = new List<OppTotalBudgetInfo>();
        errCustomList = new List<CustomObject2__c>();
        isOppErr = false;
        isCustomErr = false;
    }

    /*------------------------------------------------------------------------
	* 処理ブロック名      ：営業部予算への反映
	* 戻り値              ：
	* 機能概要            ：集計値を営業部予算へ反映
	*------------------------------------------------------------------------
	*/
    public PageReference procTotalization() {
    	isErrMsg = false;
    	isOppErr = false;
        isCustomErr = false;
    	String query = '';																	// 実行クエリー
    	List<String> listOppKeys = new List<String>();										// 見込案件の集計データのキー
    	Map<String, OppTotalBudgetInfo> mapOpp = new Map<String, OppTotalBudgetInfo>();		// 見込案件の集計データ
    	Map<String, OppTotalBudgetInfo> mapOppErr = new Map<String, OppTotalBudgetInfo>();	// エラーマップ（該当する「営業部予算」データがない場合）
    	Map<String, CustomObject2__c> mapCustomObj = new Map<String, CustomObject2__c>();	// 営業部予算データ
    	Map<String, CustomObject2__c> mapCustomErr = new Map<String, CustomObject2__c>();	// エラーマップ（該当する集計データがない場合）
    	List<CustomObject2__c> upList = new List<CustomObject2__c>();						// 更新専用リスト（営業部予算）

    	try {
    		// 集計条件選択画面にての「集計年月」に対する開始日を取得
    		String startDate = Util.getFirstDate(inpDate);
    		// 集計条件選択画面にての「集計年月」に対する終了日を取得
    		String endDate = Util.getLastDate(inpDate);

    		// 見込案件の集計データ取得
    		query = 'Select Sum(GAPEBudget__c) sum_GAPEBudget, Sum(WAPEBudget__c) sum_WAPEBudget, Area__c, SalesUnit__c, ProductName_TEXT__c, BudgetType__c';
    		query += ' From Opportunity Where RecordType.Name = \'予算\' AND CloseDate >= ' + startDate + ' AND CloseDate <= ' + endDate;
    		query += ' Group by Area__c, SalesUnit__c, ProductName_TEXT__c, BudgetType__c';
            Integer cnt = 0;
    		for (AggregateResult agg : database.query(query)) {
    			// 集計値のキーを設定
    			String key = (String)agg.get('Area__c') + KUBUN + (String)agg.get('SalesUnit__c') + KUBUN + (String)agg.get('ProductName_TEXT__c') + KUBUN + (String)agg.get('BudgetType__c');
    			// 見込案件の集計データを設定
    			OppTotalBudgetInfo info = new OppTotalBudgetInfo(agg);
    			mapOpp.put(key, info);
    			listOppKeys.add(key);
    			cnt++;
    		}
    		// 指定した該当条件の集計データが存在しない場合。
    		if (cnt == 0) {
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '該当する「見込案件」データが存在しません。'));
    			return NULL;
    		}

			// 営業部予算データ取得
    		query = 'Select Id, Area__c, Field5__c, GroupName__c, Type__c, GAPEBudget__c, WAPEBudget__c, SUMGAPEBudget__c, SUMWAPEBudget__c, Field4__c';
    		query += ' From CustomObject2__c Where Field4__c >= ' + startDate + ' AND Field4__c <= ' + endDate;
    		Integer i = 0;
    		for (CustomObject2__c cust : database.query(query)) {
    			// 営業部予算データのキーを設定
    			String key = cust.Area__c + KUBUN + cust.Field5__c + KUBUN + cust.GroupName__c + KUBUN + cust.Type__c;

    			if (mapOpp.containsKey(key)) {
    				// 営業部予算のキーと一致する集計データがある場合、集計データを営業部予算データに設定
    				cust.SUMGAPEBudget__c = mapOpp.get(key).GAPEBudget;
    				cust.SUMWAPEBudget__c = mapOpp.get(key).WAPEBudget;
    				mapCustomObj.put(key, cust);
    			} else {
    				// 該当する見込案件が存在しない場合、エラーマップに設定
    				mapCustomErr.put(key, cust);
    			}
    			i++;
    		}
    		// 指定した該当条件の営業部予算データが存在しない場合。
            if (i == 0) {
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '該当する「営業部予算」データが存在しません。'));
            	return NULL;
            }

            // 集計データに対する営業部予算データが存在しない場合
			for (String strKey : listOppKeys) {
				if (!mapCustomObj.containsKey(strKey)) {
					// 見込案件のみ、存在する集計関連情報を設定。
					mapOppErr.put(strKey, mapOpp.get(strKey));
				}
			}

    		// 営業部予算データを更新する。
			for (CustomObject2__c c : mapCustomObj.values()) {
				upList.add(c);
				// ガバナ制限のため、200件ずつ更新。
				if (upList.size() == 200) {
					database.update(upList, true);
					upList.clear();
				}
			}
			if (upList.size() > 0) database.update(upList, true);

			// 画面表示用エラー情報を設定
			if (mapOppErr.size() > 0) {
				errOppList = mapOppErr.values();
				isErrMsg = true;
				isOppErr = true;
			}
			if (mapCustomErr.size() > 0) {
				errCustomList = mapCustomErr.values();
				isErrMsg = true;
				isCustomErr = true;
			}

			// 処理結果メッセージを出力
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '「営業部予算」データの' + String.valueof(mapCustomObj.size()) + '件が更新されました。'));
			return NULL;

    	} catch (Exception e) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return NULL;
    	}
    }

	/*------------------------------------------------------------------------
	* 処理ブロック名      ：現在年月の取得
	* 戻り値              ：
	* 機能概要            ：現在年月の取得
	*------------------------------------------------------------------------
	*/
    public String getNowMonth() {
		DateTime d = System.now();
		return d.format('yyyy/MM');
	}

	/* 見込案件のビンクラス */
	public class OppTotalBudgetInfo {
		public String mArea {get; set;}
		public String mSalesUnit {get; set;}
		public String mGroupName {get; set;}
		public String mType {get; set;}
		public Decimal GAPEBudget {get; set;}
		public Decimal WAPEBudget {get; set;}

		public OppTotalBudgetInfo(AggregateResult ar) {
			this.mArea = (String)ar.get('Area__c');
    		this.mSalesUnit = (String)ar.get('SalesUnit__c');
    		this.mGroupName = (String)ar.get('ProductName_TEXT__c');
    		this.mType = (String)ar.get('BudgetType__c');
    		this.GAPEBudget = (Decimal)ar.get('sum_GAPEBudget');
    		this.WAPEBudget = (Decimal)ar.get('sum_WAPEBudget');
		}
	}
}