global with sharing class CR_RuleSubmitController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CR_Rule__c record {get{return (CR_Rule__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public String recordTypeRecordsJSON_CR_Rule_c {get; private set;}
	public String defaultRecordTypeId_CR_Rule_c {get; private set;}
	public String metadataJSON_CR_Rule_c {get; private set;}
	public String picklistValuesJSON_CR_Rule_c_Type_c {get; private set;}
	public String Component1797_hidden { get; set; }
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public CR_RuleSubmitExtender getExtender() {return (CR_RuleSubmitExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public CR_RuleSubmitController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','upload_rule');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.UrlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1882');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1882',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1878');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1878',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_rendered','{!extender.isNew}');
        tmpPropMap.put('p_newFile','{!extender.file}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','30');
		tmpPropMap.put('Component__id','Component1863');
		tmpPropMap.put('Component__Name','EUploadAttachment');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1863',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = CR_Rule__c.fields.Type__c;
		f = CR_Rule__c.fields.OtherTypeName__c;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_CR_Rule_c = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'CR_Rule__c'];
		Map<Id, RecordType> recordTypeMap_CR_Rule_c = new Map<Id, RecordType>(recordTypeRecords_CR_Rule_c);
		List<RecordType> availableRecordTypes_CR_Rule_c = new List<RecordType>();
		recordTypes = SObjectType.CR_Rule__c.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_CR_Rule_c = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_CR_Rule_c.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_CR_Rule_c.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_CR_Rule_c = System.JSON.serialize(availableRecordTypes_CR_Rule_c);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[]}}'),
			recordTypeFullNames(availableRecordTypes_CR_Rule_c),
			CR_Rule__c.SObjectType
		);
		metadataJSON_CR_Rule_c = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_CR_Rule_c_Type_c = System.JSON.serialize(filterPricklistEntries(CR_Rule__c.SObjectType.Type__c.getDescribe(), filterResult));
		try {
			mainSObjectType = CR_Rule__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('CR_Rule__c');
			mainQuery.addField('Type__c');
			mainQuery.addField('OtherTypeName__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('AgencyCode__c');
			mainQuery.addFieldAsOutput('CR_Agency__r.AgencyCode__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_RuleSubmitExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	public String getComponent1797OptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		CR_Rule__c.getSObjectType(),
		SObjectType.CR_Rule__c.fields.Type__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}