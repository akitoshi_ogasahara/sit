global with sharing class E_PortfolioController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_PortfolioExtender getExtender() {return (E_PortfolioExtender)extender;}
	
	
	public dtSVFPUnitPrice dtSVFPUnitPrice {get; private set;}
	public dtCurrentUnitPrice dtCurrentUnitPrice {get; private set;}
	public dtPrtHsry dtPrtHsry {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_PortfolioController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component2170');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2170',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_Portfolio');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2190');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2190',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','グラフ表示');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!record.SpClassification__c != \'ANNUITY\'}');
        tmpPropMap.put('p_value','E_FundTransferTransition?id={!record.id}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2176');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2176',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','申込');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!extender.isZTFRDCF}');
        tmpPropMap.put('p_value','E_FundTransferAgree?id={!record.id}&type={!Extender.isType}&origintype={!Extender.isOriginType}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2187');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2187',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','ご利用方法');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!extender.isZTFRDCF}');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_FundTransfer');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2188');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2188',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','3');
        tmpPropMap.put('executeScreen','entry');
        tmpPropMap.put('updateData','');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','200');
		tmpPropMap.put('Component__id','Component1638');
		tmpPropMap.put('Component__Name','EPieGraphPolicy');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1638',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','ユニットプライスの推移');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!NOT(extender.IsMizuhoBank)}');
        tmpPropMap.put('p_value','{!Extender.urlProductCord}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','121');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2189');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2189',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC01__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','77');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2359');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2359',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC02__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','76');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2360');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2360',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC03__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','74');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2361');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2361',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC04__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','71');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2362');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2362',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC05__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','69');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2363');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2363',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC06__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','68');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2364');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2364',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dtPrtHsry_item.record.ZHLDPERC10__c}');
        tmpPropMap.put('p_NulltoHyphen','true');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2365');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2365',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2174');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2174',tmpPropMap);


		SObjectField f;

		f = E_Policy__c.fields.SPVA_ZGRPNAMES__c;
		f = E_Policy__c.fields.SPVA_ZREVAMT01__c;
		f = E_SVCPF__c.fields.ZFUNDNAMEUNION__c;
		f = E_SVFPF__c.fields.ZUTPURE2__c;
		f = E_SVFPF__c.fields.ZEFUNDCD__c;
		f = E_SVFPF__c.fields.unitPriceDecimal__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT01__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT05__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT09__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT02__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT06__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT10__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT03__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT07__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT11__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT04__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT08__c;
		f = E_Policy__c.fields.SPVA_SINSTAMT12__c;
		f = E_SVFPF__c.fields.ZREVAMT__c;
		f = E_SVFPF__c.fields.ZSEQNO__c;
		f = E_SVPPF__c.fields.ZSEQNO__c;
		f = E_SVPPF__c.fields.ZEFUNDCD__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('SPVA_ZGRPNAMES__c');
			mainQuery.addFieldAsOutput('SPVA_ZREVAMT01__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT01__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT05__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT09__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT02__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT06__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT10__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT03__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT07__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT11__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT04__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT08__c');
			mainQuery.addFieldAsOutput('SPVA_SINSTAMT12__c');
			mainQuery.addFieldAsOutput('SpClassification__c');
			mainQuery.addFieldAsOutput('E_FundMaster__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('RecordType.DeveloperName');
			mainQuery.addFieldAsOutput('COMM_ZINVDCF__c');
			mainQuery.addFieldAsOutput('Contractor__c');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('Contractor__r.Name');
			mainQuery.addFieldAsOutput('Contractor__r.LastName');
			mainQuery.addFieldAsOutput('Contractor__r.E_CL3PF_ZHEADAY__c');
			mainQuery.addFieldAsOutput('Contractor__r.AccountId');
			mainQuery.addFieldAsOutput('Contractor__r.Account.IsPartner');
			mainQuery.addFieldAsOutput('Contractor__r.Account.E_CL1PF_ZHEADAY__c');
			mainQuery.addFieldAsOutput('Contractor__r.Account.Name');
			mainQuery.addFieldAsOutput('Contractor__r.Account.Agency__r.CreatedById');
			mainQuery.addFieldAsOutput('Contractor__r.Account.Agency__r.CreatedBy.ProfileId');
			mainQuery.addFieldAsOutput('Contractor__r.Account.Agency__r.CreatedBy.Username');
			mainQuery.addFieldAsOutput('Contractor__r.Account.Agency__r.CreatedBy.UserRoleId');
			mainQuery.addFieldAsOutput('Contractor__r.FirstName');
			mainQuery.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('ContractorName__c');
			mainQuery.addFieldAsOutput('COMM_CLNTNUM__c');
			mainQuery.addFieldAsOutput('SPVA_ZVWDESC__c');
			mainQuery.addFieldAsOutput('SPVA_ZVWDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZPRDCD__c');
			mainQuery.addFieldAsOutput('COMM_ZTFRDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZTFRDCFA__c');
			mainQuery.addFieldAsOutput('SPVA_ZRICONTDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZRINCDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNSTFLG__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNDSCA01__c');
			mainQuery.addFieldAsOutput('Annuitant__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('Annuitant__r.Name');
			mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
			mainQuery.addFieldAsOutput('DCOLI_ZNAME20A__c');
			mainQuery.addFieldAsOutput('DCOLI_ZNAME40A01__c');
			mainQuery.addFieldAsOutput('DCOLI_ZNAME40A02__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE2__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			dtSVFPUnitPrice = new dtSVFPUnitPrice(new List<E_SVFPF__c>(), new List<dtSVFPUnitPriceItem>(), new List<E_SVFPF__c>(), null);
			listItemHolders.put('dtSVFPUnitPrice', dtSVFPUnitPrice);
			query = new SkyEditor2.Query('E_SVFPF__c');
			query.addFieldAsOutput('E_SVCPF__r.ZFUNDNAMEUNION__c');
			query.addFieldAsOutput('ZUTPURE2__c');
			query.addFieldAsOutput('ZUTPURE__c');
			query.addFieldAsOutput('unitPriceDecimal__c');
			query.addFieldAsOutput('E_SVCPF__r.ZVINDEX__c');
			query.addFieldAsOutput('E_SVCPF__r.CURRFROM__c');
			query.addFieldAsOutput('E_SVCPF__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dtSVFPUnitPrice', 'E_Policy__c');
			dtSVFPUnitPrice.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dtSVFPUnitPrice', query);
			
			dtCurrentUnitPrice = new dtCurrentUnitPrice(new List<E_SVFPF__c>(), new List<dtCurrentUnitPriceItem>(), new List<E_SVFPF__c>(), null);
			listItemHolders.put('dtCurrentUnitPrice', dtCurrentUnitPrice);
			query = new SkyEditor2.Query('E_SVFPF__c');
			query.addFieldAsOutput('ZEFUNDCD__c');
			query.addFieldAsOutput('unitPriceDecimal__c');
			query.addFieldAsOutput('ZUTPURE__c');
			query.addFieldAsOutput('ZUTPURE2__c');
			query.addFieldAsOutput('E_SVCPF__c');
			query.addFieldAsOutput('E_SVCPF__r.CURRTO__c');
			query.addFieldAsOutput('E_SVCPF__r.ZVINDEX__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dtCurrentUnitPrice', 'E_Policy__c');
			dtCurrentUnitPrice.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dtCurrentUnitPrice', query);
			
			dtPrtHsry = new dtPrtHsry(new List<E_SVPPF__c>(), new List<dtPrtHsryItem>(), new List<E_SVPPF__c>(), null);
			listItemHolders.put('dtPrtHsry', dtPrtHsry);
			query = new SkyEditor2.Query('E_SVPPF__c');
			query.addFieldAsOutput('E_SVCPF__r.ZFUNDNAMEUNION__c');
			query.addFieldAsOutput('ZSEQNO__c');
			query.addFieldAsOutput('ZHLDPERC07__c');
			query.addFieldAsOutput('ZHLDPERC08__c');
			query.addFieldAsOutput('ZHLDPERC09__c');
			query.addFieldAsOutput('ZSTDDATE07__c');
			query.addFieldAsOutput('ZSTDDATE08__c');
			query.addFieldAsOutput('ZSTDDATE09__c');
			query.addFieldAsOutput('ZREVAMT07__c');
			query.addFieldAsOutput('ZREVAMT09__c');
			query.addFieldAsOutput('ZREVAMT08__c');
			query.addFieldAsOutput('ZREVAMT01__c');
			query.addFieldAsOutput('ZREVAMT02__c');
			query.addFieldAsOutput('ZREVAMT03__c');
			query.addFieldAsOutput('ZREVAMT04__c');
			query.addFieldAsOutput('ZREVAMT05__c');
			query.addFieldAsOutput('ZREVAMT06__c');
			query.addFieldAsOutput('ZREVAMT10__c');
			query.addFieldAsOutput('ZHLDPERC01__c');
			query.addFieldAsOutput('ZHLDPERC02__c');
			query.addFieldAsOutput('ZHLDPERC03__c');
			query.addFieldAsOutput('ZHLDPERC10__c');
			query.addFieldAsOutput('ZHLDPERC06__c');
			query.addFieldAsOutput('ZHLDPERC05__c');
			query.addFieldAsOutput('ZHLDPERC04__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dtPrtHsry', 'E_Policy__c');
			dtPrtHsry.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dtPrtHsry', query);
			
			registRelatedList('E_SVFPFs__r', 'dtSVFPUnitPrice');
			registRelatedList('E_SVFPFs__r', 'dtCurrentUnitPrice');
			registRelatedList('E_SVPPFs__r', 'dtPrtHsry');
			
			SkyEditor2.Query dtSVFPUnitPriceQuery = queryMap.get('dtSVFPUnitPrice');
			dtSVFPUnitPriceQuery.addWhereIfNotFirst('AND');
			dtSVFPUnitPriceQuery.addWhere('( ZREVAMT__c > 0)');
			dtSVFPUnitPriceQuery.addSort('ZSEQNO__c',True,True).addSort('ZEFUNDCD__c',True,True);
			SkyEditor2.Query dtCurrentUnitPriceQuery = queryMap.get('dtCurrentUnitPrice');
			dtCurrentUnitPriceQuery.addSort('ZEFUNDCD__c',True,True);
			SkyEditor2.Query dtPrtHsryQuery = queryMap.get('dtPrtHsry');
			dtPrtHsryQuery.addSort('ZSEQNO__c',True,True).addSort('ZEFUNDCD__c',True,True);
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_PortfolioExtender(this);
			init();
			
			dtSVFPUnitPrice.extender = this.extender;
			dtCurrentUnitPrice.extender = this.extender;
			dtPrtHsry.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class dtSVFPUnitPriceItem extends SkyEditor2.ListItem {
		public E_SVFPF__c record{get; private set;}
		@TestVisible
		dtSVFPUnitPriceItem(dtSVFPUnitPrice holder, E_SVFPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dtSVFPUnitPrice extends SkyEditor2.ListItemHolder {
		public List<dtSVFPUnitPriceItem> items{get; private set;}
		@TestVisible
			dtSVFPUnitPrice(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dtSVFPUnitPriceItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dtSVFPUnitPriceItem(this, (E_SVFPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class dtCurrentUnitPriceItem extends SkyEditor2.ListItem {
		public E_SVFPF__c record{get; private set;}
		@TestVisible
		dtCurrentUnitPriceItem(dtCurrentUnitPrice holder, E_SVFPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dtCurrentUnitPrice extends SkyEditor2.ListItemHolder {
		public List<dtCurrentUnitPriceItem> items{get; private set;}
		@TestVisible
			dtCurrentUnitPrice(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dtCurrentUnitPriceItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dtCurrentUnitPriceItem(this, (E_SVFPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class dtPrtHsryItem extends SkyEditor2.ListItem {
		public E_SVPPF__c record{get; private set;}
		@TestVisible
		dtPrtHsryItem(dtPrtHsry holder, E_SVPPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dtPrtHsry extends SkyEditor2.ListItemHolder {
		public List<dtPrtHsryItem> items{get; private set;}
		@TestVisible
			dtPrtHsry(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dtPrtHsryItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dtPrtHsryItem(this, (E_SVPPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}