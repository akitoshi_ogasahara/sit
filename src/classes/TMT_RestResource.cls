@RestResource(urlMapping='/TMTApp/*')
global with sharing class TMT_RestResource {
	
	/* API Request
	 *	  APIリクエストを定義する
	 */
	global class appStatusRequest{
		public String uuid{get;set;}					//[必須]端末のuuidをセット
		public String version{get;set;}				 //[必須]端末の利用versionをセット
		public String actionType{get;set;}			  //[必須]端末でのアクション名を設定　e.g.)メリット表、メール、初回起動・・・
		public Map<String,String> deviceInfo{get;set;}  //端末情報をKey-Valueで設定 os:iOS10, model:iPad・・・
		public String simulation{get;set;}			  //PXサーバに送信する情報をセット　XML??
		public Boolean logFlg{get;set;}			  //ログ保存フラグ
		public Boolean dispFlg{get;set;}			  //ログ表示フラグ
	}

	/* API Response
	 *	  APIレスポンス定義
	 */
	private static final Map<String,String> STATUS_CODE_NAME = new Map<String,String>{
																	 '0'=>'成功'
																	,'1'=>'PW有効期限切れ'
																	,'2'=>'認可されていないDevice'
																	,'3'=>'旧Version利用'
																	,'8'=>'無効ユーザ'
																	,'9'=>'緊急停止'
																	,'E'=>'その他エラー'
																};
	global class appStatusResponse{
		public String statusCode{get;set;}	  //0:成功  1:PW有効期限切れ 2:認可されていないDevice 3:旧Version利用 9:緊急停止 E：その他エラー
		public String status{get;set;}
		public String message{get;set;}
		public Integer lifetimeMinutes{get;set;}		//Response結果の有効期間（通常は0分　延ばすとログもその間取得不可となる。）
		public String pxServerUrl{get;set;}				//成功時のみ設定される
		public String pxServerId{get;set;}				//成功時のみ設定される
		public String pxServerPw{get;set;}				//成功時のみ設定される
        public String jsonUpdateDate{get;set;}				//成功時のみ設定される
		
        global appStatusResponse(){
			//Constructor	
			this.statusCode = '';
			this.status = '';
			this.message = '';
			this.lifetimeMinutes = 0;
			this.pxServerUrl = '';
			this.pxServerId = '';
			this.pxServerPw = '';
            this.jsonUpdateDate = '';
		}		
		global appStatusResponse(TMT_Config__c config){
			this.statusCode = '';
			this.status = '';
			this.message = '';
			try{
				this.lifetimeMinutes = Integer.valueOf(config.lifetime_min__c);
			}catch(Exception e){
				this.lifetimeMinutes = 0;   //例外時は0を設定
			}
			this.pxServerUrl = '';
			this.pxServerId = '';
			this.pxServerPw = '';
            this.jsonUpdateDate = '';
        }
	}
	
	@HttpPost
	global static appStatusResponse appStatus(appStatusRequest request){
		TMT_AppManager appMgr;
		appStatusResponse response;
		try{
            
            // URI取得。アドレスで処理分岐させる。
            RestRequest req = RestContext.request;
            String[] method_details = req.requestURI.split('/');
    
			appMgr = new TMT_AppManager(UserInfo.getUserId());
			// レスポンス生成
			response = new appStatusResponse(appMgr.config);

			// 無効ユーザ　　>Falseの場合エラー
			if(!appMgr.isActiveUser()){		 
				response.statuscode = '8';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = 'アプリケーションをご利用いただけません。';

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}
			
			// アプリステータス 緊急停止　　>Falseの場合エラー
			if(!appMgr.isActive()){		 //config.isDisabled__c
				response.statuscode = '9';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = appMgr.config.MaintenanceMsg__c;

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}
	
			// 社員でなくて最新Versionチェック　　>Trueの場合エラー
			if(!appMgr.isEmployee() && appMgr.requiredUpdateApp(request.version)){
				response.statuscode = '3';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = appMgr.config.VersionUp_Msg__c + '(ご利用中Version:' + request.version + ')';		//'最新Versionをご利用ください。';

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}
            
            //MRバージョンの場合、最新Versionチェック　　>Trueの場合エラー
            if(appMgr.isEmployee() && appMgr.requiredUpdateMRApp(request.version)){
				response.statuscode = '3';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = appMgr.config.VersionUp_Msg__c + '(ご利用中Version:' + request.version + ')';		//'最新Versionをご利用ください。';

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}
            
            if(appMgr.requiredUpdateApp(request.version)){
				response.statuscode = '3';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = appMgr.config.VersionUp_Msg__c + '(ご利用中Version:' + request.version + ')';		//'最新Versionをご利用ください。';

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}

			// 社員でなくて許可されていないDevice　　>Falseの場合エラー
			if(!appMgr.isEmployee() && !appMgr.isApprovedDevice(request.uuid)){
				response.statuscode = '2';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = appMgr.config.NewDevice_Msg__c;

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}
	
			// PW期限切れ　　>Trueの場合エラー
			if(appMgr.requiredPassWord(request.uuid)){
				response.statuscode = '1';
				response.status = STATUS_CODE_NAME.get(response.statuscode);
				response.message = appMgr.config.ChangePW_Msg__c;		//'PWが変更されています。再度ログインしなおしてください。';

				// ログ出力(エラー系ログ)
				appMgr.saveRequestLog(request, response);
				return response;
			}


			// 成功
			response.statusCode = '0';
			response.status = STATUS_CODE_NAME.get(response.statuscode);
			response.pxServerUrl = appMgr.config.PX_Server_url__c;
			response.pxServerId = appMgr.config.PX_Server_Id__c;
			response.pxServerPw = appMgr.config.PX_Server_Pw__c;
            
            Datetime dt = appMgr.config.JSON_updateDate__c;
            response.jsonUpdateDate = dt.format('yyyy-MM-dd HH:mm');
  
            //
            //  URIによる分岐　　/TMTApp/v2/appStatus の場合、request.dispFlgにアクセスできる
            //
  //          if(method_details.size() > 3){
                if(method_details[2].compareTo('v2') == 0  && method_details[3].compareTo('appStatus') == 0){	
  				    // ログ出力(正常系ログ responseを渡さない)
 				    appMgr.saveRequestLogV2(request, null,request.logFlg,request.dispFlg);
                }
                else{ // v2.appStatus以外
 				    // ログ出力(正常系ログ responseを渡さない)
			    	appMgr.saveRequestLog(request, null);
                }
   //         }
            //
            //  URIによる分岐　　/TMTApp/appStatus の場合、request.dispFlgにアクセスできない。
            //  旧バージョンのアプリの場合はこちらで処理する。
            //
 //           else{
 				// ログ出力(正常系ログ responseを渡さない)
	//			appMgr.saveRequestLog(request, null);
//            }

			return response;

		// 例外発生
		}catch(Exception e){
			if(response==null){
				response = new appStatusResponse();
			}
			response.statusCode = 'E';
			response.status = STATUS_CODE_NAME.get(response.statuscode);
			response.message = e.getMessage() + ' / ' + e.getStackTraceString();

			// ログ出力(エラー系ログ)
			if(appMgr!=null){
				appMgr.saveRequestLog(request, response);
			}
			return response;
		}
	}

	@HttpGet
	global static void doGet(){
        RestRequest req = RestContext.request;
        String method_detail = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

        // path == '/TMTApp/json' の場合
        if(method_detail.compareTo('json') == 0 ){	
            // JSONデータを読み込む
            String jsonStr = TMT_JsonManager.getJson('TMT_ConstantData' );
            
            // 読み込んだデータが空の場合
            if( jsonStr.length() == 0) {
                // 500エラーとする
                RestContext.response.statuscode = 500;
            }
            else{
                // レスポンスへ返却データをセット
               RestContext.response.addHeader('Content-Type', 'application/json');
               RestContext.response.responseBody = Blob.valueOf(jsonStr);   
            }
        }    
	}

}