public with sharing class AGC_ContactHistory_Ctrl {

    //代理店基本情報
    private AGC_DairitenKihonjoho__c kihon;
    //代理店補足情報
    private AGC_DairitenJimushoHosokujoho__c hosoku;
    //コンタクト履歴詳細
    private AGC_ContactRireki__c contactRireki;
    //コンタクト履歴リスト
    private List<AGC_ContactRireki__c> rirekiList;
    //支社マスタ
    private AGC_Shisha_Mst__c shishaMst;
    //支社MRマスタ
    private AGC_ShishaMr_Mst__c shishamrMst;
    
    private AGC_Common_Dao commonDao = new AGC_Common_Dao();
        
    //現在のレコードラベル
    private String currentRecordLabel;

    public ID RirekiId{set;get;}
    public Integer RecordCnt{set;get;}
        
    private Integer CurrentIdx{set;get;}
    private boolean NewRecordFlg{set;get;}
                
    //コンストラクタ
    public AGC_ContactHistory_Ctrl(ApexPages.StandardController controller){
        //リクエストIDを取得する
        ID id = ApexPages.currentPage().getParameters().get('id');

        try{
            //代理店基本情報を取得する。
            kihon = commonDao.getKihonJoho(id);
            //代理店補足情報を取得する。
            hosoku = commonDao.getDairitenHosokujohoBykihonId(kihon.Id);
        }catch(System.QueryException e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '本機能の使用方法が誤っているか、データに不整合が発生しています。問題が解決しない場合、システム管理者までご連絡ください。');
            ApexPages.addMessage(msg);
            return;
        }

        //コンタクト履歴リストを取得する。
        rirekiList = commonDao.getContactRirekiListByKihonId(kihon.Id);

        if(!rirekiList.isEmpty()){
            contactRireki = rirekiList[0];
            currentIdx = 0;
            newRecordFlg = false;
            RecordCnt = rirekiList.size();
        }else{
            contactRireki = createContactRireki(kihon.Id);
            newRecordFlg = true;
            currentIdx = 0;
            RecordCnt = 1;
        }

        try{
            //支社マスタを取得する。
            shishaMst = commonDao.getShishaMstById(kihon.BranchCodeRef__c);
            shishamrMst = commonDao.getShishaMrMstById(kihon.MRCodeRef__c);
        }catch(System.QueryException e){
            //マスタデータが取得できない場合は無視する。
        }
    }

    //代理店基本情報
    public AGC_DairitenKihonjoho__c getKihon(){
        return kihon;
    }

    //補足情報
    public AGC_DairitenJimushoHosokujoho__c getHosoku(){
        return hosoku;
    }

    //コンタクト履歴
    public AGC_ContactRireki__c getContactRireki(){
        return contactRireki;
    }
    
    //コンタクト履歴一覧
    public List<AGC_ContactRireki__c> getRirekiList(){
        return rirekiList;
    }

    //支社マスタ
    public AGC_Shisha_Mst__c getShishaMst(){
        return shishaMst;
    }

    //支社MRマスタ
    public AGC_ShishaMr_Mst__c getShishamrMst(){
        return shishamrMst;
    }
    
    public String getCurrentRecordLabel(){
        if(newRecordFlg){
            newRecordFlg = false;
            return '';
        }else{
            return String.valueOf(currentIdx+1)+'件目/全'+String.valueOf(recordCnt)+'件中';
        }
    }
    
    public Integer getCurrentIdx(){
        return currentIdx;
    }
    
    public Integer getRecordCnt(){
        return recordCnt;
    }
    
    //以下処理ロジック＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
    //ローカルメソッド
    private AGC_ContactRireki__c createContactRireki(Id id){
        contactRireki = new AGC_ContactRireki__c();
        contactRireki.ReceptionData__c = System.now();
        contactRireki.ReceptionStaff__c = UserInfo.getName();
        contactRireki.AGC_DairitenKihonJoho_Ref__c = id;
        return contactRireki;
    }

    //以下はAjaxで使用するメソッド
    public PageReference getRirekiDetail(){
        
        for(Integer i=0 ; i<rirekiList.size();i++){
            if(rirekiList[i].Id == rirekiId){
                currentIdx = i;
                break;
            }
        }
        
        contactRireki = commonDao.getContactRirekiListById(rirekiId);
        return null;
    }
                
    //補足情報が存在しない場合、コンタクト履歴は登録されないので、
    //補足インスタンスは持っている前提。
    public PageReference createNewRecord(){
        createContactRireki(kihon.Id);
        newRecordFlg = true;
        return null;
    }
     
    public PageReference saveRecord(){
        upsert contactRireki;
        rirekiList = commonDao.getContactRirekiListByKihonId(kihon.Id);
        RecordCnt = rirekiList.size();
        return null;
    }

    public PageReference getRirekiBefDetail(){
            
        if(currentIdx == 0){
            return null;
        }
            
        currentIdx = currentIdx -1;
        contactRireki = commonDao.getContactRirekiListById(rirekiList[currentIdx].Id);
        return null;
    }
        
    public PageReference getRirekiAftDetail(){
            
        if(currentIdx >= rirekiList.size()){
            return null;
        }
            
        currentIdx = currentIdx +1;
        contactRireki = commonDao.getContactRirekiListById(rirekiList[currentIdx].Id);
        return null;
    }
                
    public PageReference getRirekiLastDetail(){
                
        if(currentIdx >= rirekiList.size()){
            return null;
        }
                
        currentIdx = rirekiList.size()-1;
        contactRireki = commonDao.getContactRirekiListById(rirekiList[currentIdx].Id);
        return null;
    }
                
    public PageReference getRirekiFirstDetail(){
        if(currentIdx == 0){
            return null;
        }   
        currentIdx = 0;
        contactRireki = commonDao.getContactRirekiListById(rirekiList[currentIdx].Id);
        return null;
    }
}