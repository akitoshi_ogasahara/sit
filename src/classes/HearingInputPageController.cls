/**
* クラス名		:  HearingInputPageController
* クラス概要	:  ヒアリング情報詳細画面(コントローラ)
* @created	:  2014/01/21 Khanh Vo Quoc
* @modified	:  2014/01/24 Hung Nguyen The
*/
public with sharing class HearingInputPageController {
	// ファイナンス情報
	public Finance__c finance{get; set;}
	// 顧客
	public  Customer__c customer{get; set;}
	// IndustryAverage
	private TaxDefinition__c taxDefinition{get; set;}
	// URLパラメータマップ
	private Map<String, String> mapParameter{get; set;}
	// 初期化エラー
	public Boolean isInitError{get; set;}
	
	/* 2015/12/08 PHUONG DINH VU ADD START */
	// タイムアウト回避の詳細
	public string	strFCTimeOut { get; set; }
	/* 2015/12/08 PHUONG DINH VU ADD END */
	
	/**
	* HearingInputPageController
	* ページ初期化処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified : 2014/01/24 Hung Nguyen The
	*/
	public HearingInputPageController(){
		try{
			/* 2015/12/08 PHUONG DINH VU ADD START */
			// タイムアウト回避の詳細
			resetSessionTimeout();
			/* 2015/12/08 PHUONG DINH VU ADD END */
			isInitError = false;
			taxDefinition = new TaxDefinition__c();
			// ファイナンス情報を作成
			finance = new Finance__c();
			// 顧客を作成
			customer = new Customer__c();
			// URLパラメータマップを作成
			mapParameter = new Map<String, String>();
			// URLパラメータマップを取得
			mapParameter = Apexpages.currentPage().getParameters();
			// URLパラメータを取得出来ない
			if(mapParameter.isEmpty()){
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_CustomerCheck));
				isInitError = true;
				return;
			}
			// レコードが取得される場合
			if(!mapParameter.containsKey(CommonConst.STR_PARAMETER_CUID) || String.isBlank(mapParameter.get(CommonConst.STR_PARAMETER_CUID))){
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_CustomerCheck));
				isInitError = true;
				return;
			}
			// 顧客obj.カスタムオブジェクトIDを受け取る
			String strCustomsId = mapParameter.get(CommonConst.STR_PARAMETER_CUID);
			// 顧客を取得
			customer = HearingDAO.getCustomerById(strCustomsId);
			// 顧客を取得出来ない場合
			if(customer == null || String.isBlank(String.valueOf(customer.Id))){
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_CustomerCheck));
				isInitError = true;
				return;
			}
			// ファイナンス情報obj.カスタムオブジェクトID
			String strFiranceId = Apexpages.currentPage().getParameters().get(CommonConst.STR_PARAMETER_ID);
			// ファイナンス情報を取得
			finance = HearingDAO.getFinanceById(strFiranceId);
			// ファイナンス情報を取得出来ない場合
			if(finance == null || String.isBlank(String.valueOf(finance.Id))){
				finance = new Finance__c();
				finance.Term__c = null;
				finance.H_RD__c = 0;
				finance.H_JASGD__c = 0;
				finance.H_LO__c = 0;
				finance.H_DBAC__c = 0;
				finance.H_UG__c = 0;
				finance.H_DBAI__c = 0;
				finance.H_BRM__c = 0;
				finance.H_BRP__c = 0;
				finance.H_BFLEM__c = 0;
				finance.H_BFLET__c = 0;
				finance.H_RE__c = 0;
				finance.H_CRAA__c = 0;
				finance.H_ERB__c = 0;
				finance.H_ORB__c = 0;
			}
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
			isInitError = true;
		}
	}
	/**
	* upsertData
	* 「登録」ボタン処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified : 
	*/
	public Pagereference upsertData(){
		try{
			// 入力チェック
			if(checkInput()){
				return null;
			}
			/* 2014/08/02 Hung Nguyen The Add 更新時は当チェックは行わない。 Start */
			if(String.isBlank(finance.Id)){
				if(checkExist()){
					// システムエラー
					Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_H_Overlap));
					return null;
				}
			}
			// チェック条件. 　カスタム設定取得チェック
			if(checkCustomsetting()){
				return null;
			}
			// URLパラメータcuiidを取得
			String strCustomsId = mapParameter.get(CommonConst.STR_PARAMETER_CUID);
			// 顧客を取得
			Customer__c customerCheck = HearingDAO.getCustomerById(strCustomsId);
			// レコードが取得出来ない場合、エラーを表示する。
			if(customerCheck == null || String.isBlank(String.valueOf(customerCheck.Id))){
				// システムエラー
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_CustomerCheck));
				return null;
			}
			// 以下の条件で業界平均マスタレコードを1件取得する。
			// ・顧客obj.業界　＝業界平均マスタobj.業界名
			IndustryAverage__c industry = HearingDAO.getIndustryAverage(customerCheck.Industry__c);
			// レコードが取得出来ない場合
			if(industry == null || String.isBlank(String.valueOf(industry.Id))){
				// システムエラー
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_ErrorIndustryCheck));
				return null;
			}
			// Set gia tri cho Finace
			// ファイナンス情報.ヒアリング情報フラグ = true
			finance.Hearing__c = true;
			// 業界平均マスタ.ROA（総資産経常利益率）
			finance.IndustryAverage_ROA__c = industry.ROA__c;
			// 業界平均マスタ.ROE(自己資本当期純利益率）
			finance.IndustryAverage_ROE__c = industry.ROE__c;
			// 業界平均マスタ.ROS（売上高当期純利益率）
			finance.IndustryAverage_ROS__c = industry.ROS__c;
			// 業界平均マスタ.インタレスト・カバレッジ・レシオ	
			finance.IndustryAverage_ICR__c = industry.ICR__c;
			// 業界平均マスタ.債務償還年数
			finance.IndustryAverage_DR__c = industry.DR__c;
			// 業界平均マスタ.自己資本比率
			finance.IndustryAverage_CAR__c = industry.CAR__c;
			// 業界平均マスタ.手元流動性比率
			finance.IndustryAverage_LR__c = industry.LR__c;
			// 業界平均マスタ.棚卸資産回転率
			finance.IndustryAverage_ITO__c = industry.ITO__c;
			// 業界平均マスタ.当座比率
			finance.IndustryAverage_QD__c = industry.QD__c;
			// 業界平均マスタ.買入債務回転率
			finance.IndustryAverage_APTO__c = industry.APTO__c;
			// 業界平均マスタ.売上債権回転率
			finance.IndustryAverage_RTO__c = industry.RTO__c;
			// *法人税定義.施行日(TaxDefinition_From__c) = カスタム設定.法人税率定義(From__c)
			finance.TaxDefinition_From__c = taxDefinition.From__c;
			// *法人税定義.必要倍率(TaxDefinition_NRate__c) = カスタム設定.法人税率定義(NRate__c)
			finance.TaxDefinition_NRate__c = taxDefinition.NRate__c;
			// *法人税定義.法人税率(TaxDefinition_TaxRate__c) = カスタム設定.法人税率定義(TaxRate__c)
			finance.TaxDefinition_TaxRate__c = taxDefinition.TaxRate__c;
			if(!mapParameter.containsKey(CommonConst.STR_PARAMETER_ID)){
				finance = finance.clone(false, false, false);
				// ファイナンス情報.顧客 = strCustomsId
				finance.Customer__c = strCustomsId;
				insert finance;
			}else{
				// URLパラメータidを取得
				String strFiranceId = mapParameter.get(CommonConst.STR_PARAMETER_ID);
				if(String.isBlank(strFiranceId)){
					finance = finance.clone(false, false, false);
					// ファイナンス情報.顧客 = strCustomsId
					finance.Customer__c = strCustomsId;
					insert finance;
				}else{
					// ファイナンス情報を取得
					Finance__c financeCheck = HearingDAO.getFinanceById(strFiranceId);
					if(financeCheck == null || String.isBlank(String.valueOf(financeCheck.Id))){
						finance = finance.clone(false, false, false);
						finance.Customer__c = strCustomsId;
						insert finance;
					}else{
						update finance;
					}
				}
			}
			// BS・PL入力画面
			Pagereference HearingOutputPage = Page.HearingOutputPage;
			// Set fiid (ファイナンス情報obj.レコードID)
			HearingOutputPage.getParameters().put(CommonConst.STR_PARAMETER_ID, finance.Id);
			// Set id (ファイナンス情報obj.顧客obj.カスタムオブジェクトID)
			HearingOutputPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, finance.Customer__c);
			HearingOutputPage.setRedirect(true);
			return HearingOutputPage;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
		return null;
	}
	/**
	* checkInput
	* 入力チェック
	* @param 	: なし
	* @return	: 画面入力項目のいずれかに"-(マイナス)の数値が入力されている場合" -> True
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	private Boolean checkInput(){
		Boolean isError = false;
		// 以下の項目がNULLの場合、エラーメッセージを表示する。
		// ・決算期(前期)
		if(String.isBlank(String.valueOf(finance.Term__c))){
			finance.Term__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 予想売上下落率
		if(String.isBlank(String.valueOf(finance.H_RD__c))){
			finance.H_RD__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 連帯保証債務額
		if(String.isBlank(String.valueOf(finance.H_JASGD__c))){
			finance.H_JASGD__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 役員借入金
		if(String.isBlank(String.valueOf(finance.H_LO__c))){
			finance.H_LO__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 死亡保障額(法人契約)
		if(String.isBlank(String.valueOf(finance.H_DBAC__c))){
			finance.H_DBAC__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 含み益
		if(String.isBlank(String.valueOf(finance.H_UG__c))){
			finance.H_UG__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 死亡保障額(個人契約)
		if(String.isBlank(String.valueOf(finance.H_DBAI__c))){
			finance.H_DBAI__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 銀行返済額(月)
		if(String.isBlank(String.valueOf(finance.H_BRM__c))){
			finance.H_BRM__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 銀行返済額　準備額
		if(String.isBlank(String.valueOf(finance.H_BRP__c))){
			finance.H_BRP__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 遺族生活資金(月)
		if(String.isBlank(String.valueOf(finance.H_BFLEM__c))){
			finance.H_BFLEM__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 遺族生活資金(必要期間)
		if(String.isBlank(String.valueOf(finance.H_BFLET__c))){
			finance.H_BFLET__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 現在の社長の報酬/年
		if(String.isBlank(String.valueOf(finance.H_RE__c))){
			finance.H_RE__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 社長の報酬減額可能額/年
		if(String.isBlank(String.valueOf(finance.H_CRAA__c))){
			finance.H_CRAA__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 従業員退職金(予定額)
		if(String.isBlank(String.valueOf(finance.H_ERB__c))){
			finance.H_ERB__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 役員退職金予定額
		if(String.isBlank(String.valueOf(finance.H_ORB__c))){
			finance.H_ORB__c.addError(Label.Msg_Error_H_NotInput);
			isError = true;
		}
		// 画面入力項目のいずれかに"-(マイナス)の数値が入力されている場合"
		if(finance.Term__c < 0 ){
			finance.Term__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 予想売上下落率
		if(finance.H_RD__c < 0 ){
			finance.H_RD__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 連帯保証債務額
		if(finance.H_JASGD__c < 0 ){
			finance.H_JASGD__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 役員借入金
		if(finance.H_LO__c < 0 ){
			finance.H_LO__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 死亡保障額(法人契約)
		if(finance.H_DBAC__c < 0 ){
			finance.H_DBAC__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 含み益
		if(finance.H_UG__c < 0 ){
			finance.H_UG__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 死亡保障額(個人契約)
		if(finance.H_DBAI__c < 0 ){
			finance.H_DBAI__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 銀行返済額(月)
		if(finance.H_BRM__c < 0 ){
			finance.H_BRM__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 銀行返済額　準備額
		if(finance.H_BRP__c < 0 ){
			finance.H_BRP__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 遺族生活資金(月)
		if(finance.H_BFLEM__c < 0 ){
			finance.H_BFLEM__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 遺族生活資金(必要期間)
		if(finance.H_BFLET__c < 0 ){
			finance.H_BFLET__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 現在の社長の報酬/年
		if(finance.H_RE__c < 0 ){
			finance.H_RE__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 社長の報酬減額可能額/年
		if(finance.H_CRAA__c < 0 ){
			finance.H_CRAA__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 従業員退職金(予定額)
		if(finance.H_ERB__c < 0 ){
			finance.H_ERB__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		// 役員退職金予定額
		if(finance.H_ORB__c < 0 ){
			finance.H_ORB__c.addError(Label.Msg_Error_H_Minus);
			isError = true;
		}
		return isError;
	}
	/**
	* checkExist
	* 以下の条件に該当するファイナンス情報レコードが1件以上存在する場合、エラーメッセージを表示する。
	* ①URLパラメータ　cuid　= ファイナンス情報obj.顧客obj.ID
	* ②画面.決算期 = ファイナンス情報obj.決算期
	* ③ファイナンスobj.ヒアリング情報フラグ = True
	* @param 	: なし
	* @return	: 以下の条件に該当するファイナンス情報レコードが1件以上存在する -> True
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	private Boolean checkExist(){
		// 重複チェック
		List<Finance__c> finanList = [SELECT id 
									  FROM Finance__c 
									  // ①URLパラメータ　cuid　= ファイナンス情報obj.顧客obj.ID
									  WHERE Customer__c =: mapParameter.get(CommonConst.STR_PARAMETER_CUID) 
										// ②画面.決算期 = ファイナンス情報obj.決算期
										AND Term__c =:finance.Term__c
										/*// ③ファイナンスobj.ヒアリング情報フラグ = True
										AND Hearing__c = true*/];
		// 以下の条件に該当するファイナンス情報レコードが1件以上存在する場合、エラーメッセージを表示する。
		if(!finanList.isEmpty()){
			return true;
		}
		return false;
	}
	/**
	* cancel
	* 「キャンセル」ボタン処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified : 2015/03/04 keizu)ytanaka コミュニティユーザ時、キャンセルでホーム画面に戻る際のエラーに対応
	*/
	public Pagereference cancel(){
		// ホームURLを取得
		String urlHome = URL.getSalesforceBaseUrl().toExternalForm();
		// 2015/03/04 keizu)ytanaka コミュニティユーザ時、キャンセルでホーム画面に戻る際のエラーに対応 Start ↓
		if (URL.getCurrentRequestUrl().toExternalForm().indexOf('persona') > 0) {
			urlHome = urlHome + '/persona';
		}
		// 2015/03/04 keizu)ytanaka コミュニティユーザ時、キャンセルでホーム画面に戻る際のエラーに対応 End↓
		// URLパラメータを取得
		String strUrl = Apexpages.currentPage().getParameters().get(CommonConst.STR_PARAMETER_RETURL);
		// URLパラメータ　returl　＝NULLの場合ホーム画面に遷移する
		if(String.isBlank(strUrl)){
			Pagereference pageHome = new Pagereference(urlHome);
			pageHome.setRedirect(true);
			return pageHome;
		}else{// URLパラメータ　returl　<> NULLの場returl 先に遷移する。
			Pagereference pageUrl = new Pagereference(strUrl);
			pageUrl.setRedirect(true);
			return pageUrl;
		}
		return null;
	}
	/**
	* clearData
	* べてクリア」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	public void clearData(){
		finance.H_RD__c = null;
		finance.H_JASGD__c = null;
		finance.H_LO__c = null;
		finance.H_DBAC__c = null;
		finance.H_UG__c = null;
		finance.H_DBAI__c = null;
		finance.H_BRM__c = null;
		finance.H_BRP__c = null;
		finance.H_BFLEM__c = null;
		finance.H_BFLET__c = null;
		finance.H_RE__c = null;
		finance.H_CRAA__c = null;
		finance.H_ERB__c = null;
		finance.H_ORB__c = null;
	}
	/**
	* checkCustomsetting()
	* チェック条件. 　カスタム設定取得チェック
	* @param 	: なし
	* @return	: カスタム設定取得出来ないー＞True
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified : 
	*/
	private Boolean checkCustomsetting(){
		// カスタム設定.法人税率定義を1件取得
		taxDefinition = TaxDefinition__c.getOrgDefaults();
		if(taxDefinition == null || String.isBlank(taxDefinition.Id)){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_SetupCheck));
			return true;
		}
		return false;
	}
	/**
	* moveToBsplinputpage
	* BS入力画面
	* @param 	: なし
	* @return	: なし
	* @created  : Quy Nguyen Trung 2014/03/31
	* @modified : 
	*/
	public Pagereference moveToBsplinputpage(){
		PageReference newPage = Page.BSPLInputPage;
		if(!String.isBlank(finance.Id)){
			newPage.getParameters().put(CommonConst.STR_PARAMETER_ID, finance.Id);
			newPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, finance.Customer__c);
			newPage.getParameters().put(CommonConst.STR_PARAMETER_RETURL, '/apex/HearingInputPage?' + 
																CommonConst.STR_PARAMETER_ID + '=' + finance.Id + 
																'&' + CommonConst.STR_PARAMETER_CUID + '=' + finance.Customer__c);
		}else{
			String strReturn = mapParameter.get(CommonConst.STR_PARAMETER_RETURL);
			newPage.getParameters().put(CommonConst.STR_PARAMETER_RETURL, strReturn);
			String strCuid = mapParameter.get(CommonConst.STR_PARAMETER_CUID);
			newPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, strCuid);
		}
		newPage.setRedirect(true);
		return newPage;
	}
	
	/**
	* resetSessionTimeout
	* PERSONAタイムアウト回避
	* @param 	: なし
	* @return	: なし
	* @created  : 2015/12/03 KSVC PHUONG DINH VU
	* @modified : 2015/12/08 KSVC PHUONG DINH VU
	*/
	public void resetSessionTimeout(){
		try{
			// List<FC_TIMEOUT__c> fcTimeoutList =  [select FC_TIMEOUT__c From FC_TIMEOUT__c];
			FC_TIMEOUT__c fcTimeOut = HearingDAO.getFcTimeOut();
			strFCTimeOut = fcTimeOut == null || fcTimeOut.FC_TIMEOUT__c == null ? '0' : String.valueOf(fcTimeOut.FC_TIMEOUT__c);
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
	}
	
}