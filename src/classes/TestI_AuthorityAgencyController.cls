@isTest
private class TestI_AuthorityAgencyController {

	//ID管理作成（本社）
	public static E_IDCPF__c createHonIDCPF(Id userId){
		E_IDCPF__c idHonsha = new E_IDCPF__c(
			//ユーザId
			User__c = userId
			//所有者Id
			,OwnerId = userId
			//ID種別
			,ZIDTYPE__c = 'EP'
			//パスワードステータス
			,ZSTATUS01__c = '1'
			//照会者コード
			,ZINQUIRR__c = 'BR**'
			//利用アプリ
			,AppMode__c = 'IRIS'
			);
		insert idHonsha;
		return idHonsha;
	}

	//ID管理作成（代理店）
	public static E_IDCPF__c createJimIDCPF(Id userId , String idType){
		//ID管理作成
		E_IDCPF__c idJim = new E_IDCPF__c(
			//ユーザId
			User__c = userId
			//所有者Id
			,OwnerId = userId
			//ID種別
			,ZIDTYPE__c = idType
			//パスワードステータス
			,ZSTATUS01__c = '1'
			//照会者コード
			,ZINQUIRR__c = 'BR01'
			//利用アプリ
			,AppMode__c = 'IRIS'
			);
		insert idJim;
		return idJim;
	}

	//取引先（格）作成
	public static Account createKakuAccount() {
		Account accParent = new Account(
			Name = 'TestKAKU'
			//代理店格コード
			, E_CL1PF_ZHEADAY__c = '12345'
			//漢字住所
			, E_COMM_ZCLADDR__c = '東京都中央区日本橋1-1'
			//代理店格カナ名
			, E_CL1PF_ZAHKNAME__c = 'テストカク'
			//有効フラグ
			, E_COMM_VALIDFLAG__c = '1'
			//支社コード
			, E_CL2PF_BRANCH__c = '01'
			);
		insert accParent;
		return accParent;
	}

	//取引先（事務所）作成
	public static Account createJimAccount(Id parentId){
		Account accAgency = new Account(
			Name = 'TestJimusho'
			//代理店事務所コード
			, E_CL2PF_ZAGCYNUM__c = '67890'
			//有効フラグ
			, E_COMM_VALIDFLAG__c = '1'
			//支社コード
			, E_CL2PF_BRANCH__c = '01'
			//親取引先
			, ParentId = parentId
			);
		insert accAgency;
		return accAgency;
	}

	//権限セットを作成（Atria権限管理（参照のみ））
	public static void createAtriaPermissions(Id usrId) {
		Id permAgnyId = [
							Select
								Id
							FROM
								PermissionSet
							Where
								Name = 'ATR_AuthMngRead'
							].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(
											AssigneeId = usrId
											, PermissionSetId = permAgnyId
											);
		insert src;
	}

	/*
	**社員ユーザかつ本社勤務（MR）
	*/
	static testMethod void honshaMRTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

		//ID管理作成
		E_IDCPF__c idHonsha = createHonIDCPF(actUser.Id);

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		Account accAgency = createJimAccount(accParent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				//画面再読み込み
				acon.searchAgency();
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(acon.sortType, I_AuthorityAgencyController.SORT_TYPE_AGENCY);
		}
	}

	/*
	**社員ユーザかつ本社勤務（拠点長）
	*/
	static testMethod void honshaKyotenchoTest() {
		// 実行ユーザ作成（拠点長）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', '拠点長');

		//ID管理作成
		E_IDCPF__c idHonsha = createHonIDCPF(actUser.Id);

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		Account accAgency = createJimAccount(accParent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				//画面再読み込み
				acon.searchAgency();
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(acon.sortType, I_AuthorityAgencyController.SORT_TYPE_AGENCY);
		}
	}

	/*
	**社員ユーザかつ本社勤務でない（MR)
	*/
	static testMethod void mRAgencyTest(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

		//ID管理作成
		E_IDCPF__c idMr = createJimIDCPF(actUser.Id , 'AY');

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		Account accAgency = createJimAccount(accParent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				//拠点表示
				acon.authChangeAgency();
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(acon.sortType, I_AuthorityAgencyController.SORT_TYPE_AGENCY);
		}
	}
	
	/*
	**社員ユーザかつ本社勤務でない（拠点長)
	*/
	static testMethod void kyotenchoAgencyTest(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', '拠点長');

		//ID管理作成
		E_IDCPF__c idMr = createJimIDCPF(actUser.Id , 'AH');

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		Account accAgency = createJimAccount(accParent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				acon.authChangeAgency();
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(acon.sortType, I_AuthorityAgencyController.SORT_TYPE_AGENCY);
		}
	}
	
	/*
	**社員ユーザでない場合
	*/
	static testMethod void agencyTest(){

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		Account accAgency = createJimAccount(accParent.Id);

		//取引先責任者を作成
		String actUserLastName = 'fstest';
		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);

		//実行ユーザを作成
		User actUser = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);

		//ID管理作成
		E_IDCPF__c idMr = createJimIDCPF(actUser.Id , 'AT');

		//レコードを共有
		TestI_TestUtil.createAccountShare(accAgency.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(accParent.Id, actUser.Id);


		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);
		E_CookieHandler.setCookieCompliance();

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				String cPage = '';
				cPage = String.valueOf(Apexpages.currentPage());
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(cPage.contains('iris_authorityagency'), true);
		}
	}
	
	/*
	**権限のないユーザ
	*/
	static testMethod void noAuthorityTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

		//ID管理作成
		E_IDCPF__c idMr = createJimIDCPF(actUser.Id , 'EP');

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		Account accAgency = createJimAccount(accParent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				String cPage = '';
				cPage = String.valueOf(Apexpages.currentPage());
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(cPage.contains('iris_authorityagency'), true);
		}
	}

	/*
	**社員ユーザかつ本社勤務（MR）複数表示
	*/
	static testMethod void honshaMR200Test() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

		//ID管理作成
		E_IDCPF__c idHonsha = createHonIDCPF(actUser.Id);

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		List<Account> testAgencyList = new List<Account>();
		for(Integer i=0; i<200; i++){
			testAgencyList.add(new Account(
									Name = 'TestAgency'+ String.valueOf(i)
									//代理店事務所コード
									, E_CL2PF_ZAGCYNUM__c = '67890'
									//有効フラグ
									, E_COMM_VALIDFLAG__c = '1'
									//支社コード
									, E_CL2PF_BRANCH__c = '01'
									//親取引先
									, ParentId = accParent.Id
									));
		}
		insert testAgencyList;

		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgency;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgencyController acon = new I_AuthorityAgencyController();
				acon.pageAction();
				//検索文字列入力
				acon.searchText = 'Test';
				
				//画面再読み込み
				acon.searchAgency();
				
				//表示行数追加
				acon.addRows();
				
				//代理店名ソート
				ApexPages.currentPage().getParameters().put('st','agency');
				acon.sortRows();
				
				//代理店コードソート
				ApexPages.currentPage().getParameters().put('st','zheaday');
				acon.sortRows();
				
				//住所ソート
				ApexPages.currentPage().getParameters().put('st','agentaddress');
				acon.sortRows();
				
				//総行数取得
				acon.getTotalRows();
				
				//代理店リスト件数
				acon.getAgencyCount();
				
				//MAX表示件数
				acon.getListMaxRows();
				String cPage = '';
				cPage = String.valueOf(Apexpages.currentPage());
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(cPage.contains('iris_authorityagency'), true);
		}
	}
}