@isTest
private with sharing class TestMNT_OppBulkUpdateController{
		private static testMethod void testPageMethods() {	
			MNT_OppBulkUpdateController page = new MNT_OppBulkUpdateController(new ApexPages.StandardController(new Opportunity()));	
			page.getOperatorOptions_Opportunity_StageName_multi();	
			page.getOperatorOptions_Opportunity_IsOwner_c();	

		Integer defaultSize;

		defaultSize = page.Component3.items.size();
		page.Component3.add();
		System.assertEquals(defaultSize + 1, page.Component3.items.size());
		page.Component3.items[defaultSize].selected = true;
		page.Component3.doDeleteSelectedItems();
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		MNT_OppBulkUpdateController.Component3 Component3 = new MNT_OppBulkUpdateController.Component3(new List<Opportunity>(), new List<MNT_OppBulkUpdateController.Component3Item>(), new List<Opportunity>(), null);
		Component3.setPageItems(new List<MNT_OppBulkUpdateController.Component3Item>());
		Component3.create(new Opportunity());
		Component3.doDeleteSelectedItems();
		Component3.setPagesize(10);		Component3.doFirst();
		Component3.doPrevious();
		Component3.doNext();
		Component3.doLast();
		Component3.doSort();
		System.assert(true);
	}
	
}