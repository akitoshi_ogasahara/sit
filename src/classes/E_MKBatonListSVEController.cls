global with sharing class E_MKBatonListSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Contact record {get{return (Contact)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_MKBatonListSVEExtender getExtender() {return (E_MKBatonListSVEExtender)extender;}
	
	
	public friendListTab friendListTab {get; private set;}
	public targetListTab targetListTab {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_MKBatonListSVEController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1240');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1240',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1239');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1239',tmpPropMap);


		SObjectField f;

		f = Lead.fields.MK_CampaignTarget_Name__c;
		f = Lead.fields.CreatedDate;
		f = Lead.fields.NameLinkForBaton__c;
		f = Lead.fields.Company;
		f = MK_CampaignTarget__c.fields.Name;
		f = MK_CampaignTarget__c.fields.CLTPF_CLNTNUM__c;
		f = MK_CampaignTarget__c.fields.LeadTMPRecCount__c;
		f = MK_CampaignTarget__c.fields.LeadTMPCreateLeadFlagCount__c;
		f = Lead.fields.RecordTypeId;
		f = Lead.fields.MK_CampaignTarget__c;
		f = Lead.fields.ContractantCheckStatus__c;
		f = Lead.fields.OwnerId;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Contact.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('Contact');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			friendListTab = new friendListTab(new List<Lead>(), new List<friendListTabItem>(), new List<Lead>(), null);
			listItemHolders.put('friendListTab', friendListTab);
			query = new SkyEditor2.Query('Lead');
			query.addFieldAsOutput('MK_CampaignTarget_Name__c');
			query.addFieldAsOutput('CreatedDate');
			query.addFieldAsOutput('NameLinkForBaton__c');
			query.addFieldAsOutput('Company');
			query.addFieldAsOutput('RecordTypeId');
			friendListTab.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('friendListTab', query);
			
			targetListTab = new targetListTab(new List<MK_CampaignTarget__c>(), new List<targetListTabItem>(), new List<MK_CampaignTarget__c>(), null);
			listItemHolders.put('targetListTab', targetListTab);
			query = new SkyEditor2.Query('MK_CampaignTarget__c');
			query.addFieldAsOutput('Name');
			query.addFieldAsOutput('CLTPF_CLNTNUM__c');
			query.addFieldAsOutput('LeadTMPRecCount__c');
			query.addFieldAsOutput('LeadTMPCreateLeadFlagCount__c');
			query.addWhere('Agent__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('targetListTab', 'Agent__c');
			targetListTab.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('targetListTab', query);
			
			registRelatedList('CampaignTargets__r', 'targetListTab');
			
			SkyEditor2.Query friendListTabQuery = queryMap.get('friendListTab');
			friendListTabQuery.addWhereIfNotFirst('AND');
			friendListTabQuery.addWhere('( RecordType.DeveloperName = \'BatonRecType\' AND MK_CampaignTarget__r.Id != \'\' AND ContractantCheckStatus__c = \'新規リード\' AND OwnerId = \''+System.UserInfo.getUserId()+'\')');
			friendListTabQuery.addSort('MK_CampaignTarget_Name__c',True,True);
			SkyEditor2.Query targetListTabQuery = queryMap.get('targetListTab');
			targetListTabQuery.addSort('Name',True,True);
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_MKBatonListSVEExtender(this);
			init();
			
			friendListTab.extender = this.extender;
			targetListTab.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class friendListTabItem extends SkyEditor2.ListItem {
		public Lead record{get; private set;}
		@TestVisible
		friendListTabItem(friendListTab holder, Lead record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class friendListTab extends SkyEditor2.ListItemHolder {
		public List<friendListTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			friendListTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<friendListTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new friendListTabItem(this, (Lead)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class targetListTabItem extends SkyEditor2.ListItem {
		public MK_CampaignTarget__c record{get; private set;}
		@TestVisible
		targetListTabItem(targetListTab holder, MK_CampaignTarget__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class targetListTab extends SkyEditor2.ListItemHolder {
		public List<targetListTabItem> items{get; private set;}
		@TestVisible
			targetListTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<targetListTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new targetListTabItem(this, (MK_CampaignTarget__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}