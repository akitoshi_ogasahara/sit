/**
 * 
 */
@isTest
private class TestCR_Validate {

 	/**
	 * Boolean hasErrOutputRecordYm(String outputYm)
	 */
	// 正常
    static testMethod void test_hasErrOutputRecordYm01() {
    	String outputYm = '2016/06';
    	System.assert(!CR_Validate.hasErrOutputRecordYm(outputYm));
    }
    
    // エラー　全角数値
    static testMethod void test_hasErrOutputRecordYm02() {
    	String outputYm = '２０１６/０６';
    	System.assert(CR_Validate.hasErrOutputRecordYm(outputYm));
    }

	// エラー　文字列
    static testMethod void test_hasErrOutputRecordYm03() {
    	String outputYm = 'abcd/ef';
    	System.assert(CR_Validate.hasErrOutputRecordYm(outputYm));
    }

	// エラー　形式
    static testMethod void test_hasErrOutputRecordYm04() {
    	String outputYm = '201606';
    	System.assert(CR_Validate.hasErrOutputRecordYm(outputYm));
    }   
    
	// エラー　空欄
    static testMethod void test_hasErrOutputRecordYm05() {
    	String outputYm = '';
    	System.assert(CR_Validate.hasErrOutputRecordYm(outputYm));
    }  
    
    /**
     * Boolean hasErrOutputRecordYmFrom(String outputYm, Map<String, Integer> ymMap)
     */
    // 正常
    static testMethod void test_hasErrOutputRecordYmFrom01() {
    	String outputYm = '2016/06';
    	System.assert(!CR_Validate.hasErrOutputRecordYmFrom(outputYm, CR_Const.OUTPUT_RECORD_YM_REPORTS_FROM, CR_Const.OUTPUT_RECORD_YM_REPORTS_TO));
    }
    
    // エラー　期間
    static testMethod void test_hasErrOutputRecordYmFrom02() {
    	String outputYm = TestCR_TestUtil.getOutputYmLast(CR_Const.TYPE_BIZREPORTS);
    	System.assert(CR_Validate.hasErrOutputRecordYmFrom(outputYm, CR_Const.OUTPUT_RECORD_YM_REPORTS_FROM, CR_Const.OUTPUT_RECORD_YM_REPORTS_TO));
    }
    
    /**
     * Boolean hasErrSameCondition(String outputYm, String dlCondition, List<E_DownloadHistorry__c> dlList)
     */
    // 正常
    static testMethod void test_hasErrSameCondition01(){
    	String outputRecordYm = '2016/06';
    	
		// TestData
		CR_Agency__c agncy = createAgencyList()[0];
		agncy.ReportType__c = '0:aaaaaa';

		List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, outputRecordYm, toJSON(agncy)));
		insert dhList;
		
		agncy.ReportType__c = '1:aaaaaa';
		System.assert(!CR_Validate.hasErrSameCondition(outputRecordYm, toJSON(agncy), dhList));
    }
	
	// エラー　同条件
    static testMethod void test_hasErrSameCondition02(){
    	String outputRecordYm = '2016/06';
    	
		// TestData
		CR_Agency__c agncy = createAgencyList()[0];
		agncy.ReportType__c = '0:aaaaaa';

		List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, outputRecordYm, toJSON(agncy)));
		insert dhList;
		
		System.assert(CR_Validate.hasErrSameCondition(outputRecordYm, toJSON(agncy), dhList));
    }
    
    /**
     * Boolean hasErrRequestCount(List<E_DownloadHistorry__c> dlList)
     */
    // 正常　2件
    static testMethod void test_hasErrRequestCount01(){
		// TestData
		CR_Agency__c agncy = createAgencyList()[0];
		agncy.ReportType__c = '0:aaaaaa';

		List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/06', toJSON(agncy)));
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/07', toJSON(agncy)));
		insert dhList;
		
		System.assert(!CR_Validate.hasErrRequestCount(dhList));
    }
       
    // エラー　3件
    static testMethod void test_hasErrRequestCount02(){
		// TestData
		CR_Agency__c agncy = createAgencyList()[0];
		agncy.ReportType__c = '0:aaaaaa';

		List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/06', toJSON(agncy)));
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/07', toJSON(agncy)));
		dhList.add(TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/08', toJSON(agncy)));
		insert dhList;
		
		System.assert(CR_Validate.hasErrRequestCount(dhList));
    }

    /**
     * Datetime getRequestFromDatetime()
     */
    // 正常
    static testMethod void test_getRequestFromDatetime01(){
		Datetime dt = CR_Validate.getRequestFromDatetime();
		System.assert(dt != null);
    }  
	    
    /* ****************************************************************************** */
    private static String toJSON(CR_Agency__c agency){
    	E_DownloadCondition.BizReportsDownloadCondi dlCondi = new E_DownloadCondition.BizReportsDownloadCondi();
		dlCondi.ZHEADAY = agency.AgencyCode__c;
		dlCondi.reportType = agency.ReportType__c.substring(0,1);
		return dlCondi.toJSON();
    }
    
    private static List<CR_Agency__c> createAgencyList(){
		List<Account> accountList = TestCR_TestUtil.createAccountList(true, 1);
		Set<Id> ids = new Set<Id>();
		for(Account acc : accountList){
			ids.add(acc.Id);
		}
		return TestCR_TestUtil.createCRAgencyList(true, ids, CR_Const.RECTYPE_DEVNAME_BIZ);
    }
}