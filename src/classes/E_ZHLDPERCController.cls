public with sharing class E_ZHLDPERCController {

	public E_SVFPF__c SVFPF{get;set;}
	public Datetime BaseDateTime{get;set;}
	
	/**
	 *		新しい積立金割合が入力可能の場合にTrue
	 */
	public Boolean getIsEditable(){
		if(BaseDateTime == null){
			BaseDateTime = System.Now();
		}
				
		Datetime fromDT = getDatetimeGMTFromStrings(SVFPF.E_SVCPF__r.ZFSDFROM__c,	SVFPF.E_SVCPF__r.ZFSHFROM__c,	SVFPF.E_SVCPF__r.ZFSMFROM__c);
		Datetime toDT 	= getDatetimeGMTFromStrings(SVFPF.E_SVCPF__r.ZFSDTO__c,		SVFPF.E_SVCPF__r.ZFSHTO__c,		SVFPF.E_SVCPF__r.ZFSMTO__c);

		return SVFPF.E_SVCPF__r.FLAG02__c		// ファンド受付フラグTrue
				&& fromDT <= BaseDatetime
				&& toDT > BaseDatetime; 
	
	}
	
	private Datetime getDatetimeGMTFromStrings(String yyyyMMdd, String hh, String mm){
		try{
			Integer year = Integer.valueOF(yyyyMMdd.substring(0,4));
			Integer month = Integer.valueOF(yyyyMMdd.substring(4,6));
			Integer day = Integer.valueOF(yyyyMMdd.substring(6,8));
			
			//GMTで返す
			return Datetime.newInstanceGMT(	year
											,month
											,day
											,Integer.valueOf(hh) - 9			//日本のタイムゾーンの差分
											,Integer.valueOf(mm)
											,0);
		}catch(Exception e){
			
			throw new SkyEditor2.ExtenderException('GMTへの変換に失敗しました。' + yyyyMMdd + ' ' + hh + ':' + mm);
		}
	}


}