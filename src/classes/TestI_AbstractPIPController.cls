@isTest
private class TestI_AbstractPIPController {

		//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';
	private static String APP_IRIS = 'IRIS';
	private static String APP_NNLINK = 'NNLINK';

	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];   //Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Account atAcc;
	private static List<Account> ayAccList;
	private static Contact atCon;
	private static List<E_Policy__c> policyList;
	private static I_ContentMaster__c content;


	@isTest static void getListMaxRowsTest01() {
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP,APP_IRIS);
		updateAccOwner(ayAccList[0]);
		updateAccOwner(ayAccList[1]);

		System.runAs(user){
			policyList = new List<E_Policy__c>();
			policyList = createPolicys();
			createContentMaster();

			//ページ情報
			PageReference pref = Page.IRIS_Coli;
			pref.getParameters().put('id', policyList[0].Id);
			Test.setCurrentPage(pref);
			//テスト開始
			Test.startTest();

			I_ColiController con = new I_ColiController();
			//con.init();
			con.pageAction();

			Integer result = con.getListMaxRows();


			System.assertEquals(I_Const.LIST_MAX_ROWS,result);


			//テスト終了
			Test.stopTest();
		}
	}

	@isTest static void getDataSyncDateHeadTest01() {
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP,APP_IRIS);
		updateAccOwner(ayAccList[0]);
		updateAccOwner(ayAccList[1]);

		System.runAs(user){
			policyList = new List<E_Policy__c>();
			policyList = createPolicys();
			createContentMaster();
			createBiz();

			//ページ情報
			PageReference pref = Page.IRIS_Coli;
			pref.getParameters().put('id', policyList[0].Id);
			Test.setCurrentPage(pref);
			//テスト開始
			Test.startTest();

			I_ColiController con = new I_ColiController();
			//con.init();
			con.pageAction();

			String result = con.getDataSyncDateHead();


			System.assertEquals(E_Util.formatDataSyncDate(),result);


			//テスト終了
			Test.stopTest();
		}
	}

	@isTest static void openManualPdfTest01() {
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP,APP_IRIS);
		updateAccOwner(ayAccList[0]);
		updateAccOwner(ayAccList[1]);

		System.runAs(user){
			policyList = new List<E_Policy__c>();
			policyList = createPolicys();
			createContentMaster();
			createBiz();

			//ページ情報
			PageReference pref = Page.IRIS_Coli;
			pref.getParameters().put('id', policyList[0].Id);
			Test.setCurrentPage(pref);
			//テスト開始
			Test.startTest();

			I_ColiController con = new I_ColiController();
			//con.init();
			con.pageAction();

			PageReference result = con.openManualPdf();

			System.assert(result!=null);


			//テスト終了
			Test.stopTest();
		}
	}

	@isTest static void initTest01() {
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP,APP_NNLINK);
		updateAccOwner(ayAccList[0]);
		updateAccOwner(ayAccList[1]);

		System.runAs(user){
			policyList = new List<E_Policy__c>();
			policyList = createPolicys();
			createContentMaster();

			//ページ情報
			PageReference pref = Page.IRIS_Coli;
			pref.getParameters().put('id', policyList[0].Id);
			Test.setCurrentPage(pref);
			//テスト開始
			Test.startTest();

			I_ColiController con = new I_ColiController();
			//con.init();
			con.pageAction();

			//テスト終了
			Test.stopTest();
		}
	}


	@isTest static void doSendSpecialAccountTest01() {
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP,APP_NNLINK);
		updateAccOwner(ayAccList[0]);
		updateAccOwner(ayAccList[1]);

		System.runAs(user){
			policyList = new List<E_Policy__c>();
			policyList = createPolicys();
			createContentMaster();

			//ページ情報
			PageReference pref = Page.IRIS_Coli;
			pref.getParameters().put('id', policyList[0].Id);
			Test.setCurrentPage(pref);
			//テスト開始
			Test.startTest();

			I_ColiController con = new I_ColiController();
			//con.init();
			con.pageAction();

			PageReference result = con.doSendSpecialAccount();

			//テスト終了
			Test.stopTest();

			System.assert(String.isNotEmpty(String.valueOf(result)));
		}
	}

		/* Test Data **************************** */
	/** User作成 */
	private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
		Profile p = [Select Id From Profile Where Name = :profileName];

		// Base Info
		user = new User(
			Lastname = 'test'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);

		// User
		if(profileName != PF_PARTNER){
			UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
			user.UserRoleId = portalRole.Id;
			insert user;

		// Partner User
		}else{
			user.ContactId = atCon.Id;
			insert user;

			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
		}
	}

	/** データアクセス系作成 */
	private static void createDataAccessObj(Id userId, String idType,String appmode){
		system.runAs(thisuser){
			// 権限割り当て
			TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);

			// ID管理
			E_IDCPF__c idcpf = new E_IDCPF__c(
				User__c = userId
				,ZIDTYPE__c = idType
				,FLAG01__c = '1'
				,FLAG06__c = '1'
				,ZSTATUS01__c = '1'
				,OwnerId = userId
				,ZINQUIRR__c = 'BR**'
				,AppMode__c = appmode
			);
			insert idcpf;
		}
	}

	/** Data作成 */
	private static void createDataAccount(){
		system.runAs(thisuser){
			// Account 代理店格
			ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
			insert ahAcc;

			// Account 事務所
			ayAccList = new List<Account>();
			for(Integer i = 1; i < 4; i++){
				Account acc = new Account(
					Name = 'office' + i
					,ParentId = ahAcc.Id
					,E_CL2PF_ZAGCYNUM__c = 'ay001'
					,E_COMM_VALIDFLAG__c = '1'
					,AGNTBR_NM__c = '東京'
				);
				ayAccList.add(acc);
			}
			insert ayAccList;

			// Contact 募集人
			if(!ayAccList.isEmpty()){
				atCon = new Contact(LastName = 'test',AccountId = ayAccList[0].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[0].E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}
		}
	}

	private static void updateAccOwner(Account acc){
		system.runAs(thisuser){
			acc.OwnerId = user.Id;
			update acc;
		}
	}

	/**
	 * 保険契約ヘッダを複数作成
	 *
	 */
	public static List<E_Policy__c> createPolicys(){

		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(E_Const.POLICY_RECORDTYPE_COLI);

		// 保険契約ヘッダを作成
		List<E_Policy__c> policys = new List<E_Policy__c>();
		for(Integer i = 0; i < 3; i++){
			E_Policy__c policy = new E_Policy__c();

			policy.Contractor__c = atCon.Id;
			policy.MainAgent__c = atCon.Id;
			policy.Insured__c = atCon.Id;
			policy.COMM_CHDRNUM__c = '99999999'+i+'|1';
			policy.MainAgentAccount__c = ayAccList[0].Id;
			policy.COMM_SPLITC01__c = 33;
			policy.SubAgent__c = atCon.Id;
			policy.SubAgentAccount__c = ayAccList[0].Id;
			policy.COMM_SPLITC02__c = 66;
			policy.COMM_ZEBKNM__c = 'テスト金融機関';
			policy.COMM_ZEBKACOW__c = '口座名義人';
			policy.COMM_BANKACCKEY__c = '1234567890';
			policy.COLI_ZADVPDCF__c = true;
			policy.COMM_STATCODE__c = 'R';
			policy.COMM_ZRSTDESC__c = '';
			policy.COMM_ZEFREQ__c = '年 払';
			policy.COMM_ZECHNL__c = '口座振替';
			policy.COLI_ZNPTDDCF__c = true;
			policy.COLI_ZBKTRDCF__c = true;
			policy.COLI_ZBKPODIS__c = '20';
			policy.COLI_ZCSHVAL__c = 10000;
			policy.COLI_ZLOANTOT__c = 1000;
			policy.COLI_ZEADVPRM__c = 100;
			policy.COLI_ZUNPREM__c = 10;
			policy.COLI_ZCVDCF__c = true;
			policy.COLI_ZUNPCF__c = true;
			policy.COLI_ZUNPDCF__c = true;
			policy.COLI_RCPTDT__c = '20171027';
			policy.COLI_AVPRMRCV__c = 10000;
			policy.COLI_REMNBANN__c = 1;
			policy.COLI_ZEPLTOT__c = 10000;
			policy.COLI_ZEPLPRN__c = 1000;
			policy.COLI_ZEPLINT__c = 100;
			policy.COLI_ZPLCAPT__c = '1ヶ月';
			policy.COLI_ZPLRATE__c = 123;
			policy.COMM_ZPLDCF__c = true;
			policy.COLI_ZEAPLTOT__c = 10000;
			policy.COLI_ZEAPLPRN__c = 10000;
			policy.COLI_ZEAPLINT__c = 1000;
			policy.COLI_ZAPLCAPT__c = '1ヶ月';
			policy.COLI_ZAPLRATE__c = 123;
			policy.COLI_ZAPLDCF__c = true;
			policy.COMM_CLNTNUM__c = '12345678';
			policy.COMM_ZCLNAME__c = '契約者A';
			policy.COMM_ZINSNAM__c = '被保険者A';
			policy.COMM_ZKNJSEX__c = '男性';
			policy.COMM_DOB__c = '20171027';
			policy.RecordTypeId = recTypeId;
			policy.COMM_SPLITC__c = 55;



			policys.add(policy);
		}

		insert policys;

		return policys;
	}



	//個人保険特約
	//static void createCOV(E_Policy__c po){

	//	Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);


	//	E_COVPF__c cov = new E_COVPF__c();
	//	cov.E_Policy__c = po.Id;
	//	cov.COMM_STATDATE__c = '20161111';
	//	cov.COMM_SUMINS__c = 100000;
	//	cov.COLI_INSTPREM__c = 100000;
	//	cov.COLI_ZCRIND__c = 'C';
	//	cov.COMM_ZCOVRNAM__c = '4テストタイプ';
	//	cov.COLI_ZDPTYDSP__c = true;
	//	cov.COMM_CHDRNUM__c = '99999999';
	//	cov.DCOLI_ZCLNAME__c = '契約者A';
	//	cov.DCOLI_ZINSNAM__c = '被保険者A';
	//	cov.COMM_SUMINS__c = 987654321;
	//	cov.DCOLI_SINSTAMT__c = 123456789;
	//	cov.DCOLI_OCCDATE__c = '20171027';
	//	cov.COMM_ZRCESDSC__c = '終身';
	//	cov.COMM_ZPCESDSC__c = '---------';
	//	cov.RecordTypeId = recTypeId;
	//	cov.COMM_CRTABLE__c = E_Const.COVPF_CRTABLE2_JA;



	//	insert cov;
	//	covList.add(cov);
	//}


	//保険顧客情報
	private static void createCrlpf(E_Policy__c policy,String role){
		E_CRLPF__c crl = new E_CRLPF__c();

		crl.E_Policy__c = policy.Id;
		crl.BNYTYP__c = '01';
		crl.ZCLNAME__c = '照須快';
		crl.CLNTNUM__c = '99999999';
		crl.ZKNJSEX__c = '男性';
		crl.BNYPC__c = 100.00;
		crl.DOB__c = '11111111';
		crl.CRLPFCode__c = '99999999|TS|1';
		crl.CLRRROLE__c = role;
		crl.VALIDFLAG__c = '1';



		insert crl;
	}

	//保険取引履歴
	private static void createPtnpf(E_Policy__c policy){
		E_PTNPF__c ptn = new E_PTNPF__c();

		ptn.E_Policy__c = policy.Id;
		ptn.TRANDATE__c = '20171027';
		ptn.ZTRANDSC__c = 'テストクラス作成';

		insert ptn;
	}

	private static E_BizDataSyncLog__c createBiz(){
		Date d = date.newInstance(1996, 6, 23);
		DateTime dt = datetime.newInstance(1996, 6, 23);
		E_BizDataSyncLog__c rec = new E_BizDataSyncLog__c();
		rec.Kind__c = '1';
		rec.InquiryDate__c = d;
		rec.BatchEndDate__c = dt;
		rec.DataSyncStartDate__c = Datetime.now();
		rec.DataSyncEndDate__c = Datetime.now();
		insert rec;
		return rec;
	}

	private static void createContentMaster(){
		content = new I_ContentMaster__c();
		content.Name = '保険契約概要_画面の使い方PDF';
		insert content;

		Attachment att = new Attachment(Name = 'テストファイル', ParentId = content.Id, body = Blob.valueOf(content.Id));
		insert att;
	}
}