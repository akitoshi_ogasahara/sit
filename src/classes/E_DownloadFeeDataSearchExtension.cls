public with sharing class E_DownloadFeeDataSearchExtension {
	
	// コントローラ
	E_DownloadController extention;
	// パラメータ格納用
	public Boolean isColi {get; private set;}
	public Boolean isSpva {get; private set;}
	public Boolean isBonus {get; private set;}
	
	// 明細の種類
	private static final String NEWEST = 'newest';
	private static final String DESIGNATE_TERM = 'designateTerm';
	
	// 代理店格フラグ
	public Boolean isDistributor {get; private set;}
	
	// 選択された代理店事務所
	public String[] selOfficeList {get; set;}
	// 選択された期間指定方法
	public String selTerm {get; set;}
	
	// 選択可能年月の最大値・最小値
	public String startDate {get; private set;}
	public String endDate {get; private set;}
	// 選択された年月
	public String selFromDate {get; set;}
	public String selToDate {get; set;}
	
	// データが取得できる最古の年月
	public String getStartYearMonth(){
		return getYearMonth(startDate);
	}
	// データが取得できる最新の年月
	public String getEndYearMonth(){
		return getYearMonth(endDate);
	}
	private String getYearMonth(String yearMonth){
		String year  = yearMonth.split('/').get(0);
		String month = yearMonth.split('/').get(1);
		return year + '年' + month + '月';
	}
	
	// 期間指定ラジオボタン
	public List<SelectOption> getNewestOrRangeList(){
		List<SelectOption> ops = new List<SelectOption>();
		ops.add(new SelectOption(NEWEST        , '最新のみ'  ));
		ops.add(new SelectOption(DESIGNATE_TERM, '期間を指定'));
		return ops;
	}
	
	// タイトル設定用
	private String detailTypeStr;
	public String getTitle(){
		return detailTypeStr + '手数料明細データ出力条件設定';
	}
	
	// 検索条件表示フラグ
	public Boolean isDispSearchArea {get; private set;}
	
	// コンストラクタ
	public E_DownloadFeeDataSearchExtension(E_DownloadController extention){
		
		this.extention = extention;
		// パラメータによる実行者情報取得
		extention.isValidate = extention.getActor() && checkCanDisplay();
		// パラメータから取得したレコード存在チェック
		extention.pageRef = extention.init();
		
		if(extention.pageRef == null){
			isDistributor = extention.distributor != null ? true : false;
			/* 挙積対応 */
			if(isDistributor){
				String oid = ApexPages.currentPage().getParameters().get('oid');
				if(String.isNotBlank(oid)){
					Id officeId = Id.valueOf(oid);
					extention.office = E_AccountDaoWithout.getRecById(officeId);
					isDistributor = !isDistributor;
				}
			}
			/* 挙積対応ここまで */
			Id accId = isDistributor ? extention.distributor.Id : extention.office.Id;
			String objApiName = '';
			//初期値設定
			selTerm = NEWEST;
            startDate = E_Util.formatDataSyncDate('yyyy/MM/01');
            endDate = E_Util.formatDataSyncDate();
			if(isColi){
				detailTypeStr = '生保';
				objApiName = 'E_AGSPF__c';
				extention.formNo = E_Const.DH_FORMNO_FEEDATA_AGS;
			}else if(isSpva){
				detailTypeStr = '変額商品（一時払）';
				objApiName = 'E_SASPF__c';
				extention.formNo = E_Const.DH_FORMNO_FEEDATA_SPVA;
			}else if(isBonus){
				detailTypeStr = 'ボーナス';
				objApiName = 'E_BOSPF__c';
				extention.formNo = E_Const.DH_FORMNO_FEEDATA_BOS;
			}
			
			Map<String, String> dateMap = new Map<String, String>();
			String soql = 'SELECT ACCTYEARMONTH__c FROM ' + objApiName + ' ';
			if(isDistributor){
				dateMap = getStartEndDate_distributor(extention.distributor.E_CL1PF_ZHEADAY__c, soql);
			}else{
				dateMap = getStartEndDate_office(accId, soql);
			}
			if(!dateMap.isEmpty()){
				startDate = dateMap.get('mindate') + '/01';
				endDate = dateMap.get('maxdate') + '/01';
				isDispSearchArea = true;
			//データが取得できない場合
			}else{
				extention.pageMessages.addErrorMessage(extention.getMSG().get(E_Const.ERROR_MSG_NODATA));
				isDispSearchArea = false;
				//DF-001082により自画面にエラーメッセージを表示するよう修正
//				extention.pageRef = Page.E_ErrorPage;
//				extention.pageRef.getParameters().put('code', E_Const.ERROR_MSG_NODATA);
			}
			extention.setPgTitle(getTitle());
		}
//system.debug('@@@startDate:'+ startDate);
//system.debug('@@@endDate:'+ endDate);
	}
	
	//ページアクション
	public PageReference pageAction(){
		return E_Util.toErrorPage(extention.pageRef, null);
	}
	
	// CSVダウンロードボタン押下
	public PageReference doDownloadCsv(){
		return doDownload(true);
	}
	// PDFダウンロードボタン押下
	public PageReference doDownloadPDF(){
		return doDownload(false);
	}
	//共通ダウンロード処理
	public PageReference doDownload(boolean isCsv){
		extention.pageMessages.clearMessages();
		extention.dh = new E_DownloadHistorry__c();
		
		if(selTerm.equals(NEWEST)){
			selFromDate = endDate;
			selToDate = endDate;
		}
		if(!checkInputForm(isCsv)){
			return null;
		}
		
		createDlHistory(isCsv);
		
		return null;
	}
	
	/**
	 *  対象明細の最古と最新日の取得
	 */
	/*
	private AggregateResult getStartEndDate(Id accId, Boolean isDistributor, String objApiName){
		E_SoqlManager manager = new E_SoqlManager();
		String soql = 'SELECT Max(ACCTYEARMONTH__c) maxdate, MIN(ACCTYEARMONTH__c) mindate FROM ' + objApiName + ' ';
		if(isDistributor){
			manager.addAndWhere('ParentAccount__c', accId, manager.eq);
		}else{
			manager.addAndWhere('Account__c', accId, manager.eq);
		}
		soql += manager.getWhere();
		return Database.query(soql);
	}
	*/
	
	/**
	 *  対象明細の最古と最新日の取得(代理店事務所)
	 */
	private Map<String, String> getStartEndDate_office(Id accId, String soql){
		Map<String, String> dateMap = new Map<String, String>();
		
		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndWhere('Account__c', accId, manager.eq);
		soql += manager.getWhere();
		
		sObject[] sDateList = Database.query(soql + ' Order by ACCTYEARMONTH__c limit 1');
		if(!sDateList.isEmpty() && sDateList[0].get('ACCTYEARMONTH__c') != null){
			dateMap.put('mindate', string.valueOf(sDateList[0].get('ACCTYEARMONTH__c')));
			sObject[] eDateList = Database.query(soql + ' Order by ACCTYEARMONTH__c Desc limit 1');
			dateMap.put('maxdate', string.valueOf(eDateList[0].get('ACCTYEARMONTH__c')));
		}
		return dateMap;
	}
	
	/**
	 *  対象明細の最古と最新日の取得(代理店格)
	 */
	private Map<String, String> getStartEndDate_distributor(String dCode, String soql){
		boolean isExist = false;
		Map<String, String> dateMap = new Map<String, String>();
		
		//最古月データ取得
		for(integer i = E_Util.getInquiryDate().addYears(-2).year(); i <= E_Util.getInquiryDate().year(); i++){
			for(integer y = 1; y <= (i == E_Util.getInquiryDate().year() ? E_Util.getInquiryDate().month(): 12); y++){
				sObject[] sDateList = getSobjDateList(i, y, dCode, soql);
				if(!sDateList.isEmpty() && sDateList[0].get('ACCTYEARMONTH__c') != null){
					dateMap.put('mindate', string.valueOf(sDateList[0].get('ACCTYEARMONTH__c')));
					isExist = true;
					break;
				}
			}
			if(isExist){
				break;
			}
		}
		/**
		 * パフォーマンス向上パターン　※但し、データが中途半端（歯抜け、一定期間[2月~12月等]のみ）の場合、対応不可の為保留
		 */
/*
		//最古月データ取得(古い方から毎年1月のデータ有無確認。データがあれば前年12月以前を確認)
		for(integer i = E_Util.getInquiryDate().addYears(-2).year(); i <= E_Util.getInquiryDate().year(); i++){
			sObject[] sDateList = getSobjDateList(i, 1, dCode, soql);
			if(!sDateList.isEmpty() && sDateList[0].get('ACCTYEARMONTH__c') != null){
				dateMap.put('mindate', string.valueOf(sDateList[0].get('ACCTYEARMONTH__c')));
				isExist = true;
				//最古年1月取得時は前年検索不要
				if(i > E_Util.getInquiryDate().addYears(-2).year()){
					for(integer y = 12; y >= 2; y--){
						sDateList = getSobjDateList(i - 1, y, dCode, soql);
						if(!sDateList.isEmpty() && sDateList[0].get('ACCTYEARMONTH__c') != null){
							dateMap.put('mindate', string.valueOf(sDateList[0].get('ACCTYEARMONTH__c')));
							continue;
						}else{
							break;
						}
					}
				}
				break;
			}
		}
		//データが取得できなかった場合、最新年を2月から確認
		if(!isExist && E_Util.getInquiryDate().month() > 1){
			for(integer y = 2; y <= E_Util.getInquiryDate().month(); y++){
				sObject[] sDateList = getSobjDateList(E_Util.getInquiryDate().year(), y, dCode, soql);
				if(!sDateList.isEmpty() && sDateList[0].get('ACCTYEARMONTH__c') != null){
					dateMap.put('mindate', string.valueOf(sDateList[0].get('ACCTYEARMONTH__c')));
					isExist = true;
					break;
				}
			}
		}
*/
		
		if(!isExist){
			return dateMap;
		}
		//最新月データ取得（最古月が取得できた場合）
		for(integer i = E_Util.getInquiryDate().year(); i >= E_Util.getInquiryDate().addYears(-2).year(); i--){
			for(integer y = (i == E_Util.getInquiryDate().year() ? E_Util.getInquiryDate().month(): 12); y >= 1; y--){
				sObject[] eDateList = getSobjDateList(i, y, dCode, soql);
				if(!eDateList.isEmpty() && eDateList[0].get('ACCTYEARMONTH__c') != null){
					dateMap.put('maxdate', string.valueOf(eDateList[0].get('ACCTYEARMONTH__c')));
					return dateMap;
				}
			}
		}
		return dateMap;
	}
	
	private sObject[] getSobjDateList(integer i, integer y, String dCode, String soql){
		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndWhere('index__c', dCode +'|'+ i + '|' + y, manager.eq);
//system.debug('@@@soql:' + soql + manager.getWhere() + ' limit 1');
		return Database.query(soql + manager.getWhere() + ' limit 1');
	}
	
	
	/**
	 *  入力チェック
	 */
	private boolean checkInputForm(boolean isCsv){
		//代理店格の時、代理店事務所未選択
		if(isDistributor && selOfficeList.isEmpty()){
			extention.pageMessages.addErrorMessage('代理店事務所を選択してください。');
			return false;
		}
		if(selTerm != NEWEST){
			//成立年月チェック
			if(!extention.validateDates(selFromDate, selToDate, '出力年月', true)){	
				return false;
			}
			// 単月指定チェック(PDFのみ)
			if(!isCsv){
				if(!selFromDate.equals(selToDate)){
					extention.pageMessages.addErrorMessage('帳票形式(PDF形式)でダウンロードする場合は、出力条件（期間）を単月で指定してください。');
					return false;
				}
			}
		}
		return true;
	}
	/**
	 *  Download履歴作成
	 */
	private void createDlHistory(Boolean isCsv){
		E_DownloadCondition.CommissionDownloadCondi dlCondi = new E_DownloadCondition.CommissionDownloadCondi();
		dlCondi.officeCodes = new List<String>();
		try {
			if(isDistributor){
				dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
				dlCondi.distributorCode = extention.distributor.Id;
				dlCondi.ZHEADAY = extention.distributor.E_CL1PF_ZHEADAY__c;
				dlCondi.ZHEADNAME = extention.distributor.Name;
				dlCondi.officeCodes = selOfficeList;
			}else{
				dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AY;
				dlCondi.distributorCode = extention.office.ParentId;
				dlCondi.ZHEADAY = extention.office.E_ParentZHEADAY__c;
				dlCondi.ZHEADNAME = extention.office.E_ParentName__c;
				dlCondi.officeCodes.add(extention.office.Id);
			}		
			dlCondi.fromyyyy = selFromDate.substring(0,4);
			dlCondi.fromMM   = selFromDate.substring(5,7);
			dlCondi.toyyyy = selToDate.substring(0,4);
			dlCondi.toMM   = selToDate.substring(5,7);
			
			//ダウンロードログレコード生成
			extention.dh.outputType__c = isCsv ? E_Const.DH_OUTPUTTYPE_CSV : E_Const.DH_OUTPUTTYPE_PDF;
			extention.dh.formno__c = extention.formNo;
			extention.dh.Conditions__c = dlCondi.toJSON();
			insert extention.dh;
			
		}catch(Exception e){
			extention.pageMessages.addErrorMessage(e.getMessage());
		}
	}
	
	//実行者のページに対する権限チェック
	private boolean checkCanDisplay(){
		if(getParamPt() && extention.getCanDisplayFeeData()){
	 		if(extention.distributor != null || extention.office != null){
	 			return true;
	 		}
	 		extention.errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
		}
		return false;
	}
	
	public boolean getParamPt(){	
		//パラメータにより、表示非表示・初期値を制御する
		isColi = false;
		isSpva = false;
		isBonus = false;
		String detailType = ApexPages.CurrentPage().getParameters().get(extention.KEY_PT);
		if(detailType != null && detailType.equals(extention.VAL_COLI)){
			isColi = true;
		}else if(detailType != null && detailType.equals(extention.VAL_SPVA)){
			isSpva = true;
		}else if(detailType != null && detailType.equals(extention.VAL_BONUS)){
			isBonus = true;
		}else{
			//パラメータが不正の場合はエラーページへ
			extention.errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		return true;
	}
	
}