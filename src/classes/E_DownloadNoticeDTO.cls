public with sharing class E_DownloadNoticeDTO{

	public class Notice implements Comparable{

		//帳票名
		public String formatName {get;set;}
		//通知月
		public String month {get;set;}
		//保険料払込期月
		public String ZDUEDATE {get;set;}
		//保険料払込猶予期間満了日
		public String LIMITDT {get;set;}
		//ソート用 通知年月
		public Integer YYYYMM {get;set;}
		//異動内容
		public String CONTENT {get; set;}

		//未入金通知
		private Notice(E_NCOL__c rec){
			this.formatName = E_DownloadNoticeConst.E_NCOLNM;
			this.month = getMonthLabel(rec.MM__c);
			this.ZDUEDATE = E_DownloadNoticeConst.EXCLUDED;
			//今回は表示対象外に変更
//			this.LIMITDT = getLIMITDT(rec.LIMITDT__c);
			this.LIMITDT = E_DownloadNoticeConst.EXCLUDED;
			this.YYYYMM = getYYYYMM(rec.YYYYMM__c);
		}
		//異動通知
		private Notice(E_AGPO__c rec){
			this.formatName = E_DownloadNoticeConst.E_AGPONM;
			this.month = getMonthLabel(rec.MM__c);
			this.ZDUEDATE = E_DownloadNoticeConst.EXCLUDED;
		//	this.LIMITDT = getLIMITDT(rec.LIMITDT__c);
			this.YYYYMM = getYYYYMM(rec.YYYYMM__c);
			this.CONTENT = '';
		}
		//予告通知
		private Notice(E_BILA__c rec){
			this.formatName = E_DownloadNoticeConst.E_BILANM;
			this.month = getMonthLabel(rec.MM__c);
			this.ZDUEDATE = getZDUEDATE(rec.ZDUEDATE__c);
			this.LIMITDT = E_DownloadNoticeConst.EXCLUDED;
			this.YYYYMM = getYYYYMM(rec.YYYYMM__c);
		}
		//請求通知
		private Notice(E_BILS__c rec){
			this.formatName = E_DownloadNoticeConst.E_BILSNM;
			this.month = getMonthLabel(rec.MM__c);
			this.ZDUEDATE = getZDUEDATE(rec.ZDUEDATE__c);
			this.LIMITDT = E_DownloadNoticeConst.EXCLUDED;
			this.YYYYMM = getYYYYMM(rec.YYYYMM__c);
		}
		//ソート
		public Integer compareTo(Object compareTo){
			Notice compareToNoticeDTO = (Notice)compareTo;

			if (this.YYYYMM == compareToNoticeDTO.YYYYMM) {
				Integer thisFormatNo = E_DownloadNoticeConst.FORMATNO_MAP.get(this.formatName);
				Integer compareFormatNo = E_DownloadNoticeConst.FORMATNO_MAP.get(compareToNoticeDTO.formatName);
				if (thisFormatNo == compareFormatNo) return 0;
				if (thisFormatNo > compareFormatNo) return 1;
				return -1;
			}
			if (this.YYYYMM < compareToNoticeDTO.YYYYMM) return 1;
			return -1;
		}
		//月表示
		private String getMonthLabel(Decimal month){
			return month == null ? '' : String.valueOf(month) + '月';
		}
		//保険料払込期月
		private String getZDUEDATE(Decimal month){
			return (month == null || (month > 99999999 &&10000000 > month)) ? '' : String.valueOf(month).substring(4,6) + '月';
		}

		//保険料払込猶予期間満了日をフォーマット
		public String getLIMITDT(String limitdt){
			if(String.isBlank(limitdt) || limitdt.length() <> 8) return '';
			return limitdt.substring(0,4) + '/' + limitdt.substring(4,6) + '/' + limitdt.substring(6,8);
		}
		//通知年月の数値化
		private Integer getYYYYMM(String yyyymm){
			//取得側でnull値は除外するため、null判定は行わない
			return Integer.valueOf(yyyymm.replace('/',''));
		}
	}
	
	//紐づく通知リスト
	public List<Notice> noticeList {get;private set;}
	
	//結合通知名
	public String joinNoticeName {get; private set;}

	//コンストラクタ
	public E_DownloadNoticeDTO(Id pid){
		noticeList = new List<Notice>();
		//初期処理
		init(pid);
	}

	//init
	public void init(Id pId){
		//関連する通知を取得
		E_Policy__c plcy = E_PolicyDao.getPeriodNoticesByPlcyId(pId);
		
		//
		Set<String> names = new Set<String>();

		//未入金通知
		for(E_NCOL__c rec :plcy.E_NCOLs__r){
			Notice ntc = new Notice(rec);
			noticeList.add(ntc);
			names.add(E_DownloadNoticeConst.E_NCOLNM);
		}
		//異動通知
		//通知年月をkeyとする異動通知のmap作成
		Map<String, List<E_AGPO__c>> agpoMap = new Map<String, List<E_AGPO__c>>();
		for(E_AGPO__c agpo :plcy.E_AGPOs__r){
			if(agpoMap.containsKey(agpo.YYYYMM__c) == false){
				agpoMap.put(agpo.YYYYMM__c, new List<E_AGPO__c>{agpo});
			}else{
				agpoMap.get(agpo.YYYYMM__c).add(agpo);
			}
		}
		for(List<E_AGPO__c> agpoList :agpoMap.values()){
			// 異動内容マップ
			Set<String> contents = new Set<String>();
			
			//表示用リスト作成
			E_AGPO__c baseRec = agpoList.get(0);
			Notice ntc = new Notice(baseRec);
			for(E_AGPO__c rec: agpoList){
				//区分（上段）が未納の場合、主募集人の保険料払込猶予期間満了日を表示する
				if(rec.KEVNTDSC01__c == E_DownloadNoticeConst.KEVNTDSC01_NONPAYMENT && rec.ZJINTSEQ__c == E_DownloadNoticeConst.ZJINTSEQ_01){
					ntc.LIMITDT = ntc.getLIMITDT(rec.LIMITDT__c);
				}
				// 異動内容に追加
				contents.add(rec.CONTENT__c);
			}
			// 異動内容セット
			ntc.CONTENT = String.join(new List<String>(contents), ',');
			
			noticeList.add(ntc);
			names.add(E_DownloadNoticeConst.E_AGPONM + '（' + ntc.CONTENT + '）');
		}
		//予告通知
		for(E_BILA__c rec :plcy.E_BILAs__r){
			Notice ntc = new Notice(rec);
			noticeList.add(ntc);
			names.add(E_DownloadNoticeConst.E_BILANM);
		}
		//請求通知
		for(E_BILS__c rec :plcy.E_BILSs__r){
			Notice ntc = new Notice(rec);
			noticeList.add(ntc);
			names.add(E_DownloadNoticeConst.E_BILSNM);
		}
		//ソート
		noticeList.sort();
		
		// 通知名の結合
		joinNoticeName = String.join(new List<String>(names), ',');
	}
	
	//通知の存在チェック
	public Boolean getIsNotice(){
		return noticeList.isEmpty() ? false : true;
	}
	
	
}