public with sharing class I_SurveyDao {

	public static I_Survey__c getSurveyBySurveyKey(String surveyKey){
		List<I_Survey__c> surveyList = new List<I_Survey__c>();
		surveyList = [SELECT 
						id,
						Name,
						OverView__c,
						PeriodFrom__c,
						PeriodTo__c,
						SurveyTarget__c, 
						(SELECT 
							id 
						FROM 
							Attachments 
						ORDER BY 
							createddate DESC),
						(SELECT 
							id, 
							Selection__c 
						FROM 
							I_SurveyQuestions__r) 
					FROM 
						I_Survey__c 
					WHERE upsertKey__c =: surveyKey];

		if(surveyList.size()<>1){
			return new I_Survey__c();
		}
		return surveyList[0];
	}
}