global with sharing class E_AnnuitantExtender extends E_AbstractViewExtender{
    private static final String PAGE_TITLE = '年金受取人等情報';
    E_AnnuitantController extension;

    public E_AnnuitantExtender(E_AnnuitantController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }

    /* init */
    global override void init(){
        // 契約者、被保険者、年金受取人情報を追加取得
        E_ContactDaoWithout.fillContactsToPolicy(extension.record, false, true,true,false, false);         
        // アクセスチェック
    	this.pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);            
    }

    public PageReference pageAction () {
    	return E_Util.toErrorPage(pageRef, null);
    }

}