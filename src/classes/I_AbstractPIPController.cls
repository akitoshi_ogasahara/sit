/**
 * Abstract PIP
 */
public abstract class I_AbstractPIPController extends I_AbstractController{

	protected E_Const.ID_KIND kind;
	protected Id authTargetId;

	// 保険契約ヘッダId
	protected String policyId;

	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}

	// 保険契約概要画面の使い方PDFのAttachmentId
	public String manualPdfId {get; set;}

	// MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}

	// データ基準日
	public String getDataSyncDateHead(){
		return E_Util.formatDataSyncDate();
	}

	// 手続書類URL取得
	public String getLibUrl(){
		I_GlobalNavController gnController = new I_GlobalNavController();
		return gnController.libSubNavItems.getLibInfoNavItem().getDestUrl();
	}

	/**
	 * ISS判定
	 */
	public Boolean getIsIss() {
		return E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId());
	}

	/**
	 * 特別勘定ボタン
	 */
	public PageReference doSendSpecialAccount(){
		PageReference pf = Page.E_SpecialAccount;
		pf.getParameters().put('id', policyId);
		pf.getParameters().put('type', E_Const.FROM_COLI);
		return pf;
	}

	/**
	 * 保険契約概要画面の使い方PDF
	 */
	public PageReference openManualPdf(){
		PageReference attPDF = new PageReference('/servlet/servlet.FileDownload?file=' + this.manualPdfId);
		return attPDF;
	}

	/**
	 * Contructor
	 */
	public I_AbstractPIPController(){

	}

	/**
	 * init
	 */
	protected virtual override PageReference init(){
		PageReference pr = super.doAuth(kind, authTargetId);
		if(pr!=null){
			return pr;
		}

		//IRIS選択でない場合は、E_Homeへ遷移（GuestユーザもI_Abstractページの表示OKとする）
		if(!iris.getIsIRIS() && !access.isGuest()){
			pr = Page.E_AppSwitch;
			pr.getParameters().put(I_Const.APPSWITCH_URL_PARAM_APP, I_Const.APP_MODE_NNLINK);		//?app=nnlink
			return pr;
		}

		// 保険契約概要画面の使い方PDFのAttachmentId取得
		this.manualPdfId = getManualPdfId();

		return null;
	}

	/**
	 * 保険契約概要画面の使い方PDFのAttachmentId取得
	 */
	private String getManualPdfId(){
		String aId = '';
		I_ContentMaster__c content = I_ContentMasterDao.getRecordByUpsertKey('セクション|保険契約概要_画面の使い方PDF|');
		if(content != null){
			for(Attachment att : content.Attachments){
				aId = att.Id;
				break;
			}
		}
		return aId;
	}

}