public with sharing class E_SVFPFDao {

    /**
     * 保険契約IDをキーに個人保険用リストを取得する
     * @param policyId: 保険契約ID
     * @return List<E_SVFPF__c>: 変額保険ファンド情報リスト
     */
    public static List<E_SVFPF__c> getRecsByPolicyIdForCOLI(Id policyId){
        // 条件：特別勘定.設定日 <= 本日日付、特別勘定.FUND表示フラグ = true、特別勘定.ファンドコード <> 'V002'
        return [Select
                    Id, Name, ZREVAMT__c, ZHLDPERC__c,
                    E_SVCPF__c, E_SVCPF__r.ZFUNDNAMEUNION__c, E_SVCPF__r.ZEFUNDCD__c
                From
                    E_SVFPF__c
                Where
                    E_Policy__c = :policyId
                And
                    (E_SVCPF__r.CURRFROM__c != null And E_SVCPF__r.CURRFROM__c <=: E_Util.getSysDate(null))
                And
                    E_SVCPF__r.FLAG01__c = true
                And
                    E_SVCPF__r.ZCATFLAG__c =: E_Const.FUND_KIND_COLI
             //   And
             //       E_SVCPF__r.ZEFUNDCD__c <>: E_Const.NOT_FUND_COLI
                Order by
                    E_SVCPF__r.ZEFUNDCD__c
               ];
    }

    /**
     * 保険契約IDをキーにリストを取得する
     * @param policyId: 保険契約ID
     * @return List<E_SVFPF__c>: 変額保険ファンド情報リスト
     */
    public static List<E_SVFPF__c> getRecsByPolicyId(Id policyId){
        return [Select
                    Id, Name, ZHLDPERC__c, ZREVAMT__c, E_SVCPF__c
                From
                    E_SVFPF__c
                Where
                    E_Policy__c = :policyId
                Order by
                    ZSEQNO__c, E_SVCPF__r.ZEFUNDCD__c
               ];
    }

    /**
     * 特別勘定IDをキーにリストを取得する
     * @param policyId: 保険契約ID
     * @return List<E_SVFPF__c>: 変額保険ファンド情報リスト
     */
    public static List<E_SVFPF__c> getRecsBySVCPFIds(Id policyId, Set<Id> ids){
        return [Select
                    Id, Name, ZSEQNO__c, ZHLDPERC__c, E_SVCPF__c, ZREVAMT__c,
                    E_SVCPF__r.ZFSHFROM__c, E_SVCPF__r.ZFSDFROM__c, E_SVCPF__r.ZFSMFROM__c, E_SVCPF__r.ZFSHTO__c, E_SVCPF__r.ZFSDTO__c, E_SVCPF__r.ZFSMTO__c,
                    E_SVCPF__r.FLAG02__c, E_SVCPF__r.FLAG01__c, E_SVCPF__r.ZVINDEX__c, E_SVCPF__r.ZEFUNDCD__c, E_SVCPF__r.ZCATFLAG__c, E_SVCPF__r.ZFUNDNAMEUNION__c,
                    E_SVCPF__r.ZFUNDNAM01__c, E_SVCPF__r.ZFUNDNAM02__c, E_SVCPF__r.ZFUNDNAM03__c, E_SVCPF__r.ZFUNDNAM04__c, E_SVCPF__r.ZEQTYRTO__c, E_SVCPF__r.ZCOLORCD__c,
                    E_SVCPF__r.ZSWENDDT__c, E_SVCPF__r.CURRFROM__c, E_SVCPF__r.CURRTO__c
                From
                    E_SVFPF__c
                Where
                    E_Policy__c = :policyId
                And
                    E_SVCPF__c in :ids
                Order by
                    ZSEQNO__c, E_SVCPF__r.ZEFUNDCD__c
               ];
    }

    /**
     * 保険契約IDをキーに個人保険用リストを取得する:繰入比率画面呼び出し
     * @param policyId: 保険契約ID
     * @return List<E_SVFPF__c>: 変額保険ファンド情報リスト
     */
    public static List<E_SVFPF__c> getRecsByPolicyIdForCOLIFrInwardTransfer(Id policyId){
        // 条件：特別勘定.設定日 <= 本日日付、特別勘定.FUND表示フラグ = true、特別勘定.ファンドコード <> 'V002'
        return [Select
                        Id,                 //id
                        Name,               //NAME
                        E_SVCPF__r.ZFUNDNAMEUNION__c,       //【特別勘定】.[ファンド名]
                        ZINVPERC__c,            //ZINVPERC__c
                        E_SVCPF__c          //特別勘定
                From
                    E_SVFPF__c
                Where
                    E_Policy__c = :policyId
                And
                    (E_SVCPF__r.CURRFROM__c != null And E_SVCPF__r.CURRFROM__c <=: E_Util.getSysDate(null))
                And
                    E_SVCPF__r.FLAG01__c = true
                And
                    E_SVCPF__r.ZCATFLAG__c =: E_Const.FUND_KIND_COLI
                And
                    E_SVCPF__r.ZEFUNDCD__c <>: E_Const.NOT_FUND_COLI
                Order by
                    ZSEQNO__c, E_SVCPF__r.ZEFUNDCD__c
               ];
    }
}