public class MD_DoctorDTO {
	public Boolean isCheck {get; set;}
	public Integer no {get; set;}
	public Integer pno {get; set;}
	public MD_Doctor__c doctor {get; set;}

	/* Constructor */
	public MD_DoctorDTO(Integer no, MD_Doctor__c doctor) {
		this.isCheck = false;
		this.no = no;
		this.doctor = doctor;
	}
}