@istest
public class TestE_UPTable005Extension {
	private static E_UPFPF__c upfpf;
    private static E_UPBPF__c eupbpf;
    @testSetup
    private static void setup() {
        // Create common test Message
        Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get('MSG_DISCLAIMER');
        final String TYPE_MESSAGE = 'メッセージ';
        final String MSG_NAME = 'name';
        List<E_MessageMaster__c> src = new List<E_MessageMaster__c>{
              new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|006', Value__c='開始日には、{0}（特別勘定の設定開始日）以前の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|007', Value__c='終了日には、{0} 以降の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|008', Value__c='20年を超えて期間を選択することはできません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|009', Value__c='終了日には、{0} 以降の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|010', Value__c='終了日には、{0} 以前の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|011', Value__c='[{0}]は必須です。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|012', Value__c='[{0}]はyyyy/MM/dd 形式で入力して下さい。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|014', Value__c='選択期間を選択してください。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|018', Value__c='一定時間操作がありませんでした。「ユニットプライス/騰落率トップ画面」を閉じて再度アクセスして下さい。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|002', Value__c='ERR|002')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UP5|E01', Value__c='UP5|E01')
        };
        insert src;
    }
    private static Map<String, Map<String, Id>> rMap;
    private static Map<String, Map<String, Id>> getRecTypeIdMap(){
        if(rMap != null) return rMap;
        rMap = new Map<String, Map<String, Id>>();
        for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
            if(rMap.containsKey(rt.sObjectType)){
                rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
            }else{
                rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
            }
        }
        return rMap;
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    public static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
    
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testInit005() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = '20140404';
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            resultPage = Extension.init005();
            
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
        
    }
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testGetIndexTableHTML() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        String resultHtml;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = '20140404';
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            Extension.init005();
            resultHtml = Extension.getIndexTableHTML();
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assert(String.isNotBlank(resultHtml));
    }

    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testDownloadCSV() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = '20140404';
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            Extension.init005();
            Extension.downloadCSV();
            //テスト終了
            Test.stopTest();
            
        	//※正常処理
        	system.assert(String.isNotBlank(Extension.downloadUrl));
        }
    }
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testChangePage() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = '20140404';
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            Extension.init005();
            resultPage = Extension.changePage();
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertequals(null,resultPage);
    }
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testNonDLCondition() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = '20140404';
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            resultPage = Extension.init005();
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assert(resultPage.getUrl().contains(page.up999.getUrl()));
        system.assertEquals('ERR|002',resultPage.getParameters().get('code'));
    }
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testUPIndexNotExistsException() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            resultPage = Extension.init005();
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assert(resultPage.getUrl().contains(page.up999.getUrl()));
        system.assertEquals('UP5|E01',resultPage.getParameters().get('code'));
    }

    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testFromTerm() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        E_UPTable005Extension Extension;
        String html;
        String toterm;
        string fundDate = '20140404';
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = fundDate;
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;


            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/04/04';
            controller.freeSelectEndDate = '2014/04/04';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            Extension = new E_UPTable005Extension(controller);
            resultPage = Extension.init005();
            html = Extension.getIndexTableHTML();
            toterm = controller.dlCondi.toterm;
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
        
        system.assertequals(fundDate,toterm);

    }
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testToterm() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        E_UPTable005Extension Extension;
        String html;
        String fromterm;
        string fundDate = '20140404';
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            E_UPXPF__c upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = fundDate;
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            insert upxpf;
            // ユニットプライスの登録
            upxpf = new E_UPXPF__c();
            upxpf.E_UPFPF__c = upfpf.Id;
            upxpf.ZFUNDDTE__c = '20140603';
            upxpf.dataSyncDate__c = Date.valueOf('2014-04-02');
            upxpf.ZVINDEX__c = 10.9488;
            insert upxpf;


            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2014/04/04';
            controller.freeSelectEndDate = '2015/04/04';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            Extension = new E_UPTable005Extension(controller);
            resultPage = Extension.init005();
            html = Extension.getIndexTableHTML();
            fromterm = controller.dlCondi.fromterm;
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
        
        system.assertequals(fundDate,fromterm);

    }




    private static void createData(){
        E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
        log.DataSyncStartDate__c = Date.valueOf('2014-10-01');
        log.DataSyncEndDate__c = Date.valueOf('2014-10-01');
        log.InquiryDate__c = Date.valueOf('2014-12-31');
        insert log;
        // ############### テストデータの登録 ###############
        // 特別勘定の登録
        E_SVCPF__c svcpf = new E_SVCPF__c();
        insert svcpf;
        // ファンド群マスタの登録
        E_FundMaster__c fundMaster = new E_FundMaster__c();
        insert fundMaster;
        // 選択可能ファンドの登録
        E_SFGPF__c sfgpf = new E_SFGPF__c();
        sfgpf.E_SVCPF__c = svcpf.Id;
        sfgpf.FundMaster__c = fundMaster.Id;
        insert sfgpf;
        // ユニットプライス基本情報の作成
        eupbpf = new E_UPBPF__c();
        eupbpf.ZPRDCD__c = 'V9999';
        eupbpf.ZDSPFLAG__c = '1';
        eupbpf.E_FundMaster__c = fundMaster.Id;
        insert eupbpf;
        // ユニットプライス表示用ファンドの登録
        upfpf = new E_UPFPF__c();
        upfpf.SVCPF__c = svcpf.Id; 
        insert upfpf;
        
    }
    
    
}