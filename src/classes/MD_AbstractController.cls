/**
 *  ドクターリスト  superクラス
 */
public abstract class MD_AbstractController extends E_AbstractController {
	// ログ管理
	public E_LogCondition.DoctorCondi logCondition {get; set;}

	// 社員フラグ
	public Boolean isEmployee {get; private set;}

	/**
	 *  Constructor
	 */
	public MD_AbstractController() {
		logCondition = new E_LogCondition.DoctorCondi();
		access = E_AccessController.getInstance();
		isEmployee = access.isEmployee();
	}

	/**
	 * ページ名取得
	 */
	public String getPageName() {
		return ApexPages.currentPage().getUrl().split('/')[2].split('\\?')[0];
	}

	/**
	 * アクセスログ登録
	 */
	public PageReference insertAccessLog() {
		// E_Log__c
		E_Log__c log = createLog();

		try {
			insert log;
			return null;

		} catch (Exception e) {
			log = new E_Log__c();
			log.IsError__c = true;
			log.AccessUser__c = logCondition.accessUser.Id;
			log.AccessPage__c = logCondition.accessPage;
			log.AccessDatetime__c = logCondition.accessDateTime;
			log.Detail__c = E_Util.null2Blank(logCondition.detail) + e.getMessage();
			log.RecordId__c = E_Util.null2Blank(logCondition.recId);
			insert log;

			// エラーページ
			PageReference pr = Page.E_MDError;
			return pr;
		}
	}

	/**
	 * アクセスエラーログ登録
	 */
	public PageReference insertErrorLog(String errMsg){
		// E_Log__c
		E_Log__c log = createLog();
		log.IsError__c = true;
		log.Detail__c = E_Util.null2Blank(logCondition.detail) + errMsg;
		insert log;

		// エラーページ
		PageReference pr = Page.E_MDError;
		return pr;
	}

	/**
	 * ConditonからNNLinkログ生成
	 */
	private E_Log__c createLog(){
		E_Log__c elog = new E_Log__c();
		elog.IsError__c = logCondition.isError;
		elog.AccessUser__c = logCondition.accessUser.Id;
		elog.AccessPage__c = logCondition.accessPage;
		elog.AccessDatetime__c = logCondition.accessDateTime;
		elog.Detail__c = E_Util.null2Blank(logCondition.detail);
		elog.RecordId__c = E_Util.null2Blank(logCondition.recId);
		elog.Accept__c = E_Util.null2Blank(logCondition.headAccept);
		elog.Accept_Encoding__c = E_Util.null2Blank(logCondition.headAcceptEncoding);
		elog.Accept_Language__c = E_Util.null2Blank(logCondition.headAcceptLanguage);
		elog.CipherSuite__c = E_Util.null2Blank(logCondition.headCipherSuite);
		elog.Connection__c = E_Util.null2Blank(logCondition.headConnection);
		elog.Host__c = E_Util.null2Blank(logCondition.headHost);
		elog.User_Agent__c = E_Util.null2Blank(logCondition.headUserAgent);
		elog.X_Salesforce_Forwarded_To__c = E_Util.null2Blank(logCondition.headXSalesforceForwardedTo);
		elog.X_Salesforce_SIP__c = E_Util.null2Blank(logCondition.headXSalesforceSIP);
		elog.X_Salesforce_VIP__c = E_Util.null2Blank(logCondition.headXSalesforceVIP);
		return elog;
	}
}