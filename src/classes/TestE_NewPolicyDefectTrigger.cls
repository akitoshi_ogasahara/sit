@isTest
private class TestE_NewPolicyDefectTrigger {
	
	/**
	 * ステータス発信日テスト
	 * 個人契約
	 */
	private static testMethod void statusDateTest_001() {
		Datetime d1 = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		Test.startTest();
		E_NewPolicy__c newPlcy = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);

		//JST変換
		E_NewPolicy__c result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		system.assertEquals(d1.addHours(-9),result.StatusDate__c);

		//30分後　ステータスの更新はなし
		Datetime d2 = d1.addMinutes(30);
		newPlcy.SyncDate__c = d2;
		update newPlcy;

		result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d1.addHours(-9),result.StatusDate__c);

		//不備発信 ステータス更新あり
		E_NewPolicyDefect__c defect1 = TestUtil_AMS.createNewPolicyDefect(false,newPlcy.id,'AA1','O');
		List<E_NewPolicyDefect__c> defList = new List<E_NewPolicyDefect__c>{defect1};
		upsert defList;

		result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d2.addHours(-9),result.StatusDate__c);

		//30分後　ステータスの更新なし
		Datetime d3 = d2.addMinutes(30);
		newPlcy.SyncDate__c = d3;
		update newPlcy;
		result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d2.addHours(-9),result.StatusDate__c);

		//不備発信 ステータス更新なし
		defect1.FUPSTAT__c = 'F';
		E_NewPolicyDefect__c defect2 = TestUtil_AMS.createNewPolicyDefect(false,newPlcy.id,'AA1','O');
		defList = new List<E_NewPolicyDefect__c>{defect1,defect2};
		upsert defList;
		Test.stopTest();

		result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d2.addHours(-9),result.StatusDate__c);
	}	
}