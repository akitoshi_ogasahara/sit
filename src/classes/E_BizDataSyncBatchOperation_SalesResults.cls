public with sharing class E_BizDataSyncBatchOperation_SalesResults extends E_BizDataSyncBatchOperation {

	/**
	 * getQueryLocator for Batch start method
	 */
	public override Database.QueryLocator getQueryLocator(){
//		errList = new List<String>();
		String businessDate = E_Util.date2YYYYMMDD([Select id,InquiryDate__c From E_BizDataSyncLog__c Where kind__c =: 'F' ORDER BY CreatedDate desc limit 1].InquiryDate__c);

		return Database.getQueryLocator(
					[SELECT 
						Id  
						,brCode__c 
						,parentAccountCode__c 
						,AccountCode__c 
						,AgentCode__c 
						,Hierarchy__c 
						,ParentAccount__c
					From 
						E_AgencySalesResults__c 
					Where 
						BusinessDate__c =: businessDate 
					ORDER BY ParentAccountCode__c,AccountCode__c,AgentCode__c]);
	}
	
	/**
	 * execute
	 * 		batchのExecuteの本体ロジック
	 */
	public override void execute(List<Sobject> records){

		// Shareクラス生成
		E_AgencySalesResultsShare npShare = new E_AgencySalesResultsShare(new Set<Sobject>(records));
		
		// Share設定
		npShare.execute();
	}
}