/**
 * CR_ExportAgencyBizBooksController
 */
@isTest
private class TestCR_ExportAgencyBizBooksController {

    static testMethod void test_constructor01() {
        Test.startTest();
        
        CR_ExportAgencyBizBooksController controller = new CR_ExportAgencyBizBooksController();
        List<E_CSVColumnDescribe.BaseField> resultList = controller.addColumn(new E_CSVColumnDescribe.BaseField('Test'));
        
        Test.stopTest();
        
        System.assertEquals('CR_Agency__c', controller.mainSObjectAPIName());
        System.assertEquals('MRUnit__c', resultList[0].fieldAPI);
        System.assertEquals('MRName__c', resultList[1].fieldAPI);
        System.assertEquals('AgencyCode__c', resultList[2].fieldAPI);
        System.assertEquals('AgencyName__c', resultList[3].fieldAPI);
        System.assertEquals('LastRequestBook__c', resultList[4].fieldAPI);
        System.assertEquals('BusinessStatus__c', resultList[5].fieldAPI);
    }
}