@isTest
private class TestE_EmailTemplateDao {

	private static List<EmailTemplate> createData(Integer recSize, Boolean isInsert){
		List<EmailTemplate> records = new List<EmailTemplate>();
		for(Integer i=1; i<=recSize; i++){
			records.add(new EmailTemplate(
				 Subject = 'testSubject'+i
				,Body = 'testBody'+i
				,DeveloperName = 'testTemp'+i
				,Name = 'テストメールテンプレート'+i
				,Encoding = 'UTF-8'
				,TemplateType = 'text'
				,FolderId = UserInfo.getUserId()
				,IsActive = TRUE)
			);
		}
		if(isInsert) insert records;
		return records;
	}

	@isTest static void getRecByDeveloper_test001() {
		List<EmailTemplate> temp = createData(2, true);

		Test.startTest();
			EmailTemplate result = E_EmailTemplateDao.getRecByDeveloper('testTemp1');
		Test.stopTest();
		System.assertEquals(temp[0].Id, result.Id);
	}

/*
	@isTest static void getRecByDeveloper_test002() {
		createData(1, true);

		Test.startTest();
			EmailTemplate result = E_EmailTemplateDao.getRecByDeveloper('testTemp2');
		Test.stopTest();
		System.assertEquals(null, result);
	}
*/

}