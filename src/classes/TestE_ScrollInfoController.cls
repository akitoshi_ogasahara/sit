@isTest
private class TestE_ScrollInfoController {
	
	static testMethod void test_isRenderedDis_True() {
		
		//メニューマスタ作成
		E_MenuMaster__c menuMaster = TestE_TestUtil.createMenuMaster(true, 'メニューマスタ名', 'menu_master_key','E_Menu', false);
		E_MessageMaster__c messageMaster = TestE_TestUtil.createMessageMaster(true, 'メッセージマスタ名', menuMaster.Id, 'CMS', E_Const.E_MEGTYPE_CMS, 'key');
		
		Test.startTest();
		E_ScrollInfoController controller = new E_ScrollInfoController();

        controller.isRenderedDis = true;
		controller.scrollMenuMasterKey = 'menu_master_key';
		Test.stopTest();
		
		System.assertEquals(1, controller.cmsContents.size());
		System.assertEquals(menuMaster.Id, controller.menu.Id);
		System.assertEquals(messageMaster.Id, controller.cmsContents[0].Id);
	}
	
	static testMethod void test_isRenderedDis_False() {
		
		//メニューマスタ作成
		E_MenuMaster__c menuMaster = TestE_TestUtil.createMenuMaster(true, 'メニューマスタ名', 'menu_master_key','E_Menu', false);
		E_MessageMaster__c messageMaster = TestE_TestUtil.createMessageMaster(true, 'メッセージマスタ名', menuMaster.Id, 'CMS', E_Const.E_MEGTYPE_CMS, 'key');
		
		Test.startTest();
		E_ScrollInfoController controller = new E_ScrollInfoController();

        controller.isRenderedDis = false;
		controller.scrollMenuMasterKey = 'menu_master_key';
		Test.stopTest();
		
		System.assertEquals(null, controller.cmsContents);
		System.assertEquals(null, controller.menu);
	}
	
}