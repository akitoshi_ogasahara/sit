public class I_FileSearchResult implements Comparable {

	public static final Integer VIEW_MAX_LENGTH = 40;
	public static final String SORT_TYPE_PAGE_NAME = 'pagename';
	public static final String SORT_TYPE_FILE_NAME = 'filename';
	public static final String SORT_TYPE_DOCUMENT_NO = 'documentNo';

	private static String sortType = SORT_TYPE_FILE_NAME;
	private static Boolean sortIsAsc = true;

	public I_CMSComponentController cmsCompCtrl {get; set;}
	public I_ContentMaster__c record {get; set;}
	public String fileName {get; set;}
	public String fileUrl {get; set;}
	public String documentNo {get;set;}
	public String pageName {get; set;}
	public String pageUrl {get; set;}
	private I_KeywordMarker marker;

	public void setMarker(I_KeywordMarker marker) {
		this.marker = marker;
	}

	public String getFilePath() {
		return this.cmsCompCtrl.record.filePath__c;
	}

	public String getClickAction() {
		return this.cmsCompCtrl.record.clickAction__c;
	}

	public Boolean isMatched() {
		Boolean result = false;
		//Boolean result2 = false;
		if (this.marker != null) {
			//result = this.marker.wholeMatches(new List<String>{this.fileName});
			//result2 = this.marker.wholeMatches(new List<String>{this.documentNo});

			if(this.marker.wholeMatches(new List<String>{this.fileName}) || this.marker.wholeMatches(new List<String>{this.documentNo})) result = true;
		}
		return result;
	}

	public String getMarkedFileName() {
		return this.marker != null ? this.marker.mark(this.fileName, -1) : this.fileName;
	}

	public String getMarkedDocumentNo() {
		return this.marker != null ? this.marker.mark(this.documentNo, -1) : this.documentNo;
	}

	public Boolean hasAccess() {
		return I_ContentSearchResult.accesses(this.cmsCompCtrl.record);
	}

	public Integer compareTo(Object compareTo) {
		I_FileSearchResult row = (I_FileSearchResult)compareTo;
		Integer result = 0;
		if (I_FileSearchResult.sortBy(SORT_TYPE_FILE_NAME)) {
			result = I_Util.compareToString(this.fileName, row.fileName);
		} else if (I_FileSearchResult.sortBy(SORT_TYPE_PAGE_NAME)) {
			result = I_Util.compareToString(this.pageName, row.pageName);
		}else if (I_FileSearchResult.sortBy(SORT_TYPE_DOCUMENT_NO)) {
			result = I_Util.compareToString(this.documentNo, row.documentNo);
		}
		return I_FileSearchResult.isAsc() ? result : result * -1;
	}

	public static Boolean isContents(I_ContentMaster__c record) {
		return record.clickAction__c == 'DL(Contents)';
	}

	public static Boolean isNNLink(I_ContentMaster__c record) {
		return record.clickAction__c == 'DL(NNLink)';
	}

	public static Boolean isAttachment(I_ContentMaster__c record) {
		return record.clickAction__c == 'DL(Attachment)';
	}

	public static Boolean isChatterFile(I_ContentMaster__c record) {
		return record.clickAction__c== 'DL(ChatterFile)';
	}

	public static Boolean isWebContents(I_ContentMaster__c record) {
		return record.clickAction__c== 'URL(Webコンテンツ)';
	}

	public static Boolean isURL(I_ContentMaster__c record) {
		return record.clickAction__c== 'URL';
	}

	public static Boolean sortBy(String sortType) {
		return I_FileSearchResult.sortType == sortType;
	}

	public static Boolean isAsc() {
		return I_FileSearchResult.sortIsAsc;
	}

	public static void setSortOption(String sortType, Boolean sortIsAsc) {
		I_FileSearchResult.sortType = sortType;
		I_FileSearchResult.sortIsAsc = (sortIsAsc == true);
	}

	public static I_FileSearchResult build(I_ContentMaster__c record, I_CMSComponentController cmsCompCtrl) {
		I_FileSearchResult result = new I_FileSearchResult();
		result.record = record;
		result.cmsCompCtrl = cmsCompCtrl;
		result.fileName = String.isNotBlank(record.name) ? record.name : '';
		result.documentNo = String.isNotBlank(record.DocumentNo__c) ?record.DocumentNo__c: '';
		if (I_FileSearchResult.isAttachment(cmsCompCtrl.record) && cmsCompCtrl.getDLAttachment() != null) {
			result.fileUrl = cmsCompCtrl.getDLAttachment().Id;
		} else if (I_FileSearchResult.isNNLink(cmsCompCtrl.record) && cmsCompCtrl.getDLAttachment() != null) {
			result.fileUrl = cmsCompCtrl.getDLAttachment().Id;
		} else if (I_FileSearchResult.isChatterFile(cmsCompCtrl.record)) {
			result.fileUrl = cmsCompCtrl.getChatterFileDownloadUrl();
		} else if (I_FileSearchResult.isContents(cmsCompCtrl.record) && cmsCompCtrl.getDLAttachment() != null) {
			result.fileUrl = cmsCompCtrl.getDLAttachment().Id;
		}
		if (record.FeatureFlag__c) {
			// 特集の場合
			I_PageMaster__c pageRec;
			if (String.isNotBlank(record.Page__c)) {
				pageRec = record.Page__r;
			} else if (String.isNotBlank(record.ParentContent__r.Page__c)) {
				pageRec = record.ParentContent__r.Page__r;
			}
			if (pageRec != null) {
				result.pageName = String.isNotBlank(pageRec.name) ? pageRec.name : '';
				PageReference ref = Page.IRIS_Feature;
				ref.getParameters().put('pgkey', pageRec.page_unique_key__c);
				ref.setAnchor(EncodingUtil.urlEncode(cmsCompCtrl.getDomId(), 'utf-8'));
				//ref.setAnchor(cmsCompCtrl.getDomId());
				result.pageUrl = ref.getUrl();
			}
		//} else if (record.ParentContent__c != null && record.ParentContent__r.Name == 'セールスツール') {
		} else if (record.ParentContent__c != null && record.ParentContent__r.Name == '販売促進資料') {
			//result.pageName = 'セールスツール';
			result.pageName = '販売促進資料';
			PageReference ref = new PageReference('IRIS_Lib_Tools');
			ref.getParameters().put(I_Const.URL_PARAM_DOC_NUM, cmsCompCtrl.record.DocumentNo__c);
			// マルチバイト対策のためURLENCODEを行う
			ref.setAnchor(EncodingUtil.urlEncode(cmsCompCtrl.getDomId(), 'utf-8'));
			//ref.setAnchor(cmsCompCtrl.getDomId());
			result.pageUrl = ref.getUrl();
		} else if (record.ParentContent__c != null && record.ParentContent__r.Page__c != null) {
			I_PageMaster__c pageRec = cmsCompCtrl.record.ParentContent__r.Page__r;
			result.pageName = String.isNotBlank(pageRec.name) ? pageRec.name : '';
			I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(pageRec.Menu__c);
			if (menuItem != null) {
				result.pageUrl = menuItem.getDestUrl();
				if (String.isNotBlank(result.pageUrl) && String.isNotBlank(cmsCompCtrl.getDomId())) {
					// マルチバイト対策のためURLENCODEを行う
					result.pageUrl += '#' + EncodingUtil.urlEncode(cmsCompCtrl.getDomId(), 'utf-8');
					//result.pageUrl += '#' + cmsCompCtrl.getDomId();
				}
			}
		} else if (record.Page__c != null) {
			I_PageMaster__c pageRec = cmsCompCtrl.record.Page__r;
			result.pageName = String.isNotBlank(pageRec.name) ? pageRec.name : '';
			I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(pageRec.Menu__c);
			if (menuItem != null) {
				result.pageUrl = menuItem.getDestUrl();
				if (String.isNotBlank(result.pageUrl) && String.isNotBlank(cmsCompCtrl.getDomId())) {
					// マルチバイト対策のためURLENCODEを行う
					result.pageUrl += '#' + EncodingUtil.urlEncode(cmsCompCtrl.getDomId(), 'utf-8');
					//result.pageUrl += '#' + cmsCompCtrl.getDomId();
				}
			}

		// 資料発送にも掲載がある場合、資料・動画側を優先して検索結果に表示する
		} else if (record.ParentContent__c != null && I_Const.PAMPHLET_CATEGORIES.contains(record.ParentContent__r.Name)) {
			result.pageName = 'パンフレット等(' + record.ParentContent__r.Name + ')';
			PageReference ref = new PageReference('IRIS_Pamphlet');
			ref.getParameters().put(I_Const.URL_PARAM_DOC_NUM, cmsCompCtrl.record.FormNo__c);
			// マルチバイト対策のためURLENCODEを行う
			ref.setAnchor(EncodingUtil.urlEncode(cmsCompCtrl.getDomId(), 'utf-8'));
			//ref.setAnchor(cmsCompCtrl.getDomId());
			result.pageUrl = ref.getUrl();

		//20180214 資料発送申込対応 START
		} else if(I_Const.DR_DOCUMENT_CATEGORIES.contains(record.DocumentCategory__c)) {
			E_AccessController accessor = E_AccessController.getInstance();
			if(accessor.isAgent() && accessor.user.Contact.Account.CannotBeOrder__c){
				return result;
			}

			Id menuId = E_IRISHandler.getInstance().irisMenus.MenuItemsByKey.get('document_order').record.Id;
			result.pageUrl = I_Const.DOCUMENTREQUESTURL + menuId;
			if(record.FormNo__c!=null)result.pageUrl += '#' + EncodingUtil.urlEncode(record.FormNo__c, 'utf-8');
			result.pageName = I_Const.DOCUMENT_REQUEST + '('+record.DocumentCategory__c+')';
			result.fileName = result.fileName;
			//クリックアクション「URL」の場合
			if(I_FileSearchResult.isURL(cmsCompCtrl.record)){
				result.fileUrl = record.LinkURL__c;
			}
		}
		//20180214 資料発送申込対応 END
		return result;

	}
}