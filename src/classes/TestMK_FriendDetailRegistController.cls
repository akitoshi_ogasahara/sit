@isTest
private class TestMK_FriendDetailRegistController {

	private static testMethod void testinit(){

		MK_FriendDetailRegistController cntrllr = new MK_FriendDetailRegistController();

/*		//正常系 ID有り
		cntrllr.MK_leadTMP_Id = 'TESTID';
		cntrllr.init();
		System.assert(!cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,'TESTID');*/

		//ERROR: URLParam無し
		cntrllr.init();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG09_URLPARAM_ID);

		//ERROR: URLParam不正値
		PageReference pr = Page.MK_FriendDetailRegist;
		pr.getParameters().put('id','TESTID');
		Test.setCurrentPage(pr);
		cntrllr.init();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG09_URLPARAM_ID);

		//ERROR: URLparam対応レコード無し
		Profile pro = [SELECT Id FROM Profile where name = 'E_PartnerCommunity'];
		Account acc = TestMK_TestUtil.createAccount(true,'Test取引先');
		Contact con = TestMK_TestUtil.createContact(true, 'Test募集人', acc.id);
		System.debug('プロファイル'+pro);
		User user = TestMK_TestUtil.createUser(true, con.Name, pro.id, con.Id);
		System.debug('User'+user);
		CampaignManagement__c cm1 = TestMK_TestUtil.createCampaignManagement(true,system.today(),system.today());
		MK_CampaignTarget__c ct1 = TestMK_TestUtil.createMK_CampaignTarget(true,cm1.id,con.Id);
		MK_leadTMP__c ltmp1 = TestMK_TestUtil.createMK_leadTMP(true,ct1.id);
		pr.getParameters().put('request',E_EncryptUtil.getEncryptedString(ltmp1.id));
		Test.setCurrentPage(pr);
		delete ltmp1;
		cntrllr.init();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG09_URLPARAM_ID);

		//ERROR: Lead作成済み
		MK_leadTMP__c ltmp2 = TestMK_TestUtil.createMK_leadTMP(true,ct1.id,true);
		pr.getParameters().put('request',E_EncryptUtil.getEncryptedString(ltmp2.id));
		Test.setCurrentPage(pr);
		cntrllr.init();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG12_IS_CREATE_LEAD);

		//ERROR: キャンペーン管理が期間外
		CampaignManagement__c cm2 = TestMK_TestUtil.createCampaignManagement(true,system.today()-1,system.today()-1);
		MK_CampaignTarget__c ct2 = TestMK_TestUtil.createMK_CampaignTarget(true,cm2.id,con.Id);
		MK_leadTMP__c ltmp3 = TestMK_TestUtil.createMK_leadTMP(true,ct2.id);
		pr.getParameters().put('request',E_EncryptUtil.getEncryptedString(ltmp3.id));
		Test.setCurrentPage(pr);
		cntrllr.init();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG13_IS_NOT_ACTIVE_CAMPAIGN);

		//ERROR パラメータ不正
		pr.getParameters().put('request',E_EncryptUtil.getEncryptedString(cm2.id));
		Test.setCurrentPage(pr);
		cntrllr.init();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.MK_leadTMP_Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG09_URLPARAM_ID);

		//正常系
		MK_leadTMP__c ltmp4 = TestMK_TestUtil.createMK_leadTMP(true,ct1.id,false);
		pr.getParameters().put('request',E_EncryptUtil.getEncryptedString(ltmp4.id));
		Test.setCurrentPage(pr);
		cntrllr.init();
		System.assert(!cntrllr.pageMessages.hasMessages());

	}
	private static testMethod void TestdoConfirm_001() {


		MK_FriendDetailRegistController cntrllr = new MK_FriendDetailRegistController();

		//ERROR: 入力チェック
		cntrllr.pageMessages.clearMessages();
		cntrllr.friend = TestMK_TestUtil.createLead(false,'テスト契約者','TEST株式会社ABC','ﾃｽﾄｹｲﾔｸｼｬ1','testaddress01@test.nn.baton','北海道','');
		MK_CampaignTarget__c cm = TestMK_TestUtil.createMK_CampaignTarget(true);
		cntrllr.tmpFriend = TestMK_TestUtil.createMK_leadTMP(true, cm.Id, '姓','セイ','名','メイ');
		cntrllr.doConfirm();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG02_IS_NOT_ENTRY);
		//ERROR: メール判定 再確認未入力
		cntrllr.friend = TestMK_TestUtil.createLead(false,'テスト契約者','TEST株式会社ABC','ﾃｽﾄｹｲﾔｸｼｬ1','testaddress01@test.nn.baton','北海道','090-0000-0000');
		cntrllr.RecomfirmEmail = '';
		cntrllr.friend.ZipCode__c = '1111111';
		cntrllr.Address = '住所1-1-1';
		cntrllr.tmpFriend.LastNameKana__c = '漢字';
		PageReference pr = Page.MK_FriendDetailRegist;
		pr.getParameters().put('isChecked','true');
		Test.setCurrentPage(pr);

		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG02_IS_NOT_ENTRY);
		//ERROR: メール判定 再確認の値が不正
		cntrllr.RecomfirmEmail = 'testaddress02@test.nn.baton';
		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG05_IS_NOT_RECON_EMAIL);
		//ERROR: カナ文字判定
		cntrllr.friend = TestMK_TestUtil.createLead(false,'テスト契約者','TEST株式会社ABC','ﾃｽﾄｹｲﾔｸｼｬ1','testaddress01@test.nn.baton','北海道','090-0000-0000');
		cntrllr.RecomfirmEmail = 'testaddress01@test.nn.baton';
		cntrllr.friend.ZipCode__c = '1111111';
		cntrllr.Address = '住所1-1-1';
		cntrllr.tmpFriend.LastNameKana__c = '漢字';
		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG03_IS_NOT_KATAKANA);
		//ERROR: 郵便番号判定
		cntrllr.friend = TestMK_TestUtil.createLead(false,'テスト契約者','TEST株式会社ABC','ﾃｽﾄｹｲﾔｸｼｬ','testaddress01@test.nn.baton','北海道','090-0000-0000');
		cntrllr.friend.ZipCode__c = 'TEST';
		cntrllr.tmpFriend.LastNameKana__c = 'セイ';
		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG04_IS_NOT_POSTALCODE);
		//ERROR: 電話番号判定
		cntrllr.friend = TestMK_TestUtil.createLead(false,'テスト契約者','TEST株式会社ABC','ﾃｽﾄｹｲﾔｸｼｬ','testaddress01@test.nn.baton','北海道','090-XXXX-YYYY');
		cntrllr.friend.ZipCode__c = '1111111';
		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG05_IS_NOT_PHONE);
		//正常系
		cntrllr.friend = TestMK_TestUtil.createLead(false,'テスト契約者','TEST株式会社ABC','ﾃｽﾄｹｲﾔｸｼｬ','testaddress01@test.nn.baton','北海道','090-1111-2222');
		cntrllr.friend.ZipCode__c = '1111111';
		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(!cntrllr.pageMessages.hasMessages());
	}
	private static testMethod void testdoEdit(){
		MK_FriendDetailRegistController cntrllr = new MK_FriendDetailRegistController();
		cntrllr.doEdit();
	}
	private static testMethod void testdoSave(){

		Profile pro = [SELECT Id FROM Profile where name = 'E_PartnerCommunity'];
		Account acc = TestMK_TestUtil.createAccount(true,'Test取引先');
		Contact con = TestMK_TestUtil.createContact(true, 'Test募集人', acc.id);
		System.debug('プロファイル'+pro);
		User user = TestMK_TestUtil.createUser(true, con.Name, pro.id, con.Id);
		System.debug('User'+user);

		CampaignManagement__c cm1 = TestMK_TestUtil.createCampaignManagement(true,system.today(),system.today());
		MK_CampaignTarget__c ct1 = TestMK_TestUtil.createMK_CampaignTarget(true,cm1.id,con.Id);
		MK_leadTMP__c ltmp1 = TestMK_TestUtil.createMK_leadTMP(true,ct1.id,'LastName1','Company1');
		Lead lead1 = TestMK_TestUtil.createLead(false,'company','lastName','name_kana','email@test.com','state','111-1111-1111');

		MK_FriendDetailRegistController cntrllr = new MK_FriendDetailRegistController();

		cntrllr.tmpFriend = ltmp1;
		cntrllr.friend = lead1;
		cntrllr.pageMessages.clearMessages();

		//ERROR: 同意なし
		cntrllr.doSave();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG07_IS_NOT_AGREE);
		//正常系
		cntrllr.Agree = true;
		cntrllr.pageMessages.clearMessages();

		cntrllr.doSave();
		List<Lead> results = [Select id From Lead];
//		System.assertEquals(results.size(),1);
		System.assert(!cntrllr.pageMessages.hasMessages());
		cntrllr.pageMessages.clearMessages();
		//chach
		cntrllr.doSave();

		//登録済
		cntrllr = new MK_FriendDetailRegistController();
		MK_leadTMP__c ltmp2 = TestMK_TestUtil.createMK_leadTMP(true,ct1.id,'LastName2','Company2');

		cntrllr.Agree = true;
		cntrllr.tmpFriend = ltmp2;
		cntrllr.friend = lead1;

		ltmp2.CreateLeadFlag__c = true;
		update ltmp2;
		cntrllr.pageMessages.clearMessages();
		cntrllr.doSave();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG11_IS_UPDATE);

		//削除済み
		cntrllr = new MK_FriendDetailRegistController();
		MK_leadTMP__c ltmp3 = TestMK_TestUtil.createMK_leadTMP(true,ct1.id,'LastName3','Company3');

		cntrllr.Agree = true;
		cntrllr.tmpFriend = ltmp3;
		cntrllr.friend = lead1;

		delete ltmp3;
		cntrllr.pageMessages.clearMessages();
		cntrllr.doSave();
		System.assert(cntrllr.pageMessages.hasMessages());
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG11_IS_UPDATE);
	}
}