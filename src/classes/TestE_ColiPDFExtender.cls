@isTest
private class TestE_ColiPDFExtender{
	/**
	 * 契約のレコードのみで実施。
	 */
    static testMethod void testColiPDF1() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c();
        insert policy;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	
        	//テスト開始
			Test.startTest();
			
			PageReference pref = Page.E_ColiPDF;
        	pref.getParameters().put('Id',policy.Id);
		
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ColiPDFController controller = new E_ColiPDFController(standardcontroller);
            E_ColiPDFExtender extender = controller.getExtender();
            extender.getSurrenderValueSum();
            extender.getPremiumValueSum();
            extender.getDeathBenefit();
            extender.getCrtableCordFlg();
            extender.getCorporationName();
            extender.getDesignationSubstitutionFlg();
            extender.getMainCord();
        	//ページアクションの戻り値
        	resultPage = extender.PageAction();
        	//テスト終了
			Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage); 

     }
    /*
	 * 特約レコードも作成して実施
	 * 保健種類コードが MZ
	 */
    static testMethod void testColiPDF2(){
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        
        E_Policy__c policy = new E_Policy__c(
        	COLI_ZCSHVAL__c = 300 //解約返戻金
        	//, COLI_ZLOANTOT__c = 200 貸付金等精算額
        	, COLI_ZEAPLTOT__c = 200
			, COLI_ZEPLTOT__c = 100 //契約者貸付合計
			, COLI_ZEADVPRM__c = 300 //前納未経過保険料
			, COLI_ZUNPREM__c = 1000 //未経過保険料
            , COMM_ZTOTPREM__c =  1000

        );
        insert policy;
        
        //個人保険特約 主契約
        E_COVPF__c covpf1 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'C' // 契約フラグ
            , COMM_ZCOVRNAM__c = '保険種類1' //保険種類
            , COMM_ZNAME40A01__c = '保険種類2' //保険種類
            , COMM_ZNAME40A02__c = '保険種類3' //保険種類
        	, COLI_ZPREMADD__c = true // 加算フラグ
        	, COLI_INSTPREM__c = 100 //保険料
        	, COMM_CRTABLE__c = 'MZ' //
        );
        insert covpf1;
        
        //個人保険特約 特約
        E_COVPF__c covpf2 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'R'
        	, COLI_ZPREMADD__c = true
        	, COLI_INSTPREM__c = 200
             , COMM_SUMINS__c = 100  //主契約現在保険金額
            , COLI_ZSTDSUM__c = 1000 //加入時保険金額
            , COLI_ZSUMINF__c = false //S計算不能フラグ           
        );	
        insert covpf2;
        
		//個人保険特約 特約
        E_COVPF__c covpf3 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'R'
        	, COLI_ZPREMADD__c = false
            , COLI_ZSUMINF__c = true
        	, COLI_INSTPREM__c = 300
        );	
         insert covpf3;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        
        	//テスト開始
			Test.startTest();
			PageReference pref = Page.E_Coli;
        	pref.getParameters().put('Id',policy.Id);
        	
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ColiPDFController controller = new E_ColiPDFController(standardcontroller);
            E_ColiPDFExtender extender = controller.getExtender();
            //解約時受取金額の計算
            extender.getSurrenderValueSum();
            //保険料の合算
            extender.getPremiumValueSum();
            extender.getDeathBenefit();
            extender.getMainCord();
            boolean var6 = extender.getCrtableCordFlg();
            extender.getCorporationName();
            extender.getDesignationSubstitutionFlg();

            
            //ページアクションの戻り値
        	resultPage = extender.PageAction();
        	//テスト終了
			Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);		
    }
    /*
	 * 特約レコードも作成して実施
	 * 保健種類コードが MX
	 * 団体レコードも作成
	 */
    static testMethod void testColiPDF3(){
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        
        E_Policy__c policy = new E_Policy__c(
        	COLI_ZCSHVAL__c = 300 //解約返戻金
        	, COLI_ZEAPLTOT__c = 200
			, COLI_ZEPLTOT__c = 100 //契約者貸付合計
			, COLI_ZEADVPRM__c = 300 //前納未経過保険料
			, COLI_ZUNPREM__c = 1000 //未経過保険料
			, COLI_ZCVDCF__c =true
        );
        insert policy;
        
        //個人保険特約 主契約
        E_COVPF__c covpf1 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'C' // 契約フラグ
        	, COLI_ZPREMADD__c = true // 加算フラグ
        	, COLI_INSTPREM__c = 100 //保険料
        	, COMM_CRTABLE__c = 'MX' //
        	, COLI_ZDPTYDSP__c = true
        );
        insert covpf1;
        
        //個人保険特約 特約
        E_COVPF__c covpf2 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'R'
        	, COLI_ZPREMADD__c = true
        	, COLI_INSTPREM__c = 200
        	);	
        insert covpf2;

			//個人保険特約 特約
        E_COVPF__c covpf3 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'R'
        	, COLI_ZPREMADD__c = false
        	, COLI_INSTPREM__c = 300
        );	
        insert covpf3;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        
        	//テスト開始
			Test.startTest();
			
			PageReference pref = Page.E_ColiPDF;
        	pref.getParameters().put('Id',policy.Id);
			
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ColiPDFController controller = new E_ColiPDFController(standardcontroller);
            E_ColiPDFExtender extender = controller.getExtender();
            extender.getSurrenderValueSum();
            extender.getDeathBenefit();
            extender.getCrtableCordFlg();
            extender.getCorporationName();
            extender.getDesignationSubstitutionFlg();

        	//ページアクションの戻り値
        	resultPage = extender.PageAction();

        	//テスト終了
			Test.stopTest();
        }
         //※正常処理
        system.assertEquals(null, resultPage);       
    }
    
	 /* 特約レコードも作成して実施
	 * 保健種類コードが MX
	 * 団体レコードも作成
	 */
    static testMethod void testColiPDF4(){
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        
        Account acc1 = new Account(
        	Name = 'Terrasky'
        );
        insert acc1;
        Contact con1 = new Contact(
        	AccountId = acc1.Id
        	,E_CLTPF_CLNTNUM__c = '1111111'
        	,LastName = 'Yamada'
        );
        insert con1;
        
        Account acc2 = new Account(
        	Name = 'ING Company'
        );
        insert acc2;
        Contact con2 = new Contact(
        	AccountId = acc2.Id
        	,LastName = 'Tanaka'
        	,E_CLTPF_CLNTNUM__c = '2222222'
        );
        insert con2;
        
        
        E_Policy__c policy = new E_Policy__c(
        	Contractor__c = con2.Id
        	, COLI_ZCSHVAL__c = 300 //解約返戻金
        	, COLI_ZEAPLTOT__c = 200
			, COLI_ZEPLTOT__c = 100 //契約者貸付合計
			, COLI_ZEADVPRM__c = 300 //前納未経過保険料
			, COLI_ZUNPREM__c = 1000 //未経過保険料
			, COLI_ZCVDCF__c =true
			, COLI_ZGCLTNM__c = '1111111' //団体顧客番号
        );
        insert policy;
        	

        
        //個人保険特約 主契約
        E_COVPF__c covpf1 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'C' // 契約フラグ
        	, COLI_ZPREMADD__c = true // 加算フラグ
        	, COLI_INSTPREM__c = 100 //保険料
        	, COMM_CRTABLE__c = 'HH' //
        );
        insert covpf1;
        
        //個人保険特約 特約
        E_COVPF__c covpf2 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'R'
        	, COLI_ZPREMADD__c = true
        	, COLI_INSTPREM__c = 200
        );	
        insert covpf2;

		//個人保険特約 特約
        E_COVPF__c covpf3 = new E_COVPF__c(
        	E_Policy__c = policy.Id
        	, COLI_ZCRIND__c = 'R'
       		, COLI_ZPREMADD__c = false
        	, COLI_INSTPREM__c = 300
        );	
        insert covpf3;
        
        System.runAs(u){
        
        	//テスト開始
			Test.startTest();
			
			PageReference pref = Page.E_Coli;
        	pref.getParameters().put('Id',policy.Id);
		
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ColiPDFController controller = new E_ColiPDFController(standardcontroller);
            E_ColiPDFExtender extender = controller.getExtender();
            extender.getSurrenderValueSum();
            extender.getPremiumValueSum();
            extender.getCrtableCordFlg();
            extender.getCorporationName();
            extender.getDesignationSubstitutionFlg();        	
        	//ページアクションの戻り値
        	resultPage = extender.PageAction();
        	//テスト終了
			Test.stopTest();
        }
		System.assertEquals(null,resultPage);
    }
    /*
     * 団体が存在しないとき。
	 * 特約レコードも作成して実施
	 */
    static testMethod void testColiPDF5(){
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        
        E_Policy__c policy = new E_Policy__c(
        	COLI_ZCSHVAL__c = 300 //解約返戻金
        	, COLI_ZLOANTOT__c = 200 //貸付金等精算額
			, COLI_ZEPLTOT__c = 100 //契約者貸付合計
			, COLI_ZEADVPRM__c = 300 //前納未経過保険料
			, COLI_ZUNPREM__c = 1000 //未経過保険料
			, COLI_ZGCLTNM__c = '1111111' //団体顧客番号
        );
        insert policy;
        	

        //テストユーザで機能実行開始
        System.runAs(u){
        		
        	//テスト開始
			Test.startTest();
			
			PageReference pref = Page.E_Coli;
        	pref.getParameters().put('Id',policy.Id);
		
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ColiPDFController controller = new E_ColiPDFController(standardcontroller);
            E_ColiPDFExtender extender = controller.getExtender();
            extender.getCorporationName();
            extender.getDesignationSubstitutionFlg();
            //ページアクションの戻り値
        	resultPage = extender.PageAction();
        	//テスト終了
			Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }    
}