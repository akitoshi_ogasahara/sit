/**
 *		IRIS CMS　のComponentController
 *				各Pageで生成して、Componentに渡す。
 */
public class I_CMSComponentController {
	public I_ContentMaster__c record{get; private set;}
	public List<Attachment> attachments{get;private set;}
	public Id chatterFileId{get; private set;}				//ContentVersionのId

	/*
	 *		Constructor
	 */
	public I_CMSComponentController(I_ContentMaster__c cm, List<Attachment> attchs){
		this.record = cm;
		this.attachments = attchs;
		if(this.attachments==null){
			this.attachments = new List<Attachment>();
		}
		this.chatterFileId = null;
	}

	/**
	 * URLコピーボタンが表示可能かを返す.
	 *
	 * @return Boolean 表示可能であればtrue、さもなくばfalse
	 */
	public Boolean getShowUrlCopy() {
		return  E_AccessController.getInstance().isEmployee();
	}

	public Boolean getIsEmployee() {
		return  E_AccessController.getInstance().isEmployee();
	}

	public I_CMSComponentController(I_ContentMaster__c cm, List<Attachment> attchs, Id cfId){
		this(cm, attchs);
		this.chatterFileId = cfId;
	}

	/**
	 *		getDomId
	 *			Domに出力するIDを返す ()
	 */
	public String getDomId(){
		String DOM_ID_PREFIX = 'iris-cms-';
		if(String.isBlank(this.record.Dom_Id__c)){
			return DOM_ID_PREFIX + String.valueOf(this.record.id);
		}else{
			return DOM_ID_PREFIX + this.record.Dom_id__c;
		}
	}
	/**
	 *		getThumbnailAttachment
	 *			サムネイルファイルに使用する添付ファイルを返す
	 */
	public Attachment getThumbnail(){
		for(Attachment atch:this.attachments){
			//DL指定されていないファイルを選択	1件目を返す。		//TODO　ﾘﾌｧｸﾀ　contentType指定するか？画像系
			if(atch.Name != record.FilePath__c){
				return atch;
			}
		}
		//上記で取得できない場合　Attachmentが1件の場合などは、そのファイルを返す
		if(this.attachments.size()>0){
			return this.attachments[0];
		}

		return null;
	}

	/* DL ファイル*/
	private Attachment downloadFileAtch;
	private SObject downloadFile;
	public Boolean getExistsDownloadFile(){
		return getDLAttachment() != null;
	}

	/**
	 *		getDLAttachment
	 *			ClickAction=DL(Attacment)の場合、CMSコンテンツの関連添付ファイルからNameがFilePathと一致するものを返す。
	 *			上記以外の場合、Attachmentの1件目を返す。
	 */
	public Attachment getDLAttachment(){
		if(downloadFile == null){
			if(record.ClickAction__c == 'DL(Attachment)'){
				for(Attachment atch:this.attachments){
					if(atch.Name == record.FilePath__c){
						downloadfile = atch;
						downloadFileAtch = atch;
						break;
					}
				}
			}else{
				for(Attachment atch:this.attachments){
					downloadfile = atch;
					downloadFileAtch = atch;
					break;
				}
			}
		}
		return (Attachment)downloadfile;
	}
	
	/** 
	 * ファイルサイズ取得
	 */
	public String getDownloadFileSize(){
		return getFileSize(downloadFileAtch.BodyLength);
	}
	private String getFileSize(Decimal len){
		if(len > 1000000){
			return String.valueOf(E_Util.roundDecimal(len/1024/1024, 2))+'MB';
		}else{
			return String.valueOf(E_Util.roundDecimal(len/1024, 0))+'KB';
		}
	}

	public ContentVersion getDLContentVersion(){
		//TODO T.B.D.　　2017/02版以降で　実装予定
		return null;
	}

	/**
	 *		getActionIsDLAttachment
	 *			AttachmentのDLときにTrue
	 */
	public Boolean getActionIsDLAttachment(){
		return record.ClickAction__c == 'DL(Attachment)';
	}

	/**
	 *		getHasClickAction
	 *			クリックアクションが設定されているときにTrue
	 */
	public Boolean getHasClickAction(){
		return String.isNotBlank(record.ClickAction__c);
	}

	/**
	 *		getIsforUrl
	 *			ページ遷移系の場合にTrue
	 */
	public Boolean getIsforUrl(){
		return record.ClickAction__c == 'URL';
	}

	//P16-0003 Atria対応開発
	/**
	 *		getIsAtria
	 *			クリックアクションがAtriaの時にTrue
	 */
	public Boolean getIsAtria(){
		return record.ClickAction__c == 'Atria' ;
	}
	/**
	 *		getIsforUrlWebContent
	 *			Url(Webコンテンツ)の場合にTrue
	 */
	public Boolean getIsforUrlWebContent(){
		return record.ClickAction__c == 'URL(Webコンテンツ)';
	}

	/**   getAnchorTo
	 *			アンカーの遷移先を返す
	 */
	public String getAnchorTo(){
		//　CMSController.PageAnchorsから取得
		return I_CMSController.pageAnchors.get(record.LinkURL__c);
	}

//---------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 *		ChatterFile利用時のメソッド			Day2追加
	 */
	//public static final String CHATTER_FILE_THUMNBNAIL = '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB240BY180&versionId={0}';	//120BY90	//&operationContext=CHATTER&contentId=05Tp0000003noOh
	public static final String CHATTER_FILE_THUMNBNAIL = '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId={0}';	//120BY90	//&operationContext=CHATTER&contentId=05Tp0000003noOh

	public Boolean getHasChatterFile(){
		return chatterFileId != null && record.ClickAction__c == 'DL(ChatterFile)';
	}

	//ThumbnailのURL
	public String getChatterFileThumbnail(){
		if(chatterFileId != null){
			return String.format(CHATTER_FILE_THUMNBNAIL, new List<String>{String.valueOf(chatterFileId)});
		}
		return null;
	}

	//DownloadのURL
	public String getChatterFileDownloadUrl(){
		if(chatterFileId != null){
			return String.format(E_CONST.CONTENT_DOWNLOAD_URL, new List<String>{String.valueOf(chatterFileId)});
		}
		return null;
	}

	//代理店ユーザへ共有するインターネットアクセス用ChatterファイルDL　URL
	public String getChatterFileDownloadUrl_via_Internet(){
		String nnUrl = URL.getSalesforceBaseUrl().toExternalForm();
		nnUrl += getChatterFileDownloadUrl();
		return E_Util.getUrlForNNLinkage(nnUrl);
	}

	//セールスツールのDL用リンク
	public String getSalesToolShortcutUrl(){
		PageReference pr = Page.IRIS_Lib_Tools;

		// 資料管理番号がユニークとなったため、資料管理番号+枝番は使用しない
		//pr.getParameters().put(I_Const.URL_PARAM_DOC_NUM, this.record.DocumentAndBranchNo__c);
		System.debug(this.record.Name);
		System.debug(this.record.DocumentNo__c);
		String docNum = String.isNotBlank(this.record.DocumentNo__c)
			? this.record.DocumentNo__c
			: this.record.DocumentAndBranchNo__c;

		pr.getParameters().put(I_Const.URL_PARAM_DOC_NUM, docNum);
		pr.setAnchor(EncodingUtil.urlEncode(getDomId(), 'utf-8'));
		return E_Util.getUrlForNNLinkage(URL.getSalesforceBaseUrl().toExternalForm()+pr.getUrl());
	}

	/**
	 * UX2 セールスツールメンテナンス対応 201706
	 * Categoryリスト（カテゴリ結合→カテゴリへ変更）
	 */
	public List<String> getCategoryNames(){
		List<String> ret = new List<String>();

		if(String.isBlank(record.Category__c)) {
			return ret;
		}

//		List<String> cates = record.Category__c.split(';');
		Set<String> cates = new Set<String>(record.Category__c.split(';'));

		// カテゴリの1つ目のみ
		//ret.add(cates[0]);

		//20171121 カテゴリを選択リスト順に1件取得
		List<Schema.PicklistEntry> categoryEntries = SObjectType.I_ContentMaster__c.fields.Category__c.getPicklistValues();
		for(PicklistEntry entry :categoryEntries){
			String key = entry.getLabel();
			//MR作成資料と会社案内・リクルートと事前案内資料の選択リスト値とAPIを合わせる
			if(key == I_Const.CMSCONTENTS_CATEGORY_MRDOCUMENT || key == I_Const.CMSCONTENTS_CATEGORY_RECRUIT || key == I_Const.CMSCONTENTS_CATEGORY_ADVANCEINFO){
				key = key + '（＊）';
			}
			if(cates.contains(key)){
				ret.add(key);
				break;
			}
		}

		return ret;
	}

	//特集掲載日時判定メソッド
	public Boolean getFeatureDisplay(){
		Date displayDate = E_Util.SYSTEM_TODAY();
		return(this.record.FeatureDisplayFrom__c <= displayDate && this.record.FeatureDisplayTo__c >= displayDate);
	}

	/**
	 * UX2 UX2AG-182
	 * メール件名（URLエンコード）取得
	 */
	public String getMailSubject(){
		String subject = '【エヌエヌ生命】';
		return EncodingUtil.urlEncode(subject, 'UTF-8');		
	}

	/**
	 * UX2 UX2AG-182
	 * メール本文（URLエンコード）取得
	 */
	public String getMailBody(){
		String body = '';
		body += '\r\n';
		body += '[資料管理番号]　' + this.record.DocumentNo__c;
		body += '\r\n';
		body += '[資料名]　' + this.record.Name;
		body += '\r\n';
		body += '<インタ-ネット経由>';
		body += '\r\n';
		if(this.record.ClickAction__c == 'URL(Webコンテンツ)'){
			body += this.record.LinkURL__c;
		}else{
			body += getSalesToolShortcutUrl();
		}
		body += '\r\n';
		body += '\r\n';
/*
		body += '<GW経由>';
		body += E_Util.getURLForGWTB(getSalesToolShortcutUrl());
		body += '\r\n';
		body += '\r\n';
*/
		body += '【注意事項】';
		body += '\r\n';
		body += '各種資料は必ず有効期限内のものをご使用ください。有効期限は資料内に記載されています。';
		body += '\r\n';
		body += 'ただし、資料記載の有効期限内であっても内容改定によりご使用いただけないものもございますので、廃止資料一覧でご確認ください。';

		return EncodingUtil.urlEncode(body, 'utf-8');	
	}

	/*
		クリックアクションが「URL(Webコンテンツ)」のとき、リンクURL/アンカー先名が下記条件に一致する場合にNN Tubeログイン処理をはさむ
		1. ドメインがNN Tubeドメイン
		2. URLに'permitted'が存在しない
		3. URLパラメータ'mid'が存在する
	 */
	public String getLinkURL(){
		if(this.record.ClickAction__c == 'URL(Webコンテンツ)' && this.record.LinkURL__c.contains(Label.E_DOMAIN_NN_TUBE)
			 && !this.record.LinkURL__c.contains('permitted')){
			PageReference pageRef = new PageReference(this.record.LinkURL__c);
			String mid = pageRef.getParameters().get('mid');

			I_MenuProvider.LibSubNavItemsHolder holder = new I_MenuProvider.LibSubNavItemsHolder();
			if(!String.isBlank(mid)){
				return holder.getUrlForNNTube() + '&' + I_TubeConnectHelper.TUBE_REQUESTTYPE_MOVIE + '&mid=' + mid;
			}
		}
		return this.record.LinkURL__c;
	}

	/**
	 * 会社案内・リクルート、MR作成資料などのNN社員のみ表示するカテゴリ判定
	 * 20180627事前案内資料追記
	 */
	public Boolean getEmpCategory(){
		if(record.Category__c != null 
			&& record.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_MRDOCUMENT) || record.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_RECRUIT) || record.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_ADVANCEINFO)){
			return true;
		}
		return false;
	}

}