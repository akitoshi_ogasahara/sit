@isTest
private class TestI_AbstractSearchBoxController {

    //実行ユーザ作成
    private static User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');

    //コンストラクタ用
    static testMethod void ConstructorTest(){        
        system.runAs(actUser){
            Test.startTest();
            	I_PolicySearchBoxController psbc = new I_PolicySearchBoxController();
			Test.stopTest();            
        }
    }

    //getter用
    static testMethod void getterTest(){
        List<String> msgs = new List<String>();
        msgs.add('test');
        
		Test.startTest();
        	I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
        	controller.searchMessages = msgs;
        
			system.assertEquals( I_Const.LIST_MAX_ROWS, controller.getListMaxRows() );
        	system.assert( controller.getHasMessages() );
        	system.assertEquals( E_Message.getMsgMap(), controller.getMsg() );
        	system.assertEquals( E_Message.getDisclaimerMap(), controller.getDISCLAIMER());
		Test.stopTest();
    }

    //setter用
    static testMethod void setterTest(){       
		Test.startTest();
        	I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.setRefererContactSearch();
        	controller.setRefererPolicySearch();
        	controller.sortType = 'test';
        	controller.sortIsAsc = true;
        	controller.menuMap = new Map<String, I_MenuItem>();
		Test.stopTest();
    }

    //isEnablePolicy用
    static testMethod void testisEnablePolicy(){
        List<E_Policy__c> eList = new List<E_Policy__c>();
        
        system.runAs(actUser){
            Account ac = TestI_TestUtil.createAccount(true, null);
            Contact co = TestI_TestUtil.createContact(true, ac.Id, 'test');
			//テストレコード作成(個人保険ヘッダ、SPVAヘッダ、個人保険消滅、SPVA消滅それぞれでテストする)
            E_Policy__c ech = TestE_TestUtil.createPolicy(true, co.Id,'ECHDPF','11111');
            E_Policy__c esp = TestE_TestUtil.createPolicy(true, co.Id,'ESPHPF','22222');
            E_Policy__c ehl = TestE_TestUtil.createPolicy(true, co.Id,'EHLDPF','33333');
            E_Policy__c ehd = TestE_TestUtil.createPolicy(true, co.Id,'EHDSPF','44444');          
            eList.add( ech );
            eList.add( esp );
            eList.add( ehl );
            eList.add( ehd );
            //RecordTyoe.DeveloperNameが必要なので取得する
			eList = [select Id, Name, RecordType.DeveloperName from E_Policy__c where ID IN : eList]; 

            Test.startTest();
            	I_PolicySearchBoxController psbc = new I_PolicySearchBoxController();
                for( E_Policy__c ep : eList ){
                    //レコードタイプが個人ヘッダ、SPVAヘッダであればtrue、そうでなければfalse
                    if(ep.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI || 
                       ep.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA) {
                        system.assertEquals( psbc.isEnablePolicy(ep), true ); 
                    }else{
                    	system.assertEquals( psbc.isEnablePolicy(ep), false ); 
                    }                  
                }
            Test.stopTest();            
        }
    }

    //getSearchBoxLogRecord用
	static testMethod void testgetSearchBoxLogRecord(){
        //事前にNNLinkLogレコードを作成
		createLogs();
        test.startTest();
          I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
          controller.searchTypeName = I_Const.SEARCH_TYPE_LABELPOLICY_NO;
          controller.getSeachBoxLogRecord(I_Const.SEARCH_TYPE_POLICY_NO);
        test.stopTest();
	}
   
    //addRows用
    static testMethod void testaddRows(){
        system.runAs(actUser){
            test.startTest();
            	I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
            	//上限-1にセットしておくことで、addRows()により上限を超える
            	controller.rowCount =  I_Const.LIST_MAX_ROWS - 1;
            	controller.addRows();
            test.stopTest();
		}
    }		

    //refreshLog用
    static testMethod void testrefreshLog(){
        //searchTypeのパラメータを設定
		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('searchType', I_Const.SEARCH_TYPE_POLICY_NO);
		Test.setCurrentPage(pr);
        
        test.startTest();
        I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
        controller.refreshLog();
        test.stopTest();

    }	
	//NNLinkLogレコード作成用
    static void createLogs(){
        E_log__c log = new E_log__c();
        log.Name = I_Const.SEARCH_LABEL_CATEGORY_POLICY + '>' + I_Const.SEARCH_TYPE_LABELPOLICY_NO + '>99999999';
        log.Detail__c = '{"searchType": "policyNo", "searchTypeName": "証券番号", "keyword": "99999999"}';
        log.AccessUser__c = userInfo.getUserId();
        log.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_SEARCH;
        log.AccessDateTime__c = DateTime.now();
        insert log;
    }
}