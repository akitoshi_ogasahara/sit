public with sharing class SC_SelfComplianceSubmit_IrisController extends I_AbstractController {
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	//ページ名
	public String pageTitle {get;set;}
	//証跡フラグ
	public Boolean trailFlag {get;set;}
	//ドラッグ&ドロップでのファイル添付サポート判定
	public Boolean isOldIE {get; set;}
	//自主点検レコード
	public SC_SelfCompliance__c record {get; private set;}
	//提出種別
	public String pageType {get; set;}
	//単一アップロード判定
	public Boolean isSingle {get; private set;}
	//年度
	public String year {get;set;}
	//提出種別ごとの説明
	public String description {get;set;}
	//提出種別ごとの注意事項
	public String notes {get;set;}
	//閉じるボタン押下時、リロードするURL
	public String reloadUrl {get;set;}
	//募集人判定
	public Boolean isAgent {get; set;}
	//GW経由判定
	public Boolean isGW {get; set;}

	public SC_SelfComplianceSubmit_IrisController() {
		pageMessages = getPageMessages();
		pageTitle = '';
		trailFlag = false;
		isOldIE = false;
		description = '';
		notes = '';
		reloadUrl= null;

		String recordId = ApexPages.currentPage().getParameters().get('id');
		pageType = ApexPages.currentPage().getParameters().get('type');

		//自主点検レコード
		record = new SC_SelfCompliance__c();
		record.id = recordId;

		// 単一アップロード or 複数アップロード
		isSingle = true;
		if (!String.isEmpty(ApexPages.currentPage().getParameters().get('isSingle'))) {
			isSingle = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isSingle'));
		}

		year = ApexPages.currentPage().getParameters().get('year');

		if(pageType == SC_Const.SC_COMP_TYPE_COMMON){ //共通
			pageTitle = SC_Const.SC_COMMON_TITLE;
			description = SC_Const.SC_COMMON_DISCLAMER_DESC;
			notes = SC_Const.SC_COMMON_DISCLAMER_NOTES;
		}else if(pageType == SC_Const.SC_COMP_TYPE_NN){ //追加
			pageTitle = SC_Const.SC_ADD_TITLE;
			description = SC_Const.SC_ADD_DISCLAMER_DESC;
			notes = SC_Const.SC_ADD_DISCLAMER_NOTES;
		}else{ //証跡
			pageTitle = SC_Const.SC_TRAIL_TITLE;
			description = SC_Const.SC_TRAIL_DISCLAMER_DESC + year;
			notes = SC_Const.SC_TRAIL_DISCLAMER_NOTES + year;
			trailFlag = true;
		}

		// アクセスコントローラのインスタン化
		E_AccessController access = E_AccessController.getInstance();
		// 募集人判定
		isAgent = access.isAgent();
		// GW経由判定
		isGW = access.isCommonGateway();

		//提出書類一覧ページの再読み込み用URL
		SC_SelfCompliance__c compRec = [SELECT id,SC_Office__c from SC_SelfCompliance__c where id =: record.id];
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		pageRef.getParameters().put('id',compRec.SC_Office__c);
		reloadUrl = pageRef.getUrl().replace('/apex/', '');
	}

	//初期処理
	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}
		
		if (!String.isEmpty(ApexPages.currentPage().getParameters().get('isOldIE'))) {
			isOldIE = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isOldIE'));
		}

		return null;
	}

	public PageReference reload() {
		return null;
	}

	// 点検実施の手引きIRISページ取得
	public String getPdfUrl(){
		String uniqueKey = SC_Const.SC_MANUAL_PREFIX + year;
		I_PageMaster__c pageMaster = [Select Id From I_PageMaster__c Where page_unique_key__c = :uniqueKey];
		return pageMaster == null ? '' : 'IRIS?page=' + pageMaster.Id + '#iris-cms-' + year + SC_Const.SC_MANUAL_PDF_ANCHOR;
	}

}