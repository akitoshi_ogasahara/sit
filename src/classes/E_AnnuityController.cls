global with sharing class E_AnnuityController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_AnnuityExtender getExtender() {return (E_AnnuityExtender)extender;}
	
	
	public dataTableSTEP dataTableSTEP {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_AnnuityController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1626');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1626',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','ポートフォリオ');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','{!record.SPVA_ZPORTDCFA__c}');
        tmpPropMap.put('p_value','E_Portfolio?id={!record.id}&type={!Extender.FromType}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1648');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1648',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','年金受取人情報');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Annuitant?id={!record.id}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1646');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1646',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','契約内容個別照会へ');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','{!IF(Record.SpClassification__c == \'SPVA&ANNUITY\',true,false)}');
        tmpPropMap.put('p_value','E_Spva?id={!record.id}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1647');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1647',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_Annuity');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1643');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1643',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','PDF表示');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','false');
        tmpPropMap.put('p_value','E_AnnuityPdf?id={!record.id}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1644');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1644',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_dto','{!extender.noticeDTO}');
		tmpPropMap.put('Component__Width','700');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component1685');
		tmpPropMap.put('Component__Name','E_NoticeList');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1685',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','申込');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','true');
        tmpPropMap.put('p_value','E_FundTransferAgree?id={!record.id}&type={!Extender.FromType}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1649');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1649',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','ご利用方法');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','true');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_FundTransfer');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','17');
		tmpPropMap.put('Component__id','Component1645');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1645',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.SPVA_ZANNFROM__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1271');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1271',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.SPVA_ZTGTRATE__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!record.SPVA_ZSVTGDSP__c}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1623');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1623',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.SPVA_ZTGTAMT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!record.SPVA_ZSVTGDSP__c}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1508');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1508',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!extender.designatedProxyApplicantRate}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!and(record.SPVA_ZDPTYDSP__c,extender.isDesignatedProxyApplicantJudge)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','1');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','97');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1624');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1624',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','手続履歴の照会はこちら');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{! NOT(Extender.IsIss)}');
        tmpPropMap.put('p_value','E_ProcessHistory?pId={!record.id}&menu=0');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1636');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1636',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!extender.NEXTDATE}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','-');
        tmpPropMap.put('all0','-');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1272');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1272',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.SPVA_ZCVAMT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!NOT(AND(record.COMM_CRTABLE2__c==\'SU\',record.SPVA_ZSVTGDSP__c))}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1586');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1586',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.SPVA_ZGRRCVAMT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!AND(record.COMM_CRTABLE2__c==\'SU\',record.SPVA_ZSVTGDSP__c)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1587');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1587',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!dataTableSTEP_item.record.Column1Date__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','');
        tmpPropMap.put('all0','');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1270');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1270',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dataTableSTEP_item.record.Column2Number__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','true');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','<br/>');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1625');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1625',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!dataTableSTEP_item.record.Column3Number__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','true');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','<br/>');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1659');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1659',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1630');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1630',tmpPropMap);


		SObjectField f;

		f = Contact.fields.E_CLTPF_CLNTNUM__c;
		f = Contact.fields.Name;
		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.SPVA_ZANNSTATD__c;
		f = E_Policy__c.fields.COMM_ZCOVRNAM__c;
		f = E_Policy__c.fields.SPVA_ZGRPNAMES__c;
		f = E_Policy__c.fields.SPVA_ZANNDSCAS__c;
		f = E_Policy__c.fields.SPVA_ZEANNTRM__c;
		f = E_Policy__c.fields.SPVA_ZANTYBSC__c;
		f = E_Policy__c.fields.COMM_ZTOTPREM__c;
		f = E_Policy__c.fields.SPVA_ZLUMPSUM__c;
		f = E_Policy__c.fields.SPVA_ZTOTPAY__c;
		f = E_TransitionWK__c.fields.Type__c;
		f = E_TransitionWK__c.fields.SortNumber__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('Annuitant__r.Name');
			mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
			mainQuery.addFieldAsOutput('Insured__r.Name');
			mainQuery.addFieldAsOutput('SPVA_ZANNSTATD__c');
			mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
			mainQuery.addFieldAsOutput('SPVA_ZGRPNAMES__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNDSCAS__c');
			mainQuery.addFieldAsOutput('SPVA_ZEANNTRM__c');
			mainQuery.addFieldAsOutput('SPVA_ZANTYBSC__c');
			mainQuery.addFieldAsOutput('COMM_ZTOTPREM__c');
			mainQuery.addFieldAsOutput('SPVA_ZLUMPSUM__c');
			mainQuery.addFieldAsOutput('SPVA_ZTOTPAY__c');
			mainQuery.addFieldAsOutput('SpClassification__c');
			mainQuery.addFieldAsOutput('SPVA_ZSTUPDSP__c');
			mainQuery.addFieldAsOutput('SPVA_ZPORTDCFA__c');
			mainQuery.addFieldAsOutput('COMM_ZEBKNM__c');
			mainQuery.addFieldAsOutput('SPVA_ZSUSADSP__c');
			mainQuery.addFieldAsOutput('COLI_ZBKPODIS__c');
			mainQuery.addFieldAsOutput('COLI_ZBKTRDCF__c');
			mainQuery.addFieldAsOutput('COMM_BANKACCKEY__c');
			mainQuery.addFieldAsOutput('COMM_ZEBKACOW__c');
			mainQuery.addFieldAsOutput('SPVA_ZDPTYDSP__c');
			mainQuery.addFieldAsOutput('SPVA_ZDPTEIST__c');
			mainQuery.addFieldAsOutput('COMM_ZEBKBRNM__c');
			mainQuery.addFieldAsOutput('SPVA_NEXTDATE__c');
			mainQuery.addFieldAsOutput('SPVA_ZGRRCVAMT__c');
			mainQuery.addFieldAsOutput('SPVA_ZVWDESC__c');
			mainQuery.addFieldAsOutput('SPVA_ZVWDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZTFRDCFA__c');
			mainQuery.addFieldAsOutput('SPVA_ZFIXAEXIST__c');
			mainQuery.addFieldAsOutput('SPVA_ZFIXAF__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNSTAT__c');
			mainQuery.addFieldAsOutput('SPVA_ZCVAMT__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNPSDP__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNFROM__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNSTFLG__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNFREQ__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE2__c');
			mainQuery.addFieldAsOutput('SPVA_ZSVTGDSP__c');
			mainQuery.addFieldAsOutput('SPVA_ZTGTAMT__c');
			mainQuery.addFieldAsOutput('SPVA_ZTGTRATE__c');
			mainQuery.addFieldAsOutput('COMM_ZEBKACTP__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			dataTableSTEP = new dataTableSTEP(new List<E_TransitionWK__c>(), new List<dataTableSTEPItem>(), new List<E_TransitionWK__c>(), null);
			listItemHolders.put('dataTableSTEP', dataTableSTEP);
			query = new SkyEditor2.Query('E_TransitionWK__c');
			query.addFieldAsOutput('Column1Date__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dataTableSTEP', 'E_Policy__c');
			dataTableSTEP.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dataTableSTEP', query);
			
			registRelatedList('E_TransitionWKs__r', 'dataTableSTEP');
			
			SkyEditor2.Query dataTableSTEPQuery = queryMap.get('dataTableSTEP');
			dataTableSTEPQuery.addWhereIfNotFirst('AND');
			dataTableSTEPQuery.addWhere('( Type__c = \'ステップアップ金額の推移\')');
			dataTableSTEPQuery.addSort('SortNumber__c',True,True);
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_AnnuityExtender(this);
			init();
			
			dataTableSTEP.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class dataTableSTEPItem extends SkyEditor2.ListItem {
		public E_TransitionWK__c record{get; private set;}
		@TestVisible
		dataTableSTEPItem(dataTableSTEP holder, E_TransitionWK__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTableSTEP extends SkyEditor2.ListItemHolder {
		public List<dataTableSTEPItem> items{get; private set;}
		@TestVisible
			dataTableSTEP(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dataTableSTEPItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableSTEPItem(this, (E_TransitionWK__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}