global with sharing class E_MKFriendDetailSVEExtender extends E_AbstractViewExtender{

    private static final String PAGE_TITLE = 'バトンプロジェクトご友人詳細';

    /*
     * Constructor
     */
    public E_MKFriendDetailSVEExtender(E_MKFriendDetailSVEController controller){
        super();
        this.pgTitle = PAGE_TITLE;
    }
}