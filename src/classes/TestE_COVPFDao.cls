@isTest
private class TestE_COVPFDao {
	
	@isTest static void getRecByIdTest01() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);
		E_COVPF__c covpf = new E_COVPF__c(
										E_Policy__c = policy.id
										,COLI_ZCRIND__c = 'C'
										,COMM_ZCOVRNAM__c = 'テスト保険種類'
										,COLI_ZPSTDESC__c = '現在の状況'
										,RecordTypeId = recTypeId
									);
		insert covpf;
		Test.setCreatedDate(covpf.Id, Datetime.now());

		Map<id, E_COVPF__c> recs = new Map<id, E_COVPF__c>();
		Set<Id> polIds = new Set<Id>();
		polIds.add(policy.id);
		recs = E_COVPFDao.getRecById(polIds);
		
		system.assert(!recs.isEmpty());
	}


	@isTest static void getRecByIdsTest01() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);
		E_COVPF__c covpf = new E_COVPF__c(
										E_Policy__c = policy.id
										,COLI_ZCRIND__c = 'C'
										,COMM_ZCOVRNAM__c = 'テスト保険種類'
										,COLI_ZPSTDESC__c = '現在の状況'
										,RecordTypeId = recTypeId
									);
		insert covpf;
		Test.setCreatedDate(covpf.Id, Datetime.now());

		E_COVPF__c[] recs = E_COVPFDao.getRecByIds(policy.Id);

		system.assert(!recs.isEmpty());
	}

	@isTest static void getRecsIRISLapsedPolicyTest01() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);
		E_COVPF__c covpf = new E_COVPF__c(
										E_Policy__c = policy.id
										,COLI_ZCRIND__c = 'C'
										,COMM_ZCOVRNAM__c = 'テスト保険種類'
										,COLI_ZPSTDESC__c = '現在の状況'
										,RecordTypeId = recTypeId
										,COMM_STATDATE__c = String.valueOf(Date.Today().addYears(-1)).replace('-', '')
									);
		insert covpf;
		Test.setCreatedDate(covpf.Id, Datetime.now());

		List<E_COVPF__c> recs = E_COVPFDao.getRecsIRISLapsedPolicy('');

		system.assert(!recs.isEmpty());
	}


	@isTest static void getRecsByPolicyIdAndRtdevnameTest01() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);
		E_COVPF__c covpf = new E_COVPF__c(
										E_Policy__c = policy.id
										,COLI_ZCRIND__c = 'C'
										,COMM_ZCOVRNAM__c = 'テスト保険種類'
										,COLI_ZPSTDESC__c = '現在の状況'
										,RecordTypeId = recTypeId
										,COMM_STATDATE__c = String.valueOf(Date.Today().addYears(-1)).replace('-', '')
									);
		insert covpf;
		Test.setCreatedDate(covpf.Id, Datetime.now());

		List<E_COVPF__c> recs = E_COVPFDao.getRecsByPolicyIdAndRtdevname(policy.Id,E_Const.COVPF_RECORDTYPE_ECOVPF);

		system.assert(!recs.isEmpty());
	}

	@isTest static void getRecsByPolicyIdAndRtdevnameTest02() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_EHDCPF);
		E_COVPF__c covpf = new E_COVPF__c(
										E_Policy__c = policy.id
										,COLI_ZCRIND__c = 'C'
										,COMM_ZCOVRNAM__c = 'テスト保険種類'
										,COLI_ZPSTDESC__c = '現在の状況'
										,RecordTypeId = recTypeId
										,COMM_STATDATE__c = String.valueOf(Date.Today().addYears(-1)).replace('-', '')
									);
		insert covpf;
		Test.setCreatedDate(covpf.Id, Datetime.now());

		List<E_COVPF__c> recs = E_COVPFDao.getRecsByPolicyIdAndRtdevname(policy.Id,E_Const.COVPF_RECORDTYPE_EHDCPF);

		system.assert(!recs.isEmpty());
	}

	@isTest static void getRecsIRISCOVPFSearchBoxTest01() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);
		E_COVPF__c covpf = new E_COVPF__c(
										E_Policy__c = policy.id
										,COLI_ZCRIND__c = 'C'
										,COMM_ZCOVRNAM__c = 'テスト保険種類'
										,COLI_ZPSTDESC__c = '現在の状況'
										,RecordTypeId = recTypeId
										,COMM_STATDATE__c = String.valueOf(Date.Today().addYears(-1)).replace('-', '')
									);
		insert covpf;
		Test.setCreatedDate(covpf.Id, Datetime.now());

		List<AggregateResult> recs = E_COVPFDao.getRecsIRISCOVPFSearchBox('','','');

		system.assert(!recs.isEmpty());
	}

	
	/*
	**AccountData作成
	*/
	private static Account createDataAccount(Boolean isInsert){
		/*代理店格*/
		Account pacc= new Account(
			Name = 'テスト代理店格'
			, E_CL1PF_ZHEADAY__c = 'AMS01'
			);
		if(isInsert){
			insert pacc;
		}
		
		/*代理店事務所*/
		Account acc = new Account(
			Name = 'テスト代理店事務所'
			, ParentId = pacc.Id
			, E_CL2PF_ZAGCYNUM__c = 'AMS11'
			,Owner = TestE_TestUtil.createUser(true,'testMr','E_EmployeeStandard'));
		if(isInsert){
			insert acc;
		}

		return acc;

	}

	/*
	**Contact作成
	*/
	private static Contact createDataContct(Boolean isInsert,Id accId, String lastName){
		/*募集人*/
		Contact con = new Contact(
			LastName = lastName
			, AccountId = accId);
		
		if(isInsert){
			insert con;
		}
		return con;
	}
	/*
	**保険契約ヘッダ
	*/
	public static E_Policy__c createPolicy(Boolean isInsert, Id conId, Id conId2, String recTypeDevName, String cno, Decimal zc) {
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(recTypeDevName);
		E_Policy__c src = new E_Policy__c(
			RecordTypeId = recTypeId
			, COMM_CHDRNUM__c = cno
			, MainAgent__c = conId
			, SubAgent__c  = conId2
			, COLI_ZCSHVAL__c = zc
			, COMM_STATCODE__c = I_Const.LAPS_POLICY_STATCODE
			, COMM_ZRSTDESC__c = I_Const.LAPS_POLICY_ZRSTDESC
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
}