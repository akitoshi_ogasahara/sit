public with sharing class I_ColiUtil {

	private static final Map<String,String> motalMap = new Map<String,String>{
		'A' => 'A',	'B' => 'B','C' => 'C','D' => 'D','E' => 'E','F' => 'F','G' => 'G','H' => 'H','I' => 'I',
		'J' => 'J','K' => 'K','L' => 'L','M' => 'M','N' => 'N','O' => 'O','P' => 'A','Q' => 'B','R' => 'C',
		'1' => 'A\'','2' => 'B\'','3' => 'C\'','4' => 'D\'','5' => 'E\'','6' => 'F\'','7' => 'G\'','8' => 'H\'','9' => 'I\''
    };
    private static final Map<String,String> sakugenMap = new Map<String,String>{
        ''   => '','01' => '1','02' => '2','03' => '3','04' => '4','05' => '5'
    };

    /**
     * テーブルデータ取得
     * 担当者一覧　
     */
    public static I_ColiDatas.Charge getCharge(E_Policy__c policy){
        I_ColiDatas.Charge charge = new I_ColiDatas.Charge();

        // 主募集人存在フラグ
        charge.hasMainMr = String.isNotBlank(policy.MainAgentName__c);
        // 主募集人名
        charge.mainAgentName = E_Util.strblank2Value(policy.MainAgentName__c, I_Const.LABEL_NONE_VALUE);
        // 主事務所名
        charge.mainAccountName = E_Util.strblank2Value(policy.MainAgentAccountName__c, I_Const.LABEL_NONE_VALUE);
        // 主担当MR
        charge.mainMr = E_Util.strblank2Value(policy.MainAgentAccountMRName__c, I_Const.LABEL_NONE_VALUE);
        // 主担当支社
        charge.mainBr = E_Util.strblank2Value(policy.MainAgent__r.Account.AGNTBR_NM__c, I_Const.LABEL_NONE_VALUE);
        // 主割合
        if(policy.COMM_SPLITC01__c != null){
            charge.mainSplitc = policy.COMM_SPLITC01__c.format() + '%';
        }else{
            charge.mainSplitc = I_Const.LABEL_NONE_VALUE;
        }

        // 従募集人存在フラグ
        charge.hasSubMr = String.isNotBlank(policy.SubAgentName__c);
        // 従募集人名
        charge.subAgentName = E_Util.strblank2Value(policy.SubAgentName__c, I_Const.LABEL_NONE_VALUE);
        // 従事務所名
        charge.subAccountName = E_Util.strblank2Value(policy.SubAgentAccountName__c, I_Const.LABEL_NONE_VALUE);
        // 従担当MR
        charge.subMr = E_Util.strblank2Value(policy.SubAgentAccountMRName__c, I_Const.LABEL_NONE_VALUE);
        // 従担当支社
        charge.subBr = E_Util.strblank2Value(policy.SubAgent__r.Account.AGNTBR_NM__c, I_Const.LABEL_NONE_VALUE);
        // 従割合
        if(policy.COMM_SPLITC02__c != null){
            charge.subSplitc = policy.COMM_SPLITC02__c.format() + '%';
        }else{
            charge.subSplitc = I_Const.LABEL_NONE_VALUE;
        }

        return charge;
    }

    /**
     * テーブルデータ取得
     * 保障内容
     */
    public static List<I_ColiDatas.Cover> getCovers(List<E_COVPF__c> covpfs, E_COVPF__c mainCovpf){
        List<I_ColiDatas.Cover> covers = new List<I_ColiDatas.Cover>();
        I_ColiDatas.Condition specialCondition = I_HealthConditionCheck.getKenkotaiWarimashiSakugenCode(covpfs); 

        // 合計
        Decimal premiumSum = 0;

        // 個人保険特約
        for(E_COVPF__c covpf : covpfs){
            // 表示データクラス
            I_ColiDatas.Cover cover = new I_ColiDatas.Cover();

            if(covpf != null){
                cover.covpf = covpf;
                cover.hokenKikan = covpf.COMM_ZRCESDSC__c;
                cover.haraikomiKikan = covpf.COMM_ZPCESDSC__c;
            }

            // 保障内容　C：主契約、C以外：特約
            if(I_Const.COVPF_COLI_ZCRIND_C.equals(covpf.COLI_ZCRIND__c) ){
                // 主契約セット
                mainCovpf = covpf;
                cover.isMain = true;
                cover.hosyouNaiyou = I_Const.COVPF_COLI_ZCRIND_C_LABEL;
            }else{
                cover.hosyouNaiyou = I_Const.COVPF_COLI_ZCRIND_R_LABEL;
            }

            // 保険種類
            cover.hokenSyurui = I_PolicyUtil.addZCOVRNAM(covpf.COLI_ZCOVRNAMES__c, covpf.DPIND__c, covpf.ZDCPFLG__c);

            // 保険金額/入院給付金日額/年金（月）額 :現在
            cover.genzaiHoken = I_PolicyUtil.getGenzaiHoken(covpf);

            // 保険金額/入院給付金日額/年金（月）額 :加入時
            if(covpf.COLI_ZSTDSUM__c != null){
                cover.kanyuHoken = covpf.COLI_ZSTDSUM__c.format();
            }

            // 保険料(※1)(円)
            if(covpf.COLI_INSTPREM__c != null){
                cover.hokenryou = covpf.COLI_INSTPREM__c.format();

                // 合計に加算
                premiumSum += covpf.COLI_INSTPREM__c;
            }

            // 指定代理人
            cover.isDesignation = false;

            covers.add(cover);
        }

        if( specialCondition.IsStandard == false ){
            I_ColiDatas.Cover mortalCover = new I_ColiDatas.Cover();
            mortalCover.hosyouNaiyou = '特約';   
            mortalCover.hokenSyurui = getSpecialConditionRecord( specialCondition, mainCovpf );
            mortalCover.genzaiHoken = I_Const.LABEL_HYPHEN;
            mortalCover.kanyuHoken = I_Const.LABEL_HYPHEN;
            mortalCover.hokenryou = I_Const.LABEL_HYPHEN;
            mortalCover.hokenKikan = I_Const.LABEL_HYPHEN;
            mortalCover.haraikomiKikan = I_Const.LABEL_HYPHEN;
            mortalCover.isCondition = true;
            covers.add(mortalCover);
        }

        if( specialCondition.LienCondition != null && specialCondition.LienCondition != ''){
            I_ColiDatas.Cover lienCover = new I_ColiDatas.Cover();
            lienCover.hosyouNaiyou = '特約';   
            lienCover.hokenSyurui = getLienRecord(specialCondition, mainCovpf );
            lienCover.genzaiHoken = I_Const.LABEL_HYPHEN;
            lienCover.kanyuHoken = I_Const.LABEL_HYPHEN;
            lienCover.hokenryou = I_Const.LABEL_HYPHEN;
            lienCover.hokenKikan = I_Const.LABEL_HYPHEN;
            lienCover.haraikomiKikan = I_Const.LABEL_HYPHEN;
            lienCover.isCondition = true;
            covers.add(lienCover);
        }

        // 主契約の指定代理請求特約
        if(mainCovpf.COLI_ZDPTYDSP__c){
            I_ColiDatas.Cover designationCover = new I_ColiDatas.Cover();
            designationCover.hosyouNaiyou = '特約';
            designationCover.hokenSyurui = '指定代理請求特約';
            designationCover.genzaiHoken = I_Const.LABEL_HYPHEN;
            designationCover.kanyuHoken = I_Const.LABEL_HYPHEN;
            designationCover.hokenryou = I_Const.LABEL_HYPHEN;
            designationCover.hokenKikan = I_Const.LABEL_HYPHEN;
            designationCover.haraikomiKikan = I_Const.LABEL_HYPHEN;
            designationCover.isDesignation = true;
            covers.add(designationCover);
        }

        // 合計行
        I_ColiDatas.Cover totalCover = new I_ColiDatas.Cover();
        totalCover.isTotal = true;
        totalCover.hosyouNaiyou = '合計保険料';
        totalCover.hokenryou = premiumSum.format();
        covers.add(totalCover);

        return covers;
    }

    /**
     * テーブルデータ取得
     * 契約者
     */
    public static I_ColiDatas.Contractor getContractor(E_Policy__c policy){
        I_ColiDatas.Contractor contractor = new I_ColiDatas.Contractor();

        // 契約者名
        contractor.name = E_Util.strblank2Value(policy.ContractorName__c, I_Const.LABEL_NONE_VALUE);
        // 契約者名（カナ）
        contractor.nameKana = E_Util.strblank2Value(policy.ContractorE_CLTPF_ZCLKNAME__c, I_Const.LABEL_NONE_VALUE);
        // 顧客番号
        contractor.clntnum = E_Util.strblank2Value(policy.ContractorCLTPF_CLNTNUM__c, I_Const.LABEL_NONE_VALUE);
        // 契約者住所
        contractor.address = E_Util.strblank2Value(I_PolicyUtil.getAddress(policy.ContractorE_CLTPF_CLTPCODE__c, policy.ContractorAddress__c), I_Const.LABEL_NONE_VALUE);
        // 電話番号
        contractor.phone = E_Util.strblank2Value(policy.ContractorE_CLTPF_CLTPHONE01__c, I_Const.LABEL_NONE_VALUE);
        // 生年月日, 年齢
        if(I_PolicyUtil.isVal09(policy.ContractorE_CLTPF_DOB__c)){
            // 生年月日
            contractor.birth = I_Const.LABEL_ASTERISK;
            // 年齢
            contractor.altAge = I_Const.LABEL_ASTERISK;
        }else{
            // 生年月日
            contractor.birth = E_Util.strblank2Value(E_Util.formatBirthday(policy.ContractorE_CLTPF_DOB__c), I_Const.LABEL_NONE_VALUE);
            // 年齢
            contractor.age = policy.ContractorAge__c;
        }

        return contractor;
    }

    /**
     * テーブルデータ取得
     * 被保険者（主たる被保険者）
     */
    public static I_ColiDatas.Insured getInsured(E_Policy__c policy){
        I_ColiDatas.Insured insured = new I_ColiDatas.Insured();

        // 被保険者名
        insured.name = E_Util.strblank2Value(policy.InsuredName__c, I_Const.LABEL_NONE_VALUE);
        // 被保険者名（カナ）
        insured.nameKana = E_Util.strblank2Value(policy.InsuredE_CLTPF_ZCLKNAME__c, I_Const.LABEL_NONE_VALUE);
        // 顧客番号
        insured.clntnum = E_Util.strblank2Value(policy.InsuredCLTPF_CLNTNUM__c, I_Const.LABEL_NONE_VALUE);
        // 性別
        insured.sex = E_Util.strblank2Value(policy.InsuredCLTPF_ZKNJSEX__c, I_Const.LABEL_NONE_VALUE);
        // 住所
        insured.address = E_Util.strblank2Value(I_PolicyUtil.getAddress(policy.InsuredE_CLTPF_CLTPCODE__c, policy.InsuredE_COMM_ZCLADDR__c), I_Const.LABEL_NONE_VALUE);
        // 電話番号
        insured.phone = E_Util.strblank2Value(policy.InsuredE_CLTPF_CLTPHONE01__c, I_Const.LABEL_NONE_VALUE);
        // 生年月日, 現在年齢, 加入時年齢
        if(I_PolicyUtil.isVal09(policy.InsuredCLTPF_DOB__c)){
            // 生年月日
            insured.birth = I_Const.LABEL_ASTERISK;
            // 現在年齢
            insured.altAge = I_Const.LABEL_ASTERISK;
        }else{
            // 生年月日
            insured.birth = E_Util.strblank2Value(E_Util.formatBirthday(policy.InsuredCLTPF_DOB__c), I_Const.LABEL_NONE_VALUE);
            // 現在年齢
            insured.age = policy.InsuredAge__c;
        }
        // 加入時年齢
        insured.anbccd = I_PolicyUtil.getInsuredANBCCD(policy.Id);

        return insured;
    }

    /**
     * テーブルデータ取得
     * 受取人
     */
    public static I_ColiDatas.Beneficiary getBeneficiary(E_CRLPF__c crlpf, Boolean isFixedLine){
        I_ColiDatas.Beneficiary beneficiary = new I_ColiDatas.Beneficiary();

        if(isFixedLine){
            // 受取人種別
            beneficiary.bnytp = '受取人';
            // 受取人
            beneficiary.name = '約款記載の通り';
            // 顧客番号
            beneficiary.clntnum = I_Const.LABEL_NONE_VALUE;
            // 性別
            beneficiary.sex = I_Const.LABEL_NONE_VALUE;

        }else{
            // 受取人種別
            beneficiary.bnytp = crlpf.BNYTYP__c;
            // 受取人種別名
            beneficiary.bnytpName = E_Util.strblank2Value(crlpf.BNYTYPName__c, I_Const.LABEL_NONE_VALUE);
            // 受取人
            beneficiary.name = E_Util.strblank2Value(crlpf.ZCLNAME__c, I_Const.LABEL_NONE_VALUE);
            // 顧客番号
            beneficiary.clntnum = E_Util.strblank2Value(crlpf.CLNTNUM__c, I_Const.LABEL_NONE_VALUE);
            // 性別
            beneficiary.sex = E_Util.strblank2Value(crlpf.ZKNJSEX__c, I_Const.LABEL_NONE_VALUE);
            // 受取割合
            if(crlpf.BNYTYP__c == E_Const.BNYTYP_09 || crlpf.BNYPC__c == 0){
                beneficiary.bnypc = null;
                beneficiary.altBnypc = I_Const.LABEL_NONE_VALUE;
            }else{
                beneficiary.bnypc = crlpf.BNYPC__c;
                beneficiary.altBnypc = null;
            }
            // 現在年齢
            if(I_PolicyUtil.isVal09(crlpf.DOB__c)){
                beneficiary.altAge = I_Const.LABEL_ASTERISK;
            }else{
                beneficiary.age = crlpf.Age__c;
            }
        }

        return beneficiary;
    }

    /**
     * テーブルデータ取得
     * 払込・口座情報
     */
    public static I_ColiDatas.Bank getBank(E_Policy__c policy, Boolean isAgent){
        I_ColiDatas.Bank bank = new I_ColiDatas.Bank();

        // 金融機関
        bank.zebknm = E_Util.strblank2Value(policy.COMM_ZEBKNM__c, I_Const.LABEL_NONE_VALUE);
        // 口座名義人
        bank.zebkacow =  E_Util.strblank2Value(policy.COMM_ZEBKACOW__c, I_Const.LABEL_NONE_VALUE);
        // 口座番号/通帳番号
        bank.bankacckey = E_Util.strblank2Value(maskingBankacckey(policy.COMM_BANKACCKEY__c, isAgent), I_Const.LABEL_NONE_VALUE);

        // 前納
        if(policy.COLI_ZADVPDCF__c){
            bank.zadvdesc = '有';
        }else{
            bank.zadvdesc = I_Const.LABEL_NONE_VALUE;
        }

        // 払込方法, 払込経路
        if(policy.COMM_STATCODE__c == I_Const.POLICY_STATCODE_Y){
            bank.zefreq = I_Const.POLICY_STATUS_UPDATING;
            bank.zechinl = I_Const.POLICY_STATUS_UPDATING;
        }else{
            bank.zefreq = E_Util.strblank2Value(policy.COMM_ZEFREQ__c, I_Const.LABEL_NONE_VALUE);
            bank.zechinl = E_Util.strblank2Value(policy.COMM_ZECHNL__c, I_Const.LABEL_NONE_VALUE);
        }

        // 次回払込
        if((policy.COMM_STATCODE__c == I_Const.POLICY_STATCODE_Y || policy.COLI_ZNPTDDCF__c == false)){
            if(policy.COMM_STATCODE__c == I_Const.POLICY_STATCODE_Y){
                bank.ptdate = I_Const.POLICY_STATUS_UPDATING;
            }else{
                bank.ptdate = I_Const.LABEL_HYPHEN;
            }
        }else{
            if(I_PolicyUtil.isVal09(policy.COMM_PTDATE__c)){
                bank.ptdate = I_Const.LABEL_ASTERISK;
            }else{
                bank.ptdate = E_Util.strblank2Value(E_Util.getFormatDate(policy.COMM_PTDATE__c, I_Const.DATEFROMAT_YYYYMM), I_Const.LABEL_NONE_VALUE);
            }
        }

        // 最終払込
        if(policy.COMM_STATCODE__c == I_Const.POLICY_STATCODE_Y){
            bank.pcesdte = I_Const.POLICY_STATUS_UPDATING;
        }else{
            if(policy.COLI_ZNPTDDCF__c == false || policy.COLI_ZPCDTDCF__c == true){
                if(I_PolicyUtil.isVal09(policy.COLI_PCESDTE__c)){
                    bank.pcesdte = I_Const.LABEL_ASTERISK;
                }else{
                    bank.pcesdte = E_Util.strblank2Value(E_Util.getFormatDate(policy.COLI_PCESDTE__c, I_Const.DATEFROMAT_YYYYMM), I_Const.LABEL_NONE_VALUE);
                }
            }else if(policy.COLI_ZNPTDDCF__c == true && policy.COLI_ZPCDTDCF__c == false){
                bank.pcesdte = I_Const.PCESDTE_WHOLE;
            }
        }

        // 口座振替区分
        if(policy.COLI_ZBKPODIS__c == I_Const.POLICY_ZBKPODIS_20){
            // 支店
            bank.zebkbrnm = I_Const.LABEL_HYPHEN;
            // 預金種目/通帳記号
            bank.zebkType = E_Util.strblank2Value(policy.COMM_ZEBKBRNM__c, I_Const.LABEL_NONE_VALUE);
        }else{
            // 支店
            bank.zebkbrnm = E_Util.strblank2Value(policy.COMM_ZEBKBRNM__c, I_Const.LABEL_NONE_VALUE);
            // 預金種目/通帳記号
            bank.zebkType = E_Util.strblank2Value(policy.COMM_ZEBKACTP__c, I_Const.LABEL_NONE_VALUE);
        }

        return bank;
    }

    /**
     * テーブルデータ取得
     * 解約時受取金額
     */
    public static I_ColiDatas.Surrender getSurrender(E_Policy__c policy){
        I_ColiDatas.Surrender surrender = new I_ColiDatas.Surrender();

        // 合計金額（円）
        surrender.surrenderValueSum = I_PolicyUtil.getSurrenderValueSum(policy);
        // 解約返戻金（円）
        surrender.zcshval = policy.COLI_ZCSHVAL__c;
        // 貸付金等精算額（円）
        surrender.zloantot = policy.COLI_ZLOANTOT__c;
        // 前納未経過保険料（円）
        surrender.zeadvprm = policy.COLI_ZEADVPRM__c;
        // 合計金額（円）: 文字列
        surrender.altCharSurrenderValueSum = (policy.COLI_ZCVDCF__c || policy.COLI_ZUNPCF__c) ? '?' : '';
        // 解約返戻金（円）: 文字列
        surrender.altCharZcshval = policy.COLI_ZCVDCF__c ? '?' : '';
        // 未経過期間に対応する額（円）: 表示フラグ, 文字列
        if(policy.COMM_ZEFREQ__c=='月払'){
            surrender.isViewZunprem = true;
            surrender.altCharZunprem = '0';
        }else{
            surrender.isViewZunprem = policy.COLI_ZUNPDCF__c;
            surrender.altCharZunprem = policy.COLI_ZUNPCF__c ? '?' : '';
        }
        // 未経過期間に対応する額（円）
        if(surrender.isViewZunprem == true){
            surrender.zunprem = policy.COLI_ZUNPREM__c;
        }else{
            surrender.zunprem = null;
        }

        return surrender;
    }

    /**
     * テーブルデータ取得
     * 前納
     */
    public static I_ColiDatas.Advance getAdvance(E_Policy__c policy){
        I_ColiDatas.Advance advance = new I_ColiDatas.Advance();

        // 前納保険料入金日
        advance.rcptdt = policy.COLI_RCPTDT__c;
        // 前納保険料額（円）
        advance.avprmrcv = policy.COLI_AVPRMRCV__c;
        // 前納保険料の残回数
        advance.remnbann = policy.COLI_REMNBANN__c;
        // 前納表示フラグ
        advance.isViewAdvance = policy.COLI_ZADVPDCF__c;

        return advance;
    }

    /**
     * テーブルデータ取得
     * 質権
     */
    public static I_ColiDatas.Pledge getPledge(E_CRLPF__c crlpf){
        I_ColiDatas.Pledge pledge = new I_ColiDatas.Pledge();

        // 質権
        pledge.haspledge =  E_Util.strblank2Value(E_Const.CLRRROLE_NE_LABEL, I_Const.LABEL_NONE_VALUE);
        // 質権名
        pledge.zclname = E_Util.strblank2Value(crlpf.ZCLNAME__c, I_Const.LABEL_NONE_VALUE);

        return pledge;
    }

    /**
     * テーブルデータ取得
     * 契約者貸付
     */
    public static I_ColiDatas.Lone getLone(E_Policy__c policy){
        I_ColiDatas.Lone lone = new I_ColiDatas.Lone();

        // 契約者貸付合計(円)
        lone.zepltot = policy.COLI_ZEPLTOT__c;
        // 契約者貸付元金(円)
        lone.zeplprn = policy.COLI_ZEPLPRN__c;
        // 契約者貸付利息(円)
        lone.zeplint = policy.COLI_ZEPLINT__c;
        // 契約者貸付繰入期間
        lone.zplcapt =  E_Util.strblank2Value(policy.COLI_ZPLCAPT__c, I_Const.LABEL_NONE_VALUE);
        // 保険料振替貸付利率
        lone.zplrate = policy.COLI_ZPLRATE__c;
        // 振替貸付表示フラグ
        lone.isViewLone = policy.COMM_ZPLDCF__c && policy.COLI_ZEPLTOT__c != 0;

        return lone;
    }

    /**
     * テーブルデータ取得
     * 保険料自動振替貸付
     */
    public static I_ColiDatas.AutoLone getAutoLone(E_Policy__c policy){
        I_ColiDatas.AutoLone autoLone = new I_ColiDatas.AutoLone();

        // 保険料振替貸付合計(円)
        autoLone.zeapltot = policy.COLI_ZEAPLTOT__c;
        // 保険料振替貸付元金(円)
        autoLone.zeaplprn = policy.COLI_ZEAPLPRN__c;
        // 保険料振替貸付利息(円)
        autoLone.zeaplint = policy.COLI_ZEAPLINT__c;
        // 保険料振替貸付繰入期間
        autoLone.zaplcapt = E_Util.strblank2Value(policy.COLI_ZAPLCAPT__c, I_Const.LABEL_NONE_VALUE);
        // 保険料振替貸付利率
        autoLone.zaplrate = policy.COLI_ZAPLRATE__c;
        // 振替貸付表示フラグ
        autoLone.isViewAutoLone = policy.COLI_ZAPLDCF__c && policy.COLI_ZEAPLTOT__c != 0;

        return autoLone;
    }

     /**
      * テーブルデータ取得
      * 契約に関する通知
      */
    public static I_ColiDatas.Notification getNotification(Id policyId){
        I_ColiDatas.Notification notification = new I_ColiDatas.Notification();

        // 通知一覧情報
        notification.dto = new E_DownloadNoticeDTO(policyId);
        for(E_DownloadNoticeDTO.Notice dn : notification.dto.noticeList){
            // 通知月
            dn.month = E_Util.strblank2Value(dn.month, I_Const.LABEL_NONE_VALUE);
            // 保険料払込期月
            dn.ZDUEDATE = E_Util.strblank2Value(zeroSuppress(dn.ZDUEDATE), I_Const.LABEL_NONE_VALUE);
            // 保険料払込猶予期間満了日
            dn.LIMITDT = E_Util.strblank2Value(dn.LIMITDT, I_Const.LABEL_NONE_VALUE);
            // 異動内容
            dn.CONTENT = E_Util.strblank2Value(dn.CONTENT, I_Const.LABEL_NONE_VALUE);
        }

        return notification;
    }

    /**
     * テーブルデータ取得
     * 手続履歴
     */
    public static I_ColiDatas.ProcessHistory getProcessHistory(E_PTNPF__c ptnpf){
        I_ColiDatas.ProcessHistory processHistory = new I_ColiDatas.ProcessHistory();

        // 手続完了日
        processHistory.trandate = E_Util.strblank2Value(ptnpf.TRANDATE__c, I_Const.LABEL_NONE_VALUE);
        // 申し出内容
        processHistory.ztrandsc = E_Util.strblank2Value(ptnpf.ZTRANDSC__c, I_Const.LABEL_NONE_VALUE);

        return processHistory;
    }

    /**
     * 消滅テーブルデータ取得
     * 担当者一覧　
     */
    public static I_ColiDatas.Charge getChargeEHDCPF(E_Policy__c policy){
        I_ColiDatas.Charge charge = new I_ColiDatas.Charge();

        List<String> nums;

        // 証券番号が空
        if(String.isBlank(policy.COMM_CHDRNUM__c)){
            return charge;
        }else{
            nums = policy.COMM_CHDRNUM__c.split('\\|');
        }

        // 証券番号不正
        if(nums.size() <> 2){
            return charge;
        }

        //主募集フラグを初期化
        charge.hasMainMr = false;

        // 証券番号の下1桁より判定
        if(nums[1] == '1'){
            // 主募集人存在フラグ
            charge.hasMainMr = String.isNotBlank(policy.MainAgentName__c);
            // 主募集人名
            charge.mainAgentName = E_Util.strblank2Value(policy.MainAgentName__c, I_Const.LABEL_NONE_VALUE);
            // 主事務所名
            charge.mainAccountName = E_Util.strblank2Value(policy.MainAgentAccountName__c, I_Const.LABEL_NONE_VALUE);
            // 主担当MR
            charge.mainMr = E_Util.strblank2Value(policy.MainAgentAccountMRName__c, I_Const.LABEL_NONE_VALUE);
            // 主担当支社
            charge.mainBr = E_Util.strblank2Value(policy.MainAgent__r.Account.AGNTBR_NM__c, I_Const.LABEL_NONE_VALUE);
            // 主割合
            if(policy.COMM_SPLITC__c != null){
                charge.mainSplitc = policy.COMM_SPLITC__c.format() + '%';
            }else{
                charge.mainSplitc = I_Const.LABEL_NONE_VALUE;
            }
        }else{
            // 従募集人存在フラグ
            charge.hasSubMr = String.isNotBlank(policy.SubAgentName__c);
            // 従募集人名
            charge.subAgentName = E_Util.strblank2Value(policy.SubAgentName__c, I_Const.LABEL_NONE_VALUE);
            // 従事務所名
            charge.subAccountName = E_Util.strblank2Value(policy.MainAgentAccountName__c, I_Const.LABEL_NONE_VALUE);
            // 従担当MR
            charge.subMr = E_Util.strblank2Value(policy.MainAgentAccountMRName__c, I_Const.LABEL_NONE_VALUE);
            // 従担当支社
            charge.subBr = E_Util.strblank2Value(policy.SubAgent__r.Account.AGNTBR_NM__c, I_Const.LABEL_NONE_VALUE);
            // 従割合
            if(policy.COMM_SPLITC__c != null){
                charge.subSplitc = policy.COMM_SPLITC__c.format() + '%';
            }else{
                charge.subSplitc = I_Const.LABEL_NONE_VALUE;
            }
        }

        return charge;
    }

    /**
     * 消滅テーブルデータ取得
     * 保障内容
     */
    public static List<I_ColiDatas.Cover> getCoversEHDCPF(List<E_COVPF__c> covpfs){
        List<I_ColiDatas.Cover> covers = new List<I_ColiDatas.Cover>();

        // 個人保険特約
        for(E_COVPF__c covpf : covpfs){
            // 表示データクラス
            I_ColiDatas.Cover cover = new I_ColiDatas.Cover();

            // 主契約判定
            cover.isMain = covpf.DCOLI_ZCOVRDSC__c == '主契約' ? true : false;

            cover.chdrnum = I_ColiUtil.delPipeCdhrnum(covpf.COMM_CHDRNUM__c);                 // 証券番号
            cover.zclname = covpf.DCOLI_ZCLNAME__c;                 // 契約者
            cover.zinsname = covpf.DCOLI_ZINSNAM__c;                // 被保険者
            //cover.hokenSyurui = covpf.COMM_ZCOVRNAM__c;               // 主契約
            //cover.hokenSyurui = covpf.COMM_ZCOVRNAM__c;               // 特約
            cover.hokenSyurui = I_PolicyUtil.addZCOVRNAM(covpf.COLI_ZCOVRNAMES__c, covpf.DPIND__c, covpf.ZDCPFLG__c);
            if(covpf.COMM_SUMINS__c != null){
               cover.genzaiHoken = covpf.COMM_SUMINS__c.format();       // 保険金額
            }
            if(covpf.DCOLI_SINSTAMT__c != null){
                cover.hokenryou = covpf.DCOLI_SINSTAMT__c.format();     // 保険料（円）
            }
            cover.occdate = covpf.DCOLI_OCCDATE__c;                 // 契約日（更新日）

            covers.add(cover);
        }

        return covers;
    }

    /**
     * 消滅テーブルデータ取得
     * 契約者
     */
    public static I_ColiDatas.Contractor getContractorEHDCPF(E_Policy__c policy){
        I_ColiDatas.Contractor contractor = new I_ColiDatas.Contractor();

        // 顧客番号
        contractor.clntnum = E_Util.strblank2Value(policy.COMM_CLNTNUM__c, I_Const.LABEL_NONE_VALUE);
        // 契約者名
        contractor.name = E_Util.strblank2Value(policy.COMM_ZCLNAME__c, I_Const.LABEL_NONE_VALUE);

        return contractor;
    }

    /**
     * 消滅テーブルデータ取得
     * 被保険者（主たる被保険者）
     */
    public static I_ColiDatas.Insured getInsuredEHDCPF(E_Policy__c policy){
        I_ColiDatas.Insured insured = new I_ColiDatas.Insured();

        // 被保険者名
        insured.name = E_Util.strblank2Value(policy.COMM_ZINSNAM__c, I_Const.LABEL_NONE_VALUE);
        // 性別
        insured.sex = E_Util.strblank2Value(policy.COMM_ZKNJSEX__c, I_Const.LABEL_NONE_VALUE);
        // 生年月日
        if(I_PolicyUtil.isVal09(policy.COMM_DOB__c)){
            insured.birth = I_Const.LABEL_ASTERISK;
        }else{
            insured.birth = E_Util.strblank2Value(E_Util.formatBirthday(policy.COMM_DOB__c), I_Const.LABEL_NONE_VALUE);
        }

        return insured;
    }

    /**
     * ダウンロード履歴のJSON作成
     */
    public static String createDlJson(String policyId, String oName, String aName, String gName, String inquirydate, Boolean isIss, String rtDevName, List<E_DownloadNoticeDTO.Notice> noticeList){
            // JSON作成
            E_DownloadCondition.ICOLIDownloadCondi dlCondi= new E_DownloadCondition.ICOLIDownloadCondi();

            // policyId
            dlCondi.policyId = policyId;
            // officeName
            dlCondi.officeName = oName;
            // agentName
            dlCondi.agentName = aName;
            // groupName
            dlCondi.groupName = gName;
            // inquirydate
            dlCondi.inquirydate = inquirydate;
            // isIss
            dlCondi.isIss = isIss ? '1' : '0';
            // policyTypes
            dlCondi.policyTypes = new List<String>{rtDevName};
            // notificationInfos
            if(!noticeList.isEmpty()){
                dlCondi.notificationInfos = new List<E_DownloadCondition.NotificationInfo>();

                // 通知一覧をJSONの通知一覧にセット
                for(E_DownloadNoticeDTO.Notice notice : noticeList){
                    E_DownloadCondition.NotificationInfo info = new E_DownloadCondition.NotificationInfo();
                    info.type = E_Util.strblank2Value(notice.formatName, I_Const.LABEL_NONE_VALUE);
                    info.month = E_Util.strblank2Value(notice.month, I_Const.LABEL_NONE_VALUE);
                    info.zduedate = E_Util.strblank2Value(notice.ZDUEDATE, I_Const.LABEL_NONE_VALUE);
                    info.limitdt = E_Util.strblank2Value(notice.LIMITDT, I_Const.LABEL_NONE_VALUE);
                    info.content = E_Util.strblank2Value(notice.CONTENT, I_Const.LABEL_NONE_VALUE);
                    dlCondi.notificationInfos.add(info);
                }
            }

            // JSON
            return dlCondi.toJSON();
    }

    /**
     * ダウンロード履歴の生成
     */
    public static E_DownloadHistorry__c createDownloadHistorry(String json, String pdfComment){
        // ダウンロード履歴
        E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
        dh.FormNo__c = E_Const.DH_FORMNO_IRIS_COLI;
        dh.Conditions__c = json;
        dh.outputType__c = E_Const.DH_OUTPUTTYPE_PDF;
        dh.Detail__c = pdfComment;
        //this.dh.AdditionalInfo__c = '_' + policy.COMM_CHDRNUM__c + '_' + policy.ContractorName__c;
        return dh;
    }

    /**
     * 募集人の種別（主or従）を判定する
     */
    public static Boolean isViewMain(String targetCode, String mainCode, String subCode){
        Boolean isMain = true;

        // 主
        if(targetCode == mainCode){
            isMain = true;
        // 従
        }else if(targetCode == subCode){
            isMain = false;
        }

        return isMain;
    }

    /**
     * 証券番号からパイプを除去
     */
    public static String delPipeCdhrnum(String cdhrnum){
        if(String.isNotBlank(cdhrnum)){
             Integer c = cdhrnum.indexOf('|');
             if(c > 0){
                cdhrnum = cdhrnum.substring(0,c);
             }
        }
        return cdhrnum;
    }

    /**
     * 募集人ユーザの場合、口座番号/通帳番号の5文字目以降をマスキング
     */
    private static String maskingBankacckey(String str, Boolean isAgent){

        if(String.isBlank(str)){
            return str;
        }

        if(isAgent == true && str.length() >= 5){
            String target = str.substring(0, 4);
            str = target.rightPad(str.length(), 'X');
        }

        return str;
    }

    /**
     * 払込期月のフォーマット
     * 0X月の場合に、先頭0を削除する
     */
    private static String zeroSuppress(String str){
        if(String.isNotBlank(str) && str.length() == 3){
            if(str.substring(0,1) == '0'){
                str = str.substring(1,3);
            }
        }
        return str;
    }

    @testvisible
    private static String getSpecialConditionRecord( I_ColiDatas.condition specialCondition ,E_COVPF__c mainCov ){
        String hokenSyurui = '';
        if( specialCondition.MortalityClassWarimashi != null ){
                if( Integer.valueOf( mainCov.E_Policy__r.COMM_CCDATE__c ) >= I_PIPConst.SPECIAL_CONDITIONS_STANDARDDATE ) hokenSyurui += '新';
                hokenSyurui += '特別条件特約（割増保険料';
                hokenSyurui += motalMap.get( specialCondition.MortalityClassWarimashi );
                hokenSyurui += ')';            
        } else {
            switch on specialCondition.MortalityClassKenkotaiYuryotai{
                when 'Y'{
                    hokenSyurui = '健康体料率適用特約';
                    if(specialCondition.TeikicoverFlg) hokenSyurui += '(特約用)'; 
                }
                when 'Z'{
                    hokenSyurui = '優良体料率適用特約';
                    if(specialCondition.TeikicoverFlg) hokenSyurui += '(特約用)'; 
                }
                when else{
                    hokenSyurui = '';
                }
            }
        }

        return hokenSyurui;
    }

    @testvisible
    private static String getLienRecord ( I_ColiDatas.condition specialCondition ,E_COVPF__c mainCov ){
        String lienCode = '';
        if(maincov != null ){
            if( Integer.valueOf( mainCov.E_Policy__r.COMM_CCDATE__c ) >= I_PIPConst.SPECIAL_CONDITIONS_STANDARDDATE ) lienCode += '新';
            lienCode += '特別条件特約（保険金削減';
            lienCode += sakugenMap.get( specialCondition.LienCondition);
            lienCode += '年）';
        }
        return lienCode;
    }
/*
	public static I_Colidatas.condition getKenkotaiWarimashiSakugenCode(List<E_COVPF__c> covpfs){
		I_ColiDatas.Condition Condition = new I_ColiDatas.Condition() ;

		String mainMortalCls = '';
		String mainCRCode = '';
		String mainLienCode = '';
		String subMortalCls = 'S';

		for(E_COVPF__c cover: covpfs){
			//主契約判定
			if(cover.COLI_ZCRIND__c == 'C'){
				mainMortalCls = cover.COLI_MORTCLS__c;
				system.debug(' mainMortalCls = ' + mainMortalCls );
				mainCRCode = cover.COMM_CRTABLE2__c;
				if(cover.COLI_LIENCD__c == null) mainLienCode = '' ; else  mainLienCode = cover.COLI_LIENCD__c;
			}else{
				if(cover.COLI_MORTCLS__c != '' && cover.COLI_MORTCLS__c != null && cover.COLI_MORTCLS__c != 'S' && cover.COMM_CRTABLE2__c!='TR'){
					if(subMortalCls != 'S'){
						// エラーハンドリング
//						errorMessages.add(I_PIPConst.ERROR_MESSAGE_BIZLOGIC_TOO_MANY_NOT_STANDART);
					}
					subMortalCls = cover.COLI_MORTCLS__c;
				}
			}

		}

		switch on mainMortalcls {



			when '', 'S',null {

				switch on subMortalCls{
					when 'S',null {
						Condition.NomalCondition = 'S';
					}

					when 'Y' {
						Condition.NomalCondition = 'Y';
					}

					when 'Z' {
						Condition.NomalCondition = 'Z';
					}

					when else {
						// エラーハンドリング
//						errorMessages.add('subMortalCls:'+subMortalCls);
					}
				}
			}

			when 'Y' {
				Condition.NomalCondition = 'Y';
			}

			when 'Z' {
				Condition.NomalCondition = 'Z';
			}

			when else {
				String warimashi = mainMortalCls;
				if(mainCRCode == I_PIPConst.CR_CODE_TS || mainCRCode == I_PIPConst.CR_CODE_TL){
					Condition.DDALDFlg = true;
				}

				if(String.isEmpty(warimashi)){
					// エラーハンドリング
//					ErrorMessages.add('mainCRCode:'+mainCRCode);
//					ErrorMessages.add('mainMortalcls:'+mainMortalcls);
				}

				Condition.NomalCondition = '1';
				Condition.MortalityClass = warimashi;
			}

		}

		String sakugen = mainLienCode;

		if(sakugen == null){
			// エラーハンドリング
//			errorMessages.add('mainLienCode:'+mainLienCode);
		}

		Condition.LienCondition = sakugen;
		System.debug(mainLienCode);
		System.debug(sakugen);

		return Condition;

	}
*/
}