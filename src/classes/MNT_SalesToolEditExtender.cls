global with sharing class MNT_SalesToolEditExtender extends SkyEditor2.Extender{
    
	private MNT_SalesToolEditController extension;
	private I_ContentMaster__c record;
	private String beforeParam;
	private boolean isEdit;
	
    public MNT_SalesToolEditExtender(MNT_SalesToolEditController extension){
        this.extension = extension;
        isEdit = false;
    }

    global override void init(){
    	this.record = extension.record;
    	record.ClickAction__c = I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE;
    	Map<String, String> params = ApexPages.CurrentPage().getParameters();
    	if(params.containsKey('id')){
    		String pageId = params.get('id');
    		if(!String.isBlank(pageId)){
    			beforeParam = pageId;
		    	if(params.containsKey('clone') && params.get('clone').equals('1')){
			    	record.UpsertKey__c = null;
		    	}else{
		    		isEdit = true;
		    	}
    		}
    	}else{
    		I_ContentMaster__c parentContent = I_ContentMasterDao.getRecordByUpsertKey('セクション|販売促進資料|');
    		if(parentContent != null){
    			record.ParentContent__c = parentContent.Id;
    		}
    	}
    }

    public PageReference doCancel(){
		if(!String.isBlank(beforeParam)){
			return new PageReference('/apex/MNT_SalesToolView?id=' + beforeParam);
		}else{
			return new PageReference('/apex/MNT_SalesToolMainteSVE');
		}
	}


	public PageReference doSave(){

		if(String.isBlank(record.Name)){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料名を入力してください。'));
			return null;
		}else if(String.isBlank(record.DocumentNo__c)){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料管理番号を入力してください。'));
			return null;
		}else{
			I_ContentMaster__c exitContent;
			if(!isEdit){
				exitContent = I_ContentMasterDao.getRecByDocumentNoAndName(record.DocumentNo__c,record.Name);				
			}
			if(exitContent == null || isEdit){
				MNT_Util.saveParentWithAttachment(record, null);
				return new PageReference('/apex/MNT_SalesToolView?id=' + record.Id);
			}else{
				ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料名と資料管理番号が同一の資料がございます。'));
				return null;
			}
		}
	}

}