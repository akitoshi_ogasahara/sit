public abstract with sharing class E_CSVExportController extends E_AbstractController{
	
	/*	CSV区切り文字	*/
	public virtual String CSV_SEPARATOR(){
		return ',';
	}

	/*	CSV改行文字	*/
	public virtual String CSV_BREAKELINE(){
		return '\n';
	}

	/*	主オブジェクトAPI名	*/
	public abstract String mainSObjectAPIName();

	/*	レコード取得件数	*/
	public virtual Integer limitRows(){
		return 9999;				//デフォルトは9999行
	}
	
	/*	WHERE句文字列パラメータ名	*/
	public static final String SOQL_WHERE = 'qwh';
	
	/*	WHERE句文字列パラメータ名	*/
	public virtual String getFileName(){
		String sdt = System.Now().format('yyyyMMddHHmmss',UserInfo.getTimeZone().getID());
		return 'results_' + sdt +'.csv';	
	}
	
	
	/**
	 *	CSVカラムの定義と追加
	 */
	private List<E_CSVColumnDescribe.BaseField> cols;
	public List<E_CSVColumnDescribe.BaseField> addColumn(E_CSVColumnDescribe.BaseField col){
		if(cols==null){
			cols = new List<E_CSVColumnDescribe.BaseField>();
		}
		cols.add(col);
		return cols;
	}
	
	/**
	 *	Constructor
	 */
	public E_CSVExportController(){
		//
	}
	
	private String buildSOQL(){
		system.assert(String.isNotBlank(mainSObjectAPIName()), '主オブジェクトのAPI名が設定されていません。');
		List<String> fieldNames = new List<String>();
		for(E_CSVColumnDescribe.BaseField fld:cols){
			fieldNames.add(fld.fieldAPI);
		}		

		String soql = 'SELECT ';
		soql += String.join(fieldNames, ',');
		soql += ' FROM ';
		soql += mainSObjectAPIName();
		soql += ' ' + getWhereAfterClause();
		if(limitRows()>0){
			soql += ' LIMIT ' + String.valueOf(limitRows());
		}
		
		return soql;
	}

	public String getWhereAfterClause(){
		String s = ApexPages.currentPage().getParameters().get(SOQL_WHERE);
		String decrypted = E_EncryptUtil.getDecryptedString(s);
		
		if(String.isNotBlank(decrypted)){
			return ' ' + decrypted;
		}

		return '';
	}

	/**	====================================
	 *		CSVヘッダー文字列の生成
	 *			@param	なし
	 */
	public virtual String getCSVHeader(){
		String sObjNm = mainSObjectAPIName();
		List<String> header = new List<String>();
		for(E_CSVColumnDescribe.BaseField fld: cols){
			header.add(fld.getFieldLabel(sObjNm));
		}
		return String.join(header, CSV_SEPARATOR()) + CSV_BREAKELINE();
	}
	
	/**	====================================
	 *		CSVデータの生成
	 *			@param	records	　レコードリスト
	 */
	public List<String> getCSVLines(){
		List<SObject> records = Database.query(buildSOQL());
		List<String> lines = new List<String>();
		for(SObject rec:records){
			List<String> ln = new List<String>();
			for(E_CSVColumnDescribe.BaseField fld: cols){
				ln.add(fld.getEscapedString(rec));
			}
			
			lines.add(String.join(ln, CSV_SEPARATOR()) + CSV_BREAKELINE());
		}
		
		return lines;
	}

	/**	====================================
	 *		Cacheパラメータ値
	 *			IE8の場合にCacheをFalseにするとDLできなくなるので特別にTrueとする
	 */
	public Boolean getCacheSetting(){
		//IE8の時だけTrue
		return E_Util.isUserAgentIE8();
	}


	/**	====================================
	 *		debug
	 */
	public Boolean getShowDebug(){
		return false;		//CSVの最後にDebugStringsを出す場合はtrueにする;
	}
	public String getDebugStrings(){
		return 'debug strings:'+buildSOQL();
	}

}