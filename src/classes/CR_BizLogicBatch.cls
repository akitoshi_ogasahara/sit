/**
 * 代理店提出レコード（外部委託、管理規定）作成、SharingのBatchApex
 */
global with sharing class CR_BizLogicBatch extends E_AbstractCommonBatch {
    
    //BaseSOQL
    //public static String BASE_SOQL = 
    //    'Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c, ZMRCODE__c, (Select Id, Name, MR__c, Account__c, RecordTypeId From CR_Agencys__r) From Account Where E_IsAgency__c = true And E_COMM_VALIDFLAG__c <> \'2\'';

    /**
     * BatchApexの開始処理です
     * @param ctx
     * @return Database.QueryLocator 
     */
    global Database.QueryLocator start(Database.BatchableContext ctx){
        //return Database.getQueryLocator([Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c, ZMRCODE__c, (Select Id, Name, MR__c, Account__c, RecordTypeId From CR_Agencys__r) From Account Where E_IsAgency__c = true And (E_COMM_VALIDFLAG__c <> '2' And E_COMM_VALIDFLAG__c <> null)]);
    	return Database.getQueryLocator([Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c, ZMRCODE__c 
    										From Account 
    											Where E_IsAgency__c = true 
    												And (E_COMM_VALIDFLAG__c <> '2' And E_COMM_VALIDFLAG__c <> null)]);
    }
}