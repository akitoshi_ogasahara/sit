/*
 * CC_CaseSRActivityTgrHdlTest
 * Test class of CC_CaseSRActivityTgrHdl
 * created  : Accenture 2018/4/4
 * modified :
 */

@isTest
private class CC_CaseSRActivityTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create SRTypeMaster list
			List<CC_SRTypeMaster__c> SRTypeMasterList = CC_TestDataFactory.getSRTypeMasterSkelList();
			insert SRTypeMasterList;

			//Create ActivityTemplate
			CC_ActivityTemplate__c ActivityTemplate = CC_TestDataFactory.getActivityTemplateSkel();
			insert ActivityTemplate;

			//Create SRTypeMasterActivityTemplate
			CC_SRTypeMasterActivityTemplate__c SRTypeMasterActivityTemplate = CC_TestDataFactory.getSRTypeMasterActivityTemplateSkel(SRTypeMasterList[0].Id, ActivityTemplate.Id);
			insert SRTypeMasterActivityTemplate;

			//Create ActivityMaster
			CC_ActivityMaster__c ActivityMaster = CC_TestDataFactory.getActivityMasterSkel();
			insert ActivityMaster;

			//Create ActivityTemplateActivityMaster
			CC_ActivityTemplateActivityMaster__c ActivityTemplateActivityMaster = CC_TestDataFactory.getActivityTemplateActivityMasterSkel(ActivityMaster.Id, ActivityTemplate.Id);
			insert ActivityTemplateActivityMaster;

		}
	}

	/**
	* Insert Case
	*/
	static testMethod void caseSRActivityTriggerHandlerTestInsert01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get exsiting SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [select id from CC_SRTypeMaster__c where Name = '10001'];

			Test.startTest();
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			insert newCase;
			Test.stopTest();

		}
	}

	/**
	* Update Case
	* Update CMN_Source__c field
	*/
	static testMethod void caseSRActivityTriggerHandlerTestUpdate01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get exsiting SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [select id from CC_SRTypeMaster__c where Name = '10001'];

			//Create and update Case
			Case newcase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			insert newCase;
			newcase.CMN_Source__c = 'Fax';

			Test.startTest();
			update newcase;
			Test.stopTest();

		}
	}

	/**
	* Update Case
	* Update CC_SRTypeId__c field
	*/
	static testMethod void caseSRActivityTriggerHandlerTestUpdate02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get exsiting SRTypeMaster list
			List<CC_SRTypeMaster__c> SRTypeMasterList = [select id from CC_SRTypeMaster__c order by Name];

			//Create ActivityTemplate
			CC_ActivityTemplate__c ActivityTemplate = CC_TestDataFactory.getActivityTemplateSkel();
			insert ActivityTemplate;

			//Create SRTypeMasterActivityTemplate
			CC_SRTypeMasterActivityTemplate__c SRTypeMasterActivityTemplate = CC_TestDataFactory.getSRTypeMasterActivityTemplateSkel(SRTypeMasterList[1].Id, ActivityTemplate.Id);
			insert SRTypeMasterActivityTemplate;

			//Create ActivityMaster
			CC_ActivityMaster__c ActivityMaster = CC_TestDataFactory.getActivityMasterSkel();
			insert ActivityMaster;

			//Create and update ActivityTemplateActivityMaster
			CC_ActivityTemplateActivityMaster__c ActivityTemplateActivityMaster = CC_TestDataFactory.getActivityTemplateActivityMasterSkel(ActivityMaster.Id, ActivityTemplate.Id);
			ActivityTemplateActivityMaster.CC_AssignedQueue__c = '';
			insert ActivityTemplateActivityMaster;

			//Create and update Case
			Case newcase = CC_TestDataFactory.getCaseSkel(SRTypeMasterList[0].Id);
			insert newCase;
			newCase.CC_SRTypeId__c = SRTypeMasterList[1].Id;

			Test.startTest();
			update newcase;
			Test.stopTest();

		}
	}

}