global with sharing class E_AgentLkupController extends SkyEditor2.SkyEditorPageBaseWithSharing{
    
    public Contact record{get;set;}
    public E_AgentLkupExtender getExtender() {return (E_AgentLkupExtender)extender;}
    public searchDataTable searchDataTable {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c ipVal_agentcd{get;set;}
    public SkyEditor2__SkyEditorDummy__c ipVal_agentkana{get;set;}
    public E_AgentLkupController(ApexPages.StandardController controller){
        super(controller);

        SObjectField f;
        f = Contact.fields.E_CL3PF_AGNTNUM__c;
        f = Contact.fields.E_CL3PF_ZEATKNAM__c;
        f = Contact.fields.Name;
        f = Contact.fields.E_AccParentName__c;
        f = Contact.fields.E_Account__c;
        f = Contact.fields.E_CL3PF_VALIDFLAG__c;
        f = Contact.fields.E_IsAgent__c;

        try {
            mainRecord = null;
            mainSObjectType = Contact.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            
            ipVal_agentcd = new SkyEditor2__SkyEditorDummy__c();
            ipVal_agentkana = new SkyEditor2__SkyEditorDummy__c();
            
            queryMap.put(
                'searchDataTable',
                new SkyEditor2.Query('Contact')
                    .addFieldAsOutput('E_CL3PF_AGNTNUM__c')
                    .addFieldAsOutput('Name')
                    .addFieldAsOutput('E_AccParentName__c')
                    .addFieldAsOutput('E_Account__c')
                    .addField('Name')
                    .limitRecords(500)
                    .addListener(new SkyEditor2.QueryWhereRegister(ipVal_agentcd, 'SkyEditor2__Text__c', 'E_CL3PF_AGNTNUM__c', new SkyEditor2.TextHolder('eq'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(ipVal_agentkana, 'SkyEditor2__Text__c', 'E_CL3PF_ZEATKNAM__c', new SkyEditor2.TextHolder('co'), false, true, false))
                     .addWhere(' ( E_CL3PF_VALIDFLAG__c = \'1\' AND E_IsAgent__c = true)')
.addSort('E_CL3PF_ZEATKNAM__c',True,True)
            );
            
            searchDataTable = new searchDataTable(new List<Contact>(), new List<searchDataTableItem>(), new List<Contact>(), null);
            listItemHolders.put('searchDataTable', searchDataTable);
            
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Contact.SObjectType);
            
            p_showHeader = false;
            p_sidebar = false;
            presetSystemParams();
            extender = new E_AgentLkupExtender(this);
            initSearch();
            
            extender.init();
            
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }

    public List<SelectOption> getOperatorOptions_Contact_E_CL3PF_AGNTNUM_c() {
        return getOperatorOptions('Contact', 'E_CL3PF_AGNTNUM__c');
    }
    public List<SelectOption> getOperatorOptions_Contact_E_CL3PF_ZEATKNAM_c() {
        return getOperatorOptions('Contact', 'E_CL3PF_ZEATKNAM__c');
    }
    
    global with sharing class searchDataTableItem extends SkyEditor2.ListItem {
        public Contact record{get; private set;}
        @TestVisible
        searchDataTableItem(searchDataTable holder, Contact record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class searchDataTable extends SkyEditor2.ListItemHolder {
        public List<searchDataTableItem> items{get; private set;}
        @TestVisible
        searchDataTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<searchDataTableItem>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new searchDataTableItem(this, (Contact)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
}