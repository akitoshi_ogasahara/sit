/**
 * 
 */
@isTest
private class TestE_ShareUtil {

    /**
     * List<E_DownloadHistorry__Share> getDownloadHistoryShareL1(Id recId, Set<String> agencyCodes)
     */
	static testMethod void test_getDownloadHistoryShareL101(){
		// TestData
		CR_Agency__c agncy = createAgencyList()[0];
		agncy = [Select Id, Account__c, AgencyCode__c, ReportType__c From CR_Agency__c Where Id = :agncy.Id];
		agncy.ReportType__c = '0:aaaaaa';
		
		Account account = [Select Id, Name, E_CL1PF_ZHEADAY__c From Account Where Id = :agncy.Account__c];
		
		Group gp1 = new Group(Name = 'L1' + account.E_CL1PF_ZHEADAY__c, DeveloperName = 'L1' + account.E_CL1PF_ZHEADAY__c, Type = 'Regular');
		insert gp1;
		
		E_DownloadHistorry__c dh = TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/06', toJSON(agncy));
		insert dh;
		
		Set<String> cds = new Set<String>();
		cds.add(account.E_CL1PF_ZHEADAY__c);
		
		List<E_DownloadHistorry__Share> resultList = E_ShareUtil.getDownloadHistoryShareL1(dh.Id, cds);
		
		System.assert(resultList.size() > 0);
		
	}
	
	/**
	 * List<E_DownloadHistorry__Share> getDownloadHistoryShareL2(Id recId, Set<String> officeCodes, Set<String> branchCodes)
	 */
	static testMethod void test_getDownloadHistoryShareL201(){
		Account accParent = new Account(Name = 'AccountParent', E_CL1PF_ZHEADAY__c = 'X123X', E_CL1PF_ZBUSAGCY__c = 'X123X');
		insert accParent;
		
		Account accChild = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X123X', ParentId = accParent.id, E_CL2PF_ZBUSBR__c = 'AA');
		insert accChild;
		
		List<Group> gpList = new List<Group>();
		gpList.add(new Group(Name = 'L1X123X', DeveloperName = 'L1X123X', Type = 'Regular'));
		gpList.add(new Group(Name = 'L2X123X', DeveloperName = 'L2X123X', Type = 'Regular'));
		gpList.add(new Group(Name = 'BRAA',    DeveloperName = 'BRAA',    Type = 'Regular'));
		insert gpList;
		
		Set<Id> ids = new Set<Id>();
		ids.add(accParent.Id);
		CR_Agency__c agncy = TestCR_TestUtil.createCRAgencyList(true, ids, CR_Const.RECTYPE_DEVNAME_BIZ)[0];
		agncy = [Select Id, Account__c, AgencyCode__c, ReportType__c From CR_Agency__c Where Id = :agncy.Id];
		agncy.ReportType__c = '0:aaaaaa';
		
		E_DownloadHistorry__c dh = TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/06', toJSON(agncy));
		insert dh;
		
		Set<String> oCds = new Set<String>();
		oCds.add(accChild.E_CL2PF_ZAGCYNUM__c);
		
		Set<String> bCds = new Set<String>();
		bCds.add(accChild.E_CL2PF_ZBUSBR__c);
		
		List<E_DownloadHistorry__Share> resultList = E_ShareUtil.getDownloadHistoryShareL2(dh.Id, oCds, bCds);
		
		System.assert(resultList.size() > 0);
	}

	/**
     * List<E_DownloadHistorry__Share> getDownloadHistoryShareBR(Id recId, Set<String> agencyCodes)
     */
	static testMethod void test_getDownloadHistoryShareBR(){

		CR_Agency__c agncy = createAgencyList()[0];
		agncy = [Select Id, Account__c, AgencyCode__c, ReportType__c From CR_Agency__c Where Id = :agncy.Id];
		agncy.ReportType__c = '0:aaaaaa';
		
		Account account = [Select Id, Name, E_CL1PF_ZHEADAY__c From Account Where Id = :agncy.Account__c];
		Account childAccout = new Account(Name='AccountChild', E_CL2PF_BRANCH__c='7Z', ParentId=account.Id);
		insert childAccout;
		
		Group gp1 = new Group(Name = 'BR' + childAccout.E_CL2PF_BRANCH__c, DeveloperName = 'BR' + childAccout.E_CL2PF_BRANCH__c, Type = 'Regular');
		insert gp1;
		
		E_DownloadHistorry__c dh = TestCR_TestUtil.createDlHistory(agncy.Id, CR_Const.TYPE_BIZREPORTS, '2016/06', toJSON(agncy));
		insert dh;
		
		Set<String> cds = new Set<String>();
		cds.add(childAccout.E_CL2PF_BRANCH__c);
		
		List<E_DownloadHistorry__Share> resultList = E_ShareUtil.getDownloadHistoryShareBR(dh.Id, cds);
		
		System.assert(resultList.size() > 0);
		
	}
	
	
	/**
	 * 
	 */ 
	static testMethod void test_getZhedayGroups(){
		Account accParent1 = new Account(Name = 'AccountParent', E_CL1PF_ZHEADAY__c = 'X123X');
		insert accParent1;
		Account accParent2 = new Account(Name = 'AccountParent', E_CL1PF_ZHEADAY__c = 'X987X');
		insert accParent2;
		
		List<Account> accChildList = new List<Account>();
		Account accChild1 = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X123X', ParentId = accParent1.id);
		Account accChild2 = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X987X', ParentId = accParent2.id);
		accChildList.add(accChild1);
		accChildList.add(accChild2);
		insert accChildList;
		
		List<Group> gpList = new List<Group>();
		Group gp1 = new Group(Name = 'L1X123X', DeveloperName = 'L1X123X', Type = 'Regular');
		Group gp2 = new Group(Name = 'L2X234X', DeveloperName = 'L2X234X', Type = 'Regular');
		gpList.add(gp1);
		gpList.add(gp2);
		insert gpList;
		
		Set<String> cds = new Set<String>();
		cds.add(accParent1.E_CL1PF_ZHEADAY__c);
		System.assertEquals(1, E_ShareUtil.getZhedayGroups(cds).size());
		System.assertEquals(gp1, E_ShareUtil.getZhedayGroups(cds).get('L1' + accParent1.E_CL1PF_ZHEADAY__c));
	}
	
	static testMethod void test_getZagcynumGroups(){
		Account accParent = new Account(Name = 'AccountParent', E_CL1PF_ZHEADAY__c = 'X123X');
		insert accParent;
		
		List<Account> accChildList = new List<Account>();
		Account accChild1 = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X234X', ParentId = accParent.id);
		Account accChild2 = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X345X', ParentId = accParent.id);
		accChildList.add(accChild1);
		accChildList.add(accChild2);
		insert accChildList;
		
		List<Group> gpList = new List<Group>();
		Group gp1 = new Group(Name = 'L1X123X', DeveloperName = 'L1X123X', Type = 'Regular');
		Group gp2 = new Group(Name = 'L2X234X', DeveloperName = 'L2X234X', Type = 'Regular');
		Group gp3 = new Group(Name = 'L2X345X', DeveloperName = 'L2X345X', Type = 'Regular');
		Group gp4 = new Group(Name = 'L2X456X', DeveloperName = 'L2X456X', Type = 'Regular');
		gpList.add(gp1);
		gpList.add(gp2);
		gpList.add(gp3);
		gpList.add(gp4);
		insert gpList;
		
		Set<String> cds = new Set<String>();
		cds.add(accChild1.E_CL2PF_ZAGCYNUM__c);
		cds.add(accChild2.E_CL2PF_ZAGCYNUM__c);
		System.assertEquals(2, E_ShareUtil.getZagcynumGroups(cds).size());
		System.assertEquals(gp2, E_ShareUtil.getZagcynumGroups(cds).get('L2' + accChild1.E_CL2PF_ZAGCYNUM__c));
	}
	
	static testMethod void test_getZbusbrGroup(){
		Account accParent1 = new Account(Name = 'AccountParent', E_CL1PF_ZHEADAY__c = 'X123X', E_CL1PF_ZBUSAGCY__c = 'X123X');
		insert accParent1;
		Account accParent2 = new Account(Name = 'AccountParent', E_CL1PF_ZHEADAY__c = 'X987X', E_CL1PF_ZBUSAGCY__c = 'X987X');
		insert accParent2;
		
		List<Account> accChildList = new List<Account>();
		Account accChild1 = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X123X', ParentId = accParent1.id, E_CL2PF_ZBUSBR__c = 'AA');
		Account accChild2 = new Account(Name = 'AccountChild', E_CL2PF_ZAGCYNUM__c = 'X987X', ParentId = accParent2.id, E_CL2PF_ZBUSBR__c = 'BB');
		accChildList.add(accChild1);
		accChildList.add(accChild2);
		insert accChildList;
		
		List<Group> gpList = new List<Group>();
		Group gp1 = new Group(Name = 'L1X123X', DeveloperName = 'L1X123X', Type = 'Regular');
		Group gp2 = new Group(Name = 'L2X234X', DeveloperName = 'L2X234X', Type = 'Regular');
		Group gp3 = new Group(Name = 'L2X345X', DeveloperName = 'L2X987X', Type = 'Regular');
		Group gp4 = new Group(Name = 'BRAA',    DeveloperName = 'BRAA',    Type = 'Regular');
		Group gp5 = new Group(Name = 'BRBB',    DeveloperName = 'BRBB',    Type = 'Regular');
		gpList.add(gp1);
		gpList.add(gp2);
		gpList.add(gp3);
		gpList.add(gp4);
		gpList.add(gp5);
		insert gpList;
		
		Set<Id> ids = new Set<Id>();
		ids.add(accParent1.id);
		ids.add(accParent2.id);
		System.assertEquals(2, E_ShareUtil.getZbusbrGroup(ids).size());
	}

	/*
	 * 代理店格顧客コードと顧客番号が一致する募集人の法人個人区分を取得するテスト
	 */
	 static testMethod void test_CorporateIndividualClass(){

	 	//拠点長
		User director = TestI_TestUtil.createUser(false,'testdirector','拠点長');
		director.MRCD__c = 'MR00002';
		insert director;

	 	//MR作成
		User mr = TestI_TestUtil.createUser(false,'testmr','ＭＲ');
		mr.MRCD__c = 'MR00001';
		mr.Manager = director;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'testacc', E_CL1PF_ZHEADAY__c = '12345', E_CL2PF_ZAGCYNUM__c = '54321', E_CL1PF_CLNTNUM__c = '123456', E_COMM_VALIDFLAG__c = '1');
		insert acc;

		//事務所作成
		Account office1 = new Account(Name = 'testoffice1', E_CL1PF_ZHEADAY__c = '23456', E_CL2PF_ZAGCYNUM__c = '65432', E_COMM_VALIDFLAG__c = '1', ZMRCODE__c = 'MR00001',KSECTION__c = '88', ParentId = acc.Id);
		insert office1;

		Account office2 = new Account(Name = 'testoffice2', E_CL1PF_ZHEADAY__c = '34567', E_CL2PF_ZAGCYNUM__c = '76543', E_COMM_VALIDFLAG__c = '1', ZMRCODE__c = 'MR00001', ParentId = acc.Id);
		insert office2;

		//募集人作成
		Contact customer1 = TestI_TestUtil.createContact(false,office1.Id,'TestCustomer');
		customer1.E_CLTPF_CLNTNUM__c = '123456';
		customer1.E_CLTPF_CLTTYPE__c = 'P';
		insert customer1;

		Contact customer2 = TestI_TestUtil.createContact(false,office2.Id,'TestCustomer');
		customer2.E_CLTPF_CLNTNUM__c = '654321';
		customer2.E_CLTPF_CLTTYPE__c = 'C';
		insert customer2;

		//自己点検事務所作成
		SC_Office__c scOffice1 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0001', Account__c = office1.Id, ownerId = mr.Id, MRManager__c = director.Id, OfficeType__c = '法人');
		insert scOffice1;

		SC_Office__c scOffice2 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0002', Account__c = office2.Id, ownerId = mr.Id);
		insert scOffice2;

	//=====================================テスト開始=====================================
		Test.startTest();

		//Ebiz連携ログ作成
		E_BizDataSyncLog__c wbiz = new E_BizDataSyncLog__c();
		wbiz.Kind__c = '7';
		insert wbiz;

		Test.stopTest();
	//======================================テスト終了=====================================

		SC_Office__c queryScOffice1 = [SELECT Id, OfficeType__c FROM SC_Office__c WHERE Id = :scOffice1.Id LIMIT 1];
		SC_Office__c queryScOffice2 = [SELECT Id, OfficeType__c FROM SC_Office__c WHERE Id = :scOffice2.Id LIMIT 1];
	//======================================テスト判定=====================================
		system.assertEquals(queryScOffice1.OfficeType__c,E_Const.SCOFFICE_CLTTYPEMAP.get(customer1.E_CLTPF_CLTTYPE__c));
		system.assertEquals(queryScOffice2.OfficeType__c,E_Const.SCOFFICE_CLTTYPEMAP.get(customer1.E_CLTPF_CLTTYPE__c));

	}

	/**
	 * 自主点検事務所を事務所に紐づく、AT権限かつ管理責任者の有効な募集人に共有を作成
	 */ 
	static testMethod void test_ShareManagementAgent(){

		//拠点長
		User director = TestI_TestUtil.createUser(false,'testdirector','拠点長');
		director.MRCD__c = 'MR00002';
		insert director;

	 	//MR作成
		User mr = TestI_TestUtil.createUser(false,'testmr','ＭＲ');
		mr.MRCD__c = 'MR00001';
		mr.Manager = director;
		insert mr;

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		//代理店格
		Account acc = new Account(Name='acc1', E_CL1PF_ZHEADAY__c='12345', E_CL2PF_ZAGCYNUM__c='54321', E_CL1PF_CLNTNUM__c='123456', E_COMM_VALIDFLAG__c='1');
		insert acc;

		//事務所
		Account office1 = new Account(Name='off1', E_CL1PF_ZHEADAY__c='34567', E_CL2PF_ZAGCYNUM__c='76543', E_COMM_VALIDFLAG__c='1', ParentId = acc.Id, ZMRCODE__c='MR00001');
		insert office1;

		//募集人（管理責任者かつAT権限）
		List<Contact> cons = new List<Contact>();

		Contact ag1 = TestI_TestUtil.createContact(false,office1.Id,'ag1');
		ag1.E_CL3PF_ZAGMANGR__c = 'Y';
		cons.add(ag1);

		//募集人(管理責任者でないかつAT権限)
		Contact ag2 = TestI_TestUtil.createContact(false,office1.Id,'ag2');
		ag2.E_CL3PF_ZAGMANGR__c = '';
		cons.add(ag2);

		//募集人（管理責任者でないかつAT権限でない）
		Contact ag3 = TestI_TestUtil.createContact(false,office1.Id,'ag3');
		ag3.E_CL3PF_ZAGMANGR__c = '';
		cons.add(ag3);

		////募集人（管理責任者かつAT権限かつユーザが無効）
		Contact ag4 = TestI_TestUtil.createContact(false,office1.Id,'ag4');
		ag4.E_CL3PF_ZAGMANGR__c = 'Y';
		cons.add(ag4);

		insert cons;

		//ユーザ作成
		List<User> agUsers = new List<User>();
		User ag1user =  TestI_TestUtil.createAgentUser(false,'ag1User','E_PartnerCommunity_IA',ag1.Id);
		ag1User.IsActive = true;
		agUsers.add(ag1User);

		User ag2user =  TestI_TestUtil.createAgentUser(false,'ag2User','E_PartnerCommunity_IA',ag2.Id);
		ag2User.IsActive = true;
		agUsers.add(ag2User);

		User ag3user =  TestI_TestUtil.createAgentUser(false,'ag3User','E_PartnerCommunity_IA',ag3.Id);
		ag3user.IsActive = true;
		agUsers.add(ag3user);

		User ag4user =  TestI_TestUtil.createAgentUser(false,'ag4User','E_PartnerCommunity_IA',ag4.Id);
		ag4User.IsActive =true;
		agUsers.add(ag4User);

		insert agUsers;

		//ID管理作成
		List<E_IDCPF__c> idcpfs = new List<E_IDCPF__c>();
		E_IDCPF__c ag1Idcpf = TestI_TestUtil.createIDCPF(false,ag1user.Id,'AT');
		ag1Idcpf.Contact__c = ag1.Id;
		idcpfs.add(ag1Idcpf);

		E_IDCPF__c ag2Idcpf = TestI_TestUtil.createIDCPF(false,ag2user.Id,'AT');
		ag2Idcpf.Contact__c = ag2.Id;
		idcpfs.add(ag2Idcpf);

		E_IDCPF__c ag3Idcpf = TestI_TestUtil.createIDCPF(false,ag3user.Id,'');
		ag3Idcpf.Contact__c = ag3.Id;
		idcpfs.add(ag3Idcpf);

		E_IDCPF__c ag4Idcpf = TestI_TestUtil.createIDCPF(false,ag4user.Id,'AT');
		ag4Idcpf.Contact__c = ag4.Id;
		idcpfs.add(ag4Idcpf);

		insert idcpfs;


		//自己点検事務所作成
		SC_Office__c scOffice1 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0001', Account__c = office1.Id, MRManager__c = director.Id, OfficeType__c = '法人', OwnerId = actUser.Id);
		insert scOffice1;

		System.runAs(actUser){
			Test.startTest();
		//=====================================テスト開始=====================================
			User us = [SELECT Id FROM User WHERE Id=:ag4user.Id];
			us.IsActive = false;
			update us;

			//Ebiz連携ログ作成
			E_BizDataSyncLog__c wbiz = new E_BizDataSyncLog__c();
			wbiz.Kind__c = '7';
			insert wbiz;

		//=====================================テスト終了=====================================
			Test.stopTest();
		List<SC_Office__share> shareList = [SELECT iD FROM SC_Office__share];
		//=====================================テスト判定=====================================
		System.assertEquals(shareList.size(),2);
		}

	}
	/*
	 * L1グループに自己点検レコードを共有
	 */
	static testMethod void test_ShareL1Group(){
		List<User> users = new List<User>();
		//MR
		User mr1 = TestI_TestUtil.createUser(false,'testmr1','ＭＲ');
		mr1.MRCD__c = 'MR00001';
		users.add(mr1);

		User mr2 = TestI_TestUtil.createUser(false,'testmr2','ＭＲ');
		mr2.MRCD__c = 'MR00002';
		users.add(mr2);

		//拠点長
		User director = TestI_TestUtil.createUser(false,'testdirector','拠点長');
		director.MRCD__c = 'MR00003';
		users.add(director);

		insert users;

		// 実行ユーザ
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');

		//ID管理
		List<E_IDCPF__c> idcpfs = new List<E_IDCPF__c>();

		E_IDCPF__c mr1Idc = new E_IDCPF__c(User__c = mr1.id,OwnerId = mr1.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr1Idc);

		E_IDCPF__c mr2Idc = new E_IDCPF__c(User__c = mr2.id,OwnerId = mr2.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr2Idc);
		
		E_IDCPF__c actIdc = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		idcpfs.add(actIdc);

		insert idcpfs;

		//代理店格
		Account acc = new Account(Name='acc1', E_CL1PF_ZHEADAY__c='12345', E_CL2PF_ZAGCYNUM__c='54321', E_CL1PF_CLNTNUM__c='123456', E_COMM_VALIDFLAG__c='1');
		insert acc;

		//事務所
		Account office1 = new Account(Name='off1', E_CL2PF_ZAGCYNUM__c='76543', E_COMM_VALIDFLAG__c='1', ParentId = acc.Id, ZMRCODE__c='MR00001');
		insert office1;

		//公開グループ作成
		List<Group> groups = new List<Group>();
		Group g1 = new Group(Name='L1'+acc.E_CL1PF_ZHEADAY__c, Type='Regular', DeveloperName='L1'+acc.E_CL1PF_ZHEADAY__c);
		groups.add(g1);

		Group g2 = new Group(Name='L0'+acc.E_CL1PF_ZHEADAY__c, Type='Regular', DeveloperName='L0'+acc.E_CL1PF_ZHEADAY__c);
		groups.add(g2);

		insert groups;

		//自己点検事務所作成
		SC_Office__c scOffice1 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0001', Account__c = office1.Id, MRManager__c = director.Id, OwnerId = actUser.Id);
		insert scOffice1;

		system.runAs(actUser){
		//=====================================テスト開始=====================================	
			Test.startTest();

			//公開グループメンバー作成
			List<GroupMember> gMembers = new List<GroupMember>();
			gMembers.add(new GroupMember(GroupId = g1.Id, UserOrGroupId = mr1.Id));
			gMembers.add(new GroupMember(GroupId = g2.Id, UserOrGroupId = mr2.Id));
			insert gMembers;

			//Ebiz連携ログ作成
			E_BizDataSyncLog__c wbiz = new E_BizDataSyncLog__c();
			wbiz.Kind__c = '7';
			insert wbiz;

		//=====================================テスト終了=====================================
			Test.stopTest();
		//=====================================テスト判定=====================================
			List<SC_Office__share> shareList = [SELECT Id, ParentId,UserOrGroupId  FROM SC_Office__share WHERE UserOrGroupId=:mr1.Id];
			system.assertEquals(shareList.size(),1);
		}
	}

	/*
	 * L2公開グループに自己点検レコードを共有
	 */
	static testMethod void test_ShareL2Group(){
		List<User> users = new List<User>();
		//MR
		User mr1 = TestI_TestUtil.createUser(false,'testmr1','ＭＲ');
		mr1.MRCD__c = 'MR00001';
		users.add(mr1);

		User mr2 = TestI_TestUtil.createUser(false,'testmr2','ＭＲ');
		mr2.MRCD__c = 'MR00002';
		users.add(mr2);

		//拠点長
		User director = TestI_TestUtil.createUser(false,'testdirector','拠点長');
		director.MRCD__c = 'MR00003';
		users.add(director);

		insert users;

		// 実行ユーザ
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');

		//ID管理
		List<E_IDCPF__c> idcpfs = new List<E_IDCPF__c>();

		E_IDCPF__c mr1Idc = new E_IDCPF__c(User__c = mr1.id,OwnerId = mr1.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr1Idc);

		E_IDCPF__c mr2Idc = new E_IDCPF__c(User__c = mr2.id,OwnerId = mr2.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr2Idc);
		
		E_IDCPF__c actIdc = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		idcpfs.add(actIdc);

		insert idcpfs;

		//代理店格
		Account acc = new Account(Name='acc1', E_CL1PF_ZHEADAY__c='12345', E_CL2PF_ZAGCYNUM__c='54321', E_CL1PF_CLNTNUM__c='123456', E_COMM_VALIDFLAG__c='1');
		insert acc;

		//事務所
		Account office1 = new Account(Name='off1', E_CL2PF_ZAGCYNUM__c='76543', E_COMM_VALIDFLAG__c='1', ParentId = acc.Id, ZMRCODE__c='MR00002');
		insert office1;

		//公開グループ作成
		List<Group> groups = new List<Group>();
		Group g1 = new Group(Name='L1'+office1.E_CL2PF_ZAGCYNUM__c, Type='Regular', DeveloperName='L1'+office1.E_CL2PF_ZAGCYNUM__c);
		groups.add(g1);

		Group g2 = new Group(Name='L2'+office1.E_CL2PF_ZAGCYNUM__c, Type='Regular', DeveloperName='L2'+office1.E_CL2PF_ZAGCYNUM__c);
		groups.add(g2);

		insert groups;

		//自己点検事務所作成
		SC_Office__c scOffice1 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0001', Account__c = office1.Id, MRManager__c = director.Id, OwnerId = actUser.Id);
		insert scOffice1;

		system.runAs(actUser){
		//=====================================テスト開始=====================================	
			Test.startTest();

			//公開グループメンバー作成
			List<GroupMember> gMembers = new List<GroupMember>();
			gMembers.add(new GroupMember(GroupId = g1.Id, UserOrGroupId = mr1.Id));
			gMembers.add(new GroupMember(GroupId = g2.Id, UserOrGroupId = mr2.Id));
			insert gMembers;

			//Ebiz連携ログ作成
			E_BizDataSyncLog__c wbiz = new E_BizDataSyncLog__c();
			wbiz.Kind__c = '7';
			insert wbiz;

		//=====================================テスト終了=====================================
			Test.stopTest();
		//=====================================テスト判定=====================================
			List<SC_Office__share> shareList = [SELECT Id, UserOrGroupId FROM SC_Office__share WHERE UserOrGroupId=:mr2.Id];
			system.assertEquals(shareList.size(),1);
		}
	}

	/*
	 * BR公開グループに自己点検レコードを共有
	 */
	static testMethod void test_ShareBRGroup(){
		List<User> users = new List<User>();
		//MR
		User mr1 = TestI_TestUtil.createUser(false,'testmr1','ＭＲ');
		mr1.MRCD__c = 'MR00001';
		users.add(mr1);

		User mr2 = TestI_TestUtil.createUser(false,'testmr2','ＭＲ');
		mr2.MRCD__c = 'MR00002';
		users.add(mr2);

		//拠点長
		User director = TestI_TestUtil.createUser(false,'testdirector','拠点長');
		director.MRCD__c = 'MR00003';
		users.add(director);

		insert users;

		// 実行ユーザ
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');

		//ID管理
		List<E_IDCPF__c> idcpfs = new List<E_IDCPF__c>();

		E_IDCPF__c mr1Idc = new E_IDCPF__c(User__c = mr1.id,OwnerId = mr1.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr1Idc);

		E_IDCPF__c mr2Idc = new E_IDCPF__c(User__c = mr2.id,OwnerId = mr2.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr2Idc);
		
		E_IDCPF__c actIdc = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		idcpfs.add(actIdc);

		insert idcpfs;

		//代理店格
		Account acc = new Account(Name='acc1', E_CL1PF_ZHEADAY__c='12345', E_CL2PF_ZAGCYNUM__c='54321', E_CL1PF_CLNTNUM__c='123456',E_CL2PF_BRANCH__c = '8Z', E_COMM_VALIDFLAG__c='1');
		insert acc;

		//事務所
		Account office1 = new Account(Name='off1', E_CL2PF_ZAGCYNUM__c='76543', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='9Z' ,ParentId = acc.Id, ZMRCODE__c='MR00001');
		insert office1;

		//公開グループ作成
		List<Group> groups = new List<Group>();
		Group g1 = new Group(Name='BR'+office1.E_CL2PF_BRANCH__c, Type='Regular', DeveloperName='BR'+office1.E_CL2PF_BRANCH__c);
		groups.add(g1);

		Group g2 = new Group(Name='BR'+acc.E_CL2PF_BRANCH__c, Type='Regular', DeveloperName='BR'+acc.E_CL2PF_BRANCH__c);
		groups.add(g2);

		insert groups;

		//自己点検事務所作成
		SC_Office__c scOffice1 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0001', Account__c = office1.Id, MRManager__c = director.Id, OwnerId = actUser.Id);
		insert scOffice1;

		system.runAs(actUser){
		//=====================================テスト開始=====================================	
			Test.startTest();

			//公開グループメンバー作成
			List<GroupMember> gMembers = new List<GroupMember>();
			gMembers.add(new GroupMember(GroupId = g1.Id, UserOrGroupId = mr1.Id));
			gMembers.add(new GroupMember(GroupId = g2.Id, UserOrGroupId = mr2.Id));
			insert gMembers;

			//Ebiz連携ログ作成
			E_BizDataSyncLog__c wbiz = new E_BizDataSyncLog__c();
			wbiz.Kind__c = '7';
			insert wbiz;

		//=====================================テスト終了=====================================
			Test.stopTest();
		//=====================================テスト判定=====================================
			List<SC_Office__share> shareList = [SELECT Id FROM SC_Office__share WHERE UserOrGroupId=:mr1.Id];
			system.assertEquals(shareList.size(),1);
		}
	}
	/*
	 * 共有の理由が『NNLinkSharingRule』であるレコードを削除
	 */
	static testMethod void test_DeleteShare(){
		List<User> users = new List<User>();
		//MR
		User mr1 = TestI_TestUtil.createUser(false,'testmr1','ＭＲ');
		mr1.MRCD__c = 'MR00001';
		users.add(mr1);

		User mr2 = TestI_TestUtil.createUser(false,'testmr2','ＭＲ');
		mr2.MRCD__c = 'MR00002';
		users.add(mr2);

		//拠点長
		User director = TestI_TestUtil.createUser(false,'testdirector','拠点長');
		director.MRCD__c = 'MR00003';
		users.add(director);

		insert users;

		// 実行ユーザ
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');

		//ID管理
		List<E_IDCPF__c> idcpfs = new List<E_IDCPF__c>();

		E_IDCPF__c mr1Idc = new E_IDCPF__c(User__c = mr1.id,OwnerId = mr1.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr1Idc);

		E_IDCPF__c mr2Idc = new E_IDCPF__c(User__c = mr2.id,OwnerId = mr2.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'BR2A',AppMode__c = 'IRIS');
		idcpfs.add(mr2Idc);
		
		E_IDCPF__c actIdc = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		idcpfs.add(actIdc);

		insert idcpfs;

		//代理店格
		Account acc = new Account(Name='acc1', E_CL1PF_ZHEADAY__c='12345', E_CL2PF_ZAGCYNUM__c='54321', E_CL1PF_CLNTNUM__c='123456',E_CL2PF_BRANCH__c = '8Z', E_COMM_VALIDFLAG__c='1');
		insert acc;

		//事務所
		Account office1 = new Account(Name='off1', E_CL2PF_ZAGCYNUM__c='76543', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='9Z' ,ParentId = acc.Id, ZMRCODE__c='MR00001');
		insert office1;

		//公開グループ作成
		List<Group> groups = new List<Group>();
		Group g1 = new Group(Name='BR'+office1.E_CL2PF_BRANCH__c, Type='Regular', DeveloperName='BR'+office1.E_CL2PF_BRANCH__c);
		groups.add(g1);

		Group g2 = new Group(Name='BR'+acc.E_CL2PF_BRANCH__c, Type='Regular', DeveloperName='BR'+acc.E_CL2PF_BRANCH__c);
		groups.add(g2);

		insert groups;

		//自己点検事務所作成
		SC_Office__c scOffice1 = new SC_Office__c(Status__c = '未提出', UpsertKey__c = 'sc0001', Account__c = office1.Id, MRManager__c = director.Id, OwnerId = actUser.Id);
		insert scOffice1;

		//SObjectリストを作成
		Set<SObject> recs = new Set<SObject>();
		recs.add(scOffice1);

		system.runAs(actUser){
		//=====================================テスト開始=====================================	
			Test.startTest();

			//公開グループメンバー作成
			List<GroupMember> gMembers = new List<GroupMember>();
			gMembers.add(new GroupMember(GroupId = g1.Id, UserOrGroupId = mr1.Id));
			gMembers.add(new GroupMember(GroupId = g2.Id, UserOrGroupId = mr2.Id));
			insert gMembers;

			//Ebiz連携ログ作成
			E_BizDataSyncLog__c wbiz = new E_BizDataSyncLog__c();
			wbiz.Kind__c = '7';
			insert wbiz;

		//=====================================テスト終了=====================================
			Test.stopTest();
		//=====================================テスト判定=====================================
		}
		system.runAs(actUser){
			E_ShareUtil.deleteNNLinkShares(recs);
			List<SC_Office__share> shareList = [SELECT Id, ParentId, ROWCause FROM SC_Office__share WHERE UserOrGroupId=:mr1.Id];
			system.assertEquals(shareList.size(),1);
		}
	}



    /* ****************************************************************************** */
    private static String toJSON(CR_Agency__c agency){
    	E_DownloadCondition.BizReportsDownloadCondi dlCondi = new E_DownloadCondition.BizReportsDownloadCondi();
		dlCondi.ZHEADAY = agency.AgencyCode__c;
		dlCondi.reportType = agency.ReportType__c.substring(0,1);
		return dlCondi.toJSON();
    }
    
    private static List<CR_Agency__c> createAgencyList(){
		List<Account> accountList = TestCR_TestUtil.createAccountList(true, 1);
		Set<Id> ids = new Set<Id>();
		for(Account acc : accountList){
			ids.add(acc.Id);
		}
		return TestCR_TestUtil.createCRAgencyList(true, ids, CR_Const.RECTYPE_DEVNAME_BIZ);
    }
}