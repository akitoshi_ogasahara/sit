@isTest
private class MNT_DocumentSearchExtenderTest{
	@isTest static void initTest(){
		//テストデータ作成
		I_ContentMaster__c cms = createData();

		//テスト実行
		Test.startTest();
		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(cms);
		MNT_DocumentSearch controller = new MNT_DocumentSearch(standardcontroller);
		MNT_DocumentSearchExtender extender = controller.getExtender();
		extender.init();
		Test.stopTest();

		//テスト検証
		System.assertEquals(cms.Id, controller.ResultList.items[0].record.Id);
		System.assertEquals(3, controller.ResultList.items.size());
	}

	private static I_ContentMaster__c createData(){
		I_ContentMaster__c cms01 = new I_ContentMaster__c(Name = 'パンフレット', DocumentCategory__c = 'パンフレット／特に重要なお知らせ');
		insert cms01;

		I_ContentMaster__c cms02 = new I_ContentMaster__c(Name = 'しおり', DocumentCategory__c = 'ご契約のしおり・約款');
		insert cms02;

		I_ContentMaster__c cms03 = new I_ContentMaster__c(Name = '会社案内', DocumentCategory__c = '会社案内');
		insert cms03;

		//資料カテゴリ検索対象外のデータ作成
		I_ContentMaster__c cms04 = new I_ContentMaster__c(Name = 'テスト');
		insert cms04;

		return cms01;
	}
}