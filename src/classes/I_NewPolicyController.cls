public with sharing class I_NewPolicyController extends I_AbstractController {

	//表示用新契約一覧
	public List<I_NewPolicyDTO.NewPolicy> newPolicys {get;private set;}
	// リスト行数
	public Integer rowCount {get; set;}
	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}
	// ソートキー
	public String sortType {get;private set;}
	// ソート順
	public Boolean sortIsAsc {get;private set;}
	// 表示制御フラグ
	public Boolean isViewMr {get; set;}
//  public Boolean isViewDistribute {get; set;}
	public Boolean isViewOffice {get; set;}
	public Boolean isViewAgent {get; set;}
	public Boolean isViewLimit {get; set;}

	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}

	//Contractor
	public I_NewPolicyController() {
		//初期化
		super();
		pageMessages = getPageMessages();
		newPolicys = new List<I_NewPolicyDTO.NewPolicy>();
		sortIsAsc = false;
		isViewLimit = false;
		//デフォルトのソートはステータス配信日
		sortType = 'statusDate';
		setViewColumns();
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	//初期処理
	public override PageReference init() {
		PageReference pr = super.init();
		pageAccessLog.Name = '④手続き状況を確認する_一覧画面';

		createNewPolicys();
		//初期ソート処理
		I_NewPolicyDTO.sortType = sortType;
		I_NewPolicyDTO.sortIsAsc = sortIsAsc;
		newPolicys.sort();
		
		return pr;
	}
	/**
	* 総行数取得
	*/
	public Integer getTotalRows(){
		return newPolicys == null ? 0 : newPolicys.size();
	}
	/**
	 * メニューキー設定
	 */
	protected override String getLinkMenuKey(){
		return 'newpolicy';
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	* ソート
	*/
	public void sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		if(sortType != sType){
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		I_NewPolicyDTO.sortType = sortType;
		I_NewPolicyDTO.sortIsAsc = sortIsAsc;
		newPolicys.sort();
	}

	/**
	 * データ作成
	 */
	private void createNewPolicys (){
		//団体は別処理のため、一時的にMapに詰める 団体key => レコードのリスト
		Map<String,List<E_NewPolicy__c>> groupMap = new Map<String,List<E_NewPolicy__c>>();
		I_NewPolicyDTO.NewPolicy newPlcy;
		Integer listSize = 1;
		List<E_NewPolicy__c> newPlcys = E_NewPolicyDao.getNewPolicy (createNewPolicyCondition());
		//取得件数
		if(newPlcys.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('NPL|002'));
			return;
		}

		for(E_NewPolicy__c rec :newPlcys){
			//団体タイプがYのものは団体扱い
			if(rec.KFRAMEIND__c == 'Y') {
				//既にコード値があれば追加、なければput
				if(groupMap.containsKey(rec.GroupKey__c)) {
					groupMap.get(rec.GroupKey__c).add(rec);
				} else {
					//表示上限を超えたら追加なし
					if(listSize > getListMaxRows()){
						isViewLimit = true;
						continue;
					}
					groupMap.put(rec.GroupKey__c,new List<E_NewPolicy__c>{rec});
					listSize += 1;
				}
			} else {
				//表示対象外は処理を行わない
				if(rec.STATCODE__c <> 'P' 
						&& (rec.statusDate__c == null 
							|| !isDispRecord(rec.statusDate__c.date()))){
					continue;
				}
				//表示上限を超えたら追加なし
				if(listSize > getListMaxRows()){
					isViewLimit = true;
					continue;
				}
				newPlcy = new I_NewPolicyDTO.NewPolicy(rec);
				//ステータス判定
				newPlcy.styleClass = I_NewPolicyConst.statusCssMap.get(newPlcy.status);
				newPlcy.CHDRNUM = rec.CHDRNUM__c;
				newPlcy.insttot01 = rec.INSTTOT01__c;
				//newPlcy.insured = rec.COMM_ZINSNAM__c;
				//newPlcy.insuredKana = rec.InsuredE_CLTPF_ZCLKNAME__c;
				//外字対応
				newPlcy.insured = rec.InsuredName__c;
				//newPlcy.insuredKana = rec.InsuredNameKana__c;
				newPlcy.insuredKana = rec.InsuredE_CLTPF_ZCLKNAME__c;
				newPolicys.add(newPlcy);
				listSize += 1;
			}
		} 
		//グループ設定
		for(List<E_NewPolicy__c> recs :groupMap.values()){
			Integer insuredNum = 0;
			Decimal sumInsttot01 = 0;
			Boolean isDisp = false;
			String tempCHDRNUM = null;
			Set<String> statusSet = new Set<String>();
			for(E_NewPolicy__c rec: recs) {
				//親フラグが立っているものからセット
				if(rec.GroupParentFlag__c){
					newPlcy = new I_NewPolicyDTO.NewPolicy(rec);
					newPlcy.statusDate = rec.GroupStatusDate__c;
				}
				if(rec.STATCODE__c == 'P'){
					isDisp = true;
				}else{
					if(rec.statusDate__c <> null 
							&& isDispRecord(rec.statusDate__c.date())){
						isDisp = true;
					}
				}
				tempCHDRNUM = (I_Util.compareToString(tempCHDRNUM,rec.CHDRNUM__c) > 0) ? rec.CHDRNUM__c:tempCHDRNUM;
				if(rec.INSTTOT01__c <> null) sumInsttot01 += rec.INSTTOT01__c;
				insuredNum += 1;
				statusSet.add(rec.status__c);
			}
			//一件も表示対象でなければ処理を行わない
			if(!isDisp){
				continue;
			}
			
			newPlcy.status = getStatus(statusSet);
			newPlcy.styleClass = I_NewPolicyConst.statusCssMap.get(newPlcy.status);
			newPlcy.insttot01 = sumInsttot01;
			newPlcy.CHDRNUM = tempCHDRNUM;
			newPlcy.insured = String.valueOf(insuredNum) + '名';
			newPlcy.insuredKana = '';
			newPolicys.add(newPlcy);
		}
		if(newPolicys.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('NPL|002'));
			return;
		}
		if(isViewLimit){
			String errmessage = String.format(getMSG().get('NPL|001'),new String[]{Decimal.valueOf(getListMaxRows()).format()});
			pageMessages.addWarningMessage(errmessage);
		}
	}



	/**
	 * 表示対象か確認
	 * @param sDate ステータス発信日
	 * @return Boolean 表示対象ならtrue
	 */
	private Boolean isDispRecord(Date sDate) {
		return sDate >= getDispStartDate();
	}

	/**
	 * 表示開始日付
	 */
	private Date getDispStartDate(){
		Date ago = Date.today().addMonths(-1);
		return Date.newInstance(ago.year(), ago.month(), 1);
	}

	/**
	 * 表示制御フラグ設定
	 */
	private void setViewColumns(){
		isViewMr = false;
//		isViewDistribute = false;
		isViewOffice = false;
		isViewAgent = false;
		
		// 代理店ユーザ　AH権限
		if(access.isAgent() && access.isAuthAH()){
			isViewOffice = true;
			isViewAgent = true;

		// 代理店ユーザ　AY権限
		}else if(access.isAgent() && access.isAuthAY()){
			isViewAgent = true;

		// 代理店ユーザ　AT権限　表示なし
		}else if(access.isAgent() && access.isAuthAT()){

		// その他
		}else{
			isViewMr = true;
//			isViewDistribute = true;
			isViewOffice = true;
			isViewAgent = true;
		}
	}

	/*
	 * 自動検索[フィルターモード]
	 */
	private String createNewPolicyCondition(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result = ' Where ParentAccount__c = \'' + conditionValue + '\''
						   + 'OR SubParentAccount__c = \'' + conditionValue + '\' ';
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result = ' Where Account__c = \'' + conditionValue + '\' '
						   + 'OR SubAccount__c = \'' + conditionValue + '\' ';
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					//修正　山田
					String agentId = E_Util.getMainAccountByAgentId(conditionValue);
					result = ' Where Agent__c = \'' + agentId + '\' '
						   		+ 'OR SubAgent__c = \'' + agentId + '\' ';

				// 挙積ph2対応のためコメントアウト
				// 営業部フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_UNIT){
					result = ' Where Account__r.E_CL2PF_ZBUSBR__c = \'' + conditionValue + '\' '
						   + 'OR Account__r.E_CL2PF_BRANCH__c = \'' + conditionValue + '\' '
						   + 'OR SubAccount__r.E_CL2PF_ZBUSBR__c = \'' + conditionValue + '\' '
						   + 'OR SubAccount__r.E_CL2PF_BRANCH__c = \'' + conditionValue + '\' ';
				// MRフィルタ
				}else if(conditionField == I_Const.CONDITION_FIELD_MR){
					result = ' WHERE Account__r.OwnerId = \'' + conditionValue + '\' '
						   + 'OR SubAccount__r.OwnerId =  \'' + conditionValue + '\' ';
				
				}
			}
		}
		return result;
	}
	/**
	 * データ日時取得
	 */
	public String getSyncDate () {
		E_BizDataSyncLog__c biz = [SELECT DataSyncEndDate__c FROM E_BizDataSyncLog__c WHERE Kind__c = 'E' ORDER BY CreatedDate desc LIMIT 1];
		if(biz.DataSyncEndDate__c == null){
			return 'データ更新中';
		}
		return biz.DataSyncEndDate__c.format('yyyy/MM/dd HH:mm') + '　更新';
	}
/*------------------------------- ToDo Util化 --------------------------------*/
	/**
	 * ステータスコード判定
	 * 優先度： 成立 >> 査定完了入金待ち >> 不備対応中 >> 査定中 >> 申込書類到着済 >> 謝絶/取下げ >> 謝絶 = 取下げ
	 */
	public static String getStatus (Set<String> statusSet){
		String status;
		//成立
		if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_I)){
			status = I_NewPolicyConst.STATUS_NAME_I;
		//査定完了入金待ち
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_P3)){
			status = I_NewPolicyConst.STATUS_NAME_P3;
		//不備対応中
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_P2)){
			status = I_NewPolicyConst.STATUS_NAME_P2;
		//査定中　申込書類到着済を含む かつ 謝絶 または 取下げを含む場合も査定中とする
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_P4)
				|| (statusSet.contains(I_NewPolicyConst.STATUS_NAME_P1)
					&& statusSet.size() <> 1)){
			status = I_NewPolicyConst.STATUS_NAME_P4;
		//申込書類到着済
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_P1)){
			status = I_NewPolicyConst.STATUS_NAME_P1;
		//謝絶/取下げ
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_Q) 
				&& statusSet.contains(I_NewPolicyConst.STATUS_NAME_Z)){
			status = I_NewPolicyConst.STATUS_NAME_QZ;
		//謝絶
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_Q)){
			status = I_NewPolicyConst.STATUS_NAME_Q;
		//取下げ
		}else if(statusSet.contains(I_NewPolicyConst.STATUS_NAME_Z)){
			status = I_NewPolicyConst.STATUS_NAME_Z;
		}
		return status;
	}
}