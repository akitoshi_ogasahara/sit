global with sharing virtual class E_AgentLkupExtender extends E_AbstractSearchExtender {

    E_AgentLkupController extension;
    //ページタイトル
    private static final String PAGE_TITLE = '募集人選択';

    /* コンストラクタ */
    public E_AgentLkupExtender(E_AgentLkupController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        extension.execInitialSearch = false;        //初期検索を実施しない
        //社員ユーザ拠点権限の場合には、EIDCのZINQUIRRの3桁目から2桁は**以外の場合、支社権限と意味して、ECL３のZBUSBRまたはBRANCHと一致するレコードのみ検索可能とする。
        if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL)) {
            SkyEditor2.Query tableQuery1 = extension.queryMap.get('searchDataTable');
            tableQuery1.addWhereIfNotFirst('AND');
            tableQuery1.addWhere(' ( E_CL3PF_ZBUSBR__c = \''+ access.getAccessKindFromThree() + '\' OR E_CL3PF_BRANCH__c = \''+ access.getAccessKindFromThree() + '\')');
            extension.queryMap.put('searchDataTable',tableQuery1);
        }
    }

    /**
     *  検索前処理
     */
    global override void preSearch() {
        if(extension != null){
            String ipValAgentcd = extension.ipVal_agentcd.SkyEditor2__Text__C;
            String ipValAgentkana = extension.ipVal_agentkana.SkyEditor2__Text__C;
            //コードまたはカナは必須とする 
            if(String.isBlank(ipValAgentcd) && String.isBlank(ipValAgentkana)){
                throw new SkyEditor2.ExtenderException(getMSG().get('ERR|008'));
            }
            
            if (E_Util.isNotNull(ipValAgentcd) && (!E_Util.isEnAlphaNum(ipValAgentcd) || ipValAgentcd.length() != 5)) {
                throw new SkyEditor2.ExtenderException(getMSG().get('LKM|003'));
            }
            
            //①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ
            if(ipValAgentkana != null ){
                extension.ipVal_agentkana.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(ipValAgentkana))));
                ipValAgentkana = extension.ipVal_agentkana.SkyEditor2__Text__c;
            }
            
            //DF-000618対応  start
            if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL) && (E_Util.isNotNull(ipValAgentcd) || E_Util.isNotNull(ipValAgentkana))) {
                String soql = 'Select E_CL3PF_ZBUSBR__c, E_CL3PF_BRANCH__c From Contact ';
                List<String> whereList = new List<String>();
                if (E_Util.isNotNull(ipValAgentcd)) {
                    whereList.add('E_CL3PF_AGNTNUM__c =: ipValAgentcd');
                }
                if (E_Util.isNotNull(ipValAgentkana)) {
                    whereList.add('E_CL3PF_ZEATKNAM__c =: ipValAgentkana');
                }
                whereList.add('E_CL3PF_VALIDFLAG__c = \'1\'');
                whereList.add('E_IsAgent__c = true');
                if (whereList.size() > 0) {
                    soql += 'Where ';
                    soql += String.join(whereList, ' and ');
                }
                soql += ' limit 100';
                List<Contact> recs = Database.query(soql);
                if (recs.size() > 0) {
                    boolean isReference = false;
                    for (Contact con : recs) {
                        if (access.getAccessKindFromThree().equals(con.E_CL3PF_ZBUSBR__c) || access.getAccessKindFromThree().equals(con.E_CL3PF_BRANCH__c)) {
                            isReference = true;
                        }
                    }
                    if (!isReference){
                        throw new SkyEditor2.ExtenderException(getMSG().get('LKM|006'));
                    }
                }
            }
            //DF-000618対応  end
        }
    }

}