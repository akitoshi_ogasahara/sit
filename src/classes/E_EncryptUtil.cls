/*******************************************************************************
 * ING-Linkマイグレーション
 *
 * クラス名 : E_EncryptUtil
 * 概要 : 手続きパスワード暗号化のユーティリティクラス
 * 作成者 : terrasky
 * 作成日 : 2014/12/25
 * 変更履歴 :
 *
 ******************************************************************************/
public with sharing class E_EncryptUtil {
	// 例外クラス
	public class E_EncryptUtilException extends Exception{}

	// 文字と10進数の対応マップ
	public static Map<String, Integer> decimalConversionMap = new Map<String, Integer>{
		' ' => 32, '!' => 33, '"' => 34, '#' => 35, '$' => 36, '%' => 37, '&' => 38, '\'' => 39,
		'(' => 40, ')' => 41, '*' => 42, '+' => 43,	',' => 44, '-' => 45, '.' => 46, '/' => 47, '0' => 48, '1' => 49,
		'2' => 50, '3' => 51, '4' => 52, '5' => 53, '6' => 54, '7' => 55, '8' => 56, '9' => 57, ':' => 58, ';' => 59,
		'<' => 60, '=' => 61, '>' => 62, '?' => 63, '@' => 64, 'A' => 65, 'B' => 66, 'C' => 67,	'D' => 68, 'E' => 69,
		'F' => 70, 'G' => 71, 'H' => 72, 'I' => 73, 'J' => 74, 'K' => 75, 'L' => 76, 'M' => 77,	'N' => 78, 'O' => 79,
		'P' => 80, 'Q' => 81, 'R' => 82, 'S' => 83,	'T' => 84, 'U' => 85, 'V' => 86, 'W' => 87,	'X' => 88, 'Y' => 89,
		'Z' => 90, '[' => 91, '\\' => 92, ']' => 93, '^' => 94,	'_' => 95, '`' => 96, 'a' => 97, 'b' => 98, 'c' => 99,
		'd' => 100, 'e' => 101, 'f' => 102, 'g' => 103, 'h' => 104, 'i' => 105, 'j' => 106, 'k' => 107, 'l' => 108,	'm' => 109,
		'n' => 110, 'o' => 111,	'p' => 112,	'q' => 113,	'r' => 114,	's' => 115,	't' => 116,	'u' => 117,	'v' => 118,	'w' => 119,
		'x' => 120,	'y' => 121,	'z' => 122,	'{' => 123,	'|' => 124,	'}' => 125,	'~' => 126
	};

	/**
	 * 文字列を暗号化＆MD5でハッシュ化した文字列を取得します
	 * @param value 暗号化対象文字列
	 * @return String 暗号化＆MD5でハッシュ化した文字列
	 */
	public static String encrypt(String value){
		if(String.isEmpty(value)){
			return null;
		}

		List<Integer> srcValues = toDecimal(value);

		String encryptValue = 
			md5(encryptType1(srcValues, 0)) +
			md5(encryptType2(srcValues, 1)) +
			md5(encryptType1(srcValues, 2)) +
			md5(encryptType2(srcValues, 3));

		return encryptValue;
	}

	/**
	 * 引数の文字列を1文字ずつ10進数に変換する
	 * @param value 暗号化対象文字列
	 * @return List<Integer> 文字列を10進数に変換したリスト
	 */
	private static List<Integer> toDecimal(String value){
		List<Integer> cnvValues = new List<Integer>();

		for(Integer i = 0; i < value.length(); i++){
			Integer dec = decimalConversionMap.get(value.substring(i, i + 1));
			if(dec == null){
				throw new E_EncryptUtilException('10進数に変換不能な文字列が含まれています。');
			}else{
	 			cnvValues.add(dec);
			}
		}
		return cnvValues;
	}

	/**
	 * 暗号化対象の各10進数値に対し、引数の値を足し、各数値を反転させ、右から連結する
	 * （連結については、リストの順序を逆転させる）
	 * @param srcValues 暗号化対象文字列を10進数化したリスト
	 * @return List<String> 文字列に数値を加算し、順序を逆転させた値
	 */
	private static List<String> encryptType1(List<Integer> srcValues, Integer addValue){
		List<String> cnvValues = new List<String>();
		for(Integer i = srcValues.size(); i > 0; i--){
			cnvValues.add(String.valueof(srcValues.get(i - 1) + addValue).reverse());
		}
		return cnvValues;
	}

	/**
	 * 暗号化対象の各10進数値に対し、引数の値を足し、各数値を反転させず、左から連結する
	 * （連結については、リストの順序をそのままとする）
	 * @param srcValues 暗号化対象文字列を10進数化したリスト
	 * @return List<String> 文字列に数値を加算し、順序を逆転させた値
	 */
	private static List<String> encryptType2(List<Integer> srcValues, Integer addValue){
		List<String> cnvValues = new List<String>();
		for(Integer i = 0; i < srcValues.size(); i++){
			cnvValues.add(String.valueOf(srcValues.get(i) + addValue));
		}
		return cnvValues;
	}
	/**
	 * 暗号化対象の文字列を連結し、MD5でハッシュ値を取得します
	 * @param srcValues 暗号化対象文字列を10進数化＆暗号化したリスト
	 * @return String
	 */
	private static String md5(List<String> srcValues){
		String val = '';
		for(Integer i = 0; i < srcValues.size(); i++){
			val += srcValues.get(i);
		}

		return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(val)));
	}
    
//----------------------------------------------------------------------------------------------------------------------------    
//----------------------------------------------------------------------------------------------------------------------------    
//初期仮パスワードの暗号化----------------------------------------------------------------------------------------------------    
	//仮パスワード桁数
	private static final Integer tempBasicPW = 10;
	//仮パスワード計算算出用
	private static final Integer tempBasicPWLogic = 99;
	//仮パスワードSuffix
	private static final String tempBasicPWSuffix = '01';
	//変換テーブルの作成
	private static final Map<Integer,String> encryptionTable = new Map<Integer,String>
		{
			1=>'9158704623'
			,2 => '3205749816'
			,3 => '2791463085'
			,4 => '5847109236'
			,5 => '1852746930'
			,6 => '4025876931'
			,7 => '6374918052'
			,8 => '1528639704'
			,9 => '0376294158'
		};
	
	/**
	 * 初期仮パスワードの暗号化
	 * @param password 変換対象のパスワード
	 * @param param 変換key(パスワード更新日を想定)
	 * @return 生成されたパスワード
	 */
	public static String encryptInitPass(String password,String param){
		checkPass(password);
		Integer keytotal = allSum(param);
		password = changePass(passWord, keytotal);
		Map<Integer, Integer> toIntKeytotalMap = createMap(getTableMap(keytotal), true);
		Map<Integer, Integer> toIntpasswordMap = createMap(password, true);
		
		Map<Integer, Integer> firstChangeMap = conversionPasswordFirst(toIntpasswordMap, toIntKeytotalMap);
		Map<Integer, Integer> sencondChangeMap = conversionPasswordSecond(firstChangeMap, toIntKeytotalMap);
		return getPassword(sencondChangeMap);
	}
	
	/**
	 * パスワードチェック（桁数、下2桁「01」）
	 * @param password 変換を行うパスワード
	 */
	private static void checkPass(String password) {
		if (String.isEmpty(passWord) || password.length() < tempBasicPW) {
			throw new E_EncryptUtilException('パスワードの桁数が不正です。');
		} else {
			if (!E_Util.isEnNum(password)) {
				throw new E_EncryptUtilException('パスワードが不正です。');
			}
			if (!password.substring(8).equals(tempBasicPWSuffix)) {
				throw new E_EncryptUtilException('パスワードが不正です。'+password);
			}
		}
	}
	
	/**
	 * パスワードの変換を行う。変換テーブルを介さない変換。
	 * @param password 変換を行うパスワード
	 * @param keytotal 変換keyの各桁を足した値
	 * @return 変換されたパスワードの配列。
	 */
	private static String changePass(String password, Integer keytotal){
		Integer cal = tempBasicPWLogic - keytotal;
		String subPass = password.substring(0,8).leftPad(10).replace(' ','0');
		Integer mod = System.Math.mod(Integer.valueOf(subPass), cal);
		return subPass.replaceFirst('00', String.valueOf(mod)).leftPad(10).replace(' ','0');
	}
	
	/**
	 * 変換テーブルを使った1度目の変換を行う
	 * @param passwordMap 変換対象のパスワード
	 * @param keytotalMap 変換に使用する変換テーブルのmap
	 * @retrurn 変換テーブルを使った1度目の変換を実施後のパスワードの配列
	 */
	private static Map<Integer, Integer> conversionPasswordFirst(
			Map<Integer, Integer> passwordMap, Map<Integer, Integer> keytotalMap){
		for (Integer pkey : passwordMap.keySet()) {
			for (Integer tkey : keytotalMap.keySet()) {
				if (passwordMap.get(pkey) == keytotalMap.get(tkey)) {
					Integer total = pkey + tkey -1;
					if (total >= 10) {
						total -= 10;
					}
					passwordMap.put(pkey, total);
					break;
				}
			}
		}
		return passwordMap;
	}
	
	/**
	 * 変換テーブルを使った2度目の変換を行う
	 * @param passwordMap 変換対象のパスワード
	 * @param keytotalMap 変換に使用する変換テーブルの配列
	 * @retrurn 変換テーブルを使った2度目の変換を実施後のパスワードの文字列(暗号化が完了したパスワード)
	 */
	private static Map<Integer, Integer> conversionPasswordSecond(
			Map<Integer, Integer> passwordMap, Map<Integer, Integer> keytotalMap){
		Map<Integer, Integer> changeMap = new Map<Integer, Integer>();
		changeMap.putAll(passwordMap);
		
		for (Integer pkey : passwordMap.keySet()) {
			Integer total = keytotalMap.get(pkey) + 1;
			changeMap.put(total, passwordMap.get(pkey));
		}
		return changeMap;
	}
	
	/**
	 * 文字列を一桁ずつ足す
	 * @param val
	 */
	private static Integer allSum(String val){
		Integer retInteger = 0;
		for (String item : val.split('')) {
			if (string.isEmpty(item)) {
				continue;
			}
			retInteger += integer.valueof(item);
		}
		return retInteger;
	}
	
	/**
	 * 文字列を、map型に変換する
	 * @param value 文字列
	 * @return MAP<integer,integer> key=インデックス value=値
	 */
	private static Map<Integer, Integer> createMap(String value, boolean isKey){
		Map<Integer, Integer> retMap = new Map<Integer, Integer>();
		Integer i=0;
		for (String val : value.split('')) {
			if (String.isEmpty(val)) {
				continue;
			}
			if (isKey) {
				retMap.put(i+1, Integer.valueOf(val));
			} else {
				retMap.put(Integer.valueOf(value.substring(i)), i+1);
			}
			i++;
		}
		return retMap;
	}
	
	/**
	 * 変換テーブルの値を取得し、返す
	 * @param keytotal 変換keyの各桁を足した値
	 * @return 変換keyの各桁を足した値の余り
	 */
	private static String getTableMap(Integer keytotal){
		//除算の余り
		Integer mod = System.Math.Mod(keytotal, 10);
		//もし計算された値の１の位がゼロの場合は１番目変換テーブルを選択する。
		if(mod == 0) mod = 1;
		return encryptionTable.get(mod);
	}
	
	/**
	 * 変換テーブルの値を取得し、返す
	 * @param val 変換対象のパスワード
	 * @return 暗号化されたパスワード
	 */
	private static String getPassword(Map<integer,integer> passMap){
		List<Integer> sortKey = new List<Integer>(passMap.keySet());
		sortKey.sort();
		List<Integer> values = new List<Integer>();
		for (Integer i : sortKey) {
			values.add(passMap.get(i));
		}
		return String.join(values, '');
	}
	
//----------------------------------------------------------------------------------------------------------------------------    
//----------------------------------------------------------------------------------------------------------------------------    
//商品コードの暗号化----------------------------------------------------------------------------------------------------    
    public static String prdCodeConversion(String prdCode,boolean isAngou){
        if(string.isBlank(prdCode)){
            return null;
        }
        String[] henkanMoto = new String[]{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9','[',']',':','#','=','.','_'};
        String[] henkanSaki = new String[]{'z','1','S','N','x','3','T','G','c','V','d','D','F','M','r','7','f','v','b','E','0','8','C','I','6','g','y','h','L','n','9','m','j','a','4','X',':','q','Q','w','R','B','s','U','2','H','5','u','K','!','i','Z','=','W','e','t','_','l','o','J','P','p','@','k','O','Y','A','[',']'};
        String conversionedCode;
        MAP<String,String> conversionMap;
        if(isAngou){
            conversionMap = createCconversionMap(henkanMoto,henkanSaki);
        }else{
            conversionMap = createCconversionMap(henkanSaki,henkanMoto);
        }
        conversionedCode = conversionCode(prdCode,conversionMap);
        return conversionedCode;
    }
    
    private static MAP<String,String> createCconversionMap(String[] key,String[] value){
        MAP<String,String> retMAp = new MAP<String,String>();
        for(integer i =0;key.size() > i;i++){
            retMap.put(key[i],value[i]);
        }
        return retMap;
    }
    
    private static String conversionCode(String prdCode,MAP<String,String> conversionMap){
        String retStr = '';
        for(String item:prdCode.split('')){
            if(String.isNotBlank(item)){
                retStr += conversionMap.get(item);
            }
        }
        return retStr;
    }

    /**
     *	aesKey
     *		2015.10  P15-0016_Canvassing_Rules_Change　で追加
    private static Blob aesKey;
    public static Blob generateAESKey(){
    	if(aesKey==null){
    		aesKey = Crypto.generateAesKey(128);
    	}
    	return aesKey;
    }
     */

    /**
     *	hash
     *		2015.10  P15-0016_Canvassing_Rules_Change　で追加
	 */
    private static Blob hash;
    public static Blob generateHash(){
    	if(hash==null){
    		Blob targetBlob = Blob.valueOf('nnlink');
    		hash = Crypto.generateDigest('MD5',targetBlob);
    	}
    	return hash;
    }
    
    /**
     *	encrypt
     *		2015.10  P15-0016_Canvassing_Rules_Change　で追加
     */
	public static String getEncryptedString(String value){
		//Blob encrypted = Crypto.encryptWithManagedIV('AES128', generateAESKey(), Blob.valueOf(value));
		Blob encrypted = Crypto.encryptWithManagedIV('AES128', generateHash(), Blob.valueOf(value));
		return EncodingUtil.base64Encode(encrypted);
	}
    
    /**
     *	Decrypt
     *		2015.10  P15-0016_Canvassing_Rules_Change　で追加
     */
	public static String getDecryptedString(String encryptedString){
		//Blob decrypted = Crypto.decryptWithManagedIV('AES128', generateAESKey(), EncodingUtil.base64Decode(encryptedString));
		Blob decrypted = Crypto.decryptWithManagedIV('AES128', generateHash(), EncodingUtil.base64Decode(encryptedString));
		return decrypted.toString();
	}
    
}