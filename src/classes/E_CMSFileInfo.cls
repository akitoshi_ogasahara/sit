public class E_CMSFileInfo{
	public E_CMSFile__c info{get;set;}
	public Attachment attachment{get;set;}
	public ContentVersion content{get;set;}		// E_CMSControllerから設定される
	
	public E_CMSFileInfo(E_CMSFile__c rec){
		this.info = rec;
		if(info.attachments.size()>0){
			//添付ファイルは最終更新日が最新の1件のみ取得
			attachment = info.Attachments[0];
		}
	}
		
	public String getAttachmentSize(){
		if(attachment==null){return null;}
		
		return getFileSize(attachment.BodyLength);
	} 
	
	public String getAttachmentExt(){
		if(this.attachment==null){return null;}
		try{
			return E_Util.getExtOfFile(this.attachment.Name);
		}catch(Exception e){
			return '拡張子取得エラー:' + attachment.Name;
		}	
	}
		
	public String getContentSize(){
		if(content==null){return null;}
		
		return getFileSize(content.ContentSize);
	}
	
	public String getContentDownloadUrl(){
		if(content==null){return null;}
		
		return String.format(E_CONST.CONTENT_DOWNLOAD_URL, new List<String>{content.Id});
	}

	private String getFileSize(Decimal len){
		if(len > 1000000){
			return String.valueOf(E_Util.roundDecimal(len/1024/1024, 2))+'MB';
		}else{
			return String.valueOf(E_Util.roundDecimal(len/1024, 0))+'KB';
		}
	}

}