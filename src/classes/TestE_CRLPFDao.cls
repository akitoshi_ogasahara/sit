@isTest
private class TestE_CRLPFDao {
	
	@isTest static void getRecsByTypeTest01() {
		E_Policy__c policy = createPolicy();
		createCrlpf(policy,E_Const.CLRRROLE_BN);
		Test.startTest();
		List<E_CRLPF__c> crlpf = E_CRLPFDao.getRecsByType(policy.Id,E_Const.CLRRROLE_BN);
		Test.stopTest();

		System.assertEquals(crlpf[0].ZCLNAME__c,'照須快');

	}

	@isTest static void getRecByTypeTest01() {
		E_Policy__c policy = createPolicy();
		createCrlpf(policy,E_Const.CLRRROLE_BN);
		Test.startTest();
		E_CRLPF__c crlpf = E_CRLPFDao.getRecByType(policy.Id,E_Const.CLRRROLE_BN);
		Test.stopTest();

		System.assertEquals(crlpf.ZCLNAME__c,'照須快');

	}

	@isTest static void getRecsForColiTest01() {
		E_Policy__c policy = createPolicy();
		createCrlpf(policy,E_Const.CLRRROLE_BN);
		Test.startTest();
		List<E_CRLPF__c> crlpf = E_CRLPFDao.getRecsForColi(policy.Id);
		Test.stopTest();

		System.assertEquals(crlpf[0].ZCLNAME__c,'照須快');
	}

	@isTest static void getRecsByRoleNETest01() {
		E_Policy__c policy = createPolicy();
		createCrlpf(policy,E_Const.CLRRROLE_NE);
		Test.startTest();
		List<E_CRLPF__c> crlpf = E_CRLPFDao.getRecsByRoleNE(policy.Id);
		Test.stopTest();

		System.assertEquals(crlpf[0].ZCLNAME__c,'照須快');
	}


	private static E_Policy__c createPolicy(){
		E_Policy__c pol = new E_Policy__c();
		pol.COMM_CHDRNUM__c = '99999999';

		insert pol;
		return pol;
	}

	//保険顧客情報
	private static void createCrlpf(E_Policy__c policy,String role){
		E_CRLPF__c crl = new E_CRLPF__c();

		crl.E_Policy__c = policy.Id;
		crl.BNYTYP__c = '01';
		crl.ZCLNAME__c = '照須快';
		crl.CLNTNUM__c = '99999999';
		crl.ZKNJSEX__c = '男性';
		crl.BNYPC__c = 100.00;
		crl.DOB__c = '11111111';
		crl.CRLPFCode__c = '99999999|TS|1';
		crl.CLRRROLE__c = role;
		crl.VALIDFLAG__c = '1';



		insert crl;
	}
}