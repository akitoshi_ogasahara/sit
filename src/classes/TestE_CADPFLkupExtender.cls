@isTest(SeeAllData=false)
public with sharing class TestE_CADPFLkupExtender {
    
    static void init(){
        system.debug('this is init');
        insertMessage();
		List<customClass> customList = new LIST<customClass>();
		customList.add(new customClass('111','1000'));
		insertPostCode(customList);   
    }

    //レコード1件の正常系 
    private static testMethod void testStandard() {
        init();
        // インスタンス
        E_CADPF__c CADPF = new E_CADPF__c();
        Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(CADPF);
        E_CADPFLkupController controller = new E_CADPFLkupController(standardcontroller);
        E_CADPFLkupExtender extender = controller.getExtender();
 		setPageValue(new customClass('111','1000'),controller); 
        Test.startTest();	
        extender.presearch();
        controller.dosearch();
        extender.aftersearch();
        //確認
        System.assertEquals(false, Apexpages.hasMessages());
        Test.stopTest();
    }
    
    //郵便番号に数字以外が入っていた場合
    private static testMethod void testErrorNotNum() {
        init();
        // インスタンス
        E_CADPF__c CADPF = new E_CADPF__c();
        Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(CADPF);
        E_CADPFLkupController controller = new E_CADPFLkupController(standardcontroller);
        E_CADPFLkupExtender extender = controller.getExtender();
        // エラー値取得用
        String errorMessage;
        
        Test.startTest();
        // 先頭3桁に数字以外が入るケース
 		setPageValue(new customClass('11a','1000'),controller);
        SkyEditor2.messages.clear();
        try{
        	extender.presearch();
        }catch(Exception e){
        	//例外が生成されるはず
        	errorMessage = e.getMessage();
        }	
        // 確認
        System.assertEquals('CPL|001', errorMessage);
        
        // 後方4桁に数字以外が入るケース
		errorMessage = null;
        setPageValue(new customClass('111','100a'),controller);
        SkyEditor2.messages.clear();
        try{
        	extender.presearch();
        }catch(Exception e){
        	//例外が生成されるはず
        	errorMessage = e.getMessage();
        }	
        //確認
        System.assertEquals('CPL|001', errorMessage);
        Test.stopTest();
    }
    
    // 郵便番号の桁数チェック
    private static testMethod void testErrorNotPostCode() {
        init();
        // インスタンス
        E_CADPF__c CADPF = new E_CADPF__c();
        Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(CADPF);
        E_CADPFLkupController controller = new E_CADPFLkupController(standardcontroller);
        E_CADPFLkupExtender extender = controller.getExtender();
        // エラー値取得用
        String errorMessage;
        
        Test.startTest();
        // 先頭が3桁ではないケース
        setPageValue(new customClass('11','1000'),controller);
        SkyEditor2.messages.clear();
        try{
        	extender.presearch();
        }catch(Exception e){
        	//例外が生成されるはず
        	errorMessage = e.getMessage();
        }	
        //確認
        System.assertEquals('CPL|002', errorMessage);
        
        // 後方が4桁ではないケース
        errorMessage = null;
  		setPageValue(new customClass('111','10000'),controller); 
        SkyEditor2.messages.clear();
        try{
        	extender.presearch();
        }catch(Exception e){
        	//例外が生成されるはず
        	errorMessage = e.getMessage();
        }	
        //確認
        System.assertEquals('CPL|002', errorMessage);
        Test.stopTest();
    }
    
    // レコード0件の異常系 
    private static testMethod void testNoRecord() {
        init();
        E_CADPF__c testE_CADPF = [select Id from E_CADPF__c limit 1];
        // インスタンス
        E_CADPF__c CADPF = new E_CADPF__c();
        Test.setCurrentPage(Page.E_CADPFLkup);
        Apexpages.currentPage().getParameters().put('cadpfId', testE_CADPF.Id);
        Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(CADPF);
        E_CADPFLkupController controller = new E_CADPFLkupController(standardcontroller);
        E_CADPFLkupExtender extender = controller.getExtender();
        
        
 		// エラー値取得用
        String errorMessage;
 		
        Test.startTest();
        //確認
        System.assertEquals('CPL|003', extender.getErrorMessage0());
        Test.stopTest();
    }
    
    private static void insertMessage(){
        LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
        String type = 'メッセージ';
        String param = 'CPL|001';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'CPL|002';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'CPL|003';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        insert insMessage;
    }
    
    private static void insertPostCode(LIST<customClass> val){
        LIST<E_CADPF__c> insRecord = new LIST<E_CADPF__c>();
        for(customClass item : val){
            insRecord.add(new E_CADPF__c(ZPOSTCD__c = item.ZPOSTCD,ZPOSTMARK__c = item.ZPOSTMARK));
        }
        insert insRecord;
    }
    
    private class customClass{
        String ZPOSTCD;
        String ZPOSTMARK;
        customClass(String zPostCode,String zPostMark){
            this.ZPOSTCD = zPostCode;
            this.ZPOSTMARK = zPostMark;
        }
    }
    
    private static void setPageValue(customClass val,E_CADPFLkupController con){
    	con.ZPOSTCD.SkyEditor2__Text__C = val.ZPOSTCD;
    	con.ZPOSTMARK.SkyEditor2__Text__C = val.ZPOSTMARK;    	
    }
}