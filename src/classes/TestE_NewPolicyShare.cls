/**
 * 
 */
@isTest
private class TestE_NewPolicyShare {
	private static final String SHARE_ROWCAUSE_NNLink = 'NNLinkSharingRule__c';
	
	private static Account distribute;
	private static Account office;
	private static Account subdistribute;
	private static Account suboffice;
	private static E_NewPolicy__c newpolicy;

    /**
     * void execute()
	 */
	static testMethod void execute_test001(){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		
		// 公開グループ
		createGroup('L1' + CODE_DISTRIBUTE, 'L2' + CODE_OFFICE, 'BR' + CODE_BRANCH);
		createGroup('L1' + CODE_DISTRIBUTE_SUB, 'L2' + CODE_OFFICE_SUB, 'BR' + CODE_BRANCH_SUB);

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;
		subdistribute = new Account(Name = 'テスト代理店格SUB',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE_SUB);
		insert subdistribute;
		
		// Account 事務所
		office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH);
		insert office;
		suboffice = new Account(Name = 'テスト事務所SUB',ParentId = subdistribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB);
		insert suboffice;
		
		// 新契約ステータス
		newpolicy = new E_NewPolicy__c(
			ParentAccount__c = distribute.Id,
			Account__c = office.Id,
			SubParentAccount__c = subdistribute.Id,
			SubAccount__c = suboffice.id,
			CHDRNUM__c = '00000001',
			STATCODE__c = 'P',
			SyncDate__c = System.now()
		);
		insert newpolicy;
		
		// SObject Map
		office = [Select Id, ParentId, E_ParentZHEADAY__c, E_CL2PF_ZAGCYNUM__c, E_CL2PF_BRANCH__c From Account Where Id = :office.Id Limit 1];
		Map<SObject, Account> sobjMap = new Map<SObject, Account>();
		sobjMap.put(newpolicy, office);
		suboffice = [Select Id, ParentId, E_ParentZHEADAY__c, E_CL2PF_ZAGCYNUM__c, E_CL2PF_BRANCH__c From Account Where Id = :suboffice.Id Limit 1];
		Map<SObject, Account> sobjMapSub = new Map<SObject, Account>();
		sobjMapSub.put(newpolicy, suboffice);
		
		// Test
		Test.startTest();
		
		E_NewPolicyShare base = new E_NewPolicyShare(sobjMap, sobjMapSub);
		base.execute();
		
		Test.stopTest();
		
		Map<String, E_NewPolicy__Share> resultShareMap = new Map<String, E_NewPolicy__Share>();
		List<E_NewPolicy__Share> shares = [Select Id, ParentId, AccessLevel, RowCause, UserOrGroupId, UserOrGroup.Name From E_NewPolicy__Share Where ROWCause = :SHARE_ROWCAUSE_NNLink];

		for(E_NewPolicy__Share share : shares){
			resultShareMap.put(share.UserOrGroup.Name, share);
		}
		System.debug(resultShareMap);
		
		// assertion
		System.assertEquals(6, shares.size());
		System.assert(resultShareMap.get('L1' + CODE_DISTRIBUTE) != null);
		System.assert(resultShareMap.get('L2' + CODE_OFFICE) != null);
		System.assert(resultShareMap.get('BR' + CODE_BRANCH) != null);
		System.assert(resultShareMap.get('L1' + CODE_DISTRIBUTE_SUB) != null);
		System.assert(resultShareMap.get('L2' + CODE_OFFICE_SUB) != null);
		System.assert(resultShareMap.get('BR' + CODE_BRANCH_SUB) != null);

	}
	
	/**
	 * 公開グループ準備
	 */
	private static void createGroup(String d, String o, String b){
		// 既存の公開グループ
		Map<String, Group> grpMap = new Map<String, Group>();
		for(Group grp : [Select Id, DeveloperName From Group Where DeveloperName In (:d, :o, :b)]){
			grpMap.put(grp.DeveloperName, grp);
		}
		
		// 既存に存在しない公開グループ作成
		List<Group> grps = new List<Group>();
		if(grpMap.get(d) == null){
			grps.add(new Group(Name=d, DeveloperName=d, Type='Regular'));
		}
		if(grpMap.get(o) == null){
			grps.add(new Group(Name=o, DeveloperName=o, Type='Regular'));
		}
		if(grpMap.get(b) == null){
			grps.add(new Group(Name=b, DeveloperName=b, Type='Regular'));
		}
		if(!grps.isEmpty()){
			insert grps;
		}
	}
}