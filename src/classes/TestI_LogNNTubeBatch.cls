@isTest
private class TestI_LogNNTubeBatch {
	//正常動作
	@isTest static void logNNTubetest1() {
		String query = 'Select Id, AccessDatetime__c, BatchDate__c From E_Log__c Where ' +
						 'ActionType__c = \'' + I_Const.ACTION_TYPE_MOVIE + '\' And BatchDate__c <> null';

		Test.setMock(HttpCalloutMock.class, new TestI_LogNNTubeHTTPRequestMock(false));
		Test.startTest();
		createLog();
		Integer i =  Database.query(query).size();		//バッチ起動前
		System.debug(i);
		I_LogNNTubeBatch logBatch = new I_LogNNTubeBatch();
		Database.executeBatch(logBatch,200);
		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, i+2);
	}
	
	//処理結果コードがエラーの場合
	@isTest static void logNNTubetest2() {
		String query = 'Select Id, AccessDatetime__c, BatchDate__c From E_Log__c Where ' +
						'IsError__c = true';

		Test.setMock(HttpCalloutMock.class, new TestI_LogNNTubeHTTPRequestMock(true));
		Test.startTest();
		createLog();
		I_LogNNTubeBatch logBatch = new I_LogNNTubeBatch();
		Database.executeBatch(logBatch,200);
		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}
	
	static void createLog(){
		// 実行ユーザ作成（MR）
		E_Log__c testLog = new E_Log__c(Name = 'testLog', AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage',isDisplay__c = true,Detail__c = 'testLog',ActionType__c = '動画',batchDate__c = datetime.now());
		insert testLog;
	}
}