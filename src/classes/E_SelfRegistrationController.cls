public with sharing class E_SelfRegistrationController extends E_InfoController{


	//独自の例外クラス
	public class SelfException extends Exception{}

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	//ページ表示フラグ。true:発行画面、false：確認画面
	public Boolean selfRegFlag {get; set;}
	//false:確認画面　true:完了画面
	public Boolean compFlag {get; set;}
	//チェックボックス取得変数
	public Boolean infoCheck{get; set;}
	public Boolean ruleCheck{get; set;}
	public Boolean perCheck{get; set;}

	//募集人・mofコード
	public String agOrMofcode{get; set;}
	//生年月日
	private String dayofBirth {get;set;}
	//年
	public String year{get; set;}
	//月
	public String month{get; set;}
	//日
	public String day{get; set;}
	//Email（ユーザID）
	public String eMail {get;set;}

	//確認画面に値を渡す募集人レコード
	private Contact contactRec;

	//アクセスログ
	private E_Log__c pageAccessLog;

	//ラジオボタン取得変数
	public String selection{get; set;}

	//誕生日の年数表示用リスト
	public List<selectoption> birthYear{
		get{
			List<selectoption> options = new List<selectoption>();
			for(Integer i = 1900 ; i < 2017; i++){
				options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
			}
			return options;
		}
		set;
	}

	//IE8
	public Boolean isUserAgentIE8{get{return E_Util.isUserAgentIE8();} private set;}

	//ID管理のパスワードステータスが2のユーザーID
	private String userId;

	public E_SelfRegistrationController() {

		pageMessages = getPageMessages();
		//ID発行画面用
		pgTitle = 'ID自動発行';
		//ID発行画面表示
		selfRegFlag = true;
		compFlag = false;
		//チェックボックス初期値
		infoCheck = false;
		ruleCheck = false;
		perCheck = false;
		//ラジオボタン初期値
		selection = 'agent';
		//年の初期値
		year='1980';

	}

	//ラジオボタン作成
	public List<SelectOption> getOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('agent', '募集人コード'));
		options.add(new SelectOption('mof', 'MOFコード'));
		return options;
	}

/**
	 * ID自動発行
	 */
	//NN link ID発行ボタン押下
	public PageReference doCreateId(){
		//ページメッセージクリア
		pageMessages.clearMessages();

		//生年月日
		if(String.isNotBlank(year)){
			dayofBirth = year + month + day;
		}

		//入力チェック
		if(validateIsError()){
			return null;
		}

		//userIdを初期化する TODO
		userId = '';

		//募集人レコード取得
		List<Contact> contactRecs;
		if(selection == 'agent'){
			contactRecs = E_ContactDaoWithout.getRecByAgntNum(agOrMofcode);
		}else if(selection == 'mof'){
			//ID適正化改修
			contactRecs = E_ContactDaoWithout.getRecByMOF(agOrMofcode);
		}

		//募集人レコードチェック
		if(contactRecs.isEmpty()||contactRecs.size() != 1){
			//募集人情報が確認できませんでした。
			pageMessages.addErrorMessage(getMSG().get('CID|004'));
			return null;
		}
		contactRec = contactRecs[0];

		//生年月日、顧客番号が一致するContactレコードも存在するかを確認
		List<Contact> clientList = E_ContactDaoWithout.getRecByCLNTNUM(contactRec.E_CL3PF_CLNTNUM__c, dayofBirth);
		if(clientList.isEmpty()){
			//募集人情報が確認できませんでした。
			pageMessages.addErrorMessage(getMSG().get('CID|004'));
			return null;
		}

		Savepoint sp = Database.setSavepoint();

		//ID適正化 start
		//20171215  関連する募集人(メイン募集人、|つき募集人)を取得
		List<Contact> contacts = E_ContactDaoWithout.getRecByAgentNum5(contactRec.AgentNum5__c);

		//関連する募集人情報のIDから、Userを取得する
		Set<Id> contactIds = new Set<Id>();
		for(Contact con : contacts){
			contactIds.add(con.id);
		}

		//取得した募集人に紐づくユーザーレコードのリスト
		//SystemOriginFlag(ECL3由来のオリジナルレコードはTrueとなる)の降順で並び変えて取得
		List<User> usrRecs = E_UserDaoWithout.getUserRecsByContactIds(contactIds);

		//ID適正化 end

		try{
			//ID適正化 start
			//ユーザレコードが取得できた場合
			if(usrRecs.size() >= 1){
				for(User usRec : usrRecs){
					//Userに紐づくID管理
					for(E_IDCPF__c idcpf : usRec.E_IDCPFs__r){
						//パスワードステータスが2以外のとき、エラーメッセージを出す
						if(idcpf.ZSTATUS01__c <> '2'){
							//すでに登録済みです
							pageMessages.addErrorMessage(getMSG().get('CID|006'));
							return null;
						}
					}
				}
				//List取得時に並び替えを行っているため、先頭は必ずメインの募集人に対応したUser
				User usrRec = usrRecs[0];
				//対象ユーザーのIDを保持　TODO
				userId = usrRec.Id;
				//ID管理が存在しない場合
				if(usrRec.E_IDCPFs__r.size() == 0){
					//ユーザーが有効の場合は、仮パスワード発行
					if(usrRec.isActive){
						//メールアドレスが重複している他ユーザーがいないかチェック TODO
						if(userMailCheck()){
							return null;
						}
						//仮パス発行
						if(usrRec.EMAIL != eMail){
							usrRec.Username = eMail + System.Label.E_USERNAME_SUFFIX;
							usrRec.Email = eMail;
							E_UserDaoWithout.updateUserRec(usrRec);
						}
						//仮PWを再発行する
						E_SelfRegistrationController.tempPassSet(usrRec,contactRec.Name,getMSG().get('MAB|002'),eMail);

						//完了画面表示
						compFlag = true;
					}else{ //ユーザーが無効の場合は、エラー
						//すでに登録済みです
						pageMessages.addErrorMessage(getMSG().get('CID|006'));
					}
				//ID管理が存在する場合
				}else{
					//メールアドレスが重複している他ユーザーがいないかチェック TODO
					if(userMailCheck()){
						return null;
					}
					//確認画面表示
					selfRegFlag = false;
				}

				return null;
			}
			/* Link使用前登録済みユーザ判定対応 */
			//ユーザが取得できず、ID管理のレコードが取得できた場合
			else if(IdRecCheck()){
				return null;
			}

			//メールアドレスが重複しているユーザーがいないかチェック TODO
			if(userMailCheck()){
				return null;
			}

			//確認画面表示
			selfRegFlag = false;
			return null;
		}catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			pageMessages.addErrorMessage(e.getMessage());
			ApexPages.addMessage(msg);
			if(usrRecs[0].EMAIL == eMail){
				Database.rollback(sp);
			}
			//アクセスログ
			pageAccessLog = E_LogUtil.createLog();
			URL urlObj = URL.getCurrentRequestUrl();
			pageAccessLog.AccessPage__c = urlObj.getPath();
			pageAccessLog.Detail__c = E_LogUtil.getLogDetailString(ApexPages.Severity.ERROR, e.getMessage(), e.getStackTraceString());
			insert pageAccessLog;
			return null;
		}
	}

	//キャンセルボタン押下
	public PageReference doCansel(){
		//IRISログイン画面へ
		return page.E_LoginAgent;
	}

	//入力チェック
	// エラー：true / エラーなし：false
	private Boolean validateIsError(){

		//募集人コード/MOFコード必須チェック
		if(String.isBlank(this.agOrMofcode)){
			//募集人コードは必須です
			//'募集人コードまたはMOFコードは必須です。'
			pageMessages.addErrorMessage(getMSG().get('CID|001'));
		}
		//生年月日必須チェック/型チェック
		if(String.isBlank(this.dayofBirth)){
			//'生年月日は必須です。'
			pageMessages.addErrorMessage(getMSG().get('CID|002'));
		}else if(!E_Util.isYyyyMMddNonSrash(this.dayofBirth)){
			//'生年月日はyyyyMMdd 形式で入力して下さい。'
			pageMessages.addErrorMessage(getMSG().get('CID|003'));
		}
		//メールアドレス必須/型チェック
		if(String.isBlank(this.eMail)){
			//'新規電子メールアドレスを入力して下さい。'
			pageMessages.addErrorMessage(getMSG().get('MAC|001'));
		}else if(!E_Util.isMailAddress(this.eMail.toUpperCase())){
			//電子メールアドレスが正しくありません。
			pageMessages.addErrorMessage(getMSG().get('MAC|005'));
		}

		if(pageMessages.hasMessages()){
			return true;
		}
		return false;
	}

	/**
	 * ID自動発行確認画面
	 */
	//情報を修正したい方はこちらリンクを押されたときにページ遷移
	public PageReference doBackFromConfirm(){
		pageMessages.clearMessages();
		selfRegFlag = true;
		return null;
	}

	//同意するボタンを押されたときの処理
	public PageReference doConfirm(){
		pageMessages.clearMessages();

		//同意するがチェックされているか
		if(!infoCheck || !ruleCheck || !perCheck){
			//情報のお取り扱い、利用規定、個人情報の取扱いに同意した後、ボタンを押してください。
			pageMessages.addErrorMessage(getMSG().get('CID|011'));
			return null;
		}

		//プロファイルＩＤをNameから取得
		//Profile proRec = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY);
		Profile proRec = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);   //170118 E_PartnerCommunity_IAプロファイル
		if(proRec==null){
			pageMessages.addErrorMessage(getMSG().get('ERR|999'));
			return null;
		}
		String proId = proRec.Id;

		//エラー用
		Database.SaveResult result;
		Savepoint sp = Database.setSavepoint();

		//ユーザ
		User uc;

		try{
			//ユーザ項目に値を設定
			uc = this.createNewUser();
			//ID適正化 start
			if(String.isNotBlank(userId)){
				uc.Id = Id.valueof(userId);
			}else{
				//完了画面から戻ってきた場合エラー
				List<User> userList = E_UserDaoWithout.getUserRecByUserRec(uc);
				if(userList.size() > 0){
					pageMessages.addErrorMessage(getMSG().get('ERR|999'));
					return null;
				}
			}

			//ユーザ作成 プロファイルはwithoutでないと設定できない
			if(String.isNotBlank(userId)){	//20171213
				//UserとID管理のリレーションを切る
				//DML Exception 回避のため、非同期で処理する
				E_IDCPFDaoWithout.cutOutRelationWithUser(uc.Id);
				//権限セットを全て外す
				Set<Id> usIds = new Set<Id>();
				usIds.add(uc.Id);
				List<PermissionSetAssignment> deleteTargets = new List<PermissionSetAssignment>();
				for(User usr:E_UserDaoWithout.getUserWithPermissions(usIds).values()){
					deleteTargets.addAll(usr.PermissionSetAssignments);
				}
				if(!deleteTargets.isEmpty()){
					// Delete処理
					List<Database.DeleteResult> results = Database.Delete(deleteTargets, false);
					//権限セットの削除に失敗した場合、エラー処理
					for(Database.DeleteResult dr : results){
						if(!dr.isSuccess()){
							//Exception を投げてロールバック処理を行う
							SelfException ex = new SelfException();
							ex.setMessage(getMSG().get('CID|010'));
							throw ex;
						}
					}
				}
				//公開グループからの削除
				List<GroupMember> deleteGms = E_GroupMemberDaoWithout.getRecByUserORGroupId(usIds);
				if(!deleteTargets.isEmpty()){
					// Delete処理
					List<Database.DeleteResult> results = E_GroupMemberDaoWithout.doDelete(deleteGms);
					//公開グループの削除に失敗した場合、エラー処理
					for(Database.DeleteResult dr : results){
						if(!dr.isSuccess()){
							//Exception を投げてロールバック処理を行う
							SelfException ex = new SelfException();
							ex.setMessage(getMSG().get('CID|010')+dr);
							throw ex;
						}
					}
				}
				//ユーザー情報を自動発行と同一のものに更新
				Database.SaveResult updateResult = E_UserDaoWithout.updateUserRec(uc);
				if(!updateResult.isSuccess()){
					for(Database.Error err :updateResult.getErrors()){
						//Exception を投げてロールバック処理を行う
						SelfException ex = new SelfException();
						ex.setMessage(getMSG().get('CID|010'));
						throw ex;
					}
				}
				//doCreatePMS = E_PermissionSetDaoWithout.checkUserToPMS(uc.Id, 'E_PermissionSet_Base');
			}else{
				uc = E_UserDaoWithout.createUser(uc,proId);
			}

			//ユーザができていない場合エラー処理
			if(uc == null){
				pageMessages.addErrorMessage(getMSG().get('CID|010'));
				return null;
			}

			//↓↓↓ユーザーが作成された場合
			//仮ログイン時にエラーにならないよう最低限の権限セットを付加する
			Set<String> permissionNames = new Set<String>();
			permissionNames.add('E_PermissionSet_Base');//基本セット　全ユーザ対象
			E_PermissionSetDaoWithout.insertRecsToUserByUId(uc.Id,permissionNames);
			//ID適正化 end

			//仮PW設定
			E_SelfRegistrationController.tempPassSet(uc,contactRec.Name,getMSG().get('MAB|002'),eMail);

			//完了画面表示
			compFlag = true;
			return null;

		}catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			pageMessages.addErrorMessage(e.getMessage());
			ApexPages.addMessage(msg);
			if(uc != null){
				Database.rollback(sp);
			}
			//アクセスログ
			pageAccessLog = E_LogUtil.createLog();
			URL urlObj = URL.getCurrentRequestUrl();
		//  pageAccessLog.AccessPage__c = urlObj.getPath();
		//  pageAccessLog.Detail__c = E_LogUtil.getLogDetailString(ApexPages.Severity.ERROR, e.getMessage(), e.getStackTraceString());
		//  insert pageAccessLog;

			return null;
		}
	}

	/**
	 * ユーザに値を入れるメソッド
	 * @return cnu 各項目に値を入れたユーザ
	 */
	public User createNewUser(){
		//変数設定
		String username = eMail + System.Label.E_USERNAME_SUFFIX;
		//Aliasが9文字以上だとエラーになるため、先頭8文字をAliasとして設定
		String aliasName = contactRec.Name;
		if(aliasName.length() > 8){
			aliasName = aliasName.substring(0,8);
		}

		User cnu = new User();
		cnu.PortalRole = E_Const.USER_PORTALROlE_WORKER;
		cnu.Username = username;
		cnu.ContactId = contactRec.Id;
		cnu.FirstName = contactRec.FirstName;
		cnu.LastName = contactRec.LastName;
		cnu.Email = eMail;
		//cnu.CommunityNickname =username;
		cnu.Alias = aliasName;
		//後に設定値はconstから
		cnu.TimeZoneSidKey= E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		cnu.EmailEncodingKey= E_Const.USER_EMAILENCODINGKEY_UTF8;
		cnu.LocaleSidKey= E_Const.USER_LOCALESIDKRY_JA_JP;
		cnu.LanguageLocaleKey= E_Const.USER_LANGUAGELOCLEKEY_JA;
		cnu.E_UseExistingBasicPW__c = false;
		//仮パスワードTRUE
		cnu.E_UseTempBasicPW__c = true;
		cnu.E_ZWEBID__c = null;
		cnu.IsActive = true;
		cnu.E_TempBasicPWModifiedDate__c=E_Util.getSysDate(null);

		return cnu;
	}

	/**
	 * 仮PW設定用メソッド
	 *  @param uc ユーザレコード
	 *  @return proPassword 生成した仮のPW　非暗号化の状態
	 */
	public static String setProPassword(User uc){
		//パスワード変更。8桁ランダム数字+01
		String proPassword = E_Util.getRandomPassword(8) + '01';
		//パスワード暗号化
		String enPassword = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(proPassword, uc.E_TempBasicPWModifiedDate__c);
		//setPassword
		E_SetPasswordWithout.setPassword(uc.Id, enPassword);
		return proPassword;
	}

	/**
	 * メール送信用メソッド
	 * @param proPass 暗号化していない状態の仮PW
	 */
	@future
	public static void sendConfirmEmail(String proPass,String eMail,String mailText){
		Messaging.SingleEmailMessage sem = new Messaging.SingleEmailMessage();
		//送信先アドレス設定
		sem.setToAddresses(new String[]{eMail});
		//送信元アドレス設定
		OrgWideEmailAddress senderAdress = E_OrgWideEmailAddressDao.getOrgWideEmailAddress(E_Const.EMAIL_NAME_ILJPSCREGCHANGE);
		sem.setOrgWideEmailAddressId(senderAdress.Id);
		//メールの件名 →　NNLink 新規アカウントの発行
		sem.setSubject(System.Label.E_MailTitle_NewAcc);

		sem.setHtmlBody(mailText);
		//送信
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { sem });
	}

	/* Link使用前登録済みユーザ判定対応 */
	//ID管理レコード確認用メソッド
	private Boolean IdRecCheck(){
		List<E_IDCPF__c> idRecs = E_IDCPFDaoWithout.getIdRecsByContactId(contactRec.Id);

		if(idRecs.size() == 1){
			//すでに登録済みです
			pageMessages.addErrorMessage(getMSG().get('CID|006'));
			return true;
		}
		return false;
	}

	public static void tempPassSet(User uesrRec,String conName,String msg,String eMail){
		String proPass = E_SelfRegistrationController.setProPassword(uesrRec);
		String pageURL = Site.getBaseUrl() + '?maillogin=1';
		//本文
		String mailText = String.format(msg,new String[]{conName,proPass,pageURL});
		//メール送信
		E_SelfRegistrationController.sendConfirmEmail(proPass,eMail,mailText);
	}

	//メールアドレスまたはユーザーネームが該当し、かつユーザが有効である条件でユーザレコードが一件以上ヒットするとき
	//ID適正化対象ユーザーの場合、ユーザーIDが同一の場合は取得しない
	private Boolean userMailCheck(){
		List<User> usrRecs = E_UserDaoWithout.getUserRecsByEmailOrUsernameAndIsActive(this.eMail,userId); //ID適正化
		if(usrRecs.size() > 0){
			//ご指定のメールアドレスはIDとして利用できません。
			pageMessages.addErrorMessage(getMSG().get('CID|008'));
			return true;
		}
		return false;

	}

}