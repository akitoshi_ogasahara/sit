@isTest
private class TestASS_CalendarController {

	//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: なし
	 * 初期表示
	 */
	private static testMethod void areaManagerTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc1 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc1;
		Account acc2 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1');
		insert acc2;

		//事務所
		Account baOffice = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc1.Id, OwnerId = mr.Id);
		insert baOffice;
		//事務所
		Account iaOffice = new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc2.Id, OwnerId = mr.Id);
		insert iaOffice;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];

		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_CalendarController con = new ASS_CalendarController();
			con.paramMrId = mr.Id;
			con.paramUnitId = unit.Id;

			//Monthクラス
			con.getMonth();
			con.getMonth().getMonthName();
			con.getMonth().getYearName();
			con.getMonth().getWeekdayNames();
			con.getMonth().getFirstDate();
			con.getWeeks();

			//Weekクラス
			ASS_CalendarController.Week weekCon = new ASS_CalendarController.Week();
			weekCon.getDays();
			weekCon.getWeekNumber();
			weekCon.getStartingDate();

			//Dayクラス
			ASS_CalendarController.Day dayCon = new ASS_CalendarController.Day(System.today(), System.today().Month());
			dayCon.getDate();
			dayCon.getDayOfMonth();
			dayCon.getDayOfMonth2();
			dayCon.getDayOfYear();
			dayCon.getDayAgenda();
			dayCon.getFormatedDate();
			dayCon.getDayNumber();
			dayCon.getEventsToday();
			dayCon.getCSSName();
			dayCon.getDateCalendarLink();
			dayCon.getLinkDate();

			con.next();
			con.prev();
//===============================テスト終了===============================
			Test.stopTest();

			//System.assertEquals(con.events.size(),3);
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: なし
	 * 初期表示
	 */
	private static testMethod void mrTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//代理店格
		Account acc1 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc1;
		Account acc2 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1');
		insert acc2;

		//事務所
		Account baOffice = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc1.Id, OwnerId = actUser.Id);
		insert baOffice;
		//事務所
		Account iaOffice = new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc2.Id, OwnerId = actUser.Id);
		insert iaOffice;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_CalendarController con = new ASS_CalendarController();
			con.paramMrId = actUser.Id;
			con.paramUnitId = unit.Id;

			con.next();
			con.prev();
//===============================テスト終了===============================
			Test.stopTest();

			//System.assertEquals(con.isMRpage,true);
		}
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: なし
	 *　新規行動作成画面遷移
	 */
	private static testMethod void createEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc1 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc1;
		Account acc2 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1');
		insert acc2;

		//事務所
		Account baOffice = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc1.Id, OwnerId = mr.Id);
		insert baOffice;
		//事務所
		Account iaOffice = new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc2.Id, OwnerId = mr.Id);
		insert iaOffice;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('unitId',unit.Id);
		pr.getParameters().put('mo','10');
		pr.getParameters().put('yr','2018');
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_CalendarController con = new ASS_CalendarController();
			con.paramMrId = mr.Id;
			con.paramUnitId = unit.Id;
			pr.getParameters().put('date',String.valueOf(system.today().year() + '-' + system.today().month() + '-' + system.today().day()));
			Test.setCurrentPage(pr);
			//con.createEvent();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_EventInputPage;
			newPage.getParameters().put('retPgName','/apex/ASS_MRPage');
			newPage.getParameters().put('mrId',mr.Id);
			newPage.getParameters().put('unitId',unit.Id);
			newPage.getParameters().put('date',String.valueOf(system.today().year() + '-' + system.today().month() + '-' + system.today().day()));
			newPage.setRedirect(true);
			System.assertEquals(con.createEvent().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: なし
	 *　新規行動作成画面遷移
	 */
	private static testMethod void viewEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc1 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc1;
		Account acc2 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1');
		insert acc2;

		//事務所
		Account baOffice = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc1.Id, OwnerId = mr.Id);
		insert baOffice;
		//事務所
		Account iaOffice = new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc2.Id, OwnerId = mr.Id);
		insert iaOffice;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		
		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_CalendarController con = new ASS_CalendarController();
			con.paramMrId = mr.Id;
			con.paramUnitId = unit.Id;
			pr.getParameters().put('eventId',eventList[0].Id);
			Test.setCurrentPage(pr);

			//con.createEvent();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_EventInputPage;
			newPage.getParameters().put('retPgName','/apex/ASS_MRPage');
			newPage.getParameters().put('mrId',mr.Id);
			newPage.getParameters().put('unitId',unit.Id);			
			newPage.setRedirect(true);
			System.assertEquals(con.viewEvent().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: なし
	 * 日付押下テスト
	 */
	private static testMethod void clickdateTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc1 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc1;
		Account acc2 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1');
		insert acc2;

		//事務所
		Account baOffice = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc1.Id, OwnerId = mr.Id);
		insert baOffice;
		//事務所
		Account iaOffice = new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc2.Id, OwnerId = mr.Id);
		insert iaOffice;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_CalendarController con = new ASS_CalendarController();
			con.paramMrId = mr.Id;
			con.paramUnitId = unit.Id;

			//Dayクラス
			ASS_CalendarController.Day dayCon = new ASS_CalendarController.Day(System.today(), System.today().Month());
			
//===============================テスト終了===============================
			Test.stopTest();
			system.debug('test:'+dayCon.getDateCalendarLink());
			String resultURL = '/00U/c?md0=' + String.valueOf(System.today().year()) + '&md3=' + String.valueOf( Date.newInstance(System.today().year(),1,1).daysBetween(System.today())+1 )+'&cal_lkid='+ mr.Id;
			System.assertEquals(dayCon.getDateCalendarLink()+con.mrId,resultURL);
		}
	}
}