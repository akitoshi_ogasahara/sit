/**
 * E_SoqlManager テストメソッド
 */
@isTest
private class TestE_SoqlManager {
	
	/**
	*全体テスト
	*/
	static testMethod void allUnitTest() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		
		manager.addString('name = 名前');
		manager.addAndWhere('name',null, manager.eq);
		manager.addOrWhere('Num__c', 10, manager.eq);
		manager.addAndParenthesesAndWhere('name', '名前', manager.eq, manager.pStart);
		manager.addAndParenthesesOrWhere('Num__c',null, manager.eq, manager.pEnd);
		List<String> lstStr =  new List<String> {'田中', '山田'};
		manager.addOrParenthesesAndWhere('name', lstStr, manager.eq, manager.pEnd);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@str= '+ whereList);
//		String Str = 'Where name = ' + '\'' + '名前' + '\'' ;
//		system.assertEquals(Str, whereList);
		
	}
	
	/**
	* テストメソッド：addAndWhere();
	*/
	//条件：whereList.size() == 1 && whereList.get(0).equals(pStart)
	static testMethod void TestaddAndWhere_pSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
//		manager.addAndParenthesesOrWhere('name','名前', manager.co, manager.pStart);
		manager.addString('(');
		manager.addAndWhere('name','名前', manager.ne);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@str1-1= '+ whereList);
		String Str = 'Where ( name <> \'名前\'' ;
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：whereList.size()>0 Or !(whereList.get(0).equals(pStart))
	static testMethod void TestaddAndWhere_npSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndWhere('name','渡辺', manager.ne);
		manager.addAndWhere('name','山田', manager.ne);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@str1-2= '+ whereList);
		String Str = 'Where name <> \'渡辺\'  and name <> \'山田\'';
		system.assertEquals(Str, whereList);
		
	}
	
	//テストaddAndWhere()，whereList.size()<=0 && Object valueがString の場合；
	static testMethod void TestaddAndWhere_Seq() {

		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndWhere('name','名前', manager.eq);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@str1-3= '+ whereList);
		String Str = 'Where name = \'名前\'' ;
		
		system.assertEquals(Str, whereList);
		
	}
	
	//テストaddAndWhere()，whereList.size()>0 && Object valueがinteger の場合；
	static testMethod void TestaddAndWhere_Ieq() {

		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addString('name = 名前');
		manager.addAndWhere('Num__c',10, manager.eq);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@str1-4= '+ whereList);
		String Str = 'Where name = 名前  and Num__c = 10' ; 
		
		system.assertEquals(Str, whereList);
		
	}
	
	//テストaddAndWhere()，whereList.size()<=0 && Object valueがList<string> の場合 ；
	static testMethod void TestaddAndWhere_Sleq() {

		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		List<String> lstStr =  new List<String> {'田中', '山田', '中村'};
		manager.addAndWhere('Name',lstStr, manager.eq);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@str1-5= '+ whereList);
		String Str = 'Where Name = (\'田中\',\'山田\',\'中村\')';
		
		system.assertEquals(Str, whereList);
		
	}
	
	//テストaddAndWhere()，whereList.size()>0 && Object valueがnull の場合 ；
	static testMethod void TestaddAndWhere_neq() {

		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndWhere('Name','',manager.eq);
		manager.addAndWhere('Num__c',null, manager.eq);
		String whereList = manager.getWhere();
		manager.addString(whereList);
		
		Test.stopTest();
		
		system.debug('@@@str1-6= '+ whereList);
		String Str = 'Where Name = \'\'  and Num__c = ';// Stringが空白の場合はシングルクォーテーションが必要
		
		system.assertEquals(Str, whereList);
		
	}
	
	/**
	* テストメソッド：addOrWhere();
	*/
	//条件：whereList.size() == 1 && whereList.get(0).equals(pStart)
	static testMethod void TestaddOrWhere_pSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
//		date Dt = date.today();
		manager.addString('(');
		manager.addOrWhere('StartDate__c',null, manager.lt);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str2-1= '+ whereList);
		String Str = 'Where ( StartDate__c < '; 
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：whereList.size() >0  Or !(whereList.get(0).equals(pStart))
	static testMethod void TestaddOrWhere_npSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
//		date Dt = date.today();
		manager.addOrWhere('amount__c','', manager.gt);
		manager.addOrWhere('StartDate__c','2015/1/1', manager.gt);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('Str2-2= '+ whereList);
		String Str = 'Where amount__c > \'\'  or StartDate__c > \'2015/1/1\'' ; 
		system.assertEquals(Str, whereList);
		
	}
	
	
	/**
	* テストメソッド：addAndParenthesesAndWhere();
	*/
	//条件： parenthesis.equals(pStart),whereList.size()<=0
	static testMethod void TestaddAndParenthesesAndWhere_pSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesAndWhere('Num__c',20, manager.le,manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str3-1= '+ whereList);
		String Str = 'Where (Num__c <= 20 and ';
		system.assertEquals(Str, whereList);
		
	}
	
	//条件： !(parenthesis.equals(pStart)),
	static testMethod void TestaddAndParenthesesAndWhere_pEn() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesAndWhere('Ratio__c',1, manager.ge,manager.pEnd);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str3-2= '+ whereList);
		String Str = 'Where Ratio__c >= 1)';
		system.assertEquals(Str, whereList);
		
	}
	
	//条件： parenthesis.equals(pStart) && whereList.size() == 1 && whereList.get(0).equals(pStart)
	static testMethod void TestaddAndParenthesesAndWhere_ge_pSt_st() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addString('(');
		manager.addAndParenthesesAndWhere('Ratio__c',null, manager.ge,manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str3-3= '+ whereList);
		String Str = 'Where ( (Ratio__c >=  and ' ;
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pStart) && whereList.size()>0,&& !(whereList.get(0).equals(pStart))
	static testMethod void TestaddAndParenthesesAndWhere_ge_npSt_st() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addOrWhere('amount__c',1000, manager.gt);
		manager.addAndParenthesesAndWhere('Ratio__c',1, manager.ge,manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str3-4= '+ whereList);
		 
		String Str = 'Where amount__c > 1000  and (Ratio__c >= 1 and ' ;
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pStart) && whereList.size()>0,&& whereList.get(0).equals(pStart)
	static testMethod void TestaddAndParenthesesAndWhere_pSt_pSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesAndWhere('Ratio__c',1, manager.ge,manager.pStart);
		List<String> lstStr =  new List<String> {'田中', '山田'};
		manager.addAndParenthesesAndWhere('name',lstStr, manager.eq,manager.pStart);
		
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str3-5= '+ whereList);
		String Str = 'Where (Ratio__c >= 1 and   and (name = (\'田中\',\'山田\') and '; 
		
	}
	
	static testMethod void TestaddAndParenthesesAndWhere_pSt_pEn() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		
		manager.addAndParenthesesAndWhere('Num__c',20, manager.le,manager.pStart);
		manager.addAndParenthesesAndWhere('Ratio__c',1, manager.ge,manager.pEnd);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str3-6= '+ whereList);
		String Str = 'Where (Num__c <= 20 and  Ratio__c >= 1)';
		system.assertEquals(Str, whereList);
		
	}
	
	/**
	* テストメソッド：addAndParenthesesOrWhere();
	*/
	//条件：parenthesis.equals(pStart) && whereList.size()<=0
	static testMethod void TestaddAndParenthesesOrWhere_npSt_St() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesOrWhere('name','名前', manager.co, manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str4-1= '+ whereList);
		String Str = 'Where (name like \'名前\' or ' ;
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pEnd) && whereList.size()<=0
	static testMethod void TestaddAndParenthesesOrWhere_npSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesOrWhere('Name','名前', manager.nc,manager.pEnd);
		String whereList = manager.getWhere();
		
		Test.stopTest();		
		
		system.debug('@@@Str4-2= '+ whereList);
		String Str = 'Where not Name like \'名前\')';
		
		system.assertEquals(Str, whereList);
		
	}
	
	static testMethod void TestaddAndParenthesesOrWhere_St_En() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesAndWhere('Name','渡辺', manager.nc,manager.pStart);
		manager.addAndParenthesesOrWhere('Name','田中', manager.nc,manager.pEnd);
		String whereList = manager.getWhere();
		
		Test.stopTest();		
		
		system.debug('@@@Str4-3= '+ whereList);
		String Str = 'Where (not Name like \'渡辺\' and  not Name like \'田中\')';
		
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pStart) && whereList.size() == 1 && whereList.get(0).equals(pStart)
	static testMethod void TestaddAndParenthesesOrWhere_pSt_St() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addString('(');
		manager.addAndParenthesesOrWhere('name','名前', manager.co, manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str4-4= '+ whereList);
		String Str = 'Where ( (name like \'名前\' or ' ;
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pStart) && whereList.size() > 1 && !(whereList.get(0).equals(pStart))
	static testMethod void TestaddAndParenthesesOrWhere_St_pSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addOrWhere('amount__c',1000, manager.gt);
		manager.addAndParenthesesOrWhere('name','名前', manager.co, manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str4-5= '+ whereList);
		
		String Str = 'Where amount__c > 1000  and (name like \'名前\' or ' ;
		system.assertEquals(Str, whereList);
		
	}
	
	
	/**
	* テストメソッド：addOrParenthesesAndWhere();
	*/
	//条件：parenthesis.equals(pStart) && whereList.size()<=0
	static testMethod void TestaddOrParenthesesAndWhere_pSt() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		list<String> AccIdList = new list<String>{'123456','7890123'};
		manager.addOrParenthesesAndWhere('AccountId',AccIdList, manager.inn, manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str5-1= '+ whereList);
		String Str = 'Where (AccountId in (\'123456\',\'7890123\') and ';
		system.assertEquals(Str, whereList);
		
	}
	//条件：!(parenthesis.equals(pStart))
	static testMethod void TestaddOrParenthesesAndWhere_pEn() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		list<String> AccIdList = new list<String>{'123456','7890123'};
		manager.addOrParenthesesAndWhere('AccountId',AccIdList, manager.nnn, manager.pEnd);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str5-2= '+ whereList);
		String Str = 'Where AccountId not in (\'123456\',\'7890123\'))';
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pStart) && whereList.size() == 1 && whereList.get(0).equals(pStart)
	static testMethod void TestaddOrParenthesesAndWhere_pSt_St() {

		Test.startTest();
		
		E_SoqlManager manager = new E_SoqlManager();
		manager.addString('(');
		list<String> AccIdList = new list<String>{'123456','7890123'};
		manager.addOrParenthesesAndWhere('AccountId',AccIdList, manager.nnn, manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str5-3= '+ whereList);
		String Str = 'Where ( (AccountId not in (\'123456\',\'7890123\') and ';
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：parenthesis.equals(pStart) && whereList.size() > 1 or !(whereList.get(0).equals(pStart))
	static testMethod void TestaddOrParenthesesAndWhere_pSt_pSt() {

		Test.startTest();
		
		E_SoqlManager manager = new E_SoqlManager();
		manager.addString('(');
		manager.addOrWhere('amount__c',1000, manager.gt);
		list<String> AccIdList = new list<String>{'123456','7890123'};
		manager.addOrParenthesesAndWhere('AccountId',AccIdList, manager.nnn, manager.pStart);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		system.debug('@@@Str5-4= '+ whereList);
		String Str = 'Where ( amount__c > 1000  or (AccountId not in (\'123456\',\'7890123\') and ';
		system.assertEquals(Str, whereList);
		
	}
	
	//条件：
	static testMethod void TestaddAndWhere_pSt_and() {
		
		Test.startTest();

		E_SoqlManager manager = new E_SoqlManager();
		manager.addAndParenthesesOrWhere('name','渡辺', manager.co, manager.pStart);
		manager.addAndWhere('name','田中', manager.ne);
		String whereList = manager.getWhere();
		
		Test.stopTest();
		
		system.debug('@@@strst= '+ whereList);
		String Str = 'Where (name like \'渡辺\' or   and name <> \'田中\'';
		system.assertEquals(Str, whereList);
		
	}
	
	
}