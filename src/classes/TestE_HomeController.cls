@isTest(SeeAllData=false)
private class TestE_HomeController {
	private static string key = 'home';
	private static User testuser;
	private static E_IDCPF__c idcpf;
	private static Account account;
	private static Contact contact;
	private static E_MenuKind__c menuKind;
	private static E_MenuMaster__c menuMasterParent;
	private static E_MenuMaster__c menuMasterChild;
	private static E_SelectedMenu__c selectedMenu;

	static void init(){
		insertE_bizDataSyncLog();
		system.debug('this is init');
	}

	//IDリクエストのリクエスト種別がEで作成から7日以内
	private static testMethod void standardTest1(){
		init();
		createStandardData();
		createSevenDays();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
			system.assertEquals(controller.getIsEmployee(), TRUE);
		}
	}

	//IDリクエストのリクエスト種別がE,変更受付日時が入力されている
	private static testMethod void standardTest2(){
		init();
		createStandardData();
		createZUPDFLAG_E();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//ユーザ名とメールアドレスが同じ
	private static testMethod void standardTest3(){
		init();
		createStandardData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//IDリクエストのリクエスト種別がI
	private static testMethod void standardTest4(){
		init();
		createDifferentData();
		createZUPDFLAG_I();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//IDリクエストのリクエスト種別がEで作成から7日以内
	//E_MenuKind__c.ExternalId__c取得
	private static testMethod void standardTest5(){
		init();
		createStandardData();
		createSevenDays();
		insertMenuData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//ユーザ名とメールアドレスが異なる場合
	private static testMethod void errorTest1(){
		init();
		createDifferentData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:IDリクエストのリクエスト種別がEで作成から7日以内
	private static testMethod void partnerTest1(){
		init();
		createUserData();
		createSevenDays();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:ユーザ名とメールアドレスが同じ
	private static testMethod void partnerTest2(){
		init();
		createUserData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:ID管理.パスワードステータス='6'の場合
	private static testMethod void partnerTest3(){
		init();
		createUserData();
		updateGWLoginUserData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			pref.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:ユーザ名とメールアドレスが同じ
	//IRIS利用
	private static testMethod void partnerTest4(){
		init();
		createUserData();
		updateIDCPFAppMode();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:ユーザ名とメールアドレスが同じ
	//ブラウザ判定=false
	private static testMethod void partnerTest5(){
		init();
		createUserData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			pref.getHeaders().put('User-Agent', 'test');
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:ユーザ名とメールアドレスが同じ
	//User.Contact.E_MenuKindId__c != null
	private static testMethod void partnerTest6(){
		init();
		createUserData();
		updateAccountData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//代理店ユーザ:ユーザ名とメールアドレスが異なる場合
	private static testMethod void errorPartnerTest1(){
		init();
		createUserData();
		updateDifferentUserData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//コンプライアンス画面を確認したことをクッキーに保存
			E_CookieHandler.setCookieCompliance();
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	private static testMethod void sumiseiTest1(){
		init();
		createSumiseiUserData();
		System.runAs(testuser){
			PageReference pref = Page.E_Home;
			test.setCurrentPage(pref);
			E_HomeController controller = new E_HomeController();
			//pref.getParameters().put('menuid', key);
			controller.init();
			string selectkey = controller.menu.SelectedMenuKey__c;
			system.assertEquals(key, selectKey);
		}
	}

	//基本データ
	static void createStandardData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			//E_MenuMaster__c insEMenu = TestE_TestUtil.createMenuMaster(true, 'test', key, 'E_TopMenu', true);
			E_MenuMaster__c insEMenu = new E_MenuMaster__c(name = 'test',MenuMasterKey__c = key,SelectedMenuKey__c = key ,IsHideMenu__c =false,IsShowCloseBtn__c = true );
			insert insEMenu;

			// Account
			account = new Account(Name = 'testAccount');
			insert account;

			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id,E_CL3PF_AGNTNUM__c = 'Z1111');
			insert contact;

			// User ProfileName = 'E_PartnerCommunity'
			profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.SYSTEM_ADMINISTRATOR Limit 1];

			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				//, ContactId = contact.Id
			);
			insert testuser;

			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add(E_Const.PERM_BASE);//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
			);
			insert idcpf;


		}
	}

	//IDリクエストのリクエスト種別がI
	static void createZUPDFLAG_I(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_IDRequest__c eidr = new E_IDRequest__c(
				E_IDCPF__c = idcpf.ID				//ID管理
				,ZUPDFLAG__c = E_Const.ZUPDFLAG_I	//リクエスト種別
				,ZWEBID__c = idcpf.ZWEBID__c			//ID番号
				,ZIDTYPE__c = idcpf.ZIDTYPE__c		//ID種別
				,ZIDOWNER__c = idcpf.ZIDOWNER__c		//所有者コード
				,ZINQUIRR__c = idcpf.ZINQUIRR__c		//照会者コード
			);
			insert eidr;
		}
	}

	//IDリクエストのリクエスト種別がE,変更受付日時が入力されている
	static void createZUPDFLAG_E(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_IDRequest__c eidr = new E_IDRequest__c(
				E_IDCPF__c = idcpf.ID				//ID管理
				,ZUPDFLAG__c = E_Const.ZUPDFLAG_E	//リクエスト種別
				,ZWEBID__c = idcpf.ZWEBID__c			//ID番号
				,ZIDTYPE__c = idcpf.ZIDTYPE__c		//ID種別
				,ZIDOWNER__c = idcpf.ZIDOWNER__c		//所有者コード
				,ZINQUIRR__c = idcpf.ZINQUIRR__c		//照会者コード
				,dataSyncDate__c = System.now()
			);
			insert eidr;
		}
	}

	//IDリクエストのリクエスト種別がEで作成から7日以内
	static void createSevenDays(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_IDRequest__c eidr = new E_IDRequest__c(
				E_IDCPF__c = idcpf.ID				//ID管理
				,ZUPDFLAG__c = E_Const.ZUPDFLAG_E	//リクエスト種別
				,ZWEBID__c = idcpf.ZWEBID__c			//ID番号
				,ZIDTYPE__c = idcpf.ZIDTYPE__c		//ID種別
				,ZIDOWNER__c = idcpf.ZIDOWNER__c		//所有者コード
				,ZINQUIRR__c = idcpf.ZINQUIRR__c		//照会者コード
			);
			insert eidr;
		}
	}

	//ユーザ名とメールアドレスが違う場合
	static void createDifferentData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			//E_MenuMaster__c insEMenu = TestE_TestUtil.createMenuMaster(true, 'test', key, 'E_TopMenu', true);
			E_MenuMaster__c insEMenu = new E_MenuMaster__c(name = 'test',MenuMasterKey__c = key,SelectedMenuKey__c = key ,IsHideMenu__c =false,IsShowCloseBtn__c = true );
			insert insEMenu;

			// Account
			account = new Account(Name = 'testAccount');
			insert account;

			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id,E_CL3PF_AGNTNUM__c = 'Z1111');
			insert contact;

			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.SYSTEM_ADMINISTRATOR Limit 1];
			//profile profile = [Select Id, Name,usertype From Profile Where Name = 'E_PartnerCommunity' Limit 1];
			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@test.com.nnlife'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				//, ContactId = contact.Id
			);
			insert testuser;


			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
			);
			insert idcpf;


		}
	}

	private static void insertE_bizDataSyncLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
			insert log;
		}
	}

	//standardTest用 各種Menuを作成
	static void insertMenuData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '一般代理店（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'C_Home');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;
		}
	}

	//代理店ユーザ 基本データ
	static void createUserData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			//E_MenuMaster__c insEMenu = TestE_TestUtil.createMenuMaster(true, 'test', key, 'E_TopMenu', true);
//			E_MenuMaster__c insEMenu = new E_MenuMaster__c(name = 'test',MenuMasterKey__c = key,SelectedMenuKey__c = key ,IsHideMenu__c =false,IsShowCloseBtn__c = true );
//			insert insEMenu;

			// Account
			account = new Account(Name = 'testAccount');
			insert account;

			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id, E_CL3PF_AGNTNUM__c = 'Z1111', E_CL3PF_VALIDFLAG__c = '1');
			insert contact;

			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.PROFILE_E_PARTNERCOMMUNITY Limit 1];
			//profile profile = [Select Id, Name,usertype From Profile Where Name = 'E_PartnerCommunity' Limit 1];
			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				, ContactId = contact.Id
			);
			insert testuser;

			//レコードの共有
			TestI_TestUtil.createAccountShare( account.Id, testuser.Id );

			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add(E_Const.PERM_BASE);//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE								//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,OwnerId = testuser.Id									//所有者ID
				,ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS				//利用システムフラグ
			);
			insert idcpf;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '一般代理店（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'C_Home');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
//				, IsRequiredAuthCustomerSearch__c = false
//				, IsRequiredAuthInvestment__c = false
//				, IsRequiredAuthDownload__c = false
//				, IsRequiredAuthCustomerInfo__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;

		}
	}

	//UserNameとメールアドレスが異なるユーザ
	static void updateDifferentUserData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			testuser.Email = 'fstest001@terrasky.ingtesting';
			update testuser;
		}
	}

	//GWログインユーザ
	static void updateGWLoginUserData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			//idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_CGW;		//パスワードステータス
			idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;		//パスワードステータス
			idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_BANK;		//利用システムフラグ
			update idcpf;

			menuKind.ExternalId__c = E_Const.MK_EXID_BANK_AGENCY;
			update menuKind;

			menuMasterChild.MenuMasterKey__c = 'home'+ menuKind.ExternalId__c;
			update menuMasterChild;

			selectedMenu.ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c;
			update selectedMenu;
		}
	}

	//AppMode = IRISに設定
	static void updateIDCPFAppMode(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			idcpf.appMode__c = I_Const.APP_MODE_IRIS;
			update idcpf;
		}
	}

	//親取引先を設定
	static void updateAccountData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			account.E_MenuKind__c = menuKind.Id;
			update account;
		}
	}


	//代理店ユーザ 基本データ
	static void createSumiseiUserData(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){

			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			Contact con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			testuser = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);

			//レコードの共有
			TestI_TestUtil.createAccountShare( childAcc.Id, testuser.Id );

			TestI_TestUtil.createIDCPF(true,testuser.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,testuser.Id);

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '一般代理店（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'C_Home');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
//				, IsRequiredAuthCustomerSearch__c = false
//				, IsRequiredAuthInvestment__c = false
//				, IsRequiredAuthDownload__c = false
//				, IsRequiredAuthCustomerInfo__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;

		}
	}
}