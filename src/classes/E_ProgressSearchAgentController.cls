global with sharing class E_ProgressSearchAgentController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_CPCPF__c record{get;set;}	
			
	public E_ProgressSearchAgentExtender getExtender() {return (E_ProgressSearchAgentExtender)extender;}
	
		public progressTable progressTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c iAgent_val {get;set;}	
		public SkyEditor2.TextHolder iAgent_op{get;set;}	
			
	public String recordTypeRecordsJSON_E_CPCPF_c {get; private set;}
	public String defaultRecordTypeId_E_CPCPF_c {get; private set;}
	public String metadataJSON_E_CPCPF_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public E_ProgressSearchAgentController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_CPCPF__c.fields.AgentName__c;
		f = E_CPCPF__c.fields.PROPNUM__c;
		f = E_CPCPF__c.fields.ContractorName__c;
		f = E_CPCPF__c.fields.CHDRNUMOrGRUPNUM__c;
		f = E_CPCPF__c.fields.ZSTATUS01__c;
		f = E_CPCPF__c.fields.ZSTATUS02__c;
		f = E_CPCPF__c.fields.ZSTATUS03__c;
		f = E_CPCPF__c.fields.ZSTATUS04__c;
		f = E_CPCPF__c.fields.ZSTATUS05__c;
		f = E_CPCPF__c.fields.ZSTATUS06__c;
		f = E_CPCPF__c.fields.ZSTATUS07__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_CPCPF__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				iAgent_val = new SkyEditor2__SkyEditorDummy__c();	
				iAgent_op = new SkyEditor2.TextHolder('co');	
					
				queryMap.put(	
					'progressTable',	
					new SkyEditor2.Query('E_CPCPF__c')
						.addFieldAsOutput('PROPNUM__c')
						.addFieldAsOutput('AgentName__c')
						.addFieldAsOutput('ContractorName__c')
						.addFieldAsOutput('CHDRNUMOrGRUPNUM__c')
						.addFieldAsOutput('ZSTATUS01__c')
						.addFieldAsOutput('ZSTATUS02__c')
						.addFieldAsOutput('ZSTATUS03__c')
						.addFieldAsOutput('ZSTATUS04__c')
						.addFieldAsOutput('ZSTATUS05__c')
						.addFieldAsOutput('ZSTATUS06__c')
						.addFieldAsOutput('ZSTATUS07__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(iAgent_val, 'SkyEditor2__Text__c', 'AgentName__c', iAgent_op, true, 0, true ))
				);	
					
					progressTable = new progressTable(new List<E_CPCPF__c>(), new List<progressTableItem>(), new List<E_CPCPF__c>(), null);
				 progressTable.setPageItems(new List<progressTableItem>());
				 progressTable.setPageSize(100);
				listItemHolders.put('progressTable', progressTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_CPCPF__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_ProgressSearchAgentExtender(this);
			presetSystemParams();
			extender.init();
			progressTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_E_CPCPF_c_AgentName_c() { 
			return getOperatorOptions('E_CPCPF__c', 'AgentName__c');	
		}	
			
			
	global with sharing class progressTableItem extends SkyEditor2.ListItem {
		public E_CPCPF__c record{get; private set;}
		@TestVisible
		progressTableItem(progressTable holder, E_CPCPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class progressTable extends SkyEditor2.PagingList {
		public List<progressTableItem> items{get; private set;}
		@TestVisible
			progressTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<progressTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new progressTableItem(this, (E_CPCPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<progressTableItem> getViewItems() {            return (List<progressTableItem>) getPageItems();        }
	}

			
	}