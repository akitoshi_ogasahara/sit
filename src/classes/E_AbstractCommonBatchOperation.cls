/**
 * BatchApexビジネスロジックの抽象クラスです
 */
public abstract with sharing class E_AbstractCommonBatchOperation {

    //処理対象データ
    protected List<SObject> sObjects;
    //主Id
    protected Id id;
	//公開グループ取得(BRxx)
	protected Map<Id,Group> zbusbrIdMap;
	//key:母店支社コード, value:Group.Id
	protected Map<String, Group> zbusbrCdMap;
	//実行ログ
	private String executelog = '';
    
    /**
     * 初期処理です
     * @param sObjects
     * @return
     */
    public Boolean init(List<SObject> sObjects) {
        this.sObjects = sObjects;
        try {
            createData();
        } catch(Exception e) {
        	setLogger(e.getMessage());
        	setLogger(e.getStackTraceString()); 
            throw e;
        }
        return true;
    }
    
    /**
     * 主IDをセットします
     * @param id
     * @return
     */
    public Boolean execute() {
        try {
            executeData();
        } catch (DMLException e) {
        	setLogger(e.getMessage());
        	setLogger(e.getStackTraceString());
            throw e;
        }
        return true;
    }
    
    /**
     * 主IDをセットします
     * @param id
     * @return
     */
    public void setId(Id id) {
        this.id = id;
    }
    
    /**
     * 母店支社 key : Groupid, value : Groupをセットします。
     */
	public void setZbusbrIdMap(Map<Id,Group> zbusbrIdMap) {
		this.zbusbrIdMap = zbusbrIdMap;
	}
	
    /**
     * 母店支社 key : 母店支社コード, value : Groupをセットします。
     */
	public void setZbusbrCdMap(Map<String,Group> zbusbrCdMap) {
		this.zbusbrCdMap = zbusbrCdMap;
	}
	
    /**
     * 実行ログをセットします。
     */
	public void setLogger(String addlog) {
		if (executelog.length() > 0) {
			executelog += ',';
		} 
		executelog += addlog;
	}
	
    /**
     * 実行ログを取得します。
     */
	public String getLogger() {
		return executelog;
	}
	
    /**
     * データ作成処理の抽象メソッドです
     * @param 
     * @return Boolean
     */
    abstract void createData();
    
    /**
     * データ登録/更新処理の抽象メソッドです
     * @param 
     * @return Boolean
     */
    abstract void executeData();	
}