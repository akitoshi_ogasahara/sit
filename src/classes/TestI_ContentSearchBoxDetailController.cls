@isTest
private class TestI_ContentSearchBoxDetailController {
	
	private static testMethod void test() {
		
		Test.startTest();
		I_ContentSearchBoxDetailController ctrl = new I_ContentSearchBoxDetailController();
		System.assertEquals(I_Const.LIST_MAX_ROWS, ctrl.getListMaxRows());
		System.assertEquals(null, ctrl.getContentRows());
		List<I_ContentSearchResult> rows = new List<I_ContentSearchResult>();
		ctrl.setContentRows(rows);
		System.assertEquals(rows, ctrl.getSortedRows());

		Integer rowCount = I_ContentSearchBoxDetailController.DEFAULT_DISPLAY_ROW_COUNT;
		System.assertEquals(rowCount, ctrl.rowCount);
		ctrl.addRows();
		rowCount = rowCount + I_ContentSearchBoxDetailController.ADDITIONAL_ROW_COUNT;
		System.assertEquals(rowCount, ctrl.rowCount);

		Integer loopCount = (I_Const.LIST_MAX_ROWS - rowCount / I_ContentSearchBoxDetailController.ADDITIONAL_ROW_COUNT) + 1;
		for (Integer i = 0; i < loopCount; i++) {
			ctrl.addRows();
		}
		System.assertEquals(I_Const.LIST_MAX_ROWS, ctrl.rowCount);
		
		// ソート
		PageReference ref = Page.IRIS_Top;
		Map<String, String> params = ref.getParameters();
		params.put('st', I_ContentSearchResult.SORT_TYPE_PAGE_NAME);
		// 降順ソート
		Test.setCurrentPageReference(ref);
		ctrl.sortRows();

		// 対象を変更し、昇順ソート
		params.put('st', I_ContentSearchResult.SORT_TYPE_PAGE_TYPE);
		ctrl.sortRows();

		Test.stopTest();
	}
}