public with sharing class E_DiseaseNumberDao{
	/*
	* 傷病Noより傷病Noマスタを取得
	* @param category 傷病Noが検索条件(Where句)
	*/
	public static E_DiseaseNumber__c getDiseaseNumber(String no){
		List<E_DiseaseNumber__c> diseaseNumberList = new List<E_DiseaseNumber__c>();
		diseaseNumberList = [SELECT Name,
								Category__c,
								Document__c,
								DocumentSumisei__c,
								//DocumentDisplay__c,
								Situation__c,
								Tips__c,
								Information__c,
								InformationSumisei__c,
								//InformationDisplay__c,
								Extent__c,
								Symptoms__c,
								Period__c,
								Notice__c,
								NoticeSumisei__c,
								//NoticeDisplay__c,
								(SELECT DiseaseNumber__r.Name,
											InsuranceType__c,
											//InsuranceTypeDisplay__c,
											InsuranceTypeSumisei__c,
											TreatmentStatus__c,
											Judgement__c,
											//JudgementDisplay__c,
											JudgementSumisei__c,
											Proviso__c
									FROM InsTypeUWs__r
									ORDER BY TreatmentStatusOrder__c),
								(SELECT Name ,Katakana__c FROM Diseases__r ORDER BY Katakana__c )
						FROM E_DiseaseNumber__c
						WHERE Name =: no
						];
		if(!diseaseNumberList.isEmpty()){
			return diseaseNumberList[0];
		}
		return new E_DiseaseNumber__c();
	}

	/*
	* 傷病Noより傷病Noマスタを取得するマップ作製
	*/
	public static Map<String, E_DiseaseNumber__c> getRecMapByNames(Set<String> no){
		Map<String, E_DiseaseNumber__c> diseaseMap = new Map<String, E_DiseaseNumber__c>();

		List<E_DiseaseNumber__c> diseaseList =[
			SELECT Id,
					Name,
					Category__c
			FROM E_DiseaseNumber__c
			WHERE Name IN :no];
		for(E_DiseaseNumber__c d : diseaseList){
			diseaseMap.put(d.Name, d);
		}
		return diseaseMap;
	}
	/**
	 * 傷病Noよりレコードのリストを取得
	 */
	public static List<E_DiseaseNumber__c> getRecsByNames(Set<String> no){

		return [SELECT Id,
					Name,
					Category__c,
					(SELECT Name ,Katakana__c FROM Diseases__r ORDER BY Katakana__c )
			FROM E_DiseaseNumber__c
			WHERE Name IN :no];
	}
}