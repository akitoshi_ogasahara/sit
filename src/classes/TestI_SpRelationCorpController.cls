@isTest
private class TestI_SpRelationCorpController {

	private static String CODE_DISTRIBUTE = '10001';    // 代理店格コード
    private static String CODE_OFFICE = '10002';      // 事務所コード
    private static String CODE_BRANCH = '88';        // 支社コード


	/*
	* データ取得件数が0件の時
	*/
	@isTest static void testmethod_001() {
		CreateData( 0 );

			Test.startTest();

			I_SpRelationCorpController controller = new I_SpRelationCorpController();
			controller.pageAction();

			Test.stopTest();

		//assertion
		System.assertEquals( true, controller.sortIsAsc );
		System.assertEquals( 'corpCode', controller.sortType );
		System.assertEquals(I_Const.LIST_DEFAULT_ROWS, controller.rowCount  );
	}
	/*
	* データ取得件数が1000件の時
	*/
	@isTest static void testmethod_002() {
		// 実行ユーザ作成
	    User user = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
	    E_IDCPF__c idcpf = TestI_TestUtil.createIDCPF( true, user.Id, '');
		//テストデータは1000件作成
		CreateData( 1000 );

		//ページの設定：パラメータはagencyIdとデフォルト行数
		Account agency = [select Id from Account where ParentId = null Limit 1];
		PageReference pageRef = Page.IRIS_SpRelationCorp;
		pageRef.getParameters().put( I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '10000');
		pageRef.getParameters().put( 'agency' , agency.Id);
        Test.setCurrentPage(pageRef);

		system.runAs(User){
			Test.startTest();

			I_SpRelationCorpController controller = new I_SpRelationCorpController();
			controller.pageAction();
			controller.sortRows();
			controller.sortRows();

			pageRef.getParameters().put('st', 'corpName');
			controller.sortIsAsc = true;
			controller.sortRows();

			pageRef.getParameters().put('st', 'corpRelation');
			controller.sortIsAsc = true;
			controller.sortRows();

			pageRef.getParameters().put('st', 'corpCode');
			controller.sortIsAsc = true;
			controller.sortRows();

			controller.addRows();

			controller.getTotalRows();
			controller.getListMaxRows();
			//controller.ExportCSV();

			Test.stopTest();

			//assertion
			System.assertEquals( 1000, controller.rowCount  );

		}
	}


	/** data */
	private static void CreateData( Integer dataCnt ){
   		Date tday = system.today();
   		//BusinessDateの設定(yyyymmdd形式のString)
		String BusinessDate = String.valueof( tDay.year() );
		if ( tDay.Month() >= 10 ){
			BusinessDate += String.valueof( tDay.Month() );
		}else{
			BusinessDate += '0' + String.valueof( tDay.Month() );
		}
		if ( tDay.day() >= 10 ){
			BusinessDate += String.valueof( tDay.day() );
		}else{
			BusinessDate += '0' + String.valueof( tDay.day() );
		}

		//連携ログ作成
		E_BizDataSyncLog__c  ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = date.valueOf( tday ),BatchEndDate__c = tday );
		insert ebizLog;
		//代理店格
		Account ahAcc = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE );
		insert ahAcc;
		//代理店事務所
		Account office = new Account(Name = 'テスト事務所',ParentId = ahAcc.Id, E_CL2PF_ZAGCYNUM__c = CODE_OFFICE, E_CL2PF_BRANCH__c = CODE_BRANCH  );
		insert office;
		//代理店挙績情報
		E_AgencySalesResults__c asr = new E_AgencySalesResults__c(
									ParentAccount__c = ahAcc.Id,
									Account__c = office.Id,
									BusinessDate__c = BusinessDate ,
									Hierarchy__c = 'AH'
										);
		insert asr;
		//特定関係法人
		List<E_SpRelationCorp__c> SRCList = new List<E_SpRelationCorp__c>();

		for ( Integer i = 0; i < dataCnt ; i++){
			E_SpRelationCorp__c SRC = new E_SpRelationCorp__c ( E_AgencySalesResults__c = asr.Id , BusinessDate__c = BusinessDate  );
			SRCList.add( SRC );
		}
		insert SRCList;

	}

}