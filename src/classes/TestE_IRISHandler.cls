@isTest
private class TestE_IRISHandler {
	static testMethod void getInstanceTest() {
		Test.startTest();
			system.assertEquals(E_IRISHandler.getInstance() == null, false);
		Test.stopTest();
	}

	static testMethod void getIsIRISfalseTest01() {
		Test.startTest();
			system.assertEquals(E_IRISHandler.getInstance().getIsIRIS(), false);
		Test.stopTest();
	}

	static testMethod void getIsIRISfalseTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c userIdcpf = new E_IDCPF__c(
					 User__c = actUser.Id
					,OwnerId = actUser.Id
					,ZSTATUS01__c = '1'
					,ZINQUIRR__c = 'L167890'
					,AppMode__c = I_Const.APP_MODE_NNLINK
				);
		insert userIdcpf;

		system.runAs(actUser) {
			ApexPages.currentPage().getHeaders().put('User-Agent', 'mozilla');
			Test.startTest();
				system.assertEquals(E_AccessController.getInstance().idcpf.AppMode__c, I_Const.APP_MODE_NNLINK);
				system.assertEquals(E_IRISHandler.getInstance().getIsIRIS(), false);
			Test.stopTest();
		}
	}

	/** E_IRISHandler L.80~86 12/26リリース時の制限 **/
	/*static testMethod void getCanUseIRISTest() {
		// 実行ユーザ作成
		String userName = 'testUser@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = 'testUser'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		insert actUser;

		E_IDCPF__c userIdcpf = new E_IDCPF__c(
					 User__c = actUser.Id
					,OwnerId = actUser.Id
					,ZSTATUS01__c = '1'
					,ZINQUIRR__c = 'BR**'
					,ZDSPFLAG01__c = '1'
					,AppMode__c = I_Const.APP_MODE_IRIS
				);
		insert userIdcpf;

		system.runAs(actUser) {
			Test.startTest();
				system.assertEquals(E_IRISHandler.getInstance().getIsIRIS(), true);
			Test.stopTest();
		}
	}*/

	//IRIS住生ユーザーで実行
	static testMethod void getCanUseIRISTest01(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);

			// Menu作成.
			E_MenuKind__c menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '住友生命（インターネット）');
			insert menuKind;
			parentAcc.E_MenuKind__c = menuKind.Id;
			update parentAcc;

		}
		System.runAs(uc){
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			E_IRISHandler controller = E_IRISHandler.getInstance();
			System.assertEquals(controller.getCanUseIRIS(),true);
			Test.stopTest();
		}
	}

	//NNLink住生ユーザで実行
	static testMethod void getCanUseIRISTest02(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',false);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);

			// Menu作成.
			E_MenuKind__c menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '住友生命（インターネット）');
			insert menuKind;
			parentAcc.E_MenuKind__c = menuKind.Id;
			update parentAcc;

		}
		System.runAs(uc){
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			E_IRISHandler controller = E_IRISHandler.getInstance();
			System.assertEquals(controller.getCanUseIRIS(),false);
			Test.stopTest();
		}
	}

	static testMethod void IRISRecommendBrowserTureTest() {
		Test.startTest();
			ApexPages.currentPage().getHeaders().put('User-Agent', 'mozilla');
			system.assertEquals(E_IRISHandler.getInstance().IRISRecommendBrowser(), true);
		Test.stopTest();
	}

	static testMethod void IRISRecommendBrowserFalseTest() {
		Test.startTest();
			ApexPages.currentPage().getHeaders().put('User-Agent', 'msie');
			system.assertEquals(E_IRISHandler.getInstance().IRISRecommendBrowser(), false);
		Test.stopTest();
	}

	static testMethod void setPolicyConditionTest() {
		Test.startTest();
			E_IRISHandler.getInstance().setPolicyCondition('test', 'test', 'test', 'test');
			// system.assertEquals(,);
		Test.stopTest();
	}

	static testMethod void IRISTUserTest() {
		// 実行ユーザ作成
		Account testAgencyParent = new Account(Name = 'testAgencyParent');
		insert testAgencyParent;

		Account testAgency = new Account(Name = 'testAgency', ParentId = testAgencyParent.Id);
		insert testAgency;

		Contact testAgent = new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id);
		insert testAgent;
		Contact dept = [SELECT LastName, E_AccParentName__c FROM Contact WHERE Id = :testAgent.Id];

		String userName = 'MR01@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Partner Community' LIMIT 1];
		User actUser = new User(Lastname = 'MR01'
								, Username = userName
								, Email = userName
								, ProfileId = p.Id
								, Alias = 'MR01'
								, TimeZoneSidKey = UserInfo.getTimeZone().getID()
								, LocaleSidKey = UserInfo.getLocale()
								, EmailEncodingKey = 'UTF-8'
								, LanguageLocaleKey = UserInfo.getLanguage()
								, ContactId = testAgent.Id
								);
		insert actUser;

		E_IDCPF__c userIdcpf = new E_IDCPF__c(
					 User__c = actUser.Id
					,OwnerId = actUser.Id
					,ZSTATUS01__c = '1'
					,ZINQUIRR__c = 'L167890'
					,AppMode__c = I_Const.APP_MODE_NNLINK
				);
		insert userIdcpf;

		system.runAs(actUser) {
			Test.startTest();
				// Test - setUseIRIS()
				E_IRISHandler.getInstance().setUseIRIS();
				system.assertEquals(E_AccessController.getInstance().idcpf.AppMode__c, I_Const.APP_MODE_IRIS);

				// Test - getNameOfLoginUser()
				system.assertEquals(E_IRISHandler.getInstance().getNameOfLoginUser(), actUser.LastName);

				// Test - getNameOfDept()
				E_IRISHandler.getInstance().getNameOfDept();
				system.assertEquals(dept.E_AccParentName__c, testAgencyParent.Name);
			Test.stopTest();
		}
	}

	static testMethod void NNLinkUserTest() {
		// 実行ユーザを作成
		Account accParent = TestI_TestUtil.createAccount(true, null);
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		String actUserLastName = 'TestUser001';

		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);

		String userName = actUserLastName + '@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = actUserLastName
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
			, Department = 'testDepartment'
		);
		insert actUser;

		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		system.runAs(actUser) {
			Test.startTest();
				// Test - setUseIRIS()
				E_IRISHandler.getInstance().setUseNNLink();
				system.assertEquals(E_AccessController.getInstance().idcpf.AppMode__c, I_Const.APP_MODE_NNLINK);

				// Test - getNameOfLoginUser()
				system.assertEquals(E_IRISHandler.getInstance().getNameOfLoginUser(), actUserLastName);

				// Test - getNameOfDept()
				system.assertEquals(E_IRISHandler.getInstance().getNameOfDept(), 'testDepartment');
			Test.stopTest();
		}
	}

	static testMethod void policyConditionTest() {
		Test.startTest();
			Account acc = new Account(Name = 'test');
			insert acc;
			E_IRISHandler.getInstance().setPolicyCondition('agency', acc.Name, acc.Id, acc.Name);

			system.assertEquals(E_IRISHandler.getInstance().isSetPolicyCondition(), true);
			system.assertEquals(E_IRISHandler.getInstance().getPolicyConditionValue(I_Const.JSON_KEY_FIELD), 'agency');
		Test.stopTest();
	}

	static testMethod void policyConditionNullTest() {
		Test.startTest();
			system.assertEquals(E_IRISHandler.getInstance().isSetPolicyCondition(), false);
			system.assertEquals(E_IRISHandler.getInstance().getPolicyConditionValue(I_Const.JSON_KEY_FIELD), null);
		Test.stopTest();
	}

	static testMethod void irisMenuTest() {
		I_MenuMaster__c imenu = new I_MenuMaster__c(MenuKey__c = 'testKey');
		Test.startTest();
			// Test - setIrisMenuId
			E_IRISHandler.getInstance().setIrisMenuId(imenu.Id);
			system.assertEquals(E_IRISHandler.getInstance().irisMenus, I_MenuProvider.getInstance());

			// Test - setLinkMenuKey
			E_IRISHandler.getInstance().setLinkMenuKey(imenu.MenuKey__c);
			system.assertEquals(E_IRISHandler.getInstance().irisMenus, I_MenuProvider.getInstance());
		Test.stopTest();
	}

	static testMethod void irisMenuNullTest() {
		Test.startTest();
			// Test - setIrisMenuId
			E_IRISHandler.getInstance().setIrisMenuId(null);
			system.assertEquals(E_IRISHandler.getInstance().irisMenus, I_MenuProvider.getInstance());

			// Test - setLinkMenuKey
			E_IRISHandler.getInstance().setLinkMenuKey(null);
			system.assertEquals(E_IRISHandler.getInstance().irisMenus, I_MenuProvider.getInstance());
		Test.stopTest();
	}
}