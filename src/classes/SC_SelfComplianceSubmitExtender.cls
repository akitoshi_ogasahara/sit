public with sharing class SC_SelfComplianceSubmitExtender extends AMS_AbstractExtender{
    @TestVisible
    private SC_SelfComplianceSubmitController controller;
    @TestVisible
    private String officeId;
    
    public Attachment file{get;set;}
    
    public String disCode {get; set;}
    
    /**
     * Constructor
     */
    public SC_SelfComplianceSubmitExtender(SC_SelfComplianceSubmitController extension){
        super();
        this.controller = extension;
        this.officeId = ApexPages.currentPage().getParameters().get('parentid');
        this.file = new Attachment();
        
        //ヘルプ設定
        helpMenukey = SC_Const.MENU_HELP_SELFCOMPLIANCE;
        // 新規アップロードか規定種別変更かで遷移先を変更する
        if(getIsNew()){
            this.helpPageJumpTo = SC_Const.MENU_HELP_SELFCOMPLIANCE_SUBMIT_NEW; 
        }else{
            this.helpPageJumpTo = SC_Const.MENU_HELP_SELFCOMPLIANCE_SUBMIT_UPD;
        }
        
        // ヘルプページ（各種資料ダウンロード）
        this.guideMenukey = SC_Const.MENU_GUIDE_SELFCOMPLIANCE; 
    }

	/**
	 * init
	 */
    public override void init(){

        if(officeId == null || String.isBlank(officeId)){
            isSuccessOfInitValidate = false;
            pageMessages.addErrorMessage(SC_Const.ERR_MSG01_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            isSuccessOfInitValidate = true;
            
			// 自主点検事務所取得
			SC_Office__c parentOffice = [Select Id, YearRecType__c, YearDisclamer__c From SC_Office__c Where Id = :officeId];
			
			// レコードタイプ設定
			controller.record.recordTypeId = E_RecordTypeDao.getRecIdBySobjectTypeAndDevName(parentOffice.YearRecType__c, 'SC_SelfCompliance__c');
			// ディスクレーマコード設定
			disCode = parentOffice.YearDisclamer__c;
        }  
    }

	/**
	 * 新規アップロード判定
	 */
    public Boolean getIsNew(){
        //Idがない場合は新規とする。
        return controller.record.Id == null;
    }

	/**
	 * 保存ボタン押下時
	 */
    public PageReference doSaveEx(){

        pageMessages.clearMessages();

        if(getIsNew() &&
            (this.file==null||this.file.Body==null||String.isBlank(this.file.Name))
        ){
            pageMessages.addErrorMessage(SC_Const.ERR_MSG03_REQUIRED_FILE);
            return null;
        }
        
        Savepoint sp = Database.setSavepoint();
        Boolean saveResult = false;
        try{
            if(getIsNew()){
                controller.record.SC_Office__c = officeId;
                system.debug('■Save'+controller.record);
                saveResult = E_GenericDao.saveViaPage(controller.record);//insert controller.record;
                system.debug('■Save end');
                if(saveResult){
                    system.debug('■file');
                    this.file.parentId = controller.record.Id;
                    this.file.ContentType = SC_Const.CONTENTTYPE_FOR_DOWNLOAD;		//強制的にファイルダウンロードを実施させる
                    system.debug('■file insert');
                    insert file;
                    system.debug('■file end');
                }
            }else{
                saveResult = E_GenericDao.saveViaPage(controller.record); //update controller.record;
            }
            
            if(saveResult){           	
                return backToSelfCompliancesPage();
            }
            
        }catch(Exception e){
            Database.Rollback(sp);
            pageMessages.addErrorMessage('ファイルアップロードエラー：'+e.getMessage());
        }

        return null;
    }
    
    /**
     * 提出一覧へ戻る
     */
    public PageReference backToSelfCompliancesPage(){
        PageReference pr = Page.E_SCSelfCompliances;
        pr.getParameters().put('id', officeId);
        return pr;
    } 
}