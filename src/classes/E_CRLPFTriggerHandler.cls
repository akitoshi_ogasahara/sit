public with sharing class E_CRLPFTriggerHandler {
	
	private E_CRLPFTriggerBizLogic bizLogic;
	
	//コンストラクタ
	public E_CRLPFTriggerHandler(){
		bizLogic = new E_CRLPFTriggerBizLogic();
	}
	
	//after insert
	public void onAfterInsert(List<E_CRLPF__c> newList){
		bizLogic.updatePolicyContractor(newList);
	}
	
	//after update
	public void onAfterUpdate(List<E_CRLPF__c> newList, Map<Id, E_CRLPF__c> oldMap){
		bizLogic.updatePolicyContractor(newList);
	}
	
}