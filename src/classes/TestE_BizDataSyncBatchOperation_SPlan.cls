@isTest
public with sharing class TestE_BizDataSyncBatchOperation_SPlan {
	private static E_SalesPlan__c sp1;
	private static E_AgencySalesResults__c esr1;
	private static E_AgencySalesResults__c esr2;
	private static E_AgencySalesResults__c esr3;
	private static String BUSINESS_DATE;
	private static String BUSINESS_DATE2;

    /**
     * Database.QueryLocator getQueryLocator()
	 */
	static testMethod void getQueryLocator_test001(){
		/* Test Data */
		createData();
		
		// Test
		Test.startTest();
		
		E_BizDataSyncBatchOperation_SalesPlan operation = new E_BizDataSyncBatchOperation_SalesPlan();
		Database.QueryLocator dq = operation.getQueryLocator();
		
		Test.stopTest();
		
		// assertion
		System.assert(dq.getQuery().startsWith('SELECT Id'));
	}
	
    /**
     * void execute(List<Sobject> records)
	 */
	static testMethod void execute_test001(){
		/* Test Data */
		createData();
		
		List<Sobject> recs = new List<Sobject>();
		recs.add(sp1);

		// Test
		Test.startTest();
		
		E_BizDataSyncBatchOperation_SalesPlan operation = new E_BizDataSyncBatchOperation_SalesPlan();
		operation.execute(recs);
		
		Test.stopTest();
		
		// assertion
		List<E_SalesPlan__c> sps = [select  id,
										  	name,
										  	ParentAccount__c,
										  	SalesResult__c,
										  	SalesResultUpsertKey__c
									from E_SalesPlan__c
									where ParentAccount__c != null];
		System.assertEquals(1,sps.size());
		System.assertNotEquals(esr1.Id,sps[0].SalesResult__c);
		System.assertEquals(esr2.Id,sps[0].SalesResult__c);
//		System.assertEquals(BUSINESS_DATE2,sps[0].SalesResultUpsertKey__c.right(8));
	}

	/** *****************************************************************************************
	 * TestData
	 */
	private static void createData(){
		BUSINESS_DATE = '20000101';
		BUSINESS_DATE2 = '20000102';

		//代理店格
		Account acc1 = new Account(Name = 'test');
		insert acc1;

		//代理店挙積情報
		esr1 = new E_AgencySalesResults__c(
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH',
			ParentAccount__c = acc1.id
		);
		insert esr1;
		esr2= new E_AgencySalesResults__c(
			BusinessDate__c = BUSINESS_DATE2,
			Hierarchy__c = 'AH',
			ParentAccount__c = acc1.id
		);
		insert esr2;
		esr3= new E_AgencySalesResults__c(
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AY'
		);
		insert esr3;

		//営業計画
		sp1 = new E_SalesPlan__c(
			ParentAccount__c = acc1.Id,
			SalesResult__c = esr1.Id
		);
		insert sp1;
		sp1 = [select id,name,ParentAccount__c,SalesResult__c,SalesResultUpsertKey__c from E_SalesPlan__c where id = :sp1.Id];
		E_SalesPlan__c sp2 = new E_SalesPlan__c(
			SalesResult__c = esr3.Id
		);
		insert sp2;

		//EBiz連携ログ
		E_BizDataSyncLog__c log1 = new E_BizDataSyncLog__c(
			kind__c = 'F2',
			InquiryDate__c = date.newInstance(Integer.valueOf(BUSINESS_DATE2.left(4)),
											  Integer.valueOf(BUSINESS_DATE2.mid(4,2)),
											  Integer.valueOf(BUSINESS_DATE2.mid(6,2))
											  )
		);
		insert log1;

	}

}