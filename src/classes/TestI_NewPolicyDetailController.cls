@isTest
private class TestI_NewPolicyDetailController {
	
	/**
	 * pageAction
	 * 正常系
	 * 個人
	 */
	private static testMethod void pageActionTest_001() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c plcy = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		TestUtil_AMS.createNewPolicyDefect(true,plcy.id,'W50','O');

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('id', plcy.id);
		Test.setCurrentPage(pageRef);

		Test.startTest();
		I_NewPolicyDetailController controller = new I_NewPolicyDetailController();
		controller.pageAction();
		Test.stopTest();
		system.assert(!controller.pageMessages.getHasWarningMessages());
	}
	/**
	 * pageAction
	 * 正常系
	 * 団体
	 */
	private static testMethod void pageActionTest_002() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c plcy = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'Y','D001',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		TestUtil_AMS.createNewPolicyDefect(true,plcy.id,'XXX','O');
		TestUtil_AMS.createNewPolicyDefect(true,plcy.id,'XXX','D');
		TestUtil_AMS.createNewPolicyDefect(true,plcy.id,'YYY','O');
		TestUtil_AMS.createNewPolicyDefect(true,plcy.id,'ZZZ','O');
		TestUtil_AMS.createDefectMaster(true,'XXX','テスト不備1','1');
		TestUtil_AMS.createDefectMaster(true,'YYY','テスト不備1','1');

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('id', plcy.id);
		Test.setCurrentPage(pageRef);

		Test.startTest();
		I_NewPolicyDetailController controller = new I_NewPolicyDetailController();
		controller.pageAction();
		Test.stopTest();
		system.assert(!controller.pageMessages.getHasWarningMessages());
	}
	/**
	 * getter
	 * 正常系
	 */
	private static testMethod void getterTest_001() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newPlcy = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);

		Test.startTest();
		I_NewPolicyDetailController controller = new I_NewPolicyDetailController();
		controller.pageAction();
		system.assert(!controller.getIsStatusI());
		system.assert(!controller.getIsStatusZ());
		system.assert(!controller.getIsStatusQ());
		system.assert(!controller.getIsStatusQZ());
		system.assert(!controller.isDispEnd);
		Test.stopTest();
	}
}