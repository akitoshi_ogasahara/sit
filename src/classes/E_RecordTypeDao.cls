public with sharing class E_RecordTypeDao {

	/*
	*保険契約ヘッダ：レコードタイプID取得
	*＠param developername  レコードタイプ名
	*＠return recordTypeId
	*/
	public static  Id  getEpolicyRecordTypeID( String developerName ){
		String devname = developerName;
		return [SELECT Id FROM RecordType WHERE SobjectType='E_Policy__c' AND DeveloperName = :devname LIMIT 1 ].Id;
	}


	/**
	 * sobjectTypeをキーにRecordTypeを取得
	 */
	public static Map<String, Id> getRecordTypeBySobjectType(String sobjectType) {
		List<RecordType> recs = [
				SELECT
					Id,
					DeveloperName
				FROM
					RecordType
				WHERE
					SobjectType =: sobjectType
		];
		Map<String, Id> recordTypeMap = new Map<String, Id>();
		if (!recs.isEmpty()) {
			for (RecordType rec : recs) {
				recordTypeMap.put(rec.DeveloperName, rec.id);
			}
		}
		return recordTypeMap;
	}
	/*
	 * @author Terrasky
	 * @version 20160908追加
	 *
	 * レコードタイプIDを取得
	 * @param developerName レコードタイプ名
	 * @param sobjectType 対象オブジェクト
	 * @return recordTypeId
	 */
	public static ID getRecIdBySobjectTypeAndDevName(String developerName,String sobjectType){
		if(String.isEmpty(developerName) || String.isEmpty(sobjectType)) return null;
		List<RecordType> recs = [select id from RecordType where DeveloperName =: developerName AND SobjectType =: sobjectType];
		return (recs.size() == 1)? recs[0].Id:null;
	}
}