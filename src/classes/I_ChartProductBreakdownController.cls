public with sharing class I_ChartProductBreakdownController {

	public Id paramSalesResultsId {get; set;}				// 代理店挙積情報ID

	transient public String json {get; set;}				// JSONデータ

	transient public String[] paramDisclaimer {get; set;}	// 表示ディスクレーマ
	public Map<String, String> disclaimers{					// 表示ディスクレーマ(表示用)
		get{
			if(disclaimers == null){
				// パラメータよりMapを作成
				while(this.paramDisclaimer.size() < 1) {
					this.paramDisclaimer.add('');
				}
				disclaimers = new Map<String, String>();
				disclaimers.put('AMS|SR|012', this.paramDisclaimer[0]);
			}
			return disclaimers;
		}
		set;
	}

	/**
	 * コンストラクタ
	 */
	public I_ChartProductBreakdownController(){
		this.paramSalesResultsId	= null;											// 代理店挙積情報ID
		this.paramDisclaimer		= new List<String>();							// 表示ディスクレーマ

	    this.json					= '';											// JSONデータ
	}

	/**
	 * JS用値設定の取得(リモートアクション)
	 */
	@readOnly
	@RemoteAction
	public static String getJSParam(Id pSalesResultsId){
		I_ChartProductBreakdownController cls	= new I_ChartProductBreakdownController();
		cls.paramSalesResultsId			= pSalesResultsId;	// 代理店挙積情報ID
		cls.getJSParameter();								// JS用値設定の取得
		return cls.json;									// JSONデータ
	}

	/**
	 * JS用値設定の取得
	 */
	public void getJSParameter(){
		// JSONデータ
		this.json				= getJSON();
	}

	/**
	 * JSONデータの取得
	 */
	public String getJSON(){
		List<String> values			= new List<String>();
		List<String> subfields		= new List<String>();
		List<String> lines			= new List<String>();
		List<String> fields			= new List<String>();
		Integer year				= 0;
		String lineTemp				= '';

		// 商品内訳取得
		List<E_ProductBreakdown__c> productBreakdowns = E_productBreakdownDao.getRecsByAsrId(this.paramSalesResultsId);

		for(E_ProductBreakdown__c productBreakdown : productBreakdowns){

			// 年取得
			year = 0;
			try{
				year = Integer.valueOf(productBreakdown.E_AgencySalesResults__r.YYYYMM__c.left(4));
			}catch(Exception e){ continue; }

			fields			= new List<String>();

			fields.add('"product":"' + I_Util.toStr(productBreakdown.ProductName__c) + '"');	// 商品名　当年

			subfields		= new List<String>();

			values			= new List<String>();
			values.add('"year":' + I_Util.toStr(year) + '');
			values.add('"val":' + I_Util.toStr(productBreakdown.NBCANP__c) + '');			// 新契約係数ANP　当年
			subfields.add('{' + String.join(values,',') + '}');

			values			= new List<String>();
			values.add('"year":' + I_Util.toStr(year - 1) + '');
			values.add('"val":' + I_Util.toStr(productBreakdown.NBCANP_LY__c) + '');		// 新契約係数ANP　前年
			subfields.add('{' + String.join(values,',') + '}');

			values			= new List<String>();
			values.add('"year":' + I_Util.toStr(year - 2) + '');
			values.add('"val":' + I_Util.toStr(productBreakdown.NBCANP_B2Y__c) + '');		// 新契約係数ANP　2年前
			subfields.add('{' + String.join(values,',') + '}');

			fields.add('"vals":[' + String.join(subfields,',') + ']');

			if(productBreakdown.ProductName__c == 'その他'){
				lineTemp = '{' + String.join(fields,',') + '}';
			}else{
				lines.add('{' + String.join(fields,',') + '}');
			}
		}
		if(lineTemp != '') lines.add(lineTemp);

		return String.join(lines,',\n');
	}


}