@isTest
private class TestSC_StatusManager {
	// コンストラクタ
	private static testMethod void StatusManagerTest001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
	    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
		Test.stopTest();
	}

// 次アクション、差戻しアクション定義
	// getAvailableActions:自主点検（参照のみ）権限セット
	private static testMethod void StatusManagerTest002(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'readOnly', 'E_EmployeeStandard');
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（参照のみ）権限セットを付与
			TestSC_TestUtil.createReadOnlyPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), false);
			System.assertEquals(manager.getHasRejectAction(), false);
		}
	}

	// getAvailableActions:1.未提出
	private static testMethod void StatusManagerTest003(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.APPLY));
			System.assertEquals(manager.getHasRejectAction(), false);
		}
	}

	// getAvailableActions:2.担当MR確認中
	private static testMethod void StatusManagerTest004(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_02);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REPORT_MR));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_REPORT_MR));
		}
	}

	// getAvailableActions:3.営業部長確認中
	private static testMethod void StatusManagerTest005(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_03);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REPORT_MGR));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_REPORT_MGR));
		}
	}

	// getAvailableActions:4.本社確認中
	private static testMethod void StatusManagerTest006(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_04);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.APPROVE));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_APPROVE));
		}
	}

	// getAvailableActions:5.完了
	private static testMethod void StatusManagerTest007(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_05);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), false);
//			System.assertEquals(manager.getHasRejectAction(), false);
			/* 完了後の解除アクション追加のためAssertion変更 */
			System.assertEquals(manager.getHasRejectAction(), true);
		}
	}

// 次アクションへ進む
	// doNextAction:1.未提出
	private static testMethod void StatusManagerTest008(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
		    	manager.doNextAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, E_Message.getMsgMap().get('AMS|SCSC|001'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REPORT_MR));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_REPORT_MR));
		}
	}

	// doNextAction:2.担当MR確認中
	private static testMethod void StatusManagerTest009(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_02);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
				String beforeAction = manager.getNextActionLabel();

		    	manager.doNextAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, beforeAction + E_Message.getMsgMap().get('AMS|SCSC|002'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REPORT_MGR));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_REPORT_MGR));
		}
	}

	// doNextAction:3.営業部長確認中
	private static testMethod void StatusManagerTest010(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_03);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
				String beforeAction = manager.getNextActionLabel();

		    	manager.doNextAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, beforeAction + E_Message.getMsgMap().get('AMS|SCSC|002'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.APPROVE));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_APPROVE));
		}
	}

	// doNextAction:4.本社確認中
	private static testMethod void StatusManagerTest011(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_04);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
		    	manager.doNextAction();
			Test.stopTest();

			System.assertEquals(manager.getHasNextAction(), false);
//			System.assertEquals(manager.getHasRejectAction(), false);
			/* 完了後の解除アクション追加のためAssertion変更 */
			System.assertEquals(manager.getHasRejectAction(), true);
		}
	}

// 差戻しアクション
	// doRejectAction:MR -> AG
	private static testMethod void StatusManagerTest012(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_02);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
				String beforeAction = manager.getRejectActionLabel();

		    	manager.doRejectAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, beforeAction + E_Message.getMsgMap().get('AMS|SCSC|002'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.APPLY));
			System.assertEquals(manager.getHasRejectAction(), false);
		}
	}

	// doRejectAction:MGR -> MR
	private static testMethod void StatusManagerTest013(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_03);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
				String beforeAction = manager.getRejectActionLabel();

		    	manager.doRejectAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, beforeAction + E_Message.getMsgMap().get('AMS|SCSC|002'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REPORT_MR));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_REPORT_MR));
		}
	}

	// doRejectAction:CMD -> MR
	private static testMethod void StatusManagerTest014(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_04);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
				String beforeAction = manager.getRejectActionLabel();

		    	manager.doRejectAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, beforeAction + E_Message.getMsgMap().get('AMS|SCSC|002'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REPORT_MR));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_REPORT_MR));
		}
	}
	/* 201705対応 */
	// doRejectAction:完了 -> CMD
	private static testMethod void StatusManagerTest027(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_05);
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());
				String beforeAction = manager.getRejectActionLabel();

		    	manager.doRejectAction();
			Test.stopTest();

			System.assertEquals(manager.msgActionComplete, beforeAction + E_Message.getMsgMap().get('AMS|SCSC|002'));
			System.assertEquals(manager.getHasNextAction(), true);
			System.assertEquals(manager.getNextActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.APPROVE));
			System.assertEquals(manager.getHasRejectAction(), true);
			System.assertEquals(manager.getRejectActionLabel(), SC_StatusManager.scActionLabel.get(SC_StatusManager.ScAction.REJECT_APPROVE));
		}
	}

// 各種権限判定
	// getCanUpload:参照のみ
	private static testMethod void StatusManagerTest015(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'ReadOnly', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（参照のみ）権限セットを付与
			TestSC_TestUtil.createReadOnlyPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanUpload();
			Test.stopTest();

			System.assertEquals(result, false);
		}
	}

	// getCanUpload:1.未提出
	private static testMethod void StatusManagerTest016(){
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01);
		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	       TestSC_TestUtil.createAgencyPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanUpload();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanUpload:2.担当MR確認中
	private static testMethod void StatusManagerTest017(){
		// 実行ユーザ作成（MR）
		User actUser = TestSC_TestUtil.createUser(true, 'MR', 'ＭＲ');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（社員）権限セットを付与
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_02);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanUpload();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanUpload:4.本社確認中
	private static testMethod void StatusManagerTest018(){
		// 実行ユーザ作成（MR）
		User actUser = TestSC_TestUtil.createUser(true, 'CMS', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_04);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanUpload();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanDelete:CMD
	private static testMethod void StatusManagerTest019(){
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_04);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanDelete();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanComment:参照のみ
	private static testMethod void StatusManagerTest020(){
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'ReadOnly', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（参照のみ）権限セットを付与
			TestSC_TestUtil.createReadOnlyPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanComment();
			Test.stopTest();

			System.assertEquals(result, false);
		}
	}

	// getCanComment:2.担当MR確認中
	private static testMethod void StatusManagerTest021(){
		// 実行ユーザ作成（拠点長）
		User actUser = TestSC_TestUtil.createUser(true, 'Manager', '拠点長');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（社員）権限セットを付与
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_02);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanComment();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanComment:CMD
	private static testMethod void StatusManagerTest022(){
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanComment();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanChangeFixed:CMD
	private static testMethod void StatusManagerTest023(){
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanChangeFixed();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanDownload:代理店
	private static testMethod void StatusManagerTest024(){
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01);
		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	       TestSC_TestUtil.createAgencyPermissions(actUser.Id);

			Test.startTest();
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanDownload();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// getCanDownload:
	private static testMethod void StatusManagerTest025(){
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
				Account acc = TestSC_TestUtil.createAccount(true, null);
				SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

				Boolean result = manager.getCanDownload();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}

	// 「それ以外」の通し
	private static testMethod void StatusManagerTest026(){
		Test.startTest();
			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, '99.ダミー');
		    SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

			manager.doRejectAction();
			manager.doNextAction();
		Test.stopTest();
	}
	/* 201705対応 */
	// getCanChangeDefectStatus:CMD
	private static testMethod void StatusManagerTest028(){
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');

		// 社員ユーザで実行
		System.runAs(actUser){
			// 自主点検（CMD）権限セットを付与
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			Test.startTest();
				// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01);
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

			Boolean result = manager.getCanChangeDefectStatus();
			Test.stopTest();

			System.assertEquals(result, true);
		}
	}
	/* 201705対応 */
	// getCanViewDefectStatus:代理店
	private static testMethod void StatusManagerTest029(){
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01);
		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
			TestSC_TestUtil.createAgencyPermissions(actUser.Id);

			Test.startTest();
			// テストデータ作成
		    	SC_StatusManager manager = new SC_StatusManager(record, E_AccessController.getInstance());

			Boolean result = manager.getCanViewDefectStatus();
			Test.stopTest();

			System.assertEquals(result, false);
		}
	}
	
}