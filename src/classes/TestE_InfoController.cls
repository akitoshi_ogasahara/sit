@isTest
private class TestE_InfoController {
	
    /**
     * 最低限のレコードで実施
     */
    private static testMethod void testSpva1() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            E_Policy__c policy = new E_Policy__c();
            PageReference pref = Page.E_Info;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);
            E_InfoController controller = new E_InfoController();
            controller.init();

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);

    }

    /**
     * メニューを作成し実施
     */
    private static testMethod void testSpva2() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            E_Policy__c policy = new E_Policy__c();
            PageReference pref = Page.E_Info;
			// メニュー情報の登録
			E_MenuMaster__c menu = new E_MenuMaster__c();
			menu.name = 'テストメニュー1';
			menu.MenuMasterKey__c = 'test';
			insert menu;
			pref.getParameters().put('menuid', menu.Id);

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);
            E_InfoController controller = new E_InfoController();
            controller.init();

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);

    }
	
}