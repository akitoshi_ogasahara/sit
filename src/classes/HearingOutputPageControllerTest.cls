/**
* クラス名	:  HearingOutputPageController
* クラス概要	:  ヒアリング情報詳細画面(テスト)
* @created	:  2014/01/21 Khanh Vo Quoc
* @modified	:  2014/03/19 Vu Tran Tuan
*/
@isTest
private class HearingOutputPageControllerTest {
	/**
	* testMethodHearingOutputPageController()
	* ページ新規化処理（テスト）
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodHearingOutputPageController() {
		// ファイナンス情報
		Finance__c finance = createFinance();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		Test.startTest();
		// コントローラを作成
		HearingOutputPageController ct = new HearingOutputPageController(sc);
		Test.stopTest();
		System.assertEquals(1, ct.finance.Term__c);
	}
	/**
	* testMethodClickLinkKokyakuShousaiJouhou()
	* 「顧客詳細情報へ」リンク処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodClickLinkKokyakuShousaiJouhou() {
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		// コントローラを作成
		HearingOutputPageController ct = new HearingOutputPageController(sc);
		Test.startTest();
		try{
			// 「顧客詳細情報へ」リンク処理
			ct.clickLinkKokyakuShousaiJouhou();
		}catch(Exception ex){
			System.assert(true, String.valueOf(Apexpages.getMessages()).contains(label.Msg_Error_H_NotCustomer));	
		}
		Test.stopTest();
	}
	
	/**
	* testMethodClickLinkKokyakuShousaiJouhou_1()
	* 「顧客詳細情報へ」リンク処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodClickLinkKokyakuShousaiJouhou_1() {
		// ファイナンス情報
		Finance__c finance = createFinance();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		// コントローラを作成
		HearingOutputPageController ct = new HearingOutputPageController(sc);
		Test.startTest();
		//  「顧客詳細情報へ」リンク処理
		Pagereference pageTest = ct.clickLinkKokyakuShousaiJouhou();
		Test.stopTest();
		System.assert(true, pageTest.getUrl().contains(ct.finance.Customer__c));
	}
	/**
	* testMethodClickLinkKokyakuShousaiJouhou_2()
	* 「顧客詳細情報へ」リンク処理(テスト)
	* @return: なし
	* @created: 2014/03/16 Vu Tran Tuan
	*/
	static testMethod void testMethodClickLinkKokyakuShousaiJouhou_2() {
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		// コントローラを作成
		HearingOutputPageController ct = new HearingOutputPageController(sc);
		Test.startTest();
		//sau khi khởi tạo controller, set giá trị cho biến finance = null nhằm xuất hiện Exception;
		ct.finance = null;
		try{
			// 「顧客詳細情報へ」リンク処理
			ct.clickLinkKokyakuShousaiJouhou();
		}catch(Exception ex){
			System.assert(true, String.valueOf(Apexpages.getMessages()).contains(label.Msg_Error_System));	
		}
		Test.stopTest();
	}
	/**
	* testMethodActionButton()
	* test ham actionButton()
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	/*static testMethod void testMethodActionButton() {
		// ファイナンス情報
		Finance__c finance = createFinance();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		// コントローラを作成
		HearingOutputPageController ct = new HearingOutputPageController(sc);
		Test.startTest();
		Pagereference pageTest = ct.actionButton();
		Test.stopTest();
		System.assert(true, pageTest.getUrl().contains('HearingInputPage'));
	}
	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static Customer__c createCustomer(String name){
		// 顧客
		Customer__c customs = new Customer__c();
		// 顧客名
		customs.Name = name;
		// 代理店
		// 代理店【INGAgency】を作成
		AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
		agcINGA.Name = 'BSPLInput-test-test';
		insert agcINGA;
		customs.AgencyForINGAgency__c = agcINGA.Id;
		return customs;
	}
	/**
	* createFinance()
	* ファイナンス情報を作成
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static Finance__c createFinance(){
		// 顧客を作成
		Customer__c customs = createCustomer('khanh-test');
		insert customs;
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		finance.Term__c = 1;	// 決算期(前期)
		finance.H_RD__c = 2;	// 予想売上下落率
		finance.H_JASGD__c = 2;// 連帯保証債務額
		finance.H_LO__c = 2;	// 役員借入金
		finance.H_DBAC__c = 2;	// 死亡保障額(法人契約)
		finance.H_UG__c = 2;	// 含み益
		finance.H_DBAI__c = 2;	// 死亡保障額(個人契約)
		finance.H_BRM__c = 2;	// 銀行返済額(月)
		finance.H_BRP__c = 2;	// 銀行返済額　準備額
		finance.H_BFLEM__c = 2;	// 遺族生活資金(月)
		finance.H_BFLET__c = 2;	// 遺族生活資金(必要期間)
		finance.H_RE__c = 2;	// 現在の社長の報酬/年
		finance.H_CRAA__c = 2;	// 社長の報酬減額可能額/年
		finance.H_ERB__c = 2;	// 従業員退職金(予定額)
		finance.H_ORB__c = 2;	// 役員退職金予定額
		finance.Customer__c = customs.id; // 顧客名
		insert finance;
		return finance;
	}
}