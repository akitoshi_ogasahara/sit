/*********************************
 * IRIS - NNLink UI判定クラス
**********************************/
public class E_IRISHandler{
// フィルタ
	public E_CookieHandler.CookieItem policyCondi;

// その他
	// 共通アクセス権クラス
	private E_AccessController access;
	// シングルトン用
	private static E_IRISHandler instance = null;


	// (private)コンストラクタ
	private E_IRISHandler(){
		access = E_AccessController.getInstance();
		policyCondi = new E_CookieHandler.CookieItem(I_Const.COOKIE_POLICY_CONDITION);
		//入れた場合、エラーが発生する
		//if (this.getIsIRIS()) {
		//	Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
		//}
	}

	// インスタンス生成
	public static E_IRISHandler getInstance(){
		if (instance == null) {
			instance = new E_IRISHandler();
		}
		return instance;
	}

// UI関連処理
	/**
	 * IRIS 適用判定
	 * @param  なし
	 * @return Boolean：True-現在IRIS利用モードのとき
	 */
	public Boolean getIsIRIS(){
// IRIS利用可能判定
		if(!getCanUseIRIS()){
			return false;
		}

// AppMode判定
		if(access.idcpf != null){
			if(access.idcpf.AppMode__c == I_Const.APP_MODE_NNLINK){
				system.debug('AppモードがNNLink');
				return false;
			} else if(access.idcpf.AppMode__c == I_Const.APP_MODE_IRIS){
				system.debug('AppモードがIRIS');
				return true;
			}
		}

// ブラウザによる判定
		return IRISRecommendBrowser();
	}

	/**
	 * IRIS UI が使用可能か判定
	 * @param  なし
	 * @return Boolean:True-使用可能(一般代理店または社員)　かつ　IE9以上のときTrue
	 *							推奨ブラウザはIE11以上だがIE9以上での利用は可能とする（IE8以下は崩れるのでNG）
	 */
	public Boolean getCanUseIRIS(){
		// ID管理が存在しない場合はIRIS利用不可
		if(access.idcpf == null){
			return false;
		}

		// 共同GW経由の場合はIRIS利用不可
//		if(access.isCommonGateWay()){
//			if(!Boolean.valueOf(System.Label.isIRISEnableGW)){
//				system.debug('共同GW経由かつGWが無効化');
//				return false;
//			}
//		}

		// IRIS有効ユーザ判定
//		String target = System.Label.isIRISEnableUser;
//		if(target != '-'){
//			system.debug('特定ユーザのみIRIS利用可能状態');
//			if(target.indexOf(access.user.username) == -1){
//				system.debug('IRIS利用不可ユーザ');
//				return false;
//			}
//		}

		// IE8の場合はIRIS利用不可
		if(E_Util.isUserAgentIE8()){
			return false;
		}

		// ISS経由の場合はIRIS利用不可
		if(E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId())){
			return false;
		}

		// 一般代理店 または 社員ユーザ 以外はIRIS利用不可
		if(!access.isNormalAgent() && !access.isEmployee()){
			if(!access.isSumiseiIRISUser()){
				system.debug('一般代理店以外、社員以外');
				return false;
			}
		}

		return true;
	}

	/**
	 * IRIS が利用可能なブラウザか判定
	 * @param  なし
	 * @return Boolean:True-IRIS対象		IE11
	 */
	public Boolean IRISRecommendBrowser(){
		String ua = ApexPages.currentPage().getHeaders().get('User-Agent');
		if(String.isBlank(ua)){
			return false;
		}

		// IE10以下 は IRIS対象外
		ua = ua.toLowerCase();
		if(ua.indexOf('msie') != -1){
			return false;
		}

		return true;
	}

// 保有契約 - フィルタ
	/**
	 * フィルタセット
	 * @param String:フィルタ項目（代理店 or 事務所 or 募集人）
	 * @param String:Name（代理店名 or 事務所名 or 募集人名）
	 * @param String:SFID
	 * @param String:代理店名,事務所名,募集人名（パンくずリスト）
	 */
	public void setPolicyCondition(String field, String name, String sfid, String breadCrumb){
		if(String.isNotBlank(field) && String.isNotBlank(name) && String.isNotBlank(sfid)){
			// 既存のフィルタをクリア
			clearPolicyCondition();

			Map<String, String> jsonMap = new Map<String, String>();
			jsonMap.put(I_Const.JSON_KEY_FIELD, field);
			jsonMap.put(I_Const.JSON_KEY_NAME, name);
			jsonMap.put(I_Const.JSON_KEY_SFID, sfid);
			jsonMap.put(I_Const.JSON_KEY_BREADCRUMB, breadCrumb);

			// フィルタセットログを出力
			createPolicyConditionLog(jsonMap);
			// フィルタセット
			policyCondi.setCookie(JSON.serialize(jsonMap));
		}
		return;
	}
	/**
	 * フィルタ設定
	 * @param String:field フィルタ項目（代理店 or 事務所 or 募集人）
	 * @param String:SFID
	 */
	public static void setPolicyCondition(String field ,String sfid){
		String name = '';
		List<String> breadCrumb = new List<String>();
		if(field== I_Const.CONDITION_FIELD_AGENCY || field== I_Const.CONDITION_FIELD_OFFICE){
			// Name
			Account acc = E_AccountDaoWithout.getRecById(sfid);
			name = acc.name;
			// BreadCrumb
			if (field== I_Const.CONDITION_FIELD_OFFICE) {
				breadCrumb.add(acc.Parent.Name);
			}
			breadCrumb.add(acc.Name);

		}else if(field== I_Const.CONDITION_FIELD_AGENT){
			// Name
			Contact con = E_ContactDaoWithout.getRecById(sfid);
			name = con.Name;
			// BreadCrumb
			breadCrumb.add(con.E_AccParentName__c);
			breadCrumb.add(con.E_Account__c);
			breadCrumb.add(name);

		}else if(field== I_Const.CONDITION_FIELD_UNIT){
			// Name
			E_Unit__c unit = E_UnitDao.getUnitRecByBRCode(sfid);
			name = unit.Name;
			// BreadCrumb
			breadCrumb.add(name);

		}else if(field== I_Const.CONDITION_FIELD_MR){
			// Name
			User mr = E_UserDao.getUserRecByUserId(sfid);
			name = mr.Name;
			// BreadCrumb
			breadCrumb.add(mr.Unit__c);
			breadCrumb.add(name);

		}
		E_IRISHandler.getInstance().setPolicyCondition(field, name, sfid, String.join(breadCrumb, '|'));
	}

	/**
	 * フィルタセット時ログ出力
	 */
	private void createPolicyConditionLog(Map<String, String> condition){
		E_Log__c conditionLog = E_LogUtil.createLog(access.idcpf.appMode__c);

		List<String> logDetail = new List<String>();
		logDetail.add('項目：' + condition.get(I_Const.JSON_KEY_FIELD));
		logDetail.add('名称：' + condition.get(I_Const.JSON_KEY_BREADCRUMB));
		logDetail.add('SFID：' + condition.get(I_Const.JSON_KEY_SFID));

		conditionLog.Name = String.join(logDetail, '_');
		conditionLog.ActionType__c = I_Const.ACTION_TYPE_FILTER_SETTING;
		conditionLog.detail__c = String.join(logDetail, '\r\n');

		//文字数オーバーの場合Truncateしてinsert
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		conditionLog.setOptions(dml);

		insert conditionLog;

		return;
	}

	/**
	 * フィルタクリア
	 */
	public void clearPolicyCondition(){
		policyCondi.deleteCookie();

		return;
	}

	/**
	 * フィルタ有無判定
	 */
	public Boolean isSetPolicyCondition(){
		return policyCondi.isSetCookie();
	}

	/**
	 * フィルタ項目取得
	 * @param String:jsonキー
	 */
	public String getPolicyConditionValue(String jsonkey){
		String value = policyCondi.getCookieValue();

		if(String.isNotBlank(value)){
			Map<String, String> valueMap = (Map<String, String>)JSON.deserialize(value, Map<String, String>.class);
			return valueMap.get(jsonKey);
		}

		return null;
	}

// 利用アプリ選択
	/**
	 * 利用アプリをIRISに更新
	 */
	public void setUseIRIS(){
		updateUseUI(I_Const.APP_MODE_IRIS);
		return;
	}

	/**
	 * 利用アプリをNNLinkに更新
	 */
	public void setUseNNLink(){
		updateUseUI(I_Const.APP_MODE_NNLINK);
		return;
	}

	/**
	 * ID管理を更新
	 */
	private void updateUseUI(String value){
		access.idcpf.AppMode__c = value;
		update access.idcpf;
		return;
	}


/*                  ユーザ情報                   */
	//ログインユーザ名
	public String getNameOfLoginUser(){
		return userInfo.getName();
	}

	//部署名       代理店ユーザ：代理店名     MR：ロール名
	public String getNameOfDept(){
		if(access.isAgent()){
			return access.user.Contact.E_AccParentName__c;
		}

		//代理店ユーザ以外
		String dept = access.user.unit__c;
		if(String.isBlank(dept)){
			dept = access.user.Department;
		}
		return dept;
	}

/*                  IRIS　メニュー管理         */
	public void setIrisMenuId(Id mnId){
		irisMenus.setAcitveMenuId(mnId);
	}
	public void setLinkMenuKey(String link_mn_key){
		irisMenus.setAcitveMenuKey(link_mn_key);
	}

	public I_MenuProvider irisMenus{get{
											if(irisMenus == null){
												irisMenus = I_MenuProvider.getInstance();
											}
											return irisMenus;
										}
									set;
						}
}