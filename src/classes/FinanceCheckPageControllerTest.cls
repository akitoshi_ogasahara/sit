/**
* クラス名		:  FinanceCheckPageControllerTest
* クラス概要	:  ファイナンス 確認画面(テスト)
* @created	:  2014/02/25 Hung Nguyen The
* @modified	:  
*/
@isTest
private class FinanceCheckPageControllerTest {
	/**
	* testMethodInitContructor0
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static testMethod void testMethodInitContructor0(){
		// ファイナンス情報を作成
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_FC_Not));
	}
	/**
	* testMethodInitContructor01
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static testMethod void testMethodInitContructor01(){
		// ファイナンス情報を作成
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, 'a0N000000000000001');
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_FC_Not));
	}
	/**
	* testMethodInitContructor02
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static testMethod void testMethodInitContructor02(){
		// ファイナンス情報を作成
		createFinance();
		Finance__c fi = [Select Id, Customer__c From Finance__c Order By Term__c ASC Limit 1].get(0);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		Apexpages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_FC2_Not));
	}
	/**
	* testMethodInitContructor03
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static testMethod void testMethodInitContructor03(){
		// ファイナンス情報を作成
		createFinance();
		Finance__c fi = [Select Id From Finance__c Order By Term__c DESC Limit 1].get(0);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		controller.getCommonConst();
		Test.stopTest();
		System.assertNotEquals(null, controller.finance);
	}
	/**
	* testMethodInitContructor04
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static testMethod void testMethodInitContructor04(){
		// ファイナンス情報を作成
		createFinance();
		Finance__c fi = [Select Id,BS_PLFlg__c,Customer__c From Finance__c Order By Term__c DESC Limit 1].get(0);
		fi.BS_PLFlg__c = false;
		update fi;
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		Apexpages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		controller.getCommonConst();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_FC3_Not));
	}
	/**
	* testMethodInitContructor05
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static testMethod void testMethodInitContructor05(){
		// ファイナンス情報を作成
		createFinance();
		Finance__c fi = [Select Id,BS_PLFlg__c,Customer__c,Hearing__c From Finance__c Order By Term__c DESC Limit 1].get(0);
		fi.BS_PLFlg__c = true;
		fi.Hearing__c = true;
		update fi;
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		Apexpages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		controller.getCommonConst();
		Test.stopTest();
		System.assertEquals(fi.Id, controller.finance.Id);
	}
	/**
	* testMethodExportPDF
	* 「PDF出力画面へ」リンク処理(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/02/10 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodExportPDF(){
		// ファイナンス情報を作成
		createFinance();
		Finance__c fi = [Select Id From Finance__c Order By Term__c DESC Limit 1].get(0);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		// 「PDF出力画面へ」リンク処理
		Pagereference pagePDF = controller.exportPDF();
		Test.stopTest();
		// System.assertEquals(true, pagePDF.getUrl().contains(Label.PDFURL));
	}
	/**
	* testMethodGotoAccount
	* 「顧客詳細情報へ」リンク処理(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/02/10 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodGotoAccount(){
		// ファイナンス情報を作成
		createFinance();
		Finance__c fi = [Select Id From Finance__c Order By Term__c DESC Limit 1].get(0);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		Test.startTest();
		// ページ初期化処理
		FinanceCheckPageController controller = new FinanceCheckPageController();
		//  「顧客詳細情報へ」リンク処理
		Pagereference pageACC = controller.goToAccountDetail();
		Test.stopTest();
		// System.assertEquals(true, pageACC.getUrl().contains(controller.finance.Customer__c));
	}
	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static Customer__c createCustomer(String name){
		// 顧客
		Customer__c customs = new Customer__c();
		// 代理店
		//  代理店【INGAgency】を作成
		AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
		agcINGA.Name = 'BSPLInput-test-test';
		insert agcINGA;
		customs.AgencyForINGAgency__c = agcINGA.Id;
		// 顧客名
		customs.Name = name;
		// 業界平均マスタ
		customs.Industry__c = 'Test industry';
		return customs;
	}
	/**
	* createCustomer()
	* ファイナンス情報を作成
	* @return: ファイナンス情報
	* @created: 2014/02/25 Hung Nguyen The
	*/
	static void createFinance(){
		// ファイナンス情報を作成
		Customer__c customs = createCustomer('Test Custommer');
		insert customs;
		List<Finance__c> listFinance = new List<Finance__c>();
		for(Integer i = 1; i < 3; i++){
			Finance__c finance = new Finance__c();
			// 決算期(前期)
			finance.Term__c = i;
			finance.H_RD__c = 2;	// 予想売上下落率
			finance.H_JASGD__c = 2;// 連帯保証債務額
			finance.H_LO__c = 2;	// 役員借入金
			finance.H_DBAC__c = 2;	// 死亡保障額(法人契約)
			finance.H_UG__c = 2;	// 含み益
			finance.H_DBAI__c = 2;	// 死亡保障額(個人契約)
			finance.H_BRM__c = 2;	// 銀行返済額(月)
			finance.H_BRP__c = 2;	// 銀行返済額　準備額
			finance.H_BFLEM__c = 2;	// 遺族生活資金(月)
			finance.H_BFLET__c = 2;	// 遺族生活資金(必要期間)
			finance.H_RE__c = 2;	// 現在の社長の報酬/年
			finance.H_CRAA__c = 2;	// 社長の報酬減額可能額/年
			finance.H_ERB__c = 2;	// 従業員退職金(予定額)
			finance.H_ORB__c = 2;	// 役員退職金予定額
			finance.Customer__c = customs.id; // 顧客名
			listFinance.add(finance);
		}
		insert listFinance;
	}
	/**
	* createCustomSetting()
	* カスタム設定。法人税定義を作成
	* @return: カスタム設定。法人税定義
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static TaxDefinition__c createTaxDefinition(){
		TaxDefinition__c taxDf = new TaxDefinition__c();
		taxDf.From__c = Date.valueOf('2014-01-20');	// 法人税定義.施行日
		taxDf.NRate__c = 10;	// 法人税定義.必要倍率	
		taxDf.TaxRate__c = 10;	// 法人税定義.法人税率
		insert taxDf;
		return taxDf;
	}
}