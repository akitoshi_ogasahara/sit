global with sharing class E_InveSearchExtender extends E_AbstractSearchExtender {
	private static final String PAGE_TITLE = '投資信託検索';
	
	public E_InveSearchController extension;
	
	/** コンストラクタ */
	public E_InveSearchExtender(E_InveSearchController extension) {
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
	}
	
	/* init */
	global override void init(){
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, null);
  	}
	
	/** ページアクション */
	public PageReference pageAction(){
		return E_Util.toErrorPage(pageRef, null);
	}
	
	/** ①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ */
	global override void preSearch() {
		if (extension != null) {
			if (extension.NameKana_val.SkyEditor2__Text__c != null) {
				extension.NameKana_val.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(extension.NameKana_val.SkyEditor2__Text__c))));
			}
		}
	}
}