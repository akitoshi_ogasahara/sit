/**
 * ID管理データアクセスオブジェクト(Without版)
 * CreatedDate 2015/04/01
 * @Author Terrasky
 */
public without sharing class E_IDCPFDaoWithout {
    
    /**
     * ID管理データをContactIdから取得する
     * @param ContactId: コンタクトID
     * @return E_IDCPF__c: ID管理情報
     */
    public static E_IDCPF__c getRecsByContactId(Id ContactId){
        List<E_IDCPF__c> recs = [
                SELECT
                      Id                    //カスタムオブジェクト ID 
                    , OwnerId               //所有者 ID 
                    , Name                  //ID番号No 
                    , User__c               //ユーザ 
                    , CLNTNUM__c            //所有者顧客コード 
                    , FLAG01__c             //個人保険照会フラグ 
                    , FLAG02__c             //SPVA照会フラグ 
                    , FLAG03__c             //投信照会フラグ 
                    , FLAG04__c             //顧客検索フラグ 
                    , FLAG05__c             //販売状況データフラグ 
                    , FLAG06__c             //手続の履歴フラグ 
                    , ZEBZCLNT__c           //表示用顧客コード 
                    , ZEMAILAD__c           //E-MAILアドレス 
                    , ZIDOWNER__c           //所有者コード 
                    , ZIDTYPE__c            //ID種別 
                    , ZINQUIRR__c           //照会者コード 
                    , ZWEBID__c             //ID番号 
                    , TRCDE01__c            //トランザクションコード1 
                    , TRCDE02__c            //トランザクションコード2
                    , TRCDE03__c            //トランザクションコード3
                    , TRCDE04__c            //トランザクションコード4
                    , TRCDE05__c            //トランザクションコード5
                    , TRCDE06__c            //トランザクションコード6
                    , TRCDE07__c            //トランザクションコード7
                    , TRCDE08__c            //トランザクションコード8
                    , TRCDE09__c            //トランザクションコード9
                    , TRCDE10__c            //トランザクションコード10
                    , ZDSPFLAG01__c         //手数料明細書照会フラグ 
                    , ZDSPFLAG02__c         //予備 
                    , ZDSPFLAG03__c         //予備 
                    , ZDSPFLAG04__c         //予備 
                    , ZDSPFLAG05__c         //予備 
                    , ZDSPFLAG06__c         //予備 
                    , ZDSPFLAG07__c         //予備 
                    , ZDSPFLAG08__c         //予備 
                    , ZDSPFLAG09__c         //予備 
                    , ZDSPFLAG10__c         //予備 
                    , ZPASSWD02__c          //手続パスワード 
                    , ZSTATUS01__c          //パスワードステータス 
                    , ZSTATUS02__c          //手続パスワードステータス 
                    , ZPASSWD02DATE__c      //手続パスワード変更日
                    , UseTempProcedurePW__c //仮手続PW使用
                    , RegularProcedurePW__c //本手続パスワード
                    , User__r.AccountId
                    , User__r.ContactId
                    , User__r.ProfileId
                    , User__r.Contact.Account.ParentId
                    , IsMaintenanceCharge__c    //メンテナンス担当
                    , IsScAdmin__c              //自主点検（コンプラ）
                    , IsScDirector__c           //自主点検（参照のみ）
                    , IsCrApprove__c            //代理店規定管理（承認権限セット）
                    , IsCrDpa__c                //代理店規程管理（DPA権限セット)
                    , IsShowSREmployee__c       //社内挙績情報参照可能フラグ
                FROM E_IDCPF__c
                WHERE User__r.ContactId = :ContactId
                Order by SystemModstamp desc limit 1
        ];
        return recs.isEmpty() ? null : recs[0];
    }
    
    /**
     * ID管理データリストを代理店格コードから取得する
     *          有効なユーザに紐づけられたID管理のみを取得
     * @param zheadays: 代理店格コードのSet
     * @return List<E_IDCPF__c>: ID管理情報
     */
    public static List<E_IDCPF__c> getActiveUsersRecsByZHEADAY(String zheaday){
        return  [Select Id
                        , isDeleted
                        , User__c                   //ユーザ
                        , User__r.Contact.E_CL3PF_ZAGMANGR__c       //管理責任者
                        , User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c //代理店格コード
                        , User__r.Contact.E_AccParentCord__c    // 代理店格コード
                        , User__r.Contact.E_CL3PF_AGTYPE__c     // 募集人種別
                        , User__r.UserType
                        , User__r.isActive
                        , User__r.AccountId
                        , User__r.Contact.Account.ParentId
                        , User__r.ProfileId
                        , ZWEBID__c                 //ID番号
                        , ZIDTYPE__c                //ID種別
                        , ZIDOWNER__c               //所有者コード
                        , ZINQUIRR__c               //照会者コード
                        , ZPASSWD02__c              //手続パスワード
                        , ZPASSWD02DATE__c          //手続パスワード変更日
                        , CLNTNUM__c                //所有者顧客コード
                        , ZEBZCLNT__c               //表示用顧客コード
                        , ZEMAILAD__c               //E-MAILアドレス
                        , ZSTATUS01__c              //パスワードステータス
                        , ZSTATUS02__c              //手続パスワードステータス
                        , FLAG01__c                 //個人保険照会フラグ
                        , FLAG02__c                 //SPVA照会フラグ
                        , FLAG03__c                 //投信照会フラグ
                        , FLAG04__c                 //顧客検索フラグ
                        , FLAG05__c                 //販売状況データフラグ
                        , FLAG06__c                 //手続の履歴フラグ
                        , TRCDE01__c                //トランザクションコード01
                        , TRCDE02__c                //トランザクションコード02
                        , TRCDE03__c                //トランザクションコード03
                        , TRCDE04__c                //トランザクションコード04
                        , TRCDE05__c                //トランザクションコード05
                        , TRCDE06__c                //トランザクションコード06
                        , TRCDE07__c                //トランザクションコード07
                        , TRCDE08__c                //トランザクションコード08
                        , TRCDE09__c                //トランザクションコード09
                        , TRCDE10__c                //トランザクションコード10
                        , ZDSPFLAG01__c             //手数料明細書照会フラグ
                        , ZDSPFLAG02__c             //予備
                        , ZDSPFLAG03__c             //予備
                        , ZDSPFLAG04__c             //予備
                        , ZDSPFLAG05__c             //予備
                        , ZDSPFLAG06__c             //予備
                        , ZDSPFLAG07__c             //予備
                        , ZDSPFLAG08__c             //予備
                        , ZDSPFLAG09__c             //予備
                        , ZDSPFLAG10__c             //予備
                        , IsMaintenanceCharge__c    //メンテナンス担当
                        , IsScAdmin__c              //自主点検（コンプラ）
                        , IsScDirector__c           //自主点検（参照のみ）
                        , IsCrApprove__c            //代理店規定管理（承認権限セット）
                        , IsCrDpa__c                //代理店規程管理（DPA権限セット)
                        , IsShowSREmployee__c       //社内挙績情報参照可能フラグ
                FROM E_IDCPF__c e
                WHERE (User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c = :zheaday
                   OR User__r.Contact.Account.E_CL1PF_ZHEADAY__c = :zheaday)
                  AND User__r.isActive = true];
    }

    /**
     * ID管理データリストをID管理のSFIDから取得する
     *          有効なユーザに紐づけられたID管理のみを取得
     * @param eidcId: ID番号
     * @return E_IDCPF__c: ID管理情報
     */
    public static E_IDCPF__c getRecsByZWEBID(ID eidcId){
        List<E_IDCPF__c> recs = [
                SELECT
                      Id                    //カスタムオブジェクト ID 
                    , OwnerId               //所有者 ID 
                    , Name                  //ID番号No 
                    , User__c               //ユーザ 
                    , CLNTNUM__c            //所有者顧客コード 
                    , FLAG01__c             //個人保険照会フラグ 
                    , FLAG02__c             //SPVA照会フラグ 
                    , FLAG03__c             //投信照会フラグ 
                    , FLAG04__c             //顧客検索フラグ 
                    , FLAG05__c             //販売状況データフラグ 
                    , FLAG06__c             //手続の履歴フラグ 
                    , ZEBZCLNT__c           //表示用顧客コード 
                    , ZEMAILAD__c           //E-MAILアドレス 
                    , ZIDOWNER__c           //所有者コード 
                    , ZIDTYPE__c            //ID種別 
                    , ZINQUIRR__c           //照会者コード 
                    , ZWEBID__c             //ID番号 
                    , TRCDE01__c            //トランザクションコード1 
                    , TRCDE02__c            //トランザクションコード2
                    , TRCDE03__c            //トランザクションコード3
                    , TRCDE04__c            //トランザクションコード4
                    , TRCDE05__c            //トランザクションコード5
                    , TRCDE06__c            //トランザクションコード6
                    , TRCDE07__c            //トランザクションコード7
                    , TRCDE08__c            //トランザクションコード8
                    , TRCDE09__c            //トランザクションコード9
                    , TRCDE10__c            //トランザクションコード10
                    , ZDSPFLAG01__c         //手数料明細書照会フラグ 
                    , ZDSPFLAG02__c         //予備 
                    , ZDSPFLAG03__c         //予備 
                    , ZDSPFLAG04__c         //予備 
                    , ZDSPFLAG05__c         //予備 
                    , ZDSPFLAG06__c         //予備 
                    , ZDSPFLAG07__c         //予備 
                    , ZDSPFLAG08__c         //予備 
                    , ZDSPFLAG09__c         //予備 
                    , ZDSPFLAG10__c         //予備 
                    , ZPASSWD02__c          //手続パスワード 
                    , ZSTATUS01__c          //パスワードステータス 
                    , ZSTATUS02__c          //手続パスワードステータス 
                    , ZPASSWD02DATE__c      //手続パスワード変更日
                    , UseTempProcedurePW__c //仮手続PW使用
                    , RegularProcedurePW__c //本手続パスワード
                    , AtriaCommissionAuthorityEditable__c   //Atria
                    , Contact__c                            //Atria
                    , ContactName__c                        //Atria
                    , ContactAccount__c                     //Atria
                    , User__r.AccountId
                    , User__r.ContactId
                    , User__r.ProfileId
                    , User__r.Contact.Account.ParentId
                    , User__r.Contact.E_CL3PF_ZAGMANGR__c
                    , User__r.Contact.E_AccParentCord__c    // 代理店格コード
                    , User__r.Contact.E_CL3PF_AGTYPE__c     // 募集人種別
                    , IsMaintenanceCharge__c    //メンテナンス担当
                    , IsScAdmin__c              //自主点検（コンプラ）
                    , IsScDirector__c           //自主点検（参照のみ）
                    , IsCrApprove__c            //代理店規定管理（承認権限セット）
                    , IsCrDpa__c                //代理店規程管理（DPA権限セット)
                    , IsShowSREmployee__c       //社内挙績情報参照可能フラグ
                FROM E_IDCPF__c
                WHERE ID = :eidcId
                Order by SystemModstamp desc limit 1
        ];
        return recs.isEmpty() ? null : recs[0];
    }

    /**
     * ID管理レコードを作成する
     * @param eidc ID管理
     */
    public static void insertRec(E_IDCPF__c eidc){
        insert eidc;
    }

    public static void updateRec(E_IDCPF__c eidc){
        update eidc;
    }
    //P16-0003 Atria対応開発
    public static void updateRecs(List<E_IDCPF__c> eidcList){
        //トリガハンドラを呼び出さずに更新を行う
        E_IDCPFTriggerHandler.isSkipTriggerActions =true;
        update eidcList;
    }


    //ID管理を更新する
    //ID管理トリガーハンドラを呼び出さないようにする
    @future
    public static void updateEidc(ID eidcid,String eidremail){
        E_IDCPF__c eidc = getRecsByZWEBID(eidcid);
        eidc.ZEMAILAD__c = eidremail;
        //TriggerHundlerの実行不可フラグをonにする
        E_IDCPFTriggerHandler.isSkipTriggerActions = true;
        update eidc;
        E_IDCPFTriggerHandler.isSkipTriggerActions = false;

    }

        /**
     * ID管理データリストをUserIDから取得する
     *          有効なユーザに紐づけられたID管理のみを取得
     * @param userID: UserID
     * @return E_IDCPF__c: ID管理情報
     */
    public static E_IDCPF__c getRecsByUserID(ID userID){
        List<E_IDCPF__c> recs = [
                SELECT
                      Id                    //カスタムオブジェクト ID 
                    , OwnerId               //所有者 ID 
                    , Name                  //ID番号No 
                    , User__c               //ユーザ 
                    , CLNTNUM__c            //所有者顧客コード 
                    , FLAG01__c             //個人保険照会フラグ 
                    , FLAG02__c             //SPVA照会フラグ 
                    , FLAG03__c             //投信照会フラグ 
                    , FLAG04__c             //顧客検索フラグ 
                    , FLAG05__c             //販売状況データフラグ 
                    , FLAG06__c             //手続の履歴フラグ 
                    , ZEBZCLNT__c           //表示用顧客コード 
                    , ZEMAILAD__c           //E-MAILアドレス 
                    , ZIDOWNER__c           //所有者コード 
                    , ZIDTYPE__c            //ID種別 
                    , ZINQUIRR__c           //照会者コード 
                    , ZWEBID__c             //ID番号 
                    , TRCDE01__c            //トランザクションコード1 
                    , TRCDE02__c            //トランザクションコード2
                    , TRCDE03__c            //トランザクションコード3
                    , TRCDE04__c            //トランザクションコード4
                    , TRCDE05__c            //トランザクションコード5
                    , TRCDE06__c            //トランザクションコード6
                    , TRCDE07__c            //トランザクションコード7
                    , TRCDE08__c            //トランザクションコード8
                    , TRCDE09__c            //トランザクションコード9
                    , TRCDE10__c            //トランザクションコード10
                    , ZDSPFLAG01__c         //手数料明細書照会フラグ 
                    , ZDSPFLAG02__c         //予備 
                    , ZDSPFLAG03__c         //予備 
                    , ZDSPFLAG04__c         //予備 
                    , ZDSPFLAG05__c         //予備 
                    , ZDSPFLAG06__c         //予備 
                    , ZDSPFLAG07__c         //予備 
                    , ZDSPFLAG08__c         //予備 
                    , ZDSPFLAG09__c         //予備 
                    , ZDSPFLAG10__c         //予備 
                    , ZPASSWD02__c          //手続パスワード 
                    , ZSTATUS01__c          //パスワードステータス 
                    , ZSTATUS02__c          //手続パスワードステータス 
                    , ZPASSWD02DATE__c      //手続パスワード変更日
                    , UseTempProcedurePW__c //仮手続PW使用
                    , RegularProcedurePW__c //本手続パスワード
                    , User__r.AccountId
                    , User__r.ContactId
                    , User__r.ProfileId
                    , User__r.IsActive
                    , User__r.LastPasswordChangeDate
                    , User__r.UserType
                    , User__r.Contact.Account.ParentId
                    , IsMaintenanceCharge__c    //メンテナンス担当
                    , IsScAdmin__c              //自主点検（コンプラ）
                    , IsScDirector__c           //自主点検（参照のみ）
                    , IsCrApprove__c            //代理店規定管理（承認権限セット）
                    , IsCrDpa__c                //代理店規程管理（DPA権限セット)
                    , IsShowSREmployee__c       //社内挙績情報参照可能フラグ
                    , uuid1__c                  //MobileDevice1
                    , uuid2__c      //MobileDevice2
                FROM E_IDCPF__c
                WHERE User__c = :userID
                Order by SystemModstamp desc limit 1
        ];
        return recs.isEmpty() ? null : recs[0];
    }

    //P16-0003 Atria対応開発
    /**
     * ID管理データリストをIdリストから取得する
     * @param ids: EIDCレコードIDのSet
     * @return List<E_IDCPF__c>: ID管理情報
     */
    public static List<E_IDCPF__c> getUserNameByIds(Set<Id> ids){
        return [
            SELECT
                Id
                , User__r.Name
                FROM E_IDCPF__c
            WHERE Id In :ids
        ];
    }
    /**
     *ＩＤ管理レコードをContactIdから取得
     *@param  contactId:募集人ＩＤ
     *@return 検索されたID管理レコード
     */
    public static List<E_IDCPF__c> getIdRecsByContactId(Id contactId){
        List<E_IDCPF__c> recs = 
        [SELECT 
                Id,
                Contact__c
         FROM E_IDCPF__c
         WHERE Contact__c = :contactId
         limit 1];

        return recs;
    }

    /**
     * [代理店挙績情報]
     * 選択されたMRのIDと、それに紐づく支社コード(ID管理の照会者コード)を取得
     * @param   [description]
     * @return [description]
     */
    public Static E_IDCPF__c getEidcCodeByMrId(String mrParam){
        return [SELECT
                    ZINQUIRR__c
                FROM E_IDCPF__c 
                WHERE User__c =:mrParam];
    }


    //ユーザーとのリレーションを切る
    //ID管理トリガーハンドラを呼び出さないようにする
    @future
    public static void cutOutRelationWithUser(ID userId){
        E_IDCPF__c eidc = getRecsByUserID(userId);
        eidc.OldUser__c = eidc.User__c;
        eidc.User__c = null;
        //TriggerHundlerの実行不可フラグをonにする
        E_IDCPFTriggerHandler.isSkipTriggerActions = true;
        update eidc;
        E_IDCPFTriggerHandler.isSkipTriggerActions = false;

    }

    //Atria R1.1対応
    /**
     * ContactのIdリストから有効なユーザの持つID管理のリストを取得する
     *　@param List<Id> contactIds　  :募集人IDのリスト
     * @return List<E_IDCPF__c>     :ID管理情報
     */
    public static List<E_IDCPF__c> getRecsByContactIds( List<Id> contactIds ){
        List<E_IDCPF__c> recs = [
            SELECT
            Id                          //カスタムオブジェクト ID
            , User__r.Contact.E_CL3PF_ZAGMANGR__c                   //管理責任者
            FROM E_IDCPF__c
            WHERE User__r.ContactId IN :contactIds
            AND User__r.IsActive = true
        ];
        return recs;
    }
    /**
     * 募集人IDよりID管理情報を取得する
     * @param String agId
     * @return List<E_IDCPF__c>
     */
    public static List<E_IDCPF__c> getAgIdcpf (String agId){
        List<E_IDCPF__c> agIdList = [SELECT 
                                            Id
                                            , ZIDTYPE__c
                                            , AtriaCommissionAuthorityEditable__c
                                            , Contact__c
                                            , ContactName__c
                                            , ContactAccount__c
                                     FROM 
                                            E_IDCPF__c 
                                     WHERE 
                                            Contact__c = :agId
                                    ];
        return agIdList;
    }

}