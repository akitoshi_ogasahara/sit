/*
 * CC_CaseEditVOIBizLogic
 * Business logic class for CC_CaseEditOmniScript OmniScript
 * created  : Accenture 2018/4/18
 * modified :
 */

public with sharing class CC_CaseEditVOIBizLogic{

	/**
	 * initial Case
	 */
	public static Boolean initCase(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){

		Boolean success = true;

		//既存SRのIDや顧客ID(顧客画面からSRを新規する場合)を取得
		Boolean isNewCase = false;
		String caseId = '';
		Id targetId;
		Id agencyId;
		if(String.isNotEmpty((String)inputMap.get('ContextId'))){
			caseId = (String) inputMap.get('ContextId');
		}else{
			targetId = (Id) inputMap.get('targetId');
			if(String.isNotEmpty((String)inputMap.get('agencyId'))) agencyId = (Id) inputMap.get('agencyId');
			isNewCase = true;
		}

		//新規SRの場合
		if(isNewCase){
			//SR作成:Debug & 基本情報セクション1
			String outputDebugJson = '"Debug":[';
			Map<String, Object> outputDebugMap = new Map<String, Object>();

			String outputBasicInfo1Json = '"基本情報セクション1":[';
			Map<String, Object> outputBasicInfo1Map = new Map<String, Object>();

			String sobjectType = targetId.getSObjectType().getDescribe().getName();
			if(sobjectType.equals('Account')){
				//権利者名(法人
				Account accountObj = CC_CaseEditVOIDao.getAccountById(targetId);

				outputDebugMap.put('SYSCorporateName', targetId);					   //権利者名(法人)項目保存用
				outputDebugMap.put('ContactNameForPrint', accountObj.Name);			 //送付先宛名設定用
				outputDebugMap.put('AddressForPrint', accountObj.CMN_SYSAddress__c);	//送付先住所設定用
				outputDebugMap.put('ZipcodeForPrint', accountObj.CLTPCODE__c);		  //送付先郵便番号設定用
			}else if(sobjectType.equals('Contact')){
				//outputDebugMap.put('ContactId', targetId);
				Contact contactObj = CC_CaseEditVOIDao.getContactById(targetId);

				//権利者名(個人)
				if(contactObj.RecordType.Name.equals('顧客')){
					outputDebugMap.put('SYSContactName', targetId);								 //権利者名(個人)項目保存用
					outputDebugMap.put('ContactNameForPrint', contactObj.Name);					 //送付先宛名設定用
					outputDebugMap.put('AddressForPrint', contactObj.CMN_SYSAddress__c);			//送付先住所設定用
					outputDebugMap.put('ZipcodeForPrint', contactObj.CMN_FormattedPostalcode__c);   //送付先郵便番号設定用
				}
				//募集人
				else if(contactObj.RecordType.Name.equals('募集人')){
					outputBasicInfo1Map.put('募集人コード', contactObj.E_CL3PF_AGNTNUM__c);
					outputBasicInfo1Map.put('申出人', contactObj.Name);
					if(agencyId != null){
						Account accountObj = CC_CaseEditVOIDao.getAccountById(agencyId);
						outputBasicInfo1Map.put('代理店事務所コード', accountObj.E_CL2PF_ZAGCYNUM__c + '/' + accountObj.Name);
					}else if(contactObj.Account.E_CL2PF_ZAGCYNUM__c != null){
						outputBasicInfo1Map.put('代理店事務所コード', contactObj.Account.E_CL2PF_ZAGCYNUM__c + '/' + contactObj.Account.Name);
					}
					outputDebugMap.put('ContactNameForPrint', contactObj.CMN_SYSAgencyName__c + '\n' + contactObj.CMN_SYSAgentName__c);	 //送付先宛名設定用
					outputDebugMap.put('AddressForPrint', contactObj.Account.CMN_SYSAddress__c);											//送付先住所設定用
					outputDebugMap.put('ZipcodeForPrint', contactObj.Account.CLTPCODE__c);												  //送付先郵便番号設定用

				}
			}

			outputDebugMap.put('TargetId', targetId);

			Datetime now = system.now();
			String strNow = now.format('yyyy/MM/dd HH:mm');
			outputBasicInfo1Map.put('受付日時', strNow);
			outputBasicInfo1Map.put('受付担当者', UserInfo.getName());
			outputBasicInfo1Map.put('ステータス', 'オープン');

			outputDebugJson = outputDebugJson + JSON.Serialize(outputDebugMap) + ']';
			outputBasicInfo1Json = outputBasicInfo1Json + JSON.Serialize(outputBasicInfo1Map) + ']';
			System.debug('Debug '+outputDebugJson);
			System.debug('基本情報セクション1 '+outputBasicInfo1Json);


			//SR作成:帳票出力
			String outputFormJson = '"帳票出力":[';
			Map<String, Object> outputFormMap = new Map<String, Object>();
			outputFormMap.put('送付状出力', true);
			outputFormMap.put('請求書出力', true);
			outputFormJson = outputFormJson + JSON.Serialize(outputFormMap) + ']';


			//SR作成のJSON Mapを出力
			String outputSRJson = '[{' + outputDebugJson + ',' + outputBasicInfo1Json + ',' + outputFormJson +'}]';
			outputMap.put('SR作成', JSON.deserializeUntyped(outputSRJson));
		}

		//既存SRの場合
		else{
			Case caseObj = CC_CaseEditVOIDao.getCaseById(caseId);

			if(caseObj != null){
				//SR作成:Debug
				String outputDebugJson = '"Debug":[';
				Map<String, Object> outputDebugMap = new Map<String, Object>();

				//個人顧客
				if(caseObj.CMN_ContactName__c != null){
					targetId = caseObj.CMN_ContactName__c;
					outputDebugMap.put('TargetId', caseObj.CMN_ContactName__c);
					outputDebugMap.put('CurrentContactName', caseObj.CMN_ContactName__c);								   //証券番号TypeAhead設定用
					outputDebugMap.put('ContactNameForPrint', caseObj.CMN_ContactName__r.Name);							 //送付先宛名設定用
					outputDebugMap.put('AddressForPrint', caseObj.CMN_ContactName__r.CMN_SYSAddress__c);					//送付先住所設定用
					outputDebugMap.put('ZipcodeForPrint', caseObj.CMN_ContactName__r.CMN_FormattedPostalcode__c);		   //送付先郵便番号設定用
				}
				//法人顧客
				else if(caseObj.CMN_CorporateName__c != null){
					targetId = caseObj.CMN_CorporateName__c;
					outputDebugMap.put('TargetId', caseObj.CMN_CorporateName__c);
					outputDebugMap.put('CurrentCorporateName', caseObj.CMN_CorporateName__c);							   //証券番号TypeAhead設定用
					outputDebugMap.put('ContactNameForPrint', caseObj.CMN_CorporateName__r.Name);						   //送付先宛名設定用
					outputDebugMap.put('AddressForPrint', caseObj.CMN_CorporateName__r.CMN_SYSAddress__c);				  //送付先住所設定用
					outputDebugMap.put('ZipcodeForPrint', caseObj.CMN_CorporateName__r.CLTPCODE__c);						//送付先郵便番号設定用
				}
				//募集人
				else if(caseObj.CMN_AgencyName__c != null){
					targetId = caseObj.CMN_AgencyName__c;
					outputDebugMap.put('TargetId', caseObj.CMN_AgencyName__c);
					if(caseObj.CMN_AgentName__c != null){
						outputDebugMap.put('ContactNameForPrint', caseObj.CMN_AgentName__r.CMN_SYSAgencyName__c + '\n' + caseObj.CMN_AgentName__r.CMN_SYSAgentName__c); //送付先宛名設定用
						outputDebugMap.put('AddressForPrint', caseObj.CMN_AgencyName__r.CMN_SYSAddress__c);															 //送付先住所設定用
						outputDebugMap.put('ZipcodeForPrint', caseObj.CMN_AgencyName__r.CLTPCODE__c);																   //送付先郵便番号設定用
					}

				}

				outputDebugJson = outputDebugJson + JSON.Serialize(outputDebugMap) + ']';
				System.debug('Debug '+outputDebugJson);


				//SR作成:基本情報セクション1
				String outputBasicInfo1Json = '"基本情報セクション1":[';
				Map<String, Object> outputBasicInfo1Map = new Map<String, Object>();

				outputBasicInfo1Map.put('SR番号', caseObj.CaseNumber);
				outputBasicInfo1Map.put('複数代理店フラグ', caseObj.CMN_IsMultipleAgency__c);
				if(caseObj.Status != null) outputBasicInfo1Map.put('ステータス', caseObj.Status);
				if(caseObj.CC_SubStatus__c != null) outputBasicInfo1Map.put('サブステータス', caseObj.CC_SubStatus__c);
				outputBasicInfo1Map.put('受付担当者', caseObj.Owner.Name);
				if(caseObj.CMN_ReceptionDateTime__c != null){
					String strReceptiDateTime = caseObj.CMN_ReceptionDateTime__c.format('yyyy/MM/dd HH:mm');
					outputBasicInfo1Map.put('受付日時', strReceptiDateTime);
				}
				if(caseObj.CC_SRTypeId__c != null){
					String srTypeNameandSubject = CaseObj.CC_SRTypeId__r.Name + '/' + CaseObj.CC_SRTypeId__r.CC_SRTypeName__c + '/' + CaseObj.CC_SRTypeName__c;
					outputBasicInfo1Map.put('SRタイプ番号SR用件', srTypeNameandSubject);
					outputBasicInfo1Map.put('SRタイプ番号', caseObj.CC_SRTypeId__r.Name);
				}
				if(caseObj.ClosedDate != null){
					String strClosedDate = caseObj.ClosedDate.format('yyyy/MM/dd HH:mm');
					outputBasicInfo1Map.put('完了日時', strClosedDate);
				}
				if(caseObj.RecruiterCD__c != null) outputBasicInfo1Map.put('募集人コード', caseObj.RecruiterCD__c);
				if(caseObj.CMN_ClientName__c != null) outputBasicInfo1Map.put('申出人', caseObj.CMN_ClientName__c);
				if(caseObj.CMN_ClientRelation__c != null) outputBasicInfo1Map.put('申出人種別', caseObj.CMN_ClientRelation__c);
				if(caseObj.CMN_Source__c != null) outputBasicInfo1Map.put('情報源', caseObj.CMN_Source__c);
				if(caseObj.CMN_AgencyName__c != null &&
					((caseObj.CMN_AgentName__r.Account.Z1OFFING__c != null && caseObj.CMN_AgentName__r.Account.Z1OFFING__c.equals('Y')) ||
					 (caseObj.CMN_AgentName__r.Account.Parent.Z1OFFING__c != null && caseObj.CMN_AgentName__r.Account.Parent.Z1OFFING__c.equals('Y')))
				){
					outputBasicInfo1Map.put('代理店事務所コード', caseObj.CMN_AgencyName__r.E_CL2PF_ZAGCYNUM__c + '/' + caseObj.CMN_AgencyName__r.Name);
				}

				outputBasicInfo1Json = outputBasicInfo1Json + JSON.Serialize(outputBasicInfo1Map) + ']';
				System.debug('基本情報セクション1 '+outputBasicInfo1Json);


				//SR作成:基本情報セクション2
				String outputBasicInfo2Json = '"基本情報セクション2":[';
				Map<String, Object> outputbasicInfo2Map = new Map<String, Object>();

				outputbasicInfo2Map.put('コメント1', caseObj.CMN_Comment1__c);
				outputbasicInfo2Map.put('コメント2', caseObj.CMN_Comment2__c);

				outputBasicInfo2Json = outputBasicInfo2Json + JSON.Serialize(outputbasicInfo2Map) + ']';
				System.debug('基本情報セクション2 '+outputBasicInfo2Json);


				//SR作成:送付先（ブロック）
				String outputShippingJson = '"送付先（ブロック）":[';
				Map<String, Object> outputShippingMap = new Map<String, Object>();

				if(caseObj.CMN_ShippingChoices__c != null) outputShippingMap.put('送付先', caseObj.CMN_ShippingChoices__c);
				if(caseObj.CMN_ShippingTargetDate__c != null) outputShippingMap.put('発送日', caseObj.CMN_ShippingTargetDate__c);
				if(caseObj.CMN_ShippingMethod__c != null) outputShippingMap.put('発送方法', caseObj.CMN_ShippingMethod__c);
				if(caseObj.CMN_SendingEmail__c != null) outputShippingMap.put('送付先Eメール', caseObj.CMN_SendingEmail__c);
				if(caseObj.CC_ContactNameForPrint__c != null) outputShippingMap.put('送付先宛名', caseObj.CC_ContactNameForPrint__c);
				if(caseObj.CMN_ShippingNameTitle__c != null) outputShippingMap.put('送付先敬称', caseObj.CMN_ShippingNameTitle__c);
				if(caseObj.CC_ZipcodeForPrint__c != null) outputShippingMap.put('送付先郵便番号', caseObj.CC_ZipcodeForPrint__c);
				if(caseObj.CC_AddressForPrint__c != null) outputShippingMap.put('送付先住所', caseObj.CC_AddressForPrint__c);

				outputShippingJson = outputShippingJson + JSON.Serialize(outputShippingMap) + ']';
				System.debug('送付先 '+outputShippingJson);


				//SR作成:帳票出力
				String outputFormJson = '"帳票出力":[';
				Map<String, Object> outputFormMap = new Map<String, Object>();

				if(caseObj.CC_CoverLetter__c != null) outputFormMap.put('送付状', caseObj.CC_CoverLetter__r.CC_FormName__c);
				outputFormMap.put('送付状出力', caseObj.CC_IsCoverLetterOutput__c);
				outputFormMap.put('請求書出力', caseObj.CC_IsInvoiceOutput__c);
				outputFormMap.put('確認書出力', caseObj.CC_IsConfirmationFormOutput__c);
				if(caseObj.CC_NoteForCoverletter__c != null) outputFormMap.put('送付状通信欄', caseObj.CC_NoteForCoverletter__c);

				outputFormJson = outputFormJson + JSON.Serialize(outputFormMap) + ']';
				System.debug('帳票出力 '+outputFormJson);


				//SR作成:フリーセクション
				String outputFreeSpaceJson = '"フリーセクション":[';
				Map<String, Object> outputFreeSpaceMap = new Map<String, Object>();

				outputFreeSpaceMap.put('フリースペース1', caseObj.CMN_FreeSpace1__c);
				outputFreeSpaceMap.put('フリースペース2', caseObj.CMN_FreeSpace2__c);

				outputFreeSpaceJson = outputFreeSpaceJson + JSON.Serialize(outputFreeSpaceMap) + ']';
				System.debug('フリーセクション '+outputFreeSpaceJson);


				//SR作成のJSON Mapを出力
				String outputSRJson = '[{' + outputDebugJson + ','
										+ outputBasicInfo1Json + ','
										+ outputBasicInfo2Json + ','
										+ outputShippingJson + ','
										+ outputFormJson + ','
										+ outputFreeSpaceJson +'}]';

				outputMap.put('SR作成', JSON.deserializeUntyped(outputSRJson));

			}

			outputMap.put('targetId', targetId);
		}

		//'帳票出力:Inner'セクションのJSON Mapを出力
		success = extractSRPolicy(inputMap, outputMap, optionMap);

		return success;
	}


	/**
	 * Save Case
	 */
	public static Boolean saveCase(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		//データ設定. Begin//
		Case newCase = new Case();

		//ContextId
		if(String.isNotEmpty((String)inputMap.get('ContextId'))){
			newCase.Id = (String) inputMap.get('ContextId');
		}

		//RecordType:サービスリクエスト
		RecordType recordTypeOfCase = [SELECT Id FROM RecordType where SobjectType = 'Case' and Name = 'サービスリクエスト' limit 1];
		newCase.RecordTypeId = recordTypeOfCase.Id;

		//画面から'帳票出力'実施する場合
		if(optionMap.get('methodName') != null &&
			((String) optionMap.get('methodName') == 'SRFormPrint' || (String) optionMap.get('methodName') == 'SRFormPrintConfirmed')){
			newCase.CC_IsSRFormPrint__c = true;
		}

		//SR作成:基本情報セクション1
		Map<String, Object> inputSrMap=(Map<String, Object>)inputMap.get('SR作成');
		Map<String, Object> inputoutputBasicInfo1Map=(Map<String, Object>)inputSrMap.get('基本情報セクション1');

		if(inputoutputBasicInfo1Map.get('ステータス') != null) newCase.Status = (String) inputoutputBasicInfo1Map.get('ステータス');
		if(inputoutputBasicInfo1Map.get('サブステータス') != null){
			newCase.CC_SubStatus__c = (String) inputoutputBasicInfo1Map.get('サブステータス');
		}else{
			newCase.CC_SubStatus__c = '';
		}
		if(inputoutputBasicInfo1Map.get('申出人') != null) newCase.CMN_ClientName__c = (String) inputoutputBasicInfo1Map.get('申出人');
		if(inputoutputBasicInfo1Map.get('申出人種別') != null){
			newCase.CMN_ClientRelation__c = (String) inputoutputBasicInfo1Map.get('申出人種別');
		}else{
			newCase.CMN_ClientRelation__c = '';
		}
		if(inputoutputBasicInfo1Map.get('情報源') != null) newCase.CMN_Source__c = (String) inputoutputBasicInfo1Map.get('情報源');

		//SR作成:基本情報セクション1:複数代理店フラグ
		Map<String,Object> innerJSONMap = innerJSONProcess(inputMap);
		List<E_Policy__c> listEPolicy = (List<E_Policy__c>)innerJSONMap.get('listEPolicy');
		Boolean isMultipleAgency = false;
		Set<Id> mainAgentSet = new Set<Id>();
		for(E_Policy__c ePolicyObj : listEPolicy){
			//共同募集の場合
			if(ePolicyObj.SubAgentAccount__c != null){
				isMultipleAgency = true;
				break;
			}
			//複数代理店の場合
			else if(ePolicyObj.MainAgentAccount__c != null){
				mainAgentSet.add(ePolicyObj.MainAgentAccount__c);
			}
		}

		if(!isMultipleAgency && mainAgentSet.size() > 1){
			isMultipleAgency = true;
		}
		newCase.CMN_IsMultipleAgency__c = isMultipleAgency;

		//SR作成:基本情報セクション1:SRタイプ番号/SR用件
		if(inputoutputBasicInfo1Map.get('SRタイプ番号SR用件') != null){
			String SRTypeNameAndSubject = (String) inputoutputBasicInfo1Map.get('SRタイプ番号SR用件');
			CC_SRTypeMaster__c SRTypeMasterObj = CC_CaseEditVOIDao.getSRTypeMasterByName(SRTypeNameAndSubject);
			if(srTypeMasterObj == null || String.isEmpty(srTypeMasterObj.Id)){
				newCase.CC_SRTypeId__c = '000000000kariId'; //バリデーションための仮ID
			}else{
				newCase.CC_SRTypeId__c = SRTypeMasterObj.Id;
				newCase.CMN_IsClaimed__c = SRTypeMasterObj.CC_IsComplaintSR__c;
			}
		}

		//SR作成:基本情報セクション1:募集人コード
		if(inputoutputBasicInfo1Map.get('募集人コード') != null){
			String agentCode = (String) inputoutputBasicInfo1Map.get('募集人コード');
			Contact contactObj = CC_CaseEditVOIDao.getAgentByAgentCode(agentCode);
			newCase.RecruiterCD__c = agentCode;													 //募集人コード
			newCase.CMN_AgentName__c = contactObj == null ? null : contactObj.Id;				   //募集人名

			//代理店名
			if((contactObj.Account.Z1OFFING__c == null || String.isBlank(contactObj.Account.Z1OFFING__c)) &&
			   (contactObj.Account.Parent.Z1OFFING__c == null || String.isBlank(contactObj.Account.Parent.Z1OFFING__c))
			   ){
				newCase.CMN_AgencyName__c = contactObj == null ? null : contactObj.AccountId;
			}else{
				//SR作成:基本情報セクション1:代理店事務所コード
				if(inputoutputBasicInfo1Map.get('代理店事務所コード') != null){
					String agencyCode = (String) inputoutputBasicInfo1Map.get('代理店事務所コード');
					agencyCode = agencyCode.substringBefore('/');
					Account accountObj = CC_CaseEditVOIDao.getAccountByAgencyCode(agencyCode);
					if(accountObj != null && !String.isEmpty(accountObj.Id)){
						newCase.CMN_AgencyName__c = accountObj.Id;
					}else{
						newCase.CMN_AgencyName__c = '000000000kariId'; //バリデーションための仮ID
					}
				}
			}
		}else{
			newCase.RecruiterCD__c = '';
			newCase.CMN_AgentName__c = Null;
			newCase.CMN_AgencyName__c = Null;

			if(inputoutputBasicInfo1Map.get('代理店事務所コード') != null){
				newCase.RecruiterCD__c = 'kariCode'; //バリデーションための仮コード
			}
		}

		//SR作成:基本情報セクション2
		Map<String, Object> inputBasicInfo2Map = (Map<String, Object>)inputSrMap.get('基本情報セクション2');
		if(inputBasicInfo2Map.get('コメント1') != null) newCase.CMN_Comment1__c = (String) inputBasicInfo2Map.get('コメント1');
		if(inputBasicInfo2Map.get('コメント2') != null) newCase.CMN_Comment2__c = (String) inputBasicInfo2Map.get('コメント2');

		//SR作成:送付先（ブロック）
		Map<String, Object> inputShippingMap=(Map<String, Object>)inputSrMap.get('送付先（ブロック）');
		if(inputShippingMap.get('送付先') != null){
			newCase.CMN_ShippingChoices__c = (String) inputShippingMap.get('送付先');
		}else{
			newCase.CMN_ShippingChoices__c = '';
		}
		if(inputShippingMap.get('発送日') != null){
			newCase.CMN_ShippingTargetDate__c = (String) inputShippingMap.get('発送日');
		}else{
			newCase.CMN_ShippingTargetDate__c = '';
		}
		if(inputShippingMap.get('発送方法') != null){
			newCase.CMN_ShippingMethod__c = (String) inputShippingMap.get('発送方法');
		}else{
			newCase.CMN_ShippingMethod__c = '';
		}
		if(inputShippingMap.get('送付先Eメール') != null) newCase.CMN_SendingEmail__c = (String) inputShippingMap.get('送付先Eメール');
		if(inputShippingMap.get('送付先宛名') != null) newCase.CC_ContactNameForPrint__c = (String) inputShippingMap.get('送付先宛名');
		if(inputShippingMap.get('送付先敬称') != null){
			newCase.CMN_ShippingNameTitle__c = (String) inputShippingMap.get('送付先敬称');
		}else{
			newCase.CMN_ShippingNameTitle__c = '';
		}
		if(inputShippingMap.get('送付先郵便番号') != null) newCase.CC_ZipcodeForPrint__c = (String) inputShippingMap.get('送付先郵便番号');
		if(inputShippingMap.get('送付先住所') != null) newCase.CC_AddressForPrint__c = (String) inputShippingMap.get('送付先住所');

		//SR作成:帳票出力
		Map<String, Object> inputFormMap=(Map<String, Object>)inputSrMap.get('帳票出力');
		newCase.CC_IsCoverLetterOutput__c = (Boolean) inputFormMap.get('送付状出力');
		newCase.CC_IsInvoiceOutput__c = (Boolean) inputFormMap.get('請求書出力');
		newCase.CC_IsConfirmationFormOutput__c = (Boolean) inputFormMap.get('確認書出力');
		newCase.CC_NoteForCoverletter__c = (String) inputFormMap.get('送付状通信欄');

		//SR作成:帳票出力:送付状
		if(inputFormMap.get('送付状') != null && inputFormMap.get('送付状') != ''){
			String coverLetter = (String) inputFormMap.get('送付状');
			CC_FormMaster__c formMaster = CC_CaseEditVOIDao.getCoverLetterByFormName(coverLetter);
			if(formMaster != null && !String.isEmpty(formMaster.Id)){
				newCase.CC_CoverLetter__c = formMaster.Id;
			}else{
				newCase.CC_CoverLetter__c = '000000000kariId'; //バリデーションための仮ID
			}
		}else{
			newCase.CC_CoverLetter__c = Null;
		}

		//SR作成:フリーセクション
		Map<String, Object> inputFreeSpaceMap=(Map<String, Object>)inputSrMap.get('フリーセクション');
		if(inputFreeSpaceMap.get('フリースペース1') != null) newCase.CMN_FreeSpace1__c = (String) inputFreeSpaceMap.get('フリースペース1');
		if(inputFreeSpaceMap.get('フリースペース2') != null) newCase.CMN_FreeSpace2__c = (String) inputFreeSpaceMap.get('フリースペース2');

		//データ設定. End//

		//バリデーションチェック. Begin//
		if(optionMap.get('methodName') != null &&
		   (String) optionMap.get('methodName') != 'saveCaseConfirmed' &&
		   (String) optionMap.get('methodName') != 'SRFormPrintConfirmed' ){

			//SRバリデーション
			List<Case> newList = new List<Case>();
			newList.add(newCase);
			Map<Id,Case> newMap = new Map<Id,Case>{newCase.Id => newCase};
			Boolean triggerFlag = false;
			String inputAlertMessage = CC_CaseValid.alertCheck(newCase);
			Map<String, Object> outputValidMap = CC_CaseValid.errorCheckAndOutput(newList, newMap, triggerFlag, inputAlertMessage);

			//SRPolicyバリデーション
			outputValidMap = CC_SRPolicyValid.errorCheckAndOutput(newCase, outputValidMap, innerJSONMap);
			outputMap.putAll(outputValidMap);
		}else{
			outputMap.put('showValidationError', false);
			outputMap.put('showValidationAlert', false);
		}
		//バリデーションチェック. End//

		//データ設定完了後の処理. Begin//

		//エラーやアラートない場合
		if(outputMap.get('showValidationError') == false && outputMap.get('showValidationAlert') == false){
			//Insert/Update Case
			system.debug('save Case: newCase' + newCase);
			Id newCaseId;
			Case newCaseObj = new Case();

			//トリガバリデーション実行させないため
			newCase.CC_PreventTriggerValid__c = true;

			if(newCase.Id == null){
				//SR作成:基本情報セクション1:受付日時
				if(inputoutputBasicInfo1Map.get('受付日時') != null){
					String receptiDateTimeStr = (String) inputoutputBasicInfo1Map.get('受付日時');
					newCase.CMN_ReceptionDateTime__c = DateTime.parse(receptiDateTimeStr);
				}

				//SR作成:Debug
				Map<String, Object> inputDebugMap = (Map<String, Object>)inputSrMap.get('Debug');
				if(inputDebugMap.get('SYSCorporateName') != null) newCase.CMN_CorporateName__c = (String) inputDebugMap.get('SYSCorporateName');	//権利者名(法人)
				if(inputDebugMap.get('SYSContactName') != null) newCase.CMN_ContactName__c = (String) inputDebugMap.get('SYSContactName');		  //権利者名(個人)

				Database.Insert(newCase, false);
				newCaseId = newCase.Id;
				inputMap.put('DRId_Case', (Object)newCaseId);
				newCaseObj = [select CaseNumber from Case where id =: newCaseId];

			}else{
				Database.Update(newCase, false);
			}

			//Delete and Create SRPolicy
			success = renewalSRPolicy(inputMap, outputMap, innerJSONMap);

			//画面遷移処理 Begin. //
			//新規Case
			if(newCaseId != null){
				outputMap.put('isNewCase', true);

				//Open PrimaryTab of New Case
				Map<String, Object> pageReferenceMapForPrimaryTab = new Map<String, Object>();
				pageReferenceMapForPrimaryTab.put('url', '/' + (String)newCaseId + '?isdtp=vw');
				pageReferenceMapForPrimaryTab.put('tabLabel', newCaseObj.CaseNumber);
				outputMap.put('pageReferenceMapForPrimaryTab', pageReferenceMapForPrimaryTab);

			//既存Case
			}else{
				outputMap.put('isNewCase', false);
			}

			//Open SubTab of '帳票出力'
			if(newCase.CC_IsSRFormPrint__c){
				Map<String, Object> pageReferenceMapForSubTab = new Map<String, Object>();
				pageReferenceMapForSubTab.put('url', '/' + 'apex/CC_SRFormPrint?srid='+ (String)newCase.Id);
				pageReferenceMapForSubTab.put('tabLabel', '帳票出力');
				outputMap.put('pageReferenceMapForSubTab', pageReferenceMapForSubTab);
			}
			//画面遷移処理 End. //
		}
		//データ設定完了後の処理. End//

		System.debug('saveCase->outputMap '+ outputMap);
		return success;
	}

	/**
	 * Delete and Create SRPolicy
	 */
	public static Boolean renewalSRPolicy(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		String caseId;
		if(String.isNotEmpty((String)inputMap.get('ContextId'))){
			caseId = (String) inputMap.get('ContextId');
		}else{
			caseId = (String) inputMap.get('DRId_Case');
		}

		// Delete all exist SRPolicy Begin. //
		if(caseId != null){
			List<CC_SRPolicy__c> listSREPolicy = CC_CaseEditVOIDao.getSRPolicyListByCaseId(caseId);

			if(listSREPolicy.size() > 0){
				Delete listSREPolicy;
				Database.emptyRecycleBin(listSREPolicy);
			}
		}
		// Delete all exist SRPolicy End. //

		// Insert SRPolicy Begin. //
		List<CC_SRPolicy__c> srPolicyList = new List<CC_SRPolicy__c>();

		//'Inner'JSON分析整理後のデータ取得
		Map<Integer, List<Object>> allPolicyMap = (Map<Integer, List<Object>>)optionMap.get('allPolicyMap');
		Map<Integer, String> allFormMaster1Map = (Map<Integer, String>)optionMap.get('allFormMaster1Map');
		Map<Integer, String> allFormMaster2Map = (Map<Integer, String>)optionMap.get('allFormMaster2Map');
		List<E_Policy__c> listEPolicy = (List<E_Policy__c>)optionMap.get('listEPolicy');
		List<CC_FormMaster__c> FORMMASTERList = (List<CC_FormMaster__c>)optionMap.get('FORMMASTERList');

		//新規SRPolicyデータリストの作成
		if(listEPolicy.size() > 0){
			for(Integer i : allPolicyMap.keySet()){
				CC_SRPolicy__c srpolicy = new CC_SRPolicy__c();

				//Set PolicyID 1~4
				List<Object> inputPolicyList = allPolicyMap.get(i);
				Integer policyIdNum = 1;
				for(Object policy : inputPolicyList){
					String temPolicy = (String)policy;

					for(E_Policy__c ePolicy : listEPolicy){
						if(temPolicy.equals(ePolicy.COMM_CHDRNUM__c)){
							if(policyIdNum == 1){
								srpolicy.CC_PolicyID1__c = ePolicy.Id;
								srpolicy.CC_PolicyNumber1__c = ePolicy.COMM_CHDRNUM__c;
							}else if(policyIdNum == 2){
								srpolicy.CC_PolicyID2__c = ePolicy.Id;
								srpolicy.CC_PolicyNumber2__c = ePolicy.COMM_CHDRNUM__c;
							}else if(policyIdNum == 3){
								srpolicy.CC_PolicyID3__c = ePolicy.Id;
								srpolicy.CC_PolicyNumber3__c = ePolicy.COMM_CHDRNUM__c;
							}else if(policyIdNum == 4){
								srpolicy.CC_PolicyID4__c = ePolicy.Id;
								srpolicy.CC_PolicyNumber4__c = ePolicy.COMM_CHDRNUM__c;
							}

							break;
						}
					}

					if(policyIdNum < 5){
						policyIdNum++;
					}else{
						break;
					}
				}

				//Set FormMaster1, FormMaster2
				String nameFormMaster1 = allFormMaster1Map.get(i);
				String nameFormMaster2 = allFormMaster2Map.get(i);
				if(FORMMASTERList.size() > 0){
					for(CC_FormMaster__c formMaster : FORMMASTERList){
						if(nameFormMaster1.equals(formMaster.CC_FormName__c)){
							srpolicy.CC_FormMaster1__c = formMaster.Id;
						}
						if(nameFormMaster2.equals(formMaster.CC_FormName__c)){
							srpolicy.CC_FormMaster2__c = formMaster.Id;
						}
					}
				}

				srpolicy.CC_CaseId__c = caseId;
				srpolicy.CC_PreventTriggerValid__c = true;  //トリガバリデーション実行させないため
				srPolicyList.add(srpolicy);
			}
		}

		//Insert SRPolicy
		if(srPolicyList.size() > 0){
			insert srPolicyList;
		}
		// Insert SRPolicy End. //

		system.debug('renewalSRPolicy end');
		return success;
	}


	/**
	 * フォローアップ活動作成
	 */
	public static Boolean createFollowUpSRActivity(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		//Get the new or existing Case Id
		String caseId;
		if(String.isNotEmpty((String)inputMap.get('ContextId'))){
			caseId = (String) inputMap.get('ContextId');
		}else{
			caseId = (String) inputMap.get('DRId_Case');
		}

		//Get 'サブステータス', '送付状出力', '請求書出力'
		Map<String, Object> inputSrMap=(Map<String, Object>)inputMap.get('SR作成');
		Map<String, Object> inputoutputBasicInfo1Map=(Map<String, Object>)inputSrMap.get('基本情報セクション1');
		Map<String, Object> inputFormMap=(Map<String, Object>)inputSrMap.get('帳票出力');
		String subStatus = (String)inputoutputBasicInfo1Map.get('サブステータス');
		Boolean isCoverLetterOutputFlag = (Boolean)inputFormMap.get('送付状出力');
		Boolean isInvoiceOutputFlag = (Boolean)inputFormMap.get('請求書出力');
		//Create FollowUpSRActivity
		if((subStatus == null || !subStatus.equals('不備中')) && (isCoverLetterOutputFlag || isInvoiceOutputFlag)){
			CC_SRActivity__c newSRActivity = new CC_SRActivity__c(
							CC_SRNo__c = caseId,
							CC_ActivityType__c = '書類封緘',
							CC_Status__c = '開始前',
							CC_Order__c = 0,
							OwnerId = UserInfo.getUserId());

			insert newSRActivity;
		}

		return success;
	}


	/**
	 * 'SR作成:帳票出力:Inner' JSONの整理
	 */
	public static Map<String, Object> innerJSONProcess(Map<String,Object> inputMap){
		Map<String, Object> innerDataProcessedMap = new Map<String, Object>();

		Map<String, Object> inputSrMap = (Map<String, Object>)inputMap.get('SR作成');
		Map<String, Object> inputFormMap = (Map<String, Object>)inputSrMap.get('帳票出力');
		List<Object> innerArray = new List<Object>();

		if(inputFormMap.get('Inner') instanceof List<Object>){
			innerArray =(List<Object>)inputFormMap.get('Inner');
		}else{
			innerArray.add(inputFormMap.get('Inner'));
		}

		if(innerArray!= null){
			//JSONデータの整理
			Map<Integer, List<Object>> allPolicyMap = new Map<Integer, List<Object>>();
			Map<Integer, String> allFormMaster1Map = new Map<Integer, String>();
			Map<Integer, String> allFormMaster2Map = new Map<Integer, String>();
			Set<String> allPolicySet = new Set<String>();
			Set<String> allFormMasterSet = new Set<String>();
			Integer innerNum = 1;
			Boolean isPolicyHasNullValue = false;	   //証券番号バリデーション用

			for(Object innerObj: innerArray){
				Map<String, Object> innerObjMap = (Map<String, Object>)innerObj;
				//証券番号
				List<Object> inputPolicyList = new List<Object>();
				if(innerObjMap.get('証券番号') instanceof List<Object>){
					inputPolicyList=(List<Object>)innerObjMap.get('証券番号');
				}else{
					inputPolicyList.add(innerObjMap.get('証券番号'));
				}

				Boolean isSaveThisSR = false;
				List<Object> newInputPolicyList = new List<Object>();
				if(inputPolicyList != null && inputPolicyList.size() > 0){
					for(Object policy : inputPolicyList){
						if(policy != null && policy != ''){
							allPolicySet.add((String)policy);
							newInputPolicyList.add(policy);
							isSaveThisSR = true;
						}
						//証券番号がNull
						else{
							isPolicyHasNullValue = true;
						}
					}
				}

				//証券番号がNullではない場合、SRを作成
				if(isSaveThisSR){

					allPolicyMap.put(innerNum, newInputPolicyList);

					//請求書1
					if(innerObjMap.get('請求書1') != null && innerObjMap.get('請求書1') != ''){
						String nameFormMaster1;
						if(innerObjMap.get('請求書1') instanceof Map<String, Object>){
							Map<String, Object> inputFormMaster1 = (Map<String, Object>)innerObjMap.get('請求書1');
							nameFormMaster1 = (String)inputFormMaster1.get('Name');
						}else{
							nameFormMaster1 = (String)innerObjMap.get('請求書1');
						}
						allFormMasterSet.add(nameFormMaster1);
						allFormMaster1Map.put(innerNum, nameFormMaster1);
					}else{
						allFormMaster1Map.put(innerNum, 'blank');
					}

					//請求書2
					if(innerObjMap.get('請求書2') != null && innerObjMap.get('請求書2') != ''){
						String nameFormMaster2;
						if(innerObjMap.get('請求書2') instanceof Map<String, Object>){
							Map<String, Object> inputFormMaster2 = (Map<String, Object>)innerObjMap.get('請求書2');
							nameFormMaster2 = (String)inputFormMaster2.get('Name');
						}else{
							nameFormMaster2 = (String)innerObjMap.get('請求書2');
						}
						allFormMasterSet.add(nameFormMaster2);
						allFormMaster2Map.put(innerNum, nameFormMaster2);
					}else{
						allFormMaster2Map.put(innerNum, 'blank');
					}
				}

				innerNum ++;
			}

			//Get Master data
			List<E_Policy__c> listEPolicy = new List<E_Policy__c>();
			List<CC_FormMaster__c> FORMMASTERList = new List<CC_FormMaster__c>();
			if(allPolicySet.size() > 0){
				//Get EPolicy Master data
				Map<String, Object> inputDebugMap = (Map<String, Object>)inputSrMap.get('Debug');
				String currentContactId = '';
				String currentCorporateId = '';
				String currentTargetId = '';
				if(String.isNotEmpty((String)inputDebugMap.get('CurrentContactName'))){
					currentContactId = (String) inputDebugMap.get('CurrentContactName');
				}
				if(String.isNotEmpty((String)inputDebugMap.get('CurrentCorporateName'))){
					currentCorporateId = (String) inputDebugMap.get('CurrentCorporateName');
				}
				if(String.isNotEmpty((String) inputMap.get('targetId'))){
					currentTargetId = (String) inputMap.get('targetId');
				}

				listEPolicy = CC_CaseEditVOIDao.getEPolicyListByCHDRNUMSet(allPolicySet, currentContactId, currentCorporateId, currentTargetId);

				//Get FormMaster Master data
				Map<String, Object> inputoutputBasicInfo1Map = (Map<String, Object>)inputSrMap.get('基本情報セクション1');

				String srTypeNo = '';
				if(String.isNotEmpty((String) inputoutputBasicInfo1Map.get('SRタイプ番号SR用件'))){
					String srTypeNameAndSubject = (String) inputoutputBasicInfo1Map.get('SRタイプ番号SR用件');
					srTypeNo = srTypeNameAndSubject.substringBefore('/');
				}
				FORMMASTERList = CC_CaseEditVOIDao.getFormMasterListByFormNameSet(allFormMasterSet, srTypeNo);
			}

			innerDataProcessedMap.put('allPolicyMap', (Object)allPolicyMap);							//行目，証券番号リスト（空白値除外）
			innerDataProcessedMap.put('allFormMaster1Map', (Object)allFormMaster1Map);				  //行目，請求書1（空白値含み）
			innerDataProcessedMap.put('allFormMaster2Map', (Object)allFormMaster2Map);				  //行目，請求書2（空白値含み）
			innerDataProcessedMap.put('listEPolicy', (Object)listEPolicy);							  //証券番号マスタデータリスト
			innerDataProcessedMap.put('FORMMASTERList', (Object)FORMMASTERList);						//請求書マスタデータリスト
			innerDataProcessedMap.put('allPolicySet', (Object)allPolicySet);							//証券番号リスト（空白値除外、重複なし）
			innerDataProcessedMap.put('allFormMasterSet', (Object)allFormMasterSet);					//請求書1&2リスト（空白値除外、重複なし）
			innerDataProcessedMap.put('isPolicyHasNullValue', (Object)isPolicyHasNullValue);			//すべての証券番号に空白値があるかの判断
		}

		return innerDataProcessedMap;
	}


	/**
	 * Extract SRPolicy data for initial CC_CaseEdit OmniScript
	 */
	public static Boolean extractSRPolicy(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		String strPolicyId= (String) inputMap.get('selectedPolicyIdList');
		List<Id> listPolicyId = new List<Id>();

		Map<String, String> outputObj = new Map<String, String>();
		Map<String, List<Object>> outputObjList = new Map<String, List<Object>>();
		String strJson = '[';

		//Initial screen.
		if(String.isBlank(strPolicyId) || strPolicyId == null){
			String caseId = (String) inputMap.get('ContextId');
			List<CC_SRPolicy__c> listSREPolicy = CC_CaseEditVOIDao.getSRPolicyListByCaseId(caseId);

			if(listSREPolicy.size() > 0){
				for(CC_SRPolicy__c srpolicy : listSREPolicy){
					//If PolicyID2/PolicyID3/PolicyID4 are existing.
					if(srpolicy.CC_PolicyNumber2__c != null || srpolicy.CC_PolicyNumber3__c != null || srpolicy.CC_PolicyNumber4__c != null){
						List<object> policyJsonList = new List<String>();
						List<object> formMastJsonList1 = new List<String>();
						List<object> formMastJsonList2 = new List<String>();

						policyJsonList.add(srpolicy.CC_PolicyNumber1__c);
						if(srpolicy.CC_PolicyNumber2__c != null) { policyJsonList.add(srpolicy.CC_PolicyNumber2__c); }
						if(srpolicy.CC_PolicyNumber3__c != null) { policyJsonList.add(srpolicy.CC_PolicyNumber3__c); }
						if(srpolicy.CC_PolicyNumber4__c != null) { policyJsonList.add(srpolicy.CC_PolicyNumber4__c); }

						formMastJsonList1.add(srpolicy.CC_FormMaster1__r.CC_FormName__c);
						formMastJsonList2.add(srpolicy.CC_FormMaster2__r.CC_FormName__c);

						outputObjList.put('証券番号', policyJsonList);
						outputObjList.put('請求書1', formMastJsonList1);
						outputObjList.put('請求書2', formMastJsonList2);

						strJson = strJson + JSON.Serialize(outputObjList) + ',';
					}else{
						outputObj.put('証券番号', srpolicy.CC_PolicyNumber1__c);
						outputObj.put('請求書1', srpolicy.CC_FormMaster1__r.CC_FormName__c);
						outputObj.put('請求書2', srpolicy.CC_FormMaster2__r.CC_FormName__c);

						strJson = strJson + JSON.Serialize(outputObj) + ',';
					}
				}
			}

		//Opened from PolicyDetail screen.
		}else{
			listPolicyId = strPolicyId.split(',');
			List<E_Policy__c> listEPolicy = CC_CaseEditVOIDao.getEPolicyListByIdList(listPolicyId);

			for(E_Policy__c ePolicy : listEPolicy ){
				outputObj.put('証券番号', ePolicy.COMM_CHDRNUM__c);
				strJson = strJson + JSON.Serialize(outputObj) + ',';
			}
		}

		if(!strJson.equals('[')){
			strJson = strJson .substring(0,strJson.length()-1) + ']';
			outputMap.put('Inner', JSON.deserializeUntyped(strJson));
		}

		return success;
	}

	/**
	 * Get TypeAhead data of 代理店事務所コード
	 */
	public static Boolean getAgencyTypeAhead(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		System.debug('getAgencyTypeAhead -> inputMap: '+inputMap);
		Boolean success = true;
		Map<String,Object> agencyMap = new Map<String,Object>();
		List<Object> agencyCodeList = new List<Object>();

		Map<String,Object> drParamsMap = (Map<String,Object>)inputMap.get('DRParams');
		String agentCode = (String)drParamsMap.get('AgentCode');
		if(String.isNotEmpty(agentCode)){
			Contact agent = CC_CaseEditVOIDao.getAgentByAgentCode(agentCode);
			if(agent != null && agent.AccountId != null){
				List<Account> agencyList = CC_CaseEditVOIDao.getAgencyNumList(agent.Account.Parent.E_CL1PF_ZHEADAY__c, agent.Account.E_CL1PF_ZHEADAY__c, agent.AccountId);

				if(agencyList != null && agencyList.size() > 0){
					for(Account agency : agencyList){
						//代理店事務所コード/代理店名
						String strName = agency.E_CL2PF_ZAGCYNUM__c + '/' + agency.Name;
						Map<String, String> strNameMap = new Map<String, String>{'Name' => strName};
						agencyCodeList.add((Object)strNameMap);
					}
				}
			}
		}

		agencyMap.put('Agency', agencyCodeList);
		outputMap.put('OBDRresp', agencyMap);
		System.debug('getAgencyTypeAhead -> outputMap : '+outputMap);
		return success;
	}

}