@isTest
private class SendQrCodeEmailTest {
   static testMethod void inputEmailParams() {
     
        SendQrCodeEmail send_email = new SendQrCodeEmail();
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();
         
        inputParams.put('qr_code_url', '12345');
        inputParams.put('user_email_address', 'blah@blah.com');
        inputParams.put('email_subject', 'blah');
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result;
        result = send_email.invoke(request);
        
        System.AssertEquals(3,3);
    }

    static testMethod void describeTest() {

        SendQrCodeEmail send_email = new SendQrCodeEmail();
        Process.PluginDescribeResult result = send_email.describe();
        
        System.AssertEquals(result.inputParameters.size(), 3);
     }
}