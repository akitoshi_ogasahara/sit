@isTest
private class TestI_FeatureListController {
	
	@isTest static void getIrisMenuIdTest() {
		I_ContentMaster__c icm = createData();

		Test.startTest();
			I_FeatureListController flc = new I_FeatureListController();
			System.assertEquals(flc.cpmDisplayFlag[0],false);
		Test.stopTest();
	}

	@isTest static void getIrisMenuIdTest2() {
		//I_ContentMaster__c icm = createData();
		I_ContentMaster__c icm2 = createData2();

		Test.startTest();
			I_FeatureListController flc = new I_FeatureListController();
			System.assertEquals(flc.cpmDisplayFlag[0],true);
		Test.stopTest();
	}

	//データ作成
	static I_ContentMaster__c createData(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		I_ContentMaster__c icm = createIRISCMS(ipm);
		//ID管理
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		return icm;
	}


	//データ作成
	static I_ContentMaster__c createData2(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		I_PageMaster__c ipm2 = createIRISPage2();
		I_ContentMaster__c icm = createIRISCMS2(ipm,ipm2);
		//ID管理
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		return icm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.Menu__c = mmId;
		pm.Default__c = true;
		insert pm;
		return pm;
	}

	//irisPage2
	static I_PageMaster__c createIRISPage2(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey1';
		pm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		pm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS(icm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		insert icm;
		return icm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS2(I_PageMaster__c ipm,I_PageMaster__c ipm2){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS2(icm,ipm2);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS2(I_ContentMaster__c paricm,I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.IRIS_Page__c = ipm.Id;
		insert icm;
		return icm;
	}

	static I_MenuMaster__c createIRISMenuMaster(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = '特集';
		insert mm;
		return mm;
	}
}