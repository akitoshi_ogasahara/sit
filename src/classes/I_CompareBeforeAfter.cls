public with sharing class I_CompareBeforeAfter {
    private final String RESOURCES_NAME = 'CompareRule';    // 静的リソース名

    private List<String> regs = new List<String>();         // 変更履歴対象ルール
    private String objectName;                              // 変更履歴対象オブジェクト
    private String historyObjectName;                       // 変更履歴保存先オブジェクト
    private String historyParentName;                       // 参照項目API参照名

    /*
     * コンストラクタ
     *
     * @param sObjectName   対象オブジェクトのAPI参照名
     */
    public I_CompareBeforeAfter(String sObjectName) {
        getStaticResource(sObjectName);
    }

    /*
     * 変更履歴レコード作成
     *
     * @param oldMap    更新前レコードMap
     * @param newMap    更新後レコードMap 
     */
    public void compare(Map<Id, sObject> oldMap, Map<id, sObject> newMap) {
        // 履歴取得対象オブジェクトの情報取得
        Map<String, Schema.SObjectType> smap = Schema.getGlobalDescribe();
        Schema.SObjectType sobj = smap.get(objectName);
        Schema.DescribeSObjectResult sr = sobj.getDescribe();
        Map<String, Schema.SObjectField> fmap = sr.fields.getMap();

        // 履歴保存先オブジェクトの情報取得
        Schema.SObjectType historyObj = smap.get(historyObjectName);

       // 履歴取得対象項目リスト作成
        Set<String> fieldNames = new Set<String>();
        for (String reg : regs) {
            Pattern p = Pattern.compile(reg.toLowerCase());
            for (String key : fmap.keySet()) {
                Matcher m = p.matcher(key.toLowerCase());
                // 項目のAPI参照名が静的リソースのルールに一致する場合は履歴取得対象
                if (m.matches()) {
                    fieldNames.add(key);
                }
            }
        }
        system.debug('debug:fieldNames=' + fieldNames);
        system.debug('debug:historyParentName=' + historyParentName);

        List<sObject> histories = new List<sObject>();
        for (sObject record : newMap.values()) {
            for (String fieldName : fieldNames) {
                if (record.get(fieldName) != oldMap.get(record.id).get(fieldName)) {
                    system.debug('debug:fieldName=' + fieldName +' / old=' + oldMap.get(record.id).get(fieldName) + ' / new=' + record.get(fieldName));
                    
                    // 対象項目のラベル名
                    Schema.DescribeFieldResult fr = fmap.get(fieldName).getDescribe();
                    String filedLabel = fr.getLabel();

                    // 履歴オブジェクトの項目セット
                    sObject history = historyObj.newSObject();
                    history.put(historyParentName, record.Id);
                    history.put('FieldName__c', filedLabel);
                    history.put('OldValue__c', String.valueOf(oldMap.get(record.id).get(fieldName)));
                    history.put('NewValue__c', String.valueOf(record.get(fieldName)));
                    history.put('ChangeDatetime__c', system.now());
                    histories.add(history);
                }
            }
        }

        if (!histories.isEmpty()) {
            insert histories;
        }
    }

    /*
     * 静的リソース取得
     *
     * @param sObjectName   対象オブジェクトのAPI参照名
     */
    private void getStaticResource(String sObjectName) {
        StaticResource sr = [SELECT Id,Body FROM StaticResource WHERE Name = :RESOURCES_NAME LIMIT 1];
        system.debug('debug:body=' + sr.Body.toString());

        List<RegJson> jsonList = (List<RegJson>)JSON.deserialize(sr.Body.toString(), List<RegJson>.class);
        system.debug('debug:jsonList=' + jsonList);

        for (RegJson json : jsonList) {
            if (json.objectName == sObjectName) {
                objectName = json.objectName;
                regs = json.conditions;
                historyObjectName = json.historyObjectName;
                historyParentName = json.historyParentName;
                break;
            }
        }
    }


    // jsonに定義されている構成
    public class RegJson {
        public String objectName;           // 履歴取得対象オブジェクトAPI参照名
        public String historyObjectName;    // 履歴オブジェクトAPI参照名
        public String historyParentName;    // 履歴オブジェクトの親とのリレーション項目API参照名
        public List<String> conditions;     // 履歴取得条件
    }
}