global class I_PushOutBatch implements Database.Batchable<SObject>, Database.Stateful{
	
	protected Id ebizLogId;
	protected I_PushOutBatchOperation operation;
	protected List<String> errorMessages;
	protected List<I_PushOut__c> pushRecs;
    
    /**
     * Constructor
     */
    global I_PushOutBatch(Id logId){
		ebizLogId = logId;
		errorMessages = new List<String>();
		pushRecs = new List<I_PushOut__c>();
    }

	/**
	 * BatchApexの開始処理です
	 * @param ctx
	 * @return Database.QueryLocator 
	 */
	global Database.QueryLocator start(Database.BatchableContext ctx){
		E_BizDataSyncLog__c log;
		try{
			// EBiz連携ログの開始日時更新
			log = E_BizDataSyncLogDao.getRecById(ebizLogId);
			log.DataSyncStartDate__c = System.Now();
			update log;
	
			// バッチ処理クラスの生成
			operation = new I_PushOutBatchOperation(log);
			
			return operation.getQueryLocator();
			
		}catch(Exception e){
			//startメソッド内のエラーは、errorMessagesに格納され、finish時に設定される。
			//		開始時刻は未設定となる。ExecuteはダミーLocatorの結果実行されない。
			errorMessages.add(String.format('Operationクラスの生成に失敗しました。(クラス名:{0}){1}', new List<String>{log.operationClassName__c, e.getMessage()}));
		}
		
		//Operationクラスの取得が正常に行われない場合、ダミーのLocatorを返す。取得できるレコードは0件のもの
		return Database.getQueryLocator([select id from user where id = null limit 1]);
	}
    
	/**
	 * BatchApexの処理実行します
	 * @param ctx
	 * @param records
	 * @return
	 */
	global void execute(Database.BatchableContext ctx, List<Sobject> records) {
		system.debug('*** I_PushOutBatch.execute *** ');
		try{
			List<I_PushOut__c> results = operation.execute(records);
			pushRecs.addAll(results);
			
		}catch(Exception e){
			errorMessages.add(e.getMessage());
		}
	}

	/**
	 * BatchApexの後処理
	 * @param ctx
	 * @return
	 */
	global virtual void finish(Database.BatchableContext ctx) {
		system.debug('*** I_PushOutBatch.finish:pushRecs.size *** ' + pushRecs.size());
		try{
			operation.finish(pushRecs);
		}catch(Exception e){
			errorMessages.add(e.getMessage());
		}
		
		// 連携ログの更新　連携終了日時は送信バッチにて更新する
		E_BizDataSyncLog__c ebizlog = E_BizDataSyncLogDao.getRecById(ebizLogId);
		if(errorMessages.size()>0){
			ebizlog.log__c = String.join(errorMessages, ',');
		}
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		ebizlog.setOptions(dml);
		update ebizlog;
		
		// PushOut通知　メール送信バッチ呼び出し
		operation.callSendBatch(ebizlog);
	}
}