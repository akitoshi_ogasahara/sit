@isTest
private with sharing class TestE_SpecialAccountController{
	private static testMethod void testPageMethods() {		E_SpecialAccountController extension = new E_SpecialAccountController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testdataTableSVFPF() {
		E_SpecialAccountController.dataTableSVFPF dataTableSVFPF = new E_SpecialAccountController.dataTableSVFPF(new List<E_SVFPF__c>(), new List<E_SpecialAccountController.dataTableSVFPFItem>(), new List<E_SVFPF__c>(), null);
		dataTableSVFPF.create(new E_SVFPF__c());
		System.assert(true);
	}
	
}