public with sharing class I_ChangeUserIDCmpl extends E_AbstractController{

	//遷移先URL
	public String sendUrl {get;set;}

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}

	//正常動作チェック
	public Boolean correctCheck{get;set;}

	//メールアドレス、変更日時チェック
	public Boolean errorCheck{get;set;}

	//ブラウザ判定Handler
	public E_BrowserJudgeHandler browserHandler {get; set;}

	public I_ChangeUserIDCmpl() {
		pageMessages = getPageMessages();

		////ページ表記のフラグ
		correctCheck = false;
		errorCheck = false;

//      pgTitle = 'ログインID変更ページ';    
		pgTitle = 'ID変更完了';
	}

	/**
	 * urlからログインID、パスワード、メールアドレス、IDリクエストのレコードIDを取得
	 * UserのEmail項目、ID管理のEmail項目を更新、IDリクエストの変更受付日時を更新
	 * 変更用の　E_ADRPF__cレコードを作成
	 */
	public void init() {

		//現在時刻がサービス時間外ならエラーを表示
		if(access.isServiceHourMenu(E_Const.ID_KIND.USER) != null){
			pageMessages.addErrorMessage(getMSG().get('ERR|007'));
			errorCheck = true;
			return;
		}

		//URLパラメータを取得
//      String allPrmStr = URL.getCurrentRequestUrl().getQuery();

		String allPrmStr = ApexPages.CurrentPage().getParameters().get('Param');

		//URLエンコードがされているので、デコードを行う
		allPrmStr = EncodingUtil.urlDecode(allPrmStr , 'UTF-8');
		//半角スペースは cryptoデコードを行えないので、'+'へ変換する
		allPrmStr = allPrmStr.replace(' ', '+');

		//取得したURLを復号化
		String decrypted = E_EncryptUtil.getDecryptedString(allPrmStr);

		/**
		 * 復号化したURLのパラメータを「&」で区切る
		 * ↓↓↓↓URLのパラメータパターン↓↓↓↓
		 * recId=xxx&url=xxx
		 * recId=xxx 16/11/22修正
		 */
		String[] andSplit = decrypted.split('&',0);

		//IRリクエストのレコードID
		String[] eidrPrm = andSplit[0].split('=',0);
		String eidrId = eidrPrm[1];
/*
		//次ページのURL
		//インターネット経由ログインとGWログインで遷移先を分岐
		String[] userUrl = andSplit[1].split('=',0);
		String nextUrl = userUrl[1];
*/
		//現在のページのドメインを取得
//      String baseUrl = Site.getDomain();
		URL urlObj = URL.getCurrentRequestUrl();
		String baseUrl = urlObj.getHost();

		//現在のユーザのサイト名を取得
		String siteName = urlObj.getPath().substringBefore('/apex');
		siteName = siteName.removeStart('/');

		//次ページへのURL
		String nextFullUrl;

		//Site'nnlinkage' と ユーザのSiteレコードを取得
		List<Site> userSite = [SELECT GuestUserId, Name, Subdomain, UrlPathPrefix,Description,ID FROM Site WHERE Name = :siteName];
		List<Site> nnSite = [SELECT GuestUserId, Name, Subdomain, UrlPathPrefix FROM Site WHERE Name = :E_Const.NNLINK_AGE];

		//それぞれのSubDomainを格納
		String userSubDom = '';//userSite[0].Subdomain;
		String nnSubDom = '';//nnSite[0].Subdomain;

		if(!userSite.isEmpty()){
			userSubDom = userSite[0].Subdomain;
		}
		if(!nnSite.isEMpty()){
			nnSubDom = nnSite[0].Subdomain;
		}

		// secureを除外 20161228
		baseUrl = baseUrl.replace('.secure', '');

		//SubDomainを置き換える
		String siteFullUrl = baseUrl.replace(userSubDom, nnSubDom);

		//Domain以外の部分を結合
		String siteNN = 'https://' + siteFullUrl + '/'+ E_Const.NNLINK_AGE;
		//次ページへのURL
		nextFullUrl = siteNN + '/E_Home';

		if(isCommonGateway()){
			//Domain以外の部分を結合
			String siteGW = 'https://' + siteFullUrl + '/'+ E_Const.NNLINK_CGWAGE;
			//次ページへのURL
			nextFullUrl = siteGW + '/E_Home';

			nextFullUrl = E_util.getURLForGWTB(nextFullUrl);
		}

		//セーブポイント
		Savepoint sp = Database.setSavepoint();
		try{
			//IDリクエスト情報を取得
			E_IDRequest__c eidr = E_IDRequestDaoWithout.getRecById(eidrId);

			//一致するIDリクエストが見つからなかった場合、エラーを表示
			if(eidr == null || eidr.dataSyncDate__c != null || eidr.ZUPDFLAG__c != 'E'){
				pageMessages.addErrorMessage(getMSG().get('ERR|009'));
				errorCheck = true;
				return ;
			}

			//ID管理のレコードを取得
			E_IDCPF__c eidc = E_IDCPFDaoWithout.getRecsByZWEBID(eidr.E_IDCPF__c);
			//ユーザを取得
			User userRec = E_UserDaoWithout.getUserRecByUserId(eidr.E_IDCPF__r.User__c);
			String beforeEmail = userRec.EMAIL;
			//ユーザのメールアドレスを更新
			userRec.EMAIL = eidr.ZEMAILAD__c;

			//ユーザのusername をメールアドレス+SUFFIXに更新
			userRec.Username = eidr.ZEMAILAD__c + System.Label.E_USERNAME_SUFFIX;

			//ユーザ情報をupdate(Email項目、Username項目)
			E_UserDaoWithout.updateRec(userRec);

			//ID管理のEmail項目を更新
			E_IDCPFDaoWithout.updateEidc(eidc.Id,eidr.ZEMAILAD__c);

			//顧客情報変更受付履歴レコードを作成（futureメソッド、非同期処理）
			E_ADRPFDaoWithout.insertRec(userRec.Id, eidc.ID);

			//IDリクエストの変更受付日時をupdate（futureメソッド、非同期処理）
			E_IDRequestDaoWithout.updateEidr(eidr.Id,beforeEmail);
			//正常動作確認
			correctCheck = true;

		}catch(Exception e){
			System.debug('e=============' +e);
			pageMessages.addErrorMessage(getMSG().get('ERR|009'));
			Database.rollback(sp);//ロールバック
			errorCheck = true;
			
			return;
		}

		//遷移先ページと暗号化したパラメータを結合
		sendUrl = nextFullUrl;
	}
	/**
	 * 共同Gateway判定
	 */
	private boolean isCommonGateway() {
System.debug('I_ChangeUserIDCmpl isCommonGateway X-Salesforce-SIP=' + ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP'));
System.debug('I_ChangeUserIDCmpl isCommonGateway E_CMN_GW_IP_ADRS=' + System.Label.E_CMN_GW_IP_ADRS);
		return ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP') !=null
			&& ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP').equals(System.Label.E_CMN_GW_IP_ADRS);
	}
}