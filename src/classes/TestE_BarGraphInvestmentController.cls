@isTest
private class TestE_BarGraphInvestmentController {
	
	//半年ボタンを押下
	static testMethod void doHalfYearButtonTest001(){
		
		E_ITHPF__c parent = createTestData();
		E_BarGraphInvestmentController controller = new E_BarGraphInvestmentController();
		controller.parentId = parent.Id;
		
		Test.startTest();
		PageReference result = controller.doHalfYearButton();
		Test.stopTest();
		
		System.assertEquals(null, result);
		System.assertEquals(4, controller.graphRecs.size());
		System.assertEquals(System.now().addMonths(-3).addDays(2).format('yyyy/MM/dd'), controller.graphRecs[0].name);
		System.assertEquals(System.now().addMonths(-3).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[1].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(null, graphData.data3);
			System.assertEquals(null, graphData.data4);
		}
	}
	
	//2年ボタンを押下
	static testMethod void do2YearButtonTest001(){
		
		E_ITHPF__c parent = createTestData();
		E_BarGraphInvestmentController controller = new E_BarGraphInvestmentController();
		controller.parentId = parent.Id;
		
		Test.startTest();
		PageReference result = controller.do2YearButton();
		Test.stopTest();
		
		System.assertEquals(null, result);
		System.assertEquals(6, controller.graphRecs.size());
		System.assertEquals(System.now().addYears(-1).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[0].name);
		System.assertEquals(System.now().addYears(-1).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[1].name);
		System.assertEquals(System.now().addMonths(-3).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[2].name);
		System.assertEquals(System.now().addMonths(-3).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[3].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(null, graphData.data3);
			System.assertEquals(null, graphData.data4);
		}
	}
	
	//以来ボタンを押下
	static testMethod void doAllButtonTest001(){
		
		E_ITHPF__c parent = createTestData();
		E_BarGraphInvestmentController controller = new E_BarGraphInvestmentController();
		controller.parentId = parent.Id;
		
		Test.startTest();
		PageReference result = controller.doAllButton();
		Test.stopTest();
		
		System.assertEquals(null, result);
		System.assertEquals(8, controller.graphRecs.size());
		System.assertEquals(System.now().addYears(-3).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[0].name);
		System.assertEquals(System.now().addYears(-3).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[1].name);
		System.assertEquals(System.now().addYears(-1).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[2].name);
		System.assertEquals(System.now().addYears(-1).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[3].name);
		System.assertEquals(System.now().addMonths(-3).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[4].name);
		System.assertEquals(System.now().addMonths(-3).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[5].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(null, graphData.data3);
			System.assertEquals(null, graphData.data4);
		}
	}
	
	//グラフ表示データリスト
	static testMethod void getDataListTest001(){
		
		E_ITHPF__c parent = createTestData();
		E_BarGraphInvestmentController controller = new E_BarGraphInvestmentController();
		controller.parentId = parent.Id;
		
		Test.startTest();
		List<E_AbstractBarGraph.GraphData> result = controller.getDataList();
		Test.stopTest();
		
		System.assertEquals(8, controller.graphRecs.size());
		System.assertEquals(System.now().addYears(-3).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[0].name);
		System.assertEquals(System.now().addYears(-3).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[1].name);
		System.assertEquals(System.now().addYears(-1).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[2].name);
		System.assertEquals(System.now().addYears(-1).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[3].name);
		System.assertEquals(System.now().addMonths(-3).addDays(3).format('yyyy/MM/dd'), controller.graphRecs[4].name);
		System.assertEquals(System.now().addMonths(-3).addDays(4).format('yyyy/MM/dd'), controller.graphRecs[5].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(null, graphData.data3);
			System.assertEquals(null, graphData.data4);
		}
	}
	
	//テストデータ作成
	static E_ITHPF__c createTestData(){
		
		List<E_ITBPF__c> insertList = new List<E_ITBPF__c>();
		
		E_ITHPF__c parent = new E_ITHPF__c();
		insert parent;
		
		Datetime threeYearsAgo = System.now().addYears(-3);
		insertList.add(createITBPF(parent.Id, threeYearsAgo.addDays(1), false, false));
		insertList.add(createITBPF(parent.Id, threeYearsAgo.addDays(2), true, false));
		insertList.add(createITBPF(parent.Id, threeYearsAgo.addDays(3), false, true));
		insertList.add(createITBPF(parent.Id, threeYearsAgo.addDays(4), true, true));
		
		Datetime oneYearAgo = System.now().addYears(-1);
		insertList.add(createITBPF(parent.Id, oneYearAgo.addDays(1), false, false));
		insertList.add(createITBPF(parent.Id, oneYearAgo.addDays(2), true, false));
		insertList.add(createITBPF(parent.Id, oneYearAgo.addDays(3), false, true));
		insertList.add(createITBPF(parent.Id, oneYearAgo.addDays(4), true, true));
		
		Datetime threeMonthAgo = System.now().addMonths(-3);
		insertList.add(createITBPF(parent.Id, threeMonthAgo.addDays(1), false, false));
		insertList.add(createITBPF(parent.Id, threeMonthAgo.addDays(2), true, false));
		insertList.add(createITBPF(parent.Id, threeMonthAgo.addDays(3), false, true));
		insertList.add(createITBPF(parent.Id, threeMonthAgo.addDays(4), true, true));
		
		Datetime oneDayLater = System.now().addDays(1);
		insertList.add(createITBPF(parent.Id, oneDayLater.addDays(1), false, false));
		insertList.add(createITBPF(parent.Id, oneDayLater.addDays(2), true, false));
		insertList.add(createITBPF(parent.Id, oneDayLater.addDays(3), false, true));
		insertList.add(createITBPF(parent.Id, oneDayLater.addDays(4), true, true));
		
		insert insertList;
		return parent;
	}
	
	//投信残高レコードを作成
	static E_ITBPF__c createITBPF(Id parentId, Datetime promiseDate, Boolean weekly, Boolean monthly){
		E_ITBPF__c record = new E_ITBPF__c();
		record.E_ITHPF__c = parentId;
		record.ZPRMSDTE__c = promiseDate.format('yyyyMMdd');
		record.FLAG01__c = weekly;
		record.FLAG02__c = monthly;
		record.ZCURRPRC__c = 111;
		record.ZINVAMT__c = 222;
		return record;
	}
	
}