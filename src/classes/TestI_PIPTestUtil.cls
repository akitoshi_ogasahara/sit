@isTest
public class TestI_PIPTestUtil {

    //ProfileName
    private final String PF_SYSTEM = 'システム管理者';
    private static String PF_EMPLOYEE = 'E_EmployeeStandard';
    private static String PF_PARTNER = 'E_PartnerCommunity';

    //User情報
    private static User user;
    private static User thisUser;
    //private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];   //Id取得
    //private static User communityUser;
    private static Account ahAcc;
    private static Account ayAcc;
    private static Account atAcc;
    private static List<Account> ayAccList;
    private static Contact atCon;
    //private static List<E_Policy__c> policyList;
    private static List<E_COVPF__c> covList;
    private static I_ContentMaster__c content;

    /** User作成 */
    public static User createUser(String profileName){
        String userName = 'test@terrasky.ingtesting';
        Profile p = [Select Id From Profile Where Name = :profileName];

        // Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );

        // User
        if(profileName != PF_PARTNER){
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            user.UserRoleId = portalRole.Id;
            insert user;

        // Partner User
        }else{
            user.ContactId = atCon.Id;
            insert user;

            ContactShare cs = new ContactShare(
                            ContactId = atCon.Id,
                            ContactAccessLevel = 'read',
                            UserOrGroupId = user.Id);
            insert cs;
        }
        thisUser = user;
        return user;
    }

    /** データアクセス系作成 */
    public static void createDataAccessObj(Id userId, String idType){
        //system.runAs(thisuser){
        system.runAs(user){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);

            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
                ,ZINQUIRR__c = 'BR**'
                ,AppMode__c = 'IRIS'
            );
            insert idcpf;
        //}
        }
    }

    /** Data作成 */
    public static List<Account> createDataAccount(){
        system.runAs(user){
            // Account 代理店格
            ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
            insert ahAcc;

            // Account 事務所
            ayAccList = new List<Account>();
            for(Integer i = 1; i < 4; i++){
                Account acc = new Account(
                    Name = 'office' + i
                    ,ParentId = ahAcc.Id
                    ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                    ,E_COMM_VALIDFLAG__c = '1'
                    ,AGNTBR_NM__c = '東京'
                );
                ayAccList.add(acc);
            }
            insert ayAccList;

            // Contact 募集人
            if(!ayAccList.isEmpty()){
                atCon = new Contact(LastName = 'test',AccountId = ayAccList[0].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[0].E_CL2PF_ZAGCYNUM__c);
                atCon.E_CL3PF_AGNTNUM__c = 'at001';
                atCon.email = 'fstest@terrasky.ingtesting';
                atCon.e_cltpf_dob__c = '19830101';
                insert atCon;
            }
        }
        return ayAccList;
    }

    public static void updateAccOwner(Account acc){
        //system.runAs(thisuser){
        system.runAs(user){
            acc.OwnerId = user.Id;
            update acc;
        //}
        }
    }

    /**
     * 保険契約ヘッダを複数作成
     *
     */
    public static List<E_Policy__c> createPolicys(){

        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(E_Const.POLICY_RECORDTYPE_COLI);

        // 保険契約ヘッダを作成
        List<E_Policy__c> policys = new List<E_Policy__c>();
        for(Integer i = 0; i < 8; i++){
            E_Policy__c policy = new E_Policy__c();

            policy.Contractor__c = atCon.Id;
            policy.MainAgent__c = atCon.Id;
            policy.Insured__c = atCon.Id;
            //policy.COMM_CHDRNUM__c = '99999999'+i+'|1';
            policy.COMM_CHDRNUM__c = '7654321' + i;
            //データの作り方要検討
            policy.COMM_OCCDATE__c = '20000315';
            policy.MainAgentAccount__c = ayAccList[0].Id;
            policy.COMM_SPLITC01__c = 33;
            policy.SubAgent__c = atCon.Id;
            policy.SubAgentAccount__c = ayAccList[0].Id;
            policy.COMM_SPLITC02__c = 66;
            policy.COMM_ZEBKNM__c = 'テスト金融機関';
            policy.COMM_ZEBKACOW__c = '口座名義人';
            policy.COMM_BANKACCKEY__c = '1234567890';
            policy.COLI_ZADVPDCF__c = true;
            policy.COMM_STATCODE__c = 'R';
            policy.COMM_ZRSTDESC__c = '';
            policy.COMM_ZEFREQ__c = '年 払';
            policy.COMM_ZECHNL__c = '口座振替';
            policy.COLI_ZNPTDDCF__c = true;
            policy.COLI_ZBKTRDCF__c = true;
            policy.COLI_ZBKPODIS__c = '20';
            policy.COLI_ZCSHVAL__c = 10000;
            policy.COLI_ZLOANTOT__c = 1000;
            policy.COLI_ZEADVPRM__c = 100;
            policy.COLI_ZUNPREM__c = 10;
            policy.COLI_ZCVDCF__c = true;
            policy.COLI_ZUNPCF__c = true;
            policy.COLI_ZUNPDCF__c = true;
            policy.COLI_RCPTDT__c = '20171027';
            policy.COLI_AVPRMRCV__c = 10000;
            policy.COLI_REMNBANN__c = 1;
            policy.COLI_ZEPLTOT__c = 10000;
            policy.COLI_ZEPLPRN__c = 1000;
            policy.COLI_ZEPLINT__c = 100;
            policy.COLI_ZPLCAPT__c = '1ヶ月';
            policy.COLI_ZPLRATE__c = 123;
            policy.COMM_ZPLDCF__c = true;
            policy.COLI_ZEAPLTOT__c = 10000;
            policy.COLI_ZEAPLPRN__c = 10000;
            policy.COLI_ZEAPLINT__c = 1000;
            policy.COLI_ZAPLCAPT__c = '1ヶ月';
            policy.COLI_ZAPLRATE__c = 123;
            policy.COLI_ZAPLDCF__c = true;
            policy.COMM_CLNTNUM__c = '12345678';
            policy.COMM_ZCLNAME__c = '契約者A';
            policy.COMM_ZINSNAM__c = '被保険者A';
            policy.COMM_ZKNJSEX__c = '男性';
            policy.COMM_DOB__c = '20171027';
            policy.RecordTypeId = recTypeId;
            policy.COMM_SPLITC__c = 55;
            policy.COLI_BILLFREQ__c = '12';



            policys.add(policy);
        }

        insert policys;

        return policys;
    }


    // 契約日のリストを受け取り、設定する
    public static List<E_Policy__c> createPolicys(String occDate){

        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(E_Const.POLICY_RECORDTYPE_COLI);

        // 保険契約ヘッダを作成
        List<E_Policy__c> policys = new List<E_Policy__c>();
        for(Integer i = 0; i < 9; i++){
            E_Policy__c policy = new E_Policy__c();

            policy.Contractor__c = atCon.Id;
            policy.MainAgent__c = atCon.Id;
            policy.Insured__c = atCon.Id;
            //policy.COMM_CHDRNUM__c = '99999999'+i+'|1';
            policy.COMM_CHDRNUM__c = '7654321' + i;
            //データの作り方要検討
            policy.COMM_OCCDATE__c = occDate;
            policy.MainAgentAccount__c = ayAccList[0].Id;
            policy.COMM_SPLITC01__c = 33;
            policy.SubAgent__c = atCon.Id;
            policy.SubAgentAccount__c = ayAccList[0].Id;
            policy.COMM_SPLITC02__c = 66;
            policy.COMM_ZEBKNM__c = 'テスト金融機関';
            policy.COMM_ZEBKACOW__c = '口座名義人';
            policy.COMM_BANKACCKEY__c = '1234567890';
            policy.COLI_ZADVPDCF__c = true;
            policy.COMM_STATCODE__c = 'R';
            policy.COMM_ZRSTDESC__c = '';
            policy.COMM_ZEFREQ__c = '年 払';
            policy.COMM_ZECHNL__c = '口座振替';
            policy.COLI_ZNPTDDCF__c = true;
            policy.COLI_ZBKTRDCF__c = true;
            policy.COLI_ZBKPODIS__c = '20';
            policy.COLI_ZCSHVAL__c = 10000;
            policy.COLI_ZLOANTOT__c = 1000;
            policy.COLI_ZEADVPRM__c = 100;
            policy.COLI_ZUNPREM__c = 10;
            policy.COLI_ZCVDCF__c = true;
            policy.COLI_ZUNPCF__c = true;
            policy.COLI_ZUNPDCF__c = true;
            policy.COLI_RCPTDT__c = '20171027';
            policy.COLI_AVPRMRCV__c = 10000;
            policy.COLI_REMNBANN__c = 1;
            policy.COLI_ZEPLTOT__c = 10000;
            policy.COLI_ZEPLPRN__c = 1000;
            policy.COLI_ZEPLINT__c = 100;
            policy.COLI_ZPLCAPT__c = '1ヶ月';
            policy.COLI_ZPLRATE__c = 123;
            policy.COMM_ZPLDCF__c = true;
            policy.COLI_ZEAPLTOT__c = 10000;
            policy.COLI_ZEAPLPRN__c = 10000;
            policy.COLI_ZEAPLINT__c = 1000;
            policy.COLI_ZAPLCAPT__c = '1ヶ月';
            policy.COLI_ZAPLRATE__c = 123;
            policy.COLI_ZAPLDCF__c = true;
            policy.COMM_CLNTNUM__c = '12345678';
            policy.COMM_ZCLNAME__c = '契約者A';
            policy.COMM_ZINSNAM__c = '被保険者A';
            policy.COMM_ZKNJSEX__c = '男性';
            policy.COMM_DOB__c = '20171027';
            policy.RecordTypeId = recTypeId;
            policy.COMM_SPLITC__c = 55;
            policy.coli_billfreq__c = '12';



            policys.add(policy);
        }

        insert policys;

        return policys;
    }


    //個人保険特約(主契約)
    public static List<E_COVPF__c> createCOV(E_Policy__c po){

        covList = new List<E_COVPF__c>();

        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);


        E_COVPF__c cov = new E_COVPF__c();
        cov.E_Policy__c = po.Id;
        cov.COMM_STATDATE__c = '20161111';
        cov.COMM_SUMINS__c = 100000;
        cov.COLI_INSTPREM__c = 100000;
        cov.COLI_ZCRIND__c = 'C';
        cov.COMM_ZCOVRNAM__c = '4テストタイプ';
        cov.COLI_ZDPTYDSP__c = true;
        cov.COMM_CHDRNUM__c = '99999999';
        cov.DCOLI_ZCLNAME__c = '契約者A';
        cov.DCOLI_ZINSNAM__c = '被保険者A';
        cov.COMM_SUMINS__c = 987654321;
        cov.DCOLI_SINSTAMT__c = 123456789;
        cov.DCOLI_OCCDATE__c = '20171027';
        cov.COMM_ZRCESDSC__c = '終身';
        cov.COMM_ZPCESDSC__c = '---------';
        cov.RecordTypeId = recTypeId;
        cov.COMM_CRTABLE__c = E_Const.COVPF_CRTABLE2_JA;

        insert cov;
        covList.add(cov);

        return covList;
    }

    //個人保険特約(主契約＋特約)のレコードを作製
    public static List<E_COVPF__c> createCOV_CR(E_Policy__c po){

        covList = new List<E_COVPF__c>();

        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);


        E_COVPF__c cov = new E_COVPF__c();
        cov.E_Policy__c = po.Id;
        cov.COMM_STATDATE__c = '20161111';
        cov.COMM_SUMINS__c = 100000;
        cov.COLI_INSTPREM__c = 100000;
        cov.COLI_ZCRIND__c = 'C';
        cov.COMM_ZCOVRNAM__c = '重大疾病保障保険';
        cov.COLI_ZDPTYDSP__c = true;
        cov.COMM_CHDRNUM__c = '99999999';
        cov.DCOLI_ZCLNAME__c = '契約者A';
        cov.DCOLI_ZINSNAM__c = '被保険者A';
        cov.COMM_SUMINS__c = 987654321;
        cov.DCOLI_SINSTAMT__c = 123456789;
        cov.DCOLI_OCCDATE__c = '20171027';
        cov.COMM_ZRCESDSC__c = '終身';
        cov.COMM_ZPCESDSC__c = '---------';
        cov.RecordTypeId = recTypeId;
        cov.COMM_CRTABLE__c = 'TTG1';

        covList.add(cov);

        E_COVPF__c cov_tokuyaku = new E_COVPF__c();
        cov_tokuyaku.E_Policy__c = po.Id;
        cov_tokuyaku.COMM_STATDATE__c = '20161111';
        cov_tokuyaku.COMM_SUMINS__c = 100000;
        cov_tokuyaku.COLI_INSTPREM__c = 100000;
        cov_tokuyaku.COLI_ZCRIND__c = 'R';
        cov_tokuyaku.COMM_ZCOVRNAM__c = '災害割増特約';
        cov_tokuyaku.COLI_ZDPTYDSP__c = true;
        cov_tokuyaku.COMM_CHDRNUM__c = '99999999';
        cov_tokuyaku.DCOLI_ZCLNAME__c = '契約者A';
        cov_tokuyaku.DCOLI_ZINSNAM__c = '被保険者A';
        cov_tokuyaku.COMM_SUMINS__c = 987654321;
        cov_tokuyaku.DCOLI_SINSTAMT__c = 123456789;
        cov_tokuyaku.DCOLI_OCCDATE__c = '20171027';
        cov_tokuyaku.COMM_ZRCESDSC__c = '終身';
        cov_tokuyaku.COMM_ZPCESDSC__c = '---------';
        cov_tokuyaku.RecordTypeId = recTypeId;
        cov_tokuyaku.COMM_CRTABLE__c = 'ADG1';

        covList.add(cov_tokuyaku);

        insert covList;

        return covList;
    }


    //保険顧客情報
    public static void createCrlpf(E_Policy__c policy, String role){
        E_CRLPF__c crl = new E_CRLPF__c();

        crl.E_Policy__c = policy.Id;
        crl.BNYTYP__c = '01';
        crl.ZCLNAME__c = '照須快';
        crl.CLNTNUM__c = '99999999';
        crl.ZKNJSEX__c = '男性';
        crl.BNYPC__c = 100.00;
        crl.DOB__c = '11111111';
        crl.CRLPFCode__c = '99999999|TS|1';
        crl.CLRRROLE__c = role;
        crl.VALIDFLAG__c = '1';



        insert crl;
    }

    //保険取引履歴
    //private static void createPtnpf(E_Policy__c policy){
    //  E_PTNPF__c ptn = new E_PTNPF__c();

    //  ptn.E_Policy__c = policy.Id;
    //  ptn.TRANDATE__c = '20171027';
    //  ptn.ZTRANDSC__c = 'テストクラス作成';

    //  insert ptn;
    //}

    //private static E_BizDataSyncLog__c createBiz(){
    //  Date d = date.newInstance(1996, 6, 23);
    //  DateTime dt = datetime.newInstance(1996, 6, 23);
    //  E_BizDataSyncLog__c rec = new E_BizDataSyncLog__c();
    //  rec.Kind__c = '1';
    //  rec.InquiryDate__c = d;
    //  rec.BatchEndDate__c = dt;
    //  rec.DataSyncStartDate__c = Datetime.now();
    //  rec.DataSyncEndDate__c = Datetime.now();
    //  insert rec;
    //  return rec;
    //}


    public static E_Policy__c getPolicy(String policyId){
        E_policy__c pol = [select
                                    Id, Name,
                                    COMM_OCCDATE__c, MainAgentName__c, MainAgentAccountName__c, COMM_CHDRNUM__c, ContractorName__c, InsuredName__c
                                from E_Policy__c where Id =: policyId];

        return pol;
    }

    public static void createContentMaster(){
        content = new I_ContentMaster__c();
        content.Name = '保険契約概要_画面の使い方PDF';
        insert content;

        Attachment att = new Attachment(Name = 'テストファイル', ParentId = content.Id, body = Blob.valueOf(content.Id));
        insert att;
    }

    public static List<I_MeritTableDTO> createMeritTableDTO(){
        List<I_MeritTableDTO> dtoList = new List<I_MeritTableDTO>();

        for(Integer i=1;i<=3;i++){
            I_MeritTableDTO dto = new I_MeritTableDTO();

            dto.keikaNensu = String.valueOf(i);
            dto.age = String.valueOf(25+i);
            dto.S = 150000;
            dto.pRuikei = 30000000;
            dto.cv = 10000000;
            dto.simpleHenreiRt = 50.00;
            dto.sonkinP = 20000000;
            dto.koukaGaku = 6000000;
            dto.koukaGakuRuikei = 6000000;
            dto.jisshitsuFutanGaku = 30000000;
            dto.jisshitsuHenreiRt = 50.00;
            dto.shisanKeijoGaku = 18000000;
            dtoList.add(dto);
        }

        return dtoList;
    }

    public static I_CalcServiceInput createAtriaInData(){
        I_CalcServiceInput cs = new I_CalcServiceInput();
        cs.keisanKijunDate = '20180101';
        cs.taxRt = '33.00';
        cs.plcyDate = '20180101';
        cs.keiyakushaSb = '3';
        cs.kobetsuIkkatsuKb = '1';
        cs.sonkinType = '1';
        cs.zennoNensu = 0;
        //被保険者情報
        cs.planHihokensyaInfo.hihokenshaId = 'H0';
        cs.planHihokensyaInfo.hihokenshaAge = 25;
        cs.planHihokensyaInfo.hihokenshaSex = '1';
        cs.planHihokensyaInfo.sentakuShiteiUmu = '1';
        cs.planHihokensyaInfo.sentakuHoho = '04';
        cs.planHihokensyaInfo.sentakuHohoOthKoho = null;
        //主契約情報
        cs.mainCoverInfo.crCode = 'WL';
        cs.mainCoverInfo.sPlcy = 10000000;
        cs.mainCoverInfo.hokenKikanNenSaiSyushin = '3';
        cs.mainCoverInfo.hokenKikanPlcy = '999';
        cs.mainCoverInfo.haraikomiKikanNenSaiSyushin = '3';
        cs.mainCoverInfo.haraikomiKikan = '999';
        cs.mainCoverInfo.lowCvKikanNenSai = '';
        cs.mainCoverInfo.lowCvKikan = '';
        cs.mainCoverInfo.inPHoshoKikan = '';
        cs.mainCoverInfo.kenkotai = '1';
        cs.mainCoverInfo.warimashi = '0';
        cs.mainCoverInfo.sakugen = '0';
        cs.mainCoverInfo.tokuteiShippeiPMenTokusoku = '';
        cs.mainCoverInfo.haraikomiHoho = '1';
        cs.mainCoverInfo.shikiShitei = '0';
        //特約情報
        I_CalcServiceInput.SubCoverInfo sub = new I_CalcServiceInput.SubCoverInfo();
        sub.tokuyakuCrCode = 'AD';
        sub.sTokuyaku = 30000000;
        sub.hokenKikanNenSaiSyushin = '3';
        sub.hokenKikanTokuyaku = '999';
        cs.mainCoverInfo.subCoverList.add(sub);
        return cs;
    }

    public static List<I_MeritTableEntity> createEntityList(){
        List<I_MeritTableEntity> entityList = new List<I_MeritTableEntity>();
        for(Integer i=1;i<=3;i++){
            I_MeritTableEntity entity = new I_MeritTableEntity();
            entity.keikaNensu = String.valueof(i);
            entity.hokenNendo = String.valueof(i);
            entity.age = String.valueof(25+i);

            entity.shibouKoudoShogaiSManyen = String.valueof(100);
            entity.shibouSeikatsuShogaiSManyen = String.valueof(200);
            entity.judaishippeiSManyen = String.valueof(300);
            entity.shibouKyufuKin = String.valueof(400);
            entity.kijunSManyen = String.valueof(500);
            entity.saigaiSManyen = String.valueof(600);
            entity.ganNyuinKyufuKinNichigaku = String.valueof(700);
            entity.nenkinGetsugaku = String.valueof(800);

            entity.uketoriKaisu = String.valueof(0);
            entity.uketoriSogaku = String.valueof(0);
            entity.nenkinGenka = String.valueof(0);
            entity.haraikataBetsuP = String.valueof(10000);
            entity.nenkanHaraikomiP = String.valueof(20000);
            entity.pRuikei = String.valueof(3000000);
            entity.mikeikaP = String.valueof(40000);
            entity.cv = String.valueof(2000000);
            entity.mankiSRuikei = String.valueof(0);
            entity.kaiyakuUketoriKin = String.valueof(2000000);
            entity.uketoriKinRuikeiGaku = String.valueof(2000000);
            entity.simpleHenreiRt = String.valueof(20.00);
            entity.sonkinP = String.valueof(1500000);
            entity.koukaGaku = String.valueof(500000);
            entity.koukaGakuRuikei = String.valueof(1000000);
            entity.jisshitsuFutanGaku = String.valueof(5000000);
            entity.jisshitsuHenreiRt = String.valueof(99.00);
            entity.shisanKeijoGaku = String.valueof(300000);
            entityList.add(entity);
        }
        return entityList;
    }


    public class CoverParam{
        public Id policy {get;set;}
        public String coverType {get;set;}
        public String crCode {get;set;}
        public Integer S {get;set;}
        public String SPeriod {get;set;}
        public String billingPeriod {get;set;}
        public String mortcls {get;set;}        //健康体・優良体・割増特約付加状態
        public String liencd {get;set;}         //削減コード
        public String crtable2 {get;set;}       //商品コード
        public String zcrind {get;set;}         //主契約判定

    }

    public class PolicyParam{

        public String policyNumber {get;set;}
        public Id insured {get;set;}
        public Boolean zennoFlg {get;set;}//前納表示フラグ
        public String numberOfBilling {get;set;}
        public String CCDate {get;set;}
        public String contractorType {get;set;}
        public String OCCDate {get;set;}
        public String premdisc {get; set;}      //団体払い込み料率
        public String kgrptype {get; set;}      //団体種類
        public String billchnl {get; set;}      //払い込み経路
        public String currentStatus {get; set;} //現在の状況コード
        public Boolean cannotCalcCV {get; set;} //解約返戻金計算不能フラグ


    }


    public class InsuredParam {

        public String firstName {get;set;}
        public String lastName {get;set;}
        public String sex {get;set;}
        public String dayOfBirth {get;set;}
//      public String zennoFlg {get;set;}
//      public String billFrequencyCd {get;set;}
//      public String CCDATE {get;set;}
//      public String contructorType {get;set;}


    }

      public static List<E_COVPF__c> createCover(List<CoverParam> cParams){

        List<E_COVPF__c> covers = new List<E_COVPF__c>();
        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);


        for(CoverParam cParam:cParams){
            E_COVPF__c cover = new E_COVPF__c();
            cover.E_Policy__c = cParam.policy;
            cover.COLI_ZCRIND__c = cParam.coverType;
            cover.COMM_CRTABLE__c = cParam.crCode;
            cover.COLI_BASICINSURANCE__c = String.valueOf(cParam.S);
            cover.COMM_ZRCESDSC__c = cParam.SPeriod;
            cover.COMM_ZPCESDSC__c = cParam.billingPeriod;
            cover.COLI_MORTCLS__c = cParam.mortcls;
            cover.COLI_LIENCD__c = cParam.liencd;
            cover.RecordTypeId = recTypeId;

            covers.add(cover);
        }

        insert covers;

        return [select
                    Id
                    , COLI_ZCRIND__c
                    , COMM_CRTABLE2__c
                    , COLI_BASICINSURANCE__c
                    , COMM_ZRCESDSC__c
                    , COMM_ZPCESDSC__c
                    , COLI_MORTCLS__c
                    , COLI_LIENCD__c
                from
                    E_COVPF__c
                where
                    E_Policy__c = :cParams.get(0).policy
                ];

    }


        public static E_Policy__c createPolicy(PolicyParam pParam){
        //テスト用証券番号
        String policyNumber = '01234567';

        // 保険契約ヘッダ
        E_Policy__c policy = new E_Policy__c();


        /*保険契約ヘッダのセット*/
        policy.COMM_CHDRNUM__c = pParam.policyNumber;

        //被保険者
        policy.insured__c = pParam.insured;
        System.debug('被保険者ContactのID：'+ pParam.insured);

        //前納有無
        policy.put('COLI_ZADVPDCF__c',pParam.zennoFlg);

        //現在の状況コード
        policy.put('COMM_STATCODE__c',pParam.currentStatus);

        //解約返戻金計算不能フラグ
        policy.put('COLI_ZCVDCF__c',pParam.cannotCalcCV);

        //払込回数
        policy.put('COLI_BILLFREQ__c',pParam.numberOfBilling);

        //責任開始日
        policy.put('COMM_CCDATE__c',pParam.CCDate);

        //契約日
        policy.put('COMM_OCCDATE__c',pParam.OCCDate);

        //契約者区分
        policy.put('COMM_ZCNTRDSC__c',pParam.contractorType);

        //払込経路
        policy.put('COLI_BILLCHNL__c',pParam.billchnl);

        //団体料率
        policy.put('COLI_PREMDISC__c',pParam.premdisc);

        //団体種類
        policy.put('COLI_KGRPTYPE__c',pParam.kgrptype);

        insert policy;

        return (E_Policy__c)[select
                                Id
                                ,COMM_CHDRNUM__c
                                ,InsuredAge__c
                                ,InsuredCLTPF_ZKNJSEX__c
                                ,COLI_ZADVPDCF__c
                                ,COMM_STATCODE__c
                                ,COLI_ZCVDCF__c
                                ,COLI_BILLFREQ__c
                                ,COMM_CCDATE__c
                                ,COMM_OCCDATE__c
                                ,COMM_ZCNTRDSC__c
                                ,COLI_BILLCHNL__c
                                ,COLI_PREMDISC__c
                                ,COLI_KGRPTYPE__c
                                ,Insured__r.Name
                            from
                                E_Policy__c
                            where
                                Id  = :policy.id
                            ];

    }


    /*
     * InsuredParamをインプットに被保険者（Insured__c）を登録し、オブジェクトを返却する
     */
    public static Id createInsured(InsuredParam param){

        Account dummyAcc = new Account();
        dummyAcc.name = 'DummyAccount';
        insert dummyAcc;

        Contact contact = new Contact();

        //Contactのインプットパラメータを設定（インプットに無い項目はデフォルトを設定）

        //性別
        contact.firstname = param.firstname;
        contact.lastname = param.lastname;

        contact.E_CLTPF_ZKNJSEX__c = param.sex;
        contact.E_CLTPF_DOB__c = param.dayOfBirth;

        contact.AccountId = dummyAcc.Id;

        insert contact;

        //作成したContactのIDを返却
        Id rec = contact.Id;

        return rec;
    }


        public static TestI_PIPTestUtil.InsuredParam createDefaultiParam(){
        TestI_PIPTestUtil.InsuredParam iparam = new TestI_PIPTestUtil.InsuredParam();
        iparam.firstName = 'NN';
        iparam.lastName = 'man';
        iparam.sex = '男性';
        iparam.dayOfBirth = '19600101';
        return iparam;
    }

    public static TestI_PIPTestUtil.PolicyParam createDefaultpParam(Id insuredId){
        TestI_PIPTestUtil.PolicyParam pParam = new TestI_PIPTestUtil.PolicyParam();
        pParam.policyNumber = '01234567';
        pParam.insured = insuredId;
        pParam.zennoFlg = false;
        pParam.currentStatus = 'I';
        pParam.cannotCalcCV = false;

        pParam.numberOfBilling = '01';
        pParam.CCDate = '19890927';
        pParam.OCCDate = '19890927';
        pParam.contractorType = 'P';

        pParam.premdisc = '';
        pParam.kgrptype = '';
        pParam.billchnl = 'B';

        return pParam;
    }

    public static TestI_PIPTestUtil.PolicyParam createDefaultpParam(){
        //被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = createDefaultiParam();

        Id insuredId = createInsured(iparam);

        TestI_PIPTestUtil.PolicyParam pParam = new TestI_PIPTestUtil.PolicyParam();
        pParam.policyNumber = '01234567';
        pParam.insured = insuredId;
        pParam.zennoFlg = false;
        pParam.currentStatus = 'I';
        pParam.cannotCalcCV = false;

        pParam.numberOfBilling = '01';
        pParam.CCDate = '19890927';
        pParam.OCCDate = '19890927';
        pParam.contractorType = 'P';

        pParam.premdisc = '';
        pParam.kgrptype = '';
        pParam.billchnl = 'B';

        return pParam;
    }

    public static List<TestI_PIPTestUtil.CoverParam> createDefaultcParam(E_Policy__c policy){

        List<TestI_PIPTestUtil.CoverParam> cParams = new List<TestI_PIPTestUtil.CoverParam>();
        TestI_PIPTestUtil.CoverParam cParam = new TestI_PIPTestUtil.CoverParam();

//      /*主契約データのセット*/

        cParam.policy = policy.id;
        cParam.coverType = 'C';
        cParam.crCode = 'WL';
        cParam.S = 30000000;
        cParam.SPeriod = '終身';
        cParam.billingPeriod = '60歳';

        cParam.mortcls = 'S';
        cParam.liencd = '';
        cParam.crtable2 = 'WL';
        cParam.zcrind = 'C';

        cParams.add(cParam);

        /*特約データのセット*/

        TestI_PIPTestUtil.CoverParam cParam_toku = new TestI_PIPTestUtil.CoverParam();
        cParam_toku.policy = policy.id;
        cParam_toku.coverType = 'R';
        cParam_toku.crCode = 'AD';
        cParam_toku.S = 30000000;
        cParam_toku.SPeriod = '終身';
        cParam_toku.billingPeriod = '60歳';

        cParam_toku.mortcls = 'S';
        cParam_toku.liencd = '';
        cParam_toku.crtable2 = 'WL';
        cParam_toku.zcrind = 'R';

        cParams.add(cParam_toku);

        return cParams;
    }

    /* 被保険者と保険契約ヘッダを作成
     * @param
     * @return E_Policy__c: 保険契約ヘッダレコード
     */
    public static E_Policy__c createPolicyInfo(){
        //被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = createDefaultiParam();

        Id insuredId = createInsured(iparam);

        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = createDefaultpParam(insuredId);

        return createPolicy(pParam);
    }
    /* 個人保険特約リスト(主契約・特約)を作成
     * @param E_Policy__c: 保険契約ヘッダ
     * @return List<E_COVPF__c>: 個人保険特約リスト
     */
    public static List<E_COVPF__c> createCoversInfo(E_Policy__c policy){
        //作成した被保険者の個人保険特約を作成
        List<TestI_PIPTestUtil.CoverParam> cParams = createDefaultcParam(policy);

        return createCover(cParams);
    }

    public static void createInsuredInfo(E_Policy__c policy){
        //保険顧客情報を作成
        E_CRLPF__c cl = new E_CRLPF__c();
        cl.E_Policy__c = policy.id;
        cl.CLRRROLE__c = 'LA';
        cl.VALIDFLAG__c = '1';
        cl.ANBCCD__c = 25;
        insert cl;
    }

}