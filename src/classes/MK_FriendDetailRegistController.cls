public with sharing class MK_FriendDetailRegistController extends E_AbstractController{
/*	//契約者番号
	public string clientNumber {get;private set;}*/
	//キャンペーンターゲット
	public MK_CampaignTarget__c campaignTarget {get;private set;}
	//リード
	public Lead friend {get; set;}
	//リードTMPID
	public String MK_leadTMP_Id {get;private set;}
	//リードTMP
	public MK_leadTMP__c tmpFriend {get;set;}
	//都道府県以降の住所
	public String Address {get;set;}
	//同意確認
	public Boolean Agree {get; set;}
	//メール再確認用
	public String RecomfirmEmail {get;set;}
	//メール一時保存用
	private String tmpEmail;
	//変更ボタン押下フラグ
	public Boolean isChecked {get;set;}


	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;private set;}

	/**
	 * Constructor
	 */
	public MK_FriendDetailRegistController() {
		// ページメッセージの初期化
		pageMessages = getPageMessages();
		//初期化
		Agree = false;
		MK_leadTMP_Id = null;
		friend = new Lead();
		campaignTarget = new MK_CampaignTarget__c();
		isChecked = false;

		//初期ページ以外からのアクセスはエラーとする
		pageMessages.addErrorMessage(MK_Const.ERR_MSG09_URLPARAM_ID);
	}

	/**
	 * init
	 */
	public void init(){
		//初期ページアクセス エラーメッセージ初期化
		pageMessages.clearMessages();
		If(String.isEmpty(MK_leadTMP_Id)) getRecByUrlParam();
	}
	public PageReference doConfirm(){
		pageMessages = getPageMessages();
		try{
			if(String.isNotBlank(ApexPages.CurrentPage().getParameters().get('isChecked'))){
				isChecked = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('isChecked'));
			}
			//入力チェック
			//Nullチェック
			if(String.isBlank(friend.LastName)	//会社名
			|| String.isBlank(tmpFriend.LastName__c)	//姓
			|| String.isBlank(tmpFriend.LastNameKana__c)	//姓カナB
			|| String.isBlank(tmpFriend.FirstName__c)	//名
			|| String.isBlank(tmpFriend.FirstNameKana__c)	//名カナ
			|| String.isBlank(friend.Email)	//Eメールアドレス
			|| String.isBlank(friend.State)	//都道府県
			|| String.isBlank(Address)	//市町村以降
			|| String.isBlank(friend.Phone)){	//電話番号
				pageMessages.addErrorMessage(MK_Const.ERR_MSG02_IS_NOT_ENTRY);
				return null;
			}
			if(isChecked){
				if(String.isEmpty(RecomfirmEmail)){
					if(tmpEmail <> friend.Email){
						pageMessages.addErrorMessage(MK_Const.ERR_MSG02_IS_NOT_ENTRY);
						return null;
					}
				}else if(friend.Email <> RecomfirmEmail){
					pageMessages.addErrorMessage(MK_Const.ERR_MSG05_IS_NOT_RECON_EMAIL);
					return null;
				}

			}
			friend.Company = String.join(new String[]{tmpFriend.LastName__c,tmpFriend.FirstName__c},'　');
			//カナ文字判定
			if(E_Util.isKatakana(tmpFriend.LastNameKana__c.replace('ー','').replace('-',''))
			&& E_Util.isKatakana(tmpFriend.FirstNameKana__c.replace('ー','').replace('-',''))){
				//全角に統一する
				friend.name_kana__c = E_Util.enKatakanaToEm(String.join(new String[]{tmpFriend.LastNameKana__c,tmpFriend.FirstNameKana__c},'　'));
			}else{
				pageMessages.addErrorMessage(MK_Const.ERR_MSG03_IS_NOT_KATAKANA);
				return null;
			}
			//都道府県と住所の結合
			friend.Address__c = String.join(new String[]{friend.State,Address},'');
			//郵便番号判定
			if(String.isNotBlank(friend.ZipCode__c)){
				friend.ZipCode__c = E_Util.emNumberToEn(friend.ZipCode__c.replaceAll('-','').replaceAll('－',''));
				if(friend.ZipCode__c.length() <> 7
				|| !E_Util.isEnNum(friend.ZipCode__c)){
					pageMessages.addErrorMessage(MK_Const.ERR_MSG04_IS_NOT_POSTALCODE);
					return null;
				}
			}
			//電話番号判定　ToDo: 判定内容が厳しく、フリー入力には厳しく思えるため、入力側をやさしくする必要性がある？デザインがきてから
			friend.Phone = E_Util.emNumberToEn(friend.Phone.replaceAll('－','-'));
			if(!E_Util.isPhoneFormat(friend.Phone)){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG05_IS_NOT_PHONE);
				return null;
			}

		}catch(Exception e){
			return null;
		}
		PageReference pref2 = Page.MK_FriendDetailConfirm;
		pref2.setRedirect(false);
		return pref2;
	}
	/*
	 * 戻るボタン
	 * 1ページ目に遷移
	 */
	public PageReference doEdit(){
		pageMessages = getPageMessages();
		PageReference pref1 = Page.MK_FriendDetailRegist;
		pref1.setRedirect(false);
		return pref1;
	}
	/*
	 * 保存処理
	 */
	public PageReference doSave(){
		pageMessages = getPageMessages();
		//同意確認
		if(!Agree){
			pageMessages.addErrorMessage(MK_Const.ERR_MSG07_IS_NOT_AGREE);
			return null;
		}
		//排他制御
		Savepoint sp = Database.setSavePoint();
		try{
			//MK_leadTMPが更新されていないかを確認
			If(IsUpdated(tmpFriend)){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG11_IS_UPDATE);
			}
			tmpFriend.CreateLeadFlag__c = true;

			//ロールバック時のID付与を防ぐため
			Lead commitFriend = friend.clone();
			system.debug('■■■friend='+friend);
			system.debug('■■■commitFriend='+commitFriend);
			system.debug('■commitFriend.MK_CampaignTarget__r='+commitFriend.MK_CampaignTarget__r);
			insert commitFriend;
			//リードテンプ更新
			update tmpFriend;

			PageReference pref3 = Page.MK_ThanksDetailRegist;
			return pref3;
		}catch(Exception e){
			Database.rollback(sp);
			system.debug('■[doConfirm]' + e.getMessage());
			return null;
		}
	}
	/**
	 *	 更新チェック(Lead作成済みフラグで判定)
	 * @param rec: リードTMPレコード
	 * @return rec.CreateLeadFlag__c: 更新済みの場合はtrueを返す
	 * リードTMPが画面上から更新されることを想定しない為、更新の判定には、Lead作成済みフラグを使用
	 */
	private static Boolean IsUpdated(MK_leadTMP__c rec){
		rec = E_MK_leadTMPDao.getRecById(rec.Id);
		//削除された場合は更新済み扱いとする
		if(rec == null) return true;
		return rec.CreateLeadFlag__c;
	}
	/*
	 *	URLPARAMETERよりレコードの設定
	 */
	private void getRecByUrlParam(){
		try{
			String request = ApexPages.CurrentPage().getParameters().get('request');
			system.debug('■request='+request);
			if(String.isEmpty(request)){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG09_URLPARAM_ID);
				return;
			}
			MK_leadTMP_Id = E_EncryptUtil.getDecryptedString(request);
			system.debug('■[MK_leadTMP_Id]' + MK_leadTMP_Id);
			//IDの整合性チェック
			if(E_Util.isValidateId(MK_leadTMP_Id, Schema.MK_leadTMP__c.SObjectType)){
				tmpFriend = E_MK_leadTMPDao.getRecById((ID)MK_leadTMP_Id);
				if(tmpFriend == null){
					//IDに対応するtmpFriendが存在しない
					MK_leadTMP_Id = null;
					pageMessages.addErrorMessage(MK_Const.ERR_MSG09_URLPARAM_ID);
					return;
				}else if(tmpFriend.CreateLeadFlag__c){
					//IDに対応するtmpFriendからLeadが作成済み
					MK_leadTMP_Id = null;
					pageMessages.addErrorMessage(MK_Const.ERR_MSG12_IS_CREATE_LEAD);
					return;
				}
				campaignTarget = E_MK_CampaignTargetDao.getActiveRecById(tmpFriend.MK_CampaignTarget__c);
				if(campaignTarget == null){
					MK_leadTMP_Id = null;
					pageMessages.addErrorMessage(MK_Const.ERR_MSG13_IS_NOT_ACTIVE_CAMPAIGN);
					return;
				}
				friend = new Lead(
							LastName = tmpFriend.Company__c
							,Email = tmpFriend.Email__c							//メール
							,MK_CampaignTarget__c = campaignTarget.id					//キャンペーンターゲット(参照関係)
							,MK_CampaignTarget__r = campaignTarget					//キャンペーンターゲット(参照関係)
							,MK_CampaignTarget_AgentMailAddress__c = campaignTarget.AgentMailAddress__c 	//メール項目作成
							,MK_CampaignTarget_ContractantMailAddress__c = campaignTarget.ContractantMailAddress__c
							,MK_CampaignTarget_MR_MailAddress__c = campaignTarget.MR__r.Email
							,RecordTypeId = E_RecordTypeDao.getRecIdBySobjectTypeAndDevName(MK_Const.RECTYPE_DEV_NAME_BATON,SObjectType.Lead.Name)						//レコードタイプを「バトン」に設定
							,Ownerid = E_UserDao.getRecByContactId(campaignTarget.Agent__c)				//所有者を募集人に設定
							,LeadTMP__c = tmpFriend.id
							);
				tmpEmail = tmpFriend.Email__c;
			}else{
				MK_leadTMP_Id = null;
				pageMessages.addErrorMessage(MK_Const.ERR_MSG09_URLPARAM_ID);
			}
		}catch(Exception e){
			pageMessages.addErrorMessage('■'+ e.getMessage());
			system.debug('■[MK_FriendDetailRegistController]' + e.getMessage());
		}
	}
}