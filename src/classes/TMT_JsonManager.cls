// JSON関連の処理を管理するクラス
public without sharing class TMT_JsonManager {
    // 静的リソースからJSONファイルを読み込む。
    public  static String getJson(String rcName){
        //  静的リソースから”TMT_ConstantData”で登録したJsonファイルを読み込む
        try {
            StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name =  :rcName  LIMIT 1];
            return  sr.Body.toString();
        } 
        // 読み込めなかったら ’’（空文字列）を返却する。
        catch ( Exception e ) {
            return '';
        }
	}
}