/**
 * E_freezeUserBatchのスケジューラ
 */
global class E_freezeUserBatchSched implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new E_freezeUserBatch());
	}
}