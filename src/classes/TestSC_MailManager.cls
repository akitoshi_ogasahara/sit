@isTest
private class TestSC_MailManager {
	// 自主点検事務所
	private static SC_Office__c office;
	//
	private static Account accParent;
	//
	private static Account accAgency;
	// 
	private static List<SC_AgencyManager__c> agManagerList;

	/**
	 * 定義テスト：メールテンプレート
	 */
	@isTest static void constant_test001(){
		Map<String, EmailTemplate> tempMap = new Map<String, EmailTemplate>();

		for(EmailTemplate et : [Select Id, DeveloperName From EmailTemplate]){
			tempMap.put(et.DeveloperName, et);
		}
		
	    /* ［ステータス変更］ */
	    // 未提出→本社確認中（NN）
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_AGCMD_NN) != null);
	    // 未提出→本社確認中（代理店）
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_AGCMD_AG) != null);
	    // 本社確認中→未提出
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_CMDAG) != null);
	    // 不備対応中　未提出→本社確認中（NN）
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_AGCMD_NN) != null);
	    // 不備対応中　未提出→本社確認中（代理店）
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_AGCMD_AG) != null);
	    // 本社確認中→担当MR確認中
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_CMDMR) != null);
	    // 担当MR確認中→未提出
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_MRAG) != null);
	    // 不備対応中　未提出→担当MR確認中（NN）
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_AGMR_NN) != null);
	    // 不備対応中　未提出→担当MR確認中（代理店）
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_AGMR_AG) != null);
	    // 不備対応中　担当MR確認中→本社確認
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_RE_MRCMD_MR) != null);
	    // 本社確認中→完了
	    System.assert(tempMap.get(SC_MailManager.MT_STATUS_CMDAG) != null);
	    /* ［問い合わせ］ */
	    // CMD→代理店
	    System.assert(tempMap.get(SC_MailManager.MT_INQUIRY_CMDAG) != null);
	    // MR（拠点長）→代理店
	    System.assert(tempMap.get(SC_MailManager.MT_INQUIRY_MRAG) != null);
	    // 代理店→CMD
	    System.assert(tempMap.get(SC_MailManager.MT_INQUIRY_AGCMD) != null);
	    /* ［リマインド］ */
	    // 代理店向け＿締切前
	    System.assert(tempMap.get(SC_MailManager.MT_REMIND_BEFORE_AG) != null);
	    // 代理店向け＿締切後
	    System.assert(tempMap.get(SC_MailManager.MT_REMIND_AFTER_AG) != null);
	    // 代理店向け＿不備対応中
	    System.assert(tempMap.get(SC_MailManager.MT_REMIND_DEFECT_AG) != null);
	    // MR向け＿締切前
	    System.assert(tempMap.get(SC_MailManager.MT_REMIND_BEFORE_MR) != null);
	    // MR向け＿締切後
	    System.assert(tempMap.get(SC_MailManager.MT_REMIND_AFTER_MR) != null);
	    // MR向け＿不備対応中
	    System.assert(tempMap.get(SC_MailManager.MT_REMIND_DEFECT_MR) != null);
	}

	/**
	 * Map<String, List<String>> sendTargetMap
	 */
	@isTest static void sendTargetMap_test001(){
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_AGCMD_NN).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_AGCMD_AG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_CMDAG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_AGCMD_NN).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_AGCMD_AG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_CMDMR).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_MRAG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_AGMR_NN).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_AGMR_AG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_RE_MRCMD_MR).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_STATUS_CMDAG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_INQUIRY_CMDAG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_INQUIRY_MRAG).isEmpty() == false);
		System.assert(SC_MailManager.sendTargetMap.get(SC_MailManager.MT_INQUIRY_AGCMD).isEmpty() == false);
	}

	/**
	 * void sendMail(String mailType, SC_Office__c scOffice, List<SC_AgencyManager__c> agManagerList)
	 * 業務管理責任者：なし
	 */
	@isTest static void sendMail_test001() {
		// TestData
		createTestData();

		Test.startTest();
		// Test
		SC_MailManager.sendMail(SC_MailManager.MT_INQUIRY_CMDAG, office, new List<SC_AgencyManager__c>());

		Integer invocations = Limits.getEmailInvocations();

		Test.stopTest();

		System.assertEquals(1, invocations);
	}

	/**
	 * void sendMail(String mailType, SC_Office__c scOffice, List<SC_AgencyManager__c> agManagerList)
	 * 業務管理責任者：あり
	 */
	@isTest static void sendMail_test002() {
		// TestData
		createTestData();
		createAgManagerList(1);

		Test.startTest();
		// Test
		SC_MailManager.sendMail(SC_MailManager.MT_INQUIRY_CMDAG, office, agManagerList);

		Integer invocations = Limits.getEmailInvocations();

		Test.stopTest();

		System.assertEquals(1, invocations);
	}


	/**
	 * TestData 自主点検事務所
	 */
	private static void createTestData(){
		/* 実行ユーザ作成 */
		// 1. 取引先（格）を作成
		accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestSC_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestSC_TestUtil.createIDCPF(true, actUser.Id, 'AH');

		/* テストデータ作成 */
		// 6. 自主点検事務所を更新
		office = TestSC_TestUtil.createSCOfficeByFiscalYear(true, accAgency.Id, null, '2018');
		// 事務所
		office.Account__c = accAgency.Id;
		// 拠点長
		User mrmngr = TestSC_TestUtil.createUser(true, 'sc_mrmngr', '拠点長');
		office.MRManager__c = mrmngr.Id;
		update office;

		office = [Select Id, FiscalYear__c, AccZAGCYNUM__c, AccZEAYNAM__c, OwnerMRUnitName__c
							, DomainInternet__c, DomainGw__c, DomainEmployee__c, MRManager__c
					From SC_Office__c
					Where Id = :office.Id];

		// 7. レコードを共有
		TestSC_TestUtil.createSCOfficeShare(office.Id, actUser.Id);
		TestSC_TestUtil.createAccountShare(accAgency.Id, actUser.Id);
		TestSC_TestUtil.createAccountShare(accParent.Id, actUser.Id);
	}

	/**
	 * TestData 自主点検業務管理責任者
	 */
	private static void createAgManagerList(Integer cnt){
		agManagerList = new List<SC_AgencyManager__c>();

		// 業務管理責任者（SC_AgencyManager__c）
		for(Integer i = 0; i < cnt; i++){
			// 取引先責任者を作成
			String actUserLastNameAm = 'TestUserAm' + cnt;
			Contact conAm = TestSC_TestUtil.createContact(true, accAgency.Id, actUserLastNameAm);
			// ユーザを作成
			User actUserAm = TestSC_TestUtil.createAgentUser(true, actUserLastNameAm, 'E_PartnerCommunity', conAm.Id);
			// ID管理を作成
			E_IDCPF__c idMngAm = TestSC_TestUtil.createIDCPF(true, actUserAm.Id, 'AT');
			// 自主点検管理責任者
			SC_AgencyManager__c agManager = new SC_AgencyManager__c();
			agManager.SC_Office__c = office.Id;
			agManager.Contact__c = conAm.Id;
			agManager.User__c = actUserAm.Id;
			agManagerList.add(agManager);
		}
		insert agManagerList;
	}
}