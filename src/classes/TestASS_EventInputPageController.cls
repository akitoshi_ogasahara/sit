@isTest
private class TestASS_EventInputPageController {
	//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（MRページから）
	 * 行動新規作成処理
	 */
	private static testMethod void saveEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//ページ表示
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('retPgName','/apex/ass_mrpage');
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
			createCustomField(eventRecType.Id);
			
			ASS_EventInputPageController con = new ASS_EventInputPageController();

			con.event.WhatId = office.Id;
			con.event.Subject = 'test';
			con.event.StartDateTime = System.now();
			con.event.EndDateTime = System.now();
			con.event.EventType__c = '記録';
			con.event.Action__c = 'TEL';
			con.event.DesignDocReq__c = true;
			con.event.IsOpportunity__c = false;

			con.save();

//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_MRPage;
			newPage.getParameters().put('mrId',mr.Id);
			newPage.getParameters().put('unitId',unit.Id);
			newPage.setRedirect(true);

			System.assertEquals(con.save().getUrl(),newPage.getUrl());
		}	
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（代理店事務所ページから）
	 * 行動新規作成キャンセル処理
	 */
	private static testMethod void cancelEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//ページ表示
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('retPgName','/apex/ass_mrpage');
		pr.getParameters().put('unitId',unit.Id);
		pr.getParameters().put('date',String.valueOf(system.today().year() + '-' + system.today().month() + '-' + system.today().day()));
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_EventInputPageController con = new ASS_EventInputPageController();

			con.cancel();

//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_MRPage;
			newPage.getParameters().put('mrId',mr.Id);
			newPage.getParameters().put('unitId',unit.Id);

			newPage.setRedirect(true);

			System.assertEquals(con.cancel().getUrl(),newPage.getUrl());
		}	
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（代理店事務所ページから）
	 * 行動新規作成キャンセル処理
	 */
	private static testMethod void editEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		createCustomField(eventRecType.Id);
		Event ev = new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id);
		insert ev;

		//ページ表示
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('retPgName','/apex/ass_officesummary');
		pr.getParameters().put('unitId',unit.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('eventId',ev.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_EventInputPageController con = new ASS_EventInputPageController();

			con.edit();
			con.cancelEdit();

//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_OfficeSummary;
			newPage.getParameters().put('mrId',mr.Id);
			newPage.getParameters().put('unitId',unit.Id);
			newPage.getParameters().put('accountId',office.Id);
			newPage.setRedirect(true);

			System.assertEquals(con.del().getUrl(),newPage.getUrl());
		}	
	}
}