public with sharing class CC_FormDetailSRActivityTgrHdl{

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<CC_FormDetail__c> newList){}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<CC_FormDetail__c> newList, Map<Id,CC_FormDetail__c> newMap){}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<CC_FormDetail__c> newList, Map<Id,CC_FormDetail__c> newMap, Map<Id,CC_FormDetail__c> oldMap){}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<CC_FormDetail__c> newList, Map<Id,CC_FormDetail__c> newMap, Map<Id,CC_FormDetail__c> oldMap){
		//create FollowUpSRActivity
		List<Id> caseIdList = new List<Id>();
		for(CC_FormDetail__c formDetail : newList){
			caseIdList.add(formDetail.CC_Case__c);
		}
		createFollowUpSRActivity(caseIdList);
	}

	/**
	 * create FollowUpSRActivity
	 */
	public static void createFollowUpSRActivity(List<Id> caseIdList){
		List<CC_SRActivity__c> createSRActivityList = new List<CC_SRActivity__c>();
		List<Case> caseList = [select Id, CC_SubStatus__c, CC_IsCoverLetterOutput__c, CC_IsInvoiceOutput__c from Case where Id in: caseIdList];
		if(caseList.size() > 0){
			for(Case caseObj : caseList){
				if((caseObj.CC_SubStatus__c == null || !caseObj.CC_SubStatus__c.equals('不備中'))
					&& (caseObj.CC_IsCoverLetterOutput__c || caseObj.CC_IsInvoiceOutput__c)){
					CC_SRActivity__c newSRActivity = new CC_SRActivity__c(
									CC_SRNo__c = caseObj.Id,
									CC_ActivityType__c = '書類封緘',
									CC_Status__c = '開始前',
									CC_Order__c = 0,
									OwnerId = UserInfo.getUserId());

					createSRActivityList.add(newSRActivity);
				}
			}

			if(createSRActivityList.size() > 0){
				Insert createSRActivityList;
			}
		}

	}

}