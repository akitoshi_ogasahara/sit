/**
	■Statusの流れ
		作成中⇒（申請）⇒確認中⇒(承認)⇒承認済⇒(委託終了)⇒委託終了
		　　　　　⇒（報告）⇒報告済
*/
public with sharing class CR_OutSrcStatusManager {


	/** Status定義	**/
	public static String STATUS_01  = CR_Const.OUTSRC_STATUS_01;
	public static String STATUS_02a = CR_Const.OUTSRC_STATUS_02a;
	public static String STATUS_02b = CR_Const.OUTSRC_STATUS_02b;
	public static String STATUS_03  = CR_Const.OUTSRC_STATUS_03;
	public static String STATUS_09  = CR_Const.OUTSRC_STATUS_09;

	/** Action定義	**/
	public Enum OutSrcAction{NONE,APPLY,REPORT,APPROVE,REJECT_APPLY,REJECT_REPORT,REJECT_APPROVE,TERMINATE,TERMINATE_EDIT}
	public static Map<OutSrcAction,String> OutSrcActionLabel = new Map<OutSrcAction, String>{
																		OutSrcAction.NONE => ''  
																		,OutSrcAction.APPLY => '申請'
																		,OutSrcAction.REPORT => '報告'
																		,OutSrcAction.APPROVE => '承認'
																		,OutSrcAction.REJECT_APPLY => '申請取消'
																		,OutSrcAction.REJECT_REPORT => '報告取消'
																		,OutSrcAction.REJECT_APPROVE => '承認取消'
																		,OutSrcAction.TERMINATE => '委託終了'
																		,OutSrcAction.TERMINATE_EDIT => '委託終了日変更'
																};
	//次アクション、差戻しアクション種別保持クラス
	public class AvailableActions{
		public OutSrcAction nextAction{get;set;}
		public OutSrcAction rejectAction{get;set;}
		
		public AvailableActions(OutSrcAction nextAct, OutSrcAction rejectAct){
			nextAction = nextAct;
			rejectAction = rejectAct;
		}
		
		public String getNextActionLabel(){
			return OutSrcActionLabel.get(nextAction);
		}
		public String getRejectActionLabel(){
			return OutSrcActionLabel.get(rejectAction);
		}
	}
	

	/*  クラス変数   */
	private E_AccessController accCon;
	private CR_Outsrc__c record;
	private availableActions actions;
	private Boolean isHQEmployee;
	private Integer uploadedFileCount;			//アップロードされているファイル数



	/**
	 *	  Constructor
	 */
	public CR_OutSrcStatusManager(CR_Outsrc__c rec,Integer fileCnt){
		accCon = E_AccessController.getInstance();
		record = rec;
		actions = getAvailableActions();
		isHQEmployee = accCon.isCRModifyAllProfileUser() || accCon.ZINQUIRR_is_BRAll();	//BR**ユーザをHQユーザとする
		uploadedFileCount = fileCnt;
	}
	
	private AvailableActions getAvailableActions(){
		//作成中
		if(record.status__c == STATUS_01||String.isBlank(record.status__c)){	  
			if(record.hasCustomerInfo__c){		  //個人情報の取扱 有
				return new AvailableActions(OutSrcAction.APPLY, OutSrcAction.NONE);
			}
			return new AvailableActions(OutSrcAction.REPORT, OutSrcAction.NONE);
		}
		
		//承認権限ユーザのみ
		if(accCon.hasCRApprovalPermission()){	  
			//確認中
			if(record.status__c == STATUS_02a){	 
				return new AvailableActions(OutSrcAction.APPROVE, OutSrcAction.REJECT_APPLY);
			}
			//報告済
			if(record.status__c == STATUS_02b){	 
				return new AvailableActions(OutSrcAction.TERMINATE, OutSrcAction.REJECT_REPORT);
			}
			//承認済
			if(record.status__c == STATUS_03){
				return new AvailableActions(OutSrcAction.TERMINATE, OutSrcAction.REJECT_APPROVE);
			}
			//委託終了
			if(record.status__c == STATUS_09){
				return new AvailableActions(OutSrcAction.TERMINATE_EDIT, OutSrcAction.NONE);
			}
		}
		//上記以外
		return new AvailableActions(OutSrcAction.NONE, OutSrcAction.NONE);
	}


	/**
	 *	  CanEdit
	 *		  編集可能時にTrue
	 */
	public Boolean canEdit(){
		// ステータスが『01.作成中』までが編集可能
		return accCon.hasCRApprovalPermission()
					|| isHQEmployee		//本社プロファイルにはCR_Agency__cに対してすべてのデータの編集権限を付与してある
					|| record.Status__c == STATUS_01 
					|| (record.Status__c == STATUS_02a && accCon.isEmployee())		//02.確認中は社員のみ更新可能とする
					|| String.isBlank(record.Status__c);	
	}

	/**
	 *	  CanDelete
	 *		  削除可能時にTrue
	 */
	public Boolean canDelete(){
		//編集可能時に削除可能
		return canEdit();
	}

	/**
	 *	  hasNextAction
	 *		  申請/報告/承認可能時にTrue
	 */
	public Boolean hasNextAction(){
		return actions.nextAction != OutSrcAction.NONE;
	}
	//  申請/報告アクションのラベル名
	public String getNextActionLabel(){
		return actions.getNextActionLabel();
	}
	public PageReference doNextAction(){
		//申請
		if(actions.nextAction == OutSrcAction.APPLY){
			return doApply();
		}
		//報告
		if(actions.nextAction == OutSrcAction.REPORT){
			return doReport();
		}
		//承認
		if(actions.nextAction == OutSrcAction.APPROVE){
			return doApprove();
		}
		//委託終了 または委託終了日変更
		if(actions.nextAction == OutSrcAction.TERMINATE || actions.nextAction == OutSrcAction.TERMINATE_EDIT){
			return doTerminate();
		}
	
		return null;
	}

	//申請アクション
	private PageReference doApply(){
		E_PageMessagesHolder.getInstance().clearMessages();
		// ステータスを『02.申請中』に更新	添付ファイル有無チェックを行う
		updateStatus(STATUS_02a, true);
		return null;
	}
	//報告アクション
	private PageReference doReport(){
		E_PageMessagesHolder.getInstance().clearMessages();
		// ステータスを『02.報告済』に更新		//報告時は添付ファイルチェックを行わないDF-001149
		updateStatus(STATUS_02b, false);
		return null;
	}
	
	//承認アクション
	private PageReference doApprove(){
		E_PageMessagesHolder.getInstance().clearMessages();
		// ステータスを『02.報告済』に更新
		updateStatus(STATUS_03, false);
		
		return null;
	}
	//委託終了アクション	 
	private PageReference doTerminate(){
		// 委託終了画面へ遷移
		PageReference pr = Page.E_CROutsrcTerminate;
		pr.getParameters().put('id',record.Id);
		return pr;
	}


	/**
	 *	  hasRejectAction
	 *		  申請/報告/承認可能時にTrue
	 */
	public Boolean hasRejectAction(){
		return actions.rejectAction != OutSrcAction.NONE;
	}
	//  差戻しアクションのラベル名
	public String getRejectActionLabel(){
		return actions.getRejectActionLabel();
	}
	
	// 差戻しアクション
	public PageReference doRejectAction(){
		E_PageMessagesHolder.getInstance().clearMessages();
		// ステータスを『01.作成中』に更新
		updateStatus(STATUS_01, false);
		
		return null;
	}
	
	/**
	 *	  updateStatus
	 *		  ステータスの更新
	 */
	private void updateStatus(String status, Boolean requiredFiles){
		SavePoint sp = Database.setSavePoint();
		String beforeStatus = record.Status__c;
		// ステータス更新
		record.Status__c = status;
		Boolean saveResult = E_GenericDao.saveViaPage(record);
		
		//添付ファイルチェックを行う
		if(requiredFiles){
			if(validateFiles()==false){
				saveResult = false;
				Database.rollback(sp);		//ファイル添付がない場合Rollbackを実施
			}
		}
		
		// アクション再取得
		if(saveResult){
			actions = getAvailableActions();
		}else{
			record.Status__c = beforeStatus;
		}	
	}
	
	//添付ファイルが1件以上またはファイル添付できない理由が記載されている場合にTrue
	@TestVisible
	private Boolean validateFiles(){
		if(uploadedFileCount==0 && String.isBlank(record.reasonForNofiles__c)){
			E_PageMessagesHolder.getInstance().addErrorMessage(CR_Const.ERR_MSG04_REQUIRED_FILES);
			return false;
		}
		return true;
	}
	
	/**
	 *	　NextActionが報告の場合にTrue　→　個人情報取扱なしのワーニングを表示
	 */
	public Boolean nextActionIsReport(){
		return actions.nextAction == OutSrcAction.REPORT;
	}
}