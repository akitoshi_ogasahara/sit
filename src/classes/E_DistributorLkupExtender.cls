global with sharing class E_DistributorLkupExtender extends E_AbstractSearchExtender {

	E_DistributorLkupController extension;
    //ページタイトル
	private static final String PAGE_TITLE = '代理店選択';

    /* コンストラクタ */
    public E_DistributorLkupExtender(E_DistributorLkupController extension){
		super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        extension.execInitialSearch = false;        //初期検索を実施しない
        //社員ユーザ拠点権限の場合には、EIDCのZINQUIRRの3桁目から2桁は**以外の場合、支社権限と意味して、ECL1のZBUSBRと一致するレコードのみ検索可能。
        if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL)) {
	        SkyEditor2.Query tableQuery1 = extension.queryMap.get('searchDataTable');
	        tableQuery1.addWhereIfNotFirst('AND');
	        tableQuery1.addWhere('E_CL1PF_ZBUSBR__c', access.getAccessKindFromThree(), SkyEditor2.WhereOperator.Eq);
	        extension.queryMap.put('searchDataTable',tableQuery1);
        }
    }

    /* 半角カナ入力値を全角カナへ変換 */
    global override void preSearch() {
    	IF(extension != null){
			String ipValDistributorCode = extension.ipVal_distributorCode.SkyEditor2__Text__C;
			String ipValDairitenkana = extension.ipVal_dairitenkana.SkyEditor2__Text__C;
	
			if (E_Util.isNotNull(ipValDistributorCode) && (!E_Util.isEnAlphaNum(ipValDistributorCode) || ipValDistributorCode.length() != 5)) {
				throw new SkyEditor2.ExtenderException(getMSG().get('LKM|001'));
			}
    		
    		//①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ
    		IF(ipValDairitenkana != null ){
    			extension.ipVal_dairitenkana.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(ipValDairitenkana))));
    			ipValDairitenkana = extension.ipVal_dairitenkana.SkyEditor2__Text__c;
    		}
    		
        	//DF-000618対応  start
        	if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL) && (E_Util.isNotNull(ipValDistributorCode) || E_Util.isNotNull(ipValDairitenkana))) {
	        	String soql = 'Select E_CL1PF_ZBUSBR__c From Account ';
	        	List<String> whereList = new List<String>();
	        	if (E_Util.isNotNull(ipValDistributorCode)) {
	        		whereList.add('E_CL1PF_ZHEADAY__c =: ipValDistributorCode');
	        	}
	        	if (E_Util.isNotNull(ipValDairitenkana)) {
	        		whereList.add('E_CL1PF_ZAHKNAME__c =: ipValDairitenkana');
	        	}
	        	whereList.add('E_COMM_VALIDFLAG__c = \'1\'');
	        	whereList.add('E_IsAgency__c = true');
	        	if (whereList.size() > 0) {
					soql += 'Where ';
					soql += String.join(whereList, ' and ');
				}
	        	soql += ' limit 100';
	        	List<Account> recs = Database.query(soql);
	        	if (recs.size() > 0) {
		        	boolean isReference = false;
		        	for (Account acc : recs) {
						if (access.getAccessKindFromThree().equals(acc.E_CL1PF_ZBUSBR__c)) {
							isReference = true;
						}
		        	}
		        	if (!isReference){
						throw new SkyEditor2.ExtenderException(getMSG().get('LKM|004'));
					}
	        	}
        	}
        	//DF-000618対応  end
    	}
    }

}