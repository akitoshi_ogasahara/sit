/**
 *		レポートへのリダイレクトページ　コントローラ
 *			/apex/E_Report?rep=xxxxx　でレポートのDeveloperNameを受け取ってページ遷移
 */
public with sharing class E_ReportController extends E_AbstractController{
	/**
	 *		page action
	 */
	public PageReference pageAction(){
		Report rep = null;
		String repName = ApexPages.currentPage().getParameters().get('rep');
		if(String.isNotBlank(repName)){
			rep = getRecByDevName(repName);
		}
	
		if(rep!=null){
			return new PageReference('/'+rep.Id);
		}
		pageRef = Page.E_ErrorPage;
		pageRef.getParameters().put('code', 'ERR|005');
		return E_Util.toErrorPage(pageRef,  new List<String>{repName});
	}
	
	/**
	 *		レポートレコード抽出
	 */
	private static Report getRecByDevName(String devnm){
		for(Report rp:[SELECT Id,Name 
						FROM Report
						WHERE developerName = :devnm]){
			return rp;
		}
		return null;
	}
}