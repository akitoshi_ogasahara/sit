public with sharing class ASS_MRSummaryController {
	//ログインユーザId
	private String usId;
	//営業部ID
	private String unitId;
	//営業部
	private E_Unit__c unit;
	//営業部名
	public String uniName{get; private set;}
	//管理者フラグ
	public Boolean isEmployee{get; private set;}	

	//コンストラクタ
	public ASS_MRSummaryController () {
		usId = UserInfo.getUserId();
		unitId = ApexPages.currentPage().getParameters().get('unitId');
		isEmployee = String.isNotEmpty(unitId);
	}

	//MRリスト設定
	public List<User> getMRList() {
		List<User> usList = new List<User>();

		//ログインユーザ
		User loginUser = new User();
		if(!isEmployee){
			loginUser = ASS_UserDao.getUserById(usId);
			usList.add(loginUser);
		}
		for(User us : getMrSummary(loginUser)){
			usList.add(us);
		}
		return usList;
	}

	//MRリスト取得
	private List<User> getMrSummary(User loginUser) {
		//営業部名
		uniName = '';

		//営業部名を取得（管理者用）
		if(isEmployee){
			unit = ASS_E_UnitDaoWithout.getUnitById(unitId);
			if(unit != null){
				uniName = unit.Name;
			}
		//営業部名を取得（MR・拠点長用）
		} else {
			//ログインユーザのUnit__c
			uniName = loginUser.Unit__c;
		}

		List<User> usList = new List<User>();

		//営業部名が一致するログインユーザ以外のユーザのリストを取得
		if(String.isNotEmpty(uniName)){
			usList = ASS_UserDao.getOtherUsersByUnitName(uniName,usId);
		}
		return usList;
	}

	//画面遷移
	public PageReference moveToMRPage() {
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',ApexPages.currentPage().getParameters().get('mrId'));
		pr.getParameters().put('unitId',unitId);
		pr.setRedirect(true);
		return pr;
	}
	//戻る
	public PageReference back() {
		PageReference pr = Page.ASS_UnitSummary;
		pr.setRedirect(true);
		return pr;
	}
}