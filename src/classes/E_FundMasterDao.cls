public with sharing class E_FundMasterDao {

	public static E_FundMaster__c getRecByIdChildSFGPF (Id fundMasterId) {
        E_FundMaster__c rec = null;
        List<E_FundMaster__c> recs = [
            Select Id, Name, CURRFROM__c, CURRTO__c, ZGFUNDCD__c, ZGFUNDNM__c, ZEQTYLMT__c, ZARRFLAG__c,
        		 SHORTDESC__c, LONGDESC__c, ZGRPFND01__c, ZGRPFND02__c, ZGRPFND03__c,
        		(Select Id, 
	        		E_SVCPF__r.ZFSHFROM__c, E_SVCPF__r.ZFSDFROM__c, E_SVCPF__r.ZFSMFROM__c, E_SVCPF__r.ZFSHTO__c, E_SVCPF__r.ZFSDTO__c, E_SVCPF__r.ZFSMTO__c,
	        		E_SVCPF__r.FLAG02__c, E_SVCPF__r.FLAG01__c, E_SVCPF__r.ZVINDEX__c, E_SVCPF__r.ZEFUNDCD__c, E_SVCPF__r.ZCATFLAG__c, E_SVCPF__r.ZFUNDNAMEUNION__c,
	        		E_SVCPF__r.ZFUNDNAM01__c, E_SVCPF__r.ZFUNDNAM02__c, E_SVCPF__r.ZFUNDNAM03__c, E_SVCPF__r.ZFUNDNAM04__c, E_SVCPF__r.ZEQTYRTO__c, E_SVCPF__r.ZCOLORCD__c,
	        		E_SVCPF__r.ZSWENDDT__c, E_SVCPF__r.CURRFROM__c, E_SVCPF__r.CURRTO__c, E_SVCPF__r.ZFUNDNAMEUNIONEmail__c
        		From E_SFGPFs__r
        		Where
        			((E_SVCPF__r.CURRFROM__c <= :E_Util.formatDataSyncDate('yyyyMMdd') And E_SVCPF__r.CURRTO__c > :E_Util.formatDataSyncDate('yyyyMMdd')) And E_SVCPF__r.CURRFROM__c <> :'0')
        		And
        			((E_SVCPF__r.CURRFROM__c != null And E_SVCPF__r.CURRFROM__c <=: E_Util.getSysDate(null)) And E_SVCPF__r.FLAG01__c = true)
       			order by SortNumber__c
       			)
        	From E_FundMaster__c
            Where id =: fundMasterId
        ];
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
	}
}