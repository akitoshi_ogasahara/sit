global with sharing class CR_AgencySearchOSSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public CR_Agency__c record{get;set;}	
			
	public CR_AgencySearchOSExtender getExtender() {return (CR_AgencySearchOSExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component57_val {get;set;}	
		public SkyEditor2.TextHolder Component57_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component27_val {get;set;}	
		public SkyEditor2.TextHolder Component27_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component21_val {get;set;}	
		public SkyEditor2.TextHolder Component21_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component19_val {get;set;}	
		public SkyEditor2.TextHolder Component19_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component54_val {get;set;}	
		public SkyEditor2.TextHolder Component54_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component25_val {get;set;}	
		public SkyEditor2.TextHolder Component25_op{get;set;}	
			
	public String recordTypeRecordsJSON_CR_Agency_c {get; private set;}
	public String defaultRecordTypeId_CR_Agency_c {get; private set;}
	public String metadataJSON_CR_Agency_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public CR_AgencySearchOSSVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = CR_Agency__c.fields.AgencyCode__c;
		f = CR_Agency__c.fields.AgencyNameKana__c;
		f = CR_Agency__c.fields.OutOfBusiness__c;
		f = CR_Agency__c.fields.MRUnit__c;
		f = CR_Agency__c.fields.MRCode__c;
		f = CR_Agency__c.fields.HasOutsrc__c;
		f = CR_Agency__c.fields.HasConfirmationInOutSrc__c;
		f = CR_Agency__c.fields.OutsrcList__c;
		f = CR_Agency__c.fields.MRName__c;
		f = CR_Agency__c.fields.AgencyName__c;
		f = CR_Agency__c.fields.OutsrcCnt__c;
		f = CR_Agency__c.fields.ConfirmationInOutSrcCnt__c;
		f = CR_Agency__c.fields.LastModifiedDate;
		f = CR_Agency__c.fields.BusinessStatus__c;
		f = CR_Agency__c.fields.RecordTypeId;
		f = CR_Agency__c.fields.MR__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = CR_Agency__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder('co');	
					
				Component57_val = new SkyEditor2__SkyEditorDummy__c();	
				Component57_op = new SkyEditor2.TextHolder('co');	
					
				Component27_val = new SkyEditor2__SkyEditorDummy__c();	
				Component27_op = new SkyEditor2.TextHolder('eq');	
					
				Component21_val = new SkyEditor2__SkyEditorDummy__c();	
				Component21_op = new SkyEditor2.TextHolder('co');	
					
				Component19_val = new SkyEditor2__SkyEditorDummy__c();	
				Component19_op = new SkyEditor2.TextHolder('co');	
					
				Component54_val = new SkyEditor2__SkyEditorDummy__c();	
				Component54_op = new SkyEditor2.TextHolder('eq');	
					
				Component25_val = new SkyEditor2__SkyEditorDummy__c();	
				Component25_op = new SkyEditor2.TextHolder('eq');	
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('CR_Agency__c')
						.addFieldAsOutput('OutsrcList__c')
						.addFieldAsOutput('MRUnit__c')
						.addFieldAsOutput('MRName__c')
						.addFieldAsOutput('AgencyCode__c')
						.addFieldAsOutput('AgencyName__c')
						.addFieldAsOutput('OutsrcCnt__c')
						.addFieldAsOutput('ConfirmationInOutSrcCnt__c')
						.addFieldAsOutput('LastModifiedDate')
						.addFieldAsOutput('BusinessStatus__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'AgencyCode__c', Component15_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component57_val, 'SkyEditor2__Text__c', 'AgencyNameKana__c', Component57_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component27_val, 'SkyEditor2__Checkbox__c', 'OutOfBusiness__c', Component27_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component21_val, 'SkyEditor2__Text__c', 'MRUnit__c', Component21_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component19_val, 'SkyEditor2__Text__c', 'MRCode__c', Component19_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component54_val, 'SkyEditor2__Checkbox__c', 'HasOutsrc__c', Component54_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component25_val, 'SkyEditor2__Checkbox__c', 'HasConfirmationInOutSrc__c', Component25_op, true, 0, true ))
						.addWhere(' ( RecordType.DeveloperName = \'CR_Outsrc\')')
.addSort('OutOfBusiness__c',True,True).addSort('MRUnit__c',True,False).addSort('MR__c',True,False).addSort('LastModifiedDate',False,True)
				);	
					
					resultTable = new resultTable(new List<CR_Agency__c>(), new List<resultTableItem>(), new List<CR_Agency__c>(), null);
				resultTable.ignoredOnSave = true;
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(CR_Agency__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_AgencySearchOSExtender(this);
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyCode_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyCode__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyNameKana_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyNameKana__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_OutOfBusiness_c() { 
			return getOperatorOptions('CR_Agency__c', 'OutOfBusiness__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_MRUnit_c() { 
			return getOperatorOptions('CR_Agency__c', 'MRUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_MRCode_c() { 
			return getOperatorOptions('CR_Agency__c', 'MRCode__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_HasOutsrc_c() { 
			return getOperatorOptions('CR_Agency__c', 'HasOutsrc__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_HasConfirmationInOutSrc_c() { 
			return getOperatorOptions('CR_Agency__c', 'HasConfirmationInOutSrc__c');	
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public CR_Agency__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, CR_Agency__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (CR_Agency__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}