@isTest
private class TestMD_DoctorTrigger {

	//test001:ドクターの住所情報以外が更新された場合
	static testMethod void updateMD_DoctorTest001() {
		//テストレコード作成
		string name1 = 'test';
		string state1 = '東京都';
		string address1 = '中央区';
		string postalcode1 = '1112222';
		decimal longitude1 = 1;
		decimal latitude1 = 2;
		MD_Doctor__c docRec = new MD_Doctor__c(name = name1, State__c = state1, Address__c = address1, PostalCode__c = postalcode1,
											   Location__Longitude__s =longitude1, Location__Latitude__s = latitude1);
		insert docRec;

		//住所情報（都道府県、住所）以外を更新
		docRec.PostalCode__c = '1113333';
		update docRec;

		//緯度経度がNull更新されていないことを確認
		MD_Doctor__c updDocRec = [select Location__Longitude__s, Location__Latitude__s from MD_Doctor__c where name = :name1 limit 1];
		System.assertNotEquals(null, updDocRec.Location__Longitude__s);
		System.assertNotEquals(null, updDocRec.Location__Latitude__s);
	}

	//test002:ドクターの住所情報のうち都道府県のみが更新された場合
	static testMethod void updateMD_DoctorTest002() {
		//テストレコード作成
		string name1 = 'test';
		string state1 = '東京都';
		string address1 = '中央区';
		string postalcode1 = '1112222';
		decimal longitude1 = 1;
		decimal latitude1 = 2;
		MD_Doctor__c docRec = new MD_Doctor__c(name = name1, State__c = state1, Address__c = address1, PostalCode__c = postalcode1,
											   Location__Longitude__s =longitude1, Location__Latitude__s = latitude1);
		insert docRec;

		//住所情報の都道府県のみを更新
		docRec.State__c = '千葉県';
		update docRec;

		//緯度経度がNull更新されていることを確認
		MD_Doctor__c updDocRec = [select Location__Longitude__s, Location__Latitude__s from MD_Doctor__c where name = :name1 limit 1];
		System.assertEquals(null, updDocRec.Location__Longitude__s);
		System.assertEquals(null, updDocRec.Location__Latitude__s);
	}

	//test003:ドクターの住所情報のうち住所のみが更新された場合
	static testMethod void updateMD_DoctorTest003() {
		//テストレコード作成
		string name1 = 'test';
		string state1 = '東京都';
		string address1 = '中央区';
		string postalcode1 = '1112222';
		decimal longitude1 = 1;
		decimal latitude1 = 2;
		MD_Doctor__c docRec = new MD_Doctor__c(name = name1, State__c = state1, Address__c = address1, PostalCode__c = postalcode1,
											   Location__Longitude__s =longitude1, Location__Latitude__s = latitude1);
		insert docRec;

		//住所情報の都道府県のみを更新
		docRec.Address__c = '品川区';
		update docRec;

		//緯度経度がNull更新されていることを確認
		MD_Doctor__c updDocRec = [select Location__Longitude__s, Location__Latitude__s from MD_Doctor__c where name = :name1 limit 1];
		System.assertEquals(null, updDocRec.Location__Longitude__s);
		System.assertEquals(null, updDocRec.Location__Latitude__s);
	}

	//test004:ドクターの住所情報(都道府県、住所）、及びその他の項目が更新された場合
	static testMethod void updateMD_DoctorTest004() {
		//テストレコード作成
		string name1 = 'test';
		string state1 = '東京都';
		string address1 = '中央区';
		string postalcode1 = '1112222';
		decimal longitude1 = 1;
		decimal latitude1 = 2;
		MD_Doctor__c docRec = new MD_Doctor__c(name = name1, State__c = state1, Address__c = address1, PostalCode__c = postalcode1,
											   Location__Longitude__s =longitude1, Location__Latitude__s = latitude1);
		insert docRec;

		//住所情報（都道府県、住所）、その他を更新
		docRec.State__c = '千葉県';
		docRec.Address__c = '品川区';
		docRec.PostalCode__c = '1113333';
		update docRec;

		//緯度経度がNull更新されていることを確認
		MD_Doctor__c updDocRec = [select Location__Longitude__s, Location__Latitude__s from MD_Doctor__c where name = :name1 limit 1];
		System.assertEquals(null, updDocRec.Location__Longitude__s);
		System.assertEquals(null, updDocRec.Location__Latitude__s);
	}

	//test005:ドクターの住所情報(都道府県、住所）が2件更新された場合
	static testMethod void updateMD_DoctorTest005() {
		//テストレコード作成
		MD_Doctor__c docRec;
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		string name1;
		string state1;
		string address1;
		string postalcode1;
		decimal longitude1;
		decimal latitude1;
		for (Integer i = 1; i <= 2; i++) {
			name1 = 'test' + i.format();
			state1 = '東京都' + i.format();
			address1 = '中央区' + i.format();
			postalcode1 = '111' + i.format();
			longitude1 = i/100;
			latitude1 = (i + 1)/100;
			docRec = new MD_Doctor__c(name = name1, State__c = state1, Address__c = address1, PostalCode__c = postalcode1,
									  Location__Longitude__s =longitude1, Location__Latitude__s = latitude1);
			docList.add(docRec);
		}
		insert docList;

		//住所情報（都道府県、住所）を更新
		for (MD_Doctor__c doc: docList) {
			doc.State__c = null;
			doc.Address__c = null;
			doc.PostalCode__c = null;
		}
		update docList;

		//緯度経度がNull更新されていることを確認
		List<MD_Doctor__c> updDocList = [select Location__Longitude__s, Location__Latitude__s from MD_Doctor__c];
		for (MD_Doctor__c updDocRec: updDocList) {
			System.assertEquals(null, updDocRec.Location__Longitude__s);
			System.assertEquals(null, updDocRec.Location__Latitude__s);
		}
	}

	//test006:ドクターの住所情報(都道府県、住所）が201件更新された場合
	static testMethod void updateMD_DoctorTest006() {
		//テストレコード作成
		MD_Doctor__c docRec;
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		string name1;
		string state1;
		string address1;
		string postalcode1;
		decimal longitude1;
		decimal latitude1;
		for (Integer i = 1; i <= 201; i++) {
			name1 = 'test' + i.format();
			state1 = '東京都' + i.format();
			address1 = '中央区' + i.format();
			postalcode1 = '111' + i.format();
			longitude1 = i/100;
			latitude1 = (i + 1)/100;
			docRec = new MD_Doctor__c(name = name1, State__c = state1, Address__c = address1, PostalCode__c = postalcode1,
												   Location__Longitude__s =longitude1, Location__Latitude__s = latitude1);
			docList.add(docRec);
		}
		insert docList;

		//住所情報（都道府県、住所）を更新
		for (MD_Doctor__c doc: docList) {
			doc.State__c = null;
			doc.Address__c = null;
			doc.PostalCode__c = null;
		}
		update docList;

		//緯度経度がNull更新されていることを確認
		List<MD_Doctor__c> updDocList = [select Location__Longitude__s, Location__Latitude__s from MD_Doctor__c];
		for (MD_Doctor__c updDocRec: updDocList) {
			System.assertEquals(null, updDocRec.Location__Longitude__s);
			System.assertEquals(null, updDocRec.Location__Latitude__s);
		}
	}
}