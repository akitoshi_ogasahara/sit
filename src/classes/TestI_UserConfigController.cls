@isTest
private class TestI_UserConfigController {
	
	//initでnullが返ってくる場合
	@isTest static void initNullTest() {
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		createIDManage(us);

		System.runAs(us){
			Test.startTest();
			I_UserConfigController iuc = new I_UserConfigController();
			System.assertEquals(iuc.getShowSubNav(), false);
			PageReference pageref = iuc.pageAction();
			System.assertEquals(pageref,null);
			Test.stopTest();
		}
	}

	//initでnnlinkユーザと判断される場合
	@isTest static void initNnlinkTest() {

		String errMsgNo = E_Const.ERROR_MSG_NO_IDCPF.replace('|',EncodingUtil.urlEncode('|','UTF-8'));

		Test.startTest();
		I_UserConfigController iuc = new I_UserConfigController();
		PageReference pageref = iuc.pageAction();
		//System.assertEquals(pageref.getUrl(),Page.E_AppSwitch.getUrl() + '?' + I_Const.APPSWITCH_URL_PARAM_APP + '=' + I_Const.APP_MODE_NNLINK);
		System.assertEquals(pageref.getUrl(),Page.E_ErrorPage.getUrl() + '?code=' + errMsgNo);
		Test.stopTest();
	}

	//irisハンドラー
	@isTest static void getIrisHandlerTest() {

		Test.startTest();
		I_UserConfigController iuc = new I_UserConfigController();
		E_IRISHandler iris = iuc.getIrisHandler();
		E_IRISHandler asssert = E_IRISHandler.getInstance();
		System.assertEquals(iris,asssert);
		Test.stopTest();
	}
	

	//テストデータ作成
	//ID管理作成
	static void createIDManage(User us){
		 E_IDCPF__c ic = new E_IDCPF__c();
		 ic.ZWEBID__c = '1234567890';
		 ic.ZIDOWNER__c = 'AG' + 'test01';
		 ic.User__c = us.Id;
		 ic.AppMode__c = 'IRIS';
		 ic.ZINQUIRR__c = 'BR00';
		 insert ic;
	}
}