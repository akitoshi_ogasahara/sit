public with sharing class E_DiseasesDao{
	/*
	* 検索条件より傷病マスタを取得
	* @param soqlWhere 傷病名マスタの検索条件(Where句)
	*/
	public static List<E_Diseases__c> getDiseaseRecs(String soqlWhere){
		List<E_Diseases__c> diseaseSearchList = new List<E_Diseases__c>();
		String soql = 'SELECT Name'
					+ ', Hiragana__c'
					+ ', Katakana__c'
					+ ', KanaInitial__c'
					+ ', DiseaseNumber__c'
					+ ', DiseaseNumber__r.Id'
					+ ', DiseaseNumber__r.Name'
					+ ', DiseaseNumber__r.Category__c'
					+ ', DiseaseNumber__r.CategoryNumber__c'
					+ ' FROM E_Diseases__c '
					+ soqlWhere;
		diseaseSearchList = Database.query(soql);
		return diseaseSearchList;
	}
}