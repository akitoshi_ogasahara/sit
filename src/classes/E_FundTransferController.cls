global with sharing class E_FundTransferController extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_FundTransferExtender getExtender() {return (E_FundTransferExtender)extender;}
    
    
    public E_SVFPF_c_E_Policy_c E_SVFPF_c_E_Policy_c {get; private set;}
    public E_FundTransferController(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2890');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2890',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component185');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component185',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_FundTransferAgree');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','89');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2920');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2920',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2775');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2775',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('ischk','{!Extender.isAgree}');
        tmpPropMap.put('bflbl','');
        tmpPropMap.put('afLbl','{!Extender.disclaimer[\'FTA|201\']}');
        tmpPropMap.put('Component__Width','15');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1985');
        tmpPropMap.put('Component__Name','ECheckBox');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1985',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2894');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2894',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2899');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2899',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component610');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component610',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','株式比率の計算例');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!Extender.isViewStockHolding && Extender.isSZSYSX}');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_StocksharesRatio');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','94');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2921');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2921',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_FundTransferEntry');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','92');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2922');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2922',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2756');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2756',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','1');
        tmpPropMap.put('executeScreen','entry');
        tmpPropMap.put('updateData','{!Extender.jsonUpdateData}');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component2672');
        tmpPropMap.put('Component__Name','EPieGraphPolicy');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2672',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_record','{!EntryList_item.record}');
        tmpPropMap.put('p_now','{!Extender.NOWTIME}');
        tmpPropMap.put('Component__Width','70');
        tmpPropMap.put('Component__Height','20');
        tmpPropMap.put('Component__id','Component2758');
        tmpPropMap.put('Component__Name','E_ZHLDPERC');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2758',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_record','{!EntryListSPVA_item.record}');
        tmpPropMap.put('p_now','{!Extender.NOWTIME}');
        tmpPropMap.put('Component__Width','70');
        tmpPropMap.put('Component__Height','20');
        tmpPropMap.put('Component__id','Component2757');
        tmpPropMap.put('Component__Name','E_ZHLDPERC');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2757',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_val','{!Extender.totalNowInt}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!Extender.isFundDatatable}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
        tmpPropMap.put('Component__Width','98');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2759');
        tmpPropMap.put('Component__Name','ENumberLabel');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2759',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2898');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2898',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2900');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2900',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component2074');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2074',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_FundTransferConfirm');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','91');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2923');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2923',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2856');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2856',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','1');
        tmpPropMap.put('executeScreen','confirm');
        tmpPropMap.put('updateData','{!Extender.jsonUpdateData}');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component2673');
        tmpPropMap.put('Component__Name','EPieGraphPolicy');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2673',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('clrs','{!coliTab_item.record.CHDRNUM__c}');
        tmpPropMap.put('Component__Width','40');
        tmpPropMap.put('Component__Height','20');
        tmpPropMap.put('Component__id','Component2678');
        tmpPropMap.put('Component__Name','EListColors');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2678',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('clrs','{!spvaTab_item.record.CHDRNUM__c}');
        tmpPropMap.put('Component__Width','40');
        tmpPropMap.put('Component__Height','20');
        tmpPropMap.put('Component__id','Component2676');
        tmpPropMap.put('Component__Name','EListColors');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2676',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_val','{!Extender.totalNowInt}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!Extender.isFundDatatable}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2762');
        tmpPropMap.put('Component__Name','ENumberLabel');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2762',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('secret','{!Extender.inputPassWord}');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2350');
        tmpPropMap.put('Component__Name','EInputSecret');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2350',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('oldpw','{!Extender.inputPassWord}');
        tmpPropMap.put('newpw','{!Extender.newPassWord}');
        tmpPropMap.put('vrfnewpw','{!Extender.verPassWord}');
        tmpPropMap.put('newml','');
        tmpPropMap.put('vrfnewml','');
        tmpPropMap.put('chkval','');
        tmpPropMap.put('pwtype','2');
        tmpPropMap.put('ptype','');
        tmpPropMap.put('Component__Width','400');
        tmpPropMap.put('Component__Height','116');
        tmpPropMap.put('Component__id','Component2710');
        tmpPropMap.put('Component__Name','EChangePW');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2710',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2904');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2904',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2905');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2905',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component2361');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2361',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_FundTransferComplete');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2924');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2924',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2877');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2877',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','1');
        tmpPropMap.put('executeScreen','complete');
        tmpPropMap.put('updateData','{!Extender.jsonUpdateData}');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component2674');
        tmpPropMap.put('Component__Name','EPieGraphPolicy');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2674',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2919');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2919',tmpPropMap);

        lastPageNumber = 4;
        pagesMap.put(1,'E_FundTransferAgree');
        pagesMap.put(2,'E_FundTransferEntry');
        pagesMap.put(3,'E_FundTransferConfirm');
        pagesMap.put(4,'E_FundTransferComplete');

        SObjectField f;

        f = E_Policy__c.fields.COMM_CHDRNUM__c;
        f = E_Policy__c.fields.SPVA_ZGRPLABEL__c;
        f = E_SVFPF__c.fields.SVFPFCode__c;
        f = E_SVFPF__c.fields.ZREVAMT__c;
        f = E_SVFPF__c.fields.ZHLDPERC__c;
        f = E_SVFPF__c.fields.InputZINVPERC__c;
        f = E_Policy__c.fields.SPVA_ZTFRCNT__c;
        f = E_SVFPF__c.fields.InputZHLDPERC__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = E_Policy__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            recordTypeIdMap = SkyEditor2.Util.SetRecordTypesMap('E_Policy__c');
            mainQuery = new SkyEditor2.Query('E_Policy__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
            mainQuery.addFieldAsOutput('SPVA_ZGRPLABEL__c');
            mainQuery.addFieldAsOutput('SPVA_ZTFRCNT__c');
            mainQuery.addFieldAsOutput('SPVA_ZANNDSP__c');
            mainQuery.addFieldAsOutput('SpClassification__c');
            mainQuery.addFieldAsOutput('SystemModstamp');
            mainQuery.addFieldAsOutput('Id');
            mainQuery.addFieldAsOutput('E_FundMaster__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.CURRFROM__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.CURRTO__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.SystemModstamp');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZGFUNDCD__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.Name');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZGFUNDNM__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZEQTYLMT__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.LastModifiedById');
            mainQuery.addFieldAsOutput('E_FundMaster__r.LastModifiedDate');
            mainQuery.addFieldAsOutput('E_FundMaster__r.CreatedById');
            mainQuery.addFieldAsOutput('E_FundMaster__r.CreatedDate');
            mainQuery.addFieldAsOutput('E_FundMaster__r.IsDeleted');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZARRFLAG__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.OwnerId');
            mainQuery.addFieldAsOutput('E_FundMaster__r.SHORTDESC__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.LONGDESC__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZGRPFND01__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZGRPFND02__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.ZGRPFND03__c');
            mainQuery.addFieldAsOutput('E_FundMaster__r.dataSyncDate__c');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('RecordType.DeveloperName');
            mainQuery.addFieldAsOutput('Contractor__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_ZCLKNAME__c');
            mainQuery.addFieldAsOutput('Contractor__r.Email');
            mainQuery.addFieldAsOutput('Contractor__r.E_COMM_ZCLADDR__c');
            mainQuery.addFieldAsOutput('Contractor__r.MobilePhone');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLNTNUM__c');
            mainQuery.addFieldAsOutput('Contractor__r.Name');
            mainQuery.addFieldAsOutput('Contractor__r.HomePhone');
            mainQuery.addFieldAsOutput('Contractor__r.LastName');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_ZKNJSEX__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_DOB__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLTPHONE01__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CL3PF_CLNTNUM__c');
            mainQuery.addFieldAsOutput('Contractor__r.FirstName');
            mainQuery.addFieldAsOutput('ContractorName__c');
            mainQuery.addFieldAsOutput('LastModifiedById');
            mainQuery.addFieldAsOutput('LastModifiedDate');
            mainQuery.addFieldAsOutput('CreatedById');
            mainQuery.addFieldAsOutput('CreatedBy.Name');
            mainQuery.addFieldAsOutput('CreatedBy.LastName');
            mainQuery.addFieldAsOutput('CreatedBy.FirstName');
            mainQuery.addFieldAsOutput('CreatedDate');
            mainQuery.addFieldAsOutput('OwnerId');
            mainQuery.addFieldAsOutput('DSPVA_ZREVAMT__c');
            mainQuery.addFieldAsOutput('SPVA_ZFATDCF__c');
            mainQuery.addFieldAsOutput('COMM_ZTFRDCF__c');
            mainQuery.addFieldAsOutput('SPVA_ZTFRDCFA__c');
            mainQuery.addFieldAsOutput('SPVA_ZREVDCF__c');
            mainQuery.addFieldAsOutput('Annuitant__c');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_ZCLKNAME__c');
            mainQuery.addFieldAsOutput('Annuitant__r.E_COMM_ZCLADDR__c');
            mainQuery.addFieldAsOutput('Annuitant__r.MobilePhone');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLNTNUM__c');
            mainQuery.addFieldAsOutput('Annuitant__r.Name');
            mainQuery.addFieldAsOutput('Annuitant__r.HomePhone');
            mainQuery.addFieldAsOutput('Annuitant__r.LastName');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_ZKNJSEX__c');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_DOB__c');
            mainQuery.addFieldAsOutput('Annuitant__r.Birthdate');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLTPHONE01__c');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CL3PF_CLNTNUM__c');
            mainQuery.addFieldAsOutput('SPVA_ZGRPFND01__c');
            mainQuery.addFieldAsOutput('SPVA_ZGRPFND02__c');
            mainQuery.addFieldAsOutput('SPVA_ZGRPFND03__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
            mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
            mainQuery.addFieldAsOutput('COMM_CRTABLE2__c');
            mainQuery.addFieldAsOutput('dataSyncDate__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            E_SVFPF_c_E_Policy_c = new E_SVFPF_c_E_Policy_c(new List<E_SVFPF__c>(), new List<E_SVFPF_c_E_Policy_cItem>(), new List<E_SVFPF__c>(), null);
            listItemHolders.put('E_SVFPF_c_E_Policy_c', E_SVFPF_c_E_Policy_c);
            query = new SkyEditor2.Query('E_SVFPF__c');
            query.addFieldAsOutput('SVFPFCode__c');
            query.addFieldAsOutput('ZREVAMT__c');
            query.addFieldAsOutput('ZHLDPERC__c');
            query.addFieldAsOutput('InputZINVPERC__c');
            query.addFieldAsOutput('InputZHLDPERC__c');
            query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
            relationFields.put('E_SVFPF_c_E_Policy_c', 'E_Policy__c');
            query.limitRecords(500);
            queryMap.put('E_SVFPF_c_E_Policy_c', query);
            
            registRelatedList('E_SVFPFs__r', 'E_SVFPF_c_E_Policy_c');
            
            
            addInheritParameter('RecordTypeId', 'RecordType');
            extender = new E_FundTransferExtender(this);
            p_showHeader = false;
            p_sidebar = false;
            init();
            
            E_SVFPF_c_E_Policy_c.extender = this.extender;
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();

        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    
    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }

    global with sharing  class E_SVFPF_c_E_Policy_cItem extends SkyEditor2.ListItem {
        public E_SVFPF__c record{get; private set;}
        @TestVisible
        E_SVFPF_c_E_Policy_cItem(E_SVFPF_c_E_Policy_c holder, E_SVFPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global class E_SVFPF_c_E_Policy_c extends SkyEditor2.ListItemHolder {
        public List<E_SVFPF_c_E_Policy_cItem> items{get; private set;}
        @TestVisible
        E_SVFPF_c_E_Policy_c(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<E_SVFPF_c_E_Policy_cItem>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new E_SVFPF_c_E_Policy_cItem(this, (E_SVFPF__c)data, recordTypeSelector);
        }
        global override void importByJSON() {
            SkyEditor2.JSONObject.value[] data = SkyEditor2.JSONObject.JSONArray(
                new SkyEditor2.JSONObject.JSONTokener(hiddenValue)
            );
            super.importByJSON(data);
            for (Integer n = items.size() - data.size(); n < items.size(); n++) {
                E_SVFPF_c_E_Policy_cItem i = items[n];
            }
        }
    }
    public E_SVFPF__c E_SVFPF_c_E_Policy_c_Conversion { get { return new E_SVFPF__c();}}
    
    public String E_SVFPF_c_E_Policy_c_selectval { get; set; }
    
    public void callRemove_E_SVFPF_c_E_Policy_c() {
        for(Integer i = E_SVFPF_c_E_Policy_c.items.size() - 1; i >= 0; i--){
           E_SVFPF_c_E_Policy_cItem item = E_SVFPF_c_E_Policy_c.items[i];
           if(item.selected){
                item.remove();
           }
        }
    }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}