@isTest
private class TestE_BarGraphPolicyController {
	//半年ボタンを押下
	//ラチェット死亡保障金額の推移表示フラグがtrueの場合、ラチェット死亡保障額を表示
	static testMethod void doHalfYearButtonTest001(){
        system.debug('test');
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForWeekly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doHalfYearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ラチェット死亡保障額の推移ラベル', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('red', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(3, controller.graphRecs.size());
		System.assertEquals('2015/01/01', controller.graphRecs[0].name);
		System.assertEquals('2015/02/01', controller.graphRecs[1].name);
		System.assertEquals('2015/03/01', controller.graphRecs[2].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(333, graphData.data3);
			System.assertEquals(555, graphData.data4);
		}
	}
	
	//半年ボタンを押下
	//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
	static testMethod void doHalfYearButtonTest002(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, true, 'SA01');
		createTestDataForWeekly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doHalfYearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ステップアップ金額', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('green', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(3, controller.graphRecs.size());
		System.assertEquals('2015/01/01', controller.graphRecs[0].name);
		System.assertEquals('2015/02/01', controller.graphRecs[1].name);
		System.assertEquals('2015/03/01', controller.graphRecs[2].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(444, graphData.data3);
			System.assertEquals(555, graphData.data4);
		}
	}
	
	//半年ボタンを押下
	//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
	//SPVAヘッダの保険種類コードの上2桁がSRの場合、死亡給付金額とステップアップ金額のラベルを変更
	static testMethod void doHalfYearButtonTest003(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, true, 'SR01');
		createTestDataForWeekly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doHalfYearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ステップアップ死亡保障額', controller.thirdGraphLabel);
		System.assertEquals('死亡保険金額', controller.fourthGraphLabel);
		System.assertEquals('green', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(3, controller.graphRecs.size());
		System.assertEquals('2015/01/01', controller.graphRecs[0].name);
		System.assertEquals('2015/02/01', controller.graphRecs[1].name);
		System.assertEquals('2015/03/01', controller.graphRecs[2].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(444, graphData.data3);
			System.assertEquals(555, graphData.data4);
		}
	}
	
	//半年ボタンを押下
	//「ラチェット死亡保障額」「ステップアップ金額」が非表示の場合
	static testMethod void doHalfYearButtonTest004(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, false, 'SA01');
		createTestDataForWeekly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doHalfYearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals(null, controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals(null, controller.thirdGraphColor);
		System.assertEquals(false, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(3, controller.graphRecs.size());
		System.assertEquals('2015/01/01', controller.graphRecs[0].name);
		System.assertEquals('2015/02/01', controller.graphRecs[1].name);
		System.assertEquals('2015/03/01', controller.graphRecs[2].name);
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			System.assertEquals(111, graphData.data1);
			System.assertEquals(222, graphData.data2);
			System.assertEquals(0, graphData.data3);
			System.assertEquals(555, graphData.data4);
		}
	}
	
	//2年ボタンを押下
	//ラチェット死亡保障金額の推移表示フラグがtrueの場合、ラチェット死亡保障額を表示
	static testMethod void do2YearButtonTest001(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.do2YearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ラチェット死亡保障額の推移ラベル', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('red', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		Date twoYearsAgo = Date.newInstance(Date.today().year(), 12, 1).addYears(-2).addMonths(+1);
		Integer year = twoYearsAgo.year();
		Integer month = twoYearsAgo.month();
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(month * 100, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//2年ボタンを押下
	//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
	static testMethod void do2YearButtonTest002(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.do2YearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ステップアップ金額', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('green', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		Date twoYearsAgo = Date.newInstance(Date.today().year(), 12, 1).addYears(-2).addMonths(+1);
		Integer year = twoYearsAgo.year();
		Integer month = twoYearsAgo.month();
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(month * 1000, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//2年ボタンを押下
	//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
	//SPVAヘッダの保険種類コードの上2桁がSRの場合、死亡給付金額とステップアップ金額のラベルを変更
	static testMethod void do2YearButtonTest003(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, true, 'SR01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.do2YearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ステップアップ死亡保障額', controller.thirdGraphLabel);
		System.assertEquals('死亡保険金額', controller.fourthGraphLabel);
		System.assertEquals('green', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		Date twoYearsAgo = Date.newInstance(Date.today().year(), 12, 1).addYears(-2).addMonths(+1);
		Integer year = twoYearsAgo.year();
		Integer month = twoYearsAgo.month();
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(month * 1000, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//2年ボタンを押下
	//「ラチェット死亡保障額」「ステップアップ金額」が非表示の場合
	static testMethod void do2YearButtonTest004(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, false, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.do2YearButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals(null, controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals(null, controller.thirdGraphColor);
		System.assertEquals(false, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		Date twoYearsAgo = Date.newInstance(Date.today().year(), 12, 1).addYears(-2).addMonths(+1);
		Integer year = twoYearsAgo.year();
		Integer month = twoYearsAgo.month();
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(0, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//以来ボタンを押下
	//ラチェット死亡保障金額の推移表示フラグがtrueの場合、ラチェット死亡保障額を表示
	static testMethod void doAllButtonTest001(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doAllButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ラチェット死亡保障額の推移ラベル', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('red', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(36, controller.graphRecs.size());
		Integer year = System.now().addYears(-2).year();
		Integer month = 1;
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(month * 100, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//以来ボタンを押下
	//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
	static testMethod void doAllButtonTest002(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doAllButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ステップアップ金額', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('green', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(36, controller.graphRecs.size());
		Integer year = System.now().addYears(-2).year();
		Integer month = 1;
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(month * 1000, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//以来ボタンを押下
	//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
	//SPVAヘッダの保険種類コードの上2桁がSRの場合、死亡給付金額とステップアップ金額のラベルを変更
	static testMethod void doAllButtonTest003(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, true, 'SR01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doAllButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ステップアップ死亡保障額', controller.thirdGraphLabel);
		System.assertEquals('死亡保険金額', controller.fourthGraphLabel);
		System.assertEquals('green', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(36, controller.graphRecs.size());
		Integer year = System.now().addYears(-2).year();
		Integer month = 1;
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(month * 1000, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//以来ボタンを押下
	//「ラチェット死亡保障額」「ステップアップ金額」が非表示の場合
	static testMethod void doAllButtonTest004(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(false, false, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		PageReference result = controller.doAllButton();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals(null, controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals(null, controller.thirdGraphColor);
		System.assertEquals(false, controller.isDislpayThirdGraph);
		
		//グラフデータ確認
		System.assertEquals(36, controller.graphRecs.size());
		Integer year = System.now().addYears(-2).year();
		Integer month = 1;
		for(E_AbstractBarGraph.GraphData graphData : controller.graphRecs){
			String promiseDate = Datetime.newInstance(year, month, 1, 0, 0, 0).format('yyyy/MM/dd');
			System.assertEquals(promiseDate, graphData.Name);
			System.assertEquals(month, graphData.data1);
			System.assertEquals(month * 10, graphData.data2);
			System.assertEquals(0, graphData.data3);
			System.assertEquals(month * 10000, graphData.data4);
			
			month++;
			if(month > 12){
				year++;
				month = 1;
			}
		}
	}
	
	//グラフを表示するか？
	//初期表示
	static testMethod void getIsDisplayGraphTest01(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		Boolean result = controller.getIsDisplayGraph();
		Test.stopTest();
		
		System.assertEquals(false, result);
	}
	
	//グラフを表示するか？
	//データがないとき
	static testMethod void getIsDisplayGraphTest02(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		//createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		controller.doAllButton();
		
		Test.startTest();
		Boolean result = controller.getIsDisplayGraph();
		Test.stopTest();
		
		System.assertEquals(false, result);
	}
	
	//グラフを表示するか？
	//上記以外
	static testMethod void getIsDisplayGraphTest03(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		controller.doAllButton();
		
		Test.startTest();
		Boolean result = controller.getIsDisplayGraph();
		Test.stopTest();
		
		System.assertEquals(true, result);
	}
	
	//グラフを表示しないときのメッセージ
	//初期表示
	static testMethod void getNoGraphMessageTest01(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		String result = controller.getNoGraphMessage();
		Test.stopTest();
		
		System.assertEquals('期間を選択してください。', result);
	}
	
	//グラフを表示しないときのメッセージ
	//データがないとき
	static testMethod void getNoGraphMessageTest02(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		//createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		controller.doAllButton();
		
		Test.startTest();
		String result = controller.getNoGraphMessage();
		Test.stopTest();
		
		System.assertEquals('データがありません。', result);
	}
	
	//グラフを表示しないときのメッセージ
	//上記以外
	static testMethod void getNoGraphMessageTest03(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		controller.doAllButton();
		
		Test.startTest();
		String result = controller.getNoGraphMessage();
		Test.stopTest();
		
		System.assertEquals('', result);
	}
	
	//グラフ表示データリスト
	static testMethod void getDataListTest001(){
		
		//テストデータ作成
		E_Policy__c parent = createPolicy(true, true, 'SA01');
		createTestDataForMonthly(parent.Id);
		
		//コントローラ作成
		E_BarGraphPolicyController controller = createController(parent);
		
		Test.startTest();
		List<E_AbstractBarGraph.GraphData> result = controller.getDataList();
		Test.stopTest();
		
		//ラベル確認
		System.assertEquals('最低死亡保障額の推移ラベル', controller.secondGraphLabel);
		System.assertEquals('ラチェット死亡保障額の推移ラベル', controller.thirdGraphLabel);
		System.assertEquals('死亡給付金額', controller.fourthGraphLabel);
		System.assertEquals('red', controller.thirdGraphColor);
		System.assertEquals(true, controller.isDislpayThirdGraph);
	}
	
	//保険契約ヘッダを作成
	static E_Policy__c createPolicy(Boolean zrcdcf, Boolean zstupdsp, String crtable){
		E_Policy__c parent = new E_Policy__c();
		parent.SPVA_ZRCDCF__c = zrcdcf;
		parent.SPVA_ZSTUPDSP__c = zstupdsp;
		parent.SPVA_ZSINSDESC__c = '最低死亡保障額の推移ラベル';
		parent.SPVA_ZRCLABEL01__c = 'ラチェット死亡保障額の推移ラベル';
		parent.COMM_CRTABLE__c = crtable;
		insert parent;
		return parent;
	}
	
	//半年用テストデータを作成
	static List<E_SPRPF_Weekly__c> createTestDataForWeekly(Id parentId){
		List<E_SPRPF_Weekly__c> insertList = new List<E_SPRPF_Weekly__c>();
		for(Integer i = 1; i <= 3; i++){
			String zprmsdte = '20150' + i + '01';
			insertList.add(createWeekly(parentId, zprmsdte));
		}
		insert insertList;
		return insertList;
	}
	
	//SPVA積立金Weeklyを作成
	static E_SPRPF_Weekly__c createWeekly(Id parentId, String zprmsdte){
		E_SPRPF_Weekly__c record = new E_SPRPF_Weekly__c();
		record.E_Policy__c = parentId;
		record.ZPRMSDTE__c = zprmsdte;
		record.ZREVAMT__c = 111;
		record.SUMINS__c = 222;
		record.ZRCHTSUM__c = 333;
		record.ZSTUPSUM__c = 444;
		record.ZACTSUM__c = 555;
		return record;
	}
	
	//2年、ALL用テストデータを作成
	static List<E_SPRPF_Monthly__c> createTestDataForMonthly(Id parentId){
		List<E_SPRPF_Monthly__c> insertList = new List<E_SPRPF_Monthly__c>();
		for(Integer i = 0; i > -3; i--){
			String zprmsdte = String.valueOf(System.now().addYears(i).year());
			insertList.add(createMonthly(parentId, zprmsdte));
		}
		insert insertList;
		return insertList;
	}
	
	//SPVA積立金Monthlyを作成
	static E_SPRPF_Monthly__c createMonthly(Id parentId, String targetYear){
		E_SPRPF_Monthly__c record = new E_SPRPF_Monthly__c();
		record.E_Policy__c = parentId;
		record.TargetYear__c = targetYear;
		for(Integer i = 1; i <= 12; i++){
			String month = String.valueOf(i);
			if(month.length() < 2) month = '0' + month;
			record.put('ZPRMSDTE_'+ month +'__c', targetYear + month + '01');
			record.put('ZREVAMT_'+ month +'__c', i);
			record.put('SUMINS_'+ month +'__c', i * 10);
			record.put('ZRCHTSUM_'+ month +'__c', i * 100);
			record.put('ZSTUPSUM_'+ month +'__c', i * 1000);
			record.put('ZACTSUM_'+ month +'__c', i * 10000);
		}
		return record;
	}
	
	//コントローラ作成
	static E_BarGraphPolicyController createController(E_Policy__c parent){
		E_BarGraphPolicyController controller = new E_BarGraphPolicyController();
		controller.parentId = parent.Id;
		controller.ratchetFlag = parent.SPVA_ZRCDCF__c;
		controller.stepupFlag = parent.SPVA_ZSTUPDSP__c;
		controller.sinsdescLabel = parent.SPVA_ZSINSDESC__c;
		controller.ratchetLabel = parent.SPVA_ZRCLABEL01__c;
		controller.typeCode = parent.COMM_CRTABLE__c;
		return controller;
	}
	
}