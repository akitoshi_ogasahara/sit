global class AGC_SetDeleteFlag_Bch implements Database.batchable<AGC_DairitenKihonJoho__c>{
	
	global Iterable<AGC_DairitenKihonJoho__c> start(Database.batchableContext info){
	
		Decimal hanteiDays = -2;
		
		AGC_Config__c conf = AGC_Config__c.getInstance('Default');
		if(conf != null && conf.GyohaiHanteiDays__c != null) hanteiDays = conf.GyohaiHanteiDays__c * -1; 
		
		AGC_Common_Dao common = new AGC_Common_Dao();
		
		Date hanteiDate = Date.today().addDays(Integer.valueOf(hanteiDays));
		
		return common.getGyouhaiRecords(hanteiDate);
		
	}
	
	global void execute(Database.batchableContext info, List<AGC_DairitenKihonJoho__c> scope){
		
		for(AGC_DairitenKihonJoho__c k : scope){
			k.DelRecord__c = true;
		}
		
		update scope;
		
	}

	global void finish(Database.batchableContext info){}
	
}