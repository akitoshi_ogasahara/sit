@isTest
private class TestI_AtriaService {

	/*
	* 観点:getHihokensyaMeritInfo メソッド正常系
	* 条件:適当な文字列を渡してcreateAtriaJsonを実行
	* 検証:List<Object>が返されること
	*/
  	static testMethod void testGetHihokensyaMeritInfo(){
		Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
		I_AtriaService svc = new I_AtriaService();
		Test.startTest();  
		//Map<String,Object> dataMap = svc.getHihokensyaMeritInfo('"hoge"');
		List<Object> lst = svc.getHihokensyaMeritInfo('"hoge"');
		Test.stopTest();
		System.assert(!lst.isEmpty());
  }

  	/*
	* 観点:getHihokensyaMeritInfo メソッド異常系
	* 条件:AtriaのStatusCodeが200でレスポンスボディがInvalid
	* 検証:
	*/
  	static testMethod void testGetHihokensyaMeritInfo_InvalidResponse_200(){
  		String resBody = '{"statusCode":200,"data":{"AuthResult":true,"Exception":"NullPointerException"}}';
  		Map<String,String> resHeaderMap = new Map<String,String>();
		resHeaderMap.put('Content-Type','application/json');
  		I_AtriaServiceMock mock = new I_AtriaServiceMock(200,resBody,resHeaderMap);
		Test.setMock( HttpCalloutMock.class, mock);
		I_AtriaService svc = new I_AtriaService();
		
		Test.startTest();
		try{
			List<Object> lst = svc.getHihokensyaMeritInfo('"hoge"');
			System.assert(false);
		} catch(I_PIPException e){
			System.assertEquals(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_INVALID_RESPONSE,e.getMessage());
			//System.assertEquals(resBody,e.detailMessage);
		}
		Test.stopTest();
	}

	/*
	* 観点:getHihokensyaMeritInfo メソッド異常系
	* 条件:AtriaのStatusCodeが200以外の場合
	* 検証:
	*/
  	static testMethod void testGetHihokensyaMeritInfo_InvalidResponse_500(){
  		String resBody = '{"statusCode":500,"data":{"AuthResult":true,"Exception":"NullPointerException"}}';
  		Map<String,String> resHeaderMap = new Map<String,String>();
		resHeaderMap.put('Content-Type','application/json');
  		I_AtriaServiceMock mock = new I_AtriaServiceMock(200,resBody,resHeaderMap);
		Test.setMock( HttpCalloutMock.class, mock);
		I_AtriaService svc = new I_AtriaService();
		
		Test.startTest();
		try{
			List<Object> lst = svc.getHihokensyaMeritInfo('"hoge"');
			System.assert(false);
		} catch(I_PIPException e){
			System.assertEquals(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_INVALID_RESPONSE,e.getMessage());
			System.assertEquals(resBody,e.detailMessage);
		}
		Test.stopTest();
	}

	/*
	* 観点:createAtriaJson メソッド正常系
	* 条件:適当な文字列を渡してcreateAtriaJsonを実行
	* 検証:正しいkeyを持つJson が返されること
	*/
	static testMethod void testCreateAtriaJson() {
		I_AtriaService svc = new I_AtriaService();
		String resStr = svc.createAtriaJson('"hoge"');
		system.debug(resStr);
		Map<String,Object> resMap = (Map<String,Object>)JSON.deserializeUntyped(resStr);
		system.assert( resMap.containsKey('userId') );
		system.assert( resMap.containsKey('encodedImput') );
		system.assert( resMap.containsKey('requestTimeStr') );
		system.assert( resMap.containsKey('signature') );
		system.assert( resMap.containsKey('atriaRt') );
	}

	/*
	* 観点:calloutAtriaService メソッド正常系
	* 条件:適当な文字列を渡してcalloutAtriaServiceを実行
	* 検証:正しいresponseBody が返されること
	*/

	static testMethod void testCalloutAtriaService() {
		Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
		I_AtriaService svc = new I_AtriaService();

		Test.startTest();
	  	String resStr = svc.calloutAtriaService('hoge');
		Test.stopTest();
		
		Map<String,Object> resMap = (Map<String,Object>)JSON.deserializeUntyped(resStr);
		system.assert( resMap.containsKey('statusCode') );
		system.assert( resMap.containsKey('data') );    
		Map<String,Object> dataMap = (Map<String,Object>)resMap.get('data');
		system.assert( dataMap.containsKey('meritHyo') );
		system.assert( dataMap.containsKey('AuthResult') );
		system.assert( dataMap.containsKey('Exception') );
	}

	/*
	* 観点:calloutAtriaService メソッド異常系
	* 条件:404エラー
	* 検証:404エラーがthrowされること
	*/
	static testMethod void testcalloutAtriaService404() {

		Map<String,String> resHeaderMap = new Map<String,String>();
		resHeaderMap.put('Content-Type','application/json');

		I_AtriaServiceMock mock = new I_AtriaServiceMock(404,'Error',resHeaderMap);
		
		Test.setMock( HttpCalloutMock.class, mock);

		I_AtriaService svc = new I_AtriaService();
		
		Test.startTest();
		try{
			String resStr = svc.calloutAtriaService('hoge');
			System.assert(false);
		} catch(I_PIPException e){
			System.assertEquals(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_404,e.getMessage());
			System.assertEquals('Error',e.detailMessage);
		}
		Test.stopTest();
	}

	/*
	* 観点:calloutAtriaService メソッド異常系
	* 条件:404以外のエラー
	* 検証:OtherStatusのエラーがthrowされること
	*/
	static testMethod void testcalloutAtriaServiceOtherStatus() {

		Map<String,String> resHeaderMap = new Map<String,String>();
		resHeaderMap.put('Content-Type','application/json');

		I_AtriaServiceMock mock = new I_AtriaServiceMock(500,'Error',resHeaderMap);
		
		Test.setMock( HttpCalloutMock.class, mock);

		I_AtriaService svc = new I_AtriaService();
		
		Test.startTest();
		try{
			String resStr = svc.calloutAtriaService('hoge');
			System.assert(false);
		} catch(I_PIPException e){
			System.assertEquals(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_OTHER,e.getMessage());
			System.assertEquals('Error',e.detailMessage);
		}
		Test.stopTest();
	}


	static testMethod void testInsertCallAtriaLog() {
		String detail = 'testDetail';

		I_AtriaService svc = new I_AtriaService();
		svc.guId = 'P-999999';
		
		Test.startTest();
			svc.insertCallAtriaLog(detail);
		Test.stopTest();
		E_log__c resultLog = [select Id, detail__c from E_Log__c Limit 1];
		system.debug(resultLog.detail__c);
		system.assert( resultLog.detail__c.contains('guid:P-999999') );
		system.assert( resultLog.detail__c.contains('detail:testDetail') );
	}
}