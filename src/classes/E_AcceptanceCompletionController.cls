global with sharing class E_AcceptanceCompletionController extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public User record {get{return (User)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_AcceptanceCompletionExtender getExtender() {return (E_AcceptanceCompletionExtender)extender;}
    
    
    public E_AcceptanceCompletionController(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component883');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component883',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','confirmation_registration,change_the_e-mail_address,change_password');
        tmpPropMap.put('isHideMenu','{!NOT(Extender.isVisitedCompliance)}');
        tmpPropMap.put('Component__Width','119');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component812');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component812',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','ホームへ');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!extender.isVisitedCompliance}');
        tmpPropMap.put('p_value','E_Home');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','85');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component897');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component897',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','次へ');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!NOT(Extender.isVisitedCompliance)}');
        tmpPropMap.put('p_value','E_Compliance');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','80');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component898');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component898',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component899');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component899',tmpPropMap);

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = User.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('User');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = false;
            p_sidebar = false;
            extender = new E_AcceptanceCompletionExtender(this);
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }

    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}