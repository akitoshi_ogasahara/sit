public with sharing class I_AgencyInformationCollector {

	public I_AddresseeInformationDTO dto {get; set;}
	private E_AccessController access;
	public List<SearchResultInfo> results {get; set;}

	public enum Type {
		AGENCY,
		OFFICE,
		AGENT
	}
	public List<String> hierarchy = new List<String>{
		Type.AGENCY.name(),
		Type.OFFICE.name(),
		Type.AGENT.name()
	};

	public Integer numberOfHistoryIndex {get; set;}
	public Boolean hasSearched {get; private set;}
	public List<History> histories {get; private set;}
	public History cursor {get; private set;}
	public Boolean hasUnderLayer {get {
		String currenType = this.cursor.type;
		// List#indexOfはspring18から使用可能
		// Integer index = this.hierarchy.indexOf(currenType);
		Integer index = -1;
		for (Integer i = 0; i < this.hierarchy.size(); i++) {
			if (this.hierarchy.get(i) == currenType) {
				index = i;
				break;
			}
		}
		return index < this.hierarchy.size() - 1;
	}}

	// メッセージ
	public List<String> searchMessages {get; set;}
	// メッセージあり
	public Boolean getHasMessages(){
		return searchMessages != null && searchMessages.size() > 0;
	}
	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}
	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}

	public I_AgencyInformationCollector() {
		this.histories = new List<History>();
		this.cursor = new History();
		this.cursor.type = Type.AGENCY.name();
		this.hasSearched = false;
		this.access = E_AccessController.getInstance();
		this.rowCount = I_Const.LIST_DEFAULT_ROWS;
		this.searchMessages = new List<String>();
	}

	public List<SelectOption> getTypeSelectOptions() {
		return new List<SelectOption>{
			new SelectOption(Type.AGENCY.name(), I_Const.SEARCH_TYPE_LABEL_AGENCY),
			new SelectOption(Type.OFFICE.name(), I_Const.SEARCH_TYPE_LABEL_OFFICE),
			new SelectOption(Type.AGENT.name(), I_Const.SEARCH_TYPE_LABEL_AGENT)
		};
	}

	public String getUnderLayer() {
		String currenType = this.cursor.type;
		/* Integer index = this.hierarchy.indexOf(currenType); */
		Integer index = -1;
		for (Integer i = 0; i < this.hierarchy.size(); i++) {
			if (this.hierarchy.get(i) == currenType) {
				index = i;
				break;
			}
		}
		if (index < 0) return null;
		if (++index >= this.hierarchy.size() - 1) {
			index = this.hierarchy.size() - 1;
		}
		return this.hierarchy.get(index);
	}

	public void expand() {
		String currenType = this.cursor.type;
		if (!this.hasUnderLayer) return;
		String nextLayer = this.getUnderLayer();
		if (this.cursor.type == nextLayer) return;
		this.histories.add(this.cursor);
		this.cursor = new History();
		this.cursor.type = nextLayer;
		this.search();
	}

	public void collapse() {
		if (this.histories.size() == 0) return;
		Integer latestIndex = this.histories.size() - 1;
		this.cursor = this.histories.get(latestIndex);
		this.histories.remove(latestIndex);
		this.search();
	}

	public void moveToIndex() {
		Integer numberOfHistoryIndex = this.numberOfHistoryIndex;
		this.numberOfHistoryIndex = null;
		if (numberOfHistoryIndex == null) return;
		if (numberOfHistoryIndex < 0) return;
		if (numberOfHistoryIndex >= this.histories.size()) return;
		this.cursor = this.histories.get(numberOfHistoryIndex);
		for (Integer i = this.histories.size() - 1; i >= numberOfHistoryIndex; i--) {
			this.histories.remove(numberOfHistoryIndex);
		}
		this.search();
	}

	public void resetType() {
		this.histories.clear();
		this.hasSearched = false;
		this.results = new List<SearchResultInfo>();
		this.searchMessages = new List<String>();
	}

	public void search() {
		this.hasSearched = true;
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		results = new List<SearchResultInfo>();
		searchMessages = new List<String>();

		// 検索キーワードが未入力の場合はエラー（代理店、事務所リンク遷移時の場合は除外）
		if(String.isBlank(this.cursor.keyword) && this.histories.size() == 0){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_KEYWORD_NOTHING));
			return ;
		}

		String soqlWhere = '';
		//拠点長、MR用
		String brunchCode;
		if(String.isBlank(access.idcpf.ZINQUIRR__c)){
			brunchCode = '**';
		}else{
			brunchCode = access.idcpf.ZINQUIRR__c.right(2);
		}

		//代理店検索
		if (this.cursor.type == Type.AGENCY.name()) {
			if(brunchCode != '**'){
				soqlWhere = 'AND (E_CL2PF_BRANCH__c = \'' + brunchCode + '\' '
						  + 'OR E_CL2PF_ZBUSBR__c = \'' + brunchCode + '\') ';
			}
			List<Account> agencys = new List<Account>();
			agencys = E_AccountDaoWithout.getRecsIRISAgencySearchBoxAgency(soqlWhere, this.cursor.keyword);

			// 検索結果がゼロ件の場合は処理終了
			if(agencys == null || agencys.isEmpty()){
				searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
				return;
			}

			// 検索結果が上限の場合は処理終了
			if(agencys.size() >= I_Const.LIST_MAX_ROWS){
				searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
				return;
			}

			// 重複チェック用のIDセット
			Set<Id> agencyIdSet = new Set<Id>();
			for(Account agency : agencys){
				if(!agencyIdSet.contains(agency.ParentId)){
					SearchResultInfo result = new SearchResultInfo();
					result.agencyId = agency.ParentId;
					result.agencyName = agency.E_ParentName__c;
					result.agencyNameKana = agency.Parent.E_CL1PF_ZAHKNAME__c;
					result.agencyCode = agency.Parent.E_CL1PF_ZHEADAY__c;

					agencyIdSet.add(agency.ParentId);
					results.add(result);
				}
			}
			this.sortType = SORT_TYPE_AGENCY_NAME;
			this.sortIsAsc = TRUE;
			list_sortType = sortType;
			list_sortIsAsc = sortIsAsc;
			results.sort();
		}

		//事務所検索
		if (this.cursor.type == Type.OFFICE.name()) {
			History history = this.getLatestHistory();
			if (history != null && history.type == 'AGENCY') {
				Id parentId = history.recordId;
				soqlWhere += 'AND ParentId = \'' + parentId + '\' ';
			}

			if(brunchCode != '**'){
				soqlWhere +='AND (E_CL2PF_BRANCH__c = \'' + brunchCode + '\' '
						  + 'OR E_CL2PF_ZBUSBR__c = \'' + brunchCode + '\') ';
			}

			List<Account> offices = new List<Account>();
			offices = E_AccountDaoWithout.getRecsIRISAgencySearchBoxOffice(soqlWhere, this.cursor.keyword);

			// 検索結果がゼロ件の場合は処理終了
			if(offices == null || offices.isEmpty()){
				searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
				return;
			}

			//最大件数以上の場合は警告を表示
			if(offices.size() >= I_Const.LIST_MAX_ROWS){
				searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
				return;
			}

			for(Account office : offices){
				SearchResultInfo result = new SearchResultInfo();
				result.officeId = office.Id;
				result.officeName = office.E_CL2PF_ZEAYNAM__c;
				result.officeNameKana = office.E_CL2PF_ZEAYKNAM__c;
				result.officeCode = office.E_CL2PF_ZAGCYNUM__c;
				result.postalCode = office.CLTPCODE__c;
				result.address = office.E_COMM_ZCLADDR__c;
				result.phone = office.CLTPHONE01__c;

				results.add(result);
			}

			this.sortType = SORT_TYPE_OFFICE_NAME;
			this.sortIsAsc = TRUE;
			list_sortType = sortType;
			list_sortIsAsc = sortIsAsc;
			results.sort();
		}

		if (this.cursor.type == Type.AGENT.name()) {
			History history = this.getLatestHistory();
			Id accountId = null;
			if (history != null && history.type == 'OFFICE') {
				accountId = history.recordId;
				soqlWhere += 'AND AccountId = \'' + accountId + '\' ';
			}

			// 担当する事務所の募集人のみを抽出
			if(brunchCode != '**'){
				soqlWhere +='AND (Account.E_CL2PF_BRANCH__c = \'' + brunchCode + '\' '
						  + 'OR Account.E_CL2PF_ZBUSBR__c = \'' + brunchCode + '\') ';
			}

			List<Contact> agents = new List<Contact>();
			agents = E_ContactDaoWithout.getRecsIRISAgencySearchBoxAgent(soqlWhere, this.cursor.keyword);

			// 検索結果がゼロ件の場合は処理終了
			if(agents == null || agents.isEmpty()){
				if(accountId != null){
					searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_NO_AGENT));
				}else{
					searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
				}
				return;
			}

			//最大件数以上の場合は警告を表示
			if(agents.size() >= I_Const.LIST_MAX_ROWS){
				searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
				return;
			}

			// ユーザ情報取得
			Set<ID> conIds = new Set<ID>();
			for(Contact con : agents){
				conIds.add(con.Id);
			}

			List<User> userList = E_UserDaoWithout.getRecsAgentIdWithEIDC(conIds);
			Map<ID, Boolean> userActiveMap = new Map<ID, Boolean>();
			Map<String, Boolean> agentActiveMap = new Map<String, Boolean>();
			for(User u : userList){
				userActiveMap.put(u.Contact.Id, u.isActive);

				if(!agentActiveMap.containskey(u.AgentCode__c) || (!agentActiveMap.get(u.AgentCode__c) && u.isActive)){
					agentActiveMap.put(u.AgentCode__c, u.isActive);
				}
			}

			results = new List<SearchResultInfo>();
			Set<Id> contactIds = new Set<Id>();
			for(Contact con : agents){
				Boolean isDisplay = false;
				String agentCode = con.E_CL3PF_AGNTNUM__c.substringBefore('|');

				// ユーザ未作成 または ユーザ無効
				if(!userActiveMap.containskey(con.Id) || !userActiveMap.get(con.Id) ){
					// 同一募集人コードに有効ユーザがいない、かつ募集人コードに'|'が含まれていない
					if((!agentActiveMap.containskey(agentCode) || !agentActiveMap.get(agentCode))
					&& con.E_CL3PF_AGNTNUM__c.indexOf('|') == -1){
						isDisplay = true;
					}

				// ユーザが有効
				} else {
					isDisplay = true;
				}

				// 表示判定
				if(!isDisplay){
					continue;
				}

				SearchResultInfo result = new SearchResultInfo();
				result.officeName = con.Account.E_CL2PF_ZEAYNAM__c;
				result.officeNameKana = con.Account.E_CL2PF_ZEAYKNAM__c;
				result.postalCode = con.Account.CLTPCODE__c;
				result.address = con.Account.E_COMM_ZCLADDR__c;
				result.phone = con.Account.CLTPHONE01__c;
				result.agentName = con.Name;
				result.agentNameKana =con.E_CL3PF_ZEATKNAM__c;
				result.agentCode = agentCode;
				results.add(result);

				contactIds.add(con.Id);
			}

			this.sortType = SORT_TYPE_AGENT_NAME;
			this.sortIsAsc = TRUE;
			list_sortType = sortType;
			list_sortIsAsc = sortIsAsc;
			results.sort();
		}

	}

	public History getLatestHistory() {
		if (this.histories.size() == 0) return null;
		Integer latestIndex = this.histories.size() - 1;
		return this.histories.get(latestIndex);
	}

	public class History {
		public String type {get; set;}
		public String keyword {get; set;}
		public Id recordId {get; set;}
		public String displayName {get; set;}
		public I_AddresseeInformationDTO addressee {get; set;}

		public History() {
			this.addressee = new I_AddresseeInformationDTO();
		}
	}

// ソート用
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}
// ソート用
	// ソートキー
	public static String list_sortType {get; set;}
	// ソート順
	public static Boolean list_sortIsAsc {get; set;}
// ローカル定数
	private static final String SORT_TYPE_AGENCY_NAME = 'agencyName';
	private static final String SORT_TYPE_AGENCY_CODE = 'agencyCode';
	private static final String SORT_TYPE_OFFICE_NAME = 'officeName';
	private static final String SORT_TYPE_OFFICE_CODE = 'officeCode';
	private static final String SORT_TYPE_ADDRESS     = 'address';
	private static final String SORT_TYPE_AGENT_NAME  = 'agentName';
	private static final String SORT_TYPE_AGENT_CODE  = 'agentCode';

	/**
	 * ソート
	 */
	public void sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		if(sortType != sType){
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;

		results.sort();

		return;
	}

	public class SearchResultInfo implements Comparable{
		// 代理店Id
		public String agencyId {get; set;}
		// 代理店名
		public String agencyName {get; set;}
		// 代理店名カナ
		public String agencyNameKana {get; set;}
		// 代理店コード
		public String agencyCode {get; set;}
		// 事務所Id
		public String officeId {get; set;}
		// 事務所名
		public String officeName {get; set;}
		// 事務所名カナ
		public String officeNameKana {get; set;}
		// 事務所コード
		public String officeCode {get; set;}
		// 郵便番号
		public String postalCode {get; set;}
		// 住所
		public String address {get; set;}
		// 電話番号
		public String phone {get; set;}
		// 募集人名
		public String agentName {get; set;}
		// 募集人名カナ
		public String agentNameKana {get; set;}
		// 募集人コード
		public String agentCode {get; set;}

		public SearchResultInfo(){
			agencyId = '';
			agencyName = '';
			agencyNameKana = '';
			agencyCode = '';
			officeId = '';
			officeName = '';
			officeNameKana = '';
			officeCode = '';
			postalCode = '';
			address = '';
			phone = '';
			agentName = '';
			agentNameKana = '';
			agentCode = '';
		}

		// ソート
		public Integer compareTo(Object compareTo){
			SearchResultInfo compareToRow = (SearchResultInfo)compareTo;
			Integer result = 0;

			// 代理店名（カナ）
			if(list_sortType == SORT_TYPE_AGENCY_NAME){
				result = I_Util.compareToString(agencyNameKana, compareToRow.agencyNameKana);
			// 代理店コード
			} else if(list_sortType == SORT_TYPE_AGENCY_CODE){
				result = I_Util.compareToString(agencyCode, compareToRow.agencyCode);
			// 事務所名（カナ）
			} else if(list_sortType == SORT_TYPE_OFFICE_NAME){
				result = I_Util.compareToString(officeNameKana, compareToRow.officeNameKana);
			// 事務所コード
			} else if(list_sortType == SORT_TYPE_OFFICE_CODE){
				result = I_Util.compareToString(officeCode, compareToRow.officeCode);
			// 住所
			} else if(list_sortType == SORT_TYPE_ADDRESS){
				result = I_Util.compareToString(address, compareToRow.address);
			// 募集人名（カナ）
			} else if(list_sortType == SORT_TYPE_AGENT_NAME){
				result = I_Util.compareToString(agentNameKana, compareToRow.agentNameKana);
			// 募集人コード
			} else if(list_sortType == SORT_TYPE_AGENT_CODE){
				result = I_Util.compareToString(agentCode, compareToRow.agentCode);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return results == null ? 0 : results.size();
	}

	// 表示件数
	public Integer rowCount {get; set;}
	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}
}