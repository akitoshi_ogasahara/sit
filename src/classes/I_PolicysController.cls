public with sharing class I_PolicysController extends I_AbstractPolicyController {
	private static final String ECOV_ZCRIND_C = 'C';		//個人保険特約レコードの主契約を示すフラグ　	Ｃ：主契約	Ｒ：特約

	//保険契約ヘッダのリスト
	public List<PolicyListRow> policyList {get; set;}

	//総行数
	public String isTotalRows{get; set;}
	//取得リスト行数
	public Integer listSize {get; set;}

	//契約応当日期間の判別
	public Integer getFrom{get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;


	public I_PolicysController() {
		super();
		pageMessages = getPageMessages();

		//表示行数初期化
		listSize = 0;
		isTotalRows = '0';
		//デフォルト行数20行
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		//デフォルト表示フラグfalse
		//募集人
		contactFrag = false;
		//事務所
		officeFlag = false;
		//担当MR
		manegeMRFlag = false;

		// デフォルトソート - 契約応答月 : 昇順
		sortType  = 'comOccDateYmd';
		sortIsAsc = true;

		String checkFrom = ApexPages.CurrentPage().getParameters().get('from');
		if(!E_Util.isEnNum(checkFrom)){
			pageMessages.addErrorMessage(getMSG().get('POL|001'));
			return;
		}
		getFrom = Integer.valueOf(checkFrom);

		if(getFrom == 1){
			pgTitle = I_Const.LABEL_POLICY_TRHEEMONTH;
		}else if(getFrom == 4){
			pgTitle = I_Const.LABEL_POLICY_SIXMONTH;
		}else{
			pgTitle = '契約応当日が' + getFrom + '～' + (getFrom+2) + 'ヶ月後';
		}

	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = this.menu.Name;
		if(pr==null){

			//社員（募集人・事務所・担当MR）
			if(access.isEmployee()){
				contactFrag = true;
				officeFlag = true;
				manegeMRFlag = true;
			//AH（募集人・事務所）
			}else if(access.isAuthAH()){
				contactFrag = true;
				officeFlag = true;
			//AY（募集人）
			}else if(access.isAuthAY()){
				contactFrag = true;
			}

			if (String.isBlank(searchType)) {
				searchType = getSearchTypeOptions().get(0).getValue();
			}

			makePolicyList();

			//初回は契約応答月でソート
			I_PolicysController.list_sortType  = sortType;
			I_PolicysController.list_sortIsAsc = sortIsAsc;
			policyList.sort();
		}

		return pr;
	}

	public void makePolicyList() {
		//保険契約ヘッダ画面表示用リスト
		policyList = new List<PolicyListRow>();

		//保険契約ヘッダ値取得用リスト
		String termMonths = getResponseMonths(getFrom);

		String condition = createPolicyCondition();
		if (hasViewedByManager) {
			condition += createPolicyConditionByManager();
		}
		List<E_Policy__c> policyRecs = E_PolicyDao.getRecsIRISPolicys(termMonths, condition);
		//0件のときエラー
		if (policyRecs.isEmpty()) {
			pageMessages.addWarningMessage(getMSG().get('LIS|002'));
			return;
		}
		//最大件数以上の場合は警告を表示する--スプリントバックログ29のためコメントアウト(2017/1/18)
		/*if(policyRecs.size() >= I_Const.LIST_MAX_ROWS_AGGREGATE){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS_AGGREGATE).format()});
			pageMessages.addWarningMessage(errmessage);
		}*/

		// アクティブメニューキー（URLに付与することでアクティブメニューを保持）
		String irisMenu = '&' + getIRISMenuParameter();
		//団体用マップ生成
		Map<String, PolicyListRow> grupMap = new Map<String, PolicyListRow>();

		// リスト作成
		for (E_Policy__c pol : policyRecs) {

			//団体レコードで、主契約フラグCの個人保険特約を持たないものはリストに加えない
			if(pol.COLI_ZGRUPDCF__c == true && pol.E_COVPFs__r.isEmpty()){
				continue;
			}

			PolicyListRow plr = new PolicyListRow();
			//マップで使用するkey
			String grupkey = '';

			//keyを作成するのに用いるため保険種類を取得
			if (pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI) {
				//個人のレコードの時個人保険特約がない場合エラーになるのでif
				//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類格納フィールドと設定項目を変更
				if(!pol.E_COVPFs__r.isEmpty()) plr.policyType = pol.E_COVPFs__r[0].COLI_ZCOVRNAMES__c;
			} else {
				plr.policyType = pol.COLI_ZCOVRNAMES__c;
			}

			//団体trueのとき、key = 団体番号_保険種類_契約応当日_契約者顧客番号
			if (pol.COLI_ZGRUPDCF__c){
				 grupKey = pol.COLI_GRUPNUM__c + '_' + plr.policyType + '_' + pol.PolicyResponseDate__c + '_' + pol.ContractorCLTPF_CLNTNUM__c;
			//団体falseのとき、key = 証券番号
			} else {
				 grupKey = pol.COMM_CHDRNUM__c;
			}

			//上記で設定したgrupKeyでとってこれるmapがある場合--証券番号は一意であるため、団体しか入らない
			if (grupMap.containsKey(grupkey)) {
				plr = grupMap.get(grupKey);

				//保険料
				//個人
				if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI && pol.COMM_SINSTAMT__c != null){
					plr.policy.COMM_SINSTAMT__c += pol.COMM_SINSTAMT__c;
				//SPVA
				}else if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA && pol.COMM_ZTOTPREM__c != null){
					plr.policy.COMM_SINSTAMT__c += pol.COMM_ZTOTPREM__c;
				}

				//解約返戻金
				Decimal coliCanRef = getCancellationRefund(pol);
				if(plr.policy.COLI_ZCSHVAL__c < 0){
					//cancelRefが'?'のときはほかのレコードの値がなんであれ'?'になるためなにもしない。
				}else if(coliCanRef < 0){
					//解約返戻金計算不能ﾌﾗｸまたは未経過保険料計算不能ﾌﾗｸﾞがtrueのとき、-1が返ってくる
					//ソート用に-1を入れる
					plr.policy.COLI_ZCSHVAL__c = -1;
				}else{
					//個別保険
					if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI){
						plr.policy.COLI_ZCSHVAL__c += coliCanRef;
					//SPVAであり解約返戻金がnullでないとき
					}else if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA && pol.SPVA_ZREFAMT__c != null){
						plr.policy.COLI_ZCSHVAL__c += pol.SPVA_ZREFAMT__c;
					}
				}

			//ない場合--団体のレコードであっても、一度はこちらを通る
			} else {
				//契約応答日
				plr.comOccDateYmd  = pol.PolicyResponseDate__c;
				//契約者名
				plr.policy.COMM_ZCLNAME__c = pol.ContractorName__c;
				//契約者カナ名
				plr.contractorSort = pol.ContractorE_CLTPF_ZCLKNAME__c;		//スプリントバックログ9(2017/1/18) カナソート用追記
				//顧客番号
				plr.policy.COMM_CLNTNUM__c = pol.ContractorCLTPF_CLNTNUM__c;
				//団体フラグ
				plr.policy.COLI_ZGRUPDCF__c = pol.COLI_ZGRUPDCF__c;

				plr.elapsedYears = pol.PolicyElapsedYears__c;

				//個人保険
				if (pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI) {
					if (pol.COMM_SINSTAMT__c != null) plr.policy.COMM_SINSTAMT__c = pol.COMM_SINSTAMT__c;	//保険料:1回あたりの保険料
					plr.policy.COLI_ZCSHVAL__c = getCancellationRefund(pol);								//解約返戻金
				//SPVA
				} else {
					if (pol.COMM_ZTOTPREM__c != null) plr.policy.COMM_SINSTAMT__c = pol.COMM_ZTOTPREM__c;	//保険料:払込保険料
					if (pol.SPVA_ZREFAMT__c != null) plr.policy.COLI_ZCSHVAL__c = pol.SPVA_ZREFAMT__c;	//解約返戻金：解約返戻金
				}

				//団体表示フラグチェック
				if (pol.COLI_ZGRUPDCF__c) {
					//団体番号
					plr.policy.COMM_CHDRNUM__c = pol.COLI_GRUPNUM__c;
					plr.pageURL = 'IRIS_GroupPolicy?group=' + plr.policy.COMM_CHDRNUM__c + '&from=' + getFrom + irisMenu;
					//団体の場合、契約経過年数に空を設定
					plr.elapsedYears = '';

				} else {
					//証券番号
					plr.policy.COMM_CHDRNUM__c = pol.COMM_CHDRNUM__c;
					//遷移先URL
					plr.pageURL = pol.PolicyDetailLink__c + irisMenu;
					plr.pageURLTarget = pol.PolicyDetailLinkTarget__c;
					System.debug(plr.pageURL);
				}
			}
			//スプリントバックログ9(2017/1/18) カナソート用修正
			//事務所
			plr.setValueOffice(pol.MainAgentAccountName__c, pol.SubAgentAccountName__c,pol.MainAgentAccountNameKana__c);
			//plr.setValueOffice(pol.MainAgentAccountName__c, pol.SubAgentAccountName__c);
			//募集人
			plr.setValueAgent(pol.MainAgentName__c, pol.SubAgentName__c, pol.MainAgentNameKana__c);
			//plr.setValueAgent(pol.MainAgentName__c, pol.SubAgentName__c);

			//MR名
			plr.setValueMRName(pol.MainAgentAccountMRName__c,pol.SubAgentAccountMRName__c);

			grupMap.put(grupkey,plr);
			//1001件以上をvisualforceに渡すとエラーになるためbreak
			if (grupMap.size() == I_Const.LIST_MAX_ROWS) {
				break;
			}
		}

		if (grupMap != null && !grupMap.isEmpty()) {
			policyList = grupMap.values();
		}

		//最大件数以上の場合は警告を表示する--スプリントバックログ29(2017/1/18)
		//集約前件数が2000件の場合
		if (policyRecs.size() >= I_Const.LIST_MAX_ROWS_AGGREGATE) {
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS_AGGREGATE).format()});
			pageMessages.addWarningMessage(errmessage);
		//集約後リストサイズが1000件の場合
		} else if(policyList.size() >= I_Const.LIST_MAX_ROWS) {
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS).format()});
			pageMessages.addWarningMessage(errmessage);
		}

		//総行数の取得
		listSize = policyList.size();
		isTotalRows = policyList.size().format();

	}

	// 画面表示行ラッパークラス
	public class PolicyListRow implements Comparable{
		public E_Policy__c policy {get; set;}

		// 契約応答月
		public String getComOccDate(){
			return E_Util.formatMMdd(comOccDateYmd);
		}
		// 契約応答月 ソート用
		public String comOccDateYmd {get; set;}
		//画面用保険料
		public String getPolicyFee(){
			return getFormat(policy.COMM_SINSTAMT__c);
		}
		// 事務所
		public String office {get; set;}
		// 募集人
		public String agent {get; set;}
		// MR名
		public String mrName {get; set;}
		//URL
		public String pageURL{get; set;}
		//URL_target
		public String pageURLTarget{get; set;}
		//画面用解約返戻金
		public String getCancellRef(){
			return getFormat(policy.COLI_ZCSHVAL__c);
		}

		//スプリントバックログ9(2017/1/18) カナソート用追記
		// 契約者カナ名
		public String contractorSort {get; set;}
		//募集人カナ名
		public String agentSort{get; set;}
		//代理店事務所カナ名
		public String officeSort{get; set;}
		//契約経過年数
		public String elapsedYears{get;set;}

		//SITP(2017/12/1R) 保健種類(結合)表示対応 start
		//保険種類
		public String policyType {get; set;}
		//SITP(2017/12/1) 保健種類(結合)表示対応 end

		// コンストラクタ
		public PolicyListRow(){
			policy = new E_Policy__c();
			//nullに足し算をしないよう初期化
			//保険料
			policy.COMM_SINSTAMT__c = 0;
			//解約返戻金
			policy.COLI_ZCSHVAL__c = 0;

			comOccDateYmd = '';
			office	  = '';
			agent	   = '';
			mrName	  = '';
			pageURL	 = '';
			pageURLTarget = '';

			//スプリントバックログ9(2017/1/18) カナソート用追記
			contractorSort = '';
			agentSort	   = '';
			officeSort	   = '';

			elapsedYears = '';

			//SITP(2017/12/1R) 保健種類(結合)表示対応 start
			policyType = '';
			//SITP(2017/12/1R) 保健種類(結合)表示対応 end

		}

		// 事務所セット--スプリントバックログ9(2017/1/18)
		//public void setValueOffice(String main, String sub){
		public void setValueOffice(String main, String sub,String mainkana){
			// 現在値が「(複数)」の場合は何もしない
			if(office != I_Const.OL_MULTI_DATA){
				// （共同募集）主・従が空白でない かつ 主・従の値が異なる場合は「(複数)」をセット
				if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
					office = I_Const.OL_MULTI_DATA;
				// 主と office の値が異なる場合は「(複数)」をセット
				} else if(String.isNotBlank(office) && office != main){
					office = I_Const.OL_MULTI_DATA;
				// 上記以外で office が空白の場合は主をセット
				} else if(String.isBlank(office)) {
					office = main;
				}

				//スプリントバックログ9(2017/1/18) officeに（複数）がセットされているときは(複数)をセット。それ以外の場合はmainのカナ項目をセット
				if(office == I_Const.OL_MULTI_DATA){
					officeSort = I_Const.OL_MULTI_DATA;
				}else{
					officeSort = mainkana;
				}
			}
			return;
		}

		// 募集人セット--スプリントバックログ9(2017/1/18)
		//public void setValueAgent(String main, String sub){
		public void setValueAgent(String main, String sub,String mainkana){
			// 現在値が「(複数)」の場合は何もしない
			if(agent != I_Const.OL_MULTI_DATA){
				// 主、従の値が異なる場合は「(複数)」をセット
				if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
					agent = I_Const.OL_MULTI_DATA;
				// 主と agent の値が異なる場合は「(複数)」をセット
				} else if(String.isNotBlank(agent) && agent != main){
					agent = I_Const.OL_MULTI_DATA;
				// 上記以外で office が空白の場合は主をセット
				} else if(String.isBlank(agent)) {
					agent = main;
				}

				//スプリントバックログ9(2017/1/18) agentに（複数）がセットされているときは(複数)をセット。それ以外の場合はmainのカナ項目をセット
				if(agent == I_Const.OL_MULTI_DATA){
					agentSort = I_Const.OL_MULTI_DATA;
				}else{
					agentSort = mainkana;
				}
			}
			return;
		}

		// 担当MR名セット
		public void setValueMRName(String main, String sub){
			// 現在値が「(複数)」の場合は何もしない
			if(mrName != I_Const.OL_MULTI_DATA){
				// 主、従の値が異なる場合は「(複数)」をセット
				if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
					mrName = I_Const.OL_MULTI_DATA;
				// 主と mrName の値が異なる場合は「(複数)」をセット
				} else if(String.isNotBlank(mrName) && mrName != main){
					mrName = I_Const.OL_MULTI_DATA;
				// 上記以外で mrName が空白の場合は主をセット
				} else if(String.isBlank(mrName)) {
					mrName = main;
				}
			}
			return;
		}

		public String getFormat(Decimal value){
			if(value == null){
				return '0';
			} else if(value < 0){
				return '?';
			}
			return value.format();
		}

		/**
		 * ソート
		 * Mapに設定されているものは変数名
		 * 設定されていないものはAPI名
		 */
		public Integer compareTo(Object compareTo){
			PolicyListRow compareToRow = (PolicyListRow)compareTo;
			Integer result = 0;
			//保険料
			if(list_sortType == 'policyFee'){
				result = I_Util.compareToDecimal(policy.COMM_SINSTAMT__c, compareToRow.policy.COMM_SINSTAMT__c);
			// MR名
			}else if(list_sortType == 'mrName'){
				result = I_Util.compareToString(mrName, compareToRow.mrName);
			//契約応答月
			}else if(list_sortType == 'comOccDateYmd'){
				result = I_Util.compareToString(comOccDateYmd, compareToRow.comOccDateYmd);
			//契約者名
			}else if(list_sortType == 'contractor'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(contractorSort, compareToRow.contractorSort);
				//result = I_Util.compareToString(contractor, compareToRow.contractor);
			//保険種類
			}else if(list_sortType == 'policyType'){
				//SITP(2017/12/1R) 保健種類(結合)表示対応　比較フィールドを変更
				result = I_Util.compareToString(policyType, compareToRow.policyType);
			//団体番号,証券番号
			}else if(list_sortType == 'policyNum'){
				result = I_Util.compareToString(policy.COMM_CHDRNUM__c, compareToRow.policy.COMM_CHDRNUM__c);
			//解約返戻金
			}else if(list_sortType == 'cancellRef'){
				result = I_Util.compareToDecimal(policy.COLI_ZCSHVAL__c, compareToRow.policy.COLI_ZCSHVAL__c);
			//事務所
			}else if(list_sortType == 'office'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(officeSort, compareToRow.officeSort);
				//result = I_Util.compareToString(office, compareToRow.office);
			//募集人
			}else if(list_sortType == 'agent'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(agentSort, compareToRow.agentSort);
				//result = I_Util.compareToString(agent, compareToRow.agent);
			//団体フラグ
			}else if(list_sortType == 'grupFlag'){
				result = I_Util.compareToString(String.valueOf(policy.COLI_ZGRUPDCF__c), String.valueOf(compareToRow.policy.COLI_ZGRUPDCF__c));
			//顧客番号
			}else if(list_sortType == 'contractorNo'){
				result = I_Util.compareToString(policy.COMM_CLNTNUM__c, compareToRow.policy.COMM_CLNTNUM__c);
			//契約経過年数
			}else if(list_sortType == 'elapsedYears'){
				String elapsedyear = elapsedYears.substringBefore('年');
				String elapsedmonth = elapsedYears.substringBefore('ヶ月');
				elapsedmonth = elapsedmonth.substringAfter('年');
				String cmpElapsedyear = compareToRow.elapsedYears.substringBefore('年');
				String cmpElapsedmonth = compareToRow.elapsedYears.substringBefore('ヶ月');
				cmpElapsedmonth = cmpElapsedmonth.substringAfter('年');
				if(elapsedyear.length() == 1){
					elapsedyear = '0' + elapsedyear;
				}
				if(elapsedmonth.length() == 1){
					elapsedmonth = '0' + elapsedmonth;
				}
				if(cmpElapsedyear.length() == 1){
					cmpElapsedyear = '0' + cmpElapsedyear;
				}
				if(cmpElapsedmonth.length() == 1){
					cmpElapsedmonth = '0' + cmpElapsedmonth;
				}
				if(String.isBlank(elapsedYears)){
					elapsedyear = '0';
					elapsedmonth = '0';
				}
				if(String.isBlank(compareToRow.elapsedYears)){
					cmpElapsedyear = '0';
					cmpElapsedmonth = '0';
				}
				result = I_Util.compareToDecimal(Decimal.valueOf(elapsedyear+elapsedmonth), Decimal.valueOf(cmpElapsedyear+cmpElapsedmonth));
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		//現在の表示行数+20
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		//現在の表示行数が1000より大きくなった場合、表示行数は1000
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * ソート実行
	 */
	public PageReference sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		//押されたのが同じ項目だった場合逆に、違う項目だった場合昇順に
		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}

		I_PolicysController.list_sortType  = sortType;
		I_PolicysController.list_sortIsAsc = sortIsAsc;
		policyList.sort();
		System.debug('sort='+policyList);
		return null;
	}

	//メニューキー
	protected override String getLinkMenuKey(){
		return ApexPages.CurrentPage().getParameters().get('from')=='4'? 'PolicyList4To6' :'PolicyList1To3';
	}

	/*	応答月による抽出によるレコード件数
	 *		件数取得用クエリ	I_PolicysControllerのメソッドを利用する
	 *
	 */
	public String getRECORD_SIZE_SOQL_FOR_RESTAPI(){
		return I_PolicysController.getRECORD_SIZE_SOQL_FOR_RESTAPI(getFrom);		//URLから取得したNか月後
	}

	/*
	 *	TOPとIRIS_Policysで共通利用関数
	 */
	//今日＋指定月先3か月の月を連結して取得 	今月からの場合は0を指定してください　1-3か月の場合は『1』を指定
	private static String getResponseMonths(Integer nextFrom){
		//nextFrom から3か月分の月を取得
		Date sysDate = E_Util.SYSTEM_TODAY();
		Integer m1 = sysDate.toStartOfMonth().addMonths(nextFrom).month();
		Integer m2 = sysDate.toStartOfMonth().addMonths(nextFrom+1).month();
		Integer m3 = sysDate.toStartOfMonth().addMonths(nextFrom+2).month();
		return String.valueOf(m1) + ',' + String.valueOf(m2) + ',' + String.valueOf(m3);
	}

	/*  TOP画面での応答日に関するレコード件数取得
	 *	  件数取得用クエリ
	 *
	 */
	private static Map<String, Id> POLICY_RECORD_TYPES = E_RecordTypeDao.getRecordTypeBySobjectType('E_Policy__c');
	//取得用クエリ	（団体とそれ以外で２回投げた総計を件数とする　団体の場合のGroupBy条件をIRIS_Policysと合わせること）
	private static final Map<String, String> RECORD_SIZE_SOQLs_FORMAT = new Map<String, String>{
		 // 団体フラグ＝Falseの場合のレコード件数
		 'POLICY_Indivi' =>
		 'SELECT COUNT(Id) From E_Policy__c '
		 + 'WHERE CALENDAR_month(PolicyResponseDate2__c) in ({0}) '+
		 + 'AND COLI_ZGRUPDCF__c=false '
		 + 'AND recordtypeId in ({1},{2}) '
		 + 'AND COMM_STATCODE__c!={5} '
		 + 'AND COMM_ZRSTDESC__c!={6}'
		 + '{3}'

/*		 ,// 団体フラグ＝True SPVA の場合のレコード件数
		'SELECT PolicyResponseDate2__c, COLI_GRUPNUM__c,COLI_ZCOVRNAMES__c, Contractor__r.E_CLTPF_CLNTNUM__c, '
		 + 'COUNT(Id) '
		 + 'from E_Policy__c '
		 + 'WHERE CALENDAR_month(PolicyResponseDate2__c) in ({0})'
		 + 'AND COLI_ZGRUPDCF__c = true '
		 + 'AND recordtypeId in ({2}) '					 //SPVA
		 + 'group by PolicyResponseDate2__c, COLI_GRUPNUM__c,COLI_ZCOVRNAMES__c, Contractor__r.E_CLTPF_CLNTNUM__c'
*/
		 ,// 団体フラグ＝True COLIの場合のレコード件数
		 'POLICY_Group' =>
		 //SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類⇒保険種類(結合)変更
//		'SELECT E_Policy__r.PolicyResponseDate2__c, E_Policy__r.COLI_GRUPNUM__c,COLI_ZCOVRNAMES__c, E_Policy__r.Contractor__c,'		//ContractorCLTPF_CLNTNUM__cはGroupByで使えない	　E_Policy__r.Contractor__r.E_CLTPF_CLNTNUM__cは参照権限なし
		'SELECT E_Policy__r.PolicyResponseDate2__c, E_Policy__r.COLI_GRUPNUM__c,COMM_ZCOVRNAM__c, E_Policy__r.Contractor__c,'		//ContractorCLTPF_CLNTNUM__cはGroupByで使えない	　E_Policy__r.Contractor__r.E_CLTPF_CLNTNUM__cは参照権限なし
		 + 'COUNT(E_Policy__c) '
		 + 'from E_COVPF__c '
		 + 'WHERE CALENDAR_month(E_Policy__r.PolicyResponseDate2__c) in ({0}) '
		 + 'AND E_Policy__r.COLI_ZGRUPDCF__c=true '
		 + 'AND E_Policy__r.recordtypeId in ({1}) '					 //COLI
		 + 'AND COLI_ZCRIND__c= ' + '\'\'' + ECOV_ZCRIND_C + '\'\' '			//Formatした結果シングルクォートが一つなくなるので二つつけておく
		 + 'AND E_Policy__r.COMM_STATCODE__c!={5} '
		 + 'AND E_Policy__r.COMM_ZRSTDESC__c!={6}'
		 + '{4}'
		 //SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類⇒保険種類(結合)変更
//		 + 'group by E_Policy__r.PolicyResponseDate2__c, E_Policy__r.COLI_GRUPNUM__c,COLI_ZCOVRNAMES__c, E_Policy__r.Contractor__c '
		 + 'group by E_Policy__r.PolicyResponseDate2__c, E_Policy__r.COLI_GRUPNUM__c,COMM_ZCOVRNAM__c, E_Policy__r.Contractor__c '
		 + 'LIMIT 1000'

	};

	//応答日抽出クエリの取得（取得開始月を受け取る
	//  @param nextFrom	 Nか月後を指定）
	public static Map<String,String> getRECORD_SIZE_SOQL(Integer nextFrom){
		//nextFrom から3か月分の月を取得
		String termMonth = getResponseMonths(nextFrom);

		//レコードタイプId条件の設定
		String termRecordTypeId_COLI = '\''+String.valueOf(POLICY_RECORD_TYPES.get(E_Const.POLICY_RECORDTYPE_COLI))+'\'';
		String termRecordTypeId_SPVA = '\''+String.valueOf(POLICY_RECORD_TYPES.get(E_Const.POLICY_RECORDTYPE_SPVA))+'\'';

		//代理店ユーザの場合、自分の担当契約のみを抽出
		String termMyPolicys = '';
		String termMyPolicys_ECOV = '';
		// MR または 拠点長 または 代理店ユーザの場合 ⇒代理店ユーザまたは 社員(BR**)以外の時

		//UX2残案件_8
		E_AccessController access = E_AccessController.getInstance();
		if(access.isEmployee() && access.ZINQUIRR_is_BRAll()==false){
			termMyPolicys = ' AND ';
			termMyPolicys += 'PolicyInCharge__c=true ';

			termMyPolicys_ECOV = ' AND ';
			termMyPolicys_ECOV += 'E_Policy__r.PolicyInCharge__c=true ';
		}

		Map<String,String> soqls = new Map<String,String>();
		for(String s:RECORD_SIZE_SOQLs_FORMAT.keyset()){
			soqls.put(s,String.format(RECORD_SIZE_SOQLs_FORMAT.get(s), new List<String>{termMonth
														, termRecordTypeId_COLI
														, termRecordTypeId_SPVA
														, termMyPolicys
														, termMyPolicys_ECOV
														, '\''+I_Const.LAPS_POLICY_STATCODE+'\''
														, '\''+EncodingUtil.urlEncode(I_Const.LAPS_POLICY_ZRSTDESC, 'UTF-8')+'\''
														}));
		}
		return soqls;
	}


	/**
	 *  REST API用のクエリ文字列を返す
	 *	  空白を”+”に変換して、JSON形式で返す。
	 */
	public static String getRECORD_SIZE_SOQL_FOR_RESTAPI(Integer nextFrom){
		List<String> soqls = getRECORD_SIZE_SOQL(nextFrom).values();
		List<String> formattedSoqls = new List<String>();
		for(String s:soqls){
			formattedSoqls.add(s.replace(' ','+'));
		}
		return JSON.serialize(formattedSoqls);
	}

	/*
			検索クエリ取得用クラス

	*/
	public virtual class recordCounterM1 extends E_RecordCountController.recordCountExecuter{
		public virtual Integer getMonthFrom(){
			return 1;
		}

		//レコード件数問合せクエリ取得用Indicator　複数のクエリを投げる場合にそれぞれのクエリの識別子リストを返す
		public override List<String> getQueryIndicators(){

			return new List<String>(I_PolicysController.RECORD_SIZE_SOQLs_FORMAT.keyset());
		}

		//レコード件数問合せ実施　レコード件数を返す（但し文字列として） パラメータでgetQueryIndicators()の文字列を受け取る
		public override String getRecordSize(String key){

			try{
				String soql = I_PolicysController.getRECORD_SIZE_SOQL(getMonthFrom()).get(key);
				List<AggregateResult> agResult = Database.query(soql);
				if(key == 'POLICY_Group'){
					return String.valueOf(agResult.size());
				}
				return String.valueOf(agResult[0].get('expr0'));
			}catch(exception e){
				return e.getMessage();
			}

		}
	}

	public virtual class recordCounterM4 extends I_PolicysController.recordCounterM1{
		public override Integer getMonthFrom(){
			return 4;
		}
	}
}