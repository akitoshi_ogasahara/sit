@isTest
private class TestSC_ExportSearchController {
	// コンストラクタ
	private static testMethod void ExportSearchTest001(){
		Test.startTest();
			Test.setCurrentPage(Page.E_SCExportSearch);
			SC_ExportSearchController controller = new SC_ExportSearchController();
		Test.stopTest();

		System.assertEquals(controller.today, System.now().format('データ作成基準日:yyyy年MM月dd日'));
	}

	// mainSObjectAPIName
	private static testMethod void ExportSearchTest002(){
		String mainSObjectAPIName;

		Test.startTest();
			Test.setCurrentPage(Page.E_SCExportSearch);
			SC_ExportSearchController controller = new SC_ExportSearchController();

			mainSObjectAPIName = controller.mainSObjectAPIName();
		Test.stopTest();

		System.assertEquals(mainSObjectAPIName, Schema.SObjectType.SC_Office__c.Name);
	}
}