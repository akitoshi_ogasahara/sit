/**
 * Web約款_検索画面のコントローラー
 * 
 */
public with sharing class WY_WebYakkanSearchController {
	private static final String GUIDEAGREEDIV_NEW = '新契約用';
	private static final String GUIDEAGREEDIV_UPDATE = '更新・特約中途付加用';
	
	//　Web約款検索画面Top
	public Boolean isTop {get; set;}

	// 検索コードから｢ご契約のしおり･約款｣を検索する
	public Boolean isCodeResult {get; set;}
	public Boolean hasCodeSearchError {get; set;}
	// 検索コード(CRコード+契約日(YYYYMMDD)+代理店コード)	
	public String searchCode {get; set;}
	// 検索結果
	public WY_SearchResult searchResult{get; set;}

	// ご契約日から｢ご契約のしおり･約款｣を検索する
	public String searchContractDate {get; set;}
	public Boolean isContractResult {get; set;}
	public Boolean hasContractSearchError {get; set;}
	
	// 最新の｢ご契約のしおり･約款｣を検索する
	public Boolean isLatestResult {get; set;}
	
	//　検索結果
	// 【新契約用】
	public List<WY_AgencyCodeResult> agencyCodeResultList {get; set;}
	// 【更新･特約中途付加用】
	public List<WY_SearchResult> resultList {get; set;}

	private Date tday;

	/**
	 * メッセージ、ディスクレーマー表示
	 */
	public E_PageMessagesHolder pageMessages {get; set;}

	public E_PageMessagesHolder getPageMessages(){
		return E_PageMessagesHolder.getInstance();
	}

	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}

	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}

	/**
	 * コンストラクタ
	 */
	public WY_WebYakkanSearchController() {
		this.isTop = true;
		this.isCodeResult = false;
		this.isContractResult = false;
		this.isLatestResult = false;
		this.hasCodeSearchError = false;
		this.hasContractSearchError = false;
		this.searchCode = '';
		pageMessages = getPageMessages();

		tday = E_Util.SYSTEM_TODAY();
	}

	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 */
	public PageReference doSearchCode() {
		pageMessages.clearMessages();
		this.hasCodeSearchError = false;
		this.hasContractSearchError = false;
		//　検索コード必須入力チェック、検索コードの入力桁数チェック(10桁または15桁)、契約日の有効性チェック（yyyyMMdd）
		if(String.isBlank(searchCode) || (searchCode.length() != 10 && searchCode.length() != 15)){
			pageMessages.addErrorMessage(getMSG().get('WY|001'));
			hasCodeSearchError = true;
			return null;
		}else if(!E_Util.isEnAlphaNum(searchCode)){
			pageMessages.addErrorMessage(getMSG().get('WY|013'));
			hasCodeSearchError = true;
			return null;
		}else if(!E_Util.isYyyyMMddNonSrash(searchCode.mid(2, 8))){
			pageMessages.addErrorMessage(getMSG().get('WY|001'));
			hasCodeSearchError = true;
			return null;
		}

		// 検索コード（CRコード、契約日、代理店コード）により、IRIS CMSコンテンツを取得する.
		String crCode = getInsTypeValues(searchCode.left(2).toUpperCase());
		Date contractDate = Date.valueOf(searchCode.mid(2, 4) + '-' + searchCode.mid(6, 2) + '-' + searchCode.mid(8, 2));

		String condition = '';
		condition += ' AND InsType__c INCLUDES (\'' + crCode + '\')';
		condition += ' AND DisplayFrom__c != null AND DisplayFrom__c <= ' + String.valueOf(tday) + ' AND ( ValidTo__c = null OR ValidTo__c >= ' + String.valueOf(tday) + ')';
		condition += ' AND ContractDateFrom__c <= ' + String.valueOf(contractDate) + ' AND (ContractDateTo__c = null OR ContractDateTo__c >= ' + String.valueOf(contractDate) + ')';
		
		if(searchCode.length() == 10){
			condition += ' AND AgencyCode__c INCLUDES (\'' + '汎用' + '\')';
		}else if(searchCode.length() == 15){
			String agencyCode = getAgencyCodeValues(searchCode.right(5));
			condition += ' AND AgencyCode__c INCLUDES (\'' + agencyCode + '\')'; 
		}
		List<I_ContentMaster__c> contents = I_ContentMasterDao.getYakkanContents(condition, tday);

		if(contents.size() == 1){
			Set<Id> fileParentIdSet = new Set<Id>();
			for (I_ContentMaster__c child : contents[0].ChildContents__r) {
				fileParentIdSet.add(child.Id);
			}
			List<I_ContentMaster__c> children = I_ContentMasterDao.getContentAndAttachment(fileParentIdSet);
			searchResult = new WY_SearchResult(contents[0], children);
			this.isTop = false;
			this.isCodeResult = true;
			this.isContractResult = false;
			this.isLatestResult = false;
		}else{
			pageMessages.addErrorMessage(getMSG().get('WY|002'));
			hasCodeSearchError = true;
			return null;
		}
		return null;

	}

	/**
	 * ご契約日/更新日･特約中途付加日から｢ご契約のしおり･約款｣を検索
	 */
	public PageReference doSearchContract(){
		pageMessages.clearMessages();
		this.hasCodeSearchError = false;
		this.hasContractSearchError = false;

		if(String.isBlank(searchContractDate) || !E_Util.isExistsYyyyMMdd(searchContractDate)){
			pageMessages.addErrorMessage(getMSG().get('WY|011'));
			hasContractSearchError = true;
			return null;
		}
		
		Date searchConDate = Date.valueOf(searchContractDate.replaceAll('/', '-'));
		String labelstr = Label.WY_SearchStartValidDate;
		Date searchStartValidDate = Date.valueOf(labelstr.left(4) + '-' + labelstr.mid(4, 2) + '-' + labelstr.right(2));
		if(searchConDate < searchStartValidDate){
			pageMessages.addErrorMessage(getMSG().get('WY|010'));
			hasContractSearchError = true;
			return null;
		}

		String condition = '';
		// 公開日が今日以前
		condition += ' AND DisplayFrom__c != null AND DisplayFrom__c <= ' + String.valueOf(tday);
		// 公開終了日は今日以降、未来日付が入力された場合には入力値以降
		if(tday < searchConDate){
			condition += ' AND ( ValidTo__c = null OR ValidTo__c >= ' + String.valueOf(searchConDate) + ')';
		}else{
			condition += ' AND ( ValidTo__c = null OR ValidTo__c >= ' + String.valueOf(tday) + ')';
		}
		condition += ' AND ContractDateFrom__c <= ' + String.valueOf(searchConDate);
		condition += ' AND (ContractDateTo__c = null OR ContractDateTo__c >= ' + String.valueOf(searchConDate) + ')';

		List<I_ContentMaster__c> contents = I_ContentMasterDao.getYakkanContents(condition, tday);
		createCategorizeList(contents);

		this.isTop = false;
		this.isCodeResult = false;
		this.isContractResult = true;
		this.isLatestResult = false;
		return null;
	}
	
	/**
	 * 検索(最新の｢ご契約のしおり･約款｣を検索する)
	 */
	 public PageReference doSearchNew(){
	 	pageMessages.clearMessages();
	 	this.hasCodeSearchError = false;
		this.hasContractSearchError = false;

		// IRIS　CMSコンテンツ（親＋子）を取得する
		String condition = '';
		condition += ' AND DisplayFrom__c != null AND DisplayFrom__c <= ' + String.valueOf(tday);
		condition += ' AND ( ValidTo__c = null OR ValidTo__c >= ' + String.valueOf(tday) + ')';
		condition += ' AND ContractDateFrom__c <= ' + String.valueOf(tday);
		condition += ' AND (ContractDateTo__c = null OR ContractDateTo__c >= ' + String.valueOf(tday) + ')';

		List<I_ContentMaster__c> contents = I_ContentMasterDao.getYakkanContents(condition, tday);

		createCategorizeList(contents);

		this.isTop = false;
		this.isCodeResult = false;
		this.isContractResult = false;
		this.isLatestResult = true;
		return null;
	}

	private void createCategorizeList(List<I_ContentMaster__c> contents){
		// 【新契約用】 Map<代理店コード, 親コンテンツリスト>
		Map<String, List<I_ContentMaster__c>> agencyCodeMap = new Map<String, List<I_ContentMaster__c>>();
		// 【更新･特約中途付加用】 Map<親Id,親コンテンツ>
		Map<Id, I_ContentMaster__c> parentMap = new Map<Id, I_ContentMaster__c>();

		//　Attachment取得用
		Set<Id> fileParentIdSet = new Set<Id>();

		for(I_ContentMaster__c content : contents){
			if(content.GuideAgreeDiv__c.equals(GUIDEAGREEDIV_NEW)){
				List<String> codeList = content.AgencyCode__c.split(';');
				for(String agencyCode : codeList){
					if(agencyCodeMap.containsKey(agencyCode)){
						agencyCodeMap.get(agencyCode).add(content);
					}else{
						agencyCodeMap.put(agencyCode, new List<I_ContentMaster__c>{content});
					}
				}
			}else if(content.GuideAgreeDiv__c.equals(GUIDEAGREEDIV_UPDATE)){
				parentMap.put(content.Id, content);
			}
			
			for(I_ContentMaster__c child : content.ChildContents__r){
				fileParentIdSet.add(child.Id);
			}
		}

		List<I_ContentMaster__c> children = I_ContentMasterDao.getContentAndAttachment(fileParentIdSet);

		// Map<親Id,子(+Attachment)List>
		Map<Id, List<I_ContentMaster__c>> contentsMap = new Map<Id, List<I_ContentMaster__c>>();

		for(I_ContentMaster__c child : children){
			if(contentsMap.containsKey(child.ParentContent__c)){
				contentsMap.get(child.ParentContent__c).add(child);
			}else{
				contentsMap.put(child.ParentContent__c, new List<I_ContentMaster__c>{child});
			}
		}
		// 【新契約用】 
		agencyCodeResultList = new List<WY_AgencyCodeResult>();
		List<String> agencyCodeList = new List<String>();
		// 代理店コードの選択
        List<Schema.PicklistEntry> picklist = Util.getPicklist(new I_ContentMaster__c(), I_ContentMaster__c.AgencyCode__c.getDescribe().getName());
        
        // 代理店コードを含める代理店コードの値（メタデータ）を取得する
        for(Schema.PicklistEntry item : picklist){
            if(item.isActive()){
                agencyCodeList.add(item.getValue());
            }
        }

		for(String agencyCode : agencyCodeList){
			List<WY_SearchResult> agencyResults = new List<WY_SearchResult>();
			if(agencyCodeMap.containsKey(agencyCode)){
				for(I_ContentMaster__c parentContent : agencyCodeMap.get(agencyCode)){
					if(contentsMap.containsKey(parentContent.Id)){
						List<I_ContentMaster__c> childList = contentsMap.get(parentContent.Id);
						agencyResults.add(new WY_SearchResult(parentContent, childList));
					}else{
						agencyResults.add(new WY_SearchResult(parentContent, new List<I_ContentMaster__c>()));
					}
				}
				agencyCodeResultList.add(new WY_AgencyCodeResult(agencyCode, agencyResults));
			}
		}

		// 【更新･特約中途付加用】
		resultList = new List<WY_SearchResult>();
		for(Id parentId : parentMap.keySet()){
			if(contentsMap.containsKey(parentId)){
				resultList.add(new WY_SearchResult(parentMap.get(parentId), contentsMap.get(parentId)));
			}else{
				resultList.add(new WY_SearchResult(parentMap.get(parentId), new List<I_ContentMaster__c>()));
			}
		}
	}

	/**
	 * 保険種類のAPI名取得
     */
    private String getInsTypeValues(String crCode) {
        // 検索結果リスト
        List<String> results = new List<String>();
        
        // 保険種類の選択
        List<Schema.PicklistEntry> picklist = Util.getPicklist(new I_ContentMaster__c(), I_ContentMaster__c.InsType__c.getDescribe().getName());
        
        // CRコードを含める保険種類の値（メタデータ）を取得する
        for (Schema.PicklistEntry item : picklist) {
            if (item.isActive() && item.getValue().contains(crCode)) {
                results.add(item.getValue());
            }
        }
        
        // 結果を返却する
        return string.join(results,';');
    }

    /**
	 * 代理店コードのAPI名取得
     */
    private String getAgencyCodeValues(String agencyCode) {
        // 検索結果リスト
        List<String> results = new List<String>();
        
        // 代理店コードの選択
        List<Schema.PicklistEntry> picklist = Util.getPicklist(new I_ContentMaster__c(), I_ContentMaster__c.AgencyCode__c.getDescribe().getName());
        
        // 代理店コードを含める代理店コードの値（メタデータ）を取得する
        for (Schema.PicklistEntry item : picklist) {
            if (item.isActive() && item.getValue().contains(agencyCode)) {
                results.add(item.getValue());
            }
        }
        
        // 結果を返却する
        return string.join(results,';');
    }

    /**
	 *	ログレコード保存処理
	 */
	public String getPreparateLogJSON(){
		return JSON.serialize(E_LogUtil.createLog());
	}

	@RemoteAction
	public static String saveLog(String logJSON) {
		E_Log__c log;
		try{
			log = (E_Log__c)JSON.deserialize(logJSON, SObject.class);
			if(log.AccessDatetime__c == null){
				log.AccessDatetime__c = Datetime.now();
			}
			insert log;
		}catch(Exception e){
			return e.getMessage() + '@@@' + e.getStackTraceString();
		}

		return String.ValueOf(log.id);
	}
}