public with sharing class E_CMSController{
    
    public E_MessageMaster__c cmsMsg{get;set;}
    
    /*
     *      Constructor
     */
    public E_CMSController(){
    
    }
    
    /**
     *      サブメニューリストの作成
     */
    private List<E_MenuMaster__c> subPgs; 
    public List<E_MenuMaster__c> getSubPages(){
        if(subPgs==null && cmsMsg.ShowSubPageLink__c){
            subPgs = E_MessageMasterDao.getSubPagesByKey(cmsMsg.Menu__r.MenuMasterKey__c);
        }
        return subPgs;
    }

    /**
     *      添付ファイルリストの作成
     */
    private List<Attachment> attachs; 
    public List<Attachment> getAttachments(){
        if(attachs==null && cmsMsg.ShowPicsByAttachment__c){
            attachs = E_MessageMasterDao.getAttachmentsByMenuId(cmsMsg.Id);
        }
        return attachs;
    }


    /**
     *      添付ファイルリストから画面を作成するhtmlファイルを取得し、取得したファイルのbodyを返す
     */
    private List<String> conts;
    private List<Attachment> upvaAttachs;
    public List<String> getContents(){
        if(conts==null && cmsMsg.ShowHTMLByAttachment__c){
            conts = new List<String>();
            if(upvaAttachs==null){
                upvaAttachs = E_MessageMasterDao.getAttachmentsByMessageId(cmsMsg.Id);
            }
            for(Attachment at: upvaAttachs){
                if(at.Name.contains('.html')){
                    conts.add(at.Body.toString());
                }
            }
        }
        return conts;
    }

    /**
     *      CMSファイル情報リスト
     */
    public Map<String, List<E_CMSFileInfo>> getFileInfoMap(){
        if(fInfoMap==null){
            createFileInfoMap();
        }
        return fInfoMap;
    }
    public List<E_CMSFileInfo> getFileList01(){
        return getFileInfoMap().get(PATTERN01);
    }
    public List<E_CMSFileInfo> getFileList02(){
        return getFileInfoMap().get(PATTERN02);
    }
    public List<E_CMSFileInfo> getFileList03(){
        return getFileInfoMap().get(PATTERN03);
    }
    public List<E_CMSFileInfo> getFileList04(){
        return getFileInfoMap().get(PATTERN04);
    }
    public List<E_CMSFileInfo> getFileList05(){
        return getFileInfoMap().get(PATTERN05);
    }
    public List<E_CMSFileInfo> getFileList06(){
        return getFileInfoMap().get(PATTERN06);
    }
    public List<E_CMSFileInfo> getFileList07(){
        return getFileInfoMap().get(PATTERN07);
    }



    private static String PATTERN01 = 'pattern01';
    private static String PATTERN02 = 'pattern02';
    private static String PATTERN03 = 'pattern03';
    private static String PATTERN04 = 'pattern04';
    private static String PATTERN05 = 'pattern05';
    private static String PATTERN06 = 'pattern06';
    private static String PATTERN07 = 'pattern07';
    private Map<String, List<E_CMSFileInfo>> fInfoMap;

    private void createFileInfoMap(){
        fInfoMap = new Map<String, List<E_CMSFileInfo>>();
        fInfoMap.put(PATTERN01,new List<E_CMSFileInfo>());
        fInfoMap.put(PATTERN02,new List<E_CMSFileInfo>());
        fInfoMap.put(PATTERN03,new List<E_CMSFileInfo>());
        fInfoMap.put(PATTERN04,new List<E_CMSFileInfo>());
        fInfoMap.put(PATTERN05,new List<E_CMSFileInfo>());
        fInfoMap.put(PATTERN06,new List<E_CMSFileInfo>());
        fInfoMap.put(PATTERN07,new List<E_CMSFileInfo>());
        
        if(cmsMsg.E_CMSFiles__r.size()==0){
            return;
        }
        
        //コンテンツ利用の場合、コンテンツ情報を取得
        Map<String, ContentVersion> contentsMap = new Map<String, ContentVersion>();
        for(E_CMSFile__c fl:cmsMsg.E_CMSFiles__r){
            contentsMap.put(fl.contentsName__c, null);
        }
        for(ContentVersion cv: E_MessageMasterDao.getContentVersionsByTitles(contentsMap.keyset())){
            contentsMap.put(cv.Title, cv);
        }
        
        //ファイルリストを作成する
        for(E_CMSFile__c cf:E_MessageMasterDao.getCMSFilesWithAttachmentByIds(cmsMsg.E_CMSFiles__r)){
            
            E_CMSFileInfo finfo = new E_CMSFileInfo(cf);
            //コンテンツ利用Version
            if(cf.recordType.DeveloperName.equalsIgnoreCase(PATTERN03) || cf.recordType.DeveloperName.equalsIgnoreCase(PATTERN07)){
                finfo.content = contentsMap.get(cf.contentsName__c);
            }
            fInfoMap.get(cf.recordType.DeveloperName.tolowerCase()).add(finfo);
        }
    }
}