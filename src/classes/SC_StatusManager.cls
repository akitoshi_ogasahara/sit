/**
    ■Statusの流れ
        未提出⇒（提出）⇒担当MR確認中⇒(確認済：MR)⇒営業部長確認中⇒(確認済：上長)⇒本社確認中
        　　　　　⇒（完了）⇒完了
    ■Actor
        AG      ：代理店、拠点担当MR
        MR      ：拠点担当MR、本社営業部担当
        MGR     ：拠点長、本社営業部部長
        CMD     ：コンプライアンス管理部
*/
public with sharing class SC_StatusManager {
    private E_AccessController access;
    private SC_Office__c record;
    private AvailableActions actions;
    private Id profileId;
    private String beforeActionLabel;
    
    // 処理完了メッセージ
    public String msgActionComplete {get; set;}

    /** Status定義 **/
    public static String STATUS_01 = SC_Const.AMSOFFICE_STATUS_01;
    public static String STATUS_02 = SC_Const.AMSOFFICE_STATUS_02;
    public static String STATUS_03 = SC_Const.AMSOFFICE_STATUS_03;
    public static String STATUS_04 = SC_Const.AMSOFFICE_STATUS_04;
    public static String STATUS_05 = SC_Const.AMSOFFICE_STATUS_05;

    /* Defect定義 */
    public static String DEFECT_01 = SC_Const.DEFECT_STATUS_01;
    public static String DEFECT_02 = SC_Const.DEFECT_STATUS_02;
    public static String DEFECT_03 = SC_Const.DEFECT_STATUS_03;
    
    /** Action定義 **/
    public Enum ScAction {NONE, APPLY, REPORT_MR, REPORT_MGR, APPROVE, REJECT_REPORT_MR, REJECT_REPORT_MGR, REJECT_APPROVE,REJECT_COMPLETE}
    public static Map<ScAction, String> scActionLabel = new Map<ScAction, String>{
                                                            ScAction.NONE => ''
                                                            ,ScAction.APPLY => SC_Const.SC_BTNLABEL_APPLY                   		// AG　→　MR
                                                            ,ScAction.REPORT_MR => SC_Const.SC_BTNLABEL_REPORT_MR           		// MR　→　MGR
                                                            ,ScAction.REPORT_MGR => SC_Const.SC_BTNLABEL_REPORT_MGR        			// MGR　→　CMD
                                                            ,ScAction.APPROVE => SC_Const.SC_BTNLABEL_APPROVE               		// CMD
                                                            ,ScAction.REJECT_REPORT_MR => SC_Const.SC_BTNLABEL_REJECT_REPORT_MR   	// MGR,MR　→　AG
                                                            ,ScAction.REJECT_REPORT_MGR => SC_Const.SC_BTNLABEL_REJECT_REPORT_MGR   // MGR,MR　→　AG
                                                            ,ScAction.REJECT_APPROVE => SC_Const.SC_BTNLABEL_REJECT_APPROVE 		// CMD　→　MR
                                                            /* 5月対応 */
                                                            ,ScAction.REJECT_COMPLETE  => SC_Const.SC_BTNLABEL_REJECT_COMPLETE         // 完了　→　CMD
                                                        };
    /**
     * アクション種別保持クラス
     */
    public class AvailableActions{
        // 次アクション
        public ScAction nextAction {get; set;}
        // 差し戻しアクション
        public ScAction rejectAction {get; set;}
        
        /** Constructor */
        public AvailableActions(ScAction nextAct, ScAction rejectAct){
            nextAction = nextAct;
            rejectAction = rejectAct;
        }
        
        /** 次アクションのラベル取得 */
        public String getNextActionLabel(){
            return scActionLabel.get(nextAction);
        }
        
        /** 差し戻しアクションのラベル取得 */
        public String getRejectActionLabel(){
            return scActionLabel.get(rejectAction);
        }
    }
    
    /**
     * Constructor
     */
    public SC_StatusManager(SC_Office__c rec, E_AccessController accCon){
        record = rec;
        access = accCon;
        profileId = access.idcpf.user__r.profileId; // [TODO]UserInfoから取得したほうがよいのか？？
        actions = getAvailableActions();
    }
    
    /**
     * アクション種別保持クラスの取得
     */
    private AvailableActions getAvailableActions(){
        // 自主点検（参照のみ）権限セット
        if(getIsPermissionReadOnly()){
            return new AvailableActions(ScAction.NONE, ScAction.NONE);
        }

        // 1.未提出
        // 2016/09/06 営業部長も提出できるように getIsMGR() を追加
        if((record.Status__c == STATUS_01 || String.isBlank(record.Status__c))
                && (getIsAG() || getIsMR() || getIsMGR() || getIsCMD())){
            return new AvailableActions(ScAction.APPLY, ScAction.NONE);
        }
        // 2.担当MR確認中
        if(record.Status__c == STATUS_02
                && (getIsMR() || getIsMGR() || getIsCMD())){
            return new AvailableActions(ScAction.REPORT_MR, ScAction.REJECT_REPORT_MR);
        }
        // 3.営業部長確認中
        if(record.Status__c == STATUS_03
                && (getIsMGR() || getIsCMD())){
            return new AvailableActions(ScAction.REPORT_MGR, ScAction.REJECT_REPORT_MGR);
        }
        // 4.本社確認中
        if(record.Status__c == STATUS_04
                && getIsCMD()){
            return new AvailableActions(ScAction.APPROVE, ScAction.REJECT_APPROVE);
        }
        /* 20170316 完了後の差戻し対応 */
        // 5.完了後
        if(record.Status__c == STATUS_05
                && getIsCMD()){
            return new AvailableActions(ScAction.NONE, ScAction.REJECT_COMPLETE);
        }
        // それ以外
        return new AvailableActions(ScAction.NONE, ScAction.NONE);
    }
    
    /***************************************************
     * NextAction
     ***************************************************/     
    /** NextAction存在判定 */
    public Boolean getHasNextAction(){
        return  actions.nextAction != ScAction.NONE;
    }
    
    /** ラベル名取得 */
    public String getNextActionLabel(){
        return actions.getNextActionLabel();
    }
    
    /** NextAction処理 */
    public PageReference doNextAction(){
    	// 処理完了メッセージの初期化
    	msgActionComplete = '';
    	
    	// 処理実行時のラベル
    	beforeActionLabel = getNextActionLabel();
    	
        // 確認依頼
        if(actions.nextAction == ScAction.APPLY){
            return doApply();
        }
        // 確認済[MR]
        if(actions.nextAction == ScAction.REPORT_MR){
            return doReportMR();
        }
        // 確認済[MGR]
        if(actions.nextAction == ScAction.REPORT_MGR){
            return doReportMGR();
        }
        // 完了
        if(actions.nextAction == ScAction.APPROVE){
            return doApprove();
        }
        
        return null;
    }
    
    /** 確認依頼アクション */
    private PageReference doApply(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        // ステータス更新
        updateStatus(STATUS_02);
        
        // 処理完了メッセージ
        msgActionComplete = getMsgActionComplete(null, 'AMS|SCSC|001');
        
        return null;
    }
    
    /** 確認済アクション[MR] */
    private PageReference doReportMR(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        // ステータス更新
        updateStatus(STATUS_03);
        
        // 処理完了メッセージ
        msgActionComplete = getMsgActionComplete(beforeActionLabel, 'AMS|SCSC|002');
        
        return null;
    }
    
    /** 確認済アクション[MGR] */
    private PageReference doReportMGR(){
        E_PageMessagesHolder.getInstance().clearMessages();
        /* 20170330 「点検実施方法（結果）」を選択しないと「本社提出」ボタンが押せない対応 */
        if(record.SCFixed__c == SC_Const.SC_FIXED_REJECT_REPORT_MGR){
            msgActionComplete = E_Message.getMsgMap().get('AMS|SCSC|004');
        }else{
            // ステータス更新
            updateStatus(STATUS_04);
            
            // 処理完了メッセージ
            msgActionComplete = getMsgActionComplete(beforeActionLabel, 'AMS|SCSC|002');
        }        
        return null;
    }
    
    /** 完了アクション */
    private PageReference doApprove(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        /* 5月対応*/
        //--なし-- ⇒ 不備対応中
        updateStatus(STATUS_05,(record.Defect__c == DEFECT_02)? DEFECT_03 : '');
        
        return null;
    }
    
    /***************************************************
     * RejectAction
     ***************************************************/
    /** RejectAction存在判定 */
    public Boolean getHasRejectAction(){
        return actions.rejectAction != ScAction.NONE;
    }
    
    /** ラベル名取得 */
    public String getRejectActionLabel(){
        return actions.getRejectActionLabel();
    }
    
    /** RejectAction処理 */
    public PageReference doRejectAction(){
    	// 処理完了メッセージの初期化
    	msgActionComplete = '';
    	
    	// 処理実行時のラベル
    	beforeActionLabel = getRejectActionLabel();
    	
        // 差し戻し[MR → AG]
        if(actions.rejectAction == ScAction.REJECT_REPORT_MR){
            return doRejectReportMR();
        }
        // 差し戻し[MGR → MR]
        if(actions.rejectAction == ScAction.REJECT_REPORT_MGR){
            return doRejectReportMGR();
        }
        // 差し戻し[CMD → MR]
        if(actions.rejectAction == ScAction.REJECT_APPROVE){
            return doRejectApprove();
        }
        //完了取消
        if(actions.rejectAction == ScAction.REJECT_COMPLETE){
            return doRejectComplete();
        }
        
        return null;
    }
    
    /** 差し戻しアクション[MGR → AG] */
    private PageReference doRejectReportMR(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        // ステータス更新
        updateStatus(STATUS_01);
        
        // 処理完了メッセージ
        msgActionComplete = getMsgActionComplete(beforeActionLabel, 'AMS|SCSC|002');
        
        return null;
    }
    
    /** 差し戻しアクション[MGR → AG] */
    private PageReference doRejectReportMGR(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        // ステータス更新
        updateStatus(STATUS_02);
        
        // 処理完了メッセージ
        msgActionComplete = getMsgActionComplete(beforeActionLabel, 'AMS|SCSC|002');
        
        return null;
    }
    
    /** 差し戻しアクション[CMD → MR] */
    private PageReference doRejectApprove(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        /* 5月対応*/
        //--なし-- 不備対応完了 ⇒ 不備対応中
        updateStatus(STATUS_02,DEFECT_02);
        
        // 処理完了メッセージ
        msgActionComplete = getMsgActionComplete(beforeActionLabel, 'AMS|SCSC|002');
        
        return null;
    }
    /** 完了解除アクション[完了 → CMD] */
    private PageReference doRejectComplete(){
        E_PageMessagesHolder.getInstance().clearMessages();
        
        // ステータス更新
        updateStatus(STATUS_04);
        
        // 処理完了メッセージ
        msgActionComplete = getMsgActionComplete(beforeActionLabel, 'AMS|SCSC|002');
        
        return null;
    }   

    /***************************************************
     * 処理完了メッセージ取得
     ***************************************************/
    /**
     *　処理完了メッセージ取得
     */
    private String getMsgActionComplete(String label, String msgKey){
    	if(String.isBlank(label)){
    		return E_Message.getMsgMap().get(msgKey);
    	}else{
    		return label + E_Message.getMsgMap().get(msgKey);
    	}
    }

    /***************************************************
     * ステータス更新
     ***************************************************/
    /**
     *　ステータス更新
     */
     /* 不備フラグ対応自動 */
    private void updateStatus(String status){
        updateStatus(status,'');
    }
    private void updateStatus(String status,String defect){
        String str = '';
        String beforeStatus = record.Status__c;
        String beforeHistory = record.StatusHistory__c;
        
        // ステータス
        record.Status__c = status;
        
        // ステータス履歴
        str += '----------------------------------------------------------------------------------------';
        str += '\n';
        str += '- ';
        str += System.now().format();
        str += '   ';
        str += UserInfo.getName();
        str += '   [';
        str += beforeStatus;
        str += ' => ';
        str += status;
        str += ']';
        str += '\n'; 
		if(String.isBlank(beforeHistory)){
	        str += '----------------------------------------------------------------------------------------';
	        str += '\n';
		}
        str += E_Util.null2Blank(beforeHistory);
        record.StatusHistory__c = str;
        /* 不備フラグ対応自動 */
        if(String.isNotBlank(defect)){
            updateDefect(defect);
        }
        // Update
        Boolean saveResult = E_GenericDao.saveViaPage(record);
        
        // アクション再取得
        if(saveResult){
            actions = getAvailableActions();
        }else{
            record.Status__c = beforeStatus;
            record.StatusHistory__c = beforeHistory;
        }
    }

    /***************************************************
     * 不備ステータス更新
     ***************************************************/
    /**
     *　不備ステータス更新時の履歴作成
     */
    private void updateDefect(String defect){
        String str = '';
        //不備ステータス更新がないときは処理をスキップ
        if(defect == record.Defect__c) return;
            
        // 不備ステータス履歴
        str += '----------------------------------------------------------------------------------------';
        str += '\n';
        str += '- ';
        str += System.now().format();
        str += '   ';
        str += UserInfo.getName();
        str += '   [';
        str += (String.isBlank(record.Defect__c))? DEFECT_01 : record.Defect__c;
        str += ' => ';
        str += (String.isBlank(defect))? DEFECT_01 : defect;
        str += ']';
        str += '\n'; 
        if(String.isBlank(record.StatusHistory__c)){
            str += '----------------------------------------------------------------------------------------';
            str += '\n';
        }
        str += E_Util.null2Blank(record.StatusHistory__c);
        record.StatusHistory__c = str;

        record.Defect__c = defect;
    }

    /***************************************************
     * 処理可能判定
     ***************************************************/   
    /**
     * CanUpload
     * アップロード or 提出種別変更 可能時にTrue
     */
    public Boolean getCanUpload(){
        // 自主点検（参照のみ）権限セット
        if(getIsPermissionReadOnly()){
            return false;
        }
        
        // 1.未提出
        // 2016/09/06 営業部長もアップロードできるように getIsMGR() を追加
        if((record.Status__c == STATUS_01 || String.isBlank(record.Status__c))
                && (getIsAG() || getIsMGR() || getIsCMD())){
            return true;
        }
        // 2.担当MR確認中, 3.営業部長確認中
        if((record.Status__c == STATUS_02 || record.Status__c == STATUS_03)
                && (getIsMR() || getIsMGR() || getIsCMD())){
            return true;
        }
        // 4.本社確認中, 5.完了
        if((record.Status__c == STATUS_04 || record.Status__c == STATUS_05)
                && getIsCMD()){
            return true;
        }
        // それ以外
        return false;
    }

    /**
     * CanDelete
     * ファイル削除可能時にTrue
     */
    public Boolean getCanDelete(){
        return getCanUpload();
    }
    
    /**
     * CanComment
     * コメント可能時にTrue
     */
    public Boolean getCanComment(){
        // 自主点検（参照のみ）権限セット
        if(getIsPermissionReadOnly()){
            return false;
        }
        
        // 2.担当MR確認中, 3.営業部長確認中
        if(!access.hasSCAGPermission()){
            return true;
        }
        // CMD
        if(getIsCMD()){
            return true;
        }
        // それ以外
        return false;
    }
    
    /**
     * CanChangeFixed
     * 点検実施方法（結果）変更可能時にTrue
     */
    public Boolean getCanChangeFixed(){
        return getCanComment();
    }
    public Boolean getCanChangeDefectStatus(){
        return getIsCMD();
    }
    public Boolean getCanViewDefectStatus(){
        return !access.hasSCAGPermission();
    }

    /**
     * CanDownload
     * 自主点検書類をダウンロード可能時にTrue
     */
    public Boolean getCanDownload(){
		// 代理店ユーザ
		if(access.hasSCAGPermission()
		// 1.未提出, 4.本社確認中, 5.完了
		&& (record.Status__c == STATUS_01 || record.Status__c == STATUS_04 || record.Status__c == STATUS_05)){
			return true;
		}

		// 社員
		if(!access.hasSCAGPermission()){
			return true;
		}

        return false;
    }
    
    /***************************************************
     * ユーザの処理権限判定
     ***************************************************/
    /**
     * AG
     * true: 自主点検（代理店） 権限セット　または　（自主点検（社員） 権限セット　かつ　MRプロファイル）
     */
    public Boolean getIsAG(){
        if(access.hasSCAGPermission()
                || (access.hasSCNNPermission() && E_ProfileDao.isMR(profileId))
                || (access.hasSCNNPermission() && E_ProfileDao.isHQMR(profileId))){
            return true;
        }
        return false;
    }
    
    /**
     * MR
     * true: 自主点検（社員） 権限セット　かつ　（MRプロファイル　または　本社営業部MRプロファイル）
     */
    public Boolean getIsMR(){
        if(access.hasSCNNPermission() 
                && (E_ProfileDao.isMR(profileId) || E_ProfileDao.isHQMR(profileId))){
            return true;    
        }
        return false;
    }

    /**
     * MGR
     * true: 自主点検（社員） 権限セット　かつ　（拠点長プロファイル　または　本社営業部拠点長プロファイル）
     */
    public Boolean getIsMGR(){
        if(access.hasSCNNPermission() 
                && (E_ProfileDao.isMRManager(profileId) || E_ProfileDao.isHQMRManager(profileId))){
            return true;    
        }
        return false;
    }
    
    /**
     * CMD
     * true: 自主点検（コンプラ） 権限セット
     */
    public Boolean getIsCMD(){
        return access.hasSCAdminPermission();
    }
    
    /**
     * 自主点検（参照のみ）権限セット
     */
    public Boolean getIsPermissionReadOnly(){
        return access.hasSCDirectorPermission();
    }
}