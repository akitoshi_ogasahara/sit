@isTest
private class TestSC_SelfCompliancesExtender {
    // コンストラクタ
    private static testMethod void SelfCompliancesTest001() {
        SC_SelfCompliancesController controller;

        Test.startTest();
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);
        Test.stopTest();  

        System.assertEquals(extender.helpMenukey,    SC_Const.MENU_HELP_SELFCOMPLIANCE);
        System.assertEquals(extender.helpPageJumpTo, SC_Const.MENU_HELP_SELFCOMPLIANCE_VIEW);
        System.assertEquals(extender.guideMenukey,   SC_Const.MENU_GUIDE_SELFCOMPLIANCE);

        return;
    }

    // init:正常パターン
    private static testMethod void SelfCompliancesTest002() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

        Test.startTest();
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();
        Test.stopTest();  
        
        System.assertEquals(extender.isSuccessOfInitValidate, true);

        return;
    }

    // init:自主点検レコード指定なし
    private static testMethod void SelfCompliancesTest003() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

        // テストデータ更新
        record.ZAGCYNUM__c = '';
        update record;

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();
        Test.stopTest();  
        
        System.assertEquals(extender.isSuccessOfInitValidate, false);

        return;
    }

/*
    // doSubmit:「確認依頼」ボタンクリック（正常）
    private static testMethod void SelfCompliancesTest004() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
        SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
        TestSC_TestUtil.createAttachment(true, scsc.Id);

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(sco);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();

            // 「確認依頼」ボタンクリック
            extender.doSubmit();
        Test.stopTest();  
        
        System.assertEquals(extender.controller.record.status__c, SC_Const.AMSOFFICE_STATUS_02);

        return;
    }

    // doSubmit:「確認依頼」ボタンクリック（添付ファイルなし）
    private static testMethod void SelfCompliancesTest005() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(sco);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();

            // 「確認依頼」ボタンクリック
            extender.doSubmit();
        Test.stopTest();  
        
        System.assertEquals(extender.controller.record.status__c, SC_Const.AMSOFFICE_STATUS_01);

        return;
    }
*/
    // toUploadPage:「提出書類アップロード」ボタン押下
    private static testMethod void SelfCompliancesTest006() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(sco);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();

            // 「提出書類アップロード」ボタンクリック
            PageReference pr = extender.toUploadPage();
        Test.stopTest();  
        
        System.assertEquals(pr.getUrl(), Page.E_SCSelfComplianceSubmit.getUrl() + '?parentid=' + controller.record.Id);

        return;
    }

    // 自主点検レコード取得
    private static testMethod void SelfCompliancesTest007() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
        //SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
        //TestSC_TestUtil.createAttachment(true, scsc.Id);

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(sco);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();

            List<SC_SelfCompliance__c> scscList = extender.selfCompliances;
            System.debug(scscList);
        Test.stopTest();  

        System.assertEquals(scscList.size(), 3);

        return;
    }

    // 表示制御用getterメソッド
    private static testMethod void SelfCompliancesTest008() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(sco);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();

            Boolean isAy = extender.getIsAy();
            String  menuKey = extender.getMenuKey();
            Boolean hasPrevSCOffice = extender.getHasPrevSCOffice();
            Boolean hasRetSCOffice  = extender.getHasRetSCOffice();
            Boolean isReadOnly = extender.getIsReadOnly();
            Boolean isStatus01 = extender.getIsSTatus01();
            String  SCStatus02 = extender.getSCStatus02();

        Test.stopTest();  

        System.assertEquals(isAy, false);
        System.assertEquals(menuKey, SC_Const.MENU_KEY_SC_SEARCH_EMPLOYEE);
        System.assertEquals(hasPrevSCOffice, false);
        System.assertEquals(hasRetSCOffice , false);
        System.assertEquals(isReadOnly , false);
        //System.assertEquals(isStatus01 , true);
        System.assertEquals(SCStatus02 , SC_Const.AMSOFFICE_STATUS_02);

        return;
    }

    // doNextAction
    private static testMethod void SelfCompliancesTest009() {
        // テストデータ作成
        Account acc = TestSC_TestUtil.createAccount(true, null);
        SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
        SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
        TestSC_TestUtil.createAttachment(true, scsc.Id);

        Test.startTest();
            // ページ表示
            Test.setCurrentPage(Page.E_SCSelfCompliances);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(sco);
            SC_SelfCompliancesController controller = new SC_SelfCompliancesController(standardcontroller);
            SC_SelfCompliancesExtender extender = new SC_SelfCompliancesExtender(controller);

            // 初期処理実行
            extender.init();

            PageReference pr = extender.doNextAction();

        Test.stopTest();

        System.assertEquals(pr, null);

        return;
    }
}