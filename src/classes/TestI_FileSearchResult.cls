@isTest
private class TestI_FileSearchResult {

	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_attachment_record() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('enabled attachment');
			List<Attachment> attachments = E_AttachmentDao.getRecsByParentIds(new Set<Id>{rec.Id}).get(rec.Id);
			I_CMSComponentController component = new I_CMSComponentController(rec, attachments);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			I_KeywordMarker marker = new I_KeywordMarker();
			marker.addKeyword('attach');
			result.setMarker(marker);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(true, result.isMatched());
			System.assertEquals('enabled <mark>attach</mark>ment', result.getMarkedFileName());
			System.assertEquals('DL(Attachment)', result.getClickAction());
			System.assertEquals('attachment.txt', result.getFilePath());
		}
	}

	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_nnlink_record() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('enabled nnlink');
			E_CMSFile__c cms = E_MessageMasterDao.getCMSFilesWithAttachmentByKeys(new Set<String>{rec.filePath__c}).get(rec.filePath__c);
			I_CMSComponentController component = new I_CMSComponentController(rec, cms.Attachments);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled nnlink', result.getMarkedFileName());
			System.assertEquals('DL(NNLink)', result.getClickAction());
			System.assertEquals(rec.filePath__c, result.getFilePath());
		}
	}

	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_chatter_record() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('enabled chatter');
			Id chatterFileId = I_ContentMasterDao.getChatterFileIdsByRecId(new Set<Id>{rec.Id}).get(rec.Id);
			I_CMSComponentController component = new I_CMSComponentController(rec, null, chatterFileId);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled chatter', result.getMarkedFileName());
			System.assertEquals('DL(ChatterFile)', result.getClickAction());
			System.assertEquals('chatter.txt', result.getFilePath());
		}
	}

	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_contents_record() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('enabled contents');
			List<Attachment> attachments = E_AttachmentDao.getRecsByParentIds(new Set<Id>{rec.Id}).get(rec.Id);
			I_CMSComponentController component = new I_CMSComponentController(rec, attachments);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled contents', result.getMarkedFileName());
			System.assertEquals('DL(Contents)', result.getClickAction());
			System.assertEquals('Contents.txt', result.getFilePath());
		}
	}

	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_feature_record1() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('enabled feature1');
			List<Attachment> attachments = E_AttachmentDao.getRecsByParentIds(new Set<Id>{rec.Id}).get(rec.Id);
			I_CMSComponentController component = new I_CMSComponentController(rec, attachments);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled feature1', result.getMarkedFileName());
			System.assertEquals('DL(Attachment)', result.getClickAction());
			System.assertEquals('feature attachment1.txt', result.getFilePath());
		}
	}

	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_feature_record2() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('enabled feature2');
			List<Attachment> attachments = E_AttachmentDao.getRecsByParentIds(new Set<Id>{rec.Id}).get(rec.Id);
			I_CMSComponentController component = new I_CMSComponentController(rec, attachments);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled feature2', result.getMarkedFileName());
			System.assertEquals('DL(Attachment)', result.getClickAction());
			System.assertEquals('feature attachment2.txt', result.getFilePath());
		}
	}

	private static testMethod void should_return_true_when_has_matched_download_type() {
		Test.startTest();
		System.assertEquals(true, I_FileSearchResult.isContents(new I_ContentMaster__c(clickAction__c = 'DL(Contents)')));
		System.assertEquals(true, I_FileSearchResult.isNNLink(new I_ContentMaster__c(clickAction__c = 'DL(NNLink)')));
		System.assertEquals(true, I_FileSearchResult.isAttachment(new I_ContentMaster__c(clickAction__c = 'DL(Attachment)')));
		System.assertEquals(true, I_FileSearchResult.isChatterFile(new I_ContentMaster__c(clickAction__c = 'DL(ChatterFile)')));
		System.assertEquals(true, I_FileSearchResult.isWebContents(new I_ContentMaster__c(clickAction__c = 'URL(Webコンテンツ)')));
		System.assertEquals(true, I_FileSearchResult.isURL(new I_ContentMaster__c(clickAction__c = 'URL')));
		Test.stopTest();
	}

	private static testMethod void should_sort_result_list_with_sort_option() {
		System.runAs(getAdmin()) {
			List<I_FileSearchResult> results = new List<I_FileSearchResult>{
				new I_FileSearchResult(), new I_FileSearchResult(), new I_FileSearchResult()
			};
			I_FileSearchResult.setSortOption(I_FileSearchResult.SORT_TYPE_PAGE_NAME, true);
			System.assertEquals(true, I_FileSearchResult.sortBy(I_FileSearchResult.SORT_TYPE_PAGE_NAME));
			System.assertEquals(true, I_FileSearchResult.isAsc());
			results.sort();

			I_FileSearchResult.setSortOption(I_FileSearchResult.SORT_TYPE_FILE_NAME, false);
			System.assertEquals(true, I_FileSearchResult.sortBy(I_FileSearchResult.SORT_TYPE_FILE_NAME));
			System.assertEquals(false, I_FileSearchResult.isAsc());
			results.sort();
		}
	}

	private static testMethod void should_build_instance_with_marker_when_document_request_record1() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('document request1');
			TestE_TestUtil.createMenuMaster(true, 'document_order', 'document_order', 'E_Menu', false);
			List<Attachment> attachments = E_AttachmentDao.getRecsByParentIds(new Set<Id>{rec.Id}).get(rec.Id);
			I_CMSComponentController component = new I_CMSComponentController(rec, attachments);
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			I_KeywordMarker marker = new I_KeywordMarker();
			marker.addKeyword('request1');
			result.setMarker(marker);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(true, result.isMatched());
			System.assertEquals('document <mark>request1</mark>', result.getMarkedFileName());
			System.assertEquals('DL(Attachment)', result.getClickAction());
			System.assertEquals('document request.txt', result.getFilePath());
		}
	}

	private static testMethod void should_build_instance_with_marker_when_document_request_record2() {
		System.runAs(getAdmin()) {
			I_ContentMaster__c rec = getMenuByName('document request2');
			TestE_TestUtil.createMenuMaster(true, 'document_order', 'document_order', 'E_Menu', false);
			I_CMSComponentController component = new I_CMSComponentController(rec, new List<Attachment>());
			I_FileSearchResult result = I_FileSearchResult.build(rec, component);
			I_KeywordMarker marker = new I_KeywordMarker();
			marker.addKeyword('request2');
			result.setMarker(marker);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(true, result.isMatched());
			System.assertEquals('document <mark>request2</mark>', result.getMarkedFileName());
			System.assertEquals('URL', result.getClickAction());
			System.assertEquals('url', result.fileUrl);
		}
	}

	private static User getAdmin() {
		List<User> users = [SELECT id FROM User WHERE LastName =: 'fstest'];
		return users.size() > 0 ? users.get(0) : null;
	}

	@testSetup
	public static void setup() {
		TestE_TestUtil.createIDCPF(true, TestI_TestUtil.createUser(true, 'fstest', 'システム管理者').Id);

		Map<String, E_MessageMaster__c> messageMastersByKey = new Map<String, E_MessageMaster__c>();
		{
			E_MessageMaster__c messageMaster;
			messageMaster = new E_MessageMaster__c(
				Name = 'nnlink',
				Key__c = 'nnlink'
			);
			messageMastersByKey.put(messageMaster.Name, messageMaster);
		}
		insert messageMastersByKey.values();

		Map<String, E_CMSFile__c> csmFilesByKey = new Map<String, E_CMSFile__c>();
		{
			E_CMSFile__c cmsFile;
			cmsFile = new E_CMSFile__c(
				Name = 'nnlink file',
				MessageMaster__c = messageMastersByKey.get('nnlink').Id
			);
			csmFilesByKey.put(cmsFile.Name, cmsFile);
		}
		insert csmFilesByKey.values();
		for (E_CMSFile__c cmsFile: [SELECT Id, Name, MessageMaster__c, UpsertKey__c FROM E_CMSFile__c WHERE Id IN: new Map<Id, E_CMSFile__c>(csmFilesByKey.values()).keyset()]) {
			csmFilesByKey.put(cmsFile.Name, cmsFile);
		}

		Map<String, I_MenuMaster__c> menusByKey = new Map<String, I_MenuMaster__c>();
		{
			I_MenuMaster__c menu;
			menu = new I_MenuMaster__c(
				name = 'enabled menu',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_FAQ
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = 'enabled fearure menu',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = 'パンフレット等発送依頼',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT,
				MenuKey__c = 'document_order'
			);
			menusByKey.put(menu.name, menu);
		}
		insert menusByKey.values();

		Map<String, I_PageMaster__c> pagesByKey = new Map<String, I_PageMaster__c>();
		{
			I_PageMaster__c page;
			page = new I_PageMaster__c(
				name = 'enabled page',
				Menu__c = menusByKey.get('enabled menu').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = 'enabled feature page',
				Menu__c = menusByKey.get('enabled fearure menu').Id
			);
			pagesByKey.put(page.name, page);
		}
		insert pagesByKey.values();

		Map<String, I_ContentMaster__c> sectionsByKey = new Map<String, I_ContentMaster__c>();
		{
			I_ContentMaster__c section;

			section = new I_ContentMaster__c(
				name = 'enabled section',
				Page__c = pagesByKey.get('enabled page').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = 'enabled feature section',
				Page__c = pagesByKey.get('enabled feature page').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = '販売促進資料'
			);
			sectionsByKey.put(section.name, section);
		}
		insert sectionsByKey.values();

		Map<String, I_ContentMaster__c> componentsByKey = new Map<String, I_ContentMaster__c>();
		{
			I_ContentMaster__c component;

			component = new I_ContentMaster__c(
				name = 'enabled attachment',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'DL(Attachment)',
				filePath__c = 'attachment.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled nnlink',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'DL(NNLink)',
				filePath__c = csmFilesByKey.get('nnlink file').UpsertKey__c
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled chatter',
				ParentContent__c = sectionsByKey.get('販売促進資料').Id,
				ClickAction__c = 'DL(ChatterFile)',
				filePath__c = 'chatter.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled contents',
				Page__c = pagesByKey.get('enabled page').Id,
				ClickAction__c = 'DL(Contents)',
				filePath__c = 'Contents.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled feature1',
				Page__c = pagesByKey.get('enabled feature page').Id,
				ClickAction__c = 'DL(Attachment)',
				filePath__c = 'feature attachment1.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled feature2',
				ParentContent__c = sectionsByKey.get('enabled feature section').Id,
				ClickAction__c = 'DL(Attachment)',
				filePath__c = 'feature attachment2.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'document request1',
				DocumentCategory__c = 'パンフレット／特に重要なお知らせ',
				ClickAction__c = 'DL(Attachment)',
				FormNo__c = 'ZZ-1234',
				filePath__c = 'document request.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'document request2',
				DocumentCategory__c = 'ご契約のしおり・約款',
				ClickAction__c = 'URL',
				FormNo__c = 'ZZ-1234',
				LinkURL__c = 'url'
			);
			componentsByKey.put(component.name, component);
		}
		insert componentsByKey.values();

		Map<String, ContentVersion> versionFilesByKey = new Map<String, ContentVersion>();
		{
			ContentVersion versionFile;
			versionFile = new  ContentVersion(
				title = 'chtter.txt',
				PathOnClient = 'chtter.txt',
				VersionData = Blob.valueOf('chtter'),
				Origin = 'H'
			);
			versionFilesByKey.put(versionFile.title, versionFile);
		}
		insert versionFilesByKey.values();

		Map<String, FeedItem> feedsByKey = new Map<String, FeedItem>();
		{
			FeedItem feed;
			feed = new FeedItem(
				Body = 'chtter',
				Type = 'ContentPost',
				ParentId = componentsByKey.get('enabled chatter').Id,
				RelatedRecordId = versionFilesByKey.get('chtter.txt').Id
			);
			feedsByKey.put(feed.body, feed);
		}
		insert feedsByKey.values();

		Map<String, Attachment> attachmentsByKey = new Map<String, Attachment>();
		{
			Attachment attachmentFile;

			attachmentFile = new Attachment(
				Name = componentsByKey.get('enabled attachment').filePath__c,
				ParentId = componentsByKey.get('enabled attachment').Id,
				Body = Blob.valueOf('attachment.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);

			attachmentFile = new Attachment(
				Name = csmFilesByKey.get('nnlink file').UpsertKey__c,
				ParentId = csmFilesByKey.get('nnlink file').Id,
				Body = Blob.valueOf('nnlink.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);

			attachmentFile = new Attachment(
				Name = componentsByKey.get('enabled contents').filePath__c,
				ParentId = componentsByKey.get('enabled contents').Id,
				Body = Blob.valueOf('attachment.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);

			attachmentFile = new Attachment(
				Name = componentsByKey.get('enabled feature1').filePath__c,
				ParentId = componentsByKey.get('enabled feature1').Id,
				Body = Blob.valueOf('feature attachment1.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);

			attachmentFile = new Attachment(
				Name = componentsByKey.get('enabled feature2').filePath__c,
				ParentId = componentsByKey.get('enabled feature2').Id,
				Body = Blob.valueOf('feature attachment2.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);

			attachmentFile = new Attachment(
				Name = componentsByKey.get('document request1').filePath__c,
				ParentId = componentsByKey.get('document request1').Id,
				Body = Blob.valueOf('document request.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);
		}
		insert attachmentsByKey.values();
	}

	private static I_ContentMaster__c getMenuByName(String name) {
		I_ContentMaster__c result;
		for (I_ContentMaster__c record: [
			SELECT
				Id,
				Name,
				Content__c,
				ClickAction__c,
				filePath__c,
				DocumentAndBranchNo__c,
				DocumentNo__c,
				Dom_Id__c,
				CanDisplayVia__c,
				FeatureFlag__c,
				FeatureSearch__c,
				FeatureContents__c,
				DocumentCategory__c,
				FormNo__c,
				LinkURL__c,
				Page__c,
				Page__r.Name,
				Page__r.Menu__c,
				Page__r.page_unique_key__c,
				ParentContent__c,
				ParentContent__r.Name,
				ParentContent__r.Page__r.Name,
				ParentContent__r.Page__r.Menu__c,
				ParentContent__r.Page__r.page_unique_key__c
			FROM
				I_ContentMaster__c
			WHERE
				name =: name
		]) {
			result = record;
			break;
		}
		return result;
	}
}