@isTest
private class TestI_UIFrameController {
	
	@isTest static void sumiseiIRISUserTest01() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);

		}
		System.runAs(uc){
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			I_UIFrameController controller = new I_UIFrameController();
			System.assertEquals(controller.mainStyle,'sumisei-page__content sumisei-page__content--fixed clearfix');
			Test.stopTest();
		}


	}


	@isTest static void sumiseiUserTest01() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',false);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);

			
		}
		System.runAs(uc){
			TestI_TestUtil.createBasePermissions(uc.Id);
			TestI_TestUtil.createAgencyPermissions(uc.Id);
			Test.startTest();
			I_UIFrameController controller = new I_UIFrameController();
			System.assertEquals(controller.mainStyle,'iris-content iris-layout-panel');
			Test.stopTest();
		}


	}

	//代理店格作成
	static Account createParentAccount(Boolean sumisei){
		Account parentAcc = new Account();
		parentAcc.Name = 'テスト住生代理店格';
		parentAcc.E_CL1PF_ZHEADAY__c = '12345';
		if(sumisei){
			parentAcc.E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI;
		}
		parentAcc.E_COMM_VALIDFLAG__c = '1';
		Insert parentAcc;
		return parentAcc;
	}

	//代理店事務所作成
	static Account createChildAccount(Account parent){
		Account childAcc = new Account();
		childAcc.Name = 'テスト住生代理店事務所';
		childAcc.E_CL2PF_ZAGCYNUM__c = '67890';
		childAcc.E_COMM_VALIDFLAG__c = '1';
		childAcc.ZMRCODE__c = 'MR00001';
		childAcc.ParentId = parent.Id;

		Insert childAcc;
		return childAcc;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 * @param sumiseiIRIS sumiseiIRISUser
	 */
	static Contact createSumiseiContact(Id accId, String lastName,Boolean sumiseiIRIS) {
		Contact con = new Contact();
		con.AccountId = accId;
		con.LastName = lastName;
		con.E_CL3PF_AGTYPE__c = '99';
		if(sumiseiIRIS){
			con.E_CL3PF_AGTYPE__c = '02';
		}
		Insert con;
		Contact con2 = [select Id from Contact where Id =: con.Id];
		return con2;
	}

}