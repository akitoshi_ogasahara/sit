/*
 * CC_AccountVOIController
 * Controller class for Account
 * created  : Accenture 2018/7/3
 * modified :
 */

global with sharing class CC_AccountVOIController implements Vlocity_ins.VlocityOpenInterface2{
	global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);

		Boolean success = true;
		try{
			if(methodName == 'saveAccount'){
				success = CC_AccountVOIBizLogic.saveAccount(inputs, output, options);
			}

		}catch(Exception e){
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}
}