global with sharing class E_ADRPFConrtoller extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public E_ADRPF__c record {get{return (E_ADRPF__c)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_ADRPFExtender getExtender() {return (E_ADRPFExtender)extender;}
    
    
    public E_ADRPFConrtoller(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component1526');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1526',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','confirmation_registration');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component27');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component27',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','ヘルプ画面へ');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_ContactInfo');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
        tmpPropMap.put('Component__Width','80');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component1658');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1658',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1504');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1504',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1530');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1530',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component1531');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1531',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','confirmation_registration');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component740');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component740',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1525');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1525',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('secret','{!Extender.inputPassWord}');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1072');
        tmpPropMap.put('Component__Name','EInputSecret');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1072',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('oldpw','{!Extender.inputPassWord}');
        tmpPropMap.put('newpw','{!Extender.newPassWord}');
        tmpPropMap.put('vrfnewpw','{!Extender.verPassWord}');
        tmpPropMap.put('newml','');
        tmpPropMap.put('vrfnewml','');
        tmpPropMap.put('chkval','');
        tmpPropMap.put('pwtype','2');
        tmpPropMap.put('ptype','');
        tmpPropMap.put('Component__Width','400');
        tmpPropMap.put('Component__Height','116');
        tmpPropMap.put('Component__id','Component1396');
        tmpPropMap.put('Component__Name','EChangePW');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1396',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1544');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1544',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','ヘルプ画面へ');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_ContactInfo');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
        tmpPropMap.put('Component__Width','80');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component1668');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1668',tmpPropMap);

        lastPageNumber = 2;
        pagesMap.put(1,'E_ADRPFChange');
        pagesMap.put(2,'E_ADRPFConfirm');

        SObjectField f;

        f = E_ADRPF__c.fields.E_CADPF__c;
        f = E_ADRPF__c.fields.ZNKJADDR__c;
        f = E_ADRPF__c.fields.ZNKJADDR01__c;
        f = E_ADRPF__c.fields.ZNKJADDR02__c;
        f = E_ADRPF__c.fields.ZADRSKN__c;
        f = E_ADRPF__c.fields.ZNCLTADR01__c;
        f = E_ADRPF__c.fields.ZNCLTADR02__c;
        f = E_ADRPF__c.fields.CLTPHONE011__c;
        f = E_ADRPF__c.fields.CLTPHONE012__c;
        f = E_ADRPF__c.fields.CLTPHONE013__c;
        f = E_ADRPF__c.fields.CLTPHONE021__c;
        f = E_ADRPF__c.fields.CLTPHONE022__c;
        f = E_ADRPF__c.fields.CLTPHONE023__c;
        f = E_ADRPF__c.fields.FAXNO1__c;
        f = E_ADRPF__c.fields.FAXNO2__c;
        f = E_ADRPF__c.fields.FAXNO3__c;
        f = E_ADRPF__c.fields.CLTPCODE__c;
        f = E_ADRPF__c.fields.CLTPHONE01__c;
        f = E_ADRPF__c.fields.CLTPHONE02__c;
        f = E_ADRPF__c.fields.FAXNO__c;
        f = E_CADPF__c.fields.ZADRSKJ__c;
        f = E_CADPF__c.fields.Reserve__c;
        f = E_CADPF__c.fields.ZADRSKN__c;
        f = E_CADPF__c.fields.Name;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = E_ADRPF__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            recordTypeIdMap = SkyEditor2.Util.SetRecordTypesMap('E_ADRPF__c');
            mainQuery = new SkyEditor2.Query('E_ADRPF__c');
            mainQuery.addField('E_CADPF__c');
            mainQuery.addField('ZNKJADDR01__c');
            mainQuery.addField('ZNKJADDR02__c');
            mainQuery.addField('ZNCLTADR01__c');
            mainQuery.addField('ZNCLTADR02__c');
            mainQuery.addField('CLTPHONE011__c');
            mainQuery.addField('CLTPHONE012__c');
            mainQuery.addField('CLTPHONE013__c');
            mainQuery.addField('CLTPHONE021__c');
            mainQuery.addField('CLTPHONE022__c');
            mainQuery.addField('CLTPHONE023__c');
            mainQuery.addField('FAXNO1__c');
            mainQuery.addField('FAXNO2__c');
            mainQuery.addField('FAXNO3__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('ZNKJADDR__c');
            mainQuery.addFieldAsOutput('ZADRSKN__c');
            mainQuery.addFieldAsOutput('CLTPCODE__c');
            mainQuery.addFieldAsOutput('CLTPHONE01__c');
            mainQuery.addFieldAsOutput('CLTPHONE02__c');
            mainQuery.addFieldAsOutput('FAXNO__c');
            mainQuery.addFieldAsOutput('MailNotSend__c');
            mainQuery.addFieldAsOutput('E_CADPF__r.Name');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            extender = new E_ADRPFExtender(this);
            p_showHeader = false;
            p_sidebar = false;
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();

        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    
    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }

    public void loadReferenceValues_zipcode() {
        if (record.E_CADPF__c == null) {

            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNKJADDR__c)) {
                record.ZNKJADDR__c = null;
            }
        
            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNKJADDR01__c)) {
                record.ZNKJADDR01__c = null;
            }
        
            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNKJADDR02__c)) {
                record.ZNKJADDR02__c = null;
            }
        
            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZADRSKN__c)) {
                record.ZADRSKN__c = null;
            }
        
            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNCLTADR01__c)) {
                record.ZNCLTADR01__c = null;
            }
        
            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNCLTADR02__c)) {
                record.ZNCLTADR02__c = null;
            }
        
            if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.CLTPCODE__c)) {
                record.CLTPCODE__c = null;
            }
                return;
        }
        E_CADPF__c[] referenceTo = [SELECT ZADRSKJ__c,Reserve__c,ZADRSKN__c,Name FROM E_CADPF__c WHERE Id=:record.E_CADPF__c];
        if (referenceTo.size() == 0) {
            record.E_CADPF__c.addError(SkyEditor2.Messages.referenceDataNotFound(record.E_CADPF__c));
            return;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNKJADDR__c)) {
            record.ZNKJADDR__c = referenceTo[0].ZADRSKJ__c;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNKJADDR01__c)) {
            record.ZNKJADDR01__c = referenceTo[0].Reserve__c;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNKJADDR02__c)) {
            record.ZNKJADDR02__c = referenceTo[0].Reserve__c;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZADRSKN__c)) {
            record.ZADRSKN__c = referenceTo[0].ZADRSKN__c;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNCLTADR01__c)) {
            record.ZNCLTADR01__c = referenceTo[0].Reserve__c;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.ZNCLTADR02__c)) {
            record.ZNCLTADR02__c = referenceTo[0].Reserve__c;
        }
        
        if (SkyEditor2.Util.isEditable(record, E_ADRPF__c.fields.CLTPCODE__c)) {
            record.CLTPCODE__c = referenceTo[0].Name;
        }
        
    }

    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}