public with sharing class E_DownloadHoldPolicySearchExtension {
	
	E_DownloadController extention;
	
	public Boolean isColi {get; private set;}
	public List<String> targetOfficeIdList {get; set;}
	public String Coli_DataOutputUnit {get; set;}		//データ出力単位
	public String Comm_Zacctym {get; set;}		//成立年月
	public String Comm_Occdate {get; set;}		//契約日（更新日）
	public String Comm_Zcntrdsc {get; set;}		//契約者
	public String Spva_Zannfrom {get; set;}		//年金支払開始
	
	public String startDate {get; private set;}
	public String endDate {get; private set;}
	public String fromZacctym {get; set;}
	public String toZacctym {get; set;}
	public String fromOccdate {get; set;}
	public String toOccdate {get; set;}
	
	private final string SELECT_All = 'all';
	private final string SELECT_RANGE = 'range';
	
	public List<SelectOption> getDataOutputUnitList(){
		List<SelectOption> ops = new List<SelectOption>();
		ops.add(new SelectOption('1','契約単位'));
		ops.add(new SelectOption('2','保険種類単位（主契約・各特約単位）'));
		return ops;
	}
	
	public List<SelectOption> getAllOrRangeList(){
		List<SelectOption> ops = new List<SelectOption>();
		ops.add(new SelectOption(SELECT_All,'全期間指定'));
		ops.add(new SelectOption(SELECT_RANGE,'期間指定'));
		return ops;
	}
	
	public List<SelectOption> getZcntrdscList(){
		List<SelectOption> ops = new List<SelectOption>();
		ops.add(new SelectOption(SELECT_All,'全契約者'));
		ops.add(new SelectOption('P','個人契約のみ'));
		ops.add(new SelectOption('C','法人契約のみ'));
		return ops;
	}
	
	public List<SelectOption> getZannfromList(){
		List<SelectOption> ops = new List<SelectOption>();
		ops.add(new SelectOption(SELECT_All,'全契約'));
		ops.add(new SelectOption('0','年金支払開始日経過後の契約（年金支払中および年金支払案内中の契約）'));
		ops.add(new SelectOption('1','年金支払開始日経過前の契約'));
		return ops;
	}
	
	public String getTitle(){
		return '保有契約一覧　ダウンロード抽出条件';
	}
	
	public E_DownloadHoldPolicySearchExtension(E_DownloadController extention){
		this.extention = extention;
		//パラメータによる実行者情報取得
		extention.isValidate = extention.getActor() && checkCanDisplay();
		extention.pageRef = extention.init();
	
		if(extention.pageRef == null){
			extention.setPgTitle(getTitle());
			//初期値設定
			targetOfficeIdList = new List<String>();
			Comm_Zacctym = SELECT_RANGE;
			fromZacctym = E_Util.formatDataSyncDate('yyyy/MM');
			toZacctym = E_Util.formatDataSyncDate('yyyy/MM');
			Comm_Occdate = SELECT_All;
			Comm_Zcntrdsc = SELECT_All;
			endDate = E_Util.formatDataSyncDate();
			if(isColi){
				startDate = '1986/01/01';
				Coli_DataOutputUnit = '1';
				extention.formNo = E_Const.DH_FORMNO_HOLDPOLI_COLI;
			}else{
				startDate = '2000/01/01';
				Spva_Zannfrom = SELECT_All;
				extention.formNo = E_Const.DH_FORMNO_HOLDPOLI_SPVA;
			}
		}
	}
	
	public PageReference pageAction(){
		return E_Util.toErrorPage(extention.pageRef, null);
	}
	
	//実行者のページに対する権限チェック
	private boolean checkCanDisplay(){
		if(getParamPt()){
			if(isColi){
				return extention.getCanDisplayHoldPolicyColi();
			}else{
				return extention.getCanDisplayHoldPolicySpva();
			}
		}
		return false;
	}
	
	public boolean getParamPt(){	
		//パラメータにより、表示非表示・初期値を制御する
		String policyType = ApexPages.CurrentPage().getParameters().get(extention.KEY_PT);
		if(policyType != null && policyType.equals(extention.VAL_COLI)){
			isColi = true;
		}else if(policyType != null && policyType.equals(extention.VAL_SPVA)){
			isColi = false;
		}else{
			//パラメータが不正の場合はエラーページへ
			extention.errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		return true;
	}
	
	
	public PageReference doDownloadCsv(){
		PageReference ret = null;
		extention.pageMessages.clearMessages();
		
		try {
			extention.dh = new E_DownloadHistorry__c();
			if(!checkInputForm()){
				return null;
			}
			
			String tmp_ZIDTYPE;					//ID種別
			String tmp_distributorCode;			//代理店格Id
			String tmp_ZHEADAY;	//代理店格コード（5桁）
			String tmp_ZHEADNAME;			//代理店格名
			List<String> tmp_officeCodes = new List<String>();	//代理店事務所コードリスト
			String tmp_agentCode;				//代理店募集人コード
			String tmp_fromZACCTYM;				//成立年月
			String tmp_toZACCTYM;
			String tmp_fromOCCDATE;				//契約日（更新日）
			String tmp_toOCCDATE;
			String tmp_ZCNTRDSC;				//契約者
			
			//ダウンロードログレコード生成
			extention.dh.outputType__c = E_Const.DH_OUTPUTTYPE_CSV;
			
			if(extention.distributor <> null){
				tmp_ZIDTYPE = E_Const.ZIDTYPE_AH;
				tmp_distributorCode = extention.distributor.Id;
				tmp_ZHEADAY = extention.distributor.E_CL1PF_ZHEADAY__c;
				tmp_ZHEADNAME = extention.distributor.Name;
				tmp_officeCodes = targetOfficeIdList;
			} else if(extention.office <> null){
				tmp_ZIDTYPE = E_Const.ZIDTYPE_AY;
				tmp_distributorCode = extention.office.ParentId;
				tmp_ZHEADAY = extention.office.E_ParentZHEADAY__c;
				tmp_ZHEADNAME = extention.office.E_ParentName__c;
				tmp_officeCodes.add(extention.office.Id);
			} else if(extention.agent <> null){
				tmp_ZIDTYPE = E_Const.ZIDTYPE_AT;
				tmp_distributorCode = extention.agent.Account.ParentId;
				tmp_ZHEADAY = extention.agent.E_AccParentCord__c;
				tmp_ZHEADNAME = extention.agent.E_AccParentName__c;
				tmp_officeCodes.add(extention.agent.AccountId);
				Contact tmp_con = E_ContactDaoWithout.getRecByAgentNumber(extention.agent.E_CL3PF_AGNTNUM__c.Left(5));
				//Contact tmp_con = E_ContactDao.getRecByAgentNumber(extention.agent.E_CL3PF_AGNTNUM__c.Left(5));
				if(tmp_con == null){
					extention.pageMessages.addErrorMessage('メインアカウントが存在しません。');
					return null;
				}
				tmp_agentCode = tmp_con.Id;
			}
			//成立年月
			if(Comm_Zacctym != SELECT_All){
				tmp_fromZACCTYM = E_Util.slashTrim(fromZacctym);
				tmp_toZACCTYM = E_Util.slashTrim(toZacctym);
			}
			//契約日（更新日）
			if(Comm_Occdate != SELECT_All){
				tmp_fromOCCDATE = E_Util.slashTrim(fromOccdate);
				tmp_toOCCDATE = E_Util.slashTrim(toOccdate);
			}
			//契約者
			if(Comm_Zcntrdsc != SELECT_All){
				tmp_ZCNTRDSC = Comm_Zcntrdsc;
			}
			//個人保険
			if(isColi){
				//OPROにリクエストを投げる
				E_DownloadCondition.COLIDownloadCondi dlCondi = new E_DownloadCondition.COLIDownloadCondi();
				dlCondi.ZIDTYPE = tmp_ZIDTYPE;
				dlCondi.distributorCode = tmp_distributorCode;
				dlCondi.ZHEADAY = tmp_ZHEADAY;
				dlCondi.ZHEADNAME = tmp_ZHEADNAME;
				dlCondi.officeCodes = tmp_officeCodes;
				dlCondi.agentCode = tmp_agentCode;
				dlCondi.groupby = Coli_DataOutputUnit;	//データ出力単位
				dlCondi.fromZACCTYM = tmp_fromZACCTYM;
				dlCondi.toZACCTYM = tmp_toZACCTYM;
				dlCondi.fromOCCDATE = tmp_fromOCCDATE;
				dlCondi.toOCCDATE = tmp_toOCCDATE;
				dlCondi.ZCNTRDSC = tmp_ZCNTRDSC;
				
				extention.dh.FormNo__c = extention.formNo;
				extention.dh.Conditions__c = dlCondi.toJSON();
			//SPVA
			}else{
				//OPROにリクエストを投げる
				E_DownloadCondition.SPVADownloadCondi dlCondi = new E_DownloadCondition.SPVADownloadCondi();
				dlCondi.ZIDTYPE = tmp_ZIDTYPE;
				dlCondi.distributorCode = tmp_distributorCode;
				dlCondi.ZHEADAY = tmp_ZHEADAY;
				dlCondi.ZHEADNAME = tmp_ZHEADNAME;
				dlCondi.officeCodes = tmp_officeCodes;
				dlCondi.agentCode = tmp_agentCode;
				dlCondi.fromZACCTYM = tmp_fromZACCTYM;
				dlCondi.toZACCTYM = tmp_toZACCTYM;
				dlCondi.fromOCCDATE = tmp_fromOCCDATE;
				dlCondi.toOCCDATE = tmp_toOCCDATE;
				dlCondi.ZCNTRDSC = tmp_ZCNTRDSC;
				if(Spva_Zannfrom != SELECT_All){
					dlCondi.ZANNSTFLG = Spva_Zannfrom;	//年金支払開始
				}
				
				extention.dh.FormNo__c = extention.formNo;
				extention.dh.Conditions__c = dlCondi.toJSON();
			}
			insert extention.dh;
			
		} catch(Exception e) {
			extention.pageMessages.addErrorMessage(e.getMessage());
			ret = null;
		}
		
		return ret;
	}
	private boolean checkInputForm(){
		//代理店格の時、代理店事務所未選択

		if(extention.distributor != null && targetOfficeIdList.isEmpty()){
			extention.pageMessages.addErrorMessage('代理店事務所を選択してください。');
			return false;
		}
		//成立年月チェック
		if(Comm_Zacctym != SELECT_All){
			if(!extention.validateDates(fromZacctym, toZacctym, '成立年月', true)){
				return false;
			}
		}
		
		//契約日（更新日）チェック
		if(Comm_Occdate != SELECT_All){
			if(!extention.validateDates(fromOccdate, toOccdate, '契約日（更新日）', false)){
				return false;
			}
		}
		return true;
	}
}