public with sharing class MD_DetailPrintExtension extends MD_AbstractController {

	// Doctor
	private MD_Doctor__c doctor;

	/**
	 * Constructor
	 */	
	public MD_DetailPrintExtension(ApexPages.StandardController stdController) {
		this.doctor = (MD_Doctor__c)stdController.getRecord();
	}

	public String getZoom() {
		return ApexPages.currentPage().getParameters().get('zoom');
	}

	/**
	 * init
	 */
	public PageReference init() {
		// IRIS Day2 ページアクセスログは E_Logging コンポーネントで取得するように変更
		return null;
		/*
		// アクセスログ登録
		logCondition.recId = doctor.Id;
		return insertAccessLog();
		*/
	}
}