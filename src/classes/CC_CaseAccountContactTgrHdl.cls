public with sharing class CC_CaseAccountContactTgrHdl{

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){
		//顧客の苦情フラグを更新
		List<Case> newCaseList = new List<Case>();
		for(Case caseObj : newList){
			if(caseObj.CC_SRTypeId__c != null){
				newCaseList.add(caseObj);
			}
		}

		if(newCaseList.size() > 0){
			Boolean isDelete = false;
			updateIsClaimedFlag(newCaseList, isDelete);
		}

	}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		//顧客の苦情フラグを更新
		List<Case> newCaseList = new List<Case>();
		for(Case caseObj : newList){
			if(caseObj.CC_SRTypeId__c != null && caseObj.CC_SRTypeId__c != oldMap.get(caseObj.Id).CC_SRTypeId__c){
				newCaseList.add(caseObj);
			}
		}

		if(newCaseList.size() > 0){
			Boolean isDelete = false;
			updateIsClaimedFlag(newCaseList, isDelete);
		}

	}

	/**
	 * Before Delete呼び出しメソッド
	 */
	public static void onBeforeDelete(List<Case> oldList, Map<Id,Case> oldMap){}

	/**
	 * After Delete呼び出しメソッド
	 */
	public static void onAfterDelete(List<Case> oldList, Map<Id,Case> oldMap){
		//顧客の苦情フラグを更新
		List<Case> oldCaseList = new List<Case>();
		for(Case caseObj : oldList){
			if(caseObj.CC_SRTypeId__c != null){
				oldCaseList.add(caseObj);
			}
		}

		if(oldCaseList.size() > 0){
			Boolean isDelete = true;
			updateIsClaimedFlag(oldCaseList, isDelete);
		}
	}

	/**
	 * update IsClaimed flag
	 */
	public static void updateIsClaimedFlag(List<Case> caseList, Boolean isDelete){
		Set<Id> updateAccIdSetForTrue = new Set<Id>();
		Set<Id> updateContactIdSetForTrue = new Set<Id>();
		Set<Id> accountIdSetForCheck = new Set<Id>();
		Set<Id> contactIdSetForCheck = new Set<Id>();

		//現在SRの苦情フラグを確認
		//Insert/Updateの場合
		if(!isDelete){
			for(Case caseObj : caseList){
				//顧客は個人顧客の場合
				if(caseObj.CMN_ContactName__c != null){
					if(caseObj.CMN_IsClaimed__c){
						updateContactIdSetForTrue.add(caseObj.CMN_ContactName__c);
					}else{
						contactIdSetForCheck.add(caseObj.CMN_ContactName__c);
					}
				}
				//顧客は法人顧客の場合
				else if(caseObj.CMN_CorporateName__c != null){
					if(caseObj.CMN_IsClaimed__c){
						updateAccIdSetForTrue.add(caseObj.CMN_CorporateName__c);
					}else{
						accountIdSetForCheck.add(caseObj.CMN_CorporateName__c);
					}
				}
			}
		}
		//Deleteの場合
		else{
			for(Case caseObj : caseList){
				//顧客は個人顧客の場合
				if(caseObj.CMN_ContactName__c != null){
					contactIdSetForCheck.add(caseObj.CMN_ContactName__c);
				}
				//顧客は法人顧客の場合
				else if(caseObj.CMN_CorporateName__c != null){
					accountIdSetForCheck.add(caseObj.CMN_CorporateName__c);
				}
			}
		}

		//現在SRの苦情フラグはFlaseの場合、対象顧客に紐づくSRの苦情フラグを確認
		List<Case> relatedCaseList = getCaseListByAccountContactIdList(accountIdSetForCheck, contactIdSetForCheck);
		if(relatedCaseList.size() > 0){
			for(Case relatedcase : relatedCaseList){
				if(relatedcase.CMN_ContactName__c != null){
					updateContactIdSetForTrue.add(relatedcase.CMN_ContactName__c);
					contactIdSetForCheck.remove(relatedcase.CMN_ContactName__c);
				}else if(relatedcase.CMN_CorporateName__c != null){
					updateAccIdSetForTrue.add(relatedcase.CMN_CorporateName__c);
					accountIdSetForCheck.remove(relatedcase.CMN_CorporateName__c);
				}
			}
		}

		//Accountの苦情フラグを更新
		Set<Id> accountIdSetForSearch = new Set<Id>();
		accountIdSetForSearch.addAll(updateAccIdSetForTrue);
		accountIdSetForSearch.addAll(accountIdSetForCheck);
		if(accountIdSetForSearch.size() > 0){
			List<Account> accountList = getAccountListByIdList(accountIdSetForSearch);
			if(accountList!= null && accountList.size() > 0){
				List<Account> updateAccountList = new List<Account>();

				for(Account accountObj : accountList){
					if(updateAccIdSetForTrue.contains(accountObj.Id) && accountObj.CMN_IsClaimed__c == false){
						accountObj.CMN_IsClaimed__c = true;
						updateAccountList.add(accountObj);
					}else if(!updateAccIdSetForTrue.contains(accountObj.Id) && accountObj.CMN_IsClaimed__c == true){
						accountObj.CMN_IsClaimed__c = false;
						updateAccountList.add(accountObj);
					}
				}

				if(updateAccountList != null && updateAccountList.size() > 0){
					Update updateAccountList;
				}

			}
		}

		//Contact(個人と法人)の苦情フラグを更新
		Set<Id> contactIdSetForSearch = new Set<Id>();
		contactIdSetForSearch.addAll(updateContactIdSetForTrue);
		contactIdSetForSearch.addAll(contactIdSetForCheck);
		if(contactIdSetForSearch.size() > 0 || accountIdSetForSearch.size() > 0){
			List<Contact> contactList = getContactListByIdandAccountIdList(contactIdSetForSearch, accountIdSetForSearch);
			if(contactList != null && contactList.size() > 0){
				List<Contact> updateContactList = new List<Contact>();

				for(Contact contactObj : contactList){
					//Trueに更新する個人
					if(updateContactIdSetForTrue.contains(contactObj.Id) && contactObj.CMN_IsClaimed__c == false){
						contactObj.CMN_IsClaimed__c = true;
						updateContactList.add(contactObj);
					}
					//Trueに更新するAccountに紐付く法人
					else if(updateAccIdSetForTrue.contains(contactObj.AccountId) && contactObj.CMN_IsClaimed__c == false){
						contactObj.CMN_IsClaimed__c = true;
						updateContactList.add(contactObj);
					}
					//それ以外のFalseに更新する個人/法人
					else if(!updateContactIdSetForTrue.contains(contactObj.Id)
									&& !updateAccIdSetForTrue.contains(contactObj.AccountId)
									&& contactObj.CMN_IsClaimed__c == true){
						contactObj.CMN_IsClaimed__c = false;
						updateContactList.add(contactObj);
					}
				}

				if(updateContactList != null && updateContactList.size() > 0){
					Update updateContactList;
				}
			}
		}
	}

	/**
	 * Object: Case
	 * Parameter: list of CMN_CorporateName__c and CMN_ContactName__c
	 * return: List<Case>
	 */
	public static List<Case> getCaseListByAccountContactIdList(Set<Id> accountIdSet, Set<Id> contactIdSet){
		List<Case> caseList = [
							SELECT Id, CMN_CorporateName__c, CMN_ContactName__c
							  FROM Case
							 WHERE (CMN_CorporateName__c IN: accountIdSet
								OR CMN_ContactName__c IN: contactIdSet)
							   AND CC_SRTypeId__c != null
							   AND CMN_IsClaimed__c = true
							   AND RecordType.Name = 'サービスリクエスト'];

		return caseList;
	}

	/**
	 * Object: Account
	 * Parameter: list of Id
	 * return: List<Account>
	 */
	public static List<Account> getAccountListByIdList(Set<Id> IdSet){
		List<Account> accountList = [
							SELECT Id, Name, CMN_IsClaimed__c, E_CL2PF_ZAGCYNUM__c, IsMotherOffice__c, E_IsAgency__c, ZubsMain__c,
									Parent.E_CL1PF_ZHEADAY__c
							  FROM Account
							 WHERE Id IN: IdSet];

		return accountList;
	}

	/**
	 * Object: Contact
	 * Parameter: list of Id and AccountId
	 * return: List<Contact>
	 */
	public static List<Contact> getContactListByIdandAccountIdList(Set<Id> IdSet, Set<Id> accountIdSet){
		List<Contact> contactList = [
							SELECT Id, CMN_IsClaimed__c, AccountId, CMN_isAddressUnknown__c
							  FROM Contact
							 WHERE Id IN: IdSet
								OR (AccountId IN: accountIdSet
							   AND E_CLTPF_CLTTYPE__c = 'C')];

		return contactList;
	}

}