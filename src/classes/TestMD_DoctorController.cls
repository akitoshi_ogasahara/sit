/**
 * 
 */
@isTest
private class TestMD_DoctorController {
	
	/**
	 * List<SelectOption> getSearchTypeItems()
	 * 正常
	 */
	static testMethod void getSearchTypeItemsTest01() {

		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();

		Test.stopTest();

		// assert
		System.assertEquals(2, con.getSearchTypeItems().size());
		System.assertEquals(MD_Const.MD_LABEL_CONDITION_HOSPTALNAME, con.getSearchTypeItems()[0].getLabel());
		System.assertEquals(MD_Const.DOCTOR_COL_API_SEARCHHOSPITALNAME, con.getSearchTypeItems()[0].getValue());
		System.assertEquals(MD_Const.MD_LABEL_CONDITION_NAME, con.getSearchTypeItems()[1].getLabel());
		System.assertEquals(MD_Const.DOCTOR_COL_API_SEARCHNAME, con.getSearchTypeItems()[1].getValue());
	}

	/**
	 * Constructor
	 * 正常
	 */
	static testMethod void constructorTest01() {
		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();

		Test.stopTest();

		// assert
		System.assertEquals(false, con.isSearched);
		System.assertEquals(MD_Const.DOCTOR_COL_API_SEARCHHOSPITALNAME, con.searchType);
		System.assertEquals(0, con.doctorDtoList.size());
	}

	/**
	 * void init()
	 * 正常
	 */
	static testMethod void initTest01() {
		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();

		Test.stopTest();

		// [IRIS] ログをコントローラーで取得しないように変更したため削除
		// assert
		//List<E_Log__c> logList = [Select Id From E_Log__c];
		//System.assertEquals(1, logList.size());
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]嘱託医名　空白
	 */
	static testMethod void doSearchTest01() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('嘱託医　テスト1', '病院名　ビョウイン1', '50'));
		docList.add(createDoctor('嘱託医　TEST2', '病院名　HOSPITAL2', '40'));
		docList.add(createDoctor('嘱託医　テスト3', '病院名　ビョウイン3', '30'));
		docList.add(createDoctor('嘱託医　TEST4', '病院名　HOSPITAL4', '20'));
		docList.add(createDoctor('嘱託医　テスト5', '病院名　ビョウイン5', '10'));
		insert docList;

		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();
		con.searchType = con.getSearchTypeItems()[1].getValue();
		con.doSearch();

		Test.stopTest();

		// assert
		System.assertEquals(0, con.doctorDtoList.size());
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]嘱託医名　空白以外
	 */
	static testMethod void doSearchTest02() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('嘱託医　テスト1', '病院名　ビョウイン1', '50'));
		docList.add(createDoctor('嘱託医　TEST2', '病院名　HOSPITAL2', '40'));
		docList.add(createDoctor('嘱託医　テスト3', '病院名　ビョウイン3', '30'));
		docList.add(createDoctor('嘱託医　TEST4', '病院名　HOSPITAL4', '20'));
		docList.add(createDoctor('嘱託医　ﾃｽﾄ5', '病院名　ﾋﾞｮｳｲﾝ5', '10'));
		insert docList;
		
		system.debug([select searchHospitalName__c, searchName__c From MD_Doctor__c]);

		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();
		con.searchType = con.getSearchTypeItems()[1].getValue();
		con.searchText = 'テスト';
		con.doSearch();

		Test.stopTest();

		// assert
		System.assertEquals(true, con.isSearched);
		System.assertEquals(3, con.doctorDtoList.size());
		System.assertEquals(docList[0].Id, con.doctorDtoList[2].doctor.Id);
		System.assertEquals(docList[2].Id, con.doctorDtoList[1].doctor.Id);
		System.assertEquals(docList[4].Id, con.doctorDtoList[0].doctor.Id);
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]病院名　空白
	 */
	static testMethod void doSearchTest03() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('嘱託医　テスト1', '病院名　ビョウイン1', '50'));
		docList.add(createDoctor('嘱託医　TEST2', '病院名　HOSPITAL2', '40'));
		docList.add(createDoctor('嘱託医　テスト3', '病院名　ビョウイン3', '30'));
		docList.add(createDoctor('嘱託医　TEST4', '病院名　HOSPITAL4', '20'));
		docList.add(createDoctor('嘱託医　ﾃｽﾄ5', '病院名　ﾋﾞｮｳｲﾝ5', '10'));
		insert docList;

		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();
		con.searchType = con.getSearchTypeItems()[0].getValue();
		con.doSearch();

		Test.stopTest();

		// assert
		System.assertEquals(0, con.doctorDtoList.size());
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]病院名　空白以外
	 */
	static testMethod void doSearchTest04() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('嘱託医　テスト1', '病院名　ビョウイン1', '50'));
		docList.add(createDoctor('嘱託医　TEST2', '病院名　HOSPITAL2', '40'));
		docList.add(createDoctor('嘱託医　テスト3', '病院名　ビョウイン3', '30'));
		docList.add(createDoctor('嘱託医　TEST4', '病院名　HOSPITAL4', '20'));
		docList.add(createDoctor('嘱託医　ﾃｽﾄ5', '病院名　ﾋﾞｮｳｲﾝ5', '10'));
		insert docList;

		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();
		con.searchType = con.getSearchTypeItems()[0].getValue();
		con.searchText = 'ビョウイン';
		con.doSearch();

		Test.stopTest();

		// assert
		System.assertEquals(true, con.isSearched);
		System.assertEquals(3, con.doctorDtoList.size());
		System.assertEquals(docList[0].Id, con.doctorDtoList[2].doctor.Id);
		System.assertEquals(docList[2].Id, con.doctorDtoList[1].doctor.Id);
		System.assertEquals(docList[4].Id, con.doctorDtoList[0].doctor.Id);
	}
	
	/**
	 * void doSearch()
	 * 正常
	 * [検索結果]病院名　空白
	 */
	static testMethod void doSearchTest05() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('嘱託医　テスト1', '病院名　ビョウイン1', '50'));
		docList.add(createDoctor('嘱託医　テスト2', '', '40'));
		docList.add(createDoctor('嘱託医　テスト3', '病院名　ビョウイン3', '30'));
		docList.add(createDoctor('嘱託医　テスト4', '', '20'));
		docList.add(createDoctor('嘱託医　ﾃｽﾄ5', '病院名　ﾋﾞｮｳｲﾝ5', '10'));
		insert docList;

		// 病院から検索
		PageReference pr = Page.E_MDDoctor;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_DoctorController con = new MD_DoctorController();
		con.init();
		con.searchType = con.getSearchTypeItems()[1].getValue();
		con.searchText = 'テスト';
		con.doSearch();

		Test.stopTest();

		// assert
		System.assertEquals(3, con.doctorDtoList.size());
	}

	/* Test Data　位置情報固定 */
	private static MD_Doctor__c createDoctor(String name, String hosName, String scode) {
		MD_Doctor__c doc = new MD_Doctor__c();
		doc.Name = name;
		doc.HospitalName__c = hosName;
		doc.StateCode__c = scode;
		
		doc.SearchName__c = E_Util.emToEn(doc.Name);
		doc.SearchHospitalName__c = E_Util.emToEn(doc.HospitalName__c);
		
		return doc;
	}
}