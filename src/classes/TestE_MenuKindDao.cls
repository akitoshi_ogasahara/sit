@isTest
public with sharing class TestE_MenuKindDao {
	/**
     * E_SVFPFDao 分岐網羅
     */
    private static testMethod void testE_MenuKindDao() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        E_MenuKind__c m = CreateMenuKind(true, 'テストメニュ種類' );
        Account acc = createAcc(true, 'dummyAccount',m.id);
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            //テスト開始
            Test.startTest();
            Id dummyId = acc.id;
            
            //メソッドがなくなりました。System.assert(E_MenuKindDao.getMenuKndByAccountId(dummyId) != null);
            System.assert(E_MenuKindDao.getMenuKnds().size()!= 0);

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
    
     /**
     * 取引先作成
     * @param isInsert: whether to insert
     * @param accName: 取引先名
     * @return Account: 取引先
     */
    private static Account createAcc(Boolean isInsert, String accName, Id MenuKindId){
        Account src = new Account(
                  name = accName
                 ,E_MenuKind__c = MenuKindId
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    
    /**
     * メニュー種類作成
     * @param isInsert: whether to insert
     * @param accName: 取引先名
     * @return Account: 取引先
     */
    private static E_MenuKind__c CreateMenuKind(Boolean isInsert, String Mname){
        E_MenuKind__c src = new E_MenuKind__c(
                  name = Mname
                 ,ExternalId__c = 'TestMK_Agency'
                  
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }

}