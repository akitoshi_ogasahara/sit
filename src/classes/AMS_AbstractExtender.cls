/**
 *    AgencyManagementSystem Project  superクラス
 */
public abstract class AMS_AbstractExtender extends E_AbstractExtender{

    public class AMS_SVEException extends Exception{}

    //初期処理チェック
    @TestVisible
    protected Boolean isSuccessOfInitValidate{get;set;}
    public Boolean getIsSuccessInit(){
        return isSuccessOfInitValidate;
    }

    /**
     * Constructor
     */
    public AMS_AbstractExtender() {
        access = E_AccessController.getInstance();
        isSuccessOfInitValidate = false;
    }
    
    /**
     * ヘルプページURL
     */
    @TestVisible
    protected String helpMenukey{get;set;}
    @TestVisible
    protected String helpPageJumpTo{get;set;}
    public String getUrlforHelp(){
        String url = 'E_Info?';
        url += 'menu='+ helpMenukey;
        if(String.isNotBlank(helpPageJumpTo)){
            url += '#' + helpPageJumpTo;
        }
        return url;
    }
    
    /**
     * ヘルプページURL(各種資料ダウンロード)
     */
    @TestVisible
    protected String guideMenukey{get;set;}
    @TestVisible
    protected String guidePageJumpTo{get;set;}
    public String getUrlforGuide(){
        String url = 'E_Info?';
        url += 'menu='+ guideMenukey;
        if(String.isNotBlank(guidePageJumpTo)){
            url += '#' + guidePageJumpTo;
        }
        return url;
    }
    
    /**
     * 代理店事務所コード
     */
    @TestVisible
    protected String viewingOfficeCode{
    	get{
    		if(String.isBlank(viewingOfficeCode)){
    			return access.getAccessKindFromThree();
    		}
    		return viewingOfficeCode;
    	}
    	set;
    }
    
    /**
     * 代理店事務所検索（自主点検）
     */
    public String getUrlforScSearch(){
    	/* [TODO] 画面遷移、アクセス権周りが決まり次第、制御処理を追加
        //社員以外または外部委託の参照権限を持っていない場合Null
        if(getHasOutsrcViewPermission()==false||access.isEmployee()==false){
            return null;
        }
        */
                
        // 社員ユーザ
        if(access.isEmployee() == true){
        	return Page.E_SCSearch.getUrl();
        	
        // 代理店ユーザ
        }else if(access.isAgent() == true){
        	return Page.E_SCSearch_ag.getUrl();
        	
        }else{
        	return null;
        }
    }
}