@isTest
private class TestI_KeywordMarker {

	private static testMethod void test() {
		I_KeywordMarker marker = new I_KeywordMarker();
		marker.addKeyword('keyword1');
		marker.addKeyword('keyword2');
		System.assertEquals(false, marker.wholeMatches(new List<String>{'keyword1 is test literal'}));
		System.assertEquals(false, marker.wholeMatches(new List<String>{'keyword2 is test literal'}));
		System.assertEquals(true, marker.wholeMatches(new List<String>{'keyword1 is test literal', 'keyword2 is test literal!'}));
		System.assertEquals('<mark>keyword1</mark> is test literal!', marker.mark('keyword1 is test literal!'));
		System.assertEquals('<mark>keyword1</mark> is tes...', marker.mark('keyword1 is test literal!', 15));

		marker = new I_KeywordMarker();
		System.assertEquals(false, marker.wholeMatches(new List<String>()));
		marker.addKeyword('test');
		System.assertEquals('...is <mark>test</mark> li...', marker.mark('keyword1 is test literal!', 10));

		marker = new I_KeywordMarker();
		marker.addKeyword('ral!');
		System.assertEquals('...t lite<mark>ral!</mark>', marker.mark('keyword1 is test literal!', 10));

		marker = new I_KeywordMarker();
		marker.addKeyword(' is ');
		marker.addKeyword('test');
		System.assertEquals('keyword1<mark> is </mark><mark>test</mark> literal!', marker.mark('keyword1 is test literal!'));
		System.assertEquals('<mark>... i...</mark>', marker.mark('keyword1 is test literal!', 2));

		marker = new I_KeywordMarker();
		marker.addKeyword('nomatch');
		System.assertEquals('keyword1 is test literal!', marker.mark('keyword1 is test literal!'));
		System.assertEquals('keyword1 i...', marker.mark('keyword1 is test literal!', 10));

		System.assertEquals('(?:A|a|Ａ|ａ)(?:B|b|Ｂ|ｂ)(?:C|c|Ｃ|ｃ)(?:D|d|Ｄ|ｄ)(?:E|e|Ｅ|ｅ)', I_KeywordMarker.createFuzzyPattern('abcde'));
		System.assertEquals('(?:A|a|Ａ|ａ)(?:B|b|Ｂ|ｂ)(?:C|c|Ｃ|ｃ)(?:D|d|Ｄ|ｄ)(?:E|e|Ｅ|ｅ)', I_KeywordMarker.createFuzzyPattern('ABCDE'));
		System.assertEquals('(?:A|a|Ａ|ａ)(?:B|b|Ｂ|ｂ)(?:C|c|Ｃ|ｃ)(?:D|d|Ｄ|ｄ)(?:E|e|Ｅ|ｅ)', I_KeywordMarker.createFuzzyPattern('ａｂｃｄｅ'));
		System.assertEquals('(?:A|a|Ａ|ａ)(?:B|b|Ｂ|ｂ)(?:C|c|Ｃ|ｃ)(?:D|d|Ｄ|ｄ)(?:E|e|Ｅ|ｅ)', I_KeywordMarker.createFuzzyPattern('ＡＢＣＤＥ'));
		System.assertEquals('あいうえお', I_KeywordMarker.createFuzzyPattern('あいうえお'));
		System.assertEquals('(?:ア|ｱ)(?:イ|ｲ)(?:ウ|ｳ)(?:エ|ｴ)(?:オ|ｵ)', I_KeywordMarker.createFuzzyPattern('アイウエオ'));
		System.assertEquals('(?:ア|ｱ)(?:イ|ｲ)(?:ウ|ｳ)(?:エ|ｴ)(?:オ|ｵ)', I_KeywordMarker.createFuzzyPattern('ｱｲｳｴｵ'));
		System.assertEquals('(?:1|１)(?:2|２)(?:3|３)(?:4|４)(?:5|５)(?:6|６)(?:7|７)(?:8|８)(?:9|９)(?:0|０)', I_KeywordMarker.createFuzzyPattern('１２３４５６７８９０'));
		System.assertEquals('(?:1|１)(?:2|２)(?:3|３)(?:4|４)(?:5|５)(?:6|６)(?:7|７)(?:8|８)(?:9|９)(?:0|０)', I_KeywordMarker.createFuzzyPattern('1234567890'));
		System.assertEquals(
			'(?:!|！)(?:#|＃)(?:\\$|＄)(?:%|％)(?:&|＆)(?:\\(|（)(?:\\)|）)(?:\\*|＊)(?:\\+|＋)(?:,|，)(?:\\-|－)(?:\\.|．)(?:/|／)(?::|：)(?:;|；)(?:<|＜)(?:=|＝)(?:>|＞)(?:\\?|？)(?:@|＠)(?:\\[|［)(?:\\]|］)(?:\\^|＾)(?:_|＿)(?:\\{|｛)(?:\\||｜)(?:\\}|｝)(?:~|～)(?:。|｡)(?:「|｢)(?:」|｣)(?:、|､)(?:`|｀)(?: |　)(?:"|”)(?:・|･)(?:\'|’)(?:\\\\|￥)',
			I_KeywordMarker.createFuzzyPattern('!#$%&()*+,-./:;<=>?@[]^_{|}~｡｢｣､` "･\'\\'));

		System.assertEquals(
			'(?:!|！)(?:#|＃)(?:\\$|＄)(?:%|％)(?:&|＆)(?:\\(|（)(?:\\)|）)(?:\\*|＊)(?:\\+|＋)(?:,|，)(?:\\-|－)(?:\\.|．)(?:/|／)(?::|：)(?:;|；)(?:<|＜)(?:=|＝)(?:>|＞)(?:\\?|？)(?:@|＠)(?:\\[|［)(?:\\]|］)(?:\\^|＾)(?:_|＿)(?:\\{|｛)(?:\\||｜)(?:\\}|｝)(?:~|～)(?:。|｡)(?:「|｢)(?:」|｣)(?:、|､)(?:`|｀)(?: |　)(?:"|”)(?:・|･)(?:\'|’)(?:\\\\|￥)',
			I_KeywordMarker.createFuzzyPattern('！＃＄％＆（）＊＋，－．／：；＜＝＞？＠［］＾＿｛｜｝～。「」、｀　”・’￥'));
		System.assertEquals('(?:A|a|Ａ|ａ)(?:B|b|Ｂ|ｂ)(?:C|c|Ｃ|ｃ)(?:D|d|Ｄ|ｄ)あ(?:イ|ｲ)(?:ウ|ｳ)(?:1|１)(?:2|２)(?:!|！)(?:#|＃)', I_KeywordMarker.createFuzzyPattern('aBｃＤあイｳ1２!＃'));
	}

}