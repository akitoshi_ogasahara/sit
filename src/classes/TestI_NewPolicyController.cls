@isTest
private class TestI_NewPolicyController {
	
	/**
	 * constructor
	 * 正常系
	 */
	private static testMethod void constructorTest_001() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newPlcy = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		Test.stopTest();
		system.assert(!controller.pageMessages.hasMessages());
	}
	/**
	 * constructor
	 * 正常系
	 * 1000件以上のRowCount指定
	 */
/* TODO 事前リリースのためにコメントアウト
	private static testMethod void constructorTest_002() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newPlcy = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		Test.startTest();
		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(Integer.valueOf(I_Const.LIST_MAX_ROWS)+1));
		Test.setCurrentPage(pageRef);
		I_NewPolicyController controller = new I_NewPolicyController();
		Test.stopTest();
		system.assert(!controller.pageMessages.hasMessages());
	}
*/
	/**
	 * pageAction
	 * 正常系
	 * データ取得件数0件
	 */
	private static testMethod void pageActionTest_001() {
		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();
		system.assert(controller.pageMessages.getHasWarningMessages());
		Test.stopTest();
	}
	/**
	 * pageAction
	 * 正常系
	 * フィルターモード
	 */
	private static testMethod void pageActionTest_002() {
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		E_IRISHandler iris = E_IRISHandler.getInstance();
		iris.setPolicyCondition('agency', '代理店名', distribute.id, 'breadCrumb');
		controller.pageAction();
		iris.setPolicyCondition('office', '事務所名', office.id, 'breadCrumb');
		controller.pageAction();
		iris.setPolicyCondition('agent', '募集人名', agent.id, 'breadCrumb');
		controller.pageAction();
		Test.stopTest();
	}
	/**
	 * pageAction
	 * 正常系
	 * データ取得件数超過
	 */
	private static testMethod void pageActionTest_003() {
		Datetime d = system.now();
		Datetime beforeDate = d.addDays(-1);
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		List<E_NewPolicy__c> newPolicys = new List<E_NewPolicy__c>();
		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		Integer limitRows = controller.getListMaxRows();
		for(Integer i = 0;i<limitRows;i++){
			newPolicys.add(TestUtil_AMS.createNewPolicy(false,'P','TEST'+String.valueOf(i),d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id));
		}
		insert newPolicys;
		Integer totalRows = controller.getTotalRows();
		system.assertEquals(totalRows,0);
		//I_Const.LIST_MAX_ROWS_AGGREGATE 件
		controller.pageAction();
		totalRows = controller.getTotalRows();
		system.assertEquals(totalRows,limitRows);

		//I_Const.LIST_MAX_ROWS_AGGREGATE + 1 + 1 件
		TestUtil_AMS.createNewPolicy(true,'P','ADD001',beforeDate,'Y','1111',200001,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id);
		controller = new I_NewPolicyController();
		controller.pageAction();
		totalRows = controller.getTotalRows();
		system.assertEquals(totalRows,limitRows);
		Test.stopTest();
	}
	/**
	 * pageAction
	 * 正常系
	 * 表示対象外データ
	 */
	private static testMethod void pageActionTest_004() {
		Datetime d = system.now();
		Datetime excludedDate = d.addMonths(-2);
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		//非表示
		E_NewPolicy__c newpolicy1_1 = 
			TestUtil_AMS.createNewPolicy(true,'I','D1001',excludedDate,'Y','1111',200001,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy1_2 = 
			TestUtil_AMS.createNewPolicy(true,'Z','D1002',excludedDate,'','',0,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id);
		//非表示
		E_NewPolicy__c newpolicy2 = 
			TestUtil_AMS.createNewPolicy(true,'Q','K2001',excludedDate,'Y','1111',200001,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id);
		//表示対象
		E_NewPolicy__c newpolicy3_1 = 
			TestUtil_AMS.createNewPolicy(true,'Q','D3001',d,'Y','1111',200001,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3_2 = 
			TestUtil_AMS.createNewPolicy(true,'P','D3002',excludedDate,'Y','1111',200001,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id);

		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();
		system.assertEquals(controller.getTotalRows(),1);

		Test.stopTest();
	}
	/**
	 * pageAction
	 * 正常系
	 * 表示対象データ上限
	 */
	private static testMethod void pageActionTest_005() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');

		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		Integer maxRows = controller.getListMaxRows();
		List<E_NewPolicy__c> plcys = new List<E_NewPolicy__c>();
		for(Integer i = 0;i < maxRows+1;i++){
			plcys.add(TestUtil_AMS.createNewPolicy(false,'P','T' + String.valueOf(i),d,'','',200001,'契約者名ADD1','ケイヤクシャメイADD1','被保険者名ADD1','ヒホケンシャメイADD1',distribute.id,office.id,agent.id));
		}
		insert plcys;
		controller.pageAction();
		system.assertEquals(controller.getTotalRows(),maxRows);

		Test.stopTest();
	}
	/**
	 * addRows
	 * 正常系
	 * 各データでソート
	 */
	private static testMethod void addRowsTest_001() {
		
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.rowCount = I_Const.LIST_MAX_ROWS - I_Const.LIST_ADD_MORE_ADD_ROWS - 1;
		controller.addRows();
		system.assertEquals(controller.rowCount,I_Const.LIST_MAX_ROWS - 1);

		controller.rowCount = I_Const.LIST_MAX_ROWS - I_Const.LIST_ADD_MORE_ADD_ROWS;
		controller.addRows();
		system.assertEquals(controller.rowCount,I_Const.LIST_MAX_ROWS);

		controller.rowCount = I_Const.LIST_MAX_ROWS - I_Const.LIST_ADD_MORE_ADD_ROWS + 1;
		controller.addRows();
		system.assertEquals(controller.rowCount,I_Const.LIST_MAX_ROWS);

		Test.stopTest();
	}
	/**
	 * sortRows
	 * 正常系
	 * 各データでソート
	 */
 //TODO 事前リリースのためにコメントアウト
	private static testMethod void sortRowsTest_001() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);
		
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'owner');
		Test.setCurrentPage(pageRef);
		//契約者名昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).CHDRNUM,newpolicy1.CHDRNUM__c);
		//契約者名降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).CHDRNUM,newpolicy2.CHDRNUM__c);

		Test.stopTest();


	}

	private static testMethod void sortRowsTest_002() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'I','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);
		
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'status');
		Test.setCurrentPage(pageRef);
		//ステータス昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).status,I_NewPolicyConst.STATUS_NAME_P1);
		//ステータス降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).status,I_NewPolicyConst.STATUS_NAME_I);

		Test.stopTest();
	}

	private static testMethod void sortRowsTest_003() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);
		
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'GRUPKEY');
		Test.setCurrentPage(pageRef);
		//団体番号昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).GRUPKEY,null);
		//団体番号降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).GRUPKEY,null);

		Test.stopTest();
	}

	private static testMethod void sortRowsTest_004() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);
		
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'CHDRNUM');
		Test.setCurrentPage(pageRef);
		//証券番号昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).CHDRNUM,newpolicy1.CHDRNUM__c);
		//証券番号降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).CHDRNUM,newpolicy3.CHDRNUM__c);

		Test.stopTest();
	}

	private static testMethod void sortRowsTest_005() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		newpolicy1.InsuranceType__c = 'テスト1';
		update newpolicy1;
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		newpolicy2.InsuranceType__c = 'テスト2';
		update newpolicy2;
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);
		newpolicy3.InsuranceType__c = 'テスト3';
		update newpolicy3;

		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'cntType');
		Test.setCurrentPage(pageRef);
		//保険種類昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).cntType,newpolicy1.InsuranceType__c);
		//保険種類降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).cntType,newpolicy3.InsuranceType__c);

		Test.stopTest();
	}

	private static testMethod void sortRowsTest_006() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		newpolicy1.INSTTOT01__c = 100;
		update newpolicy1;
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		newpolicy2.INSTTOT01__c = 1000;
		update newpolicy2;
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);
		newpolicy3.INSTTOT01__c = 10000;
		update newpolicy3;
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'insttot01');
		Test.setCurrentPage(pageRef);
		//保険料昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).insttot01,newpolicy1.INSTTOT01__c);
		//保険料降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).insttot01,newpolicy3.INSTTOT01__c);

		Test.stopTest();
	}

	private static testMethod void sortRowsTest_007() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);

		newpolicy1 = [select id,AgentAccountName__c from E_NewPolicy__c where CHDRNUM__c = 'TEST001'];
		newpolicy2 = [select id,AgentAccountName__c from E_NewPolicy__c where CHDRNUM__c = 'TEST002'];
		
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'office');
		Test.setCurrentPage(pageRef);
		//契約者名昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).office,newpolicy1.AgentAccountName__c);
		//契約者名降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).office,newpolicy2.AgentAccountName__c);

		Test.stopTest();
	}

	private static testMethod void sortRowsTest_008() {
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c newpolicy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイイチ','被保険者名1','ヒホケンシャメイイチ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy2 = TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200001,'契約者名2','ケイヤクシャメイニ','被保険者名2','ヒホケンシャメイニ',distribute.id,office.id,agent.id);
		E_NewPolicy__c newpolicy3 = TestUtil_AMS.createNewPolicy(true,'P','TEST003',d,'','',200001,'契約者名3','ケイヤクシャメイサン','被保険者名3','ヒホケンシャメイサン',distribute.id,office.id,agent.id);


		newpolicy1 = [select id,Agent_Name__c from E_NewPolicy__c where CHDRNUM__c = 'TEST001'];
		newpolicy2 = [select id,Agent_Name__c from E_NewPolicy__c where CHDRNUM__c = 'TEST002'];
		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.pageAction();

		PageReference pageRef = Page.IRIS_NewPolicy;
		//パラメータをセット
		pageRef.getParameters().put('st', 'agent');
		Test.setCurrentPage(pageRef);
		//契約者名昇順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).agent,newpolicy1.Agent_Name__c);
		//契約者名降順
		controller.sortRows();
		system.assertEquals(controller.newPolicys.get(0).agent,newpolicy2.Agent_Name__c);

		Test.stopTest();
	}

	/**
	 * getSyncDate
	 * 正常系
	 * 最後の連携日時を取得
	 */
	private static testMethod void getSyncDateTest_001() {
		TestUtil_AMS.createEbizLog(true,'E');

		Test.startTest();

		I_NewPolicyController controller = new I_NewPolicyController();
		controller.getSyncDate();

		Test.stopTest();
	}
	/**
	 * getStatus
	 * 正常系
	 * 団体ステータス判定
	 */
	private static testMethod void getStatusTest_001() {
		Set<String> status = new Set<String>();
		Test.startTest();
		//取下げ
		status.add(I_NewPolicyConst.STATUS_NAME_Z);
		String result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_Z);

		//謝絶
		status = new Set<String>();
		status.add(I_NewPolicyConst.STATUS_NAME_Q);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_Q);

		//謝絶/取下げ
		status = new Set<String>();
		status.add(I_NewPolicyConst.STATUS_NAME_Q);
		status.add(I_NewPolicyConst.STATUS_NAME_Z);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_QZ);

		//申込書類到着済
		status = new Set<String>();
		status.add(I_NewPolicyConst.STATUS_NAME_P1);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_P1);

		//査定中
		status = new Set<String>();
		status.add(I_NewPolicyConst.STATUS_NAME_P1);
		status.add(I_NewPolicyConst.STATUS_NAME_Z);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_P4);

		//不備対応中
		status = new Set<String>();
		status.add(I_NewPolicyConst.STATUS_NAME_P2);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_P2);

		//査定完了入金待ち
		status.add(I_NewPolicyConst.STATUS_NAME_P3);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_P3);

		//成立
		status.add(I_NewPolicyConst.STATUS_NAME_I);
		result = I_NewPolicyController.getStatus(status);
		system.assertEquals(result,I_NewPolicyConst.STATUS_NAME_I);

		Test.stopTest();
	}

	/**
	 * フィルターモード
	 */
	private static testMethod void filterModeTest() {
		Datetime d = system.now();
		//格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//無効で5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		//新契約作成
		E_NewPolicy__c nPolicy = TestUtil_AMS.createNewPolicy(true,'P','TEST',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agentA.id);


		//=====================テスト開始=====================
		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		//インスタンス生成
		E_IRISHandler iris = E_IRISHandler.getInstance();

		//募集人Bで参照
		iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
		controller.pageAction();
		Integer totalRows = controller.getTotalRows();
		system.assertEquals(1,totalRows);
		//=====================テスト終了=====================
		Test.stopTest();
	}

	/**
	 * フィルターモード
	 * 別事務所の場合
	 */
	private static testMethod void filterModeTest2() {
		Datetime d = system.now();
		//格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所1作成
		Account office = TestI_TestUtil.createAccount(true,distribute);
		//事務所2作成
		Account office2 = TestI_TestUtil.createAccount(true,distribute);


		//無効で5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office2.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		//新契約作成
		E_NewPolicy__c nPolicy = TestUtil_AMS.createNewPolicy(true,'P','TEST',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agentA.id);


		//=====================テスト開始=====================
		Test.startTest();
		I_NewPolicyController controller = new I_NewPolicyController();
		//インスタンス生成
		E_IRISHandler iris = E_IRISHandler.getInstance();

		//募集人Bで参照
		iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
		controller.pageAction();
		Integer totalRows = controller.getTotalRows();
		system.assertEquals(1,totalRows);
		//=====================テスト終了=====================
		Test.stopTest();
	}
}