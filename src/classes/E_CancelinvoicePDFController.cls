global with sharing class E_CancelinvoicePDFController extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_CancelinvoicePDFExtender getExtender() {return (E_CancelinvoicePDFExtender)extender;}
    
    
    public E_CancelinvoicePDFController(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('dateString','{!record.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2928');
        tmpPropMap.put('Component__Name','EDateLabel');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component2928',tmpPropMap);


        SObjectField f;

        f = E_Policy__c.fields.COMM_CHDRNUM__c;
        f = E_Policy__c.fields.COMM_ZRSTDESC__c;
        f = E_Policy__c.fields.COMM_ZCOVRNAM__c;
        f = E_Policy__c.fields.SPVA_ZREFAMT__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = E_Policy__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('E_Policy__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
            mainQuery.addFieldAsOutput('COMM_ZRSTDESC__c');
            mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
            mainQuery.addFieldAsOutput('SPVA_ZREFAMT__c');
            mainQuery.addFieldAsOutput('DCOLI_ZCSBNAMT__c');
            mainQuery.addFieldAsOutput('COLI_ZCSHVAL__c');
            mainQuery.addFieldAsOutput('COLI_ZCVDCF__c');
            mainQuery.addFieldAsOutput('COLI_ZPLCAPT__c');
            mainQuery.addFieldAsOutput('COLI_ZEPLPRN__c');
            mainQuery.addFieldAsOutput('COLI_ZEPLTOT__c');
            mainQuery.addFieldAsOutput('COMM_ZPLDCF__c');
            mainQuery.addFieldAsOutput('COLI_ZEPLINT__c');
            mainQuery.addFieldAsOutput('COLI_ZPLRATE__c');
            mainQuery.addFieldAsOutput('SPVA_ZPLINTRAT__c');
            mainQuery.addFieldAsOutput('SPVA_ZINTLABEL__c');
            mainQuery.addFieldAsOutput('COMM_OCCDATE__c');
            mainQuery.addFieldAsOutput('COLI_PCESDTE__c');
            mainQuery.addFieldAsOutput('COLI_ZPCDTDCF__c');
            mainQuery.addFieldAsOutput('SPVA_ZDPTYDSC__c');
            mainQuery.addFieldAsOutput('SPVA_ZDPTYDSP__c');
            mainQuery.addFieldAsOutput('SPVA_ZDPTEIST__c');
            mainQuery.addFieldAsOutput('COMM_PTDATE__c');
            mainQuery.addFieldAsOutput('COLI_ZNPTDDCF__c');
            mainQuery.addFieldAsOutput('SPVA_ZFATRATE__c');
            mainQuery.addFieldAsOutput('MainAgent__c');
            mainQuery.addFieldAsOutput('MainAgent__r.Name');
            mainQuery.addFieldAsOutput('MainAgent__r.AccountId');
            mainQuery.addFieldAsOutput('MainAgent__r.Account.E_CL1PF_ZAHKNAME__c');
            mainQuery.addFieldAsOutput('MainAgent__r.Account.E_CL2PF_ZEAYNAM__c');
            mainQuery.addFieldAsOutput('MainAgent__r.Account.Name');
            mainQuery.addFieldAsOutput('MainAgent__r.Account.AgencyName__c');
            mainQuery.addFieldAsOutput('MainAgent__r.Account.Agency__c');
            mainQuery.addFieldAsOutput('SubAgent__c');
            mainQuery.addFieldAsOutput('SubAgent__r.Name');
            mainQuery.addFieldAsOutput('SubAgent__r.LastName');
            mainQuery.addFieldAsOutput('SubAgent__r.AccountId');
            mainQuery.addFieldAsOutput('SubAgent__r.Account.Name');
            mainQuery.addFieldAsOutput('SubAgent__r.Account.AgencyName__c');
            mainQuery.addFieldAsOutput('SubAgent__r.Account.Agency__c');
            mainQuery.addFieldAsOutput('SubAgent__r.FirstName');
            mainQuery.addFieldAsOutput('COLI_ZAPLDCF__c');
            mainQuery.addFieldAsOutput('COMM_CCDATE__c');
            mainQuery.addFieldAsOutput('DCOLI_ZADVDESC__c');
            mainQuery.addFieldAsOutput('COLI_ZADVPDCF__c');
            mainQuery.addFieldAsOutput('COLI_REMNBANN__c');
            mainQuery.addFieldAsOutput('COLI_AVPRMRCV__c');
            mainQuery.addFieldAsOutput('COLI_RCPTDT__c');
            mainQuery.addFieldAsOutput('COMM_ZAGCYNUM01__c');
            mainQuery.addFieldAsOutput('COMM_ZAGCYNUM02__c');
            mainQuery.addFieldAsOutput('COMM_ZAGCYDCF__c');
            mainQuery.addFieldAsOutput('COLI_ZGCLTNM__c');
            mainQuery.addFieldAsOutput('COLI_GRUPNUM__c');
            mainQuery.addFieldAsOutput('COLI_ZGRUPDCF__c');
            mainQuery.addFieldAsOutput('COMM_ZTOTPREM__c');
            mainQuery.addFieldAsOutput('COLI_ZVARFLAG__c');
            mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
            mainQuery.addFieldAsOutput('COMM_CRTABLE2__c');
            mainQuery.addFieldAsOutput('COLI_ZAPLCAPT__c');
            mainQuery.addFieldAsOutput('COLI_ZEAPLPRN__c');
            mainQuery.addFieldAsOutput('COLI_ZEAPLTOT__c');
            mainQuery.addFieldAsOutput('COLI_ZEAPLINT__c');
            mainQuery.addFieldAsOutput('COLI_ZAPLRATE__c');
            mainQuery.addFieldAsOutput('COLI_ZAGNTNUM01__c');
            mainQuery.addFieldAsOutput('SPVA_AGNTNUM01__c');
            mainQuery.addFieldAsOutput('COLI_ZAGNTNUM02__c');
            mainQuery.addFieldAsOutput('SPVA_AGNTNUM02__c');
            mainQuery.addFieldAsOutput('COLI_ZUNPREM__c');
            mainQuery.addFieldAsOutput('COLI_ZUNPCF__c');
            mainQuery.addFieldAsOutput('COLI_ZUNPDCF__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = false;
            p_sidebar = false;
			p_isPdf = true;
			p_pdfPageSize = 'A4';
			p_pdfOrientation = 'portrait';
			p_pdfMargin = '0.5cm';
            addInheritParameter('RecordTypeId', 'RecordType');
            extender = new E_CancelinvoicePDFExtender(this);
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    
    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}