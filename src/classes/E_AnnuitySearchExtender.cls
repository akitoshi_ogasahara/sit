global with sharing class E_AnnuitySearchExtender extends E_AbstractSearchExtender {
    
    private static final String PAGE_TITLE = '契約照会（契約一覧検索）';

    public E_AnnuitySearchController extension;

    public String contactId{get;set;}
    public String disId{get;set;}
    public String offId{get;set;}
    public String ageId{get;set;}

    /** コンストラクタ */
    public E_AnnuitySearchExtender(E_AnnuitySearchController extension) {
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        contactId = ApexPages.CurrentPage().getParameters().get('cId');
        disId = ApexPages.currentPage().getParameters().get('disId');
        offId = ApexPages.currentPage().getParameters().get('offId');
        ageId = ApexPages.currentPage().getParameters().get('ageId');
        pageRef = doAuth(E_Const.ID_KIND.CONTACT, contactId);
        if(pageRef == null){
            SkyEditor2.Query tableQuery1 = extension.queryMap.get('dataTable');
            tableQuery1.addWhereIfNotFirst('AND');
            tableQuery1.addWhere('( Contractor__c = \'' + contactId + '\' OR Annuitant__c  = \'' + contactId + '\')' );
            
            /*
            tableQuery1.addWhere('Contractor__c',contactId,SkyEditor2.WhereOperator.Eq);
            tableQuery1.addWhereIfNotFirst('AND');
            tableQuery1.addWhere(' ( RecordType.DeveloperName = \'ESPHPF\')');
            tableQuery1.addWhereIfNotFirst('AND');
            tableQuery1.addWhere(' ( SpClassification__c like \'%ANNUITY%\' OR SpClassification__c like \'%SPVA&ANNUITY%\' )');
            */
            
            if (getIsEmployee()) {
                if (!String.isBlank(disId)) {
                    tableQuery1.addWhereIfNotFirst('AND');
                    tableQuery1.addWhere('( MainAgentAccount__r.ParentId = \'' + disId + '\' OR SubAgentAccount__r.ParentId = \'' + disId + '\' )' );                
                }
                if (!String.isBlank(offId)) {
                    tableQuery1.addWhereIfNotFirst('AND');
                    tableQuery1.addWhere('( MainAgentAccount__c = \'' + offId + '\' OR SubAgentAccount__c = \'' + offId + '\' )' );
                }
                if (!String.isBlank(ageId)) {
                    tableQuery1.addWhereIfNotFirst('AND');
                    tableQuery1.addWhere('( MainAgent__c = \'' + ageId + '\' OR SubAgent__c = \'' + ageId + '\' )' );
                }
            }
            extension.queryMap.put('dataTable',tableQuery1);
        }
    }

    /** ページアクション */
    public PageReference pageAction(){
        if(pageRef == null){
            extension.doSearch();
        }
        return E_Util.toErrorPage(pageRef, null);
    }

    /** ①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ */
    global override void preSearch() {
        if (extension != null) {
            if (extension.NameKana_val.SkyEditor2__Text__c != null) {
                extension.NameKana_val.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(extension.NameKana_val.SkyEditor2__Text__c))));
            }
        }
    }
}