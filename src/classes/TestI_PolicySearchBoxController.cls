@isTest
private class TestI_PolicySearchBoxController {

	static String contactName = 'テスト募集人Z';
	static String customerNumber = '20161220';
	static String agentNum = 'AGN2016';
	static String mofCode = 'MOF2016';
	static String dayOfBirth = '20001220';

	// getter
	private static testmethod void testgetIsDisplayOwnerList(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.exeSearchType = I_Const.SEARCH_TYPE_OWNER;
		test.stopTest();

		system.assert(controller.getIsDisplayOwnerList());
	}

	// getter
	private static testmethod void testgetIsDisplayInsuredList(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.exeSearchType = I_Const.SEARCH_TYPE_INSURED;
		test.stopTest();

		system.assert(controller.getIsDisplayInsuredList());
	}

	// getter
	private static testmethod void testgetIsDisplayPolicyNoList(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.exeSearchType = I_Const.SEARCH_TYPE_POLICY_NO;
		test.stopTest();

		system.assert(controller.getIsDisplayPolicyNoList());
	}

	// getter
	private static testmethod void testgetIsDisplayCustomerNoList(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.exeSearchType = I_Const.SEARCH_TYPE_CUSTOMER_NO;
		test.stopTest();

		system.assert(controller.getIsDisplayCustomerNoList());
	}

	// getter
	private static testmethod void testgetSearchCategoryLabel(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
		test.stopTest();

		system.assertEquals(controller.getSearchCategoryLabel(), I_Const.SEARCH_LABEL_CATEGORY_POLICY);
	}

	// getter
	private static testmethod void testgetTotalRows(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
		test.stopTest();

		system.assertEquals(controller.getTotalRows(), 0);	
	}

	// getter
	private static testmethod void testgetIsDisplaySearch1(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.exeSearchType = I_Const.SEARCH_TYPE_OWNER;
		test.stopTest();

		system.assert(controller.getIsDisplaySearch());
	}

	// getter
	private static testmethod void testgetIsDisplaySearch2(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.exeSearchType = I_Const.SEARCH_TYPE_INSURED;
		test.stopTest();

		system.assert(controller.getIsDisplaySearch());
	}

	private static testmethod void getSelectOptionsTest() {
		Test.startTest();
		I_PolicySearchBoxController policySearch = new I_PolicySearchBoxController();
		List<SelectOption> options = policySearch.getSearchOptions();
		System.assertEquals(options.size(),2);
		Test.stopTest();
	}

	// 初期化処理
	private static testmethod void testflush(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
		test.stopTest();

		system.assertEquals(controller.searchCategory, I_Const.SEARCH_LABEL_CATEGORY_POLICY);
		system.assertEquals(controller.rowCount, I_Const.LIST_DEFAULT_ROWS);
		system.assertEquals(controller.keyword, '');
		system.assert(!controller.isSearch);
		system.assert(controller.policyRows.isEmpty());
		system.assert(controller.searchMessages.isEmpty());
	}

	// 検索 - キーワード未入力
	private static testmethod void testdoSearchNoneKeyword(){
		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.doSearch();
		test.stopTest();

		system.assert(!controller.isSearch);
	}

	// 検索 - 証券番号
	private static testmethod void testdoSearchPolicyNoColi(){
		E_Policy__c policy = TestUtil_I_SearchBox.createEmptyPolicy(false, null);
		policy.COMM_CHDRNUM__c = '12345678';
		insert policy;

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = '12345678';
			controller.searchType = I_Const.SEARCH_TYPE_POLICY_NO;
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 1);
	}

	/*JIRA_64 失効消滅契約関連
	// 検索 - 証券番号（消滅）
	private static testmethod void testdoSearchPolicyNoDColi(){
		Account acc = createAccount();
		E_Policy__c policy = TestUtil_I_SearchBox.createEmptyPolicy(false, E_Const.POLICY_RECORDTYPE_DCOLI);
		policy.COMM_CHDRNUM__c = '12345678';
		policy.COMM_STATDATE__c =  String.valueOf(Date.Today().addMonths(-1)).replace('-', '');
		policy.MainAgentAccount__c = acc.Id;
		insert policy;

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = '12345678';
			controller.searchType = I_Const.SEARCH_TYPE_POLICY_NO;
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 1);
	}

	// 検索 - 証券番号（失効）
	private static testmethod void testdoSearchLapsedPolicy(){
		Account acc = createAccount();
		E_Policy__c policy = TestUtil_I_SearchBox.createEmptyPolicy(false, null);
		policy.COMM_CHDRNUM__c = '12345678';
		policy.COMM_STATDATE__c =  String.valueOf(Date.Today().addMonths(-1)).replace('-', '');
		policy.MainAgentAccount__c = acc.Id;
		policy.COMM_STATCODE__c = I_Const.LAPS_POLICY_STATCODE;
		policy.COMM_ZRSTDESC__c = I_Const.LAPS_POLICY_ZRSTDESC;
		insert policy;

		createCOV(policy);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = '12345678';
			controller.searchType = I_Const.SEARCH_TYPE_POLICY_NO;
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 1);
	}
	*/

	// 検索 - 顧客番号
	private static testmethod void testdoSearchCustomerNo(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
				I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
				controller.keyword = '67890';
				controller.searchType = I_Const.SEARCH_TYPE_CUSTOMER_NO;
				controller.doSearch();
			test.stopTest();

			system.assertEquals(controller.policyRows.size(), 2);
		}
	}

	// 検索 - 契約者名（社員） and検索
	private static testmethod void testdoSearchOwnerNameEP1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, false);

		Account acc = createAccount();
		Contact con = createContact01(acc);
		E_Policy__c policy = createPolicy(acc, con);


		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'テスト Z';
			controller.searchType = I_Const.SEARCH_TYPE_OWNER;
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 1);
	}

	// 検索 - 契約者名（社員） or検索
	private static testmethod void testdoSearchOwnerNameEP2(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, false);

		Account acc = createAccount();
		Contact con = createContact01(acc);
		E_Policy__c policy = createPolicy(acc, con);


		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'テスト Z';
			controller.searchType = I_Const.SEARCH_TYPE_OWNER;
			controller.selection = 'or';
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 2);
	}

	// 検索 - 被保険者名（社員） and検索
	private static testmethod void testdoSearchInsuredNameEP1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, false);

		Account acc = createAccount();
		Contact con = createContact02(acc);
		E_Policy__c policy = createPolicy(acc, con);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'テスト Z';
			controller.searchType = I_Const.SEARCH_TYPE_INSURED;
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 1);
	}

	// 検索 - 被保険者名（社員） or検索
	private static testmethod void testdoSearchInsuredNameEP2(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, false);

		Account acc = createAccount();
		Contact con = createContact02(acc);
		E_Policy__c policy = createPolicy(acc, con);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'テスト Z';
			controller.searchType = I_Const.SEARCH_TYPE_INSURED;
			controller.selection = 'or';
			controller.doSearch();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 2);
	}

	// 検索 - 契約者名（代理店ユーザ） and検索
	private static testmethod void testdoSearchOwnerNameAG1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		List<E_Policy__c> policylist = TestUtil_I_SearchBox.createPolicys();
		for(E_Policy__c pol : policylist){
			TestUtil_I_SearchBox.createPolicyShare(pol.Id,util.aUser.Id);
		}

		System.runAs(util.aUser){
			test.startTest();
				I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
				controller.keyword = 'テスト契約者 1';
				controller.searchType = I_Const.SEARCH_TYPE_OWNER;
				controller.doSearch();
			test.stopTest();

			system.assertEquals(controller.policyRows.size(), 1);
		}
	}

	// 検索 - 契約者名（代理店ユーザ） or検索
	private static testmethod void testdoSearchOwnerNameAG2(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		List<E_Policy__c> policylist = TestUtil_I_SearchBox.createPolicys();
		for(E_Policy__c pol : policylist){
			TestUtil_I_SearchBox.createPolicyShare(pol.Id,util.aUser.Id);
		}

		System.runAs(util.aUser){
			test.startTest();
				I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
				controller.keyword = 'テスト契約者 1';
				controller.searchType = I_Const.SEARCH_TYPE_OWNER;
				controller.selection = 'or';
				controller.doSearch();
			test.stopTest();

			system.assertEquals(controller.policyRows.size(), 4);
		}
	}

	// 検索 - 被保険者名（代理店ユーザ） and検索
	private static testmethod void testdoSearchInsuredNameAG1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		List<E_Policy__c> policylist = TestUtil_I_SearchBox.createPolicys();
		for(E_Policy__c pol : policylist){
			TestUtil_I_SearchBox.createPolicyShare(pol.Id,util.aUser.Id);
		}

		System.runAs(util.aUser){
			test.startTest();
				I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
				controller.keyword = 'テスト被保険者 1';
				controller.searchType = I_Const.SEARCH_TYPE_INSURED;
				controller.doSearch();
			test.stopTest();

			system.assertEquals(controller.policyRows.size(), 1);
		}
	}

	// 検索 - 被保険者名（代理店ユーザ） or検索
	private static testmethod void testdoSearchInsuredNameAG2(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		List<E_Policy__c> policylist = TestUtil_I_SearchBox.createPolicys();
		for(E_Policy__c pol : policylist){
			TestUtil_I_SearchBox.createPolicyShare(pol.Id,util.aUser.Id);
		}

		System.runAs(util.aUser){
			test.startTest();
				I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
				controller.keyword = 'テスト被保険者 1';
				controller.searchType = I_Const.SEARCH_TYPE_INSURED;
				controller.selection = 'or';
				controller.doSearch();
			test.stopTest();

			system.assertEquals(controller.policyRows.size(), 4);
		}
	}

	// 検索 - 最近検索したキーワード
	private static testmethod void testdoSearchLog(){
		E_Policy__c policy = TestUtil_I_SearchBox.createEmptyPolicy(false, null);
		policy.COMM_CHDRNUM__c = '12345678';
		insert policy;

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('searchType', I_Const.SEARCH_TYPE_POLICY_NO);
		pr.getParameters().put('keyword', '12345678');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.doSearchLog();
		test.stopTest();

		system.assertEquals(controller.policyRows.size(), 1);
	}

	// 

	// ソート - 顧客番号
	private static testmethod void testsortRowsCustomerNo(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'customerNo');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ケイヤク';
			controller.searchType = I_Const.SEARCH_TYPE_OWNER;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT ContractorCLTPF_CLNTNUM__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		system.assertEquals(controller.policyRows.get(0).customerNo, assertPolicy.ContractorCLTPF_CLNTNUM__c);
	}

	// ソート - 契約者名（降順）
	private static testmethod void testsortRowsOwnerNameDesc(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'ownerName');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ケイヤク';
			controller.searchType = I_Const.SEARCH_TYPE_OWNER;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT ContractorName__c, ContractorE_CLTPF_ZCLKNAME__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		system.assertEquals(controller.policyRows.get(0).ownerName, assertPolicy.ContractorName__c);
		system.assertEquals(controller.policyRows.get(0).ownerNameKana, assertPolicy.ContractorE_CLTPF_ZCLKNAME__c);
	}

	// ソート - 住所
	private static testmethod void testsortRowsAddress(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'address');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ケイヤク';
			controller.searchType = I_Const.SEARCH_TYPE_OWNER;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT ContractorAddress__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		system.assertEquals(controller.policyRows.get(0).address, assertPolicy.ContractorAddress__c);
	}

	// ソート - 被保険者名
	private static testmethod void testsortRowsInsuredName(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'insuredName');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ヒホケンシャ';
			controller.searchType = I_Const.SEARCH_TYPE_INSURED;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT InsuredName__c, InsuredE_CLTPF_ZCLKNAME__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		system.assertEquals(controller.policyRows.get(0).insuredName, assertPolicy.InsuredName__c);
		system.assertEquals(controller.policyRows.get(0).insuredNameKana, assertPolicy.InsuredE_CLTPF_ZCLKNAME__c);
	}

	// ソート - 生年月日
	private static testmethod void testsortRowsBirthDay(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'birthDay');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ヒホケンシャ';
			controller.searchType = I_Const.SEARCH_TYPE_INSURED;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT InsuredCLTPF_DOB__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		Date assertBirthDay = Date.parse(E_Util.formatBirthday(assertPolicy.InsuredCLTPF_DOB__c));
		system.assertEquals(controller.policyRows.get(0).birthDay, assertBirthDay);
	}

	// ソート - 年齢（降順）
	private static testmethod void testsortRowsAgeDesc(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'age');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ヒホケンシャ';
			controller.searchType = I_Const.SEARCH_TYPE_INSURED;
			controller.doSearch();
			controller.sortRows();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT InsuredAge__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		system.assertEquals(controller.policyRows.get(0).age, assertPolicy.InsuredAge__c);
	}

	// ソート - 性別
	private static testmethod void testsortRowsSex(){
		List<E_Policy__c> policys = TestUtil_I_SearchBox.createPolicys();

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'sex');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = 'ヒホケンシャ';
			controller.searchType = I_Const.SEARCH_TYPE_INSURED;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		E_Policy__c assertPolicy = [SELECT InsuredCLTPF_ZKNJSEX__c FROM E_Policy__c WHERE Id = :policys.get(0).Id];
		system.assertEquals(controller.policyRows.get(0).sex, assertPolicy.InsuredCLTPF_ZKNJSEX__c);
	}

	// ソート - 顧客種別（降順）
	private static testmethod void testdoSearchCustomerTypeDesc(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, false);

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'customerType');
		Test.setCurrentPage(pr);

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.keyword = '67890';
			controller.searchType = I_Const.SEARCH_TYPE_CUSTOMER_NO;
			controller.doSearch();
			controller.sortRows();
		test.stopTest();

		system.assertEquals(controller.policyRows.get(0).customerType, I_Const.CUSTOMER_TYPE_INSURED);
	}

	// 検索 - 最近検索したキーワード
	private static testmethod void testgetSeachBoxLog(){
		createlogs();

		test.startTest();
			I_PolicySearchBoxController controller = new I_PolicySearchBoxController();
			controller.searchTypeName = I_Const.SEARCH_TYPE_LABELPOLICY_NO;
			controller.getSeachBoxLog();
		test.stopTest();
	}

	static void createLogs(){
		E_log__c log = new E_log__c();
		log.Name = I_Const.SEARCH_LABEL_CATEGORY_POLICY + '>' + I_Const.SEARCH_TYPE_LABELPOLICY_NO + '>99999999';
		log.Detail__c = '{"searchType": "policyNo", "searchTypeName": "証券番号", "keyword": "99999999"}';
		log.AccessUser__c = userInfo.getUserId();
		log.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_SEARCH;
		log.AccessDateTime__c = DateTime.now();
		insert log;
	}

	//親取引先
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先';
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}


	//取引先
	static Account createAccount(){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先';
		acp.E_CL2PF_ZEAYKNAM__c = 'トリヒサキ';
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		acp.OwnerId = UserInfo.getUserId();
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c,Id from Account where Id = :acp.Id];
	}

	//個人保険特約
	static void createCOV(E_Policy__c po){
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_STATDATE__c = String.valueOf(Date.Today().addMonths(-1)).replace('-', '');
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		cov.COLI_ZCRIND__c = 'C';
		cov.COMM_ZCOVRNAM__c = '4テストタイプ';
		insert cov;
	}


	//テストデータ作成
	//募集人
	static Contact createContact(Account acc){
		Contact con = new Contact();
		con.LastName = contactName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		return con;
	}

	//保険契約ヘッダ
	static E_Policy__c createPolicy(Account acc,Contact con){
		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.Insured__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.Annuitant__c = con.Id;
		insert po;
		return po;
	}

	//契約者募集人
	static Contact createContact01(Account acc){
		Contact con = new Contact();
		con.LastName = contactName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CLTPF_FLAG01__c = '1';		//契約者フラグ
		insert con;
		return con;
	}


	//被保険者募集人
	static Contact createContact02(Account acc){
		Contact con = new Contact();
		con.LastName = contactName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CLTPF_FLAG02__c = '1';		//被保険者フラグ
		insert con;
		return con;
	}
}