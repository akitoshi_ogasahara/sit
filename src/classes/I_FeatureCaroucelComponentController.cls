public with sharing class I_FeatureCaroucelComponentController{


	//特集バナー用コンテンツ群
	//public List<I_ContentMaster__c> bannerContentList{get;set;}
	//特集掲載期間内コンテンツ群
	public List<I_CMSComponentController> cmsComps{get;set;}

	public static final String CHATTER_FILE_THUMNBNAIL = '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId={0}';
	//バナーURLのリスト
	public List<String> chatterUrl{get;set;}


	public I_FeatureCaroucelComponentController() {
		this.cmsComps = new List<I_CMSComponentController>();
	}

	public String getCompIdsAsJson(){
		return JSON.serialize(cmsComps);
	}

	public String getChatterJson(){
		chatterUrl = new List<String>();

		URL urlObj = URL.getCurrentRequestUrl();
		String baseUrl = urlObj.getHost();

		//現在のユーザのサイト名を取得
		String siteName = urlObj.getPath().substringBefore('/apex');
		siteName = siteName.removeStart('/');

		if(siteName != ''){
			siteName = '/' + siteName;
		}

		List<Attachment> att = new List<Attachment>();
		for(I_CMSComponentController comps : this.cmsComps){
			chatterUrl.add(siteName + String.format(CHATTER_FILE_THUMNBNAIL, new List<String>{String.valueOf(comps.chatterFileId)}));
		}

		return JSON.serialize(chatterUrl);
	}

}