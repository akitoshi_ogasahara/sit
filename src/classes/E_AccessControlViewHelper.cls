/**
 *	E_AccessControlViewHelper
 *		E_AccessControllerなどのみにアクセスする必要があるコンポーネントなどの共通コントローラクラス
 */
public class E_AccessControlViewHelper {

	public E_AccessController accessController{
									get{
											if(accessController == null){
												accessController = E_AccessController.getInstance();
											}
											return accessController;
									}
									private set;
	}
	

	public Boolean getIsSumiseiUser(){
		return accessController.isSumiseiUser();
	}
}