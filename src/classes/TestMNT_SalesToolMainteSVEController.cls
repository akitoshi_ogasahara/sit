@isTest
private with sharing class TestMNT_SalesToolMainteSVEController{
		private static testMethod void testPageMethods() {	
			MNT_SalesToolMainteSVEController page = new MNT_SalesToolMainteSVEController(new ApexPages.StandardController(new I_ContentMaster__c()));	
			page.getOperatorOptions_I_ContentMaster_c_Name();	
			page.getOperatorOptions_I_ContentMaster_c_DocumentNo_c();	
			page.getOperatorOptions_I_ContentMaster_c_ApprovalNo_c();	
			page.getOperatorOptions_I_ContentMaster_c_Category_c();	
			page.getOperatorOptions_I_ContentMaster_c_Management_c();	
			page.getOperatorOptions_I_ContentMaster_c_ForCustomers_c();	
			page.getOperatorOptions_I_ContentMaster_c_ForAgency_c();	
			page.getOperatorOptions_I_ContentMaster_c_CompanyLimited_c();	
			System.assert(true);
		}	
			
	private static testMethod void testresultTable() {
		MNT_SalesToolMainteSVEController.resultTable resultTable = new MNT_SalesToolMainteSVEController.resultTable(new List<I_ContentMaster__c>(), new List<MNT_SalesToolMainteSVEController.resultTableItem>(), new List<I_ContentMaster__c>(), null);
		resultTable.create(new I_ContentMaster__c());
		resultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}