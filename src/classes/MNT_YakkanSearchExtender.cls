global with sharing class MNT_YakkanSearchExtender extends SkyEditor2.Extender{

	public List<String> agencyCodeList {
		get{
			if(agencyCodeList == null){
				agencyCodeList = new List<String>();
				agencyCodeOptions = new Map<String, String>();
				for(Schema.PicklistEntry item : I_ContentMaster__c.AgencyCode__c.getDescribe().getPicklistValues()){
					 if (item.isActive()) {
					 	agencyCodeList.add(item.getValue());
						agencyCodeOptions.put(item.getValue(), item.getLabel());
					 }
			   	}
				return agencyCodeList;
			}
			return agencyCodeList;
		} set;}

	public Map<String, String> agencyCodeOptions {get; set;}


	public MNT_YakkanSearchExtender(MNT_YakkanSearchController extension){
	}

}