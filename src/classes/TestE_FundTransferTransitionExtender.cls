@isTest
private class TestE_FundTransferTransitionExtender {
	
	//private static final String PAGE_TITLE = '['+ E_Const.SYSTEM_NAME +'] - ' + '積立金と死亡給付金額等の推移グラフ';
	private static final String PAGE_TITLE = '積立金と死亡保険金額等の推移グラフ';
	
	//コンストラクタ
	static testMethod void constructorTest001(){
		
		E_Policy__c record = new E_Policy__c(COMM_CRTABLE__c = 'SR');
		insert record;
		
		Test.startTest();
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_FundTransferTransitionController extension = new E_FundTransferTransitionController(controller);
		

		E_FundTransferTransitionExtender extender = new E_FundTransferTransitionExtender(extension);
		Test.stopTest();
		
		//System.assertEquals(PAGE_TITLE, extender.getPageTitle());
		
	}
	//init
	//doAuth成功
	static testMethod void initTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_FundTransferTransitionExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
		}
	}
	
	//init
	//doAuth失敗
	static testMethod void initTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_FundTransferTransitionExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
			
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_FundTransferTransitionExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			system.assertEquals(null, result);
		}
	}
	
	//ページアクション
	//doAuth失敗
	static testMethod void pageActionTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_FundTransferTransitionExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			system.assertNotEquals(null, result);
		}
	}
	
	//テストユーザ作成
	static User createUser(String flag02){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG02__c = flag02;
		insert idcpf;
		return u;
	}
	
	//Extender作成
	static E_FundTransferTransitionExtender createExtender(){
		RecordType recordType = [Select Id From RecordType Where DeveloperName = 'ESPHPF' LIMIT 1];
		E_Policy__c record = new E_Policy__c();
		record.RecordTypeId = recordType.Id;
		record.COMM_CRTABLE__c = 'SR';
		insert record;
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_FundTransferTransitionController extension = new E_FundTransferTransitionController(controller);
		E_FundTransferTransitionExtender extender = new E_FundTransferTransitionExtender(extension);
		return extender;
	}
}