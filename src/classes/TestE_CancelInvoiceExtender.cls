@isTest
private class TestE_CancelInvoiceExtender{
	/**
	 * 最低限のレコードで実施
	 */
	private static testMethod void testCancelInvoice1() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        E_Policy__c policy = new E_Policy__c();
	        PageReference pref = Page.E_CancelInvoiceDownloads;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_CancelInvoiceController controller = new E_CancelInvoiceController(standardcontroller);
	        E_CancelInvoiceExtender extender = controller.getExtender();
	        extender.init();
	        resultPage = extender.PageAction();
	        extender.getFN();
	        extender.getDownloaId();
	        extender.getCancelInvoicePdfPath();
	        extender.isAgree = true;
	        extender.doNextAgree();
	        extender.doNextDownload();
	        extender.doPrevAgree();
	        extender.doPrevConfirm();
	        // 同意チェックボックスをオフ
	        extender.isAgree = false;
	        extender.doNextDownload();

	        //テスト終了
			Test.stopTest();
		}
		//※正常処理ならこれを確認↓
		system.assertNotEquals(null, resultPage);
    }
    /**
	 * 最低限のレコードで実施
	 */
	private static testMethod void testCancelInvoice2() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        E_Policy__c policy = new E_Policy__c();
	        PageReference pref = Page.E_CancelInvoiceAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_CancelInvoiceController controller = new E_CancelInvoiceController(standardcontroller);
	        E_CancelInvoiceExtender extender = controller.getExtender();
	        extender.init();
	        resultPage = extender.PageAction();
	        extender.getFN();
	        extender.getDownloaId();
	        extender.getCancelInvoicePdfPath();
	        extender.isAgree = true;
	        extender.doNextAgree();
	        extender.doNextDownload();
	        extender.doPrevAgree();
	        extender.doPrevConfirm();
	        // 同意チェックボックスをオフ
	        extender.isAgree = false;
	        extender.doNextDownload();

	        //テスト終了
			Test.stopTest();
		}
		//※正常処理ならこれを確認↓
		system.assertNotEquals(null, resultPage);
    }
	/**
	 * ステップアップ情報の登録
	 * 引出割合をハイフンで出力
	 */
    private static testMethod void testCancelInvoice3() {
    			//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
	    	//取引先登録
			Account acc = new Account();
			acc.Name = '代理店';
			insert acc;
			//主募集人情報登録
			Contact cntMainAgent = new Contact();
			cntMainAgent.FirstName = 'テスト主募集人';
			cntMainAgent.LastName = 'テスト苗字';
			cntMainAgent.AccountId = acc.Id;
			cntMainAgent.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
			cntMainAgent.E_CLTPF_DOB__c = '20141111';
			cntMainAgent.E_CLTPF_ZKNJSEX__c = '男';
			insert cntMainAgent;

	    	// 保険契約ヘッダの登録
	        E_Policy__c policy = new E_Policy__c();
	        policy.MainAgent__c = cntMainAgent.Id;
	        insert policy;
	        PageReference pref = Page.E_Spva;

	        // 解約申込書DL設定の登録
	        E_CancellFormDownloadControl__c cancelDownload = new E_CancellFormDownloadControl__c();
	        cancelDownload.Account__c = acc.Id;
	        insert cancelDownload;

	        // 添付ファイルの登録
	        Attachment attach = new Attachment();
	        attach.Name = 'UTest Attachment';
	        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
	        attach.body = bodyBlob;

	        attach.parentId = cancelDownload.id;
	        insert attach;
	 
	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_CancelInvoiceController controller = new E_CancelInvoiceController(standardcontroller);
	        E_CancelInvoiceExtender extender = controller.getExtender();
	        extender.init();
	        resultPage = extender.PageAction();
	        extender.getFN();
	        extender.getDownloaId();
	        extender.getCancelInvoicePdfPath();
	        extender.isAgree = true;
	        extender.doNextAgree();
	        extender.doNextDownload();
	        extender.doPrevAgree();

	        //テスト終了
			Test.stopTest();
		}
		//※正常処理ならこれを確認↓
		system.assertEquals(null, resultPage);
    }	
}