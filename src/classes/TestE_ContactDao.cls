@isTest
private class TestE_ContactDao {

	static String contactName = '募集人';
	static String customerNumber = '20161220';
	static String agentNum = 'AGN2016';
	static String mofCode = 'MOF2016';
	static String dayOfBirth = '20001220';

	//getRecByIdのテスト
	@isTest static void getRecByIdTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRec = E_ContactDao.getRecById(con.Id);
		System.assertEquals(conRec.Name, contactName);
		Test.stopTest();
	}

	//getRecByIdのテスト
	@isTest static void getRecByAgentNumberTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRec = E_ContactDao.getRecByAgentNumber(agentNum);
		System.assertEquals(conRec.E_CL3PF_AGNTNUM__c, agentNum);
		Test.stopTest();
	}

	//getRecsByOfficeIdのテスト
	@isTest static void getRecsByOfficeIdTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsByOfficeId(acc.Id);
		System.assertEquals(conRec[0].Name, contactName);
		Test.stopTest();
	}

	//getRecsIRISPolicySearchBoxのテスト1
	@isTest static void getRecsIRISPolicySearchBoxTest01() {
		Account acc = createAccount();
		Contact con = createContact01(acc);
		E_Policy__c policy = createPolicy(acc, con);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsIRISPolicySearchBox(contactName, I_Const.SEARCH_TYPE_OWNER);
		System.assertEquals(conRec[0].Name, contactName);
		Test.stopTest();
	}

	//getRecsIRISPolicySearchBoxのテスト2
	@isTest static void getRecsIRISPolicySearchBoxTest02() {
		Account acc = createAccount();
		Contact con = createContact02(acc);
		E_Policy__c policy = createPolicy(acc, con);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsIRISPolicySearchBox(contactName, I_Const.SEARCH_TYPE_INSURED);
		System.assertEquals(conRec[0].Name, contactName);
		Test.stopTest();
	}

	//getRecsIRISPolicySearchBoxのテスト3 and検索
	@isTest static void getRecsIRISPolicySearchBoxTest03() {
		Account acc = createAccount();
		Contact con = createContact01(acc);
		E_Policy__c policy = createPolicy(acc, con);

		Account acc2 = createAccount();
		Contact con2 = createContact01(acc2,'テスト募集人');
		E_Policy__c policy2 = createPolicy(acc2, con2);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsIRISPolicySearchBox('テスト　募集人', I_Const.SEARCH_TYPE_OWNER,'and');
		System.assertEquals(1, conRec.size());
		Test.stopTest();
	}

	//getRecsIRISPolicySearchBoxのテスト4 or検索
	@isTest static void getRecsIRISPolicySearchBoxTest04() {
		Account acc = createAccount();
		Contact con = createContact01(acc);
		E_Policy__c policy = createPolicy(acc, con);

		Account acc2 = createAccount();
		Contact con2 = createContact01(acc2,'テスト募集人');
		E_Policy__c policy2 = createPolicy(acc2, con2);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsIRISPolicySearchBox('テスト 募集人', I_Const.SEARCH_TYPE_OWNER,'or');
		System.assertEquals(2, conRec.size());
		Test.stopTest();
	}

	//getRecsIRISPolicySearchBoxのテスト5 and検索
	@isTest static void getRecsIRISPolicySearchBoxTest05() {
		Account acc = createAccount();
		Contact con = createContact02(acc);
		E_Policy__c policy = createPolicy(acc, con);

		Account acc2 = createAccount();
		Contact con2 = createContact02(acc2,'テスト募集人');
		E_Policy__c policy2 = createPolicy(acc2, con2);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsIRISPolicySearchBox('テスト　募集人', I_Const.SEARCH_TYPE_INSURED,'and');
		System.assertEquals(1, conRec.size());
		Test.stopTest();
	}

	//getRecsIRISPolicySearchBoxのテスト6 or検索
	@isTest static void getRecsIRISPolicySearchBoxTest06() {
		Account acc = createAccount();
		Contact con = createContact02(acc);
		E_Policy__c policy = createPolicy(acc, con);

		Account acc2 = createAccount();
		Contact con2 = createContact02(acc2,'テスト募集人');
		E_Policy__c policy2 = createPolicy(acc2, con2);

		Test.startTest();
		List<Contact> conRec = E_ContactDao.getRecsIRISPolicySearchBox('テスト 募集人', I_Const.SEARCH_TYPE_INSURED,'or');
		System.assertEquals(2, conRec.size());
		Test.stopTest();
	}

	//getRecByAgClCodeのテスト
	@isTest static void getRecByAgClCodeTest01() {
		Account acc = createAccount();
		Contact con = createContact02(acc);

		Test.startTest();
		Contact conRec = E_ContactDao.getRecByAgClCode(customerNumber);
		System.assertEquals(conRec.E_CL3PF_AGNTNUM__c, agentNum);
		Test.stopTest();
	}

	//getRecByAgClCodeのテスト
	@isTest static void getRecsByIdsTest01() {
		Account acc = createAccount();
		Contact con = createContact02(acc);
		update con;

		Set<Id> ids = new Set<Id>();
		ids.add(con.id);

		Test.startTest();
		Map<Id, Contact> conRec = E_ContactDao.getRecsByIds(ids);
		System.assertEquals(conRec.get(con.id).Name, contactName);
		Test.stopTest();
	}



	//テストデータ作成
	//募集人
	static Contact createContact(Account acc){
		Contact con = new Contact();
		con.LastName = contactName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		return con;
	}

	//取引先
	static Account createAccount(){
		Account acp = new Account();
		acp.Name = '取引先';
		insert acp;
		return acp;
	}

	//保険契約ヘッダ
	static E_Policy__c createPolicy(Account acc,Contact con){
		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.Insured__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.Annuitant__c = con.Id;
		insert po;
		return po;
	}

	//契約者募集人
	static Contact createContact01(Account acc){
		Contact con = new Contact();
		con.LastName = contactName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CLTPF_FLAG01__c = '1';		//契約者フラグ
		insert con;
		return con;
	}

	//契約者募集人
	static Contact createContact01(Account acc,String conName){
		Contact con = new Contact();
		con.LastName = conName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = '20161221';
		con.E_CL3PF_CLNTNUM__c = '20161221';
		con.E_CL3PF_AGNTNUM__c = 'AGN2017';
		con.E_CL3PF_KMOFCODE__c = 'MOF2017';
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CLTPF_FLAG01__c = '1';		//契約者フラグ
		insert con;
		return con;
	}

	//被保険者募集人
	static Contact createContact02(Account acc){
		Contact con = new Contact();
		con.LastName = contactName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CLTPF_FLAG02__c = '1';		//被保険者フラグ
		insert con;
		return con;
	}

	//被保険者募集人
	static Contact createContact02(Account acc,String conName){
		Contact con = new Contact();
		con.LastName = conName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = '20161221';
		con.E_CL3PF_CLNTNUM__c = '20161221';
		con.E_CL3PF_AGNTNUM__c = 'AGN2017';
		con.E_CL3PF_KMOFCODE__c = 'MOF2017';
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CLTPF_FLAG02__c = '1';		//契約者フラグ
		insert con;
		return con;
	}
}