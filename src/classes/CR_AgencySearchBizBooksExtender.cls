public with sharing class CR_AgencySearchBizBooksExtender extends SkyEditor2.Extender{
	
	private CR_AgencySearchBizBooksSVEController controller;
	
	/**
	 * Construstor
	 */
	public CR_AgencySearchBizBooksExtender(CR_AgencySearchBizBooksSVEController extension){
		this.controller = extension;
	}
	
	/**
	 * CSVExport
	 */
	public PageReference ExportCSV(){
		PageReference pr = Page.E_CRExportAgencyBizBooks;
		String soql = controller.queryMap.get('resultTable').toSoql();
		String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
		sWhere = E_SoqlUtil.trimLimitClause(sWhere);
		system.debug('source:' + sWhere);
		pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
								E_EncryptUtil.getEncryptedString(sWhere));  
		return pr;
	}
}