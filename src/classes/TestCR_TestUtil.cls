/**
 * CRのテスト用データ作成共通クラス
 */
@isTest
public with sharing class TestCR_TestUtil {

    /**
     * Profileマップ取得
     * @return Map<String, Id>: Map of ProfileName & ProfileID
     */
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    public static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }

    /**
     * 代理店ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @param ConId： 募集人ID
     * @return User: ユーザ
     */
    public static User createAgentUser(Boolean isInsert, String LastName, String profileDevName, ID ConId){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , ContactId = ConId
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }

    /**
     * Account作成
     * @param isInsert: whether to insert
     * @return Account: 取引先レコード
     */
    public static Account createAccount(Boolean isInsert) {
        Account src = new Account(
              Name = 'TestAccountName'
            , E_CL1PF_ZHEADAY__c = '12345'
            , E_COMM_VALIDFLAG__c = '1'
            , ZMRCODE__c = 'MR00001'
        );
        if(isInsert){
            insert src;
            src = [Select Id, Name, E_CL1PF_ZHEADAY__c, E_COMM_VALIDFLAG__c, ZMRCODE__c, E_IsAgency__c From Account Where Id =: src.Id];
        }
        return src;
    }
    
    /**
     * Account作成
     * @param isInsert: whether to insert
     * @param recCnt: record count
     * @return List<Account>: 取引先レコード
     */
    public static List<Account> createAccountList(Boolean isInsert, Integer recCnt) {
    	List<Account> srcList = new List<Account>();
    	for(Integer i = 0; i < recCnt; i++){
	        Account src = new Account(
	              Name = 'TestAccountName' + String.valueOf(i)
	            , E_CL1PF_ZHEADAY__c = '1234' + String.valueOf(i)
	            , E_COMM_VALIDFLAG__c = '1'
	            , ZMRCODE__c = 'MR0000' + String.valueOf(i)
	        );
	        srcList.add(src);
    	}
    	
        if(isInsert){
            insert srcList;
            srcList = [Select Id, Name, E_CL1PF_ZHEADAY__c, E_COMM_VALIDFLAG__c, ZMRCODE__c, E_IsAgency__c From Account Where Id in: srcList];
        }
        return srcList;
    }
    
    /**
     * Account作成(代理店事務所)
     * @param isInsert: whether to insert
     * @return Account: 取引先レコード
     */
    public static Account createOffice(Boolean isInsert, Id parentAccId, String officeCd) {
        Account src = new Account(
              Name = 'TestOfficeName'
            , E_CL2PF_BRANCH__c = '99'
            , E_CL2PF_ZAGCYNUM__c = officeCd
            , E_COMM_VALIDFLAG__c = '1'
            , ZMRCODE__c = 'MR00001'
            , ParentId = parentAccId
        );
        if(isInsert){
            insert src;
            src = [Select Id, Name, E_CL2PF_BRANCH__c, E_CL2PF_ZAGCYNUM__c, E_COMM_VALIDFLAG__c, ZMRCODE__c, ParentId, E_IsAgency__c From Account Where Id =: src.Id];
        }
        return src;
    }

    /**
     * Contact作成
     * @param isInsert: whether to insert
     * @param accId: AccountId
     * @param lastName: LastName
     */
    public static Contact createContact(Boolean isInsert, Id accId, String lastName) {
        Contact src = new Contact(
              AccountId = accId
            , LastName = lastName
        );
        if(isInsert){
            insert src;
            src = [Select Id, Account.Name, LastName, E_AccParentCord__c From Contact Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Agency作成(Init処理 False用)
     * @param isInsert: whether to insert
     * @return CR_Agency: CR_Agencyレコード
     */
    public static CR_Agency__c createCRAgency(boolean isInsert) {
        CR_Agency__c src = new CR_Agency__c();
        if(isInsert){
            insert src;
        }
        return src;
    }


    /**
     * CR_Agency作成
     * @param isInsert: whether to insert
     * @param accId: AccountId
     * @Param recId: RecordType.DeveloperName
     * @return CR_Agency: CR_Agencyレコード
     */
    public static CR_Agency__c createCRAgency(Boolean isInsert, ID accId, String devName) {
        Schema.DescribeSObjectResult result = CR_Agency__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> target = result.getRecordTypeInfos();
        
        String recTypeName = [SELECT Id, Name FROM RecordType WHERE SobjectType='CR_Agency__c' AND DeveloperName = :devname LIMIT 1 ].Name;
        Id recId;
        
        for(RecordTypeInfo i : target){
            if(i.getName() == recTypeName){
                recId = i.getRecordTypeId();
                break;
            }
        }
                
        CR_Agency__c src = new CR_Agency__c(Account__c = accId, RecordTypeID = recId);
        if(isInsert){
            insert src;
            src = [Select Id, AgencyCode__c, SystemModStamp From CR_Agency__c Where Id =: src.Id];
        }
        return src;
    }
    
    /**
     * CR_Agency作成
     * @param isInsert: whether to insert
     * @param accId: AccountId
     * @Param recId: RecordType.DeveloperName
     * @return List<CR_Agency>: CR_Agencyレコード
     */
    public static List<CR_Agency__c> createCRAgencyList(Boolean isInsert, Set<ID> accIds, String devName) {
        Schema.DescribeSObjectResult result = CR_Agency__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> target = result.getRecordTypeInfos();
        
        String recTypeName = [SELECT Id, Name FROM RecordType WHERE SobjectType='CR_Agency__c' AND DeveloperName = :devname LIMIT 1 ].Name;
        Id recId;
        
        for(RecordTypeInfo i : target){
            if(i.getName() == recTypeName){
                recId = i.getRecordTypeId();
                break; 
            }
        }
                
        List<CR_Agency__c> srcList = new List<CR_Agency__c>();
        for(ID accId : accIds){
        	CR_Agency__c src = new CR_Agency__c(Account__c = accId, RecordTypeID = recId);
        	srcList.add(src);
        }
        
        if(isInsert){
            insert srcList;
            srcList = [Select Id, AgencyCode__c, SystemModStamp From CR_Agency__c Where Id in :srcList];
        }
        return srcList;
    }
    
    /**
     * ダウンロード履歴リスト作成
     * 事業x1、帳簿x2
     */
    public static List<E_DownloadHistorry__c> createDlHistoryList(String agencyId){
    	List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
    	for(Integer i = 1; i < 3; i++){
	    	// 出力基準年月
			Date d = Date.today();
			DateTime dt = DateTime.newInstance(d.year(), d.month() + i, d.day());
	    	String outputYm = dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT);
	    	
	    	// 検索条件
	    	String dlCondition = 'condition' + i;
	    	
	    	// 事業報告書
	    	if(i < 2){
	    		dhList.add(createDlHistory(agencyId, CR_Const.TYPE_BIZREPORTS, outputYm, dlCondition));
	    	}
	    	
	    	// 帳簿書類
	    	dhList.add(createDlHistory(agencyId, CR_Const.TYPE_BIZBOOKS, outputYm, dlCondition));
    	}
    	insert dhList;
    	
    	return dhList;
    }
    
	/**
	 *  ダウンロード履歴作成
	 */
	public static E_DownloadHistorry__c createDlHistory(String agencyId, String formNo, String outputYm, String dlCondition){
		// ダウンロード履歴作成
		E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
		dh.Status__c = CR_Const.DL_STATUS_WAIT;
		dh.RequestDateTime__c = Datetime.now();
		dh.CR_Agency__c = agencyId;
		dh.FormNo__c = formNo;
		dh.Conditions__c = dlCondition;
		dh.OutputRecordYM__c = outputYm.replace('/', '');
	    
		return dh;
	}


    /**
     * CR_Rule作成(コンストラクタ処理 新規用)
     * @param isInsert: whether to insert
     * @return CR_Rule: CR_Ruleレコード
     */
    public static CR_Rule__c createCRRule() {
        CR_Rule__c src = new CR_Rule__c();

        return src;
    }
    
    /**
     * CR_Rule作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Rule: CR_Ruleレコード
     */
    public static CR_Rule__c createCRRule(Boolean isInsert, ID agencyId) {
        CR_Rule__c src = new CR_Rule__c(CR_Agency__c = agencyId, Type__c = '顧客サポート等管理規程');
        if(isInsert){
            insert src;
        }
        return src;
    }

    /**
     * CR_Oursrc作成(コンストラクタ処理 新規用)
     * @param isInsert: whether to insert
     * @return CR_Outsrc: CR_Outsrcレコード
     */
    public static CR_Outsrc__c createCROutsrc() {
        CR_Outsrc__c src = new CR_Outsrc__c();

        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrc(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', 
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'いいえ');
        if(isInsert){
            insert src;
            src = [Select Id, Status__c, CR_Agency__c, hasCustomerInfo__c, ReasonForNofiles__c From CR_Outsrc__c Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcEndDate(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', 
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'いいえ', 
        									OutsrcTo__c = system.today());
        if(isInsert){
            insert src;
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcHasCustomerInfo(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', Representative__c = '代表者名',
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'はい',
        									SharedInfo__c = 'Test001', Confirm06_1__c = 'はい', Confirm06_2__c = 'はい', 
        									Confirm06_3__c = 'はい', Confirm06_4__c = 'はい');
        if(isInsert){
            insert src;
            src = [Select Id, Status__c, hasCustomerInfo__c From CR_Outsrc__c Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcStatusConfirm(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', Representative__c = '代表者名', address__c = '東京都中央区日本橋',
        									Business__c = '業務委託内容', OutsrcFrom__c = System.Today(),
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'はい',
        									SharedInfo__c = 'Test001', Confirm06_1__c = 'はい', Confirm06_2__c = 'はい', 
        									Confirm06_3__c = 'はい', Confirm06_4__c = 'はい', Status__c = CR_Const.OUTSRC_STATUS_02a);
        if(isInsert){
            insert src;
            src = [Select Id, Status__c, hasCustomerInfo__c From CR_Outsrc__c Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcStatusReport(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', Representative__c = '代表者名', address__c = '東京都中央区日本橋',
        									Business__c = '業務委託内容', OutsrcFrom__c = System.Today(),
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'はい',
        									SharedInfo__c = 'Test001', Confirm06_1__c = 'はい', Confirm06_2__c = 'はい', 
        									Confirm06_3__c = 'はい', Confirm06_4__c = 'はい', 
        									Status__c = CR_Const.OUTSRC_STATUS_02b);
        if(isInsert){
            insert src;
            src = [Select Id, Status__c, hasCustomerInfo__c From CR_Outsrc__c Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcStatusApprove(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', 
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'はい',
        									SharedInfo__c = 'Test001', Confirm06_1__c = 'はい', Confirm06_2__c = 'はい', 
        									Confirm06_3__c = 'はい', Confirm06_4__c = 'はい', Status__c = CR_Const.OUTSRC_STATUS_03);
        if(isInsert){
            insert src;
            src = [Select Id, Status__c, hasCustomerInfo__c From CR_Outsrc__c Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcStatusTerminate(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', 
        									Confirm01__c = 'はい', Confirm02__c = 'はい', Confirm03__c = 'いいえ', 
        									Confirm04__c = 'はい', Confirm05__c = 'はい', Confirm06__c = 'はい',
        									SharedInfo__c = 'Test001', Confirm06_1__c = 'はい', Confirm06_2__c = 'はい', 
        									Confirm06_3__c = 'はい', Confirm06_4__c = 'はい', 
        									Status__c = CR_Const.OUTSRC_STATUS_09, OutsrcTo__c = system.today());
        if(isInsert){
            insert src;
            src = [Select Id, Status__c, hasCustomerInfo__c, OutsrcTo__c From CR_Outsrc__c Where Id =: src.Id];
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcReason(Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', ReasonForNofiles__c = 'test');
        if(isInsert){
            insert src;
        }
        return src;
    }

    /**
     * CR_Oursrc作成
     * @param isInsert: whether to insert
     * @param agencyId: CR_Agency__c.Id
     * @return CR_Oursrc: CR_Oursrcレコード
     */
    public static CR_Outsrc__c createCROutsrcAddConfirm06 (Boolean isInsert, ID agencyId) {
        CR_Outsrc__c src = new CR_Outsrc__c(CR_Agency__c = agencyId, Name = '外部委託業者名', Confirm06__c = 'いいえ');
        if(isInsert){
            insert src;
        }
        return src;
    }


    /**
     * Attachment作成
     * @param isInsert: whether to insert
     * @param ruleId: CR_Rule.Id
     * @return Attachment: Attachmentレコード(規定ファイル)
     */
    public static Attachment createAttachment(Boolean isInsert, ID ruleId) {
        Blob body = Blob.valueOf('test.jpg');
        Attachment src = new Attachment(Name = 'File001', ParentId = ruleId, Body = body);
        if(isInsert){
            insert src;
        }
        return src;
    }

    /**
     * 社員・承認権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createPermissions(ID usrId) {
        Id permEmpId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_EMPLOYEE].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permEmpId);
        insert src1;
        
        Id permId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_APPROVE].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permId);
        insert src2;
    }

    /**
     * 代理店権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createAgencyPermissions(ID usrId) {
        Id permAgnyId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_AGENCY].Id;
        PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
        insert src;
    }

    /**
     * 代理店・外部委託権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createAgencyOutSrcPermissions(ID usrId) {
        Id permAgnyId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_AGENCY].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
        insert src1;
        
        Id permOutSrcId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_OUTSRC].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permOutSrcId);
        insert src2;
    }

    /**
     * 社員・外部委託権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createOutSrcPermissions(ID usrId) {
        Id permEmpId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_EMPLOYEE].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permEmpId);
        insert src1;
        
        Id permOutSrcId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_OUTSRC].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permOutSrcId);
        insert src2;
    }    

    /**
     * 社員・承認権限、外部委託権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createAllPermissions(ID usrId) {
        Id permEmpId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_EMPLOYEE].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permEmpId);
        insert src1;
        
        Id permId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_APPROVE].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permId);
        insert src2;

        Id permOutSrcId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_OUTSRC].Id;
        PermissionSetAssignment src3 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permOutSrcId);
        insert src3;
    }

    /**
     * 代理店・事業報告書権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createAgencyBizReportsPermissions(ID usrId) {
        Id permAgnyId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_AGENCY].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
        insert src1;
        
        Id permBizReportsId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_REPORTS].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permBizReportsId);
        insert src2;
    }

    /**
     * 社員・事業報告書権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createBizReportsPermissions(ID usrId) {
        Id permEmpId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_EMPLOYEE].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permEmpId);
        insert src1;
        
        Id permBizReportsId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_REPORTS].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permBizReportsId);
        insert src2;
    }   
    
    /**
     * 代理店・帳簿書類権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createAgencyBizBooksPermissions(ID usrId) {
        Id permAgnyId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_AGENCY].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
        insert src1;
        
        Id permBizBooksId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_BOOKS].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permBizBooksId);
        insert src2;
    }

    /**
     * 社員・帳簿書類権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createBizBooksPermissions(ID usrId) {
        Id permEmpId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_EMPLOYEE].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permEmpId);
        insert src1;
        
        Id permBizBooksId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_BOOKS].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permBizBooksId);
        insert src2;
    }  

    /**
     * 社員・DPA権限セット付与
     * @param isInsert: whether to insert
     * @param usrId: ユーザID
     */
    public static void createBizDpaPermissions(ID usrId) {
        Id permEmpId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_EMPLOYEE].Id;
        PermissionSetAssignment src1 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permEmpId);
        insert src1;
        
        Id permBizDpaId = [Select Id FROM PermissionSet Where Name =: CR_Const.PERMISSIONSET_DPA].Id;
        PermissionSetAssignment src2 = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permBizDpaId);
        insert src2;
    }
    
    /**
     * CR_Agency 承認日付更新
     * @param agencyId: 代理店提出ID
     * @return CR_Agency: CR_Agencyレコード
     */
    public static void updateCRAgency(ID agencyId) {
        List<CR_Agency__c> src =[Select ApprovalDate__c, SystemModStamp FROM CR_Agency__c Where Id =: agencyId];
        for(CR_Agency__c ag: src){
            ag.ApprovalDate__c = system.Now();
        }
        update src;
    }
    
    /**
     * ContactShare 
     */
    public static void createContactShare(Id conId, Id uId){
		ContactShare conShare = new ContactShare();
        conShare.ContactId = conId;
        conShare.UserOrGroupId = uId;
        conShare.ContactAccessLevel = 'Read';
        conShare.RowCause = Schema.ContactShare.RowCause.Manual;
        insert conShare;
    }
    
    /**
     * 出力基準年月の期間Fromの1月前取得
     */
    public static String getOutputYmLast(String bizType){
    	String outputFrom;
    	
    	if(bizType == CR_Const.TYPE_BIZREPORTS){
    		outputFrom = CR_Const.OUTPUT_RECORD_YM_REPORTS_FROM;
    	}else{
    		outputFrom = CR_Const.OUTPUT_RECORD_YM_BOOKS_FROM;
    	}
    	
    	List<String> fromYm = outputFrom.split('/');
    	Integer y = Integer.valueOf(fromYm[0]);
    	Integer m = Integer.valueOf(fromYm[1]) -1;
    	if(m == 0){
    		y -= 1;
    		m = 12;
    	}
    	
    	String mstr = m < 10 ? '0' + String.valueOf(m) : String.valueOf(m);
    	
    	return String.valueOf(y) + '/' + mstr;
    }
}