@isTest
private class TestI_ChangeUserIDContloller {

	private final static String userName = 'shoyusya@test.com';
	private final static String password = '1234567801';
	private final static String tempBasicPWModifiedDate = '20150407';

	static void init(){
		insertE_bizDataSyncLog();
		insertMessage();
		insertEmail();
		system.debug('this is init');
	}

	//コンストラクタテスト1
	//suggestionなし
	static testMethod void constructorTest01() {

		Test.startTest();
		I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
		Test.stopTest();

		System.assertEquals(false, ctrl.pageMessages.hasMessages());
		System.assertEquals(ctrl.inputLabel, 'メールアドレス');
	}

	//コンストラクタテスト2
	//suggestionあり
	static testMethod void constructorTest02() {
		PageReference pageRef = Page.IRIS_ChangeUserID;
		pageRef.getParameters().put('suggestion', '1');
		Test.setCurrentPage(pageRef);

		Test.startTest();
		I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
		Test.stopTest();

		System.assertEquals(false, ctrl.pageMessages.hasMessages());
		System.assertEquals(ctrl.inputLabel, 'メールアドレス');
	}

	//changeUserIDテスト01
	//正常終了
	static testMethod void changeUserIDTest01() {
//		User user01 = createUser(false, false);
		init();
		Contact con = createContactData('1');
//		createIDRequest(idcp);
		User user01 = createUser(con.id);
		E_IDCPF__c idcp =  createIDManage('0',user01.Id);

		String loginpass = 'irisTest1';
		//System.setPassword(user01.Id, loginpass);
		//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
		//System.debug(ret);
		System.runAs(user01){
			//パラメータ作成
			String param = createParam();
			PageReference pageRef = Page.IRIS_ChangeUserID;
			//パラメータをセット
//			pageRef.getParameters().put('p', param);
			Test.setCurrentPage(pageRef);
			System.debug(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			System.debug(Site.getDomain());
			Test.startTest();

			ctrl.mailAddress = 'test1@test.com';
			ctrl.confirmEmailAddress = 'test1@test.com';
			PageReference result = ctrl.updateEmail();
			Test.stopTest();

			//System.assert(result.getUrl().contains('/e_errorpage?code')); //Menuマスタが取得できないエラーとなる
			System.assertEquals(result, null);
		}
	}

	//changeUserIDテスト02
	//入力メールアドレスが既存ユーザ名と重複
	static testMethod void changeUserIDTest02() {
		init();
		//メッセージ作成
		createMenuData();
		createMessageData('CID|008','ご指定のメールアドレスはIDとして利用できません。');

		Contact con = createContactData('1');
		Contact con2 = createContactData2('1');
		User user01 = createUser(con.id);
		User user02 = createUser2(con2.id);
		E_IDCPF__c idcp =  createIDManage('0',user01.Id);

		String loginpass = 'irisTest1';
		//System.setPassword(user01.Id, loginpass);
		//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
		//System.debug(ret);
		System.runAs(user01){
			//パラメータ作成
			String param = createParam();
			PageReference pageRef = Page.IRIS_ChangeUserID;
			//パラメータをセット
			pageRef.getParameters().put('p', param);
			Test.setCurrentPage(pageRef);
			System.debug(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			System.debug(Site.getDomain());
			Test.startTest();

			ctrl.mailAddress = '1' + userName;
			ctrl.confirmEmailAddress = '1' + userName;
			PageReference result = ctrl.updateEmail();
			Test.stopTest();

			System.assertEquals(ctrl.pageMessages.getErrorMessages()[0].summary, 'ご指定のメールアドレスはIDとして利用できません。');
			System.assertEquals(result, null);
		}
	}

	//changeUserIDテスト03
	//入力欄が空欄
	static testMethod void changeUserIDTest03() {
		//init();
		//メッセージ作成
		createMenuData();
		createMessageData('MAC|001','新規電子メールアドレスを入力して下さい。');
		createMessageData('CUI|001','新規電子メールアドレス（確認）を入力して下さい。');

		Contact con = createContactData('1');
		User user01 = createUser(con.id);
		E_IDCPF__c idcp =  createIDManage('0',user01.Id);

		String loginpass = 'irisTest1';
		//System.setPassword(user01.Id, loginpass);
		//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
		//System.debug(ret);
		System.runAs(user01){
			//パラメータ作成
			String param = createParam();
			PageReference pageRef = Page.IRIS_ChangeUserID;
			//パラメータをセット
			pageRef.getParameters().put('p', param);
			Test.setCurrentPage(pageRef);
			System.debug(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			System.debug(Site.getDomain());
			Test.startTest();

			ctrl.mailAddress = null;
			ctrl.confirmEmailAddress = null;
			PageReference result = ctrl.updateEmail();
			Test.stopTest();

			List<E_PageMessagesHolder.nlinkPageMessage> msgHolder = ctrl.pageMessages.getErrorMessages();
			System.assertEquals(msgHolder[0].summary, '新規電子メールアドレスを入力して下さい。');
			System.assertEquals(msgHolder[1].summary, '新規電子メールアドレス（確認）を入力して下さい。');
			System.assertEquals(result, null);
		}
	}

	//changeUserIDテスト04
	//メールアドレスと確認メールアドレスが異なる
	static testMethod void changeUserIDTest04() {
		//init();
		//メッセージ作成
		createMenuData();
		createMessageData('MAC|003','新規電子メールアドレスと新規電子メールアドレス（確認）が相違しております。');

		Contact con = createContactData('1');
		User user01 = createUser(con.id);
		E_IDCPF__c idcp =  createIDManage('0',user01.Id);

		String loginpass = 'irisTest1';
		//System.setPassword(user01.Id, loginpass);
		//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
		//System.debug(ret);
		System.runAs(user01){
			//パラメータ作成
			String param = createParam();
			PageReference pageRef = Page.IRIS_ChangeUserID;
			//パラメータをセット
			pageRef.getParameters().put('p', param);
			Test.setCurrentPage(pageRef);
			System.debug(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			System.debug(Site.getDomain());
			Test.startTest();

			ctrl.mailAddress = 'test1@test.com';
			ctrl.confirmEmailAddress = 'test2@test.com';
			PageReference result = ctrl.updateEmail();
			Test.stopTest();

			System.assertEquals(ctrl.pageMessages.getErrorMessages()[0].summary, '新規電子メールアドレスと新規電子メールアドレス（確認）が相違しております。');
			System.assertEquals(result, null);
		}
	}

	//changeUserIDテスト05
	//メールアドレス形式ではない
	static testMethod void changeUserIDTest05() {
		//init();
		//メッセージ作成
		createMenuData();
		createMessageData('MAC|005','電子メールアドレスが正しくありません。');

		Contact con = createContactData('1');
		User user01 = createUser(con.id);
		E_IDCPF__c idcp =  createIDManage('0',user01.Id);

		String loginpass = 'irisTest1';
		//System.setPassword(user01.Id, loginpass);
		//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
		//System.debug(ret);
		System.runAs(user01){
			//パラメータ作成
			String param = createParam();
			PageReference pageRef = Page.IRIS_ChangeUserID;
			//パラメータをセット
			pageRef.getParameters().put('p', param);
			Test.setCurrentPage(pageRef);
			System.debug(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			System.debug(Site.getDomain());
			Test.startTest();

			ctrl.mailAddress = 'test1@test1@test.com';
			ctrl.confirmEmailAddress = 'test1@test1@test.com';
			PageReference result = ctrl.updateEmail();
			Test.stopTest();

			System.assertEquals(ctrl.pageMessages.getErrorMessages()[0].summary, '電子メールアドレスが正しくありません。');
			System.assertEquals(result, null);
		}
	}


	//changeUserIDテスト06
	//現在のメールアドレス(ユーザーID)と同値
	//static testMethod void changeUserIDTest06() {
	//	init();
	//	//メッセージ作成
	//	createMenuData();
	//	createMessageData('MAC|005','電子メールアドレスが正しくありません。');

	//	Contact con = createContactData('1');
	//	User user01 = createUser(con.id);
	//	E_IDCPF__c idcp =  createIDManage('0',user01.Id);

	//	String loginpass = 'irisTest1';
	//	//System.setPassword(user01.Id, loginpass);
	//	//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
	//	//System.debug(ret);
	//	System.runAs(user01){
	//		//パラメータ作成
	//		String param = createParam();
	//		PageReference pageRef = Page.IRIS_ChangeUserID;
	//		//パラメータをセット
	//		pageRef.getParameters().put('p', param);
	//		Test.setCurrentPage(pageRef);
	//		System.debug(pageRef);
	//		I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
	//		System.debug(Site.getDomain());
	//		Test.startTest();

	//		ctrl.mailAddress = userName;
	//		ctrl.confirmEmailAddress = userName;
	//		PageReference result = ctrl.updateEmail();
	//		Test.stopTest();

	//		System.assertEquals(ctrl.pageMessages.getErrorMessages()[0].summary, '電子メールアドレスが正しくありません。');
	//		System.assertEquals(result, null);
	//	}
	//}

	//「今回は変更しない」
	static testMethod void updateNextTimeTest01() {
		init();
//		User user01 = createUser(false, false);
		Contact con = createContactData('1');
//		createIDRequest(idcp);
		User user01 = createUser(con.id);

		System.runAs(user01){
			E_IDCPF__c idcp =  createIDManage('0',user01.Id);
			//パラメータ作成
			String param = createParam();
			PageReference pageRef = Page.IRIS_ChangeUserID;
			//パラメータをセット
			pageRef.getParameters().put('p', param);
			Test.setCurrentPage(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			String loginpass = 'irisTest1';
			Test.startTest();
			//System.setPassword(user01.Id, loginpass);
			//PageReference ret = Site.login(user01.Username, loginpass, Page.E_Compliance.getUrl());
			ctrl.mailAddress = 'test1@test.com';
			ctrl.confirmEmailAddress = 'test1@test.com';
			PageReference result = ctrl.updateNextTime();
			Test.stopTest();

			//System.assert(result.getUrl().contains('/e_errorpage?code')); //Menuマスタが取得できないエラーとなる
			Integer idReqestNum = [SELECT Id FROM E_IDRequest__c WHERE CreatedById = :user01.Id].size();
			System.assertEquals(idReqestNum, 1);
			System.assertEquals(result.getUrl().contains('/e_home'), true);
		}
	}

	//「戻る」
	static testMethod void doReturnTest01(){
		init();
		Contact con = createContactData('1');
		User user01 = createUser(con.id);

		System.runAs(user01){
			PageReference pageRef = Page.IRIS_ChangeUserID;
			Test.setCurrentPage(pageRef);
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();

			Test.startTest();
			PageReference result = ctrl.doReturn();
			Test.stopTest();

			System.assertEquals(result.getUrl().contains('/iris_userconfig'), true);
		}
	}
/*
	static testMethod void closePageTest01() {
		I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
		Test.startTest();
		PageReference result = ctrl.closePage();
		Test.stopTest();
	}


	//pageActionテスト02
	//パラメータがない
	static testMethod void pageActionTest02() {
//      User user01 = createUser(false, false);
		Contact con = createContactData('1');
		E_IDCPF__c idcp =  createIDManage('0');
//      createIDRequest(idcp);
		User user01 = createUser(con.id);

		System.runAs(user01){
			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();

			Test.startTest();
			PageReference result = ctrl.pageAction();
			Test.stopTest();

			System.assertEquals(result, null);
		}
	}

	//pageActionテスト03
	//パラメータが不足
	static testMethod void pageActionTest03() {
//      User user01 = createUser(false, false);
		Contact con = createContactData('1');
		E_IDCPF__c idcp =  createIDManage('0');
//      createIDRequest(idcp);
		User user01 = createUser(con.id);

		System.runAs(user01){
			//パラメータ作成
			String param = 'usID='+ userName;
			String cryptoParam = E_EncryptUtil.getEncryptedString(param);
			String encodeParam = EncodingUtil.urlEncode(cryptoParam, 'UTF-8');
			ApexPages.currentPage().getParameters().put('p', encodeParam);

			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();

			Test.startTest();
			PageReference result = ctrl.pageAction();
			Test.stopTest();

			System.assertEquals(result, null);
		}
	}

	//updateEmailテスト01
	//正常終了
	static testMethod void updateEmailTest01(){
//      User user01 = createUser(false, false);
		Contact con = createContactData('1');
		E_IDCPF__c idcp =  createIDManage('0');
//      createIDRequest(idcp);
		User user01 = createUser(con.id);

		System.runAs(user01){
			//パラメータ作成
			String param = createParam();
			ApexPages.currentPage().getParameters().put('p', param);

			I_ChangeUserIDContloller ctrl = new I_ChangeUserIDContloller();
			ctrl.pageAction();

			Test.startTest();
			ctrl.mailAddress = 'updateEmailTest01@test.co.jp';
			ctrl.confirmEmailAddress = 'updateEmailTest01@test.co.jp';
			PageReference result = ctrl.updateEmail();
			Test.stopTest();

			System.assertEquals(result, null);
		}
	}
*/
	//ログインユーザ作成
/*  static User createUser(Boolean useTempBasicPW, Boolean useExistingBasicPW){


		UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
		User ownerUser = TestE_TestUtil.createUser(false, username, E_Const.SYSTEM_ADMINISTRATOR);

		ownerUser.userroleid = uRole.Id;
		ownerUser.username =  username +  System.Label.E_USERNAME_SUFFIX;
		ownerUser.E_UseTempBasicPW__c = useTempBasicPW;
		ownerUser.E_UseExistingBasicPW__c = useExistingBasicPW;
		ownerUser.E_TempBasicPWModifiedDate__c = tempBasicPWModifiedDate;
		insert ownerUser;
		return ownerUser;
	}
*/
	//パラメータ作成
	static String createParam(){
		String param = 'usID='+ userName + '&np='+ password;
		String cryptoParam = E_EncryptUtil.getEncryptedString(param);
		return EncodingUtil.urlEncode(cryptoParam, 'UTF-8');
	}



	//メッセージマスタ作成
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//メニューマスタ作成
	static void createMenuData(){
		E_MenuMaster__c mn = new E_MenuMaster__c();
		mn.MenuMasterKey__c = 'internet_service_policy_agency';
		insert mn;
	}

	//募集人作成
	static Contact createContactData(String flag){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.E_CL3PF_AGNTNUM__c = 'test01';
			con.E_CLTPF_DOB__c = '20161017';
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.E_CL3PF_VALIDFLAG__c = flag;
			con.AccountId = accid;
			con.E_CL3PF_KMOFCODE__c = 'test01';
			insert con;
		}

		return con = [select Name,Id,FirstName,LastName from Contact where E_CL3PF_AGNTNUM__c = 'test01'];
	}


	//募集人作成
	static Contact createContactData2(String flag){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.E_CL3PF_AGNTNUM__c = 'test02';
			con.E_CLTPF_DOB__c = '20161017';
			con.FirstName = '花子2';
			con.LastName = 'テストテスト';
			con.E_CL3PF_VALIDFLAG__c = flag;
			con.AccountId = accid;
			con.E_CL3PF_KMOFCODE__c = 'test02';
			insert con;
		}

		return con = [select Name,Id,FirstName,LastName from Contact where E_CL3PF_AGNTNUM__c = 'test02'];
	}
	//ID管理作成
	static E_IDCPF__c createIDManage(String sta,ID userId){
		 E_IDCPF__c ic = new E_IDCPF__c();
		 ic.ZWEBID__c = '1234567890';
		 ic.ZSTATUS01__c = sta;
		 ic.ZIDOWNER__c = 'AG' + 'test01';
		 ic.User__c = userId;
		 insert ic;

		 return ic;
	}
/*
	//IDリクエスト作成
	static void createIDRequest(E_IDCPF__c idmn){
		E_IDRequest__c idr = new E_IDRequest__c();
		idr.ZEMAILAD__c = 'test@test.com';
		idr.E_IDCPF__c = idmn.Id;
		insert idr;
	}
*/
	//ユーザ作成
	static User createUser(Id id){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			us.Username = userName + System.Label.E_USERNAME_SUFFIX;
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = Id;
			us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
			insert us;
		}
		return us;
	}

	//ユーザ作成
	static User createUser2(Id id){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			us.Username = '1' + userName + System.Label.E_USERNAME_SUFFIX;
			us.Alias = 'テスト花子2';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = Id;
			us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
			insert us;
		}
		return us;
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;

		return acc.Id;
	}

	/*
	* テスト用ページの取得
	* @param    : なし
	* @return   : テスト用ページ
	*/
	static PageReference getPageReference() {
		return new PageReference('/nnlinkage/IRIS_ChangeUserID');
	}

	private static void insertE_bizDataSyncLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
			insert log;
		}
	}

	private static void insertMessage(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
			String type = 'メッセージ';
			String param = 'MAC|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|002';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|003';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|004';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|005';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAB|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAB|003';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			
			type = 'ディスクレーマー';
			param = 'MAC|006';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|007';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|008';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|009';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			insert insMessage;
		}
	}

	private static void insertEmail(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_Email__c eMail = new E_Email__c(NAME = 'E_101',Email__c = 'xxxx@xx.xx');
			insert eMail;
		}
	}
}