global class CMN_VlocityTriggerOnBatchSched implements Schedulable {

	global void execute(SchedulableContext sc){
		String soql = 'SELECT Id, Name FROM Account WHERE E_UpsertKey__c = \'99999999999\' LIMIT 1';
		Database.executebatch(new CMN_VlocityTriggerOnBatch(soql));
		System.abortJob(sc.getTriggerId());
		setBatchSchedule();
	}

	public static void setBatchSchedule() {
		String strCronExpr = getCronExpression(System.Label.CMN_VlocityTriggerOnBatchSched_System_CronExpression);
		System.schedule('CMN_VlocityTriggerOnBatch', strCronExpr, new CMN_VlocityTriggerOnBatchSched());
	}

	/**
	 * 概要 : CronExpressionを作成（動的作成は時分のみ対応）
	 */
	public static String getCronExpression(String cron){
		String strCronExpr = '';
		List<String> cronList = cron.split(' ');
		List<String> timeFormatList = new List<String>{'ss', 'mm', 'HH', 'dd', 'MM', 'F', 'y'};
		List<Integer> nextCronDtList = new List<Integer>{0, 0, 0, 0, 0, 0, 0};
		Datetime t = Datetime.now();
		t = t.addMinutes(1);

		for(Integer i=0; i<cronList.size(); i++){
			if(!Pattern.matches('[\\*\\?]+', cronList[i])){
				nextCronDtList = getNextCronDtList(cronList, t.format(timeFormatList[i]), timeFormatList[i], i, nextCronDtList);
			}
		}

		for(Integer i=0; i<cronList.size(); i++){
			if(!Pattern.matches('[\\*\\?]+', cronList[i])){
				strCronExpr += nextCronDtList[i] + ' ';
			}else{
				strCronExpr += cronList[i] + ' ';
			}
		}

		return strCronExpr;
	}

	private static List<Integer> getNextCronDtList(List<String> cronList, String strCmpTime, String strTimeFormat, Integer intListNum, List<Integer> nextCronDtList){
		Integer intCronExpr = null;

		if(Pattern.matches('[0-9]+', cronList[intListNum])){
			nextCronDtList[intListNum] = Integer.valueOf(cronList[intListNum]);
		}else if(Pattern.matches('[0-9]+[\\-][0-9]+', cronList[intListNum])){
			Integer intStart = Integer.valueOf(cronList[intListNum].split('-')[0]);
			Integer intStop = Integer.valueOf(cronList[intListNum].split('-')[1]);
			String strEnum = String.valueOf(intStart);
			for(Integer i=intStart+1; i<=intStop; i++){
				strEnum+=','+String.valueOf(i);
			}
			cronList[intListNum] = strEnum;
		}

		if(Pattern.matches('([0-9]+[\\,])+[0-9]+', cronList[intListNum])){
			List<String> cronItemList = cronList[intListNum].split(',');
			List<Integer> tmpList = new List<Integer>(nextCronDtList);
			Boolean isCountUp = true;
			Datetime ncdt;
			Integer intAdjust = tmpList[intListNum];
			Integer i;

			for(i=0; i<cronItemList.size(); i++){
				tmpList[intListNum] = Integer.valueOf(cronItemList[i]);
				ncdt = Datetime.newInstance(tmpList[6], tmpList[4], tmpList[3], tmpList[2], tmpList[1], tmpList[0]);

				if(intCronExpr == null) intCronExpr = tmpList[intListNum];
				if(Integer.valueOf(strCmpTime) <= Integer.valueOf(ncdt.format(strTimeFormat))){
					intCronExpr = tmpList[intListNum];
					isCountUp = false;
					break;
				}
			}

			if(intAdjust != 0){
				if(i >= cronItemList.size() - 1){
					isCountUp = true;
				}else{
					intCronExpr = Integer.valueOf(cronItemList[i+1]);
				}
			}

			if(isCountUp){
				nextCronDtList[intListNum+1]++;
				for(Integer j=0; j<=intListNum; j++){
					nextCronDtList[j] = Integer.valueOf(cronList[j].split(',')[0]);
				}
			}else{
				nextCronDtList[intListNum] = intCronExpr;
			}
		}

		return nextCronDtList;
	}

}