@isTest
private class TestASS_EventDaoWithout {
	//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}

	//エリア部長で実行
	private static testMethod void deleteEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_EventDaoWithout.deleteEvent(eventList[0]);
//===============================テスト終了===============================
			Test.stopTest();
			List<Event> retEvList = [SELECt Id FROM Event WHERE WhatId = :office.Id];
			System.assertEquals(retEvList.size(), 2);
		}
	
	}

	//MRで実行
	private static testMethod void deleteEventMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		
		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_EventDaoWithout.deleteEvent(eventList[0]);
//===============================テスト終了===============================
			Test.stopTest();
			List<Event> retEvList = [SELECt Id FROM Event WHERE WhatId = :office.Id];
			System.assertEquals(retEvList.size(), 2);
		}
	
	}
}