/**
 * NNTubeログ　登録項目管理クラス
 */
public class I_LogNNTubeResult {

	// 
	public NNTubeLogJsonResult jsonResult {get; set;}
	// 
	public List<NNTubeMedia> movieRows {get; set;}

	/**
	 * JSON ログ定義
	 */
	public class NNTubeMedia {
		public String ActionType {get;set;}
		public String AccessPage {get;set;}
		public String AccessUser {get;set;}
		public String AccessDatetime {get;set;}
		public String DisplayUrl {get;set;}
		public String SummaryName {get;set;}
		public String AppName {get;set;}
		public String TubeLogSeq {get;set;}
	}

	/**
	 * JSONの返却値
	 */
	public class NNTubeLogJsonResult {
		public String resultcode {get; set;}
		public List<NNTubeMedia> data {get; set;}
	}
	
	/**
	 * Constructor
	 */
	public I_LogNNTubeResult() {
		this.jsonResult = new NNTubeLogJsonResult();
		this.movieRows = new List<NNTubeMedia>();
	}
	
	/**
	 * NNLinkLogリスト生成
	 */
	public void createMedias(NNTubeLogJsonResult jsonResult){
		for(NNTubeMedia resultJson : jsonResult.data){
			resultJson = setLogInfo(resultJson);
			movieRows.add(resultJson);
		}
	}

	/**
	 * ログ情報セット
	 */
	private NNTubeMedia setLogInfo(NNTubeMedia rec){
		// アクセス日時のフォーマット
		rec.AccessDatetime = rec.AccessDatetime.replace('/','-') + ':00';
		return rec;
	}
}