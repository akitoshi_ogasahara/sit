global with sharing class E_ProceduralPasswordChangeExtender extends E_AbstractEditExtender{
    private static final String PAGE_TITLE = '手続サービス用パスワード変更';

    E_ProceduralPasswordChangeController extension;
    
    // 処理種別：初期化
    public Boolean isInit {get; set;}
    // 現在のパスワード
    public String oldPassword {get; set;}
    // 新しいパスワード
    public String newPassword {get; set;}
    // 新しいパスワード（確認）
    public String verPassword {get; set;}
    // レコード
    private E_IDCPF__c record;
    // User
    private User user;
    // ContactId
    public String contactId{get;set;}
    // 現在日時
    private Datetime dt;
    
    
    /* コンストラクタ */
    public E_ProceduralPasswordChangeExtender(E_ProceduralPasswordChangeController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }
    
    /* init */
    global override void init(){
        // ユーザ情報取得
        this.user = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
        
        // アクセスチェック
        pageRef = doAuth(E_Const.ID_KIND.USER, this.user.Id);
        if(pageRef == null){
            // URLパラメータ取得
            this.contactId = ApexPages.currentPage().getParameters().get('conid');
            
            // ログインユーザのユーザIDより、ID管理を取得する
            this.record = E_IDCPFDao.getRecsById(this.user.Id);
            
            // 初期化（手続パスワード変更日が空白）かを判定
            this.isInit = String.isBlank(this.record.ZPASSWD02DATE__c);
            
        }
    }
    
    /* PageAction */
    public Pagereference pageAction(){
        return E_Util.toErrorPage(pageRef, null);
    }
    
    /* 実行ボタン押下処理 */
    public Pagereference doExecute(){
    	pageMessages.clearMessages();
        // 入力チェック
        Boolean hasError = false;
		try{
			List<String> errMsgList = E_UtilPasswordChange.validatePassword(this.record, this.oldPassword, this.newPassword, this.verPassword);
			if(errMsgList.size() > 0){
				// エラーメッセージセット
				for(String errMsg : errMsgList){
					pageMessages.addErrorMessage(errMsg);
				}
							
				hasError = true;
			}
		}catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
			return null;
		}
        
        // エラーなし
        if(!hasError){
            try{
                Boolean isMailNotSend = this.isInit;
                
                //現在日時の文字列変換
                this.dt = datetime.now();
                
                // 手続サービス用パスワード更新
                E_UtilPasswordChange.updateProceduralPassword(this.record, this.newPassword);
                /*
                this.record.ZPASSWD02__c = E_EncryptUtil.encrypt(this.newPassword);
                this.record.ZSTATUS02__c = E_Const.PW_STATUS_OK;
                this.record.ZPASSWD02DATE__c = String.valueOf(dt.format('yyyyMMdd'));
                update this.record;
                */
                
                this.isInit = false;
                
                // 顧客情報変更受付履歴情報登録
                E_ADRPF__c adrpf = createAdrpf(isMailNotSend);
                insert adrpf;
                
                Pagereference pref = Page.E_AcceptanceCompletion;
                pref.getParameters().put('type', 'ACC|005');
                pref.getParameters().put('conid', this.contactId);
                return pref;
                
            }catch(Exception e){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                pageMessages.addErrorMessage(e.getMessage());
                return null;
            }
            
        // エラーあり
        }else{
            return null;
        }
    }

    // 顧客情報変更受付履歴情報
    private E_ADRPF__c createAdrpf(Boolean isMailNotSend){
        E_ADRPF__c adrpf = new E_ADRPF__c();
        adrpf.ReceptionDatetime__c = String.valueOf(dt.format('yyyy年MM月dd日 HH時mm分'));   // 受付日時
        adrpf.E_IDCPF__c = this.record.Id;                                  // ID管理
        adrpf.CLNTNUM__c = this.user.Contact.E_CLTPF_CLNTNUM__c;            // 顧客番号
        adrpf.ContactName__c = this.user.Contact.Name;                      // お客様氏名
        adrpf.ContactNameKana__c = this.user.Contact.E_CLTPF_ZCLKNAME__c;   // お客様フリガナ
        adrpf.Type__c = E_Const.MAIL_TYPE_2P;                               // 手続き種別
        adrpf.EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c;// 担当者メールアドレス
        adrpf.Kbn__c = E_Const.KBN_2PPW;                                    // 区分
        adrpf.MailNotSend__c = isMailNotSend;
        return adrpf;
    }
}