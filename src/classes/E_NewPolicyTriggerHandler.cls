public with sharing class E_NewPolicyTriggerHandler {

	public E_NewPolicyTriggerHandler (){
	}

	/**
	 * Insert時アクション
	 */
	public void onBeforeInsert(List<E_NewPolicy__c> newList){
		setStstusDate(newList);
		Set<String> gSet = new Set<String>();
		for(E_NewPolicy__c plcy :E_NewPolicyDao.getGroupParentRecs()){
			gSet.add(plcy.GroupKey__c);
		}
		for(E_NewPolicy__c newRec :newList){
			/* UX 新契約Push対応 */
			// 団体の場合、団体サマリーキー（テキスト）を設定
			if(newRec.KFRAMEIND__c == 'Y' && newRec.KPAKETNM__c != null){
				String str = String.valueOf(newRec.KPAKETNM__c);
				String[] strs = str.split('\\.');
				newRec.GroupKeyText__c = newRec.GRUPKEY__c + '|'+ strs[0];
			}
			
			if(newRec.KFRAMEIND__c <> 'Y' || gSet.contains(newRec.GroupKey__c)) continue;
			newRec.GroupParentFlag__c = true;
			gSet.add(newRec.GroupKey__c);
		}
	}

	/**
	 * Insert時アクション
	 */
	public void onAfterInsert(List<E_NewPolicy__c> newList){
		setGroupStatusDate(newList);
	}

	/**
	 * update時アクション
	 */
	public void onBeforeUpdate(Map<Id, E_NewPolicy__c> newMap, Map<Id, E_NewPolicy__c> oldMap) {
		setStstusDate(newMap.values());
	}

	/**
	 * update時アクション
	 */
	public void onAfterUpdate (Map<Id, E_NewPolicy__c> newMap, Map<Id, E_NewPolicy__c> oldMap) {
		setGroupStatusDate(newMap.values());
	}

	/**
	 * ステータスが変更された際に、ステータス発信日をセット
	 */
	public void setStstusDate(List<E_NewPolicy__c> newList){
		for(E_NewPolicy__c newRec : newList) {
			// ステータスコードの変更があった際は変更
			if(newRec.SystemStatus__c <> newRec.Status__c){
				newRec.StatusDate__c = newRec.SyncDate__c.addHours(-9);
				newRec.SystemStatus__c = newRec.Status__c;
			}
		}
	}

	/**
	 * 団体ステータス発信日を設定
	 */
	public void setGroupStatusDate(List<E_NewPolicy__c> newList){
		Datetime syncDate;
		Set<String> gSet = new Set<String>();
		Map<String,List<E_NewPolicy__c>> gMap = new Map<String,List<E_NewPolicy__c>>();
		List<E_NewPolicy__c> upd = new List<E_NewPolicy__c>();
		//対象のSetを作成
		for(E_NewPolicy__c newRec : newList) {
			if(newRec.KFRAMEIND__c <> 'Y') continue;
			gSet.add(newRec.GroupKey__c);
		}
		if(gSet.isEmpty()){
			return;
		} 
		for(E_NewPolicy__c plcy :E_NewPolicyDao.getRecsByGroupKeys(gSet)){
			Datetime syncDateJST = plcy.SyncDate__c.addHours(-9);
			if(syncDate == null || syncDate < syncDateJST)syncDate = syncDateJST;
			String key = plcy.GroupKey__c;
			if(gMap.containsKey(key)){
				gMap.get(key).add(plcy);
			}else{
				gMap.put(plcy.GroupKey__c,new List<E_NewPolicy__c>{plcy});
			}
		}
		for(List<E_NewPolicy__c> plcys:gMap.values()){
			Set<String> newStatusSet = new Set<String>();
			for(E_NewPolicy__c plcy :plcys){
				newStatusSet.add(plcy.Status__c);
			}
			for(E_NewPolicy__c plcy :plcys){
				if(plcy.GroupParentFlag__c){
					if(plcy.GroupStatus__c <> I_NewPolicyController.getStatus(newStatusSet)){
						plcy.GroupStatusDate__c = syncDate;
						plcy.GroupStatus__c = I_NewPolicyController.getStatus(newStatusSet);
						upd.add(plcy);
					}
					break;
				}
			}
		}
		update upd;
	}
}