/**
* 
*/
@isTest
private class TestE_MKBatonListSVEExtender {
    
    
	/* 
	* global override void init() 
	*/
    static testMethod void testInit() {
        
        PageReference pref = Page.E_MKBatonListSVE;
        Test.setCurrentPage(pref);
        
        Account acc = new Account(Name='TestAccount');
        Contact con = new Contact(firstname='Test', lastname='Test', AccountId = acc.Id);
        
        Test.startTest();

        E_MKBatonListSVEController controller = new E_MKBatonListSVEController(new ApexPages.StandardController(con));
        E_MKBatonListSVEExtender extender = new E_MKBatonListSVEExtender(controller);

        Test.stopTest();
    }
}