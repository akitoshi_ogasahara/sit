/**
 *		マスク処理バッチ処理
			≪Usage≫ 
			MNT_RefreshJobBatch batch = new MNT_RefreshJobBatch('User');
			//batch.executeNextJob = true;		連続して実行する場合にTrue
			Database.executeBatch(batch, 2000);
 */
public class MNT_RefreshJobBatch implements Database.Batchable<SObject>, Database.Stateful {

	public MNT_RefreshJobHandler handler;
	public Boolean executeNextJob;		//次のバッチ処理を実行する場合にTrue（デフォルトfalse　まとめて実行するにはTrueに ExecuteBatch前に明示的に書き換えてください）

	public MNT_RefreshJobBatch(String sObjNm){
		handler = new MNT_RefreshJobHandler(sObjNm);
		executeNextJob = false;
	}


	/**
	 * BatchApexの開始処理です
	 * @param ctx
	 * @return Database.QueryLocator 
	 */
	public Database.QueryLocator start(Database.BatchableContext ctx){
		return handler.getQueryLocator();
	}


	/**
	 * BatchApexの処理実行します
	 * @param ctx
	 * @param records
	 * @return
	 */
	public void execute(Database.BatchableContext ctx, List<Sobject> records) {
		handler.maskRecords(records);
	}

	/**
	 * BatchApexの後処理
	 * @param ctx
	 * @return
	 */
	public void finish(Database.BatchableContext ctx) {
		String nextObj = handler.getNextSObjectName();
		if(executeNextJob && String.isNotBlank(nextObj)){
			MNT_RefreshJobBatch batch = new MNT_RefreshJobBatch(nextObj);
			batch.executeNextJob = true;
			Database.executeBatch(batch, 2000);
		}	
	}

}