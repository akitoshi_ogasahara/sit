global with sharing class E_ProgressSearchBankSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
	    	
	    public E_MPQPF__c record{get;set;}	
	    	
    public E_ProgressSearchBankSVEExtender getExtender() {return (E_ProgressSearchBankSVEExtender)extender;}
    
	    public Component2 Component2 {get; private set;}	
	    	
	    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
	    	
	    public SkyEditor2__SkyEditorDummy__c iStoreCd_val {get;set;}	
        public SkyEditor2.TextHolder iStoreCd_op{get;set;}	
	    	
	    public E_MPQPF__c iStaus_val {get;set;}	
	    public SkyEditor2__SkyEditorDummy__c iStaus_val_dummy {get;set;}	
        public SkyEditor2.TextHolder iStaus_op{get;set;}	
        public List<SelectOption> valueOptions_E_MPQPF_c_STATDESC_c_multi {get;set;}
	    	
    public String recordTypeRecordsJSON_E_MPQPF_c {get; private set;}
    public String defaultRecordTypeId_E_MPQPF_c {get; private set;}
    public String metadataJSON_E_MPQPF_c {get; private set;}
    public String picklistValuesJSON_E_MPQPF_c_STATDESC_c {get; private set;}
    public Boolean QueryPagingConfirmationSetting {get;set;}
	    public E_ProgressSearchBankSVEController(ApexPages.StandardController controller) {	
	        super(controller);	

            SObjectField f;

            f = E_MPQPF__c.fields.ZBRCDE4X__c;
            f = E_MPQPF__c.fields.STATDESC__c;
            f = E_MPQPF__c.fields.ZAYKNJ__c;
            f = E_MPQPF__c.fields.CNTTYPE__c;
            f = E_MPQPF__c.fields.CHDRNUM__c;
            f = E_MPQPF__c.fields.ZDATSEND_yyyyMMdd__c;
            f = E_MPQPF__c.fields.ZPRPDATE_yyyyMMdd__c;
            f = E_MPQPF__c.fields.ZCIF4A__c;
            f = E_MPQPF__c.fields.ZCIF8A__c;
            f = E_MPQPF__c.fields.ZCLKNAME__c;
            f = E_MPQPF__c.fields.SINGPF__c;
            f = E_MPQPF__c.fields.SORTSEQ__c;
            f = E_MPQPF__c.fields.ZISUDATE__c;

        List<RecordTypeInfo> recordTypes;
	        try {	
	            	
	            mainRecord = null;	
	            mainSObjectType = E_MPQPF__c.SObjectType;	
	            	
	            	
	            mode = SkyEditor2.LayoutMode.TempSearch_01; 
	            	
	            iStoreCd_val = new SkyEditor2__SkyEditorDummy__c();	
	            iStoreCd_op = new SkyEditor2.TextHolder('eq');	
	            	
	            iStaus_val = new E_MPQPF__c();	
	            iStaus_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
	            iStaus_op = new SkyEditor2.TextHolder('inx');	
	            valueOptions_E_MPQPF_c_STATDESC_c_multi = new List<SelectOption>{
	                new SelectOption('', Label.none)
	            };
	            for (PicklistEntry e : E_MPQPF__c.STATDESC__c.getDescribe().getPicklistValues()) {
	                if (e.isActive()) {
	                    valueOptions_E_MPQPF_c_STATDESC_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
	                }
	            }
	            	
	            queryMap.put(	
	                'Component2',	
	                new SkyEditor2.Query('E_MPQPF__c')	
	                    .addFieldAsOutput('ZBRCDE4X__c')
	                    .addFieldAsOutput('ZAYKNJ__c')
	                    .addFieldAsOutput('CNTTYPE__c')
	                    .addFieldAsOutput('CHDRNUM__c')
	                    .addFieldAsOutput('ZDATSEND_yyyyMMdd__c')
	                    .addFieldAsOutput('ZPRPDATE_yyyyMMdd__c')
	                    .addFieldAsOutput('ZCIF4A__c')
	                    .addFieldAsOutput('ZCIF8A__c')
	                    .addFieldAsOutput('ZCLKNAME__c')
	                    .addFieldAsOutput('SINGPF__c')
	                    .addFieldAsOutput('STATDESC__c')
	                    .limitRecords(500)	
	                    .addListener(new SkyEditor2.QueryWhereRegister(iStoreCd_val, 'SkyEditor2__Text__c', 'ZBRCDE4X__c', iStoreCd_op, true, 0, true ))
	                    .addListener(new SkyEditor2.QueryWhereRegister(iStaus_val_dummy, 'SkyEditor2__Text__c','STATDESC__c', iStaus_op, true, 0, true ))
.addSort('SORTSEQ__c',True,False).addSort('ZISUDATE__c',True,True).addSort('CHDRNUM__c',True,True)
	            );	
	            	
	                Component2 = new Component2(new List<E_MPQPF__c>(), new List<Component2Item>(), new List<E_MPQPF__c>(), null, queryMap.get('Component2'));
                 Component2.setPageSize(100);
	            listItemHolders.put('Component2', Component2);	
	            	
	            	
	            recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_MPQPF__c.SObjectType, true);
	            	
	            	
            p_showHeader = false;
            p_sidebar = false;
	            	Cookie c = ApexPages.currentPage().getCookies().get('QueryPagingConfirm');
	            	QueryPagingConfirmationSetting = c != null && c.getValue() == '0';
            extender = new E_ProgressSearchBankSVEExtender(this);
            presetSystemParams();
            extender.init();
            Component2.extender = this.extender;
	        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
	        } catch (SkyEditor2.Errors.FieldNotFoundException e) {	
	            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            } catch (SkyEditor2.ExtenderException e) {                 e.setMessagesToPage();
	        } catch (Exception e) {	
	            System.Debug(LoggingLevel.Error, e);	
	            SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
	        }	
	    }	
	    	
        public List<SelectOption> getOperatorOptions_E_MPQPF_c_ZBRCDE4X_c() { 
            return getOperatorOptions('E_MPQPF__c', 'ZBRCDE4X__c');	
	    }	
        public List<SelectOption> getOperatorOptions_E_MPQPF_c_STATDESC_c_multi() { 
            return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	    }
	    	
	    	
    global with sharing class Component2Item extends SkyEditor2.ListItem {
        public E_MPQPF__c record{get; private set;}
        @TestVisible
        Component2Item(Component2 holder, E_MPQPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component2 extends SkyEditor2.QueryPagingList {
        public List<Component2Item> items{get; private set;}
        @TestVisible
        Component2(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector, SkyEditor2.Query query) {
            super(records, items, deleteRecords, recordTypeSelector, query);
            this.items = (List<Component2Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component2Item(this, (E_MPQPF__c)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
        public void doFirst(){first();}
        public void doPrevious(){previous();}
        public void doNext(){next();}
        public void doLast(){last();}
        public void doSort(){sort();}

    }
}