public with sharing class I_HealthConditionCheck {

	public static I_Colidatas.condition getKenkotaiWarimashiSakugenCode(List<E_COVPF__c> covpfs){
		I_ColiDatas.Condition condition = new I_ColiDatas.Condition();

		String mainMortalCls = '';
		String mainCRCode = '';
		String mainLienCode = '';
		String subMortalCls = 'S';

		for(E_COVPF__c cover: covpfs){
			//主契約判定
			if(cover.COLI_ZCRIND__c == 'C'){
				mainMortalCls = cover.COLI_MORTCLS__c;
				mainCRCode = cover.COMM_CRTABLE2__c;
				mainLienCode = cover.COLI_LIENCD__c;
			}else{
				//逓増定期かつ特約のMortalClsが空でもSでもない場合、特約のMotalClsを参照する必要がある
				if( cover.COMM_CRTABLE2__c == 'TR' && cover.COLI_MORTCLS__c != '' && cover.COLI_MORTCLS__c != null && cover.COLI_MORTCLS__c != 'S' ){
					if(subMortalCls != 'S'){
						// 逓増定期特約かつMortalClsに空でもSでもないものは1件までしかつけらない
//						errorMessages.add(I_PIPConst.ERROR_MESSAGE_BIZLOGIC_TOO_MANY_NOT_STANDART);
						condition.errorMsg.add( I_PIPConst.ERROR_MESSAGE_BIZLOGIC_TOO_MANY_NOT_STANDART ) ;
					}
					subMortalCls = cover.COLI_MORTCLS__c;
				}
			}
		}

		switch on mainMortalcls {
			when '', 'S',null {
				switch on subMortalCls{
					when 'S',null {
						condition.MortalityClassKenkotaiYuryotai = 'S';
						condition.IsStandard = true;
					}
					when 'Y' {
						condition.MortalityClassKenkotaiYuryotai = 'Y';
						condition.TeikicoverFlg = true;
					}
					when 'Z' {
						condition.MortalityClassKenkotaiYuryotai = 'Z';
						condition.TeikicoverFlg = true;
					}
					when else {
						// エラーハンドリング
						condition.errorMsg.add( 'subMortalCls:'+subMortalCls ) ;
					}
				}
			}

			when 'Y' {
				condition.MortalityClassKenkotaiYuryotai = 'Y';
			}
			when 'Z' {
				condition.MortalityClassKenkotaiYuryotai = 'Z';
			}

			when else {
				condition.MortalityClassKenkotaiYuryotai = 'S';
				condition.MortalityClassWarimashi = mainMortalCls;
				
				//生活障害の場合計算方法が異なるのでフラグをセットする
				if(mainCRCode == I_PIPConst.CR_CODE_TS || mainCRCode == I_PIPConst.CR_CODE_TL){
					condition.DDALDFlg = true;
				}

			}

		}

		//削減コードの設定
		condition.LienCondition =  mainLienCode == null ? '': mainLienCode;

		return condition;

	}

}