public with sharing class E41_AgentLoginController {
    /**
     *      Constructor
     */
    public E41_AgentLoginController(){
    }

    /**
    *       Pageaction
    */
    public  Pagereference toErrPage(){
    System.debug('@@デバッグ@@'+E_Util.getUserAgent());
        //ブラウザチェック
        if(E_Util.isUserAgentMacintosh()){
            return Page.E41_ErrMcAgent;
        }else if(!E_Util.isUserAgentIE()){
            return Page.E41_ErrIeAgent;
        }else{
            //正常処理
            return null;
        }
    }


}