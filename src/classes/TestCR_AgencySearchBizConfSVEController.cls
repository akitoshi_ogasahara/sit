@isTest
private with sharing class TestCR_AgencySearchBizConfSVEController{
		private static testMethod void testPageMethods() {	
			CR_AgencySearchBizConfSVEController page = new CR_AgencySearchBizConfSVEController(new ApexPages.StandardController(new CR_Agency__c()));	
			page.getOperatorOptions_CR_Agency_c_AgencyCode_c();	
			page.getOperatorOptions_CR_Agency_c_AgencyNameKana_c();	
			page.getOperatorOptions_CR_Agency_c_LargeAgency_c();	
			System.assert(true);
		}	
			
	private static testMethod void testresultTable() {
		CR_AgencySearchBizConfSVEController.resultTable resultTable = new CR_AgencySearchBizConfSVEController.resultTable(new List<CR_Agency__c>(), new List<CR_AgencySearchBizConfSVEController.resultTableItem>(), new List<CR_Agency__c>(), null);
		resultTable.create(new CR_Agency__c());
		resultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
}