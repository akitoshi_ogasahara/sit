public with sharing class I_CalcServiceAdapter {

	public String nnlinkid {get;set;}

	// AtriaのInput用Entityをもとに、試算結果のEntityを返す
	public  List<I_MeritTableEntity> calcMeritTable (I_CalcServiceInput AtriaInData){


		String svcInput = createServiceInput(AtriaInData);
		I_AtriaService svc = new I_AtriaService();
		svc.nnlinkid = nnlinkid;

		List<I_MeritTableDTO> meritDTOList;
		List<Object> lst = new List<Object>();

		try{
			lst = svc.getHihokensyaMeritInfo(svcInput);
		}catch(I_PIPException e){
			throw e;
		}

		List<I_MeritTableEntity> entityList = new List<I_MeritTableEntity>();

		//lstのレコード数分、各項目（keikanensu等）をEntityにセットし、EntityのListにAddする処理を繰り返す
		for(Object recordObj : lst){
			I_MeritTableEntity entity = new I_MeritTableEntity();

			Map<String, Object> recordMap =  (Map<String, Object>)recordObj;

			entity.keikaNensu = String.valueof(recordMap.get('keikaNensu'));
			entity.hokenNendo = String.valueof(recordMap.get('hokenNendo'));
			entity.age = String.valueof(recordMap.get('age'));
			entity.shibouKoudoShogaiSManyen = String.valueof(recordMap.get('shibouKoudoShogaiSManyen'));
			entity.shibouSeikatsuShogaiSManyen = String.valueof(recordMap.get('shibouSeikatsuShogaiSManyen'));
			entity.judaishippeiSManyen = String.valueof(recordMap.get('judaishippeiSManyen'));
			entity.shibouKyufuKin = String.valueof(recordMap.get('shibouKyufuKin'));
			entity.kijunSManyen = String.valueof(recordMap.get('kijunSManyen'));
			entity.saigaiSManyen = String.valueof(recordMap.get('saigaiSManyen'));
			entity.ganNyuinKyufuKinNichigaku = String.valueof(recordMap.get('ganNyuinKyufuKinNichigaku'));
			entity.nenkinGetsugaku = String.valueof(recordMap.get('nenkinGetsugaku'));
			entity.uketoriKaisu = String.valueof(recordMap.get('uketoriKaisu'));
			entity.uketoriSogaku = String.valueof(recordMap.get('uketoriSogaku'));
			entity.nenkinGenka = String.valueof(recordMap.get('nenkinGenka'));
			entity.haraikataBetsuP = String.valueof(recordMap.get('haraikataBetsuP'));
			entity.nenkanHaraikomiP = String.valueof(recordMap.get('nenkanHaraikomiP'));
			entity.pRuikei = String.valueof(recordMap.get('pRuikei'));
			entity.mikeikaP = String.valueof(recordMap.get('mikeikaP'));
			entity.cv = String.valueof(recordMap.get('cv'));
			entity.mankiSRuikei = String.valueof(recordMap.get('mankiSRuikei'));
			entity.kaiyakuUketoriKin = String.valueof(recordMap.get('kaiyakuUketoriKin'));
			entity.uketoriKinRuikeiGaku = String.valueof(recordMap.get('uketoriKinRuikeiGaku'));
			entity.simpleHenreiRt = String.valueof(recordMap.get('simpleHenreiRt'));
			entity.sonkinP = String.valueof(recordMap.get('sonkinP'));
			entity.koukaGaku = String.valueof(recordMap.get('koukaGaku'));
			entity.koukaGakuRuikei = String.valueof(recordMap.get('koukaGakuRuikei'));
			entity.jisshitsuFutanGaku = String.valueof(recordMap.get('jisshitsuFutanGaku'));
			entity.jisshitsuHenreiRt = String.valueof(recordMap.get('jisshitsuHenreiRt'));
			entity.shisanKeijoGaku = String.valueof(recordMap.get('shisanKeijoGaku'));

			//system.debug(entity.keikaNensu);

			entityList.add(entity);

		}


		//メリット表DTOを返却する
		return entityList;

 	}

	private String createServiceInput(I_CalcServiceInput AtriaInData){
				JSONGenerator gen = JSON.createGenerator(false);
		gen.writeStartObject();

		/*
		*画面入力値の情報を設定する
		*/
		gen.writeFieldName('planSekkeiList');
		gen.writeStartArray();
		gen.writeStartObject();
		gen.writeStringField('planId','');
		gen.writeStringField('planSeq','');
		gen.writeStringField('tekiyoShodakushoId','');
		gen.writeStringField('kobetsuIkkatsuKb',E_Util.null2Blank(AtriaInData.kobetsuIkkatsuKb));
		gen.writeStringField('planName','');
		gen.writeStringField('plcyDate',E_Util.null2Blank(AtriaInData.plcyDate).length() != 0?
										AtriaInData.plcyDate.substring(0,4) + '/' +
										AtriaInData.plcyDate.substring(4,6) + '/' +
										AtriaInData.plcyDate.substring(6,8): '');//責任開始日
		gen.writeStringField('tokuninFlg','');
		gen.writeStringField('crntFlg','');
		gen.writeStringField('userId','');
		gen.writeBooleanField('_isKariSave', true);
		gen.writeStringField('_shohinName','');
		gen.writeStringField('_shohinNameRyakusho','');
		gen.writeBooleanField('_isActive', true);
		gen.writeStringField('gokeiP','');
		gen.writeStringField('kansanApe','');
		gen.writeStringField('keisuAnp','');
		gen.writeStringField('gokeiZennoP','');
		gen.writeStringField('gokeiWarimashiP','');
		gen.writeStringField('keiyakushaSb',E_Util.null2Blank(AtriaInData.keiyakushaSb));//契約者種別
		gen.writeStringField('keiyakushaHojinName','');
		gen.writeStringField('keiyakushaHojinNameKana','');
		gen.writeStringField('plcyShohinCode','');
		gen.writeStringField('plcyKataCode','');
		gen.writeStringField('haraikomiKikan',E_Util.null2Blank(AtriaInData.MainCoverInfo.haraikomiKikan));//払込期間
		gen.writeStringField('haraikomiKikanNenSaiSyushin',E_Util.null2Blank(AtriaInData.MainCoverInfo.haraikomiKikanNenSaiSyushin));//払込期間種別
		gen.writeStringField('sPlcyDefault','');
		gen.writeStringField('haraikomiHoho',E_Util.null2Blank(AtriaInData.MainCoverInfo.haraikomiHoho));//払込方法
		gen.writeStringField('hojinFlg','');
		gen.writeStringField('sonkinType',E_Util.null2Blank(AtriaInData.sonkinType));//損金タイプ
		gen.writeStringField('taxRt',E_Util.null2Blank(AtriaInData.taxRt));//実効税率
		gen.writeStringField('keisanKijunDate',E_Util.null2Blank(AtriaInData.keisanKijunDate).length() != 0?
										AtriaInData.keisanKijunDate.substring(0,4) + '/' +
										AtriaInData.keisanKijunDate.substring(4,6) + '/' +
										AtriaInData.keisanKijunDate.substring(6,8): '');//計算基準日
		gen.writeStringField('sTeizoTeiki','');
		gen.writeStringField('plcyCrCode',E_Util.null2Blank(AtriaInData.MainCoverInfo.crCode));

		/*
		*被保険者情報＆主契約情報を設定する
		*/
		gen.writeFieldName('planHihokenshaList');
		gen.writeStartArray();
		gen.writeStartObject();

		gen.writeStringField('planId','');
		gen.writeStringField('planSeq','');
		gen.writeStringField('tekiyoShodakushoId','');
		gen.writeStringField('hihokenshaId',E_Util.null2Blank(AtriaInData.PlanHihokensyaInfo.hihokenshaId));//被保険者を識別するキー
		gen.writeStringField('kobetsuIkkatsuKb',E_Util.null2Blank(AtriaInData.kobetsuIkkatsuKb));//申込形態
		gen.writeStringField('keiyakushaSb',E_Util.null2Blank(AtriaInData.keiyakushaSb));
		gen.writeStringField('hihokenshaNameSei','');
		gen.writeStringField('hihokenshaNameMei','');
		gen.writeStringField('hihokenshaNameKanaSei','');
		gen.writeStringField('hihokenshaNameKanaMei','');
		gen.writeStringField('hihokenshaBirthFormat','');
		gen.writeNumberField('hihokenshaAge',AtriaInData.PlanHihokensyaInfo.hihokenshaAge);//被保険者年齢
		gen.writeStringField('hihokenshaSex',E_Util.null2Blank(AtriaInData.PlanHihokensyaInfo.hihokenshaSex));//被保険者性別
		gen.writeStringField('plcyShohinCode','');
		gen.writeStringField('kataCode','');
		gen.writeStringField('haraikomiKikan',E_Util.null2Blank(AtriaInData.MainCoverInfo.haraikomiKikan));//払込期間種別
		gen.writeStringField('haraikomiKikanNenSaiSyushin',E_Util.null2Blank(AtriaInData.MainCoverInfo.haraikomiKikanNenSaiSyushin));//払込期間
		gen.writeStringField('pSanshutsuTaisho','');
		gen.writeNumberField('sPlcy',AtriaInData.MainCoverInfo.sPlcy);//保険金額
		gen.writeStringField('hokenKikanPlcy',E_Util.null2Blank(AtriaInData.MainCoverInfo.hokenKikanPlcy));//保険期間
		gen.writeStringField('hokenKikanNenSaiSyushin',E_Util.null2Blank(AtriaInData.MainCoverInfo.hokenKikanNenSaiSyushin));//保険期間種別
		gen.writeStringField('inPHoshoKikan',E_Util.null2Blank(AtriaInData.MainCoverInfo.inPHoshoKikan));//保証期間
		gen.writeStringField('tokuteiShippeiPMenTokusoku',E_Util.null2Blank(AtriaInData.MainCoverInfo.tokuteiShippeiPMenTokusoku));//特定疾病保険料払込免除特則 有無
		gen.writeStringField('pTeigenHaraikomiTokusoku','');
		gen.writeStringField('shikiShitei',E_Util.null2Blank(AtriaInData.MainCoverInfo.shikiShitei));//始期指定
		gen.writeStringField('sentakuShiteiUmu',E_Util.null2Blank(AtriaInData.PlanHihokensyaInfo.sentakuShiteiUmu));//選択方法有無
		gen.writeStringField('sentakuHoho',E_Util.null2Blank(AtriaInData.PlanHihokensyaInfo.sentakuHoho));//選択方法
		gen.writeStringField('sentakuHohoOthKoho',E_Util.null2Blank(AtriaInData.PlanHihokensyaInfo.sentakuHohoOthKoho));//選択方法候補
		gen.writeStringField('kenkotai',E_Util.null2Blank(AtriaInData.MainCoverInfo.kenkotai));//健康体・優良体特約付加状態
		gen.writeStringField('warimashi',E_Util.null2Blank(AtriaInData.MainCoverInfo.warimashi));//割増（新特別条件特約）
		gen.writeStringField('sakugen',E_Util.null2Blank(AtriaInData.MainCoverInfo.sakugen));//削減年数
		gen.writeStringField('shiteiProxySeikyuTokuyaku','');
		gen.writeStringField('livingNeedsTokuyaku','');
		gen.writeStringField('tokuteiShogaiFutampoTokuyaku','');
		gen.writeStringField('haraikomiHoho',E_Util.null2Blank(AtriaInData.MainCoverInfo.haraikomiHoho));//払込方法
		gen.writeStringField('tokuyakuShohinCode','');
		gen.writeStringField('crCode',E_Util.null2Blank(AtriaInData.MainCoverInfo.crCode));//CRコード
		gen.writeStringField('_maxS','');
		gen.writeStringField('_minS','');
		gen.writeStringField('_minSSeidoOff','');

		/*
		*特約情報を設定する
		*/
		gen.writeFieldName('tokuyakuList');
		gen.writeStartArray();

		if(AtriaInData.MainCoverInfo.subCoverList.size() > 0){
			for(I_CalcServiceInput.SubCoverInfo sub :AtriaInData.MainCoverInfo.subCoverList){
				gen.writeStartObject();
				gen.writeStringField('planId','');
				gen.writeStringField('planSeq','');
				gen.writeStringField('tekiyoShodakushoId','');
				gen.writeStringField('hihokenshaId','');
				gen.writeStringField('tokuyakuShohinCode','');
				gen.writeStringField('kataCode','');
				gen.writeStringField('hokenKikanTokuyaku',E_Util.null2Blank(sub.hokenKikanTokuyaku));//保険期間
				gen.writeStringField('hokenKikanNenSaiSyushin',E_Util.null2Blank(sub.hokenKikanNenSaiSyushin));//保険期間種別
				gen.writeNumberField('sTokuyaku',sub.sTokuyaku);//保険金額
				gen.writeStringField('tokuyakuCrCode',E_Util.null2Blank(sub.tokuyakuCrCode));//CRコード
				gen.writeStringField('_maxS','');
				gen.writeStringField('_minS','');
				gen.writeStringField('_minSSeidoOff','');
				gen.writeEndObject();
			}
		}

		//tokuyakuListのクローズ
		gen.writeEndArray();
		//planHihokenshaListのクローズ
		gen.writeEndObject();
		gen.writeEndArray();
		//planSekkeiListのクローズ
		gen.writeEndObject();
		gen.writeEndArray();

		/*
		*メリット表出力情報を設定する
		*/
		gen.writeFieldName('scHanyoInfo');
		gen.writeStartObject();
		gen.writeFieldName('pCalc');
		gen.writeStartObject();
		gen.writeBooleanField('execMeritHyo', true);
		gen.writeEndObject();
		gen.writeEndObject();
		gen.writeEndObject();

		return gen.getAsString();

	}

}