public with sharing class E_ComplianceController extends E_InfoController{
	
	public static final String MENUMASTERKEY_CUSTOMER = 'compliance_contractant';
	public static final String INTERNET_POLICY_CUSTOMER = 'internet_service_policy_contractant';

	public static final String MENUMASTERKEY_AGENT = 'compliance_agency';
	public static final String MENUMASTERKEY_CGW_AGENT = 'compliance_agency_GW';
	
	public static final String INTERNET_POLICY_AGENCY = 'internet_service_policy_agency'; 
	public static final String INTERNET_POLICY_AGENCY_CGW = 'internet_service_policy_agency_gw'; 
	public static final String PERSONAL_POLICY_AGENCY = 'personal_policy';


	//コンストラクタ
	public E_ComplianceController(){
		super();
	}
	
	//ページアクション
	public PageReference pageAction(){
		init();
		PageReference pageRef = doAuth(E_Const.ID_KIND.COMPLIANCE, UserInfo.getUserId());
		return E_Util.toErrorPage(pageRef, null);
	}
	
	//メニューキーを返却
	@TestVisible
	protected override String getMenuKey(){
		if(access.isAgent()){
			return access.isCommonGateway() ? MENUMASTERKEY_CGW_AGENT : MENUMASTERKEY_AGENT;
		} else {
			return MENUMASTERKEY_CUSTOMER;
		}
	}
	
	//確認ボタン
	public PageReference doConfirm(){
		//コンプライアンス画面を確認したことをクッキーに保存
		E_CookieHandler.setCookieCompliance();
		return Page.E_Home;
	}
	
	public E_MessageMaster__c[] getInternetPolicyContents(){
		//String msgKey = access.isAgent() ? access.isCommonGateway() ? INTERNET_POLICY_AGENCY_CGW : INTERNET_POLICY_AGENCY : INTERNET_POLICY_CUSTOMER;
		String msgKey = access.isAgent() ? INTERNET_POLICY_AGENCY : INTERNET_POLICY_CUSTOMER;
		
		E_MenuMaster__c internetPolicyMenu = E_MessageMasterDao.getMenuMasterRecByKey(msgKey);
		return E_MessageMasterDao.getMessagesByMenuId(internetPolicyMenu.Id);
	}

	public E_MessageMaster__c[] getPersonalPolicyContents(){
		if(access.isAgent()){
			E_MenuMaster__c personalPolicyMenu = E_MessageMasterDao.getMenuMasterRecByKey(PERSONAL_POLICY_AGENCY);
			return E_MessageMasterDao.getMessagesByMenuId(personalPolicyMenu.Id);
		}else{
			return null;
		}
	}
}