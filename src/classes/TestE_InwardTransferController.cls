@isTest
private class TestE_InwardTransferController {
	
	private static testMethod void testPageMethods() {
        E_InwardTransferController extension = new E_InwardTransferController(new ApexPages.StandardController(new E_Policy__c()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;

        defaultSize = extension.E_SVFPF_c_E_Policy_c.items.size();
        extension.E_SVFPF_c_E_Policy_c.add();
        System.assertEquals(defaultSize + 1, extension.E_SVFPF_c_E_Policy_c.items.size());
        extension.E_SVFPF_c_E_Policy_c.items[defaultSize].selected = true;
        extension.callRemove_E_SVFPF_c_E_Policy_c();
        System.assertEquals(defaultSize, extension.E_SVFPF_c_E_Policy_c.items.size());
        extension.getExtender();
    }

    private static testMethod void testE_SVFPF_c_E_Policy_c() {
        E_InwardTransferController.E_SVFPF_c_E_Policy_c E_SVFPF_c_E_Policy_c = new E_InwardTransferController.E_SVFPF_c_E_Policy_c(new List<E_SVFPF__c>(), new List<E_InwardTransferController.E_SVFPF_c_E_Policy_cItem>(), new List<E_SVFPF__c>(), null);
        E_SVFPF_c_E_Policy_c.create(new E_SVFPF__c());
        System.assert(true);
    }
    
    public static testMethod void test_importByJSON_E_SVFPF_c_E_Policy_c() {
        E_InwardTransferController.E_SVFPF_c_E_Policy_c table = new E_InwardTransferController.E_SVFPF_c_E_Policy_c(
            new List<E_SVFPF__c>(),
            new List<E_InwardTransferController.E_SVFPF_c_E_Policy_cItem>(),
            new List<E_SVFPF__c>(), null);
        table.hiddenValue = '[{}]';
        table.importByJSON();
        System.assert(true);
    }
}