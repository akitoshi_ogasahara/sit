/**
 * 
 */
@isTest
private class TestE_DownloadNewPolicyExtension {

	//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];	//Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Account atAcc;
	private static List<Account> ayAccList;
	private static Contact atCon;
	private static List<E_NewPolicy__c> newpolicys;

	/**
	 * String getTitle()
	 */
	static testMethod void getTitle_test01(){
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
		createData(user.Id, true, 0);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		pref.getParameters().put('did', ahAcc.Id);
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			
			// assertion
			System.assertEquals('新契約ステータスダウンロード', extension.getTitle());
		}
		
		Test.stopTest();
	}
	
	/**
	 * PageReference pageAction()
	 * 社員 / 正常
	 */
	static testMethod void pageAction_test01(){
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		pref.getParameters().put('did', ahAcc.Id);
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();

			// assertion
			System.assertEquals(false, controller.pageMessages.hasMessages());
		}

		Test.stopTest();
	}
	
	/**
	 * PageReference pageAction()
	 * 社員 / エラー　：　対象件数0件
	 */
	static testMethod void pageAction_test02(){
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
		createData(user.Id, true, 0);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		pref.getParameters().put('did', ahAcc.Id);
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();

			// assertion
			System.assertEquals(true, controller.pageMessages.hasMessages());
		}

		Test.stopTest();
	}
	
	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AH） / 正常
	 */
	static testMethod void doDownloadCsv_test01(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();
			
			// 事務所選択
			extension.selOfficeList = new List<String>();
			extension.selOfficeList.add(controller.getActiveOfficeOptions()[0].getValue());
			
			PageReference resultPr = extension.doDownloadCsv();

			// assertion
			System.assertEquals(controller.getActiveOfficeOptions()[0].getValue(), extension.paramOfficeId);
		}

		Test.stopTest();
	}
	
	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AY） / 正常
	 */
	static testMethod void doDownloadCsv_test02(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AY);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();
			
			PageReference resultPr = extension.doDownloadCsv();

			// assertion
			User u = [Select Id, Contact.AccountId From User Where id = :user.Id ];
			System.assertEquals(u.Contact.AccountId, extension.paramOfficeId);
		}

		Test.stopTest();
	}
	
	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AT） / 正常
	 */
	static testMethod void doDownloadCsv_test03(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AT);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();
			
			PageReference resultPr = extension.doDownloadCsv();

			// assertion
			System.assertEquals(user.ContactId, extension.agentId);
		}

		Test.stopTest();
	}
	
	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AH） / エラー
	 */
	static testMethod void doDownloadCsv_test04(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();
			
			// 事務所選択
			extension.selOfficeList = new List<String>();
			
			PageReference resultPr = extension.doDownloadCsv();

			// assertion
			System.assertEquals(true, controller.pageMessages.hasMessages());
		}

		Test.stopTest();
	}

	/**
	 * String getCSVHeader()
	 */
	static testMethod void getCSVHeader_test01(){
		Test.StartTest();
		
		E_DownloadController controller = new E_DownloadController();
		E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
		extension.pageAction();
		
		Test.stopTest();
		
		// 予想値
		List<String> header = new List<String>();
		header.add('申込番号');
		header.add('証券番号');
		header.add('契約データ入力日');
		header.add('ステータス更新日');
		header.add('新契約ステータス');
		header.add('入金日');
		header.add('成立処理日');
		header.add('募集年');
		header.add('募集月');
		header.add('募集人コード');
		header.add('支部コード');
		header.add('個人コード');
		header.add('取扱者漢字名');
		header.add('募集人コード（従）');
		header.add('支部コード（従）');
		header.add('個人コード（従）');
		header.add('取扱者漢字名（従）');
		header.add('契約者名（漢字）');
		header.add('契約者名（カナ）');
		header.add('被保険者名（漢字）');
		header.add('被保険者名（カナ）');
		header.add('保険種類（主契約）');
		header.add('1P合計保険料（主契約＋特約）');		
		// 不備情報　10件
		for(Integer i = 1; i < 11; i++){
			header.add('不備発信日' + i);
			header.add('不備ステータス' + i);
			header.add('不備コード' + i);
			header.add('不備内容' + i);
			header.add('不備内容詳細' + i);
			header.add('不備処理方法' + i + '-1');
			header.add('不備処理方法' + i + '-2');
			header.add('不備処理方法' + i + '-3');
		}
		header.add('団体番号');
		header.add('データ配信日時');
		header.add('支社名（事務所名）');
		header.add('支社名（事務所名）(従)');
		header.add('申込日');
		header.add('告知日');
		header.add('診査方法');
		header.add('保険金額');
		header.add('被保険者生年月日');
		header.add('被保険者性別');
		header.add('契約時年齢');
		header.add('払方');
		header.add('払込経路');
		header.add('特別保険料');
		header.add('入金済保険料金額');
		header.add('入金済保険料金額との差額');
		header.add('ANP');
		String expected =  String.join(header, ',') + '\n';
	
		// assertion
		System.assertEquals(expected, extension.getCSVHeader());
	}


	/**
	 * List<String> getLines()
	 * AH
	 */
	static testMethod void getLines_test01(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
		createData(user.Id, true, 5);
		
		// 不備情報登録
		createDefect(newpolicys[0].Id, 10);
		
		//ページ情報
		PageReference pref = Page.E_DownloadNewPolicy;
		Test.setCurrentPage(pref);
		
		List<E_NewPolicy__c> policys = E_NewPolicyDao.getRecsForCsv(user.Id, null, null);
		for(E_NewPolicy__c p : policys){
			system.debug(p.E_NewPolicyDefects__r.size());
		}
		Test.startTest();
		
		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNewPolicyExtension extension = new E_DownloadNewPolicyExtension(controller);
			extension.pageAction();

			// 事務所選択
			extension.selOfficeList = new List<String>();
			extension.selOfficeList.add(controller.getActiveOfficeOptions()[0].getValue());

			List<String> resultList = extension.getLines();
			// assertion
			System.assertEquals(5, resultList.size());
		}
		Test.stopTest();
	}
	
/* Test Data **************************** */    
	/** User作成 */
	private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
		Profile p = [Select Id From Profile Where Name = :profileName];
		
		// Base Info
		user = new User(
			Lastname = 'test'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		
		// User
		if(profileName != PF_PARTNER){
			UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
			user.UserRoleId = portalRole.Id;
			insert user;
			
		// Partner User
		}else{
			user.ContactId = atCon.Id;
			insert user;
				
			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
		}
		
		
	}

	/** データアクセス系作成 */
	private static void createDataAccessObj(Id userId, String idType){
		system.runAs(thisuser){
			// 権限割り当て
			TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
			
			// ID管理
			E_IDCPF__c idcpf = new E_IDCPF__c(
				User__c = userId
				,ZIDTYPE__c = idType
				,FLAG01__c = '1'
				,FLAG06__c = '1'
				,ZSTATUS01__c = '1'
				,OwnerId = userId
			);
			insert idcpf;
		}
	}
	
	/** Data作成 */
	private static void createDataAccount(){
		system.runAs(thisuser){
			// Account 代理店格
			ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
			insert ahAcc;

			// Account 事務所
			ayAccList = new List<Account>();
			for(Integer i = 1; i < 4; i++){
				Account acc = new Account(
					Name = 'office' + i
					,ParentId = ahAcc.Id
					,E_CL2PF_ZAGCYNUM__c = 'ay001'
					,E_COMM_VALIDFLAG__c = '1'
				);
				ayAccList.add(acc);
			}
			insert ayAccList;

			// Contact 募集人
			if(!ayAccList.isEmpty()){
				atCon = new Contact(LastName = 'test',AccountId = ayAccList[0].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[0].E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}
		}
	}

	/** Data作成 */
	private static void createData(Id shareUserId, Boolean isCreateOffice, Integer roopCnt){
		system.runAs(thisuser){
			// 新契約ステータス
			if(roopCnt > 0){
				newpolicys = new List<E_NewPolicy__c>();
				for(Integer i = 0; i < roopCnt; i++){
					newpolicys.add(new E_NewPolicy__c(
						STATCODE__c = 'P'				// 新契約ステータス
						,CHDRNUM__c = '0000000' + i		// 証券番号
						,SyncDate__c = System.now()		// データ発信日
						,ParentAccount__c = ahAcc.Id
					));
				}
				insert newpolicys;
				
				// Share
				List<E_NewPolicy__Share> shares = new List<E_NewPolicy__Share>();
				for(E_NewPolicy__c p : newpolicys){
					E_NewPolicy__Share share = new E_NewPolicy__Share();
					share.parentId = p.Id;
					share.AccessLevel = 'read';
					share.RowCause = 'NNLinkSharingRule__c';
					share.UserOrGroupId = shareUserId;
					shares.add(share);
				}
				insert shares;
			}
		}
	}

	/**
	 * 新契約不備情報作成
	 */
	private static void createDefect(Id parentRecId, Integer dataCnt){
		List<E_NewPolicyDefect__c> defects = new List<E_NewPolicyDefect__c>();
		for(Integer i = 0; i < dataCnt; i++){
			defects.add(new E_NewPolicyDefect__c(
				E_NewPolicy__c = parentRecId
				,FUPSTAT__c = 'F'
			));
		}
		insert defects;
	}
}