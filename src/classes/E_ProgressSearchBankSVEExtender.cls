/**
 * 新契約申込進捗照会(BANK) Extenderクラス
 */
global with sharing class E_ProgressSearchBankSVEExtender extends E_AbstractSearchExtender {
	
	//extension
	E_ProgressSearchBankSVEController extension;
	
	public E_ProgressSearchBankSVEExtender(E_ProgressSearchBankSVEController extension) {
		super();
		this.extension = extension;
	}
	
	global override void init() {
		pageRef = doAuth(E_Const.ID_KIND.SPECIAL_FEATURES, null);
	}
	
	public Pagereference pageAction (){
		return E_Util.toErrorPage(pageRef, null);
	}
	
	global override void preSearch() {
		if (extension != null) {
			String iStoreCd = extension.iStoreCd_val.SkyEditor2__Text__C;
			String iStatus = extension.iStaus_val_dummy.SkyEditor2__Text__c;
			
			//#1 「取扱店番（支店コード）」がスペースの場合
			if (!E_Util.isNotNull(iStoreCd)) {
				pageMessages.addErrorMessage(getMSG().get('PGB|001'));
				throw new SkyEditor2.ExtenderException('');
			}
			//#2 「取扱店番（支店コード）」が数字４桁以外の場合
			if (!E_Util.isEnNum(iStoreCd) || iStoreCd.length() != 4) {
				pageMessages.addErrorMessage(getMSG().get('PGB|002'));
				throw new SkyEditor2.ExtenderException('');
			}
			//#4「契約ステータス」が一つも選択されていない場合
			if (!E_Util.isNotNull(iStatus)) {
				pageMessages.addErrorMessage(getMSG().get('PGB|003'));
				throw new SkyEditor2.ExtenderException('');
			}
		}
	}
}