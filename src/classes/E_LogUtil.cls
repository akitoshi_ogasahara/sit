/**
 * アクセスログ　共通クラス
 * NNLinkLog(E_Log__c)　オブジェクト
 */
public class E_LogUtil {
	/**		E_Log__cのDetail__c項目へ出力するフォーマット文字列	*/
	public static final String NN_LOG_DETAIL_FORMAT = '【{0}】 {1}   @@@ {2}';

	/**		E_Log__cのActionType項目へ出力する値*/
	public static final String NN_LOG_ACTIONTYPE_PAGE = 'ページ';
	public static final String NN_LOG_ACTIONTYPE_DOWNLOAD = 'ダウンロード';
	public static final String NN_LOG_ACTIONTYPE_SEARCH = '検索';
	/**
	 * ページ名取得
	 */
	public static String getPageName() {
		return ApexPages.currentPage().getUrl().split('/')[2].split('\\?')[0];
	}

	/**
	 * ログレコード生成(成功)
	 */
	public static E_Log__c getLogRec(String pageName, String detail){
		// E_Log__c
		return createLogDetail(pageName, detail, null, false);
	}

	/**
	 * ログレコード生成(エラー)
	 */
	public static E_Log__c getLogRec(String pageName, String detail, Boolean isError){
		// E_Log__c
		return createLogDetail(pageName, detail, null, isError);
	}

	/**
	 * ログレコード生成(成功: レコードIDあり)
	 */
	public static E_Log__c getLogRec(String pageName, String detail, String recId){
		// E_Log__c
		return createLogDetail(pageName, detail, recId, false);
	}

	/**
	 * ログレコード生成(エラー: レコードIDあり)
	 */
	public static E_Log__c getLogRec(String pageName, String detail, String recId, Boolean isError){
		// E_Log__c
		return createLogDetail(pageName, detail, recId, isError);
	}

	/**
	 * ログレコード生成
	 *
	 * ログインユーザ
	 * アクセスページ
	 * アクセス日時
	 * 詳細
	 * Accept
	 * Accept-Encoding
	 * Accept-Language
	 * Cache-Control
	 * CipherSuite
	 * Connection
	 * Host
	 * Upgrade-Insecure-Requests
	 * User-Agent
	 * X-Salesforce-Forwarded-To
	 * X-Salesforce-SIP
	 * X-Salesforce-VIP
	 */
	private static E_Log__c createLogDetail(String pageName, String detail, String recId, Boolean isError){
		// E_Log__c
		E_Log__c log = new E_Log__c();

		// エラー
		log.IsError__c = isError;
		// ログインユーザ
		log.AccessUser__c = UserInfo.getUserId();
		// アクセスページ
		log.AccessPage__c = String.isNotBlank(pageName) ? pageName : getPageName();
		// アクセス日時
		log.AccessDatetime__c = Datetime.now();
		// 詳細
		log.Detail__c = E_Util.null2Blank(detail);
		// レコードID
		log.RecordId__c = String.isNotBlank(recId) ? recId : '';

		// ヘッダー情報
		Map<String, String> headerMap = Apexpages.currentPage().getHeaders();
		log.Accept__c = E_Util.null2Blank(headerMap.get('Accept'));
		log.Accept_Encoding__c = E_Util.null2Blank(headerMap.get('Accept-Encoding'));
		log.Accept_Language__c = E_Util.null2Blank(headerMap.get('Accept-Language'));
		log.CipherSuite__c = E_Util.null2Blank(headerMap.get('CipherSuite'));
		log.Connection__c = E_Util.null2Blank(headerMap.get('Connection'));
		log.Host__c = E_Util.null2Blank(headerMap.get('Host'));
		log.User_Agent__c = E_Util.null2Blank(headerMap.get('User-Agent'));
		log.X_Salesforce_Forwarded_To__c = E_Util.null2Blank(headerMap.get('X-Salesforce-Forwarded-To'));
		log.X_Salesforce_SIP__c = E_Util.null2Blank(headerMap.get('X-Salesforce-SIP'));
		log.X_Salesforce_VIP__c = E_Util.null2Blank(headerMap.get('X-Salesforce-VIP'));

		return log;
	}

	/**
	 * ログレコード生成		最低限システム的に取得できる内容でログレコードを生成する
	 */
	public static E_Log__c createLog(){
		return createLog(null);
	}
	public static E_Log__c createLog(String appName){
		// E_Log__c
		E_Log__c log = new E_Log__c();

		// Name項目
		//log.Name = title;
		// 利用アプリケーション
		log.AppName__c = appName;
		// ログインユーザ
		log.AccessUser__c = UserInfo.getUserId();
		// アクセス日時
		log.AccessDatetime__c = Datetime.now();

		// レコードID
		//log.RecordId__c = String.isNotBlank(recId) ? recId : '';

		// ページ情報
		if(ApexPages.currentPage()!=null){
			if(log.actionType__c == null){
				log.actionType__c = NN_LOG_ACTIONTYPE_PAGE;
			}
			// アクセスページ
			log.AccessPage__c =  ApexPages.currentPage().getUrl();		//getPageName();

			//ヘッダー情報
			Map<String, String> headerMap = Apexpages.currentPage().getHeaders();
			log.Accept__c = E_Util.null2Blank(headerMap.get('Accept'));
			log.Accept_Encoding__c = E_Util.null2Blank(headerMap.get('Accept-Encoding'));
			log.Accept_Language__c = E_Util.null2Blank(headerMap.get('Accept-Language'));
			log.CipherSuite__c = E_Util.null2Blank(headerMap.get('CipherSuite'));
			log.Connection__c = E_Util.null2Blank(headerMap.get('Connection'));
			log.Host__c = E_Util.null2Blank(headerMap.get('Host'));
			log.User_Agent__c = E_Util.null2Blank(headerMap.get('User-Agent'));
			log.X_Salesforce_Forwarded_To__c = E_Util.null2Blank(headerMap.get('X-Salesforce-Forwarded-To'));
			log.X_Salesforce_SIP__c = E_Util.null2Blank(headerMap.get('X-Salesforce-SIP'));
			log.X_Salesforce_VIP__c = E_Util.null2Blank(headerMap.get('X-Salesforce-VIP'));
		}

		return log;
	}


	/**
	 *		getLogDetailString
	 *			param	severity, summary, detail
	 *				or	E_PageMessagesHolder.nlinkPageMessage
	 *				or	List<E_PageMessagesHolder.nlinkPageMessage>
	 *
	 *			return String	エラーメッセージをフォーマットした文字列を返す。
	 */
	public static String getLogDetailString(ApexPages.Severity severity, String summary, String detail){
		return String.format(NN_LOG_DETAIL_FORMAT
							, new List<String>{String.valueOf(severity), summary, E_Util.null2Blank(detail)});
	}
	public static String getLogDetailString(E_PageMessagesHolder.nlinkPageMessage msg){
		return getLogDetailString(new List<E_PageMessagesHolder.nlinkPageMessage>{msg});
	}
	public static String getLogDetailString(List<E_PageMessagesHolder.nlinkPageMessage> messages){
		List<String> logDetails = new List<String>();
		for(E_PageMessagesHolder.nlinkPageMessage msg:messages){
			if(msg!=null){
				logDetails.add(String.format(NN_LOG_DETAIL_FORMAT
											, new List<String>{msg.getSeverityString(), msg.summary, E_Util.null2Blank(msg.detail)}));
			}
		}
		if(logDetails.size()>0){
			return String.join(logDetails, '\r\n');
		}

		return null;
	}

	/**
	 *		requiredLogging
	 *			ログ取得対象ユーザの時True
	 */
	public static Boolean requiredLogging(){
		E_AccessController access = E_AccessController.getInstance();

		//EIDCのないユーザの場合もログを取得(ゲストユーザ除く)
		if(access.idcpf==null || access.idcpf.id==null){
			return !access.isGuest();
		}

		//社員または一般代理店の場合にログを取得
		return access.isNormalAgent() || access.isEmployee()				//TODO20161219 Day1 PreReleasePatchでは社員（BR**のみ）
				|| access.isSumiseiIRISUser();								//20180619 住生IRISユーザもログ登録をする
	}

}