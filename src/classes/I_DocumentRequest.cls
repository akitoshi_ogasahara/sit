public with sharing class I_DocumentRequest extends I_AbstractController {

	/**
	 *ENTER		//入力画面
	 *CONFIRM	//確認画面
	 *COMPLETE	//完了画面
	 */
	public enum RequestStatus {
		ENTER,
		CONFIRM,
		COMPLETE
	}

	//表示画面
	public RequestStatus currentStatus {get; set;}
	//key:資料カテゴリ value:資料リスト
	public Map<String, DocumentCategory> categoriesByName {get; set;}

	//お知らせリスト
	public List<I_ContentMaster__c> changeInfo{get;set;}

	//資料カテゴリアンカー用
	public MAP<String, String> getCategoryMap(){
		return I_Const.DR_DOCUMENT_CATEGORIE_MAP;
	}
	//表示資料のUpsertKeyリスト
	public List<String> upsertKeys{get{
		upsertKeys = new List<String>();
		for(DocumentCategory category : getCategories()){
			for(I_DocumentRequest.RequestItem item : category.items){
				upsertKeys.add(item.record.UpsertKey__c);
			}
		}
		return upsertKeys;
	}set;}
	//入力情報
	public RequestForm form {get; set;}
	//発注番号
	public String orderNo {get;private set;}
	//拠点長、MR、社内ユーザ
	public Boolean userIsMR {get{
		return access.isAuthManager() || access.isAuthMR() || access.isEmployee();
	} private set;}

	public I_DocumentRequest() {
		this.currentStatus = RequestStatus.ENTER;
		this.refresh();
		changeInfo = I_ContentMasterDao.getRecordByInfoName(I_Const.DR_INFO_LABEL, 5);
	}

	//ページアクション
	public PageReference toErrPage(){
		if(access.user.Contact.Account.CannotBeOrder__c){
			pageRef = Page.E_ErrorPage;
			pageRef.getParameters().put('code', E_Const.ERROR_MSG_FUNCTION);
			return pageRef;
		}
		return super.PageAction();
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = I_Const.DOCUMENT_REQUEST;

		return pr;
	}

	//表示中のページ名
	public String getCurrentStatusName() {
		return (this.currentStatus != null ? this.currentStatus.name() : null);
	}

	//メッセージ有無判定用
	public Boolean hasInternalMessage {get {
		return ApexPages.hasMessages();
	} private set; }
	//メッセージ
	public List<ApexPages.Message> getInternalMessages() {
		return ApexPages.getMessages();
	}

	//有効なメールアドレスが登録されているかどうかのチェック
	public Boolean availableEmail{
		get{
			return access.user.IsChangeMailaddress__c;
		}
	}

	//メールアドレス
	public String userEmail{
		get{return UserInfo.getUserEmail();}
	}

	public void stepToEnter() {
		this.currentStatus = RequestStatus.ENTER;
	}

	public void stepToConfirm() {
		this.validate();
		if (ApexPages.hasMessages()) {
			return;
		}
		this.currentStatus = RequestStatus.CONFIRM;
	}

	public void stepToComplete() {
		Savepoint sp = Database.setSavepoint();
		try{
			//発注情報登録
			I_OrderInfo__c oi = new I_OrderInfo__c(
				 Agency__c		= form.agencyName				//代理店名／事務所名
				,Agent__c		= form.agentName				//募集人名
				,PostCode__c	= form.postalCode				//郵便番号
				,Address01__c	= form.addressMain.left(30)		//住所1
				,Address02__c	= form.addressMain.mid(30,30)	//住所2
				,PhoneNo__c		= form.phoneNumber				//電話番号
				,AvailableFlag__c = '1'							//一時利用フラグ
				,AllSending__c	= ''							//一斉発送フラグ
				,CSOffice__c	= I_Const.CONEONE_OFFICE_CODE		//事業所コード
				,DeliveryDCode__c	= I_Const.CONEONE_OFFICE_CODE	//納品先コード
				,EStop__c		= ''							//停止
				,UseDivision__c	= '1'							//利用区分
				,ArrivalDate__c = null							//着日
			);
			insert oi;
			orderNo = [SELECT Name FROM I_OrderInfo__c WHERE Id = :oi.Id].Name;

			//ログ内容
			String logStr = '【納品先】';
			logStr += form.agencyName;
			logStr += '_' + form.addressMain;
			logStr += oi.Agent__c != '' ? '_' + oi.Agent__c : '';	//募集人が入力されている場合のみ

			logStr += '<br/>';
			logStr += '【発送物】';
			List<I_OrderDetails__c> ods = new List<I_OrderDetails__c>();
			for(DocumentCategory category : this.categoriesByName.values()){
				for(I_DocumentRequest.RequestItem item : category.getRequestingItems()){
					I_OrderDetails__c od = new I_OrderDetails__c(
						 ContentMaster__c	= item.record.Id		//CMSコンテンツ
						,FormNo__c			= item.record.FormNo__c	//フォームNo
						,OrderNum__c		= item.numberOfCopies	//数量
						,OrderInfo__c		= oi.Id					//発注情報
					);
					logStr += '_' + item.record.Name + ':' + item.numberOfCopies +'部';
					ods.add(od);
				}
			}
			insert ods;

			logStr = logStr.replace('】_', '】');
			if(logStr.length()>255){
				logStr = logStr.left(245) + '＜以降の情報は省略＞';
			}

			pageAccessLog = E_LogUtil.createLog('IRIS');
			pageAccessLog.Name = I_Const.DOCUMENT_REQUEST;
			pageAccessLog.SummaryName__c = logStr;
			pageAccessLog.ActionType__c = I_Const.DOCUMENT_REQUEST;
			pageAccessLog.AccessPage__c = ApexPages.currentPage().getUrl().split('\\?')[0];
			insert pageAccessLog;

			//メール送信（ワークフロー）
			if(availableEmail){
				oi.EMail__c = userEmail;
				update oi;
			}

		}catch(Exception e){
			System.debug('I_DocumentRequest '+e.getLineNumber()+'_'+e.getMessage());
			addMsg(I_Const.MESSAGE_KEY_REQUEST_ERROR);
			Database.rollback(sp);
			return;
		}

		refresh();

		this.currentStatus = RequestStatus.COMPLETE;
	}

	public void refresh() {
		this.form = new RequestForm();
		if(access.user.AccountId != null){
			if(access.user.Contact.Account.ParentZ1OFFING__c == 'Y'){
				form.agentName		= String.isBlank(UserInfo.getName()) ? '' : UserInfo.getName().left(15);
			}else if(access.isAuthAH() || access.isAuthAY() || access.isAuthAT()){
				form.agencyName		= String.isBlank(access.user.Contact.Account.E_CL2PF_ZEAYNAM__c) ? '' : access.user.Contact.Account.E_CL2PF_ZEAYNAM__c.left(30);
				form.agentName		= String.isBlank(UserInfo.getName()) ? '' : UserInfo.getName().left(15);
				form.postalCode		= access.user.Contact.Account.CLTPCODE__c;
				form.addressMain	= String.isBlank(access.user.Contact.Account.E_COMM_ZCLADDR__c) ? '' : access.user.Contact.Account.E_COMM_ZCLADDR__c.left(60);
				form.phoneNumber	= access.user.Contact.Account.CLTPHONE01__c;
			}
		}

		this.categoriesByName = buildCategoriesByName();
	}

	//入力チェック
	public void validate() {
		Boolean hasRequesting = false;
		for (DocumentCategory category: this.getCategories()) {
			if (category.hasRequestingItem) {
				hasRequesting = true;
				break;
			}
		}

		//代理店名／事務所名
		if (String.isEmpty(this.form.agencyName)) {
			addMsg(I_Const.MESSAGE_KEY_NO_AGENCYNAME);
		} else {
			form.agencyName = form.agencyName.replaceAll(' ','　');
			if (form.agencyName.length() > 30) {
				addMsg(I_Const.MESSAGE_KEY_SIZEOVER_AGENCYNAME);
			}
			if (!E_Util.isEm(form.agencyName)) {
				addMsg(I_Const.MESSAGE_KEY_EM_AGENCYNAME);
			}
			checkSymbol('代理店／事務所名', form.agencyName);
		}
		//募集人名
		if (!String.isEmpty(this.form.agentName) && !E_Util.isEm(form.agentName)) {
			addMsg(I_Const.MESSAGE_KEY_EM_AGENTNAME);
		}
		if(!String.isEmpty(this.form.agentName)){
			form.agentName = form.agentName.replaceAll(' ','　');
			if(form.agentName.length() > 15) {
				addMsg(I_Const.MESSAGE_KEY_SIZEOVER_AGENTNAME);
			}
			checkSymbol('募集人名', form.agentName);
		}
		//郵便番号
		if (String.isEmpty(this.form.postalCode)) {
			addMsg(I_Const.MESSAGE_KEY_NO_POSTALCODE);
		} else {
			if (this.form.postalCode.length() > 9) {
				addMsg(I_Const.MESSAGE_KEY_SIZEOVER_POSTALCODE);
			}
			if (!Pattern.matches('^[0-9０-９ー-]+$', this.form.postalCode)) {
				addMsg(I_Const.MESSAGE_KEY_WRONG_POSTALCODE);
			}
		}
		//住所
		if (String.isEmpty(this.form.addressMain)) {
			addMsg(I_Const.MESSAGE_KEY_NO_ADDRESS);
		} else {
			form.addressMain = form.addressMain.replaceAll(' ','　');
			if (form.addressMain.length() > 60) {
				addMsg(I_Const.MESSAGE_KEY_SIZEOVER_ADDRESS);
			}
			if (!E_Util.isEm(form.addressMain)) {
				addMsg(I_Const.MESSAGE_KEY_EM_ADDRESS);
			}
			checkSymbol('住所', form.addressMain);
		}
		//電話番号
		if (String.isEmpty(this.form.phoneNumber)) {
			addMsg(I_Const.MESSAGE_KEY_NO_PHONENO);
		} else {
			if (this.form.phoneNumber.length() > 17) {
				addMsg(I_Const.MESSAGE_KEY_SIZEOVER_PHONENO);
			}
			if (!Pattern.matches('^[0-9０-９ー-]+$', this.form.phoneNumber)) {
				addMsg(I_Const.MESSAGE_KEY_WRONG_PHONENO);
			}
		}

		if (!hasRequesting) {
			addMsg(I_Const.MESSAGE_KEY_NO_REQUEST);
		}
	}

	private void addMsg(String key){
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(key)));
	}

	//特殊文字チェック
	private void checkSymbol(String itemName, String word){
		String symbolS = '\\,;%*<>&\"\'＼，；％＊＜＞＆”’';
		if(word.containsAny(symbolS)){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, itemName + 'に' + getMSG().get(I_Const.MESSAGE_KEY_WRONG_SYMBOL)));
		}
	}

	/**
	 * 資料カテゴリ名から、該当資料リストを取得するMAP
	 * key:資料カテゴリ名
	 * value:資料リスト
	 */
	private static Map<String, DocumentCategory> buildCategoriesByName() {
		Map<String, DocumentCategory> categoriesByName = new Map<String, DocumentCategory>();
		for(String category : I_Const.DR_DOCUMENT_CATEGORIES){
			categoriesByName.put(category, new DocumentCategory(category));
		}

		for (I_ContentMaster__c record: I_ContentMasterDao.retrieveDocumentalContents(categoriesByName.keySet())) {
			RequestItem item = new RequestItem(record);
			categoriesByName.get(item.getCategory()).items.add(item);
		}
		return categoriesByName;
	}

	public List<DocumentCategory> getCategories() {
		return this.categoriesByName.values();
	}

	//資料カテゴリ別情報
	public class DocumentCategory {
		public List<I_DocumentRequest.RequestItem> items {get; private set;}
		public String name {get; set;}
		public Boolean hasRequestingItem {get {
			for (I_DocumentRequest.RequestItem item: items) {
				if (item.numberOfCopies > 0) {
					return true;
				}
			}
			return false;
		} private set;}
		public Boolean hasItem {get {
			return this.items.size() > 0;
		} private set;}

		public DocumentCategory(String name) {
			this.name = name;
			this.items = new List<I_DocumentRequest.RequestItem>();
		}

		public List<I_DocumentRequest.RequestItem> getRequestingItems() {
			List<I_DocumentRequest.RequestItem> result = new List<I_DocumentRequest.RequestItem>();
			for (RequestItem item: items) {
				if (!item.canRequest) continue;
				if (item.numberOfCopies <= 0) continue;
				if (item.numberOfCopies > item.getMaxNumberOfCopies()) {
					item.numberOfCopies = item.getMaxNumberOfCopies();
				}
				result.add(item);
			}
			return result;
		}

	}

	//資料情報
	public class RequestItem {
		public I_ContentMaster__c record {get; private set;}
		public Integer numberOfCopies {get; set;}
		public Boolean canRequest {get {
			return !this.record.CannotBeOrder__c;
		} private set;}
		public Boolean isMax {get {
			return this.numberOfCopies >= this.getMaxNumberOfCopies();
		} private set;}

		public RequestItem(I_ContentMaster__c record) {
			this.record = record;
			this.numberOfCopies = 0;
		}

		public String getUpsertKey() {
			return this.record.UpsertKey__c;
		}
		public String getName() {
			return this.record.Name;
		}

		public String getFormNo() {
			return this.record.FormNo__c;
		}

		public Date getDisplayFrom() {
			return this.record.DisplayFrom__c;
		}

		public Date getNewValidTo() {
			return this.record.NewValidTo__c;
		}

		public String getCategory() {
			return this.record.DocumentCategory__c;
		}
		public Integer getMaxNumberOfCopies() {
			return this.record.MaxCopies__c != null ? this.record.MaxCopies__c.intValue() : 0;
		}
	}

	public class RequestForm extends I_AddresseeInformationDTO {}

	class PageException extends Exception {}

}