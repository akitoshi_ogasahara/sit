public with sharing class E_SVCPFDao {

    /**
     * 特別勘定のIDセットをキーにリストを取得する
     * @param idSet: 特別勘定のIDセット
     * @return List<E_SVCPF__c>: 特別勘定情報リスト
     */
    public static List<E_SVCPF__c> getRecsByPolicyId(Set<Id> idSet){
        return [Select
                    Id,
                    Name,
                    ZFUNDNAMEUNION__c,
                    ZFUNDNAMEUNIONEmail__c,
                    ZEQTYRTO__c,
                    ZEFUNDCD__c,
                    ZCOLORCD__c,
                    ZVINDEX__c
                From
                    E_SVCPF__c
                Where
                    Id In :idSet
                And
                    CURRFROM__c <= :E_Util.getSysDate(null)
                And
                    FLAG01__c = true
                Order by
                    ZEFUNDCD__c
               ];
    }

    /**
     * ファンド区分が『COLI』の特別勘定を取得する(COLI)
     * @param
     * @return List<E_SVCPF__c>: 特別勘定情報リスト
     */
    public static List<E_SVCPF__c> getRecsByCOLI(){
        return [Select
                    Id,
                    Name,
                    ZFUNDNAMEUNION__c,
                    ZFUNDNAMEUNIONEmail__c,
                    ZEQTYRTO__c,
                    ZEFUNDCD__c,
                    ZCOLORCD__c,
                    ZFSHFROM__c,
                    ZFSDFROM__c,
                    ZFSMFROM__c,
                    ZFSHTO__c,
                    ZFSDTO__c,
                    ZFSMTO__c,
                    FLAG02__c
                From
                    E_SVCPF__c
                Where
                    ZCATFLAG__c = :E_Const.FUND_KIND_COLI
                And
                    ((CURRFROM__c <= :E_Util.formatDataSyncDate('yyyyMMdd') And CURRTO__c > :E_Util.formatDataSyncDate('yyyyMMdd')) And CURRFROM__c <> :'0')
        		And 
        			((CURRFROM__c != null And CURRFROM__c <= :E_Util.getSysDate(null)) And FLAG01__c = true)
                And
                    ZEFUNDCD__c <> :E_Const.NOT_FUND_COLI
                Order by
                    ZEFUNDCD__c
               ];
    }

    /**
     * 積立金移転の申込における特別勘定の取得を行う。 
     * V002は除外しない。
     * @param
     * @return List<E_SVCPF__c>: 特別勘定情報リスト
     */
    public static List<E_SVCPF__c> getRecsByFundTransfer(){
        return [Select
                    Id,
                    Name,
                    ZFUNDNAMEUNION__c,
                    ZFUNDNAMEUNIONEmail__c,
                    ZEQTYRTO__c,
                    ZEFUNDCD__c,
                    ZCOLORCD__c,
                    ZFSHFROM__c,
                    ZFSDFROM__c,
                    ZFSMFROM__c,
                    ZFSHTO__c,
                    ZFSDTO__c,
                    ZFSMTO__c,
                    FLAG02__c
                From
                    E_SVCPF__c
                Where
                    ZCATFLAG__c = :E_Const.FUND_KIND_COLI
                And
                    ((CURRFROM__c <= :E_Util.formatDataSyncDate('yyyyMMdd') And CURRTO__c > :E_Util.formatDataSyncDate('yyyyMMdd')) And CURRFROM__c <> :'0')
                And 
                    ((CURRFROM__c != null And CURRFROM__c <= :E_Util.getSysDate(null)) And FLAG01__c = true)
                Order by
                    ZEFUNDCD__c
               ];
    }



    /**
     * ファンド区分が『SPVA』、特別勘定のIDセットをキーにリストを取得する(SPVA)
     * @param idSet: 特別勘定のIDセット
     * @return List<E_SVCPF__c>: 特別勘定情報リスト
     */
    public static List<E_SVCPF__c> getRecsByPolicyIdSPVA(Set<Id> idSet){
        return [Select
                    Id,
                    Name,
                    ZFUNDNAMEUNION__c,
                    ZFUNDNAMEUNIONEmail__c,
                    ZEQTYRTO__c,
                    ZEFUNDCD__c,
                    ZVINDEX__c,
                    ZCOLORCD__c,
                    ZFSHFROM__c,
                    ZFSDFROM__c,
                    ZFSMFROM__c,
                    ZFSHTO__c,
                    ZFSDTO__c,
                    ZFSMTO__c,
                    FLAG02__c
                From
                    E_SVCPF__c
                Where
                    Id In :idSet
                And
                    ZCATFLAG__c = :E_Const.FUND_KIND_SPVA
                And
                    CURRFROM__c <= :E_Util.getSysDate('yyyyMMdd')
                And
                    FLAG01__c = true
                Order by
                    ZEFUNDCD__c
               ];
    }
}