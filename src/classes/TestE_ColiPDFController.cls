@isTest
private with sharing class TestE_ColiPDFController{
	private static testMethod void testPageMethods() {		E_ColiPDFController extension = new E_ColiPDFController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testCRLPFPDFtable() {
		E_ColiPDFController.CRLPFPDFtable CRLPFPDFtable = new E_ColiPDFController.CRLPFPDFtable(new List<E_CRLPF__c>(), new List<E_ColiPDFController.CRLPFPDFtableItem>(), new List<E_CRLPF__c>(), null);
		CRLPFPDFtable.create(new E_CRLPF__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableCOVPF() {
		E_ColiPDFController.dataTableCOVPF dataTableCOVPF = new E_ColiPDFController.dataTableCOVPF(new List<E_COVPF__c>(), new List<E_ColiPDFController.dataTableCOVPFItem>(), new List<E_COVPF__c>(), null);
		dataTableCOVPF.create(new E_COVPF__c());
		System.assert(true);
	}
	
}