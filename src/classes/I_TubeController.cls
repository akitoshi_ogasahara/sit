/**
 *		/apex/IRIS_Tube	pageのController
 *				1./apex/IRIS_Tube?url=xxxx　：　Sessionを追加してurlへリダイレクト
 *				2./apex/IRIS_Tube?category=newOpp :	該当カテゴリトップに遷移
 */
public with sharing class I_TubeController extends I_AbstractController {
	public static final String PARAM_TUBE_URL = 'url';
	public static final String PARAM_VALUE_NNTUBE = 'nntube';

//-------------------------------------------------------
//						DEBUG   TODO DEL
	public string getDebug(){
		return null;
	}

	public String getURLValues(){
		Map<String,String> params = ApexPages.currentPage().getParameters();
		String s = '==========';
		for(String key:params.keyset()){
			s += key + ':' + params.get(key) + '<br/>';
		}
		
		return '[baseUrl]' + URL.getSalesforceBaseUrl().toExternalForm() + '<br/>'
				+ '[requestUrl]' + URL.getCurrentRequestUrl().toExternalForm()+ '<br/>' 
				+ '[Host]:'+ URL.getCurrentRequestUrl().getPath()+ '<br/>'
				+ '[Authority]:'+ URL.getCurrentRequestUrl().getAuthority()+ '<br/>'
				+ '[Path]:'+ URL.getCurrentRequestUrl().getPath()+ '<br/>'
				+ '[Ref]:'+ URL.getCurrentRequestUrl().getRef()+ '<br/>'
				+ '[Query]:'+ URL.getCurrentRequestUrl().getQuery()+ '<br/>'
				+ '[Site.getName()]:'+ Site.getName() + '<br/>'
				+ s
		;
	}

//-------------------------------------------------------


	//ページ処理処理
	protected override PageReference init(){
		// url指定の場合は、SessionID追加してurlへリダイレクト
/*
		<apex:outputLink value="{!'https://www.nntube.jp/auth.aspx?session=' +  GETSESSIONID() 
									+ '&user=' + $User.E_ZWEBID__c 
									+ '&sfid=' + $User.Id 
									+ '&domain=' + URLFOR('/')}"

*/
		String pUrl = ApexPages.currentPage().getParameters().get(PARAM_TUBE_URL);
		if(String.isNotBlank(pUrl)){
			I_TubeConnectHelper connector = I_TubeConnectHelper.getInstance();
			//url=nntubeの場合にTubeTop画面へ遷移させる
			if(pUrl == PARAM_VALUE_NNTUBE){
				pUrl = connector.getTubeTopUrl();
			}
			PageReference pr = new PageReference(pUrl);
			pr.getParameters().putAll(connector.getUrlParamMapForNNTube());
			return pr;
		}
	
		// カテゴリ指定の場合は、カテゴリTOPへリダイレクト
		String pCate = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_CATEGORY); //category
		//20180725動画遷移用START
		String docNo = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_DOC_NUM); //docnumber
		if(String.isNotBlank(pCate)){
			return getRedirectTo(pCate,docNo);
		}
		//20180725動画遷移用END

		//if(String.isNotBlank(pCate)){
		//	return getRedirectTo(pCate);
		//}
		
		//return null;
		return Page.IRIS_Top;
	}
	
	//20180725動画遷移用START
	private PageReference getRedirectTo(String pCate, String docNo){
	//private PageReference getRedirectTo(String pCate){
		
		String cateName = I_Const.CATEGORY_ALIAS_NAME.get(pCate);
		
		if(String.isNotBlank(cateName)){
			I_GlobalNavController gNav = new I_GlobalNavController();
			
			//---	サブカテゴリ指定の場合	---
			//'lib_tool' => 'ライブラリ＞セールスツール'
			if(cateName == 'lib_tool'){
				if(String.isNotBlank(docNo)){
					return replaceUrl('iris_lib_tools?docnumber='+ docNo + '#iris-cms-'+ docNo);
				}
				return replaceUrl('iris_lib_tools');
			}
			//'lib_mnt' => 'ライブラリ＞保全手続書類',
			if(cateName == 'lib_mnt'){
				return replaceUrl(gnav.libSubNavItems.getLibInfoNavItem().getCategoryTopUrl());
			}
			//'lib_manual' => 'ライブラリ＞マニュアル',
			if(cateName == 'lib_manual'){
				return replaceUrl(gnav.libSubNavItems.getLibManualNavItem().getCategoryTopUrl());
			}
			//'lib_other' => 'ライブラリ＞その他',
			if(cateName == 'lib_other'){
				return replaceUrl(gnav.libSubNavItems.getLibOtherNavItem().getCategoryTopUrl());
			}
			//'lib_feat' => 'ライブラリ＞特集'
			if(cateName == 'lib_feat'){
				return replaceUrl('iris_featurelist');
			}

			//---	メインカテゴリから遷移先を特定	---
			for(I_GlobalNavController.gNavItem gi:gNav.getgNavItems()){
				if(cateName == gi.record.MainCategory__c){
					return replaceUrl(gi.getCategoryTopUrl());
				}
			}
		}
		return Page.iris_Top;		//return Page.iris_top;  TODO　取得できない場合IRISTOPとすることを検討
	}

	//20180725動画遷移用END
	
	//iris_tube部分を新しいURLで置換
	private PageReference replaceUrl(String toUrl){
		String retUrl = URL.getCurrentRequestUrl().getPath();
		retUrl = retUrl.toLowerCase().replaceFirst('iris_tube', toUrl);		
		PageReference pr = new PageReference(retUrl);
		return pr;
	}
}