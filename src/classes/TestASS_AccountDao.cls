@isTest
private class TestASS_AccountDao {
	//エリア部長で実行
	private static testMethod void getOfficeByIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			Account retAcc = ASS_AccountDao.getOfficeById(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retAcc.Id,office.Id);
		}
	
	}

	private static testMethod void getOfficesByIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Account> retAccList = ASS_AccountDao.getOfficesById(mr.Id,office.Id);

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retAccList.size(),1);
		}
	
	}

	private static testMethod void getOfiicesByMRIdANDLoginUsIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Account> retAccList = ASS_AccountDao.getOfiicesByMRIdANDLoginUsId(mr.Id,actUser.Id);

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retAccList.size(),1);
		}
	
	}

	//MRで実行
	private static testMethod void getOfficeByIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			Account retAcc = ASS_AccountDao.getOfficeById(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retAcc.Id,office.Id);
		}
	
	}

	private static testMethod void getOfficesByIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Account> retAccList = ASS_AccountDao.getOfficesById(mr.Id,office.Id);

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retAccList.size(),1);
		}
	
	}

	private static testMethod void getOfiicesByMRIdANDLoginUsIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Account> retAccList = ASS_AccountDao.getOfiicesByMRIdANDLoginUsId(mr.Id,actUser.Id);

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retAccList.size(),1);
		}
	
	}
}