@isTest
private with sharing class TestE_ProcessHistoryController{
	private static testMethod void testPageMethods() {		E_ProcessHistoryController extension = new E_ProcessHistoryController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testinsuredTab() {
		E_ProcessHistoryController.insuredTab insuredTab = new E_ProcessHistoryController.insuredTab(new List<E_PTNPF__c>(), new List<E_ProcessHistoryController.insuredTabItem>(), new List<E_PTNPF__c>(), null);
		insuredTab.create(new E_PTNPF__c());
		System.assert(true);
	}
	
	private static testMethod void testinveHistoryTab() {
		E_ProcessHistoryController.inveHistoryTab inveHistoryTab = new E_ProcessHistoryController.inveHistoryTab(new List<E_ITTPF__c>(), new List<E_ProcessHistoryController.inveHistoryTabItem>(), new List<E_ITTPF__c>(), null);
		inveHistoryTab.create(new E_ITTPF__c());
		System.assert(true);
	}
	
}