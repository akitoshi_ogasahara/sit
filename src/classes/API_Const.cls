/**
 * API用定数を定義するクラス
 * CreatedDate 2018/10/19
 * @Author Terrasky
 */
public class API_Const {

	//エラーコードとエラーメッセージのマップ
	public static final Map<String,API_ErrorDefined> errorMessagesMap =  new Map<String,API_ErrorDefined>{
		'AUTH_NO_ACCESS' => new API_ErrorDefined('001','ユーザー権限がありません'),
		'AUTH_NO_COLI' => new API_ErrorDefined('002','個人保険照会権限がありません'),
		'AUTH_NO_REASON' => new API_ErrorDefined('003','アクセス権取得時にエラーが発生しました'),
		'NO_COLI_RECORDS' => new API_ErrorDefined('004','保険契約が存在しません'),
		'COLI_DATA_CORRUPTION' => new API_ErrorDefined('005','保険契約のデータが不正です')
	};

	//エラー定義格納クラス
	public class API_ErrorDefined{
		public String code {get;set;}
		public String msg {get;set;}

		public API_ErrorDefined(){
			this.code = '';
			this.msg = '';
		}

		public API_ErrorDefined(String code,String msg){
			this.code = code;
			this.msg = msg;
		}
	}
}