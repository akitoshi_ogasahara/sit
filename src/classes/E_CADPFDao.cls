public with sharing class E_CADPFDao {

    /**
     * IDをキーに、住所管理情報を取得する
     * @param cadpfId: 住所管理ID
     * @return E_CADPF__c: 住所管理情報
     */
    public static E_CADPF__c getRecById (Id cadpfId) {
        E_CADPF__c rec = null;
        List<E_CADPF__c> recs = [
            Select 
                Id, ZPOSTCD__c
            From 
                E_CADPF__c
            Where 
                Id = :cadpfId
        ];
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
    }
}