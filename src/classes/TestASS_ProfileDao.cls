@isTest
private class TestASS_ProfileDao {
	//エリア部長で実行
	private static testMethod void getProfileByIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			Profile retProf = ASS_ProfileDao.getProfileById(UserInfo.getProfileId());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retProf.Name,'エリア部長');
		}
	
	}

	//MRで実行
	private static testMethod void getProfileByIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			Profile retProf = ASS_ProfileDao.getProfileById(UserInfo.getProfileId());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retProf.Name,'ＭＲ');
		}
	
	}
}