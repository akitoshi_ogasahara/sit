@isTest
private class TestI_TotalController {

	/**
	 * topMenusのテスト
	 */

	@isTest static void topMenusTest() {

		Test.startTest();

		I_TotalController totalController	= new I_TotalController();

		Map<String, I_MenuItem> topMenus = totalController.topMenus;

		System.assertEquals(0, topMenus.size());

		Test.stopTest();
	}

	/**
	 * getRecordInfo(その他)のテスト
	 */

	@isTest static void getRecordInfoTest_TypeOther001() {

		E_BizDataSyncLog__c recBizDataSyncLog = new E_BizDataSyncLog__c();
		recBizDataSyncLog.DataSyncEndDate__c = null;
		recBizDataSyncLog.Kind__c = 'E';
		insert recBizDataSyncLog;
		createIRISMenu();

		Test.startTest();
		String temp			= '';
		String answer		= '';
		Date d				= null;

		I_TotalController totalController	= new I_TotalController();

		temp = I_TotalController.getRecordInfo('UpdateTime','','',false);
		System.assertEquals('データ更新中',temp);

		// 期間
		d = Date.today().addMonths(-1);
		answer = Datetime.newInstance(d.year(), d.month(), 1).format('yyyy/MM/dd');
		answer += '～';
		d = Date.today();
		answer += Datetime.newInstance(d.year(), d.month(), d.day()).format('yyyy/MM/dd');

		temp = I_TotalController.getRecordInfo('Period1','','',false);
		System.assertEquals(answer,temp);

		// 期間
		d = Date.today().addMonths(-1);
		answer = Datetime.newInstance(d.year(), d.month(), 1).format('yyyy/MM/dd');
		answer += '～';
		d = Date.newInstance(Date.today().year(), Date.today().month(), 1).addDays(-1);
		answer += Datetime.newInstance(d.year(), d.month(), d.day()).format('yyyy/MM/dd');

		temp = I_TotalController.getRecordInfo('Period2','','',false);
		System.assertEquals(answer,temp);


		// 推移
		Pagereference pr = totalController.moveToPage();
		System.assertEquals('', pr.getUrl());

		// 未指定
		temp = I_TotalController.getRecordInfo('','','',false);
		System.assertEquals('',temp);

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		pageRef.getParameters().put('next_urltype', 'IRIS_Users');
		Test.setCurrentPage(pageRef);

		pr = totalController.moveToPage();
		//System.assertEquals('', pr.getUrl());
		System.debug(pr.getUrl());

		Test.stopTest();
	}


	/**
	 * getRecordInfo(その他)のテスト
	 */
	@isTest static void getRecordInfoTest_TypeOther002() {
		E_BizDataSyncLog__c recBizDataSyncLog = new E_BizDataSyncLog__c();
		recBizDataSyncLog.DataSyncEndDate__c =  Datetime.now();
		recBizDataSyncLog.Kind__c = 'E';
		insert recBizDataSyncLog;

		Test.startTest();
		String temp = '';

		temp = I_TotalController.getRecordInfo('UpdateTime','','',false);
		System.assertEquals(recBizDataSyncLog.DataSyncEndDate__c.format('yyyy/MM/dd HH:mm'),temp);
		Test.stopTest();
	}

	/**
	 * getRecordInfo(新契約)のテスト
	 */

	@isTest static void getRecordInfoTest_TypeNew() {

		User userMR		= new User();		// MRユーザ
		Account accAH	= new Account();	// 代理店格
		Account accAY	= new Account();	// 代理店事務所
		Contact conAT	= new Contact();	// 募集人
		User userAT		= new User();		// 募集人ユーザ

		User runUser = [select id from user where id =: UserInfo.getUserId()][0];
		System.RunAs(runUser){
			// Profileマップ取得
			Map<String, Id> pMap = getProfileIdMap();

			// MRユーザの作成
			userMR	= createUser('MR001', pMap.get('ＭＲ'), '東京営業部', '東日本', '1');

			// 代理店格の作成
			accAH	= createAccount(userMR, 'ah001');

			// 代理店事務所の作成
			accAY	= createAccount(userMR, 'ay001', accAH);

			// 募集人の作成
			conAT	= createContact(accAH, accAY, 'at001');
			userAT	= createUser('TEST', pMap.get('E_PartnerCommunity_IA'), '', '', conAT);

			// 新契約ステータスの作成(個人)
			List<E_NewPolicy__c> newPolicys= new List<E_NewPolicy__c>();
			newPolicys = createNewPolicy(accAH, accAY, conAT, '');
			// 新契約ステータスの作成(団体)
			newPolicys = createNewPolicy(accAH, accAY, conAT, 'G0001');
		}

		Test.startTest();


		I_TotalController totalController	= new I_TotalController();
		String temp			= '';
		String answer		= '"est":2,"ack":2,"wit":2,"ackwit":0,"arr":2,"def":2,"acc":2,"ass":2';

		// 新契約(条件なし)
		temp = I_TotalController.getRecordInfo('New','','',false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// フィルタ条件 - 営業部
		temp = I_TotalController.getRecordInfo('New',I_Const.CONDITION_FIELD_UNIT, '1',false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// フィルタ条件 - MR
		temp = I_TotalController.getRecordInfo('New',I_Const.CONDITION_FIELD_MR, userMR.Id,false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// フィルタ条件 - 代理店
		temp = I_TotalController.getRecordInfo('New',I_Const.CONDITION_FIELD_AGENCY, accAH.Id,false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// フィルタ条件 - 事務所
		temp = I_TotalController.getRecordInfo('New',I_Const.CONDITION_FIELD_OFFICE, accAY.Id,false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// フィルタ条件 - 募集人
		temp = I_TotalController.getRecordInfo('New',I_Const.CONDITION_FIELD_AGENT, conAT.Id,false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		Test.stopTest();
	}

	/**
	 * getRecordInfo(保有契約)のテスト
	 */

	@isTest static void getRecordInfoTest_TypePossessionAgreement() {

		User userMR		= new User();		// MRユーザ
		Account accAH	= new Account();	// 代理店格
		Account accAY	= new Account();	// 代理店事務所
		Contact conAT	= new Contact();	// 募集人
		User userAT		= new User();		// 募集人ユーザ

		User runUser = [select id from user where id =: UserInfo.getUserId()][0];
		System.RunAs(runUser){
			// Profileマップ取得
			Map<String, Id> pMap = getProfileIdMap();

			// MRユーザの作成
			userMR	= createUser('MR001', pMap.get('ＭＲ'), '東京営業部', '東日本', '1');

			// 代理店格の作成
			accAH	= createAccount(userMR, 'ah001');

			// 代理店事務所の作成
			accAY	= createAccount(userMR, 'ay001', accAH);

			// 募集人の作成
			conAT	= createContact(accAH, accAY, 'at001');
			userAT	= createUser('TEST', pMap.get('E_PartnerCommunity_IA'), '', '', conAT);

			// 保有契約関連の作成
			createPossessionAgreement(UserInfo.getUserId(), accAY, conAT);

		}

		Test.startTest();


		I_TotalController totalController	= new I_TotalController();
		String temp			= '';
		String answer		= '';

		answer		= '1';

		// 新契約(未入金通知 条件なし)
		temp = I_TotalController.getRecordInfo('NCOL','','',false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// 新契約(請求通知 条件なし)
		temp = I_TotalController.getRecordInfo('AGPO','','',false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// 新契約(予告通知 条件なし)
		temp = I_TotalController.getRecordInfo('BILS','','',false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// 新契約(異動通知 条件なし)
		temp = I_TotalController.getRecordInfo('BILA','','',false);
		//system.debug(temp);
		System.assertEquals(answer, temp);

		// フィルタ条件 - 営業部
		temp = I_TotalController.getRecordInfo('NCOL',I_Const.CONDITION_FIELD_UNIT, '1',false);
		system.debug('営業部:' + temp);
		//System.assertEquals(answer, temp);

		// フィルタ条件 - MR
		temp = I_TotalController.getRecordInfo('NCOL',I_Const.CONDITION_FIELD_MR, userMR.Id,false);
		system.debug('MR:' + temp);
		//System.assertEquals(answer, temp);

		// フィルタ条件 - 代理店
		temp = I_TotalController.getRecordInfo('NCOL',I_Const.CONDITION_FIELD_AGENCY, accAH.Id,false);
		system.debug('代理店:' + temp);
		//System.assertEquals(answer, temp);

		// フィルタ条件 - 事務所
		temp = I_TotalController.getRecordInfo('NCOL',I_Const.CONDITION_FIELD_OFFICE, accAY.Id,false);
		system.debug('事務所:' + temp);
		//System.assertEquals(answer, temp);

		// フィルタ条件 - 募集人
		temp = I_TotalController.getRecordInfo('NCOL',I_Const.CONDITION_FIELD_AGENT, conAT.Id,false);
		system.debug('募集人:' + temp);
		//System.assertEquals(answer, temp);

		Test.stopTest();
	}

	/**
	 * getRecordInfo(保有契約 月後)のテスト
	 */

	@isTest static void getRecordInfoTest_TypePossessionAgreementMonth() {

		User userMR		= new User();		// MRユーザ
		Account accAH	= new Account();	// 代理店格
		Account accAY	= new Account();	// 代理店事務所
		Contact conAT	= new Contact();	// 募集人
		User userAT		= new User();		// 募集人ユーザ

		User runUser = [select id from user where id =: UserInfo.getUserId()][0];
		System.RunAs(runUser){
			// Profileマップ取得
			Map<String, Id> pMap = getProfileIdMap();

			// MRユーザの作成
			userMR	= createUser('MR001', pMap.get('ＭＲ'), '東京営業部', '東日本', '1');

			// 代理店格の作成
			accAH	= createAccount(userMR, 'ah001');

			// 代理店事務所の作成
			accAY	= createAccount(userMR, 'ay001', accAH);

			// 募集人の作成
			conAT	= createContact(accAH, accAY, 'at001');
			userAT	= createUser('TEST', pMap.get('E_PartnerCommunity_IA'), '', '', conAT);

			// 保有契約 月後関連の作成
			createPossessionAgreementMonth(userMR.Id, accAY, conAT);

		}

		Test.startTest();
		System.RunAs(userMR){

			I_TotalController totalController	= new I_TotalController();
			String temp			= '';
			String answer		= '';

			answer		= '6';

			// 契約応当日(1～3ヶ月後 条件なし)
			temp = I_TotalController.getRecordInfo('CounterM1','','',false);
			system.debug(temp);
			//System.assertEquals(answer, temp);

			// 契約応当日(4～6ヶ月後 条件なし)
			temp = I_TotalController.getRecordInfo('CounterM4','','',false);
			system.debug(temp);
			//System.assertEquals(answer, temp);

			// フィルタ条件 - 営業部
			temp = I_TotalController.getRecordInfo('CounterM1',I_Const.CONDITION_FIELD_UNIT, '1',false);
			system.debug('営業部:' + temp);
			//System.assertEquals(answer, temp);

			// フィルタ条件 - MR
			temp = I_TotalController.getRecordInfo('CounterM1',I_Const.CONDITION_FIELD_MR, userMR.Id,false);
			system.debug('MR:' + temp);
			//System.assertEquals(answer, temp);

			// フィルタ条件 - 代理店
			temp = I_TotalController.getRecordInfo('CounterM1',I_Const.CONDITION_FIELD_AGENCY, accAH.Id,false);
			system.debug('代理店:' + temp);
			//System.assertEquals(answer, temp);

			// フィルタ条件 - 事務所
			temp = I_TotalController.getRecordInfo('CounterM1',I_Const.CONDITION_FIELD_OFFICE, accAY.Id,false);
			system.debug('事務所:' + temp);
			//System.assertEquals(answer, temp);

			// フィルタ条件 - 募集人
			temp = I_TotalController.getRecordInfo('CounterM1',I_Const.CONDITION_FIELD_AGENT, conAT.Id,false);
			system.debug('募集人:' + temp);
			//System.assertEquals(answer, temp);

		}
		Test.stopTest();
	}

	/**
	 * getRecordInfo(ランキング)のテスト
	 */
	@isTest static void getRecordInfoTest_TypeRanking() {

		User userMR		= new User();		// MRユーザ
		Account accAH01	= new Account();	// 代理店格
		Account accAH11	= new Account();	// 代理店格
		Account accAY01	= new Account();	// 代理店事務所
		Account accAY11	= new Account();	// 代理店事務所
		Contact conAT01	= new Contact();	// 募集人
		Contact conAT02	= new Contact();	// 募集人
		Contact conAT11	= new Contact();	// 募集人
		Contact conAT12	= new Contact();	// 募集人
		User userAT01	= new User();		// 募集人ユーザ
		User userAT02	= new User();		// 募集人ユーザ
		User userAT11	= new User();		// 募集人ユーザ
		User userAT12	= new User();		// 募集人ユーザ

		User runUser = [select id from user where id =: UserInfo.getUserId()][0];
		System.RunAs(runUser){
			// Profileマップ取得
			Map<String, Id> pMap = getProfileIdMap();

			// MRユーザの作成
			userMR	= createUser('MR001', pMap.get('ＭＲ'), '東京営業部', '東日本', '1');

			// 代理店格の作成
			accAH01	= createAccount(userMR, 'ah001');
			accAH11	= createAccount(userMR, 'ah011');

			// 代理店事務所の作成
			accAY01	= createAccount(userMR, 'ay001', accAH01);
			accAY11	= createAccount(userMR, 'ay011', accAH11);

			// 募集人の作成
			conAT01	= createContact(accAH01, accAY01, 'at001');
			userAT01	= createUser('TEST01', pMap.get('E_PartnerCommunity_IA'), '', '', conAT01);
			conAT02	= createContact(accAH01, accAY01, 'at002');
			userAT02	= createUser('TEST02', pMap.get('E_PartnerCommunity_IA'), '', '', conAT02);
			conAT11	= createContact(accAH11, accAY11, 'at011');
			userAT11	= createUser('TEST11', pMap.get('E_PartnerCommunity_IA'), '', '', conAT11);
			conAT12	= createContact(accAH11, accAY11, 'at012');
			userAT12	= createUser('TEST12', pMap.get('E_PartnerCommunity_IA'), '', '', conAT12);

		}
 		// NNLinkLogの作成
		System.RunAs(userAT01){	createLog(); }
		System.RunAs(userAT02){	createLog(); }
		System.RunAs(userAT11){	createLog(); }
		System.RunAs(userAT12){	createLog(); }

		Test.startTest();
		System.RunAs(runUser){

			I_TotalController totalController	= new I_TotalController();
			String temp			= '';
			String answer		= '';
			answer += '{"agentId":"' + userAT01.Id + '","agentName":"at001","officeName":"officeay001","loginCount":"null"},';
			answer += '{"agentId":"' + userAT02.Id + '","agentName":"at002","officeName":"officeay001","loginCount":"null"},';
			answer += '{"agentId":"' + userAT11.Id + '","agentName":"at011","officeName":"officeay011","loginCount":"null"},';
			answer += '{"agentId":"' + userAT12.Id + '","agentName":"at012","officeName":"officeay011","loginCount":"null"}';

			// フィルタ条件 - 営業部
			temp = I_TotalController.getRecordInfo('Ranking',I_Const.CONDITION_FIELD_UNIT, '1',false);
			//system.debug('営業部:' + temp);
			System.assertEquals(answer, temp);

			// フィルタ条件 - MR
			temp = I_TotalController.getRecordInfo('Ranking',I_Const.CONDITION_FIELD_MR, userMR.Id,false);
			//system.debug('MR:' + temp);
			System.assertEquals(answer, temp);

			answer		= '';
			answer += '{"agentId":"' + userAT01.Id + '","agentName":"at001","officeName":"officeay001","loginCount":"null"},';
			answer += '{"agentId":"' + userAT02.Id + '","agentName":"at002","officeName":"officeay001","loginCount":"null"}';

			// フィルタ条件 - 代理店
			temp = I_TotalController.getRecordInfo('Ranking',I_Const.CONDITION_FIELD_AGENCY, accAH01.Id,false);
			//system.debug('代理店:' + temp);
			System.assertEquals(answer, temp);

			// フィルタ条件 - 事務所
			temp = I_TotalController.getRecordInfo('Ranking',I_Const.CONDITION_FIELD_OFFICE, accAY01.Id,false);
			//system.debug('事務所:' + temp);
			System.assertEquals(answer, temp);

			answer		= '';
			answer += '{"agentId":"' + userAT01.Id + '","agentName":"at001","officeName":"officeay001","loginCount":"null"}';

			// フィルタ条件 - 募集人
			temp = I_TotalController.getRecordInfo('Ranking',I_Const.CONDITION_FIELD_AGENT, conAT01.Id,false);
			//system.debug('募集人:' + temp);
			System.assertEquals(answer, temp);

			//営業日
			insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = System.today());
			System.assertEquals(System.now().format('yyyy/MM/dd'), totalController.bizDate);
		}
		Test.stopTest();
	}



/*-- データ作成 --*/
	/**
	 * Profileマップ取得
	 * @return Map<String, Id>: Map of ProfileName & ProfileID
	 */
	public static Map<String, Id> getProfileIdMap(){
		Map<String, Id> pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}

	/**
	 * ユーザ作成
	 * @param lastName		:姓
	 * @param profileId		:プロファイルId
	 * @param unit			:営業部
	 * @param unitName		:拠点名
	 * @param memberNo		:社員番号
	 * @return User			:ユーザ
	 */
	public static User createUser(String lastName, Id profileId, String unit, String unitName, String memberNo){
		UserRole role = [SELECT id FROM UserRole where PortalType = 'None' and DeveloperName = 'E_DummyRole' limit 1];
		String userName = lastName + '@terrasky.ingtesting';
		User rec = new User(
			 Lastname				= lastName							// 姓
			,Username				= userName							// 名
			,Email					= userName							// メール
			,ProfileId				= profileId							// プロファイル
			,Alias					= lastName.left(8)					// 別名
			,TimeZoneSidKey			= UserInfo.getTimeZone().getID()	// タイムゾーン
			,LocaleSidKey			= UserInfo.getLocale()				// 地域
			,EmailEncodingKey		= 'UTF-8'							// メールの文字コード
			,LanguageLocaleKey		= UserInfo.getLanguage()			// 言語
			,Unit__c				= unit								// 営業部
			,UnitName__c			= unitName							// 拠点名
			,MemberNo__c			= memberNo							// 社員番号
			,UserRoleId				= role.Id
		);
		insert rec;
		return rec;
	}
    /**
     * コミュニティユーザ作成
     * @param lastName		:姓
	 * @param profileId		:プロファイルId
     * @param E_Unit__c		:営業部
     * @param UnitName__c	:拠点名
     * @param Contact		:募集人Id
     * @return User			:ユーザ
     */
	public static User createUser(String lastName, Id profileId, String unit, String unitName, Contact contact){
		String userName = lastName + '@terrasky.ingtesting';
		User rec = new User(
			 Lastname 				= lastName							// 姓
			,Username 				= userName							// 名
			,Email					= userName							// メール
			,ProfileId				= profileId							// プロファイル
			,Alias					= lastName.left(8)					// 別名
			,TimeZoneSidKey			= UserInfo.getTimeZone().getID()	// タイムゾーン
			,LocaleSidKey			= UserInfo.getLocale()				// 地域
			,EmailEncodingKey		= 'UTF-8'							// メールの文字コード
			,LanguageLocaleKey		= UserInfo.getLanguage()			// 言語
			,Unit__c				= unit								// 営業部
			,UnitName__c			= unitName							// 拠点名
			,ContactId				= contact.Id						// 募集人Id
		);
		insert rec;
		return rec;
	}

	/**
	 * 代理店格の作成
	 */
	public static Account createAccount(User userMR, String zagcynum){
		// Account 代理店格
		Account accAH = new Account(
			Name					= 'agency' + zagcynum	// 取引先名
			,E_CL1PF_ZHEADAY__c		= zagcynum				// 代理店事務所コード
			,Ownerid				= userMR.Id				// 所有者
		);
		insert accAH;
		return accAH;
	}

	/**
	 * 代理店事務所の作成
	 */
	public static Account createAccount(User userMR, String zagcynum, Account accAH){
		// Account 代理店事務所
		Account accAY = new Account(
			Name					= 'office' + zagcynum	// 取引先名
			,ParentId				= accAH.Id				// 親取引先
			,E_CL2PF_ZAGCYNUM__c	= zagcynum				// 代理店事務所コード
			,E_COMM_VALIDFLAG__c	= '1'					// 有効フラグ
			,E_CL2PF_BRANCH__c		= '1'					// 支社コード
			,E_CL2PF_ZBUSBR__c		= '2'					// 営業母店支社コード
			,KSECTION__c			= '00'					// 課コード
			,Ownerid				= userMR.Id				// 所有者
		);
		insert accAY;
		return accAY;
	}

	/**
	 * 募集人の作成
	 * @param accAH			:代理店格ID
	 * @param accAY			:代理店事務所ID
	 * @param agntnum		:募集人コード
	 */
	public static Contact createContact(Account accAH, Account accAY, String agntnum){
		Contact con = new Contact();
		String userName = agntnum + '@terrasky.ingtesting';
		con.LastName				= agntnum;			// 姓
		con.email					= userName;			// メール
		con.AccountId				= accAY.Id;			// 取引先ID
		con.E_COMM_ZCLADDR__c		= '東京都日本橋';	// 漢字住所
		con.E_CLTPF_CLNTNUM__c		= agntnum;			// 顧客番号
		con.E_CL3PF_CLNTNUM__c		= agntnum;			// 募集人顧客コード
		con.E_CL3PF_AGNTNUM__c		= agntnum;			// 募集人コード
		con.E_CL3PF_KMOFCODE__c		= 'MOF2016';		// MOFコード
		con.E_CLTPF_DOB__c			= '20001220';		// 生年月日
		con.E_CL3PF_ZHEADAY__c		= accAH.E_CL1PF_ZHEADAY__c;		// 代理店格コード
		con.E_CL3PF_ZAGCYNUM__c		= accAY.E_CL2PF_ZAGCYNUM__c;	// 代理店事務所コード
		con.E_CLTPF_FLAG01__c		= '1';				// 契約者フラグ
		//con.E_CLTPF_FLAG02__c		= '1';				// 被保険者フラグ
		con.E_CL3PF_VALIDFLAG__c	= '1';				// 有効フラグ
		insert con;
		return con;
	}
	/**
	 * NNLinkLogの作成
	 * @param userAT		:ユーザ(募集人)
	 */
	public static E_log__c createLog(){
		E_log__c log = new E_log__c();
		log.Name				= I_Const.SEARCH_LABEL_CATEGORY_POLICY + '>' + I_Const.SEARCH_TYPE_LABELPOLICY_NO + '>' + userInfo.getUserId().right(8);
		log.Detail__c			= '{"searchType":"policyNo", "searchTypeName":"証券番号", "keyword":"' + userInfo.getUserId().right(8) + '"}';
		log.AccessUser__c		= userInfo.getUserId();
		log.ActionType__c		= E_LogUtil.NN_LOG_ACTIONTYPE_SEARCH;
		log.AccessDateTime__c	= DateTime.now();
		log.isDisplay__c		= true;
		insert log;
		return log;
	}

	/**
	 * 新契約ステータスの作成
	 */
	public static List<E_NewPolicy__c> createNewPolicy(Account accAH, Account accAY, Contact conAT, String groupKey){

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		Date d = Date.today().addMonths(-1);
		Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());

		Id ParentId = null;
		Boolean groupParentFlag = false;
		Integer loopCount = (groupKey == '') ? 1 : 2;

		for(Integer i = 0; i < loopCount; i++){
			groupParentFlag = (i == 0) ? false : true;

			E_NewPolicy__c rec01 = new E_NewPolicy__c();				// (成立)
			rec01.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec01.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '1';	// 団体番号
			rec01.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec01.ParentAccount__c		= accAH.Id;						// 代理店格
			rec01.Account__c			= accAY.Id;						// 代理店事務所
			rec01.Agent__c				= conAT.Id;						// 募集人
			rec01.STATCODE__c			= 'I';							// 新契約ステータス
			rec01.StatusDate__c			= dt;							// ステータス配信日
			rec01.SyncDate__c			= dt;							// データ発信日
			insert rec01;
			recs.add(rec01);

			E_NewPolicy__c rec02 = new E_NewPolicy__c();				// (謝絶)
			rec02.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec02.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '2';	// 団体番号
			rec02.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec02.ParentAccount__c		= accAH.Id;						// 代理店格
			rec02.Account__c			= accAY.Id;						// 代理店事務所
			rec02.Agent__c				= conAT.Id;						// 募集人
			rec02.STATCODE__c			= 'Z';							// 新契約ステータス
			rec02.StatusDate__c			= dt;							// ステータス配信日
			rec02.SyncDate__c			= dt;							// データ発信日
			insert rec02;
			recs.add(rec02);

			E_NewPolicy__c rec03 = new E_NewPolicy__c();				// (取下げ)
			rec03.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec03.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '3';	// 団体番号
			rec03.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec03.ParentAccount__c		= accAH.Id;						// 代理店格
			rec03.Account__c			= accAY.Id;						// 代理店事務所
			rec03.Agent__c				= conAT.Id;						// 募集人
			rec03.STATCODE__c			= 'Q';							// 新契約ステータス
			rec03.StatusDate__c			= dt;							// ステータス配信日
			rec03.SyncDate__c			= dt;							// データ発信日
			insert rec03;
			recs.add(rec03);

			E_NewPolicy__c rec04 = new E_NewPolicy__c();				// (申込書類到着済)
			rec04.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec04.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '4';	// 団体番号
			rec04.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec04.ParentAccount__c		= accAH.Id;						// 代理店格
			rec04.Account__c			= accAY.Id;						// 代理店事務所
			rec04.Agent__c				= conAT.Id;						// 募集人
			rec04.STATCODE__c			= 'P';							// 新契約ステータス
			rec04.SyncDate__c			= dt;							// データ発信日
			insert rec04;
			recs.add(rec04);

			E_NewPolicy__c rec05 = new E_NewPolicy__c();				// (不備対応中)
			rec05.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec05.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '5';	// 団体番号
			rec05.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec05.ParentAccount__c		= accAH.Id;						// 代理店格
			rec05.Account__c			= accAY.Id;						// 代理店事務所
			rec05.Agent__c				= conAT.Id;						// 募集人
			//rec05.DefectCount__c		= 1;							// 不備数
			rec05.STATCODE__c			= 'P';							// 新契約ステータス
			rec05.SyncDate__c			= dt;							// データ発信日
			insert rec05;
			recs.add(rec05);
			if(groupParentFlag==false){
				// 新契約不備(不備数)
				E_NewPolicyDefect__c subRec05 = new E_NewPolicyDefect__c();
				subRec05.E_NewPolicy__c			= rec05.Id;				// 新契約
				subRec05.FUPCDE__c				= 'AA1';				// 不備コード
				subRec05.FUPSTAT__c				= 'O';					// 不備ステータス
				insert subRec05;
			}

			E_NewPolicy__c rec06 = new E_NewPolicy__c();				// (査定完了入金待ち)
			rec06.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec06.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '6';	// 団体番号
			rec06.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec06.ParentAccount__c		= accAH.Id;						// 代理店格
			rec06.Account__c			= accAY.Id;						// 代理店事務所
			rec06.Agent__c				= conAT.Id;						// 募集人
			//rec06.W50Count__c			= 1;							// 未入金数
			rec06.STATCODE__c			= 'P';							// 新契約ステータス
			rec06.SyncDate__c			= dt;							// データ発信日
			insert rec06;
			recs.add(rec06);
			if(groupParentFlag==false){
				// 新契約不備(未入金数)
				E_NewPolicyDefect__c subRec06 = new E_NewPolicyDefect__c();
				subRec06.E_NewPolicy__c			= rec06.Id;				// 新契約
				subRec06.FUPCDE__c				= 'W50';				// 不備コード
				subRec06.FUPSTAT__c				= 'O';					// 不備ステータス
				insert subRec06;
			}

			E_NewPolicy__c rec07 = new E_NewPolicy__c();				// (査定中)
			rec07.KFRAMEIND__c			= (groupKey == '') ? 'N' : 'Y';	// 団体タイプ
			rec07.GRUPKEY__c 			= (groupKey == '') ? '' : groupKey + '7';	// 団体番号
			rec07.GroupParentFlag__c	= groupParentFlag;				// 団体親フラグ
			rec07.ParentAccount__c		= accAH.Id;						// 代理店格
			rec07.Account__c			= accAY.Id;						// 代理店事務所
			rec07.Agent__c				= conAT.Id;						// 募集人
			//rec07.TotalDefectCount__c	= 1;							// 発信不備数
			rec07.STATCODE__c			= 'P';							// 新契約ステータス
			rec07.SyncDate__c			= dt;							// データ発信日
			insert rec07;
			recs.add(rec07);
			if(groupParentFlag==false){
				// 新契約不備(未入金数)
				E_NewPolicyDefect__c subRec07 = new E_NewPolicyDefect__c();
				subRec07.E_NewPolicy__c			= rec07.Id;				// 新契約
				subRec07.FUPCDE__c				= '';					// 不備コード
				subRec07.FUPSTAT__c				= 'F';					// 不備ステータス
				insert subRec07;
			}

		}
		return recs;
	}

	/**
	 * 保有契約関連の作成
	 */
	public static integer createPossessionAgreement(Id runUserId, Account accAY, Contact conAT){
		// ID管理作成
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false, runUserId);
		idcpf.ZINQUIRR__c = 'xx1';	// 照会者コード
		insert idcpf;

		// 保険契約ヘッダ レコードタイプIDの取得
		RecordType recTypeCOLI = [SELECT Id FROM RecordType WHERE sObjectType = 'E_Policy__c' AND DeveloperName = :E_Const.POLICY_RECORDTYPE_COLI limit 1];

		// 保険契約ヘッダの作成
		E_Policy__c policy01 = new E_Policy__c(
			 Contractor__c			= conAT.id			// 契約者
			,RecordTypeId			= recTypeCOLI.Id	// レコードタイプ
			,COMM_CHDRNUM__c		= '10001'			// 証券番号
			,MainAgentAccount__c	= accAY.Id			// 主募集人代理店事務所
			,MainAgent__c			= conAT.Id			// 主募集人
		);
		insert policy01;

		// 保有契約異動通知の作成
		Date d01 = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
		E_AGPO__c agpo = new E_AGPO__c();
		agpo.YYYY__c		= d01.year();		// 年
		agpo.MM__c			= d01.month();		// 月
		agpo.E_Policy__c	= policy01.id;		// 保険契約ヘッダ
		agpo.ZJINTSEQ__c	= '1'; 				// 募集人SEQ
		agpo.Account__c		= accAY.Id;			// 代理店事務所
		insert agpo;

		// 保険契約ヘッダの作成
		E_Policy__c policy02 = new E_Policy__c(
			 Contractor__c			= conAT.id			// 契約者
			,RecordTypeId			= recTypeCOLI.Id	// レコードタイプ
			,COMM_CHDRNUM__c		= '10002'			// 証券番号
			,MainAgentAccount__c	= accAY.Id			// 主募集人代理店事務所
			,MainAgent__c			= conAT.Id			// 主募集人
		);
		insert policy02;

		// 保険料請求予告通知の作成
		Date d02 = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA);
		E_BILA__c bila = new E_BILA__c();
		bila.YYYY__c		= d02.year();		// 年
		bila.MM__c			= d02.month();		// 月
		bila.E_Policy__c	= policy02.id;		// 保険契約ヘッダ
		bila.ZJINTSEQ__c	= '1'; 				// 募集人SEQ
		bila.Account__c		= accAY.Id;			// 代理店事務所
		insert bila;

		// 保険契約ヘッダの作成
		E_Policy__c policy03 = new E_Policy__c(
			 Contractor__c			= conAT.id			// 契約者
			,RecordTypeId			= recTypeCOLI.Id	// レコードタイプ
			,COMM_CHDRNUM__c		= '10003'			// 証券番号
			,MainAgentAccount__c	= accAY.Id			// 主募集人代理店事務所
			,MainAgent__c			= conAT.Id			// 主募集人
		);
		insert policy03;

		// 保険料請求通知の作成
		Date d03 = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS);
		E_BILS__c bils = new E_BILS__c();
		bils.YYYY__c		= d03.year();		// 年
		bils.MM__c			= d03.month();		// 月
		bils.E_Policy__c	= policy03.id;		// 保険契約ヘッダ
		bils.ZJINTSEQ__c	= '1'; 				// 募集人SEQ
		bils.Account__c		= accAY.Id;			// 代理店事務所
		insert bils;

		// 保険契約ヘッダの作成
		E_Policy__c policy04 = new E_Policy__c(
			 Contractor__c			= conAT.id			// 契約者
			,RecordTypeId			= recTypeCOLI.Id	// レコードタイプ
			,COMM_CHDRNUM__c		= '10004'			// 証券番号
			,MainAgentAccount__c	= accAY.Id			// 主募集人代理店事務所
			,MainAgent__c			= conAT.Id			// 主募集人
		);
		insert policy04;

		// 保険料請求予告通知の作成
		Date d04 = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
		E_NCOL__c ncol = new E_NCOL__c();
		ncol.YYYY__c		= d04.year();		// 年
		ncol.MM__c			= d04.month();		// 月
		ncol.E_Policy__c	= policy04.id;		// 保険契約ヘッダ
		ncol.ZJINTSEQ__c	= '1'; 				// 募集人SEQ
		ncol.Account__c		= accAY.Id;			// 代理店事務所
		insert ncol;

		return 4;
	}

	/**
	 * 保有契約 月後関連の作成
	 */
	public static integer createPossessionAgreementMonth(Id runUserId, Account accAY, Contact conAT){
		// ID管理作成
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false, runUserId);
		idcpf.ZINQUIRR__c = 'xx1';	// 照会者コード
		insert idcpf;

		// 保険契約ヘッダ レコードタイプIDの取得
		RecordType recTypeCOLI = [SELECT Id FROM RecordType WHERE sObjectType = 'E_Policy__c' AND DeveloperName = :E_Const.POLICY_RECORDTYPE_COLI limit 1];

		// 契約日範囲の取得
		List<String> occDates = new List<String>();
		Integer nextFrom = 1;
		for(Integer p = 0; p < 6; p++){
			Date sysDate = Date.today().addMonths(1);
			Datetime dt = Datetime.newInstance(sysDate.year(), sysDate.month() + p, 25);
			occDates.add(dt.format('yyyyMMdd'));
		}

		List<E_Policy__c> policys1 = new List<E_Policy__c>();
		for(Integer i = 0; i < occDates.size(); i++){
			// 保険契約ヘッダの作成
			E_Policy__c policy = new E_Policy__c(
				 Contractor__c			= conAT.id			// 契約者
				,RecordTypeId			= recTypeCOLI.Id	// レコードタイプ
				,COMM_CHDRNUM__c		= '2000' + String.valueOf(i)		// 証券番号
				,MainAgentAccount__c	= accAY.Id			// 主募集人代理店事務所
				,MainAgent__c			= conAT.Id			// 主募集人
				,COMM_OCCDATE__c		= occDates[i]		// 契約日
				,COLI_ZGRUPDCF__c		= false				// 団体表示フラグ
				,COMM_STATCODE__c		= 'Y'				// 現在の状況コード
				,COMM_ZRSTDESC__c		= '口座振替'		// 現在の状況
				,Ownerid				= runUserId			// 所有者
			);
			policys1.add(policy);
		}
		insert policys1;

		List<E_Policy__c> policys2 = new List<E_Policy__c>();
		for(Integer i = 0; i < occDates.size(); i++){
			// 保険契約ヘッダの作成
			E_Policy__c policy = new E_Policy__c(
				 Contractor__c			= conAT.id			// 契約者
				,RecordTypeId			= recTypeCOLI.Id	// レコードタイプ
				,COMM_CHDRNUM__c		= '2100' + String.valueOf(i)	// 証券番号
				,COLI_GRUPNUM__c		= 'G002' + String.valueOf(i)	// 団体番号
				,MainAgentAccount__c	= accAY.Id			// 主募集人代理店事務所
				,MainAgent__c			= conAT.Id			// 主募集人
				,COMM_OCCDATE__c		= occDates[i]		// 契約日
				,COLI_ZGRUPDCF__c		= true				// 団体表示フラグ
				,COMM_STATCODE__c		= 'Y'				// 現在の状況コード
				,COMM_ZRSTDESC__c		= '口座振替'		// 現在の状況
				,Ownerid				= runUserId			// 所有者
			);
			policys2.add(policy);
		}
		insert policys2;

		// 個人保険特約の作成
		List<E_COVPF__c> covpfs = new List<E_COVPF__c>();
		for(E_Policy__c policy : policys2){
			E_COVPF__c covpf = new E_COVPF__c(
				 E_Policy__c			= policy.Id			// 保険契約ヘッダー
				,COMM_ZCOVRNAM__c		= '個人保険特約'	// 保険種類
				,COLI_ZCRIND__c			= 'C'				// 主契約フラグ
			);
			covpfs.add(covpf);
		}
		insert covpfs;


		return policys1.size() + policys2.size();
	}

	/**
	 * irisメニュー
	 */
	public static void createIRISMenu(){
		I_MenuMaster__c mn = new I_MenuMaster__c();
		mn.Name					= '利用募集人一覧';
		mn.DisplayOrder__c		= '01';
		mn.MainCategory__c		= '6.利用履歴';
		mn.MenuKey__c			= 'iris_log';
		insert mn;
	}

}