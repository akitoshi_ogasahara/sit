public with sharing class I_CalcServiceInput {

	//計算基準日
	public String keisanKijunDate {get;set;}

	//実効税率
	public String taxRt {get;set;}

	//責任開始日
	public String plcyDate {get;set;}

	//契約者種別
	public String keiyakushaSb {get;set;}

	//申込形態
	public String kobetsuIkkatsuKb {get;set;}

	//損金タイプ
	public String sonkinType {get;set;}

	//前納年数
	public Integer zennoNensu {get;set;}

	//被保険者情報
	public PlanHihokensyaInfo planHihokensyaInfo {get;set;}

	//主契約情報
	public MainCoverInfo mainCoverInfo {get;set;}

	//コンストラクタ
	public I_CalcServiceInput(){

		planHihokensyaInfo = new I_CalcServiceInput.PlanHihokensyaInfo();

		mainCoverInfo = new I_CalcServiceInput.MainCoverInfo();
	}

 	public class PlanHihokensyaInfo{

 		//被保険者を特定できるキー
 		public String hihokenshaId {get;set;}

 		//被保険者年齢
		public Integer hihokenshaAge {get;set;}

		//性別
		public String hihokenshaSex {get;set;}

		//選択方法有無
		public String sentakuShiteiUmu {get;set;}

		//選択方法
		public String sentakuHoho {get;set;}

		//選択方法候補
		public String sentakuHohoOthKoho {get;set;}


 	}

 	public class MainCoverInfo{

		//CRコード
		public String crCode {get;set;}

		//保険金額
		public Integer sPlcy {get;set;}

		//保険期間種別
		public String hokenKikanNenSaiSyushin {get;set;}

		//保険期間
		public String hokenKikanPlcy {get;set;}

		//払込期間種別
		public String haraikomiKikanNenSaiSyushin {get;set;}

		//払込期間
		public String haraikomiKikan {get;set;}

		//低CV期間種別
		public String lowCvKikanNenSai {get;set;}

		//低CV期間
		public String lowCvKikan {get;set;}

		//保証期間
		public String inPHoshoKikan {get;set;}

		//健康体・優良体特約付加状態
		public String kenkotai {get;set;}

		///割増（新特別条件特約）
		public String warimashi {get;set;}

		//削減年数
		public String sakugen {get;set;}

		//特定疾病保険料払込免除特則 有無
		public String tokuteiShippeiPMenTokusoku {get;set;}

		//払い込み方法
		public String haraikomiHoho {get;set;}

		//始期指定
		public String shikiShitei {get;set;}

		//特約リスト
		public List<SubCoverInfo> subCoverList {get;set;}

		public MainCoverInfo(){
			subCoverList = new List<I_CalcServiceInput.SubCoverInfo>();
		}

 	}

 	public class SubCoverInfo{

		//CRコード
		public String tokuyakuCrCode {get;set;}

		//保険金額
		public Integer sTokuyaku {get;set;}

		//保険期間種別
		public String hokenKikanNenSaiSyushin {get;set;}

		//保険期間
		public String hokenKikanTokuyaku {get;set;}

 	}

}