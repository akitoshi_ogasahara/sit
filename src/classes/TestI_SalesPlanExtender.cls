@isTest
private class TestI_SalesPlanExtender{
	private static Account acc;
	private static E_AgencySalesResults__c asr;
	private static E_SalesPlan__c sp;
	private static Attachment att;
	
	@isTest static void initTest01() {
		createData();

		Pagereference testpage = Page.IRIS_SalesPlan;
		testpage.getParameters().put('id', sp.Id);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(sp);
		I_SalesPlanController controller = new I_SalesPlanController(standardcontroller);
		I_SalesPlanExtender extender = controller.getExtender();
		extender.init();
		extender.getYYYY();
		extender.getMM();
	}
	// PDF出力
	@isTest static void doSaveTest01() {
		createData();

		Pagereference testpage = Page.IRIS_SalesPlan;
		testpage.getParameters().put('id', sp.Id);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(sp);
		I_SalesPlanController controller = new I_SalesPlanController(standardcontroller);
		I_SalesPlanExtender extender = controller.getExtender();
		extender.planData.pdfTitle = 'テスト';
		extender.init();
		testpage.getParameters().put('isPdf', '1');
		extender.doSaveAndCreatePDF();
	}
	// 未入力出力
	@isTest static void doSaveTest02() {
		createData();

		Pagereference testpage = Page.IRIS_SalesPlan;
		testpage.getParameters().put('id', sp.Id);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(sp);
		I_SalesPlanController controller = new I_SalesPlanController(standardcontroller);
		I_SalesPlanExtender extender = controller.getExtender();
		extender.init();
		testpage.getParameters().put('isClear', 'true');
		testpage.getParameters().put('isPdf', '1');
		extender.init();
		extender.createClearPDF();
	}

	// テストデータ作成
	private static void createData(){

		acc = new Account(Name = 'テスト代理店格'
						, E_CL1PF_ZHEADAY__c = '10001');
		insert acc;

		asr = new E_AgencySalesResults__c(ParentAccount__c = acc.Id
										, BusinessDate__c = '20000101'
										, Hierarchy__c = 'AH'
										, QualifSim__c = '優績S');
		insert asr;

		sp = new E_SalesPlan__c(ParentAccount__c = acc.Id
								, SalesResult__c = asr.Id
								);
		insert sp;
	}  
}