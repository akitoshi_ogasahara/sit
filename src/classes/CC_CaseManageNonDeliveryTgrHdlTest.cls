/*
 * CC_CaseManageNonDeliveryTgrHdl
 * Test class of CC_CaseManageNonDeliveryTgrHdl
 * created  : Accenture 2018/10/12
 * modified :
 */

@isTest
private class CC_CaseManageNonDeliveryTgrHdlTest{

    private static User testUser = CC_TestDataFactory.createTestUser();

    /**
    * Prepare test data
    */
    @testSetup static void prepareTestData(){
        System.runAs ( testUser ) {

            //Create Account
            RecordType recordTypeOfAccount = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '法人顧客' limit 1];
            Account newAccount = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
            newAccount.CC_LastUpdateDateForAddress__c = Datetime.now();
            Insert newAccount;

            //Create Contact
            RecordType recordTypeOfContact = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND Name = '顧客' limit 1];
            List<Contact> newContactList = CC_TestDataFactory.getContactSkelList();
            for(Contact newContact : newContactList){
                if(newContact.FirstName.equals('test0')){
                    newContact.RecordTypeId = recordTypeOfContact.Id;
                    newContact.AccountId = newAccount.Id;
                    newContact.E_CLTPF_CLTTYPE__c = 'C';
                }
                if(newContact.FirstName.equals('test1')){
                    newContact.RecordTypeId = recordTypeOfContact.Id;
                    newContact.AccountId = newAccount.Id;
                    newContact.E_CLTPF_CLTTYPE__c = 'P';
                    newContact.CMN_isAddressUnknown__c = true;
                }
            }
            Insert newContactList;

            //Create SRTypeMaster
            CC_SRTypeMaster__c newSRTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
            newSRTypeMaster.Name = '0124007';
            Insert newSRTypeMaster;            
        }
    }

    /**
    * Test onBeforeInsert()
    * 法人顧客、住所更新有
    */
    static testMethod void onBeforeInsertTest01() {
        System.runAs ( testUser ) {
            try{
                //Create Case data
                CC_SRTypeMaster__c srtypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE Name = '0124007'];
                Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0'];

                Case newCase = CC_TestDataFactory.getCaseSkelWithNoParameter();
                newCase.CC_isNonDeliveryManage__c = true;
                newCase.CMN_ContactName__c = contact.Id;
                Date today = Date.today();
                newCase.CC_DPSPostingTime__c = today;
                newCase.CC_DPSItemID__c = 'B5628';
                newCase.CC_NonDeliveryReason__c = '宛所尋ね当たらず';

                Test.startTest();
                insert newCase;
                Test.stopTest();
            }catch(Exception e){
                System.debug('Exception caught: ' + e.getMessage());
            }
        }
    }

    /**
    * Test onBeforeInsert()
    * 個人顧客、初回不明
    */
    static testMethod void onBeforeInsertTest02() {
        System.runAs ( testUser ) {
            try{                                        
                //Create Case data
                Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test1' AND LastName = 'Contact1'];
                CC_SRTypeMaster__c srtypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE Name = '0124007'];
                Case newCase = CC_TestDataFactory.getCaseSkelWithNoParameter();
                newCase.CC_isNonDeliveryManage__c = true;
                newCase.CMN_ContactName__c = contact.Id;
                Date today = Date.today();
                newCase.CC_DPSPostingTime__c = today;
                newCase.CC_DPSItemID__c = 'B5628';
                newCase.CC_NonDeliveryReason__c = '宛所尋ね当たらず';

                Test.startTest();
                insert newCase;
                Test.stopTest();
            }catch(Exception e){
                System.debug('Exception caught: ' + e.getMessage());
            }
        }
    }
    
    /**
    * Test onBeforeInsert()
    * Insert複数SR
    */
    static testMethod void onBeforeInsertTest03() {
        System.runAs ( testUser ) {
            try{
                //Update Contact
                Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0'];
                contact.CMN_isAddressUnknown__c = false;
                Update contact;

                //Create Case data
                CC_SRTypeMaster__c srtypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE Name = '0124007'];       
                List<Case> newCaseList = new List<Case>();
                Case newCase1 = CC_TestDataFactory.getCaseSkelWithNoParameter();
                newCase1.CC_isNonDeliveryManage__c = true;
                newCase1.CMN_ContactName__c = contact.Id;
                Date today = Date.today();
                newCase1.CC_DPSPostingTime__c = today;
                newCase1.CC_DPSItemID__c = 'B5628';
                newCase1.CC_NonDeliveryReason__c = '宛所尋ね当たらず';
                newCaseList.add(newCase1);
                Case newCase2 = CC_TestDataFactory.getCaseSkelWithNoParameter();
                newCase2.CC_isNonDeliveryManage__c = true;
                newCase2.CMN_ContactName__c = contact.Id;
                today = today + 1;
                newCase2.CC_DPSPostingTime__c = today;
                newCase2.CC_DPSItemID__c = 'B5628';
                newCase2.CC_NonDeliveryReason__c = '宛所尋ね当たらず';
                newCaseList.add(newCase2);

                Test.startTest();
                insert newCaseList;
                Test.stopTest();
            }catch(Exception e){
                System.debug('Exception caught: ' + e.getMessage());
            }
        }
    }

}