@isTest
private class RecruitTriggerTest {
	static testMethod void myUnitTest() {
		// TO DO: implement unit test
		System.debug('-------------------------------------------------------OpportunityUpdateTriggerTest：START');
		try{
			User userData = [SELECT Id, Unit__c, UnitName__c FROM User where isActive=true limit 1];
			userData.UnitName__c = 'テスト拠点名';
			userData.Unit__c = 'テスト営業部';
			update userData;

			System.runAs(userData){
				Recruit__c recruitData = new Recruit__c();
				recruitData.Area__c = 'テスト';
				recruitData.SalesUnit__c = 'テスト';
				recruitData.Name = 'テスト';
				recruitData.Company__c = 'テスト';
				insert recruitData;

				Recruit__c InputDataList = [SELECT Id, Area__c, SalesUnit__c FROM Recruit__c WHERE ID =: recruitData.Id];
				// 所有者の拠点名と一致すること
				System.assertEquals(userData.UnitName__c, InputDataList.Area__c);
				// 所有者の営業部と一致すること
				System.assertEquals(userData.Unit__c, InputDataList.SalesUnit__c);
			}
		}catch(QueryException qe){
			System.debug('-------------------------------------------------------QueryException例外内容'+qe);
		}catch(Exception e){
			System.debug('-------------------------------------------------------Exception例外内容'+e);
		}
		System.debug('-------------------------------------------------------OpportunityUpdateTriggerTest：END');
	} // END myUnitTest()
}