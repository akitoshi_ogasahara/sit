global class CMN_VlocityTriggerOnBatch implements Database.Batchable<sObject> {

	private String query;

	global CMN_VlocityTriggerOnBatch (String soql){
		query = soql;

		vlocity_ins__TriggerSetup__c ts = vlocity_ins__TriggerSetup__c.getValues('AllTriggers');
		if(ts.vlocity_ins__IsTriggerOn__c == false){
			ts.vlocity_ins__IsTriggerOn__c = true;
			update ts;
		}
	}

	// The batch job starts
	global Database.Querylocator start(Database.BatchableContext bc){
		System.debug('>>>> start ' + query);
		return Database.getQuerylocator(query);
	}

	// The batch job executes and operates on one batch of records
	global void execute(Database.BatchableContext bc, List<sObject> scope){
		System.debug('>>>> execute ' + scope.size());
	}

	// The batch job finishes
	global void finish(Database.BatchableContext bc){
		AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()];
		System.debug('>>>> finish ' + job.Status);
	}
}