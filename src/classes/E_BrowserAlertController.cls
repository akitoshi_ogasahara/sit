public with sharing class E_BrowserAlertController extends E_InfoController {
    private static final String MENU_KEY_AGENT = 'login_agent';
    private static final String MENU_KEY_CUSTOMER = 'login_customer';

    //ブラウザ判定Handler
    public E_BrowserJudgeHandler browserHandler{get;set;}

    //ユーザ名
    public String userId {get;set;}
    //パスワード
    public String password {get;set;}
    //メニューキー
    public String menuKey {get;set;}

    public E_BrowserAlertController() {
        pgTitle = 'ログイン';
    }


    //メニューキーを返却
    protected override String getMenuKey(){
        
        return 'browserAlert';
    }
}