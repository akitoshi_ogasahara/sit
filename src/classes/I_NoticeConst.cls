/**
 * IRIS 4帳票一覧
 */
public class I_NoticeConst {
    
    /* 帳票種別 */
    public static String NOTICE_TYPE_AGPO = '保有契約異動通知';
    public static String NOTICE_TYPE_BILA = '保険料請求予告通知';
    public static String NOTICE_TYPE_BILS = '保険料請求通知';
    public static String NOTICE_TYPE_NCOL = '保険料未入金通知';

    /*MenuKey*/
    public static String MNKEY_BILA = 'notice_bila';
    public static String MNKEY_BILS = 'notice_bils';
    public static String MNKEY_NCOL = 'notice_ncol';
    public static String MNKEY_AGPO = 'notice_agpo';

    /* 共同募集 */
	public static String VIEW_MAIN_SUB_AGENT = '共同募集';

    /* 異動通知タイトル */
    public static String AGPO_TITLE01 = '全て表示';
    public static String AGPO_TITLE02 = 'フォローを要する通知';
    public static String AGPO_TITLE03 = '保険金等支払の通知';
    public static String AGPO_TITLE04 = '契約状況変更の通知';
    public static String AGPO_TITLE05 = 'その他';

}