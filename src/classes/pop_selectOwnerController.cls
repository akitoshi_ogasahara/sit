public with sharing class pop_selectOwnerController {
    public String title {get;set;}              //画面タイトル
    public List<OwnerInfo> userList {get;set;}  //画面ビンのリストクラス
    public OwnerInfo own {get;set;}             //画面ビン
    public Boolean isUser {get;set;}            //所有者情報存在有無フラグ
    public String ownerName {get;set;}          //所有者名

    /* コンストラクタ */
    public pop_selectOwnerController() {
        title = '所有者選択';
        isUser = false;

        //パラメータ取得
        if (ApexPages.currentPage().getParameters().get('type') <> null) {
            if ('update'.equals(ApexPages.currentPage().getParameters().get('type'))) {
                title = '変更' + title;
            }
        }

        if (ApexPages.currentPage().getParameters().get('ownerName') <> null) {
            //検索種別
            ownerName = '%' + String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('ownerName')) + '%';
            // ポップアップ表示情報取得
            getUserInfo();
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：所有者関連情報の検索
    * 戻り値              ：
    * 機能概要            ：所有者関連情報の検索
    *------------------------------------------------------------------------
    */
    private void getUserInfo() {
        userList = new List<OwnerInfo>();
        try {
            for (User u : [Select u.Id, u.UserRole.Name, u.Name From User u Where u.IsActive = true and u.Name like :ownerName order by u.Name asc NULLS LAST]) {
                own = new OwnerInfo();
                own.ownerId = u.Id;
                if(u.Name != null) {
                    own.ownerName = u.Name;
                } else {
                    own.ownerName = '　';
                }
                if(u.UserRole.Name != null) {
                    own.ownerRole = u.UserRole.Name;
                } else {
                    own.ownerRole = '　';
                }
                userList.add(own);
                if(userList.size() >= 200){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, '検索条件で得られる結果が200件を超えています。200件目以降は表示されません。'));
                    break;
                }
            }
            if (userList.isEmpty()) {
                isUser = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '該当する所有者名は存在しません。'));
            } else {
                isUser = true;
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：所有者のビンクラス
    * 戻り値              ：
    * 機能概要            ：所有者のビンクラス
    *------------------------------------------------------------------------
    */
    public class OwnerInfo {
        public String ownerId {get; set;}           //ユーザID
        public String ownerName {get; set;}         //所有者名
        public String ownerRole {get; set;}         //所有者ロール
    }

}