global class CC_InformClosedSRActivityBatchSched implements Schedulable {
    global void execute(SchedulableContext sc){
        Database.executeBatch(new CC_InformClosedSRActivityBatch());
    }
}