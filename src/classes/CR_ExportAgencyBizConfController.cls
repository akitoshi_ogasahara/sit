public with sharing class CR_ExportAgencyBizConfController extends E_CSVExportController{

	public override String mainSObjectAPIName(){
		return Schema.SObjectType.CR_Agency__c.Name;
	}

	/**
	 *	Constructor
	 */
	public CR_ExportAgencyBizConfController(){
		super();
		
		//項目の追加
		addColumn(new E_CSVColumnDescribe.BaseField('AgencyCode__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('AgencyName__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('AgencyNameKana__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('MRName__c','MR名'));
		addColumn(new E_CSVColumnDescribe.DatetimeField('LastRequestReport__c','事業報告書データ作成依頼最新日時'));
		addColumn(new E_CSVColumnDescribe.DatetimeField('LastRequestBook__c','法定帳簿データ作成依頼最新日時'));
		addColumn(new E_CSVColumnDescribe.BooleanField('LargeAgency__c'));		//CSVには大規模代理店フラグ項目も出力する
		//addColumn(new E_CSVColumnDescribe.BooleanField('OutOfBusiness__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('BusinessStatus__c'));
	}  
}