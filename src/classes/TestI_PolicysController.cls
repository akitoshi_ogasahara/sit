@isTest
private class TestI_PolicysController {	
	public static Date today = E_Util.SYSTEM_TODAY().addMonths(1);//Date.newInstance(Date.Today().year(), Date.Today().month()+1,1);

	//pageActionのテスト(MR)
	@isTest static void pageActionMRTest() {
		User us = createRunAsUser('MR');

		System.runAs(us){
			Test.startTest();
			PageReference pageRef = Page.IRIS_GroupPolicy;
			pageRef.getParameters().put('from','1');
			Test.setCurrentPage(pageRef);

			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			System.assert(lpc.pageMessages.hasMessages());
			System.debug('テストデバッグ' + lpc.pageMessages);
			Test.stopTest();
		}
	}

	//addRowsのテスト
	@isTest static void addRowsTest() {
		Test.startTest();
		I_PolicysController lpc = new I_PolicysController();
		lpc.rowCount = 981;
		lpc.addRows();
		System.assertEquals(lpc.rowCount,1000);
		Test.stopTest();
	}

	//保険用ヘッダが0件エラー
	@isTest static void makePolicyListTest() {
		createMessageData('LIS|002','該当する契約がありません');

		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','4');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			System.assertEquals(lpc.pageMessages.getWarningMessages()[0].summary,'該当する契約がありません');
			Test.stopTest();
		}
	}

	//保険用ヘッダが0件エラー(getFrom = 5)
	@isTest static void makePolicyListTest2() {
		createMessageData('LIS|002','該当する契約がありません');

		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','5');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			System.assertEquals(lpc.pageMessages.getWarningMessages()[0].summary,'該当する契約がありません');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約応答日
	@isTest static void sortRowsOccDayTest() {
		//Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		createPolicy5(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			String strAssert;
			if(Integer.valueOf(today.month())<10){
				strAssert = '0' + String.valueOf(today.month()) + '/01';
			}else{
				strAssert = String.valueOf(today.month()) + '/01';
			}
			System.assertEquals(lpc.policyList[0].getComOccDate(),strAssert);
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約者
	@isTest static void sortRowsContractTest() {
		//Test.startTest();
		clear();
		createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		createPolicy5(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'contractor');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			conNameList.sort();
			System.assertEquals(lpc.policyList[0].contractorSort,conNameList[0]);
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険種類
	@isTest static void sortRowsTypeTest() {
		//Test.startTest();
		clear();
		createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		createPolicy5(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyType');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			//lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].policyType,'');
			//System.assertEquals(lpc.policyList[0].policyType,'1テストタイプ');
			//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類格納フィールドを変更
			System.assertEquals(lpc.policyList[0].policyType,'1テストタイプ<br>1テストタイプ<br>1テストタイプ');
			Test.stopTest();
		}
	}


	//sortRowsのテスト-保険料
	@isTest static void sortRowsFeeTest() {
		//Test.startTest();
		clear();
		createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		createPolicy5(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyFee');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].policyFee,'');
			//System.assertEquals(lpc.policyList[0].policyFee,'10,100');
			System.assertEquals(lpc.policyList[0].getPolicyFee(),'10,100');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-証券番号,団体番号
	@isTest static void sortRowsNumTest() {
		//Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyNum');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].policyNum,'Test0003');
			System.assertEquals(lpc.policyList[0].policy.COMM_CHDRNUM__c,'Test0003');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-解約返戻金
	@isTest static void sortRowsCancelTest() {
		//Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'cancellRef');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].cancellRef,'?');
			//System.assertEquals(lpc.policyList[0].cancellRef,'0');
			System.assertEquals(lpc.policyList[0].getCancellRef(),'?');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-団体フラグ
	@isTest static void sortRowsFragTest() {
		//Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'grupFlag');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].grupFlag,true);
			//System.assertEquals(lpc.policyList[0].grupFlag,false);
			System.assertEquals(lpc.policyList[0].policy.COLI_ZGRUPDCF__c,true);
			Test.stopTest();
		}
	}

	//sortRowsのテスト-顧客番号
	@isTest static void sortRowsConNoTest() {
		//Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'contractorNo');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].contractorNo,null);
			System.assertEquals(lpc.policyList[0].policy.COMM_CLNTNUM__c,null);
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険金額
	/*@isTest static void sortRowsMoneyTest() {
		Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyAmount');
		Test.setCurrentPage(pageRef);

		I_PolicysController lpc = new I_PolicysController();
		lpc.pageAction();
		lpc.sortRows();
		lpc.sortRows();
		System.assertEquals(lpc.policyList[0].policyAmount,Decimal.valueOf(100000).format());
		Test.stopTest();
	}*/

	//sortRowsのテスト-担当MR名
	@isTest static void sortRowsMRNameTest() {
		//est.startTest();
		clear();
		User us = createUserKyotentyo();
		createPolicy(false,us,us.ContactId);
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'mrName');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User usr = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(usr){
			createIDManage(usr);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].mrName,'(複数)');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-代理店
	/*@isTest static void sortRowsAgencyTest() {
		Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'agency');
		Test.setCurrentPage(pageRef);

		I_PolicysController lpc = new I_PolicysController();
		lpc.pageAction();
		lpc.sortRows();
		System.assertEquals(lpc.policyList[0].agency,'親取引先1');
		Test.stopTest();
	}*/


	//sortRowsのテスト-事務所
	@isTest static void sortRowsOfficeTest() {
		//Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'office');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].office,null);
			System.assertEquals(lpc.policyList[0].office,'(複数)');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-募集人
	@isTest static void sortRowsAgentTest() {
		//Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'agent');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].agent,null);
			System.assertEquals(lpc.policyList[0].agent,'(複数)');
			Test.stopTest();
		}
	}

	//getIsTotalRowsのテスト
	@isTest static void getIsTotalRowsTest(){
		//Test.startTest();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		createPolicy5(false,UserInfo.getUserId());
		createPolicy6(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			lpc.pageAction();
			System.debug('エラーメッセージ' + lpc.pageMessages);
			String rows = lpc.policyList.size().format();
			//System.assertEquals(rows,'1');
			System.assertEquals(lpc.isTotalRows,'3');
			Test.stopTest();
		}
	}

	//getRECORD_SIZE_SOQL_FOR_RESTAPI
	@isTest static void getRECORD_SIZE_SOQL_FOR_RESTAPITest(){
		//Test.startTest();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		createPolicy5(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			//I_topController top = new I_topController();
			lpc.getFrom = 1;
			lpc.pageAction();
			//top.getRECORD_SIZE_SOQL_FOR_RESTAPI_from1();
			lpc.getRECORD_SIZE_SOQL_FOR_RESTAPI();
			System.assertEquals(lpc.policyList.size(),3);
			//System.assertEquals(lpc.policyList.size(),1);
			Test.stopTest();
		}
	}

	/*
	 *フィルターモード
	 *契約応当日が1～3ヶ月後
	 */

	@isTest static void filterModeF1Test() {

		clear();
		Datetime d = system.now();
		//格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//無効で5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		//契約ヘッダ作成
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = agentA.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = agentA.Id;
		po.COMM_SINSTAMT__c = 100;
		po.COMM_CHDRNUM__c = 'Test0004';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = -1;
		po.MainAgentAccount__c = distribute.Id;
		po.SubAgentAccount__c =office.Id;
		po.MainAgent__c = agentA.Id;
		po.SubAgent__c = agentA.Id;
		insert po;

		//個人保険特約作成
		createCOV(po);
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);

		User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		createIDManage(us);
		System.runAs(us){		
			//=====================テスト開始=====================
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			//インスタンス生成
			E_IRISHandler iris = E_IRISHandler.getInstance();
			//募集人Bで参照
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');

			lpc.pageAction();
			Integer totalRows = lpc.listSize;
			System.assertEquals(1,totalRows);
			//=====================テスト終了=====================
			Test.stopTest();
		}
	}
	/*
	 *フィルターモード
	 *契約応当日が4～6ヶ月後
	 */

	@isTest static void filterModeF4Test() {

		clear();
		Datetime d = system.now();
		//格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//無効で5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		//契約ヘッダ作成
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = agentA.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = agentA.Id;
		po.COMM_SINSTAMT__c = 100;
		po.COMM_CHDRNUM__c = 'Test0004';
		//今日から契約応当日を5か月後にする
		if(Integer.valueOf(today.month())<8){
			if(Integer.valueOf(today.month())<5){
				po.COMM_OCCDATE__c = String.valueOf(today.year()) +'0'+ String.valueOf(today.month()+5) + '01';
			}else{
				po.COMM_OCCDATE__c = String.valueOf(today.year()) + String.valueOf(today.month()+5) + '01';
			}
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()+1) +'0'+ String.valueOf(today.month()-7) + '01';
		}
		po.COLI_ZCSHVAL__c = -1;
		po.MainAgentAccount__c = distribute.Id;
		po.SubAgentAccount__c =office.Id;
		po.MainAgent__c = agentA.Id;
		po.SubAgent__c = agentA.Id;
		insert po;

		//個人保険特約作成
		createCOV(po);
		PageReference pageRef = Page.IRIS_Policys;
		pageRef.getParameters().put('from','4');
		pageRef.getParameters().put('st', 'comOccDateYmd');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);

		User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		createIDManage(us);
		System.runAs(us){		
			//=====================テスト開始=====================
			Test.startTest();
			I_PolicysController lpc = new I_PolicysController();
			//インスタンス生成
			E_IRISHandler iris = E_IRISHandler.getInstance();
			//募集人Bで参照
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
			lpc.pageAction();
			Integer totalRows = lpc.listSize;
			System.assertEquals(1,totalRows);
			//=====================テスト終了=====================
			Test.stopTest();
		}
	}

	//データ作成
	static Integer num = 0;
	static List<String> conNameList = new List<String>();
	static List<String> usNameList = new List<String>();

	static void clear(){
		num = 0;
		conNameList = new List<String>();
		usNameList = new List<String>();
	}
	//ログイン判定ユーザ
	static User runAsUser(String sfid){
		User thisUser = [ select Id,ContactId from User where Id = :sfid ];
		return thisUser;
	}

	//保険契約ヘッダ
	static void createPolicy(Boolean choice,User us,String conId){
		Account acc = createAccountOwner(us);
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0004';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		System.debug('デバッグ' + po.COMM_OCCDATE__c);
		//String.valueOf(Date.today().year()) + String.valueOf(Date.today().month()+1) + '01';
		//po.COLI_ZCSHVAL__c = 15000;
		po.COLI_ZCSHVAL__c = -1;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		//po.PolicyInCharge__c = true;
		po.OwnerId = us.Id;
		System.debug('保険契約' + po);
		insert po;
		createCOV(po);
	}
	//団体Flag true
	static E_Policy__c createPolicy1(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0002';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		//po.COLI_ZCSHVAL__c = 10000;
		po.COLI_ZCSHVAL__c = null;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.OwnerId = userId;
		insert po;
		createCOV(po);
		return po;
	}
	//団体フラグ true
	static void createPolicy2(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10000;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0001';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		//po.COLI_GRUPNUM__c = null;
		//po.COLI_ZCSHVAL__c = 1000;
		po.COLI_ZCSHVAL__c = -2;
		po.OwnerId = userId;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con2.Id;
		insert po;
		createCOV(po);
	}

	//団体Flag true
	static E_Policy__c createPolicy3(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 500;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0005';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		//po.COLI_ZCSHVAL__c = 10000;
		po.COLI_ZCSHVAL__c = null;
		po.COLI_ZCVDCF__c = true;
		po.COLI_ZUNPCF__c = true;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.OwnerId = userId;
		insert po;
		createCOV(po);
		return po;
	}

	//個人
	static void createPolicy4(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.DCOLI_ZNAME40A01__c = '3テストタイプ';
		po.DCOLI_ZNAME40A02__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0003';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		//po.COLI_ZCSHVAL__c = 20000;
		po.COLI_ZCSHVAL__c = -1;
		po.OwnerId = userId;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;
		insert po;
		createCOV(po);
	}

	//SPVA
	static void createPolicy5(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.DCOLI_ZNAME40A01__c = '3テストタイプ';
		po.DCOLI_ZNAME40A02__c = '3テストタイプ';
		po.Insured__c = con.Id;
		//po.COMM_SINSTAMT__c = 10;
		po.COMM_SINSTAMT__c = null;
		po.COMM_ZTOTPREM__c = 100;
		po.SPVA_ZREFAMT__c  = 500;
		po.COLI_ZGRUPDCF__c = true;
		po.COMM_CHDRNUM__c = 'Test0005';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		po.COLI_ZCSHVAL__c = null;
		po.OwnerId = userId;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
		 				  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_SPVA];
		po.RecordTypeId = rec.Id;
		insert po;
		createCOV(po);
	}

	//SPVA2
	static void createPolicy6(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.DCOLI_ZNAME40A01__c = '3テストタイプ';
		po.DCOLI_ZNAME40A02__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_ZTOTPREM__c = 500;
		po.SPVA_ZREFAMT__c  = 500;
		po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0006';
		po.COLI_ZGRUPDCF__c = true;
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}

		po.COLI_ZCSHVAL__c = null;
		po.OwnerId = userId;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
		 				  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_SPVA];
		po.RecordTypeId = rec.Id;
		insert po;
		createCOV(po);
	}

	//個人保険特約
	static void createCOV(E_Policy__c po){
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		cov.COLI_ZCRIND__c = 'C';
		cov.COMM_ZCOVRNAM__c = '1テストタイプ';
		cov.COMM_ZNAME40A01__c = '1テストタイプ';
		cov.COMM_ZNAME40A02__c = '1テストタイプ';
		insert cov;
	}

	//募集人（取引先責任者)
	static Contact cretateContact(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//募集人（取引先責任者)
	static Contact cretateContact2(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//親取引先
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//親取引先2
	static Account createParentAccount2(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//取引先
	static Account createAccount(){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先2
	static Account createAccount2(){
		Account acc = createParentAccount2();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先3
	static Account createAccountOwner(User us){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		acp.OwnerId = us.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//ユーザ
	static void createUser(Contact con,Account acc){
		User us = new User();
//		us.Username = 'test@test.com@sfdc.com';
		us.Username = 'testUser@terrasky.ingtesting' + String.valueOf(++num);
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
	}
	//ユーザ（拠点長)
	static User createUserKyotentyo(){
		User us = new User();
//		us.Username = 'kyotentyo@test.com@sfdc.com';
		us.Username = 'testUser@terrasky.ingtesting';
		us.Alias = '拠点長';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('拠点長');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		//us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		//us.ContactId = con.Id;
		insert us;
		return us;
	}

	//ユーザ
	static User createRunAsUser(String proName){
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		System.runAs(runAsUs){
//			us.Username = 'test@test.com@sfdc.com' + String.valueOf(++num);
			us.Username = 'testUser@terrasky.ingtesting' + String.valueOf(++num);
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト' + String.valueOf(++num);
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(proName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.IsActive = true;

			insert us;

			TestI_TestUtil.createIDCPF(true, us.Id, null);
		}
		return us;
	}

	//ID管理
	static void createIDManage(User us){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.User__c = us.Id;
		ic.AppMode__c = 'IRIS';
		ic.ZINQUIRR__c = 'BR00';
		insert ic;
	}

	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

}