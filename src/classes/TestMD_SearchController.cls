/**
 * 
 */
@isTest
private class TestMD_SearchController {
	// 位置情報(日本橋)
	private static String pLat = '35.68296479016284';
	private static String pLon = '139.77340936660767'; 

	// ドクター情報リスト
	private static List<MD_Doctor__c> doctorList;

	/**
	 * sortTypeOptions
	 * 正常
	 */
	static testMethod void sortTypeOptionsTest01() {
		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();

		Test.stopTest();

		// assert
		System.assertEquals(2, con.sortTypeOptions.size());
		System.assertEquals(MD_Const.SELECT_LABEL_SORT_LOCATION, con.sortTypeOptions[0].getLabel());
		System.assertEquals(MD_Const.DOCTOR_COL_API_LOCATION, con.sortTypeOptions[0].getValue());
		System.assertEquals(MD_Const.SELECT_LABEL_SORT_EXAMINATIONDATE, con.sortTypeOptions[1].getLabel());
		System.assertEquals(MD_Const.DOCTOR_COL_API_EXAMINATIONDATE, con.sortTypeOptions[1].getValue());
	}

	/**
	 * void init()
	 * 正常
	 */
	static testMethod void initTest01() {
		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.init();

		Test.stopTest();

		// assert
		// [TODO] accesslog
		System.assertEquals(false, con.isError);
		System.assertEquals(false, con.isSearched);
		System.assertEquals(false, con.isSearchResultOverLimit);
		System.assertEquals(MD_Const.DOCTOR_COL_API_LOCATION, con.sortType);

		// assert searchCondition
		System.assertEquals(5, con.searchCondition.selectedAges.size());
		System.assertEquals(MD_Const.SELECTED_NONE_VALUE, con.searchCondition.selectedGender);
		System.assertEquals(MD_Const.SELECTED_NONE_VALUE, con.searchCondition.selectedBlood);
		//System.assertEquals(MD_Const.SELECTED_NONE_VALUE, con.searchCondition.selectedParticularDoctor);
		System.assertEquals(MD_Const.SELECTED_NONE_VALUE, con.searchCondition.selectedElectrocardiogram);
		System.assertEquals(MD_Const.SELECTED_NONE_VALUE, con.searchCondition.selectedAge);
		//System.assertEquals('3', con.searchCondition.selectedDistance);
	}

	/**
	 * void init()
	 * エラー
	 */

	/**
	 * void initPrint()
	 * 
	 */
	static testMethod void initPrintTest01() {
		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.init();

		// 検索条件セット
		con.searchCondition.searchAges = new List<String>();
		con.searchCondition.searchAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' < 40)');
		con.searchCondition.searchAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 70)');
		con.searchCondition.searchGender = MD_Const.DOCTOR_COL_API_GENDER + ' = \'M\'';
		con.searchCondition.searchBlood = MD_Const.DOCTOR_COL_API_BLOOD + ' = \'' + '有' + '\'';
		//con.searchCondition.searchParticularDoctor = MD_Const.DOCTOR_COL_API_PARTICULARDOCTOR + ' = \'Y\'';
		con.searchCondition.searchElectrocardiogram = MD_Const.DOCTOR_COL_API_ELECTROCARDIOGRAM + ' = \'' + '有' + '\'';
		con.searchCondition.searchDistance = '10';

		con.initPrint();

		Test.stopTest();

		// assert
		System.assertEquals('30歳代まで,70歳代以上', con.searchCondition.selectedAgeLabel);
		System.assertEquals('男性', con.searchCondition.selectedGenderLabel);
		System.assertEquals('有', con.searchCondition.selectedBloodLabel);
		//System.assertEquals('該当', con.searchCondition.selectedParticularDoctorLabel);
		System.assertEquals('有', con.searchCondition.selectedElectrocardiogramLabel);
		System.assertEquals('10Km', con.searchCondition.selectedDistanceLabel);
	}

	/**
	 * void init()
	 * エラー
	 */

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]年齢
	 */
	static testMethod void doSearchTest01() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('1'));
		docList.add(createDoctor('2'));
		docList.add(createDoctor('3'));
		docList.add(createDoctor('4'));
		docList.add(createDoctor('5'));

		Date d = Date.today();
		docList[0].Birth__c = d.addYears(-32);	// 32歳
		docList[1].Birth__c = d.addYears(-42);	// 42歳
		docList[2].Birth__c = d.addYears(-52);	// 52歳
		docList[3].Birth__c = d.addYears(-62);	// 62歳
		docList[4].Birth__c = d.addYears(-72);	// 72歳

		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		// 年齢：30歳代まで
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' < 40)');
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);

		// 年齢：40歳代
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 40 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 50)');
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[1].Id, con.doctorList[0].Id);

		// 年齢：50歳代
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 50 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 60)');
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[2].Id, con.doctorList[0].Id);

		// 年齢：60歳代
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 60 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 70)');
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[3].Id, con.doctorList[0].Id);

		// 年齢：70歳代以上
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 70)');
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[4].Id, con.doctorList[0].Id);

		// 年齢：30歳代まで,50歳代,70歳代以上
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' < 40)');
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 50 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 60)');
		con.searchCondition.selectedAges.add('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 70)');
		con.doSearch();
		System.assertEquals(3, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);
		System.assertEquals(docList[2].Id, con.doctorList[1].Id);
		System.assertEquals(docList[4].Id, con.doctorList[2].Id);

		// 年齢：チェックなし
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.doSearch();
		System.assertEquals(5, con.doctorList.size());

		Test.stopTest();
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]性別
	 */
	static testMethod void doSearchTest02() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('1'));
		docList.add(createDoctor('2'));

		docList[0].Gender__c = 'M';	// 男性
		docList[1].Gender__c = 'F';	// 女性

		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		// 性別：男性
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedGender = con.searchCondition.getGenderOptions()[0].getValue();
		system.debug(con.searchCondition.selectedGender);
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);

		// 性別：女性
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedGender = con.searchCondition.getGenderOptions()[1].getValue();
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[1].Id, con.doctorList[0].Id);

		// 性別：指定なし
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedGender = con.searchCondition.getGenderOptions()[2].getValue();
		con.doSearch();
		System.assertEquals(2, con.doctorList.size());

		Test.stopTest();
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]血液検査
	 */
	static testMethod void doSearchTest03() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('1'));
		docList.add(createDoctor('2'));

		docList[0].Blood__c = '有';	// 有
		docList[1].Blood__c = '無';	// 無

		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		// 血液検査：有
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedBlood = con.searchCondition.getBloodOptions()[0].getValue();
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);

		// 血液検査：無
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedBlood = con.searchCondition.getBloodOptions()[1].getValue();
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[1].Id, con.doctorList[0].Id);

		// 血液検査：指定なし
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedBlood = con.searchCondition.getBloodOptions()[2].getValue();
		con.doSearch();
		System.assertEquals(2, con.doctorList.size());

		Test.stopTest();
	}


	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]心電図
	 */
	static testMethod void doSearchTest05() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('1'));
		docList.add(createDoctor('2'));

		docList[0].Electrocardiogram__c = '有';	// 有
		docList[1].Electrocardiogram__c = '無';	// 無

		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		// 心電図：有
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedElectrocardiogram = con.searchCondition.getElectrocardiogramOptions()[0].getValue();
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);

		// 心電図：無
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedElectrocardiogram = con.searchCondition.getElectrocardiogramOptions()[1].getValue();
		con.doSearch();
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[1].Id, con.doctorList[0].Id);

		// 心電図：指定なし
		con.init();
		con.searchCondition.selectedAges = new List<String>();
		con.searchCondition.selectedElectrocardiogram = con.searchCondition.getElectrocardiogramOptions()[2].getValue();
		con.doSearch();
		System.assertEquals(2, con.doctorList.size());

		Test.stopTest();
	}

	/**
	 * void doSearch()
	 * 正常
	 * [検索条件]距離
	 */
	static testMethod void doSearchTest06(){
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('0-5'));
		docList.add(createDoctor('5-10'));
		docList.add(createDoctor('10-20'));
		docList.add(createDoctor('20-50'));
		docList.add(createDoctor('50-100'));
		docList.add(createDoctor('100over'));

		docList[0].Location__Latitude__s = 35.715201;	// 0～5km以内	35.715201, 139.793970
		docList[0].Location__Longitude__s = 139.793970;
		docList[1].Location__Latitude__s = 35.749524;	// 5～10km以内	35.749524, 139.803572
		docList[1].Location__Longitude__s = 139.803572;
		docList[2].Location__Latitude__s = 35.822149;	// 10～20km以内	35.822149, 139.838497
		docList[2].Location__Longitude__s = 139.838497;
		docList[3].Location__Latitude__s = 35.950618;	// 20～50km以内	35.950618, 139.977592
		docList[3].Location__Longitude__s = 139.977592;
		docList[4].Location__Latitude__s = 36.346252;	// 50～100km以内	36.346252, 140.304779
		docList[4].Location__Longitude__s = 140.304779;
		docList[5].Location__Latitude__s = 36.962635;	// 100km以上	36.962635, 140.058605
		docList[5].Location__Longitude__s = 140.058605;

		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;
		con.init();
		con.searchCondition.selectedAges = new List<String>();

		// 指定位置からの距離：5km
		con.searchCondition.selectedDistance = con.searchCondition.getDistanceOptions()[3].getValue();
		con.searchCondition.setZoom(5);
		con.doSearch();
		System.assertEquals(6, con.searchCondition.zoom);
		System.assertEquals(1, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);

		// 指定位置からの距離：10km
		con.searchCondition.selectedDistance = con.searchCondition.getDistanceOptions()[4].getValue();
		con.searchCondition.setZoom(10);
		con.doSearch();
		System.assertEquals(5, con.searchCondition.zoom);
		System.assertEquals(2, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);
		System.assertEquals(docList[1].Id, con.doctorList[1].Id);

		// 指定位置からの距離：20km
		con.searchCondition.selectedDistance = con.searchCondition.getDistanceOptions()[5].getValue();
		con.searchCondition.setZoom(20);
		con.doSearch();
		System.assertEquals(4, con.searchCondition.zoom);
		System.assertEquals(3, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);
		System.assertEquals(docList[1].Id, con.doctorList[1].Id);
		System.assertEquals(docList[2].Id, con.doctorList[2].Id);

		// 指定位置からの距離：50km
		con.searchCondition.selectedDistance = con.searchCondition.getDistanceOptions()[6].getValue();
		con.searchCondition.setZoom(50);
		con.doSearch();
		System.assertEquals(3, con.searchCondition.zoom);
		System.assertEquals(4, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);
		System.assertEquals(docList[1].Id, con.doctorList[1].Id);
		System.assertEquals(docList[2].Id, con.doctorList[2].Id);
		System.assertEquals(docList[3].Id, con.doctorList[3].Id);

		// 指定位置からの距離：100km
		con.searchCondition.selectedDistance = con.searchCondition.getDistanceOptions()[7].getValue();
		con.searchCondition.setZoom(100);
		con.doSearch();
		System.assertEquals(2, con.searchCondition.zoom);
		System.assertEquals(5, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);
		System.assertEquals(docList[1].Id, con.doctorList[1].Id);
		System.assertEquals(docList[2].Id, con.doctorList[2].Id);
		System.assertEquals(docList[3].Id, con.doctorList[3].Id);
		System.assertEquals(docList[4].Id, con.doctorList[4].Id);

		Test.stopTest();
	}

	/**
	 * void doSearch()
	 * 正常
	 * 並び順
	 */
	static testMethod void doSearchTest07() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('1'));
		docList.add(createDoctor('2'));

		docList[0].Location__Latitude__s = 35.6804985281479;	// 八重洲
		docList[0].Location__Longitude__s = 139.77049112319946;
		docList[1].Location__Latitude__s = 35.678842691092036;	// 丸の内
		docList[1].Location__Longitude__s = 139.76197242736816;

		Date d = Date.today();
		docList[0].ExaminationDate__c = d.addMonths(-2);	// 2ヶ月前
		docList[1].ExaminationDate__c = d.addMonths(-1);	// 1ヶ月前

		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		con.init();
		con.searchCondition.selectedAges = new List<String>();

		// 指定された位置から近い順（デフォルト）
		con.doSearch();
		System.assertEquals(2, con.doctorList.size());
		System.assertEquals(docList[0].Id, con.doctorList[0].Id);
		System.assertEquals(docList[1].Id, con.doctorList[1].Id);

		// 直近の診査日
		con.sortType = MD_Const.DOCTOR_COL_API_EXAMINATIONDATE;
		con.doSearch();
		System.assertEquals(2, con.doctorList.size());
		System.assertEquals(docList[1].Id, con.doctorList[0].Id);
		System.assertEquals(docList[0].Id, con.doctorList[1].Id);

		Test.stopTest();
	}

	/**
	 * void doSearch()
	 * 正常
	 * 取得件数上限30件
	 */
	static testMethod void doSearchTest08() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		for (Integer i = 0; i < 50; i++) {
			docList.add(createDoctor(String.valueOf(i)));
		}
		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		con.init();
		con.searchCondition.selectedAges = new List<String>();
		
		con.doSearch();

		Test.stopTest();

		System.assertEquals(30, con.doctorList.size());
	}

	/**
	 * void doSearch()
	 * エラー　位置情報取得できない
	 * 住所検索
	 */
	static testMethod void doSearchTest09() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		for (Integer i = 0; i < 3; i++) {
			docList.add(createDoctor(String.valueOf(i)));
		}
		insert docList;

		// 住所から検索
		PageReference pr = Page.E_MDAddress;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();

		con.init();
		con.searchCondition.selectedAges = new List<String>();
		
		con.doSearch();

		Test.stopTest();

		System.assertEquals(true, con.isError);
	}

	/**
	 * void doSearch()
	 * エラー　位置情報取得できない
	 * エリア検索
	 */
	static testMethod void doSearchTest10() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		for (Integer i = 0; i < 3; i++) {
			docList.add(createDoctor(String.valueOf(i)));
		}
		insert docList;

		// エリアから検索
		PageReference pr = Page.E_MDArea;
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();

		con.init();
		con.searchCondition.selectedAges = new List<String>();
		
		con.doSearch();

		Test.stopTest();

		System.assertEquals(true, con.isError);
	}

	/**
	 * void doSearchDistance()
	 * 正常
	 */
	static testMethod void doSearchDistanceTest01() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		docList.add(createDoctor('1'));
		docList.add(createDoctor('2'));
		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		//pr.getParameters().put('changeDistance', '10');
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;
		con.nnMD_changeDistance = '10';

		con.init();
		con.searchCondition.selectedAges = new List<String>();
		
		con.doSearchDistance();

		Test.stopTest();

		System.assertEquals('10', con.searchCondition.selectedDistance);
	}

	/**
	 * void doPrint()
	 * 正常
	 * 
	 */
	static testMethod void doPrintTest01() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		for (Integer i = 0; i < 50; i++) {
			docList.add(createDoctor(String.valueOf(i)));
		}
		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		con.init();
		con.searchCondition.selectedAges = new List<String>();
		
		con.doSearch();
		con.wrapDoctorList[0].isCheck = true;
		con.wrapDoctorList[10].isCheck = true;
		con.wrapDoctorList[20].isCheck = true;
		con.initPrint();
		PageReference resultPr = con.doPrint();

		Test.stopTest();

		System.assertEquals(Page.E_MDList_print.getUrl(), resultPr.getUrl());
		System.assertEquals(3, con.wrapPrintList.size());
		System.assertEquals(1, con.wrapPrintList[0].pno);
		System.assertEquals(2, con.wrapPrintList[1].pno);
		System.assertEquals(3, con.wrapPrintList[2].pno);
	}

	/**
	 * void doPrint()
	 * エラー　未選択
	 * 
	 */
	static testMethod void doPrintTest02() {
		// Test Data
		List<MD_Doctor__c> docList = new List<MD_Doctor__c>();
		for (Integer i = 0; i < 50; i++) {
			docList.add(createDoctor(String.valueOf(i)));
		}
		insert docList;

		// 現在地から検索
		PageReference pr = Page.E_MDLocation;
		//pr.getParameters().put('lat', pLat);
		//pr.getParameters().put('lon', pLon);
		Test.setCurrentPage(pr);

		Test.startTest();

		MD_SearchController con = new MD_SearchController();
		con.nnMD_Search_lat = pLat;
		con.nnMD_Search_lon = pLon;

		con.init();
		con.searchCondition.selectedAges = new List<String>();
		
		con.doSearch();
		con.initPrint();
		PageReference resultPr = con.doPrint();

		Test.stopTest();

		System.assertEquals(null, resultPr);
		System.assertEquals(true, con.isError);
	}

	/* Test Data　位置情報固定 */
	private static MD_Doctor__c createDoctor(String no) {
		MD_Doctor__c doc = new MD_Doctor__c();
		doc.Name = 'test doc' + no;
		doc.Name_kana__c = 'テストドクター';
		doc.HospitalName__c = 'test hospital' + no;
		doc.DoctorNumber__c = no;
		doc.Phone__c = '0312345678';
		doc.PostalCode__c = '123-45678';
		doc.Location__Latitude__s = Decimal.valueOf(pLat);
		doc.Location__Longitude__s = Decimal.valueOf(pLon);
		return doc;
	}
}