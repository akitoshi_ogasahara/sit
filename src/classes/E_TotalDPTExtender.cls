/**
 * 保険料逓減推移画面拡張クラス
 * CreatedDate 2015/8/4
 * @Author Terrasky
 */
global with sharing class E_TotalDPTExtender extends E_AbstractViewExtender{
	
	E_TotalDPTController extension;
	
	/** ページタイトル */
//	private static final String PAGE_TITLE = '合計保険料逓減推移';
	private static final String PAGE_TITLE = '保険料逓減払込方式に関する特則による保険料推移';

	
	public E_TotalDPTExtender(E_TotalDPTController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;

		SkyEditor2.Query tableQuery1 = extension.queryMap.get('dataTableDPT');
		tableQuery1.addWhereIfNotFirst('AND');
		//データテーブルのWhere句拡張　 [保険料逓減日（基準日）]が[照会日]以降のものを表示する 
		tableQuery1.addWhere(' DCPDATE__c >= \'' + (E_bizDataSyncLogDao.getNotBlankEndDateByLast1() == null ? '99999999' : E_Util.formatDataSyncDate('yyyy0101')) + '\'');
		extension.queryMap.put('dataTableSVFPF',tableQuery1);
		
		//todo：当契約の個人保険特約にP逓減特則が付加されているかのチェックの要否確認

	}

	/* init */
	global override void init(){
		// アクセスチェック
		pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
/*
		if(pageRef == null){
			//表示データが無い場合、エラー画面に遷移する
			if(extension.dataTableDPT.items.size() == 0){
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_NODATA);
			}
		}
*/
	}
	
	public pagereference pageAction() {
        return E_Util.toErrorPage(pageRef,null);
	}

	
}