@isTest
private with sharing class TestCR_AgencySearchOSSVEController{
		private static testMethod void testPageMethods() {	
			CR_AgencySearchOSSVEController page = new CR_AgencySearchOSSVEController(new ApexPages.StandardController(new CR_Agency__c()));	
			page.getOperatorOptions_CR_Agency_c_AgencyCode_c();	
			page.getOperatorOptions_CR_Agency_c_AgencyNameKana_c();	
			page.getOperatorOptions_CR_Agency_c_OutOfBusiness_c();	
			page.getOperatorOptions_CR_Agency_c_MRUnit_c();	
			page.getOperatorOptions_CR_Agency_c_MRCode_c();	
			page.getOperatorOptions_CR_Agency_c_HasOutsrc_c();	
			page.getOperatorOptions_CR_Agency_c_HasConfirmationInOutSrc_c();	
			System.assert(true);
		}	
			
	private static testMethod void testresultTable() {
		CR_AgencySearchOSSVEController.resultTable resultTable = new CR_AgencySearchOSSVEController.resultTable(new List<CR_Agency__c>(), new List<CR_AgencySearchOSSVEController.resultTableItem>(), new List<CR_Agency__c>(), null);
		resultTable.create(new CR_Agency__c());
		resultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
}