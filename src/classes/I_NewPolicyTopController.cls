public with sharing class I_NewPolicyTopController extends I_AbstractController{
	
	private static final Integer NUMBER_OF_DISPLY= 3;
	private List<I_NewPolicyDTO.NewPolicy> polys;
	
	public I_NewPolicyTopController() {
		//Constructor
	}
	
	
	
	
	/*
	 * NUMBER_OF_DISPLYの件数分作成
	 */
	public List<I_NewPolicyDTO.NewPolicy> getNewPolicys() {
		if(polys==null){
			polys = new List<I_NewPolicyDTO.NewPolicy>();
			for(E_NewPolicy__c rec :E_NewPolicyDao.getNewPolicysWithNumberOfLimit(NUMBER_OF_DISPLY)){
				polys.add(new I_NewPolicyDTO.NewPolicy(rec));
			}
/* NUMBER_OF_DISPLY　未満の場合も空レコードは生成しない　UXOUAT-188
			for(Integer i = newPolicys.size(); i < NUMBER_OF_DISPLY; i++) {
				newPolicys.add(new I_NewPolicyDTO.NewPolicy());
			}
*/
		}
		return polys;
	}
	public Map<String, I_MenuItem> getTopMenus(){
		return iris.irisMenus.MenuItemsByKey;
	}
	
	public Boolean getHasRecord(){
		return getNewPolicys().size()>0;
	}
}