/**
 * 保険料未入金通知(E_NCOL__c)のデータアクセスクラス
 */
public with sharing class E_NCOLDao {

	/**
	 * ソート項目によるデータ取得
	 * @param String ソート項目
	 * @param Boolean ソート順
	 * @return List<E_NCOL__c>
	 */
	public static List<E_NCOL__c> getRecsBySortKey(String policyCondition){
		// 3ヶ月
		Set<String> periods = E_DownloadNoticeUtil.getPeriodSetNCOL();

		String soql = ' Select ';
		soql += ' Id ';
		soql += ' ,Name ';
		soql += ' ,ParentAccountId__c ';
		soql += ' ,ParentAccountName__c ';
		soql += ' ,AccountId__c ';
		soql += ' ,AccountName__c ';
		soql += ' ,ContactName__c ';
		soql += ' ,AccountOwnerName__c ';
		soql += ' ,Contact__r.Id ';
		soql += ' ,E_Policy__c';
		soql += ' ,OWNAME__c ';
		soql += ' ,IsZGRUP__c ';
		soql += ' ,CHDRNUM__c ';
		soql += ' ,ZGRUPNUM__c ';
		soql += ' ,GrpOrPolicyNo__c ';
		soql += ' ,CNTTYPE__c ';
		soql += ' ,ZCOVRNAM__c ';
		/* SITP対応 */
		soql += ' ,InsuranceType__c ';
		soql += ' ,INSTAMT12__c ';
		soql += ' ,ZSUMINS12N__c ';
		soql += ' ,ZCSVN__c ';
		soql +=	' ,ZBLGAGCY__c ';
		soql +=	' ,KBILLDESC__c ';
		soql +=	' ,CreatedDate ';
		//外字対応
		soql +=	' ,ContractorName__c ';

		soql +=  ',E_Policy__r.COLI_ZCSHVAL__c';
		soql +=  ',E_Policy__r.COLI_ZEAPLTOT__c';
		soql +=  ',E_Policy__r.COLI_ZEPLTOT__c';
		soql +=  ',E_Policy__r.COLI_ZEADVPRM__c';
		soql +=  ',E_Policy__r.COLI_ZUNPREM__c';
		soql +=  ',E_Policy__r.COLI_ZCVDCF__c';
		soql +=  ',E_Policy__r.COLI_ZUNPCF__c';
		soql +=  ',E_Policy__r.COLI_ZUNPDCF__c';
		soql +=  ',E_Policy__r.MainAgent__c';
		soql +=  ',E_Policy__r.SubAgent__c';
		soql +=  ',E_Policy__r.COMM_OCCDATE_yyyyMMdd__c';
		soql +=  ',E_Policy__r.PolicyElapsedYears__c';
		soql += ' From ';
		soql += ' E_NCOL__c ';
		soql += ' Where ';
		soql += ' E_Policy__r.recordTypeId = \'' + E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI) + '\' ';	// Coliのみ
		soql += ' And ';
		soql += ' YYYYMM__c In :periods ';
		soql += policyCondition;
		//soql += queryOrder;
		soql += ' Order by CreatedDate desc NULLS LAST';
		soql += ' ,AccountName__c desc NULLS LAST';
		soql += ' ,ContactName__c desc NULLS LAST';
		soql += ' Limit 1001';

		return Database.query(soql);
	}
}