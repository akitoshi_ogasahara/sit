public with sharing class E_DownloadHistoryDao {
	
	/**
	 * 代理店提出とフォームNoをキーに、ダウンロード履歴情報を取得する(作成日が90日以内)
	 * @param String 代理店提出ID
	 * @param String フォームNo
	 * @return List<E_DownloadHistorry__c>
	 */
	public static List<E_DownloadHistorry__c> getRecsByAgencyAndFormNo(String agencyId, String formNo, Date createdBefore){
		return [
					Select 
						Id, RequestDateTime__c, OutputRecordYM__c, Message__c, Status__c, StatusLabel__c,Conditions__c,
						(Select Id From Attachments) 
					From 
						E_DownloadHistorry__c 
					Where 
						CR_Agency__c = :agencyId
					And
						FormNo__c = :formNo
					And
						CreatedDate >= :createdBefore
					Order By 
						RequestDateTime__c desc, OutputRecordYM__c desc
		];
	}
	
	/**
	 * ダウンロード履歴作成
	 * @param String 代理店提出レコードID
	 * @param Stirng 帳票種別
	 * @param String 出力基準年月
	 * @param String 検索条件
	 */
	public static E_DownloadHistorry__c createDlHistory(String agencyId, String formNo, String outputYm, String dlCondition){
		// ダウンロード履歴作成
		E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
		dh.Status__c = CR_Const.DL_STATUS_WAIT;
		dh.RequestDateTime__c = Datetime.now();
		dh.CR_Agency__c = agencyId;
		dh.FormNo__c = formNo;
		dh.Conditions__c = dlCondition;
		
		// 月の0埋め処理
		if(String.isNotBlank(outputYm)){			//日付が空欄の場合エラーとなるためスキップ
			List<String> ym = outputYm.split('/');
			if(ym.size()>1){
				dh.OutputRecordYM__c = ym[0] + E_Util.leftPad(ym[1], 2, '0');
			}
		}
	    
		return dh;
	}
}