public with sharing class MD_DoctorTriggerBizLogic {
	
	/**
	 * 検索用テキストに値を設定する
	 * カタカナ、英語、数字を全角から半角へ変換
	 */
	public void createSearchTextEmToEn(List<MD_Doctor__c> newList){
		for(MD_Doctor__c rec : newList){
			// 嘱託医名
			rec.SearchName__c = E_Util.emToEn(rec.Name);
			
			// 病院名
			rec.SearchHospitalName__c = E_Util.emToEn(rec.HospitalName__c);
		}
	}
	
	/**
	 * 住所情報が変更された場合、緯度経度をブランク更新する
	 */
	public void updateLocationBlank(List<MD_Doctor__c> newList, Map<Id, MD_Doctor__c> oldMap) {
		for (MD_Doctor__c rec : newList) {
			// 更新前レコード取得
			MD_Doctor__c oldRec = oldMap.get(rec.Id);

			// 住所情報が更新された場合、緯度経度をブランク更新
			if (hasUpdateAddress(rec, oldRec)) {
				rec.Location__Longitude__s = null;
				rec.Location__Latitude__s = null;
			}
		}
	}

	/**
	 * 住所情報の比較
	 */
	private Boolean hasUpdateAddress(MD_Doctor__c rec, MD_Doctor__c oldRec) {
		// 都道府県
		if (isSameValue(rec.State__c, oldRec.State__c) == false) {
			return true;
		// 住所
		} else if (isSameValue(rec.Address__c, oldRec.Address__c) == false) {
			return true;
		// 更新なし
		} else {
			return false;
		}
	}

	/**
	 * 文字列の比較
	 */
	private Boolean isSameValue(String targetStr1, String targetStr2) {
		return String.isNotBlank(targetStr1) && targetStr1.equals(targetStr2);
	}
}