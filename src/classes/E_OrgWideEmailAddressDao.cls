/**
 * メール作成時のメール送信者名・アドレスを取得するためのDao(Without)
 */
public without sharing class E_OrgWideEmailAddressDao {
	
	public static OrgWideEmailAddress getOrgWideEmailAddress(String displayName){
		OrgWideEmailAddress rec = null;
		List<OrgWideEmailAddress> recs =
		[SELECT
				Address,
				DisplayName
		FROM
				OrgWideEmailAddress
		WHERE
				DisplayName = :displayName
		];
		if(recs.size() > 0){
			rec = recs[0];
		}
		return rec;
	}
}