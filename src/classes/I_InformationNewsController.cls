public with sharing class I_InformationNewsController {

	public I_ContentMaster__c getInfoNews(){
		//『お知らせ』セクションレコードを紐づくレコードとともに取得

		//20180214 資料発送申込対応 START
		//for(I_ContentMaster__c cm:I_ContentMasterDao.getRecordsInfoContent()){
		//	return cm;
		//}
		//return null;
		return I_ContentMasterDao.getOshiraseInfo();
		//20180214 資料発送申込対応 END
	}

}