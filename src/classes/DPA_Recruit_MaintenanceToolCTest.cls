@isTest
private class DPA_Recruit_MaintenanceToolCTest {
	@isTest
    static void ImportBtnTest() {
        PageReference pageRef = Page.DPA_Recruit_MaintenanceTool;
        Test.setCurrentPage(pageRef);
        DPA_Recruit_MaintenanceToolController tool = new DPA_Recruit_MaintenanceToolController();
        
        // ファイルを指定しなかった場合
        System.assert(tool.ImportBtn() == NULL);
        
        // テスト用Account
        Account ac = new Account(name = 'test', E_CL2PF_ZAGCYNUM__c = 'X0001');
        insert ac;
        
        // 正常系 位置情報初期化
        tool.isDMLExceptionTest = false;
        tool.file = blob.valueOf('X0001,0,0,0');
        System.assert(tool.ImportBtn() == NULL);
        
        // 正常系 位置情報セット
        tool.isDMLExceptionTest = false;
        tool.file = blob.valueOf('X0001,1,35.779261,139.835751');
        System.assert(tool.ImportBtn() == NULL);
        
        // 無効データ
        tool.isDMLExceptionTest = false;
        tool.file = blob.valueOf('');
        System.assert(tool.ImportBtn() == NULL);
        
        // DMLException
        tool.isDMLExceptionTest = true;
        tool.file = blob.valueOf('X0001,0,0,0');
        System.assert(tool.ImportBtn() == NULL);        
    }
}