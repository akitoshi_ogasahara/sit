@isTest
private class TestE_DownloadController {
	
	static testmethod void doReturnTest() {
		test.startTest();
			
			E_DownloadController controller = new E_DownloadController();
			PageReference prrtn = controller.doReturn();

			PageReference pr = Page.E_DownloadPrintSheetsSelect;
			pr.getParameters().put('isSearched','true');
		test.stopTest();

		system.assertEquals(prrtn.getUrl(),pr.getUrl());
	}
	
	static testmethod void validateDateTest() {
		test.startTest();
			
			E_DownloadController controller = new E_DownloadController();
			
		test.stopTest();
		system.assert(!controller.validateDate('','',true,''));
		system.assert(!controller.validateDate('13','',true,''));
		system.assert(controller.validateDate('2000/12','',true,''));

		system.assert(!controller.validateDates('', '', '', true));
		system.assert(!controller.validateDates('2000/12', '1999/12', '', true));
	}

	static testmethod void getOfficeOptionsTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);

			E_DownloadController controller = new E_DownloadController();
			controller.distributor = acc;
			controller.office = officeRec;
			List<SelectOption> opps = controller.getActiveOfficeOptions();
			List<SelectOption> opps2 = controller.getOfficeOptions();

		test.stopTest();
		system.assertEquals(1, opps.size());
		system.assertEquals(officeRec.id, opps[0].getValue());
		system.assertEquals(1, opps2.size());
		system.assertEquals(officeRec.id, opps2[0].getValue());
		
	}
	static testmethod void isValidateTest() {
		test.startTest();
			E_DownloadController controller = new E_DownloadController();
		test.stopTest();
		system.assert(controller.isValidate());
	}

	static testmethod void initTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			System.runAs(agentUser) {
				E_DownloadController controller = new E_DownloadController();
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				controller.getIsViewNewPolicyDL();
				system.assertEquals(controller.access.isSumiseiUser(), controller.getIsViewNewPolicyDL());
				system.assert(!controller.getIsEmployee());
			}
		test.stopTest();
	}
	static testmethod void getActorATTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AT');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorAYTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AY');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorAHTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorERRTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'ZZ');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(!controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorPolicyConditionTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition(I_Const.CONDITION_FIELD_AGENCY, acc.name, acc.id, acc.name);

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorPolicyConditionTest2() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition(I_Const.CONDITION_FIELD_OFFICE, officeRec.name, officeRec.id, officeRec.name);

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorPolicyConditionTest3() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition(I_Const.CONDITION_FIELD_AGENT, con.LastName, con.id, con.LastName);

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorPolicyConditionTest4() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');
			User agentUser = TestI_TestUtil.createAgentUser(true,'AgentUser', 'E_PartnerCommunity',con.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition('a', con.LastName, con.id, con.LastName);

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(!controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getIsViewNewPolicyDLEmployeeTest() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');

			User agentUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				PageReference pr = Page.IRIS_Agency;
				Test.setCurrentPage(pr);
				pr.getParameters().put('did', acc.Id); 

				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition('a', con.LastName, con.id, con.LastName);

				controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(!controller.getIsViewNewPolicyDL());
				system.assert(controller.getActor());
				
				
			}
		test.stopTest();
	}
	static testmethod void getIsViewNewPolicyDLEmployeeTest2() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			officeRec = [select id,name,E_ParentZHEADAY__c ,E_CL1PF_ZHEADAY__c, E_COMM_VALIDFLAG__c, ZMRCODE__c, E_IsAgency__c From Account Where Id =: officeRec.Id ];
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');

			User agentUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);
				
				PageReference pr = Page.IRIS_Agency;
				Test.setCurrentPage(pr);
				pr.getParameters().put('oid', officeRec.Id); 

				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition('a', officeRec.Name, officeRec.id, officeRec.Name);

				//controller.distributor = acc;
				controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(!controller.getIsViewNewPolicyDL());
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getIsViewNewPolicyDLEmployeeTest3() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');

			User agentUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);

				PageReference pr = Page.IRIS_Agency;
				Test.setCurrentPage(pr);
				pr.getParameters().put('aid', con.Id); 
				
				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition('a', con.LastName, con.id, con.LastName);

				//controller.distributor = acc;
				//controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(!controller.getIsViewNewPolicyDL());
				system.assert(controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getActorERRTest2() {
		test.startTest();
			Account acc = TestI_TestUtil.createAccount(true,null);
			Account officeRec = TestI_TestUtil.createAccount(true,acc);
			Contact con = TestI_TestUtil.createContact(true, officeRec.Id, 'AgentName');

			User agentUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, agentUser.Id, 'AH');
			System.runAs(agentUser) {
				TestI_TestUtil.createBasePermissions(agentUser.Id);

				PageReference pr = Page.IRIS_Agency;
				Test.setCurrentPage(pr);
				//pr.getParameters().put('aid', con.Id); 
				
				E_DownloadController controller = new E_DownloadController();
				controller.iris.setPolicyCondition('a', con.LastName, con.id, con.LastName);

				//controller.distributor = acc;
				//controller.office = officeRec;
				controller.agent = con;
				controller.setPgTitle('TestPage');
				controller.init();
				system.assert(!controller.getIsViewNewPolicyDL());
				system.assert(!controller.getActor());
				
			}
		test.stopTest();
	}
	static testmethod void getDownloadIdTest() {
		test.startTest();
			E_DownloadController controller = new E_DownloadController();
		test.stopTest();

		system.assertEquals('',controller.getDownloadId());
	}

	static testmethod void getDownloadIdTest2() {
		test.startTest();
			E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
			insert dh;
			E_DownloadController controller = new E_DownloadController();
			controller.dh=dh;
		test.stopTest();

		system.assertEquals(dh.id,controller.getDownloadId());
	}
	static testmethod void otherTest() {
		test.startTest();
			E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
			insert dh;
			E_DownloadController controller = new E_DownloadController();
			controller.formNo='123';
			controller.getIsSupportBrowser();
		test.stopTest();

		system.assertEquals(!E_Util.isUserAgentIE8(),controller.getIsSupportBrowser());
	}
}