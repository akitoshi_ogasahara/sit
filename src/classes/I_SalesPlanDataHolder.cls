public with sharing class I_SalesPlanDataHolder {
	// 営業計画ID
	private Id salesPlanId;
	
	//保存時間
	private String saveTimeStamp;
	//保存ユーザ
	private String saveUser;

	// supportItemNames
	private static List<String> SUPPORT_ITEMS = new List<String>{
															'稼動月',
															'定期',
															'無解約定期',
															'無解約収保',
															'逓増定期',
															'生活障害定期',
															'重大疾病保障'
																	};
	// ファイル名
	public String pdfTitle {get; set;}

	//　特定保険種類
	public Decimal spInsType_ly {get; set;}
	public Decimal spInsType_h1 {get; set;}
	public Decimal spInsType_h2 {get; set;}
	public Decimal spInsType_cy {get; set;}
	public Decimal spInsType_total {get{
										if(spInsType_h1 != null && spInsType_h2 != null){
											return spInsType_h1 + spInsType_h2;
										}else if(spInsType_h1 == null && spInsType_h2 == null){
											return null;
										}
										return spInsType_total;
									} set;}
	// 新契約係数ANP
	public Decimal anp_ly {get; set;}
	public Decimal anp_h1 {get; set;}
	public Decimal anp_h2 {get; set;}
	public Decimal anp_cy {get; set;}
	public Decimal anp_total {get{
								if(anp_h1 != null && anp_h2 != null){
									return anp_h1 + anp_h2;
								}else if(anp_h1 == null && anp_h2 == null){
									return null;
								}
								return anp_total;
							} set;}
	// IQA継続率
	public Decimal iqa_ly {get; set;}
	public String iqa_h1 {get; set;}
	public String iqa_h2 {get; set;}
	public Decimal iqa_cy {get; set;}
	public String iqa_total {get; set;}

	// 稼働月、商品別計画
	public List<prdSupportPlan> plans_h1 {get; set;}
	public List<prdSupportPlan> plans_h2 {get; set;}
	// 右列商品以外		prdSupportPlanで管理
	// サポート内容
	public List<supportSchedule> schedules_h1 {get; set;}
	public List<supportSchedule> schedules_h2 {get; set;}
	//希望サポート内容
	public List<String> supportContents{get{
												if(supportContents == null){
													supportContents = new List<String>();
													// 選択リスト値のメタから文字列リストを生成
													Schema.DescribeFieldResult PCFidldResult = E_SalesPlan__c.SupportContent__c.getDescribe();
													List<Schema.PicklistEntry> PCPicklistEntry = PCFidldResult.getPicklistValues();
													for (Schema.PicklistEntry pick : PCPicklistEntry) {
														supportContents.add(String.valueOf(pick.getLabel()));
													}
												}
												return supportContents;			
											}
									private set;
						}
	public List<SelectOption> getSupportContentsSelectOptions(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('',''));		//未選択の選択肢を追加
		for(String s:supportContents){
			options.add(new SelectOption(s,s));
		}
		return options;
	}

	//希望サポート内容の日程選択用　日付SelectOptions
	public static Map<Integer, List<SelectOption>> daysByMonth;		//TODO private
	public Map<Integer, List<SelectOption>> getDaysByMonth(){
		if(daysByMonth==null){
			daysByMonth = new Map<Integer, List<SelectOption>>();
			for(Integer i=1;i<=12;i++){
				daysByMonth.put(i, new List<SelectOption>());
				daysByMonth.get(i).add(new SelectOption(' ',' '));		//未選択の選択肢を追加 '-'半角空文字へ変更
				//うるう年である2020年で月末日を算出
				Integer lastday = Date.newInstance(2020, i+1, 1).adddays(-1).day();		//翌月1日―1日＝月末日
				for(Integer day=1;day<=lastday;day++){
					daysByMonth.get(i).add(new SelectOption(String.valueOf(day),String.valueOf(day)));
				}
			}
		}
		return daysByMonth;
	}

	// 計画・商談内容・メモ欄
	public String memo{get{
							if(!String.isBlank(memo)){
								String memoStr = '';
								List<String> lines = new List<String>();
								if(memo.contains('\r\n')){
									memo.removeStart('\r\n');
									memo.removeEnd('\r\n');
									for(Integer i = 0; i < memo.split('\r\n').size(); i++){
										lines.add(memo.split('\r\n')[i]);
									}
								}else{
									lines.add(memo);
								}
								for(String str: lines){
									if(str.length() > 28){
										for(Integer j = 0; j < str.length() /28; j++){
											memoStr += str.subString(j * 28, j * 28 + 28) + '\r\n';
										}
									}else{
										memoStr += str + '\r\n';
									}
								}
								return memoStr;
							}
							return memo;
						} set;}
	// 代理店名
	public String agencyName {get; set;}
	// 募集人名
	public String agentName {get; set;}
	// MR名
	public String mrName {get; set;}

	/*	Constructor	*/
	public I_SalesPlanDataHolder(E_SalesPlan__c sp, Boolean createNew, Boolean isClear) {
		this.salesPlanId = sp.Id;
		if(isClear){
			this.pdfTitle = '';
			sp.SalesResult__r.IQA_LY__c = null;
			sp.SalesResult__r.IQA__c = null;
		}else{
			Date today = System.today();
			pdfTitle = String.valueOf(today.year()) + '年' + String.valueOf(today.month()) + '月' + String.valueOf(today.day()) + '日 営業計画書';
			this.spInsType_ly = sp.SalesResult__r.NBCANP_SpInsTypeLYYTD__c;
			this.spInsType_cy = sp.SalesResult__r.NBCANP_SpInsTypeYTD__c;
			this.anp_ly = sp.SalesResult__r.NBCANP_LYYTD__c;
			this.anp_cy = sp.SalesResult__r.NBCANP_YTD__c;
			this.iqa_ly = sp.SalesResult__r.IQA_LY__c;
			this.iqa_cy = sp.SalesResult__r.IQA__c;
			this.agencyName = sp.SalesResult__r.AgencyName__c;
		}
		
		//新規　目標値入力の場合
		if(createNew){
			// 稼働月、商品別計画
			this.plans_h1 = new List<prdSupportPlan>();
			this.plans_h2 = new List<prdSupportPlan>();
			//稼動月~重点商品まで
			for(String nm:SUPPORT_ITEMS){
				this.plans_h1.add(new prdSupportPlan(nm,1));
				this.plans_h2.add(new prdSupportPlan(nm,7));
			}
			
			//重点商品.右列商品以外
			this.plans_h1.add(new prdSupportPlan('',1));
			this.plans_h2.add(new prdSupportPlan('',7));

			// サポート内容 2段
			this.schedules_h1 = new List<supportSchedule>();
			this.schedules_h1.add(new supportSchedule(1));
			this.schedules_h1.add(new supportSchedule(1));
			this.schedules_h2 = new List<supportSchedule>();
			this.schedules_h2.add(new supportSchedule(7));
			this.schedules_h2.add(new supportSchedule(7));
		}
	}

	/*	JSON文字列からI_SalesPlanDataHolderを生成	*/
	public static I_SalesPlanDataHolder createByDeserialize(String jsonStr){
system.debug('★★★createByDeserialize');
		I_SalesPlanDataHolder ret = (I_SalesPlanDataHolder)JSON.deserialize(jsonStr, I_SalesPlanDataHolder.class);
		return ret;
	}
	
	public String toJSON(){
		this.saveTimeStamp = String.valueOf(system.now());
		this.saveUser = UserInfo.getName();
		return JSON.serialize(this);
	}

	/* INNER CLASS 年間サポート計画 重点商品 */
	public class prdSupportPlan {
		//年間サポート計画の　重点商品名など
		public String supportItemName{get; set;}
		public Boolean isEditableLabel{get;private set;}
		//月とCheckBoxのMap　 12個の値を持つ
		public Map<Integer, Boolean> supportActionByMonth{get; set;}
		
		//Constructor
		public prdSupportPlan(String sinm, Integer startmonth ){
			this.supportItemName = sinm;
			this.isEditableLabel = String.isBlank(sinm);		//デフォルトでラベル未設定の場合、編集可能なラベル
			this.supportActionByMonth = new Map<Integer, Boolean>();
			for(Integer i=0;i<6;i++){
				this.supportActionByMonth.put(i+startmonth, false);			//デフォルトチェック外す
			}
		}
	}
		
	/* INNER CLASS 年間サポート計画 希望サポート内容・日程 */
	public class supportSchedule {
		 public String selectedContentName{get; set;}
		//月と日付のMap　 12個の値を持つ
		public Map<Integer, String> planningDateByMonth{get; set;}
	
		public supportSchedule(Integer startmonth){
			planningDateByMonth = new Map<Integer, String>();
			for(Integer i=0;i<6;i++){
				this.planningDateByMonth.put(i+startmonth, ' ');		//デフォルト空欄
			}
		}
	}
}