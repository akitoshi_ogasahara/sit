global with sharing class E_PortfolioExtender extends E_AbstractViewExtender {
    private static final String PAGE_TITLE = 'ポートフォリオ';

    E_PortfolioController extension;
    private E_Policy__c record;

    public Boolean isZTFRDCF {get; private set;}    //「積立金の移転」表示フラグ
    
    //過去のポートフォリオ履歴の合計行
    private class historySumRow{
        private String getHeaderColumnTitle(){
            return '積立金の合計（円）';
        }
        private Decimal reserve1month {get; private set;} // 1ヶ月前
        private Decimal reserve3month {get; private set;} // 3ヶ月前
        private Decimal reserve6month {get; private set;} // 6ヶ月前
        private Decimal reserve1year {get; private set;} // 1年前
        private Decimal reserve2year {get; private set;} // 2年前
        private Decimal reserve3year {get; private set;} // 3年前
        private String getWariai(){
            return '';      //常に空欄
        }
        
        //Constructor
        public historySumRow(List<E_PortfolioController.dtPrtHsryItem> items){
            reserve1month = 0;
            reserve3month = 0;
            reserve6month = 0;
            reserve1year = 0;
            reserve2year = 0;
            reserve3year = 0;
            //契約に紐づく【ポートフォリオ履歴】.[積立金]を合計する
            for (E_PortfolioController.dtPrtHsryItem ph : items) {
                if(ph.record.ZREVAMT01__c!=null){
                    reserve1month += ph.record.ZREVAMT01__c;
                }
                if(ph.record.ZREVAMT02__c!=null){
                    reserve3month += ph.record.ZREVAMT02__c;
                }
                if(ph.record.ZREVAMT03__c!=null){
                    reserve6month += ph.record.ZREVAMT03__c;
                }
                if(ph.record.ZREVAMT04__c!=null){
                    reserve1year += ph.record.ZREVAMT04__c;
                }
                if(ph.record.ZREVAMT05__c!=null){
                    reserve2year += ph.record.ZREVAMT05__c;
                }
                if(ph.record.ZREVAMT06__c!=null){
                    reserve3year += ph.record.ZREVAMT06__c;
                }
            }
        }
        
        //合計行の<tr>　DOM文字列の生成
        public String getTRDom(){
            String html = '<tr>';
            html += '<td style="font-weight:bold;">' + getHeaderColumnTitle() + '</td>';
            html += '<td>' + reserve1month.format() + '</td>';
            html += '<td>' + reserve3month.format() + '</td>';
            html += '<td>' + reserve6month.format() + '</td>';
            html += '<td>' + reserve1year.format() + '</td>';
            html += '<td>' + reserve2year.format() + '</td>';
            html += '<td>' + reserve3year.format() + '</td>';
            html += '<td>' + getWariai() + '</td>';
            html += '</tr>';
            return html;
        }
    }
    
    //合計行の保持変数
    public historySumRow hisSumRow{get; private set;}


    //ユニットプライス推移に遷移するときに暗号化した商品コードの文字列を付与したURL
    public String urlProductCord {get; private set;} 



    // SVE上にある現在の特別勘定ユニットプライスのデータテーブル
    E_PortfolioController.dtCurrentUnitPrice dtUnitPrice;   // 変額ファンドリスト（データテーブル）

    public String isType { get; private set; }  //遷移元個別照会画面の判定に使用
    public String isOriginType { get; private set; }  //遷移元画面の判定に使用

    /*
     * Constructor
     */
    public E_PortfolioExtender(E_PortfolioController extension) {
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;

        this.isType = ApexPages.currentPage().getParameters().get('type');
        this.isOriginType = E_Const.FROM_PORTFOLIO;
    }

    /*
     * Init
     */
    global override void init() {

        this.record = extension.record;
        pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
        this.dtUnitPrice = extension.dtCurrentUnitPrice;
        if (pageRef == null) {
            // Contact情報を追加取得
            E_ContactDaoWithout.fillContactsToPolicy(extension.record, true, false,true,false, false);              
            //ユニットプライスの推移　ボタン押後のURL
            unitPriceButtonJudge();
            // 申込ボタンの表示可否
            ctrlEntryButton();
            //「現在の特別勘定ユニットプライス」作成
            setUnitPriceTable();
            // ポートフォリオ履歴.積立金合計の算出
            //portHistorySum();
            this.hisSumRow = new historySumRow(extension.dtPrtHsry.Items);
        }
    }

    /*
     * PageAction
     */
    public PageReference pageAction () {
        return E_Util.toErrorPage(pageRef, null);
    }

    /*
     * みずほ銀行であるかの判定
     */
    public boolean getIsMizuhoBank() {
        String accCode = access.getAccountCode();
        return (accCode != null && accCode.equals(E_Const.E_CL3PF_ZHEADAY_2VSL3)) ? true : false;
    }

    /*
     * ユニットプライスの推移ボタンの遷移先URl取得
     */
    private void unitPriceButtonJudge() {
        String domainURL = System.Label.E_DOMAIN_UNITP;
        String productCord = (String)extension.record.get('SPVA_ZPRDCD__c');
        urlProductCord = domainURL;
        if(productCord!=null){
            String pCord = E_EncryptUtil.prdCodeConversion(productCord,true);
            urlProductCord += pCord ;
        }
    }
    
    /*
     * 申込ボタンの表示可否
     */
    
    private void ctrlEntryButton() {
        // 申込ボタンを表示するかの判定
        isZTFRDCF = false;

		if (extension.record.SpClassification__c != null && getIsFundTransfer()) {
			System.debug('extension.record.SpClassification__c=' + extension.record.SpClassification__c);
			//SP種別が"SPVA"かつCOMM_ZTFRDCF__c="true"
			if (extension.record.SpClassification__c.equals(E_Const.SP_CLASS_SPVA)) {
				isZTFRDCF = extension.record.COMM_ZTFRDCF__c;
			//SP種別が"SPVWL"かつCOMM_ZTFRDCF__c="true"
			} else if (extension.record.SpClassification__c.equals(E_Const.SP_CLASS_SPVWL)) {
				isZTFRDCF = extension.record.COMM_ZTFRDCF__c;
			//SP種別が"SPVA&ANNUITY"かつCOMM_ZTFRDCF__c="true"またはSPVA_ZTFRDCFA__c="true"
			} else if (extension.record.SpClassification__c.equals(E_Const.SP_CLASS_SPVAANNUITY)) {
				isZTFRDCF = (extension.record.COMM_ZTFRDCF__c || extension.record.SPVA_ZTFRDCFA__c);
			//SP種別が"ANNUITY"かつSPVA_ZTFRDCFA__c="true"
			} else if (extension.record.SpClassification__c.equals(E_Const.SP_CLASS_ANNUITY)) {
				isZTFRDCF = extension.record.SPVA_ZTFRDCFA__c;
			}
        }
    }

    /*
     * 現在の特別勘定ユニットプライス
     */
    private void setUnitPriceTable() {        
        // インデックスを1000で割って小数点3桁以下を切り捨て、2桁まで表示するための処理に使用
        Decimal index02 = 0;

        // 現在の特別勘定ユニットプライスのデータテーブルのデータの値を消す
        this.dtUnitPrice.clear();
        // ファンド群マスタ、選択可能ファンド情報取得
        E_FundMaster__c fundMaster = new E_FundMaster__c();
        if (this.record.E_FundMaster__c != null) {
            fundMaster = E_FundMasterDao.getRecByIdChildSFGPF(this.record.E_FundMaster__c);
        }

        // 選択可能ファンドに紐付く、特別勘定のIDセット生成
        /*
        Set<Id> svcpfIds = new Set<Id>();
        for (E_SFGPF__c es : fundMaster.E_SFGPFs__r) {
            svcpfIds.add(es.E_SVCPF__c);
        }
        */

        // 特別勘定情報取得
        // Dao側で、ファンド表示フラグがtrue かつ　設定日<=システム日付　の時のデータのみ取得できるようにしている
        //List<E_SVCPF__c> svcpfList = E_SVCPFDao.getRecsByPolicyId(svcpfIds);
        // 変額保険ファンド情報取得(保険契約ヘッダ.ファンド群マスタをキーに取得)
        List<E_SVFPF__c> svfpfList = new List<E_SVFPF__c>();
        if (this.record.E_FundMaster__c != null) {
            svfpfList = E_SVFPFDao.getRecsByPolicyId(this.record.Id);
        }

        // 特別勘定をキーとした変額保険ファンドマップ生成
        Map<Id, E_SVFPF__c> svfpfMap = new Map<Id, E_SVFPF__c>();
        for (E_SVFPF__c svfpf : svfpfList) {
            svfpfMap.put(svfpf.E_SVCPF__c, svfpf);
        }
 
        // 特別勘定のデータ件数分、データテーブルに追加
        //for (E_SVCPF__c svcpf : svcpfList) {
        for (E_SFGPF__c sfgpf : fundMaster.E_SFGPFs__r) {
            // 保険種類(上2ケタ)="SU"の場合は、変額保険ファンドの特別勘定である必要がある。
            if (this.record.COMM_CRTABLE2__c!= null) {
                // E_Const.PORTFOLIO_FUND_POLICYKIND = 'SU'
                if (this.record.COMM_CRTABLE2__c.left(2).equals(E_Const.PORTFOLIO_FUND_POLICYKIND)) {
                    // 変額保険ファンド(svfpfMap)に紐付く特別勘定の存在を判定する
                    if (!svfpfMap.containsKey(sfgpf.E_SVCPF__c)) {
                    //if (!svfpfMap.containsKey(svcpf.Id)) {
                    // もし紐づいていなければ処理をスキップする
                        continue;
                    }
                }
            }

            if(sfgpf.E_SVCPF__r.ZVINDEX__c!=null){
                index02 = Math.floor((sfgpf.E_SVCPF__r.ZVINDEX__c/1000)*100)/100;
            }

            //現在の特別勘定ユニットプライスの[特別勘定]と[ユニットプライス]へ値を入れる             
            this.dtUnitPrice.add(new E_SVFPF__c(

                // 【特別勘定】[特別勘定名]
                ZEFUNDCD__c = sfgpf.E_SVCPF__r.ZFUNDNAMEUNION__c,
                // 【特別勘定】[インデックス（数式）]
                unitPriceDecimal__c = index02
            ));
        }
    }
    
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		return new PageReference(E_CookieHandler.getCookieRefererPortfolio());
	}
}