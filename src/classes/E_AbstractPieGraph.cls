public abstract class E_AbstractPieGraph implements E_IGraph {

    //証券番号
    public String policyId {get;set;}
    //処理区分
    public String executeKind {get;set;}
    //更新データ
    public String updateData {get;set;}
    //処理区分(入力：false,確認:true)
    public String executeScreen {get;set;}
    //レコードタイプ名
    public String recordTypeName {get;set;}

    //グラフ表示データ
    public List<GraphData> graphRecs;
    //グラフデータテーブル（更新）
    public List<GraphData> upGraphRecs;
    //グラフデータテーブル
    public List<GraphDataTable> graphTableRecs;
    //申込グラフの色
    public String colors;
    //グラフの色（更新）
    public String upColors;

    //更新データMap
    protected Map<Id, Decimal> updateDataMap = new Map<Id, Decimal>();
    //グラフの色List
    protected List<String> colorList = new List<String>();
    //グラフの色List（更新）
    protected List<String> upColorList = new List<String>();

    public E_AbstractPieGraph () {
		init();
    }

    //初期処理
    public virtual void init () {
    }

    //グラフ生成
    abstract void createGraph();

    //グラフ表示データリスト
    public List<GraphData> getDataList() {
        if  (graphRecs == null) {
            createGraph();
        }
System.debug('*********************E_AbstractPieGraph getDataList=' + graphRecs);
        return graphRecs;
    }
    //グラフ表示データリスト（更新）
    public List<GraphData> getUpDataList() {
        if  (upGraphRecs == null) {
            createGraph();
        }
System.debug('*********************E_AbstractPieGraph getUpDataList=' + upGraphRecs);
        return upGraphRecs;
    }
    //グラフデータテーブルリスト
    public List<GraphDataTable> getDataTableList() {
        if  (graphTableRecs == null) {
            createGraph();
        }
System.debug('*********************E_AbstractPieGraph getDataTableList=' + graphTableRecs);
        return graphTableRecs;
    }
    //グラフ色
    public String getColorSet() {
        if (colors == null) {
            createGraph();
        }
System.debug('*********************E_AbstractPieGraph getColorSet=' + colors);
        return colors;
    }
    //グラフ色（更新）
    public String getUpColorSet() {
        if (upColors == null) {
            createGraph();
        }
System.debug('*********************E_AbstractPieGraph getUpColorSet=' + upColors);
        return upColors;
    }

    //申込 or 参照画面フラグ
    public Boolean getIsEntry() {
    	return executeScreen.equals(E_Const.GRAPH_POLICY_ENTRY);
    }

    //確認画面フラグ
    public Boolean getIsConfirm() {
    	return executeScreen.equals(E_Const.GRAPH_POLICY_CONFIRM);
    }

    //完了画面フラグ
    public Boolean getIsComplete() {
    	return executeScreen.equals(E_Const.GRAPH_POLICY_COMPLETE);
    }

    //グラフのレコードの順番を逆にする
    protected List<GraphData> sortDescGraphRecord(List<GraphData> tempGraphRecordList){
        List<GraphData> tempList = new List<GraphData>();
        if(tempGraphRecordList.size() != 0){
            for(Integer i = tempGraphRecordList.size()-1 ; i >= 0 ; i--){
                tempList.add(tempGraphRecordList.get(i));
            }
            return tempList;
        }
        return tempGraphRecordList;
    }

    //グラフの色の順番を逆にする
    protected String sortDescColor(List<String> tempColorList){
        List<String> tempList = new List<String>();
        for(Integer i = tempColorList.size()-1 ; i >= 0 ; i--){
            tempList.add(tempColorList.get(i));
        }
        return String.join(tempList,',');
    }



    /**
     * グラフ表示クラス
     */
    public class GraphData {
        public String name {get;set;}
        public Decimal data {get;set;}
        public GraphData (String name, Decimal data) {
            this.name = name;
            this.data = data;
        }
    }



    /**
     * グラフデータ表示クラス
     */
    public class GraphDataTable {
        public String color {get;set;}
        public String name {get;set;}
        public String amount {get;set;}
        public Decimal ratio {get;set;}
        //積立金移転、ポートフォリオ
        public GraphDataTable (String color, String name, Decimal amount, Decimal ratio) {
            this.color = color;
            this.name = name;
            this.amount = amount.format();
            this.ratio = ratio;
        }
        //繰入比率
        public GraphDataTable (String color, String name, Decimal ratio) {
            this.color = color;
            this.name = name;
            this.ratio = ratio;
        }
    }
}