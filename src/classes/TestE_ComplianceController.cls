@isTest
private class TestE_ComplianceController {

	//ページアクション
	static testMethod void pageActionTest01() {

		E_ComplianceController controller = new E_ComplianceController();
		//TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		Test.startTest();
		PageReference result = controller.pageAction();
		Test.stopTest();

		//System.assertEquals(null, result);
		System.assert(result.getUrl().contains(page.E_errorPage.getURL()));
		//System.assertEquals(result.getUrl(),page.E_errorPage.getURL());

	}


	//メニューキーを返却
	//代理店ユーザの場合
	static testMethod void getMenuKeyTest01() {

		//メニューマスタ作成
		E_MenuMaster__c compliance_agency = TestE_TestUtil.createMenuMaster(false, 'コンプライアンス（代理店）', 'compliance_agency','E_Menu', false);
		E_MenuMaster__c compliance_agency_gw = TestE_TestUtil.createMenuMaster(false, 'コンプライアンス（代理店）GW', 'compliance_agency_GW','E_Menu', false);
		E_MenuMaster__c compliance_contractant = TestE_TestUtil.createMenuMaster(false, 'コンプライアンス（契約者）', 'compliance_contractant','E_Menu', false);
		compliance_agency.UseInternet__c = true;
		compliance_agency.UseCommonGW__c = false;
		compliance_agency_gw.UseInternet__c = false;
		compliance_agency_gw.UseCommonGW__c = true;
		compliance_contractant.UseInternet__c = true;
		compliance_contractant.UseCommonGW__c = true;
		insert compliance_agency;
		insert compliance_agency_gw;
		insert compliance_contractant;

		//ログインユーザ作成
		UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
		User ownerUser = TestE_TestUtil.createUser(false, 'shoyusya', E_Const.SYSTEM_ADMINISTRATOR);
		ownerUser.userroleid = uRole.Id;
		user thisuser = [select id from user where id =:system.UserInfo.getUserId()];
		system.runas(thisuser){
			insert ownerUser;
		}
		User user01;
		System.runAs(ownerUser){
			Account account01 = TestE_TestUtil.createAccount(true);
			Contact contact01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName', 'cno', 'ano');
			user01 = TestE_TestUtil.createUser(false, 'keiyakusha', 'E_PartnerCommunity');
			user01.ContactId = contact01.Id;
			insert user01;
			TestE_TestUtil.createIDCPF(true,user01.Id);
		}

		//実行
		System.runAs(user01){

			E_ComplianceController controller = new E_ComplianceController();
			Test.startTest();
			controller.isValidate();
			PageReference result = controller.pageAction();
			Test.stopTest();

			System.assertEquals(null, result);
			system.assertEquals('compliance_agency',controller.getMenuKey());
			System.assertEquals('compliance_agency', controller.menu.MenuMasterKey__c);
		}
	}

	//メニューキーを返却
	//契約者ユーザの場合
	static testMethod void getMenuKeyTest02() {

		//メニューマスタ作成
		E_MenuMaster__c compliance_agency = TestE_TestUtil.createMenuMaster(false, 'コンプライアンス（代理店）', 'compliance_agency','E_Menu', false);
		E_MenuMaster__c compliance_contractant = TestE_TestUtil.createMenuMaster(false, 'コンプライアンス（契約者）', 'compliance_contractant','E_Menu', false);
		compliance_agency.UseInternet__c = true;
		compliance_agency.UseCommonGW__c = true;
		compliance_contractant.UseInternet__c = true;
		compliance_contractant.UseCommonGW__c = true;
		insert compliance_agency;
		insert compliance_contractant;
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());
		
		//実行
		E_ComplianceController controller = new E_ComplianceController();
		Test.startTest();
		controller.isValidate();
		PageReference result = controller.pageAction();
		Test.stopTest();

		System.assertEquals(null, result);
		system.assertEquals('compliance_contractant', controller.menu.MenuMasterKey__c);
	}

	//確認ボタン
	static testMethod void doConfirmTest01() {

		E_ComplianceController controller = new E_ComplianceController();

		Test.startTest();
		PageReference result = controller.doConfirm();
		Test.stopTest();

		System.assertEquals(Page.E_Home.getUrl(), result.getUrl());
		System.assert(String.isNotBlank(E_CookieHandler.getCookieCompliance()));
	}

	////インターネット利用規約を取得
	//static testMethod void getInternetPolicyTest() {

	//	//メニューマスタ作成
	//	E_MenuMaster__c internet_policy = TestE_TestUtil.createMenuMaster(false, 'インターネットサービス利用規定', 'internet_service_policy_agency','E_Menu', false);

	//	insert internet_policy;

	//	//ログインユーザ作成
	//	UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
	//	User ownerUser = TestE_TestUtil.createUser(false, 'shoyusya', E_Const.SYSTEM_ADMINISTRATOR);
	//	ownerUser.userroleid = uRole.Id;
	//	user thisuser = [select id from user where id =:system.UserInfo.getUserId()];
	//	system.runas(thisuser){
	//		insert ownerUser;
	//	}
	//	User user01;
	//	System.runAs(ownerUser){
	//		Account account01 = TestE_TestUtil.createAccount(true);
	//		Contact contact01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName', 'cno', 'ano');
	//		user01 = TestE_TestUtil.createUser(false, 'keiyakusha', 'E_PartnerCommunity');
	//		user01.ContactId = contact01.Id;
	//		insert user01;
	//	}

	//	//実行
	//	System.runAs(user01){

	//		E_ComplianceController controller = new E_ComplianceController();
	//		Test.startTest();
	//		List<E_MessageMaster__c> result = controller.getInternetPolicyContents();
	//		Test.stopTest();

	//		System.assertEquals(null, result);
	//		system.assertEquals('internet_service_policy_agency',controller.getMenuKey());
	//		System.assertEquals('internet_service_policy_agency', controller.menu.MenuMasterKey__c);
	//	}
	//}

	////メッセージマスタ作成
	//static void createMessageData(String key,String value){
	//	User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
	//	system.runAs(thisUser){
	//		E_MessageMaster__c msg = new E_MessageMaster__c();
	//		msg.Name = 'テストメッセージ';
	//		msg.Key__c = key;
	//		msg.Value__c = value;
	//		msg.Type__c = 'メッセージ';
	//		insert msg;
	//	}
	//}
}