/*
 * CC_ContactVOIController
 * Controller class for Contact
 * created  : Accenture 2018/6/18
 * modified :
 */

global with sharing class CC_ContactVOIController implements Vlocity_ins.VlocityOpenInterface2{
	global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);

		Boolean success = true;
		try{
			//募集人画面更新する場合
			if(methodName == 'saveAgent'){
				success = CC_ContactVOIBizLogic.saveAgent(inputs, output, options);
			}

			//個人顧客画面更新する場合
			else if(methodName == 'saveClient'){
				success = CC_ContactVOIBizLogic.saveClient(inputs, output, options);
			}
		}catch(Exception e){
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}
}