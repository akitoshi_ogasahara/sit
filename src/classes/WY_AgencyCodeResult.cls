public with sharing class WY_AgencyCodeResult {
    public String agencyCode {get; set;}
    public List<WY_SearchResult> resultList {get; set;}

    public WY_AgencyCodeResult(String agCode, List<WY_SearchResult> results){
        agencyCode = agCode;
        resultList = results;
    }
}