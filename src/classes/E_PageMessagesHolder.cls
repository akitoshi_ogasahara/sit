/**
 * 		E_PageMessagesHolder
 *			PageMessage管理クラス　E_AbstractXXXXから呼出し
 * 				CreatedDate 2015/02/07
 * 				@Author Terrasky
 */
public with sharing class E_PageMessagesHolder {
	
	/*	nlinkPageMessage
	 		ApexPages.MessageはSerilizeできないので独自クラスを定義
	 */
	public class nlinkPageMessage{
		public ApexPages.Severity severity{get;set;}
		public String summary{get;set;}
		public String componentLabel{get;set;}
		public String detail{get;set;}
		
		public nlinkPageMessage(ApexPages.Severity sv, String smry){
			this.severity = sv;
			this.summary = smry;
		}
	
		public nlinkPageMessage(ApexPages.Severity sv, String smry, String dtl){
			this.severity = sv;
			this.summary = smry;
			this.detail = dtl;
		}

		public nlinkPageMessage(ApexPages.Message msg){
			this.severity = msg.getSeverity();
			this.summary = msg.getSummary();
			this.detail = msg.getDetail();
			this.componentLabel = msg.getComponentLabel();
		}
		
		public String getSeverityString(){
			return String.ValueOf(severity);
		}
	}
	
	private static E_PageMessagesHolder pgmsgHolder = null;
	
	/*	Map<nlinkPageMessage>	*/
	private Map<ApexPages.Severity, List<nlinkPageMessage>> messages;

	/**
	 *		Constructor
	 */
	private E_PageMessagesHolder(){
		messages = new Map<ApexPages.Severity, List<nlinkPageMessage>>();
	}
	
	public static E_PageMessagesHolder getInstance(){
		if (pgmsgHolder == null) {
			pgmsgHolder = new E_PageMessagesHolder();
		}
		return pgmsgHolder;
	}

	/**
	 *		hasMessages() 
	 */
	public Boolean hasMessages(){
		return !messages.isEmpty();
	}
	public Boolean getHasMessages(){
		return !messages.isEmpty();
	}
	
	/**
	 *		addMessage
	 */
	public void addMessage(ApexPages.Severity severity, String summary){
		if(messages.containsKey(severity)==false){
			messages.put(severity, new List<nlinkPageMessage>());
		}
		messages.get(severity).add(new nlinkPageMessage(severity, summary));
	}
	
	public void addMessage(ApexPages.Severity severity, String summary, String detail){
		if(messages.containsKey(severity)==false){
			messages.put(severity, new List<nlinkPageMessage>());
		}
		messages.get(severity).add(new nlinkPageMessage(severity, summary, detail));
	}
	
	/**
	 *		addMessages
	 *			ApexPages.Message[]からメッセージリストを追加
	 */
	public void addMessages(ApexPages.Message[] pgMsgs){
		for(ApexPages.Message msg:pgMsgs){
			ApexPages.Severity severity = msg.getSeverity();
			if(messages.containsKey(severity)==false){
				messages.put(severity, new List<nlinkPageMessage>());
			}
			messages.get(severity).add(new nlinkPageMessage(msg));
		}
	}

	/**		getMessages	 */
	public List<nlinkPageMessage> getMessages(){
		List<nlinkPageMessage> ret = new List<nlinkPageMessage>();
		for(ApexPages.Severity sv:messages.keyset()){
			ret.addAll(messages.get(sv));
		}
		
		return ret;
	}

	/**
	 *		clearMessage
	 */
	public void clearMessages(){
		messages.clear();
	}

	/**	=============================================================
							ERROR MESSAGE
		=============================================================	*/
	/**		addErrorMessage(サマリ)	 */
	public void addErrorMessage(String summary){
		addMessage(ApexPages.Severity.ERROR, summary);
	}
	/**		addErrorMessage(サマリ、Detail)	 */
	public void addErrorMessage(String summary,String detail){
		addMessage(ApexPages.Severity.ERROR, summary, detail);
	}
	/**		getErrorMessages	 */
	public List<nlinkPageMessage> getErrorMessages(){
		return messages.get(ApexPages.Severity.ERROR);
	}
	/**		hasErrorMessages	 */
	public Boolean getHasErrorMessages(){
		
		return messages.get(ApexPages.Severity.ERROR) != null
				&& messages.get(ApexPages.Severity.ERROR).isEmpty() == false;
	}

	/**	=============================================================
							WARNING MESSAGE
		=============================================================	*/
	/**		addWarningMessage(サマリ)	 */
	public void addWarningMessage(String summary){
		addMessage(ApexPages.Severity.Warning, summary);
	}
	/**		addWarningMessage(サマリ、Detail)	 */
	public void addWarningMessage(String summary,String detail){
		addMessage(ApexPages.Severity.Warning, summary, detail);
	}
	/**		getWarningMessages	 */
	public List<nlinkPageMessage> getWarningMessages(){
		return messages.get(ApexPages.Severity.Warning);
	}
	/**		hasWarningMessages	 */
	public Boolean getHasWarningMessages(){
		
		return messages.get(ApexPages.Severity.Warning) != null
				&& messages.get(ApexPages.Severity.Warning).isEmpty() == false;
	}

	/**	=============================================================
							INFO MESSAGE
		=============================================================	*/
	/**		addINFOMessage(サマリ)	 */
	public void addINFOMessage(String summary){
		addMessage(ApexPages.Severity.INFO, summary);
	}
	/**		addINFOMessage(サマリ、Detail)	 */
	public void addINFOMessage(String summary,String detail){
		addMessage(ApexPages.Severity.INFO, summary, detail);
	}
	/**		getINFOMessages	 */
	public List<nlinkPageMessage> getINFOMessages(){
		return messages.get(ApexPages.Severity.INFO);
	}
	/**		hasINFOMessages	 */
	public Boolean getHasINFOMessages(){
		
		return messages.get(ApexPages.Severity.INFO) != null
				&& messages.get(ApexPages.Severity.INFO).isEmpty() == false;
	}
}