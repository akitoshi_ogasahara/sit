@isTest
public class TestMNT_PamhletSearchCSVController{

	@isTest
	public static void CSVcontrollerTest(){

		String mainSObjectAPIName;

        Test.startTest();
            Test.setCurrentPage(Page.MNT_PamphletCSV);
            MNT_PamhletSearchCSVController controller = new MNT_PamhletSearchCSVController();

            mainSObjectAPIName = controller.mainSObjectAPIName();
        Test.stopTest();

        System.assertEquals(mainSObjectAPIName, Schema.SObjectType.I_ContentMaster__c.Name);
	}
}