public with sharing class E_SWDPFDao {
    /**
     * 保険契約IDをキーに、指定した定期引出情報を取得する。
     * 複数件存在する場合は、１件のみ取得する。
     * @param policyId: 保険契約ID
     * @return E_SWDPF__c: 定期引出情報
     */
    public static E_SWDPF__c getRecByType (Id policyId) {
        E_SWDPF__c rec = null;
        List<E_SWDPF__c> recs = [
            Select 
                Id
                ,CreatedDate
                ,CreatedById
                ,LastModifiedDate
                ,LastModifiedById
                ,Name
                ,Policy__c
                ,CHDRNUM__c
                ,ZWDRNEXT__c
                ,ZWDRBAMT__c
                ,ZWTHDRRN__c
                ,ZWTHDRFN__c
                ,ZWDRAMT__c
                ,USRPRF__c
                ,JOBNM__c
                ,DATIME__c
                ,dataSyncDate__c
            From 
                E_SWDPF__c
            Where 
                Policy__r.id =: policyId
        ];
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
    }
}