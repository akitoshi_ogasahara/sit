global with sharing class CR_RulesController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CR_Agency__c record {get{return (CR_Agency__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public CR_RulesExtender getExtender() {return (CR_RulesExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public CR_RulesController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','upload_rule');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','代理店検索');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!Not(isBlank(Extender.UrlforSearch)) && Extender.isSuccessInit && Extender.isEmployee}');
        tmpPropMap.put('p_value','{!Extender.UrlforSearch}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2028');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2028',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','外部委託先一覧');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!Not(isBlank(Extender.UrlforOutsrcs)) && Extender.isSuccessInit}');
        tmpPropMap.put('p_value','{!Extender.UrlforOutsrcs}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2027');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2027',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','False');
        tmpPropMap.put('p_rendered','true');
        tmpPropMap.put('p_value','{!Extender.UrlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2026');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2026',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2029');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2029',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_sbmtFiles','{!extender.Rules}');
		tmpPropMap.put('Component__Width','400');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','Component2023');
		tmpPropMap.put('Component__Name','CR_RulesList');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2023',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_agencyRec','{!record}');
        tmpPropMap.put('p_canComment','{!Extender.isEmployee}');
		tmpPropMap.put('Component__Width','700');
		tmpPropMap.put('Component__Height','400');
		tmpPropMap.put('Component__id','Component2018');
		tmpPropMap.put('Component__Name','CRComment');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2018',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = CR_Agency__c.fields.AgencyCode__c;
		f = CR_Agency__c.fields.AgencyName__c;
		f = CR_Agency__c.fields.ZSHRMAIN__c;
		f = CR_Agency__c.fields.MRUnit__c;
		f = CR_Agency__c.fields.MRCode__c;
		f = CR_Agency__c.fields.MRName__c;
		f = CR_Agency__c.fields.BusinessStatus__c;
		f = CR_Agency__c.fields.Status__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CR_Agency__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('CR_Agency__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('AgencyCode__c');
			mainQuery.addFieldAsOutput('AgencyName__c');
			mainQuery.addFieldAsOutput('ZSHRMAIN__c');
			mainQuery.addFieldAsOutput('MRUnit__c');
			mainQuery.addFieldAsOutput('MRCode__c');
			mainQuery.addFieldAsOutput('MRName__c');
			mainQuery.addFieldAsOutput('BusinessStatus__c');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('Mail2DPA__c');
			mainQuery.addFieldAsOutput('Mail2MR__c');
			mainQuery.addFieldAsOutput('MR__c');
			mainQuery.addFieldAsOutput('SystemModstamp');
			mainQuery.addFieldAsOutput('Comment__c');
			mainQuery.addFieldAsOutput('ApprovalDate__c');
			mainQuery.addFieldAsOutput('addComment__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new CR_RulesExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}