global with sharing class E_InveCompareController extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public E_ITHPF__c record {get{return (E_ITHPF__c)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_InveCompareExtender getExtender() {return (E_InveCompareExtender)extender;}
    
    
    public E_InveCompareController(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component1326');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1326',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','XXXX');
        tmpPropMap.put('isHideMenu','true');
        tmpPropMap.put('Component__Width','119');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component812');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component812',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('ithpfId','{!record.Id}');
        tmpPropMap.put('executeKind','{!Extender.ExecuteKindBuy}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component1170');
        tmpPropMap.put('Component__Name','EInvestmentGraph');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1170',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('ithpfId','{!record.Id}');
        tmpPropMap.put('executeKind','{!Extender.ExecuteKindNow}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component1171');
        tmpPropMap.put('Component__Name','EInvestmentGraph');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1171',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1330');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1330',tmpPropMap);

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = E_ITHPF__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('E_ITHPF__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('Contractor__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = false;
            p_sidebar = false;
            extender = new E_InveCompareExtender(this);
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    
    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}