public with sharing class E_ITAPFDao {
	
	/**
	 * 集計フラグが立っているアセットクラスとそれに紐付く投信ファンドを取得する
	 * @param ithpfId: 投信ヘッダID
	 * @return List<E_ITAPF__c>: アセットクラスリスト
	 */
	public static List<E_ITAPF__c> getRecWithFundByflagAndHeaderId(Id ithpfId){
		return [
			select Id
				, ZASSTCLS__c
				, ZASSTNAM__c
				, FLAG__c
				, ZCOLORCD__c
				, dataSyncDate__c
				, (select Id
					, ZBRNDPER02__c
					, ZBRNDPER04__c
					from E_ITFPFs__r
					where E_ITHPF__c = :ithpfId)
			from E_ITAPF__c
			where FLAG__c = '1'
			order by ZASSTCLS__c
		];
	}
}