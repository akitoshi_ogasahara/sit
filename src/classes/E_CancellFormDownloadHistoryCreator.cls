public with sharing class E_CancellFormDownloadHistoryCreator {
	
	public E_CancellFormDownloadHistory__c createLog(Id policyId, String DLFileName, String IDType){
		E_Policy__c policy = E_PolicyDaoWithout.getRecForDLHistoryById(policyId);
		
		E_CancellFormDownloadHistory__c ret = new E_CancellFormDownloadHistory__c(
																	E_Policy__c = policy.Id
																	,ZIDTYPE__c = IDType      										//ID種別
																	,DLPDFNM__c = DLFileName      										//ダウンロードファイル名
																	,ZJINTF__c = String.isNotBlank(policy.SPVA_AGNTNUM02__c)			//共同募集有無
																	,CRTDATETIME__c = System.Now()      								//ダウンロード年月日（時刻）
																	,ZCLNAME__c = policy.ContractorName__c     							//契約者名
																	,OCCDATE__c = policy.COMM_OCCDATE__c      							//契約日
																	,ZREFAMT__c = policy.SPVA_ZREFAMT__c      							//現時点での解約返戻金
																	,ZACTSUM__c = policy.SPVA_ZACTSUM__c      							//現時点での死亡給付金額
																	,CLNTNUM__c = policy.ContractorCLTPF_CLNTNUM__c	      				//顧客番号
																	,CHDRNUM__c = policy.COMM_CHDRNUM__c      							//証券番号

																	//代理店格
																	,ZHEADAY__c = policy.MainAgent__r.Account.E_CL1PF_ZHEADAY__c		//代理店コード
																	,KCHANNEL__c = policy.MainAgent__r.Account.E_IsAgency__c		//代理店チャネル
																						?policy.MainAgent__r.Account.E_CL1PF_KCHANNEL__c		
																						 :policy.MainAgent__r.Account.Parent.E_CL1PF_KCHANNEL__c
																	,ZAHNAME__c = policy.MainAgent__r.Account.E_IsAgency__c
																						?policy.MainAgent__r.Account.Name						//代理店名
																						 :policy.MainAgent__r.Account.Parent.Name
																	//代理店事務所
																	,ZAGCYNUM__c = policy.MainAgent__r.Account.E_CL2PF_ZAGCYNUM__c		//代理店事務所コード
																	,ZEAYNAM__c = policy.MainAgent__r.Account.Name		//代理店事務所名

																	,ZTOTPREM__c = policy.COMM_ZTOTPREM__c								//払込保険料
																	,CRTABLE__c = policy.COMM_CRTABLE__c								//保険種類コード
																	,ZCOVRNAM__c = policy.COMM_ZCOVRNAM__c								//保険種類名
																	,AGNTNUM__c = policy.MainAgent__r.E_CL3PF_AGNTNUM__c				//募集人番号
																	,ZEATNAM__c = policy.MainAgent__r.Name								//募集人名
													);
		insert ret;
		return ret;
	}
	
    public E_CancellFormDownloadHistory__c createCancellationDLLog(E_Policy__c policy, E_COVPF__c mainCovpf, String DLFileName, boolean toNotifyFlag, User DLUser, Id OwnerId){
        E_CancellFormDownloadHistory__c ret = new E_CancellFormDownloadHistory__c(
            														E_Policy__c = policy.Id
																	,CRTDATETIME__c = System.Now()      								//ダウンロード年月日（時刻）
            														,DLDateTime__c = System.Now().format('yyyy年MM月dd日 HH時mm分')     //ダウンロード日時（メール）
            														,DLPDFNM__c = DLFileName      										//ダウンロードファイル名
																	,ZCLNAME__c = policy.ContractorName__c     							//契約者名
																	,CLNTNUM__c = policy.ContractorCLTPF_CLNTNUM__c	      				//顧客番号
            														,InsuredName__c = policy.InsuredName__c                             //被保険者名
																	,CHDRNUM__c = policy.COMM_CHDRNUM__c      							//証券番号
            														,GRUPNUM__c = policy.COLI_GRUPNUM__c                                //団体番号
																	,ZCOVRNAM__c = mainCovpf.COMM_ZCOVRNAMFIX__c						//保険種類名
            														,MainAgentName__c = policy.MainAgentName__c                         //主担当募集人名
            														,MainAgentNum__c = policy.MainAgentNum5__c                          //主担当募集人コード
            														,SubAgentName__c = policy.SubAgentName__c                           //従担当募集人名
            														,SubAgentNum__c = policy.SubAgentNum5__c                            //従担当募集人コード
																	,OCCDATE__c = policy.COMM_OCCDATE__c      							//契約日
																	,ZTOTPREM__c = policy.COMM_ZTOTPREM__c								//払込保険料
																	,ZHEADAY__c = policy.MainParentAccountE_CL1PF_ZHEADAY__c      		//主募集人代理店格コード（数式）
																	,ZAHNAME__c = policy.MainAgentAccountParenName__c					//主募集人代理店格名（数式）
																	,ZAGCYNUM__c = policy.MainAgentAccountCL2PF_ZAGCYNUM__c      		//主募集人代理店事務所コード（数式）
																	,ZEAYNAM__c = policy.MainAgentAccountName__c						//主募集人代理店事務所名（数式）
            														,MainAgentAccountMR__c = OwnerId                                    //主募集人担当MR
                                                                    ,ToNotifyFlag__c = toNotifyFlag                                     //通知メール送信フラグ
																	,CRTABLE__c = mainCovpf.COMM_CRTABLE2__c							//保険種類コード E_COVPF__c mainCovpfから取得
        );
        
        //募集人ユーザの場合、DLUserが設定される。社員ユーザの場合、DLUserはnull（CreateByでダウンロード者を特定できる）
        if (DLUser != null) {
            ret.DLParentName__c = DLUser.Contact.E_AccParentName__c;                //ダウンロード代理店名
            ret.DLParentCode__c = DLUser.Contact.E_AccParentCord__c;                //ダウンロード代理店コード
            ret.DLAgentName__c  = DLUser.Contact.Name;                              //ダウンロード募集人名
            ret.DLAgentNum__c   = DLUser.Contact.AgentNum5__c;                      //ダウンロード募集人コード
        }
            
		insert ret;
		return ret;
	}
}