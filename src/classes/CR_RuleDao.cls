public with sharing class CR_RuleDao {

	public static List<CR_Rule__c> getRulesWithAttachmentsById(Id recId){
		
		return [SELECT Id,Name,Type__c,CR_Agency__c,LastModifiedDate
				,(select id,name,lastmodifiedDate from Attachments)
				FROM CR_Rule__c
				WHERE CR_Agency__c = :recId
				ORDER By LastModifiedDate DESC];
	}



/*
	//pageMessageHolderのclearMessageは呼出元で呼び出してください
	
	public static Boolean saveViaPage(CR_Rule__c rec){
		try{
			
			List<CR_Rule__c> recs = new List<CR_Rule__c>{rec};
			Database.UpsertResult[] results = Database.upsert(recs,false);
			System.assertEquals(results.size(),1);	//1件のみのDML結果
			
			if(results[0].isSuccess()){
				return true;
			}else{
				for(Database.Error err:results[0].getErrors()){
					E_PageMessagesHolder.getInstance().addErrorMessage(err.getMessage()+String.join(err.getFields(),'|'));
				}
			}
		}catch(Exception e){
			E_PageMessagesHolder.getInstance().addErrorMessage('保存時エラー：'+e.getMessage());
		}
		return false;
	}
*/


}