/**
 *		ダウンロードログと添付ファイルの削除テストメソッド
 *			テストデータの作成日変更のためAPIVer.36.0以上で利用すること
 */
@isTest
private class TestCR_AttachmentsDelete {

    static testMethod void test_asyncDelete() {
		//削除対象作成日の取得 カスタム表示ラベルの日数分前の日付
		Datetime dt = Datetime.now();
		Integer offset = UserInfo.getTimeZone().getoffset(dt);
		Datetime local = dt.addSeconds(offset/1000);
		Datetime xDay = local.addDays(CR_Const.CR_ATTACHMENTS_EXPIRE_DAYS * -1);
		
		E_DownloadHistorry__c dl1 = new E_DownloadHistorry__c(formNo__c = '1'
																,Conditions__c = '対象外の帳票種類');
		E_DownloadHistorry__c dl2 = new E_DownloadHistorry__c(formNo__c = CR_Const.TYPE_BIZREPORTS
															, Status__c = CR_Const.DL_STATUS_SUCCESS
															, Conditions__c = '削除対象');
		E_DownloadHistorry__c dl3 = new E_DownloadHistorry__c(formNo__c = CR_Const.TYPE_BIZREPORTS
															, Status__c = CR_Const.DL_STATUS_SUCCESS
															, Conditions__c = '削除対象外の作成日');
		E_DownloadHistorry__c dl4 = new E_DownloadHistorry__c(formNo__c = CR_Const.TYPE_BIZBOOKS
															, Status__c = CR_Const.DL_STATUS_ERROR
															, Conditions__c = '対象外のステータス　失敗');

		List<E_DownloadHistorry__c> dlRecs = new List<E_DownloadHistorry__c>();
		dlRecs.add(dl1);
		dlRecs.add(dl2);
		dlRecs.add(dl3);
		dlRecs.add(dl4);
		insert dlRecs;
		
		System.Test.setCreatedDate(dl1.Id, xDay.addDays(-1));
		System.Test.setCreatedDate(dl2.Id, xDay.addDays(-1));
		System.Test.setCreatedDate(dl3.Id, xDay);				//削除されない
		System.Test.setCreatedDate(dl4.Id, xDay.addDays(-1));
		
		List<Attachment> files = new List<Attachment>();
		for(E_DownloadHistorry__c dl:dlRecs){
			files.add(new Attachment(	ParentId = dl.Id
										, Name = dl.Conditions__c
										, body = Blob.valueOf(dl.Id)));
		}
		insert files;

		
		Test.startTest();
			E_BizDataSyncLog__c log = new E_BizDataSyncLog__c(kind__c = CR_Const.EBIZ_LOGKBN_ATTACHMENTS_DELETE);
			insert log;
		Test.stopTest();

		dlRecs = [select id,status__c,createdDate,conditions__c 
						,(select id from attachments)
				  from E_DownloadHistorry__c
				  where id in :dlRecs];
		
		System.assertEquals(dlRecs.size(),4);
		for(E_DownloadHistorry__c dl:dlRecs){
			system.debug('作成日'+dl.createdDate);
			if(dl.Id == dl1.Id){
				System.assertEquals(dl.Status__c, dl1.Status__c);
				System.assertEquals(dl.attachments.size(), 1);
			}	
			if(dl.Id == dl2.Id){		//dl2のみ更新され、ファイルが削除されている
				System.assertEquals(dl.Status__c, CR_Const.DL_STATUS_DELETED);
				System.assertEquals(dl.attachments.size(), 0);
			}
			if(dl.Id == dl3.Id){
				System.assertEquals(dl.Status__c, dl3.Status__c);
				System.assertEquals(dl.attachments.size(), 1);
			}	
			if(dl.Id == dl4.Id){		// ステータスは問わず日数経過分を削除
				System.assertEquals(dl.Status__c, CR_Const.DL_STATUS_DELETED);
				System.assertEquals(dl.attachments.size(), 0);
			}	
		}

    }
}