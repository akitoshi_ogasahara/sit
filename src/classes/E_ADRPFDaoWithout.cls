/**
 * 顧客情報変更受付履歴レコードの作成用(without版)
 * CreateDate 2016/10/7
 * @author Terrasky
 */
public without sharing class E_ADRPFDaoWithout {

    /**
    * 顧客情報変更受付履歴レコードを作成する
    * @param userContact :ユーザ情報
    *        eidc :ID管理のID
    * @return 顧客情報変更受付履歴レコード
    */

    public static E_ADRPF__c insertRec(User userContact,ID eidc){

        String ReceptionDatetime = System.now().format('yyyy年MM月dd日 HH時mm分');
        E_ADRPF__c insE_ADRPFItem = new E_ADRPF__c(
        Type__c = E_Const.MAIL_TYPE_EM
            ,Kbn__c = E_Const.KBN_MAIL
            ,ReceptionDatetime__c = ReceptionDatetime
            ,E_IDCPF__c = eidc
            ,ContactNameKana__c = userContact.contact.E_CLTPF_ZCLKNAME__c
            ,ContactName__c = userContact.contact.name
            ,EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c
            ,CLNTNUM__c = userContact.contact.E_CLTPF_CLNTNUM__c
        );
        insert insE_ADRPFItem;
        return insE_ADRPFItem;
    }

    

    /**
    * 顧客情報変更受付履歴レコードを作成する
    * @param userId :ユーザID
    *        eidcId :ID管理のID
    */
    @future
    public static void insertRec(ID userId,ID eidcId){
        User userContact = E_UserDaoWithout.getUserRecByUserId(userId);
        
        E_IDCPF__c eidc = E_IDCPFDaoWithout.getRecsByZWEBID(eidcId);

        String ReceptionDatetime = System.now().format('yyyy年MM月dd日 HH時mm分');
        E_ADRPF__c insE_ADRPFItem = new E_ADRPF__c(
        Type__c = E_Const.MAIL_TYPE_EM
            ,Kbn__c = E_Const.KBN_MAIL
            ,ReceptionDatetime__c = ReceptionDatetime
            ,E_IDCPF__c = eidc.Id
            ,ContactNameKana__c = userContact.contact.E_CLTPF_ZCLKNAME__c
            ,ContactName__c = userContact.contact.name
            ,EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c
            ,CLNTNUM__c = userContact.contact.E_CLTPF_CLNTNUM__c
        );
        insert insE_ADRPFItem;
    }

}