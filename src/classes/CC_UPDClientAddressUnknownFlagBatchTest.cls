/*
 * CC_UPDClientAddressUnknownFlagBatchTest
 * Test class of CC_UPDClientAddressUnknownFlagBatch, CC_UPDClientAddressUnknownFlagBatchSched
 * created  : Accenture 2018/10/15
 * modified :
 */

@isTest
private class CC_UPDClientAddressUnknownFlagBatchTest{

    private static User testSysAdminUser = new User(Id = UserInfo.getUserId());

    /**
     * Prepare test data
     */
    @testSetup static void prepareTestData(){
        System.runAs ( testSysAdminUser ) {
            //Get Date
            Date today = Date.today();
            //Create Account
            RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
            Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
            account.CMN_isAddressUnknown__c = true;
            Insert account;
            //Create Contact
            Contact contact = CC_TestDataFactory.getContactSkel();
            Account testAccount = [SELECT Id FROM Account WHERE Name = 'testAccount' Limit 1];
            contact.CMN_isAddressUnknown__c = true;
            contact.E_CLTPF_CLTTYPE__c = 'C';
            contact.E_COMM_ZCLADDR__c = '東京都港区六本木１－１－１';
            contact.E_dataSyncDate__c = today;
            contact.E_CLTPF_CLTPCODE__c = '1020094';
            contact.AccountId = testAccount.Id;
            Insert contact;
        }
    }

    /**`
     * Batch Execute
     */
    static testMethod void CC_UPDClientAddressUnknownFlagBatchTestExecute() {
        System.runAs ( testSysAdminUser ) {
            Test.startTest();
            String jobId = System.schedule('UPDClientAddressUnknownFlagBatchTest', '0 0 * * * ?', new CC_UPDClientAddressUnknownFlagBatchSched());
            Test.stopTest();
        }
    }

}