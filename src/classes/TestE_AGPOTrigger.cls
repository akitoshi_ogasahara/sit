/**
 * E_AGPOTrigger
 */
@isTest
private class TestE_AGPOTrigger {

	// 保険契約ヘッダ
	private static E_Policy__c policy;
	// 販売取扱者
	private static List<E_CHTPF__c> chtpfs;
	// 募集人番号
	private static List<String> agcodes = new List<String>{'11111', '22222'};

	/**
	 * void onBeforeInsert (List<E_AGPO__c> newList)
	 */
    static testMethod void onBeforeInsert_test01() {
        createData();
        
        Test.startTest();
        
        // 保有契約異動通知
        E_AGPO__c rec = new E_AGPO__c(E_Policy__c = policy.Id, AGNT__c = agcodes[0],ZJINTSEQ__c = '1');
        insert rec;
        
        Test.stopTest();

		// assert
		rec = [Select Id, E_CHTPF__c From E_AGPO__c Where Id = :rec.Id];
		System.assertEquals(rec.E_CHTPF__c, getChtpf(1).Id);
    }
    
	/**
	 * void onBeforeUpdate (List<E_AGPO__c> newList,Map<Id,E_AGPO__c> oldMap)
	 */
    static testMethod void onBeforeUpdate_test01() {
        createData();
        
        Test.startTest();
        
        // 保有契約異動通知
        E_AGPO__c rec = new E_AGPO__c(E_Policy__c = policy.Id, AGNT__c = agcodes[0],ZJINTSEQ__c = '1');
        insert rec;
        
        rec.AGNT__c = agcodes[1];
        update rec;
        
        Test.stopTest();

		// assert
		rec = [Select Id, E_CHTPF__c From E_AGPO__c Where Id = :rec.Id];
		System.assertEquals(rec.E_CHTPF__c, getChtpf(1).Id);
    }
    
    /** *********************************************************************************************************
     * TestData
     */
    private static void createData(){
    	// 保険契約ヘッダ
    	policy = new E_Policy__c();
    	insert policy;
    	
    	// 販売取扱者
    	chtpfs = new List<E_CHTPF__c>();
        Integer i = 1;
    	for(String agcode : agcodes){
    		chtpfs.add(new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = agcode,ZATSEQNO__c = i));
             i++;
    	}
    	insert chtpfs;
    }
    
//    private static E_CHTPF__c getChtpf(String agcode){
//    	return [Select Id From E_CHTPF__c Where AGNTNUM__c = :agcode AND ZATSEQNO__c = 1 Limit 1];
//    }
    private static E_CHTPF__c getChtpf(Integer seqCode){
        return [Select Id From E_CHTPF__c Where ZATSEQNO__c =: seqCode Limit 1];
    }
}