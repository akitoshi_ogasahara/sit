@isTest
private class TestE_EncryptUtil {
    private static string password;
    private static testMethod void testEncrypt(){
        password = E_EncryptUtil.encrypt('terrask1');
        system.assertEquals('eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'
                            , password);
    }
    
    private static testMethod void testencryptInitPass1(){
        password = E_EncryptUtil.encryptInitPass('1234567801', '20150121');
        system.assertEquals('1341157570', password);
    }
    private static testMethod void testencryptInitPass2(){
        password = E_EncryptUtil.encryptInitPass('1234567801', '20000008');
        system.assertEquals('2133945427', password);
    }
    private static testMethod void testprdCodeConversion1(){
        //暗号化のテスト
        password = E_EncryptUtil.prdCodeConversion('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]:#=._', true);
        system.assertEquals('z1SNx3TGcVdDFMr7fvbE08CI6gyhLn9mja4X:qQwRBsU2H5uK!iZ=Wet_loJPp@kOYA[]'
                            , password);
    }
    private static testMethod void testprdCodeConversion2(){
        //複合化のテスト
        password = E_EncryptUtil.prdCodeConversion('z1SNx3TGcVdDFMr7fvbE08CI6gyhLn9mja4X:qQwRBsU2H5uK!iZ=Wet_loJPp@kOYA[]', false);
        system.assertEquals('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]:#=._'
                            , password);
    }
    private static testMethod void testEncryptDecrypt(){
    	String s = 'Thrown if the data is greater than 1 MB. For decryption, 1048608 bytes are allowed for the initialization vector header, plus any additional padding the encryption added to align to block size.';
    	s += '東京都中央区日本橋1-3-13 東京建物日本橋ビル7階';
    	String encrypted = E_EncryptUtil.getEncryptedString(s);
		system.debug('■'+encrypted);
		String decrypted = E_EncryptUtil.getDecryptedString(encrypted);
		system.debug('■'+decrypted);
    	System.assertEquals(decrypted, s);
    }
}