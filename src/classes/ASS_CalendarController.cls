public with sharing class ASS_CalendarController {
	//ユーザid
	public String usId {get; private set;}
	//ユーザ名
	public String usName {get; private set;}
	//MRID
	public String mrId{get; set;}
	//MRID
	public String paramMrId{get; set;}
	//営業部ID
	public String paramUnitId{get; set;}
	//行動のリスト
	private List<Event> events;
	//Month
	private Month month;

	//コンストラクタ
	public ASS_CalendarController() {
		usId = UserInfo.getUserId();
		usName = UserInfo.getName();
		mrId = ApexPages.currentPage().getParameters().get('mrId');
		paramMrId = '';
		paramUnitId = '';

		//今日の日付を設定
		Date d = system.today();
		//今日の月を取得
		Integer mo = d.month();
		//パラメータを設定
		String m_param = System.currentPageReference().getParameters().get('mo');
		String y_param = System.currentPageReference().getParameters().get('yr');

		if(String.isNotEmpty(m_param)){
			Integer mi = Integer.valueOf(m_param);
			if(mi > 0 && mi <= 12){
				d = Date.newInstance(d.year(),mi,d.day());
			}
		}

		if(String.isNotEmpty(y_param)) {
			Integer yr = Integer.valueOf(y_param);
			d = Date.newInstance(yr, d.month(), d.day());
		}

		setMonth(d);
	}

	public void setMonth(Date d) {
		month = new Month(d);
		system.assert(month != null);

		//
		Date[] da = month.getValidDateRange();

		events = [SELECT Id 
						, subject				//件名
						, description			//説明
						, activitydate 			//日付
						, activitydatetime 		//時間
						, DurationInMinutes 	//所要時間
						, CalendarLink__c 		//カレンダーリンク
						, IsAllDayEvent 		//終日行動
				FROM 
						Event
				WHERE 
						activitydate >= :da[0] 
				AND 
						activitydate <= :da[1]
				AND 
						OwnerID = :mrId
						//OwnerID = :usId
				ORDER BY 
						activitydatetime
				];
		month.setEvents(events);
	}
	//次の月を表示
	public void next() {
		addMonth(1);
	}
	//前の月を表示
	public void prev() { 
		addMonth(-1); 
	}

	public List<ASS_CalendarController.Week> getWeeks() {
		system.assert(month != null);
		return month.getWeeks();
	}

	public Month getMonth(){
		return month;
	}

	public void addMonth(Integer val) {
		Date d = month.getFirstDate();
		d = d.addMonths(val);
		setMonth(d);
	}

	//内部クラス（Month）
	public class Month {
		public List<Week> weeks;
		public Date firstDate;
		public Date upperLeft;

		//コンストラクタ
		public Month(Date value) {
			weeks = new List<week>();
			//valueの月の最初の日付を取得
			firstDate = value.toStartOfMonth();
			//firstDateの週の最初の日付を取得
			upperLeft = firstDate.toStartOfWeek();

			Date tmp = upperLeft;
			for(Integer i = 0; i < 5; i++) {
				Week w = new Week(i+1,tmp,value.month());
				System.assert(w != null);
				this.weeks.add(w);
				tmp = tmp.addDays(7);
			}
		}

		//ひと月のカレンダーに表示する最初の日付と最後の日付を取得
		public List<Date> getValidDateRange() {
			List<Date> ret = new List<Date>();
			ret.add(upperLeft);
			ret.add(upperLeft.addDays(5*7));
			return ret;
		}

		//月名を取得
		public String getMonthName() {
			return String.valueOf(firstDate.month() + '月');
		}
		//年を取得
		public String getYearName() {
			return Datetime.newInstance(firstDate.year(),firstDate.month(),firstDate.day()).format('yyyy');
		}

		//曜日のリストを作成
		public String[] getWeekdayNames() {
			//今週の開始日を取得
			Date today = system.today().toStartOfWeek();
			Datetime dt = Datetime.newInstanceGmt(today.year(),today.month(),today.day());
			List<String> ret = new List<String>();
			for(Integer i = 0; i < 7; i++) {
				//retに曜日をadd
				ret.add(dt.formatGmt('EEEE'));
				dt = dt.addDays(1);
			}
			return ret;
		}

		public Date getFirstDate() {
			return firstDate;
		}
		//
		public void setEvents(List<Event> ev) {
			for(Event e :ev) {
				for(Week w :weeks) {
					for(Day c :w.getDays()) {
						if(e.activitydate.isSameDay(c.theDate)) {
							c.eventsToday.add(new EventItem(e));
						}
					}
				}
			}
		}
		public List<Week> getWeeks() { 
			return this.weeks; 
		}
	}

	//内部クラス（週）
	public class Week {
		public List<Day> days;
		public Integer weekNumber;
		public Date startingDate;

		public Week() {
			days = new List<Day>();
		}

		public Week(Integer value, Date sunday, Integer month) {
			this();
			//何週目か設定
			weekNumber = value;
			//開始日
			startingDate = sunday;
			Date tmp = startingDate;
			for(Integer i = 0; i < 7; i++) {
				Day d = new Day(tmp,month);
				tmp = tmp.addDays(1);
				d.dayOfWeek = i+1;
				days.add(d);
			}
		}
		public List<Day> getDays() {
			return this.days;
		}

		public Integer getWeekNumber() {
			return this.weekNumber;
		}

		public Date getStartingDate() {
			return this.startingDate;
		}
	}
	//内部クラス（日）
	public class Day {
		public Date theDate;
		public List<EventItem> eventsToday;
		public Integer month, dayOfWeek;
		public String formatedDate;
		public String cssClass = 'calActive';
		public String dateCalendarLink;
		public String linkDate;

		public Day(Date value, Integer vmonth) {
			theDate = value;
			month = vmonth;
			dateCalendarLink = '/00U/c?md0=' + String.valueOf(theDate.year()) + '&md3=' + String.valueOf( Date.newInstance(theDate.year(),1,1).daysBetween(theDate)+1 )+'&cal_lkid=';
			linkDate = String.valueOf(theDate.year() + '-' + theDate.month() + '-' + theDate.day());
			formatedDate = '';
			eventsToday = new List<EventItem>();
			if(theDate.daysBetween(System.today()) == 0) {
				cssClass ='calToday';
			}
			if(theDate.month() != month) {
				cssClass = 'calInactive';
			}
		}

		public Date getDate() {
			return theDate;
		}

		public Integer getDayOfMonth() {
			return theDate.day();
		}

		public String getDayOfMonth2() {
			if(theDate.day() <= 9) {
				return '0' + theDate.day();
			}
			return String.valueOf(theDate.day());
		}

		public Integer getDayOfYear() {
			return theDate.dayOfYear();
		}

		public List<EventItem> getDayAgenda() {
			return eventsToday;
		}

		public String getFormatedDate() {
			return formatedDate;
		}

		public Integer getDayNumber() {
			return dayOfWeek;
		}

		public List<EventItem> getEventsToday() {
			return eventsToday;
		}

		public String getCSSName() {
			return cssClass;
		}

		public String getDateCalendarLink() {
			return dateCalendarLink;
		}

		public String getLinkDate() {
			return linkDate;
		}
	}
	//内部クラス（行動）
	public class eventItem {
		public Event ev;
		public String formatedDate;

		public eventItem(Event e) {
			ev = e;
			if(!e.IsAllDayEvent){
				Datetime endd = e.activitydatetime.addMinutes(e.DurationInMinutes);
				formatedDate = e.activitydatetime.format('HH:mm') + ' - ' + endd.format('HH:mm');
			}else {
				formatedDate = '終日行動';
			}
		}
		public Event getEv() { 
			return ev;
		}
		public String getFormatedDate() { 
			return formatedDate; 
		}
	}

	//行動作成
	public PageReference createEvent(){
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('retPgName','/apex/ASS_MRPage');
		pr.getParameters().put('mrId',paramMrId);
		pr.getParameters().put('unitId',paramUnitId);
		pr.getParameters().put('date',ApexPages.currentPage().getParameters().get('date'));
		pr.setRedirect(true);
		return pr;
	}
	//行動参照
	public PageReference viewEvent(){
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('retPgName','/apex/ASS_MRPage');
		pr.getParameters().put('mrId',paramMrId);
		pr.getParameters().put('unitId',paramUnitId);
		pr.getParameters().put('eventId',ApexPages.currentPage().getParameters().get('evId'));
		pr.setRedirect(true);
		return pr;
	}

}