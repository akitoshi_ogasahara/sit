/**
 *	  /apex/IRIS_Pamphlet  pageのController
 *
 */
public with sharing class I_PamphletController extends I_CMSController {

	// コンストラクタ
	public I_PamphletController() {
		
	}

	protected virtual override List<I_ContentMaster__c> getContentMasterRecords(){
		// 親コンテンツ『パンフレット等アンカー』、『パンフレット／特に重要なお知らせ』、『ご契約のしおり・約款』、『ニード喚起資料等』のIRISCMSコンテンツ親子レコード取得
		return I_ContentMasterDao.getPamphletRecords(this.menu.Name);
	}

	protected virtual override Id getIrisMenuId(){

		String pid = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGE);		//?page=
		String formNo = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_DOC_NUM); //?docnumber=

		// フォームNoがURLパラメータに含まれる場合
		if(String.isNotBlank(formNo)) {


			I_ContentMaster__c cont = I_ContentMasterDao.getRecByFormNo(formNo);
			
			if(cont != null && String.isNotBlank(cont.PamphletCategory__c)){
				
				String menuName = cont.PamphletCategory__c.split(';').get(0);
				
				I_MenuMaster__c menu = I_MenuMasterDao.getRecByNameAndSubCategory(menuName, I_Const.MENU_SUB_CATEGORY_LIBLARY_PAMPHLET);

				if(menu != null) {

					String pageId = menu.Pages__r.size() > 0 ? menu.Pages__r.get(0).Id : '';
					ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_PAGE, pageId);
					ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_MENUID, menu.Id);
					if (String.isBlank(ApexPages.currentPage().getAnchor())) {
						ApexPages.currentPage().setAnchor(cont.DOM_Id__c);
					}
					System.debug(ApexPages.currentPage().getUrL());
					return super.getIrisMenuId();
				}
			}
		}
		
		if(pid == null){
			//Pamphletのデフォルトページを開く
			I_MenuMaster__c pamphletDefaultMenu = iris.irisMenus.extractDefaultMenuRecord(I_Const.MENU_MAIN_CATEGORY_LIBLARY
																							, I_Const.MENU_SUB_CATEGORY_LIBLARY_PAMPHLET);
			if(pamphletDefaultMenu.pages__r.size() > 0){
				ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_PAGE, pamphletDefaultMenu.pages__r[0].Id);
			}
		}
		return super.getIrisMenuId();
	}

}