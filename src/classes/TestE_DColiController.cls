@isTest
private with sharing class TestE_DColiController{
	private static testMethod void testPageMethods() {		E_DColiController extension = new E_DColiController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testCRLPFtable() {
		E_DColiController.CRLPFtable CRLPFtable = new E_DColiController.CRLPFtable(new List<E_CRLPF__c>(), new List<E_DColiController.CRLPFtableItem>(), new List<E_CRLPF__c>(), null);
		CRLPFtable.create(new E_CRLPF__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableCOVPF() {
		E_DColiController.dataTableCOVPF dataTableCOVPF = new E_DColiController.dataTableCOVPF(new List<E_COVPF__c>(), new List<E_DColiController.dataTableCOVPFItem>(), new List<E_COVPF__c>(), null);
		dataTableCOVPF.create(new E_COVPF__c());
		System.assert(true);
	}
	
}