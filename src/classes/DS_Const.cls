public class DS_Const {
	// 利用アプリケーション
	public static final String APPNAME = '医的な引受の目安';
	//ダウンロード履歴.帳票種別
	public static final String DH_FORMNO_DISEASE = 'G';		//傷病No情報
	//ファイル種別
	public static final String FILETYPE_NORMAL = '0';
	public static final String FILETYPE_SUMISEI = '1';		//住生の場合

	//傷病Noパラメータ
	public static final String DISEASE_URL_PARAM_NUMBER = 'p';

	//繋ぎ文字
	public static final String GROUP_PUNCTUATION = ' / ';

	public static final String HYPHEN = '-';
	public static final String COMMA = ',';


	/* 
	　* 傷病一覧
	　*/
	public static final String PAGE_NAME_DS_INDEX = '傷病一覧';

	public static Map<String, List<String>> INITIAL_MAP = new Map<String, List<String>>{
																						'ア' => new List<String>{'ア','イ','ウ','エ','オ'}
																						,'カ' => new List<String>{'カ','キ','ク','ケ','コ'}
																						,'サ' => new List<String>{'サ','シ','ス','セ','ソ'}
																						,'タ' => new List<String>{'タ','チ','ツ','テ','ト'}
																						,'ナ' => new List<String>{'ナ','ニ','ヌ','ネ','ノ'}
																						,'ハ' => new List<String>{'ハ','ヒ','フ','ヘ','ホ'}
																						,'マ' => new List<String>{'マ','ミ','ム','メ','モ'}
																						,'ヤ' => new List<String>{'ヤ','ユ','ヨ'}
																						,'ラ' => new List<String>{'ラ','リ','ル','レ','ロ'}
																						,'ワ' => new List<String>{'ワ','ヲ'}
																						};
	//INITIAL_MAPのKey(昇順)
	public static final List<String> INDEX_LIST = new List<String>{'ア','カ','サ','タ','ナ','ハ','マ','ヤ','ラ','ワ'};
	//Indexの初期値
	public static final String INDEX_A = 'ア';


	/* 
	　* 疾患検索
	　*/
	public static final String PAGE_NAME_DS_SEARCH = '傷病名検索';
	public static final String ROUTE_KEYWORD = 'キーワード';
	public static final String ROUTE_CATEGORY = '分類';
	public static final String ROUTE_INDEX = '五十音';

	// 表示するリンクの数(現在ページを中心に前後に表示する数)
	public static final Integer NUMBER_OF_LINKS = 2;
	public static final Integer PAGE_SIZE = 5;

	//注意事項のIRISページのページユニークキー
	public static final String PAGE_UNIQUE_KEY_NORMAL = 'ds0';
	public static final String PAGE_UNIQUE_KEY_SUMISEI = 'ds1';		//住生の場合

}