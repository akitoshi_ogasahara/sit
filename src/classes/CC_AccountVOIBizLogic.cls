/*
 * CC_AccountVOIBizLogic
 * Business logic class for Account
 * created  : Accenture 2018/7/3
 * modified :
 */

public with sharing class CC_AccountVOIBizLogic{

	/**
	 * Save account
	 */
	public static Boolean saveAccount(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		Account accountObj = new Account();
		String strErrorMessage = '';
		accountObj.Id = (String)inputMap.get('targetId');
		System.debug(inputMap);

		if(inputMap.get('[\'CMN_Phone3__c\']') != null) accountObj.CMN_Phone3__c = (String)inputMap.get('[\'CMN_Phone3__c\']');
		if(inputMap.get('[\'CMN_Phone3PIC__c\']') != null) accountObj.CMN_Phone3PIC__c = (String)inputMap.get('[\'CMN_Phone3PIC__c\']');
		if(inputMap.get('[\'CMN_Email2__c\']') != null) accountObj.CMN_Email2__c = (String)inputMap.get('[\'CMN_Email2__c\']');
		if(inputMap.get('[\'Description\']') != null) accountObj.Description = (String)inputMap.get('[\'Description\']');

		//システムエラーを取得
		if(strErrorMessage == ''){
			Database.SaveResult result = Database.update(accountObj, false);
			for(Database.Error err : result.getErrors()){
				String errorContent = err.getMessage();
				if(errorContent.contains('メール2')){
					strErrorMessage += '無効なメールアドレスです。';
				}
				else{
					strErrorMessage += errorContent;
				}

			}
			System.debug('strErrorMessage'+strErrorMessage);
		}

		if(strErrorMessage == ''){
			Map<String, Object> latestObjectValueMap = new Map<String, Object>();
			for(Account latestObj : [SELECT Id, SystemModstamp, CMN_Phone3__c, CMN_Phone3PIC__c, CMN_Email2__c, Description, CC_LastUpdateDateForAccountInfo__c FROM Account WHERE Id = :accountObj.Id]){
				Map<String, Object> fieldValues = latestObj.getPopulatedFieldsAsMap();
				for (String fieldName : fieldValues.keySet()) {
					if(fieldName == 'SystemModstamp'){
						latestObjectValueMap.put(fieldName, ((Datetime)fieldValues.get(fieldName)).getTime());
					}else{
						latestObjectValueMap.put(fieldName, fieldValues.get(fieldName));
					}
				}
			}

			//項目をブランクにするとき画面上でブランクを即時に表示ちょっこ
			if(latestObjectValueMap.size() != 0){
				if(latestObjectValueMap.get('CMN_Phone3__c') == null){
					latestObjectValueMap.put('CMN_Phone3__c', '');
				}
				if(latestObjectValueMap.get('CMN_Phone3PIC__c') == null){
					latestObjectValueMap.put('CMN_Phone3PIC__c', '');
				}
				if(latestObjectValueMap.get('CMN_Email2__c') == null){
					latestObjectValueMap.put('CMN_Email2__c', '');
				}
				if(latestObjectValueMap.get('Description') == null){
					latestObjectValueMap.put('Description', '');
				}
				outputMap.put('latestObjectValue', (Object)latestObjectValueMap);
				System.debug('latestObjectValue'+latestObjectValueMap);
			}
		}else{
			outputMap.put('inputErrorMessage', strErrorMessage);
			success = false;
		}

		return success;
	}

}