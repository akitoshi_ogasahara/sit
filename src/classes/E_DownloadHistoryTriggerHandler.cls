public with sharing class E_DownloadHistoryTriggerHandler {

	//after insert
	public void onAfterInsert(List<E_DownloadHistorry__c> newList){

		//insert時に使うリスト
		List<E_Log__c> logList = new List<E_Log__c>();

		try{
			//Trigger.newで渡されたリストを全て回す
			for(E_DownloadHistorry__c dh : newList){

				//帳票種別の中身が1,2,3,4,5,6,A,B,C,Dのいずれかの場合ログレコードを作成する
				if(E_Const.DH_FORMNOS.contains(dh.FormNo__c)){
					E_Log__c log = E_LogUtil.createLog();

					//ログ名 = 帳票名
					log.Name = dh.FormName__c;
					//アクションタイプ = 帳票種別によって設定
					log.ActionType__c = getActionType(dh.FormNo__c);
					//ダウンロード履歴 = ダウンロード履歴ID
					log.DownloadHistory__c = dh.Id;
					//一覧表示フラグ = false
					log.isDisplay__c = false;


					// アクセスページ -- 文字数オーバーになるため空文字更新
					log.AccessPage__c = '';

					//リストに足す
					logList.add(log);
				}
			}

			if(!logList.isEmpty()){
				insert logList;
			}

		}catch(Exception e){
			System.debug('****E_DownloadHistoryTriggerHandler ログの作成に失敗しました****' + e);
		}
	}

	/**
	 * 帳票種別よりアクションタイプを取得する
	 */
	private String getActionType(String formNo){
		String aType = '';

		// PDF出力
		if(I_Const.TYPEPDF.contains(formNo)){
			aType = I_Const.ACTION_TYPE_PDF;
		// 帳票ダウンロード
		}else{
			aType = I_Const.ACTION_TYPE_REPORT_DOWNLOAD;
		}

		return aType;
	}

}