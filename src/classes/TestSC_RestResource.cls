@isTest
private class TestSC_RestResource {

    // 成功
    static testMethod void test_status_Success(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            RestRequest req = new RestRequest();
            req.params.put('token', selfComp.LimeSurveyToken__c);
            req.params.put('surveyid', selfComp.LimeSurveyId__c);
            RestContext.request = req;

            SC_RestResource.appStatusResponse res = SC_RestResource.appStatus();
        
            System.assertEquals(res.StatusCode, 'SCS001');
        }
    }

    // token なし 1
    static testMethod void test_status_NoToken1(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            RestRequest req = new RestRequest();
            req.params.put('token', '');
            req.params.put('surveyid', selfComp.LimeSurveyId__c);
            RestContext.request = req;

            SC_RestResource.appStatusResponse res = SC_RestResource.appStatus();

            System.assertEquals(res.StatusCode, 'SCE001');
        }
    }

    // token なし 2
    static testMethod void test_status_NoToken2(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            RestRequest req = new RestRequest();
            req.params.put('token', 'AAAAA');
            req.params.put('surveyid', selfComp.LimeSurveyId__c);
            RestContext.request = req;

            SC_RestResource.appStatusResponse res = SC_RestResource.appStatus();

            System.assertEquals(res.StatusCode, 'SCE003');
        }
    }

    // 成功
    static testMethod void test_answer_Success(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.token = selfComp.LimeSurveyToken__c;
            req.surveyid = Integer.valueOf(selfComp.LimeSurveyId__c);

            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCS001');
        }
    }

    // token なし 1
    static testMethod void test_answer_NoToken1(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.surveyid = Integer.valueOf(selfComp.LimeSurveyId__c);

            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCE001');
        }
    }

    // surveyid なし
    static testMethod void test_answer_NoSurveyid(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.token = selfComp.LimeSurveyToken__c;
            
            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCE002');
        }
    }

    // token なし 2
    static testMethod void test_answer_NoToken2(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.token = 'AAAAA';
            req.surveyid = 12345;

            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCE004');
        }
    }

    // 提出方法 PDF
    static testMethod void test_answer_DifferentTypeSubmit(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        selfComp.TypeSubmit__c = 'PDF';
        update selfComp;

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.token = selfComp.LimeSurveyToken__c;
            req.surveyid = Integer.valueOf(selfComp.LimeSurveyId__c);

            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCE005');
        }
    }

    // ステータス 未提出以外
    static testMethod void test_answer_DifferentStatus(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        office.Status__c = '担当MR確認中';
        update office;

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.token = selfComp.LimeSurveyToken__c;
            req.surveyid = Integer.valueOf(selfComp.LimeSurveyId__c);

            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCE006');
        }
    }

    // その他エラー
    static testMethod void test_answer_OtherException(){
    	createAccount();
        createPartnerUser();
    	createOffice();
    	createShare();

        System.runAs(partnerUsr){
        	TestSC_TestUtil.createAgencyPermissions(partnerUsr.id);

            SC_RestResource.appAnswerRequest req = buildAPIRequest();
            req.token = selfComp.LimeSurveyToken__c;
            req.surveyid = Integer.valueOf(selfComp.LimeSurveyId__c);
            req.responses.put('Q99901', 'test');

            SC_RestResource.appAnswerResponse res = SC_RestResource.answerRegistered(req);

            System.assertEquals(res.StatusCode, 'SCE000');
        }
    }

//==============================================
//          テスト環境構築
//==============================================
	private static Account accParent;
	private static Account accAgency;
	private static Contact con;
	private static void createAccount() {
		if(accAgency == null){
    		accParent = TestSC_TestUtil.createAccount(true, null);
		    accAgency = TestSC_TestUtil.createAccount(false, accParent);
		    accAgency.Parentid = accParent.id;
		    insert accAgency;

		}
	}


    // 自主点検レコード作成
    private static SC_Office__c office;
    private static SC_SelfCompliance__c selfComp;
    private static void createOffice() {

    	if (office == null) {
	    	office = new SC_Office__c();
	    	office.Account__c = accAgency.id;
	    	office.OfficeType__c = '法人';
	    	office.FiscalYear__c = '2018';
	    	office.LimeSurveyTokenCommon__c = 'ABCDE12345';
	    	office.LimeSurveyTokenAdd__c = 'BCDEF23456';
            office.Status__c = SC_Const.AMSOFFICE_STATUS_01;
	    	insert office;


	    	selfComp = [select id, name, kind__c, LimeSurveyToken__c, LimeSurveyId__c, TypeSubmit__c from SC_SelfCompliance__c where SC_Office__c = :office.id and kind__c = '共通'];
    	}
   }

    //PartnerCommunity Userの作成
    private static User partnerUsr;
    private static void createPartnerUser(){
        if(partnerUsr == null){
		    Contact con = TestSC_TestUtil.createContact(false, accAgency.Id, 'TestUser001');
		    con.E_CL3PF_ZAGMANGR__c = 'Y';
		    insert con;

            // User
            partnerUsr = TestSC_TestUtil.createAgentUser(true, 'TestUser001', 'E_PartnerCommunity', con.Id);
       }
    }

    private static void createShare(){
    	TestSC_TestUtil.createSCOfficeShare(office.Id, partnerUsr.Id);
  		TestSC_TestUtil.createAccountShare(accAgency.Id, partnerUsr.Id);
    	TestSC_TestUtil.createAccountShare(accParent.Id, partnerUsr.Id);

    }

    private static SC_RestResource.appAnswerRequest buildAPIRequest(){
        SC_RestResource.appAnswerRequest req = new SC_RestResource.appAnswerRequest();
        req.holdstatus = 'none';
        req.responseid = '1';
        req.submitdate = System.now().format('yyyy-MM-dd');
        req.responses = new Map<String,String>{
                                     'Q00101'=>'Y_はい'
                                    ,'Q00102'=>'N_いいえ'
                                };
        return req;
    }
}