public with sharing class I_PollingController {
	private Integer pollingCount = 0;

	public Integer getPollingCount() {
		return pollingCount;
	}

	public PageReference polling() {
		pollingCount += 1;
		return null;
	}
}