global with sharing class E_InveExtender extends E_AbstractViewExtender{
	private static final String PAGE_TITLE = '投資金額';
	
	E_InveController extension;
	
	public String zinvamtAmount {get;set;}
	public String zbrndper01Amount {get;set;}
	
	//グラフコンポーネントの引数
	public String getExecuteKind(){
		return E_Const.GRAPH_INVESTMENT_BUY;
	}
	
	/** コンストラクタ */
	public E_InveExtender(E_InveController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
	}

	/** init */
	global override void init(){
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, extension.record.id);
		if (pageRef == null) {
			// 投資金額合計を設定
			initInvestmentAmount();
		}
	}

	/** ページアクション */
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}
	
	/** 投資金額合計を設定 */
	private void initInvestmentAmount(){
		Decimal zinvamt = 0;
		Decimal zbrndper01 = 0;
		List<E_InveController.dataTableITFPFItem> items = extension.dataTableITFPF.items;
		if(items!=null){
	 		for(E_InveController.dataTableITFPFItem item :items){
	 			E_ITFPF__c obj = (E_ITFPF__c)item.record;
	 			if(obj.ZINVAMT__c != null) zinvamt += obj.ZINVAMT__c;
	 			if(obj.ZBRNDPER01__c != null) zbrndper01 += obj.ZBRNDPER01__c;
	 		}
	 	}
		zinvamtAmount = zinvamt.format();
		zbrndper01Amount = String.valueOf(zbrndper01);
	}
	
 	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		return new PageReference(E_CookieHandler.getCookieRefererInve());
	}
}