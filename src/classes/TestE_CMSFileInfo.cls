@isTest
private class TestE_CMSFileInfo
{
	/**
     * CMSFileInfo 分岐網羅
     */
    private static testMethod void testCMSFileInfo1() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	//テストデータ作成
            E_MenuMaster__c menuMaster1 = new E_MenuMaster__c(MenuMasterKey__c='key1',SelectedMenuKey__c='key1');
            insert menuMaster1;

        	List<E_MessageMaster__c> insMessage = new List<E_MessageMaster__c>();
	        String type = 'メッセージ';
	        String param = 'CPL|001';

	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        param = 'CPL|002';
	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        param = 'CPL|003';
	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        insert insMessage;

	        RecordType rtype = [Select Id From RecordType Where DeveloperName = 'pattern03' Limit 1];
            E_CMSFile__c cmsFile = new E_CMSFile__c(ContentsName__c='ContentsName');
            cmsFile.RecordTypeId = rtype.Id;
            cmsFile.MessageMaster__c = insMessage.get(0).Id;
            insert cmsFile;

            ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
            content.VersionData = Blob.valueOf('Testing base 64 encode');
            insert content;
            content = [Select Id,ContentSize From ContentVersion Where Id = :content.id Limit 1];

            List<E_MessageMaster__c> messageMasterList = E_MessageMasterDao.getMessagesByMenuId(menuMaster1.Id);
            // 添付ファイルの登録
	        Attachment attach = new Attachment();
	        attach.Name = 'UTest Attachment';
	        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
	        attach.body = bodyBlob;

	        attach.parentId = cmsFile.id;
	        insert attach;
	        attach = [Select Id,Name,body,BodyLength From Attachment Where Parent.Id = :cmsFile.id Limit 1];
	        //E_AttachmentDao.getRecById(cmsFile.id);
	        //cmsFile = [Select Id,Name,ContentsName__c,RecordTypeId,MessageMaster__c From E_CMSFile__c Where Id = :cmsFile.id Limit 1];
	        cmsFile = [Select Id,Name,ContentsName__c,RecordTypeId,MessageMaster__c,(Select Id,Name,BodyLength From Attachments) From E_CMSFile__c Where Id = :cmsFile.id Limit 1];
            //テスト開始
            Test.startTest();
            E_CMSFileInfo fileInfo = new E_CMSFileInfo(cmsFile);
            fileInfo.info = cmsFile;
            fileInfo.attachment = null;
            fileInfo.content = null;
            fileInfo.getAttachmentSize();
            fileInfo.getAttachmentExt();
            fileInfo.getContentSize();
            fileInfo.getContentDownloadUrl();
            // 値の設定後
            fileInfo.attachment = attach;
            fileInfo.content = content;
            //fileInfo = new E_CMSFileInfo(cmsFile);
            fileInfo.getAttachmentSize();
            fileInfo.getAttachmentExt();
            fileInfo.getContentDownloadUrl();
            fileInfo.getContentSize();
            // 添付ファイルの更新
            attach.Name = 'UTest Attachment.';
            fileInfo.attachment = attach;
            fileInfo.getAttachmentExt();

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }

	/**
     * CMSFileInfo 分岐網羅
     */
    private static testMethod void testCMSFileInfoLongLen() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	//テストデータ作成
            E_MenuMaster__c menuMaster1 = new E_MenuMaster__c(MenuMasterKey__c='key1',SelectedMenuKey__c='key1');
            insert menuMaster1;

        	List<E_MessageMaster__c> insMessage = new List<E_MessageMaster__c>();
	        String type = 'メッセージ';
	        String param = 'CPL|001';

	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        param = 'CPL|002';
	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        param = 'CPL|003';
	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        insert insMessage;

	        RecordType rtype = [Select Id From RecordType Where DeveloperName = 'pattern03' Limit 1];
            E_CMSFile__c cmsFile = new E_CMSFile__c(ContentsName__c='ContentsName');
            cmsFile.RecordTypeId = rtype.Id;
            cmsFile.MessageMaster__c = insMessage.get(0).Id;
            insert cmsFile;

            ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
            content.VersionData = Blob.valueOf('Testing base 64 encode');
            insert content;
            content = [Select Id,ContentSize From ContentVersion Where Id = :content.id Limit 1];

            List<E_MessageMaster__c> messageMasterList = E_MessageMasterDao.getMessagesByMenuId(menuMaster1.Id);
            // 添付ファイルの登録
	        Attachment attach = new Attachment();
	        attach.Name = 'UTest Attachment';
            String len = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
            for(integer i=0;i<10;i++){
                len += len;
            }
            len += '1';
            system.debug(len.length());
            //Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
            Blob bodyBlob = Blob.valueOf(len);
	        attach.body = bodyBlob;
			
	        attach.parentId = cmsFile.id;
	        insert attach;
	        attach = [Select Id,Name,body,BodyLength From Attachment Where Parent.Id = :cmsFile.id Limit 1];
            system.debug(attach.bodyLength);
	        //E_AttachmentDao.getRecById(cmsFile.id);
	        //cmsFile = [Select Id,Name,ContentsName__c,RecordTypeId,MessageMaster__c From E_CMSFile__c Where Id = :cmsFile.id Limit 1];
	        cmsFile = [Select Id,Name,ContentsName__c,RecordTypeId,MessageMaster__c,(Select Id,Name,BodyLength From Attachments) From E_CMSFile__c Where Id = :cmsFile.id Limit 1];
            //テスト開始
            Test.startTest();
            E_CMSFileInfo fileInfo = new E_CMSFileInfo(cmsFile);
            fileInfo.info = cmsFile;
            fileInfo.attachment = null;
            fileInfo.content = null;
            fileInfo.getAttachmentSize();
            fileInfo.getAttachmentExt();
            fileInfo.getContentSize();
            fileInfo.getContentDownloadUrl();
            // 値の設定後
            fileInfo.attachment = attach;
            fileInfo.content = content;
            //fileInfo = new E_CMSFileInfo(cmsFile);
            fileInfo.getAttachmentSize();
            fileInfo.getAttachmentExt();
            fileInfo.getContentDownloadUrl();
            fileInfo.getContentSize();
            // 添付ファイルの更新
            attach.Name = 'UTest Attachment.';
            fileInfo.attachment = attach;
            fileInfo.getAttachmentExt();

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }    
    
    
    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}