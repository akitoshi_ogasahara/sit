/**
 *		EBiz連携ログ　から発火するBatch処理標準クラス
 *				
 */
//with sharing 
global virtual class E_BizDataSyncBatch implements Database.Batchable<SObject>, Database.Stateful {

	protected Id ebizLogId;
	protected String ebizLogKind;
	protected E_BizDataSyncBatchOperation operation;
	protected List<String> errorMessages;


	/**
	 *		Constructor
	 */
	global E_BizDataSyncBatch(Id logId){
		ebizLogId = logId;
		errorMessages = new List<String>();
	}

	/**
	 * BatchApexの開始処理です
	 * @param ctx
	 * @return Database.QueryLocator 
	 */
	global Database.QueryLocator start(Database.BatchableContext ctx){
		E_BizDataSyncLog__c log;
		try{
			log = E_BizDataSyncLogDao.getRecById(ebizLogId);
			log.DataSyncStartDate__c = System.Now();
			
			// 区分
			ebizLogKind = log.Kind__c;
			
			//バッチ処理クラスの生成
			Type t = Type.forName(log.operationClassName__c);
			operation = (E_BizDataSyncBatchOperation)t.NewInstance();
			update log;
			return operation.getQueryLocator();
		}catch(Exception e){
			//startメソッド内のエラーは、errorMessagesに格納され、finish時に設定される。
			//		開始時刻は未設定となる。ExecuteはダミーLocatorの結果実行されない。
			errorMessages.add(String.format('Operationクラスの生成に失敗しました。(クラス名:{0}){1}', new List<String>{log.operationClassName__c, e.getMessage()}
										));
		}
		
		//Operationクラスの取得が正常に行われない場合、ダミーのLocatorを返す。取得できるレコードは0件のもの
		return Database.getQueryLocator([select id from user where id = null limit 1]);
	}
	
	/**
	 * BatchApexの処理実行します
	 * @param ctx
	 * @param records
	 * @return
	 */
	global void execute(Database.BatchableContext ctx, List<Sobject> records) {
		try{
			this.operation.execute(records);
		}catch(Exception e){
			// Exception
			if(operation.errMsgs.isEmpty()){
				errorMessages.add(e.getMessage());

			// メッセージあり
			}else{
				for(String str : operation.errMsgs){
					errorMessages.add(str);
				}
			}
		}
	}

	/**
	 * BatchApexの後処理
	 * @param ctx
	 * @return
	 */
	global virtual void finish(Database.BatchableContext ctx) {
		E_BizDataSyncLog__c ebizlog = new E_BizDataSyncLog__c();
		ebizlog.id = ebizLogId;
		if(errorMessages.size()>0){
			ebizlog.log__c = String.join(errorMessages, ',');
		}
		
		ebizlog.datasyncenddate__c = Datetime.now();
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		ebizlog.setOptions(dml);
		//挙積情報追加 バッチ終了日時
		ebizlog.BatchEndDate__c = Datetime.now();
		update ebizlog;
		
		// 新契約の場合、PushOut通知バッチを呼び出し用のEBiz連携ログを登録する
		if(ebizLogKind == I_Const.EBIZDATASYNC_KBN_NEWPOLICY && System.Label.E_PROCESS_PUSHOUT_NEWPOLICY == '1'){
			insert new E_BizDataSyncLog__c(kind__c = I_Const.EBIZDATASYNC_KBN_PUSHOUT, NotificationKind__c = I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		}
		//営業計画代理店挙積情報付替えをchain実行
		if(ebizLogKind == I_Const.EBIZDATASYNC_KBN_SALESRSLT_AGT){
			insert new E_BizDataSyncLog__c(kind__c = I_Const.EBIZDATASYNC_KBN_SALESPLN, InquiryDate__c = E_BizDataSyncLogDao.getRecById(ebizLogId).InquiryDate__c);
		}
	}
}