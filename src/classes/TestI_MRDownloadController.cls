@isTest
private class TestI_MRDownloadController {
    /*初期表示_社員ユーザでない場合*/
    @isTest static void pageAction_user_test01() {
        // Account 代理店格
        Account ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
        insert ahAcc;
        // Account 事務所
        Account ayAcc = new Account(
                Name = 'office1'
                ,ParentId = ahAcc.Id
                ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                ,E_COMM_VALIDFLAG__c = '1'
        );
        insert ayAcc;
        // Contact 募集人
        Contact atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
        atCon.E_CL3PF_AGNTNUM__c = 'at001';
        atCon.email = 'fstest@terrasky.ingtesting';
        insert atCon;
        //ユーザ作成
        User atUser = createUser(true, 'TEST', 'E_PartnerCommunity_IA', '', '', atCon);
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = atUser.Id, OwnerId = atUser.Id, ZINQUIRR__c = 'OW23456789');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(atUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            // ページが閲覧不可であること
//            System.assertEquals(0, mrDlCtrl.areaList.size());
//            System.assertEquals(true, mrDlCtrl.pageMessages.getHasWarningMessages());
        }
    }
    /*初期表示_MRの場合*/
    @isTest static void pageAction_user_test02() {
        //ユーザ作成
        User testUser = createUser(true, 'MR', 'ＭＲ', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = testUser.Id, OwnerId = testUser.Id, ZINQUIRR__c = 'BR3A');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');
        // MRの作成
        User testTokyoMr01User = createUser(true, 'MRtokyo01', 'ＭＲ', '東京営業部', '東日本', '2');
        User testTokyoMr02User = createUser(true, 'MRtokyo02', 'ＭＲ', '東京営業部', '東日本', '3');
        User testChibaMr01User = createUser(true, 'MRchiba01', 'ＭＲ', '千葉営業部', '東日本', '4');
        System.runAs(testUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            
            // MRのエリアと部署が表示されること、MR選択・拠点選択ボタンが非表示となること、全ての拠点選択が非表示となること
            System.assertEquals(false, mrDlCtrl.getAllArea());
            System.assertEquals(false, mrDlCtrl.getUnitSpecific());
            System.assertEquals('東日本', mrDlCtrl.areaList[0].area.Name);
            System.assertEquals('東京営業部', mrDlCtrl.areaList[0].units[0].unit.Name);
            System.assertEquals(1, mrDlCtrl.areaList.size());
            System.assertEquals(1, mrDlCtrl.areaList[0].units.size());
            System.assertEquals(true, mrDlCtrl.isSelectMR);
        }
    }
    /*初期表示_拠点長の場合*/
    @isTest static void pageAction_user_test03() {
        //ユーザ作成
        User testUser = createUser(true, 'Manager', '拠点長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = testUser.Id, OwnerId = testUser.Id, ZINQUIRR__c = 'BR3D');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');
        System.runAs(testUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            
            // 拠点長のエリアと部署が表示されること、MR選択・拠点選択ボタンが非表示となること、全ての拠点選択が非表示となること
            System.assertEquals(false, mrDlCtrl.getAllArea());
            System.assertEquals(false, mrDlCtrl.getUnitSpecific());
            System.assertEquals(1, mrDlCtrl.areaList.size());
            System.assertEquals(1, mrDlCtrl.areaList[0].units.size());
            System.assertEquals('東京営業部', mrDlCtrl.areaList[0].units[0].unit.Name);
            System.assertEquals(true, mrDlCtrl.isSelectMR);
        }
    }
    /*初期表示_エリア部長の場合*/
    @isTest static void pageAction_user_test04() {
        //ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');
        E_Area__c areaWest = createArea('西日本', 2);
        E_Unit__c unitOsaka = createUnit('大阪営業部', areaWest.Id, '1');
        E_Unit__c unitKyoto = createUnit('京都営業部', areaWest.Id, '2');
        System.runAs(areaManagerUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            // エリア部長自身のエリアが表示されること、MR選択・拠点選択ボタンが表示されること、全ての拠点選択が非表示となること
            System.assertEquals(false, mrDlCtrl.getAllArea());
            System.assertEquals(true, mrDlCtrl.getUnitSpecific());
            System.assertEquals(false, mrDlCtrl.isSelectMR);
            System.assertEquals(1, mrDlCtrl.areaList.size());
            System.assertEquals(2, mrDlCtrl.areaList[0].units.size());
        }
    }
    /*初期表示_社員ユーザの場合*/
    @isTest static void pageAction_user_test05() {
        //ユーザ作成
        User employeeUser = createUser(true, 'Employee', 'E_EmployeeStandard', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = employeeUser.Id, OwnerId = employeeUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');
        E_Area__c areaWest = createArea('西日本', 2);
        E_Unit__c unitOsaka = createUnit('大阪営業部', areaWest.Id, '1');
        E_Unit__c unitKyoto = createUnit('京都営業部', areaWest.Id, '2');
        System.runAs(employeeUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            // 4帳票の期間が取得されること
            System.assertEquals(E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO, false), mrDlCtrl.getAGPOPeriod());
            System.assertEquals(E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA, false), mrDlCtrl.getBILAPeriod());
            System.assertEquals(E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS, false), mrDlCtrl.getBILSPeriod());
            System.assertEquals(E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL, false), mrDlCtrl.getNCOLPeriod());
            // 全エリア・全営業部が表示されること、MR選択・拠点選択ボタンが表示されること、全ての拠点選択が表示されること
            System.assertEquals(true, mrDlCtrl.getAllArea());
            System.assertEquals(true, mrDlCtrl.getUnitSpecific());
            System.assertEquals(false, mrDlCtrl.isSelectMR);
            System.assertEquals(2, mrDlCtrl.areaList.size());
            System.assertEquals(2, mrDlCtrl.areaList[0].units.size());
            System.assertEquals('東京営業部', mrDlCtrl.areaList[0].units[0].unit.Name);
            System.assertEquals('千葉営業部', mrDlCtrl.areaList[0].units[1].unit.Name);
            System.assertEquals(2, mrDlCtrl.areaList[1].units.size());
            System.assertEquals('大阪営業部', mrDlCtrl.areaList[1].units[0].unit.Name);
            System.assertEquals('京都営業部', mrDlCtrl.areaList[1].units[1].unit.Name);
        }
    }
    /*初期表示_dNo有の場合*/
    @isTest static void pageAction_dNo_test01() {
        // ユーザ作成
        User employeeUser = createUser(true, 'Employee', 'E_EmployeeStandard', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = employeeUser.Id, OwnerId = employeeUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(employeeUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);    
            ApexPages.currentPage().getParameters().put('dNo', 'A');
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            //パラメータに渡されている通知が選択されていること
            System.assertEquals('A', mrDlCtrl.dlFormNo);
        }
    }
    /*初期表示_dNo無の場合*/
    @isTest static void pageAction_dNo_test02() {
        // ユーザ作成
        User employeeUser = createUser(true, 'Employee', 'E_EmployeeStandard', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = employeeUser.Id, OwnerId = employeeUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(employeeUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            Test.stopTest();
            //デフォルト値の未入金が選択されること
            System.assertEquals('D', mrDlCtrl.dlFormNo);
        }
    }
    /*MR選択_営業部未選択の場合*/
    @isTest static void pageAction_doChangeDisplay_test01() {
        // エリア部長ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');
        // チェック
        List<String> selectedUnits = new List<String>();
        String selectedUnitName;

        System.runAs(areaManagerUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            mrDlCtrl.doChangeDisplay();
            Test.stopTest();

            //営業部を1件選択するようエラー、MRが取得されないこと、MR選択フラグがfalse
            System.assertEquals(false, mrDlCtrl.isSelectMR);
            System.assertEquals(true, mrDlCtrl.pageMessages.getHasWarningMessages());

        }
    }
    /*MR選択_営業部複数選択の場合*/
    @isTest static void pageAction_doChangeDisplay_test02() {
        // エリア部長ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(areaManagerUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            mrDlCtrl.areaList[0].units[1].isChecked = true;
            mrDlCtrl.doChangeDisplay();
            Test.stopTest();

            //営業部を1件選択するようエラー、MRが取得されないこと、MR選択フラグがfalse
            System.assertEquals(false, mrDlCtrl.isSelectMR);
            System.assertEquals(true, mrDlCtrl.pageMessages.getHasWarningMessages());
            
        }
    }
    /*MR選択_営業部1件選択の場合*/
    @isTest static void pageAction_doChangeDisplay_test03() {
        /// エリア部長ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        // MRの作成
        User testTokyoMr01User = createUser(true, 'MRtokyo01', 'ＭＲ', '東京営業部', '東日本', '2');
        User testTokyoMr02User = createUser(true, 'MRtokyo02', 'ＭＲ', '東京営業部', '東日本', '3');
        User testChibaMr01User = createUser(true, 'MRchiba01', 'ＭＲ', '千葉営業部', '東日本', '4');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(areaManagerUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            mrDlCtrl.doChangeDisplay();
            Test.stopTest();

            //MRが件取得されること
            System.assertEquals(true, mrDlCtrl.isSelectMR);
            //エリア部長もmrListに含むため
            System.assertEquals(3, mrDlCtrl.mrList.size());
            
        }
    }
    /*MR選択→拠点選択*/
    @isTest static void pageAction_doChangeDisplay_test04() {
        /// エリア部長ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        // MRの作成
        User testTokyoMr01User = createUser(true, 'MRtokyo01', 'ＭＲ', '東京営業部', '東日本', '2');
        User testTokyoMr02User = createUser(true, 'MRtokyo02', 'ＭＲ', '東京営業部', '東日本', '3');
        User testChibaMr01User = createUser(true, 'MRchiba01', 'ＭＲ', '千葉営業部', '東日本', '4');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(areaManagerUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            mrDlCtrl.doChangeDisplay();
            mrDlCtrl.doChangeDisplay();
            Test.stopTest();

            //拠点選択に戻ること
            System.assertEquals(false, mrDlCtrl.isSelectMR);
        }
    }
    /*MR選択_営業部複数選択の場合*/
    @isTest static void pageAction_doChangeDisplay_test05() {
        // エリア部長ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        // MRの作成
        User testTokyoMr01User = createUser(true, 'MRtokyo01', 'ＭＲ', '東京営業部', '東日本', '2');
        User testTokyoMr02User = createUser(true, 'MRtokyo02', 'ＭＲ', '東京営業部', '東日本', '3');
        User testChibaMr01User = createUser(true, 'MRchiba01', 'ＭＲ', '千葉営業部', '東日本', '4');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(areaManagerUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            mrDlCtrl.areaList[0].units[1].isChecked = true;
            Test.stopTest();

            //MRが取得されていないこと
            System.assertEquals(false, mrDlCtrl.isSelectMR);
        }
    }
    /*ダウンロードボタン押下_主担当未選択の場合*/
    @isTest static void pageAction_doDownload_test01() {
        // ユーザ作成
        User employeeUser = createUser(true, 'Employee', 'E_EmployeeStandard', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = employeeUser.Id, OwnerId = employeeUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(employeeUser){
            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            // 課コードが未選択
            mrDlCtrl.isUnit = false;
            mrDlCtrl.isReal = false;
            mrDlCtrl.isHaed = false;
            mrDlCtrl.doDL();
            Test.stopTest();
            //主担当を選択するようエラーとなること
            System.assertEquals(true, mrDlCtrl.pageMessages.getHasWarningMessages());
            System.assertEquals('', mrDlCtrl.getDownloadId());
        }
    }

    /*ダウンロードボタン押下_本社担当分未選択_MR選択_MRの場合*/
    @isTest static void pageAction_doDownload_test02() {
        //ユーザ作成
        User testUser = createUser(true, 'MR', 'ＭＲ', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = testUser.Id, OwnerId = testUser.Id, ZINQUIRR__c = 'BR3A');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(testUser){
            //BILA作成
            createAccount();
            createContact('ay00', ayAcc00);
            createData(5, testUser.Id, ayAcc00);

            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            // 課コード1件選択
            mrDlCtrl.isUnit = true;
            mrDlCtrl.isReal = false;
            mrDlCtrl.isHaed = false;
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            ApexPages.currentPage().getParameters().put('dNo', 'B');
            ApexPages.currentPage().getParameters().put('periodB', '2017/06,2017/05,2017/04');
            mrDlCtrl.mrList[0].isChecked = true;
            mrDlCtrl.doDL();
            mrDlCtrl.getDownloadId();
            Test.stopTest();
            //MR選択モードで担当者指定をしていること
            System.assertEquals(true, mrDlCtrl.isSelectMR);
            //ダウンロード履歴のレコードが作成されること
            System.assertEquals('B', mrDlCtrl.dlhistory.FormNo__c);
            System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, mrDlCtrl.dlhistory.outputType__c);
            //System.assertEquals(mrDlCtrl.dlhistory.Id, mrDlCtrl.getDownloadId());
        }
    }
    /*ダウンロードボタン押下_本社担当分選択_MR選択_MRの場合*/
    @isTest static void pageAction_doDownload_test03() {
        //ユーザ作成
        User testUser = createUser(true, 'MR', 'ＭＲ', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = testUser.Id, OwnerId = testUser.Id, ZINQUIRR__c = 'BR3A');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(testUser){
            //BILA作成
            createAccount();
            createContact('ay99', ayAcc99);
            createData(5, testUser.Id, ayAcc99);

            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            // 課コード1件選択
            mrDlCtrl.isUnit = false;
            mrDlCtrl.isReal = false;
            mrDlCtrl.isHaed = true;
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            ApexPages.currentPage().getParameters().put('dNo', 'B');
            ApexPages.currentPage().getParameters().put('periodB', '2017/06');
            mrDlCtrl.mrList[0].isChecked = true;
            mrDlCtrl.doDL();
            Test.stopTest();
            //MR選択モードで担当者指定をしていること
            System.assertEquals(true, mrDlCtrl.isSelectMR);
            //ダウンロード履歴のレコードが作成されること
            System.assertEquals('B', mrDlCtrl.dlhistory.FormNo__c);
            System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, mrDlCtrl.dlhistory.outputType__c);
        }
    }
    /*ダウンロードボタン押下_主担当全件選択_MR選択_MRの場合*/
    @isTest static void pageAction_doDownload_test04() {
        //ユーザ作成
        User testUser = createUser(true, 'MR', 'ＭＲ', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = testUser.Id, OwnerId = testUser.Id, ZINQUIRR__c = 'BR3A');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(testUser){
            //BILA作成
            createAccount();
            createContact('ay99', ayAcc99);
            createData(5, testUser.Id, ayAcc99);

            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            // 課コード1件選択
            mrDlCtrl.isUnit = true;
            mrDlCtrl.isReal = true;
            mrDlCtrl.isHaed = true;
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            ApexPages.currentPage().getParameters().put('dNo', 'B');
            ApexPages.currentPage().getParameters().put('periodB', '2017/06');
            mrDlCtrl.mrList[0].isChecked = true;
            mrDlCtrl.doDL();
            Test.stopTest();
            //MR選択モードで担当者指定をしていること
            System.assertEquals(true, mrDlCtrl.isSelectMR);
            //ダウンロード履歴のレコードが作成されること
            System.assertEquals('B', mrDlCtrl.dlhistory.FormNo__c);
            System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, mrDlCtrl.dlhistory.outputType__c);
        }   
    }
    /*ダウンロードボタン押下_本社担当分未選択_営業選択_MRの場合*/
    @isTest static void pageAction_doDownload_test05() {
        // エリア部長ユーザ作成
        User areaManagerUser = createUser(true, 'AreaManager', 'エリア部長', '東京営業部', '東日本', '1');
        //ID管理作成
        E_IDCPF__c idcpf = new E_IDCPF__c(User__c = areaManagerUser.Id, OwnerId = areaManagerUser.Id, ZINQUIRR__c = 'BR**');
        insert idcpf;
        // MRの作成
        User testTokyoMr01User = createUser(true, 'MRtokyo01', 'ＭＲ', '東京営業部', '東日本', '2');
        User testTokyoMr02User = createUser(true, 'MRtokyo02', 'ＭＲ', '東京営業部', '東日本', '3');
        User testChibaMr01User = createUser(true, 'MRchiba01', 'ＭＲ', '千葉営業部', '東日本', '4');
        //　該当エリア・営業部作成
        E_Area__c areaEast = createArea('東日本', 1);
        E_Unit__c unitTokyo = createUnit('東京営業部', areaEast.Id, '1');
        E_Unit__c unitChiba = createUnit('千葉営業部', areaEast.Id, '2');

        System.runAs(areaManagerUser){
            //BILA作成
            createAccount();
            createContact('ay88', ayAcc88);
            createData(5, areaManagerUser.Id, ayAcc88);

            Test.startTest();
            PageReference mrDlPage = Page.IRIS_MRDownload;
            Test.setCurrentPageReference(mrDlPage);
            I_MRDownloadController mrDlCtrl = new I_MRDownloadController();
            mrDlCtrl.pageAction();
            // 課コード1件選択
            mrDlCtrl.isUnit = false;
            mrDlCtrl.isReal = true;
            mrDlCtrl.isHaed = false;
            mrDlCtrl.areaList[0].units[0].isChecked = true;
            ApexPages.currentPage().getParameters().put('dNo', 'B');
            ApexPages.currentPage().getParameters().put('periodB', '2017/06');
            mrDlCtrl.doDL();
            Test.stopTest();
            //MR選択モードで担当者指定をしていること
            System.assertEquals(false, mrDlCtrl.isSelectMR);
            //ダウンロード履歴のレコードが作成されること
            System.assertEquals('B', mrDlCtrl.dlhistory.FormNo__c);
            System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, mrDlCtrl.dlhistory.outputType__c);
        }
        
    }
    private static Account ahAcc;
    private static Account ayAcc00;
    private static Account ayAcc88;
    private static Account ayAcc99;
    private static Contact atCon;
    private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];
    private static void createAccount(){
        // Account 代理店格
        ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
        insert ahAcc;

        // Account 事務所　課コード00
        ayAcc00 = new Account(
                Name = 'office00'
                ,ParentId = ahAcc.Id
                ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                ,E_COMM_VALIDFLAG__c = '1'
                ,E_CL2PF_BRANCH__c = '1'    //追加
                ,E_CL2PF_ZBUSBR__c = '2'    //追加
                ,KSECTION__c = E_DownloadNoticeConst.KSECTION00         //追加
        );
        insert ayAcc00;
         // Account 事務所　課コード88
        ayAcc88 = new Account(
                Name = 'office88'
                ,ParentId = ahAcc.Id
                ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                ,E_COMM_VALIDFLAG__c = '1'
                ,E_CL2PF_BRANCH__c = '1'    //追加
                ,E_CL2PF_ZBUSBR__c = '2'    //追加
                ,KSECTION__c = E_DownloadNoticeConst.KSECTION88         //追加
        );
        insert ayAcc88;
         // Account 事務所 課コード99
        ayAcc99 = new Account(
                Name = 'office99'
                ,ParentId = ahAcc.Id
                ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                ,E_COMM_VALIDFLAG__c = '1'
                ,E_CL2PF_BRANCH__c = '1'    //追加
                ,E_CL2PF_ZBUSBR__c = '2'    //追加
                ,KSECTION__c = '99'         //追加
        );
        insert ayAcc99;
    }
    // Contact 募集人
    private static void createContact(String lastName, Account ayAcc){
        atCon = new Contact(LastName = lastName, AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
        atCon.E_CL3PF_AGNTNUM__c = 'ay01';
        atCon.email = lastName+'@terrasky.ingtesting';
        insert atCon;
    }
    

    private static void createData(Integer dataCnt, String shareUserId, Account ayAcc){
        system.runAs(thisuser){
            //保険契約ヘッダ（個人タイプ）
            E_Policy__c policy = new E_Policy__c();
            policy = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');

            // Share
            if(String.isNotBlank(shareUserId)){
                E_Policy__Share ps = new E_Policy__Share(
                                ParentId = policy.Id,
                                AccessLevel = 'read',
                                UserOrGroupId = shareUserId);
                insert ps;
            }

            //販売取扱者
            E_CHTPF__c chtpf = new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
            insert chtpf;

            //保険料請求予告通知
            List<E_BILA__c> bilas = new List<E_BILA__c>();
            //対象月
            //date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA);
            for(Integer i = 0; i < dataCnt; i++){
                //Date d = targetDate.addMonths(-2);
                E_BILA__c bila = new E_BILA__c(
                    ParentAccount__c = ahAcc.Id
                    , Account__c = ayAcc.Id
                    , AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c
                    , Contact__c = atCon.Id
                    , E_Policy__c = policy.Id
                    , E_CHTPF__c = chtpf.Id
                    , YYYY__c = 2017
                    , MM__c = 6
                    , OWNAME__c = 'Owner' + i
                    , ZJINTSEQ__c = '1'
                );
                bilas.add(bila);
            }
            insert bilas;
        }
    }


    /**
     * Profileマップ取得
     * @return Map<String, Id>: Map of ProfileName & ProfileID
     */
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @param E_Unit__c: 営業部
     * @param UnitName__c: 拠点名
     * @return User: ユーザ
     */
    public static User createUser(Boolean isInsert, String LastName, String profileDevName, String unit, String unitName, String memberNo){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , Unit__c = unit
                , UnitName__c = unitName
                , MemberNo__c = memberNo
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    /**
     * コミュニティユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @param E_Unit__c: 営業部
     * @param UnitName__c: 拠点名
     * @param Contact: 募集人
     * @return User: ユーザ
     */
    public static User createUser(Boolean isInsert, String LastName, String profileDevName, String unit, String unitName, Contact contact){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , Unit__c = unit
                , UnitName__c = unitName
                , ContactId = contact.Id
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    /**
     *　エリア作成
     * @param Name: エリア名
     * @return Area: エリア
     */
    public static E_Area__c createArea(String areaName, Decimal no){
        E_Area__c area = new E_Area__c(
                        Name = areaName
                        ,IsActive__c = '1'
                        ,SortNo__c = no
        );
        insert area;
        return area;
    }
    /**
     *　営業部作成
     * @param Name: 営業部名
     * @param E_Area__c: エリア
     * @return Unit: 営業部
     */
    public static E_Unit__c createUnit(String unitName, Id areaId, String branch){
        E_Unit__c unit = new E_Unit__c(
                        Name = unitName
                      , Area__c = areaId
                      , BRANCH__c = branch
                      , DisplayOrder__c = null
                      , IsActive__c = '1'
        );
        insert unit;
        return unit;
    }
}