@isTest
private with sharing class MNT_PamphletSearchControllerTest{
		private static testMethod void testPageMethods() {	
			MNT_PamphletSearchController page = new MNT_PamphletSearchController(new ApexPages.StandardController(new I_ContentMaster__c()));	
			page.getOperatorOptions_I_ContentMaster_c_Name();	
			page.getOperatorOptions_I_ContentMaster_c_ParentContent_c();	
			page.getOperatorOptions_I_ContentMaster_c_PamphletCategory_c();	
			page.getOperatorOptions_I_ContentMaster_c_FormNo_c();	
			page.getOperatorOptions_I_ContentMaster_c_ApprovalNo_c();	
			System.assert(true);
		}	
			
	private static testMethod void testPamphletResultTable() {
		MNT_PamphletSearchController.PamphletResultTable PamphletResultTable = new MNT_PamphletSearchController.PamphletResultTable(new List<I_ContentMaster__c>(), new List<MNT_PamphletSearchController.PamphletResultTableItem>(), new List<I_ContentMaster__c>(), null);
		PamphletResultTable.create(new I_ContentMaster__c());
		PamphletResultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
}