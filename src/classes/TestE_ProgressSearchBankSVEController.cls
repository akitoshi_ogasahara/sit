@isTest
private class TestE_ProgressSearchBankSVEController {

	private static testMethod void testPageMethods() {	
        E_ProgressSearchBankSVEController page = new E_ProgressSearchBankSVEController(new ApexPages.StandardController(new E_MPQPF__c()));	
        page.getOperatorOptions_E_MPQPF_c_ZBRCDE4X_c();	
        page.getOperatorOptions_E_MPQPF_c_STATDESC_c_multi();	
        System.assert(true);
    }	
	private static testMethod void testComponent2() {
        E_ProgressSearchBankSVEController.Component2 Component2 = new E_ProgressSearchBankSVEController.Component2(new List<E_MPQPF__c>(), new List<E_ProgressSearchBankSVEController.Component2Item>(), new List<E_MPQPF__c>(), null, null);
        Component2.create(new E_MPQPF__c());
        Component2.doDeleteSelectedItems();
        Component2.setPagesize(10);
        Component2.doFirst();
        Component2.doPrevious();
        Component2.doNext();
        Component2.doLast();
        Component2.doSort();
        System.assert(true);
    }
}