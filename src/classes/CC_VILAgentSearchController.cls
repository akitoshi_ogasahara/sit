/*
 * CC_VILAgentSearchController
 * Controller class for Interaction Launcher:募集人検索
 * created  : Accenture 2018/5/11
 * modified :
 */

 global with sharing class CC_VILAgentSearchController implements vlocity_ins.VlocityOpenInterface2{

	/**
	 * 概要 : This method is mandatory as we implements vlocity Open interface, this is the method by default called from Interaction launcher events
	 *
	 */
	global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);
		System.debug('inputs: ' + inputs);
		System.debug('outputs: ' + output);
		System.debug('options: ' + options);

		Boolean success = true;
		try{
			// Check the method if name is getSearchRequest then call getSearchRequest methods
			if(methodName.equalsIgnoreCase('getSearchRequest')){
				success = getSearchRequest(inputs, output, options);

			// Check the method if name is getSearchResults then call getSearchResults methods
			}else if(methodName.equalsIgnoreCase('getSearchResults')){
				success = getSearchResults(inputs, output, options);

			// launchConsole
			}else if(methodName.equalsIgnoreCase('launchConsole')){
				success = launchConsole(inputs, output, options);
			}

		}catch(Exception e){
			System.debug('Error invoke method: ' + methodName + ' with error: '+ e);
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}

	private static Boolean launchConsole(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		String interactionObj = (String) inputMap.get('interactionObjName');
		String interactionId = (String) inputMap.get('recordId');
		String interactionName = (String) inputMap.get('name');

		if( String.isNotBlank(interactionId)){
			outputMap.put('parent'+interactionObj+'Id', interactionId);
			outputMap.put(interactionObj+'Id', interactionId);
		}
		if( String.isNotBlank(interactionName)) {
			outputMap.put(interactionObj+'Name', interactionName);
		}

		return success;
	}

	/**
	 * 概要 : This method generates the SearchRequest for LaunchInteraction
	 */
	private Boolean getSearchRequest(Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options){

		vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest) inputs.get('lookupRequest');

		List<Map<String, Object>> previousSearchResults = request.previousSearchResults;
		Map<String, Object> additionalData = request.additionalData;

		//検索項目設定
		List<String> searchFieldList = request.searchFieldList;
		searchFieldList.add('isSearchAllStatus');   //業廃・仮業廃・未承認を含む
		searchFieldList.add('CL3PFAGNTNUM');   //募集人コード
		searchFieldList.add('CL3PFZEATKNAM');   //募集人カナ氏名
		searchFieldList.add('CLTPFDOB');   //生年月日

		//検索項目表示ラベル設定
		Map<String, Map<String, String>>labelTypeMap = request.searchFieldsLabelTypeMap;
		Map<String, String> newType = new Map<String, String> ();

		//業廃・仮業廃・未承認を含む
		newType.put('label','業廃・仮業廃・未承認を含む');
		newType.put('datatype','checkbox');
		labelTypeMap.put('isSearchAllStatus',newType);

		//募集人コード
		newType = new Map<String, String> ();
		newType.put('label','募集人コード');
		newType.put('datatype','text');
		labelTypeMap.put('CL3PFAGNTNUM',newType);

		//募集人カナ氏名
		newType = new Map<String, String> ();
		newType.put('label','募集人カナ氏名');
		newType.put('datatype','text');
		labelTypeMap.put('CL3PFZEATKNAM',newType);

		//生年月日
		newType = new Map<String, String> ();
		newType.put('label','生年月日');
		newType.put('datatype','text');
		labelTypeMap.put('CLTPFDOB',newType);

		output.put('lookupRequest', request);

		return true;
	}

	/**
	 * 概要 : Method performs search and evaluates search result, as well as verification result set
	 */
	private Boolean getSearchResults(Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options){

		// 検索結果項目設定
		List<String> resultFieldList = new List<String> ();
		resultFieldList.add('募集人名');
		resultFieldList.add('募集人カナ氏名');
		resultFieldList.add('募集人コード');
		resultFieldList.add('代理店名');
		resultFieldList.add('生年月日');
		resultFieldList.add('登録状況');

		Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>>();
		Map<String, String> rfType = new Map<String, String> ();

		rfType.put('label', '募集人名');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('募集人名', rfType);

		rfType = new Map<String, String> ();
		rfType.put('label', '募集人カナ氏名');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('募集人カナ氏名', rfType);

		rfType = new Map<String, String> ();
		rfType.put('label', '募集人カナ氏名');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('募集人カナ氏名', rfType);

		rfType = new Map<String, String> ();
		rfType.put('label', '募集人コード');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('募集人コード', rfType);

		rfType = new Map<String, String> ();
		rfType.put('label', '代理店名');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('代理店名', rfType);

		rfType = new Map<String, String> ();
		rfType.put('label', '生年月日');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('生年月日', rfType);

		rfType = new Map<String, String> ();
		rfType.put('label', '登録状況');
		rfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('登録状況', rfType);


		// 必須確認項目設定
		List<String> verificationFieldList = new List<String> ();
		verificationFieldList.add('募集人名');
		verificationFieldList.add('代理店名');

		Map<String, String> vfType = new Map<String, String> ();
		vfType.put('label', '募集人名');
		vfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('募集人名', vfType);

		vfType = new Map<String, String> ();
		vfType.put('label', '代理店名');
		vfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('代理店名', vfType);


		// オプション確認項目設定
		List<String> optionalVerificationFieldList = new List<String> ();
		optionalVerificationFieldList.add('募集人コード');
		optionalVerificationFieldList.add('生年月日');

		Map<String, String> opVfType = new Map<String, String> ();
		opVfType.put('label', '募集人コード');
		opVfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('募集人コード', opVfType);

		opVfType = new Map<String, String> ();
		opVfType.put('label', '生年月日');
		opVfType.put('datatype','text');
		resultFieldsLabelTypeMap.put('生年月日', opVfType);


		// 検索結果用変数
		Map<String, Object> resultValueMap = new Map<String, Object>();
		List<vlocity_ins.LookupResult> results = new List<vlocity_ins.LookupResult> ();
		vlocity_ins.LookupResult result;
		String recordId;
		Map<String, Object> verificationResult = new Map<String, Object>();

		Map<String, Object> resultInfo = (Map<String, Object>)inputs.get('resultInfo');
		String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
		String drBundleName = (String) resultInfo.get('drBundleName');
		String interactionObjName = (String) resultInfo.get('interactionObjName');

		vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest) inputs.get('lookupRequest');
		Map<String, Object> searchValueMap = request.searchValueMap;
		Boolean isSearchAllStatus = searchValueMap.get('isSearchAllStatus') != NULL ? (Boolean)searchValueMap.get('isSearchAllStatus') : False;  //業廃・仮業廃・未承認を含む
		String CL3PFAGNTNUM = (String)searchValueMap.get('CL3PFAGNTNUM');    //募集人コード
		String CL3PFZEATKNAM = (String)searchValueMap.get('CL3PFZEATKNAM');   //募集人カナ氏名
		//生年月日
		String CLTPFDOB = (String)searchValueMap.get('CLTPFDOB');
		if(CLTPFDOB!= null && CLTPFDOB != ''){
			CLTPFDOB = CLTPFDOB.removeStart('*');
			CLTPFDOB = CLTPFDOB.removeEnd('*');
			if(!(CLTPFDOB.isNumeric() && CLTPFDOB.length() == 8)) CLTPFDOB = CC_VILUtils.birthdateProcess(CLTPFDOB);
		}

		String query = 'SELECT Id, Name, E_CL3PF_ZEATKNAM__c, E_CL3PF_AGNTNUM__c, CMN_SYSAgencyName__c, CMN_SYSBirthdate__c, CMN_RegistrationStatus__c, E_CLTPF_DOB__c, CMN_BirthdateJP__c';
		query += ' FROM Contact';
		query += ' WHERE Recordtype.Name = \'募集人\' AND CMN_isPolicyRelatedAgent__c = true ';
		if(CL3PFAGNTNUM != null && CL3PFAGNTNUM != ''){
			CL3PFAGNTNUM = CL3PFAGNTNUM.removeStart('*');
			CL3PFAGNTNUM = CL3PFAGNTNUM.removeEnd('*');
			CL3PFAGNTNUM = '%' + CL3PFAGNTNUM + '%';
			query += ' AND E_CL3PF_AGNTNUM__c like :CL3PFAGNTNUM';
		}
		if(CL3PFZEATKNAM != null && CL3PFZEATKNAM != ''){
			CL3PFZEATKNAM = CC_VILUtils.getSeionKana(CL3PFZEATKNAM);
			CL3PFZEATKNAM = CL3PFZEATKNAM.removeStart('*');
			CL3PFZEATKNAM = CL3PFZEATKNAM.removeEnd('*');
			CL3PFZEATKNAM = '%' + CL3PFZEATKNAM + '%';
			query += ' AND CC_AgentNameSeionKana__c like :CL3PFZEATKNAM';
		}
		if(CLTPFDOB != null && CLTPFDOB != ''){
			if(CLTPFDOB.length() == 4){
				CLTPFDOB = '%' + CLTPFDOB;
				query += ' AND CMN_Birthdate__c like :CLTPFDOB ';
			}else{
				query += ' AND CMN_Birthdate__c = :CLTPFDOB ';
			}
		}
		if(!isSearchAllStatus){
			query += ' AND CMN_RegistrationStatus__c = \'承認済\'';
		}
		query += ' ORDER BY Name';

		List<Contact> contactList = Database.query(query);

		for(Contact c : contactList){
			// 検索結果出力
			resultValueMap = new Map<String, Object>();
			resultValueMap.put('募集人名', c.Name);
			resultValueMap.put('募集人カナ氏名', c.E_CL3PF_ZEATKNAM__c);
			resultValueMap.put('募集人コード', c.E_CL3PF_AGNTNUM__c);
			resultValueMap.put('代理店名', c.CMN_SYSAgencyName__c);
			resultValueMap.put('生年月日', c.CMN_SYSBirthdate__c);
			resultValueMap.put('登録状況', c.CMN_RegistrationStatus__c);
			recordId = c.Id;

			result = new vlocity_ins.LookupResult(resultFieldsLabelTypeMap, resultFieldList, resultValueMap,
												  recordId, uniqueRequestName, request,
												  verificationResult, verificationFieldList, drBundleName, interactionObjName);

			result.optionalVerificationFieldList = optionalVerificationFieldList;
			result.numOfOptionalVerificationFields = 1;

			results.add(result);

			System.debug(results);
		}

		output.put('lookupResults', results);

		return true;
	}

}