public with sharing class I_CMS_Thumbnail_MovieController extends E_AbstractController{

	public Boolean companyLimitedFlag{get;set;}

	public Integer movieTitle{get;set;}

	public Integer movieDescription{get;set;}
	
	public I_CMS_Thumbnail_MovieController() {
		//社員ユーザ判別フラグ
		companyLimitedFlag = access.isEmployee();

		movieTitle = I_Const.NN_TUBE_MOVIE_LENGTH_TITLE;

		movieDescription = I_Const.NN_TUBE_MOVIE_LENGTH_DESCRIPTION;
	}
}