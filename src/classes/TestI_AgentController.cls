@isTest
private class TestI_AgentController {
	// MR - 募集人一覧表示 - 初期処理
	static testMethod void pageActionMRTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;
		Contact testAgent = new Contact(LastName = 'testAgent', AccountId = testAgency.Id);
		insert testAgent;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
				acon.officeid = testAgency.id;
//				acon.pageAction();
				acon.readyAction();
			Test.stopTest();

			//System.assertEquals(acon.accid, testAgency.Id);
//			System.assert(acon.pageMessages.hasMessages());
		}
	}

	// MR - パラメータなしリダイレクト - 初期処理
	static testMethod void pageActionRedirectTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人レコード作成
		Contact agent = new Contact(LastName = 'testAgent');
		insert agent;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.readyAction();
			Test.stopTest();

			PageReference redirectPage = Page.IRIS_Agency;
//			System.assertEquals(acon.pageAction().getUrl(), redirectPage.getUrl());
		}
	}

	static testMethod void pageActionAYTest() {
		// 代理店実行ユーザ作成（AY）
		// 1. 取引先（格）を作成
		Account accParent = TestI_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'fstest';
		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		E_CookieHandler.setCookieCompliance();

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
				acon.officeid = accAgency.id;
//				acon.pageAction();
				acon.readyAction();
			Test.stopTest();

			//System.assertEquals(acon.accid, accAgency.Id);
//			System.assert(acon.pageMessages.hasMessages());
		}
	}

	static testMethod void pageActionNoAuth() {
		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		//ID管理作成
		E_IDCPF__c src = new E_IDCPF__c(User__c = UserInfo.getUserId(),OwnerId = UserInfo.getUserId(),AppMode__c='NNLink');
		insert src;

		Test.startTest();
			I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.readyAction();
		Test.stopTest();

		PageReference redirectPage = Page.E_AppSwitch;
		redirectPage.getParameters().put(I_Const.APPSWITCH_URL_PARAM_APP, I_Const.APP_MODE_NNLINK);
//		System.assertEquals(acon.pageAction().getUrl(), redirectPage.getUrl());
	}

	static testMethod void sortRowsTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人リスト作成
		// 1.Account作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		// 2.Contact作成
		List<Contact> testAgentList = new List<Contact>();
		testAgentList.add(new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー01',E_CL3PF_AGNTNUM__c='ZT001'));
		testAgentList.add(new Contact(LastName = 'TestAgent02', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー02',E_CL3PF_AGNTNUM__c='ZT002'));
		testAgentList.add(new Contact(LastName = 'TestAgent03', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー03',E_CL3PF_AGNTNUM__c='ZT003'));
		testAgentList.add(new Contact(LastName = 'TestAgent04', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー04',E_CL3PF_AGNTNUM__c='ZT004'));
		testAgentList.add(new Contact(LastName = 'TestAgent05', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー05',E_CL3PF_AGNTNUM__c='ZT005'));
		insert testAgentList;
		testAgentList.sort();

		// 3.User作成
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
		List<User> testUserList = new List<User>();
		for (Integer i = 0, j = testAgentList.size(); i < j; i++) {
			userName = 'testUser' + i + '@terrasky.ingtesting';
			testUserList.add(new User(Lastname = 'testUser' + i
									, Username = userName
									, Email = userName
									, ProfileId = p.Id
									, Alias = 'tUser' + i
									, TimeZoneSidKey = UserInfo.getTimeZone().getID()
									, LocaleSidKey = UserInfo.getLocale()
									, EmailEncodingKey = 'UTF-8'
									, LanguageLocaleKey = UserInfo.getLanguage()
									, ContactId = testAgentList[i].Id)
									);
		}
		insert testUserList;

		// 4.E_IDRequest作成
		List<E_IDCPF__c> testIDcpf = new List<E_IDCPF__c>();
		for (Integer i = 0, j = testUserList.size(); i < j; i++) {
			testIDcpf.add(new E_IDCPF__c(User__c = testUserList[i].Id, ZSTATUS01__c = '1'));
		}
		insert testIDcpf;
		List<E_IDRequest__c> testIDRList = new List<E_IDRequest__c>();
		for (Integer i = 0, j = testIDcpf.size(); i < j; i++) {
			testIDRList.add(new E_IDRequest__c(E_IDCPF__c = testIDcpf[i].Id, ZIDOWNER__c = 'AG', ZIDTYPE__c = 'AT', ZINQUIRR__c = 'L3', ZWEBID__c = '50', ZUPDFLAG__c = 'I'));
		}
		insert testIDRList;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);
		pr.getParameters().put('st', I_AgentController.SORT_TYPE_AGENT);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
				acon.sortRows();

				pr.getParameters().put('st', I_AgentController.SORT_TYPE_CODE);
				acon.sortRows();

				pr.getParameters().put('st', I_AgentController.SORT_TYPE_IDSTATUS);
				acon.sortRows();

			Test.stopTest();
/* UX2 リリースのため 2017/06/21
				System.assertEquals(acon.agentRows[0].name, testAgentList[4].LastName);
				System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_AGENT);
				System.assertEquals(acon.sortIsAsc, false);

				pr.getParameters().put('st', I_AgentController.SORT_TYPE_CODE);
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].code, testAgentList[0].E_CL3PF_AGNTNUM__c);
				System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_CODE);
				System.assertEquals(acon.sortIsAsc, true);

				pr.getParameters().put('st', I_AgentController.SORT_TYPE_IDSTATUS);
				acon.sortRows();

//				System.assertEquals(acon.agentRows[0].status, I_Const.NNLINKID_STATUS_ACTIVE);
				System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_IDSTATUS);
				System.assertEquals(acon.sortIsAsc, true);
			Test.stopTest();

			System.assertEquals(acon.getAgentCount(), 5);
			System.assertEquals(acon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
*/
		}
	}

	static testMethod void sortRowsTest2() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人リスト作成
		// 1.Account作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		// 2.Contact作成
		List<Contact> testAgentList = new List<Contact>();
		testAgentList.add(new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー01',E_CL3PF_AGNTNUM__c='ZT001'));
		testAgentList.add(new Contact(LastName = 'TestAgent02', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー02',E_CL3PF_AGNTNUM__c='ZT002'));
		testAgentList.add(new Contact(LastName = 'TestAgent03', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー03',E_CL3PF_AGNTNUM__c='ZT003'));
		testAgentList.add(new Contact(LastName = 'TestAgent04', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー04',E_CL3PF_AGNTNUM__c='ZT004'));
		testAgentList.add(new Contact(LastName = 'TestAgent05', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー05',E_CL3PF_AGNTNUM__c='ZT005'));
		insert testAgentList;
		testAgentList.sort();

		Datetime bizDate = Date.newInstance(2017, 1, 10);
		List<E_AgencySalesResults__c> testSRList = new List<E_AgencySalesResults__c>();
			testSRList.add(new E_AgencySalesResults__c(BusinessDate__c=bizDate.format('yyyyMMdd'), Hierarchy__c='AT', Agent__c=testAgentList[0].Id, NBCANP_YTD__c=1000, NBCANP_SpInsTypeYTD__c=1000, IANP__c=1000, IQA__c=0.2
				, NBWANP_TotalYTD__c=1000, NBWANP_FSYTD__c=1000, NBWANP_ProtectionYTD__c=1000, DefactRate__c=20, ActiveAgent__c=0.2, ActivePolicyNumber__c=1000));
			testSRList.add(new E_AgencySalesResults__c(BusinessDate__c=bizDate.format('yyyyMMdd'), Hierarchy__c='AT', Agent__c=testAgentList[1].Id, NBCANP_YTD__c=1000, NBCANP_SpInsTypeYTD__c=0, IANP__c=1000, IQA__c=20
				, NBWANP_TotalYTD__c=1000, NBWANP_FSYTD__c=1000, NBWANP_ProtectionYTD__c=1000, DefactRate__c=0.2, ActiveAgent__c=20, ActivePolicyNumber__c=10));
			testSRList.add(new E_AgencySalesResults__c(BusinessDate__c=bizDate.format('yyyyMMdd'), Hierarchy__c='AT', Agent__c=testAgentList[2].Id, NBCANP_YTD__c=1000, NBCANP_SpInsTypeYTD__c=1000, IANP__c=0, IQA__c=20
				, NBWANP_TotalYTD__c=1000, NBWANP_FSYTD__c=1000, NBWANP_ProtectionYTD__c=0, DefactRate__c=20, ActiveAgent__c=20, ActivePolicyNumber__c=1000));
			testSRList.add(new E_AgencySalesResults__c(BusinessDate__c=bizDate.format('yyyyMMdd'), Hierarchy__c='AT', Agent__c=testAgentList[3].Id, NBCANP_YTD__c=0, NBCANP_SpInsTypeYTD__c=1000, IANP__c=1000, IQA__c=20
				, NBWANP_TotalYTD__c=1000, NBWANP_FSYTD__c=0, NBWANP_ProtectionYTD__c=1000, DefactRate__c=20, ActiveAgent__c=20, ActivePolicyNumber__c=1000));
			testSRList.add(new E_AgencySalesResults__c(BusinessDate__c=bizDate.format('yyyyMMdd'), Hierarchy__c='AT', Agent__c=testAgentList[4].Id, NBCANP_YTD__c=1000, NBCANP_SpInsTypeYTD__c=1000, IANP__c=1000, IQA__c=20
				, NBWANP_TotalYTD__c=0, NBWANP_FSYTD__c=1000, NBWANP_ProtectionYTD__c=1000, DefactRate__c=20, ActiveAgent__c=20, ActivePolicyNumber__c=1000));
		insert testSRList;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = bizDate.date());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);
		pr.getParameters().put('st', I_AgentController.SORT_TYPE_AGENT);
		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
				acon.sortRows();

				pr.getParameters().put('st', 'usageHistory');
				acon.sortRows();

				pr.getParameters().put('st', 'nbcnp_ytd');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent04');

				pr.getParameters().put('st', 'nbcanp_spytd');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent02');

				pr.getParameters().put('st', 'ianp');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent03');

				pr.getParameters().put('st', 'iqa');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent01');

				pr.getParameters().put('st', 'totalYTD');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent05');

				pr.getParameters().put('st', 'fsYTD');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent04');

				pr.getParameters().put('st', 'proYTD');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].name, 'TestAgent03');

				pr.getParameters().put('st', 'agentCount');
				acon.sortRows();
				System.assertEquals(acon.agentRows[0].Name, 'TestAgent02');

				System.assertEquals(acon.bizDate, '2017/01/10');
			Test.stopTest();
		}

	}

	static testMethod void notActiveContactTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人リスト作成
		// 1.Account作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		// 2.Contact作成
		List<Contact> testAgentList = new List<Contact>();
		testAgentList.add(new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー01', E_CL3PF_AGNTNUM__c='ZT001'));
		testAgentList.add(new Contact(LastName = 'TestAgent02', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー02', E_CL3PF_AGNTNUM__c='ZT002'));
		insert testAgentList;
		testAgentList.sort();

		// 3.User作成
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
		String userName = 'testUser1@terrasky.ingtesting';
		User testUser = new User(Lastname = 'testUser'
									, Username = userName
									, Email = userName
									, ProfileId = p.Id
									, Alias = 'tUser'
									, TimeZoneSidKey = UserInfo.getTimeZone().getID()
									, LocaleSidKey = UserInfo.getLocale()
									, EmailEncodingKey = 'UTF-8'
									, LanguageLocaleKey = UserInfo.getLanguage()
									, ContactId = testAgentList[0].Id);
		insert testUser;

		// 4.E_IDRequest作成
		E_IDCPF__c testIDcpf = new E_IDCPF__c(User__c = testUser.Id, ZSTATUS01__c = '1');
		insert testIDcpf;
		E_IDRequest__c testIDR = new E_IDRequest__c(E_IDCPF__c = testIDcpf.Id
													, ZIDOWNER__c = 'AG'
													, ZIDTYPE__c = 'AT'
													, ZINQUIRR__c = 'L3'
													, ZWEBID__c = '50'
													, ZUPDFLAG__c = 'D');
		insert testIDR;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);
		pr.getParameters().put('st', I_AgentController.SORT_TYPE_IDSTATUS);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
			Test.stopTest();
/* UX2 リリースのため 2017/06/21
System.debug('notActiveContactTest:'+acon.agentRows);
			System.assertEquals(acon.agentRows[0].name, testAgentList[0].LastName);
//			System.assertEquals(acon.agentRows[0].status, I_Const.NNLINKID_STATUS_ACTIVE);
			System.assertEquals(acon.agentRows[1].name, testAgentList[1].LastName);
			System.assertEquals(acon.agentRows[1].status, I_Const.NNLINKID_STATUS_NOPOLICY);
			System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_AGENT);
			System.assertEquals(acon.sortIsAsc, true);
*/
		}
	}

	static testMethod void agentKeyPipeContactTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人リスト作成
		// 1.Account作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		// 2.Contact作成
		List<Contact> testAgentList = new List<Contact>();
		testAgentList.add(new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー01',E_CL3PF_AGNTNUM__c='ZT001'));
		testAgentList.add(new Contact(LastName = 'TestAgent02', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー02',E_CL3PF_AGNTNUM__c='ZT001|1'));
		testAgentList.add(new Contact(LastName = 'TestAgent03', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー03',E_CL3PF_AGNTNUM__c='ZT001|2'));
		testAgentList.add(new Contact(LastName = 'TestAgent04', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー04',E_CL3PF_AGNTNUM__c='ZT001|3'));
		testAgentList.add(new Contact(LastName = 'TestAgent04', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー05',E_CL3PF_AGNTNUM__c='ZT001|4'));
		insert testAgentList;
		testAgentList.sort();

		// 3.User作成
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
		List<User> testUserList = new List<User>();
		for (Integer i = 0, j = testAgentList.size(); i < j; i++) {
			userName = 'testUser' + i + '@terrasky.ingtesting';
			testUserList.add(new User(Lastname = 'testUser' + i
									, Username = userName
									, Email = userName
									, ProfileId = p.Id
									, Alias = 'tUser' + i
									, TimeZoneSidKey = UserInfo.getTimeZone().getID()
									, LocaleSidKey = UserInfo.getLocale()
									, EmailEncodingKey = 'UTF-8'
									, LanguageLocaleKey = UserInfo.getLanguage()
									, ContactId = testAgentList[i].Id)
									);
		}
		insert testUserList;

		// 4.E_IDRequest作成
		List<E_IDCPF__c> testIDcpf = new List<E_IDCPF__c>();
		for (Integer i = 0, j = testUserList.size(); i < j; i++) {
			testIDcpf.add(new E_IDCPF__c(User__c = testUserList[i].Id, ZSTATUS01__c = '1'));
		}
		insert testIDcpf;
		List<E_IDRequest__c> testIDRList = new List<E_IDRequest__c>();
		for (Integer i = 0, j = testIDcpf.size(); i < j; i++) {
			testIDRList.add(new E_IDRequest__c(E_IDCPF__c = testIDcpf[i].Id, ZIDOWNER__c = 'AG', ZIDTYPE__c = 'AT', ZINQUIRR__c = 'L3', ZWEBID__c = '50', ZUPDFLAG__c = 'D'));
		}
		testIDRList[4].ZUPDFLAG__c = 'I';
		testIDRList.remove(2);
		insert testIDRList;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);
		pr.getParameters().put('st', I_AgentController.SORT_TYPE_AGENT);

		//Date startDate_Of_lastLogin = I_Const.IRIS_GOLIVE_DAY.addDays(Integer.valueOf(System.Label.E_LASTLOGINDATE_LAPSEDDAYS));

		//E_Util.systemToday_forTest = startDate_Of_lastLogin.addDays(-1);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
			Test.stopTest();

/* UX2 リリースのため 2017/06/21
			System.assertEquals(acon.agentRows[0].name, testAgentList[0].LastName);
			System.assertEquals(acon.agentRows[0].code, testAgentList[2].E_CL3PF_AGNTNUM__c.substringBefore('|'));
//			System.assertEquals(acon.agentRows[0].status, I_Const.NNLINKID_STATUS_ACTIVE);

			System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_AGENT);
			System.assertEquals(acon.getAgentCount(), 5);
*/
		}
	}

	static testMethod void overRowsLimitTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		//メッセージマスタ作成
		createMessageData('LIS|004', '件数が1000件を超えているため、全てのデータを表示することが出来ません。<br/>お手数ですが、情報をお知りになりたい場合は、当社サービスセンターまでお問い合せ下さい。<br/>');

		// テスト用募集人リスト作成
		// 1.Account作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		// 2.Contact作成
		List<Contact> testAgentList = new List<Contact>();
		for(Integer i=0; i<I_Const.LIST_MAX_ROWS+1; i++){
			testAgentList.add(new Contact(LastName = 'TestAgent'+i, AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー'+i,E_CL3PF_AGNTNUM__c='Z'+i));
		}
		insert testAgentList;
		testAgentList.sort();
/*
		// 3.User作成
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
		List<User> testUserList = new List<User>();
		for (Integer i = 0, j = testAgentList.size(); i < j; i++) {
			userName = 'testUser' + i + '@terrasky.ingtesting';
			testUserList.add(new User(Lastname = 'testUser' + i
									, Username = userName
									, Email = userName
									, ProfileId = p.Id
									, Alias = 'tUser' + i
									, TimeZoneSidKey = UserInfo.getTimeZone().getID()
									, LocaleSidKey = UserInfo.getLocale()
									, EmailEncodingKey = 'UTF-8'
									, LanguageLocaleKey = UserInfo.getLanguage()
									, ContactId = testAgentList[i].Id)
									);
		}
		insert testUserList;

		// 4.E_IDRequest作成
		List<E_IDCPF__c> testIDcpf = new List<E_IDCPF__c>();
		for (Integer i = 0, j = testUserList.size(); i < j; i++) {
			testIDcpf.add(new E_IDCPF__c(User__c = testUserList[i].Id, ZSTATUS01__c = '1'));
		}
		insert testIDcpf;
		List<E_IDRequest__c> testIDRList = new List<E_IDRequest__c>();
		for (Integer i = 0, j = testIDcpf.size(); i < j; i++) {
			testIDRList.add(new E_IDRequest__c(E_IDCPF__c = testIDcpf[i].Id, ZIDOWNER__c = 'AG', ZIDTYPE__c = 'AT', ZINQUIRR__c = 'L3', ZWEBID__c = '50', ZUPDFLAG__c = 'I'));
		}
		insert testIDRList;
*/

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);
		pr.getParameters().put('st', I_AgentController.SORT_TYPE_AGENT);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS+1));

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
			Test.stopTest();

/*			System.assertEquals(acon.agentRows[0].name, testAgentList[0].LastName);
			System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_AGENT);
			System.assertEquals(acon.sortIsAsc, true);
			System.assertEquals(acon.getAgentCount(), I_Const.LIST_MAX_ROWS);
			System.assertEquals(acon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
			System.assertEquals(acon.pageMessages.getMessages()[0].summary,'件数が1000件を超えているため、全てのデータを表示することが出来ません。<br/>お手数ですが、情報をお知りになりたい場合は、当社サービスセンターまでお問い合せ下さい。<br/>');
*/
		}
	}

	static testMethod void agentUserTest() {

		// テスト用募集人リスト作成
		// 1.Account作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		// 実行ユーザ作成（代理店ユーザ）
		Contact actCon = TestI_TestUtil.createContact(false, testAgency.Id, 'fstast');
		actCon.E_CL3PF_VALIDFLAG__c = '1';
		actCon.E_CL3PF_AGNTNUM__c = 'ZT000';
		insert actCon;
		User actUser = TestI_TestUtil.createAgentUser(true, 'fstest', 'E_PartnerCommunity_IA', actCon.Id);
		TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		// 2.Contact作成
		List<Contact> testAgentList = new List<Contact>();
		testAgentList.add(new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー01',E_CL3PF_AGNTNUM__c='ZT001'));
		testAgentList.add(new Contact(LastName = 'TestAgent02', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー02',E_CL3PF_AGNTNUM__c='ZT001|1'));
		testAgentList.add(new Contact(LastName = 'TestAgent03', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー03',E_CL3PF_AGNTNUM__c='ZT001|2'));
		testAgentList.add(new Contact(LastName = 'TestAgent04', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー04',E_CL3PF_AGNTNUM__c='ZT001|3'));
		testAgentList.add(new Contact(LastName = 'TestAgent04', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー05',E_CL3PF_AGNTNUM__c='ZT001|4'));
		insert testAgentList;
		testAgentList.sort();

		// 3.User作成
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
		List<User> testUserList = new List<User>();
		for (Integer i = 0, j = testAgentList.size(); i < j; i++) {
			userName = 'testUser' + i + '@terrasky.ingtesting';
			testUserList.add(new User(Lastname = 'testUser' + i
									, Username = userName
									, Email = userName
									, ProfileId = p.Id
									, Alias = 'tUser' + i
									, TimeZoneSidKey = UserInfo.getTimeZone().getID()
									, LocaleSidKey = UserInfo.getLocale()
									, EmailEncodingKey = 'UTF-8'
									, LanguageLocaleKey = UserInfo.getLanguage()
									, ContactId = testAgentList[i].Id)
									);
		}
		insert testUserList;

		// 4.E_IDRequest作成
		List<E_IDCPF__c> testIDcpf = new List<E_IDCPF__c>();
		for (Integer i = 0, j = testUserList.size(); i < j; i++) {
			testIDcpf.add(new E_IDCPF__c(User__c = testUserList[i].Id, ZSTATUS01__c = '1'));
		}
		insert testIDcpf;
		List<E_IDRequest__c> testIDRList = new List<E_IDRequest__c>();
		for (Integer i = 0, j = testIDcpf.size(); i < j; i++) {
			testIDRList.add(new E_IDRequest__c(E_IDCPF__c = testIDcpf[i].Id, ZIDOWNER__c = 'AG', ZIDTYPE__c = 'AT', ZINQUIRR__c = 'L3', ZWEBID__c = '50', ZUPDFLAG__c = 'D'));
		}
		testIDRList[4].ZUPDFLAG__c = 'I';
		testIDRList.remove(2);
		insert testIDRList;

		//JIRA #251
		createLogs(testUserList[0]);

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);
		pr.getParameters().put('st', I_AgentController.SORT_TYPE_AGENT);
		pr.getParameters().put('agent', 'ZT00');

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
			Test.stopTest();
/* UX2 リリースのため 2017/06/21
			System.assertEquals(acon.agentRows[0].name, testAgentList[0].LastName);
			System.assertEquals(acon.agentRows[0].code, testAgentList[2].E_CL3PF_AGNTNUM__c.substringBefore('|'));
//			System.assertEquals(acon.agentRows[0].status, I_Const.NNLINKID_STATUS_ACTIVE);
			System.assertEquals(acon.agentRows[1].name,  testAgentList[1].LastName);
			System.assertEquals(acon.agentRows[1].code, testAgentList[1].E_CL3PF_AGNTNUM__c.substringBefore('|'));
			//System.assertEquals(acon.agentRows[1].status, I_Const.NNLINKID_STATUS_ACTIVE);
			//0623変更
			System.assertEquals(acon.agentRows[1].status, I_Const.NNLINKID_STATUS_PWCHANGE);
			System.assertEquals(acon.sortType, I_AgentController.SORT_TYPE_AGENT);
			System.assertEquals(acon.getAgentCount(), 6);

			System.assert(acon.agentRows[0].haveLogs);
			System.assertEquals(acon.agentRows[0].usagehistory,'有り');
			System.assert(!acon.agentRows[1].haveLogs);
			System.assertEquals(acon.agentRows[1].usagehistory,'無し');
*/
		}
	}

	static testMethod void moveToOwnerListTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// test用事務所レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		Contact testAgent = new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1');
		insert testAgent;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		//irisメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('id', testAgent.Id);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.readyAction();
				acon.moveToOwnerList();
			Test.stopTest();

			PageReference ownerPage = Page.IRIS_Owner;
			System.assertEquals(acon.moveToOwnerList().getUrl(), ownerPage.getUrl() + '?irismn=' + mn.Id);
		}
	}

	static testMethod void moveToNewPolicyListTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// test用事務所レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		Contact testAgent = new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1');
		insert testAgent;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		//irisメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('id', testAgent.Id);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.readyAction();
				acon.moveToNewPolicyList();
			Test.stopTest();

			PageReference ownerPage = Page.IRIS_NewPolicy;
			System.assertEquals(acon.moveToNewPolicyList().getUrl(), ownerPage.getUrl());
		}
	}

	static testMethod void moveToLogViewTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// test用事務所レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		Contact testAgent = new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1');
		insert testAgent;

		Profile p = [SELECT Id FROM Profile WHERE Name = 'Partner Community User' LIMIT 1];
		User testUser = new User(Lastname = 'testUser'
									, Username = 'testUser@terrasky.ingtesting'
									, Email = 'testUser@terrasky.ingtesting'
									, ProfileId = p.Id
									, Alias = 'tUser'
									, TimeZoneSidKey = UserInfo.getTimeZone().getID()
									, LocaleSidKey = UserInfo.getLocale()
									, EmailEncodingKey = 'UTF-8'
									, LanguageLocaleKey = UserInfo.getLanguage()
									, ContactId = testAgent.Id);
		insert testUser;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		//irisメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('id', testAgent.Id);
		pr.getParameters().put('userId', testUser.Id);

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.readyAction();
				acon.moveToLogView();
			Test.stopTest();

			PageReference ownerPage = Page.IRIS_LogView;
			ownerPage.getParameters().put('id', testUser.Id);
			System.assertEquals(acon.moveToLogView().getUrl(), ownerPage.getUrl());
		}
	}


	/**
	 * 表示リストテスト
	 */
	static testMethod void listRowsTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// test用事務所レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		Contact testAgent = new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1');
		insert testAgent;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		//irisメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		//pr.getParameters().put('id', testAgent.Id);

		//結果
		PageReference pref;
		Integer rowsBefore;
		Integer rowsAfter;

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgentController acon = new I_AgentController();
//				acon.pageAction();
				acon.officeid = testAgency.id;
				acon.readyAction();
				rowsBefore = acon.rowCount;
				pref = acon.addRows();
				rowsAfter = acon.getTotalRows();
			Test.stopTest();

			System.assertEquals(pref, null);
			System.assertEquals(rowsBefore + I_Const.LIST_ADD_MORE_ADD_ROWS, acon.rowCount);
			System.assertEquals(rowsAfter, 0);
		}
	}
	// テスト
	static testMethod void forCoverageTest(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// test用事務所レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;

		Contact testAgent = new Contact(LastName = 'TestAgent01', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1');
		insert testAgent;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now());

		//irisメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		//pr.getParameters().put('id', testAgent.Id);

		//結果
		PageReference pref;
		Integer rowsBefore;
		Integer rowsAfter;

		System.runAs(actUser){
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);
			I_AgentController acon = new I_AgentController();

			Test.startTest();
				acon.getAgentCount();
				acon.getListMaxRows();

				pr.getParameters().put('st','agent');
				acon.sortIsAsc = true;
				acon.sortRows();
				pr.getParameters().put('st','code');
				acon.sortIsAsc = true;
				acon.sortRows();
				pr.getParameters().put('st','idStatus');
				acon.sortIsAsc = true;
				acon.sortRows();
				pr.getParameters().put('st','usageHistory');
				acon.sortIsAsc = true;
				acon.sortRows();

				acon.redirectAgencyPage();

				pr.getParameters().put('srTab','ag');
				acon.selectTab();
			Test.stopTest();

			System.assertEquals(acon.srTab, 'ag');
			System.assertEquals(acon.viewSR, false);
		}
	}

	// MR - 募集人一覧表示 - 初期処理
	static testMethod void getSRTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;
		Contact testAgent = new Contact(LastName = 'TestAgent', AccountId = testAgency.Id, E_CL3PF_VALIDFLAG__c = '1',E_CL3PF_ZEATKNAM__c='テストエージェンシー',E_CL3PF_AGNTNUM__c='Z');
		insert testAgent;
		E_AgencySalesResults__c sr = new E_AgencySalesResults__c(BusinessDate__c=System.now().format('yyyyMMdd'), Hierarchy__c='AT', Agent__c=testAgent.Id, NBCANP_YTD__c=100);
		insert sr;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = System.today());

		// ページ表示
		PageReference pr = Page.IRIS_Agent;
		Test.setCurrentPage(pr);
		pr.getParameters().put('accid', testAgency.Id);

		System.runAs(actUser){
			// 権限セットを付与
			Id permAgnyId = [Select Id FROM PermissionSet Where Name = 'I_PermissionSet_SREmployee'].Id;
			PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = actUser.Id, PermissionSetId = permAgnyId);
			insert src;

			Test.startTest();
				I_AgentController acon = new I_AgentController();
				acon.officeid = testAgency.id;
//				acon.pageAction();
				acon.readyAction();
			Test.stopTest();

			//System.assertEquals(acon.accid, testAgency.Id);
//			System.assert(acon.pageMessages.hasMessages());
			System.assertEquals(acon.viewSR, true);
		}
	}

	//irisメニューー
	static I_MenuMaster__c createIRISMenu(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		I_MenuMaster__c mn = new I_MenuMaster__c();
		system.runAs(thisUser){
			mn.MenuKey__c = 'ownerList';
			mn.MainCategory__c = '2.保有契約';
			insert mn;
		}
		return mn;
	}

	//メッセージマスタ
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//NNlinkLog 作成
	static void createLogs(User us){
		E_log__c log = new E_log__c();
		log.Name = 'お知らせ';
		log.isDisplay__c = true;
		log.AccessUser__c = us.id;
		log.ActionType__c = 'お知らせ';
		log.AccessDateTime__c = DateTime.now();
		insert log;
	}
}