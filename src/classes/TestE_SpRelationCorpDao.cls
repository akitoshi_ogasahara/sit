@isTest
private class TestE_SpRelationCorpDao {
	
	private static Account distribute;
	private static E_AgencySalesResults__c asr;
	private static E_SpRelationCorp__c spRelationCorps;

	@isTest static void getCorpRecByIdTest() {

		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String BUSINESS_DATE = '20000101';

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH',
			QualifSim__c = '優績S'
		);
		insert asr;

		// 特定関係法人
		spRelationCorps = new E_SpRelationCorp__c(
			SpRelationCorpName__c='テスト特定関係法人',
			E_AgencySalesResults__c=asr.Id,
			BusinessDate__c = BUSINESS_DATE
		);
		insert spRelationCorps;

		String agencyId = distribute.Id;

		// テスト
		Test.startTest();
		List<E_SpRelationCorp__c> rec = E_SpRelationCorpDao.getCorpRecById(agencyId);
		System.assertEquals(spRelationCorps.Id, rec.get(0).Id);
		Test.stopTest();
	}
	
}