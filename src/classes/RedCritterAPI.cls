global class RedCritterAPI{
   //AddAction Variation 1
   @Future(callout=true) global static void addAction(string secretKey, string profileEntity, string actionName, double value){
        
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedActionName = EncodingUtil.urlEncode(actionName,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/addaction?profileEntity=' + encodedProfileEntity + '&actionname=' + encodedActionName + '&value=' + value + '&secretKey=' +  secretKey;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }
   
   //AddAction Variation 2
   @Future(callout=true) global static void addAction(string secretKey, string externalAccountID, string profileEntity, string actionName, double value){
    
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedActionName = EncodingUtil.urlEncode(actionName,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/addaction?profileEntity=' + encodedProfileEntity + '&actionname=' + encodedActionName + '&value=' + value + '&secretKey=' +  secretKey + '&externalAccountID=' +  encodedExternalAccountID;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }
     
   //AwardBadge Variation 1
   @Future(callout=true) global static void awardBadge(string secretKey, string profileEntity, string badgeName){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedBadgeName = EncodingUtil.urlEncode(badgeName,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/awardbadge?profileEntity=' + encodedProfileEntity + '&badgename=' + encodedBadgeName + '&secretKey=' +  secretKey;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }
   
   //AwardBadge Variation 2
   @Future(callout=true) global static void awardBadge(string secretKey, string externalAccountID, string profileEntity, string badgeName){
    
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedBadgeName = EncodingUtil.urlEncode(badgeName,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/awardbadge?profileEntity=' + encodedProfileEntity + '&badgename=' + encodedBadgeName + '&secretKey=' + secretKey + '&externalAccountID=' +  encodedExternalAccountID;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }
   
   //RevokeBadge Variation 1
   @Future(callout=true) global static void revokeBadge(string secretKey, string profileEntity, string badgeName){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedBadgeName = EncodingUtil.urlEncode(badgeName,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/revokebadge?profileEntity=' + encodedProfileEntity + '&badgename=' + encodedBadgeName + '&secretKey=' +  secretKey;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }
   
   //RevokeBadge Variation 2
   @Future(callout=true) global static void revokeBadge(string secretKey, string externalAccountID, string profileEntity, string badgeName){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedBadgeName = EncodingUtil.urlEncode(badgeName,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/revokebadge?profileEntity=' + encodedProfileEntity + '&badgename=' + encodedBadgeName + '&secretKey=' + secretKey + '&externalAccountID=' +  encodedExternalAccountID;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }
   
   //AwardCert Variation 1
   @Future(callout=true) global static void awardCert(string secretKey, string profileEntity, string certName, string dateIssued, string expirationDate){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedCertName = EncodingUtil.urlEncode(certName,'UTF-8');
     String encodedDateIssued = EncodingUtil.urlEncode(dateIssued,'UTF-8');
     String encodedExpirationDate = EncodingUtil.urlEncode(expirationDate,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/awardcert?profileEntity=' + encodedProfileEntity + '&certname=' + encodedCertName + '&secretKey=' +  secretKey + '&dateIssued=' +  encodedDateIssued + '&expirationDate=' +  encodedExpirationDate ;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }   
   
   //AwardCert Variation 2
   @Future(callout=true) global static void awardCert(string secretKey, string externalAccountID, string profileEntity, string certName, string dateIssued, string expirationDate){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedCertName = EncodingUtil.urlEncode(certName,'UTF-8');
     String encodedDateIssued = EncodingUtil.urlEncode(dateIssued,'UTF-8');
     String encodedExpirationDate = EncodingUtil.urlEncode(expirationDate,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/awardcert?profileEntity=' + encodedProfileEntity + '&certname=' + encodedCertName + '&secretKey=' +  secretKey + '&externalAccountID=' +  encodedExternalAccountID + '&dateIssued=' +  encodedDateIssued + '&expirationDate=' +  encodedExpirationDate ;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }   
   
   //RevokeCert Variation 1
   @Future(callout=true) global static void revokeCert(string secretKey, string profileEntity, string certName, string expirationDate){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedCertName = EncodingUtil.urlEncode(certName,'UTF-8');
     String encodedExpirationDate = EncodingUtil.urlEncode(expirationDate,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/revokecert?profileEntity=' + encodedProfileEntity + '&certname=' + encodedCertName + '&secretKey=' +  secretKey + '&expirationDate=' +  encodedExpirationDate ;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   } 
   
   //RevokeCert Variation 2
   @Future(callout=true) global static void revokeCert(string secretKey, string externalAccountID, string profileEntity, string certName, string expirationDate){
    
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedCertName = EncodingUtil.urlEncode(certName,'UTF-8');
     String encodedExpirationDate = EncodingUtil.urlEncode(expirationDate,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/revokecert?profileEntity=' + encodedProfileEntity + '&certname=' + encodedCertName + '&secretKey=' +  secretKey + '&externalAccountID=' +  encodedExternalAccountID + '&expirationDate=' +  encodedExpirationDate ;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   } 
   
   //AdjustSkill Variation 1
   @Future(callout=true) global static void adjustSkill(string secretKey, string profileEntity, string skillName, integer points){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedSkillName = EncodingUtil.urlEncode(skillName,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/adjustskill?profileEntity=' + encodedProfileEntity + '&skillname=' + encodedSkillName + '&secretKey=' +  secretKey + '&points=' +  points;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }   
   
    //AdjustSkill Variation 2
   @Future(callout=true) global static void adjustSkill(string secretKey, string externalAccountID, string profileEntity, string skillName, integer points){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedSkillName = EncodingUtil.urlEncode(skillName,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/adjustskill?profileEntity=' + encodedProfileEntity + '&skillname=' + encodedSkillName + '&secretKey=' +  secretKey + '&externalAccountID=' +  encodedExternalAccountID + '&points=' +  points;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }   
   
   //AdjustRewardPoints Variation 1
   @Future(callout=true) global static void adjustRewardPoints(string secretKey, string profileEntity, integer points, string comment){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedComment = EncodingUtil.urlEncode(comment,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/adjustrewardpoints?secretKey=' +  secretKey + '&profileEntity=' + encodedProfileEntity + '&points=' +  points + '&comment=' +  encodedComment;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }   
   
   //AdjustRewardPoints Variation 2
   @Future(callout=true) global static void adjustRewardPoints(string secretKey, string externalAccountID, string profileEntity, integer points, string comment){
     
     String encodedProfileEntity = EncodingUtil.urlEncode(profileEntity,'UTF-8');
     String encodedComment = EncodingUtil.urlEncode(comment,'UTF-8');
     String encodedExternalAccountID = EncodingUtil.urlEncode(externalAccountID,'UTF-8');
     
     string url = 'https://www.redcritterconnecter.com/services/gamificationv1/adjustrewardpoints?secretKey=' +  secretKey + '&externalAccountID=' +  encodedExternalAccountID + '&profileEntity=' + encodedProfileEntity + '&points=' +  points + '&comment=' +  encodedComment;
     
     HttpRequest req = new HttpRequest();
     req.setMethod('GET');
     req.setEndpoint(url);
  
     Http http = new Http();
     if (secretKey != 'test'){
        HTTPResponse res = http.send(req);
     }
   }   
   
}