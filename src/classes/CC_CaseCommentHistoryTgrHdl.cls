public with sharing class CC_CaseCommentHistoryTgrHdl{

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){
		//Insert SRCommentHistory Begin.//
		List<CC_SRCommentHistory__c> SRCommentHistoryInsertList = new List<CC_SRCommentHistory__c>();

		for(Case caseObj: newList){
			//Comment1
			if(String.isNotBlank(caseObj.CMN_Comment1__c) && caseObj.CMN_Comment1__c != null){
				CC_SRCommentHistory__c SRCommentHistory = new CC_SRCommentHistory__c(
					CC_CaseId__c = caseObj.Id,
					CC_CommentField__c = 'コメント1',
					CC_Content__c = caseObj.CMN_Comment1__c
					);
				SRCommentHistoryInsertList.add(SRCommentHistory);
			}
			//Comment2
			if(String.isNotBlank(caseObj.CMN_Comment2__c) && caseObj.CMN_Comment2__c != null){
				CC_SRCommentHistory__c SRCommentHistory = new CC_SRCommentHistory__c(
					CC_CaseId__c = caseObj.Id,
					CC_CommentField__c = 'コメント2',
					CC_Content__c = caseObj.CMN_Comment2__c
					);
				SRCommentHistoryInsertList.add(SRCommentHistory);
			}
		}

		//Insert SRCommentHistory
		if(SRCommentHistoryInsertList != null && SRCommentHistoryInsertList.size() > 0){
			insert SRCommentHistoryInsertList;
		}
		//Insert SRCommentHistory End.//
	}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		Boolean insertCommHistoryFlag = false;
		Set<Id> CaseIdSet = new Set<Id>();

		//Insert SRCommentHistory Begin.//
		List<CC_SRCommentHistory__c> SRCommentHistoryInsertList = new List<CC_SRCommentHistory__c>();

		for(Integer i = 0; i < newList.size(); i++){
			//Comment1
			if(newList[i].CMN_Comment1__c != oldMap.get(newList[i].Id).CMN_Comment1__c){
				CC_SRCommentHistory__c SRCommentHistory = new CC_SRCommentHistory__c(
					CC_CaseId__c = newList[i].Id,
					CC_CommentField__c = 'コメント1',
					CC_Content__c = newList[i].CMN_Comment1__c
					);
				SRCommentHistoryInsertList.add(SRCommentHistory);

				CaseIdSet.add(newList[i].Id);
			}
			//Comment2
			if(newList[i].CMN_Comment2__c != oldMap.get(newList[i].Id).CMN_Comment2__c){
				CC_SRCommentHistory__c SRCommentHistory = new CC_SRCommentHistory__c(
					CC_CaseId__c = newList[i].Id,
					CC_CommentField__c = 'コメント2',
					CC_Content__c = newList[i].CMN_Comment2__c
					);
				SRCommentHistoryInsertList.add(SRCommentHistory);

				CaseIdSet.add(newList[i].Id);
			}
		}

		//Insert
		if(SRCommentHistoryInsertList != null && SRCommentHistoryInsertList.size() > 0){
			insertCommHistoryFlag = true;
			insert SRCommentHistoryInsertList;
		}
		//Insert SRCommentHistory End.//

		//Delete the exceeding CommentHistory Begin.//
		if(insertCommHistoryFlag){
			Set<CC_SRCommentHistory__c> deleteSRCommHisSet = new Set<CC_SRCommentHistory__c>();
			List<CC_SRCommentHistory__c> SRCommentHistoryObj = getSRCommHistoryListByCaseIdList(CaseIdSet);
			Map<Id, List<CC_SRCommentHistory__c> > SRCommentHistoryMapByCase = new Map<Id, List<CC_SRCommentHistory__c> >();

			if(SRCommentHistoryObj != null && SRCommentHistoryObj.size() > 0){
				//Break up the SRCommentHistory by Case Id.
				for(CC_SRCommentHistory__c SRCommHistory : SRCommentHistoryObj){
					List<CC_SRCommentHistory__c> SRCommHisListByCase = new List<CC_SRCommentHistory__c>();
					if(SRCommentHistoryMapByCase.get(SRCommHistory.CC_CaseId__c) != null){
						SRCommHisListByCase = SRCommentHistoryMapByCase.get(SRCommHistory.CC_CaseId__c);
					}
					SRCommHisListByCase.add(SRCommHistory);
					SRCommentHistoryMapByCase.put(SRCommHistory.CC_CaseId__c, SRCommHisListByCase);
				}


				for(Id caseid : CaseIdSet){
					//Break up the SRCommentHistory of every Case Id by CommentField.
					List<CC_SRCommentHistory__c> SRCommtHisListOfoneCase = SRCommentHistoryMapByCase.get(caseid);
					Map<String, List<CC_SRCommentHistory__c> > SRCommHisMapByField = new Map<String, List<CC_SRCommentHistory__c> >();

					for(CC_SRCommentHistory__c SRCommHistory : SRCommtHisListOfoneCase){
						List<CC_SRCommentHistory__c> SRCommHisIdListByField = new List<CC_SRCommentHistory__c>();
						if(SRCommHisMapByField.get(SRCommHistory.CC_CommentField__c) != null){
							SRCommHisIdListByField = SRCommHisMapByField.get(SRCommHistory.CC_CommentField__c);
						}
						SRCommHisIdListByField.add(SRCommHistory);
						SRCommHisMapByField.put(SRCommHistory.CC_CommentField__c, SRCommHisIdListByField);
					}

					//Count and pull out delete list of every CommentField.
					//Comment1
					List<CC_SRCommentHistory__c> Comment1IdList = SRCommHisMapByField.get('コメント1');
					if(Comment1IdList!= null && Comment1IdList.size() > 5){
						for(Integer i = 0; i < Comment1IdList.size() - 5; i++){
							deleteSRCommHisSet.add(Comment1IdList[i]);
						}
					}

					//Comment2
					List<CC_SRCommentHistory__c> Comment2IdList = SRCommHisMapByField.get('コメント2');
					if(Comment2IdList != null && Comment2IdList.size() > 5){
						for(Integer i = 0; i < Comment2IdList.size() - 5; i++){
							deleteSRCommHisSet.add(Comment2IdList[i]);
						}
					}
				}

				//Delete SRCommentHistory
				if(deleteSRCommHisSet != null && deleteSRCommHisSet.size() > 0){
					List<CC_SRCommentHistory__c> deleteSRCommHisList = new List<CC_SRCommentHistory__c>();
					deleteSRCommHisList.addall(deleteSRCommHisSet);
					Delete deleteSRCommHisList;
				}
				//Delete the exceeding CommentHistory End.//
			}
		}
	}

	/**
	 * Object: CC_SRCommentHistory__c
	 * Parameter: list of Case Id
	 * return: List<CC_SRCommentHistory__c>
	 */
	public static List<CC_SRCommentHistory__c> getSRCommHistoryListByCaseIdList(Set<Id> CaseIdSet){
		List<CC_SRCommentHistory__c> srCommentHistoryList = [
							SELECT Id, CC_CaseId__c, CC_CommentField__c
							  FROM CC_SRCommentHistory__c
							 WHERE CC_CaseId__c IN: CaseIdSet
						  ORDER BY CC_CaseId__c, Name];

		return srCommentHistoryList;
	}

}