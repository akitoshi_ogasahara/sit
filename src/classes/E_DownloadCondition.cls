/**
 *		E_DownloadCondition
 *			PDF,CSVダウンロード時のOPROサービス呼出し時のパラメータを管理
 */
public with sharing class E_DownloadCondition {

	public virtual class DownloadCondi{
		//　代理店格Id
		public String distributorCode{get;set;}
		//　代理店格コード
		public String ZHEADAY{get;set;}
		//　代理店格名
		public String ZHEADNAME{get;set;}
		
		//　代理店事務所コードリスト
		public List<String> officeCodes{get;set;}
	
		//　ID種別
		public String ZIDTYPE{get;set;}
		
		public String toJSON(){
			return JSON.serialize(this);
		}
	}
	
	/**
	 *		手数料明細
	 */
	public class CommissionDownloadCondi extends DownloadCondi{
		public String fromyyyy{get;set;}
		public String fromMM{get;set;}
		public String toyyyy{get;set;}
		public String toMM{get;set;}
	}

	/**
	 *		一般変額保険明細
	 */
	public virtual class SPVADownloadCondi extends DownloadCondi{
		//　募集人コード
		public String agentCode{get;set;}
		public String fromZACCTYM{get;set;}			
		public String toZACCTYM{get;set;}			
		public String fromOCCDATE{get;set;}			
		public String toOCCDATE{get;set;}			
		public String ZCNTRDSC{get;set;}			
		public String ZANNSTFLG{get;set;}
	}

	/**
	 *		個人保険明細 （SPVAを継承）
	 */
	public class COLIDownloadCondi extends SPVADownloadCondi{
		public String groupby{get;set;}
	}

	
	/**
	 *		手続履歴保険明細
	 */
	public class HistoryDownloadCondi extends DownloadCondi{
		//　募集人コード
		public String agentCode{get;set;}
		public String fromTRANDATE{get;set;}
		public String toTRANDATE{get;set;}
	}
	
	/**
	 * 		ユニットプライス
	*/
	public class UnitPriceDownloadCondi extends DownloadCondi{
		public String EUPBPF{get;set;}			//ユニットプライス基本情報ID
	    public String NAMEKBN{get;set;}			// 名称取得区分
	    public List<String> ZSFUNDCD{get;set;}	// ユニットプライス表示用ファンド
	    public String fromTerm{get;set;}		// 期間選択開始日
	    public String toTerm{get;set;}			// 期間選択終了日
	    
	    public E_DownloadCondition.UnitPriceDownloadCondi getInstanceByJSON(String jsonVal){
	    	return (E_DownloadCondition.UnitPriceDownloadCondi)JSON.deserialize(jsonVal, UnitPriceDownloadCondi.class);
	    }
	    
	    public E_DownloadCondition.UnitPriceDownloadCondi clone(E_DownloadCondition.UnitPriceDownloadCondi originalCondi){
	    	E_DownloadCondition.UnitPriceDownloadCondi dlCondi = new E_DownloadCondition.UnitPriceDownloadCondi();
	    	dlCondi.EUPBPF = originalCondi.EUPBPF;
	    	dlCondi.NAMEKBN = originalCondi.NAMEKBN;
	    	dlCondi.ZSFUNDCD = originalCondi.ZSFUNDCD;
	    	dlCondi.fromTerm = originalCondi.fromTerm;
	    	dlCondi.toTerm = originalCondi.toTerm;
	    	return dlCondi;
	    }
	}
	
	/**
	 * 保全4帳票 NNLink
	 */
	public class NoticeDownloadCondi extends DownloadCondi{
		public String agentCode{get;set;}
		public String fromyyyyMM{get; set;}
		public String toyyyyMM{get; set;}
		public List<String> policyTypes{get; set;}	//契約種類
		public String csvType{get; set;}	//CSV種別
	}
	
	/**
	 * 保全4帳票 IRIS
	 */
	public class NoticeIrisDownloadCondi extends DownloadCondi{
		public String agentCode{get;set;}
		public String fromyyyyMM{get; set;}
		public String toyyyyMM{get; set;}
		public List<String> policyTypes{get; set;}	//契約種類
		
		public List<String> distributorCodes {get; set;}
	}

	/**
	 *		規制強化
	 */
	public virtual class BizDownloadCondi{
		//　代理店格コード
		public String ZHEADAY{get;set;}
		
		public String toJSON(){
			return JSON.serialize(this);
		}
	}
	
	/**
	 *		規制強化(事業報告書)
	 */
	public class BizReportsDownloadCondi extends BizDownloadCondi{
		public String reportType {get; set;}			// 取扱商品
	}

	/**
	 *		規制強化(帳簿書類)
	 */
	public class BizBooksDownloadCondi extends BizDownloadCondi{
		public List<String> officeCodes {get; set;}	// 事務所コード
	}
	
	/**
	 * 保険契約概要　PIP
	 */
	public class ICOLIDownloadCondi{
		public List<String> policyTypes {get; set;}					// 保険種類（保険契約ヘッダのレコードタイプ）
		public String policyId {get; set;}							// 保険契約ヘッダレコードID
		public String officeName {get; set;}						// 事務所名
		public String agentName {get; set;}							// 募集人名
		public String groupName {get; set;}							// 団体情報
		public String inquirydate {get; set;}						// データ基準日（yyyy/MM/dd）
		public String isIss {get; set;}								// ISS経由判定
		//public String comment {get; set;}							// コメント
		public List<NotificationInfo> notificationInfos {get; set;}	// 通知一覧情報
		
		public String toJSON(){
			return JSON.serialize(this);
		}
	}
	/**
	 * 保険契約概要　PIP
	 * 通知一覧情報
	 */
	public class NotificationInfo {
		public String type {get; set;}
		public String month {get; set;}
		public String zduedate {get; set;}
		public String limitdt {get; set;}
		public String content {get; set;}
	}
	/**
	 * 医的引受検索　デジタルマニュアル
	 * 傷病No情報
	 */
	public class DiseaseNumberInfo{
		public String csvType {get; set;}
		public String diseaseNumber {get; set;}

		public String toJSON(){
			return JSON.serialize(this);
		}
	}
}