@isTest
private class TestE_MK_CampaignTargetDao{
	
	private static testMethod void TestgetActiveRecByCLTPF_CLNTNUM_001(){

		//キャンペーン管理を作成
		CampaignManagement__c CampaignManagement = TestMK_TestUtil.createCampaignManagement(true,date.today().adddays (-1),date.today().adddays (1));
		//キャンペーンターゲットを作成
		MK_CampaignTarget__c MK_CampaignTarget = TestMK_TestUtil.createMK_CampaignTarget(true);
		//キャンペーン管理ID呼び出し
		String cmid = CampaignManagement.id;	
		String CLTPF_CLNTNUM = '12345678';
		String contractantKana = 'contractantKana';	
		//ID、顧客番号、契約者名カナをパラメータに複数の値を取得
		MK_CampaignTarget__c cmpt = TestMK_TestUtil.createMK_CampaignTarget(true,CampaignManagement.id,CLTPF_CLNTNUM,ContractantKana);
   		//顧客番号(CLTPF_CLNTNUM__c)・契約者名カナ(ContractantKana__c)を取得
		CLTPF_CLNTNUM = cmpt.CLTPF_CLNTNUM__c;
		ContractantKana = cmpt.ContractantKana__c;

   		//テストされるメソッドに顧客番号を渡す
		MK_CampaignTarget__c ret = E_MK_CampaignTargetDao.getActiveRecByCLTPF_CLNTNUM(CLTPF_CLNTNUM);
		

		System.assertEquals(cmpt.id,ret.id);
		System.assertEquals(cmpt.name,ret.name);
		System.assertEquals(CLTPF_CLNTNUM,ret.CLTPF_CLNTNUM__c);
		System.assertEquals(contractantKana,ret.ContractantKana__c);
		System.assertEquals('a@example.com',ret.ContractantMailAddress__c);
		System.assertEquals(CampaignManagement.id,ret.Campaign__c);
		System.assertEquals(CampaignManagement.id,ret.Campaign__r.id);
		System.assertEquals('名前',ret.Campaign__r.Name);
		System.assertEquals('インセンティブ',ret.Campaign__r.Incentive__c);
		System.assertEquals('紹介者限定インセンティブ',ret.Campaign__r.ContractantIncentive__c);
		System.assertEquals('インフォメーション',ret.Campaign__r.Information__c);
		System.assertEquals('概要',ret.Campaign__r.Overview__c);
		System.assertEquals('導入',ret.Campaign__r.Introduction__c);
		System.assertEquals('コンテストレギュレーションキー',ret.Campaign__r.E_MessageMaster_UpsertKey__c);
	}

	private static testMethod void TestgetActiveRecById_001(){

		//募集人を作成(ID=Agent__c)
		String lastName = '姓';
		Contact contact = TestMK_TestUtil.createContact(true,lastName);
		//キャンペーン管理を作成
		CampaignManagement__c CampaignManagement = TestMK_TestUtil.createCampaignManagement(true,date.today().adddays (-1),date.today().adddays (1));
		//キャンペーンターゲットを作成
		MK_CampaignTarget__c MK_CampaignTarget = TestMK_TestUtil.createMK_CampaignTarget(true,CampaignManagement.id,contact.id);
		System.debug(MK_CampaignTarget);
		//テストされるメソッドにキャンペーンターゲットIDを渡す
		MK_CampaignTarget__c ret = E_MK_CampaignTargetDao.getActiveRecById(MK_CampaignTarget.id);
		
		System.debug(ret);
		System.assertEquals(MK_CampaignTarget.id,ret.id);
		System.assertEquals('契約者名',ret.Name);
		System.assertEquals(contact.id,ret.Agent__c);
		System.assertEquals('a@example.com',ret.AgentMailAddress__c);
		System.assertEquals('b@example.com',ret.ContractantMailAddress__c);
		System.assertEquals(CampaignManagement.id,ret.Campaign__r.id);
		System.assertEquals('名前',ret.Campaign__r.Name);
		System.assertEquals('タイトル',ret.Campaign__r.title__c);
		System.assertEquals('インセンティブ',ret.Campaign__r.Incentive__c);
		System.assertEquals('インフォメーション',ret.Campaign__r.Information__c);
		System.assertEquals('概要',ret.Campaign__r.Overview__c);
		System.assertEquals('導入',ret.Campaign__r.Introduction__c);
		System.assertEquals('コンテストレギュレーションキー',ret.Campaign__r.E_MessageMaster_UpsertKey__c);
	}
}