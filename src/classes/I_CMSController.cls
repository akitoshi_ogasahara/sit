/**
 *      /apex/IRIS  pageのController
 *
 */
public virtual with sharing class I_CMSController extends I_AbstractController {

    //GW経由でアクセスしたときのpgkey読み替え       インターネット→GW呑みに対応
    //      NNTubeからはインターネットのpgkeyが指定されて飛んでくるため読み替え
    private static Map<String,String> PGKEY_MAPPING_FOR_GW = new Map<String,String>{
        'InternetUsePolicy' => 'GWUsePolicy'
    };


    // IRISコンテンツ（コンポーネント）
    public List<I_CMSSection> CMSSections {get; set;}

    //メニューIdを設定
    protected virtual override Id getIrisMenuId(){
        // URLパラメータを取得
        String pid = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGE);       //?page=
        if(String.isNotBlank(pid)){
            // ページを取得
            this.activePage = I_PageMasterDao.getRecordById(pid);
        }

        if (this.activePage == null) {
            //取得できない場合は、PageKeyで取得
            String pkey = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGEKEY);       //?pgkey=

            if(String.isNotBlank(pkey)){
                //GW経由はpgKeyの読み替えを実施
                if(access.isCommonGateWay()){
                    if(PGKEY_MAPPING_FOR_GW.containsKey(pkey)){
                        pkey = PGKEY_MAPPING_FOR_GW.get(pkey);
                    }
                }
                this.activePage = I_PageMasterDao.getRecordByKey(pkey);
                //pgkeyがあるのに取得できない場合エラーとする
                if (this.activePage == null) {
                    throw new GeneralException('ページが存在しません['+ ApexPages.currentPage().getUrl() +']');
                }
            }
        }

        //ゲストユーザかつメニューに紐づいているPageの場合エラーとする
        if(access.isGuest() && this.activePage != null && this.activePage.Menu__c != null){
            throw new GeneralException('ページが存在しません['+ ApexPages.currentPage().getUrl() +']');
        }
        return this.activePage.Menu__c;
    }

    // NNLinkの補足情報とIRISCMSコンテンツの説明を結合するHTML
    private static final String HTML_CONCAT_CONTENTS = '<p>{0}</p><p class="p-l-2">{1}</p>';

    // コンストラクタ
    public I_CMSController() {

        try {
            // Section+Componentを取得
            List<I_ContentMaster__c> sectionRecs = getContentMasterRecords();

            Map<Id, List<I_ContentMaster__c>> componentRecsBySection = new Map<Id, List<I_ContentMaster__c>>();

            Set<Id> pageCompIds = new Set<Id>();
            Map<String, E_CMSFile__c> nnlinkCMSFiles = new Map<String, E_CMSFile__c>();
            PageAnchors = new Map<String,String>();
            
            for(I_ContentMaster__c secRec: sectionRecs){
                pageCompIds.add(secRec.Id);
                PageAnchors.put(secRec.Name, String.isBlank(secRec.dom_id__c)?String.valueOf(secRec.Id):secRec.dom_id__c);
                
                // 表示可能権限／経路に基づき、コンポーネント再構築
                Integer loopLimit = secRec.ChildContents__r.size();
                List<I_ContentMaster__c> childlen = new List<I_ContentMaster__c>();
                for(Integer i = 0; i < loopLimit; i++) {

                    I_ContentMaster__c child = secRec.ChildContents__r.get(i);

                    if(String.isBlank(child.CanDisplayVia__c)) {
                        //特集の場合
                        if(child.FeatureFlag__c && child.FeatureContents__c == I_Const.IRIS_CONTENTS_NAME_SALESTOOL){
                            String selectcondition = 'ParentContent__r.Name = \'' + I_Const.IRIS_CONTENTS_NAME_SALESTOOL +'\' And DocumentNo__c = \'' + child.FeatureSearch__c + '\'' + 'AND ValidTo__c >= today' + ' AND DisplayFrom__c <= today';
                            if(!access.isEmployee()){
                                selectcondition = selectcondition + ' AND CompanyLimited__c = false';
                            }
                            for(I_ContentMaster__c featureSalse:I_ContentMasterDao.getPickupContentsForLibTop(selectcondition, 1)){
                                //親レコード（セクション）を入れ替えてリストに追加
                                featureSalse.putSObject('ParentContent__r', secRec);
                                childlen.add(featureSalse);
                            }
                        }else{
                            childlen.add(child);
                        }
                    } else {
                        Set<String> rights = new Set<String>(child.CanDisplayVia__c.split(';'));

                        if(rights.contains(I_Const.DISP_RIGHT_EMPLOYEE)) {
                            if(!E_AccessController.getInstance().isEmployee()) {
                                continue;
                            }
                        }

                        if(rights.contains(I_Const.DISP_RIGHT_AGENCY)) {
                            if(!E_AccessController.getInstance().isAgent()) {
                                continue;
                            }
                        }

                        if(rights.contains(I_Const.ACCESS_VIA_INTERNET)) {
                            if(!E_AccessController.getInstance().isViaInternet()) {
                                continue;
                            }
                        }

                        if(rights.contains(I_Const.ACCESS_VIA_CMN_GW)) {
                            if(!E_AccessController.getInstance().isCommonGateway()) {
                                continue;
                            }
                        }

                        // P16-0003 Atria対応開発 ここから
                        if(rights.contains(I_Const.ATRIA_CAN_ACCESS_USER)) {
                            if(!E_AccessController.getInstance().canAccessAtria()) {
                                continue;
                            }
                        }

                        if(rights.contains(I_Const.ATRIA_CAN_NOT_ACCESS_USER )) {
                            if(E_AccessController.getInstance().canAccessAtria()) {
                                continue;
                            }
                        }
                        // P16-0003 Atria対応開発 ここまで

                        childlen.add(child);
                    }
                }

                componentRecsBySection.put(secRec.Id, childlen);


                //ピックアップレコードの再取得
                //      （クエリ発行回数に気を付ける必要あり loop内クエリだがほとんど利用されないためロジックの平易さを優先）
                if(secRec.SelectFrom__c == 'CMSコンテンツ'){
                    for(I_ContentMaster__c pickedUp:I_ContentMasterDao.getPickupContentsForLibTop(secRec.SelectCondition__c
                                                                                                        , secRec.SelectLimit__c)){
						//カテゴリが「会社案内・リクルート」と「事前案内資料」は社員にのみ表示する
						if(pickedUp.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_RECRUIT)
							|| pickedUp.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_ADVANCEINFO)) {
                            if(!E_AccessController.getInstance().isEmployee()) {
                                continue;
                            }
                        }
                        //親レコード（セクション）を入れ替えてリストに追加
                        pickedUp.putSObject('ParentContent__r', secRec);
                        componentRecsBySection.get(secRec.Id).add(pickedUp);
                    }
                }

                for(I_ContentMaster__c cmpRec: componentRecsBySection.get(secRec.Id)){
                    if(cmpRec.ClickAction__c == 'DL(NNLink)'){
                        nnlinkCMSFiles.put(cmpRec.filePath__c, null);
                    }else{
                        pageCompIds.add(cmpRec.Id);
                    }
                    PageAnchors.put(secRec.Name, String.isBlank(secRec.dom_id__c)?String.valueOf(secRec.Id):secRec.dom_id__c);      //セクションとコンポーネントが同じ名前の場合コンポーネントで上書きされる
                }
            }

            this.CMSSections = new List<I_CMSSection>();

            // コンポーネントの添付ファイルを取得
            Map<ID, List<Attachment>> files = E_AttachmentDao.getRecsByParentIds(pageCompIds);

            // ChatterFileのIdを取得
            Map<Id,Id> chatterFileIds = I_ContentMasterDao.getChatterFileIdsByRecId(pageCompIds);

            // NNLinkのファイル情報を取得　取得できない場合、DaoにてKeyからも削除される
            nnlinkCMSFiles = E_MessageMasterDao.getCMSFilesWithAttachmentByKeys(nnlinkCMSFiles.keyset());

            // コンポーネント情報を作成
            for (I_ContentMaster__c secRec: sectionRecs) {
                I_CMSSection section = new I_CMSSection(secRec);

                // コンテンツ情報を作成
                for (I_ContentMaster__c cmpRec: componentRecsBySection.get(secRec.Id)) {

                    //20180214 資料発送申込対応 START
                    if(I_Const.DR_INFO_LABEL.contains(cmpRec.InfoLabel__c)){
                        if(access.user.Contact.Account.CannotBeOrder__c){
                            continue;
                        }
                    }
                    //20180214 資料発送申込対応 END

                    if(cmpRec.ClickAction__c == 'DL(NNLink)' && nnlinkCMSFiles.containsKey(cmpRec.filePath__c)){        //NNLink情報が取得できた場合
                        //CMSコンテンツ情報をNNLink設定内容で補足
                        E_CMSFile__c cmsFile = nnlinkCMSFiles.get(cmpRec.filePath__c);      //Keyがある場合はCMSFileレコードあり
                        if(cmpRec.Content__c != cmsFile.Note__c){       //if(secRec.Name == '■NN TIMES'){
                            //IRISの内容とファイル情報の補足の値が異なる場合は結合して表示
                            cmpRec.Content__c = String.format(HTML_CONCAT_CONTENTS, new List<String>{E_Util.null2blank(cmpRec.Content__c)
                                                                                                    , E_Util.null2blank(cmsFile.Note__c)});
                        }else{
                            cmpRec.Content__c = cmsFile.Note__c;    //NNLinkファイル情報の補足文字列を追加
                        }
                        cmpRec.isHTML__c = true;        //HTMLとして表示
                        if(String.isNotBlank(cmsFile.UpdateDate__c)){
                            cmpRec.LabelforDate__c = '最終更新日';
                            cmpRec.DateStr__c = cmsFile.UpdateDate__c;  //最終更新日
                        }
                        cmpRec.FormNo__c = cmsFile.FormVersion__c;
                        section.addCMSComp(cmpRec, cmsfile.Attachments, chatterFileIds.get(cmpRec.Id));
                    }else{
                        section.addCMSComp(cmpRec, files.get(cmpRec.Id), chatterFileIds.get(cmpRec.Id));
                    }
                }
                this.CMSSections.add(section);
            }
            //this.menu = activePage.Menu__r;
        } catch(Exception e) {
            //ApexPages.addMessages(e);
            getPageMessages().addErrorMessage(e.getMessage(), e.getStackTraceString());
        }

        if (this.activePage == null) {
            this.activePage = new I_PageMaster__c();
        }
    }

    protected virtual List<I_ContentMaster__c> getContentMasterRecords(){
        return I_ContentMasterDao.getRecordsByPageId(this.activePage.id);
    }

    //ページ処理処理
    protected virtual override PageReference init(){
        //アクセスログのName項目の設定
        this.pageAccessLog.Name = this.activePage.Name;
        if(pageAccessLog.Name==null){
            pageAccessLog.Name = this.menu.Name;
        }

        //Superクラスでエラーなしの場合に処理を実施
        PageReference pr = super.init();
        if(pr!=null){
            return pr;
        }

        return null;
    }

    //Subカテゴリを持っている場合にサブナビを表示
    public Boolean getShowSubNav(){
        return String.isNotBlank(this.menu.subCategory__c);
    }

    //Page内のIRIS　CMSコンテンツレコード名とIdのMap
    public static Map<String, String> PageAnchors{get;private set;}

    public class GeneralException extends Exception {}
}