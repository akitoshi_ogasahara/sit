/**
 * 募集人毎利用履歴画面に表示する項目を更新するBatchApex
 */

global class I_UpdateLogBatch implements Database.Batchable<sObject> ,Database.Stateful{

	// バッチ終了時刻
	//private static final Integer BATCH_END_HOUR = 17;
	// バッチ名
	private static final String BATCH_JOB_NAME = 'ログ更新バッチ';
	private static final String BATCH_JOB_NAME_TUBE = 'ログ更新バッチ_NNTube';

	//Prefix 格納変数
	private String attPre; //Attachment prefix
	private String contentPre; //Content prefix

	//アクセスページURL
	private static final String ACCESS_URL_COLI			= 'e_coli';
	private static final String ACCESS_URL_SPVA			= 'e_spva';
	private static final String ACCESS_URL_ANNUITY		= 'e_annuity';
	private static final String ACCESS_URL_DCOLI		= 'e_dcoli';
	private static final String ACCESS_URL_DSPVA		= 'e_dspva';
	private static final String ACCESS_URL_NEWPOLICY	= 'iris_newpolicydetail';
	private static final String ACCESS_URL_FEATURE		= 'iris_feature';
	private static final String ACCESS_URL_AGENCY_SR	= 'iris_agencysalesresults';
	private static final String ACCESS_URL_IRIS_COLI	= 'iris_coli';
	private static final String ACCESS_URL_IRIS_DCOLI	= 'iris_dcoli';
	private static final String ACCESS_URL_DSSEARCH		= 'e_dssearch';	//20171208　傷病一覧表示
	private static final String ACCESS_URL_LIBTOOLS		= 'iris_lib_tools';	//20180309　Webコンテンツ_コンテンツを開く

	//URLパラメータ
	private static final String URLPARAM_AGENCY_SR_AGENCY		= 'agency';		// 代理店
	private static final String URLPARAM_AGENCY_SR_OFFICE		= 'office';		// 事務所
	private static final String URLPARAM_AGENCY_SR_AGENT		= 'agent';		// 募集人
	private static final String URLPARAM_AGENCY_SR_TYPE			= 'type';		// type
	private static final String URLPARAM_AGENCY_SR_ID			= 'id';			// ID
	private static final String URLPARAM_DSSEARCH_P			= 'p';			// 傷病No

	// 代理店情報処理判定文字列
	private static final String AGENCY_SR_DETAIL_CHECK = '【INFO】 [リダイレクト] ';

	//アクセスページURL 保有契約
	private static final Set<String> ACCESS_CONTACT_URL = new Set<String>{
											ACCESS_URL_COLI,
											ACCESS_URL_SPVA,
											ACCESS_URL_ANNUITY,
											ACCESS_URL_DCOLI,
											ACCESS_URL_DSPVA,
											ACCESS_URL_IRIS_COLI,
											ACCESS_URL_IRIS_DCOLI
	};

	//ログ一覧表示対象ページMap(key：ページ名)
	private Map<String, I_LogViewTarget__mdt> pageMdMap;
	//ログ一覧表示対象帳票Map(key：帳票種別（数式）)
	private Map<String, I_LogViewTarget__mdt> reportMdMap;
	//ログ一覧表示対象帳票Map(key：帳票種別（数式）)
	private Map<String, I_LogViewTarget__mdt> pdfMdMap;

	// コンストラクタ
	public void I_UpdateLogBatch(){}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		//Attachment,ContentのPrefixを取得
		attPre = Attachment.SObjectType.getDescribe().getKeyPrefix();
		contentPre = ContentVersion.SObjectType.getDescribe().getKeyPrefix();

		// カスタムメタデータ型を取得し、Mapにセット
		pageMdMap = new Map<String, I_LogViewTarget__mdt>();
		reportMdMap = new Map<String, I_LogViewTarget__mdt>();
		pdfMdMap = new Map<String, I_LogViewTarget__mdt>();

		for(I_LogViewTarget__mdt target : [SELECT DeveloperName, SelectCategory__c, ActionType__c,ApiName__c FROM I_LogViewTarget__mdt WHERE isActive__c = true]){
			if(target.ActionType__c == I_Const.ACTION_TYPE_PAGE_ACCESS){
				//pageMdMap.put(target.DeveloperName, target);
				pageMdMap.put(target.ApiName__c, target);
			} else if(target.ActionType__c == I_Const.ACTION_TYPE_REPORT_DOWNLOAD){
				//reportMdMap.put(target.DeveloperName, target);
				reportMdMap.put(target.ApiName__c, target);
			} else if(target.ActionType__c == I_Const.ACTION_TYPE_PDF){
				//pdfMdMap.put(target.DeveloperName, target);
				pdfMdMap.put(target.ApiName__c, target);
			}
		}

		return Database.getQueryLocator( [SELECT Id, isDisplay__c,
											batchDate__c,
											SummaryName__c,
											DisplayURL__c,
											Name,
											ActionType__c,
											AccessUser__c,
											RecordId__c,
											ProcAccessPage__c,
											DownloadHistory__r.FormNoLabel__c,
											DownloadHistory__r.FormNo__c, //帳票種別
											DownloadHistory__r.Conditions__c, //検索条件
											IsError__c,				// エラー
											Detail__c,				// 詳細情報
											AccessUser__r.UserType	// アクセスユーザ.ユーザタイプ
											FROM E_Log__c
											WHERE batchDate__c = null
										]);
	}

	global void execute(Database.BatchableContext BC, List<E_Log__c> scope) {
		Set<Id> accountIds	= new Set<Id>();
		Set<Id> contactIds	= new Set<Id>();
		Set<Id> userIds		= new Set<Id>();
		Map<Id, Integer> agencySRSetting = new Map<Id, Integer>();
		Set<String> diseaseNos = new Set<String>();
		Map<Id,E_DownloadCondition.ICOLIDownloadCondi> pipCondiMap = new Map<Id,E_DownloadCondition.ICOLIDownloadCondi>();
		Map<Id,E_DownloadCondition.DiseaseNumberInfo> diseaseCondiMap = new Map<Id,E_DownloadCondition.DiseaseNumberInfo>();

		//現在の日付を取得
		Datetime nowDate = DateTime.now();

		//取得したログから保険契約ヘッダのIDをSetする
		Set<Id> policyIds = new Set<Id>();
		Set<Id> newPolicyIds = new Set<Id>();
		for(E_Log__c log : scope){
			//アクションタイプが'ページ'の場合
			if(log.ActionType__c == I_Const.ACTION_TYPE_PAGE_ACCESS){
				String pageName;
				if(String.isNotBlank(log.ProcAccessPage__c)){
					pageName = accessPageCheck(log.ProcAccessPage__c.toLowerCase());
				}

				//契約照会ページ
				if(String.isNotBlank(log.ProcAccessPage__c) && ACCESS_CONTACT_URL.contains(pageName)){
					//20171206 エラーログの場合は、URLパラメータを取得しない
					if(log.IsError__c == false){
						// URLパラメータの値を取得
						String policyId = getStringPolicyId(log.ProcAccessPage__c);
						if(String.isNotBlank(policyId)){
							policyIds.add(Id.valueof(policyId));
						}
					}
				// 新契約ステータス詳細
				} else if(String.isNotBlank(pageName) && pageName == ACCESS_URL_NEWPOLICY){
					//20171206 エラーログの場合は、URLパラメータを取得しない
					if(log.IsError__c == false){
						// URLパラメータの値を取得
						String npId = getUrlParamValue(log.ProcAccessPage__c, 'id');
						if(String.isNotBlank(npId)){
							newPolicyIds.add(Id.valueof(npId));
						}
					}

				// 代理店挙績情報
				} else if(String.isNotBlank(pageName) && pageName == ACCESS_URL_AGENCY_SR){
					agencySRSetting.put(log.Id, 0);
					if(log.IsError__c == false
					&& log.AccessUser__r.UserType != E_Const.USERTYPE_STANDARD
					&& (String.isBlank(log.Detail__c) || log.Detail__c.containsIgnoreCase(AGENCY_SR_DETAIL_CHECK) == false)){
						// エラーログ・リダイレクト処理・社員のログ以外を対象とする
						if(log.ProcAccessPage__c.containsIgnoreCase(URLPARAM_AGENCY_SR_AGENCY + '=')){
							// 代理店の場合
							try{
								accountIds.add(Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_AGENCY)));
								agencySRSetting.put(log.Id, 1);
							}catch(Exception e){}
						}else if(log.ProcAccessPage__c.containsIgnoreCase(URLPARAM_AGENCY_SR_OFFICE + '=')){
							// 事務所の場合
							try{
								accountIds.add(Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_OFFICE)));
								agencySRSetting.put(log.Id, 2);
							}catch(Exception e){}
						}else if(log.ProcAccessPage__c.containsIgnoreCase(URLPARAM_AGENCY_SR_AGENT + '=')){
							// 募集人の場合
							try{
								contactIds.add(Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_AGENT)));
								agencySRSetting.put(log.Id, 3);
							}catch(Exception e){}
						}else if(log.ProcAccessPage__c.containsIgnoreCase(URLPARAM_AGENCY_SR_TYPE + '=')){
							if(log.ProcAccessPage__c.containsIgnoreCase(URLPARAM_AGENCY_SR_TYPE + '=' + URLPARAM_AGENCY_SR_OFFICE)){
								// type=officeの場合
								try{
									accountIds.add(Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_ID)));
									agencySRSetting.put(log.Id, 4);
								}catch(Exception e){}
							}
						}else{
							// アクセスユーザの場合
							userIds.add(log.AccessUser__c);
							agencySRSetting.put(log.Id, 5);
						}
					}
				//20171208　傷病一覧表示
				} else if(String.isNotBlank(pageName) &&  pageName == ACCESS_URL_DSSEARCH){
					String diseaseNo = getUrlParamValue(Log.ProcAccessPage__c, URLPARAM_DSSEARCH_P);
					if(!String.isBlank(diseaseNo)) diseaseNos.add(diseaseNo);
				}
			//PDF出力
			}else if(log.ActionType__c == I_Const.ACTION_TYPE_PDF){
				String policyId;
				String disease; //傷病情報格納変数
				//検索条件より、対象の値を変数に格納
				if(log.DownloadHistory__r.FormNo__c == E_Const.DH_FORMNO_IRIS_COLI){
					E_DownloadCondition.ICOLIDownloadCondi condi = (E_DownloadCondition.ICOLIDownloadCondi)JSON.deserialize(log.DownloadHistory__r.Conditions__c,E_DownloadCondition.ICOLIDownloadCondi.class);
					policyId = condi.policyId;
					pipCondiMap.put(log.id,condi);
				}else if(log.DownloadHistory__r.FormNo__c == E_Const.DH_FORMNO_DISEASE){ //傷病PDF
					E_DownloadCondition.DiseaseNumberInfo condi = (E_DownloadCondition.DiseaseNumberInfo)JSON.deserialize(log.DownloadHistory__r.Conditions__c,E_DownloadCondition.DiseaseNumberInfo.class);
					//検索条件内の'diseaseNumber'の値を取得
					disease = condi.diseaseNumber;
					diseaseCondiMap.put(log.id,condi);
				}
				if(String.isNotBlank(policyId)){
					policyIds.add(Id.valueof(policyId));
				}
				if(String.isNotBlank(disease)){
					diseaseNos.add(disease);
				}

			}
		}

		Map<Id, E_Policy__c> polMap		= E_PolicyDao.getRecMapByIds(policyids);
		Map<Id, E_NewPolicy__c> npolMap	= E_NewPolicyDao.getRecsForLog(newPolicyIds);

		Map<Id, Account> accountMap		= E_AccountDao.getRecsByIds(accountIds);
		Map<Id, Contact> contactMap		= E_ContactDao.getRecsByIds(contactIds);
		Map<Id, User> userMap			= E_UserDao.getUserByIdsWithBatch(userIds);
		Map<Id, E_IDCPF__c> idcpfMap	= new Map<Id, E_IDCPF__c>();
		List<E_IDCPF__c> idcpfList = E_IDCPFDao.getRecsByUserIds(userIds);
		for(E_IDCPF__c idcpf : idcpfList){
			idcpfMap.put(idcpf.User__c, idcpf);
		}
//		Map<String, E_DiseaseNumber__c> diseaseMap = E_DiseaseNumberDao.getRecMapByNames(diseaseNos);	//20171208　傷病一覧表示

		//20171227　傷病一覧表示
		Map<String,String> diseaseMap = new Map<String,String>();
		for(E_DiseaseNumber__c rec :E_DiseaseNumberDao.getRecsByNames(diseaseNos)){
			List<String> names = new List<String>();
			for(E_Diseases__c child : rec.Diseases__r){
				names.add(child.Name);
			}
			diseaseMap.put(rec.Name, DS_Const.APPNAME + '_' + String.join(names, '_'));
		}

		for(E_Log__c log : scope){
			//バッチ処理日付に現在日時を格納
			log.batchDate__c = nowDate;

			//20171215 エラーログの場合は何もしない
			if(log.IsError__c) continue;

			//アクションタイプが'ページ'
			if(log.ActionType__c == I_Const.ACTION_TYPE_PAGE_ACCESS){
				String pageName;
				if(String.isNotBlank(log.ProcAccessPage__c)){
					pageName = accessPageCheck(log.ProcAccessPage__c.toLowerCase());
				}

				// 契約内容個別照会ページ
				if(String.isNotBlank(pageName) && ACCESS_CONTACT_URL.contains(pageName)){
					//URLパラメータ「id」から保険契約ヘッダのSFIDを取得
					String policyId = getStringPolicyId(log.ProcAccessPage__c);
					log.SummaryName__c = log.Name;
					if(String.isNotBlank(policyId)){
						Id polid = Id.valueof(policyId);
						if(polMap.containskey(polid)){
							E_Policy__c pol = polMap.get(polid);

							//一覧用ログ名 = [ログ名（Name）]_[保険契約ヘッダ.証券番号]_[保険契約ヘッダ.契約者名]
							if(log.ProcAccessPage__c.toLowerCase().indexOf(ACCESS_URL_DCOLI) != -1
							|| log.ProcAccessPage__c.toLowerCase().indexOf(ACCESS_URL_DSPVA) != -1
							|| log.ProcAccessPage__c.toLowerCase().indexOf(ACCESS_URL_IRIS_DCOLI) != -1){
								log.SummaryName__c += '_' + pol.COMM_CHDRNUM__c.substringBefore('|') + '_' + pol.COMM_ZCLNAME__c;
							}else{
								log.SummaryName__c += '_' + pol.COMM_CHDRNUM__c + '_' + pol.ContractorName__c;
							}
						}
					}

					log.isDisplay__c = true;
					//URLパラメータ「no-cache」が付与されている場合は削除する
					log.DisplayURL__c = cacheRemove(log.ProcAccessPage__c);
					log.DisplayURL__c = addCategoryPrm(log.DisplayURL__c,I_Const.MENU_CATEGORY_ALIAS_POLICY);

				// 新契約ステータス詳細
				} else if(String.isNotBlank(pageName) &&  pageName == ACCESS_URL_NEWPOLICY){
					//URLパラメータ「id」から新契約ステータスのSFIDを取得
					String strNpid = getUrlParamValue(log.ProcAccessPage__c, 'id');

					log.SummaryName__c = log.Name;
					if(String.isNotBlank(strNpid)){
						ID npid = ID.valueOf(strNpid);
						// 一覧用ログ名 = [ログ名（Name）]_[新契約ステータス.証券番号]_[新契約ステータス.契約者名]_[新契約ステータス.保険種類]
						if(npolMap.containskey(npid)){
							E_NewPolicy__c npol = npolMap.get(npid);
							log.SummaryName__c += '_' + npol.CHDRNUM__c + '_' + npol.COMM_ZCLNAME__c + '_' + npol.InsuranceType__c;
						}
					}

					log.isDisplay__c = true;
					//URLパラメータ「no-cache」が付与されている場合は削除する
					log.DisplayURL__c = cacheRemove(log.ProcAccessPage__c);

				// 代理店挙績情報
				} else if(agencySRSetting.containsKey(log.Id)){
					Id key		= null;
					if(agencySRSetting.get(log.Id) == 1){
						// 代理店の場合
						key = Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_AGENCY));
						if(accountMap.containsKey(key)){
							Account acc			= accountMap.get(key);
							log.SummaryName__c	= '代理店挙績情報_' + + ((acc.Parent.Name == null) ? '' : acc.Parent.Name);
							log.DisplayURL__c	= log.ProcAccessPage__c;
							log.isDisplay__c	= true;
						}
					}else if(agencySRSetting.get(log.Id) == 2){
						// 事務所の場合
						key = Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_OFFICE));
						if(accountMap.containsKey(key)){
							Account acc			= accountMap.get(key);
							log.SummaryName__c	= '事務所挙績情報_' + acc.Name;
							log.DisplayURL__c	= log.ProcAccessPage__c;
							log.isDisplay__c	= true;
						}
					}else if(agencySRSetting.get(log.Id) == 3){
						// 募集人の場合
						key = Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_AGENT));
						if(contactMap.containsKey(key)){
							Contact cnt			= contactMap.get(key);
							log.SummaryName__c	= '募集人挙績情報_';
							log.SummaryName__c	+= cnt.E_Account__c + '_';
							log.SummaryName__c	+= cnt.Name;
							log.DisplayURL__c	= log.ProcAccessPage__c;
							log.isDisplay__c	= true;
						}
					}else if(agencySRSetting.get(log.Id) == 4){
						// type=officeの場合
						key = Id.valueOf(getUrlParamValue(log.ProcAccessPage__c, URLPARAM_AGENCY_SR_ID));
						if(accountMap.containsKey(key)){
							Account acc			= accountMap.get(key);
							log.SummaryName__c	= '事務所挙績情報_' + acc.Name;
							log.DisplayURL__c	= log.ProcAccessPage__c;
							log.isDisplay__c	= true;
						}
					}else if(agencySRSetting.get(log.Id) == 5){
						// アクセスユーザの場合
						key = log.AccessUser__c;
						if(idcpfMap.containsKey(key) && userMap.containsKey(key)){
							E_IDCPF__c idcpf	= idcpfMap.get(key);
							User us				= userMap.get(key);
							if(idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AH){
								log.SummaryName__c	= '代理店挙績情報_' + ((us.Contact.Account.Parent.Name == null) ? '' : us.Contact.Account.Parent.Name);
								log.DisplayURL__c	= '/apex/IRIS_AgencySalesResults?' + URLPARAM_AGENCY_SR_AGENCY + '=';
								log.DisplayURL__c	+= us.Contact.Account.ParentId;
								log.isDisplay__c	= true;
							}else if(idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AY){
								log.SummaryName__c	= '事務所挙績情報_' + ((us.Contact.Account.Name == null) ? '' : us.Contact.Account.Name);
								log.DisplayURL__c	= '/apex/IRIS_AgencySalesResults?' + URLPARAM_AGENCY_SR_OFFICE + '=';
								log.DisplayURL__c	+= us.Contact.AccountId;
								log.isDisplay__c	= true;
							}else if(idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AT){
								log.SummaryName__c	= '募集人挙績情報_';
								log.SummaryName__c	+= us.Contact.Account.Name + '_';
								log.SummaryName__c	+= us.Contact.Name;
								log.DisplayURL__c	= '/apex/IRIS_AgencySalesResults?' + URLPARAM_AGENCY_SR_AGENT + '=';
								log.DisplayURL__c	+= us.ContactId;
								log.isDisplay__c	= true;
							}
						}
					}
				//20171208　傷病一覧表示
				} else if(String.isNotBlank(pageName) &&  pageName == ACCESS_URL_DSSEARCH){
					String diseaseNo = getUrlParamValue(Log.ProcAccessPage__c, URLPARAM_DSSEARCH_P);
/*					if(diseaseNo != null && diseaseMap.containsKey(diseaseNo)){
						E_DiseaseNumber__c disease = diseaseMap.get(diseaseNo);
						List<String> diseaseNameGroupList = new List<String>();
						for(E_Diseases__c dis : disease.Diseases__r){
							diseaseNameGroupList.add(dis.Name);
						}
						//一覧用ログ名
						log.SummaryName__c = '傷病検索_' + String.join(diseaseNameGroupList, '_') + '_' + disease.Category__c;
						//一覧用URL
						log.DisplayURL__c = cacheRemove(Log.ProcAccessPage__c);
						//一覧表示フラグをtrue
						log.isDisplay__c = true;
					}*/
					//20171227　傷病一覧表示
					if(diseaseNo != null && diseaseMap.containsKey(diseaseNo)){
						//一覧用ログ名
						log.SummaryName__c = diseaseMap.get(diseaseNo);
						//一覧用URL
						log.DisplayURL__c = cacheRemove(Log.ProcAccessPage__c);
						//一覧表示フラグをtrue
						log.isDisplay__c = true;
					}
				//コンテンツを開く
				} else if(String.isNotBlank(pageName) && pageName == ACCESS_URL_LIBTOOLS){
					//「コンテンツを開く」ボタン押下時のみログ表示する
					if(String.isNotBlank(log.SummaryName__c)){
						log.isDisplay__c = true;
					}
				// 上記以外の利用履歴画面に表示するページ
				}else if(String.isNotBlank(pageName) && pageMdMap.keySet().contains(pageName)){
					log.SummaryName__c = log.Name;
					if(pageName == ACCESS_URL_FEATURE){
						log.SummaryName__c = '特集_' + log.SummaryName__c;
					}
					//URLパラメータ「no-cache」が付与されている場合は削除する
					log.DisplayURL__c = cacheRemove(log.ProcAccessPage__c);
					//URLパラメータ「category」を付与
					I_LogViewTarget__mdt lldp = pageMdMap.get(pageName);
					if(lldp.SelectCategory__c != null){
						log.DisplayURL__c = addCategoryPrm(log.DisplayURL__c, lldp.SelectCategory__c);
					}
					log.isDisplay__c = true;
				}

			//アクションタイプが'帳票ダウンロード'
			}else if(log.ActionType__c == I_Const.ACTION_TYPE_REPORT_DOWNLOAD){
				//ダウンロード履歴の帳票種別(数式)判定
				if(reportMdMap.keySet().contains(log.DownloadHistory__r.FormNoLabel__c)){
					log.isDisplay__c = true;
				}

			//アクションタイプが'ファイルダウンロード'
			}else if(log.ActionType__c == I_Const.ACTION_TYPE_FILE_DOWNLOAD){
				log.SummaryName__c = log.Name;
				//「レコードID」が空ではないとき
				if(String.isNotBlank(log.RecordId__c)){
					//「レコードID」の先頭3文字を取得し、判定を行う
					if(log.RecordId__c.startsWith(attPre)){ //Attachmentの場合
						log.DisplayURL__c = I_Const.ATTACHMENTURL;
					}else if(log.RecordId__c.startsWith(contentPre)){ //Contentの場合
						log.DisplayURL__c = I_Const.CONTENTURL;
					}

					log.isDisplay__c = true;
					log.DisplayURL__c = log.DisplayURL__c.replace('%record%',log.RecordId__c);
				}
			//アクションタイプが'お知らせ'
			}else if(log.ActionType__c == I_Const.ACTION_TYPE_READ_INFOMATION){
				//一覧用ログ名
				log.SummaryName__c = log.Name;
				//一覧用URL
				log.DisplayURL__c = I_Const.INFORMATIONURL;
				//一覧表示フラグをtrue
				log.isDisplay__c = true;
				//一覧用URLの一部を定数からレコードIDへ変更
				log.DisplayURL__c = log.DisplayURL__c.replace('%record%',log.RecordId__c);
			//20180214 資料発送申込対応 START
			//アクションタイプが'資料発送申込'
			}else if(log.ActionType__c == I_Const.DOCUMENT_REQUEST){
				//一覧表示フラグをtrue
				log.isDisplay__c = true;
			//20180214 資料発送申込対応 END
			//アクションタイプが'PDF出力'
			}else if(log.ActionType__c == I_Const.ACTION_TYPE_PDF){
				//ダウンロード履歴の帳票種別(数式)判定
				if(log.DownloadHistory__c != null && pdfMdMap.keySet().contains(log.DownloadHistory__r.FormNoLabel__c)){
					JSONParser parser = JSON.createParser(log.DownloadHistory__r.Conditions__c);
					String policyId;
					String policyTypes;
					String disease;
					//検索条件より、対象の値を変数に格納
					if(log.DownloadHistory__r.FormNo__c == E_Const.DH_FORMNO_IRIS_COLI){
						E_DownloadCondition.ICOLIDownloadCondi condi = pipCondiMap.get(log.id);
						policyId = condi.policyId;
						policyTypes = condi.policyTypes[0];
						log.SummaryName__c = log.Name;
						if(String.isNotBlank(policyId)){
							Id polid = Id.valueof(policyId);
							if(polMap.containskey(polid)){
								E_Policy__c pol = polMap.get(polid);
								//一覧用ログ名 = [ログ名（Name）]_[保険契約ヘッダ.証券番号]_[保険契約ヘッダ.契約者名]
								if(policyTypes.equals(E_Const.POLICY_RECORDTYPE_DCOLI)
								|| policyTypes.equals(E_Const.POLICY_RECORDTYPE_DSPVA)){
									log.SummaryName__c += '_' + pol.COMM_CHDRNUM__c.substringBefore('|') + '_' + pol.COMM_ZCLNAME__c;
								}else{
									log.SummaryName__c += '_' + pol.COMM_CHDRNUM__c + '_' + pol.ContractorName__c;
								}
								log.isDisplay__c = true;
							}
						}
					}else if(log.DownloadHistory__r.FormNo__c == E_Const.DH_FORMNO_DISEASE){
						E_DownloadCondition.DiseaseNumberInfo condi = diseaseCondiMap.get(log.id);
						//検索条件内の'diseaseNumber'の値を取得
						disease = condi.diseaseNumber;
						if(String.isNotBlank(disease)&& diseaseMap.containsKey(disease)){
							//一覧用ログ名
							log.SummaryName__c = diseaseMap.get(disease);
							log.isDisplay__c = true;
						}
					}
				// ダウンロード履歴と関連してない場合
				}else if(log.DownloadHistory__c == null){
					log.isDisplay__c = true;
				}
			}
			// たもつくんのケースを追加
			else if ( log.ActionType__c == I_Const.ACTION_TYPE_TMT ){
				log.isDisplay__c = true;
			}
		}

		try{
			//update
			List<Database.Saveresult> results = Database.update(scope);

			// Errorレコード判定
			for (Database.SaveResult sr : results) {
				if(!sr.isSuccess()) {
					Database.Error err = sr.getErrors()[0];
					err.getMessage();
				}
			}
		}catch(Exception e){
			system.debug('Exception='+e);
		}
	}

	global void finish(Database.BatchableContext BC) {
		if(!(datetime.now().hour() >= I_Const.BATCH_END_HOUR) && !System.Test.isRunningTest()){
			system.scheduleBatch(new I_UpdateLogBatch(), BATCH_JOB_NAME, 60);
			system.scheduleBatch(new I_LogNNTubeBatch(), BATCH_JOB_NAME_TUBE + E_Util.getSysDate('yyyyMMddHHmm'), 60);
		}
	}

	private String getStringPolicyId(String logPage){
		//URLパラメータ「id」から保険契約ヘッダを検索してセットする
		String policyId = logPage.substringAfter('id=');
		//policyIdに&が含まれる場合
		if(policyId.indexOf('&') != -1){
			//'&'より前をidとして格納
			policyId = policyId.substringBefore('&');
		}
		return policyId;
	}

	/**
	 * URLパラメータの値を取得する処理
	 * @param String pageUrl : 対象URL
	 * @param String urlPrm : URLパラメータ名
	 * @return String
	 */
	private String getUrlParamValue(String pageUrl, String urlPrm){
		// 引数のパラメータから値を取得
		String prmValue = pageUrl.substringAfter(urlPrm + '=');
		// 値に & が含まれる場合
		if(prmValue.indexOf('&') != -1){
			//'&'より前をidとして格納
			prmValue = prmValue.substringBefore('&');
		}
		return prmValue;
	}

	//AccessPageのno-cacheを削除
	private String cacheRemove(String page){
		//AccessPageをPageReference型に変換
		pagereference pr = new Pagereference(page);
		//パラメータのno-cacheを取り除く
		pr.getParameters().remove('no-cache');
		//URLを返す
		return pr.geturl();
	}

	//一覧用URLにcategoryを加える
	private String addCategoryPrm(String page,String prm){
		//DisplayURLをPagereference型に変換
		pagereference pr = new PageReference(page);
		//URLに「category=」 と対応したパラメータを付与
		pr.getParameters().put('category',prm);
		//URLを返す
		return pr.geturl();
	}

	//log.ProcAccessPage__c のページ名取り出し
	private String accessPageCheck(String page){
		//URLをパラメータ部分と分ける
		page = page.substringBefore('?');
		//pageに'/'が含まれる場合
		if(page.indexOf('/') != -1){
			//'/'以下を保持
			page = page.substringAfterLast('/');
		}
		return page;
	}
}