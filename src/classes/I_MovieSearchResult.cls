public with sharing class I_MovieSearchResult {

	public String subject {get; set;}
	public String description {get; set;}
	public Date publishedDate {get; set;}
	public Decimal views {get; set;}
	public String thumbnail {get; set;}
	public String url {get; set;}
	public String duration {get; set;}

	public String getFormattedSubject() {
		return String.isNotBlank(this.subject) ? this.subject.abbreviate(I_Const.NN_TUBE_MOVIE_LENGTH_TITLE + 3) : '';
	} 

	public String getFormattedDescription() {
		return String.isNotBlank(this.description) ? this.description.stripHtmlTags().abbreviate(I_Const.NN_TUBE_MOVIE_LENGTH_DESCRIPTION + 3) : '';
	} 

	public static I_MovieSearchResult build(NNTubeMedia media) {
		I_MovieSearchResult result = new I_MovieSearchResult();
		result.subject = media.title;
		result.description = media.long_description;
		result.views = media.play_cnt;
		result.thumbnail = media.small_thumbnail_url;
		result.publishedDate = transformStringToDate(media.publication_date);
		Time duration = Time.newInstance(0, 0, 0, 0).addSeconds(Integer.valueOf(media.duration));
		result.duration = String.format('{0}:{1}', new List<String>{String.valueOf(duration.minute()), String.valueOf(duration.second())});
		I_TubeConnectHelper helper = I_TubeConnectHelper.getInstance();
		result.url = String.format('{0}?{1}&mid={2}', new List<String>{
			helper.getTubeTopUrl(), helper.getParams_MovieType(), String.valueOf(media.mid)
		});
		return result;
	}

	private static Pattern datePtn = Pattern.compile('(\\d{4}\\/\\d{2}\\/\\d{2})');
	public static Date transformStringToDate(String value) {
		Date result;
		Matcher m = datePtn.matcher(String.isNotBlank(value) ? value : '');
		if (m.find()) {
			result = E_Util.formattedString2DateyyyyMMdd(m.group());
		}
		return result;
	}

	public class NNTubeResult {
		public String resultcode {get; set;}
		public Decimal moviesum {get; set;}
		public List<NNTubeMedia> meta {get; set;}
		// 拡張
		public String error {get; set;}
	}

	public class NNTubeMedia {
		public String api_keywords {get; set;}
		public String ccode {get; set;}
		public String cname {get; set;}
		public String copyright {get; set;}
		public String copyright_url {get; set;}
		// public Map<String, String> custom_metadata {get; set;}
		public String customer_movie {get; set;}
		public Decimal disclaimer_id {get; set;}
		public Decimal duration {get; set;}
		// public List<Map<String, String>> exlink {get; set;}
		public String expiration_date {get; set;}
		public String insert_date {get; set;}
		public String keywords {get; set;}
		public String level {get; set;}
		public Decimal like_cnt {get; set;}
		public String long_description {get; set;}
		public Decimal mid {get; set;}
		// public Map<String, String> movie_definition {get; set;}
		// public Map<String, String> movie_filesize {get; set;}
		public String obfuscated_mid {get; set;}
		public Decimal play_cnt {get; set;}
		public String publication_date {get; set;}
		public String recommend {get; set;}
		public String scode {get; set;}
		public String small_thumbnail_url {get; set;} 
		public String small_thumbnail_url_ssl {get; set;}
		public String thumbnail_url {get; set;}
		public String thumbnail_url_ssl {get; set;}
		public String title {get; set;}
		public String update_date {get; set;}
	}
}