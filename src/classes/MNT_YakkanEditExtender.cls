global with sharing class MNT_YakkanEditExtender extends SkyEditor2.Extender{

	// インスタンス
	public MNT_YakkanEditController extension;
	private I_ContentMaster__c record;
	
	// 定数定義
	// 親約款名
	private static final String YAKKAN_PARENT_NAME_BASE = 'ご契約のしおり・約款';
	
	// 約款種別
	private static Map<Integer, String> yakkanTypeMap = new Map<Integer, String> {
		0 => '本編',
		1 => '別添1',
		2 => '別添2'
	};
	
	// 保存後の遷移ページ
	private static final String PAGE_AFTER_SAVE = '/apex/MNT_YakkanView';
	
	// ページ表示時の子レコード群
	private Map<Id, I_ContentMaster__c> initChildrenMap;
	
	// ご契約のしおり・約款_区分
	private String guideAgreeDiv = '';

	/**
	 * コンストラクタ.
	 *
	 * @param MNT_YakkanEditController コントローラインスタンス
	 */
	public MNT_YakkanEditExtender(MNT_YakkanEditController extension){
		this.extension = extension;
		this.initChildrenMap = new Map<Id, I_ContentMaster__c>();
	}

	/**
	 * 初期処理.
	 */
	global override void init(){
		
		this.record = extension.record;

		// 編集モード
		if(String.isNotBlank(this.record.Id)) {
			
			// ページ表示時の子レコードを保持しておく
			for(MNT_YakkanEditController.yakkanFileListItem item : this.extension.yakkanFileList.items){
			
				this.initChildrenMap.put(item.record.Id, item.record);
			}
			return;
		}
		
		// 約款種別設定
		for(Integer i = 0; i < extension.yakkanFileList.items.size(); i++) {
			MNT_YakkanEditController.yakkanFileListItem item = extension.yakkanFileList.items.get(i);
			
			item.record.YakkanType__c = yakkanTypeMap.get(i);
		}
	}
	
	/**
	 * 保存処理.
	 *
	 * @return PageReference 保存後のページ
	 */
	public PageReference doSave() {
		
		if(!this.validate()) {
			return null;
		}
		
		this.guideAgreeDiv = String.isNotBlank(this.record.GuideAgreeDiv__c) ? this.record.GuideAgreeDiv__c : '';
		
		// 子レコードの設定 & 空行削除
		this.setUpChildren();
		
		this.record.UpsertKey__c = '';
		this.record.Name = YAKKAN_PARENT_NAME_BASE + ':' + this.record.FormNo__c + ':' + this.guideAgreeDiv;
		this.record.IsYakkanParentFlg__c = true;
		
		try {
			// 親保存
			upsert this.record;

			List<I_ContentMaster__c> children = new List<I_ContentMaster__c>();
			
			for(MNT_YakkanEditController.yakkanFileListItem item : this.extension.yakkanFileList.items){
				item.record.ParentContent__c = this.record.Id;
			
				children.add(item.record);
				
				// 行削除された子レコードを保持
				this.initChildrenMap.remove(item.record.Id);
			}

			// 子保存
			upsert children;

			// 行削除された子レコード削除
			if(!this.initChildrenMap.isEmpty()) {
				
				delete this.initChildrenMap.values();
			}
			
			return new PageReference(PAGE_AFTER_SAVE + '?Id=' + this.record.Id);
		} catch(Exception e) {
			this.setErrorMessage('保存時にエラーが発生しました。' + e.getMessage());
			
			return null;
		}
	}
	
	/**
	 * 子レコード群の設定.
	 */
	private void setUpChildren() {
		
		// 削除の対象となるレコードを格納するリストを作成
		List<MNT_YakkanEditController.yakkanFileListItem> removeList = new List<MNT_YakkanEditController.yakkanFileListItem>();

		// テーブルのリストを検証
		for(MNT_YakkanEditController.yakkanFileListItem item : this.extension.yakkanFileList.items){
			// DLファイル格納先項目が空だった場合に削除
			if(String.isBlank(item.record.filePath__c)){
				removeList.add(item); 
			}else{
				item.record.UpsertKey__c = '';
				item.record.Name = item.record.YakkanType__c + ':' + this.record.FormNo__c + ':' + this.guideAgreeDiv;
				item.record.WindowTarget__c = '_blank';
				item.record.ClickAction__c = 'DL(Attachment)';
				item.record.isSitePublishFlg__c = this.record.isSitePublishFlg__c;
			}
		}

		for(MNT_YakkanEditController.yakkanFileListItem item : removeList){
			// レコード削除
			item.remove();
		}
	}
	
	/**
	 * 保存時チェック.
	 */
	private Boolean validate() {
		
		// 子レコードの約款種別をチェック
		if(!isValidYakkanType()) {
			
			this.setErrorMessage('選択されている約款種別が不正です。');
			
			return false;
		}
		
		return true;
	}
	
	/**
	 * エラーメッセージ表示.
	 *
	 * @param String エラーメッセージ
	 */ 
	private void setErrorMessage(String msg) {
		SkyEditor2.ExtenderException exp = new SkyEditor2.ExtenderException();
		exp.addMessage(msg);
		exp.setMessagesToPage();
	}
	
	/**
	 * 子レコードの約款種別をチェック.
	 *
	 * @return Boolean チェックOKならばtrue, さもなくばfalse
	 */
	private Boolean isValidYakkanType() {
		// 子レコード数と選択された約款種別のユニーク数を突合する
		List<MNT_YakkanEditController.yakkanFileListItem> items = this.extension.yakkanFileList.items;
		Integer size = items.size();
		Set<String> yakkanTypeSet = new Set<String>();
		
		for(MNT_YakkanEditController.yakkanFileListItem item : items) {
			yakkanTypeSet.add(item.record.YakkanType__c);
		}
		
		if(size != yakkanTypeSet.size()) {
			return false;
		}
		
		if(size == 1 && !yakkanTypeSet.contains('本編')) {
			return false;
		}
		
		return true;
	}
}