public class DPA_Recruit_MaintenanceToolController {
    // 入力ファイル
	public blob file {get; set;}

    // テスト用スイッチ
    @TestVisible private boolean isDMLExceptionTest;    
    
    // ボタン押した時のメソッド
	public PageReference ImportBtn(){
        if (file == NULL) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, 'ファイルを指定してください。'));
            return null;            
        }
    
        // 取込みCSV
        ContentVersion v = new ContentVersion();
        v.versionData = file;
        String csvFile = v.versionData.toString();
        
        // 文字列からCSV行毎のリストを作成
        List<String> rowList = csvFile.split('\r\n', 0);
        Map<String, String> AH5CodeLatLon = new Map<String, String>();
        for (String r : rowList) {
            List<String> rl = r.split(',');
            if (rl.size() == 4) {
                if (rl[0] != '' && rl[1] != '') {
                    AH5CodeLatLon.put(rl[0], r);
                }
            }
        }
        
        if (AH5CodeLatLon.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'データとして有効な行がありませんでした。'));
            return null;
        }
        
        // Accountから該当のレコードを抽出して、位置ステータスを設定する
        List<Account> updateList = [SELECT id, E_CL2PF_ZAGCYNUM__c, AddressLoc__latitude__s, AddressLoc__longitude__s, AddressLocStatus__c FROM Account WHERE E_CL2PF_ZAGCYNUM__c IN: AH5CodeLatLon.keySet()];
        for(Account ac : updateList) {
            List<String> rl = AH5CodeLatLon.get(ac.E_CL2PF_ZAGCYNUM__c).split(',');
            if (rl[1] == '0') {
                ac.AddressLocStatus__c = '未設定';
				ac.AddressLoc__latitude__s = 35.679261;
                ac.AddressLoc__longitude__s = 139.735751; // ガーデンコート
                
            } else if (rl[1] == '1') {
                if (rl[2] != '' && rl[3] != '') {
                    ac.AddressLocStatus__c = '設定済（自動）';
                    ac.AddressLoc__latitude__s = Double.valueOf(rl[2]);
                    ac.AddressLoc__longitude__s = Double.valueOf(rl[3]);                    
                }
            }
            
            // DmlExceptionのテスト用
            if(Test.isRunningTest() && this.isDMLExceptionTest){
                ac.AddressLocStatus__c = 'test';
            }
        }
        
        // 更新
        if (updateList.size() != 0) {
            try{
                update updateList;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, '更新しました。'));
            }
            catch(DmlException e) {
                ApexPages.addMessages(e);
                return null;
            }

        }
            
        return null;
	}
        
}