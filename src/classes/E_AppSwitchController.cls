/**
 *		NNLinkとIRIS切替ページ　
 *				IRIS側から遷移することもあるのでI_AbstractControllerを継承させる
 */
public class E_AppSwitchController extends I_AbstractController{
    private static final Map<String,PageReference> APP_TOP_PAGE = new Map<String, PageReference>{
    																	I_Const.APP_MODE_NNLINK => Page.E_Home
    																	, I_Const.APP_MODE_IRIS => Page.IRIS_Top
    																};
    
    public E_AppSwitchController(){
    	super();
    	this.pgTitle = 'アプリケーション切替';
    }
    
    protected override PageReference init(){
    	pageAccessLog.Detail__c = 'アプリケーションを切替(' + access.idcpf.AppMode__c + '→)';
    	return switchApp();
    }
    
    private PageReference switchApp(){
		//Filter機能を無効化
		if(iris.isSetPolicyCondition()){
			iris.clearPolicyCondition();
		}

    	String app = ApexPages.currentPage().getParameters().get(I_Const.APPSWITCH_URL_PARAM_APP);		//app
    	if(APP_TOP_Page.containsKey(app)){
			//IRIS利用不可ユーザの場合
			//if(app == I_Const.APP_MODE_IRIS && !iris.getCanUseIRIS()){
			//	E_PageMessagesHolder.getInstance().addErrorMessage('IRISをご利用いただけません。');
		    //	access.switchEIDC_appMode(I_Const.APP_MODE_NNLINK);		//appMode=NNLinkへ強制的に修正
		    //	return null;
    		//}

			// NNLinkを利用する場合、コンプライアンス画面を省略する
			if(app == I_Const.APP_MODE_NNLINK){
				E_CookieHandler.setCookieCompliance();
			}

    		//EIDCのAppModeを更新
			access.switchEIDC_appMode(app);		//appMode切替
			//access.switchEIDC_appMode();		//「--なし--」のときにNNLinkに切り替わらないので削除		
    	
    		return APP_TOP_PAGE.get(app);
    	}

		E_PageMessagesHolder.getInstance().addErrorMessage('アプリケーション<' + app + '>への切り替えに失敗しました。');
    	return null;
    }
    
    //IRIS利用可能ユーザの場合 かつ　自動的に遷移できない場合に表示
    public Boolean getCanUseIRIS(){
    	return iris.getCanUseIRIS();
    }
    
}