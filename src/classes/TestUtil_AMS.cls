@isTest
public class TestUtil_AMS {
	/**
	 * 新契約ステータス作成
	 * @param  isInsert: whether to insert
	 * @param statCode: 新契約ステータス
	 * @param chdrnum: 証券番号
	 * @param syncDate: データ発信日
	 * @param kframeind: 団体タイプ
	 * @param grupkey: 団体番号
	 * @param kpaketnm: 申込年月
	 * @param owner: 契約者
	 * @param ownerKana: 契約者（カナ）
	 * @param insured: 被保険者
	 * @param insuredKana: 被保険者（カナ）
	 * @param distributeId: 代理店格id
	 * @param officeId: 事務所id
	 * @param agentId: 募集人id
	 * @return E_NewPolicy__c
	 */
	public static E_NewPolicy__c createNewPolicy (
					Boolean isInsert 
					,String statCode 
					,String chdrnum
					,Datetime syncDate
					,String kframeind
					,String grupkey
					,Decimal kpaketnm
					,String owner
					,String ownerKana
					,String insured
					,String insuredKana
					,id distributeId
					,id officeId
					,id agentId
					) {
		E_NewPolicy__c src = new E_NewPolicy__c(
			//基本情報(必須)
			STATCODE__c = statcode	//新契約ステータス
			,CHDRNUM__c = chdrnum	//証券番号
			,SyncDate__c = syncDate	//データ発信日
			//団体(団体時必須)
			,KFRAMEIND__c = kframeind	//団体タイプ
			,GRUPKEY__c = grupkey	//団体番号
			,KPAKETNM__c = kpaketnm	//申込年月
			//基本情報(画面表示)
			,COMM_ZCLNAME__c = owner	//契約者
			,ContractorE_CLTPF_ZCLKNAME__c = ownerKana	//契約者（カナ）
			,COMM_ZINSNAM__c = insured	//被保険者
			,InsuredE_CLTPF_ZCLKNAME__c = insuredKana	//被保険者（カナ）
			,ParentAccount__c = distributeId	//代理店格
			,Account__c = officeId	//代理店事務所
			,Agent__c = agentId	//募集人
			);
		if(isInsert){
			insert src;
		}
		return src;
	}
	/**
	 * 新契約不備作成
	 * @param  isInsert: whether to insert
	 * @param newPolicyId: 新契約id
	 * @param fupCde: 不備コード
	 * @param fupStat: 不備ステータス
	 * @return E_NewPolicyDefect__c
	 */
	public static E_NewPolicyDefect__c createNewPolicyDefect (Boolean isInsert ,id newPolicyId ,String fupCde ,String fupStat) {
		E_NewPolicyDefect__c src = new E_NewPolicyDefect__c(
			E_NewPolicy__c = newPolicyId	//新契約
			,FUPCDE__c = fupCde	//不備コード
			,FUPSTAT__c = fupStat	//不備ステータス
			);
		if(isInsert){
			insert src;
		}
		return src;
	}
	/**
	 * 不備マスタ作成
	 * @param  isInsert: whether to insert
	 * @param code: コード値
	 * @param value: 値
	 * @param sortKey: ソートキー
	 * @return E_DefectMaster__c
	 */
	public static E_DefectMaster__c createDefectMaster (Boolean isInsert ,String code ,String value ,String sortKey) {
		E_DefectMaster__c src = new E_DefectMaster__c(
			Code__c = code	//コード値
			,Value__c = value	//値
			,SortKey__c = sortKey	//ソートキー
			);
		if(isInsert){
			insert src;
		}
		return src;
	}
	/**
	 * @param isInsert: whether to insert
	 * @param kind: 連携種別
	 * @return E_bizDataSyncLog__c
	 */
	public static E_bizDataSyncLog__c createEbizLog(Boolean isInsert ,String kind){
		E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = kind);
		if(isInsert){
			insert log;        	
		}
		return log;
	}
}