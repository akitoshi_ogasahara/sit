@isTest
private class TESTE_IDCPFTriggerEditGroup {

	@isTest static void testAddMembeUpd() {
		Group testGp = [SELECT Id FROM Group WHERE DeveloperName = 'DocumentReqReportV'];
		User mrUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c mridcpf = TestI_TestUtil.createIDCPF(true, mrUser.Id, null);

		Test.startTest();
			mridcpf.IsDocMaint__c = TRUE;
			update mridcpf;
		Test.stopTest();

		List<GroupMember> result = [SELECT Id FROM GroupMember WHERE GroupId = :testGp.Id AND UserOrGroupId = :mrUser.Id];
		System.assertEquals(1, result.size());
	}

	@isTest static void testAddMembeIns() {
		Group testGp = [SELECT Id FROM Group WHERE DeveloperName = 'DocumentReqReportV'];
		User mrUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c mridcpf = TestI_TestUtil.createIDCPF(false, mrUser.Id, null);

		Test.startTest();
			mridcpf.IsDocMaint__c = TRUE;
			insert mridcpf;
		Test.stopTest();

		List<GroupMember> result = [SELECT Id FROM GroupMember WHERE GroupId = :testGp.Id AND UserOrGroupId = :mrUser.Id];
		System.assertEquals(1, result.size());
	}

	@isTest static void testDelMember() {
		User u = TestI_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		Group testGp = [SELECT Id FROM Group WHERE DeveloperName = 'DocumentReqReportV'];
		User mrUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c mridcpf = TestI_TestUtil.createIDCPF(false, mrUser.Id, null);
		mridcpf.IsDocMaint__c = TRUE;
		insert mridcpf;
		System.runAs(u){
			insert new GroupMember(GroupId = testGp.Id, UserOrGroupId = mrUser.Id);
		}

		List<GroupMember> result1 = [SELECT Id FROM GroupMember WHERE GroupId = :testGp.Id AND UserOrGroupId = :mrUser.Id];
		System.assertEquals(1, result1.size());

		Test.startTest();
			mridcpf.IsDocMaint__c = FALSE;
			update mridcpf;
		Test.stopTest();

		List<GroupMember> result2 = [SELECT Id FROM GroupMember WHERE GroupId = :testGp.Id AND UserOrGroupId = :mrUser.Id];
		System.assertEquals(0, result2.size());
	}

}