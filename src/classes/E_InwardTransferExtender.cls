global with sharing class E_InwardTransferExtender extends  E_AbstractViewExtender{
/*-----------------------------------------------Field----------------------------------------------------*/
    // extension
    E_InwardTransferController extension;
    // 変額ファンドリスト（データテーブル）
    E_InwardTransferController.E_SVFPF_c_E_Policy_c inwardList;
    // 変額ファンドリスト（データテーブル） 積立金の移転の申込画面、遷移時表示用
    List<E_InwardTransferController.E_SVFPF_c_E_Policy_cItem> backUpInwardListRecords;

    // 同意チェック
    public Boolean isAgree {get; set;}
    // 顧客番号
    public String contactCLNTNUM {get; set;}
    //フリガナ
    public String contactKANA;
    // お客様名
    public String contactName {get; set;}
    // 変更後の表用のJSON形式データ
    public String jsonUpdateData {get; set;}
    // 合計：現在の繰入比率
    public Decimal totalZINVPERCNow {get; set;}
    // 合計：新しい繰入比率
    public Decimal totalZINVPERCNew {get; set;}
    // 入力パスワード
    public String inputPassWord {get; set;}
    // 新入力パスワード
    public String newPassWord { get; set; }
    // 確認入力パスワード
    public String verPassWord { get; set; }
    // 仮パスワード
    public Boolean isTempPassWord {get; set;}
    // 権限設定
    private E_IDCPF__c idcpf;
    // パスワード周りのエラー判定
    private Boolean hasPassWordError;
    // リターン画面
    private Pagereference returnPage;
    // 同意画面リターン画面
    private Pagereference agreeReturnPage;
    // 保険契約ヘッダレコード
    private E_Policy__c record;
    // 変額ファンドリスト
    private List<E_SVFPF__c> svfpfList;
    // 変更後の表用マップ
    private Map<ID, String> updateDataMap;
    // エラーメッセージコード：同意チェックがされていない
    private String errMsgNotCheckAgree;
    // エラーメッセージコード：合計割合が100でない
    private String errMsgTotalNot100;
    // エラーメッセージコード：数字以外が入力されている
    private String errMsgNotNumber;
    // エラーメッセージコード：繰入比率の割合が変わっていない
    private String errMsgNotValueChange;
    // エラーメッセージコード：手続きパスワードが違う(非仮パスワード時)
    private String errMsgNotSamePw;
    // エラーメッセージコード：手続きパスワードが未入力
    private String errMsgRequiredPw;
    //エラーメッセージコード：小数が入力
    private String errMixedDecimal;
    // 前画面
    private Pagereference prevPage;
    // 画面制御：申込
    private Boolean isEntry = false;
    // 画面制御：確認
    private Boolean isConfirm = false;
    // 割合が変わってない入力項目判定
    private List<Boolean> isNoChangeds;
    // 入力されている値の数を取得
    private Integer notNullCount;
    //特別勘定：ファンド名（メール送信）
    private  Map<Id,String> emailMap;
    //特別勘定
    List<E_SVCPF__c> svcpfList;
    //ページタイトル
    private static final String PAGE_TITLE = '繰入比率の変更';
	//合計行定数100
	private static final Integer  NUMBER_HUNDRED = 100;
	//変数
	Decimal outputZINVPERC;

/*--------------------------------------------------------------------------------------------------------*/
	//コンストラクタ
    public E_InwardTransferExtender(E_InwardTransferController extension){
        super();
        this.extension = extension;
        this.inwardList = extension.E_SVFPF_c_E_Policy_c;
        this.pgTitle = PAGE_TITLE;
    }
/*--------------------------------------------------------------------------------------------------------*/
    global override void init(){
		this.isAgree = false;
		// レコードセット
		this.record = extension.record;
		// アクセスチェック
		pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
		if (pageRef == null) {
            // 契約者、被保険者、年金受取人情報を追加取得
            E_ContactDaoWithout.fillContactsToPolicy(extension.record, true, false,true,false, false);  				
			try{
		    	// リターンページ設定
		    	setReturnPage();
		    	// メッセージコード設定
		    	setMsgCode();
				// 顧客情報(顧客番号とお客様名をセット)
					this.contactCLNTNUM = this.record.Contractor__r.E_CLTPF_CLNTNUM__c;
					this.contactName = this.record.Contractor__r.Name;
					this.contactKANA = this.record.Contractor__r.E_CLTPF_ZCLKNAME__c;
				// データテーブル削除
					for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
				// 申込テーブル
				 	this.inwardList.items[i].remove();
				}
		    	// 変額保険ファンドデータテーブル生成（レコードタイプ：個人保険）
		    	createDatatable();
		    	// 新しい繰入比率に100をセット
		    	this.totalZINVPERCNew = 100;
	    		// ログインユーザに紐付くID管理情報取得
				this.idcpf = E_IDCPFDao.getRecsById(UserInfo.getUserId());
				// ID管理.手続パスワード変更日　で仮パスワードかの判定
				this.isTempPassWord = E_UtilPasswordChange.getIsTempPassWord(this.idcpf);
	    	}catch(Exception e){
	    		//SkyEditor2.Messages.addErrorMessage(e.getMessage());
	    		pageMessages.addErrorMessage(e.getMessage());
	    		System.debug('ヌル⇒'+e.getStackTraceString());
	    	}
    	 }
    }
/*--------------------------------------------------------------------------------------------------------*/
   	/* PageAction */
    public Pagereference pageAction(){
    	if (pageRef == null) {
	    	// 不正遷移チェック
			pageRef = doCheckPageTransition(ApexPages.currentPage().getUrl(), Page.E_InwardTransferAgree, this.isAgree);
    	}
		return E_Util.toErrorPage(pageRef, null);
    }
/*--------------------------------------------------------------------------------------------------------*/
	/* 個別チェック処理 */
	public override Boolean isValidate() {
		Boolean isTransferable  = false;
		isTransferable = this.record.COMM_ZINVDCF__c;
		return getIsTrCodeFundRatio() && isTransferable;
	}
/*--------------------------------------------------------------------------------------------------------*/
	/* [同意/完了画面]戻るボタン押下処理 */
	public override Pagereference doReturn(){
		pageMessages.clearMessages();
		return this.returnPage;
	}
	/* [同意画面]戻るボタン押下処理 */
	public Pagereference doAgreeReturn(){
		pageMessages.clearMessages();
		return this.agreeReturnPage;
	}
/*--------------------------------------------------------------------------------------------------------*/
    /* [申込/確認画面]キャンセルボタン押下処理 */
    public Pagereference doCancel(){
    	pageMessages.clearMessages();
    	Pagereference pref = this.prevPage;

		// DF-000662 対応
        if(backUpInwardListRecords != null){
	    	// DF-000458 対応
	        // 変額保険ファンドの切り戻し
	        for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
	            this.inwardList.items[i].remove();
	        }
	        for(E_InwardTransferController.E_SVFPF_c_E_Policy_cItem svfpfItem : backUpInwardListRecords){
	            extension.E_SVFPF_c_E_Policy_c.add(svfpfItem.record);
	        }
        }

    	// 前画面が同意画面の場合、同意チェックをFalseにする
    	if(String.valueOf(this.prevPage) == String.valueOf(Page.E_InwardTransferAgree)){
			for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
                E_InwardTransferController.E_SVFPF_c_E_Policy_cItem item = this.inwardList.items[i];
                item.record.InputZINVPERC__c = null;
            }
    		this.isAgree = false;
			this.isEntry = false;
    	}else if(String.valueOf(this.prevPage) == String.valueOf(Page.E_InwardTransferEntry)){
			this.prevPage = Page.E_InwardTransferAgree;
    	}
    	return pref;
    }
/*--------------------------------------------------------------------------------------------------------*/
	/* [同意画面]同意ボタン押下処理 */
	public Pagereference doNextEntry(){
		pageMessages.clearMessages();
		// 同意チェックボックス判定
		if(this.isAgree){
			this.prevPage = Page.E_InwardTransferAgree;
			this.isEntry = true;
			return Page.E_InwardTransferEntry;
		}else{
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(this.errMsgNotCheckAgree)));
			pageMessages.addErrorMessage(getMSG().get(this.errMsgNotCheckAgree));
			return null;
		}
	}
/*--------------------------------------------------------------------------------------------------------*/
	/* [申込画面]次へボタン押下処理 */
	public Pagereference doNextConfirm(){
		try{
			pageMessages.clearMessages();
			Decimal decZINVPERC;
			Decimal totalZINVPERC = 0;
			Integer errCnt = 0;
			notNullCount = 0;
			isNoChangeds = New List<Boolean>();
			if(this.isAgree && this.isEntry){
				// 入力チェック処理
				for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
					String zinperc;
					Boolean hasError = false;
					E_InwardTransferController.E_SVFPF_c_E_Policy_cItem item = this.inwardList.items[i];
					// 未入力の場合、「0」をセット
					if(item.record.InputZINVPERC__c == null){
						zinperc = '0';
					}else{
						zinperc = item.record.InputZINVPERC__c;
					}
					// レコード毎の入力チェック
					hasError = validateCheck(item.record);

					// エラーあり
					if(hasError){
						errCnt ++;
					// エラーなし
					}else{
						// 新しい繰入比率の合計
						totalZINVPERC = totalZINVPERC + Decimal.valueOf(zinperc);
					}
				}

				// 整数値以外エラー
				if(errCnt > 0){
					pageMessages.addErrorMessage(getMSG().get(this.errMsgNotNumber));
					return null;
				}
				// [入力チェック]新しい繰入比率の合計割合<>100%
				if(totalZINVPERC <> 100){
					errCnt ++;
					//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(this.errMsgTotalNot100)));
					pageMessages.addErrorMessage(getMSG().get(this.errMsgTotalNot100));
					return null;
				}
				//[入力チェック]全入力項目について割合が変わっていない場合、エラー
			    if(notNullCount == isNoChangeds.size() && notNullCount != 0){
					errCnt++;
					//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(this.errMsgNotValueChange)));
					pageMessages.addErrorMessage(getMSG().get(this.errMsgNotValueChange));
			    	return null;
			    }
				// 新しい繰入比率
				//this.totalZINVPERCNew = totalZINVPERC;
				// エラーなし
				if(errCnt == 0){
					// 変更後の表用のマップ生成
					this.updateDataMap = new Map<ID, String>();
					for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
						E_InwardTransferController.E_SVFPF_c_E_Policy_cItem item = this.inwardList.items[i];
						if(!(item.record.InputZINVPERC__c == null || ''.equals(item.record.InputZINVPERC__c))){
							this.updateDataMap.put(item.record.E_SVCPF__c, item.record.InputZINVPERC__c);
						}
					}
					// MapをJSON型へ変換
					this.jsonUpdateData = JSON.serialize(this.updateDataMap);
					this.prevPage = Page.E_InwardTransferEntry;
					this.isConfirm = true;

	                // DF-000458 対応
	                this.backUpInwardListRecords = new List<E_InwardTransferController.E_SVFPF_c_E_Policy_cItem>();
	                this.backUpInwardListRecords.addAll(this.inwardList.items);
	                // 積立金の移転の確認画面に表示する特別勘定は、現在保有している特別勘定（変更前）と新たに選択した特別勘定のみを表示
	                for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
	                    if((this.inwardList.items[i].record.ZINVPERC__c == null || this.inwardList.items[i].record.ZINVPERC__c == 0 ) && (String.isBlank(this.inwardList.items[i].record.InputZINVPERC__c) || this.inwardList.items[i].record.InputZINVPERC__c == '0' )){
	                        // 更新対象外とする
	                        this.inwardList.items[i].remove();
	                    }
	                }
					return Page.E_InwardTransferConfirm;
				// エラーあり
				}else{
					return null;
				}
			// 不正遷移
			}else {
				pageRef = Page.E_ErrorPage;
    			pageRef.getParameters().put('code', E_Const.INWARD_TRANSFER_NOT_AGREED);
				return E_Util.toErrorPage(pageRef, null);
			}
		}catch(Exception e){
			System.debug(e.getStackTraceString());
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			pageMessages.addErrorMessage(e.getMessage());
			return null;
		}
	}
/*--------------------------------------------------------------------------------------------------------*/
	/* [確認画面]実行ボタン押下処理 */
	public Pagereference doConfirmSave(){
		Savepoint sp = Database.setSavepoint();
		try{
			pageMessages.clearMessages();
			if(this.isAgree && this.isEntry && this.isConfirm){
				/*
				*パスワード判定処理
				*/
				// パスワード判定フラグ
				Boolean hasErrorPW = false;
				//現在日時の文字列変換
				Datetime dt = datetime.now();
				// 手続サービス用パスワードが初期パスワード
				if(isTempPassWord){
					// 入力されたパスワードのチェック
					List<String> errMsgList = E_UtilPasswordChange.validatePassword(this.idcpf, this.inputPassWord, this.newPassWord, this.verPassWord);
					if(errMsgList.size() > 0){
						// エラーメッセージセット
						for(String errMsg : errMsgList){
							pageMessages.addErrorMessage(errMsg);
						}

						hasErrorPW = true;
						//エラーメッセージありのため終了
						return null;
					}

					// 入力チェックでエラーがない場合、ID管理の手続サービス用パスワードを更新
					if(!hasErrorPW){
		    			// 手続サービス用パスワード更新
		    			E_UtilPasswordChange.updateProceduralPassword(this.idcpf, this.newPassword);
		    			/*
		    			this.idcpf.ZPASSWD02__c = E_EncryptUtil.encrypt(this.newPassword);
		    			this.idcpf.ZSTATUS02__c = E_Const.PW_STATUS_OK;
		    			this.idcpf.ZPASSWD02DATE__c = String.valueOf(dt.format('yyyyMMdd'));
		    			update this.idcpf;
		    			*/
    					// 顧客情報変更受付履歴情報登録
	   					E_ADRPF__c adrpf = new E_ADRPF__c();
						adrpf.ReceptionDatetime__c = String.valueOf(dt.format('yyyy年MM月dd日HH時mm分'));	// 受付日時
						adrpf.E_IDCPF__c = this.idcpf.Id;				// ID管理
						adrpf.CLNTNUM__c = this.contactCLNTNUM;			// 顧客番号
						adrpf.ContactName__c = this.contactName;		// お客様氏名
						adrpf.ContactNameKana__c = this.contactKANA;	// お客様フリガナ
						adrpf.Type__c = E_Const.MAIL_TYPE_2P;			// 手続き種別
						adrpf.EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_TRANSFER).Email__c;// 担当者メールアドレス
						adrpf.Kbn__c = E_Const.KBN_2PPW;				// 区分
						adrpf.MailNotSend__c = true;
						insert adrpf;

						isTempPassWord = false;
					}
				//手続きサービス用パスワードが初期パスワード以外
				}else{
                    //入力されたパスワードが未入力の場合
                    if(String.isBlank(this.inputPassWord)){
                        hasErrorPW = true;
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(this.errMsgNotSamePw)));
                        pageMessages.addErrorMessage(getMSG().get(this.errMsgRequiredPw));
                        return null;
                    }
					// 半角英数字チェック→そもそも全角登録不可の為、全角入力の場合はPW比較時に一致しない（桁数、英数混在チェックも同）
                    /*
					else if(!E_Util.isEnAlphaNum(this.inputPassWord)){
						hasErrorPW = true;
						//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(this.errMsgNotSamePw)));
						pageMessages.addErrorMessage(getMSG().get(this.errMsgNotSamePw));
						return null;
					}
					*/
					// ID管理の手続きパスワードと入力されたパスワード(暗号化された)を比較
					else if(!(this.idcpf.ZSTATUS02__c == E_Const.PW_STATUS_OK
						//&& this.idcpf.ZPASSWD02__c == E_EncryptUtil.encrypt(this.inputPassWord))){
						&& this.idcpf.RegularProcedurePW__c == E_EncryptUtil.encrypt(this.inputPassWord))){
						hasErrorPW = true;
						//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get(this.errMsgNotSamePw)));
						pageMessages.addErrorMessage(getMSG().get(this.errMsgNotSamePw));
						return null;
					}
				}
				// パスワード処理が正常に完了
				if(!hasErrorPW){
					// データ登録(ファンドスイッチング更新メール：E_SPSPFEmail__c)
					E_SPSPFEmail__c fsemail = setContactInfo(dt);
					insert fsemail;
					fsemail = E_SPSPFEmailDao.getRecById(fsemail.Id);
					// データ登録(ファンドスイッチング更新：E_SPSPF__c)
					List<E_SPSPF__c> spspfList = new List<E_SPSPF__c>();
					
					//2015/9/14 golive後対応 start
					for(Integer i = 0; i < backUpInwardListRecords.size(); i++){
						// 変更後の比率をDecimal型へ
						String strInputZINVPERC = backUpInwardListRecords[i].record.InputZINVPERC__c;
						Decimal decInputZINVPERC = String.isBlank(strInputZINVPERC) ? 0 : Decimal.valueOf(strInputZINVPERC);
						// ファンドスイッチング更新
						E_SPSPF__c spspf = new E_SPSPF__c(
						ZUPDNO__c = fsemail.Name,									// 更新No
						ZEMAILNO__c = fsemail.MailNunmer__c,						// メール送信No
						ZEFUNDCD__c = backUpInwardListRecords[i].record.ZEFUNDCD__c,	// ファンドコード
						ZEACTIME__c = String.valueOf(dt.format('HHmmss')),			// 受付時間
						ZEACDATE__c = String.valueOf(dt.format('yyyyMMdd')),		// 受付日
						TRCDE__c = E_Const.TRCDE_TD05,								// 処理区分
						CHDRNUM__c = this.record.COMM_CHDRNUM__c,					// 証券番号
						ZNEWPERC__c = decInputZINVPERC,								// 変更後の比率
						E_Policy__c = this.record.Id,								// 保険契約ヘッダー
						E_SPSPF__c = fsemail.Id
						);
						spspfList.add(spspf);
					}
					/*
					for(Integer i = this.inwardList.items.size() - 1; i >= 0; i--){
						// 変更後の比率をDecimal型へ
						String strInputZINVPERC = this.inwardList.items[i].record.InputZINVPERC__c;
						Decimal decInputZINVPERC = String.isBlank(strInputZINVPERC) ? 0 : Decimal.valueOf(strInputZINVPERC);

						// 比率が変更されている場合、ファンドスイッチング更新へ登録
						if(decInputZINVPERC != this.inwardList.items[i].record.ZINVPERC__c){
							// ファンドスイッチング更新
							E_SPSPF__c spspf = new E_SPSPF__c(
							ZUPDNO__c = fsemail.Name,									// 更新No
							ZEMAILNO__c = fsemail.MailNunmer__c,						// メール送信No
							ZEFUNDCD__c = this.inwardList.items[i].record.ZEFUNDCD__c,	// ファンドコード
							ZEACTIME__c = String.valueOf(dt.format('HHmmss')),			// 受付時間
							ZEACDATE__c = String.valueOf(dt.format('yyyyMMdd')),		// 受付日
							TRCDE__c = E_Const.TRCDE_TD05,								// 処理区分
							CHDRNUM__c = this.record.COMM_CHDRNUM__c,					// 証券番号
							ZNEWPERC__c = decInputZINVPERC,								// 変更後の比率
							E_Policy__c = this.record.Id,								// 保険契約ヘッダー
							E_SPSPF__c = fsemail.Id
							);
							spspfList.add(spspf);
						}
					}
					*/
					//2015/9/14 golive後対応 end
					insert spspfList;

					// ワークフロー動作条件更新
					fsemail.SendMail__c = true;
					update fsemail;
				}
				//正常に遷移
				return Page.E_InwardTransferComplete;
			//不正遷移
			}else{
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', E_Const.INWARD_TRANSFER_NOT_AGREED);
				return E_Util.toErrorPage(pageRef, null);
			}
		}catch(Exception e){
			//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			pageMessages.addErrorMessage(e.getMessage());
			System.debug('エラー原因'+e.getMessage());
			Database.rollback(sp);
			return null;
		}
	}
/*--------------------------------------------------------------------------------------------------------*/
	/* 不正遷移チェック */
	private PageReference doCheckPageTransition(String url, Pagereference firstPage, Boolean isPrevCheck){
		Pagereference pageRef = null;

		// URLを小文字へ変換
		url = url.toLowerCase();

    	// URLのページが「繰入比率変更同意」画面かを判定
    	if(url.indexOf(firstPage.getUrl()) == -1){
    		// 同意チェックがTrueでない場合Falseをセット
    		if(!isPrevCheck){
 				pageRef = Page.E_ErrorPage;
    			pageRef.getParameters().put('code', E_Const.INWARD_TRANSFER_NOT_AGREED);
    		}
    	}

    	return pageRef;
	}
/*--------------------------------------------------------------------------------------------------------*/
    /* 顧客情報設定 */
    private E_SPSPFEmail__c setContactInfo(Datetime dt){
    	E_SPSPFEmail__c fsemail = new E_SPSPFEmail__c();
		// 顧客情報
		fsemail.ContactNameKana__c = this.record.Contractor__r.E_CLTPF_ZCLKNAME__c;	// contact.カナ名
		fsemail.ContactName__c = this.record.Contractor__r.Name;					// contact.名前
		fsemail.ContactAddress__c = this.record.Contractor__r.E_COMM_ZCLADDR__c;	// contact.漢字住所
		fsemail.ContactGender__c = this.record.Contractor__r.E_CLTPF_ZKNJSEX__c;	// contact.性別
		fsemail.ContactBirth__c = E_Util.formatBirthday(this.record.Contractor__r.E_CLTPF_DOB__c);	// contact.生年月日
		fsemail.ContactTel__c = this.record.Contractor__r.E_CLTPF_CLTPHONE01__c;	// contact.電話番号01
		fsemail.ReceptionDatetime__c = String.valueOf(dt.format('yyyy年MM月dd日 HH時mm分')); 	// 受付日時
		fsemail.CHDRNUM__c = this.record.COMM_CHDRNUM__c;									// 証券番号
		fsemail.CRTABLE__c = this.record.COMM_CRTABLE__c;									// 保険種類
		fsemail.EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_TRANSFER).Email__c;		//担当者メールアドレス
		fsemail.Kbn__c = E_Const.KBN_INWARD;	// 区分
		// 比率情報
		fsemail.TextRatios__c = '';
		for(Integer i = 0; i < this.inwardList.items.size(); i++){
			// 比率セット
			String inputZINVPERC;
			if(this.inwardList.items[i].record.InputZINVPERC__c == null ||
					this.inwardList.items[i].record.InputZINVPERC__c == '0'){
				inputZINVPERC = '-';
			}else{
				inputZINVPERC = this.inwardList.items[i].record.InputZINVPERC__c + '% ';
			}

			fsemail.TextRatios__c += E_Const.EMAIL_SPACE + this.emailMap.get(this.inwardList.items[i].record.E_SVCPF__c) + ' : ' + inputZINVPERC + '\n\n';
		}
		return fsemail;
    }
/*--------------------------------------------------------------------------------------------------------*/
    /* 変額保険ファンドデータテーブル生成 */
    private void createDatatable(){
    	this.totalZINVPERCNow = 0.0;
    	this.outputZINVPERC = 0.0;
    	// 特別勘定
    	svcpfList = new List<E_SVCPF__c>();

    	// 変額保険ファンド
    	List<E_SVFPF__c> svfpfList = new List<E_SVFPF__c>();
		// 特別勘定情報取得
		svcpfList = E_SVCPFDao.getRecsByCOLI();

    	//マップのインスタンス作成
    	eMailMap = new Map<Id, String>();

    	//特別勘定:ファンド名(メール送信)のマップ作成
		for(E_SVCPF__c svcpf : svcpfList){
			eMailMap.put(svcpf.Id,svcpf.ZFUNDNAMEUNIONEmail__c);
		}

		// 変額保険ファンド情報取得
		svfpfList = E_SVFPFDao.getRecsByPolicyIdForCOLIFrInwardTransfer(this.record.Id);
		if(!svcpfList.isEmpty()){
			// 特別勘定をキーとした変額保険ファンドマップ生成
			Map<Id, E_SVFPF__c> svfpfMap = new Map<Id, E_SVFPF__c>();
			for(E_SVFPF__c svfpf : svfpfList){
				svfpfMap.put(svfpf.E_SVCPF__c, svfpf);
			}
			// 特別勘定のデータ件数分、データテーブルに追加
			for(E_SVCPF__c svcpf : svcpfList){
				E_SVFPF__c svfpf = svfpfMap.get(svcpf.Id);
				// 変額保険ヘッダに紐付く特別勘定の存在判定
				if(svfpf != null){
					// 「現在の繰入比率」に『変額ファンドに登録されている値』をセット
					this.outputZINVPERC = svfpf.ZINVPERC__c;
				}else{
					// 「現在の繰入比率」に『0』をセット
					outputZINVPERC = 0;
				}
				// 繰入比率テーブル(※データテーブルに特別勘定の値を表示できないため、変額ファンドの同じ型の項目に特別勘定の値をセットし代用している)
				this.inwardList.add(new E_SVFPF__c(
											CHDRNUM__c = svcpf.ZCOLORCD__c,	     //色コード（【代用項目】 証券番号）
											SVFPFCode__c = svcpf.ZFUNDNAMEUNION__c,					// ファンド名(【代用項目】変額保険ファンドコード)
											ZINVPERC__c = outputZINVPERC,		//繰入比率
											E_SVCPF__c = svcpf.Id,									// 特別勘定
											ZEFUNDCD__c = svcpf.ZEFUNDCD__c							// ファンドコード
				));
				// 現在の割合
				if(this.outputZINVPERC != null){
					this.totalZINVPERCNow += this.outputZINVPERC;
				}
			}
		}
    }
/*--------------------------------------------------------------------------------------------------------*/
	/* [申込画面]入力チェック処理 */
	private Boolean validateCheck(E_SVFPF__c svfpf){
		Boolean hasError = false;
		// 新しい繰入比率に数字以外の値または1％単位以外の値
		if(String.isNotBlank(svfpf.InputZINVPERC__c)
				&& !E_Util.isEnNum(svfpf.InputZINVPERC__c)
				&& E_Util.isDecimal(svfpf.InputZINVPERC__c)){
			hasError = true;
			//svfpf.InputZINVPERC__c.addError(getMSG().get(this.errMsgNotNumber));
		}

		// 0以下の値が入力されている
		if(!hasError && String.isNotBlank(svfpf.InputZINVPERC__c) && Decimal.valueOf(svfpf.InputZINVPERC__c) < 0){
			hasError = true;
		}

		//nullではない入力項目の数を取得
		if(svfpf.InputZINVPERC__c != null) notNullCount++;

		// 現在の比率と同じ値を入力　
		if(!hasError && svfpf.InputZINVPERC__c != null && Decimal.valueOf(svfpf.InputZINVPERC__c) == svfpf.ZINVPERC__c) isNoChangeds.add(true);
		//エラー有無結果判定
		return hasError;
	}
/*--------------------------------------------------------------------------------------------------------*/
	/* リターンページを設定 */
	private void setReturnPage() {
		//this.returnPage = Page.E_InwardTransferAgree;
		this.returnPage = Page.E_Coli;
		this.agreeReturnPage =  Page.E_SpecialAccount;
		// パラメータ
		this.returnPage.getParameters().put('id', this.record.Id);
		this.agreeReturnPage.getParameters().put('id', this.record.Id);
	}
/*--------------------------------------------------------------------------------------------------------*/
    /* メッセージコード設定 */
    private void setMsgCode(){
	   		// エラーメッセージ
	   		this.errMsgNotCheckAgree = 'IER|000';
	   		this.errMsgTotalNot100 = 'IER|001';
			this.errMsgNotNumber = 'IER|003';
			this.errMsgNotValueChange = 'IER|004';
			this.errMsgNotSamePw = 'IER|005';
	        this.errMsgRequiredPw = 'IER|006';
    }
/*--------------------------------------------------------------------------------------------------------*/

}