@isTest
private class TestE_CADPFLkupController {
	
	 private static testMethod void testresultTable() {
        E_CADPFLkupController.resultTable resultTable = new E_CADPFLkupController.resultTable(new List<E_CADPF__c>(), new List<E_CADPFLkupController.resultTableItem>(), new List<E_CADPF__c>(), null);
        resultTable.create(new E_CADPF__c());
        resultTable.doDeleteSelectedItems();
        System.assert(true);
    }

    @isTest(SeeAllData=true)
    private static void testPageMethods() {
        E_CADPFLkupController page;
        page = new E_CADPFLkupController(new ApexPages.StandardController(new E_CADPF__c()));
        page.getOperatorOptions_E_CADPF_c_ZPOSTCD_c();
        page.getOperatorOptions_E_CADPF_c_ZPOSTMARK_c();
		System.assert(true);
    }
	
}