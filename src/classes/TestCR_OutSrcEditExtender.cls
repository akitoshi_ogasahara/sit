@isTest
private class TestCR_OutSrcEditExtender {
	
	// コンストラクタテスト
  	private static testMethod void CROutSrcEditTest001(){
    	CR_OutSrcEditController controller;
        CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
        System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_OUTSRC);
        System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_OUTSRC_Edit);
  	}

  	// initテスト (外部委託参照権限なしエラー)
	private static testMethod void CROutSrcEditTest002(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
				CR_Outsrc__c outsrc = New CR_Outsrc__c();
				
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest();
        }
	}

  	// initテスト２ (レコードId、またはレコード自体が存在しない場合)
	private static testMethod void CROutSrcEditTest003(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){
        	Test.startTest();
				CR_Outsrc__c outsrc = New CR_Outsrc__c();
				
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest(); 	
        } 
	}

	// initテスト３
	private static testMethod void CROutSrcEditTest004(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	ApexPages.currentPage().getParameters().put('parentId', '12345');
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, true);
	        Test.stopTest();
		}
	}

	// 保存
	private static testMethod void CROutSrcEditTest005(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);

        system.runAs(usr){		
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
				CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc();
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	pref.getParameters().put('parentId', agny.Id);				
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	System.assertEquals(target.doSaveEx(), null);
	        Test.stopTest(); 
        } 
	}

	// 保存２
	private static testMethod void CROutSrcEditTest006(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);

        system.runAs(usr){		
	        Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
				CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	System.assertEquals(target.doSaveEx().getUrl(), '/apex/e_croutsrcview?id=' + outsrc.Id);
	        Test.stopTest(); 
        }
	}

	// 参照ページ遷移
	private static testMethod void CROutSrcEditTest007(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
	        Test.startTest();
				CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc();
				
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	pref.getParameters().put('parentId', '12345');
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	System.assertEquals(target.getIsNew(), true);
	        	System.assertEquals(target.getUrlforCancel(), '/apex/e_croutsrcs?id=12345');
	        Test.stopTest(); 
        }
	}

	// 参照ページ遷移２
	private static testMethod void CROutSrcEditTest008(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
				CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcEditController controller = new CR_OutSrcEditController(standardcontroller);
	        	CR_OutSrcEditExtender target = new CR_OutSrcEditExtender(controller);
	        	System.assertEquals(target.getIsNew(), false);
	        	System.assertEquals(target.getUrlforCancel(), '/apex/e_croutsrcview?id=' + outsrc.Id);
	        Test.stopTest();          	
        }		
	}
}