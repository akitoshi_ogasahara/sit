public with sharing class I_LapsedPolicyController extends I_AbstractPolicyController{

	//保険契約ヘッダのリスト
	public List<PolicyListRow> policyList {get; set;}

	//取得リスト行数
	public Integer listRows {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	// コンストラクタ
	public I_LapsedPolicyController(){
		super();
		pageMessages = getPageMessages();

		//表示行数初期化
		listRows = 0;
		//デフォルト行数20行
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		//デフォルト表示フラグfalse
		//募集人
		contactFrag = false;
		//事務所
		officeFlag = false;
		//担当MR
		manegeMRFlag = false;

		// デフォルトソート - 失効日 : 降順
		sortType  = 'lapsedDay';
		sortIsAsc = false;
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = this.menu.Name;
		if(pr==null){
			//ログイン判定
			//社員（募集人・事務所・担当MR）
			if(access.isEmployee()){
				contactFrag = true;
				officeFlag = true;
				manegeMRFlag = true;
			//AH（募集人・事務所）
			}else if(access.isAuthAH()){
				contactFrag = true;
				officeFlag = true;
			//AY（募集人）
			}else if(access.isAuthAY()){
				contactFrag = true;
			}

			if (String.isBlank(searchType)) {
				searchType = getSearchTypeOptions().get(0).getValue();
			}

			makePolicyList();

			//初回は契約応答月でソート
			I_LapsedPolicyController.list_sortType  = sortType;
			I_LapsedPolicyController.list_sortIsAsc = sortIsAsc;
			policyList.sort();
		}

		return pr;
	}

	public void makePolicyList(){
		//保険契約ヘッダ画面表示用リスト
		policyList = new List<PolicyListRow>();

		String condition = createCovpfCondition();
		if (hasViewedByManager) {
			condition += createCovpfConditionByManager();
		}
		//保険契約ヘッダ値取得用リスト
		List<E_COVPF__c> policyRecs = E_COVPFDao.getRecsIRISLapsedPolicy(condition);
		//0件の時エラー
		if(policyRecs.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('LIS|002'));
			return;
		}
		//最大件数以上の場合は警告を表示する
		if(policyRecs.size() >= I_Const.LIST_MAX_ROWS){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS).format()});
			pageMessages.addWarningMessage(errmessage);
			//return;
		}
		// アクティブメニューキー（URLに付与することでアクティブメニューを保持）
		String irisMenu = '&' + getIRISMenuParameter();
		for(E_COVPF__c pol : policyRecs){
			PolicyListRow plr = new PolicyListRow();
			// 各項目をセット
			//契約者
			plr.contractor = pol.E_Policy__r.ContractorName__c;
			//契約者カナ名
			plr.contractorSort = pol.E_Policy__r.ContractorE_CLTPF_ZCLKNAME__c;		//スプリントバックログ9(2017/1/18) カナソート用追記
			//証券番号
			plr.policyNum = pol.E_Policy__r.COMM_CHDRNUM__c;
			//契約日
			plr.contractDay = E_Util.formatBirthday(pol.E_Policy__r.COMM_OCCDATE__c);
			//保険料
			if(pol.E_Policy__r.COMM_SINSTAMT__c != null) plr.policyFeeDec = pol.E_Policy__r.COMM_SINSTAMT__c;
			plr.policyFee = plr.policyFeeDec.format();
			//事務所--スプリントバックログ9(2017/1/18)
			plr.setValueOffice(pol.E_Policy__r.MainAgentAccountName__c, pol.E_Policy__r.SubAgentAccountName__c,pol.E_Policy__r.MainAgentAccountNameKana__c);
			//plr.setValueOffice(pol.E_Policy__r.MainAgentAccountName__c, pol.E_Policy__r.SubAgentAccountName__c);
			//募集人--スプリントバックログ9(2017/1/18)
			plr.setValueAgent(pol.E_Policy__r.MainAgentName__c, pol.E_Policy__r.SubAgentName__c,pol.E_Policy__r.MainAgentNameKana__c);
			//plr.setValueAgent(pol.E_Policy__r.MainAgentName__c, pol.E_Policy__r.SubAgentName__c);
			//MR名
			plr.setValueMRName(pol.E_Policy__r.MainAgentAccountMRName__c,pol.E_Policy__r.SubAgentAccountMRName__c);
			//'/E_Coli?id=' + SFID
			plr.pageURL += pol.E_Policy__r.Id + irisMenu;
			//失効日
			plr.lapsedDay  = E_Util.formatBirthday(pol.COMM_STATDATE__c);
			//保険種類
			//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類設定項目を変更
			plr.policyType = pol.COLI_ZCOVRNAMES__c;

			//リストに足す
			policyList.add(plr);

		}
		listRows = policyList.size();
	}

	// 画面表示行ラッパークラス
	public class PolicyListRow implements Comparable{
		// 失効日
		public String lapsedDay {get; set;}
		// 契約者
		public String contractor {get; set;}
		// 保険種類
		public String policyType {get; set;}
		//保険料
		public Decimal policyFeeDec {get; set;}
		//画面用保険料
		public String policyFee {get; set;}
		//証券番号
		public String policyNum {get; set;}
		//契約日
		public String contractDay {get; set;}
		// 募集人
		public String agent {get; set;}
		//事務所
		public String office {get; set;}
		// MR名
		public String mrName {get; set;}
		//URL
		public String pageURL{get; set;}

		//スプリントバックログ9(2017/1/18) カナソート用追記
		// 契約者カナ名
		public String contractorSort {get; set;}
		//募集人カナ名
		public String agentSort{get; set;}
		//代理店事務所カナ名
		public String officeSort{get; set;}

		// コンストラクタ
		public PolicyListRow(){
			lapsedDay  = '';
			contractor = '';
			policyType = '';
			policyNum = '';
			contractDay = '';
			office = '';
			agent = '';
			mrName = '';
			//pageURL = 'E_Coli?id=';
			pageURL = 'IRIS_Coli?id=';
			policyFee = '';
			policyFeeDec = 0;

			//スプリントバックログ9(2017/1/18) カナソート用追記
			contractorSort = '';
			agentSort	   = '';
			officeSort	   = '';
		}

		//事務所セット--スプリントバックログ9(2017/1/18)
		//public void setValueOffice(String main, String sub){
		public void setValueOffice(String main, String sub,String mainkana){
			// （共同募集）主・従が空白でない かつ 主・従の値が異なる場合は「(複数)」をセット
			if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
				office = I_Const.OL_MULTI_DATA;
				officeSort = I_Const.OL_MULTI_DATA;
			// 上記以外の場合は主をセット
			}else{
				office = main;
				officeSort = mainkana;
			}
			return;
		}


		// 募集人セット--スプリントバックログ9(2017/1/18)
		//public void setValueAgent(String main, String sub){
		public void setValueAgent(String main, String sub,String mainkana){
			// 主、従の値が異なる場合は「(複数)」をセット
			if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
				agent = I_Const.OL_MULTI_DATA;
				agentSort = I_Const.OL_MULTI_DATA;
			// 上記以外の場合は主をセット
			}else{
				agent = main;
				agentSort = mainkana;
			}
			return;
		}

		//担当MR名セット
		public void setValueMRName(String main, String sub){
			// 主、従の値が異なる場合は「(複数)」をセット
			if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
				mrName = I_Const.OL_MULTI_DATA;
			// 上記以外の場合は主をセット
			}else{
				mrName = main;
			}
			return;
		}

		// ソート
		public Integer compareTo(Object compareTo){
			PolicyListRow compareToRow = (PolicyListRow)compareTo;
			Integer result = 0;
			//保険種類
			if(list_sortType == 'policyType'){
				result = I_Util.compareToString(policyType, compareToRow.policyType);
			//事務所
			}else if(list_sortType == 'office'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(officeSort, compareToRow.officeSort);
				//result = I_Util.compareToString(office, compareToRow.office);
			// 募集人
			} else if(list_sortType == 'agent'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(agentSort, compareToRow.agentSort);
				//result = I_Util.compareToString(agent, compareToRow.agent);
			// MR名
			}else if(list_sortType == 'mrName'){
				result = I_Util.compareToString(mrName, compareToRow.mrName);
			//失効日
			}else if(list_sortType == 'lapsedDay'){
				result = I_Util.compareToString(lapsedDay, compareToRow.lapsedDay);
			//契約者名前
			}else if(list_sortType == 'contractor'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(contractorSort, compareToRow.contractorSort);
				//result = I_Util.compareToString(contractor, compareToRow.contractor);
			//保険料
			}else if(list_sortType == 'policyFee'){
				result = I_Util.compareToDecimal(policyFeeDec, compareToRow.policyFeeDec);
			//証券番号
			}else if(list_sortType == 'policyNum'){
				result = I_Util.compareToString(policyNum, compareToRow.policyNum);
			//契約日
			}else if(list_sortType == 'contractDay'){
				result = I_Util.compareToString(contractDay, compareToRow.contractDay);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		//現在の表示行数+20
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		//現在の表示行数が1000より大きくなった場合、表示行数は1000
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * ソート
	 */
	public PageReference sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');
		//押されたのが同じ項目だった場合逆に、違う項目だった場合昇順に
		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}

		I_LapsedPolicyController.list_sortType  = sortType;
		I_LapsedPolicyController.list_sortIsAsc = sortIsAsc;
		policyList.sort();
		return null;
	}

	//総行数
	public String getIsTotalRows(){

		//スプリントバックログ29(2017/1/18) 1000件の時1000+件表示
		String listSize = policyList.size().format();

		if(policyList.size() >= I_Const.LIST_MAX_ROWS){
			listSize += ' +';
		}

		return listSize;

		//return policyList.size().format();
	}

	//メニューキー
	protected override String getLinkMenuKey(){
		return 'invalidationList';
	}



}