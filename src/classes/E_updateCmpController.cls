public with sharing class E_updateCmpController extends E_AbstractController{

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get; set;}

	//完了メッセージ
	public String compMsg {get; set;}
	//Update対象
	public String updateItem {get; set;}
	//メッセージ表示フラグ
	public Boolean formFlg {get; set;}

	public E_updateCmpController() {
		pageMessages = getPageMessages();
	}

	public PageReference init(){
		formFlg = true;

		//ユーザレコード取得
		User userRec = E_UserDaoWithout.getUserRecByUserId(UserInfo.getUserId());
		if(userRec == null){
			pageMessages.addErrorMessage('ISS|001');
			return null;
		}

		//アカウント種類
		String accType = ApexPages.currentPage().getParameters().get('type');

		//パラメータにより表示メッセージを変更
		if(accType == 'gwmail'){
			compMsg = getMSG().get('ACC|006');
			updateItem = 'メールアドレス';
		} else {
			compMsg = getMSG().get('UEC|001');
			pageMessages.addErrorMessage(compMsg);
			formFlg = false;
		}

		pageRef = doAuth(E_Const.ID_KIND.LOGIN, userRec.Id);
		return E_Util.toErrorPage(pageRef, null);
	}

	//戻るボタン押下
	public PageReference backPage(){
		return Page.E_LoginAgent;
	}

	private PageReference getBackPage() {
		if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_CLI)) {
			return Page.E_LoginCustomer;
		} else if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_AGE)) {
			return Page.E_LoginAgent;
		} else {
			return Page.E_Exception;
		}
	}
}