global with sharing class E_FundTransferTransitionExtender extends E_AbstractViewExtender {
	private static final String PAGE_TITLE_SPVA = '積立金と死亡給付金額等の推移グラフ';
	private static final String PAGE_TITLE_SPVWL = '積立金と死亡保険金額等の推移グラフ';
    /** 保険種類SR */
    private static final String COMM_CRTABLE_SR = 'SR';
	
	E_FundTransferTransitionController extension;
	
	/** コンストラクタ */
	public E_FundTransferTransitionExtender(E_FundTransferTransitionController extension){
		super();
		this.extension = extension;
	}

	/** init */
	global override void init(){
		pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
        // タイトルの設定
        if(extension.record.COMM_CRTABLE2__c != null) {
        	if (extension.record.COMM_CRTABLE2__c.equals(COMM_CRTABLE_SR)){
            this.pgTitle = PAGE_TITLE_SPVWL;
	        }else{
	        	this.pgTitle = PAGE_TITLE_SPVA;
	        }
        }
	}

	/** ページアクション */
	public PageReference pageAction () {
		System.debug('*************************2' + pageRef);
		return E_Util.toErrorPage(pageRef, null);
	}
}