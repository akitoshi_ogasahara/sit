@isTest
private class TestE_InveTransitionExtender {
	
	//コンストラクタ
	static testMethod void constructorTest001(){
		
		E_ITHPF__c record = new E_ITHPF__c();
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveTransitionController extension = new E_InveTransitionController(controller);
		
		Test.startTest();
		E_InveTransitionExtender extender = new E_InveTransitionExtender(extension);
		Test.stopTest();
		
	}
	
	//init
	//doAuth成功
	static testMethod void initTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveTransitionExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
			
		}
	}
	
	//init
	//doAuth失敗
	static testMethod void initTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveTransitionExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
			
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveTransitionExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			system.assertNotEquals(null, result);
		}
	}
	
	//ページアクション
	//doAuth失敗
	static testMethod void pageActionTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveTransitionExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			system.assertNotEquals(null, result);
		}
	}
	
	//テストユーザ作成
	static User createUser(String flag03){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG03__c = flag03;
		insert idcpf;
		return u;
	}
	
	//Extender作成
	static E_InveTransitionExtender createExtender(){
		E_ITHPF__c record = new E_ITHPF__c();
		insert record;
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveTransitionController extension = new E_InveTransitionController(controller);
		E_InveTransitionExtender extender = new E_InveTransitionExtender(extension);
		return extender;
	}
}