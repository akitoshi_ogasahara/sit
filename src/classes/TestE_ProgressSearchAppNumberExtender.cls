@isTest
private class TestE_ProgressSearchAppNumberExtender {
	
	private static testMethod void TestNulliProductNum(){
		//テストデータを作成
		createMessageMaster_Messages(true);
		
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        PageReference pref = Page.E_ProgressSearchAppNumberSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAppNumberController controller = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAppNumberExtender extender = controller.getExtender();
            
            String iProductNum1 = controller.iProductNum1_val.SkyEditor2__Text__C = '';
            String iProductNum2 = controller.iProductNum2_val.SkyEditor2__Text__C = '';
            String iProductNum3 = controller.iProductNum3_val.SkyEditor2__Text__C = '';
            String iProductCd1 = controller.iProductCd1_val.SkyEditor2__Text__C = '02';
			String iProductCd2 = controller.iProductCd2_val.SkyEditor2__Text__C = '2';
			String iProductCd3 = controller.iProductCd3_val.SkyEditor2__Text__C = '';
            

            try{
	            extender.preSearch(); 
			}catch(SkyEditor2.ExtenderException e){
				system.debug(e);
            }		            
            
            String errMsg= extender.getMSG().get('PGJ|001');
            system.debug('@@@errMsg1= '+ errMsg);
            
            String actMsg = extender.pageMessages.getErrorMessages().get(0).summary;
            system.debug('@@@actMsg1= '+ actMsg);
            
            system.assertEquals(errMsg, actMsg);
        	
        //テスト終了
        Test.stopTest();
        }
	}
    
    
    private static testMethod void TestnotIProductNum(){
		//テストデータを作成
		createMessageMaster_Messages(true);
		
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        PageReference pref = Page.E_ProgressSearchAppNumberSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAppNumberController controller = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAppNumberExtender extender = controller.getExtender();
            
            String iProductNum1 = controller.iProductNum1_val.SkyEditor2__Text__C = '12345';
            String iProductNum2 = controller.iProductNum2_val.SkyEditor2__Text__C = '123456789';
            String iProductNum3 = controller.iProductNum3_val.SkyEditor2__Text__C = 'ab1234567';
            String iProductCd1 = controller.iProductCd1_val.SkyEditor2__Text__C = '2';
			String iProductCd2 = controller.iProductCd2_val.SkyEditor2__Text__C = '02';
			String iProductCd3 = controller.iProductCd3_val.SkyEditor2__Text__C = '';
            
            try{
	            extender.preSearch(); 
			}catch(SkyEditor2.ExtenderException e){
				system.debug(e);
            }		            
            
            String errMsg= extender.getMSG().get('PGJ|002');
            system.debug('@@@errMsg2= '+ errMsg);
            
            String actMsg = extender.pageMessages.getErrorMessages().get(0).summary;
            system.debug('@@@actMsg2= '+ actMsg);
            
            system.assertEquals(errMsg, actMsg);
        	
        //テスト終了
        Test.stopTest();
        }
	}
	
	private static testMethod void TestproductNumAndNotIProductCd(){
		//テストデータを作成
		createMessageMaster_Messages(true);
		
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        PageReference pref = Page.E_ProgressSearchAppNumberSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAppNumberController controller = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAppNumberExtender extender = controller.getExtender();
            
            String iProductNum1 = controller.iProductNum1_val.SkyEditor2__Text__C = '123456789';
            String iProductNum2 = controller.iProductNum2_val.SkyEditor2__Text__C = '';
            String iProductNum3 = controller.iProductNum3_val.SkyEditor2__Text__C = '';
            String iProductCd1 = controller.iProductCd1_val.SkyEditor2__Text__C = '';
			String iProductCd2 = controller.iProductCd2_val.SkyEditor2__Text__C = '02';
			String iProductCd3 = controller.iProductCd3_val.SkyEditor2__Text__C = '02';
		
            try{
	            extender.preSearch(); 
			}catch(SkyEditor2.ExtenderException e){
				system.debug(e);
            }		            
            
            String errMsg= extender.getMSG().get('PGJ|003');
            system.debug('@@@errMsg3= '+ errMsg);
            
            String actMsg = extender.pageMessages.getErrorMessages().get(0).summary;
            system.debug('@@@actMsg3= '+ actMsg);
            
            system.assertEquals(errMsg, actMsg);
        	
        //テスト終了
        Test.stopTest();
        }
	}
	
	private static testMethod void TestnotIProductCd(){
		//テストデータを作成
		createMessageMaster_Messages(true);
		
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        PageReference pref = Page.E_ProgressSearchAppNumberSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAppNumberController controller = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAppNumberExtender extender = controller.getExtender();
            
            String iProductNum1 = controller.iProductNum1_val.SkyEditor2__Text__C = '123456789';
            String iProductNum2 = controller.iProductNum2_val.SkyEditor2__Text__C = '';
            String iProductNum3 = controller.iProductNum3_val.SkyEditor2__Text__C = '';
            String iProductCd1 = controller.iProductCd1_val.SkyEditor2__Text__C = '2';
			String iProductCd2 = controller.iProductCd2_val.SkyEditor2__Text__C = '02';
			String iProductCd3 = controller.iProductCd3_val.SkyEditor2__Text__C = '';
		

            try{
	            extender.preSearch(); 
			}catch(SkyEditor2.ExtenderException e){
				system.debug(e);
            }		            
            
            String errMsg= extender.getMSG().get('PGJ|004');
            system.debug('@@@errMsg4= '+ errMsg);
            
            String actMsg = extender.pageMessages.getErrorMessages().get(0).summary;
            system.debug('@@@actMsg4= '+ actMsg);
            
            system.assertEquals(errMsg, actMsg);
        	
        //テスト終了
        Test.stopTest();
        }
	}
	
	private static testMethod void TestpreSearch(){
		//テストデータを作成
		createMessageMaster_Messages(true);
		
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        PageReference pref = Page.E_ProgressSearchAppNumberSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAppNumberController controller = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAppNumberExtender extender = controller.getExtender();
            
            String iProductNum1 = controller.iProductNum1_val.SkyEditor2__Text__C = 'ab1000000';
            String iProductNum2 = controller.iProductNum2_val.SkyEditor2__Text__C = 'ab2000000';
            String iProductNum3 = controller.iProductNum3_val.SkyEditor2__Text__C = 'ab3000000';
            String iProductCd1 = controller.iProductCd1_val.SkyEditor2__Text__C = '01';
			String iProductCd2 = controller.iProductCd2_val.SkyEditor2__Text__C = '02';
			String iProductCd3 = controller.iProductCd3_val.SkyEditor2__Text__C = '03';
			
			extender.init();
			extender.pageAction ();
			extender.preSearch();
		
            system.assertEquals('ab1000000', controller.iProductNum1_val.SkyEditor2__Text__C);
            system.assertEquals('ab2000000', controller.iProductNum2_val.SkyEditor2__Text__C);
            system.assertEquals('ab3000000', controller.iProductNum3_val.SkyEditor2__Text__C);
            
        	
        //テスト終了
        Test.stopTest();
        }
	}
	
	private static testMethod void TestafterSearch(){
		//テストデータを作成
		createMessageMaster_Messages(true);
		
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        PageReference pref = Page.E_ProgressSearchAppNumberSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAppNumberController controller = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAppNumberExtender extender = controller.getExtender();
            
            String iProductNum1 = controller.iProductNum1_val.SkyEditor2__Text__C = 'ab1000000';
            String iProductNum2 = controller.iProductNum2_val.SkyEditor2__Text__C = 'ab2000000';
            String iProductNum3 = controller.iProductNum3_val.SkyEditor2__Text__C = 'ab3000000';
            String iProductCd1 = controller.iProductCd1_val.SkyEditor2__Text__C = '01';
			String iProductCd2 = controller.iProductCd2_val.SkyEditor2__Text__C = '02';
			String iProductCd3 = controller.iProductCd3_val.SkyEditor2__Text__C = '03';
			
            extender.init();
			extender.pageAction ();
			extender.preSearch();
			extender.afterSearch();
            
            system.assertEquals('ab1000000', controller.iProductNum1_val.SkyEditor2__Text__C);
            system.assertEquals('ab2000000', controller.iProductNum2_val.SkyEditor2__Text__C);
            system.assertEquals('ab3000000', controller.iProductNum3_val.SkyEditor2__Text__C);
            system.assertEquals('01', controller.iProductCd1_val.SkyEditor2__Text__C);
            system.assertEquals('02', controller.iProductCd2_val.SkyEditor2__Text__C);
            system.assertEquals('03', controller.iProductCd3_val.SkyEditor2__Text__C);
            
        	
        //テスト終了
        Test.stopTest();
        }
	}
	
	
	
	
 /*********************************************************************************************************************
 * テストデータ作成用
 *********************************************************************************************************************/
    /**
     * RecordTypeマップ取得
     * @return Map<String, Map<String, Id>>: Map(sObjectType, Map(RecTypeDevName,RecTypeID)
     */
    private static Map<String, Map<String, Id>> rMap;
    private static Map<String, Map<String, Id>> getRecTypeIdMap(){
        if(rMap != null) return rMap;
        rMap = new Map<String, Map<String, Id>>();
        for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
            if(rMap.containsKey(rt.sObjectType)){
                rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
            }else{
                rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
            }
        }
        return rMap;
    }
    

    private static E_MessageMaster__c[] createMessageMaster_Messages(Boolean isInsert) {
        Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get('MSG_DISCLAIMER');
        final String TYPE_MESSAGE = 'メッセージ';
        final String MSG_NAME = 'name';
        List<E_MessageMaster__c> src = new List<E_MessageMaster__c>{
              new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= 'PGJ|001', Value__c='検索する申込番号を入力してください。')            
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= 'PGJ|002', Value__c='申込番号は半角英数9文字で入力してください。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= 'PGJ|003', Value__c='検索する商品コードを入力してください。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= 'PGJ|004', Value__c='商品コードは半角英数2文字で入力してください。')
            
            
        };
        if(isInsert){
            insert src;
        }
        return src;
    }

     /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
            

}