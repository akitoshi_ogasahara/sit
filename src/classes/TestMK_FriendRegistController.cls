@isTest
private class TestMK_FriendRegistController {


	private static testMethod void testdoCheckCustomer_001(){
		CampaignManagement__c cm1 = TestMK_TestUtil.createCampaignManagement(true,system.today(),system.today());

		MK_CampaignTarget__c ct1 = TestMK_TestUtil.createMK_CampaignTarget(true,cm1.Id,'TestNo01','テストイチケイヤクシャカナ');
		MK_CampaignTarget__c ct2 = TestMK_TestUtil.createMK_CampaignTarget(true,'TestNo02','テストニケイヤクシャカナ');
		MK_CampaignTarget__c ct3 = TestMK_TestUtil.createMK_CampaignTarget(true,'TestNo02','テストサンケイヤクシャカナ');

		Test.startTest();
		//init処理なし
		MK_FriendRegistController cntrllr = new MK_FriendRegistController();
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG09_URLPARAM_ID);
		//ERROR:Nullチェック 両方未入力
		cntrllr.init();

		//ERROR:Nullチェック 片方入力
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.CLTPF_CLNTNUM__c = 'TestNo01';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG02_IS_NOT_ENTRY);
		//ERROR:Nullチェック 片方入力
		cntrllr = new MK_FriendRegistController();
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.ContractantKana__c = 'ケイヤクシャ';
		cntrllr.campaignTarget.Name = '契約者';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG02_IS_NOT_ENTRY);
		cntrllr.pageMessages.clearMessages();
		//ERROR:Nullチェック 入力値整形結果空白
		cntrllr = new MK_FriendRegistController();
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.CLTPF_CLNTNUM__c = 'TestNo02';
		cntrllr.campaignTarget.ContractantKana__c = 'カブシキガイシャ';
		cntrllr.campaignTarget.Name = '契約者';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG01_IS_NOT_CONFIRM);
		//ERROR:禁則文字チェック
		cntrllr = new MK_FriendRegistController();
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.CLTPF_CLNTNUM__c = '*';
		cntrllr.campaignTarget.ContractantKana__c = 'カブシキガイシャテストイチ';
		cntrllr.campaignTarget.Name = '契約者';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG10_IS_NO_SEND);
		//ERROR:認証処理 該当キャンペーンターゲットが複数件存在
		cntrllr = new MK_FriendRegistController();
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.CLTPF_CLNTNUM__c = ct2.CLTPF_CLNTNUM__c;
		cntrllr.campaignTarget.ContractantKana__c = 'カブシキガイシャテストイチ';
		cntrllr.campaignTarget.Name = '契約者';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG01_IS_NOT_CONFIRM);
		//ERROR:認証処理 入力した顧客名が異なる場合
		cntrllr = new MK_FriendRegistController();
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.CLTPF_CLNTNUM__c = ct1.CLTPF_CLNTNUM__c;
		cntrllr.campaignTarget.ContractantKana__c = 'カブシキガイシャテストニ';
		cntrllr.campaignTarget.Name = '契約者';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG01_IS_NOT_CONFIRM);
		//正常系
		cntrllr = new MK_FriendRegistController();
		cntrllr.pageMessages.clearMessages();
		cntrllr.campaignTarget.CLTPF_CLNTNUM__c = ct1.CLTPF_CLNTNUM__c;
		cntrllr.campaignTarget.ContractantKana__c = ct1.ContractantKana__c;
		cntrllr.campaignTarget.Name = '契約者';
		System.assert(cntrllr.doCheckCustomer() == null);
		System.assert(!cntrllr.pageMessages.hasMessages());

		Test.stopTest();
	}
	private static testMethod void testdoConfirm_001(){
		MK_CampaignTarget__c ct = TestMK_TestUtil.createMK_CampaignTarget(true);
		MK_leadTMP__c ltmp = TestMK_TestUtil.createMK_leadTMP(true,ct.Id,'testaddress01@test.nn.baton');
		MK_FriendRegistController cntrllr = new MK_FriendRegistController();
		cntrllr.campaignTarget = ct;

		Test.startTest();

		//
		cntrllr.pageMessages.clearMessages();
		cntrllr.friends = new List<MK_FriendRegistController.MK_leadTMPDTO>();
		cntrllr.friends.add(new MK_FriendRegistController.MK_leadTMPDTO(ct.id,1));
		cntrllr.friends.add(new MK_FriendRegistController.MK_leadTMPDTO(ct.id,2));
		System.assert(cntrllr.doConfirm() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG02_IS_NOT_ENTRY);
		//
		cntrllr.pageMessages.clearMessages();
		cntrllr.friends[0].mk_leadtmp.Company__c = '法人名';
		cntrllr.friends[0].mk_leadtmp.LastName__c = '姓';
		cntrllr.friends[0].mk_leadtmp.FirstName__c = '名';
		cntrllr.friends[0].mk_leadtmp.Email__c = 'testaddress02@test.nn.baton';
		cntrllr.friends[0].mk_leadtmp.LastNameKana__c = 'せい';
		cntrllr.friends[0].mk_leadtmp.FirstNameKana__c = 'メイ';
		cntrllr.friends[0].RecomfirmEmail = 'testaddress02@test.nn.baton';
		cntrllr.campaignTarget = ct;
		System.assert(cntrllr.doConfirm() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG03_IS_NOT_KATAKANA);
		//
		cntrllr.pageMessages.clearMessages();
		cntrllr.friends[0].mk_leadtmp.Company__c = '法人名@';
		cntrllr.friends[0].mk_leadtmp.LastName__c = '姓';
		cntrllr.friends[0].mk_leadtmp.FirstName__c = '名';
		cntrllr.friends[0].mk_leadtmp.Email__c = 'testaddress02@test.nn.baton';
		cntrllr.friends[0].mk_leadtmp.LastNameKana__c = 'セイ';
		cntrllr.friends[0].mk_leadtmp.FirstNameKana__c = 'メイ';
		cntrllr.friends[0].RecomfirmEmail = 'testaddress02@test.nn.baton';
		cntrllr.campaignTarget = ct;
		System.assert(cntrllr.doConfirm() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG10_IS_NO_SEND);
		//
		cntrllr.pageMessages.clearMessages();
		cntrllr.friends[0].mk_leadtmp.Company__c = '法人名';
		cntrllr.friends[0].mk_leadtmp.LastNameKana__c = 'セイ';
		cntrllr.friends[0].RecomfirmEmail = 'testaddress03@test.nn.baton';
		System.assert(cntrllr.doConfirm() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG05_IS_NOT_RECON_EMAIL);
		//同一メール
		cntrllr.pageMessages.clearMessages();
		cntrllr.friends[0].mk_leadtmp.Email__c = ltmp.Email__c;
		cntrllr.friends[0].RecomfirmEmail = cntrllr.friends[0].mk_leadtmp.Email__c;
//		System.assert(cntrllr.doConfirm() == null);
//		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG06_IS_NOT_SAME_EMAIL);
		//
		cntrllr.pageMessages.clearMessages();

		cntrllr.friends[0].mk_leadtmp.Email__c = 'testaddress02@test.nn.baton';
		cntrllr.friends[0].RecomfirmEmail = cntrllr.friends[0].mk_leadtmp.Email__c;
		TestMK_TestUtil.createMK_leadTMP(true,ct.Id,integer.valueOf(system.label.E_MK_BATON_FRIEND_REGIST_MAX)-2);

		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		System.assert(!cntrllr.pageMessages.hasMessages());

		TestMK_TestUtil.createMK_leadTMP(true,ct.Id);

		cntrllr.pageMessages.clearMessages();
		cntrllr.friends[0].mk_leadtmp.Email__c = 'testaddress03@test.nn.baton';
		cntrllr.friends[0].RecomfirmEmail = cntrllr.friends[0].mk_leadtmp.Email__c;
		System.assert(cntrllr.doConfirm() == null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary
						,String.format(MK_Const.ERR_MSG08_MAX_BATON, new String[]{system.label.E_MK_BATON_FRIEND_REGIST_MAX}));
		Test.stopTest();
	}
	private static testMethod void testdoSave_001(){
		MK_CampaignTarget__c ct1 = TestMK_TestUtil.createMK_CampaignTarget(true);
		TestMK_TestUtil.createMK_leadTMP(true,ct1.Id,integer.valueOf(system.label.E_MK_BATON_FRIEND_REGIST_MAX)-1);
		MK_FriendRegistController cntrllr = new MK_FriendRegistController();

		cntrllr.pageMessages.clearMessages();
		cntrllr.doSave();
		system.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG07_IS_NOT_AGREE);

		cntrllr.pageMessages.clearMessages();

		cntrllr.init();
		MK_leadTMP__c rec1 = TestMK_TestUtil.createMK_leadTMP(false,ct1.id,'testaddress01@test.nn.baton');
		cntrllr.friends = new List<MK_FriendRegistController.MK_leadTMPDTO>();
		cntrllr.friends.add(new MK_FriendRegistController.MK_leadTMPDTO(ct1.id,1));

		cntrllr.friends[0].mk_leadtmp = rec1;
		cntrllr.friends[0].RecomfirmEmail = rec1.Email__c;
		cntrllr.Agree = true;

		cntrllr.campaignTarget = ct1;
		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();

		system.debug('cntrllr.friends[0].mk_leadtmp='+cntrllr.friends[0].mk_leadtmp);
		System.assert(!cntrllr.pageMessages.hasMessages());

		Test.startTest();

		cntrllr.doSave();
		//評価用
		system.assertNotEquals(rec1.Id,null);
		List<MK_leadTMP__c> results = [Select id From MK_leadTMP__c Where MK_CampaignTarget__c =: ct1.Id];
		System.assertEquals(results.size(),integer.valueOf(system.label.E_MK_BATON_FRIEND_REGIST_MAX));

		MK_leadTMP__c rec2 = TestMK_TestUtil.createMK_leadTMP(false,ct1.id,'testaddress02@test.nn.baton');
		cntrllr.friends[0] = new MK_FriendRegistController.MK_leadTMPDTO(ct1.id,1);
		cntrllr.friends[0].mk_leadtmp = rec2;
		cntrllr.friends[0].RecomfirmEmail = rec2.Email__c;
		cntrllr.Agree = true;

		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		cntrllr.doSave();

		System.assertEquals(rec2.Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary
						,String.format(MK_Const.ERR_MSG08_MAX_BATON, new String[]{system.label.E_MK_BATON_FRIEND_REGIST_MAX}));

		MK_CampaignTarget__c ct2 = TestMK_TestUtil.createMK_CampaignTarget(true);

		MK_leadTMP__c rec4 = TestMK_TestUtil.createMK_leadTMP(false,ct2.Id,'testaddress03@test.nn.baton');
		cntrllr.friends[0] = new MK_FriendRegistController.MK_leadTMPDTO(ct2.id,1);
		cntrllr.friends[0].mk_leadtmp = rec4;
		cntrllr.friends[0].RecomfirmEmail = rec4.Email__c;
		cntrllr.campaignTarget = ct2;
		cntrllr.Agree = true;
		system.debug('cntrllr.campaignTarget.id=' + cntrllr.campaignTarget.id);

		cntrllr.pageMessages.clearMessages();
		cntrllr.doConfirm();
		MK_leadTMP__c rec3 = TestMK_TestUtil.createMK_leadTMP(true,ct2.Id,'testaddress03@test.nn.baton');
		cntrllr.doSave();

		system.assertEquals(rec4.Id,null);
		System.assertEquals(cntrllr.pageMessages.getErrorMessages()[0].summary,MK_Const.ERR_MSG06_IS_NOT_SAME_EMAIL);

		cntrllr.friends[0] = new MK_FriendRegistController.MK_leadTMPDTO(ct2.id,1);
		cntrllr.friends[0].mk_leadtmp = rec3;
		cntrllr.friends[0].RecomfirmEmail = rec3.Email__c;

		cntrllr.doConfirm();
		cntrllr.doSave();
	}
	//ページ遷移のみなためアサーション無し
	private static testMethod void testdoEdit_001(){
		MK_FriendRegistController cntrllr = new MK_FriendRegistController();
		cntrllr.doEdit();
	}
}