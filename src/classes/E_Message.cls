/**
 * 共通メッセージクラス
 * CreatedDate 2014/12/03
 * @Author Terrasky
 */
public with sharing class E_Message {

	public static final String TYPE_MESSAGE = 'メッセージ';
	public static final String TYPE_DISCLAIMER = 'ディスクレイマー';

//-------------------------------------------
//		メッセージ(INFO,WARNING,ERROR)
//-------------------------------------------
	private static Map<String, String> msgs;
	public static Map<String, String> getMsgMap(){
		if(msgs==null){
			msgs = E_MessageMasterDao.getMessageMapByType(TYPE_MESSAGE);
		}
		return msgs;
	}

//-------------------------------------------
//		ディスクレーマー
//-------------------------------------------
	private static Map<String, String> disclaimers;
	public static Map<String, String> getDisclaimerMap(){
		if(disclaimers==null){
			disclaimers = E_MessageMasterDao.getMessageMapByType(TYPE_DISCLAIMER);
		}
		return disclaimers;
	}

}