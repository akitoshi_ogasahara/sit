/**
 * 
 */
@isTest
private class TestI_PushOutSendBatch {
	//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';
	
	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];	//Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Contact atCon;
	private static E_BizDataSyncLog__c log;
	
    static testMethod void batch_test01() {
        // Data
        createDataEBizLog(I_Const.EBIZDATASYNC_KBN_PUSHOUT, I_Const.EBIZDATASYNC_KBN_BILA);
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AT');
        I_PushOut__c rec = createPushRec(user, I_Const.EBIZDATASYNC_KBN_BILA);
system.debug([Select Id, Type__c, CreatedDate, IsSended__c from I_PushOut__c where id = :rec.Id]);
system.debug([select id, CreatedDate, NotificationKind__c from E_BizDataSyncLog__c]);
        
        // Before
        System.assertEquals(false, rec.IsSended__c);
        
        Test.startTest();
		I_PushOutSendBatch batch = new I_PushOutSendBatch(log);
		Database.executeBatch(batch,200);
        Test.stopTest();     
        
    }
    
    /*  */
    private static I_PushOut__c createPushRec(User u, String ntype){
    	I_PushOut__c rec = new I_PushOut__c();
    	rec.User__c = u.Id;
    	rec.Type__c = ntype;
    	rec.IsSended__c = false;
    	rec.IsSendTarget__c = false;
    	insert rec;
    	return rec;
    }
    
    private static void createDataEBizLog(String kind, String noticekind){
        system.runAs(thisuser){
            log = new E_BizDataSyncLog__c();
            log.Kind__c = kind;
            log.NotificationKind__c = noticekind;
            insert log;
        }
    }
    
    private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
        Profile p = [Select Id From Profile Where Name = :profileName];
    	
		// Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );
    	
    	// User
    	if(profileName != PF_PARTNER){
    		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
    		user.UserRoleId = portalRole.Id;
    		insert user;
    		
    	// Partner User
    	}else{
			system.runAs(thisuser){
				// Account 代理店格
				ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
				insert ahAcc;
		
				// Account 事務所
				ayAcc = new Account(
						Name = 'office1'
						,ParentId = ahAcc.Id
						,E_CL2PF_ZAGCYNUM__c = 'ay001'
						,E_COMM_VALIDFLAG__c = '1'
				);
				insert ayAcc;
		
				// Contact 募集人
				atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}
			
    		user.ContactId = atCon.Id;
    		insert user;
				
			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
    	}
    }
    
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }
}