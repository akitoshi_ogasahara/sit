public with sharing class I_NoticeBILSController extends I_NoticeAbstractController{

	// MenuKey
	protected override String getLinkMenuKey(){
		return I_NoticeConst.MNKEY_BILS;
	}

	public String discTransdate{get;set;}

	/**
	 * Constructor
	 */
	public I_NoticeBILSController() {
		super();

		// ソートデフォルト設定
		sortIsAsc = false;
		sortType = 'response';

		// 帳票種別
		dlFormNo = E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILS;
	}
	/**
	 * init
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = '保険料請求通知';
		return pr;
	}


	/**
	 * 表示リスト作成
	 */
	protected override void createList(){
		// DTO
		dtos = new List<I_NoticeDTO>();
		discTransdate = '';

		// データ取得
		//List<E_BILS__c> recs = E_BILSDao.getRecsBySortKey(manager.createOrderBy(manager.sortMap.get(manager.sortType), manager.sortIsAsc),createNoticeCondition());
		List<E_BILS__c> recs = E_BILSDao.getRecsBySortKey(createNoticeCondition());
		rowCnt = recs.size();

		// リストセット
		for(E_BILS__c rec : recs){
			dtos.add(new I_NoticeDTO(rec));
		}

		//通知発信日設定
		if(dtos.size() != 0){
			discTransdate = '通知発信日：' + dtos[0].transmissionDate.left(4) + '年' + dtos[0].transmissionDate.mid(5,2) + '月' + dtos[0].transmissionDate.right(2) + '日';
		}
	}

}