global class SendQrCodeEmail implements Process.Plugin
{    
    global Process.PluginResult invoke(Process.PluginRequest request)
    {   
        Map<String, Object> result = new Map<String, Object>();  
        boolean isTrustedIP = false;
        
        String qr_code_url = (String)request.inputParameters.get('qr_code_url');
        
        String user_email_address = (String)request.inputParameters.get('user_email_address');

        String email_subject = (String)request.inputParameters.get('email_subject');

        String body='<IMG SRC="'+qr_code_url+'"/>';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {user_email_address};
        mail.setToAddresses(toAddresses);
        mail.setSubject(email_subject);
        mail.setHtmlBody(body);
        // Pass this email message to the built-in sendEmail method 
        // of the Messaging class
        Messaging.SendEmailResult[] results = Messaging.sendEmail(
                                 new Messaging.SingleEmailMessage[] { mail });
        
        return new Process.PluginResult(result);
    }

    global Process.PluginDescribeResult describe()
    {
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.description='This sends a html email with their qr code when they first register their email address to Authenticator';
        result.tag='Identity';
        
        result.inputParameters = new List<Process.PluginDescribeResult.InputParameter> {
            new Process.PluginDescribeResult.InputParameter('qr_code_url', Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('user_email_address', Process.PluginDescribeResult.ParameterType.STRING, true),
            new Process.PluginDescribeResult.InputParameter('email_subject', Process.PluginDescribeResult.ParameterType.STRING, true)
        };
        
        return result;
    }
}