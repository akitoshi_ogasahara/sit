global with sharing class E_ProcessHistoryExtender extends E_AbstractViewExtender{

    public String contractNo {get; private set;}
    public String contractName {get; private set;}
    public Boolean isCustomerWay {get; private set;}
    
    public E_ProcessHistoryController extension;

    private static final String PAGE_TITLE = '手続履歴';
    private String cId = null;
    private String pId = null;
    private String cno = null;
	private String cname = null;

    public E_ProcessHistoryExtender(E_ProcessHistoryController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
		cId = ApexPages.CurrentPage().getParameters().get('cid');
		pId = ApexPages.CurrentPage().getParameters().get('pid');
		cno = ApexPages.currentPage().getParameters().get('cno');
		cname = ApexPages.currentPage().getParameters().get('cname');
		
        IF(E_Util.isNotNull(cId)){
        	pageRef = doAuth(E_Const.ID_KIND.CONTACT, cId);
        	if(pageRef == null){
            	SkyEditor2.Query tableQuery1 = extension.queryMap.get('insuredTab');
            	tableQuery1.addWhere('( E_Policy__r.Contractor__c = \'' + cId + '\' )' );
            	extension.queryMap.put('insuredTab',tableQuery1);

            	SkyEditor2.Query tableQueryInveHistoryTab = extension.queryMap.get('inveHistoryTab');
            	tableQueryInveHistoryTab.addWhere('( E_ITHPF__r.Contractor__c = \'' + cId + '\' )' );
            	extension.queryMap.put('inveHistoryTab',tableQueryInveHistoryTab);
            	isCustomerWay = true;
        	}
        }else if(E_Util.isNotNull(pId)){
        	System.debug('テスト用：pIdの中身'+pId);
        	pageRef = doAuth(E_Const.ID_KIND.POLICY, pId);
            System.debug('テスト用：pageRefの結果'+pageRef);
        	if(pageRef == null){
            	SkyEditor2.Query tableQuery1 = extension.queryMap.get('insuredTab');
            	tableQuery1.addWhere('E_Policy__c',pId,SkyEditor2.WhereOperator.Eq);
            	extension.queryMap.put('insuredTab',tableQuery1);
            	isCustomerWay = false;

        	}
        }else{
        	//cid、pidどちらも存在しない場合は例外となる
        	isCustomerWay = false;
        	pageRef = Page.E_ErrorPage;
			pageRef.getParameters().put('code', E_Const.ERROR_MSG_PARAMETER);
        }
    }

	/* init */
	global override void init(){
		if(pageRef == null){
			extension.dosearch();
			if (isCustomerWay) {
				for(E_ProcessHistoryController.insuredTabItem item : extension.insuredTab.Items) {
					 item.record.E_Policy__r.COMM_CHDRNUM__c = item.record.E_Policy__r.COMM_CHDRNUM_LINK__c;
				}
			}
		}
	}

	//個別でチェック判定上書き
	public override boolean isValidate() {
		//顧客名と顧客番号に変数を設定する
		setContractorName_Num();
		// URLパラメータ整合性チェック
		return checkConsistency();
	}

	/* ページアクション */
    public PageReference pageAction () {
    	return E_Util.toErrorPage(pageRef, null);
    }

    /**
     * 閉じる＆戻るボタン表示ラベル
     */
    public String getProcessHistoryCloseBackLabel(){
        String label = system.label.E_BTN_BACK;
        if(!isCustomerWay){
            label = system.label.E_BTN_CLOSE;
        }
        return label;
    }

	/**
	 * 保険契約ヘッダより値を取得し変数に設定する
	 */
	private void setContractorName_Num(){
		if(E_Util.isNotNull(cId)){
			checkParamCid(cId);
		}else if(E_Util.isNotNull(pId)){
			checkParamPid(pId);
		}
	}

	/**
	 * cidを条件に保険契約ヘッダから顧客番号と顧客名を取得する
	 */
	private void checkParamCid(String cId){
		E_Policy__c epoli = E_PolicyDao.getRecByIdFrProcessHistoryCid(cId);
		if (epoli != null) {
			contractNo = epoli.ContractorCLTPF_CLNTNUM__c;
			contractName = epoli.ContractorName__c;
		}
	}

	/**
	 * pidより保険契約ヘッダから顧客番号と顧客名を取得する
	 */
	private void checkParamPid(String pId){
		E_Policy__c epoli = E_PolicyDao.getRecByIdFrProcessHistoryPid(pId);
		if (epoli != null) {
			contractNo = epoli.ContractorCLTPF_CLNTNUM__c;
			contractName = epoli.ContractorName__c;
		}
	}

	/**
	 * URLパラメータと取得データをチェックする整合性チェックする
	 * @param ---
	 * @return boolean： 一致してればTRUE
	 */
	private boolean checkConsistency(){
		//Customerコミュニティユーザはチェックしない
		System.debug('****************************'+ access.isCustomer());
		if(access.isCustomer()){
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
		if(E_Util.isNotNull(cId)){
			if(String.isBlank(cno) || String.isBlank(cname)){
				return false;
			}else{
				return (contractNo == cno && contractName == cname);
			}
		}
		return true;
	}
	
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		return new PageReference(E_CookieHandler.getCookieRefererHistory());
	}
}