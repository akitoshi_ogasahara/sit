/***********************************************************
 *
 * Application: CRM SYSTEM
 * File Name  : EventTriggerHandlerTest.cls
 *
 * created  : 2013/11/26 keizu)　曹文傑
 * modified :
 ************************************************************/
/**
 *
 * 行動トリガHandlerテストクラス
 *
 */
@isTest
private class EventTriggerHandlerTest {

	/*
	* eventTriggerHandlerTest01
	* 正常の場合(insert)。
	* @param なし
	* @return  なし
	* @created: 2013/11/22 keizu)　曹文傑
	* @modified:
	*/
    static testMethod void eventTriggerHandlerTest01() {
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs ( thisUser ) {
			try{
				RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and DeveloperName = 'BAC' limit 1];
		
		        EventCheck__c eventCheck = new EventCheck__c();
		        eventCheck.Name = 'システム定義';
		        eventCheck.Recordtypename__c = recordType.DeveloperName;
		        insert eventCheck;
				
				Agency__c agency = createAgency();
				Account acc = createAccount(agency.Id);
				Event event = createEvent(acc.Id, recordType.Id);
		
				Test.startTest();
		    	insert event;
				Test.stopTest();
			}catch(System.DmlException e){
				System.assert(true, String.valueOf(Apexpages.getMessages()).contains(Label.Msg_AfterperformingCommentsError));
			}
		}
    }
	/*
	* eventTriggerHandlerTest02
	* カスタム設定がしていない場合
	* @param なし
	* @return  なし
	* @created: 2013/11/22 keizu)　曹文傑
	* @modified:
	*/
    static testMethod void eventTriggerHandlerTest02() {
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs ( thisUser ) {
			try{
				RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and DeveloperName = 'BAC' limit 1];
				
				Agency__c agency = createAgency();
				Account acc = createAccount(agency.Id);
				Event event = createEvent(acc.Id, recordType.Id);
		
				Test.startTest();
		    	insert event;
				Test.stopTest();
			}catch(System.DmlException e){
				System.assert(true, String.valueOf(Apexpages.getMessages()).contains(Label.Msg_GetCustumRecordtypenameError));
			}
		}
    }

	/*
	* eventTriggerHandlerTest03
	* カスタム設定.行動チェック.レコードタイプ名がレコードタイプOBJに存在しない場合.
	* @param なし
	* @return  なし
	* @created: 2013/11/22 keizu)　曹文傑
	* @modified:
	*/
    static testMethod void eventTriggerHandlerTest03() {
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs ( thisUser ) {
			try{
				RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and DeveloperName = 'BAC' limit 1];
		
		        EventCheck__c eventCheck = new EventCheck__c();
		        eventCheck.Name = 'システム定義';
		        eventCheck.Recordtypename__c = 'like';
		        insert eventCheck;
				
				Agency__c agency = createAgency();
				Account acc = createAccount(agency.Id);
				Event event = createEvent(acc.Id, recordType.Id);
		
				Test.startTest();
		    	insert event;
				Test.stopTest();
			}catch(System.DmlException e){
				System.assert(true, String.valueOf(Apexpages.getMessages()).contains(Label.Msg_GetCustumRecordtypenameError));
			}
		}
    }

	/*
	* eventTriggerHandlerTest04
	* 正常の場合(update)。
	* @param なし
	* @return  なし
	* @created: 2013/11/22 keizu)　曹文傑
	* @modified:
	*/
    static testMethod void eventTriggerHandlerTest04() {
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs ( thisUser ) {
			try{
				RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and DeveloperName = 'BAC' limit 1];
		
		        EventCheck__c eventCheck = new EventCheck__c();
		        eventCheck.Name = 'システム定義';
		        eventCheck.Recordtypename__c = recordType.DeveloperName;
		        insert eventCheck;
				
				Agency__c agency = createAgency();
				Account acc = createAccount(agency.Id);
				Event event = createEventUpdate(acc.Id, recordType.Id);
				event.Comment__c = null;
				
				Test.startTest();
		    	update event;
				Test.stopTest();
				
			}catch(System.DmlException e){
				System.assert(true, String.valueOf(Apexpages.getMessages()).contains(Label.Msg_AfterperformingCommentsError));
			}
		}
    }

 	/*
	* createEvent
	* create Event
	* @param なし
	* @return なし
	* @created: 2013/11/22 keizu) 曹文傑
	* @modified:
	*/
	private static Event createEvent(Id whatId,Id recordTypeId){
        Event eventData = new Event();
        eventData.Area__c = 'テスト';
        eventData.SalesUnit__c = 'テスト';
        eventData.DurationInMinutes = 1;
        eventData.Comment__c = null;
        eventData.WhatId = whatId;
        eventData.RecordTypeId = recordTypeId;
        eventData.ActivityDateTime = System.now();

		return eventData;
	}

 	/*
	* createEventUpdate
	* create Event
	* @param なし
	* @return なし
	* @created: 2013/11/22 keizu) 曹文傑
	* @modified:
	*/
	private static Event createEventUpdate(Id whatId,Id recordTypeId){
        Event eventData = new Event();
        eventData.Area__c = 'テスト';
        eventData.SalesUnit__c = 'テスト';
        eventData.DurationInMinutes = 1;
        eventData.Comment__c = 'test';
        eventData.WhatId = whatId;
        eventData.RecordTypeId = recordTypeId;
        eventData.ActivityDateTime = System.now();
		
		insert eventData;
		return eventData;
	}
    
    /*
    * createAgency
    * 代理店 create
    * @param 
    * @return  代理店
    * @created: 2013/11/26 keizu) 曹文傑
    * @modified:
    */
    static Agency__c createAgency(){
        // insert 代理店
        Agency__c agency = new Agency__c();
        agency.Name = 'keizuOffice';
        agency.CommentsRequired__c = true;
        insert agency;
        return agency;
    }

    /*
    * createAccount
    * 代理店事務所 create
    * @param strName : ストリング
    * @return  代理店事務所
    * @created: 2013/11/26 keizu) 曹文傑
    * @modified:
    */
    static Account createAccount(Id AgencyId){
        // insert 代理店事務所
        Account acc = new Account();
        acc.Name = 'keizuOffice';
        acc.Agency__c = AgencyId;
        insert acc;
        return acc;
    }
}