/*
 * CC_CaseSRActivityBizLogic
 * Business logic class for CC_SRActivity
 * created  : Accenture 2018/6/5
 * modified :
 */

public with sharing class CC_SRActivityVOIBizLogic{

	/**
	 * Get Initiate Data
	 */
	public static Boolean getInitData(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;
		Map<String, String> urlParamMap = new Map<String, String>();

		for(FieldDefinition obj : [SELECT DurableId FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName='CC_SRActivity__c' and QualifiedApiName = 'CC_SRNo__c']){
			urlParamMap.put('durableId', obj.DurableId.split('\\.')[1]);
		}
		for(EntityDefinition obj : [SELECT KeyPrefix FROM EntityDefinition WHERE QualifiedApiName = 'CC_SRActivity__c']){
			urlParamMap.put('keyPrefix', obj.KeyPrefix);
		}
		outputMap.put('urlParam', (Object)urlParamMap);

		return success;
	}

	/**
	 * Get PickList
	 */
	public static Boolean getPicklist(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		String obj = (String) inputMap.get('obj');
		String item = (String) inputMap.get('item');

		List<Schema.PicklistEntry> pickList;
		Schema.SObjectType sobjtype = (Schema.SObjectType)Schema.getGlobalDescribe().get(obj);
		if(sobjtype != null){
			Map<String, Schema.SObjectField> fieldmap = sobjtype.getDescribe().fields.getMap();
			Schema.SObjectField f = (Schema.SObjectField)fieldmap.get(item);
			if(f != null){
				Schema.DescribeFieldResult r = f.getDescribe();
				pickList = r.getPicklistValues();
			}
		}
		System.debug('★値' + JSON.Serialize(pickList));
		//return JSON.Serialize(pickList);
		outputMap.put('pickList', pickList);


		return success;
	}

	/**
	 * Save SRActivity
	 */
	public static Boolean saveSRActivity(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		CC_SRActivity__c obj = new CC_SRActivity__c();
		String strErrorMessage = '';
		obj.Id = (String)inputMap.get('targetId');

		if(inputMap.get('[\'CC_ActivityType__c\']') != null) obj.CC_ActivityType__c = (String)inputMap.get('[\'CC_ActivityType__c\']');
		if(inputMap.get('[\'CC_Status__c\']') != null) obj.CC_Status__c = (String)inputMap.get('[\'CC_Status__c\']');

		if(inputMap.get('[\'CC_Worker__c\']') != null){
			String strWorker = (String)inputMap.get('[\'CC_Worker__c\']');
			Map<Id, sObject> workerObjMap = new Map<Id, sObject>();

			if(strWorker.contains('/')){
				strWorker = strWorker.split('/')[0];
				workerObjMap = new Map<Id, sObject>([SELECT Id FROM User WHERE MemberNo__c = :strWorker LIMIT 1]);
			}else{
				workerObjMap = new Map<Id, sObject>([SELECT Id FROM Group WHERE Name = :strWorker LIMIT 1]);
			}

			if(workerObjMap.size() != 0){
				for(sObject workerObj : workerObjMap.values()){
					obj.OwnerId = workerObj.Id;
				}
			}else{
				strErrorMessage += '作業者が正しく入力されていません。';
			}
		}

		if(strErrorMessage == ''){
			Database.SaveResult result = Database.update(obj, false);
			for(Database.Error err : result.getErrors()){
				strErrorMessage += err.getMessage();
			}
		}

		if(strErrorMessage == ''){
			Map<String, Object> latestObjectValueMap = new Map<String, Object>();
			for(CC_SRActivity__c latestObj : [SELECT Id, CC_ActivityType__c, CC_Worker__c, CC_Status__c, CC_ClosedDate__c, SystemModstamp, CC_LastUpdateDateForActivity__c FROM CC_SRActivity__c WHERE Id = :obj.Id]){
				Map<String, Object> fieldValues = latestObj.getPopulatedFieldsAsMap();
				for (String fieldName : fieldValues.keySet()) {
					if(fieldName == 'SystemModstamp'){
						latestObjectValueMap.put(fieldName, ((Datetime)fieldValues.get(fieldName)).getTime());
					}else{
						latestObjectValueMap.put(fieldName, fieldValues.get(fieldName));
					}
				}
			}

			if(latestObjectValueMap.size() != 0){
				outputMap.put('latestObjectValue', (Object)latestObjectValueMap);
			}
		}else{
			outputMap.put('inputErrorMessage', strErrorMessage);
			success = false;
		}

		return success;
	}
}