public abstract class E_AbstractBarGraph implements E_IGraph {
	
	//親レコードのID
	public String parentId {get;set;}
    
    //グラフ表示データ
    public List<GraphData> graphRecs;
    
    //期間選択されているか？
    public Boolean isSelectedPeriod;
    
    //グラフを表示するか？
    public Boolean getIsDisplayGraph(){
    	Boolean ret = true;
    	if(!isSelectedPeriod){
    		//初期表示
    		return false;
    	} else if(isSelectedPeriod && (graphRecs.equals(null) || graphRecs.isEmpty())){
    		//データがないとき
    		return false;
    	}
    	return ret;
    }
    
    //グラフを表示しないときのメッセージ
    public String getNoGraphMessage(){
    	String ret = '';
    	if(!isSelectedPeriod){
    		//初期表示
    		return '期間を選択してください。';
    	} else if(isSelectedPeriod && (graphRecs.equals(null) || graphRecs.isEmpty())){
    		//データがないとき
    		return 'データがありません。';
    	}
    	return ret;
    }
    
    public E_AbstractBarGraph () {
    	System.debug('***********E_AbstractBarGraph');
    	isSelectedPeriod = false;
		init();
    }
    
    //初期処理
    public virtual void init () {
    }
    
    //グラフ生成
    abstract void createGraph();
    
    //グラフ表示データリスト
    public List<GraphData> getDataList() {
        if  (graphRecs == null) {
            createGraph();
        }
        System.debug('getDataList=' + graphRecs);
        return graphRecs;
    }

    /**
     * グラフ表示クラス
     */
    public class GraphData {
        public String name {get;set;}
        public Decimal data1 {get;set;}
        public Decimal data2 {get;set;}
        public Decimal data3 {get;set;}
        public Decimal data4 {get;set;}
        public GraphData (String name, Decimal data1, Decimal data2, Decimal data3, Decimal data4) {
            this.name = name;
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            this.data4 = data4;
        }
    }
}