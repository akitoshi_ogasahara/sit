// P16-0003 Atria対応開発
/**
 * Atria接続アプリケーションプラグインクラス.
 */
global class I_AtriaConnectedAppPlugin extends Auth.ConnectedAppPlugin{
	
	// 符号化スキーム
	private static final String ENCODING_SCHEME = 'UTF-8';

	// SAMLアサーション属性マップ
	private Map<String, String> samlAttMap;

	/**
	 * コンストラクタ.
	 */
	public I_AtriaConnectedAppPlugin() {
		this.samlAttMap = new Map<String, String>();
	}

	global override Map<String,String> customAttributes(Id userId, Id connectedAppId, Map<String,String> formulaDefinedAttributes, Auth.InvocationContext context) {

		// SAMLアサーション属性マップの構築
		this.initAttributes();
		
		this.setUpAttributes(userId);
		
		formulaDefinedAttributes.putAll(this.samlAttMap);
		
		return formulaDefinedAttributes;
	}
	

	/**
	 * SAMLアサーション属性マップの構築.
	 *
	 * @param Id ユーザID
	 */
	private void setUpAttributes(Id userId) {
		
		Id uId = userId;
		Boolean isAltViewFlg = false;

		// ログ取得
		E_Log__c log = this.getLatestLog(userId);
		
		// 代理閲覧かを判定
		if(String.isNotBlank(log.TargetUser__c)) {
			uId = log.TargetUser__c;
			isAltViewFlg = true;
		}
		
		// ユーザレコード取得
		User u = this.getUser(uId);

		// ID管理取得
		E_IDCPF__c eidc = !u.E_IDCPFs__r.isEmpty() ? u.E_IDCPFs__r.get(0) : new E_IDCPF__c();

		// ID種別
		this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_ID_TYPE
			: I_Const.SAML_ATT_TAG_ID_TYPE, eidc.ZIDTYPE__c);
		
		// NNLinkID
		this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_NNLINK_ID
				: I_Const.SAML_ATT_TAG_NNLINK_ID, u.E_ZWEBID__c);
		
		// 照会者コード
		this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_INQUIRER_CODE
			: I_Const.SAML_ATT_TAG_INQUIRER_CODE, eidc.ZINQUIRR__c);

		/** 募集人 **/
		if(u.UserType == E_Const.USERTYPE_POWERPARTNER) {
			// ユーザ名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_USER_NAME
				: I_Const.SAML_ATT_TAG_USER_NAME, this.encode(u.Contact.Name, ENCODING_SCHEME));
			
			// 募集人コード
			/*
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGENT_CODE
				: I_Const.SAML_ATT_TAG_AGENT_CODE, u.Contact.E_CL3PF_AGNTNUM__c);
			*/
			// パイプより前を送る
			String agCode = u.Contact.E_CL3PF_AGNTNUM__c;
			
			if(String.isNotBlank(agCode) && agCode.contains('|')) {
				agCode = agCode.left(agCode.indexOf('|'));
			}
			
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGENT_CODE
				: I_Const.SAML_ATT_TAG_AGENT_CODE, agCode);

			// 所属代理店コード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGECY_CODE
				: I_Const.SAML_ATT_TAG_AGECY_CODE, u.Contact.E_CL3PF_ZHEADAY__c);

			// 所属代理店名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGECY_NAME
				: I_Const.SAML_ATT_TAG_AGECY_NAME, this.encode(u.Contact.Account.Parent.Name, ENCODING_SCHEME));

			// 所属代理店の一事務所登録フラグ
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGECY_1OFFICE_FLG
				: I_Const.SAML_ATT_TAG_AGECY_1OFFICE_FLG, u.Contact.Account.Parent.Z1OFFING__c);

			// 所属代理店資格コード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGECY_CLS_CODE
				: I_Const.SAML_ATT_TAG_AGECY_CLS_CODE, u.Contact.Account.Parent.E_CL1PF_AGCLS__c);

			// 所属事務所コード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_CODE
				: I_Const.SAML_ATT_TAG_OFFICE_CODE, u.Contact.E_CL3PF_ZAGCYNUM__c);

			// 所属事務所名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_NAME
				: I_Const.SAML_ATT_TAG_OFFICE_NAME, this.encode(u.Contact.Account.E_CL2PF_ZAYKNJNM__c, ENCODING_SCHEME));

			// 所属事務所郵便番号
			/*
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_POSTAL
				: I_Const.SAML_ATT_TAG_OFFICE_POSTAL, u.Contact.Account.CLTPCODE__c);
			*/
			String postal = u.Contact.Account.CLTPCODE__c;
			
			postal = String.isNotBlank(postal) ? postal.replace('-', '') : '';
			
			// ハイフンは削除する
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_POSTAL
				: I_Const.SAML_ATT_TAG_OFFICE_POSTAL, postal);

			// 所属事務所住所
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_ADDRESS
				: I_Const.SAML_ATT_TAG_OFFICE_ADDRESS, this.encode(u.Contact.Account.E_COMM_ZCLADDR__c, ENCODING_SCHEME));

			// 所属事務所電話番号01
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_PHONE1
				: I_Const.SAML_ATT_TAG_OFFICE_PHONE1, u.Contact.Account.CLTPHONE01__c);

			// 所属事務所電話番号02
			/*
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_PHONE2
				: I_Const.SAML_ATT_TAG_OFFICE_PHONE2, u.Contact.E_CLTPF_CLTPHONE02__c);
			*/
			
			// 所属事務所手数料カテゴリー
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_OFFICE_COMM_CAT
				: I_Const.SAML_ATT_TAG_OFFICE_COMM_CAT, u.Contact.Account.E_CL2PF_RCMTAB__c);

			// 担当MRコード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_MR_CODE
				: I_Const.SAML_ATT_TAG_MR_CODE, u.Contact.Account.ZMRCODE__c);

			// 担当MR名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_MR_NAME
				: I_Const.SAML_ATT_TAG_MR_NAME, this.encode(u.Contact.Account.Owner.Name, ENCODING_SCHEME));

			// 所属部署（拠点、営業部）コード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_BRANCH_CODE
				: I_Const.SAML_ATT_TAG_BRANCH_CODE, u.Contact.E_CL3PF_BRANCH__c);

			// 所属部署（拠点、営業部）名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_BRANCH_NAME
				: I_Const.SAML_ATT_TAG_BRANCH_NAME, this.encode(u.Contact.Account.AGNTBR_NM__c, ENCODING_SCHEME));

			// 所属代理店金融機関コード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGENCY_BK_CODE
				: I_Const.SAML_ATT_TAG_AGENCY_BK_CODE, u.Contact.Account.Parent.E_CL1PF_ZBKCODE__c);
			
			// チャネル
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_CHANNEL
				: I_Const.SAML_ATT_TAG_CHANNEL, u.Contact.Account.Parent.E_CL1PF_KCHANNEL__c);

			// MOFコード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_MOF_CODE
				: I_Const.SAML_ATT_TAG_MOF_CODE, u.Contact.Account.Parent.E_CL1PF_KMOFCODE__c);
			
			// 支店コード（金融機関支店）
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_FI_BRANCH_CODE
				: I_Const.SAML_ATT_TAG_FI_BRANCH_CODE, u.Contact.Account.E_CL2PF_ZAYSECT__c);
			
			// 取扱者コード
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_MEM_CODE
				: I_Const.SAML_ATT_TAG_MEM_CODE, u.Contact.E_CL3PF_ZMEMNUM__c);

			// 手数料試算権限
			String flgStr = eidc.AtriaCommissionAuthorityEditable__c ? '1' : '0';
			
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_ATR_COMM_FLG
				: I_Const.SAML_ATT_TAG_ATR_COMM_FLG, flgStr);
		} else {
			/** 社員 **/
			// NNLink ID
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_NNLINK_ID
				:I_Const.SAML_ATT_TAG_NNLINK_ID, u.E_ZWEBID__c);
			
			// ユーザ名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_USER_NAME
				: I_Const.SAML_ATT_TAG_USER_NAME, this.encode(u.Name, ENCODING_SCHEME));

			// 募集人コード EIDCの所有者顧客コードから、Contactを取得
			String agCode = '';

			if(String.isNotBlank(eidc.CLNTNUM__c)) {
				Contact usrCon = E_ContactDao.getRecByAgClCode(eidc.CLNTNUM__c);
				
				agCode = usrCon.E_CL3PF_AGNTNUM__c;
			}
			
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_AGENT_CODE
				: I_Const.SAML_ATT_TAG_AGENT_CODE, agCode);
			
			// 所属部署（拠点、営業部）コード
			String branchCode = '';

			if(String.isNotBlank(eidc.ZINQUIRR__c) && eidc.ZINQUIRR__c.length() > 2) {
			
				branchCode = eidc.ZINQUIRR__c.substring(2);
			}
			
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_BRANCH_CODE
				: I_Const.SAML_ATT_TAG_BRANCH_CODE, branchCode);
			
			// 所属部署（拠点、営業部）名
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_BRANCH_NAME
				: I_Const.SAML_ATT_TAG_BRANCH_NAME, this.encode(u.Department, ENCODING_SCHEME));
			
			// 手数料試算権限
			this.samlAttMap.put(isAltViewFlg ? I_Const.ALT_SAML_ATT_TAG_ATR_COMM_FLG
				: I_Const.SAML_ATT_TAG_ATR_COMM_FLG, '1');
		}
		
		// 代理閲覧の場合、操作した社員ユーザ情報も追加する
		if(isAltViewFlg) {
			// 操作した社員ユーザ取得
			u = this.getUser(userId);
			
			// EIDC取得
			eidc = !u.E_IDCPFs__r.isEmpty() ? u.E_IDCPFs__r.get(0) : new E_IDCPF__c();
			
			// NNLink ID
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_NNLINK_ID, u.E_ZWEBID__c);
			
			// ID種別
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_ID_TYPE, eidc.ZIDTYPE__c);
			
			// 照会者コード
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_INQUIRER_CODE, eidc.ZINQUIRR__c);
			
			// ユーザ名
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_USER_NAME, this.encode(u.Name, ENCODING_SCHEME));

			// 募集人コード EIDCの所有者顧客コードから、Contactを取得
			String agCode = '';

			if(String.isNotBlank(eidc.CLNTNUM__c)) {
				Contact usrCon = E_ContactDao.getRecByAgClCode(eidc.CLNTNUM__c);
				
				agCode = usrCon.E_CL3PF_AGNTNUM__c;
			}
			
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGENT_CODE, agCode);
			
			// 所属部署（拠点、営業部）コード
			String branchCode = '';

			if(String.isNotBlank(eidc.ZINQUIRR__c) && eidc.ZINQUIRR__c.length() > 2) {
			
				branchCode = eidc.ZINQUIRR__c.substring(2);
			}
			
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_BRANCH_CODE, branchCode);
			
			// 所属部署（拠点、営業部）名
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_BRANCH_NAME, this.encode(u.Department, ENCODING_SCHEME));
			
			// 手数料試算権限
			this.samlAttMap.put(I_Const.SAML_ATT_TAG_ATR_COMM_FLG, '1');
		}
	}

	/**
	 * SAMLアサーション属性マップのクリア処理.
	 */
	private void initAttributes() {

		Map<String, String> result = new Map<String, String>();

		this.samlAttMap.put(I_Const.SAML_ATT_TAG_NNLINK_ID, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_ID_TYPE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_INQUIRER_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_USER_NAME, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGENT_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGECY_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGECY_NAME, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGECY_1OFFICE_FLG, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGECY_CLS_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_NAME, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_POSTAL, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_ADDRESS, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_PHONE1, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_PHONE2, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_OFFICE_COMM_CAT, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_MR_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_MR_NAME, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_BRANCH_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_BRANCH_NAME, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_AGENCY_BK_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_MOF_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_CHANNEL, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_FI_BRANCH_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_MEM_CODE, '');
		this.samlAttMap.put(I_Const.SAML_ATT_TAG_ATR_COMM_FLG, '');
		
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_NNLINK_ID, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_ID_TYPE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_INQUIRER_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_USER_NAME, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_AGENT_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_AGECY_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_AGECY_NAME, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_AGECY_1OFFICE_FLG, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_AGECY_CLS_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_NAME, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_POSTAL, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_ADDRESS, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_PHONE1, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_PHONE2, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_OFFICE_COMM_CAT, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_MR_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_MR_NAME, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_BRANCH_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_BRANCH_NAME, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_AGENCY_BK_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_MOF_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_CHANNEL, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_FI_BRANCH_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_MEM_CODE, '');
		this.samlAttMap.put(I_Const.ALT_SAML_ATT_TAG_ATR_COMM_FLG, '');
	}

	/**
	 * 作成者からNNLinkログを取得する.
	 *
	 * @param Id 作成者ID
	 * @return NNLinkログレコード
	 */
	private E_Log__c getLatestLog(Id userId) {
		return E_LogDao.getLatestRecByCreatedAndActType(userId, I_Const.ACTION_TYPE_ATRIA_SSO);
	}

	/**
	 * ユーザを取得する.
	 *
	 * @param Id ユーザID
	 * @return ユーザレコード
	 */
	private User getUser(Id userId) {
		return E_UserDao.getUserWithPermissionsByUserId(userId);
	}
	
	/**
	 * 文字列を符号化する.
	 *
	 * @param String 元文字列
	 * @param String 符号化スキーム
	 * @return String 符号化した文字列
	 */
	private String encode(String src, String scheme) {
		return String.isNotBlank(src) ? EncodingUtil.urlEncode(src, scheme) : '';
	}
}