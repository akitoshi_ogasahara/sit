global with sharing class E_InveTransitionExtender extends E_AbstractViewExtender {
	private static final String PAGE_TITLE = '評価金額(残高)の推移';
	
	E_InveTransitionController extension;
	
	/** コンストラクタ */
	public E_InveTransitionExtender(E_InveTransitionController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
	}

	/** init */
	global override void init(){
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, extension.record.id);
	}

	/** ページアクション */
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}
}