public with sharing class E_BizDataSyncBatchOperation_SalesRsltEP extends E_BizDataSyncBatchOperation {

	/**
	 * getQueryLocator for Batch start method
	 */
	public override Database.QueryLocator getQueryLocator(){
//		errList = new List<String>();
		String businessDate = E_Util.date2YYYYMMDD([Select id,InquiryDate__c From E_BizDataSyncLog__c Where kind__c =: 'F1' ORDER BY CreatedDate desc limit 1].InquiryDate__c);

		return Database.getQueryLocator(
					[SELECT 
						Id  
						,brCode__c 
					From 
						E_EmployeeSalesResults__c 
					Where 
						BusinessDate__c =: businessDate 
					ORDER BY BrCode__c,SectionCode__c,MRCode__c]);
	}
	
	/**
	 * execute
	 * 		batchのExecuteの本体ロジック
	 */
	public override void execute(List<Sobject> records){

		// Shareクラス生成
		E_EmployeeSalesResultsShare epSrShare = new E_EmployeeSalesResultsShare(new Set<Sobject>(records));
		
		// Share設定
		epSrShare.execute();
	}
}