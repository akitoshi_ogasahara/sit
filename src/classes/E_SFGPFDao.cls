/**
 * 選択可能ファンドDAO
 */
public with sharing class E_SFGPFDao {
	/**
     * ファンド群マスタIDをキーに選択可能ファンドリストを取得する
     * @param fundMasterId: ファンド群マスタID
     * @return List<E_SFGPF__c>: ユニットプライス基本情報
	 */
	public static List<E_SFGPF__c> getRecByFundMasterId (String fundMasterId) {
        List<E_SFGPF__c> recs = [
            Select 
            Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
            SystemModstamp, E_SVCPF__c, E_SFGPFCode__c, SortNumber__c, FundMaster__c
        	From E_SFGPF__c
            Where FundMaster__c =: fundMasterId
            order by SortNumber__c
        ];
        return recs;
	}

	/**
     * ファンド群マスタIDと特別勘定IDをキーに選択可能ファンドリストを取得する
     * @param fundMasterId: ファンド群マスタID
     * @param specialAcountId: 特別勘定ID
     * @return List<E_SFGPF__c>: ユニットプライス基本情報
	 */
	public static List<E_SFGPF__c> getRecByFundMasterId (String fundMasterId,String specialAcountId) {
        List<E_SFGPF__c> recs = [
            Select 
            Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
            SystemModstamp, E_SVCPF__c, E_SFGPFCode__c, SortNumber__c, FundMaster__c
        	From E_SFGPF__c
            Where FundMaster__c =: fundMasterId
            And E_SVCPF__c =: specialAcountId
            order by SortNumber__c
        ];
        return recs;
	} 
    
	/**
     * ファンド群マスタIDをキーに選択可能ファンドリストを取得する
     * @param fundMasterId: ファンド群マスタID
     * @return List<E_SFGPF__c>: ユニットプライス基本情報
	 */
	public static List<E_SFGPF__c> getRecByFundMasterIdOrderBy (String fundMasterId) {
        List<E_SFGPF__c> recs = [
            Select 
            Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
            SystemModstamp, E_SVCPF__c, E_SFGPFCode__c, SortNumber__c, FundMaster__c
        	From E_SFGPF__c
            Where FundMaster__c =: fundMasterId
            //order by E_SFGPFCode__c ASC
        	Order by SortNumber__c ASC
        ];
        return recs;
	}

	/**
     * ファンド群マスタIDと、選択可能ファンドコードをキーに選択可能ファンドリストを取得する
     * @param fundMasterId: ファンド群マスタID
     * @param SFGPFCode: 選択可能ファンドコード
     * @return List<E_SFGPF__c>: ユニットプライス基本情報
	 */
	public static List<E_SFGPF__c> getRecByFundMasterBySFGPFCode(String fundMasterId,String SFGPFCode) {
        List<E_SFGPF__c> recs = [
            Select 
            Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById,
            SystemModstamp, E_SVCPF__c, E_SFGPFCode__c, SortNumber__c, FundMaster__c
        	From E_SFGPF__c
            Where FundMaster__c =: fundMasterId
            AND E_SFGPFCode__c = :SFGPFCode
            order by SortNumber__c
        ];
        return recs;
	}
    
    
}