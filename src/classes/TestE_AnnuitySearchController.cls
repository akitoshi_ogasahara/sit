@isTest
private with sharing class TestE_AnnuitySearchController{
		private static testMethod void testPageMethods() {	
			E_AnnuitySearchController page = new E_AnnuitySearchController(new ApexPages.StandardController(new E_Policy__c()));	
			page.getOperatorOptions_E_Policy_c_COMM_CHDRNUM_c();	
			page.getOperatorOptions_E_Policy_c_InsuredE_CLTPF_ZCLKNAME_c();	
			page.getOperatorOptions_E_Policy_c_InsuredName_c();	
			System.assert(true);
		}	
			
	private static testMethod void testdataTable() {
		E_AnnuitySearchController.dataTable dataTable = new E_AnnuitySearchController.dataTable(new List<E_Policy__c>(), new List<E_AnnuitySearchController.dataTableItem>(), new List<E_Policy__c>(), null, null);
		dataTable.create(new E_Policy__c());
		dataTable.doDeleteSelectedItems();
		dataTable.setPagesize(10);		dataTable.doFirst();
		dataTable.doPrevious();
		dataTable.doNext();
		dataTable.doLast();
		dataTable.doSort();
		System.assert(true);
	}
	
}