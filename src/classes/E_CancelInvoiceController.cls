global with sharing class E_CancelInvoiceController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_CancelInvoiceExtender getExtender() {return (E_CancelInvoiceExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public E_CancelInvoiceController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component3155');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3155',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
		tmpPropMap.put('Component__Width','140');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component185');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component185',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component3112');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3112',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','キャンセル');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Spva?Id={!record.Id}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component3171');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3171',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component3159');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3159',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component3160');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3160',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
		tmpPropMap.put('Component__Width','140');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component610');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component610',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component3133');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3133',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component3019');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3019',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('ischk','{!Extender.isAgree}');
        tmpPropMap.put('bflbl','');
        tmpPropMap.put('afLbl','{!Extender.disclaimer[\'CID|007\']}');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2962');
		tmpPropMap.put('Component__Name','ECheckBox');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component2962',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component3164');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3164',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component3166');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3166',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
		tmpPropMap.put('Component__Width','140');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component2074');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component2074',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component3154');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3154',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component3066');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3066',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.SPVA_ZPLAMT01__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!AND(record.COMM_ZPLDCF__c,record.COMM_ZRSTDESC__c!=\'一時払定額年金へ移行\')}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','1');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component3232');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3232',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','{!Extender.fN}');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','{!NOT(ISBLANK(Extender.DownloaId))}');
        tmpPropMap.put('p_value','/servlet/servlet.FileDownload?file={!Extender.DownloaId}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','109');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component3229');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3229',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','SAE.pdf');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!NOT(ISBLANK($Resource.nnlink_SAE))}');
        tmpPropMap.put('p_value','{!$Resource.nnlink_SAE}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component3197');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3197',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','600');
		tmpPropMap.put('Component__Height','40');
		tmpPropMap.put('Component__id','Component2908');
		tmpPropMap.put('Component__Name','EAdobeReader');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component2908',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','契約内容個別照会画面へ戻る');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Spva?Id={!record.Id}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','167');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component3172');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3172',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component3170');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		appComponentProperty.put('Component3170',tmpPropMap);

		lastPageNumber = 3;
		pagesMap.put(1,'E_CancelInvoiceConfirm');
		pagesMap.put(2,'E_CancelInvoiceAgree');
		pagesMap.put(3,'E_CancelInvoiceDownloads');

		SObjectField f;

		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.COMM_ZRSTDESC__c;
		f = E_Policy__c.fields.COMM_ZCOVRNAM__c;
		f = E_Policy__c.fields.COMM_ZTOTPREM__c;
		f = E_Policy__c.fields.SPVA_ZREVAMT01__c;
		f = E_Policy__c.fields.SPVA_ZREFAMT__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			recordTypeIdMap = SkyEditor2.Util.SetRecordTypesMap('E_Policy__c');
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
			mainQuery.addFieldAsOutput('COMM_ZRSTDESC__c');
			mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
			mainQuery.addFieldAsOutput('COMM_ZTOTPREM__c');
			mainQuery.addFieldAsOutput('SPVA_ZREVAMT01__c');
			mainQuery.addFieldAsOutput('SPVA_ZREFAMT__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNDSP__c');
			mainQuery.addFieldAsOutput('SpClassification__c');
			mainQuery.addFieldAsOutput('SystemModstamp');
			mainQuery.addFieldAsOutput('Id');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('SPVA_ZSUF__c');
			mainQuery.addFieldAsOutput('SPVA_ZDEDUCT__c');
			mainQuery.addFieldAsOutput('SPVA_ZDEDUCTF__c');
			mainQuery.addFieldAsOutput('SPVA_ZEXFROMSC__c');
			mainQuery.addFieldAsOutput('SPVA_ZEXFROMSCF__c');
			mainQuery.addFieldAsOutput('Contractor__c');
			mainQuery.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('ContractorName__c');
			mainQuery.addFieldAsOutput('SPVA_ZPLAMT01__c');
			mainQuery.addFieldAsOutput('COMM_ZPLDCF__c');
			mainQuery.addFieldAsOutput('COMM_OCCDATE__c');
			mainQuery.addFieldAsOutput('COMM_OCCDATE_yyyyMMdd__c');
			mainQuery.addFieldAsOutput('LastModifiedById');
			mainQuery.addFieldAsOutput('LastModifiedDate');
			mainQuery.addFieldAsOutput('CreatedById');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addFieldAsOutput('SPVA_ZACTSUM__c');
			mainQuery.addFieldAsOutput('MainAgent__c');
			mainQuery.addFieldAsOutput('MainAgent__r.E_AccParentName__c');
			mainQuery.addFieldAsOutput('MainAgent__r.AccountId');
			mainQuery.addFieldAsOutput('MainAgent__r.Account.E_IsAgency__c');
			mainQuery.addFieldAsOutput('OwnerId');
			mainQuery.addFieldAsOutput('SPVA_ZPRDCD__c');
			mainQuery.addFieldAsOutput('SPVA_ZPRDCD2__c');
			mainQuery.addFieldAsOutput('DSPVA_ZREVAMT__c');
			mainQuery.addFieldAsOutput('SPVA_ZFATDCF__c');
			mainQuery.addFieldAsOutput('COMM_ZTFRDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZREVDCF__c');
			mainQuery.addFieldAsOutput('Annuitant__c');
			mainQuery.addFieldAsOutput('SPVA_ZGRPFND01__c');
			mainQuery.addFieldAsOutput('SPVA_ZGRPFND02__c');
			mainQuery.addFieldAsOutput('SPVA_ZGRPFND03__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE2__c');
			mainQuery.addFieldAsOutput('SPVA_AGNTNUM02__c');
			mainQuery.addFieldAsOutput('dataSyncDate__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_CancelInvoiceExtender(this);
			p_showHeader = false;
			p_sidebar = false;
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}