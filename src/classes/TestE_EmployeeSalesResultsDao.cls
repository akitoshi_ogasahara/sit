@isTest
public class TestE_EmployeeSalesResultsDao {

	/**
	 * getRecTypeAByIdのテスト
	 */
	@isTest static void getRecByIdTest() {
		// 社内挙積情報の作成
		E_EmployeeSalesResults__c employeeSalesResults = createEmployeeSalesResults();
		insert employeeSalesResults;

		// テスト
		Test.startTest();
		E_EmployeeSalesResults__c rec = E_EmployeeSalesResultsDao.getRecById(employeeSalesResults.Id);
		System.assertEquals(employeeSalesResults.Id, rec.Id);
		Test.stopTest();

	}


	/**
	 * 代理店挙積情報の作成
	 */
	public static E_EmployeeSalesResults__c createEmployeeSalesResults(){
		E_EmployeeSalesResults__c employeeSalesResults = new E_EmployeeSalesResults__c();
		employeeSalesResults.MRCode__c						= '';					// MRコード
		employeeSalesResults.E_Unit__c						= null;					// 営業部
		employeeSalesResults.SectionCode__c					= '';					// 課コード
		employeeSalesResults.Hierarchy__c					= '';					// 階層
		employeeSalesResults.YYYYMM__c						= '201707';				// 年月
		employeeSalesResults.MR__c							= null;					// MR
		employeeSalesResults.AchiveRate_FS__c				= 90;					// 達成率 FS
		employeeSalesResults.Budget_FS__c					= 100000000;			// 予算 FS
		employeeSalesResults.NBWANP_FS__c					= 90000000;				// 新契約換算ANP　FS
		employeeSalesResults.AchiveRate_Protection__c		= 88.88888889;			// 達成率 Protection
		employeeSalesResults.Budget_Protection__c			= 90000000;				// 予算 Protection
		employeeSalesResults.NBWANP_Protection__c			= 80000000;				// 新契約換算ANP　Protection
		employeeSalesResults.AchiveRate_BACOLI__c			= 87.5;					// 達成率 BA-COLI
		employeeSalesResults.Budget_BACOLI__c				= 80000000;				// 予算 BA-COLI
		employeeSalesResults.NBWANP_BACOLI__c				= 70000000;				// 新契約換算ANP　BA-COLI
		employeeSalesResults.AchiveRate_Total__c			= 88.88888889;			// 達成率 Total
		employeeSalesResults.Budget_Total__c				= 270000000;			// 予算 Total
		employeeSalesResults.NBWANP_Total__c				= 240000000;			// 新契約換算ANP　Total
		employeeSalesResults.AchiveRate_FSYTD__c			= 88.88888889;			// 達成率（YTD） FS
		employeeSalesResults.Budget_FSYTD__c				= 9900000;				// 予算（YTD） FS
		employeeSalesResults.NBWANP_FSYTD__c				= 8800000;				// 新契約換算ANP（YTD）　FS
		employeeSalesResults.AchiveRate_ProtectionYTD__c	= 87.5;					// 達成率（YTD） Protection
		employeeSalesResults.Budget_ProtectionYTD__c		= 8800000;				// 予算（YTD） Protection
		employeeSalesResults.NBWANP_ProtectionYTD__c		= 7700000;				// 新契約換算ANP（YTD）　Protection
		employeeSalesResults.AchiveRate_BACOLIYTD__c		= 85.71428571;			// 達成率（YTD） BA-COLI
		employeeSalesResults.Budget_BACOLIYTD__c			= 7700000;				// 予算（YTD） BA-COLI
		employeeSalesResults.NBWANP_BACOLIYTD__c			= 6600000;				// 新契約換算ANP（YTD）　BA-COLI
		employeeSalesResults.AchiveRate_TotalYTD__c			= 87.5;					// 達成率（YTD） Total
		employeeSalesResults.Budget_TotalYTD__c				= 26400000;				// 予算（YTD） Total
		employeeSalesResults.NBWANP_TotalYTD__c				= 23100000;				// 新契約換算ANP（YTD）　Total
		employeeSalesResults.ActiveAgency__c				= 1000;					// 稼動事務所数
		employeeSalesResults.ActiveAgent__c					= 99999;				// 稼働募集人数
		employeeSalesResults.IQA__c							= 80;					// IQA継続率
		employeeSalesResults.IQA_Numerator__c				= 80000;				// IQA継続率 分子
		employeeSalesResults.IQA_Denominator__c				= 100000;				// IQA継続率 分母
		employeeSalesResults.MOF13__c						= 77.77777778;			// MOF13継続率
		employeeSalesResults.MOF13_Numerator__c				= 70000;				// MOF13継続率 分子
		employeeSalesResults.MOF13_Denominator__c			= 90000;				// MOF13継続率 分母
		employeeSalesResults.MOF25__c						= 75;					// MOF25継続率
		employeeSalesResults.MOF25_Numerator__c				= 60000;				// MOF25継続率 分子
		employeeSalesResults.MOF25_Denominator__c			= 80000;				// MOF25継続率 分母
		employeeSalesResults.DefactRate__c					= 10;					// 単純不備率
		employeeSalesResults.DefactRate_Numerator__c		= 100;					// 単純不備率 単純不備数
		employeeSalesResults.DefactRate_Denominator__c		= 1000;					// 単純不備率 総数

		return employeeSalesResults;
	}

}