@isTest
private class NetworkAccessTest
{
    static testMethod void inputIP() {
     
        NetworkAccess network = new NetworkAccess();
        boolean isTrustedIP = True;
        Map<String,Object> inputParams = new Map<String,Object>();
        Map<String,Object> outputParams = new Map<String,Object>();
        String ChatterFreeUserProfileId=[SELECT Id FROM Profile where Name='MR'].Id;
        isTrustedIP = Auth.SessionManagement.isIpAllowedForProfile(ChatterFreeUserProfileId,'1.1.1.1');
        //Map<String,String> sessionAttributes = Auth.SessionManagement.getCurrentSession();
        inputParams.put('IP', '1.1.1.1');
        outputParams.put('IsTrusted', isTrustedIP);
        //String sourceIP = sessionAttributes.get('SourceIp');
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        Process.PluginResult result = new Process.PluginResult(outputParams);
  
                
        result = network.invoke(request); 
        
        //result.put('IsTrusted', isTrustedIP);
        System.AssertEquals(1,1);
    }

    static testMethod void describeTest() {

        NetworkAccess network = new NetworkAccess();
        Process.PluginDescribeResult result = network.describe();       
        System.AssertEquals(result.inputParameters.size(), 1);
        System.AssertEquals(result.OutputParameters.size(), 1);
     }
}