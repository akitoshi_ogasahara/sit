global with sharing class E_InveController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_ITHPF__c record {get{return (E_ITHPF__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_InveExtender getExtender() {return (E_InveExtender)extender;}
	
	
	public dataTableITFPF dataTableITFPF {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_InveController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome',' {!Extender.welcome} ');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1307');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1307',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','inve_search,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','現在の評価金額');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_InveValue?id={!record.id}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','91');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1323');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1323',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','評価金額（残高）の推移');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_InveTransition?id={!record.id}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','115');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1324');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1324',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','取引履歴');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_InveHistory?ithpfId={!record.id}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1325');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1325',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_Inve');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','90');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1326');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1326',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!dataTableITFPF_item.record.Z1STBYDT__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1040');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1040',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('ithpfId','{!record.Id}');
        tmpPropMap.put('executeKind','{!Extender.executeKind}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','200');
		tmpPropMap.put('Component__id','Component1017');
		tmpPropMap.put('Component__Name','EInvestmentGraph');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1017',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','評価金額と比較する');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_InveCompare?id={!record.id}');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','110');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1312');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1312',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1311');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1311',tmpPropMap);


		SObjectField f;

		f = E_ITHPF__c.fields.ContractorName__c;
		f = E_ITHPF__c.fields.ZCLNTCDE__c;
		f = E_ITFPF__c.fields.ZASSTNAM__c;
		f = E_ITFPF__c.fields.ZBRNDNMS__c;
		f = E_ITFPF__c.fields.ZITUNIT__c;
		f = E_ITFPF__c.fields.ZINVAMT__c;
		f = E_ITFPF__c.fields.ZBRNDPER01__c;
		f = E_ITFPF__c.fields.ZASSTCLS__c;
		f = E_ITFPF__c.fields.ZBRNDCDE__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_ITHPF__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_ITHPF__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('ContractorName__c');
			mainQuery.addFieldAsOutput('ZCLNTCDE__c');
			mainQuery.addFieldAsOutput('Contractor__c');
			mainQuery.addFieldAsOutput('CreatedDate');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			dataTableITFPF = new dataTableITFPF(new List<E_ITFPF__c>(), new List<dataTableITFPFItem>(), new List<E_ITFPF__c>(), null);
			listItemHolders.put('dataTableITFPF', dataTableITFPF);
			query = new SkyEditor2.Query('E_ITFPF__c');
			query.addFieldAsOutput('ZASSTNAM__c');
			query.addFieldAsOutput('ZBRNDNMS__c');
			query.addFieldAsOutput('ZITUNIT__c');
			query.addFieldAsOutput('ZINVAMT__c');
			query.addFieldAsOutput('ZBRNDPER01__c');
			query.addFieldAsOutput('Z1STBYDT__c');
			query.addWhere('E_ITHPF__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dataTableITFPF', 'E_ITHPF__c');
			dataTableITFPF.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dataTableITFPF', query);
			
			registRelatedList('E_ITFPFs__r', 'dataTableITFPF');
			
			SkyEditor2.Query dataTableITFPFQuery = queryMap.get('dataTableITFPF');
			dataTableITFPFQuery.addSort('ZASSTCLS__c',True,True).addSort('ZBRNDCDE__c',True,True);
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_InveExtender(this);
			init();
			
			dataTableITFPF.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class dataTableITFPFItem extends SkyEditor2.ListItem {
		public E_ITFPF__c record{get; private set;}
		@TestVisible
		dataTableITFPFItem(dataTableITFPF holder, E_ITFPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTableITFPF extends SkyEditor2.ListItemHolder {
		public List<dataTableITFPFItem> items{get; private set;}
		@TestVisible
			dataTableITFPF(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dataTableITFPFItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableITFPFItem(this, (E_ITFPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}