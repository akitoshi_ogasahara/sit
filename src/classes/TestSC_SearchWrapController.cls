@istest
private class TestSC_SearchWrapController {
    // コンストラクタ、初期処理
	private static testMethod void SearchWrapTest001() {

		Test.startTest();
			SC_SearchWrapController controller = new SC_SearchWrapController();

			PageReference pr = controller.pageAction();
		Test.stopTest();  

		System.assertEquals(controller.getPageTitle(), '代理店事務所検索（代理店自己点検）');
		System.assertEquals(pr, null);

    	return;
    }

    /**
     * void setMailTemplate()
     */
     @isTest(SeeAllData=true)
    private static void setMailTemplate_test01() {

		Test.startTest();
		SC_SearchWrapController controller = new SC_SearchWrapController();
		Controller.templateDevName = SC_MailManager.MT_REMIND_BEFORE_MR;
		controller.setMailTemplate();
		Test.stopTest();  

		EmailTemplate template = E_EmailTemplateDao.getRecByDeveloper(SC_MailManager.MT_REMIND_BEFORE_MR);
		System.assertEquals(template.Subject, controller.subject);
		System.assertEquals(template.Body, controller.mailBody);
    }

    /**
     * void doSend()
     */
	private static testMethod void doSend_test001() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		SC_SearchWrapController controller = new SC_SearchWrapController();
		controller.selectedIds = '';
		controller.doSend();
		Test.stopTest();  

		List<SC_MailHistory__c> results = [Select Id From SC_MailHistory__c];
		System.assert(true == results.isEmpty());
    }

	/**
     * void doSend()
     */
	private static testMethod void doSend_test002() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		SC_SearchWrapController controller = new SC_SearchWrapController();
		controller.selectedIds = record.Id;
		controller.doSend();
		Test.stopTest();  

		List<SC_MailHistory__c> results = [Select Id From SC_MailHistory__c];
		System.assert(false == results.isEmpty());
    }
}