/**
 * Contactデータアクセスオブジェクト
 * CreatedDate 2014/12/04
 * @Author Terrasky
 */
public with sharing class E_ContactDao {

	public static Contact getRecById (String contactId) {
		Contact rec = null;
		List<Contact> recs = [
			Select
				id, Name, E_CLTPF_ZCLKNAME__c, E_CLTPF_CLNTNUM__c,
				E_CLTPF_CLTPHONE01__c, E_CLTPF_CLTPHONE02__c, E_CLTPF_FAXNO__c,
				E_CLTPF_ZNKJADDR01__c, E_CLTPF_ZNKJADDR02__c, E_CLTPF_ZNCLTADR01__c, E_CLTPF_ZNCLTADR02__c,
				E_CADPF__c, E_CADPF__r.Name, E_CADPF__r.ZADRSKJ__c, E_CADPF__r.ZADRSKN__c,
				E_CL3PF_AGNTNUM__c, E_Account__c, E_CL3PF_ZAGCYNUM__c, E_AccParentName__c, E_CL3PF_ZHEADAY__c,
				AccountId, Account.Name, Account.ParentId, //Account.Parent.Name, Account.Parent.E_CL1PF_ZHEADAY__c,
				E_AccParentCord__c, E_MenuKindId__c, Account.E_CL2PF_ZAGCYNUM__c, Account.E_CL2PF_BRANCH__c
			From
				Contact
			Where
				id =: contactId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}


	/**
	 * 募集人コードでContactを検索する
	 */
	public static Contact getRecByAgentNumber (String AgentNumber) {
		Contact rec = null;
		List<Contact> recs = [
			Select
				id, Name ,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c From Contact
			Where
				E_CL3PF_AGNTNUM__c =: AgentNumber
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 * 事務所IDからContactレコードを取得
	 */
	public static List<Contact> getRecsByOfficeId(String accid) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL3PF_AGNTNUM__c'
					+ ', Account.Name'
					+ ', Account.Parent.Name '
					+ 'FROM Contact '
					+ 'WHERE Account.Id = :accid'
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\''
					+ ' AND E_CL3PF_AGTYPE__c != \'01\''
					+ ' AND E_CL3PF_AGTYPE__c != \'07\''
					+ ' AND E_CL3PF_AGTYPE__c != \'08\''
					+ ' AND E_CL3PF_AGTYPE__c != \'99\''
					+ ' AND (NOT E_CL3PF_AGNTNUM__c LIKE \'%|%\')'
					+ ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	// IRIS Day2
	/**
	 * 契約者、被保険者検索（検索ボックス）
	 */
	public static List<Contact> getRecsIRISPolicySearchBox(String keyword, String searchType){
		List<String> keywords = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			keywords.add('%' + text.replaceAll('(%|_)', '\\\\$1') + '%');
		}

		String soqlWhere = '';
		String childRelationName = '';
		// 契約者
		if(searchType == I_Const.SEARCH_TYPE_OWNER){
			childRelationName = 'E_PolicyContractors__r';
			soqlWhere = 'AND (Name like :keywords OR E_CLTPF_ZCLKNAME__c like :keywords) '
					  + 'AND E_CLTPF_FLAG01__c = \'1\' ';
		// 被保険者名
		} else if(searchType == I_Const.SEARCH_TYPE_INSURED){
			childRelationName = 'E_PolicyInsureds__r';
			soqlWhere = 'AND (Name like :keywords OR E_CLTPF_ZCLKNAME__c like :keywords) '
					  + 'AND E_CLTPF_FLAG02__c = \'1\' ';
		}

		List<Contact> result = new List<Contact>();
		String soql = 'SELECT Id'
					+ ', Name '
					+ ', E_CLTPF_ZCLKNAME__c '
					+ ', E_CLTPF_CLNTNUM__c '
					+ ', E_COMM_ZCLADDR__c '
					+ ', E_CLTPF_ZCLKADDR__c '
					+ ', E_CLTPF_DOB__c '
					+ ', E_CLTPF_ZKNJSEX__c '
					+ ', E_CLTPF_FLAG01__c '
					+ ', E_CLTPF_FLAG02__c '
					+ ', Age__c '
					+ ', (SELECT Id FROM ' + childRelationName + ' LIMIT 1) '
					+ 'FROM Contact '
					+ 'WHERE E_IsAgent__c = false '
					+ soqlWhere
					+ 'LIMIT ' + I_Const.SEARCH_CONTACT_SOQL_LIMIT;
system.debug(soql);
		result = Database.query(soql);

		return result;
	}

	// IRIS Day2
	/**
	 * 契約者、被保険者検索（検索ボックス）
	 */
	//IRISエンハンス IRIS-749 IRIS-750 ここから
	public static List<Contact> getRecsIRISPolicySearchBox(String keyword, String searchType,String selection){
		List<String> keywords = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			keywords.add('%' + text.replaceAll('(%|_)', '\\\\$1') + '%');
		}

		//名前項目 検索条件文
		String nameKeywords = '';
		nameKeywords = E_Util.createAndSearchQuery(keyword,'Name');

		//顧客名(カナ)項目 検索条件文
		String kanaKeywords = '';
		kanaKeywords = E_Util.createAndSearchQuery(keyword,'E_CLTPF_ZCLKNAME__c');

		String soqlWhere = '';
		String childRelationName = '';
		// 契約者
		if(searchType == I_Const.SEARCH_TYPE_OWNER){
			childRelationName = 'E_PolicyContractors__r';
			//AND検索
			if(selection == 'and'){
				soqlWhere = 'AND ((' + nameKeywords + ') OR (' + kanaKeywords + ')) '
					  + 'AND E_CLTPF_FLAG01__c = \'1\' ';
			//OR検索
			}else{
				soqlWhere = 'AND (Name like :keywords OR E_CLTPF_ZCLKNAME__c like :keywords) '
					  + 'AND E_CLTPF_FLAG01__c = \'1\' ';
			}

		// 被保険者名
		} else if(searchType == I_Const.SEARCH_TYPE_INSURED){
			childRelationName = 'E_PolicyInsureds__r';
			//AND検索
			if(selection == 'and'){
				soqlWhere = 'AND ((' + nameKeywords + ') OR (' + kanaKeywords + ')) '
					  + 'AND E_CLTPF_FLAG02__c = \'1\' ';
			//OR検索
			}else{
				soqlWhere = 'AND (Name like :keywords OR E_CLTPF_ZCLKNAME__c like :keywords) '
					  + 'AND E_CLTPF_FLAG02__c = \'1\' ';
			}
			
		}

		List<Contact> result = new List<Contact>();
		String soql = 'SELECT Id'
					+ ', Name '
					+ ', E_CLTPF_ZCLKNAME__c '
					+ ', E_CLTPF_CLNTNUM__c '
					+ ', E_COMM_ZCLADDR__c '
					+ ', E_CLTPF_ZCLKADDR__c '
					+ ', E_CLTPF_DOB__c '
					+ ', E_CLTPF_ZKNJSEX__c '
					+ ', E_CLTPF_FLAG01__c '
					+ ', E_CLTPF_FLAG02__c '
					+ ', Age__c '
					+ ', (SELECT Id FROM ' + childRelationName + ' LIMIT 1) '
					+ 'FROM Contact '
					+ 'WHERE E_IsAgent__c = false '
					+ soqlWhere
					+ 'LIMIT ' + I_Const.SEARCH_CONTACT_SOQL_LIMIT;
system.debug(soql);
		result = Database.query(soql);

		return result;
	}

	//IRISエンハンス IRIS-749 IRIS-750 ここまで

	/**
	 * 募集人名(I_UpdateLogBatch)
	 * @param  Set<ID>:IDのリスト
	 * @return Map<Id, Contact>:パラメータのIDとContactレコード
	 */
	public static Map<Id, Contact> getRecsByIds(Set<Id> Ids){
		Map<Id, Contact> result = new Map<Id, Contact>();
		for(Contact cnt : [SELECT
							Id
							, Name					  //氏名
							, E_Account__c				//代理店事務所(数式)
							FROM Contact
						   WHERE Id in :Ids]){
			result.put(cnt.Id, cnt);
		}
		return result;
	}

	// P16-0003 Atria対応開発
	/**
	 * 募集人顧客コードからContactを取得.
	 *
	 * @param String 募集人顧客コード
	 * @return Contact 募集人レコード
	 */
	public static Contact getRecByAgClCode(String agClCode) {
		
		Contact result = new Contact();
		
		List<Contact> lst =
			[
				Select
					Id, E_CL3PF_CLNTNUM__c, E_CL3PF_AGNTNUM__c
				From
					Contact
				Where
					E_CL3PF_CLNTNUM__c =: agClCode
				Limit 1
			];
		
		if(!lst.isEmpty()) {
			result = lst.get(0);
		}
		
		return result;
	}

	/**
	 * 事務所IDと法人/個人区分のMapを作成
	 *　@param Map<String,Account> 代理店格顧客コードと事務所のMap
	 * @return Map<Account,String> 事務所と法人/個人区分のMap
	 */
	public static Map<String,String> getCustomerCodeMap (Set<String> keys){
		Map<String,String> result = new Map<String,String>();
		for(Contact cnt : [SELECT 
							Id
							, E_CLTPF_CLTTYPE__c		//法人／個人区分
							, E_CLTPF_CLNTNUM__c		//顧客番号
							FROM Contact 
							WHERE E_CLTPF_CLNTNUM__c IN :keys
							]){
			if(String.isNotBlank(cnt.E_CLTPF_CLNTNUM__c)){
				result.put(cnt.E_CLTPF_CLNTNUM__c,cnt.E_CLTPF_CLTTYPE__c);
			}
		}
		return result;
	}

}