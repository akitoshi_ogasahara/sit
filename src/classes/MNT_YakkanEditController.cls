global with sharing class MNT_YakkanEditController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	public MNT_YakkanEditExtender getExtender() {return (MNT_YakkanEditExtender)extender;}
	public yakkanFileList yakkanFileList {get; private set;}
	{
	setApiVersion(42.0);
	}
	public MNT_YakkanEditController(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.InsType__c;
		f = I_ContentMaster__c.fields.AgencyCode__c;
		f = I_ContentMaster__c.fields.ContractDateFrom__c;
		f = I_ContentMaster__c.fields.GuideAgreeDiv__c;
		f = I_ContentMaster__c.fields.ContractDateTo__c;
		f = I_ContentMaster__c.fields.isSitePublishFlg__c;
		f = I_ContentMaster__c.fields.YakkanType__c;
		f = I_ContentMaster__c.fields.filePath__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_YakkanEditController';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addField('FormNo__c');
			mainQuery.addField('DisplayFrom__c');
			mainQuery.addField('ValidTo__c');
			mainQuery.addField('InsType__c');
			mainQuery.addField('AgencyCode__c');
			mainQuery.addField('ContractDateFrom__c');
			mainQuery.addField('GuideAgreeDiv__c');
			mainQuery.addField('ContractDateTo__c');
			mainQuery.addField('isSitePublishFlg__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			yakkanFileList = new yakkanFileList(new List<I_ContentMaster__c>(), new List<yakkanFileListItem>(), new List<I_ContentMaster__c>(), null);
			listItemHolders.put('yakkanFileList', yakkanFileList);
			query = new SkyEditor2.Query('I_ContentMaster__c');
			query.addField('YakkanType__c');
			query.addField('filePath__c');
			query.addWhere('ParentContent__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('yakkanFileList', 'ParentContent__c');
			yakkanFileList.queryRelatedEvent = False;
			query.limitRecords(3);
			queryMap.put('yakkanFileList', query);
			registRelatedList('ChildContents__r', 'yakkanFileList');
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_YakkanEditExtender(this);
			init();
			yakkanFileList.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
				 if((ApexPages.currentPage().getParameters().get('clone') == null || Decimal.valueOf(ApexPages.currentPage().getParameters().get('clone')) != 1) && I_ContentMaster__c.fields.ParentContent__c.getDescribe().createable){
					 for (Integer i = 0; i < 3; i++) {
						 yakkanFileList.add();
					 }
				 }
			}

			extender.init();
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class yakkanFileListItem extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		yakkanFileListItem(yakkanFileList holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class yakkanFileList extends SkyEditor2.ListItemHolder {
		public List<yakkanFileListItem> items{get; private set;}
		@TestVisible
			yakkanFileList(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<yakkanFileListItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new yakkanFileListItem(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}