@isTest
public class TestMNT_Util {

	@isTest
	private static void saveParentWithAttachmentTest(){

	I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険');

        //テスト用のcsvファイル作成
        String body = 'abc';
        Attachment att = new Attachment(
        	Name = 'TestData.txt',
        	Body = Blob.valueOf(body)
        );

        Test.startTest();
        MNT_Util.saveParentWithAttachment(content, att);
        Test.stopTest();

        Attachment selectAtt = [SELECT Id,Name,ParentId FROM Attachment LIMIT 1];
        I_ContentMaster__c selectContent = [SELECT Id FROM I_ContentMaster__c LIMIT 1];

        System.assertEquals(selectContent.Id,selectAtt.parentId);
	}
}