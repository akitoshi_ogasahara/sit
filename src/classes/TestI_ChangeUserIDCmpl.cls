@isTest
private class TestI_ChangeUserIDCmpl {
	private static User testuser;
	private static E_IDCPF__c eidc;
	private static E_IDRequest__c eidr;
	private static Account account;
	private static Contact contact;
	private static RecordType recordType; 
	private static datetime nowDateTime = system.now();
	static void init(){
		insertE_bizDataSyncLog();
		insertMessage();
		insertEmail();
		system.debug('this is init');
	}
	////正常系
	//@isTest static void testStandard01() {
	//	init();
	//	createDataCommon();
	//	I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

	//	Test.startTest();
	//	System.runAs(testuser){
	//		String cryptoParam = createStandardParam();
	//		PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
	//		//パラメータをセット
	//		pageRef.getParameters().put('Param',cryptoParam);
	//		Test.setCurrentPage(pageRef);
	//		ctrl.init();
	//	}
	//	Test.stopTest();
	//}

	////正常系
	////共同GWユーザ
	//@isTest static void testStandard02() {
	//	init();
	//	createDataCommon();
	//	I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

	//	Test.startTest();
	//	System.runAs(testuser){
	//		String cryptoParam = createStandardParam();
	//		PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
	//		//パラメータをセット
	//		pageRef.getParameters().put('Param',cryptoParam);
	//		pageRef.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
	//		Test.setCurrentPage(pageRef);
	//		ctrl.init();
	//	}
	//	Test.stopTest();

	//	System.assertEquals(ctrl.sendUrl, 'E_Home');
	//}

	////エラー確認
	//@isTest static void testNotMatch01() {
	//	init();
	//	createDataCommon();
	//	I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

	//	Test.startTest();
	//	System.runAs(testuser){
	//		String cryptoParam = createNotMatchParam();
	//		PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
	//		//パラメータをセット
	//		pageRef.getParameters().put('Param',cryptoParam);
	//		Test.setCurrentPage(pageRef);
	//		ctrl.init();
	//	}
	//	Test.stopTest();
	//}

	////エラー確認
	////共同GWユーザ IDリクエスト取得失敗
	//@isTest static void testNotMatch02() {
	//	init();
	//	createDataCommon();
	//	I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

	//	Test.startTest();
	//	System.runAs(testuser){
	//		String cryptoParam = createNotMatchParam();
	//		PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
	//		//パラメータをセット
	//		pageRef.getParameters().put('Param',cryptoParam);
	//		pageRef.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
	//		Test.setCurrentPage(pageRef);
	//		ctrl.init();
	//	}
	//	Test.stopTest();

	//	System.assertEquals(ctrl.errorCheck, true);
	//	System.assertEquals(ctrl.pageMessages.getErrorMessages()[0].summary, 'ERR|009');
	//}

	//エラー確認
	//現在時刻がサービス時間外エラー
	@isTest static void testNotMatch03() {
		init();
		createDataCommon();
		I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

		Test.startTest();
		System.runAs(testuser){
			String cryptoParam = createNotMatchParam();
			PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
			//パラメータをセット
			pageRef.getParameters().put('Param',cryptoParam);
			Test.setCurrentPage(pageRef);
			//Ebiz連携ログレコード作成
			E_BizDataSyncLog__c bizLog = new E_BizDataSyncLog__c(Kind__c = '1', DataSyncStartDate__c = Datetime.now());
			insert bizLog;
			ctrl.init();
		}
		Test.stopTest();

		System.assertEquals(ctrl.errorCheck, true);
		System.assertEquals(ctrl.pageMessages.getErrorMessages()[0].summary, 'ERR|007');
	}

	@isTest static void testNotMatch04() {
		init();
		createDataCommon();
		I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

		Test.startTest();
		System.runAs(testuser){
			String cryptoParam = createNotMatchParam();
			PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
			//パラメータをセット
			pageRef.getParameters().put('Param',cryptoParam);
			Test.setCurrentPage(pageRef);
			//Ebiz連携ログレコード作成
			//E_BizDataSyncLog__c bizLog = new E_BizDataSyncLog__c(Kind__c = '1', DataSyncStartDate__c = Datetime.now());
			//insert bizLog;
			ctrl.init();
			ctrl.sendUrl='';
			ctrl.browserHandler = null;
			
		}
		Test.stopTest();
	}
	@isTest static void testNotMatch05() {
		init();
		createDataCommon();
		I_ChangeUserIDCmpl ctrl = new I_ChangeUserIDCmpl();

		Test.startTest();
		System.runAs(testuser){
			String cryptoParam = createStandardParam();
			PageReference pageRef = Page.IRIS_ChangeUserIDCmpl;
			//パラメータをセット
			pageRef.getParameters().put('Param',cryptoParam);
			Test.setCurrentPage(pageRef);
			//Ebiz連携ログレコード作成
			//E_BizDataSyncLog__c bizLog = new E_BizDataSyncLog__c(Kind__c = '1', DataSyncStartDate__c = Datetime.now());
			//insert bizLog;
			ctrl.init();
			ctrl.sendUrl='';
			ctrl.browserHandler = null;
			
		}
		Test.stopTest();
	}

	/* test data */
	static void createDataCommon(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			testuser = new User();
			testuser.Username = 'updateEmailtest@sfdc.com';
			testuser.Alias = 'テスト花子';
			testuser.Email = 'test@test.com';
			testuser.EmailEncodingKey = 'UTF-8';
			testuser.LanguageLocaleKey = 'ja';
			testuser.LastName = 'テスト';
			testuser.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			testuser.ProfileId = pro.Id;
			testuser.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			testuser.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
			
			// Account
			account = new Account(Name = 'testAccount');
			insert account;
			
			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id);
			insert contact;
			
			testuser.ContactId = contact.Id;
			insert testuser;

			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add('E_PermissionSet_Base');//基本セット　全ユーザ対象
			permissionNames.add('E_PermissionSet_Content');

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
		   }
			
			//
			eidc = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = testuser.Id,
				ZPASSWD02DATE__c = '99999999',
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
			);
			insert eidc;

			eidr = new E_IDRequest__c(
				E_IDCPF__c = eidc.Id                //ID管理
				,ZUPDFLAG__c = 'E'                  //リクエスト種別
				,ZWEBID__c = eidc.ZWEBID__c         //ID番号
				,ZEMAILAD__c = testuser.Email                 //E-MAILアドレス
				,dataSyncDate__c = null             //変更受付日時
			);
			insert eidr;
		}
	}

	private static void insertMessage(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
			String type = 'メッセージ';
			String param = 'SPW|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|002';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|003';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'ERR|009';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'ERR|008';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'ERR|007';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			type = 'ディスクレイマー';
			param = 'SPW|004';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|005';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|006';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|007';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|008';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'LGN|201';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'LGN|202';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			insert insMessage;
		}
	}
	
	private static void insertEmail(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_Email__c eMail = new E_Email__c(NAME = 'E_101',Email__c = 'xxxx@xx.xx');
			insert eMail;
		}
	}
	private static void insertE_bizDataSyncLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
			insert log;
		}    
	}

	//正常系パラメータ
	private static String createStandardParam(){
		//IDリクエストオブジェクトのレコードId
		String recId = 'recId='+ eidr.Id;

		String nextUrl = '&url=' + 'http://hogehoge/apex/nnlinkage/E_home';

		String param = recId + nextUrl;
		//パラメータ暗号化
		String cryptoParam = E_EncryptUtil.getEncryptedString(param);

		cryptoParam = EncodingUtil.urlEncode(cryptoParam , 'UTF-8');

		return cryptoParam;
	}

	//エラー用パラメータ
	private static String createNotMatchParam(){
		//IDリクエストオブジェクトのレコードId
		String recId = 'recId='+ '111111111111111';

		String nextUrl = '&url=' + 'http://hogehoge/apex/nnlinkage/E_home';

		String param = recId + nextUrl;
		//パラメータ暗号化
		String cryptoParam = E_EncryptUtil.getEncryptedString(param);

		cryptoParam = EncodingUtil.urlEncode(cryptoParam , 'UTF-8');

		return cryptoParam;
	}
}