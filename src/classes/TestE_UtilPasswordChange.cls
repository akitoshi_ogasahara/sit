/**
* 
*/
@isTest
private class TestE_UtilPasswordChange {
    private static E_IDCPF__c IDCPF;
    private static User thisUser;
    private static String oldPw;
    private static String newPw;
    private static String verPw;
    private static MAP<String,String> errMsgMap = new MAP <String,String>();
    
    //仮パスワード判定
    //仮手続PW使用がtrue
    private static testmethod void getIsTempPassWordTest01(){
        createDataCommon();
        IDCPF.UseTempProcedurePW__c = true;
        system.runAs(thisUser){
            insert IDCPF;
            system.assertEquals(true, E_UtilPasswordChange.getIsTempPassWord(IDCPF));
        }
    }
    
    //仮パスワード判定
    //仮手続PW使用がfalse
    private static testmethod void getIsTempPassWordTest02(){
        createDataCommon();
        IDCPF.UseTempProcedurePW__c = false;
        system.runAs(thisUser){
            insert IDCPF;
            system.assertEquals(false, E_UtilPasswordChange.getIsTempPassWord(IDCPF));
        }
    }
    
    //通常のパスワード変更チェック（エラーなし）
    private static testmethod void standard(){
        oldPw = '1234567801';
        newPw = 'terrask2';
        verPw = 'terrask2';
        createDataCommon();
        IDCPF.UseTempProcedurePW__c = true;
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(oldPw, E_Util.getSysDate(null));
        //IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(1, errMsgList.size(), errMsgList);
        }
    }
    
    //エラーパターン：各入力項目が空の時
    private static testmethod void inputNull(){
        createDataCommon();
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        oldPw = '';
        newPw = '';
        verPw = '';
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(3, errMsgList.size());
            for(String item : errMsgList){
                String[] work = item.split(':');
                errMsgMap.put(work[0],work[1]);
            }
            system.assertEquals(' PPC|001', errMsgMap.get(E_Const.LABEL_2P_OLD));
            system.assertEquals(' PPC|003', errMsgMap.get(E_Const.LABEL_2P_NEW));
            system.assertEquals(' PPC|005', errMsgMap.get(E_Const.LABEL_2P_VER));
        }
    }
    
    //エラーパターン：入力項目が一致しない時
    private static testmethod void inputNotEqual(){
        createDataCommon();
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        oldPw = 'terrask2';
        newPw = 'terrask3';
        verPw = 'terrask4';
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(2, errMsgList.size());
            for(String item : errMsgList){
                String[] work = item.split(':');
                errMsgMap.put(work[0],work[1]);
            }
            system.assertEquals(' PPC|002', errMsgMap.get(E_Const.LABEL_2P_OLD));
            system.assertEquals(' PPC|006', errMsgMap.get(E_Const.LABEL_2P_VER));
        }
    }
    
    //エラーパターン：バリデーションエラー（禁則文字に対して）
    private static testmethod void errMsgValidateIsEnAlphaNum(){
        createDataCommon();
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        oldPw = 'terrask1';
        newPw = 'terrask㈱';
        verPw = 'terrask㈱';
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(2, errMsgList.size());
            for(String item : errMsgList){
                String[] work = item.split(':');
                errMsgMap.put(work[0],work[1]);
            }
            system.assertEquals(' PPC|004', errMsgMap.get(E_Const.LABEL_2P_NEW));
        }
    }

    //エラーパターン：バリデーションエラー（英字のみの場合）
    private static testmethod void errMsgValidateIsEnNum(){
        createDataCommon();
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        oldPw = 'terrask1';
        newPw = 'terrasky';
        verPw = 'terrasky';
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(2, errMsgList.size());
            for(String item : errMsgList){
                String[] work = item.split(':');
                errMsgMap.put(work[0],work[1]);
            }
            system.assertEquals(' PPC|004', errMsgMap.get(E_Const.LABEL_2P_NEW));
        }
    }
    
    //エラーパターン：バリデーションエラー（数字のみの場合）
    private static testmethod void errMsgValidateIsEnAlpha(){
        createDataCommon();
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        oldPw = 'terrask1';
        newPw = '12345678';
        verPw = '12345678';
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(2, errMsgList.size());
            for(String item : errMsgList){
                String[] work = item.split(':');
                errMsgMap.put(work[0],work[1]);
            }
            system.assertEquals(' PPC|004', errMsgMap.get(E_Const.LABEL_2P_NEW));
        }
    }

    //エラーパターン：バリデーションエラー（8文字ではない時）
    private static testmethod void errMsgValidateNot8(){
        createDataCommon();
        IDCPF.ZPASSWD02DATE__c = '20150101';
        IDCPF.ZSTATUS02__c = '1';
        IDCPF.ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b'; // terrask1
        oldPw = 'terrask1';
        newPw = 'terrasky9';
        verPw = 'terrasky9';
        system.runAs(thisUser){
            insert IDCPF;
            List<String> errMsgList = E_UtilPasswordChange.validatePassword(IDCPF, oldPw,newPw,verPw);
            system.assertEquals(2, errMsgList.size());
            for(String item : errMsgList){
                String[] work = item.split(':');
                errMsgMap.put(work[0],work[1]);
            }
            system.assertEquals(' PPC|004', errMsgMap.get(E_Const.LABEL_2P_NEW));
        }
    }
    
    //手続パスワード変更
    //仮手続PW使用がtrue
    private static testmethod void updateProceduralPasswordTest01(){
        createDataCommon();
        IDCPF.UseTempProcedurePW__c = true;
        IDCPF.RegularProcedurePW__c = null;
        IDCPF.ZPASSWD02DATE__c = null;
        IDCPF.ZSTATUS02__c = null;
        IDCPF.RegularProcedurePWChangeDate__c = null;
        String password = 'pw123456';
        system.runAs(thisUser){
            insert IDCPF;
            
            Test.startTest();
            E_UtilPasswordChange.updateProceduralPassword(IDCPF, password);
            Test.stopTest();
            
            E_IDCPF__c result = getIDCPF(IDCPF.Id);
            system.assertEquals(false, result.UseTempProcedurePW__c);
            system.assertEquals(E_EncryptUtil.encrypt(password), result.RegularProcedurePW__c);
            system.assertEquals(null, result.ZPASSWD02DATE__c); //2016/7仕様変更のため変更
            system.assertEquals(E_Const.PW_STATUS_OK, result.ZSTATUS02__c);
            system.assertEquals(null, result.RegularProcedurePWChangeDate__c);
        }
    }
    
    //手続パスワード変更
    //仮手続PW使用がfalse
    private static testmethod void updateProceduralPasswordTest02(){
        createDataCommon();
        IDCPF.UseTempProcedurePW__c = false;
        IDCPF.RegularProcedurePW__c = null;
        IDCPF.ZPASSWD02DATE__c = null;
        IDCPF.ZSTATUS02__c = null;
        IDCPF.RegularProcedurePWChangeDate__c = null;
        String password = 'pw123456';
        system.runAs(thisUser){
            insert IDCPF;
            
            Test.startTest();
            E_UtilPasswordChange.updateProceduralPassword(IDCPF, password);
            Test.stopTest();
            
            E_IDCPF__c result = getIDCPF(IDCPF.Id);
            system.assertEquals(false, result.UseTempProcedurePW__c);
            system.assertEquals(E_EncryptUtil.encrypt(password), result.RegularProcedurePW__c);
            system.assertEquals(null, result.ZPASSWD02DATE__c);
            system.assertEquals(null, result.ZSTATUS02__c);
            system.assertEquals(E_Util.getSysDate(null), result.RegularProcedurePWChangeDate__c);
        }
    }
    private static E_IDCPF__c getIDCPF(Id idcpfId){
        return [
            select Id
                , RegularProcedurePW__c
                , UseTempProcedurePW__c
                , ZPASSWD02DATE__c
                , ZSTATUS02__c
                , RegularProcedurePWChangeDate__c
            from E_IDCPF__c
            where Id = :IDCPF.Id
            limit 1
        ];
    }
    
    private static void createDataCommon(){
        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            IDCPF = Teste_TestUtil.createIDCPF(false,thisUser.id);    
            E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
            insert log;
            insertMessage();
        }
    }
    
    private static void insertMessage(){
            LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
            String type = 'メッセージ';
            String param = 'PPC|001';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'PPC|002';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'PPC|003';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'PPC|004';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'PPC|005';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'PPC|006';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            insert insMessage;
    }

    
    
}