/**
 * 基底となるExtenderの抽象クラスです
 */
public abstract class E_AbstractEditExtender extends E_AbstractExtender  {
	
	/**
	 * 登録情報関連機能判定
	 */
	protected override boolean doAuthUser (String userId) {
		//オブジェクトアクセス制限
		if (!access.isAccessibleCustomer()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		
		//パラ―メータ確認
		if (!E_Util.isValidateId(userId,Schema.User.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		if (!Id.valueOf(userID).equals(UserInfo.getUserId())) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		return true;
	}
	
	/**
	 * 登録情報関連機能判定
	 */
	protected override virtual boolean doAuthContact (String contactId) {
		//機能制限確認
		//住所/電話・FAX番号変更
		if (this instanceof E_ADRPFExtender) {
			if (!getIsTrCodeAddress()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}
		}
		
		//オブジェクトアクセス制限
		if (!access.isAccessibleCustomer()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		
		//パラ―メータ確認
		if (!E_Util.isValidateId(contactId, Schema.Contact.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		//クエリ発行
		Contact contact = E_ContactDao.getRecById(contactId);
		if (contact == null) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		if (!Id.valueOf(contactId).equals(contact.id)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		return true;
	}
}