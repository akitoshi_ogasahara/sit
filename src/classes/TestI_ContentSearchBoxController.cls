@isTest
private class TestI_ContentSearchBoxController {

	/**
	 * キーワードがブランクの時
	 */ 
	private static testMethod void should_not_search_record_when_keyword_length_is_empty() {

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '';
			ctrl.doSearch();
			ctrl.getSeachBoxLog();
			System.assertEquals(I_ContentSearchBoxController.SEARCH_LABEL_CATEGORY_CONTENT, ctrl.getSearchCategoryLabel());
			System.assertEquals(I_TubeConnectHelper.getInstance(), ctrl.getTubeHelper());
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * キーワードが2文字以下の時
	 */ 
	private static testMethod void should_not_search_record_when_keyword_length_is_less_than_2_characters() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = 'a';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * キーワードと一致するレコードが見つからなかったとき
	 */ 
	private static testMethod void should_not_find_pages_and_files_when_has_not_matched_content_with_keyword() {

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '検索不可';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * メニューパターン
	 */ 
	private static testMethod void should_find_page_menu_when_has_matched_content_with_keyword() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 メニュー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * ページデータを持たないメニューレコード
	 */
	private static testMethod void should_find_page_from_menu_when_target_did_not_have_page() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '代理店 メニュー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}
	

	/**
	 * ライブラリメニューでリンクURLを持たないパターン
	 */
	private static testMethod void should_not_find_page_from_library_menu_when_did_not_have_value_at_link_url_field() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '商品 メニュー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * ライブラリメニューでリンクURLを持つパターン
	 */
	private static testMethod void should_find_page_from_library_menu_when_have_value_at_link_url_field() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '継続 メニュー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * コンテンツ検索対象外にチェックがついているパターン
	 */
	private static testMethod void should_not_find_page_from_menu_when_target_was_excluded() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '特集 メニュー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
	}
}

	/**
	 * ライブラリメニューで親が存在するパターン
	 */
	private static testMethod void should_find_page_from_library_menu_when_target_is_child_record() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '定期 メニュー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
	}
}

	/**
	 * ライブラリに属さないページのパターン
	 */
	private static testMethod void should_find_page_from_page_when_parent_menu_is_not_library_category() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 詳細';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * ライブラリに属するページで特集以外のパターン
	 */
	private static testMethod void should_find_page_from_page_when_parent_menu_is_not_feature_category() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '商品 view';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * ライブラリ/特集に属するページで実行日範囲外のパターン
	 */
	private static testMethod void should_not_find_page_from_feature_library_page_when_executed_date_is_out_of_range() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '特集 一覧';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * ライブラリ/特集に属するページで実行日範囲内のパターン
	 */
	private static testMethod void should_find_page_featrue_library_page_when_when_when_executed_date_is_in_a_range() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '今月 特集';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * セールツールに属さないコンテンツのパターン
	 */
	private static testMethod void should_find_page_from_content_when_parent_is_not_salestool() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 コンポ';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * 実行日が範囲外のセールツールコンテンツのパターン
	 */
	private static testMethod void should_find_page_from_salestool_content_when_executed_date_is_out_of_range() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '定期 コンポ';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * 実行日が範囲内のセールツールコンテンツのパターン
	 */
	private static testMethod void should_find_page_from_salestool_content_when_executed_date_is_in_a_range() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '定期 comp';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * よくあるご質問パターン
	 */ 
	private static testMethod void should_find_qa_page_when_has_matched_content_with_keyword() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = 'よくある コンポ';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * お知らせパターン
	 */ 
	private static testMethod void should_find_news_page_when_has_matched_content_with_keyword() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = 'お知らせ news component';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * クリックアクションが対象外であるパターン
	 */ 
	private static testMethod void should_not_find_page_from_content_when_value_at_content_field_is_not_enabled() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '商品 コンポ';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * クリックアクションが対象外であるパターン
	 */ 
	private static testMethod void should_not_find_page_from_content_when_target_was_excluded() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '商品 comp';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * chatterfileを検索するパターン
	 */
	private static testMethod void should_find_file_from_chatter_when_has_matched_content_with_keyword() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 chat';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(1, ctrl.fileRows.size());
		}
	}

	/**
	 * chatterfileが存在しないパターン
	 */
	private static testMethod void should_not_find_file_from_chatter_when_target_did_not_have_chatter_file () {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 チャット';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * chatterfileが期限ぎれのパターン
	 */
	private static testMethod void should_not_find_file_from_chatter_when_executed_date_is_out_of_range() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 ったー';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * nnlinkを検索するパターン
	 */
	private static testMethod void should_find_file_from_nnlink_when_has_matched_content_with_keyword() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 nn';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(1, ctrl.fileRows.size());
		}
	}

	/**
	 * 参照先であるnnlinkが存在しないパターン
	 */
	private static testMethod void should_not_find_file_from_nnlink_when_nnlink_data_did_not_exists() {
		fixSearchResult();
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 エヌ';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * attachmentを検索するパターン
	 */
	private static testMethod void should_find_file_from_attachment_when_has_matched_content_with_keyword() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 添付';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(1, ctrl.fileRows.size());
		}
	}

	/**
	 * 参照先であるattachmenが存在しないパターン
	 */
	private static testMethod void should_not_find_file_from_attachment_when_attachment_data_did_not_exists() {
		fixSearchResult();

		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.keyword = '契約 attach';
			ctrl.doSearch();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * 最近検索したキーワードから検索を行うパターン
	 */
	private static testMethod void should_find_page_from_menu_when_it_is_executed_from_log_keyword() {
		fixSearchResult();
		PageReference ref = Page.IRIS_Top;
		Map<String, String> params = ref.getParameters();
		params.put('searchType', I_ContentSearchBoxController.SEARCH_TYPE_PAGE_AND_FILE);
		params.put('keyword', '契約 メニュー');
		Test.setCurrentPageReference(ref);
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.doSearchLog();
			System.assertEquals(true, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}

	}

	/**
	 * 動画検索の検証を行うパターン
	 */
	private static testMethod void should_validate_nntube_finding_conndition_when_search_type_is_movie() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.searchType = I_ContentSearchBoxController.SEARCH_TYPE_MOVIE;
			ctrl.searchTypeName = I_ContentSearchBoxController.SEARCH_TYPE_LABEL_MOVIE;
			ctrl.keyword = '動画検索';
			ctrl.doSearch();
			System.assertEquals(false, ctrl.showPageAndFileList);
			System.assertEquals(true, ctrl.showMovieList);
			System.assertEquals(true, ctrl.isRequestingToNNtube);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.contentRows.size());
			System.assertEquals(0, ctrl.fileRows.size());
		}
	}

	/**
	 * 検索結果をパースするパターン
	 */
	private static testMethod void should_succeed_to_parse_json_when_has_received_valid_data() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.searchType = I_ContentSearchBoxController.SEARCH_TYPE_MOVIE;
			ctrl.searchTypeName = I_ContentSearchBoxController.SEARCH_TYPE_LABEL_MOVIE;
			ctrl.keyword = '動画検索';
			ctrl.doSearch();

			I_MovieSearchResult.NNTubeResult result = new I_MovieSearchResult.NNTubeResult();
			result.resultcode = '0000';
			result.moviesum = 1;
			result.meta = new List<I_MovieSearchResult.NNTubeMedia>();
			I_MovieSearchResult.NNTubeMedia media = new I_MovieSearchResult.NNTubeMedia();
			media.publication_date = '2000/01/01';
			media.duration = 200;
			media.mid = 555;
			result.meta.add(media);

			ctrl.nntubeJsonResult = JSON.serialize(result);
			ctrl.renderNNTubeMedia();
			System.assertEquals(false, ctrl.showPageAndFileList);
			System.assertEquals(true, ctrl.showMovieList);
			System.assertEquals(false, ctrl.isRequestingToNNtube);
			System.assertEquals(0, ctrl.searchMessages.size());
			System.assertEquals(1, ctrl.movieRows.size());
		}
	}

	/**
	 * 検索結果のパース時に画面側で不整合が起きてるパターン
	 */
	private static testMethod void should_fail_to_parse_json_when_view_has_invalid_status() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();

			I_MovieSearchResult.NNTubeResult result = new I_MovieSearchResult.NNTubeResult();
			result.resultcode = '0000';
			result.moviesum = 1;
			result.meta = new List<I_MovieSearchResult.NNTubeMedia>();
			I_MovieSearchResult.NNTubeMedia media = new I_MovieSearchResult.NNTubeMedia();
			media.publication_date = '2000/01/01';
			media.duration = 200;
			media.mid = 555;
			result.meta.add(media);

			ctrl.nntubeJsonResult = JSON.serialize(result);
			ctrl.renderNNTubeMedia();
			System.assertEquals(false, ctrl.showPageAndFileList);
			System.assertEquals(false, ctrl.showMovieList);
			System.assertEquals(false, ctrl.isRequestingToNNtube);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.movieRows.size());
		}
	}

	/**
	 * 検索結果がエラーを返してきたパターン
	 */
	private static testMethod void should_fail_to_parse_json_when_has_received_exception_data() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.searchType = I_ContentSearchBoxController.SEARCH_TYPE_MOVIE;
			ctrl.searchTypeName = I_ContentSearchBoxController.SEARCH_TYPE_LABEL_MOVIE;
			ctrl.keyword = '動画検索';
			ctrl.doSearch();

			I_MovieSearchResult.NNTubeResult result = new I_MovieSearchResult.NNTubeResult();
			result.resultcode = '9999';
			result.moviesum = 0;
			result.meta = null;

			ctrl.nntubeJsonResult = JSON.serialize(result);
			ctrl.renderNNTubeMedia();
			System.assertEquals(false, ctrl.showPageAndFileList);
			System.assertEquals(true, ctrl.showMovieList);
			System.assertEquals(false, ctrl.isRequestingToNNtube);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.movieRows.size());
		}
	}

	/**
	 * 検索結果が0件のパターン
	 */
	private static testMethod void should_fail_to_parse_json_when_has_received_empty_result() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.searchType = I_ContentSearchBoxController.SEARCH_TYPE_MOVIE;
			ctrl.searchTypeName = I_ContentSearchBoxController.SEARCH_TYPE_LABEL_MOVIE;
			ctrl.keyword = '動画検索';
			ctrl.doSearch();

			I_MovieSearchResult.NNTubeResult result = new I_MovieSearchResult.NNTubeResult();
			result.resultcode = '0000';
			result.moviesum = 0;
			result.meta = null;

			ctrl.nntubeJsonResult = JSON.serialize(result);
			ctrl.renderNNTubeMedia();
			System.assertEquals(false, ctrl.showPageAndFileList);
			System.assertEquals(true, ctrl.showMovieList);
			System.assertEquals(false, ctrl.isRequestingToNNtube);
			System.assertEquals(1, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.movieRows.size());
		}
	}

	/**
	 * 検索結果のパースに失敗したパターン
	 */
	private static testMethod void should_fail_to_parse_json_when_has_received_invalid_data_structure() {
		System.runAs(getAdmin()) {
			I_ContentSearchBoxController ctrl = new I_ContentSearchBoxController();
			ctrl.searchType = I_ContentSearchBoxController.SEARCH_TYPE_MOVIE;
			ctrl.searchTypeName = I_ContentSearchBoxController.SEARCH_TYPE_LABEL_MOVIE;
			ctrl.keyword = '動画検索';
			ctrl.doSearch();

			I_MovieSearchResult.NNTubeResult result = new I_MovieSearchResult.NNTubeResult();
			result.resultcode = '0000';
			result.moviesum = 1;
			result.meta = new List<I_MovieSearchResult.NNTubeMedia>();
			I_MovieSearchResult.NNTubeMedia media = new I_MovieSearchResult.NNTubeMedia();
			result.meta.add(media);

			ctrl.nntubeJsonResult = JSON.serialize(result);
			ctrl.renderNNTubeMedia();
			System.assertEquals(false, ctrl.showPageAndFileList);
			System.assertEquals(true, ctrl.showMovieList);
			System.assertEquals(false, ctrl.isRequestingToNNtube);
			System.assertEquals(2, ctrl.searchMessages.size());
			System.assertEquals(0, ctrl.movieRows.size());
		}
	}

	/**
	 * soslで検索できるようにIDを追加する
	 */
	private static void fixSearchResult() {
		Set<Id> fixedIds = new Set<Id>();
		fixedIds.addAll(new Map<Id, I_MenuMaster__c>([SELECT id FROM I_MenuMaster__c]).keyset());
		fixedIds.addAll(new Map<Id, I_PageMaster__c>([SELECT id FROM I_PageMaster__c]).keyset());
		fixedIds.addAll(new Map<Id, I_ContentMaster__c>([SELECT id FROM I_ContentMaster__c]).keyset());
		Test.setFixedSearchResults(new List<Id>(fixedIds));
	}

	/**
	 * 検索実行ユーザを取得
	 */
	private static User getAdmin() {
		return [SELECT id FROM User WHERE LastName =: 'fstest'][0];
	}

	@testSetup
	public static void setup() {
		TestE_TestUtil.createIDCPF(true, TestI_TestUtil.createUser(true, 'fstest', 'システム管理者').Id);

		Map<String, E_MessageMaster__c> messageMastersByKey = new Map<String, E_MessageMaster__c>();
		{
			E_MessageMaster__c messageMaster;
			messageMaster = new E_MessageMaster__c(
				Name = '契約者一覧|メッセージ',
				Key__c = '契約者一覧|メッセージ/2'
			);
			messageMastersByKey.put(messageMaster.Name, messageMaster);
		}
		insert messageMastersByKey.values();
		
		Map<String, E_CMSFile__c> csmFilesByKey = new Map<String, E_CMSFile__c>();
		{
			E_CMSFile__c cmsFile;
			cmsFile = new E_CMSFile__c(
				Name = '契約者一覧|CMSファイル',
				MessageMaster__c = messageMastersByKey.get('契約者一覧|メッセージ').Id
			);
			csmFilesByKey.put(cmsFile.Name, cmsFile);
		}
		insert csmFilesByKey.values();
		for (E_CMSFile__c cmsFile: [SELECT Id, Name, MessageMaster__c, UpsertKey__c FROM E_CMSFile__c WHERE Id IN: new Map<Id, E_CMSFile__c>(csmFilesByKey.values()).keyset()]) {
			csmFilesByKey.put(cmsFile.Name, cmsFile);
		}

		Map<String, I_MenuMaster__c> menusByKey = new Map<String, I_MenuMaster__c>();
		{
			I_MenuMaster__c menu;
			menu = new I_MenuMaster__c(
				name = 'お知らせ',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				subCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_INFO,
				UpsertKey__c = 'お知らせ|メニュー',
				IRISPageName__c = 'IRIS_Information'
			);
			menusByKey.put(menu.name, menu);
			
			menu = new I_MenuMaster__c(
				name = 'よくあるご質問|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_FAQ,
				UpsertKey__c = 'よくあるご質問|メニュー',
				IRISPageName__c = 'IRIS_FAQ'
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = '契約者一覧|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_POSSESSION_CONTACT,
				UpsertKey__c = '契約者一覧|メニュー',
				IRISPageName__c = 'IRIS_Owner'
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = '代理店一覧|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_POSSESSION_CONTACT,
				UpsertKey__c = '代理店一覧|メニュー',
				IRISPageName__c = 'IRIS_Agency'
			);
			menusByKey.put(menu.name, menu);
	
			menu = new I_MenuMaster__c(
				name = '商品別|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL,
				UpsertKey__c = '商品別|メニュー',
				IRISPageName__c = null
			);
			menusByKey.put(menu.name, menu);
	
			menu = new I_MenuMaster__c(
				name = '継続教育制度|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				subCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_INFO,
				UpsertKey__c = '継続教育制度|メニュー',
				LinkURL__c = 'test_page'
			);
			menusByKey.put(menu.name, menu);
	
			menu = new I_MenuMaster__c(
				name = '特集|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				subCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE,
				UpsertKey__c = '特集|メニュー',
				LinkURL__c = 'IRIS_FeatureList',
				ExcludeFromSearch__c = true
			);
			menusByKey.put(menu.name, menu);
		}
		insert menusByKey.values();

		Map<String, I_MenuMaster__c> innerMenusByKey = new Map<String, I_MenuMaster__c>();
		{
			I_MenuMaster__c innerMenu;
			innerMenu = new I_MenuMaster__c(
				name = '定期保険|メニュー',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL,
				ParentMenu__c = menusByKey.get('商品別|メニュー').Id,
				UpsertKey__c = '定期保険|メニュー',
				IRISPageName__c = null
			);
			innerMenusByKey.put(innerMenu.name, innerMenu);
		}
		insert innerMenusByKey.values();
		
		Map<String, I_PageMaster__c> pagesByKey = new Map<String, I_PageMaster__c>();
		{
			I_PageMaster__c page;
			page = new I_PageMaster__c(
				name = 'お知らせ',
				page_unique_key__c = 'お知らせ|ページ',
				Menu__c = menusByKey.get('お知らせ').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = 'よくあるご質問|ページ',
				page_unique_key__c = 'よくあるご質問|ページ',
				Menu__c = menusByKey.get('よくあるご質問|メニュー').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = '契約者一覧|詳細',
				page_unique_key__c = '契約者一覧|詳細',
				Menu__c = menusByKey.get('契約者一覧|メニュー').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = '商品別|overview',
				page_unique_key__c = '商品別|overview',
				Menu__c = menusByKey.get('商品別|メニュー').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = '継続教育制度|一覧',
				page_unique_key__c = '継続教育制度|一覧',
				Menu__c = menusByKey.get('継続教育制度|メニュー').Id,
				ExcludeFromSearch__c = true
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = '定期保険|リスト',
				page_unique_key__c = '定期保険|リスト',
				Menu__c = innerMenusByKey.get('定期保険|メニュー').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = '特集|一覧',
				page_unique_key__c = '特集|一覧',
				Default__c = true,
				Menu__c = menusByKey.get('特集|メニュー').Id,
				FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(1),
				FeatureDisplayTo__c = E_Util.SYSTEM_TODAY().addDays(-1)
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = '今月の特集',
				page_unique_key__c = '今月の特集',
				Default__c = false,
				Menu__c = menusByKey.get('特集|メニュー').Id,
				FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				FeatureDisplayTo__c = E_Util.SYSTEM_TODAY().addDays(1)
			);
			pagesByKey.put(page.name, page);
			
			page = new I_PageMaster__c(
				name = 'ignore特集',
				page_unique_key__c = 'ignore特集',
				Default__c = false,
				Menu__c = menusByKey.get('特集|メニュー').Id
			);
			pagesByKey.put(page.name, page);
		}
		insert pagesByKey.values();

		Map<String, I_ContentMaster__c> sectionsByKey = new Map<String, I_ContentMaster__c>();
		{
			I_ContentMaster__c section;
			section = new I_ContentMaster__c(
				name = I_Const.IRIS_CONTENTS_NAME_INFO,
				UpsertKey__c = 'お知らせ|セクション',
				Page__c = pagesByKey.get('お知らせ').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = 'よくあるご質問|セクション',
				UpsertKey__c = 'よくあるご質問|セクション',
				Page__c = pagesByKey.get('よくあるご質問|ページ').Id
			);
			sectionsByKey.put(section.name, section);
			
			section = new I_ContentMaster__c(
				name = '契約者一覧|セクション1',
				UpsertKey__c = '契約者一覧|セクション1',
				Page__c = pagesByKey.get('契約者一覧|詳細').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = '商品別|セクション1',
				UpsertKey__c = '商品別|セクション1',
				Page__c = pagesByKey.get('商品別|overview').Id
			);
			sectionsByKey.put(section.name, section);
			/*'継続教育制度|セクション1' => new I_ContentMaster__c(
				name = '継続教育制度|セクション1',
				UpsertKey__c = '継続教育制度|セクション1',
				Page__c = pagesByKey.get('継続教育制度|一覧').Id
			),*/
			section = new I_ContentMaster__c(
				name = I_Const.IRIS_CONTENTS_NAME_SALESTOOL,
				UpsertKey__c = '定期保険|section1',
				Page__c = pagesByKey.get('定期保険|リスト').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = '特集|セクション1',
				UpsertKey__c = '特集|セクション1',
				Page__c = pagesByKey.get('特集|一覧').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = '今月の|セクション1',
				UpsertKey__c = '今月の|セクション1',
				Page__c = pagesByKey.get('今月の特集').Id
			);
			sectionsByKey.put(section.name, section);
		}
		insert sectionsByKey.values();

		Map<String, I_ContentMaster__c> componentsByKey = new Map<String, I_ContentMaster__c>();
		{
			I_ContentMaster__c component;
			component = new I_ContentMaster__c(
				name = 'お知らせ|news-component',
				DateStr__c = String.valueOf(E_Util.SYSTEM_TODAY().addDays(-1)).replaceAll('-', '/'),
				UpsertKey__c = 'お知らせ|news-component',
				ParentContent__c = sectionsByKey.get(I_Const.IRIS_CONTENTS_NAME_INFO).Id
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'よくあるご質問|コンポーネント',
				UpsertKey__c = 'よくあるご質問|コンポーネント',
				ParentContent__c = sectionsByKey.get('よくあるご質問|セクション').Id
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|コンポーネント',
				UpsertKey__c = '契約者一覧|コンポーネント',
				clickAction__c = 'URL',
				LinkURL__c = 'IRIS_Owner',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|chatter',
				UpsertKey__c = '契約者一覧|chatter',
				clickAction__c = 'DL(ChatterFile)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1)
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|チャットアー',
				UpsertKey__c = '契約者一覧|チャットアー',
				clickAction__c = 'DL(ChatterFile)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1)
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|チャッター',
				UpsertKey__c = '契約者一覧|チャッター',
				clickAction__c = 'DL(ChatterFile)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(-1)
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|nnlink',
				UpsertKey__c = '契約者一覧|nnlink',
				clickAction__c = 'DL(NNLink)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1),
				filePath__c = csmFilesByKey.get('契約者一覧|CMSファイル').UpsertKey__c
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|エヌエヌリンク',
				UpsertKey__c = '契約者一覧|エヌエヌリンク',
				clickAction__c = 'DL(NNLink)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1),
				filePath__c = '契約者一覧|エヌエヌリンク.txt'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|添付ファイル',
				UpsertKey__c = '契約者一覧|添付ファイル',
				clickAction__c = 'DL(Attachment)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1),
				filePath__c = 'attachment'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '契約者一覧|attachment',
				UpsertKey__c = '契約者一覧|attachment',
				clickAction__c = 'DL(Attachment)',
				ParentContent__c = sectionsByKey.get('契約者一覧|セクション1').Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1),
				filePath__c = 'attachment'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '商品別|コンポーネント',
				UpsertKey__c = '商品別|コンポーネント',
				clickAction__c = null,
				LinkURL__c = 'IRIS_Lib_Tools',
				ParentContent__c = sectionsByKey.get('商品別|セクション1').Id
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '商品別|component',
				UpsertKey__c = '商品別|component',
				clickAction__c = 'URL',
				LinkURL__c = 'IRIS_Lib_Tools',
				ParentContent__c = sectionsByKey.get('商品別|セクション1').Id,
				ExcludeFromSearch__c = true
			);
			componentsByKey.put(component.name, component);
			/*'継続教育制度|コンポーネント' => new I_ContentMaster__c(
				name = '継続教育制度|コンポーネント',
				UpsertKey__c = '継続教育制度|コンポーネント',
				clickAction__c = 'URL',
				LinkURL__c = 'iris',
				ParentContent__c = sectionsByKey.get('継続教育制度|セクション1').Id
			),*/
			component = new I_ContentMaster__c(
				name = '定期保険|コンポーネント',
				UpsertKey__c = '定期保険|コンポーネント',
				clickAction__c = 'URL',
				LinkURL__c = 'IRIS_Lib_Tools',
				ParentContent__c = sectionsByKey.get(I_Const.IRIS_CONTENTS_NAME_SALESTOOL).Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(-1)
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '定期保険|component',
				UpsertKey__c = '定期保険|component',
				clickAction__c = 'URL',
				LinkURL__c = 'IRIS_Lib_Tools',
				ParentContent__c = sectionsByKey.get(I_Const.IRIS_CONTENTS_NAME_SALESTOOL).Id,
				DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-1),
				ValidTo__c = E_Util.SYSTEM_TODAY().addDays(1)
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '特集|コンポーネント1',
				UpsertKey__c = '特集|コンポーネント1',
				clickAction__c = 'URL',
				LinkURL__c = 'IRIS_FeatureList',
				ExcludeFromSearch__c = true,
				ParentContent__c = sectionsByKey.get('特集|セクション1').Id
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = '今月のコンポーネント',
				UpsertKey__c = '今月のコンポーネント',
				clickAction__c = 'URL',
				LinkURL__c = 'IRIS_Feature',
				ParentContent__c = sectionsByKey.get('今月の|セクション1').Id
			);
			componentsByKey.put(component.name, component);
		}
		insert componentsByKey.values();

		Map<String, ContentVersion> versionFilesByKey = new Map<String, ContentVersion>();
		{
			ContentVersion versionFile;
			versionFile = new  ContentVersion(
				title = '契約者一覧|コンテンツバージョン',
				PathOnClient = '契約者一覧|コンテンツバージョン.txt',
				VersionData = Blob.valueOf('test'),
				Origin = 'H'
			);
			versionFilesByKey.put(versionFile.title, versionFile);

			versionFile = new  ContentVersion(
				title = '契約者一覧|contents versiion',
				PathOnClient = '契約者一覧|contents versiion.txt',
				VersionData = Blob.valueOf('test'),
				Origin = 'H'
			);
			versionFilesByKey.put(versionFile.title, versionFile);
		}
		insert versionFilesByKey.values();

		Map<String, FeedItem> feedsByKey = new Map<String, FeedItem>();
		{
			FeedItem feed;
			feed = new FeedItem(
				Body = '契約者一覧|フィード',
				Type = 'ContentPost',
				ParentId = componentsByKey.get('契約者一覧|chatter').Id,
				RelatedRecordId = versionFilesByKey.get('契約者一覧|コンテンツバージョン').Id
			);
			feedsByKey.put(feed.body, feed);

			feed = new FeedItem(
				Body = '契約者一覧|feed',
				Type = 'ContentPost',
				ParentId = componentsByKey.get('契約者一覧|チャッター').Id,
				RelatedRecordId = versionFilesByKey.get('契約者一覧|contents versiion').Id
			);
			feedsByKey.put(feed.body, feed);
		}
		insert feedsByKey.values();


		Map<String, Attachment> attachmentsByKey = new Map<String, Attachment>();
		{
			Attachment attachmentFile;
			attachmentFile = new Attachment(
				Name = csmFilesByKey.get('契約者一覧|CMSファイル').UpsertKey__c,
				ParentId = csmFilesByKey.get('契約者一覧|CMSファイル').Id,
				Body = Blob.valueOf('nnlink.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);

			attachmentFile = new Attachment(
				Name = componentsByKey.get('契約者一覧|添付ファイル').filePath__c,
				ParentId = componentsByKey.get('契約者一覧|添付ファイル').Id,
				Body = Blob.valueOf('attachment.txt'),
				IsPrivate = false
			);
			attachmentsByKey.put(attachmentFile.Name, attachmentFile);
		}
		insert attachmentsByKey.values();
	}

}