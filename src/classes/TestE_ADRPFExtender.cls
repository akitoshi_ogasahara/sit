/**
* 
*/
@isTest
private class TestE_ADRPFExtender {
    
    // test data
    private static User user;
    private static E_IDCPF__c idcpf;
    private static E_CADPF__c cadpf;
    private static Account account;
    private static Contact contact;
    private static List<E_MessageMaster__c> msgList;
    
    /* 
* global override void init() 
*/

    static testMethod void testInit() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            Contact testContact = E_ContactDao.getRecById(contact.Id);
            
            system.assertEquals(controller.record.E_CADPF__c, testContact.E_CADPF__c);
            system.assertEquals(controller.record.CLTPCODE__c, testContact.E_CADPF__r.Name);
            system.assertEquals(controller.record.ZNKJADDR__c, testContact.E_CADPF__r.ZADRSKJ__c);
            system.assertEquals(controller.record.ZADRSKN__c, testContact.E_CADPF__r.ZADRSKN__c);
            system.assertEquals(controller.record.ZNKJADDR01__c, testContact.E_CLTPF_ZNKJADDR01__c);
            system.assertEquals(controller.record.ZNKJADDR02__c, testContact.E_CLTPF_ZNKJADDR02__c);
            system.assertEquals(controller.record.ZNCLTADR01__c, testContact.E_CLTPF_ZNCLTADR01__c);
            system.assertEquals(controller.record.ZNCLTADR02__c, testContact.E_CLTPF_ZNCLTADR02__c);
            
            String[] strArr;
            strArr = testContact.E_CLTPF_CLTPHONE01__c.split('-', 5);
            system.assertEquals(controller.record.CLTPHONE011__c, strArr[0]);
            system.assertEquals(controller.record.CLTPHONE012__c, strArr[1]);
            system.assertEquals(controller.record.CLTPHONE013__c, strArr[2]);
            
            strArr = testContact.E_CLTPF_CLTPHONE02__c.split('-', 5);
            system.assertEquals(controller.record.CLTPHONE021__c, strArr[0]);
            system.assertEquals(controller.record.CLTPHONE022__c, strArr[1]);
            system.assertEquals(controller.record.CLTPHONE023__c, strArr[2]);
            
            strArr = testContact.E_CLTPF_FAXNO__c.split('-', 5);
            system.assertEquals(controller.record.FAXNO1__c, strArr[0]);
            system.assertEquals(controller.record.FAXNO2__c, strArr[1]);
            system.assertEquals(controller.record.FAXNO3__c, strArr[2]);
            
        }
        Test.stopTest();
    }
    
    /* 
* pageAction()  
*/
    static testMethod void testPageAction() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            Pagereference resultPref = extender.pageAction();
            
            system.assertEquals(resultPref, null);
        }
        Test.stopTest();
    }
    
    /* 
* pageAction() 
* エラー：不正遷移 
*/
    static testMethod void testDoCheckPageTransition() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFConfirm;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            Pagereference resultPref = extender.pageAction();
            
            String resultUrl = resultPref.getUrl().toLowerCase();
            system.assert(resultUrl.indexOf(Page.E_ErrorPage.getUrl()) != -1);
        }
        Test.stopTest();
    }
    
    /* 
* isValidate() 
*/
    static testMethod void testIsValidate() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            Boolean result = extender.isValidate();
            system.assert(result);
        }
        Test.stopTest();
    }
    
    /* 
* doReturn() 
*/
    static testMethod void testDoReturn() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            Pagereference resultPref = extender.doReturn();
            system.assert(resultPref.getUrl().indexOf(Page.E_ContactInfo.getUrl()) != -1);
            system.assert(resultPref.getUrl().indexOf(contact.Id) != -1);
        }
        Test.stopTest();
    }
    
    /* 
* doRevision() 
*/
    static testMethod void testDoRevision() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            Pagereference resultPref = extender.doRevision();
            system.assertEquals(resultPref.getUrl(), Page.E_ADRPFChange.getUrl());
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* 正常
*/
    static testMethod void testDoNextConfirm01() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            Pagereference resultPref = extender.doNextConfirm();
            system.assertEquals(resultPref.getUrl(), Page.E_ADRPFConfirm.getUrl());
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：自宅電話番号と日中連絡先が入力されていない
*/
    static testMethod void testDoNextConfirm02() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.CLTPHONE011__c = '';
            controller.record.CLTPHONE012__c = '';
            controller.record.CLTPHONE013__c = '';
            controller.record.CLTPHONE021__c = '';
            controller.record.CLTPHONE022__c = '';
            controller.record.CLTPHONE023__c = '';
            
            Pagereference resultPref = extender.doNextConfirm();
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), 'ACE|005');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：ご住所の続き 全角チェック
*/
    static testMethod void testDoNextConfirm03() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNKJADDR01__c = 'ﾊﾝｶｸ';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNKJADDR01__c.getDescribe().getLabel() + ':ACE|001');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：ご住所の続き 禁則文字チェック
*/
    static testMethod void testDoNextConfirm04() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNKJADDR01__c = 'ご住所①続き';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNKJADDR01__c.getDescribe().getLabel() + ':ACE|001');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：マンション名・様方 全角チェック
*/
    static testMethod void testDoNextConfirm05() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNKJADDR02__c = 'ﾊﾝｶｸ';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNKJADDR02__c.getDescribe().getLabel() + ':ACE|001');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：自宅電話番号 電話形式チェック
*/
    static testMethod void testDoNextConfirm06() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.CLTPHONE011__c = '03';
            controller.record.CLTPHONE012__c = '';
            controller.record.CLTPHONE013__c = '0001';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.CLTPHONE01__c.getDescribe().getLabel() + ':ACE|004');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：日中連絡先 電話形式チェック
*/
    static testMethod void testDoNextConfirm07() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.CLTPHONE021__c = '03';
            controller.record.CLTPHONE022__c = '';
            controller.record.CLTPHONE023__c = '0001';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.CLTPHONE02__c.getDescribe().getLabel() + ':ACE|004');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：FAX番号 電話形式チェック
*/
    static testMethod void testDoNextConfirm08() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.FAXNO1__c = '03';
            controller.record.FAXNO2__c = '';
            controller.record.FAXNO3__c = '0001';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.FAXNO__c.getDescribe().getLabel() + ':ACE|004');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：ご住所の続き 桁数チェック
*/
    static testMethod void testDoNextConfirm09() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNKJADDR01__c = '１２３４５６７８９０１２３４５６７８９０１２３４５６７８９０１';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNKJADDR01__c.getDescribe().getLabel() + ':ACE|007');
        }
        Test.stopTest();
    }
    
    /* 
* doNextConfirm() 
* エラー：マンション名・様方 桁数チェック
*/
    static testMethod void testDoNextConfirm10() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNCLTADR02__c = 'ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏ';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNCLTADR02__c.getDescribe().getLabel() + ':ACE|007');
        }
        Test.stopTest();
    }
    /* 
* doNextConfirm() 
* エラー半角カナ
*/
    static testMethod void testDoNextConfirm11() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNCLTADR02__c = 'ぜんかく';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNCLTADR02__c.getDescribe().getLabel() + ':ACE|002');
        }
        Test.stopTest();
    }
    /* 
* doNextConfirm() 
* エラー半角カナ禁則文字
*/
    static testMethod void testDoNextConfirm12() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.ZNCLTADR02__c = '①';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.ZNCLTADR02__c.getDescribe().getLabel() + ':ACE|002');
        }
        Test.stopTest();
    }
    /* 
* doNextConfirm() 
* エラー数値チェック電話
*/
    static testMethod void testDoNextConfirm13() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            controller.record.CLTPHONE011__c = 'あ';
            controller.record.CLTPHONE012__c = '';
            controller.record.CLTPHONE013__c = '0001';
            
            Pagereference resultPref = extender.doNextConfirm();
            
            system.assertEquals(resultPref, null);
            //system.assertEquals(ApexPages.getMessages().get(0).getDetail(), E_ADRPF__c.CLTPHONE01__c.getDescribe().getLabel() + ':ACE|003');
        }
        Test.stopTest();
    } 
    /* 
* doSend() 
* 正常
*/
    static testMethod void testDoSend01() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            String kj01 = '変更ご住所の続き';
            String kj02 = '変更マンション名';
            String kn01 = 'ﾍﾝｺｳｺﾞｼﾞｭｳｼｮﾉﾂﾂﾞｷ';
            String kn02 = 'ﾍﾝｺｳﾏﾝｼｮﾝﾒｲ';
            
            controller.record.ZNKJADDR01__c = kj01;
            controller.record.ZNKJADDR02__c = kj02;
            controller.record.ZNCLTADR01__c = kn01;
            controller.record.ZNCLTADR02__c = kn02;
            extender.doNextConfirm();
            
            extender.inputPassWord = 'terrask1';
            extender.isTempPassWord = false; //20150325追加
            
            Pagereference resultPref = extender.doSend();
            system.debug('resultPref' + resultPref);
            E_ADRPF__c resultRecord = [Select Id, Name, CLNTNUM__c, ContactName__c, ContactNameKana__c,
                                       Type__c, EmailCharge__c, ZNKJADDR01__c, ZNKJADDR02__c, ZNCLTADR01__c, ZNCLTADR02__c
                                       From E_ADRPF__c Where Id = :controller.record.Id Limit 1];
            PageReference expectedPref = Page.E_AcceptanceCompletion;
            expectedPref.getParameters().put('type', 'ACC|002');
            expectedPref.getParameters().put('conid', contact.Id);
            
            system.assertEquals(resultPref.getUrl(), expectedPref.getUrl());
            system.assertEquals(resultRecord.CLNTNUM__c, contact.E_CLTPF_CLNTNUM__c);
            system.assertEquals(resultRecord.ContactName__c, contact.LastName + ' ' + contact.FirstName);
            system.assertEquals(resultRecord.ContactNameKana__c, contact.E_CLTPF_ZCLKNAME__c);
            system.assertEquals(resultRecord.Type__c, 'AD');
            system.assertEquals(resultRecord.ZNKJADDR01__c, kj01);
            system.assertEquals(resultRecord.ZNKJADDR02__c, kj02);
            system.assertEquals(resultRecord.ZNCLTADR01__c, kn01);
            system.assertEquals(resultRecord.ZNCLTADR02__c, kn02);
            system.assertNotEquals(null,resultPref);
        }
        Test.stopTest();
    }
    
    /* 
* doSend() 
* エラー
*/
    static testMethod void testDoSend02() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            
            String kj01 = '変更ご住所の続き';
            String kj02 = '変更マンション名';
            String kn01 = 'ﾍﾝｺｳｺﾞｼﾞｭｳｼｮﾉﾂﾂﾞｷ';
            String kn02 = 'ﾍﾝｺｳﾏﾝｼｮﾝﾒｲ';
            
            extender.doNextConfirm();
            
            extender.inputPassWord = 'terrasky55555';
            Pagereference resultPref = extender.doSend();
            
            List<E_ADRPF__c> resultList = [Select Id From E_ADRPF__c Where Id = :controller.record.Id];
            
            system.assertEquals(resultPref, null);
            system.assertEquals(resultList.size(), 0);
        }
        Test.stopTest();
    }
    
    /* 
* doSend() 
* エラー：Insertエラー
*/
    static testMethod void testDoSend03() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            extender.init();
            extender.doNextConfirm();
            
            extender.inputPassWord = 'terrask1';
            controller.record.FAXNO3__c = '00000000000000001';
            Pagereference resultPref = extender.doSend();
            
            system.assertEquals(resultPref, null);
        }
        Test.stopTest();
    }
    
    /* 
* doSend() 
* 正常：仮パスワード
*/
    static testMethod void testDoSend04() {
        // createData
        createDataCommon();
        
        PageReference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', contact.Id);
        Test.setCurrentPage(pref);
        
        E_ADRPF__c adrpf = new E_ADRPF__c();
        
        Test.startTest();
        system.runAs(user){
            E_ADRPFConrtoller controller = new E_ADRPFConrtoller(new ApexPages.StandardController(adrpf));
            E_ADRPFExtender extender = new E_ADRPFExtender(controller);
            
            idcpf.UseTempProcedurePW__c = true;
            idcpf.ZPASSWD02__c = E_EncryptUtil.encryptInitPass('1234567801', E_Util.getSysDate(null));
            update idcpf;
            
            extender.init();
            
            String kj01 = '変更ご住所の続き';
            String kj02 = '変更マンション名';
            String kn01 = 'ﾍﾝｺｳｺﾞｼﾞｭｳｼｮﾉﾂﾂﾞｷ';
            String kn02 = 'ﾍﾝｺｳﾏﾝｼｮﾝﾒｲ';
            
            controller.record.ZNKJADDR01__c = kj01;
            controller.record.ZNKJADDR02__c = kj02;
            controller.record.ZNCLTADR01__c = kn01;
            controller.record.ZNCLTADR02__c = kn02;
            extender.doNextConfirm();
            extender.isTempPassWord = true; //20150326追加
            
            extender.inputPassWord = '1234567801';
            extender.newPassWord = 'terrask2';
            extender.verPassWord = 'terrask2';
            
            Pagereference resultPref = extender.doSend();
            
            E_IDCPF__c result = [Select Id, RegularProcedurePW__c, ZPASSWD02DATE__c From E_IDCPF__c Where Id = :idcpf.Id Limit 1];
            system.assert(String.isNotBlank(result.ZPASSWD02DATE__c));
            system.assertEquals('8d21ce3cb014db1acc204700206eb76077429960b17c184b5d93bc85ebe08355b135ea86f3654f0bfe1a00e8ae872f1190b716391be2f4c7ace8fbf2651e3782', E_EncryptUtil.encrypt('terrask2'));
        }
        Test.stopTest();
    }
    
    /* test data */
    static void createDataCommon(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            // User
            Profile profile = [Select Id, Name From Profile Where Name = 'システム管理者' Limit 1];
            user = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
            );
            insert user;
            
            // ID管理
            idcpf = new E_IDCPF__c(
                TRCDE01__c = 'TDB0',
                User__c = user.Id,
                ZSTATUS02__c = '1',
                ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b', // terrask1
                RegularProcedurePW__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b', // terrask1
                ZPASSWD02DATE__c = '20150101'
            );
            insert idcpf;
            
            // 住所管理
            cadpf = new E_CADPF__c(
                Name = '100-1000',
                ADDRCD__c = '00000000001',
                E_CADPFCode__c = 'K000001',
                ZPOSTCD__c = '100',
                ZPOSTMARK__c = '1000',
                ZADRSKJ01__c = '試験県',
                ZADRSKJ02__c = '試験市区郡',
                ZADRSKJ03__c = '試験町名',
                ZADRSKJ04__c = '試験番地',
                ZADRSKN01__c = 'ｼｹﾝｹﾝ',
                ZADRSKN02__c = 'ｼｹﾝｼｸｸﾞﾝ',
                ZADRSKN03__c = 'ｼｹﾝﾁｮｳﾒｲ',
                ZADRSKN04__c = 'ｼｹﾝﾊﾞﾝﾁ'            
            );
            insert cadpf;
            
            // Account
            account = new Account(Name = 'testAccount');
            insert account;
            
            // Contact
            contact = new Contact(
                LastName = 'lastName', 
                FirstName = 'firstName', 
                AccountId = account.Id,
                E_CLTPF_ZCLKNAME__c = 'ｶﾅﾒｲ',
                E_CADPF__c = cadpf.Id,
                E_CLTPF_CLNTNUM__c = '12345678',
                E_CLTPF_CLTPCODE__c = '100-1000',
                E_CLTPF_ZNKJADDR01__c = 'ご住所の続き',
                E_CLTPF_ZNKJADDR02__c = 'マンション名',
                E_CLTPF_ZNCLTADR01__c = 'ｺﾞｼﾞｭｳｼｮﾉﾂﾂﾞｷ',
                E_CLTPF_ZNCLTADR02__c = 'ﾏﾝｼｮﾝﾒｲ',
                E_CLTPF_CLTPHONE01__c = '01-1234-0001',
                E_CLTPF_CLTPHONE02__c = '02-1234-0001',
                E_CLTPF_FAXNO__c = '03-1234-0001'
            );
            insert contact;
            
            // E_MessageMaster__c
            RecordType rtype = [Select Id From RecordType Where DeveloperName = 'MSG_DISCLAIMER' Limit 1];
            msgList = new List<E_MessageMaster__c>();
            for(Integer i = 1; i < 8; i++){
                // メッセージ
                msgList.add(new E_MessageMaster__c(
                    Name = 'Err' + String.valueOf(i),
                    RecordTypeId = rtype.Id,
                    Type__c = 'メッセージ',
                    Key__c = 'ACE|00' + String.valueOf(i),
                    Value__c = 'ACE|00' + String.valueOf(i)
                ));
            }
            insert msgList;
            E_Email__c eMail = new E_Email__c(NAME = 'E_101',Email__c = 'xxxx@xx.xx');
            insert eMail;
        }
        insertE_bizDataSyncLog();
    }
    private static void insertE_bizDataSyncLog(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
            insert log;
        }    
    }
    
}