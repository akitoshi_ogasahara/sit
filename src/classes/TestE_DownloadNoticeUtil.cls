@isTest
private class TestE_DownloadNoticeUtil {

    /**
     * Date getPeriodStartDate()
     */
    private static testMethod void getPeriodStartDate_test01() {
        Test.startTest();
        Date result = E_DownloadNoticeUtil.getPeriodStartDate (E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
        Test.stopTest();
        system.assertEquals(Date.today().addMonths(-1),result);
    }
    /**
     * Date getPeriodStartDate()
     */
    private static testMethod void getPeriodStartDate_test02() {
        Test.startTest();
        TestUtil_AMS.createEbizLog(true,E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
        Date result = E_DownloadNoticeUtil.getPeriodStartDate (E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
        Test.stopTest();
        system.assertEquals(Date.today(),result);
    }

    /**
     * List<SelectOption> createPeriodOptions()
     */
    private static testMethod void createPeriodOptions_test01() {
        Test.startTest();
        List<SelectOption> results = E_DownloadNoticeUtil.createPeriodOptions (E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
        Test.stopTest();
        system.assertEquals(results.size(),4);
    }
    

    /**
     * Boolean hasDownloadDatas(String objApi, String periodOptions, Account distributor, Account office, Contact agent)
     */
    private static testMethod void hasDownloadDatas_test01 (){
        Account acc = TestE_TestUtil.createAccount(true);
        Test.startTest();
        Boolean result = E_DownloadNoticeUtil.hasDownloadDatas (
                    E_DownloadNoticeConst.OBJ_API_E_AGPO
                    , '2000/01'
                    , acc
                    , null
                    , null
                    );
        Test.stopTest();
        system.assert(!result);
    }

    /**
     * Boolean hasDownloadDatas(String objApi, String periodOptions, Account distributor, Account office, Contact agent)
     */
    private static testMethod void hasDownloadDatas_test02 (){
        Account acc = TestE_TestUtil.createAccount(true);
        Test.startTest();
        Boolean result = E_DownloadNoticeUtil.hasDownloadDatas (
                    E_DownloadNoticeConst.OBJ_API_E_BILA
                    , '2000/01'
                    , null
                    , acc
                    , null
                    );
        Test.stopTest();
        system.assert(!result);
    }

    /**
     * Boolean hasDownloadDatas(String objApi, String periodOptions, Account distributor, Account office, Contact agent)
     */
    private static testMethod void hasDownloadDatas_test03 (){
        Account acc = TestE_TestUtil.createAccount(true);
        Contact agt = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'22222' );
        Test.startTest();
        Boolean result = E_DownloadNoticeUtil.hasDownloadDatas (
                    E_DownloadNoticeConst.OBJ_API_E_BILS
                    , '2000/01'
                    , null
                    , null
                    , agt
                    );
        Test.stopTest();
        system.assert(!result);
    }
    
    /**
     * E_DownloadController createDlHistory(E_DownloadController controller ,String[] selOfficeList ,String periodValue ,Boolean isCsv)
     * 事務所未選択エラー
     * 代理店格 かつ 事務所未選択
     */
    private static testMethod void createDlHistory_test01 (){
        Account acc = TestE_TestUtil.createAccount(true);
        E_DownloadController controller = new E_DownloadController();
        controller.distributor = acc;
        Test.startTest();
        controller = E_DownloadNoticeUtil.createDlHistory(
                                    controller
                                    , new String[]{}
                                    , '2000/01'
                                    , true);
        Test.stopTest();
        system.assert(controller.pageMessages.hasMessages());
    }
    
    /**
     * E_DownloadController createDlHistory(E_DownloadController controller ,String[] selOfficeList ,String periodValue ,Boolean isCsv)
     * 正常系
     * 代理店格
     */
    private static testMethod void createDlHistory_test02 (){
        Account acc = TestE_TestUtil.createAccount(true);
        E_DownloadController controller = new E_DownloadController();
        controller.distributor = acc;
        Test.startTest();
        controller = E_DownloadNoticeUtil.createDlHistory(
                                    controller
                                    , new String[]{'TEST_ID001'}
                                    , '2000/01'
                                    , true);
        Test.stopTest();
        system.assert(!controller.pageMessages.hasMessages());
        system.assert(controller.dh.id <> null);
    }
    
    /**
     * E_DownloadController createDlHistory(E_DownloadController controller ,String[] selOfficeList ,String periodValue ,Boolean isCsv)
     * 正常系
     * 代理店事務所
     */
    private static testMethod void createDlHistory_test03 (){
        Account acc = TestE_TestUtil.createAccount(true);
        E_DownloadController controller = new E_DownloadController();
        controller.office = acc;
        Test.startTest();
        controller = E_DownloadNoticeUtil.createDlHistory(
                                    controller
                                    , new String[]{}
                                    , '2000/01,2000/02,2000/03'
                                    , true);
        Test.stopTest();
        system.assert(!controller.pageMessages.hasMessages());
        system.assert(controller.dh.id <> null);
    }
    /**
     * メインアカウント取得エラー
     * 募集人 かつ メインアカウントなし
     */
    private static testMethod void createDlHistory_test04 (){
        Account acc = TestE_TestUtil.createAccount(true);
        Contact agt = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'2222212345' );
        E_DownloadController controller = new E_DownloadController();
        controller.agent = agt;
        Test.startTest();
        controller = E_DownloadNoticeUtil.createDlHistory(
                                    controller
                                    , new String[]{}
                                    , '2000/01'
                                    , true);
        Test.stopTest();
        system.assert(controller.pageMessages.hasMessages());
    }
    /**
     * 正常系
     * 募集人
     */
    private static testMethod void createDlHistory_test05 (){
        Account acc = TestE_TestUtil.createAccount(true);
        Contact agt = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'2222212345' );
        Contact mainAgt = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'111112' ,'22222' );

        E_DownloadController controller = new E_DownloadController();
        controller.agent = agt;
        Test.startTest();
        controller = E_DownloadNoticeUtil.createDlHistory(
                                    controller
                                    , new String[]{}
                                    , '2000/01'
                                    , true);
        Test.stopTest();
        system.assert(!controller.pageMessages.hasMessages());
        system.assert(controller.dh.id <> null);
    }


    private static testMethod void checkPeriodPDF(){
        E_DownloadController controller = new E_DownloadController();
        Test.startTest();
         E_DownloadNoticeUtil.checkPeriodPDF(controller,'2017/02,2017/01,2016/12');
        Test.stopTest();
        system.assertNotEquals(null,controller.pagemessages);
    }


    private static testMethod void setRelateToE_CHTPF_test01(){
        Account acc = TestE_TestUtil.createAccount(true);
        Contact con = TestE_TestUtil.createContact(true,acc.id,'テスト契約者' ,'11111' ,'22222' );
        E_Policy__c policy = createDataPolicy(true,con.Id,E_Const.POLICY_RECORDTYPE_COLI,'33333');
        E_CHTPF__c chtpf = createDataCHTPF(true,policy.Id,con.E_CL3PF_AGNTNUM__c);
        E_AGPO__c agpo = createDataAGPO(true,policy.Id,con.E_CL3PF_AGNTNUM__c);

        List<sObject> newList = new List<sObject>();
            newList.add(agpo);

        Test.startTest();
        E_DownloadNoticeUtil.setRelateToE_CHTPF(newList);
        Test.stopTest();
        system.assertEquals(agpo.E_Policy__c,policy.Id);
    }


    private static testMethod void setRelateToE_CHTPF_test02(){
        Account acc = TestE_TestUtil.createAccount(true);
        Contact con = TestE_TestUtil.createContact(true,acc.id,'テスト契約者' ,'11111' ,'22222' );
        E_Policy__c policy = createDataPolicy(true,con.Id,E_Const.POLICY_RECORDTYPE_COLI,'33333');
        E_CHTPF__c chtpf = createDataCHTPF(true,policy.Id,con.E_CL3PF_AGNTNUM__c);
        E_AGPO__c agpo = createDataAGPO(true,policy.Id,con.E_CL3PF_AGNTNUM__c);
        E_AGPO__c agpo2 = new E_AGPO__c(Id = agpo.Id,AGNT__c = '33333');
        
        List<sObject> newList = new List<sObject>();
            newList.add(agpo);

        Map<Id,Sobject> oldMap = new Map<Id,sObject>{agpo2.Id => agpo2};

        Test.startTest();
        E_DownloadNoticeUtil.setRelateToE_CHTPF(newList,oldMap);
        Test.stopTest();
        system.assertNotEquals(agpo2.E_Policy__c,policy.Id);
        system.assertEquals(agpo.E_Policy__c,policy.Id);

    }


    /*
    **保険契約ヘッダ
    */
    private static E_Policy__c createDataPolicy(Boolean isInsert, Id conId, String recTypeDevName, String cno) {
        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(recTypeDevName);
        E_Policy__c src = new E_Policy__c(
            RecordTypeId = recTypeId
            , COMM_CHDRNUM__c = cno
            , MainAgent__c = conId
        );
        if(isInsert){
            insert src;
        }
        return src;
    }

/*
**販売取扱者作成
*/
    private static E_CHTPF__c createDataCHTPF(Boolean isInsert, Id policyId,String conCode){
        E_CHTPF__c chtpf = new E_CHTPF__c(E_Policy__c = policyId
                                        , AGNTNUM__c = conCode);
        if(isInsert){
            insert chtpf;
        }
        return chtpf;
    }

    private static E_AGPO__c createDataAGPO(Boolean isInsert,Id policyId,String conCode){
        E_AGPO__c agpo = new E_AGPO__c(E_Policy__c = policyId
                                    , AGNT__c = conCode);
        if(isInsert){
            insert agpo;
        }
        return agpo;
    }

}