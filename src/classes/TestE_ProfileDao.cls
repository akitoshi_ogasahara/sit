@isTest
private class TestE_ProfileDao {

	/**
	 * Profile getRecById(Id profileId)
	 */
	@isTest static void test_getRecById() {
		Profile result = E_ProfileDao.getRecById(UserInfo.getProfileId());
		System.assertEquals(UserInfo.getProfileId(), result.Id);
	}

	/**
	 * List<ObjectPermissions> getAllModifyRecordsObjectPermissionsBySObject(String sobjNm)
	 */
	@isTest static void test_getAllModifyRecordsObjectPermissionsBySObject() {
		List<ObjectPermissions> result = E_ProfileDao.getAllModifyRecordsObjectPermissionsBySObject('Account');
		//System.assertEquals(0, result.size());
	}

	/**
	 * Map<Id, Profile> getProfileMap()
	 */
	@isTest static void test_getProfileMap() {
		Map<Id, Profile> result = E_ProfileDao.getProfileMap();
		System.assertEquals(true, result.containsKey(UserInfo.getProfileId()));
	}

	/**
	 * Boolean isMR(Id profId)
	 */
	@isTest static void test_isMR() {
		User u = TestE_TestUtil.createUser(true, 'testName001', 'ＭＲ');
		System.assertEquals(true, E_ProfileDao.isMR(u.ProfileId));
	}

	/**
	 * Boolean isMRManager(Id profId)
	 */
	@isTest static void test_isMRManager() {
		User u = TestE_TestUtil.createUser(true, 'testName001', '拠点長');
		System.assertEquals(true, E_ProfileDao.isMRManager(u.ProfileId));
	}

	/**
	 * Boolean isHQMR(Id profId)
	 */
	@isTest static void test_isHQMR() {
		User u = TestE_TestUtil.createUser(true, 'testName001', '本社営業部MR');
		System.assertEquals(true, E_ProfileDao.isHQMR(u.ProfileId));
	}

	/**
	 * Boolean isHQMRManager(Id profId)
	 */
	@isTest static void test_isHQMRManager() {
		User u = TestE_TestUtil.createUser(true, 'testName001', '本社営業部拠点長');
		System.assertEquals(true, E_ProfileDao.isHQMRManager(u.ProfileId));
	}

	/**
	 * Boolean isAreaManager(Id profId)
	 */
	@isTest static void test_isAreaManager() {
		User u = TestE_TestUtil.createUser(true, 'testName001', 'エリア部長');
		System.assertEquals(true, E_ProfileDao.isAreaManager(u.ProfileId));
	}

	/**
	 * Boolean isPlanningManagement(Id profId)
	 */
	@isTest static void test_isPlanningManagement() {
		User u = TestE_TestUtil.createUser(true, 'testName001', '営業企画管理部');
		System.assertEquals(true, E_ProfileDao.isPlanningManagement(u.ProfileId));
	}

	/**
	 * Boolean isSalesPromotion(Id profId)
	 */
	@isTest static void test_isSalesPromotion() {
		User u = TestE_TestUtil.createUser(true, 'testName001', '営業推進部');
		System.assertEquals(true, E_ProfileDao.isSalesPromotion(u.ProfileId));
	}

}