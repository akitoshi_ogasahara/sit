/*
 * CC_CaseCommentHistoryTgrHdlTest
 * Test class of CC_CaseCommentHistoryTgrHdl
 * created  : Accenture 2018/4/24
 * modified :
 */

@isTest
private class CC_CaseCommentHistoryTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {
			//Create SRTypeMaster
			CC_SRTypeMaster__c newSRTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
			Insert newSRTypeMaster;
		}
	}

	/**
	* Insert Case
	*/
	static testMethod void caseSRCommHisTriHandlerTestInsert01() {
		System.runAs ( testUser ) {

			Test.startTest();

			CC_SRTypeMaster__c srtypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE Name = '10001'];
			Case newCase = CC_TestDataFactory.getCaseSkelWithNoParameter();
			newCase.CC_SRTypeId__c = srtypeMaster.Id;
			newCase.CMN_Source__c = '郵便';
			insert newCase;

			Test.stopTest();

		}
	}

	/**
	* Update Case
	*/
	static testMethod void caseSRCommHisTriHandlerTestUpdate01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Case
			CC_SRTypeMaster__c srtypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE Name = '10001'];
			Case newCase = CC_TestDataFactory.getCaseSkelWithNoParameter();
			newCase.CC_SRTypeId__c = srtypeMaster.Id;
			newCase.CMN_Source__c = '郵便';
			insert newCase;

			//Create SRCommentHistory
			List<CC_SRCommentHistory__c> SRCommentHistoryList = CC_TestDataFactory.getSRCommentHistorySkelList(newCase.Id);
			insert SRCommentHistoryList;

			//Update Case
			newCase.CMN_Comment1__c = 'UpdatetestComment1';
			newCase.CMN_Comment2__c = 'UpdatetestComment2';

			Test.startTest();
			Update newCase;
			Test.stopTest();

		}
	}

}