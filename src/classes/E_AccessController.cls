/**
 * アクセス制御クラス
 * CreatedDate 2014/12/11
 * @Author Terrasky
 */
public with sharing class E_AccessController {

	//ユーザオブジェクト
	public User user;
	//ID管理オブジェクト
	public E_IDCPF__c idcpf;
	//プロファイルオブジェクト
	public Profile profile;

	//アクセス制御インスタンス
	private static E_AccessController instance = null;

	/**
	 * コンストラクタ
	 */
	private E_AccessController(){
		//user = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		user = E_UserDao.getUserWithPermissionsByUserId(UserInfo.getUserId());
		idcpf = E_IDCPFDao.getRecsById(UserInfo.getuserId());
		profile = E_ProfileDao.getRecById(UserInfo.getProfileId());
	}

	/**
	 * インスタンス生成
	 */
	public static E_AccessController getInstance() {
		if (instance == null) {
			instance = new E_AccessController();
		}
		return instance;
	}


	/**
	 * 個人保険照会フラグ
	 */
	public boolean canColi(){
		return idcpf != null ? isInquiry(idcpf.FLAG01__c) : false;
	}

	/**
	 * SPVA照会フラグ
	 */
	public boolean canSpva(){
		return idcpf != null ? isInquiry(idcpf.FLAG02__c) : false;
	}

	/**
	 * 顧客検索フラグ
	 */
	public boolean canCustomerSearch(){
		return idcpf != null ? isInquiry(idcpf.FLAG04__c) : false;
	}

	/**
	 * 手続の履歴フラグ
	 */
	public boolean canProcessHistory(){
		return idcpf != null ? isInquiry(idcpf.FLAG06__c) : false;
	}

	/**
	 * 手数料明細書照会フラグ
	 */
	public boolean canFeeDetail(){
		return idcpf != null ? isInquiry(idcpf.ZDSPFLAG01__c) : false;
	}

	/**
	 * 投信照会フラグ
	 */
	public boolean canInvestment(){
		return idcpf != null ? isInquiry(idcpf.FLAG03__c) : false;
	}

	/**
	 * 共通：オブジェクトアクセス判定
	 */
	public boolean isAccessible(List<String> sObjectList){
		Map<String, Schema.Sobjecttype> smap = Schema.getGlobalDescribe();
		Boolean flag = true;
		for (String key : sObjectList) {
			Schema.Sobjecttype sobj = smap.get(key);
			if (!sobj.getDescribe().isAccessible()) {
				flag = false;
				break;
			}
		}
		return flag;
	}

	/**
	 * 社員フラグ
	 */
	public boolean isEmployee(){
		if (user != null && user.UserType.equals(E_Const.USERTYPE_STANDARD)) {
			return true;
		}
		return false;
	}

	/**
	 * 代理店（ユーザタイプ）フラグ
	 */
	public boolean isAgent(){
		if (user != null && user.UserType.equals(E_Const.USERTYPE_POWERPARTNER)) {
			return true;
		}
		return false;
	}

	/**
	 * 代理店格権限有無フラグ
	 */
	public boolean hasAuthDistributor(){
		if(isEmployee() || (isAgent() && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AH)){
			return true;
		}
		return false;
	}

	/**
	 * 代理店事務所権限有無フラグ
	 */
	public boolean hasAuthOffice(){
		if(isEmployee() || (isAgent() && (idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AH
				 || idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AY))){
			return true;
		}
		return false;
	}

	/**
	 * 募集人権限有無フラグ
	 */
	public boolean hasAuthAgent(){
		if(isEmployee() || (isAgent() && (idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AH
				 || idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AY || idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AT))){
			return true;
		}
		return false;
	}

	/**
	 * 契約者フラグ
	 */
	public boolean isCustomer(){
		if (user != null && user.UserType.equals(E_Const.USERTYPE_CSPLITEPORTAL)) {
			return true;
		}
		return false;
	}
	/**
	 * コールセンターフラグ
	 */

	public boolean isCallCenter(){
		//カスタム表示ラベルから '|'区切りのロール名を取得し、リストにつめる。
		List<String> roles = System.Label.E_CCBTN_ROLE.split('\\|',0);
		//ログインユーザのロール名を取得
		String logUserDevName = user.UserRole.DeveloperName;
		//ロールがコールセンターであるときTrueを返す。
		if(E_Util.isNotNull(logUserDevName)){
			for(String str : roles){
				if(str.equals(logUserDevName)){
					//CC顧客用ボタンを表示する
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * システム管理者フラグ
	 */
	public boolean isSystemAdministrator(){
		if (profile != null && profile.Name.equals(E_Const.SYSTEM_ADMINISTRATOR)) {
			return true;
		}
		return false;
	}

	/**
	 * Guestユーザフラグ
	 */
	public boolean isGuest(){
		if (user != null && user.UserType.equals(E_Const.USERTYPE_GUEST)) {
			return true;
		}
		return false;
	}

	/**
	 * 住友生命ユーザか判定
	 * ログインユーザ（代理店ユーザ）の代理店格コードより判定する
	 */
	public boolean isSumiseiUser(){
		if(E_Util.isSumiseiAccountCode(user) && !E_Util.isSumiseiIRISAgentType(user)){
			return true;
		}
		return false;
	}

	/**
	 * 住友生命ユーザベースか判定
	 * ログインユーザ（代理店ユーザ）の代理店格コードと募集人種別より判定する
	 */
	//IRISfor住友対応
	public boolean isSumiseiIRISUser(){
		if(E_Util.isSumiseiAccountCode(user) && E_Util.isSumiseiIRISAgentType(user)){
			return true;
		}
		return false;
	}

	/**
	 * 住友生命IRIS向けのページを判定
	 * 住友生命募集人 または 住生TOPから画面遷移した社員のみ、住生IRIS用画面を表示する
	 */
	public boolean canViewSumiseiIRIS(){
		String isS = ApexPages.currentPage().getParameters().get('isSMTM');
		if(isSumiseiIRISUser()
			|| (isEmployee() && isS == '1') ){
			return true;
		}
		return false;
	}

	/**
	 * 契約関連オブジェクト判定
	 */
	public boolean isAccessiblePolicy() {
		List<String> sobjectList = new List<String>{'E_Policy__c','E_CRLPF__c','E_COVPF__c',
			'E_SPRPF_Weekly__c','E_SPRPF_Monthly__c','E_TransitionWK__c','E_SUPPF__c','E_CHTPF__c','E_SWDPF__c',
			'E_SVPPF__c','E_SVFPF__c','E_SPSPF__c','E_SVCPF__c','E_SFGPF__c','E_FundMaster__c','E_UPBPF__c',
			'E_CancellFormDownloadControl__c','E_CancellFormDownloadHistory__c','Contact','Account','User','E_SPSPF__c'};
		return isAccessible(sobjectList);
	}

	/**
	 * 契約関連オブジェクト（手続き履歴）判定
	 */
	public boolean isAccessiblePolicyHistory() {
		List<String> sobjectList = new List<String>{'E_PTNPF__c'};
		return isAccessible(sobjectList);
	}


	/**
	 * 投信関連オブジェクト判定
	 */
	public boolean isAccessibleInvestment() {
		List<String> sobjectList = new List<String>{'E_ITHPF__c','E_ITFPF__c','E_ITTPF__c','E_ITBPF__c','E_ITAPF__c',
			'Contact','Account','User'};
		return isAccessible(sobjectList);
	}


	/**
	 * ダウンロード関連オブジェクト判定
	 */
	public boolean isAccessibleDownload() {
		//List<String> sobjectList = new List<String>{'E_PrintSheetsMaster__c','E_PrintSheetsSetting__c','E_ASPPF__c',
		List<String> sobjectList = new List<String>{'E_DownloadHistorry__c',
			'Contact','Account','User'};
		return isAccessible(sobjectList);
	}

	/**
	 * 登録情報関連, 契約関連(100件以上)オブジェクト判定
	 */
	public boolean isAccessibleCustomer() {
		List<String> sobjectList = new List<String>{'E_CADPF__c','E_ADRPF__c', 'E_Policy__c',
			'Contact','Account','User'};
		return isAccessible(sobjectList);
	}

	/**
	 * 特定代理店用機能オブジェクト判定(JP)
	 */
	public boolean isAccessibleSpecialFeaturesJp() {
		List<String> sobjectList = new List<String>{'E_CPCPF__c','Contact','Account','User'};
		return isAccessible(sobjectList);
	}

	/**
	 * 特定代理店用機能オブジェクト判定(Bank)
	 */
	public boolean isAccessibleSpecialFeaturesBank() {
		List<String> sobjectList = new List<String>{'E_MPQPF__c','Contact','Account','User'};
		return isAccessible(sobjectList);
	}


	/**
	 * 実行可能な処理のコード判定（繰入比率変更：TD05,積立金の移転：TD06,住所変更受付：TDB0）
	 */
	public boolean isTransactionCode(String code) {
		if (idcpf != null) {
			for (Integer i=1; i<=10; i++) {
				String sCount = String.valueOf(i).leftPad(2).replace(' ','0');
				String data = (String)idcpf.get('TRCDE' + sCount + '__c');
				if (code.equals(data)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	* 共同Gateway判定
	 */
	public boolean isCommonGateway() {
		return ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP') !=null
			&& ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP').equals(System.Label.E_CMN_GW_IP_ADRS);
	}

	/**
	 * インターネット経由判定
	 */
	public boolean isViaInternet() {
		return !isCommonGateway();
	}

	/**
	 * コンプライアンス画面を経由判定
	 */
	public boolean isComplianceView() {
		//IRISユーザはコンプライアンス画面スキップ
		if(E_IRISHandler.getInstance().getIsIRIS()){
			return true;
		}

		//住生ユーザはコンプライアンス画面をスキップ
		if(isSumiseiUser()||isSumiseiIRISUser()){
			return true;
		}

		//NNLink判定
		if (idcpf != null && idcpf.ZSTATUS01__c != null) {
			if (idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)) {
				String value = E_CookieHandler.getCookieCompliance();
				if (value != null && value.equals(E_Util.getSessionId())) {
					return true;
				} else {
					return false;
				}
			}
		}

		return false;
	}

	/**
	 * 初回ログイン、PW有効期限判定
	 */
	public boolean isInitAndReissuePassword() {
		boolean isPw = false;
		//初回ログイン判定
		if (user.E_UseTempBasicPW__c) {
			isPw = true;
		} else {
			isPw = Site.isPasswordExpired();
		}
		return isPw;
	}

	/**
	 * ログイン関連チェック
	 * １．初回Pw＆有効期限Pwチェック
	 * ２．コンプライアンス経由チェック
	 */
	public PageReference isLoginControl(E_Const.ID_KIND kind) {
		PageReference pageRef = null;
		E_IrisHandler iris = E_IRISHandler.getInstance();
		//社員とゲストユーザチェック
		if (isEmployee() || isGuest()) {
			return pageRef;
		}
		/* たもつ君対応 START 2017.12 たもつ君 */
		//たもつ君経由チェック (ログイン画面以外)
		if(isViaMobileSDK() && kind != E_Const.ID_KIND.LOGIN){
			//初回ログイン判定
			if (user.E_UseTempBasicPW__c){
				//エラーページへ
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_NOTALLOW);
				return pageRef;
			}
			return pageRef;
		}
		/* たもつ君対応 END */

		//ID管理.パスワードステータスが６の場合、共同GWチェック
		//if (idcpf != null && idcpf.ZSTATUS01__c != null) {
			//if (idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_CGW)) {
		if (idcpf != null && idcpf.ZSTATUS01__c != null && idcpf.ZDSPFLAG02__c != null) {
			if (idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE) && idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_BANK)) {
				if (isCommonGateway()) {
					return pageRef;
				}
			}
		}

		//ISS経由チェック
		if (isCommonGateway() && (E_Util.getIsIssParam() || E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId()))) {
			return pageRef;
		}

		//IRIS住友チェック
		if(isSumiseiIRISUser()){
            return pageRef;
		}

		//インターネット経由の場合、初回Pw＆有効期限Pwチェック
		if (isViaInternet() && isInitAndReissuePassword()) {
			//IRIS利用時はIRISのPW変更ページを返す。
			return iris.getIsIRIS()?Page.IRIS_ChangePW:Page.E_StandardPasswordChange;
		}
		//コンプライアンス経由チェック
		if (isComplianceView()) {
			pageRef = null;
		} else {
			if (kind == E_Const.ID_KIND.COMPLIANCE) {
				pageRef = null;
			} else {
				pageRef = Page.E_Compliance;
			}
		}
		return pageRef;
	}

	/**
	 * データ連携中チェック
	 */
	public PageReference isServiceHourMenu(E_Const.ID_KIND kind) {
		PageReference pageRef = null;

		if (System.Label.E_SERVICE_HOUR_MENU == null || kind == null) {
			return pageRef;
		}
		List<String> menus = System.Label.E_SERVICE_HOUR_MENU.split(',', 0);
		for (String value : menus) {
			if (value.trim().equals(kind.name())) {
				if (!E_Util.isSyncEndData()){
					return Page.E_ErrorPage;
				}
			}
		}
		return pageRef;
	}

	/**
	 * ログインユーザの代理店格コード
	 */
	public String getAccountCode() {
		if(user.ContactId != null){
//			Contact con = E_ContactDao.getRecById(user.ContactId);
//			return con.E_AccParentCord__c;
			return user.Contact.E_AccParentCord__c;
		}
		return null;
	}

	/**
	 * ログインユーザの代理店事務所コード
	 */
	public String getOfficeCode() {
		if(user.ContactId != null){
			return user.Contact.Account.E_CL2PF_ZAGCYNUM__c;
		}
		return null;
	}

	/**
	 * ログインユーザの代申フラグ
	 */
	public Boolean getAccZSHRMAIN() {
		if(user.ContactId != null){
			return user.Contact.E_AccZSHRMAIN__c;
		}
		return null;
	}

	/**
	 * 照会可能フラグ判定
	 */
	private boolean isInquiry(String isItem) {
		if (isItem != null) {
			if (isItem.equals(E_Const.IS_INQUIRY_1)) {
				return true;
			} else if (isItem.equals(E_Const.IS_INQUIRY_0) || isItem.equals(E_Const.IS_INQUIRY_2)) {
				return false;
			}
		}
		return false;
	}

	/**
	 * ダウンロード 手数料のご案内 機能判定
	 */
	public boolean canDisplayFeeGuidance () {
		if(hasAuthDistributor() && canFeeDetail()) {
			return true;
		}
		return false;
	}

	/**
	 * ダウンロード 手数料明細(生保/変額商品/ボーナス) 機能判定
	 */
	public boolean canDisplayFeeData () {
		if(hasAuthOffice() && canFeeDetail()) {
			return true;
		}
		return false;
	}

	/**
	 * ダウンロード 保有契約(個人保険) 機能判定
	 */
	public boolean canDisplayHoldPolicyColi () {
		if(hasAuthAgent() && canColi()) {
			return true;
		}
		return false;
	}

	/**
	 * ダウンロード 保有契約(SPVA) 機能判定
	 */
	public boolean canDisplayHoldPolicySpva () {
		if(hasAuthAgent() && canSpva()) {
			return true;
		}
		return false;
	}

	/**
	 * ダウンロード 手続きの履歴 機能判定
	 */
	public boolean canDisplayPolicyHisotry () {
		if(hasAuthAgent() && canProcessHistory()){
			return true;
		}
		return false;
	}

	/**
	 * ダウンロード関連ID管理フラグ判定
	 */
	public boolean canDownloads() {
		return (canDisplayFeeGuidance() || canDisplayFeeData() || canDisplayHoldPolicyColi() || canDisplayHoldPolicySpva() || canDisplayPolicyHisotry());
	}

	/**
	 * ID管理.照会権限判定
	 */
	public boolean isAccessKind(String kind) {
		if (idcpf != null) {
			if (idcpf.ZINQUIRR__c != null) {
				return idcpf.ZINQUIRR__c.startsWith(kind) ? true : false;
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * ID管理.照会権限取得（3文字以降）
	 */
	public String getAccessKindFromThree() {
		if (idcpf != null) {
			if (idcpf.ZINQUIRR__c != null && idcpf.ZINQUIRR__c.length() > 2) {
				return idcpf.ZINQUIRR__c.substring(2);
			}
		}
		return null;
	}

	/**
	 * Referer設定メソッド
	 */
	public String getReferer(E_Const.ID_KIND kind) {
		String referer =  ApexPages.currentPage().getHeaders().get('Referer');
		String subreferer = null;
		if (kind == E_Const.ID_KIND.INFO) {
			return E_CookieHandler.getCookieReferer();
		}
		if(referer != null){
			if (isEmployee()) {
				if (referer.indexOf('/apex') > 0) {
					subreferer =  referer.substring(referer.indexOf('/apex'));
				}
			} else if (isAgent()) {
				if (referer.indexOf(E_Const.NNLINK_AGE) > 0) {
					subreferer =  referer.substring(referer.indexOf(E_Const.NNLINK_AGE) -1);
				} else if (referer.indexOf(E_Const.NNLINK_CGWAGE) > 0) {
					subreferer =  referer.substring(referer.indexOf(E_Const.NNLINK_CGWAGE) -1);
				}
			} else if (isCustomer()) {
				if (referer.indexOf(E_Const.NNLINK_CLI) > 0) {
					subreferer =  referer.substring(referer.indexOf(E_Const.NNLINK_CLI) -1);
				}
			}
			if (subreferer == null) {
				subreferer = referer;
			}
		} else {
			subreferer = E_CookieHandler.getCookieReferer();
		}
System.debug('E_AccessController getReferer subreferer=' + subreferer);
		return subreferer;
	}

	/**
	 * Referer設定メソッド（汎用）
	 * ※今後、E_AbstractViewExtender、setRefererPolicy/setRefererPolicy/setRefererInve/setRefererHistoryをsetReferer移行
	 * @param List<PageReference> pages Refererを設定する前ページ条件
	 */
	public void setReferer(E_Const.ID_KIND kind, List<PageReference> pages, String cookieKey) {
		String referer =  ApexPages.currentPage().getHeaders().get('Referer');
		if(referer != null){
			String lowerReferer = referer.toLowerCase();
			boolean isIncluded = false;
			for (PageReference pageRef : pages) {
				String url = pageRef.getUrl().replace('/apex/', '').toLowerCase();
				System.debug('setReferer:lowerReferer=======================' + lowerReferer);
				System.debug('setReferer:url=======================' + url);
				System.debug('setReferer:url=======================' + E_CookieHandler.getCookieGeneralReferer());
				if (lowerReferer.indexOf(url) > 0) {
					String refererStr = E_CookieHandler.getCookieGeneralReferer();
					Map<String, String> refererMap = new Map<String, String>();
					if (!String.isEmpty(refererStr)) {
						refererMap = (Map<String, String>)Json.deserialize(refererStr, Map<String, String>.class);
					}
					refererMap.put(cookieKey, getReferer(kind));
					E_CookieHandler.setCookieGeneralReferer(Json.serialize(refererMap));
					break;
				}
			}
		}
	}

	/**
	 * 権限セット名Setの取得
	 */
	private	Set<String> psaNames;
	public Set<String> getAssignedPermissionSetNames(){
		if(psaNames==null){
			psaNames = new Set<String>();
			for(PermissionSetAssignment psa:user.PermissionSetAssignments){
				psaNames.add(psa.PermissionSet.Name);
			}
		}
		return psaNames;
	}

	/**
	 * 特定オブジェクトに対してすべての参照権限をもつProfileId群
	 */
	public Set<Id> getModifyAllProfileIdsBySObject(String sobjNm){
		Set<Id> ret = new Set<Id>();
		for(ObjectPermissions os:E_ProfileDao.getAllModifyRecordsObjectPermissionsBySObject(sobjNm)){
			ret.add(os.parent.profileId);
		}
		return ret;
	}

	/**
	 * 代理店規定管理　承認権限を持つか？
	 */
	public Boolean hasCRApprovalPermission(){
		//権限セットの承認権限　CR_PermissionSet_Approvalを持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(CR_Const.PERMISSIONSET_APPROVE);
	}

	/**
	 * 代理店規定管理　外部委託参照権限を持つか？
	 */
	public Boolean hasCROutsrcViewPermission(){
		//外部委託参照権限　CR_PermissionSet_OutSrcを持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(CR_Const.PERMISSIONSET_OUTSRC);
	}

	/**
	 * 代理店規定管理　全データの編集権限を持つプロファイルか？
	 */
	public Boolean isCRModifyAllProfileUser(){
		Set<Id> proIds = getModifyAllProfileIdsBySObject(Schema.SObjectType.CR_Agency__c.Name);
		return proIds.contains(profile.Id);
	}

	/**
	 * 代理店規定管理　DPA権限を持つか？
	 */
	public Boolean hasCRDpaPermission(){
		//権限セットのDPA権限　CR_PermissionSet_DPAを持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(CR_Const.PERMISSIONSET_DPA);
	}

	/**
	 * 代理店規定管理　事業報告書権限を持つか？
	 */
	public Boolean hasCRReportsPermission(){
		//権限セットの事業報告書権限　CR_PermissionSet_Reportsを持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(CR_Const.PERMISSIONSET_REPORTS);
	}

	/**
	 * 代理店規定管理　帳簿書類権限を持つか？
	 */
	public Boolean hasCRBooksPermission(){
		//権限セットの帳簿書類権限　CR_PermissionSet_Booksを持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(CR_Const.PERMISSIONSET_BOOKS);
	}

	/**
	 * 自主点検（代理店） 権限セット付与チェック
	 */
	public Boolean hasSCAGPermission(){
		//権限セット　AMS_sc_PermissionSet_AG を持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(SC_Const.PERMISSIONSET_SC_Agent);
	}

	/**
	 * 自主点検（社員） 権限セット付与チェック
	 */
	public Boolean hasSCNNPermission(){
		//権限セット　AMS_sc_PermissionSet_NN を持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(SC_Const.PERMISSIONSET_SC_NN);
	}

	/**
	 * 自主点検（参照のみ） 権限セット付与チェック
	 */
	public Boolean hasSCDirectorPermission(){
		//権限セット　AMS_sc_PermissionSet_Director を持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(SC_Const.PERMISSIONSET_SC_Director);
	}

	/**
	 * 自主点検（コンプラ） 権限セット付与チェック
	 */
	public Boolean hasSCAdminPermission(){
		//権限セット　AMS_sc_PermissionSet_Admin を持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(SC_Const.PERMISSIONSET_SC_Admin);
	}

	/**
	 *  挙績情報（社内挙績情報） 権限セット付与チェック
	 */
	public Boolean hasSREmployeePermission(){
		//権限セット　I_PermissionSet_SREmployee を持っている場合にTrueとする
		return getAssignedPermissionSetNames().contains(E_Const.PERM_SREmployee);
	}

	/**
	 * 権限セット付与チェック
	 * @param String 検証権限セット名
	 * @return Boolean 引数の権限セットを付与さらえている場合にTrueを返す
	 */
	public Boolean hasPermission(String target){
		return getAssignedPermissionSetNames().contains(target);
	}
	public Boolean hasPermission(Set<String> targets){
		Set<String> psnames = getAssignedPermissionSetNames();		//現在付与されている権限セット名のSet
		for(String s:targets){
			if(psnames.contains(s)){								//引数の検証対象の権限セット名のどれかを含んでいるときにTrue
				return true;
			}
		}
		return false;
	}

	//EIDCの照会者コードがBR**の時にTrueとする
	public Boolean ZINQUIRR_is_BRAll(){
		return idcpf!=null && E_Const.USER_ACCESS_KIND_BRALL.equalsIgnoreCase(idcpf.ZINQUIRR__c);
	}

	/**
	 * AH権限ユーザか？
	 */
	public Boolean isAuthAH(){
		if(isAgent() && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AH){
			return true;
		}
		return false;
	}

	/**
	 * AY権限ユーザか？
	 */
	public Boolean isAuthAY(){
		if(isAgent() && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AY){
			return true;
		}
		return false;
	}

	/**
	 * AT権限ユーザか？
	 */
	public Boolean isAuthAT(){
		if(isAgent() && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AT){
			return true;
		}
		return false;
	}


	/**
	 * [IRIS]MRか？
	 */
	public Boolean isAuthMR(){
		return (E_ProfileDao.isMR(profile.Id));
	}

	/**
	 * [IRIS]拠点長か？
	 */
	public Boolean isAuthManager(){
		return (E_ProfileDao.isMRManager(profile.Id));
	}

	/**
	 * [AMS]エリア部長か？
	 */
	public Boolean isAreaManager(){
		return (E_ProfileDao.isAreaManager(profile.Id));
	}

	/**
	 * [IRIS]一般代理店か？
	 */
	public Boolean isNormalAgent(){
		return (isAgent()
				&& idcpf.ZSTATUS01__c == E_Const.ZSTATUS01_ENABLE
				&& idcpf.ZDSPFLAG02__c == E_Const.ZDSPFLAG02_IRIS	//TODO ID適正化
				&& String.isBlank(user.contact.E_MenuKindId__c));
	}

	/**
	 * [Atria]一般代理店/銀行代理店
	 */
	public Boolean isNormalORBankAgent(){
		return (isAgent()
				&& String.isBlank(user.contact.E_MenuKindId__c));
	}

	/**
	 * ログインユーザの代理店事務所のMRコード
	 */
	public String getOfficeMRCode() {
		if(user.ContactId != null){
			return user.Contact.Account.ZMRCODE__c;
		}
		return null;
	}

	/**
	 * ユーザがSFDCで作成されたかどうかの判定
	 * IDリクエスト.種別 = I
	 */
	public Boolean isSelfCreatedUser(){
		List<E_IDRequest__c> idr = E_IDRequestDaoWithout.getRecsByIDCPFId(idcpf.Id);
		if(idr == null || idr.isEmpty()){
			return false;
		}
		return true;
	}

	/**
	 *		EIDCのAppModeを切り替える
	 *				IRIS　→　NNLINK
	 *				上記以外（NNLINKまたは空欄）　→　IRIS
	 */
	public void switchEIDC_appMode(){
		if(idcpf==null) return;

		if(idcpf.appMode__c == I_Const.APP_MODE_IRIS){
			idcpf.appMode__c = I_Const.APP_MODE_NNLINK;
		}else{
			idcpf.appMode__c = I_Const.APP_MODE_IRIS;
		}
		E_IDCPFTriggerHandler.isSkipTriggerActions = true;
		update idcpf;
		E_IDCPFTriggerHandler.isSkipTriggerActions = false;
	}

	// appModeを指定した時は強制的にSwitch
	public void switchEIDC_appMode(String appMode){
		if(idcpf!=null && idcpf.appMode__c != appMode){
			idcpf.appMode__c = appMode;
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			update idcpf;
			E_IDCPFTriggerHandler.isSkipTriggerActions = false;
		}
	}

	/**
	 * [TODO]削除 利用アプリがIRISか？
	 */
	public Boolean isUseIRIS(){
		return (idcpf != null
					&& idcpf.appMode__c == I_Const.APP_MODE_IRIS
					&& (isNormalAgent()||isEmployee())
				);
	}

	/**
	 * [TODO]削除利用アプリがNNLinkか？
	 */
	public Boolean isUseNNLink(){
		return (idcpf != null
			 && idcpf.appMode__c == I_Const.APP_MODE_NNLink);
	}

	/**
	 *		EMail変更を推奨するユーザかどうか？
	 *				E_HomeControllerから移植
	 */
	public Boolean isEmailSuggestUser(){
		// サービス時間のチェック　時間外：false
		if(isServiceHourMenu(E_Const.ID_KIND.USER)!=null){
			return false;
		}

		//住生ユーザーの場合、登録を促さない
		if(isSumiseiUser()||isSumiseiIRISUser()){
			return false;
		}

		//代理店ユーザ以外の場合、登録を促さない
		//if(isAgent()==false){				//一般代理店ユーザ以外は登録を促さない　2016.12.13
		if(isNormalAgent()==false){
			return false;
		}

		//ユーザレコードを取得
		//User userRec = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		//EIDCレコードを取得
		//E_IDCPF__c eidcRec = E_IDCPFDao.getRecsById(UserInfo.getUserId());

		//ユーザ名とメールアドレスが同じ場合、登録済み
		String userName = user.Username.substringBefore(System.Label.E_USERNAME_SUFFIX);		//ユーザ名のSUFFIXより前の部分を抽出
		if(userName.equals(user.EMAIL)){
			return false;
		}

		//推奨有効期限日数をカスタム表示ラベルより取得
		Integer confDt;
		try{
			//confDt = Integer.valueOf(System.Label.E_PWLmitDays);
			confDt = Integer.valueOf(System.Label.E_Confirm_After_N_Days);
		}catch(Exception e){ //数値変換不可な値の場合は推奨不要
			return false;
		}
		if(confDt==null || confDt==0){
			return false;
		}
		//EIDCレコードIDからEIDRレコードリストを取得
		List<E_IDRequest__c> eidrRecList = E_IDRequestDao.getRecsByIDCPFId(idcpf.Id);

		Datetime beforeNDt = DateTime.now();
		beforeNDt = beforeNDt.addDays(-confDt);
		//IDリクエストのチェック
		for(E_IDRequest__c idReqest : eidrRecList){
			//EIDRに種別'E'で作成日が7日間以内のレコードがある場合、登録済み
			if(idReqest.ZUPDFLAG__c == E_Const.ZUPDFLAG_E && idReqest.CreatedDate >= beforeNDt){
				return false;
			}
		}

		return true;
	}

	//スプリントバックログ対応 #2 ここから 2017/1/17
	/**
	 *		PW変更を提示するかどうか
	 *				E_HomeControllerから移植
	 */
	public Boolean isSuggestPWChange(){
		//一般代理店ユーザ以外の場合、PW変更定時しない
		if(isNormalAgent()==false){
			return false;
		}
		//GW経由のアクセスの場合はPW変更定時しない
		if(isCommonGateway()){
			return false;
		}
		//前回変更日がnull
		if(user.LastPasswordChangeDate == null){
			return false;
		}
		//カスタム表示ラベルよりパスワードの有効期間を取得
		Integer limitDays;
		try{
			limitDays = Integer.valueOf(System.Label.E_PWLmitDays);
		}catch(Exception e){
			return false;//数値変換できない場合（'-'など）は無期限とみなす
		}

		//EIDCレコードIDからEIDRレコードリストを取得
		List<E_IDRequest__c> eidrRecList = E_IDRequestDao.getRecsByIDCPFId(idcpf.Id);

		//前回変更日が90日以上前
		if(user.LastPasswordChangeDate.addDays(limitDays) <= datetime.now()){
			Integer confDt = Integer.valueOf(System.Label.E_PWLmitDays);
			Datetime beforeNDt = DateTime.now();
			beforeNDt = beforeNDt.addDays(-confDt);
			//IDリクエストのチェック
			for(E_IDRequest__c idReqest : eidrRecList){
				//EIDRに種別'PW_C'で作成日が90日間以内のレコードがある場合、案内を出さない
				if(idReqest.ZUPDFLAG__c == E_Const.ZUPDFLAG_PW && idReqest.CreatedDate >= beforeNDt){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	//スプリントバックログ対応 #2 ここまで


	//============	2017.12 たもつくん対応 START	=============//
	/**
	 * MobileSDK経由判定
	 */
	public Boolean isViaMobileSDK(){
		//【Headers】
		//{"X-Salesforce-VIP":"FORCE","X-Salesforce-SIP":"122.208.71.146","X-Salesforce-Forwarded-To":"cs31.salesforce.com"
		//	,"User-Agent":"SalesforceMobileSDK/5.2.0 iOS/10.3.1 (iPhone) NIPLite/1.5(8) Native uid_DADE6E37-8E59-4D49-BBF8-BA6FB04A367F ftr_"
		//	,"Host":"irisdev3-nnlife-jp.cs31.force.com","Connection":"keep-alive","CipherSuite":"ECDHE-RSA-AES256-GCM-SHA384 TLSv1.2 256-bits"
		//	,"Cache-Control":"max-age=259200","Accept-Language":"ja-jp","Accept-Encoding":"gzip, deflate","Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}

		//【Parameters】
		//{"startURL":"/nnlinkage/setup/secur/RemoteAccessAuthorizationPage.apexp?source=CAAAAV6393GTME8wcDAwMDAwMDA0Qzk0AAAA0q8Wx5hZZl_rDNXYInN54knIRY57Zx4c1a6b-SZKqZsVPvx7YbFsRiPLg8BjysYc

		for(String val:E_Util.getUserAgentStrings()){
			if(val.containsIgnoreCase('SalesforceMobileSDK')){
				return true;
			}
		}
		return false;
	}
	//============	2017.12 たもつくん対応  END	=============//
	// 20171101 銀行代理店開放 START
	/**
	 * アクセス判定
	 * アクセス可能:ture, アクセス負荷:false
	 */
	public Boolean accessIRIS(){
		//社員ユーザはスキップ
		if(isEmployee()) return true;

		//インターネット経由
		if(isViaInternet()){
			//モバイル経由ではない
			if(!isViaMobileSDK()){
				//銀行ユーザ or IPアドレスチェックフラグ:true
				Account office = user.Contact.Account;
				if(office.E_DoCheckIP__c && office.ParentId!=null){
					//IPアドレス判定
					if(!E_Util.ismatchIpRange(E_AccountDaoWithout.getParentAcc(office.ParentId).E_WhiteIPAddress__c)){
						return false;
					}
				}
			}
		}
		return true;
	}
	// 20171101 銀行代理店開放 END
	// P16-0003 Atria対応開発 ここから
	/**
	 * Atriaへのアクセスが許可されているかを判定する.
	 *
	 * @return Boolean Atriaへアクセス可であればtrue さもなくばfalse
	 */
	public Boolean canAccessAtria() {

		if(this.isEmployee()) {
			return true;
		}

		if(this.user.Contact == null) {
			return false;
		}

		// 代理店事務所の登録状況コード(AGステータス)をチェック
		if(this.user.Contact.E_CL3PF_VALIDFLAG__c != '1') {
			return false;
		}

		// 募集人の募集資格をチェック
		if(this.user.Contact.E_CL3PF_KQUALFLG__c != 'Y') {
			return false;
		}

		// 募集人の仮業廃フラグをチェック
		if(this.user.Contact.E_CL3PF_ZSUBTRM__c == 'Y') {
			return false;
		}

		/*
 		if(this.idcpf.appMode__c == I_Const.APP_MODE_NNLINK
 			&& !isNormalORBankAgent()){
 //			&& !this.isNormalAgent()) {

 			return false;
 		}
		 */
		//appMode = Nullの場合に正常に動作していなかった。
		//一般代理店 or 銀行代理店ユーザであれば利用できるように修正
		 if(!isNormalORBankAgent()) {
			 return false;
		 }

 		return true;
	}

	/**
	* 権限管理画面を使用できるユーザか判定する。
	*
	* @return 権限管理使用のための権限セットを持っていればture,さもなくばfalse
	*/
	public Boolean hasATRAuthMngRead(){
		//Atria権限管理(参照のみ)/Atria権限管理(更新可能)いずれかを持っていればtrue
		return ( getAssignedPermissionSetNames().contains( E_Const.PERM_ATRAuthRead ) ||
				  getAssignedPermissionSetNames().contains( E_Const.PERM_ATRAuthUpd ) ) ;
	}

	public Boolean hasATRAuthMngUpd(){
		//Atria権限管理(更新可能)を持っていればtrue
		 return getAssignedPermissionSetNames().contains( E_Const.PERM_ATRAuthUpd );
	}

	// P16-0003 Atria対応開発 ここまで
}