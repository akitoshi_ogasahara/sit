@isTest
private class TestI_SalesPlanListController {
	private static Account acc;
	private static E_AgencySalesResults__c asr;
	private static E_SalesPlan__c sp;
	private static Attachment att;
	
	// 目標値入力
	@isTest static void inputGoalTest() {
		createData();
		I_SalesPlanListController controller = new I_SalesPlanListController();
		controller.sResultsId = asr.Id;
		controller.parentAccId = acc.Id;
		controller.getIsDisp();
		controller.addRows();
		controller.getTotalRows();
		controller.getListMaxRows();
		PageReference PageRef = controller.inputGoal();
	}
	// ファイル表示
	@isTest static void openPDFTest() {
		createData();
		I_SalesPlanListController controller = new I_SalesPlanListController();
		controller.sResultsId = asr.Id;
		controller.parentAccId = acc.Id;
		PageReference PageRef = controller.openPDF();
	}
	// 削除
	//@isTest static void deletePDFTest() {
	//	createData();
	//	I_SalesPlanListController controller = new I_SalesPlanListController();
	//	PageReference pageRef = Page.IRIS_SalesPlan;
	//	controller.sResultsId = asr.Id;
	//	controller.parentAccId = acc.Id;
	//	pageRef.getParameters().put('deleteId', att.Id);
	//	controller.deletePDF();
	//}

	// テストデータ作成
	private static void createData(){

		acc = new Account(Name = 'テスト代理店格'
						, E_CL1PF_ZHEADAY__c = '10001');
		insert acc;

		asr = new E_AgencySalesResults__c(ParentAccount__c = acc.Id
										, BusinessDate__c = '20000101'
										, Hierarchy__c = 'AH'
										, QualifSim__c = '優績S'
										, AgencyName__c = '代理店名'
										);
		insert asr;

		sp = new E_SalesPlan__c(ParentAccount__c = acc.Id
								, SalesResult__c = asr.Id);
		insert sp;
		// 営業計画書PDF
		//pdfファイルの元作成
		String body = 'テスト';
		body += '\r\n';
		body += '営業';
		body += '\r\n';
		body += '計画書';
		body += '\r\n';
		//バイナリデータ
		Blob pdfFileBody = Blob.valueOf(body);
		att = new Attachment(Name = 'テスト'
							, ParentId = sp.Id
							, Body = pdfFileBody);
		insert att;
	}
}