public with sharing class I_AuthorityAgencyController extends I_AbstractController {
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;private set;}
	// リスト行数
	public Integer rowCount {get; set;}

// 代理店一覧
	// 代理店リスト
	public List<AgencyListRow> agencyRows {get; private set;}
	// 代理店リスト件数
	public Integer getAgencyCount() {
		return agencyRows == null ? 0 : agencyRows.size();
	}

// 共通
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}

	// 代理店一覧
	public List<Account> agencies {get;private set;}
	// 検索用文字列
	public String searchText {get; set;}
	//拠点担当営業フラグ
	public Boolean IsAuthFlag {get;private set;}
	//拠点分表示フラグ
	public Boolean IsAuthExtract {get; set;}

	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE	 = 'st';
	// URLパラメータ名 - フィルタID
	public static final String URL_PARAM_FILTER_ID	 = 'id';
	// 代理店一覧用ソートキー
	public static final String SORT_TYPE_AGENCY		 = 'agency';
	public static final String SORT_TYPE_ZHEADAY	 = 'zheaday';
	public static final String SORT_TYPE_AGENTADDRESS	 = 'agentaddress';
	// 代理店一覧用ソート項目
	public static final String SORT_FIELD_AGENCY	 = 'E_CL1PF_ZAHKNAME__c';
	public static final String SORT_FIELD_ZHEADAY	 = 'Parent.E_CL1PF_ZHEADAY__c';
	public static final String SORT_FIELD_AGENTADDRESS	 = 'Parent.E_COMM_ZCLADDR__c';

	// コンストラクタ
	public I_AuthorityAgencyController() {
		super();
		pageMessages = getPageMessages();

		// デフォルトソート - 昇順
		sortIsAsc = true;
		searchText = '';


		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	protected override String getLinkMenuKey() {
		//権限変更画面（代理店一覧）用のログを作成
		return 'AuthorityManage';
	}

	//Atria権限管理用の権限セットがついていなければエラーページへ遷移
	protected override Boolean isValidate(){
		return access.hasATRAuthMngRead();
	}

	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}

		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}

		if( access.isEmployee() ){
			//プロファイルがMR または拠点長のときに拠点分表示ボタンを表示する
			isAuthFlag = ( access.isAuthMR() || access.isAuthManager() ) && !access.ZINQUIRR_is_BRAll();
		} else {
			return redirectAgentPage(); //社員でない場合は、募集人一覧に遷移
		}

		//初期値は絞り込み拠点表示ではなく全件表示
		IsAuthExtract = false;

		// デフォルトソート - 代理人名
		sortType = SORT_TYPE_AGENCY;
		sortIsAsc = true;
		getAgencyList();

		return null;
	}

	/**
	 * 検索：getAgencyListを呼び、画面を再読み込みする
	 */
	public PageReference searchAgency(){
		getAgencyList();
		return null;
	}

	/**
	 * 拠点分表示/自身担当代理店表示：絞り込みフラグを反転し、行を再作成する
	 */

	public PageReference authChangeAgency(){
		searchText = '';
		IsAuthExtract = !IsAuthExtract;
		getAgencyList();
		return null;
	}

	/**
	 * 代理店一覧作成：条件によってクエリを変更し、必要な情報を取得する
	 *
	 */
	private void getAgencyList() {
		pageMessages.clearMessages();

		String ownerId = '';
		String brunchCode = '';

		if( isAuthFlag ){
			if( IsAuthExtract ){
				brunchCode = access.idcpf.ZINQUIRR__c.right(2);//営業かつ拠点分表示の場合は拠点コードを設定
			} else {
				ownerId = UserInfo.getUserId();//営業かつ自身担当分表示の場合は自身のユーザIdを設定
			}
		}
		agencies = new List<Account>();
		agencies = E_AccountDaoWithout.getRecsIRISAuthorityAgencySearch( ownerId, brunchCode, searchText );

		if( agencies.size() > I_Const.LIST_MAX_ROWS  ){
			pageMessages.addWarningMessage(getMSG().get('LIS|009'));
		} else if( agencies.size() ==0 ){
			pageMessages.addWarningMessage(getMSG().get('IRIS|SEARCH|001'));
		}
		createAgencyRows( agencies );

		// ソート
		I_AuthorityAgencyController.list_sortType	= sortType;
		I_AuthorityAgencyController.list_sortIsAsc = sortIsAsc;
		agencyRows.sort();

		system.debug(pageMessages);
		system.debug( 'pageMessages.hasMessages = ' + pageMessages.hasMessages());

	}

	/**
	 * 代理店一覧作成
	 */
	private void createAgencyRows( List<Account> agencies ){
		// 重複チェック用のIDセット
	Set<Id> agencyIdSet = new Set<Id>();
	agencyRows = new List<AgencyListRow>();

		for (Account acc : agencies) {
			// 重複チェック
			if (!agencyIdSet.contains(acc.Parent.Id)) {
				AgencyListRow row = new AgencyListRow();
				row.id = acc.Parent.Id;
				row.agency = acc.Parent.Name;
				row.zheaday = acc.Parent.E_CL1PF_ZHEADAY__c;
				row.address = acc.Parent.E_COMM_ZCLADDR__c;
				row.agencySort = acc.Parent.E_CL1PF_ZAHKNAME__c; //カナソート用
				agencyRows.add(row);
				if(agencyRows.size() >= I_Const.LIST_MAX_ROWS ){
					//行数が1000件を超えると表示できなくなってしまうので、break
					break;
				}
			}
			agencyIdSet.add(acc.Parent.Id);
		}
	}

	/**
	 * ソート
	 */
	public void sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		// ソート方法 - SOQL
		if (sortType != sType) {
			sortType	= sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		I_AuthorityAgencyController.list_sortType	= sortType;
		I_AuthorityAgencyController.list_sortIsAsc = sortIsAsc;
		agencyRows.sort();

		return;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		Integer totalRow = 0;
		totalRow = agencyRows == null ? 0 : agencyRows.size();
		return totalRow;
	}

	/**
	 * 募集人一覧へ遷移
	 */
	private PageReference redirectAgentPage() {
		PageReference pr = Page.IRIS_AuthorityAgent;
		pr.setRedirect(true);
		return pr;
	}

	 /**
	 * 内部クラス
	 */
	public class AgencyListRow implements Comparable {
		// 代理店SFID
		public String id {get; set;}
		// 代理店名
		public String agency {get; set;}
		// 代理店格コード
		public String zheaday {get; set;}
		// 住所
		public String address {get; set;}
		//カナソート用変数
		public String agencySort{get; set;}

		// コンストラクタ
		public AgencyListRow() {
			id = '';
			agency = '';
			zheaday = '';
			address = '';
			agencySort = '';//カナソート用変数
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			AgencyListRow compareToRow = (AgencyListRow)compareTo;
			Integer result = 0;

			// 代理店名
			if (list_sortType == SORT_TYPE_AGENCY) {
				result = I_Util.compareToString(agencySort, compareToRow.agencySort);//カナソート用
			// 代理店コード
			} else if (list_sortType == SORT_TYPE_ZHEADAY) {
				result = I_Util.compareToString(zheaday, compareToRow.zheaday);
			// 住所
			} else if (list_sortType == SORT_TYPE_AGENTADDRESS) {
				result = I_Util.compareToString(address, compareToRow.address);
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}
}