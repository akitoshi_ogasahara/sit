public with sharing class E_ProfileDao {
	
	/**
	 * プロファイルデータをプロファイルIdから取得する
	 * @param profileId: プロファイルID
	 * @return Profile: プロファイル情報
	 */
	public static Profile getRecById(Id profileId){
		return [
			SELECT
				id,
				Name
			FROM Profile
			WHERE Id = :profileId
		];
	}

	/**
	 * 特定オブジェクトに対してすべての参照権限をもつProfileIdを含むObjectPermission
	 */
	public static List<ObjectPermissions> getAllModifyRecordsObjectPermissionsBySObject(String sobjNm){
		return	[SELECT Id, SobjectType, 
						ParentId, 
						Parent.IsOwnedByProfile, 
						Parent.ProfileId,
						Parent.Profile.Name
						,PermissionsViewAllRecords
						,PermissionsRead
						,PermissionsModifyAllRecords
						,PermissionsEdit
						,PermissionsDelete
						,PermissionsCreate 
				From ObjectPermissions
				WHERE SobjectType = :sobjNm
				  AND Parent.IsOwnedByProfile = true
				  AND PermissionsModifyAllRecords = true];
	}


	/*
	 *	ProfileのMap
	 */
	private static Map<Id, Profile> profileMap;
	public static Map<Id, Profile> getProfileMap(){
		if(profileMap == null){
			profileMap = new Map<Id,Profile>([SELECT Id,Name FROM Profile]);
		}
		return profileMap;
	}
	
	/**
	 *	Profile名定義とBoolean
	 */
	public static Boolean equalsProfileName(Id profId, String profName){
		Profile prof = getProfileMap().get(profId);
		return (prof!=null) && (prof.Name == profName); 
	}

	//拠点MRユーザ判定
	public static final String PROFILE_MR = 'MR'; 
	public static Boolean isMR(Id profId){
		return equalsProfileName(profId, PROFILE_MR);
	}
	
	//拠点営業部長ユーザ判定（拠点長）
	public static final String PROFILE_MRManager = '拠点長'; 
	public static Boolean isMRManager(Id profId){
		return equalsProfileName(profId, PROFILE_MRManager);
	}
	
	//本社MRユーザ判定
	public static final String PROFILE_HQMR = '本社営業部MR'; 
	public static Boolean isHQMR(Id profId){
		return equalsProfileName(profId, PROFILE_HQMR);
	}

	//本社営業部部長ユーザ判定
	public static final String PROFILE_HQMRManager = '本社営業部拠点長'; 
	public static Boolean isHQMRManager(Id profId){
		return equalsProfileName(profId, PROFILE_HQMRManager);
	}

	//エリア部長ユーザ判定
	public static final String PROFILE_AreaManager = 'エリア部長'; 
	public static Boolean isAreaManager(Id profId){
		return equalsProfileName(profId, PROFILE_AreaManager);
	}

	//営業企画管理部ユーザ判定
	public static final String PROFILE_PlanningManagement = '営業企画管理部'; 
	public static Boolean isPlanningManagement(Id profId){
		return equalsProfileName(profId, PROFILE_PlanningManagement);
	}

	//営業推進部ユーザ判定
	public static final String PROFILE_SalesPromotion = '営業推進部'; 
	public static Boolean isSalesPromotion(Id profId){
		return equalsProfileName(profId, PROFILE_SalesPromotion);
	}


	// P16-0003 Atria対応開発
	//コールセンター部
	public static final String PROFILE_CallCenter = 'コールセンター部';
	public static Boolean isCallCenter(Id profId){
		return equalsProfileName(profId, PROFILE_CallCenter);
	}


	//営業推進部長
	public static final String PROFILE_HQEManager = '営業推進部長';
	public static Boolean isHQEManager(Id profId){
		return equalsProfileName(profId, PROFILE_HQEManager);
	}

	//システム管理者
	public static final String PROFILE_SystemAdmin = 'システム管理者';
	public static Boolean isSystemAdmin(Id profId){
		return equalsProfileName(profId, PROFILE_SystemAdmin);
	}

	//本社営業関連部
	public static final String PROFILE_HOSDivision = '本社営業関連部';
	public static Boolean isHOSDivision(Id profId){
		return equalsProfileName(profId, PROFILE_HOSDivision);
	}
}