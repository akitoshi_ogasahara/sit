public with sharing class CR_AgencySearchWrapController extends CR_AbstractExtender{
    /**
     * Constractor
     */
    public CR_AgencySearchWrapController() {
        pgTitle = '代理店検索（規程）';
    }

    /**
     * 規定管理一覧（規定）へのページ遷移判定
     */
    public pageReference pageAction(){
        String url = getUrlforRules();
        if(String.isNotBlank(url)){
            return new PageReference(url);
        } 
        return null;
    }   
}