global with sharing class CR_OutsrcsExtender extends CR_AbstractExtender{
    
    private CR_OutsrcsController controller;
    
    public CR_OutsrcsExtender(CR_OutsrcsController extension){
        super();
        this.controller = extension;
        this.helpMenukey = CR_Const.MENU_HELP_OUTSRC;
        this.helpPageJumpTo = CR_Const.MENU_HELP_OUTSRC_List;
    }
    
    public override void init(){
        isSuccessOfInitValidate = false;
        if(getHasOutsrcViewPermission()==false){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG05_REQUIRED_PERMISSION_OUTSRC);        //'外部委託参照権限がありません'
        }else if(this.controller.record == null 
                || String.isBlank(this.controller.record.AgencyCode__c)){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            this.viewingAgencyCode = this.controller.record.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }
    }

    //新規外部委託
    public String getUrlforNew(){
        PageReference pr = Page.E_CROutSrcEdit;
        pr.getParameters().put('parentId', controller.record.Id);
        return pr.getUrl();
    }



}