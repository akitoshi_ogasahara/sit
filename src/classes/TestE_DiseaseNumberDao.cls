@isTest
private class TestE_DiseaseNumberDao{

	/**
	 * getDiseaseNumberTestメソッドテスト
	 * 傷病No、傷病マスタ、保険種類引受条件
	 */
	static testMethod void getDiseaseNumber_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underwritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		Test.startTest();
		E_DiseaseNumber__c result = E_DiseaseNumberDao.getDiseaseNumber('5');
		Test.stopTest();

		System.assertEquals(diseaseNumberList[4].Name, result.Name);
		System.assertEquals(diseaseList[10].Name, result.Diseases__r[0].Name);
		System.assertEquals(diseaseList[11].Name, result.Diseases__r[1].Name);
		System.assertEquals(diseaseList[12].Name, result.Diseases__r[2].Name);
		System.assertEquals(diseaseList[13].Name, result.Diseases__r[3].Name);
		System.assertEquals(diseaseList[14].Name, result.Diseases__r[4].Name);
		// 状況表示順項目でソート取得している関係でassertがややこしいため省略
		System.assertEquals(8, result.InsTypeUWs__r.size());
	}


	/**
	 * getRecMapByNamesメソッドテスト
	 * 傷病No
	 */
	static testMethod void getRecMapByNames_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		Set<String> numberSet = new Set<String>{'2', '3'};

		Test.startTest();
		Map<String, E_DiseaseNumber__c> result = E_DiseaseNumberDao.getRecMapByNames(numberSet);
		Test.stopTest();

		System.assertEquals(diseaseNumberList[1].Name, result.get('2').Name);
		System.assertEquals(diseaseNumberList[2].Name, result.get('3').Name);
	}

	/**
	 * getRecsByNamesメソッドテスト
	 * 傷病No、傷病マスタ
	 */
	static testMethod void getRecsByNames_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		Set<String> numberSet = new Set<String>{'3', '4'};

		Test.startTest();
		List<E_DiseaseNumber__c> result = E_DiseaseNumberDao.getRecsByNames(numberSet);
		Test.stopTest();

		System.assertEquals(diseaseNumberList[2].Name, result[0].Name);
		System.assertEquals(diseaseList[3].Name, result[0].Diseases__r[0].Name);
		System.assertEquals(diseaseList[4].Name, result[0].Diseases__r[1].Name);
		System.assertEquals(diseaseList[5].Name, result[0].Diseases__r[2].Name);
		System.assertEquals(diseaseNumberList[3].Name, result[1].Name);
		System.assertEquals(diseaseList[6].Name, result[1].Diseases__r[0].Name);
		System.assertEquals(diseaseList[7].Name, result[1].Diseases__r[1].Name);
		System.assertEquals(diseaseList[8].Name, result[1].Diseases__r[2].Name);
		System.assertEquals(diseaseList[9].Name, result[1].Diseases__r[3].Name);
	}
}