/*
		I_SideMenuController
			I_MenuProviderを利用してメニューレコードを取得する。
*/
public with sharing class I_SideMenuController {

	// 現在指定されているIRISメニューのリンクメニューキー
	public String mainCategoryLabel{get;private set;}
	public String mainCategoryStyleClass{get;private set;}
	public String subCategoryLabel{get;private set;}

	/**
	 * @return List<sideMenuItem> メニュー情報
	 */
	public List<sideMenuItem> getMenuItems() {
		E_IRISHandler iris = E_IRISHandler.getInstance();
		this.mainCategoryLabel = iris.irisMenus.activeMenu.MainCategoryAlias__c;
		this.mainCategoryStyleClass = iris.irisMenus.activeMenu.MainCategoryStyleClass__c;
		if(String.isNotBlank(iris.irisMenus.activeMenu.subCategory__c)){
			//this.subCategoryLabel = iris.irisMenus.activeMenu.subCategory__c.mid(2, iris.irisMenus.activeMenu.subCategory__c.length());
			this.subCategoryLabel = iris.irisMenus.activeMenu.subCategory__c.subStringAfter('.');
		}
		List<I_MenuMaster__c> menuRecs = iris.irisMenus.getActiveCategoryMenus();
//system.assert(menurecs!=null, 'ActiveCategoryのメニューリストが取得できませんでした。');
		if(menuRecs==null || menuRecs.isEmpty()){
			return null;
		}

		Map<Id, sideMenuItem> menuItemsById = new Map<Id, sideMenuItem>();
		List<sideMenuItem> rootMenuItems = new List<sideMenuItem>();
		// 木構造の作成を１ループで行う
		for (I_MenuMaster__c menuRec: menuRecs) {
			if (!menuItemsById.containsKey(menuRec.Id)) {
				// 作成済みの格納先が存在しない場合は用意する
				menuItemsById.put(menuRec.Id, new sideMenuItem());
			}
			sideMenuItem item = menuItemsById.get(menuRec.Id);
			item.record = menuRec;
//			item.isActive = (menuRec.Id == iris.irisMenus.activeMenu.Id);	isActiveはメソッド経由で取得

			Id parentMenuId = menuRec.ParentMenu__c;
			Boolean isRoot = String.isBlank(parentMenuId);
			if (isRoot) {
				rootMenuItems.add(item);
			} else {
				if (!menuItemsById.containsKey(parentMenuId)) {
					// 作成済みの格納先が存在しない場合は用意する
					menuItemsById.put(parentMenuId, new sideMenuItem());
				}
				menuItemsById.get(parentMenuId).addChild(item);
			}
		}

		return rootMenuItems;
	}

/*
	private static I_MenuMaster__c getMenuRecordByLinkMenuKey(String linkMenuKey) {

		String value = linkMenuKey;
		if (String.isNotBlank(value)) {
			value = value.replaceAll('%', '\\\\%');
			value = value.replaceAll('_', '\\\\_');
			value = '%' + value + '%';
		}

		return I_MenuMasterDao.findRecordByLinkMenuKey(value);
	}
*/


	/** メニュー情報を格納するクラス */
	public class sideMenuItem extends I_MenuItem{
//		public Boolean isActive {get; set;}
		public Boolean isActive {get{
										return record != null 
											&& record.SubCategory__c != I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL
												&& record.Id == E_IrisHandler.getInstance().irisMenus.activeMenu.Id;
									}
								set;}

		public Boolean hasActivatedChild {get; set;}
		public List<sideMenuItem> children{get; set;}

		public sideMenuItem() {
			this.isActive = false;
			this.hasActivatedChild = false;
			this.children = new List<sideMenuItem>();
		}

		public void addChild(sideMenuItem childItem) {
			this.hasActivatedChild = (this.hasActivatedChild || childItem.isActive);
			this.children.add(childItem);
		}

		public override String getDestUrl(){
			//　子メニューがない場合遷移先を返す
			if(children.isEmpty()){
				return super.getDestUrl();
			}
			return null;
		}
	}

	/** クラス内で利用するEXception */
	public class GeneralException extends Exception {}
}