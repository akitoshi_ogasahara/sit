/**
 * 営業日報データアクセスオブジェクト
 * CreatedDate 2018/04/27
 * @Author Terrasky
 */

public class ASS_UserDao {

	/**
	 * ユーザ取得
	 * @param usId: ユーザID
	 * @return User: ユーザ
	 */
	public static User getUserById(Id usId) {
		return [SELECT
					Name
					, Username 					//ユーザ名
					, Unit__c 					//営業部
				FROM 
					User
				WHERE 
					Id = :usId
				];
	}
	/**
	 * ユーザ取得
	 * @pram unitName: 営業部名
	 * @param usId: ユーザID
	 * @return List<User>: ユーザリスト
	 */
	public static List<User> getOtherUsersByUnitName(String unitName, Id usId) {
		return [SELECT
					Name 
				FROM 
					User 
				WHERE 
					Unit__c = :unitName 
				AND 
					Id != :usId
				AND
					IsActive = true
				Order By 
					MemberNo__c
					];
	}
}