/**
 * MD_TopControllerテストクラス
 */
@isTest
private class TestMD_TopController {

	//test001
	//お知らせリスト、掲示ファイルリスト0件
	static testMethod void MD_TopControllerTest001() {
		PageReference pageRef = Page.E_MDTop;

		Test.setCurrentPage(pageRef);

		Test.startTest();
		MD_TopController ctl = new MD_TopController();
		ctl.init();
		ctl.getIsEmployee();
		//ctl.doPrev();
		//ctl.doNext();
		Test.stopTest();

		system.assertEquals(ctl.infoList,null);
	}

	//test002
	//お知らせリスト：5件の場合（閾値5件）
	//掲示ファイルリスト1件
	static testMethod void MD_TopControllerTest002() {
		//テストデータ作成
		//お知らせリスト作成
		E_MenuMaster__c menuMaster1 = new E_MenuMaster__c(MenuMasterKey__c='MD_Information',SelectedMenuKey__c='key1');
		insert menuMaster1;
		//メッセージマスタリスト作成：5件の場合（閾値5件）
		List<E_MessageMaster__c> insMessage = new List<E_MessageMaster__c>();
		String type = 'メッセージ';
		String param = '9MSSI|001';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|002';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|003';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|004';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|005';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		insert insMessage;

		//掲示ファイルリスト作成
		E_MenuMaster__c menuMaster2 = new E_MenuMaster__c(MenuMasterKey__c='MD_File',SelectedMenuKey__c='key1');
		insert menuMaster2;
		//メッセージマスタリスト作成
		List<E_MessageMaster__c> insMessage2 = new List<E_MessageMaster__c>();
		String type2 = 'メッセージ';
		String param2 = 'MDT001';
		insMessage2.add(new E_MessageMaster__c(Value__C = param2,Key__c = param2,Name = param2,Type__C = type2,ShowSubPageLink__c = true,Menu__c = menuMaster2.Id,ShowPicsByAttachment__c = true));
		insert insMessage2;

		//ファイル情報作成
		RecordType rtype = [Select Id From RecordType Where DeveloperName = 'pattern01' Limit 1];
		E_CMSFile__c cmsFile = new E_CMSFile__c(ContentsName__c='ContentsName');
		cmsFile.RecordTypeId = rtype.Id;
		cmsFile.MessageMaster__c = insMessage2.get(0).Id;
		insert cmsFile;

		PageReference pageRef = Page.E_MDTop;

		Test.setCurrentPage(pageRef);

		Test.startTest();
		MD_TopController ctl = new MD_TopController();
		ctl.init();
		ctl.getIsEmployee();
		ctl.doNext();
		ctl.doPrev();
		Test.stopTest();

		system.assertEquals(ctl.infoList.size(),5);
		system.assertEquals(ctl.fileList.size(),1);
	}

	//test003
	//お知らせリスト：6件の場合（閾値5件）
	//掲示ファイルリスト1件
	static testMethod void MD_TopControllerTest003() {
		//テストデータ作成
		//お知らせリスト作成
		E_MenuMaster__c menuMaster1 = new E_MenuMaster__c(MenuMasterKey__c='MD_Information',SelectedMenuKey__c='key1');
		insert menuMaster1;
		//メッセージマスタリスト作成：6件の場合（閾値5件）
		List<E_MessageMaster__c> insMessage = new List<E_MessageMaster__c>();
		String type = 'メッセージ';
		String param = '9MSSI|001';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|002';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|003';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|004';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|005';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		param = '9MSSI|006';
		insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
		insert insMessage;

		//掲示ファイルリスト作成
		E_MenuMaster__c menuMaster2 = new E_MenuMaster__c(MenuMasterKey__c='MD_File',SelectedMenuKey__c='key1');
		insert menuMaster2;
		//メッセージマスタリスト作成
		List<E_MessageMaster__c> insMessage2 = new List<E_MessageMaster__c>();
		String type2 = 'メッセージ';
		String param2 = 'MDT001';
		insMessage2.add(new E_MessageMaster__c(Value__C = param2,Key__c = param2,Name = param2,Type__C = type2,ShowSubPageLink__c = true,Menu__c = menuMaster2.Id,ShowPicsByAttachment__c = true));
		insert insMessage2;

		//ファイル情報作成
		RecordType rtype = [Select Id From RecordType Where DeveloperName = 'pattern01' Limit 1];
		E_CMSFile__c cmsFile = new E_CMSFile__c(ContentsName__c='ContentsName');
		cmsFile.RecordTypeId = rtype.Id;
		cmsFile.MessageMaster__c = insMessage2.get(0).Id;
		insert cmsFile;

		PageReference pageRef = Page.E_MDTop;

		Test.setCurrentPage(pageRef);

		Test.startTest();
		MD_TopController ctl = new MD_TopController();
		ctl.init();
		ctl.getIsEmployee();
		ctl.doNext();
		Test.stopTest();

		system.assertEquals(ctl.infoList.size(),1); //doNext（）で5件ずつリストの2つ目
		system.assertEquals(ctl.fileList.size(),1);
	}

	static testMethod void getIsSumiseiIRISTest(){
		//実行ユーザー作成
		Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
		Account childAcc = TestI_TestUtil.createAccount(true, parentAcc);
		Contact con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ', true);
		User actUser = TestI_TestUtil.createAgentUser(true, 'sumiseiIRIS', E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS, con.Id);
		TestI_TestUtil.createIDCPF(true, actUser.Id, 'AT');
		TestI_TestUtil.createContactShare(con.Id, actUser.Id);

		PageReference pageRef = Page.E_MDTop;
		Test.setCurrentPage(pageRef);

		System.runAs(actUser){
			Test.startTest();
			MD_TopController ctl = new MD_TopController();
			System.assertEquals(TRUE, ctl.getIsSumiseiIRIS());
			Test.stopTest();
		}
	}
}