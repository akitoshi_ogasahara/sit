/**
 * メニューコンポーネントコントローラクラス
 * CreatedDate 2014/12/02
 * @Author Terrasky
 */

public with sharing class E_MenuController {

	//メニュー表示用データクラスリスト
	private static List<TopMenu> menuItems;
	public List<TopMenu> getMenuItems(){
		if(menuItems == null){
			Id mkid = getMenuKindId();
			createMenuItems(E_MenuMasterDao.getMenuMstsByMenukndId(mkid));
			return menuItems;
		}
		return menuItems;
	}
	
	//社員（Standardライセンスユーザ判定）
	public boolean getIsStandardUser(){
		return E_AccessController.getInstance().isEmployee();
	}
	
	//住生ユーザー
	public boolean getIsSumiseiUser(){
		return E_AccessController.getInstance().isSumiseiUser();
	}

	//代理店ユーザ インターネット経由かつ一般代理店の判定
	public boolean getIsAgentUser(){
		E_AccessController access = E_AccessController.getInstance();
		return access.isAgent() 
			&& access.user.Contact.E_MenuKindId__c == null
			&& (access.idcpf != null && access.idcpf.ZSTATUS01__c != null && access.idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)
				&& access.idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_IRIS))
			&& !access.isCommonGateway();
	}
	
	// 一般代理店の判定
	public Boolean getIsAgent(){
		E_AccessController access = E_AccessController.getInstance();
		return access.isAgent() 
			&& access.user.Contact.E_MenuKindId__c == null
			&& (access.idcpf != null && access.idcpf.ZSTATUS01__c != null && access.idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)
				&& access.idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_IRIS));
	}
	
	//ディスクレイマーMap
	public String getDisclaimerMEN001(){
		return E_Message.getDisclaimerMap().get('MEN|001');
	}
	public String getDisclaimerMEN002(){
		return E_Message.getDisclaimerMap().get('MEN|002');
	}
	
	//社員用メニュー種類選択リスト
	public static List<Selectoption> menuKindOptions {get; set;}
	//社員用メニュー種類選択値
	public String menuKindSelected {get; set;}
	
	//選択メニュー保持変数(attributeで取得)
	public String selectingMenuKeys {get; 
									set{
											selectingMenuKeys = value;
											E_IRISHandler.getInstance().setLinkMenuKey(value);
									}
	}
	//選択メニュー分割セット
	private static Set<String> selectingMenuSet;
	
	//メニュー部分表示制御
	public Boolean getRenderedMenu(){
		//ISS経由以外の時に表示する、かつメニュー表示フラグが0以外　かつ　IsHideMenu=Falseの時に表示する
		return !E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId()) /*&& E_Util.getIsMenu()*/ && !isHideMenu;
	}
	
	//社員用メニュー種類Id引き当て用マップ
	private Map<String, Id> menuMap;
	//メニュー種類リスト
	private List<E_MenuKind__c> mklist;
	
	//Attribute hideMenu
	public Boolean isHideMenu{get;set;}
	
	//アクセス経由種別（社員のみ）
	public String accessKindSelected {get; set;}
	
	/**
	 * コンストラクタ
	 */
	public E_MenuController(){
		//Constructor
	}
	
	/**
	 * メニュー種類IDを取得する
	 */
	private Id getMenuKindId(){
		if(E_AccessController.getInstance().isEmployee()){
			menuMap = new Map<String, Id>();
			menuKindOptions = new List<Selectoption>();
			
			mklist = E_MenuKindDao.getMenuKnds();
			//system.debug(mklist);
			for(E_MenuKind__c mk: mklist){
				if (!String.isEmpty(E_CookieHandler.getCookieAccessKind())) {
					if (E_CookieHandler.getCookieAccessKind().equals('0')) {
						//インターネット専用のメニュー以外は除く
						if (!isDisplayMenu(System.Label.E_INTERNET_MENU, mk.externalid__c)) continue;
					} else {
						//共同GW専用のメニュー以外は除く
						if (!isDisplayMenu(System.Label.E_CGW_MENU, mk.externalid__c)) continue;
					}
				} else {
					//インターネット専用のメニュー以外は除く
					if (!isDisplayMenu(System.Label.E_INTERNET_MENU, mk.externalid__c)) continue;
				}
				menuMap.put(mk.ExternalId__c, mk.Id);
				menuKindOptions.add(new SelectOption(mk.ExternalId__c, mk.Name));
			}
			accessKindSelected = E_CookieHandler.getCookieAccessKind();
			menuKindSelected = E_CookieHandler.getCookieMenuKind();
			if(String.isEmpty(menuKindSelected) || String.isEmpty(menuMap.get(menuKindSelected))){
				menuKindSelected = mklist[0].ExternalId__c;
				E_CookieHandler.setCookieMenuKind(menuKindSelected);
			}
			return menuMap.get(menuKindSelected);
			
		}else{
//system.debug('@@@社員以外');
			User usr = E_AccessController.getInstance().user;
/*
			Id AgentId = usr.Contact.Account.ParentId != null ? usr.Contact.Account.ParentId : usr.AccountId;
//system.debug('@@@AgentId:' + AgentId);
			return E_MenuKindDao.getMenuKndByAccountId(AgentId).Id;
*/
			E_MenuKind__c mk = new E_MenuKind__c();
			//メニュー種類がある時（一般代理店/銀行代理店以外）
			if (usr.Contact.E_MenuKindId__c != null) {
				mk = E_MenuKindDao.getRecById(usr.Contact.E_MenuKindId__c);
			//メニュー種類がない時（一般代理店/銀行代理店）
			}else{
				E_IDCPF__c idcpf = E_AccessController.getInstance().idcpf;
				if(idcpf != null && idcpf.ZSTATUS01__c != null && idcpf.ZDSPFLAG02__c != null){
					//ZSTATUS01=1 の場合、'MK_Agency'
					//if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)){
					if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE) && idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_IRIS)){
						mk = E_MenuKindDao.getRecByExternalId(E_Const.MK_EXID_AGENCY);
					//ZSTATUS01=6 の場合、 ‘MK_BankAgency’
					//}else if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_CGW)){
					}else if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE) && idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_BANK)){
						mk = E_MenuKindDao.getRecByExternalId(E_Const.MK_EXID_BANK_AGENCY);
					}
				}
			}
			if (mk != null) {
				E_CookieHandler.setCookieMenuKind(mk.ExternalId__c);
				return mk.Id;
			}
		}
		return null;
	}
	
	
	/**
	 * メニューを作成する
	 * @param mmlist: メニューマスタリスト
	 * @return ---
	 */
	private void createMenuItems(List<E_MenuMaster__c> mmlist){
//system.debug('@@@mmlist:'+ mmlist);
//system.debug('@@@selectingMenuKeys:'+ selectingMenuKeys);
		menuItems = new List<TopMenu>();
		selectingMenuSet = new Set<String>();
		if(String.isNotBlank(selectingMenuKeys)){
			for(String s: selectingMenuKeys.split(',')){
				selectingMenuSet.add(s.trim());
			}
		}
//system.debug('@@@mmlist.size():'+ mmlist.size());
		for(Integer i = 0; i < mmlist.size(); i++){
//system.debug('@@@mmlist[i].E_MenuMasters__r.size():'+ mmlist[i].E_MenuMasters__r.size());
			if(!mmlist[i].E_MenuMasters__r.isEmpty()){
				menuItems.add(new TopMenu(mmlist[i]));
			}
		}
	}
	
	
	/**
	 * 社員用選択リスト変更時、メニュー情報再作成処理
	 * @param ---
	 * @return PageReference: 遷移元ページ
	 */
	public PageReference changeMenuKind(){
		createMenuItems(E_MenuMasterDao.getMenuMstsByMenukndId(menuMap.get(menuKindSelected)));
		E_CookieHandler.setCookieMenuKind(menuKindSelected);
		return Page.E_Home.setRedirect(true);
	}
	
	/**
	 * 社員用アクセス経由種別リスト変更時、メニュー情報再作成処理
	 * @param ---
	 * @return PageReference: 遷移元ページ
	 */
	public PageReference changeAccessKind(){
		E_CookieHandler.setCookieAccessKind(accessKindSelected);
		E_CookieHandler.setCookieMenuKind('MK_Agency');
		createMenuItems(E_MenuMasterDao.getMenuMstsByMenukndId(menuMap.get(menuKindSelected)));
		return Page.E_Home.setRedirect(true);
	}
	
	/**
	 * トップメニュークラス
	 */
	private class TopMenu{
		//トップメニュー
		public transient E_MenuMaster__c tm {get; private set;}
		//メニューアイテムリスト
		public transient List<MenuItem> menuList {get; private set;}
		//選択中のメニューがあるか？
		public transient Boolean hasSelectedMenu{get; private set;}
		
		/**
		 * コンストラクタ(トップメニュークラス)
		 * @param mmlist: メニューマスタ(トップメニュー)
		 * @return ---
		 */
		public TopMenu(E_MenuMaster__c mm_tm){
			this.tm = mm_tm;
			menuList = new List<MenuItem>();
			for(E_MenuMaster__c mm_mi: mm_tm.E_MenuMasters__r){
				MenuItem mi = new MenuItem(mm_mi);
				if(mi.isSelected){				  //選択済メニューがあるときにはカテゴリ側のフラグを設定
					this.hasSelectedMenu = true;
					selectingMenuSet.clear();
				}
				this.menuList.add(mi);
			}
		}
	}
	
	/**
	 * メニューアイテムクラス
	 */
	private class MenuItem{
		//メニューアイテム
		public transient E_MenuMaster__c mi {get; private set;}
		//URL
		public transient String url {get; private set;}
		//選択されているときにTrue
		public transient Boolean isSelected{get; private set;}
		
		/**
		 * コンストラクタ（メニューアイテムクラス）
		 * @param mm_mi: メニューマスタ（メニューアイテム）
		 * @return ---
		 */
		public MenuItem(E_MenuMaster__c mm_mi){
			this.mi = mm_mi;
			
			this.url = mm_mi.Url__c;
			//追加パラメータチェック
			if(String.isNotBlank(mm_mi.AddParameters__c)){ 
				//01：ContactIDが設定されてた場合(key=id)
				if(mm_mi.AddParameters__c.contains('Pram01.')){
					this.url = E_Util.createUrlParam(this.url, 'id', E_AccessController.getInstance().user.ContactId);
				}
				//02：代理店コード（暗号化）が設定されていた場合
				if(mm_mi.AddParameters__c.contains('Pram02.') && E_AccessController.getInstance().isAgent()){
					this.url += E_EncryptUtil.prdCodeConversion(E_AccessController.getInstance().getAccountCode(), true);
				}
				//03:ContactIDが設定されてた場合(key=cid)
				if(mm_mi.AddParameters__c.contains('Pram03.')){
					this.url = E_Util.createUrlParam(this.url, 'cid', E_AccessController.getInstance().user.ContactId);
				}
/*
			//getPramにユーザID付与チェック⇒不要？
			if(mm_mi.IsParam_LoginUserId__c){
				this.url = E_Util.createUrlParam(this.url, 'uid', UserInfo.getUserId());
			}
			//getPramにメニューマスタID付与チェック
			if(mm_mi.IsParam_MenuMasterId__c){
				this.url = E_Util.createUrlParam(this.url, 'id', mm_mi.Id);
			}
*/
			}
			//キャッシュページ対応
			if(this.url.startsWith('E_')){
				this.url = E_Util.createUrlParam(this.url, 'no-cache', system.now().format('HHmmss'));
			}
			//選択状態
			this.isSelected = selectingMenuSet.contains(mm_mi.SelectedMenuKey__c);
		}
	}
	
	private boolean isDisplayMenu (String displayMenu, String targetMenu) {
		List<String> menus = displayMenu.split(',', 0);
		for (String value : menus) {
			if (value.trim().equals(targetMenu)) return true;
		}
		return false;
	}

    public boolean getIsDisplayTubeBanner (){
        try{
            //現在日時がカスタム表示ラベル[NNLinkTubeバナー表示期限]の日時より小さい間はtrue
            if(Datetime.now() < Datetime.valueOf(System.Label.E_TUBE_BANNER_DISP_END_DATETIME)){
                return true;
            }else{
                return false;
            }
        }catch(exception ex){
            //カスタム表示ラベルからの変換例外時
            return false;
        }
    }

//------------------------------------------------------------------------
/*
	//要否確認
	
	public Boolean getIsPopUp(){
		String val = ApexPages.currentPage().getParameters().get('ispopup');
		return val == '1';
	}
	
	 public String getIPadress() {
		return ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
	}
*/
//--------------------------------------------------------------------------
}