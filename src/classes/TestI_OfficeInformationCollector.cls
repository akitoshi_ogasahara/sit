@isTest
private class TestI_OfficeInformationCollector {

	@isTest
	static void shouldSearchTheOfficesThatBelongedToAgency() {
		User agent = [SELECT id, Contact.Name FROM User WHERE lastName = 'first.user'];
		System.runAs(agent) {
			Test.startTest();
			E_AccessController.getInstance().user = new Retriever().getUserWithPermissionsByUserId(UserInfo.getUserId());
			System.assertEquals(([SELECT id FROM Account]).size(), 1);
			I_OfficeInformationCollector collector = new I_OfficeInformationCollector();
			System.assertEquals(collector.getTotalRows(), 3);
			System.assertEquals(collector.results[0].officeName, 'thirdOffice');
			collector.keyword = 'off';
			collector.search();
			System.assertEquals(collector.results.size(), 3);
			collector.keyword = 'ndOf';
			collector.search();
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].officeName, 'secondOffice');
			collector.keyword = null;
			collector.search();
			System.assertEquals(collector.results.size(), 3);
			System.assertEquals(collector.dto, null);
			System.assertEquals(collector.rowCount, 20);
			collector.addRows();
			System.assertEquals(collector.rowCount, 40);
			Test.stopTest();
			collector.getMSG();
			collector.getDISCLAIMER();
		}
	}

	@isTest
	static void shouldSearchTheOfficesByKeyword01() {
		User agent = [SELECT id, Contact.Name FROM User WHERE lastName = 'first.user'];
		System.runAs(agent) {
			Test.startTest();
			E_AccessController.getInstance().user = new Retriever().getUserWithPermissionsByUserId(UserInfo.getUserId());
			System.assertEquals(([SELECT id FROM Account]).size(), 1);
			I_OfficeInformationCollector collector = new I_OfficeInformationCollector();
			System.assertEquals(collector.getTotalRows(), 3);
			collector.keyword = 'ショA';
			collector.search();
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].officeName, 'thirdOffice');
			collector.keyword = 'OF001';
			collector.search();
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].officeName, 'firstOffice');
			Test.stopTest();
		}
	}

	@isTest
	static void shouldSearchTheOfficesByKeyword02() {
		User agent = [SELECT id, Contact.Name FROM User WHERE lastName = 'first.user'];
		System.runAs(agent) {
			Test.startTest();
			E_AccessController.getInstance().user = new Retriever().getUserWithPermissionsByUserId(UserInfo.getUserId());
			I_OfficeInformationCollector collector = new I_OfficeInformationCollector();
			collector.keyword = 'noresult';
			collector.search();
			System.assertEquals(collector.results.size(), 3);
			System.assertEquals(collector.getHasMessages(), true);
			Test.stopTest();
		}
	}

	@isTest
	static void testSortRows() {
		User agent = [SELECT id, Contact.Name FROM User WHERE lastName = 'first.user'];
		System.runAs(agent) {
			Test.startTest();
			E_AccessController.getInstance().user = new Retriever().getUserWithPermissionsByUserId(UserInfo.getUserId());
			System.assertEquals(([SELECT id FROM Account]).size(), 1);
			I_OfficeInformationCollector collector = new I_OfficeInformationCollector();
			System.assertEquals(collector.getTotalRows(), 3);
			System.assertEquals(collector.results[0].officeName, 'thirdOffice');
			ApexPages.currentPage().getParameters().put('st', 'officeName');
			collector.sortRows();
			System.assertEquals(collector.results[0].officeName, 'secondOffice');
			ApexPages.currentPage().getParameters().put('st', 'officeCode');
			collector.sortRows();
			System.assertEquals(collector.results[0].officeName, 'firstOffice');
			ApexPages.currentPage().getParameters().put('st', 'address');
			collector.sortRows();
			System.assertEquals(collector.results[0].officeName, 'secondOffice');
			Test.stopTest();
		}
	}

	@testSetup
	static void setup() {
		Account agency = new Account(Name = 'agency');
		insert agency;

		Account firstOffice = new Account(name = 'firstOffice', E_CL2PF_ZEAYNAM__c = 'firstOffice', parentId = agency.id, E_COMM_VALIDFLAG__c = '1', E_CL2PF_ZEAYKNAM__c = 'ジムショB', E_CL2PF_ZAGCYNUM__c = 'OF001', E_COMM_ZCLADDR__c = '住所2');
		Account secondOffice = new Account(name = 'secondOffice', E_CL2PF_ZEAYNAM__c = 'secondOffice', parentId = agency.id, E_COMM_VALIDFLAG__c = '1', E_CL2PF_ZEAYKNAM__c = 'ジムショC', E_CL2PF_ZAGCYNUM__c = 'OF002', E_COMM_ZCLADDR__c = '住所1');
		Account thirdOffice = new Account(name = 'thirdOffice', E_CL2PF_ZEAYNAM__c = 'thirdOffice', parentId = agency.id, E_COMM_VALIDFLAG__c = '1', E_CL2PF_ZEAYKNAM__c = 'ジムショA', E_CL2PF_ZAGCYNUM__c = 'OF003', E_COMM_ZCLADDR__c = '住所3');
		insert new List<Account>{
			firstOffice,
			secondOffice,
			thirdOffice
		};

		Contact firstAgent = new Contact(LastName = 'firstAgent', AccountId = firstOffice.Id);
		Contact secondAgent = new Contact(LastName = 'secondAgent', AccountId = secondOffice.Id);
		insert new List<Contact>{
			firstAgent,
			secondAgent
		};

		User firstUser = TestI_TestUtil.createAgentUser(true, 'first.user', 'E_PartnerCommunity', firstAgent.id);
		TestI_TestUtil.createIDCPF(true, firstUser.Id, null);

		User secondUser = TestI_TestUtil.createAgentUser(true, 'second.user', 'E_PartnerCommunity', secondAgent.id);
		TestI_TestUtil.createIDCPF(true, secondUser.Id, null);

		User myself = [SELECT id FROM User WHERE id =: UserInfo.getUserId()];
		System.runAs(myself) {
			TestI_TestUtil.createBasePermissions(firstUser.Id);
			TestI_TestUtil.createBasePermissions(secondUser.Id);
		}
	}

	private without sharing class Retriever {
		/**
		 * E_UserDaoを経由して取引先オブジェクトを参照できなかったため、直接取得する。
		 * 参照できるようになったら削除する。2018/02/16
		 */
		public User getUserWithPermissionsByUserId(Id userId){
			return [
				SELECT
					AccountId,
					ContactId,
					UserType,
					Email,
					Username,
					Department,
					UserRole.DeveloperName,
					E_UseTempBasicPW__c,
					E_UseExistingBasicPW__c,
					E_ZWEBID__c,
					/* LastPasswordChangeDate, */
					Unit__c,
					MRCD__c,
					Contact.Id,
					Contact.Name,
					Contact.LastName,
					Contact.FirstName,
					Contact.E_CLTPF_CLNTNUM__c,
					Contact.E_CLTPF_ZCLKNAME__c,
					Contact.E_COMM_ZCLADDR__c,
					Contact.E_CLTPF_ZCLKADDR__c,
					Contact.E_CLTPF_CLTPHONE01__c,
					Contact.E_CLTPF_CLTPHONE02__c,
					Contact.E_CLTPF_FAXNO__c,
					Contact.E_CADPF__r.Name,
					Contact.E_AccParentCord__c,
					Contact.Account.ParentId,
					Contact.Account.E_DoCheckIP__c,	//20171106追加 銀行代理店解放
					Contact.Account.CannotBeOrder__c,	//20180214 資料発送申込対応
					Contact.AccountId,
					Contact.E_MenuKindId__c,
					Contact.Account.E_CL2PF_ZAGCYNUM__c,
					Contact.E_AccZSHRMAIN__c,
					Contact.E_AccParentName__c,
					Contact.E_Account__c,
					Contact.E_KCHANNEL__c,
					Contact.AgentNum5__c

					//付与された権限セット
					,(Select Id
							, PermissionSetId
							, PermissionSet.Name
							, AssigneeId
					  From PermissionSetAssignments
					  Where PermissionSet.ProfileId = null		//ProfileIdがNullのものが権限セット
						//AND PermissionSet.Name in :E_Const.PERMISSIONSET_NAMES		//NNLink以外もすべて取得
					)
				FROM User
				WHERE Id = :userId
			];
		}
	}
}