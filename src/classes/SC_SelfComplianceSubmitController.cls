global with sharing class SC_SelfComplianceSubmitController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public SC_SelfCompliance__c record {get{return (SC_SelfCompliance__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public String recordTypeRecordsJSON_SC_SelfCompliance_c {get; private set;}
	public String defaultRecordTypeId_SC_SelfCompliance_c {get; private set;}
	public String metadataJSON_SC_SelfCompliance_c {get; private set;}
	public String picklistValuesJSON_SC_SelfCompliance_c_Type_c {get; private set;}
	public String checkList_hidden { get; set; }
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public SC_SelfComplianceSubmitExtender getExtender() {return (SC_SelfComplianceSubmitExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public SC_SelfComplianceSubmitController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','upload_selfcompliance');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','点検表はこちら');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.UrlforGuide}');
        tmpPropMap.put('p_target','_nnlinkguide');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1920');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1920',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','操作説明はこちら');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.UrlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','19');
		tmpPropMap.put('Component__id','Component1894');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1894',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1888');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1888',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_newFile','{!extender.file}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','30');
		tmpPropMap.put('Component__id','Component1889');
		tmpPropMap.put('Component__Name','EUploadAttachment');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1889',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','14');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = SC_SelfCompliance__c.fields.Type__c;
		f = SC_SelfCompliance__c.fields.OtherTypeName__c;

		List<RecordTypeInfo> recordTypes;
		FilterMetadataResult filterResult;
		List<RecordType> recordTypeRecords_SC_SelfCompliance_c = [SELECT Id, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType = 'SC_SelfCompliance__c'];
		Map<Id, RecordType> recordTypeMap_SC_SelfCompliance_c = new Map<Id, RecordType>(recordTypeRecords_SC_SelfCompliance_c);
		List<RecordType> availableRecordTypes_SC_SelfCompliance_c = new List<RecordType>();
		recordTypes = SObjectType.SC_SelfCompliance__c.getRecordTypeInfos();

		for (RecordTypeInfo t: recordTypes) {
			if (t.isDefaultRecordTypeMapping()) {
				defaultRecordTypeId_SC_SelfCompliance_c = t.getRecordTypeId();
			}
			if (t.isAvailable()) {
				RecordType rtype = recordTypeMap_SC_SelfCompliance_c.get(t.getRecordTypeId());
				if (rtype != null) {
					availableRecordTypes_SC_SelfCompliance_c.add(rtype);
				}
			}
		}
		recordTypeRecordsJSON_SC_SelfCompliance_c = System.JSON.serialize(availableRecordTypes_SC_SelfCompliance_c);
		filterResult = filterMetadataJSON(
			System.JSON.deserializeUntyped('{"CustomObject":{"recordTypes":[{"fullName":"FY2016","picklistValues":[{"picklist":"Type__c","values":[{"fullName":"1%2E代理店点検表","default":false},{"fullName":"2%2E継続教育研修の実施状況が確認できる資料%EF%BC%88例%EF%BC%9A研修実施の通達%E3%80%81研修受講記録%E3%83%BB履修簿%EF%BC%89","default":false},{"fullName":"3%2E法令等遵守事項に関する教育研修の実施状況が確認できる資料%EF%BC%88例%EF%BC%9A研修実施の通達%E3%80%81研修受講記録%E3%83%BB履修簿%EF%BC%89","default":false},{"fullName":"4%2E苦情報告体制%E3%83%BB方法等に関する社内の周知%E3%83%BB徹底状況が確認できる資料%EF%BC%88例%EF%BC%9Aお客様の声受付簿のブランクフォーム%E3%80%81苦情研修受講記録%EF%BC%89","default":false},{"fullName":"5%2E個人データ取扱等に関する従業者の役割%E3%83%BB責任及び違反時の懲戒処分を示した資料%EF%BC%88例%EF%BC%9A就業規則%E3%80%81誓約書%E3%83%BB念書のブランクフォーム%EF%BC%89","default":false},{"fullName":"6%2E個人データの保存%E3%80%81持出%E3%80%81廃棄等の管理状況が確認できる資料%EF%BC%88例%EF%BC%9A個人データ管理台帳のブランクフォーム%E3%80%81個人情報持出管理台帳のブランクフォーム%EF%BC%89","default":false},{"fullName":"7%2Eフランチャイジーに関する管理%E3%83%BB指導の状況が確認できる資料%EF%BC%88例%EF%BC%9Aフランチャイジーに対する監査結果%EF%BC%89","default":false},{"fullName":"8%2E特定関係法人の一覧に関する代理店内の周知%E3%83%BB徹底状況が確認できる資料%EF%BC%88例%EF%BC%9A社内保管ファイル%E3%80%81社内通達%E3%80%81社内イントラネット画面%EF%BC%89","default":false},{"fullName":"9%2Eその他","default":false}]}]},{"fullName":"FY2017","picklistValues":[{"picklist":"Type__c","values":[{"fullName":"0%2E代理店点検表","default":false},{"fullName":"1%2E法令等遵守事項の教育等実施状況が確認できる資料%EF%BC%88例%EF%BC%9A研修受講記録%E3%80%81履修簿%E3%80%81研修実施の通達%EF%BC%89","default":false},{"fullName":"2%2E意向把握に係るプロセス等の運用状況が確認できる資料%EF%BC%88例%EF%BC%9A意向把握アンケートのブランクフォーム%E3%80%81意向把握システム入力画面のプリントアウト%EF%BC%89","default":false},{"fullName":"3%2E苦情等対応体制に関する周知%E3%83%BB徹底状況が確認できる資料%EF%BC%88例%EF%BC%9Aお客様の声受付簿のブランクフォーム%E3%80%81苦情研修受講記録%EF%BC%89","default":false},{"fullName":"4%2E個人データの保存%E3%80%81持出%E3%80%81廃棄等の管理状況が確認できる資料%EF%BC%88例%EF%BC%9A個人データ管理台帳のブランクフォーム%E3%80%81個人情報持出管理簿のブランクフォーム%EF%BC%89","default":false},{"fullName":"5%2Eフランチャイジーの一覧表%E3%80%80%E2%80%BBフランチャイザーである場合に限りご提出下さい","default":false},{"fullName":"6%2Eその他","default":false}]}]}]}}'),
			recordTypeFullNames(availableRecordTypes_SC_SelfCompliance_c),
			SC_SelfCompliance__c.SObjectType
		);
		metadataJSON_SC_SelfCompliance_c = System.JSON.serialize(filterResult.data);
		picklistValuesJSON_SC_SelfCompliance_c_Type_c = System.JSON.serialize(filterPricklistEntries(SC_SelfCompliance__c.SObjectType.Type__c.getDescribe(), filterResult));
		try {
			mainSObjectType = SC_SelfCompliance__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('SC_SelfCompliance__c');
			mainQuery.addField('Type__c');
			mainQuery.addField('OtherTypeName__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('SC_Office__c');
			mainQuery.addFieldAsOutput('SC_Office__r.Status__c');
			mainQuery.addFieldAsOutput('SC_Office__r.YearDisclamer__c');
			mainQuery.addFieldAsOutput('SC_Office__r.YearRecType__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new SC_SelfComplianceSubmitExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	public String getcheckListOptionsJS() {
		return SkyEditor2.JSON.stringify(SkyEditor2.PicklistValueSelector.getEntries(
		SC_SelfCompliance__c.getSObjectType(),
		SObjectType.SC_SelfCompliance__c.fields.Type__c.getSObjectField()
		));
		}
	@TestVisible		static Set<String> recordTypeFullNames(RecordType[] records) {
		Set<String> result = new Set<String>();
		for (RecordType r : records) {
			result.add(r.DeveloperName);
			if (r.NamespacePrefix != null) {
				result.add(r.NamespacePrefix + '__' + r.DeveloperName);
			}
		}
		return result;
	}
	
	@TestVisible		static FilterMetadataResult filterMetadataJSON(Object metadata, Set<String> recordTypeFullNames, SObjectType soType) {
		Map<String, Object> metadataMap = (Map<String, Object>) metadata;
		Map<String, Object> customObject = (Map<String, Object>) metadataMap.get('CustomObject');
		List<Object> recordTypes = (List<Object>) customObject.get('recordTypes');
		Map<String, Set<String>> availableEntries = new Map<String, Set<String>>();
		for (Integer i = recordTypes.size() - 1; i >= 0; i--) {
			Map<String, Object> recordType = (Map<String, Object>)recordTypes[i];
			String fullName = (String)recordType.get('fullName');
			if (! recordTypeFullNames.contains(fullName)) {
				recordTypes.remove(i);
			} else {
				addAll(availableEntries, getOutEntries(recordType, soType));
			}
		}	
		return new FilterMetadataResult(metadataMap, availableEntries, recordTypes.size() == 0);
	}
	public class FilterMetadataResult {
		public Map<String, Object> data {get; private set;}
		public Map<String, Set<String>> availableEntries {get; private set;}
		public Boolean master {get; private set;}
		public FilterMetadataResult(Map<String, Object> data, Map<String, Set<String>> availableEntries, Boolean master) {
			this.data = data;
			this.availableEntries = availableEntries;
			this.master = master;
		}
	}
	
	static void addAll(Map<String, Set<String>> toMap, Map<String, Set<String>> fromMap) {
		for (String key : fromMap.keySet()) {
			Set<String> fromSet = fromMap.get(key);
			Set<String> toSet = toMap.get(key);
			if (toSet == null) {
				toSet = new Set<String>();
				toMap.put(key, toSet);
			}
			toSet.addAll(fromSet);
		}
	}

	static Map<String, Set<String>> getOutEntries(Map<String, Object> recordType, SObjectType soType) {
		Map<String, Set<String>> result = new Map<String, Set<String>>();
		List<Object> entries = (List<Object>)recordType.get('picklistValues');
		Map<String, SObjectField> fields = soType.getDescribe().fields.getMap();
		for (Object e : entries) {
			Map<String, Object> entry = (Map<String, Object>) e;
			String picklist = (String) entry.get('picklist');
			SObjectField f = fields.get(picklist);
			List<Object> values = (List<Object>)(entry.get('values'));
			if (f != null && f.getDescribe().isAccessible()) {
				Set<String> entrySet = new Set<String>();
				for (Object v : values) {
					Map<String, Object> value = (Map<String, Object>) v;
					entrySet.add(EncodingUtil.urlDecode((String)value.get('fullName'), 'utf-8'));
				}
				result.put(picklist, entrySet);
			} else { 
				values.clear(); 
			}
		}
		return result;
	}
	
	static List<PicklistEntry> filterPricklistEntries(DescribeFieldResult f, FilterMetadataResult parseResult) {
		List<PicklistEntry> all = f.getPicklistValues();
		if (parseResult.master) {
			return all;
		}
		Set<String> availables = parseResult.availableEntries.get(f.getName());
		List<PicklistEntry> result = new List<PicklistEntry>();
		if(availables == null) return result;
		for (PicklistEntry e : all) {
			if (e.isActive() && availables.contains(e.getValue())) {
				result.add(e);
			}
		}
		return result;
	}
	
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}