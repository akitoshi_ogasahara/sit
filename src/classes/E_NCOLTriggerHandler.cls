public with sharing class E_NCOLTriggerHandler {
	public void onBeforeInsert (List<E_NCOL__c> newList){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList);
	}
	public void onBeforeUpdate (List<E_NCOL__c> newList,Map<Id,E_NCOL__c> oldMap){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList,oldMap);
	}
}