/*
 * CC_CaseEditVOIControllerTest
 * Test class of CC_CaseEditVOIController
 * created  : Accenture 2018/10/19
 * modified :
 */

@isTest
private class CC_CaseEditVOIControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Test invokeMethod()
	* methodName is initCase
	*/
	static testMethod void invokeMethodTest01() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'initCase';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

	/**
	* Test invokeMethod()
	* methodName is extractSRPolicy
	*/
	static testMethod void invokeMethodTest02() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'extractSRPolicy';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

	/**
	* Test invokeMethod()
	* methodName is saveCase
	*/
	static testMethod void invokeMethodTest03() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'saveCase';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

	/**
	* Test invokeMethod()
	* methodName is SRFormPrint
	*/
	static testMethod void invokeMethodTest04() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'SRFormPrint';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

	/**
	* Test invokeMethod()
	* methodName is saveCaseConfirmed
	*/
	static testMethod void invokeMethodTest05() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'saveCaseConfirmed';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

	/**
	* Test invokeMethod()
	* methodName is SRFormPrintConfirmed
	*/
	static testMethod void invokeMethodTest06() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'SRFormPrintConfirmed';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

	/**
	* Test invokeMethod()
	* methodName is getAgencyTypeAhead
	*/
	static testMethod void invokeMethodTest07() {
		CC_CaseEditVOIController caseEditVOICtl = new CC_CaseEditVOIController();
		System.runAs ( testUser ) {
			//Prepare test data
			String methodName = 'getAgencyTypeAhead';

			Test.startTest();
			caseEditVOICtl.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
			Test.stopTest();
		}
	}

}