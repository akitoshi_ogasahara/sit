/**
 * 新契約申込進捗結果(bank)コントローラクラス
 * CreatedDate 2014/12/15
 * @Author Terrasky
 */
public with sharing class E_ProgressSearchBankController extends E_AbstractController{
	
	public E_ProgressSearchBankController() {
		pgTitle = '新契約申込進捗結果';
	}
}