@isTest
private class TestASS_MRSummaryController {
	//テストデータ作成
	//営業部作成
	private static List<E_Unit__c> createunits() {
		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		List<E_Unit__c> units = new List<E_Unit__c>();
		units.add(new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト2営業部', BRANCH__c = '01', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト3営業部', BRANCH__c = '02', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト4営業部', BRANCH__c = '03', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト5営業部', BRANCH__c = '04', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト6営業部', BRANCH__c = '05', IsActive__c = '1', Area__c = area.Id));
		insert units;

		return units;
	}
	//MR作成
	private static List<User> createmrs() {
		//MRユーザ作成
		List<User> mrs = new List<User>();
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'ＭＲ'];
		mrs.add(new User(LastName = 'mr1', UserName = 'mr1@terrasky.ingtesting', Email = 'mr1@terrasky.ingtesting', ProfileId = prof.Id, IsActive = true, Unit__c = 'テスト1営業部'
			, Alias = 'mr1', TimeZoneSidKey = UserInfo.getTimeZone().getID(), LocaleSidKey = UserInfo.getLocale(), EmailEncodingKey = 'UTF-8', LanguageLocaleKey = UserInfo.getLanguage()));
		mrs.add(new User(LastName = 'mr2', UserName = 'mr2@terrasky.ingtesting', Email = 'mr2@terrasky.ingtesting', ProfileId = prof.Id, IsActive = true, Unit__c = 'テスト1営業部'
			, Alias = 'mr2', TimeZoneSidKey = UserInfo.getTimeZone().getID(), LocaleSidKey = UserInfo.getLocale(), EmailEncodingKey = 'UTF-8', LanguageLocaleKey = UserInfo.getLanguage()));
		mrs.add(new User(LastName = 'mr3', UserName = 'mr3@terrasky.ingtesting', Email = 'mr3@terrasky.ingtesting', ProfileId = prof.Id, IsActive = true, Unit__c = 'テスト1営業部'
			, Alias = 'mr3', TimeZoneSidKey = UserInfo.getTimeZone().getID(), LocaleSidKey = UserInfo.getLocale(), EmailEncodingKey = 'UTF-8', LanguageLocaleKey = UserInfo.getLanguage()));
		mrs.add(new User(LastName = 'mr4', UserName = 'mr4@terrasky.ingtesting', Email = 'mr4@terrasky.ingtesting', ProfileId = prof.Id, IsActive = true, Unit__c = 'テスト1営業部'
			, Alias = 'mr4', TimeZoneSidKey = UserInfo.getTimeZone().getID(), LocaleSidKey = UserInfo.getLocale(), EmailEncodingKey = 'UTF-8', LanguageLocaleKey = UserInfo.getLanguage()));
		mrs.add(new User(LastName = 'mr5', UserName = 'mr5@terrasky.ingtesting', Email = 'mr5@terrasky.ingtesting', ProfileId = prof.Id, IsActive = true, Unit__c = 'テスト1営業部'
			, Alias = 'mr5', TimeZoneSidKey = UserInfo.getTimeZone().getID(), LocaleSidKey = UserInfo.getLocale(), EmailEncodingKey = 'UTF-8', LanguageLocaleKey = UserInfo.getLanguage()));
		insert mrs;

		return mrs;
	}
	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（営業部一覧から）
	 * エリア部長の場合の画面と画面遷移
	 */
	private static testMethod void areaManagerTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//営業部作成
		List<E_Unit__c> units = createunits();

		//MRユーザ作成
		List<User> mrs = createmrs();

		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);
		pr.getParameters().put('unitId',units[0].Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRSummaryController con = new ASS_MRSummaryController();
			con.getMRList();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.getMRList().size(),5);
		}
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（営業部一覧から）
	 * エリア部長の戻るボタン処理
	 */
	private static testMethod void areaManagerRetPageTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//営業部作成
		List<E_Unit__c> units = createunits();

		//MRユーザ作成
		List<User> mrs = createmrs();



		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);
		pr.getParameters().put('unitId',units[0].Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRSummaryController con = new ASS_MRSummaryController();
			con.getMRList();
			con.back();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_UnitSummary;
			System.assertEquals(con.back().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（営業部一覧から）
	 * MRの場合の画面表示
	 */
	private static testMethod void mrTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'actUser', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//営業部作成
		List<E_Unit__c> units = createunits();

		//MRユーザ作成
		List<User> mrs = createmrs();



		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);
		//pr.getParameters().put('unitId',units[0].Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRSummaryController con = new ASS_MRSummaryController();
			con.getMRList();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.getMRList().size(),6);
			System.assertEquals(con.getMRList()[0].Name,'actUser');
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（営業部一覧から）
	 * MRの場合の画面遷移
	 */
	private static testMethod void mrMoveToPageTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'actUser', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//営業部作成
		List<E_Unit__c> units = createunits();

		//MRユーザ作成
		List<User> mrs = createmrs();



		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);
		//pr.getParameters().put('unitId',units[0].Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRSummaryController con = new ASS_MRSummaryController();
			pr.getParameters().put('mrId',mrs[0].Id);
			pr.getParameters().put('unitId',units[0].Id);
			con.getMRList();
			con.moveToMRPage();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_MRPage;
			newPage.getParameters().put('mrId',mrs[0].Id);
			newPage.setRedirect(true);
			System.assertEquals(con.moveToMRPage().getUrl(),newPage.getUrl());
		}
	}
}