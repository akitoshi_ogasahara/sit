public with sharing class I_PolicyUtil {
	
	/**
	 * 郵便番号と住所を結合
	 */
    public static String getAddress(String postalCode, String address){
        String joinAddress = '';
        // 郵便番号
        if(String.isNotBlank(postalCode)){
            joinAddress += E_Util.postNoFormat(postalCode) + ' ';
        }
        // 住所
        if(String.isNotBlank(address)){
        	joinAddress += address;
        }
        
        return joinAddress;
    }
    
	/**
	 * 値が「00000000」「99999999」か判定
	 */
    public static Boolean isVal09(String s) {
        Boolean isVal09 = false;
        if(s == '00000000' || s == '99999999'){
            isVal09 = true;
        }else{
            isVal09 = false;
        }
        return isVal09;
    }

	/**
	 * 解約返戻金取得メソッド
	 */
	public static Decimal getCancellationRefund(E_Policy__c pol){
		//解約返戻金
		Decimal cancelref = 0;
		//保険料振替貸付合計
		Decimal policyTransferLoan = 0;
		//契約者貸付合計
		Decimal contractorLoan = 0;
		//前納未経過保険料
		Decimal prePolicyLoan = 0;
		//未経過保険料
		Decimal unearnedPremiums = 0;

		if(pol.COLI_ZCSHVAL__c != null) cancelref = pol.COLI_ZCSHVAL__c;
		if(pol.COLI_ZEAPLTOT__c != null) policyTransferLoan = pol.COLI_ZEAPLTOT__c;
		if(pol.COLI_ZEPLTOT__c != null) contractorLoan = pol.COLI_ZEPLTOT__c;
		if(pol.COLI_ZEADVPRM__c != null) prePolicyLoan = pol.COLI_ZEADVPRM__c;
		if(pol.COLI_ZUNPREM__c != null) unearnedPremiums = pol.COLI_ZUNPREM__c;

		Decimal cancelrefSum = 0;
		//解約返戻金計算不能ﾌﾗｸまたはﾞ未経過保険料計算不能ﾌﾗｸﾞがtrueのとき
		if(pol.COLI_ZCVDCF__c == true || pol.COLI_ZUNPCF__c == true){
			return -1;
		//未経過保険料計算不能ﾌﾗｸﾞがfalseかつ未経過保険料表示ﾌﾗｸﾞがfalseのとき
		}else if(pol.COLI_ZUNPCF__c == false && pol.COLI_ZUNPDCF__c == false){
							//解約返戻金 - (保険料振替貸付合計 + 契約者貸付合計) + 前納未経過保険料
			cancelrefSum = cancelref - (policyTransferLoan + contractorLoan) + prePolicyLoan;
		}else{
							//解約返戻金 - (保険料振替貸付合計 + 契約者貸付合計) + 前納未経過保険料 + 未経過保険料
			cancelrefSum = cancelref - (policyTransferLoan + contractorLoan) + prePolicyLoan + unearnedPremiums;
		}
		return cancelrefSum;
	}
	
/* E_ColiExtenderのロジックより抽出 ********************************************************************************* */

	private static final String CRTABLE_TT = 'TT'; // 特約の保険種類コードTT

	/**
	 * 『保険金額/入院給付金日額/年金（月）額 :現在』の取得
	 */
	public static String getGenzaiHoken(E_COVPF__c covpf){
		String genzaiHoken = '';
		// 【特約】.[現在Ｓ計算不能不能フラグ]がTrue、且つ、
		//       保険種類コードがTT（重大疾病保障保険）以外の場合  [P15-0020_TermXにて追加]
		if((covpf.COLI_ZSUMINF__c) && (covpf.COMM_CRTABLE2__c != CRTABLE_TT)){
			genzaiHoken = '?';
		}else{
			// 【特約】.[主契約現在保険金額]
			if(covpf.COMM_SUMINS__c != null){
				genzaiHoken = covpf.COMM_SUMINS__c.format();
			}
		}
		
		return genzaiHoken;
	}
	
	/**
	 * P免及びP逓減フラグがTrueの時、保険種類に文言を付与する
	 */
	public static String addZCOVRNAM(String s, Boolean DPIND, Boolean ZDCPFLG){
		if(DPIND){
			s += '<br>（保険料払込免除特則付加あり）';
		}
		if(ZDCPFLG){
			s += '<br>（保険料逓減払込特則付加あり）';
		}
		return s;
	}
	
	/**
	 * 保険顧客（ LA:(主たる)被保険者 ）の『加入時年齢』を取得します。
	 */
	public static Decimal getInsuredANBCCD(Id policyId){
		Decimal anbccd;
		E_CRLPF__c rec = E_CRLPFDao.getRecByType(policyId, 'LA');
		if(rec != null && rec.ANBCCD__c != null){
			anbccd = Math.abs(rec.ANBCCD__c);
		}
		return anbccd;
	}
	
	/*
	 * ＜解約時受取金額の計算＞
	 * 【保険契約ヘッダ】の対象項目において下記計算を行う
	 * （１）解約返戻金計算不能フラグ==true または 未経過保険料計算不能フラグ==true のとき
	 *  ?を返す。
	 * （２）解約返戻金計算不能フラグ==false かつ 未経過保険料計算不能フラグ==false のとき
	 *  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料
	 * （３）上記以外の時、
	 *  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料＋未経過保険料
	 */
	public static Decimal getSurrenderValueSum(E_Policy__c policy){

		// 解約返戻金
		Decimal Surrender1 = policy.COLI_ZCSHVAL__c;
		// 保険料振替貸付合計
		Decimal Surrender2 = policy.COLI_ZEAPLTOT__c;
		// 契約者貸付合計
		Decimal Surrender3 = policy.COLI_ZEPLTOT__c;
		// 前納未経過保険料
		Decimal Surrender4 = policy.COLI_ZEADVPRM__c;
		// 未経過保険料 (未経過期間に対応する額)
		Decimal Surrender5 = policy.COLI_ZUNPREM__c;

		//　解約返戻金計算不能フラグ
		Boolean flg1 = policy.COLI_ZCVDCF__c;
		//　未経過保険料計算不能フラグ
		Boolean flg2 = policy.COLI_ZUNPCF__c;
		//　未経過保険料表示フラグ
		Boolean flg3 = policy.COLI_ZUNPDCF__c;

		//合計値
		Decimal SurrenderSum = 0;

		// （２）未経過表示フラグ==false かつ 未経過保険料計算不能フラグ==false のとき
		//  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料
		// （３）それ以外の時、
		//  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料＋未経過保険料

		if(Surrender1 != null && Surrender1 != 0){
			// 解約返戻金 (COLI_ZCSHVAL__c)
			SurrenderSum += Surrender1;
		}
		if(Surrender2 != null && Surrender2 != 0){
			// 貸付金等精算額 (COLI_ZLOANTOT__c)
			// マイナス
			SurrenderSum -= Surrender2;
		}
		if(Surrender3 != null && Surrender3 != 0){
			// 契約者貸付合計 (COLI_ZEPLTOT__c)
			// マイナス
			SurrenderSum -= Surrender3;
		}
		if(Surrender4 != null && Surrender4 != 0){
			// 前納未経過保険料 (COLI_ZEADVPRM__c)
			SurrenderSum += Surrender4;
		}
		if(Surrender5 != null && Surrender5 != 0){
			// 未経過保険料 (COLI_ZUNPREM__c)
			SurrenderSum += Surrender5;
		}

		if((!flg3) && (!flg2)){
			if(Surrender5 != null && Surrender5 != 0){
				// 未経過保険料 (COLI_ZUNPREM__c)
				SurrenderSum -= Surrender5;
			}
		}
		return SurrenderSum;
	}
	
	/*
	 * ＜団体顧客番号より団体名称を取得＞
	 * 【保険契約ヘッダ】[団体顧客番号(COLI_ZGCLTNM__c)]==【Contact】[顧客番号]の時
	 */
	public static String getCorporationName(String customerNumber){
		//customerNumber = (String)extension.record.get('COLI_ZGCLTNM__c'); //団体顧客番号
		if(!(String.isBlank(customerNumber))){
			// 【個人保険ヘッダ】[団体顧客番号]==【Contact】[顧客番号] となるContactレコードを取得
			Contact corp = E_ContactDaoWithout.getRecByCustomerNumber(CustomerNumber);
			if(corp!=null){
				return corp.Name;
			}else{
				// ContactレコードのNameがnuｌｌの時
				return '';
			}
		}else{
			// 【個人契約ヘッダ】[団体顧客番号]がnuｌｌの時
			return '';
		}
	}
	
	/**
	 * 失効契約判定
	 */
	public static Boolean getIsDisablePolicy(E_Policy__c policy){
		return  (policy.COMM_STATCODE__c == I_Const.LAPS_POLICY_STATCODE
			  && policy.COMM_ZRSTDESC__c == I_Const.LAPS_POLICY_ZRSTDESC);
	}
	
	/**
	 * 口座情報表示判定
	 * 		払込経路（経路） == '口座振替' かつ
	 * 		現在の状況コード != 'Y' かつ
	 * 		次回保険料払込期月フラグ == true かつ
	 * 		口座振替表示フラグ == true かつ
	 * 		(口座振替区分 '10' or '20')
	 */
	public static Boolean getIsViewBank(E_Policy__c policy){
		if(policy.COMM_ZECHNL__c == I_Const.POLICY_ZECHNL_ACCOUNTTRAN		// 払込経路（経路）
				&& policy.COMM_STATCODE__c != I_Const.POLICY_STATCODE_Y		// 現在の状況コード
				&& policy.COLI_ZNPTDDCF__c == true							// 次回保険料払込期月フラグ
				&& policy.COLI_ZBKTRDCF__c == true	 						// 口座振替表示フラグ
				&& (policy.COLI_ZBKPODIS__c == I_Const.POLICY_ZBKPODIS_10	//　口座振替区分
						|| policy.COLI_ZBKPODIS__c == I_Const.POLICY_ZBKPODIS_20)){ 						
			return true;
		}else{
			return false;
		}
	}
}