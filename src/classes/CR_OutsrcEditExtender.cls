global with sharing class CR_OutsrcEditExtender extends CR_AbstractExtender{
    
    private CR_OutsrcEditController controller;
    
    /** 
     * Constructor
     */
    public CR_OutsrcEditExtender(CR_OutsrcEditController extension){
        super();
        this.controller = extension;
        this.helpMenukey = CR_Const.MENU_HELP_OUTSRC;
        this.helpPageJumpTo = CR_Const.MENU_HELP_OUTSRC_Edit;
    }
    
    /** 
     * init
     */
    global override void init() {
        isSuccessOfInitValidate = false;
        if(getHasOutsrcViewPermission()==false){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG05_REQUIRED_PERMISSION_OUTSRC);        //'外部委託参照権限がありません'
        }else if(String.isBlank(ApexPages.currentPage().getParameters().get('parentId'))  
                && String.isBlank(controller.record.Id)){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            this.viewingAgencyCode = this.controller.record.CR_Agency__r.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }
    }

    public Boolean getIsNew(){
        return controller.record.Id == null;
    }

    //外部委託　参照
    public String getUrlforCancel(){
        PageReference pr;
        if(controller.record.Id!=null){
            pr = Page.E_CROutSrcView;
            pr.getParameters().put('id', controller.record.Id);
        }else{
            pr = Page.E_CROutSrcs;
            pr.getParameters().put('id', ApexPages.currentPage().getParameters().get('parentId'));
        }
        return pr.getUrl();
    }
    
    /** 
     * 保存処理
     */
    public PageReference doSaveEx(){
        pageMessages.clearMessages();
        if(controller.record.Id == null){
            controller.record.CR_Agency__c = ApexPages.currentPage().getParameters().get('parentId');
            controller.record.Status__c = CR_Const.OUTSRC_STATUS_01;
        }

        Boolean saveResult = E_GenericDao.saveViaPage(controller.record);

        //保存成功時は参照ページへ
        if(saveResult){
            PageReference pr = Page.E_CROutsrcView;
            pr.getParameters().put('id', controller.record.Id);
            return pr;
        }

        return null;
    }
}