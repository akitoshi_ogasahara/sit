public with sharing class E_IssJudgeController {

	//private Map<String, String> typeAndDomain = new Map<String, String>();
	
	/*
	//E_IssReferenceControllerに処理を行うため、コメントアウト
	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}
	
	*/
	
	/*
	//E_IssReferenceControllerに処理を行うため、コメントアウト
	public E_IssJudgeController() {
		typeAndDomain.put(E_Const.USERTYPE_STANDARD, System.Label.E_DOMAIN_EMPLOYEE);
		typeAndDomain.put(E_Const.USERTYPE_POWERPARTNER, System.Label.E_DOMAIN_AGENT);
	}
	
	*/
	
    public PageReference init() {
        
        // 証券番号
        String ISS_SHOUKENNO = ApexPages.currentPage().getParameters().get('RENKEI_SHOUBAN');
        // SAML統合ID
        String UID = ApexPages.currentPage().getHeaders().get('PARAM022');
        
        /*
        //E_IssReferenceControllerに処理を行うため、コメントアウト
		//パフォーマンス対応
        // SAML統合IDがnullであるとき、認証情報が不正
        if (String.isEmpty(UID)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|001'),''));
            return null;
        }
        
        if (String.isEmpty(ISS_SHOUKENNO)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|001'),''));
            return null;
        }
        
        
        // ユーザオブジェクトのID,名前,取引先責任者ID,プロファイルID,SAML統合ID
        User user = E_UserDaoWithout.getUserRecByFederationIdentifier(UID);
        
        // ユーザが存在するとき
        if (user != null) {
			if (user.usertype.equals(E_Const.USERTYPE_CSPLITEPORTAL)) {
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|002'),''));
            	return null;
	        }
            // 保険契約ヘッダを取得
            E_Policy__c policy = E_PolicyDaoWithout.getRecByIdCHDRNUM(ISS_SHOUKENNO);
            
            if (policy != null) {
            	//SPVAヘッダDB「年金支払開始日到来フラグ(ZANNSTFLG)」＝”１”（年金支払開始日以降）の時、
      			//SPVAヘッダDB「抽出対象判定フラグ(契約者) (ZEXTFLG01)」＝”0”かつ「抽出対象判定フラグ(被保険者) (ZEXTFLG02)」＝”0”（年金受取人が契約者とも被保険者とも違う）の時
           		if (policy.RecordType.DeveloperName.equals(E_Const.POLICY_RECORDTYPE_SPVA)) {
           			if (policy.SPVA_ZANNSTFLG__c && (!policy.SPVA_ZEXTFLG01__c && !policy.SPVA_ZEXTFLG02__c)) {
	           			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|005'),''));
	           			return null;
           			}
           		}
                // 遷移先URL用
                //String url = typeAndDomain.get(user.usertype);
            	String url = typeAndDomain.get(E_Const.USERTYPE_POWERPARTNER);
                
                String SPkind = policy.SpClassification__c;
                Set<String> spKinds = new Set<String>{E_Const.SP_CLASS_SPVA, E_Const.SP_CLASS_SPVWL, E_Const.SP_CLASS_SPVAANNUITY};

                if(SPkind == null || SPkind == ''){
                    url += Page.E_Coli.getUrl();
                } else if (spKinds.contains(SPkind)) {
                    url += Page.E_Spva.getUrl();
                } else if (E_Const.SP_CLASS_ANNUITY.equals(SPkind)) {
                    url += Page.E_Annuity.getUrl();
                }
                
                // パラメータセットして遷移
                PageReference pageRef = new PageReference(url);
                pageRef.getParameters().put('id', policy.id);
                pageRef.getParameters().put('iss', '1');
                pageRef.setRedirect(true);
                return pageRef;
                
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|003'),''));
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|004'),''));
        }
        
        */
        PageReference pageRef = new PageReference(System.Label.E_DOMAIN_AGENT);
        pageRef.getParameters().put('RENKEI_SHOUBAN', ISS_SHOUKENNO);
        pageRef.getParameters().put('iss', '1');
        pageRef.setRedirect(true);
        return pageRef;
    }
}