@isTest(SeeAllData=false)
private class AGC_ContactHistory_Ctrl_Test{
    
    private static testMethod void testRun(){

        AGC_TestCommon common = new AGC_TestCommon();
        AGC_DairitenKihonJoho__c kihon;
        AGC_DairitenJimushoHosokuJoho__c hosoku;
        AGC_ShishaMr_Mst__c tanto;
        AGC_Shisha_Mst__c sisha;
        AGC_Common_Dao dao = new AGC_Common_Dao();
        AGC_ContactRireki__c rireki;
        Integer nowCount = 0;
        
        common.getCustomSetting();
        
        User apiUser = common.createApiUser();
        insert apiUser;     
        
        System.RunAs(apiUser){      
            //準備
            sisha = common.createShishaMst();
            insert sisha;
            tanto = common.createTantoMst();
            insert tanto;
            kihon = common.createKihonInfo(sisha.Id,tanto.Id);
            insert kihon;
            hosoku = dao.getDairitenHosokujohoBykihonId(kihon.Id);
        }
        
        User u =common.createEigyoUser();
        insert u;
        
        //create vf
        PageReference pageRef = Page.AGC_ContactHistoryRegist_Page;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',kihon.Id);

        //create controller
        AGC_ContactHistory_Ctrl controller = new AGC_ContactHistory_Ctrl(new ApexPages.StandardController(kihon));  
        
        
        System.RunAs(u){
            
            controller.getKihon();
            controller.getHosoku();
            controller.getCurrentIdx();
            controller.getRecordCnt();
            
            System.assertEquals('テスト支社名',controller.getShishaMst().Name);
            System.assertEquals('担当者',controller.getShishamrMst().Name);
        
            //例外ケース
            //次へ
            controller.getRirekiAftDetail();
        
            //最後へ
            controller.getRirekiLastDetail();
            //前へ
            controller.getRirekiBefDetail();
            //最初へ
            controller.getRirekiFirstDetail();
        
        
        
            //レコードの新規作成
            controller.createNewRecord();
            rireki = controller.getContactRireki();

            System.assertEquals(UserInfo.getName(),rireki.ReceptionStaff__c );
            System.assertEquals(kihon.id,rireki.AGC_DairitenKihonJoho_Ref__c);

            //レコードの編集、保存
            rireki = controller.getContactRireki();
            rireki.InquiryTitle__c = '件名０';
            rireki.InquiryDetails__c = 'test問い合わせ';
            controller.saveRecord();
        
            System.assertEquals(nowCount+1,controller.RecordCnt);
            System.assertEquals('test問い合わせ',controller.getRirekiList()[0].InquiryDetails__c);
        
            //明細の一覧をクリックしたときに、クリックされた明細レコードを取得する
            controller.RirekiId = rireki.Id;
            controller.getRirekiDetail();
            rireki = controller.getContactRireki();
            System.assertEquals('test問い合わせ',rireki.InquiryDetails__c);
        
            //レコードを追加する
            controller.createNewRecord();
            rireki = controller.getContactRireki();
            rireki.InquiryTitle__c = '件名１';
            controller.saveRecord();

            controller.createNewRecord();
            rireki = controller.getContactRireki();
            rireki.InquiryTitle__c = '件名２';
            controller.saveRecord();

            controller.createNewRecord();
            rireki = controller.getContactRireki();
            rireki.InquiryTitle__c = '件名３';
            controller.saveRecord();
        
        
            //AJAXの動作は画面で確認してください
            //次へ
            controller.getRirekiAftDetail();
            //最後へ
            controller.getRirekiLastDetail();
            //前へ
            controller.getRirekiBefDetail();
            //最初へ
            controller.getRirekiFirstDetail();
        
            controller.getCurrentRecordLabel();
        
            //既に履歴が存在している状態でレコードを作成する
            AGC_ContactHistory_Ctrl controller2 = new AGC_ContactHistory_Ctrl(new ApexPages.StandardController(kihon)); 
        }
    }
}