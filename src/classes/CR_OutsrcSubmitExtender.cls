global with sharing class CR_OutsrcSubmitExtender extends CR_AbstractExtender{
	
	@TestVisible
    private CR_OutsrcSubmitController controller;
    public Attachment file{get;set;}
    @TestVisible
    private List<Attachment> uploadedfiles{get;set;}        //既にアップロード済のファイル

    public CR_OutsrcSubmitExtender(CR_OutsrcSubmitController extension){
        super();
        controller = extension;
        this.file = new Attachment();
        this.uploadedfiles = E_AttachmentDao.getRecsById(controller.record.Id);
        this.helpMenukey = CR_Const.MENU_HELP_OUTSRC;
        this.helpPageJumpTo = CR_Const.MENU_HELP_OUTSRC_Submit;
    }

    /** 
     * init
     */
    global override void init() {
        isSuccessOfInitValidate = false;
        if(getHasOutsrcViewPermission()==false){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG05_REQUIRED_PERMISSION_OUTSRC);        //'外部委託参照権限がありません'
        }else if(this.controller.record == null 
                || String.isBlank(this.controller.record.Id)){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            this.viewingAgencyCode = this.controller.record.CR_Agency__r.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }
    }
    
    //既にアップロード済のファイルがあるか？　→ない場合、添付不可理由を記載可能とする。
    public Boolean getExistsUploadedFiles(){
        return uploadedfiles.size()>0;
    }

    //外部委託　参照
    public String getUrlforView(){
        PageReference pr = Page.E_CROutSrcView;
        pr.getParameters().put('id', controller.record.Id);
        return pr.getUrl();
    }

    //ファイルが選択されていない場合にTrue
    @TestVisible
    private Boolean fileIsNotAttached(){
        return this.file==null||this.file.Body==null||String.isBlank(this.file.Name);
    }

    public PageReference doSaveEx(){
        pageMessages.clearMessages();
        
        if(getExistsUploadedFiles()){
        }else{
        }
        try{
            //アップロード済のファイルがある場合
            if(getExistsUploadedFiles()){
                //添付ファイルがないとエラーとする
                if(fileIsNotAttached()){
                    pageMessages.addErrorMessage(CR_Const.ERR_MSG02_REQUIRED_FILE);
                    return null;
                }
            
            //添付不可理由　表示時
            }else{
                //ファイルアップロード未状態の場合は、添付ファイルもしくは添付不可理由を必須とする
                if(fileIsNotAttached() && String.isBlank(controller.record.ReasonForNofiles__c)){
                    pageMessages.addErrorMessage(CR_Const.ERR_MSG02b_REQUIRED_FILE_REASON);
                    return null;
                }

                //ファイル添付されている場合は、添付不可理由をクリアする
                if(fileIsNotAttached()==false){
                    controller.record.reasonforNoFiles__c = null;
                }
                Boolean saveResult = E_GenericDao.saveViaPage(controller.record);
                if(saveResult){
                    // do Nothing
                }else{
                    return null;
                }   
            }
            
            //ファイル添付されている場合は更新処理を実施
            if(fileIsNotAttached()==false){
                this.file.parentId = controller.record.Id;
                this.file.ContentType = CR_Const.CONTENTTYPE_FOR_DOWNLOAD;		//強制的にファイルダウンロードを実施させる
                insert file;
            }

            PageReference pr = Page.E_CROutsrcView;
            pr.getParameters().put('id', controller.record.Id);
            return pr;
        }catch(Exception e){
            pageMessages.addErrorMessage('ファイルアップロードエラー：'+e.getMessage());
        }
        return null;
    }
  
    public PageReference doSaveAndNew(){
        PageReference pr = doSaveEx();
        if(pr != null){
            PageReference prNew = Page.E_CROutsrcSubmit;
            prNew.getParameters().put('id', controller.record.Id);
            prNew.setRedirect(true);
            return prNew;
        }
        return null;
    }
}