public abstract class I_AbstractSalesResultsController extends I_AbstractController{

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	//挙積表示フラグ
	public Boolean isShowSR {get;protected set;}
	
	//URLパラメータ
	//営業部名
	public String unitId {
		get{
			if(String.isNotBlank(unitId)){
				return unitId;
			}
			return apexPages.currentPage().getParameters().get('brno');
		}protected set;
	}
	//MRID
	public String mrId {
		get{
			if(String.isNotBlank(mrId)){
				return mrId;
			}
			return apexPages.currentPage().getParameters().get('mrId');
		}protected set;
	}

	//代理店ID
	public String agencyId {
		get{
			if(String.isNotBlank(agencyId)){
				return agencyId;
			}
			return apexPages.currentPage().getParameters().get('agency');
		}protected set;
	}
	//事務所ID
	public String officeId {
		get{
			if(String.isNotBlank(officeId)){
				return officeId;
			}
			return apexPages.currentPage().getParameters().get('office');
		}protected set;
	}
	//募集人ID
	public String agentId {
		get{
			if(String.isNotBlank(agentId)){
				return agentId;
			}
			return apexPages.currentPage().getParameters().get('agent');
		}protected set;
	}

	//支社コード
	public String getBrunchCode() {
		return (access.isEmployee())? access.idcpf.ZINQUIRR__c.right(2) : '';
	}

	/**
	 * Const
	 */
	public I_AbstractSalesResultsController() {
		pageMessages = getPageMessages();
		isShowSR = false;
		fromSearchBox();
	}

	// MenuIdを初期化する。
	public override Id getIrisMenuId(){
		//ActiveMenuクリア
		iris.irisMenus.clearAcitveMenu();

		return null;
	}
	/**
	 * 社員フラグ
	 */
	public Boolean getIsEmployee(){
		return access.isEmployee();
	}
	/**
	 * 自己データ参照フラグ
	 */
	public Boolean getIsSelf(){
		return mrid == UserInfo.getUserId();
	}

	/**
	 * 初期処理
	 */
	public virtual override PageReference init () {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}

		//リダイレクト
		PageReference pr = doRedirect();
		if (pr!=null) {
			return pr;
		}
		// Superクラスでエラーなしの場合に処理を実施
		pr = super.init();
		if (pr!=null) {
			return pr;
		}
		//当日データに切り替わっていない場合にはワーニングを表示
		if(!getIsDispTodayData() && isShowSR){
			pageMessages.addWarningMessage(getMSG().get('AMS|SR|005'));
		}
		return null;
	}

	/**
	 * リダイレクト
	 */
	protected virtual PageReference doRedirect() {
		return null;
	}


	/**
	 * 社員挙積情報へ遷移
	 */
	public PageReference redirectEmployeeSRPage() {
		PageReference pr = Page.IRIS_SalesResults;
		if(String.isNotBlank(mrId)){
			pr.getParameters().put('mrId',mrId);
		}
		pr.setRedirect(true);
		return pr;
	}

	/**
	 * 営業部一覧へ遷移
	 */
	public PageReference redirectUnitPage() {
		PageReference pr = Page.IRIS_Unit;
		pr.setRedirect(true);
		return pr;
	}

	/**
	 * 代理店挙績情報へ遷移
	 */
	public PageReference redirectAgencySalesPage() {
		PageReference pr = Page.IRIS_AgencySalesResults;
		pr.setRedirect(true);
		return pr;
	}


	/**
	 * 検索ボックスからの遷移かどうか判定
	 */
	public Boolean fromSearchBox(){
		String searchType = apexPages.currentPage().getParameters().get('type');
		String searchId   = apexPages.currentPage().getParameters().get('id');

		if(String.isBlank(searchType) || String.isBlank(searchId)){
			return false;
		}		

		// [TODO] const
		if(searchType == 'office'){
			officeId = searchId;
		} else if(searchType == 'agency'){
			agencyId = searchId;
		} else if(searchType == 'agent'){
			agentId = searchId;
		} else {
			return false;
		}

		return true;
	}
	/**
	 * 営業日の取得
	 */
	protected String getBizDate (){
		return E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
	}
	/**
	 * 本日データ切替済フラグ
	 */
	public Boolean getIsDispTodayData(){
		Datetime cDate = E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().CreatedDate;
		Date tDay = System.today();
		return cDate.year() == tDay.year() && cDate.month() == tDay.month() && cDate.day() == tDay.day();
	}
}