/**
 * 
 */
@isTest
private class TestE_DownloadHoldPolicySearchExtension {

	private static User user;
	private static User thisUser = [select id from user where id =:system.UserInfo.getUserId()];
	private static Account kakuAcc;
	private static List<Account> offAccList;
	private static Contact agentCon;
	
	/** 
	 * SelectOption
	 */
    static testMethod void testSelectOption01() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('oid', offAccList[0].Id);		
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	List<SelectOption> doul = extension.getDataOutputUnitList();
	    	system.assert(doul.size() == 2);
	    	
	    	List<SelectOption> aorl = extension.getAllOrRangeList();
	    	system.assert(aorl.size() == 2);
	    	
	    	List<SelectOption> zcl = extension.getZcntrdscList();
	    	system.assert(zcl.size() == 3);
	    	
	    	List<SelectOption> zal = extension.getZannfromList();
	    	system.assert(zal.size() == 3);
    	}

    	Test.stopTest();
    }
    
    /** PageReference pageAction() */
    static testMethod void testPageAction01(){
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('oid', offAccList[0].Id);		
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	Pagereference resultPref = extension.pageAction();
	    	system.assertEquals(null, resultPref);
    	}

    	Test.stopTest();
    }
	
	/** 
	 * PageReference doDownloadCsv() 
	 * 正常：COLI,代理店
	 */
    static testMethod void testDoDownloadCsv01() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('oid', offAccList[0].Id);		
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	// Constructor assertion
	    	system.assertEquals('range',extension.Comm_Zacctym);
	    	system.assertEquals('all',extension.Comm_Occdate);
	    	system.assertEquals('all',extension.Comm_Zcntrdsc);
	    	system.assertEquals('1986/01/01', extension.startDate);
	    	system.assertEquals(Datetime.now().format('yyyy/MM/dd'), extension.endDate);
	    	
	    	Pagereference resultPref = extension.doDownloadCsv();
	    	
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
    	}

    	Test.stopTest();
    }
    
	/** 
	 * PageReference doDownloadCsv() 
	 * 正常：SPVA,代理店
	 */
    static testMethod void testDoDownloadCsv02() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_SPVA);	// SPVA
        pref.getParameters().put('oid', offAccList[0].Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	// Constructor assertion
	    	system.assertEquals('range',extension.Comm_Zacctym);
	    	system.assertEquals('all',extension.Comm_Occdate);
	    	system.assertEquals('all',extension.Comm_Zcntrdsc);
	    	system.assertEquals('2000/01/01', extension.startDate);
	    	system.assertEquals(Datetime.now().format('yyyy/MM/dd'), extension.endDate);
	    	
	    	Pagereference resultPref = extension.doDownloadCsv();
	    	
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
    	}

    	Test.stopTest();
    }

	/** 
	 * PageReference doDownloadCsv() 
	 * 正常：COLI,代理店
	 */
    static testMethod void testDoDownloadCsv03() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	for(Account offAcc : offAccList){
	    		extension.targetOfficeIdList.add(offAcc.Id);
	    	}
	    	// 成立年月
	    	extension.Comm_Zacctym = 'range';
	    	extension.fromZacctym = '2015/01';
	    	extension.toZacctym = '2015/04';
	    	// 契約日
	    	extension.Comm_Occdate = 'range';
	    	extension.fromOccdate = '2015/01/01';
	    	extension.toOccdate = '2015/04/01';
	    	// 契約者
	    	extension.Comm_Zcntrdsc = 'P';
	    	// 
	    	extension.Spva_Zannfrom = '0';
	    	
	    	Pagereference resultPref = extension.doDownloadCsv();
	    	
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
    	}

    	Test.stopTest();
    }

	/** 
	 * PageReference doDownloadCsv() 
	 * 正常：COLI,募集人
	 */
    static testMethod void testDoDownloadCsv04() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('aid', agentCon.Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	Pagereference resultPref = extension.doDownloadCsv();
	    	
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
    	}

    	Test.stopTest();
    }
    
	/** 
	 * Constructor
	 * エラー：パラメータ不正
	 */
    static testMethod void testErrConstructor01() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'TEST');	// 不正値
        pref.getParameters().put('oid', offAccList[0].Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	system.assertEquals(E_Const.ERROR_MSG_PARAMETER, controller.errorMsgCode);
    	}

    	Test.stopTest();
    }
    
	/** 
	 * boolean checkInputForm() 
	 * エラー：代理店格の時、代理店事務所未選択
	 */
    static testMethod void testErrCheckInputForm01() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);

	    	Pagereference resultPref = extension.doDownloadCsv();
	    	system.assert(controller.pageMessages.getHasErrorMessages());
    	}

    	Test.stopTest();
    }
    
	/** 
	 * boolean checkInputForm()
	 * エラー：成立年月チェック
	 */
    static testMethod void testErrCheckInputForm02() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	for(Account offAcc : offAccList){
	    		extension.targetOfficeIdList.add(offAcc.Id);
	    	}
	    	// 成立年月
	    	extension.Comm_Zacctym = 'range';
	    	extension.fromZacctym = '';
	    	extension.toZacctym = '';
	    	
	    	Pagereference resultPref = extension.doDownloadCsv();
	    	system.assert(controller.pageMessages.getHasErrorMessages());
    	}

    	Test.stopTest();
    }
    
	/** 
	 * boolean checkInputForm()
	 * エラー：契約日（更新日）チェック
	 */
    static testMethod void testErrCheckInputForm03() {
    	// User
    	//createData('E_EmployeeStandard');
    	createUser();
    	createData(true);
    	
    	// ページ情報
    	Pagereference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', E_Const.FROM_COLI);	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadHoldPolicySearchExtension extension = new E_DownloadHoldPolicySearchExtension(controller);
	    	
	    	for(Account offAcc : offAccList){
	    		extension.targetOfficeIdList.add(offAcc.Id);
	    	}
	    	// 契約日
	    	extension.Comm_Occdate = 'range';
	    	extension.fromOccdate = '';
	    	extension.toOccdate = '';
	    	
	    	Pagereference resultPref = extension.doDownloadCsv();
	    	system.assert(controller.pageMessages.getHasErrorMessages());
    	}

    	Test.stopTest();
    }
    

    
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */    
    
    /** User作成 */
    private static void createUser(){
        system.runAs(thisuser){
            // User
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            String userName = 'test@terrasky.ingtesting';
            Profile p = [Select Id From Profile Where Name = 'システム管理者'];
            user = new User(
                Lastname = 'test'
                , Username = userName
                , Email = userName
                , ProfileId = p.Id
                , Alias = 'test'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , UserRoleId = portalRole.Id
            );
            insert user;
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = user.Id
                ,ZIDTYPE__c = E_Const.ZIDTYPE_AH
                ,FLAG01__c = '1'
                ,FLAG02__c = '1'
                ,FLAG06__c = '1'
                ,ZDSPFLAG01__c = '1'
            );
            insert idcpf;
        }
    }
    
    /** Data作成 */
    private static void createData(Boolean isCreateOffice){
        system.runAs(thisUser){
            // Account 代理店格
            kakuAcc = new Account(Name = 'kakuAccount');
            insert kakuAcc;
            
            if(isCreateOffice){
                offAccList = new List<Account>();
                for(Integer i = 1; i < 4; i++){
                    Account acc = new Account(
                        Name = 'office' + i
                        ,ParentId = kakuAcc.Id
                    );
                    offAccList.add(acc);
                }
                insert offAccList;
            }
            
            if(!offAccList.isEmpty()){
                agentCon = new Contact(
                    LastName = 'test'
                    ,AccountId = offAccList[0].Id
                    , E_CL3PF_AGNTNUM__c = 'agnum'
                );
                insert agentCon;
            }
            
            // 代理店手数料生保明細CSVダウンロードデータ用
            E_AGSPF__c agspf = new E_AGSPF__c();

            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        }
    }
}