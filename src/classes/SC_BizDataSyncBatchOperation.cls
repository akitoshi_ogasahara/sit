public with sharing class SC_BizDataSyncBatchOperation extends E_BizDataSyncBatchOperation{

	/**
	 *      getQueryLocator for Batch start method
	 */
	public override Database.QueryLocator getQueryLocator(){
		return Database.getQueryLocator(
					[SELECT Id,Name
						,OwnerId
						,isHeadSection__c
						,Account__r.E_ParentZHEADAY__c    //代理店コード
						,Account__r.E_CL2PF_ZAGCYNUM__c  //事務所コード
						,Account__r.E_CL2PF_BRANCH__c      //支社コード
						,Account__r.ZMRCODE__c            //MR番号
						,Account__r.Parent.E_CL1PF_CLNTNUM__c
						,DomainInternet__c
						,DomainGw__c
						,IsIris__c
						,FiscalYear__c
						,OfficeType__c
					FROM SC_Office__c
					ORDER BY FiscalYear__c desc]);
	}
	
	
	/**
	 *      execute
	 *          batchのExecuteの本体ロジック
	 */
	public override void execute(List<Sobject> records){
		Set<Id> scOfficeIds = new Set<Id>();
		Map<SObject, Account> amsOffices = new Map<SObject, Account>();
		for(SObject rec:records){
			Boolean isUpdate = false;
			SC_Office__c office = (SC_Office__c)rec;
			amsOffices.put(office, office.Account__r);
			scOfficeIds.add(office.Id);
		}
	
		//既存のShareレコードの削除  => 20161107 matsuzaki コメントアウト（担当変更後にもともとの担当がデータをみれなくなることが困るとのこと） 
		//E_ShareUtil.deleteNNLinkShares(amsOffices.keyset());
		Boolean hasError = false;
		try{
			// Shareレコードの作成(自主点検事務所レコードではなく、代理店事務所（最新の担当支社など）で設定を行う。)
			List<String> resultMsgs = E_ShareUtil.createNNLinkShares(amsOffices);
			if(!resultMsgs.isEmpty()){
				for(String errMsg : resultMsgs){
					errMsgs.add(errMsg);
				}
				hasError = true;
			}

			// 自主点検管理責任者の登録/削除
			SC_Util scUtil = new SC_Util();
			scUtil.createScAgencyManagers(scOfficeIds);

			// エラーあり
			if(hasError){
				throw new ClassException();
			}
		}catch(Exception e){
			errMsgs.add(e.getMessage());
			throw e;
		}
	}
	/**
	 * Exception
	 */
	private class ClassException extends Exception {}
	
}
/**
			Map<SObject, Account> amsOffices = new Map<SObject, Account>();
			for(SC_Office__c rec:[SELECT Id,Name
										,OwnerId
										,Account__r.E_ParentZHEADAY__c    //代理店コード
										,Account__r.E_CL2PF_ZAGCYNUM__c  //事務所コード
										,Account__r.E_CL2PF_BRANCH__c      //支社コード
										,Account__r.ZMRCODE__c            //MR番号
									FROM SC_Office__c]){
				amsOffices.put(rec, rec.Account__r);
			}
			
			//既存のShareレコードの削除
			E_ShareUtil.deleteNNLinkShares(amsOffices.keyset());
			
			//Shareレコードの作成(自主点検事務所レコードではなく、代理店事務所（最新の担当支社など）で設定を行う。)
			E_ShareUtil.createNNLinkShares(amsOffices);

*/