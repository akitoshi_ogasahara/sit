public without sharing class E_UserLoginDaoWithout {
	/*
	 * ユーザログイン情報をユーザIDから取得する
	 * @param userId: ユーザID
	 * @return UserLogin: ユーザログイン情報
	 */
	public static UserLogin getRecordByUserId(Id uid){
		for(UserLogin rec:[Select Id,
								UserId, 
								LastModifiedDate, 
								LastModifiedById, 
								IsPasswordLocked, 
								IsFrozen 
							From 
								UserLogin
							WHERE 
								UserId = :uid
							ORDER BY LastModifiedDate DESC]){
			return rec;
		}
		return null;
	}

	/**
	
		※※※　2017.10　以降当メソッドの利用禁止　to be Name changed　※※※
				メソッド名が不適切な為（メソッド名で推定されないWHERE句が付与されている）
					
	 * ユーザログイン情報をユーザIDから取得する　ロック状態出ないユーザのみ対象
	 * @param userId: ユーザID
	 * @return UserLogin: ユーザログイン情報
	 */
	public static UserLogin getRecByUserId(Id uid){
		UserLogin rec = null;
		List<UserLogin> recs =
			[Select 
				u.UserId, 
				u.LastModifiedDate, 
				u.LastModifiedById, 
				u.IsPasswordLocked, 
				u.IsFrozen, u.Id 
			From 
				UserLogin u
			WHERE 
				u.UserId = :uid
			And
				u.IsPasswordLocked = false
			Order by
				u.LastModifiedDate DESC LIMIT 1
			];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 * ユーザログイン情報をユーザIDから取得する ロック状態は無視
	 * @param userId: ユーザID
	 * @return UserLogin: ユーザログイン情報
	 */
	public static UserLogin getRecWithIsLockByUserId(Id uid){
		UserLogin rec = null;
		List<UserLogin> recs =
			[Select 
				u.UserId, 
				u.LastModifiedDate, 
				u.LastModifiedById, 
				u.IsPasswordLocked, 
				u.IsFrozen, u.Id 
			From 
				UserLogin u
			WHERE 
				u.UserId = :uid
			Order by
				u.LastModifiedDate DESC LIMIT 1
			];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 * ユーザログイン情報リストをユーザIDセットから取得する ロック状態は無視
	 * @param userIds: ユーザIDセット
	 * @return List<UserLogin>: ユーザログイン情報
	 */
	public static List<UserLogin> getRecsWithIsLockByUserIds(Set<Id> userIds){
		return [Select 
				u.UserId, 
				u.LastModifiedDate, 
				u.LastModifiedById, 
				u.IsPasswordLocked, 
				u.IsFrozen, u.Id 
			From 
				UserLogin u
			WHERE 
				u.UserId in :userIds
			Order by
				u.LastModifiedDate
			];
	}

	public static void updateRec(UserLogin ulogin){
		update ulogin;
	}

}