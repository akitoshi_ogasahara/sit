public class SC_Const {
	/* メッセージ一覧 */
	public static final String ERR_MSG01_INVALID_URL = 'Urlが不正です。';
	public static final String ERR_MSG02_NOT_UPLOAD_FILES = '書類がアップロードされていません。';
	public static final String ERR_MSG03_REQUIRED_FILE = '添付ファイルを選択してください。';
	public static final String ERR_MSG03b_REQUIRED_FILE_REASON = '添付ファイルを選択するか、添付いただけない理由を記載してください。';
	public static final String ERR_MSG04_REQUIRED_FILES = 'ファイルをアップロードいただくか、アップロードいただけない理由をファイルアップロードページから記載ください。';
	public static final String ERR_MSG05_SCOFFICE_NONE  = '代理店業務点検の対象ではありません。';

	//ファイルアップロード時のContentType：強制的にダウンロードさせる
	public static final String CONTENTTYPE_FOR_DOWNLOAD = 'application/octet-stream';

	/* ステータス一覧 */
/* 2018年度対応　番号削除
	public static final String AMSOFFICE_STATUS_01 = '1.未提出';
	public static final String AMSOFFICE_STATUS_02 = '2.担当MR確認中';
	public static final String AMSOFFICE_STATUS_03 = '3.営業部長確認中';
	public static final String AMSOFFICE_STATUS_04 = '4.本社確認中';
	public static final String AMSOFFICE_STATUS_05 = '5.完了';
*/
	public static final String AMSOFFICE_STATUS_01 = '未提出';
	public static final String AMSOFFICE_STATUS_02 = '担当MR確認中';
	public static final String AMSOFFICE_STATUS_03 = '営業部長確認中';	// ※2018年度以降は使用しない
	public static final String AMSOFFICE_STATUS_04 = '本社確認中';
	public static final String AMSOFFICE_STATUS_05 = '完了';

	// 代理店用表示切り替え
	public static final String AMSOFFICE_STATUS_01_AG = '未提出';
	public static final String AMSOFFICE_STATUS_02_AG = 'エヌエヌ生命確認中';
	//public static final String AMSOFFICE_STATUS_03_AG = '営業部長確認中';	// ※2018年度以降は使用しない
	public static final String AMSOFFICE_STATUS_04_AG = 'エヌエヌ生命確認中';
	public static final String AMSOFFICE_STATUS_05_AG = '完了';
	
	/* Menuキー */
	// 代理店事務所検索　社員ユーザ
	public static final String MENU_KEY_SC_SEARCH_EMPLOYEE = 'upload_selfcompliance';
	// 代理店事務所検索　代理店ユーザ
	public static final String MENU_EKY_SC_SEARCH_AGENT = 'upload_selfcompliance_ag';

	/* HELP Menu名*/
	// 提出書類一覧 各種資料ダウンロード
	public static final String MENU_GUIDE_SELFCOMPLIANCE = 'Guide_AMS_SelfCompliance';
	// 各種資料ダウンロード　タグ
	public static final String MENU_GUIDE_SELFCOMPLIANCE_GUIDE = 'cms_contents_001';
	
	// 提出書類一覧
	public static final String MENU_HELP_SELFCOMPLIANCE = 'Help_AMS_SelfCompliance';
	// 提出書類一覧  提出書類一覧画面タグ
	public static final String MENU_HELP_SELFCOMPLIANCE_VIEW = 'cms_contents_001';
	// 提出書類一覧 提出書類アップロード画面　タグ
	public static final String MENU_HELP_SELFCOMPLIANCE_SUBMIT_NEW = 'cms_contents_002';
	// 提出書類一覧 提出書類種別変更画面一覧　タグ
	public static final String MENU_HELP_SELFCOMPLIANCE_SUBMIT_UPD = 'cms_contents_003';
	
	// 自主点検　権限セット名
	public static final String PERMISSIONSET_SC_NN = 'AMS_sc_PermissionSet_NN';					//自主点検権限セット　NN
	public static final String PERMISSIONSET_SC_Director = 'AMS_sc_PermissionSet_Director';		//自主点検権限セット　参照のみ
	public static final String PERMISSIONSET_SC_Agent = 'AMS_sc_PermissionSet_AG';				//自主点検権限セット　代理店ユーザ
	public static final String PERMISSIONSET_SC_Admin = 'AMS_sc_PermissionSet_Admin';			//自主点検権限セット　管理者（コンプラ）

	// 自主点検　EBiz連携ログ
	public static final String EBIZ_LOGKBN_AMS_SC_ShareSet = '7';
	
	/* ボタン名　提出書類一覧 */
	public static final String SC_BTNLABEL_APPLY = '提出';
	public static final String SC_BTNLABEL_REPORT_MR = '営業部長へ提出';
	public static final String SC_BTNLABEL_REPORT_MGR = '本社へ提出';
	public static final String SC_BTNLABEL_APPROVE = '完了';
	public static final String SC_BTNLABEL_REJECT_REPORT_MR = '差し戻し';
	public static final String SC_BTNLABEL_REJECT_REPORT_MGR = '差し戻し';
	public static final String SC_BTNLABEL_REJECT_APPROVE = '差し戻し';
	public static final String SC_BTNLABEL_REJECT_COMPLETE = '完了解除';

	public static final String SC_BTNLABEL_WEB_ANSWER = 'Webで回答';
	public static final String SC_BTNLABEL_WEB_ANSWER_CONFIRM = 'Webで回答を確認';
	public static final String SC_BTNLABEL_WEB_ANSWER_CHANGE = 'Webで回答に変更';
	public static final String SC_BTNLABEL_PDF_UPLOAD = 'PDFをアップロード';
	public static final String SC_BTNLABEL_PDF_CHANGE = 'PDFアップロードに変更';
	public static final String SC_BTNLABEL_PDF_REUPLOAD = 'PDFを再アップロード';
	public static final String SC_BTNLABEL_UPLOAD = 'アップロード';
	public static final String SC_BTNLABEL_REUPLOAD = '再アップロード';
	public static final String SC_BTNLABEL_ADDUPLOAD = '追加アップロード';

	//本社提出不可点検実施方法（結果）
	public static final String SC_FIXED_REJECT_REPORT_MGR = '未実施';

	/* 不備ステータス */
	public static final String DEFECT_STATUS_01 = '--なし--';
	public static final String DEFECT_STATUS_02 = '不備対応中';
	public static final String DEFECT_STATUS_03 = '不備対応完了';
	
	/* リマインドメール */
	// メールタイプ
	public static final String SC_REMAIND_TYPE_AG = 'agency';
	public static final String SC_REMAIND_TYPE_MR = 'mr';
	
	/* 自主点検.種別 */
	public static final String SC_COMP_TYPE_COMMON = '共通';
	public static final String SC_COMP_TYPE_NN = 'NN追加';
	public static final String SC_COMP_TYPE_TRAIL = '証跡';
	public static final List<String> SC_COMP_TYPE_LIST = new List<String>{
										SC_COMP_TYPE_COMMON, SC_COMP_TYPE_NN, SC_COMP_TYPE_TRAIL};
										
	/* 自主点検.提出方法 */
	public static final String SC_TYPE_SUBMIT_WEB = 'WEB';
	public static final String SC_TYPE_SUBMIT_PDF = 'PDF';
						
	/* 提出一覧タイトル */				
	public static final String SC_TITLE_COMMON = '共通点検表';
	public static final String SC_TITLE_NN = 'NN生命追加点検表';
	public static final String SC_TITLE_TRAIL = '証跡資料';

	//提出一覧画面 操作方法の説明
	public static final String SC_OPERETION_EXPLANATION = '年度代理店業務点検　操作方法の説明|';

	/* 点検実施の手引き　ページユニークキーのPrefix */
	public static final String SC_MANUAL_PREFIX = 'SCMANUAL';
	// 点検実施の手引き 点検表PDFファイルダウンロードアンカー
	public static final String SC_MANUAL_PDF_ANCHOR = 'downloadTable';

	/* 点検表アップロード画面 */
	public static final String SC_COMMON_TITLE = '共通点検表アップロード（代理店自己点検）';
	public static final String SC_ADD_TITLE = 'エヌエヌ生命追加点検表アップロード（代理店自己点検）';
	public static final String SC_TRAIL_TITLE = '証跡資料アップロード（代理店自己点検）';
	/* ディスクレーマー */
	//共通
	public static final String SC_COMMON_DISCLAMER_DESC = 'SCS|001';
	public static final String SC_COMMON_DISCLAMER_NOTES = 'SCS|004';
	//追加
	public static final String SC_ADD_DISCLAMER_DESC = 'SCS|002';
	public static final String SC_ADD_DISCLAMER_NOTES = 'SCS|005';
	//証跡 末尾に年度をつける
	public static final String SC_TRAIL_DISCLAMER_DESC = 'SCS|003|';
	public static final String SC_TRAIL_DISCLAMER_NOTES = 'SCS|006|';

	// 年度のセット
	public static final Set<String> SC_FISCALYEAR_BEFORE2017 = new Set<String>{'2016', '2017'};

	// メールの送信元アドレス（組織のメール）
	public static final String EMAIL_NAME_JIKOTENKEN = 'ML-JP-Compliance_JIKOTENKEN';
}