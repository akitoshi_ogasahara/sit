public with sharing class I_OwnerController extends I_AbstractPolicyController{
	// 契約者リスト
	public List<OwnerListRow> owners {get; set;}
	//集約前件数が2000件か判定フラグ--スプリントバックログ29(2017/1/18)
	public Boolean twoThousandOverFlag {get; set;}

// ソート用
	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE = 'st';
	// ソートキー
	public static final String SORT_TYPE_CUSTOMER_NO = 'customerNo';
	public static final String SORT_TYPE_CONTRACTANT = 'contractant';
	public static final String SORT_TYPE_NUM         = 'num';
	public static final String SORT_TYPE_ADDRESS     = 'address';
	public static final String SORT_TYPE_AGENCY      = 'agency';
	public static final String SORT_TYPE_OFFICE      = 'office';
	public static final String SORT_TYPE_AGENT       = 'agent';
	public static final String SORT_TYPE_MR          = 'mr';
	public static final String SORT_TYPE_PHONENUM    = 'phoneNum';
	// ソート項目
	public static final String SORT_FIELD_CUSTOMER_NO = 'ContractNo__c';
	public static final String SORT_FIELD_CONTRACTANT = 'ContractorName__c';
	public static final String SORT_FIELD_ADDRESS     = 'ContractorAddress__c';


	// コンストラクタ
	public I_OwnerController(){
		super();
		pageMessages = getPageMessages();

		//デフォルト表示フラグfalse
		//募集人
		contactFrag = false;
		//事務所
		officeFlag = false;
		//担当MR
		manegeMRFlag = false;

		//デフォルト集約前件数が2000件か判定フラグfalse--スプリントバックログ29(2017/1/18)
		twoThousandOverFlag = false;

		// デフォルトソート - 契約者名 : 昇順
		sortType  = SORT_TYPE_CONTRACTANT;
		sortIsAsc = true;

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	protected override String getLinkMenuKey(){
		return 'ownerList';
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = this.menu.Name;
		if(pr==null){
			//ログイン判定
			//社員（募集人・事務所・担当MR）
			if(access.isEmployee()){
				contactFrag = true;
				manegeMRFlag = true;
				officeFlag = true;
			//AH（募集人・事務所）
			}else if(access.isAuthAH()){
				contactFrag = true;
				officeFlag = true;
			//AY（募集人）
			}else if(access.isAuthAY()){
				contactFrag = true;
			}

			if (String.isBlank(searchType)) {
				searchType = getSearchTypeOptions().get(0).getValue();
			}

			createOwnerList();

	    	list_sortType  = sortType;
			list_sortIsAsc = sortIsAsc;

			owners.sort();
		}

		return pr;
	}

	/**
	 * 契約者一覧取得
	 */
	public void createOwnerList(){
		owners = new List<OwnerListRow>();
		Map<String, OwnerListRow> ownerMap = new Map<String, OwnerListRow>();

		//MRコード（事務所の項目)格納用リスト
		List<String> mrCodeList = new List<String>();

		//DAO呼び出し
		List<E_Policy__c> policyRecs = new List<E_Policy__c>();
		String condition = createPolicyCondition();
		// 拠点長の場合
		if (hasViewedByManager) {
			condition += createPolicyConditionByManager();
		}

		policyRecs = E_PolicyDao.getRecsIRISOwnerList(condition);

		//0件の時エラー
		if(policyRecs.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('LIS|003'));
			return;
		}
		//最大件数以上の場合は警告を表示する--スプリントバックログ29のためコメントアウト(2017/1/18)
		/*if(policyRecs.size() >= I_Const.LIST_MAX_ROWS_AGGREGATE){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS_AGGREGATE).format()});
			pageMessages.addWarningMessage(errmessage);
		}*/

		// アクティブメニューキー（URLに付与することでアクティブメニューを保持）
		String irisMenu = '&' + getIRISMenuParameter();
		
		String filteringParam = crateCustomerInfoParameter();
		// 保険契約ヘッダを取得
		for(E_Policy__c p : policyRecs){
			OwnerListrow row = new OwnerListRow();

			// 既に存在する顧客の場合
			if(ownerMap.containskey(p.ContractNo__c)){
				row = ownerMap.get(p.ContractNo__c);
				// 件数をプラス1
				row.num += 1;

			} else {
				// 各項目をセット
				row.customerNo  = p.ContractNo__c;
				row.owner       = p.ContractorName__c;
				row.num         = 1;
				row.address     = p.ContractorAddress__c;
				row.destURL    += 'cid='    + p.Contractor__c
								+ '&cNo='   + p.ContractorCLTPF_CLNTNUM__c
								+ '&cName=' + EncodingUtil.urlEncode(p.ContractorName__c,'UTF-8')
								+ filteringParam
								+ irisMenu;
				row.phoneNum    = p.Contractorphone__c;

				//スプリントバックログ9(2017/1/18) カナソート用追記
				row.ownerSort   = p.ContractorE_CLTPF_ZCLKNAME__c;
				row.addressSort = p.ContractorAddressKana__c;
			}

			//スプリントバックログ9(2017/1/18) カナソート用修正
			//代理店事務所
			row.setValueOffice(p.MainAgentAccountName__c, p.SubAgentAccountName__c,p.MainAgentAccountNameKana__c);
			//row.setValueOffice(p.MainAgentAccountName__c, p.SubAgentAccountName__c);
			//募集人
			row.setValueAgent(p.MainAgentName__c, p.SubAgentName__c,p.MainAgentNameKana__c);
			//row.setValueAgent(p.MainAgentName__c, p.SubAgentName__c);
			//担当MR
			row.setValueMRName(p.MainAgentAccountMRName__c, p.SubAgentAccountMRName__c);

			ownerMap.put(p.ContractNo__c, row);
			//1001件以上をvisualforceに渡すとエラーになるためbreak
			if(ownerMap.size() == I_Const.LIST_MAX_ROWS){
				break;
			}

		}

		if(ownerMap != null && !ownerMap.isEmpty()){
			owners = ownerMap.values();
		}

		//最大件数以上の場合は警告を表示する--スプリントバックログ29(2017/1/18)
		//集約前件数が2000件以上の時
		if(policyRecs.size() >= I_Const.LIST_MAX_ROWS_AGGREGATE){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS_AGGREGATE).format()});
			pageMessages.addWarningMessage(errmessage);
			//スプリントバックログ29用フラグ
			twoThousandOverFlag = true;
		//集約後件数が1000件以上の時
		}else if(owners.size() >= I_Const.LIST_MAX_ROWS){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS).format()});
			pageMessages.addWarningMessage(errmessage);
		}

		return;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){

		return owners == null ? 0 : owners.size();
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * ソート
	 */
	public void sortRows(){
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		// ソート方法 - SOQL
		if(sortType != sType){
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;

		owners.sort();

		return;
	}

	// 画面表示行ラッパークラス
	public class OwnerListRow implements Comparable{
		// 顧客番号
		public String customerNo {get; set;}
		// 契約者
		public String owner {get; set;}
		// 件数
		public Integer num {get; set;}
		// 住所
		public String address {get; set;}
		// 事務所
		public String office {get; set;}
		// 募集人
		public String agent {get; set;}
		// 担当MR
		public String mr {get; set;}
		// MRコード
		public String mrCode {get; set;}
		//電話番号
		public String phoneNum{get; set;}

		// 遷移先URL
		public STring destURL {get; set;}

		//スプリントバックログ9(2017/1/18) カナソート用変数
		//契約者カナ名
		public String ownerSort{get; set;}
		//契約者カナ住所
		public String addressSort{get; set;}
		//募集人カナ名
		public String agentSort{get; set;}
		//代理店事務所カナ名
		public String officeSort{get; set;}

		// コンストラクタ
		public OwnerListRow(){
			customerNo = '';
			owner      = '';
			address    = '';
			office     = '';
			agent      = '';
			mr         = '';
			destURL    = 'E_CustomerInfo?';
			mrCode     = '';
			phoneNum   = '';

			num = 0;

			//スプリントバックログ9(2017/1/18) カナソート用追記
			ownerSort   = '';
			addressSort = '';
			agentSort 	= '';
			officeSort 	= '';
		}

		// 事務所セット--スプリントバックログ9(2017/1/18)
		// public void setValueOffice(String main, String sub){
		public void setValueOffice(String main, String sub, String mainkana){
			// 現在値が「(複数)」の場合は何もしない
			if(office != I_Const.OL_MULTI_DATA){
				// （共同募集）主・従が空白でない かつ 主・従の値が異なる場合は「(複数)」をセット
				if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
					office = I_Const.OL_MULTI_DATA;
				// 主と office の値が異なる場合は「(複数)」をセット
				} else if(String.isNotBlank(office) && office != main){
					office = I_Const.OL_MULTI_DATA;
				// 上記以外で office が空白の場合は主をセット
				} else if(String.isBlank(office)) {
					office = main;
				}

				//スプリントバックログ9(2017/1/18) officeに（複数）がセットされているときは(複数)をセット。それ以外の場合はmainのカナ項目をセット
				if(office == I_Const.OL_MULTI_DATA){
					officeSort = I_Const.OL_MULTI_DATA;
				}else{
					officeSort = mainkana;
				}
			}
			return;
		}

		// 募集人セット--スプリントバックログ9(2017/1/18)
		// public void setValueAgent(String main, String sub)
		public void setValueAgent(String main, String sub, String mainkana){
			// 現在値が「(複数)」の場合は何もしない
			if(agent != I_Const.OL_MULTI_DATA){
				// 主、従の値が異なる場合は「(複数)」をセット
				if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
					agent = I_Const.OL_MULTI_DATA;
				// 主と agent の値が異なる場合は「(複数)」をセット
				} else if(String.isNotBlank(agent) && agent != main){
					agent = I_Const.OL_MULTI_DATA;
				// 上記以外で office が空白の場合は主をセット
				} else if(String.isBlank(agent)) {
					agent = main;
				}

				//スプリントバックログ9(2017/1/18) agentに（複数）がセットされているときは(複数)をセット。それ以外の場合はmainのカナ項目をセット
				if(agent == I_Const.OL_MULTI_DATA){
					agentSort = I_Const.OL_MULTI_DATA;
				}else{
					agentSort = mainkana;
				}
			}
			return;
		}

		// 担当MR名セット
		public void setValueMRName(String main, String sub){
			// 現在値が「(複数)」の場合は何もしない
			if(mr != I_Const.OL_MULTI_DATA){
				// 主、従の値が異なる場合は「(複数)」をセット
				if(String.isNotBlank(main) && String.isNotBlank(sub) && main != sub){
					mr = I_Const.OL_MULTI_DATA;
				// 主と mr の値が異なる場合は「(複数)」をセット
				} else if(String.isNotBlank(mr) && mr != main){
					mr = I_Const.OL_MULTI_DATA;
				// 上記以外で mr が空白の場合は主をセット
				} else if(String.isBlank(mr)) {
					mr = main;
				}
			}
			return;
		}

		// ソート
		public Integer compareTo(Object compareTo){
			OwnerListRow compareToRow = (OwnerListRow)compareTo;
			Integer result = 0;

			//顧客番号
			if(list_sortType == SORT_TYPE_CUSTOMER_NO){
				result = I_Util.compareToString(customerNo, compareToRow.customerNo);
			//契約者
			}else if(list_sortType == SORT_TYPE_CONTRACTANT){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(ownerSort, compareToRow.ownerSort);
				//result = I_Util.compareToString(owner, compareToRow.owner);
			//住所
			}else if(list_sortType == SORT_TYPE_ADDRESS){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(addressSort, compareToRow.addressSort);
				//result = I_Util.compareToString(address, compareToRow.address);
			// 件数
			}else if(list_sortType == SORT_TYPE_NUM){
				result = I_Util.compareToInteger(num, compareToRow.num);
			// 事務所
			} else if(list_sortType == SORT_TYPE_OFFICE){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(officeSort, compareToRow.officeSort);
				//result = I_Util.compareToString(office, compareToRow.office);
			// 募集人
			} else if(list_sortType == SORT_TYPE_AGENT){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(agentSort, compareToRow.agentSort);
				//result = I_Util.compareToString(agent, compareToRow.agent);
			// 担当MR
			} else if(list_sortType == SORT_TYPE_MR){
				result = I_Util.compareToString(mr, compareToRow.mr);
			// 担当MR
			} else if(list_sortType == SORT_TYPE_PHONENUM){
				result = I_Util.compareToString(phoneNum, compareToRow.phoneNum);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	public String getSiteName() {
		return Site.getName();
	}

}