public with sharing class I_PushOutBatchOperation_NewPolicy extends I_AbstractPushOutBatchOperation{

	/**
	 * <InnerClass>
	 *  契約単位の通知情報
	 */
	private class ContractInfo{
		private Boolean isGroup;		// 団体フラグ
		private String name;			// 契約者名
		private String insuredName;		// 被保険者名
		private String chdrnum;			// 証券番号
		private String groupnum;		// 団体番号
		private String insuranceType;	// 保険種類
		private String accName;			// 代理店事務所名
		private String agentName;		// 募集人名

		/** Constructor */
		private ContractInfo(E_NewPolicy__c rec, GroupAggregate ga){
			// 共通
			this.isGroup = ga == null ? false : true;
			/* 外字暫定対応
			this.name = rec.COMM_ZCLNAME__c;

			this.name = rec.ContractorE_CLTPF_ZCLKNAME__c;
			*/
			//外字対応
			this.name = rec.ContractorName__c;
			this.insuranceType = rec.InsuranceType__c;
			this.accName = rec.AgentAccountName__c;
			this.agentName = rec.Agent_Name__c;

			// 団体以外
			if(this.isGroup == false){
				/* 外字暫定対応
				this.insuredName = rec.COMM_ZINSNAM__c;

				this.insuredName = rec.InsuredE_CLTPF_ZCLKNAME__c;
				*/
				//外字対応
				this.insuredName = rec.InsuredName__c;
				this.chdrnum = rec.CHDRNUM__c;
				this.groupnum = rec.GRUPKEY__c;

			// 団体
			}else{
				this.insuredName = ga.insuredCnt;
				this.chdrnum = ga.chdrnum;
				this.groupnum = ga.groupKey;
			}
		}
	}

	/**
	 * <InnerClass>
	 *  団体集計情報
	 */
	private class GroupAggregate{
		private String groupKey;		// 団体番号
		private String chdrnum;			// 証券番号
		private String insuredCnt;		// 被保険者数

		/** Constructor */
		private GroupAggregate(String groupKey, String chdrnum, Integer cnt){
			this.groupKey = groupKey;
			this.chdrnum = chdrnum;
			this.insuredCnt = String.valueOf(cnt) + '名';
		}
	}

	/**
	 * Constructor
	 */
	public I_PushOutBatchOperation_NewPolicy(){
	}

	/**
	 * setQueryCondition
	 * 1つ前のEBiz連携ログの連携処理開始日時以降に更新（ステータス更新日、団体ステータス更新日）された新契約データ
	 */
	public override void setQueryCondition(){
		// 1つ前のEbiz連携ログ取得
		E_BizDataSyncLog__c ebizLogAgo = E_BizDataSyncLogDao.getRecAgoByNotificationKindForPush(ebizLog.Id , I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		//String convertedDate = ebizLogAgo.DataSyncStartDate__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
		dt = ebizLogAgo.DataSyncStartDate__c;
		System.debug('*ebizLogAgo.DataSyncStartDate__c* ' + dt);

		// 条件作成
		queryCondition = ' Where ';

		// 団体
		queryCondition += ' ( ';
		queryCondition += ' KFRAMEIND__c = \'Y\' ';
		queryCondition += ' And ';
		queryCondition += ' GroupParentFlag__c = True ';
		queryCondition += ' And ';
		//queryCondition += ' GroupStatusDate__c > ' + convertedDate;
		queryCondition += ' GroupStatusDate__c > :dt';
		queryCondition += ' And ';
		queryCondition += ' GroupStatus__c = \'' + I_NewPolicyConst.STATUS_NAME_I + '\'';
		queryCondition += ' ) ';

		// 団体以外
		queryCondition += ' Or ';
		queryCondition += ' ( ';
		queryCondition += ' KFRAMEIND__c <> \'Y\' ';
		queryCondition += ' And ';
		//queryCondition += ' StatusDate__c > ' + convertedDate;
		queryCondition += ' StatusDate__c > :dt';
		queryCondition += ' And ';
		queryCondition += ' Status__c = \'' + I_NewPolicyConst.STATUS_NAME_I + '\'';
		queryCondition += ' ) ';

		system.debug(queryCondition);
	}

	/**
	 * execute
	 * 新契約情報からPushOut通知レコードを作成する
	 */
	public override List<I_PushOut__c> execute(List<Sobject> records){
		List<I_PushOut__c> pushRecs = new List<I_PushOut__c>();

		// 対象ユーザのID管理マップ取得（キー → 募集人：募集人コード（5桁）、MR：UserId）
		Map<String, E_IDCPF__c> targetIdcpfMap = getTargetIdcpfMap(records);

		// ユーザ毎の送信する通知情報リスト（メール本文となる情報）のマップ作成
		Map<E_IDCPF__c, List<ContractInfo>> targetUserMap = createTargetUserMap(records, targetIdcpfMap);

		// PushOut通知レコード作成
		List<I_PushOut__c> pushOutRecs = createRecs(targetUserMap);

		return pushOutRecs;
	}

	/**
	 * finish
	 * UpsertKeyでマージしたレコードを登録する
	 */
	public override void finish(List<I_PushOut__c> recs){
		// PushOut通知レコードのマージ（メールアドレスでのマージ）
		List<I_PushOut__c> mergeRecords = mergeRecords(recs);

		// Insert
		insert mergeRecords;
	}

	/**
	 * PushOut通知　メール送信バッチ呼び出し
	 * 夜間の場合、メール送信は実行せずに、朝一のバッチで送信する
	 */
	public override void callSendBatch(E_BizDataSyncLog__c ebizlog){
		List<String> stopTimeList = new List<String>();

		// メール停止時間の取得
		String stopTime = System.Label.E_PROCESS_STOPTIME_NEWPOLICY;
		if(String.isNotBlank(stopTime)){
			stopTimeList = stopTime.split(',');
		}

		if(stopTimeList.size() == 2){
			// 現在時刻
			Datetime dt = System.now();
			Integer nowHour = Integer.valueOf(dt.hour());
			// メール停止時間
			Integer fromHour = Integer.valueOf(stopTimeList[0]);
			Integer toHour = Integer.valueOf(stopTimeList[1]);

			// メール停止時間に当てはまる場合、処理終了
			if(fromHour < toHour){
				if(fromHour <= nowHour && nowHour <= toHour){
					return;
				}
			}else{
				if(!(toHour < nowHour && nowHour < fromHour)){
					return;
				}
			}
		}

		// PushOut通知　メール送信バッチ呼び出し
		I_PushOutSendBatch sendBatch = new I_PushOutSendBatch(ebizlog);
		Database.executeBatch(sendBatch, 200);
	}

	/**
	 * UserをキーとしたID管理レコードマップ取得
	 */
	private Map<String, E_IDCPF__c> getTargetIdcpfMap(List<Sobject> records){
		Set<String> agentNums = new Set<String>();	// 募集人の募集人コード（5桁）
		Set<Id> userIds = new Set<Id>();			// 担当MRのUserId

		// UserをキーとしたID管理レコードマップ
		Map<String, E_IDCPF__c> idcpfMap = new Map<String, E_IDCPF__c>();

		// 対象のUser情報をID管理より取得
		for(Sobject record : records){
			E_NewPolicy__c policy = (E_NewPolicy__c)record;

			// 主募集人
			if(policy.Agent__r.Id != null){
				agentNums.add(policy.MainAgentNum5__c);
			}
			// 従募集人
			if(policy.SubAgent__r.Id != null){
				agentNums.add(policy.SubAgentNum5__c);
			}
			// 主担当MR ： 本社営業部（課コード「88」は対象外）
			if(policy.IsHqMR__c == false && policy.Account__r.OwnerId != null){
				userIds.add(policy.Account__r.OwnerId);
			}
			// 従担当MR ： 本社営業部（課コード「88」は対象外）
			if(policy.IsHqSubMR__c == false && policy.SubAccount__r.OwnerId != null){
				userIds.add(policy.SubAccount__r.OwnerId);
			}
		}

		// 募集人のID管理取得
		for(E_IDCPF__c conIdcpf : E_IDCPFDao.getNotificationRecsByContactAgentNum5Query(agentNums, false)){
			idcpfMap.put(conIdcpf.User__r.Contact.AgentNum5__c, conIdcpf);
		}

		// MRのID管理取得
		for(E_IDCPF__c userIdcpf : E_IDCPFDao.getNotificationRecsByUserIds(userIds)){
			idcpfMap.put(userIdcpf.User__r.Id, userIdcpf);
		}

		return idcpfMap;
	}

	/**
	 * ユーザ毎の送信する通知情報リスト（メール本文となる情報）のマップ作成
	 */
	private Map<E_IDCPF__c, List<ContractInfo>> createTargetUserMap(List<Sobject> records, Map<String, E_IDCPF__c> targetIdcpfMap){
		Map<E_IDCPF__c, List<ContractInfo>> targetUserMap = new Map<E_IDCPF__c, List<ContractInfo>>();

		// 団体処理対象の団体サマリーキー
		Set<String> groupKeys = new Set<String>();

		// レコードから契約単位の通知情報の生成する
		for(Sobject record : records){
			E_NewPolicy__c policy = (E_NewPolicy__c)record;

			// 団体以外 : 契約単位の通知情報の生成
			if(policy.KFRAMEIND__c != I_NewPolicyConst.KFRAMEIND_Y){
				ContractInfo cinfo = new ContractInfo(policy, null);
				for(E_IDCPF__c targetIdcpf : getIdcpfs(policy, targetIdcpfMap)){
					targetUserMap = setTargetUserMap(targetUserMap, targetIdcpf, cinfo);
				}

			// 団体 : 団体サマリーキーに追加
			}else{
				groupKeys.add(policy.GroupKey__c);
			}
		}

		// 団体の処理
		if(groupKeys.isEmpty() == false){
			Map<String, AggregateResult> chdrnumMap = new Map<String, AggregateResult>();
			Map<String, GroupAggregate> groupAggregateMap = new Map<String, GroupAggregate>();

			// 団体サマリーキー単位で、新契約情報を取得する
			for(AggregateResult groupPolicy : E_NewPolicyDao.getAggregatesByGroupKey(groupKeys, new Set<String>{I_NewPolicyConst.STATUS_NAME_I})){
				// 証券番号単位の団体集計情報の生成
				groupAggregateMap.put(
					(String)groupPolicy.get('chdrnum'),new GroupAggregate(
																(String)groupPolicy.get('GRUPKEY__c'),
																(String)groupPolicy.get('chdrnum'),
																(Integer)groupPolicy.get('cnt')));
			}

			// 証券番号をキーに必要な情報を取得する
			for(E_NewPolicy__c newPolicy : E_NewPolicyDao.getRecsByCHDRNUMForPush(groupAggregateMap.keySet())){
				// 契約単位の通知情報の生成
				ContractInfo gcinfo = new ContractInfo(newPolicy, groupAggregateMap.get(newPolicy.CHDRNUM__c));

				for(E_IDCPF__c targetIdcpf : getIdcpfs(newPolicy, targetIdcpfMap)){
					targetUserMap = setTargetUserMap(targetUserMap, targetIdcpf, gcinfo);
				}
			}
		}

		return targetUserMap;
	}

	/**
	 * 新契約レコードから対象のID管理取得
	 */
	private Set<E_IDCPF__c> getIdcpfs(E_NewPolicy__c policy, Map<String, E_IDCPF__c> targetIdcpfMap){
		Set<E_IDCPF__c> idcpfs = new Set<E_IDCPF__c>();

		// 取得対象のID
		Set<String> ids = new Set<String>{
			policy.MainAgentNum5__c			// 主募集人
			,policy.SubAgentNum5__c			// 従募集人
			,policy.Account__r.OwnerId		// 主MR
			,policy.SubAccount__r.OwnerId	// 従MR
		};
		for(String id : ids){
			E_IDCPF__c idcpf = String.isBlank(id) ? null : targetIdcpfMap.get(id);
			if(idcpf != null){
				idcpfs.add(idcpf);
			}
		}

		return idcpfs;
	}

	/**
	 * ID管理をキーとした契約者情報のセット
	 */
	private Map<E_IDCPF__c, List<ContractInfo>> setTargetUserMap(Map<E_IDCPF__c, List<ContractInfo>> targetUserMap, E_IDCPF__c idcpf, ContractInfo cinfo){
		// メールアドレスが取得できた場合のみ処理
		if(idcpf != null){
			List<ContractInfo> cinfos = new List<ContractInfo>();

			// 対象メールアドレスが存在する場合、保持している契約者情報をセット
			if(targetUserMap.get(idcpf) != null){
				cinfos = targetUserMap.get(idcpf);
			}
			cinfos.add(cinfo);
			targetUserMap.put(idcpf, cinfos);
		}

		return targetUserMap;
	}

	/**
	 * PushOut通知レコード作成
	 */
	private List<I_PushOut__c> createRecs(Map<E_IDCPF__c, List<ContractInfo>> targetUserMap){
		List<I_PushOut__c> pushOutRecs = new List<I_PushOut__c>();

		// PushOut通知作成
		for(E_IDCPF__c key : targetUserMap.keySet()){
			Boolean isEmployee = key.User__r.UserType == E_Const.USERTYPE_STANDARD ? true : false;
			I_PushOut__c pushOutRec = createRecordByIdcpf(key, I_Const.EBIZDATASYNC_KBN_NEWPOLICY, isEmployee, 'yyyyMMddHHmm');

			// 本文
			String body = '';
			for(ContractInfo ci : targetUserMap.get(key)){
				body += I_Const.BREAK_LINE;
				body += I_Const.BREAK_LF;
				body += '契約者： ' + E_Util.null2Blank(ci.name);
				body += I_Const.BREAK_LF;
				body += '被保険者： ' + E_Util.null2Blank(ci.insuredName);
				body += I_Const.BREAK_LF;
				body += '証券番号： ' + E_Util.null2Blank(ci.chdrnum);
				body += I_Const.BREAK_LF;
				body += '団体番号： ' + E_Util.null2Blank(ci.groupnum);
				body += I_Const.BREAK_LF;
				body += '保険種類： ' + E_Util.null2Blank(ci.insuranceType);
				body += I_Const.BREAK_LF;
				// 社員のみ
				if(isEmployee == true){
					body += '代理店名： ' + E_Util.null2Blank(ci.accName);
					body += I_Const.BREAK_LF;
					body += '募集人名： ' + E_Util.null2Blank(ci.agentName);
					body += I_Const.BREAK_LF;
				}
			}
			pushOutRec.Body__c = body;

			pushOutRecs.add(pushOutRec);
		}

		return pushOutRecs;
	}

	/**
	 * PushOut通知レコードのマージ（メールアドレスを含むUpsertKeyでのマージ）
	 */
	protected override List<I_PushOut__c> mergeRecords(List<I_PushOut__c> recs){
		List<I_PushOut__c> mergeRecords = new List<I_PushOut__c>();

		Map<String, I_PushOut__c> upsertKeyMap = new Map<String, I_PushOut__c>();
		for(I_PushOut__c rec : recs){
			I_PushOut__c baseRec = upsertKeyMap.get(rec.UpsertKey__c);

			// 対象メールアドレスが存在する場合、本文を結合する
			if(baseRec != null){
				baseRec.Body__c += rec.Body__c;
				upsertKeyMap.put(rec.UpsertKey__c, baseRec);

			// 対象メールアドレスが存在しない場合、マップ追加
			}else{
				upsertKeyMap.put(rec.UpsertKey__c, rec);
			}
		}

		// 本文の最後の区切り線を追加
		for(String key : upsertKeyMap.keySet()){
			I_PushOut__c mergeRecord = upsertKeyMap.get(key);
			mergeRecord.Body__c += I_Const.BREAK_LINE;
			mergeRecord.Body__c += I_Const.BREAK_LF;
			mergeRecords.add(mergeRecord);
		}

		return mergeRecords;
	}
}