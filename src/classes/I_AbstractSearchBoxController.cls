public abstract class I_AbstractSearchBoxController{
// 入力
	// 検索項目（選択値）
	public String searchType {get; set;}
	// 検索項目（選択値・名前）
	public String searchTypeName {get; set;}
	// 検索項目選択リスト
	public List<SelectOption> searchTypeItems {get; set;}

	// 検索テキスト
	public String keyword {get; set;}

// 出力
	// 最近検索した項目
	public List<SearchBoxLog> searchBoxLog {get; set;}
	// メッセージ
	public List<String> searchMessages {get; set;}

// 制御
	// アクセス権判定
	public E_AccessController access; 
	// 検索カテゴリ
	public String searchCategory {get; set;}
	// 検索項目（検索値）
	public String exeSearchType;
	// 検索済みフラグ
	public Boolean isSearch {get; set;}
	// 表示件数
	public Integer rowCount {get; set;}
	// 遷移先の選択メニューMap
	public Map<String, I_MenuItem> menuMap {get; set;}
	// MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}
	// メッセージあり
	public Boolean getHasMessages(){
		return searchMessages != null && searchMessages.size() > 0;
	}

	//JIRA_#64 0406
	// [IRIS]
	public E_IRISHandler iris {get; private set;}


// ソート用
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}


// 定数
	// 検索ログ用キー
	public static final String LOG_KEY_SEARCH_TYPE = 'searchType';
	public static final String LOG_KEY_SEARCH_TYPE_NAME = 'searchTypeName';
	public static final String LOG_KEY_KEYWORD = 'keyword';

	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}
	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}


	// コンストラクタ
	public I_AbstractSearchBoxController(){
		searchMessages = new List<String>();
		access = E_AccessController.getInstance();
		//JIRA_#64 0406
		iris = E_IRISHandler.getInstance();
	}

	/**
	 * 保険契約ヘッダのレコードタイプ判定
	 * @param E_Policy__c
	 * @return Boolean　:　True -> 個人保険ヘッダ、SPVAヘッダ（消滅以外の契約）
	 */
	public Boolean isEnablePolicy(E_Policy__c policy){
		// 個人保険ヘッダ,　SPVAヘッダ
		if(policy.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI
		|| policy.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA){
			return true;
		}

		return false;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * 検索タイプ変更時のログ再取得
	 */
	public void refreshLog(){
		String searchType = ApexPages.currentPage().getParameters().get('searchType');
		if(String.isNotBlank(searchType)){
			getSeachBoxLogRecord(searchType);
		}

		return;
	}

	/**
	 * 検索ログを取得
	 */
	public void getSeachBoxLogRecord(String searchType){
		List<E_Log__c> logs = new List<E_Log__c>();
//		logs = E_LogDao.getSearchLog(searchCategory);
		//JIRA_#63 170331
		// [TODO]最近検索したキーワードをカテゴリ、項目ごとにするときに↓に変更
		logs = E_LogDao.getSearchLog(searchCategory + '>' + searchType);

		Set<String> logKeys = new Set<String>();
		searchBoxLog = new List<SearchBoxLog>();
		for(E_log__c log : logs){
			
			// デシリアライズの際に[改行,リターン,タブ,改ページ]を含んでいた際にスペースへ置換するよう修正 
			String detail = (String.isNotBlank(log.detail__c) ? log.detail__c : '').replaceAll('[\r\n\t\f]', ' ');
			Map<string, string> valueMap = (Map<String, String>)JSON.deserialize(detail, Map<String, String>.class);
			String logkey = String.join(valueMap.values(), '|');
			if(!logkeys.contains(logkey)){
				SearchBoxLog slog = new SearchBoxLog();

				slog.keyword    = valueMap.get(LOG_KEY_KEYWORD);
				slog.searchType = valueMap.get(LOG_KEY_SEARCH_TYPE);
				List<String> logNames = log.Name.split('>');
				if(logNames != null && logNames.size() > 1){
					slog.searchTypeName = logNames.get(1);
				}

				searchBoxLog.add(slog);
				if(searchBoxLog.size() == I_Const.SEARCH_BOX_LOG_NUM){
					break;
				}
				logkeys.add(logkey);
			}
		}
	}

	/**
	 * Referer設定メソッド（E_CustomerInfo, E_InsuredInfo）
	 */
	public void setRefererContactSearch(){
		E_CookieHandler.setCookieContactReferer(Page.IRIS_Top.getUrl());
		return;
	}

	/**
	 * Referer設定メソッド（E_Coli, E_Spva, E_DColi, E_DSpva, E_Annuity）
	 */
	public void setRefererPolicySearch(){
		E_CookieHandler.setCookieRefererPolicy(Page.IRIS_Top.getUrl());
		return;
	}

	/**
	 * 入力チェック
	 */
	protected boolean isInputValidate(){
		// 検索キーワードが未入力の場合はエラー
		if(String.isBlank(keyword)){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_KEYWORD_NOTHING));
			return false;
		}

		return true;
	}

	// 最近検索したキーワード用
	public class SearchBoxLog {
		// キーワード
		public String keyword {get; set;}
		// 検索タイプ（検索項目）
		public String searchType {get; set;}
		// 検索タイプ（名前）
		public String searchTypeName {get; set;}

		public SearchBoxLog(){
			keyword = '';
			searchType = '';
			searchTypeName = '';
		}
	}
}