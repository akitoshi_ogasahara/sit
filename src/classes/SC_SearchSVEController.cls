global with sharing class SC_SearchSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public SC_Office__c record{get;set;}	
			
	public SC_SearchExtender getExtender() {return (SC_SearchExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component90_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component90_to{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component53_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component53_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component53_op{get;set;}	
		public List<SelectOption> valueOptions_SC_Office_c_FiscalYear_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c limitSearch_val {get;set;}	
		public SkyEditor2.TextHolder limitSearch_op{get;set;}	
		public List<SelectOption> valueOptions_SC_Office_c_LimitFixed_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
		public List<SelectOption> valueOptions_SC_Office_c_Status_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component12_val {get;set;}	
		public SkyEditor2.TextHolder Component12_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component14_val {get;set;}	
		public SkyEditor2.TextHolder Component14_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component16_val {get;set;}	
		public SkyEditor2.TextHolder Component16_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component87_val {get;set;}	
		public SkyEditor2.TextHolder Component87_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component18_val {get;set;}	
		public SkyEditor2.TextHolder Component18_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component20_val {get;set;}	
		public SkyEditor2.TextHolder Component20_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component48_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component48_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component48_op{get;set;}	
		public List<SelectOption> valueOptions_SC_Office_c_SCPlan_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component57_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component57_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component57_op{get;set;}	
		public List<SelectOption> valueOptions_SC_Office_c_Defect_c_multi {get;set;}
			
	public String recordTypeRecordsJSON_SC_Office_c {get; private set;}
	public String defaultRecordTypeId_SC_Office_c {get; private set;}
	public String metadataJSON_SC_Office_c {get; private set;}
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	{
	setApiVersion(31.0);
	}
		public SC_SearchSVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = SC_Office__c.fields.FiscalYear__c;
		f = SC_Office__c.fields.LimitFixed__c;
		f = SC_Office__c.fields.IsActiveOffice__c;
		f = SC_Office__c.fields.Status__c;
		f = SC_Office__c.fields.OwnerMRUnitName__c;
		f = SC_Office__c.fields.IsHeadSection__c;
		f = SC_Office__c.fields.OwnerMRName__c;
		f = SC_Office__c.fields.OwnerMRCode__c;
		f = SC_Office__c.fields.HasAgManagerMail__c;
		f = SC_Office__c.fields.ZEAYNAM__c;
		f = SC_Office__c.fields.ZAGCYNUM__c;
		f = SC_Office__c.fields.SCPlan__c;
		f = SC_Office__c.fields.Defect__c;
		f = SC_Office__c.fields.ToView__c;
		f = SC_Office__c.fields.DispDefect__c;
		f = SC_Office__c.fields.CMDRemandDate__c;
		f = SC_Office__c.fields.LimitFixedNum__c;
 f = SC_Office__c.fields.CMDRemandDate__c;
		f = SC_Office__c.fields.BusinessStatus__c;
		f = SC_Office__c.fields.AccZMRCODE__c;

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_targetClassName','searchDateFieldItems');
		tmpPropMap.put('Component__Width','20');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component92');
		tmpPropMap.put('Component__Name','E_DatePicker');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component92',tmpPropMap);

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = SC_Office__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component90_from = new SkyEditor2__SkyEditorDummy__c();	
				Component90_to = new SkyEditor2__SkyEditorDummy__c();	
					
				Component53_val = new SkyEditor2__SkyEditorDummy__c();	
				Component53_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component53_op = new SkyEditor2.TextHolder('inx');	
				valueOptions_SC_Office_c_FiscalYear_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : SC_Office__c.FiscalYear__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_SC_Office_c_FiscalYear_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				limitSearch_val = new SkyEditor2__SkyEditorDummy__c();	
				limitSearch_op = new SkyEditor2.TextHolder('eq');	
				valueOptions_SC_Office_c_LimitFixed_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : SC_Office__c.LimitFixed__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_SC_Office_c_LimitFixed_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder('eq');	
					
				Component6_val = new SkyEditor2__SkyEditorDummy__c();	
				Component6_op = new SkyEditor2.TextHolder('eq');	
				valueOptions_SC_Office_c_Status_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : SC_Office__c.Status__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_SC_Office_c_Status_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder('co');	
					
				Component12_val = new SkyEditor2__SkyEditorDummy__c();	
				Component12_op = new SkyEditor2.TextHolder('eq');	
					
				Component14_val = new SkyEditor2__SkyEditorDummy__c();	
				Component14_op = new SkyEditor2.TextHolder('co');	
					
				Component16_val = new SkyEditor2__SkyEditorDummy__c();	
				Component16_op = new SkyEditor2.TextHolder('eq');	
					
				Component87_val = new SkyEditor2__SkyEditorDummy__c();	
				Component87_op = new SkyEditor2.TextHolder('eq');	
					
				Component18_val = new SkyEditor2__SkyEditorDummy__c();	
				Component18_op = new SkyEditor2.TextHolder('co');	
					
				Component20_val = new SkyEditor2__SkyEditorDummy__c();	
				Component20_op = new SkyEditor2.TextHolder('eq');	
					
				Component48_val = new SkyEditor2__SkyEditorDummy__c();	
				Component48_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component48_op = new SkyEditor2.TextHolder('inx');	
				valueOptions_SC_Office_c_SCPlan_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : SC_Office__c.SCPlan__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_SC_Office_c_SCPlan_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component57_val = new SkyEditor2__SkyEditorDummy__c();	
				Component57_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component57_op = new SkyEditor2.TextHolder('inx');	
				valueOptions_SC_Office_c_Defect_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : SC_Office__c.Defect__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_SC_Office_c_Defect_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('SC_Office__c')
						.addFieldAsOutput('ToView__c')
						.addFieldAsOutput('FiscalYear__c')
						.addFieldAsOutput('LimitFixedNum__c')
						.addFieldAsOutput('Status__c')
						.addFieldAsOutput('OwnerMRUnitName__c')
						.addFieldAsOutput('OwnerMRName__c')
						.addFieldAsOutput('ZEAYNAM__c')
						.addFieldAsOutput('ZAGCYNUM__c')
						.addFieldAsOutput('HasAgManagerMail__c')
						.addFieldAsOutput('SCPlan__c')
						.addFieldAsOutput('DispDefect__c')
						.addFieldAsOutput('CMDRemandDate__c')
						.limitRecords(350)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component90_from, 'SkyEditor2__Date__c', 'CMDRemandDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component90_to, 'SkyEditor2__Date__c', 'CMDRemandDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component53_val_dummy, 'SkyEditor2__Text__c','FiscalYear__c', Component53_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(limitSearch_val, 'SkyEditor2__Text__c', 'LimitFixed__c', limitSearch_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Checkbox__c', 'IsActiveOffice__c', Component8_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'SkyEditor2__Text__c', 'Status__c', Component6_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'OwnerMRUnitName__c', Component10_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component12_val, 'SkyEditor2__Checkbox__c', 'IsHeadSection__c', Component12_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component14_val, 'SkyEditor2__Text__c', 'OwnerMRName__c', Component14_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component16_val, 'SkyEditor2__Text__c', 'OwnerMRCode__c', Component16_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component87_val, 'SkyEditor2__Checkbox__c', 'HasAgManagerMail__c', Component87_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component18_val, 'SkyEditor2__Text__c', 'ZEAYNAM__c', Component18_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component20_val, 'SkyEditor2__Text__c', 'ZAGCYNUM__c', Component20_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component48_val_dummy, 'SkyEditor2__Text__c','SCPlan__c', Component48_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component57_val_dummy, 'SkyEditor2__Text__c','Defect__c', Component57_op, true, 0, true ))
.addSort('Status__c',True,False).addSort('BusinessStatus__c',True,False).addSort('OwnerMRUnitName__c',True,False).addSort('AccZMRCODE__c',True,False).addSort('ZAGCYNUM__c',True,False)
				);	
					
					resultTable = new resultTable(new List<SC_Office__c>(), new List<resultTableItem>(), new List<SC_Office__c>(), null);
				resultTable.ignoredOnSave = true;
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(SC_Office__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new SC_SearchExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_SC_Office_c_FiscalYear_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_LimitFixed_c() { 
			return getOperatorOptions('SC_Office__c', 'LimitFixed__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_IsActiveOffice_c() { 
			return getOperatorOptions('SC_Office__c', 'IsActiveOffice__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_Status_c() { 
			return getOperatorOptions('SC_Office__c', 'Status__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_OwnerMRUnitName_c() { 
			return getOperatorOptions('SC_Office__c', 'OwnerMRUnitName__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_IsHeadSection_c() { 
			return getOperatorOptions('SC_Office__c', 'IsHeadSection__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_OwnerMRName_c() { 
			return getOperatorOptions('SC_Office__c', 'OwnerMRName__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_OwnerMRCode_c() { 
			return getOperatorOptions('SC_Office__c', 'OwnerMRCode__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_HasAgManagerMail_c() { 
			return getOperatorOptions('SC_Office__c', 'HasAgManagerMail__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_ZEAYNAM_c() { 
			return getOperatorOptions('SC_Office__c', 'ZEAYNAM__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_ZAGCYNUM_c() { 
			return getOperatorOptions('SC_Office__c', 'ZAGCYNUM__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_SCPlan_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_Defect_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public SC_Office__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, SC_Office__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (SC_Office__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}