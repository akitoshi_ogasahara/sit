@isTest
private class TestI_TopSumiseiController {

	// IRIS住生ユーザーの場合は IRIS_TopSumisei を表示 // 利用アプリを IRIS に更新する
	static testMethod void sumiseiUserTest01(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);
			insert new I_PageMaster__c(Name = 'その他', page_unique_key__c = 'iris_FAQ_Sumisei_8');
		}
		System.runAs(uc){
			TestI_TestUtil.createBasePermissions(uc.Id);
			TestI_TestUtil.createAgencyPermissions(uc.Id);
			Test.startTest();
			I_TopSumiseiController instance = new I_TopSumiseiController();
			PageReference ret = instance.pageAction();
			System.assertEquals(ret,null);
			Test.stopTest();
		}
	}

	// IRIS住生ユーザーの場合は IRIS_TopSumisei を表示 // 利用アプリを IRIS に更新する
	static testMethod void sumiseiUserTest02(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',false);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);


		}
		System.runAs(uc){
			TestI_TestUtil.createBasePermissions(uc.Id);
			TestI_TestUtil.createAgencyPermissions(uc.Id);
			Test.startTest();
			I_TopSumiseiController instance = new I_TopSumiseiController();
			PageReference ret = instance.pageAction();
			System.assertEquals(ret.getUrl(),Page.E_Home.getUrl());
			Test.stopTest();
		}
	}

	// 社員の場合はリダイレクトしない
	static testMethod void employeeUserTest01(){
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');

		System.runAs(actUser){
			TestI_TestUtil.createBasePermissions(actUser.Id);
			TestI_TestUtil.createIDCPF(true, actUser.Id, 'BR');
			TestI_TestUtil.createEmployeePermissions(actUser.Id);
			 //E_IRISHandler
			E_IRISHandler iris = E_IRISHandler.getInstance();
			I_TopSumiseiController instance = new I_TopSumiseiController();
			PageReference ret = instance.pageAction();
			//System.assertEquals(ret.getUrl(),'/apex/e_appswitch?app=NNLink');
			System.assertEquals(ret,null);       //
		}
	}

	static testMethod void getTopMenusTest(){
		I_TopSumiseiController instance = new I_TopSumiseiController();
		Map<String, I_MenuItem> ret = instance.getTopMenus();
		System.assertNotEquals(null, ret);
	}
}