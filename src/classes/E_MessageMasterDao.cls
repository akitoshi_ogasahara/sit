/**
 *		E_MessageMasterDao
 *			メッセージ関連DAO（メッセージ＆ファイル）
 *
 */
public with sharing class E_MessageMasterDao {

	public static E_MessageMaster__c getRecByCode (String code) {
		E_MessageMaster__c rec = null;
		List<E_MessageMaster__c> recs = 
			[
				SELECT
					id,
					Key__c,
					UpsertKey__c,
					UpsertKeyValue__c,
					Menu__c,
					Type__c,
					Value__c,
					DispFrom__c,
					DispTo__c
				FROM E_MessageMaster__c
				WHERE Key__c = :code
			];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}
	
	/**
	 *		Keyとメッセージ文言のMap
	 *			@sType  種別（メッセージ / ディスクレイマー）
	 */
	public static Map<String, String> getMessageMapByType(String sType){
		Map<String, String> ret = new Map<String,String>();
		for(E_MessageMaster__c msg:[Select e.Value__c, e.Type__c, e.Name, e.Key__c, e.Id 
									From E_MessageMaster__c e
									where Type__c = :sType]){
			ret.put(msg.Key__c, msg.Value__c);							
		}
		return ret;	
	
	}
	
	/**
	 *		CMS用メッセージ取得
	 *			今日の日付値のレコードを取得する。
	 */
	public static List<E_MessageMaster__c> getMessagesByMenuId(String menuId){
		List<E_MessageMaster__c> ret = new List<E_MessageMaster__c>();
		for(E_MessageMaster__c rec :[SELECT Id,Name 
										, isHTML__c 
										, NameToSectionTitle__c 
										, value__c
										, DispFrom__c 
										, DispTo__c
										, HTML_Dom_Id__c
										, ShowAdobeReader__c
										, ShowPDFUsage__c 
										, ShowSubPageLink__c
										, ShowPicsByAttachment__c
										, ShowHTMLByAttachment__c 
										, ShowTopOfThePage__c
										, ShowBorderBottom__c
										, Menu__r.MenuMasterKey__c
										,(select id,name,recordType.DeveloperName,contentsName__c 
											from E_CMSFiles__r 
											order by sortOrder__c nulls last)		//添付ファイル管理用オブジェクト
										From E_MessageMaster__c
										Where Menu__c = :menuId
										Order By Key__c, DispFrom__c nulls first, DispTo__c nulls last]){
			if((rec.DispFrom__c == null || rec.DispFrom__c <= System.Now()) 
				&&
				(rec.DispTo__c ==null || rec.DispTo__c.addDays(1) > System.Now())){
				ret.add(rec);
			}
		}
		return ret;
	}

	/**
	 *		コンテンツファイルリスト取得
	 *			タイトルを条件にコンテンツのリストを取得
	 */
	public static ContentVersion[] getContentVersionsByTitles(Set<String> titles){
		return [SELECT Id,LastModifiedDate,Title,ContentSize,PathOnClient 
				 From ContentVersion
				WHERE Title in:titles
				  AND IsLatest = true];
	}

	/**
	 *		CMSファイルリスト取得
	 *			添付ファイルを含めて、CMSファイルリストを再取得する
	 */
	public static E_CMSFile__c[] getCMSFilesWithAttachmentByIds(List<E_CMSFile__c> recs){
		return [SELECT Id,Name,RecordType.DeveloperName
						, ContentsName__c
						, Note__c	//補足
						, sortOrder__c
						, exVersion__c
						, Version__c
						, ReleaseDate__c
						, MessageMaster__c
						, IconFileName__c
						, FormVersion__c
						, UpdateDate__c

						//最新の添付ファイルを取得
						,(select Id,Name,ContentType, BodyLength, LastModifiedDate 
						  from Attachments
						  Order By LastModifiedDate DESC, Id DESC			//万が一最終更新日が同じ場合はIDの降順
						  limit 1)
				 FROM E_CMSFile__c
				WHERE Id in :recs
				ORDER BY SortOrder__c nulls last
						, Id];	
	}

	/**
	 *		CMSファイルリスト取得		IRIS　CMSで利用　
	 *			UpsertKeyによる抽出	添付ファイルを含めて、CMSファイルリストを取得する
	 */
	public static Map<String, E_CMSFile__c> getCMSFilesWithAttachmentByKeys(Set<String> keys){
		Map<String, E_CMSFile__c> ret = new Map<String, E_CMSFile__c>();
		for(E_CMSFile__c cmsfile: [SELECT Id,Name,RecordType.DeveloperName
						, ContentsName__c
						, Note__c	//補足
						, sortOrder__c
						, exVersion__c
						, Version__c
						, ReleaseDate__c
						, MessageMaster__c
						, IconFileName__c
						, FormVersion__c
						, UpdateDate__c
						, UpsertKey__c
						//最新の添付ファイルを取得
						,(select Id,Name,ContentType, BodyLength, LastModifiedDate 
						  from Attachments
						  Order By LastModifiedDate DESC, Id DESC			//万が一最終更新日が同じ場合はIDの降順
						  limit 1)
				 FROM E_CMSFile__c
				WHERE UpsertKey__c in :keys
				ORDER BY SortOrder__c nulls last
						, Id]){
			ret.put(cmsfile.upsertkey__c, cmsfile);			
		}	
		return ret;
	}

	/**
	 *		添付ファイルリスト取得
	 *			メッセージマスタレコードに直添付されているファイルリストを取得
	 */
	public static Attachment[] getAttachmentsByMenuId(String msgId){
		return [SELECT Id,Name,Description,Body 
				FROM Attachment
				WHERE parentId = :msgId
				ORDER By Name, Id];
	}
	

	/**
	 *		添付ファイルリスト取得
	 *			メッセージマスタレコードに直添付されているファイルリストを取得
	 */
	public static Attachment[] getAttachmentsByMessageId(String msgId){
		return [SELECT Id,Name,Description,Body,ParentId
				FROM Attachment
				WHERE parentId = :msgId AND Name like '%.html'
				ORDER By Name, Id];
	}
	
    /**
     * メニューマスタレコードをIdに取得
     * @param MenukndId: メニューId
     * @return E_MenuMaster__c
     */
    public static E_MenuMaster__c getMenuMasterRecById(Id menuId){
        for(E_MenuMaster__c menu: [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c,IsHideMenu__c, IsShowCloseBtn__c 
        									,IsStandardUserOnly__c, IsPartnerUserOnly__c, UseCommonGW__c, UseInternet__c
                                     FROM E_MenuMaster__c 
                                    WHERE Id = :menuId
                                    Order By Id]){
            return menu;
        }
        return null;
    }

    /**
     * メニューマスタレコードをKeyに取得
     * @param menuKey: メニューKey
     * @return E_MenuMaster__c
     */
    public static E_MenuMaster__c getMenuMasterRecByKey(String menukey){
    	if(String.isBlank(menuKey)) return null;
        for(E_MenuMaster__c menu: [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c,IsHideMenu__c, IsShowCloseBtn__c 
        									,IsStandardUserOnly__c, IsPartnerUserOnly__c, UseCommonGW__c, UseInternet__c
                                     FROM E_MenuMaster__c 
                                    WHERE MenuMasterKey__c = :menukey
                                    Order By Id]){
            return menu;
        }
        return null;
    }

    /**
     * メニューをKeyにそのサブページを取得取得
     * @param menuKey: メニューKey
     * @return List<E_MenuMaster__c>
     */
    public static List<E_MenuMaster__c> getSubPagesByKey(String menukey){
        return [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c , IconsUrl__c,IsHideMenu__c, IsShowCloseBtn__c
                                     FROM E_MenuMaster__c 
                                    WHERE SelectedMenuKey__c = :menukey
                                      AND MenuMasterKey__c != :menukey      //サブページなので自分自身は取得しない
                                    Order By SortNumberAll__c];
    }
    
    /**
     * メニューマスタレコードをKeyにLike検索で取得
     * @param menuKey: メニューKey
     * @return E_MenuMaster__c
     */
    public static E_MenuMaster__c getMenuMasterRecByLikeKey(String menukey){
    	String menukeystr = '%' + menukey + '%';
        for(E_MenuMaster__c menu: [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c,IsHideMenu__c, IsShowCloseBtn__c 
                                     FROM E_MenuMaster__c 
                                    WHERE MenuMasterKey__c Like :menukeystr
                                    Order By Id]){
            return menu;
        }
        return null;
    }

    /**
     * メニューマスタレコードをKeyにメッセージマスタ情報リスト取得(Limit 100)
     * @param menuKey: メニューKey
     * @return List<E_MessageMaster__c>
     */
    public static List<E_MessageMaster__c> getRecsByMenuKey(String menukey, Integer limitCnt){
		List<E_MessageMaster__c> ret = new List<E_MessageMaster__c>();
		for(E_MessageMaster__c rec :[Select
										 Id, Name, Value__c, Key__c, DispFrom__c, DispTo__c, Menu__r.MenuMasterKey__c
									 From
									 	E_MessageMaster__c
									 Where
									 	Menu__r.MenuMasterKey__c = :menukey
									 Order By
									 	Key__c desc, DispFrom__c desc nulls first, DispTo__c asc nulls last Limit :limitCnt]){
			if((rec.DispFrom__c == null || rec.DispFrom__c <= System.Now()) 
				&& (rec.DispTo__c == null || rec.DispTo__c.addDays(1) > System.Now())){
				ret.add(rec);
			}
		}
		return ret;
	}
}