public class E_UPMRRedirect001Controller extends E_AbstractUnitPriceRedirectHandler {

	/* コンストラクタ */
	public E_UPMRRedirect001Controller(){
        super();
        pgTitle = '特別勘定の月次運用レポート';
        redirectPref = Page.upmr002;
    }
}