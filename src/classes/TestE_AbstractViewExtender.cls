@isTest
private class TestE_AbstractViewExtender
{
	/**
	 * E_AbstractControllerの網羅テスト1(異常系)
	 */
	private static testMethod void testAbstractViewExtender001() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            E_ProcessHistoryExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}

	/**
	 * E_AbstractControllerの網羅テスト2(異常系)
	 */
	private static testMethod void testAbstractViewExtender002() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;
		Id policyId = policy.id;
		delete policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

            // 不正なpolicyIdを設定
			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policyId);
			System.assertNotEquals(null,reference);
			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト3 個人ヘッダ(正常系)
	 */
	private static testMethod void testAbstractViewExtender003() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
	    RecordType rtype = [Select Id From RecordType Where DeveloperName = 'ECHDPF' Limit 1];
		E_Policy__c policy = new E_Policy__c();
		policy.comm_crtable__c = 'SR01';
        policy.RecordTypeId = rtype.Id;
		insert policy;
        PageReference pref = Page.E_Spva;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト4 SPVAヘッダ(正常系)
	 */
	private static testMethod void testAbstractViewExtender004() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
	    RecordType rtype = [Select Id From RecordType Where DeveloperName = 'ESPHPF' Limit 1];
		E_Policy__c policy = new E_Policy__c();
		policy.comm_crtable__c = 'SR01';
        policy.RecordTypeId = rtype.Id;
		insert policy;
        PageReference pref = Page.E_Spva;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト5 個人保険消滅(正常系)
	 */
	private static testMethod void testAbstractViewExtender005() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
	    RecordType rtype = [Select Id From RecordType Where DeveloperName = 'EHLDPF' Limit 1];
		E_Policy__c policy = new E_Policy__c();
        policy.RecordTypeId = rtype.Id;
		insert policy;
        PageReference pref = Page.E_Spva;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト6 SPVA消滅(正常系)
	 */
	private static testMethod void testAbstractViewExtender006() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
	    RecordType rtype = [Select Id From RecordType Where DeveloperName = 'EHDSPF' Limit 1];
		E_Policy__c policy = new E_Policy__c();
        policy.RecordTypeId = rtype.Id;
		insert policy;
        PageReference pref = Page.E_Spva;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト7(異常系)
	 */
	private static testMethod void testAbstractViewExtender007() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            E_ProcessHistoryExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.DOWNLOAD, policy.id);
			System.assertEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト8 policyID不正(異常系)
	 */
	private static testMethod void testAbstractViewExtender008() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		//insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            E_ProcessHistoryExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
//****************

	/**
	 * E_AbstractControllerの網羅テスト9(異常系)
	 */
	private static testMethod void testAbstractViewExtender009() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_InsuredInfoController controller = new E_InsuredInfoController(standardcontroller);
            E_InsuredInfoExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.CONTACT, policy.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト10(異常系)
	 */
	private static testMethod void testAbstractViewExtender010() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		//E_Policy__c policy = new E_Policy__c();
		//insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(conCust);
            E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
            E_CustomerInfoExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.CONTACT, conCust.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト11(異常系)
	 */
	private static testMethod void testAbstractViewExtender011() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		//E_Policy__c policy = new E_Policy__c();
		//insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(conCust);
            E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            E_ProcessHistoryExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.CONTACT, conCust.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト12(異常系)
	 */
	private static testMethod void testAbstractViewExtender012() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            //Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(conCust);
            //E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            //E_ProcessHistoryExtender extender = controller.getExtender();

            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.CONTACT, conCust.id);
			System.assertEquals(null,reference);

			Test.stopTest();
		}
	}

	/**
	 * E_AbstractControllerの網羅テスト13 アクセス権エラー(異常系)
	 */
	private static testMethod void testAbstractViewExtender013() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.INVESTMENT, conCust.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}

	/**
	 * E_AbstractControllerの網羅テスト14 ID不備(異常系)
	 */
	private static testMethod void testAbstractViewExtender014() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		policy.comm_crtable__c = 'SR01';
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.INVESTMENT, conCust.id);
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * E_AbstractControllerの網羅テスト15 (異常系)
	 */
	private static testMethod void testAbstractViewExtender015() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
		E_ITHPF__c ithpf = new E_ITHPF__c();
		insert ithpf;
		E_Policy__c policy = new E_Policy__c();
		policy.comm_crtable__c = 'SR01';
		insert policy;
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpvaController controller = new E_SpvaController(standardcontroller);
            E_SpvaExtender extender = controller.getExtender();

			PageReference reference = extender.doAuth(E_Const.ID_KIND.INVESTMENT, ithpf.id);
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			System.assertNotEquals(null,reference);

			Test.stopTest();
		}
	}
	/**
	 * コードカバー率上げるために追加
	 */
	private static testMethod void forCoverage01() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
		// ANNUITYの場合
	    RecordType rtype = [Select Id From RecordType Where DeveloperName = 'ESPHPF' Limit 1];
		E_Policy__c policy = new E_Policy__c();
		policy.comm_crtable__c = 'SR01';
        policy.RecordTypeId = rtype.Id;
        policy.SPVA_ZANNSTFLG__c = true;
        policy.SPVA_ZVWDCF__c = '0';
		insert policy;
        PageReference pref = Page.E_Annuity;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
			ApexPages.currentPage().getHeaders().put('Referer', 'User-Agent');
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_AnnuityController controller = new E_AnnuityController(standardcontroller);
            E_AnnuityExtender extender = controller.getExtender();
			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertEquals(null,reference);

			Test.stopTest();
		}
	}
	private static testMethod void forCoverage02() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
		// ANNUITYの場合
	    RecordType rtype = [Select Id From RecordType Where DeveloperName = 'ESPHPF' Limit 1];
		E_Policy__c policy = new E_Policy__c();
		policy.comm_crtable__c = 'SR01';
        policy.RecordTypeId = rtype.Id;
        policy.SPVA_ZANNSTFLG__c = true;
        policy.SPVA_ZVWDCF__c = '0';
		insert policy;
        PageReference pref = Page.E_Portfolio;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
			ApexPages.currentPage().getHeaders().put('Referer', 'User-Agent');
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_PortfolioController controller = new E_PortfolioController(standardcontroller);
            E_PortfolioExtender extender = controller.getExtender();
			PageReference reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertEquals(null,reference);

			Test.stopTest();
		}
	}
	private static testMethod void forCoverage03() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;

        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            ApexPages.currentPage().getHeaders().put('Referer', 'User-Agent');
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(conCust);
            E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            E_ProcessHistoryExtender extender = controller.getExtender();

			extender.setRefererInve();

			Test.stopTest();
		}
	}
	private static testMethod void forCoverage04() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		
        PageReference pref = Page.E_ProcessHistory;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
            Test.setCurrentPage(pref);
            ApexPages.currentPage().getHeaders().put('Referer', 'User-Agent');
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(conCust);
            E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
            E_ProcessHistoryExtender extender = controller.getExtender();

			extender.setRefererHistory(true);

			Test.stopTest();
		}
	}
}