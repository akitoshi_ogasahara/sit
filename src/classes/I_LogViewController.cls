public with sharing class I_LogViewController extends I_AbstractController {
// 出力
	// ログ一覧(表示用)
	public List<logListRow> logRows {get; set;}
	// ヘッダ用
	public User agentInfo {get; set;}

// 制御
	//MAX表示件数
	public Integer getListMaxRows() {
		return I_Const.LIST_MAX_ROWS;
	}
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get; set;}
	// リスト行数
	public Integer rowCount {get; set;}

// ソート
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE = 'st';
	// ソートキー
	public static final String SORT_TYPE_ACCESS_DATETIME = 'accessDatetime';
	public static final String SORT_TYPE_LOG_TYPE = 'logType';
	public static final String SORT_TYPE_LOG_NAME = 'logName';

	/**
	 * コンストラクタ
	 */
	public I_LogViewController() {
		super();
		pageMessages = getPageMessages();

		// 変数の初期化
		logRows = new List<LogListRow>();

		// 初期値セット
		sortType = SORT_TYPE_ACCESS_DATETIME;
		sortIsAsc = false;

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try {
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if (pRows > I_Const.LIST_MAX_ROWS) {
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch (Exception e) {
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name == null) {
			pageAccessLog.Name = this.menu.Name;
		}

		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}

		// URLパラメータからログ取得対象のUser.IDを取得
		String agentId = ApexPages.currentPage().getParameters().get('id');
		// パラメータが取得できない場合は処理終了
		if(String.isBlank(agentId)){
			return null;
		}

		// ヘッダ情報取得
		agentInfo = E_UserDaoWithout.getUserRecByUserId(ID.valueOf(agentId));
		// 利用ログリスト作成
		createLogList(agentId);
		// 「戻る」ボタンの遷移先をセット
		PageReference returnPage = Page.IRIS_LogView;
		returnPage.getParameters().put('id', agentId);
		E_CookieHandler.setCookieRefererPolicy(returnPage.getUrl());
		E_CookieHandler.setCookieRefererHistory(returnPage.getUrl());

		return null;
	}
	protected override String getLinkMenuKey(){
		return 'iris_log';
	}
	/**
	 * ログ一覧作成
	 */
	private void createLogList(String agentId) {
		// ログレコード取得Dao呼び出し
		List<E_Log__c> logs = E_LogDao.getRecAccessLog(agentId);
		I_TubeConnectHelper connect = new I_TubeConnectHelper();
		for (E_Log__c log : logs) {
			LogListRow row = new LogListRow();

			row.accessDatetime = log.AccessDatetime__c;
			row.logType = log.ActionType__c;
			row.logName = log.DisplayName__c;
			row.url = log.DisplayURL__c;
			if(row.logType == I_Const.ACTION_TYPE_MOVIE){
				//row.url = log.TubeDisplayURL__c + '&' + connect.getUrlParamsForNNTube();
				row.url = log.Detail__c + '&' + connect.getUrlParamsForNNTube();
			}

			logRows.add(row);
		}

		// ログが0の場合、メッセージを表示
		if (logRows.size() <= 0) {
			pageMessages.addWarningMessage('該当ログがありません');
			// TODO:後でメッセージマスタに移す
			// pageMessages.addWarningMessage(getMSG().get('AGT|001'));
		}

		// ソート
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;
		logRows.sort();

		return;
	}

	/**
	 * ソート
	 */
	public void sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if (sortType != sType) {
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		I_LogViewController.list_sortType  = sortType;
		I_LogViewController.list_sortIsAsc = sortIsAsc;
		logRows.sort();

		return;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows() {
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if (rowCount > I_Const.LIST_MAX_ROWS) {
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows() {
		return logRows == null ? 0 : logRows.size();
	}

	/**
	 * 内部クラス
	 */
	public class LogListRow implements Comparable {
		// 利用日時
		public DateTime accessDateTime {get; set;}
		// 利用日時（表示用）
		public String accessDateTimeJST {
			get{
				return accessDateTime.format('yyyy/MM/dd HH:mm', 'JST');
			}
			set;
		}
		// ログ種別
		public String logType {get; set;}
		// 利用内容
		public String logName {get; set;}
		// URL
		public String url {get; set;}
		// 利用内容をリンクにする
		public Boolean getIsLink(){
			return String.isNotBlank(url);
		}

		// コンストラクタ
		public LogListRow() {
			accessDateTime = null;
			logType = '';
			logName = '';
			url = '';
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			LogListRow compareToRow = (LogListRow)compareTo;
			Integer result = 0;

			// 利用日時
			if (list_sortType == SORT_TYPE_ACCESS_DATETIME) {
				result = I_Util.compareToDateTime(accessdatetime, compareToRow.accessdatetime);
			// ログ種別
			} else if (list_sortType == SORT_TYPE_LOG_TYPE) {
				result = I_Util.compareToString(logType, compareToRow.logType);
			// 利用内容
			} else if (list_sortType == SORT_TYPE_LOG_NAME) {
				result = I_Util.compareToString(logName, compareToRow.logName);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}
}