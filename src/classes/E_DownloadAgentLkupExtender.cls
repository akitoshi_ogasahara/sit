global with sharing class E_DownloadAgentLkupExtender extends E_AbstractSearchExtender {

	E_DownloadAgentLkupController extension;
	//ページタイトル
	private static final String PAGE_TITLE = '募集人選択';

    /* コンストラクタ */
    public E_DownloadAgentLkupExtender(E_DownloadAgentLkupController extension){
		super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        extension.execInitialSearch = false;        //初期検索を実施しない
        //社員ユーザ拠点権限の場合には、EIDCのZINQUIRRの3桁目から2桁は**以外の場合、支社権限と意味して、ECL３のZBUSBRまたはBRANCHと一致するレコードのみ検索可能とする。
        if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL)) {
	        SkyEditor2.Query tableQuery1 = extension.queryMap.get('searchDataTable');
	        tableQuery1.addWhereIfNotFirst('AND');
	        tableQuery1.addWhere(' ( User__r.Contact.E_CL3PF_ZBUSBR__c = \''+ access.getAccessKindFromThree() + '\' OR User__r.Contact.E_CL3PF_BRANCH__c = \''+ access.getAccessKindFromThree() + '\')');
	        extension.queryMap.put('searchDataTable',tableQuery1);
        }
    }

    /*半角カナ入力値を全角カナへ変換*/
    global override void preSearch() {
    	IF(extension != null){
    		String ipValAgentcd = extension.ipVal_agentcd.SkyEditor2__Text__C;
    		String ipValAgentkana = extension.ipVal_agentkana.SkyEditor2__Text__C;
    		//コードまたはカナは必須とする
    		if(String.isBlank(ipValAgentcd) && String.isBlank(ipValAgentkana)){
    			throw new SkyEditor2.ExtenderException(getMSG().get('ERR|008'));
    		}
    		
			if (E_Util.isNotNull(ipValAgentcd) && (!E_Util.isEnAlphaNum(ipValAgentcd) || ipValAgentcd.length() != 5)) {
				throw new SkyEditor2.ExtenderException(getMSG().get('LKM|003'));
			}
    		
    		//①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ
    		IF(extension.ipVal_agentkana.SkyEditor2__Text__c != null ){
    			extension.ipVal_agentkana.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(extension.ipVal_agentkana.SkyEditor2__Text__c))));
    		}
    	}
    }

}