public with sharing class E_IDCPFTriggerEditGroup {

	//ID管理.資料メンテナンス=TRUEのとき、レポート参照可能な公開グループに追加する。
	public static void MentenanceReport(List<E_IDCPF__c> EIDCPFs){
		try{
			List<Group> gp = [SELECT Id FROM Group WHERE DeveloperName = 'DocumentReqReportV' LIMIT 1];
			if(gp.size() == 0) return;

			List<GroupMember> members = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId = :gp[0].Id];
			Map<Id, GroupMember> getGMByUserId = new Map<Id, GroupMember>();
			for(GroupMember member : members){
				getGMByUserId.put(member.UserOrGroupId, member);
			}

			List<GroupMember> addMember = new List<GroupMember>();
			List<GroupMember> delMember = new List<GroupMember>();

			for(E_IDCPF__c idcpf : EIDCPFs){
				if(!(idcpf.User__c != null && idcpf.User__r.isActive)) continue;

				//権限無し、共有有りの場合、公開グループから削除する
				if(!idcpf.IsDocMaint__c && getGMByUserId.containsKey(idcpf.User__c)){
					delMember.add(getGMByUserId.get(idcpf.User__c));

				//権限有り、共有無しの場合、公開グループに追加する
				} else if(idcpf.IsDocMaint__c && !getGMByUserId.containsKey(idcpf.User__c)) {
					addMember.add(new GroupMember(GroupId = gp[0].Id, UserOrGroupId = idcpf.User__c));
				}

			}

			System.debug('公開グループから削除_'+delMember);
			System.debug('公開グループに追加_'+addMember);

			if(delMember.size() > 0) delete delMember;
			if(addMember.size() > 0) insert addMember;

		}catch(Exception e){
			System.debug('E_IDCPFTriggerEditGroup__'+e.getMessage());
		}
	}
}