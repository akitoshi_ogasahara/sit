@isTest
private class TestE_UserLoginDaoWithout {
	
	//getRecByUserIdのテスト
	@isTest static void getRecByUserIdTest() {
		Test.startTest();
		UserLogin ulRec = E_UserLoginDaoWithout.getRecByUserId(UserInfo.getUserId());
		System.assertEquals(ulRec.UserId,UserInfo.getUserId());
		Test.stopTest();
	}
	
	//getRecWithIsLockByUserIdのテスト
	@isTest static void getRecWithIsLockByUserIdTest() {
		Test.startTest();
		UserLogin ulRec = E_UserLoginDaoWithout.getRecWithIsLockByUserId(UserInfo.getUserId());
		System.assertEquals(ulRec.UserId,UserInfo.getUserId());
		Test.stopTest();
	}

	//updateRecのテスト
	@isTest static void updateRecTest() {
		Test.startTest();
		UserLogin ulRec = E_UserLoginDaoWithout.getRecWithIsLockByUserId(UserInfo.getUserId());
		Datetime pastTime = ulRec.LastModifiedDate;
		E_UserLoginDaoWithout.updateRec(ulRec);
		ulRec = E_UserLoginDaoWithout.getRecWithIsLockByUserId(UserInfo.getUserId());
		//更新したので最終更新日時が異なる
		System.assertNotEquals(pastTime,ulRec.LastModifiedDate);
		Test.stopTest();
	}

}