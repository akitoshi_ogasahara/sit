public with sharing class DS_IndexController extends DS_AbstractController{
	public String showIndex{get; set;}		//表示するIndex

	//画面名
	public String getPageName(){
		return DS_Const.PAGE_NAME_DS_INDEX;
	}

	//indexのリスト（あ,か,さ,た,..）
	public List<String> indexList{
		get{
			if(indexList == null){
				this.indexList = DS_Const.INDEX_LIST;
			}
			return this.indexList;
		}
		private set;
	}

	//index->initialのMap(あ->あいうえお、か->かきくけこ..)
	private Map<String, List<String>> initialMap;
	private Map<String, List<String>> getInitialMap(){
		if(initialMap == null){
			initialMap = DS_Const.INITIAL_MAP;
		}
		return initialMap;
	}

	//index->List<内部クラス(InitialDiseases)>(あ->「あ」の内部クラス、「い」の内部クラス..)
	public Map<String, List<InitialDiseases>> indexDsMap{
		get{
			if(this.indexDsMap == null){
				this.indexDsMap = new Map<String, List<InitialDiseases>>();
				//傷病名カナの昇順にすべてのレコードを取得
				String order = 'ORDER BY Katakana__c';
				Map<String,List<E_Diseases__c>> tempMap = new Map<String,List<E_Diseases__c>>();
				for(E_Diseases__c src : E_DiseasesDao.getDiseaseRecs(order)){
					//傷病名カナ頭文字(数式)のでMap化	※半角のものは全角で処理
					String key = E_Util.enKatakanaToEm(src.KanaInitial__c);
					if(tempMap.containsKey(key)){
						tempMap.get(key).add(src);
					}else{
						tempMap.put(key,new List<E_Diseases__c>{src});
					}
				}
				//indexごとにinitialを取得し、initialと一致する内部クラスをList化
				for(String index : this.indexList){
					Integer keyNo = 0;
					//index->List<内部クラス(InitialDiseases)>を作成
					this.indexDsMap.put(index,new List<InitialDiseases>());
					for(String key : this.getInitialMap().get(index)){
						//レコードのないInitialは、空のリストを作成
						if(tempMap.containsKey(key)){
							this.indexDsMap.get(index).add(new InitialDiseases(key,keyNo,tempMap.get(key)));
						}else{
							this.indexDsMap.get(index).add(new InitialDiseases(key,keyNo,new List<E_Diseases__c>()));
						}
						keyNo ++;
					}
				}
			}
			return this.indexDsMap;
		}
		private set;
	}

/*
* Constructor
*/
	public DS_IndexController(){
		this.showIndex = DS_Const.INDEX_A;	//表示するIndexを設定(初期値はア行)
	}

/*
* init
*/
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = getPageName();

		return pr;
	}


/*
* 内部クラス
*/
	public class InitialDiseases{
		public String initial{get; private set;}	//Initial(あ、い、う..)
		public List<E_Diseases__c> diseases{get; set;}	//傷病マスタレコードのリスト

		public Integer idNo{get; private set;}	//ページ側でIdとするNo

		public InitialDiseases(String ini,Integer no,List<E_Diseases__c> recs){
			this.initial = ini;
			this.idNo = no;
			this.diseases = recs;
		}
	}

	public String getSumiseiParam(){
		return access.canViewSumiseiIRIS() && access.isEmployee() ? '&isSMTM=1' : '';
	}

}