/**
 * 検索ページのコントローラ
 */
public with sharing class MD_SearchController extends MD_AbstractController {

	// VF
	private String vfPage;

	// 検索実行フラグ
	public Boolean isSearched {get; set;}
	// 検索条件クラス
	public MD_SearchConditionDTO searchCondition {get; set;}
	// ドクター情報リスト(Map)
	public List<MD_Doctor__c> doctorList {get; set;}
	// ドクター情報リスト印刷一覧(Map)
	public List<MD_Doctor__c> doctorListPrint {get; set;}
	// 表示用ラッパークラスリスト
	public List<MD_DoctorDTO> wrapDoctorList {get; set;}
	// 印刷用ラッパークラスリスト
	public List<MD_DoctorDTO> wrapPrintList {get; set;}
	// 検索結果の最大件数を超えているディスクレーマー表示フラグ
	public Boolean isSearchResultOverLimit {get; set;}
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// エラーメッセージあり
	public Boolean isError {
		get {
			return pageMessages.hasMessages();
		}
	}

	// 検索結果ソート順
	public String sortType {get; set;}
	public List<SelectOption> sortTypeOptions {
		get {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption(MD_Const.DOCTOR_COL_API_LOCATION, MD_Const.SELECT_LABEL_SORT_LOCATION));
			options.add(new SelectOption(MD_Const.DOCTOR_COL_API_EXAMINATIONDATE, MD_Const.SELECT_LABEL_SORT_EXAMINATIONDATE));
			return options;
		}
		set;
	}

	/**
	 * Constructor
	 */
	public MD_SearchController() {
		system.debug('■[start Constructor]');

		// ページメッセージの初期化
		pageMessages = getPageMessages();
	}

	/**
	 * init
	 */
	public PageReference init() {
		try {
			system.debug('■[start init]');

			// 検索実行判定フラグの初期化
			isSearched = false;

			// ディスクレーマー表示フラグの初期化
			isSearchResultOverLimit = false;

			// 検索結果のソート順の初期化
			sortType = MD_Const.DOCTOR_COL_API_LOCATION;

			// 検索条件の初期化
			searchCondition = new MD_SearchConditionDTO();

			// 検索種別（検索画面論理名）の取得
			searchCondition.selectedKind = MD_Const.screenNameMap.get(string.valueOf(Apexpages.currentPage()));

			// IRIS Day2 ページアクセスログは E_Logging コンポーネントで取得するように変更
			return null;
			// アクセスログ登録
			//return insertAccessLog();

		} catch (Exception e) {
			System.debug('■［Error_init］' + e.getMessage());

			// アクセスログ登録、 エラーページへ
			return insertErrorLog(e.getMessage());
		}
	}

	/**
	 * init(Print)
	 */
	public PageReference initPrint() {
		try {
			system.debug('■[start initPrint]');
			logCondition = new E_LogCondition.DoctorCondi();

			// ページメッセージの初期化
			pageMessages = getPageMessages();

			// 検索条件のラベルを取得
			searchCondition.getSelectOptionLabel();

			// IRIS Day2 ページアクセスログは E_Logging コンポーネントで取得するように変更
			return null;
			/*
			// アクセスログ登録
			logCondition.accessPage = 'E_MDList_print';
			logCondition.detail = '[検索種別: ' + vfPage + ']';
			return insertAccessLog();
			*/

		} catch (Exception e) {
			System.debug('■［Error_initPrint］' + e.getMessage());

			// アクセスログ登録、 エラーページへ
			logCondition.accessPage = 'E_MDList_print';
			return insertErrorLog(e.getMessage());
		}
	}

	/**
	 * 検索
	 */
	public String nnMD_Search_lat {get; set;}
	public String nnMD_Search_lon {get; set;}

	public String nnMD_AddrStation {get; set;}
	public String nnMD_AddrStationText {get; set;}

	public String nnMD_StateCode {get; set;}
	public String nnMd_StateText {get; set;}
	public pageReference doSearch() {
		try {
			// メッセージ初期化
			pageMessages.clearMessages();

			// 位置情報セット(ページからのURLパラメータ)
			String paramLat = nnMD_Search_lat;
			String paramLon = nnMD_Search_lon;

			vfPage = getPageName();

			if (String.isNotBlank(paramLat) && String.isNotBlank(paramLon)) {
				searchCondition.latitude = Double.valueOf(paramLat);
				searchCondition.longitude = Double.valueOf(paramLon);

			// 位置情報が取得できない場合、エラーメッセージ設定
			} else {
				// 住所やエリアが選択されていない場合はエラーメッセージ表示

				// 住所・駅名から探す 画面
				if (vfPage == MD_Const.VF_NAME_MD_ADDRESS) {
					pageMessages.addErrorMessage(E_Message.getMsgMap().get('MDUS|005'));

				// エリアから探す 画面
				} else if (vfPage == MD_Const.VF_NAME_MD_AREA) {
					pageMessages.addErrorMessage(E_Message.getMsgMap().get('MDUS|006'));
				} else {
					pageMessages.addErrorMessage('緯度経度情報が取得できません');
				}
				return null;
			}

			// 住所・駅情報のセット
			searchCondition.addrStation = nnMD_AddrStation;
			searchCondition.addrStationText = nnMD_AddrStationText;

			// エリア検索情報のセット
			searchCondition.areaCode = nnMD_StateCode;
			searchCondition.areaText = nnMd_StateText;

//
			// 選択された検索値をセット
			setCondition();

			// 縮尺のセット
			searchCondition.setZoom(Double.valueOf(searchCondition.selectedDistance));

			// Where句作成
			String strWhere = getQueryWhere(searchCondition);

			// OrderBy句作成
			String strOrder = getQueryOrderby(searchCondition);

			// ドクターリスト件数カウント
			if (MD_DoctorDAO.getRecsByWhereCount(strWhere, MD_Const.MD_LIMIT_SEARCH_RESULT) > MD_Const.MD_LIMIT_SEARCH_RESULT) {
				isSearchResultOverLimit = true;
			} else {
				isSearchResultOverLimit = false;
			}

			// ドクターリスト取得
			doctorList = MD_DoctorDAO.getRecsByWhere(strWhere, strOrder, MD_Const.MD_LIMIT_SEARCH_RESULT);

			// 表示用のリスト作成
			wrapDoctorList = new List<MD_DoctorDTO>();
			Integer cnt = 1;
			for (Integer i = 0; i < doctorList.size(); i++) {
				MD_DoctorDTO wd = new MD_DoctorDTO(cnt, doctorList[i]);
				wrapDoctorList.add(wd);
				cnt++;
			}

			// 検索実行フラグ更新
			isSearched = true;
/*
			// 指定位置の設定
			searchCondition.searchDistance = searchCondition.selectedDistance;
*/
		} catch (Exception e) {
			pageMessages.addErrorMessage(e.getMessage());
		}
		return null;
	}

	/**
	 * 検索（指定位置からの距離）
	 */
	public String nnMD_changeDistance {get; set;}
	public void doSearchDistance() {
		try {
			// ページメッセージの初期化
			pageMessages = getPageMessages();

			String changeDistance = nnMD_changeDistance;

			// URLパラメータより縮尺が取得出来た場合、検索条件の指定位置からの距離を上書き
			if (String.isNotBlank(changeDistance)) {
				searchCondition.selectedDistance = changeDistance;
			}

			// 検索
			doSearch();

		} catch (Exception e) {
			pageMessages.addErrorMessage(e.getMessage());
		}
	}

	/* 印刷 */
	public PageReference doPrint() {
		try {
			// メッセージ初期化
			pageMessages.clearMessages();

			// 選択済みのレコードリスト作成
			doctorListPrint = new List<MD_Doctor__c>();
			wrapPrintList = new List<MD_DoctorDTO>();

			Integer no = 1;
			for (MD_DoctorDTO wrap : wrapDoctorList) {
				if (wrap.isCheck) {
					wrap.pno = no;
					wrapPrintList.add(wrap);
					no++;

					doctorListPrint.add(wrap.doctor);
				}
			}

			// 選択された件数チェック
			if(wrapPrintList.size() > 0){
				PageReference pref = Page.E_MDList_print;
				pref.setRedirect(false);
				return pref;

			// 選択されたデータが存在しない場合エラーメッセージを返す
			} else {
				pageMessages.addErrorMessage(E_Message.getMsgMap().get('9MSSI|011'));
				return null;
			}
		} catch (Exception e) {
			pageMessages.addErrorMessage(e.getMessage());
			return null;
		}
	}

	/**
	 * 選択値より検索値をセット
	 */
	private void setCondition(){
		// 検索値 性別
		searchCondition.searchGender = searchCondition.selectedGender;
		// 検索値 血液型
		searchCondition.searchBlood = searchCondition.selectedBlood;
		// 検索値 特定医
		// searchCondition.searchParticularDoctor = searchCondition.selectedParticularDoctor;
		// 検索値 心電図
		searchCondition.searchElectrocardiogram = searchCondition.selectedElectrocardiogram;
		// 指定位置の設定
		searchCondition.searchDistance = searchCondition.selectedDistance;
		// 検索値 年齢
		searchCondition.searchAge = searchCondition.selectedAge;
		searchCondition.searchAges = new List<String>();
		if(!searchCondition.selectedAges.isEmpty()){
			searchCondition.searchAges = searchCondition.selectedAges;
		}
	}

	/**
	 * 検索条件よりWhere句生成
	 */
	private String getQueryWhere(MD_SearchConditionDTO condition) {
		String strWhere = '';

		// 年齢
		if (!condition.selectedAges.isEmpty()) {
			for (String age : condition.selectedAges) {
				if (String.isNotBlank(strWhere)) {
					strWhere += ' OR ';
				} else {
					strWhere += ' ( ';
				}
				strWhere += age;
			}
			strWhere += ' ) ';
		}

		// 性別
		if (!(condition.selectedGender == MD_Const.SELECTED_NONE_VALUE)) {
			strWhere = addAnd(strWhere);
			strWhere += condition.selectedGender;
		}
		// 血液検査
		if (!(condition.selectedBlood == MD_Const.SELECTED_NONE_VALUE)) {
			strWhere = addAnd(strWhere);
			strWhere += condition.selectedBlood;
		}
		// 特定医
		// if (!(condition.selectedParticularDoctor == MD_Const.SELECTED_NONE_VALUE)) {
		// 	strWhere = addAnd(strWhere);
		// 	strWhere += condition.selectedParticularDoctor;
		// }
		// 心電図
		if (!(condition.selectedElectrocardiogram == MD_Const.SELECTED_NONE_VALUE)) {
			strWhere = addAnd(strWhere);
			strWhere += condition.selectedElectrocardiogram;
		}
		// 指定位置からの距離
		strWhere = addAnd(strWhere);
		strWhere += getQueryDistance(condition.latitude, condition.longitude) + ' < ' + condition.selectedDistance;

		// 病院名（空白のデータは検索対象外）
		strWhere = addAnd(strWhere);
		strWhere += ' HospitalName__c <> null ';

		return strWhere;
	}

	/**
	 * AND連結用メソッド
	 */
	private String addAnd(String strWhere) {
		String retStr = '';
		if (!String.isEmpty(strWhere)) {
			retStr = strWhere += ' AND ';
		}
		return retStr;
	}

	/**
	 * 検索条件よりOrderby句生成
	 */
	private String getQueryOrderby(MD_SearchConditionDTO condition) {
		String strOrder = '';

		// ソート順：「指定された位置から近い順」
		if (sortType == MD_Const.DOCTOR_COL_API_LOCATION) {
			strOrder = getQueryDistance(condition.latitude, condition.longitude) + ' ASC';
		// ソード順：「直近の診査日」
		} else if (sortType == MD_Const.DOCTOR_COL_API_EXAMINATIONDATE) {
			strOrder = MD_Const.DOCTOR_COL_API_EXAMINATIONDATE + ' DESC nulls last';
		}

		return strOrder;
	}

	/**
	 * SOQL用の緯度経度項目値取得
	 */
	private String getQueryDistance(Double lat, Double lon) {
		return 'Distance(Location__c, GEOLOCATION(' + lat + ', ' + lon + '), \'km\')';
	}
}