/**
 * CR_ExportAgencyBizReportsController
 */
@isTest
private class TestCR_ExportAgencyBizReportsController {

    static testMethod void test_constructor01() {
        Test.startTest();
        
        CR_ExportAgencyBizReportsController controller = new CR_ExportAgencyBizReportsController();
        List<E_CSVColumnDescribe.BaseField> resultList = controller.addColumn(new E_CSVColumnDescribe.BaseField('Test'));
        
        Test.stopTest();
        
        System.assertEquals('CR_Agency__c', controller.mainSObjectAPIName());
        System.assertEquals('MRUnit__c', resultList[0].fieldAPI);
        System.assertEquals('MRName__c', resultList[1].fieldAPI);
        System.assertEquals('AgencyCode__c', resultList[2].fieldAPI);
        System.assertEquals('AgencyName__c', resultList[3].fieldAPI);
        System.assertEquals('LastRequestReport__c', resultList[4].fieldAPI);
        System.assertEquals('BusinessStatus__c', resultList[5].fieldAPI);
    }
}