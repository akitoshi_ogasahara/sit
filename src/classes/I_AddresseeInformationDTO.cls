public virtual class I_AddresseeInformationDTO {

	public String agencyName {get; set;}
	public String agentName {get; set;}
	public String postalCode {get; set;}
	public String addressMain {get; set;}
	public String phoneNumber {get; set;}

}