@isTest
private class TestI_AbstractSalesResultsController {

	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static Contact agent;
	private static E_BizDataSyncLog__c ebizLog;
	private static E_AgencySalesResults__c asr;
	
	/**
	 * 実行ユーザ: 本社営業部
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test001(){
		// 実行ユーザ作成（本社営業部）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('AH');

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));		//パラメータ'row'に繰り返し行数を付与
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			Test.startTest();
			I_AbstractSalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();

			Test.stopTest();
		}
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test002(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('AH');
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88');
		insert unit;

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));		//パラメータ'row'に繰り返し行数を付与
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			Test.startTest();
			I_AbstractSalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();

			Test.stopTest();
		}
	}
	
	private static void createData(String hierarchy){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';		// 営業日

		//連携ログ作成
		Date d = E_Util.string2DateyyyyMMdd(BUSINESS_DATE);
		ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = d,BatchEndDate__c = d);
		insert ebizLog;

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;
		
		// Account 事務所
		office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
		insert office;
		suboffice = new Account(Name = 'テスト事務所SUB',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
		insert suboffice;
		
		// Contact 募集人作成
		agent = new Contact(AccountId = office.id, LastName = 'テスト募集人');
		insert agent;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = hierarchy,
			QualifSim__c = '優績S'
		);
		if(hierarchy == 'AY'){
			asr.Account__c = office.id;
		}else if(hierarchy == 'AT'){
			asr.Account__c = office.id;
			asr.Agent__c = agent.id;
		}
		insert asr;
	}
}