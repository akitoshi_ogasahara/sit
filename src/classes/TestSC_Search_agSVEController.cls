@isTest
private with sharing class TestSC_Search_agSVEController{
		private static testMethod void testPageMethods() {	
			SC_Search_agSVEController page = new SC_Search_agSVEController(new ApexPages.StandardController(new SC_Office__c()));	
			page.getOperatorOptions_SC_Office_c_FiscalYear_c_multi();	
			page.getOperatorOptions_SC_Office_c_ZEAYNAM_c();	
			page.getOperatorOptions_SC_Office_c_ZAGCYNUM_c();	
			page.getOperatorOptions_SC_Office_c_IsActiveOffice_c();	
			System.assert(true);
		}	
			
	private static testMethod void testresultTable() {
		SC_Search_agSVEController.resultTable resultTable = new SC_Search_agSVEController.resultTable(new List<SC_Office__c>(), new List<SC_Search_agSVEController.resultTableItem>(), new List<SC_Office__c>(), null);
		resultTable.create(new SC_Office__c());
		resultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
}