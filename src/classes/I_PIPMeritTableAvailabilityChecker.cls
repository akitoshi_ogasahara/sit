public with sharing class I_PIPMeritTableAvailabilityChecker {
	private E_Policy__c pol;
    private E_COVPF__c mainCov;
    private List<E_COVPF__c> tokuyakuList = new List<E_COVPF__c>();
    private List<String> availableProducts = new List<String>{I_PIPConst.CR_CODE_TT,I_PIPConst.CR_CODE_TS,I_PIPConst.CR_CODE_TP,I_PIPConst.CR_CODE_WL,I_PIPConst.CR_CODE_OF,I_PIPConst.CR_CODE_OG,I_PIPConst.CR_CODE_OH,I_PIPConst.CR_CODE_OI};
    private List<String> availableLCV2s = new List<String>{I_PIPConst.CR_CODE_GA,I_PIPConst.CR_CODE_GB,I_PIPConst.CR_CODE_GC,I_PIPConst.CR_CODE_GD,I_PIPConst.CR_CODE_GE,I_PIPConst.CR_CODE_GF,I_PIPConst.CR_CODE_GG,I_PIPConst.CR_CODE_GH,I_PIPConst.CR_CODE_UL,I_PIPConst.CR_CODE_UM,I_PIPConst.CR_CODE_UN,I_PIPConst.CR_CODE_UO,I_PIPConst.CR_CODE_UP};

    @TestVisible private static Datetime testDatetime{get;set;}


    public I_PIPMeritTableAvailabilityChecker(E_Policy__c inPol,List<E_COVPF__c> coverList) {
        //TODO:polがnullの場合の処理
        this.pol = inPol;
        for( E_COVPF__c rec : coverList ){
            if( rec.COLI_ZCRIND__c == 'C' ){
                this.mainCov = rec;
            } else {
                this.tokuyakuList.add(rec);
            }
        }
    }

    public Boolean isAvailableContract(){
    	//現在の状況コードが「有効(I)」「通常保険料払込満了」以外の場合、利用不可（＝払済保険、データ更新中も利用不可）。
    	if(pol.COMM_STATCODE__c != 'I' && pol.COMM_STATCODE__c != 'U') return false;

    	//解約返戻金計算ができない場合、利用不可。
    	if(pol.COLI_ZCVDCF__c == true) return false;

    	//前納の場合、利用不可。
    	if(pol.COLI_ZADVPDCF__c == true) return false;

    	return true;

    }

    public Boolean isServiceTime(){

    	//GMTからローカル時間に補正
    	DateTime dt = Datetime.now() + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);

    	if(Test.isRunningTest()) dt = testDatetime;

    	if(dt.hourGMT() < 6){
    		 return false;
    	}else{
    		return true;
    	}

    }

    public Boolean isAvailableProduct(){
        //失効の場合
        if(pol.COMM_STATCODE__c == 'L') return false;
        //失効(貸付中)の場合
        if(pol.COMM_STATCODE__c == 'O') return false;

        //消滅契約の場合
        if( mainCov == null ) return false;

        //対象商品以外の場合
        if(!availableProducts.contains(mainCov.COMM_CRTABLE2__c)) return false;

        //逓増定期(定期)の場合
        if(mainCov.COMM_CRTABLE2__c == I_PIPConst.CR_CODE_TP || mainCov.COMM_CRTABLE2__c == I_PIPConst.CR_CODE_WL ){
            return isAvailableLCV2();
        }
        //終身ガン(新税制)の場合
        if(isCancerProduct(mainCov.COMM_CRTABLE2__c)){
            Integer newTaxSystemStertDate = 20120427;
            return Integer.valueOf(pol.COMM_OCCDATE__c) >= newTaxSystemStertDate;
        }
        //対象商品の場合表示可能とする
        return true;
    }

    private Boolean isAvailableLCV2(){
        for(E_COVPF__c cov : tokuyakuList){
            if(availableLCV2s.contains(cov.COMM_CRTABLE2__c)) return true;
        }
        return false;
    }

    private Boolean isCancerProduct(String crCode){
       switch on crCode {
            when 'OF', 'OG', 'OH', 'OI' {
                return true;
            } 
            when else {
                return false;
            }
       }
    }
}