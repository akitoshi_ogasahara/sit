@isTest
private class TestSC_ChangeValue_LimitController {

	@isTest static void doChangeDisplay_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice.LimitFixed__c = '1月';
		insert scOffice;

		SC_ChangeValue_LimitController controller = new SC_ChangeValue_LimitController();
		controller.record = scOffice;

		PageReference pr = Page.IRIS_SCSelfCompliances;
		pr.getParameters().put('id', scOffice.Id);

		Test.startTest();
			PageReference result = controller.doChangeDisplay();
			System.assertEquals(controller.beforeValue, scOffice.LimitFixed__c);
			System.assertEquals(result, null);

			result = controller.doChangeDisplay();
			System.assertEquals(result.getUrl(), pr.getUrl());
			System.assertEquals(controller.canEdit, null);
		Test.stopTest();
	}

	@isTest static void doSave_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice.LimitFixed__c = '1月';
		insert scOffice;

		SC_ChangeValue_LimitController controller = new SC_ChangeValue_LimitController();
		controller.record = scOffice;
		controller.doChangeDisplay();

		PageReference pr = Page.IRIS_SCSelfCompliances;
		pr.getParameters().put('id', scOffice.Id);

		Test.startTest();
			PageReference result = controller.doSave();
			System.assertEquals(controller.isEdit, false);
			System.assertEquals(result.getUrl(), pr.getUrl());
			System.assertEquals(controller.record.StatusHistory__c, '');

			controller.doChangeDisplay();
			controller.record.LimitFixed__c = '2月';
			controller.doSave();
			System.assertNotEquals(controller.record.StatusHistory__c, null);
		Test.stopTest();
	}

}