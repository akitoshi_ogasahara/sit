global with sharing class MNT_SearchEIDC extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_IDCPF__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public E_IDCPF__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
			
		public E_IDCPF__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component12_val {get;set;}	
		public SkyEditor2.TextHolder Component12_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component14_val {get;set;}	
		public SkyEditor2.TextHolder Component14_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component16_val {get;set;}	
		public SkyEditor2.TextHolder Component16_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component18_val {get;set;}	
		public SkyEditor2.TextHolder Component18_op{get;set;}	
			
	public String recordTypeRecordsJSON_E_IDCPF_c {get; private set;}
	public String defaultRecordTypeId_E_IDCPF_c {get; private set;}
	public String metadataJSON_E_IDCPF_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public MNT_SearchEIDC(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_IDCPF__c.fields.User__c;
		f = E_IDCPF__c.fields.OwnerId;
		f = E_IDCPF__c.fields.ContactCL3PF_AGNTNUM__c;
		f = E_IDCPF__c.fields.ContactAccount__c;
		f = E_IDCPF__c.fields.ZEMAILAD__c;
		f = E_IDCPF__c.fields.ZIDTYPE__c;
		f = E_IDCPF__c.fields.ZSTATUS01__c;
		f = E_IDCPF__c.fields.Name;
		f = E_IDCPF__c.fields.ZINQUIRR__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_IDCPF__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				E_IDCPF__c lookupObjComponent38 = new E_IDCPF__c();	
				Component6_val = lookupObjComponent38;	
				Component6_op = new SkyEditor2.TextHolder();	
					
				Component8_val = lookupObjComponent38;	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder();	
					
				Component12_val = new SkyEditor2__SkyEditorDummy__c();	
				Component12_op = new SkyEditor2.TextHolder();	
					
				Component14_val = new SkyEditor2__SkyEditorDummy__c();	
				Component14_op = new SkyEditor2.TextHolder();	
					
				Component16_val = new SkyEditor2__SkyEditorDummy__c();	
				Component16_op = new SkyEditor2.TextHolder();	
					
				Component18_val = new SkyEditor2__SkyEditorDummy__c();	
				Component18_op = new SkyEditor2.TextHolder();	
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('E_IDCPF__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('ZIDTYPE__c')
						.addFieldAsOutput('ZINQUIRR__c')
						.addFieldAsOutput('User__c')
						.addFieldAsOutput('OwnerId')
						.addFieldAsOutput('ZEMAILAD__c')
						.addFieldAsOutput('ZSTATUS01__c')
						.addFieldAsOutput('ContactCL3PF_AGNTNUM__c')
						.addFieldAsOutput('ContactAccount__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'User__c', 'User__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'OwnerId', 'OwnerId', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'ContactCL3PF_AGNTNUM__c', Component10_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component12_val, 'SkyEditor2__Text__c', 'ContactAccount__c', Component12_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component14_val, 'SkyEditor2__Text__c', 'ZEMAILAD__c', Component14_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component16_val, 'SkyEditor2__Text__c', 'ZIDTYPE__c', Component16_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component18_val, 'SkyEditor2__Text__c', 'ZSTATUS01__c', Component18_op, true, 0, false ))
				);	
					
					Component3 = new Component3(new List<E_IDCPF__c>(), new List<Component3Item>(), new List<E_IDCPF__c>(), null);
				Component3.ignoredOnSave = true;
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_IDCPF__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			presetSystemParams();
			Component3.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_User_c() { 
			return getOperatorOptions('E_IDCPF__c', 'User__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_OwnerId() { 
			return getOperatorOptions('E_IDCPF__c', 'OwnerId');	
		}	
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_ContactCL3PF_AGNTNUM_c() { 
			return getOperatorOptions('E_IDCPF__c', 'ContactCL3PF_AGNTNUM__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_ContactAccount_c() { 
			return getOperatorOptions('E_IDCPF__c', 'ContactAccount__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_ZEMAILAD_c() { 
			return getOperatorOptions('E_IDCPF__c', 'ZEMAILAD__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_ZIDTYPE_c() { 
			return getOperatorOptions('E_IDCPF__c', 'ZIDTYPE__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_IDCPF_c_ZSTATUS01_c() { 
			return getOperatorOptions('E_IDCPF__c', 'ZSTATUS01__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public E_IDCPF__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, E_IDCPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (E_IDCPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}