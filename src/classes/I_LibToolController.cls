/**
 * ライブラリ＞セールスツール画面コントローラクラス.
 */
public with sharing class I_LibToolController extends I_CMSController{
	
	public String noticeDownloadUrl {get; set;}
	public String chatterFileId{get; set;}
	public I_ContentMaster__c salestoolParent{get; set;}
	
	public I_LibToolController() {
		if(CMSSections.size() == 0) {
			return;
		}
		this.salestoolParent = CMSSections[0].record;
		this.noticeDownloadUrl = this.createNoticeDownloadUrl();
	}
	
	/**
	 * NN社員かを判定する.
	 *
	 * @return Boolean NN社員である場合はtrue、さもなくばfalse
	 */
	public Boolean getIsEmployee() {
		return E_AccessController.getInstance().isEmployee();
	}

	protected virtual override List<I_ContentMaster__c> getContentMasterRecords(){
		return I_ContentMasterDao.getSalesToolRecords(this.menu.Name);
	}
	
	protected virtual override Id getIrisMenuId(){
		/*redirectflag = false;*/

		String pid = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGE);		//?page=
		String docNum = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_DOC_NUM); //?docnumber=
		System.debug('=====' + ApexPages.currentPage().getUrl());
		// 資料管理番号がURLパラメータに含まれる場合
		if(String.isNotBlank(docNum)) {

			/*keepMenu = new I_MenuMaster__c();
			redirectflag = true;
			System.debug('redirectflag true');
			anchor = 'iris-cms-' + docNum;*/

			I_ContentMaster__c cont = I_ContentMasterDao.getRecByDocNumber(docNum);
			if(cont != null && !getIsEmployee() && cont.isEmployeeCategory__c){
				cont = null;
			}

			/* UX2 20170627 			
			if(cont != null && String.isNotBlank(cont.CategoryJoin__c)) {
				String menuName = cont.CategoryJoin__c.split(',').get(0);
			*/
			
			if(cont != null && String.isNotBlank(cont.Category__c)){
				
				String menuName = cont.Category__c.split(';').get(0);
				
				I_MenuMaster__c menu = I_MenuMasterDao.getRecByNameAndSubCategory(menuName,I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL);
				/*keepMenu = menu;*/


				if(menu != null) {

					String pageId = menu.Pages__r.size() > 0 ? menu.Pages__r.get(0).Id : '';
					ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_PAGE, pageId);
					ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_MENUID, menu.Id);
					if (String.isBlank(ApexPages.currentPage().getAnchor())) {
						ApexPages.currentPage().setAnchor(cont.DOM_Id__c);
					}
					//ApexPages.currentPage().setAnchor('iris-cms-' + cont.DOM_Id__c);
					System.debug(ApexPages.currentPage().getUrL());
					return super.getIrisMenuId();
				}
			}
		}
		
		if(pid==null){
			//セールスツールのデフォルトページを開く
			I_MenuMaster__c salesToolDefaultMenu = iris.irisMenus.extractDefaultMenuRecord(I_Const.MENU_MAIN_CATEGORY_LIBLARY
																							, I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL);
			if(salesToolDefaultMenu.pages__r.size()>0){
				ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_PAGE,salesToolDefaultMenu.pages__r[0].Id);
			}
		}
		/*System.debug('redirectflag false');*/
		return super.getIrisMenuId();
	}
	
	
	//資料なし　ターゲットユーザ種別　※ターゲットユーザ種別別のセールスツールが0件の場合に出力するkラス名
	public String getNotExistsToolsClassName(){
		Set<String> toolsFor = new Set<String>{'forAll','forCustomer','forAgency','forNN'};
		for(I_ContentMaster__c stool:CMSSections[0].record.ChildContents__r){
			toolsFor.remove('forAll');
			if(stool.ForCustomers__c){
				toolsFor.remove('forCustomer');
			}
			if(stool.ForAgency__c){
				toolsFor.remove('forAgency');
			}
			if(stool.CompanyLimited__c){
				toolsFor.remove('forNN');
			}
			if(toolsFor.size()==0){
				break;
			}
		}
		if(toolsFor.size()>0){
			return String.join(new List<String>(toolsFor), ' ');		//半角スペースでつなぐ
		}
		return null;
	}
	
	public String createNoticeDownloadUrl() {
		Id recId = this.salestoolParent.Id;
		Set<Id> recIds = new Set<Id>();
		recIds.add(recId);
		
		Map<Id,Id> chatterFileIds =  I_ContentMasterDao.getChatterFileIdsByRecId(recIds);
		
		if(chatterFileIds.size() == 0) {
			return '';
		}
		
		this.chatterFileId = chatterFileIds.get(recId);
		
		return String.format(E_CONST.CONTENT_DOWNLOAD_URL, new List<String>{String.valueOf(this.chatterFileId)});
	}
	
	public Boolean getHasChatterFile() {
		return String.isNotBlank(this.noticeDownloadUrl);
	}

}