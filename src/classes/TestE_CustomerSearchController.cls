@isTest(SeeAllData=false)
public with sharing class TestE_CustomerSearchController {

	/**
	 * construct test
	 */
	private static testMethod void constructTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//保険契約ヘッダ
		TestE_TestUtil.createPolicy(true, conCust.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxxx');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			resultPage = controller.pageAction();

			Test.stopTest();
		}
		system.assertEquals(E_Const.CS_CONTRACTOR, controller.selectKind);
		system.assertEquals(E_Const.CS_DISTORIBUTOR, controller.selectTerm);
		system.assert(controller.iDistoributor != null);
		system.assert(controller.iOffice != null);
		system.assert(controller.iAgent != null);
		System.assert(!controller.isSearched);
		system.assertEquals(E_Const.CS_CONTRACTOR, controller.getCS_CONTRACTOR());
		system.assertEquals(E_Const.CS_INSURED, controller.getCS_INSURED());
		system.assertEquals(E_Const.CS_DISTORIBUTOR, controller.getCS_DISTORIBUTOR());
		system.assertEquals(E_Const.CS_OFFICE, controller.getCS_OFFICE());
		system.assertEquals(E_Const.CS_AGENT, controller.getCS_AGENT());
		system.assert(controller.getIsEmployee());
		system.assertEquals(100, controller.getMax());
		system.assertEquals(null, resultPage);
	}

	/**
	 * reDraw test
	 */
	private static testMethod void reDrawTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//保険契約ヘッダ
		TestE_TestUtil.createPolicy(true, conCust.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxxx');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;
		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.reDraw();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
	}

	/**
	 * doSearch test
	 * 代理店
	 * 契約者-証券番号,契約者-顧客番号,契約者-契約者カナ,契約者-契約者名漢字
	 */
	private static testMethod void doSearchDistoributorAndContractorTest() {
        system.debug('Start-------doSearchDistoributorAndContractorTest');
		//親代理店
		Account parentAccDis = TestE_TestUtil.createAccount(true);
		//代理店
		Account accDis = TestE_TestUtil.createAccount(false);
        accDis.ParentId = parentAccDis.id;
        insert accDis;
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//契約者
		Contact conCont = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '11111111', '');
		conCont.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
		insert conCont;
		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, conCont.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxx1');
		policy.COMM_STATCODE__c = 'SA';
		policy.MainAgent__c = conAge.id;
		insert policy;
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.selectKind = E_Const.CS_CONTRACTOR;
			controller.selectTerm = E_Const.CS_DISTORIBUTOR;
			//controller.iDistoributor.AccountId = accDis.id;
			controller.iDistoributor.AccountId = parentAccDis.id;
			//controller.iPolicyNo = policy.COMM_CHDRNUM__c;
			controller.iCustomerNo = conCont.E_CLTPF_CLNTNUM__c;
			controller.iCustomerKana = conCont.E_CLTPF_ZCLKNAME__c;
			controller.iCustomerKanji = conCont.LastName;
			controller.doSearch();
			Test.stopTest();
		}
		System.debug(controller);
		system.assertEquals(1, controller.searchResult.size());
		//system.assertEquals(0, controller.searchResultDisply.size());
        system.debug('End-------doSearchDistoributorAndContractorTest');
	}

	/**
	 * doSearch test
	 * 代理店
	 * 被保険者-証券番号,被保険者-顧客番号,被保険者-被保険者カナ,被保険者-被保険者名漢字
	 */
	private static testMethod void doSearchDistoributorAndInsuredTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//契約者
		Contact conCont = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '11111111', '');
		conCont.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
		insert conCont;
		//被保険者
		Contact conIns = TestE_TestUtil.createContact(false, accCust.Id, 'testsan2', '22222222', '');
		conIns.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
		insert conIns;
		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, conCont.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxx1');
		policy.COMM_STATCODE__c = 'SA';
		policy.MainAgent__c = conAge.id;
		policy.Insured__c = conIns.id;
		insert policy;
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.selectKind = E_Const.CS_INSURED;
			controller.selectTerm = E_Const.CS_DISTORIBUTOR;
			controller.iDistoributor.AccountId = accDis.id;
			controller.iPolicyNo = policy.COMM_CHDRNUM__c;
			controller.iCustomerNo = conIns.E_CLTPF_CLNTNUM__c;
			controller.iCustomerKana = conIns.E_CLTPF_ZCLKNAME__c;
			controller.iCustomerKanji = conIns.LastName;
			controller.doSearch();

			Test.stopTest();
		}
		//system.assertEquals(null, resultPage);
		//system.assertEquals(1, controller.searchResult.size());
		//system.assertEquals(1, controller.searchResultDisply.size());
		System.assert(controller.isSearched, E_PageMessagesHolder.getInstance().getErrorMessages());
	}

	/**
	 * doSearch test
	 * 代理店事務所
	 */
	private static testMethod void doSearchOfficeTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//契約者
		Contact conCont = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '11111111', '');
		conCont.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
		insert conCont;
		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, conCont.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxx1');
		policy.COMM_STATCODE__c = 'SA';
		policy.MainAgent__c = conAge.id;
		insert policy;
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.selectKind = E_Const.CS_CONTRACTOR;
			controller.selectTerm = E_Const.CS_OFFICE;
			controller.iOffice.AccountId = accDis.id;
			controller.doSearch();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
		system.assertEquals(1, controller.searchResult.size());
		system.assertEquals(1, controller.searchResultDisply.size());
		System.assert(controller.isSearched);
	}

	/**
	 * doSearch test
	 * 募集人
	 */
	private static testMethod void doSearchAgentTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//契約者
		Contact conCont = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '11111111', '');
		conCont.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
		insert conCont;
		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, conCont.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxx1');
		policy.COMM_STATCODE__c = 'SA';
		policy.MainAgent__c = conAge.id;
		insert policy;
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.selectKind = E_Const.CS_CONTRACTOR;
			controller.selectTerm = E_Const.CS_AGENT;
			controller.iAgent.MainAgent__c = conAge.id;
			controller.doSearch();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
		system.assertEquals(1, controller.searchResult.size());
		system.assertEquals(1, controller.searchResultDisply.size());
		System.assert(controller.isSearched);
	}

	/**
	 * doSearch test
	 * 0件
	 */
	private static testMethod void doSearchZeroTest() {
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.doSearch();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
		system.assertEquals(0, controller.searchResult.size());
		system.assertEquals(null, controller.searchResultDisply);
		System.assert(controller.isSearched);
	}

	/**
	 * doSearch test
	 * 100件超え
	 */
	private static testMethod void doSearchNumberOverTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');

		List<Contact> conList = new List<Contact>();
		List<E_Policy__c> poList = new List<E_Policy__c>();

		for (Integer i=0; i<200; i++){
			//契約者
			Contact con = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', String.valueOf(i), '');
			con.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
			conList.add(con);
		}
		insert conList;

		//カウント
		Integer i = 1;
		for (Contact con : conList) {
			String cno = String.valueof(i);
			//保険契約ヘッダ
			E_Policy__c policy = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_COLI, cno);
			policy.COMM_STATCODE__c = 'SA';
			policy.MainAgent__c = conAge.id;
			poList.add(policy);
			i++;
			cno = null;
		}
		insert poList;

		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			controller.doSearch();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
		system.assertEquals(200, controller.searchResult.size());
		system.assertEquals(100, controller.searchResultDisply.size());
		System.assert(controller.isSearched);
	}


	/**
	 * innerClass test
	 */
	private static testMethod void innerClassTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//契約者/被保険者1
		Contact conCont1 = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '11111111', '');
		conCont1.E_CLTPF_ZCLKNAME__c = 'サシスセソ';
		PageReference resultPage;
		insert conCont1;
		//契約者/被保険者2
		Contact conCont2 = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '2222222', '');
		conCont2.E_CLTPF_ZCLKNAME__c = 'カキクケコ';
		insert conCont2;
		//契約者/被保険者3
		Contact conCont3 = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '33333333', '');
		conCont3.E_CLTPF_ZCLKNAME__c = 'タチツテト';
		insert conCont3;
		//保険契約ヘッダ1
		E_Policy__c policy1 = TestE_TestUtil.createPolicy(false, conCont1.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxx1');
		policy1.COMM_STATCODE__c = 'SA';
		policy1.Insured__c = conCont1.Id;
		policy1.MainAgent__c = conAge.id;
		insert policy1;
		//保険契約ヘッダ2
		E_Policy__c policy2 = TestE_TestUtil.createPolicy(false, conCont2.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxx2');
		policy2.COMM_STATCODE__c = 'SA';
		policy2.Insured__c = conCont2.Id;
		policy2.MainAgent__c = conAge.id;
		insert policy2;
		//保険契約ヘッダ3
		E_Policy__c policy3 = TestE_TestUtil.createPolicy(false, conCont3.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxx3');
		policy3.COMM_STATCODE__c = 'SA';
		policy3.Insured__c = conCont3.Id;
		policy3.MainAgent__c = conAge.id;
		insert policy3;

		Test.startTest();

		//契約者
		List<E_CustomerSearchController.SearchResult> srList = new List<E_CustomerSearchController.SearchResult>();
		srList.add(new E_CustomerSearchController.SearchResult(conCont1, E_Const.CS_CONTRACTOR));
		srList.add(new E_CustomerSearchController.SearchResult(conCont2, E_Const.CS_CONTRACTOR));
		srList.add(new E_CustomerSearchController.SearchResult(conCont3, E_Const.CS_CONTRACTOR));
		srList.sort();
		//被保険者
		srList = new List<E_CustomerSearchController.SearchResult>();
		srList.add(new E_CustomerSearchController.SearchResult(conCont1, E_Const.CS_INSURED));
		srList.add(new E_CustomerSearchController.SearchResult(conCont2, E_Const.CS_INSURED));
		srList.add(new E_CustomerSearchController.SearchResult(conCont3, E_Const.CS_INSURED));
		srList.sort();
		Test.stopTest();
		system.assert(resultPage==null);
	}

	/**
	 * error test
	 */
	private static testMethod void errorTest() {
		Account acc = TestE_TestUtil.createAccount(true);
		Contact conC = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		TestE_TestUtil.createPolicy(true, conC.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxxx');
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '2';
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);

			controller = new E_CustomerSearchController();
			resultPage = controller.pageAction();

			Test.stopTest();
		}
		system.assert(resultPage!=null);
	}

	/* 
	 * フィルターモード
	 */
	private static testMethod void filterTest() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//顧客
		Account accCust = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conAge = TestE_TestUtil.createContact(false, accDis.Id, 'testsan', '99999999', '');
		conAge.E_CL3PF_AGNTNUM__c = '12345';
		conAge.E_CL3PF_VALIDFLAG__c = '2';
		insert conAge;
		
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'conAge', 'E_PartnerCommunity', conAge.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestE_TestUtil.createContact(false, accDis.Id, 'agentB', '88888888', '');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;
		//ID管理を付与
		E_IDCPF__c idcpfB = TestI_TestUtil.createIDCPF(false, userB.Id, 'AT');
		idcpfB.FLAG01__c = '1';
		idcpfB.FLAG02__c = '1';
		idcpfB.FLAG04__c = '1';
		insert idcpfB;

		//契約者
		Contact conCont = TestE_TestUtil.createContact(false, accCust.Id, 'testsan1', '11111111', '');
		conCont.E_CLTPF_ZCLKNAME__c = 'アイウエオ';
		insert conCont;
		
		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, conCont.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxx1');
		policy.COMM_STATCODE__c = 'SA';
		policy.MainAgent__c = conAge.id;
		insert policy;
		
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		u = [select usertype from user where Id =: u.Id];
		//ID管理
		E_IDCPF__c idcpf = TestI_TestUtil.createIDCPF(false, u.Id, 'EP');
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.ZINQUIRR__c = 'BR**';
		idcpf.OwnerId = u.Id;
		insert idcpf;

		PageReference resultPage;
		E_CustomerSearchController controller;

		System.runAs(u){
			//=====================テスト開始=====================
			Test.startTest();

			PageReference pref = Page.E_CustomerSearch;
			Test.setCurrentPage(pref);
			E_IRISHandler iris = E_IRISHandler.getInstance();
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');

			controller = new E_CustomerSearchController();
			resultPage = controller.pageAction();
			controller.doSearch();
			//=====================テスト終了=====================
			Test.stopTest();
			system.assertEquals(1, controller.searchResult.size());
		}
	}
}