@isTest
private with sharing class MNT_SearchEIDCTest{
		private static testMethod void testPageMethods() {	
			MNT_SearchEIDC page = new MNT_SearchEIDC(new ApexPages.StandardController(new E_IDCPF__c()));	
			page.getOperatorOptions_E_IDCPF_c_User_c();	
			page.getOperatorOptions_E_IDCPF_c_OwnerId();	
			page.getOperatorOptions_E_IDCPF_c_ContactCL3PF_AGNTNUM_c();	
			page.getOperatorOptions_E_IDCPF_c_ContactAccount_c();	
			page.getOperatorOptions_E_IDCPF_c_ZEMAILAD_c();	
			page.getOperatorOptions_E_IDCPF_c_ZIDTYPE_c();	
			page.getOperatorOptions_E_IDCPF_c_ZSTATUS01_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		MNT_SearchEIDC.Component3 Component3 = new MNT_SearchEIDC.Component3(new List<E_IDCPF__c>(), new List<MNT_SearchEIDC.Component3Item>(), new List<E_IDCPF__c>(), null);
		Component3.create(new E_IDCPF__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}