public class E_UPRedirect001Controller extends E_AbstractUnitPriceRedirectHandler {
    
    /* コンストラクタ */
    public E_UPRedirect001Controller(){
        super();
        pgTitle = 'ユニットプライス';
        redirectPref = Page.up002;
    }   
}