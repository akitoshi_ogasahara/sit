global with sharing class MNT_DocumentMaintenanceExtender extends SkyEditor2.Extender{
    MNT_DocumentMaintenance extension;

    private I_ContentMaster__c record;
    //編集中レコードのid
    private String param;
    // 添付ファイル
    public Attachment uploadFile {get; set;}
    public boolean isUpload {get; set;}
    //　コピー判定用
    private String clone;
    //　編集時
    private boolean isEdit;


    public MNT_DocumentMaintenanceExtender(MNT_DocumentMaintenance extension){
        this.extension = extension;
        uploadFile = new Attachment();
        isEdit = false;
    }

    global override void init(){
        this.record = extension.record;

        ApexPages.PageReference page = ApexPages.currentPage();
        param = page.getParameters().get('id');

        // 新規作成時
        if(String.isBlank(param)){
            //　キャンセルボタン押下時に、元のレコード詳細画面へ遷移するためのId取得
            param = ApexPages.currentPage().getParameters().get('rid');
            record.ClickAction__c = I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT;
            record.DocumentCategory__c = I_Const.IRIS_COMPANY_GUIDANCE;
            // ファイル添付はないため
            isUpload = true;
        }else{
            clone = page.getParameters().get('clone');
            if(clone != null && clone.equals('1')){
                // コピー時
                //初期化処理
                record.UpsertKey__c = null;
                record.ParentContent__c = null;
                record.filePath__c = null;
                // ファイル添付はないため
                isUpload = true;
            }else{
                //　編集時
                Attachment alreadyFiles = E_AttachmentDao.getRecById(record.Id);
                isEdit = true;
                if(alreadyFiles == null){
                    isUpload = true;
                }else{
                    isUpload = false;
                }
            }
        }
    }

    //コピーキャンセル時コピー元のレコード詳細画面へ遷移
    public PageReference doCancel(){
        PageReference pageRef = Page.MNT_DocumentView;
        pageRef.getParameters().put('id', param);
        return pageRef;
    }


    public PageReference doSave(){

        if(String.isBlank(record.Name)){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料名を入力してください。'));
            return null;
        }else if(String.isBlank(record.FormNo__c)){
            ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, 'フォームNoを入力してください。'));
            return null;
        }else{
            // UpsertKey生成時のエラー回避のため入力されているNameとフォームNoが一致するレコードの有無チェック
            I_ContentMaster__c exitContent = I_ContentMasterDao.getRecByFormNoAndName(record.FormNo__c, record.Name);
            if(exitContent == null || (exitContent.Id == param && isEdit)){
                //　保存処理
                MNT_Util.saveParentWithAttachment(record, uploadFile);
                PageReference pageRef = Page.MNT_DocumentView;
                pageRef.getParameters().put('id', record.Id);
                return pageRef;
            }else{
                ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料名とフォームNoが同一の資料がございます。'));
                return null;
            }

        }

    }

}