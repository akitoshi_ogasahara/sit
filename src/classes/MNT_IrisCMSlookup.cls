global with sharing class MNT_IrisCMSlookup extends SkyEditor2.SkyEditorPageBaseWithSharing{

	public I_ContentMaster__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component4{get;set;}
	public List<SelectOption> Component4_options {get;set;}
	{
	setApiVersion(31.0);
	}
	public MNT_IrisCMSlookup(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.Page__c;
        f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DispAs__c;

		try {
			mainRecord = null;
			mainSObjectType = I_ContentMaster__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempProductLookup_01;

			Component4 = new SkyEditor2__SkyEditorDummy__c();

			queryMap.put(
				'Component3',
				new SkyEditor2.Query('I_ContentMaster__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('Page__c')
					.addField('Name')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component4, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('eq'), false, true, false))
					 .addWhere(' (  ( Name = \'お知らせ\' OR Name = \'■Welcome News\' OR Name = \'販売促進資料\' OR Name = \'■NN TIMES\' OR Page__r.Name = \'コンプライアンス事例集\')  AND DispAs__c = \'セクション\') ')
			);

			Component3 = new Component3(new List<I_ContentMaster__c>(), new List<Component3Item>(), new List<I_ContentMaster__c>(), null);
			listItemHolders.put('Component3', Component3);
			Component3.ignoredOnSave = true;

			recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_ContentMaster__c.SObjectType);

			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			update_Component4_options();
			initSearch();

		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e){
			e.setMessagesToPage();
		} catch (SkyEditor2.Errors.PricebookNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
			hidePageBody = true;
		}
	}



	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}

			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}


	public void update_Component4_options() {
		Component4_options = new SelectOption[]{ new SelectOption('', label.none) };
			SObject[] results;
			String soql = 'SELECT Name FROM I_ContentMaster__c WHERE Name != null ';
			soql += 'AND(  ( Name = \'お知らせ\' OR Name = \'■Welcome News\' OR Name = \'販売促進資料\' OR Name = \'■NN TIMES\')  AND DispAs__c = \'セクション\') ';
			soql += 'GROUP BY Name ORDER BY Name LIMIT 999';
			results = Database.query(soql);
			for (SObject r : results) {
				String value = (String)(r.get('Name'));
				Component4_options.add(new SelectOption(value, value));
			}
	}

}