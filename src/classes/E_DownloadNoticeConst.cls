public class E_DownloadNoticeConst {
    
    /* メッセージ */
    // 該当する情報はございません。
    public static final String ERROR_MSG_NODATA = 'ERR|009';
    //対象データが0件です。
    public static final String NOTICE_MSG_DATA0 = 'DNI|002';
    //対象データが1000件以上存在しています。
    public static final String NOTICE_MSG_DATA1000 = 'DNI|003';
    
    /* オブジェクトAPI名 */
    public static String OBJ_API_E_AGPO = 'E_AGPO__c';		// 保有契約異動通知
    public static String OBJ_API_E_BILA = 'E_BILA__c';		// 保険料請求予告通知
    public static String OBJ_API_E_BILS = 'E_BILS__c';		// 保険料請求通知
    public static String OBJ_API_E_NCOL = 'E_NCOL__c';		// 保険料未入金通知
    
    /* オブジェクト名 */
    //保険料未入金通知
    public static final String E_NCOLNM = E_NCOL__c.sObjectType.getDescribe().getLabel();
    //保有契約異動通知
    public static final String E_AGPONM = E_AGPO__c.sObjectType.getDescribe().getLabel();
    //保険料請求予告通知
    public static final String E_BILANM = E_BILA__c.sObjectType.getDescribe().getLabel();
    //保険料請求通知
    public static final String E_BILSNM = E_BILS__c.sObjectType.getDescribe().getLabel();
    //帳票番号Map
    public static final Map<String,Integer> FORMATNO_MAP = new Map<String,Integer>{
	                                                                                    E_NCOLNM    => 1,   //未入金
	                                                                                    E_AGPONM    => 2,   //異動
	                                                                                    E_BILSNM    => 3,   //請求
	                                                                                    E_BILANM    => 4    //予告
	                                                                                };
    //ダウンロード履歴.帳票種別
    public static final String DH_FORMNO_NOTICE_AGPO = 'A';     //保有契約異動通知
    public static final String DH_FORMNO_NOTICE_BILA = 'B';     //保険料請求予告通知
    public static final String DH_FORMNO_NOTICE_BILS = 'C';     //保険料請求通知
    public static final String DH_FORMNO_NOTICE_NCOL = 'D';     //保険料未入金通知

    //EBiz連携ログ.区分名称
    public static final String BIZSYNC_KIND_NOTICE_AGPO = 'A';     //保有契約異動通知
    public static final String BIZSYNC_KIND_NOTICE_BILS = 'B';     //保険料請求通知
    public static final String BIZSYNC_KIND_NOTICE_BILA = 'C';     //保険料請求予告通知
    public static final String BIZSYNC_KIND_NOTICE_NCOL = 'D';     //保険料未入金通知

    //対象外項目表示文字
    public static final String EXCLUDED = '-';

    //ObjName => EBiz連携ログ.区分名称
    public static final Map<String,String> KIND_MAP = new Map<String,String>{
        E_DownloadNoticeConst.OBJ_API_E_BILA => E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA,     // 保険料請求予告通知
        E_DownloadNoticeConst.OBJ_API_E_BILS => E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS,     // 請求通知
        E_DownloadNoticeConst.OBJ_API_E_NCOL => E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL,     // 未入金通知
        E_DownloadNoticeConst.OBJ_API_E_AGPO => E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO      // 異動通知
    };
    //ダウンロード履歴.帳票種別 => ObjName
    public static final Map<String,String> FNO_OBJNM_MAP = new Map<String,String>{
        E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA => E_DownloadNoticeConst.OBJ_API_E_BILA,     // 保険料請求予告通知
        E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILS => E_DownloadNoticeConst.OBJ_API_E_BILS,     // 請求通知
        E_DownloadNoticeConst.DH_FORMNO_NOTICE_NCOL => E_DownloadNoticeConst.OBJ_API_E_NCOL,     // 未入金通知
        E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO => E_DownloadNoticeConst.OBJ_API_E_AGPO      // 異動通知
    };
    /* 区分（上段） */
    //未納
    public static final String KEVNTDSC01_NONPAYMENT = '未納';

    /* 募集人SEQ */
    //主募集人
    public static final String ZJINTSEQ_01 = '1';

    /* 課コード */
    //拠点が主担当
    public static final String KSECTION00 = '00';
    //本社営業部が主担当
    public static final String KSECTION88 = '88';

}