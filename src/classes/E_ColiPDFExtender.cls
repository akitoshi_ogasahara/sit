global with sharing class E_ColiPDFExtender extends E_AbstractViewExtender {
    private static final String PAGE_TITLE = '契約内容個別照会(個人保険)';


    E_ColiPDFController extension;

    private static final String substituteRequestName = '指定代理請求特約';
    // 保険契約特約のレコード
    private List<E_COVPF__c> covpfList = new List<E_COVPF__c>();
    // 主契約の保険種類コード
    private String MainContractCord {get;set;}
    // 主契約の指定代理請求特則フラグ(COLI_ZDPTYDSP)
    private Boolean ColiZdptydspFlg {get;set;}
    // 解約時受取金額
    private Decimal PremiumSum = 0;
    private String CustomerNumber = '';

    // 主契約の保険種類コードに入りうる値の定義
    private static final String CRTABLE_MY = 'MY'; // 特約の保険種類コードMY
    private static final String CRTABLE_MZ = 'MZ'; // 特約の保険種類コードMZ
    private static final String CRTABLE_MX = 'MX'; // 特約の保険種類コードMX
    private static final String CRTABLE_MW = 'MW'; // 特約の保険種類コードMW

    //遷移元情報取得
    public String getFromType(){
        return E_Const.FROM_COLI;
    }

    /* コンストラクタ */
    public E_ColiPDFExtender(E_ColiPDFController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }

    /** init */
    global override void init(){
        // アクセスチェック
        pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
        if (pageRef == null) {
            // 契約者、被保険者、年金受取人情報を追加取得
            E_ContactDaoWithout.fillContactsToPolicy(extension.record, true, false,false,true, true);
            // データテーブルから個人保険特約データを取得
            initDataTableCOVPF();
            // 個人保険特約データの中から主契約の保険種類コードを取得
            initCrtableCord();
            // COLIDataTable
            setUnitPriceTable();
        }
    }

    public PageReference pageAction () {
        return E_Util.toErrorPage(pageRef, null);
    }

    /*
     * ＜解約時受取金額の計算＞
     * ※SVEからコール
     * 【保険契約ヘッダ】の対象項目において下記計算を行う
     * （１）解約返戻金計算不能フラグ==true または 未経過保険料計算不能フラグ==true のとき
     *  ?を返す。
     * （２）解約返戻金計算不能フラグ==false かつ 未経過保険料計算不能フラグ==false のとき
     *  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料
     * （３）上記以外の時、
     *  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料＋未経過保険料
     */
    public Decimal getSurrenderValueSum(){

        // 解約返戻金
        Decimal Surrender1 = (Decimal)extension.record.get('COLI_ZCSHVAL__c');
        // 保険料振替貸付合計
        Decimal Surrender2 = (Decimal)extension.record.get('COLI_ZEAPLTOT__c');
        // 契約者貸付合計
        Decimal Surrender3 = (Decimal)extension.record.get('COLI_ZEPLTOT__c');
        // 前納未経過保険料
        Decimal Surrender4 = (Decimal)extension.record.get('COLI_ZEADVPRM__c');
        // 未経過保険料 (未経過期間に対応する額)
        Decimal Surrender5 = (Decimal)extension.record.get('COLI_ZUNPREM__c');

        //　解約返戻金計算不能フラグ
        Boolean flg1 = (Boolean)extension.record.get('COLI_ZCVDCF__c');
        //　未経過保険料計算不能フラグ
        Boolean flg2 = (Boolean)extension.record.get('COLI_ZUNPCF__c');
        //　未経過保険料表示フラグ
        Boolean flg3 = (Boolean)extension.record.get('COLI_ZUNPDCF__c');

        if(flg1==null){
            flg1 = false;
        }
        if(flg2==null){
            flg2 = false;
        }
        if(flg3==null){
            flg3 = false;
        }
        //合計値
        Decimal SurrenderSum = 0;

            // （２）未経過表示フラグ==false かつ 未経過保険料計算不能フラグ==false のとき
            //  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料
            // （３）それ以外の時、
            //  解約返戻金－（保険料振替貸付合計＋契約者貸付）＋前納未経過保険料＋未経過保険料

            if(Surrender1!=null && Surrender1!=0){
                // 解約返戻金 (COLI_ZCSHVAL__c)
                SurrenderSum += Surrender1;
            }
            if(Surrender2!=null && Surrender2!=0){
                // 貸付金等精算額 (COLI_ZLOANTOT__c)
                // マイナス
                SurrenderSum -= Surrender2;
            }
            if(Surrender3!=null && Surrender3!=0){
                // 契約者貸付合計 (COLI_ZEPLTOT__c)
                // マイナス
                SurrenderSum-= Surrender3;
            }
            if(Surrender4!=null && Surrender4!=0){
                // 前納未経過保険料 (COLI_ZEADVPRM__c)
                SurrenderSum += Surrender4;
            }

            if(Surrender5!=null && Surrender5!=0){
                // 未経過保険料 (COLI_ZUNPREM__c)
                SurrenderSum += Surrender5;
            }

            if((!flg3) && (!flg2)){
                if(Surrender5!=null && Surrender5!=0){
                    // 未経過保険料 (COLI_ZUNPREM__c)
                    SurrenderSum -= Surrender5;
                }
            }
            return SurrenderSum;

    }

    /*
     * ＜加算フラグをもとに、保険料の合算を行う。＞
     * ※SVEからコール
     * 加算フラグがtrueである時、主契約および特約の保険料を合算する。値がないならば0を返す。
     */
    public Decimal getPremiumValueSum(){
        // 保険契約特約の値がある場合
        if(covpfList!=null){
            for(E_COVPF__c item :covpfList ){
                // 加算フラグがtrueである場合、保険料を合算していく
                // 2015/02/06 加算フラグの制御を消した。
                if(item.COLI_INSTPREM__c!=null){
                    PremiumSum += item.COLI_INSTPREM__c;
                }
            }
            return Integer.valueOf(PremiumSum);
        }
        return 0;
    }

    /*
     * ＜現在死亡保険金額の値を取得＞
     * 主契約の保険種類コード(COMM_CRTABLE2__c)がMYまたはMZのとき
     * 主契約(C)の現在死亡保険金額(COLI_ZDTHAMTA__c)を取得する
     */
    public Decimal getDeathBenefit(){
        Decimal deathBenefitPremium = 0;
        //　保険契約特約のデータが存在し、主契約の保険種類がMYまたはMZの時
        if(covpfList!=null && mainContractCord !=null && mainContractCord!=''){
            if( mainContractCord.equals(CRTABLE_MY) || mainContractCord.equals(CRTABLE_MZ) ){
                for(E_COVPF__c item :covpfList ){
                    // 主契約フラグがC(主契約)の時
                    if(item.COLI_ZCRIND__c.equals('C')){
                        deathBenefitPremium = item.COLI_ZDTHAMTA__c;
                    }
                }
            }
        }
        return deathBenefitPremium;
    }

    /*
     * ＜主契約の保険種類で【保険契約ヘッダ】[現在死亡保険金額]の表示を制御する＞
     * もし、MYまたはMZならばtrue
     */
    public boolean getCrtableCordFlg(){
        Boolean flg = false;
        //MYとMZの時は表示する
        if(covpfList!=null && mainContractCord !=null && mainContractCord!=''){
            if(( mainContractCord.equals(CRTABLE_MY) || mainContractCord.equals(CRTABLE_MZ))){
                flg = true;
            }
        }
        return flg;
    }


    /*
     * ＜団体名称を取得＞
     * 【保険契約ヘッダ】[団体顧客番号(COLI_ZGCLTNM__c)]==【Contact】[顧客番号]の時
     * ※SVEからコール
     */
    public String getCorporationName(){
        customerNumber = (String)extension.record.get('COLI_ZGCLTNM__c'); //団体顧客番号
        if(!(String.isBlank(customerNumber))){
            // 【個人保険ヘッダ】[団体顧客番号]==【Contact】[顧客番号] となるContactレコードを取得
            Contact corp = E_ContactDaoWithout.getRecByCustomerNumber(CustomerNumber);
            if(corp!=null){
                return corp.Name;
            }else{
                // ContactレコードのNameがnuｌｌの時
                return '';
            }
        }else{
            // 【個人契約ヘッダ】[団体顧客番号]がnuｌｌの時
            return '';
        }
    }

    /*
     * ＜年金分割回数(年間)を取得＞
     * 【保険契約ヘッダ】[年金分割回数(年間)(SPVA_ZANNFREQ__c)]
     * ※SVEからコール
     */
/*    public String getZANNFREQ(){
        Boolean zannstflg = extension.record.SPVA_ZANNSTFLG__c;
        
        customerNumber = (String)extension.record.get('SPVA_ZANNFREQ__c'); //年金分割回数(年間)
        if(!(String.isBlank(customerNumber))){
            // 【個人保険ヘッダ】[団体顧客番号]==【Contact】[顧客番号] となるContactレコードを取得
            Contact corp = E_ContactDaoWithout.getRecByCustomerNumber(CustomerNumber);
            if(corp!=null){
                return corp.Name;
            }else{
                // ContactレコードのNameがnuｌｌの時
                return '';
            }
        }else{
            // 【個人契約ヘッダ】[団体顧客番号]がnuｌｌの時
            return '';
        }
    }
*/
    
    /*
     * ＜個人保険特約＞
     * データテーブル(dataTableCOVPFItem)を用いて
     * 【個人保険特約】からレコードを取得する。
     */
     private void initDataTableCOVPF(){
        List<E_ColiPDFController.dataTableCOVPFItem> items = extension.dataTableCOVPF.items;
        //もしデータテーブルにレコードがある場合
        if(items!=null){
            for(E_ColiPDFController.dataTableCOVPFItem item :items ){
                E_COVPF__c obj = (E_COVPF__c)item.record;
                covpfList.add(obj);
            }
        }
     }

    /*

     * ＜主契約の保険種類コード取得＞
     * データテーブル(dataTableCOVPFItem)を用いて
     * 主契約の保険種類コードを取得する。
     */
    private void initCrtableCord(){
        List<String> crtables =new List<String>();
        if(covpfList!=null){
            for(E_COVPF__c item :covpfList ){
                // 主契約フラグがC(主契約)の時
                if(item.COLI_ZCRIND__c.equals('C')){
                    mainContractCord = (String)item.COMM_CRTABLE2__c;
                }
            }
            // 保険種類コードのListの1列目のデータがnullまたはカラでない時
            //mainContractCord='';
        }
    }

    /*
     * 主契約の保険種類コード
     */
    public String getMainCord(){
        return mainContractCord;
    }


    /*
     * ＜主契約の指定代理請求特約表示フラグ情報を取得＞
     * データテーブル(dataTableCOVPFItem)を用いて
     * 主契約のフラグを取得する。
     */
    public Boolean getDesignationSubstitutionFlg(){
        Boolean flg = false;
        List<String> crtables =new List<String>();
        if(covpfList!=null){
            for(E_COVPF__c item :covpfList ){
                // 主契約フラグがC(主契約)の時
                if(item.COLI_ZCRIND__c.equals('C')){
                    //指定代理請求特約表示フラグ
                    flg = (Boolean)item.COLI_ZDPTYDSP__c;
                    return flg;
                }
            }
        }
        return flg;
    }

//==========================================================
//   C O L I データテーブル対応　
//==========================================================

    private void setUnitPriceTable() {
        System.debug('=====COLIpdfTableStart=====');
        // 契約にひもづく特約を取得。主契約フラグでOrderBy　←再取得＆クリアは意味が無い＆ページとの2重管理となる為、要検討
        covpfList = E_COVPFDao.getRecByIds(extension.record.id);
        // COLIデータテーブルの値をクリアする。
        extension.dataTableCOVPF.clear();
        if(covpfList!=null){
            for(E_COVPF__c covpf : covpfList){
                
                // 保障内容 
                String hosyouNaiyou = '';
                if(covpf.COLI_ZCRIND__c.equals('C') ){
                    // Cならば主契約
                    hosyouNaiyou = '主契約';
                }else{
                    // Rならば特約
                    hosyouNaiyou = '特約';
                }
  
                // 保険金額/入院給付金日額/年金（月）額 :現在
                String genzaiHoken = '';
                // 【特約】.[現在Ｓ計算不能不能フラグ]
                if(covpf.COLI_ZSUMINF__c){
                    genzaiHoken = '?';
                }else{
                    // 【特約】.[主契約現在保険金額]
                    if(covpf.COMM_SUMINS__c!=null){
                        genzaiHoken = String.valueOf((covpf.COMM_SUMINS__c).format());
                    }
                }

                //　保険金額/入院給付金日額/年金（月）額 :加入時 
                String kanyuHoken ='';
                // 【個人保険特約】.[加入時保険金額]
                if(covpf.COLI_ZSTDSUM__c!=null){
                    kanyuHoken = String.valueOf((covpf.COLI_ZSTDSUM__c).format());
                }

                // 保険料(※1)(円) 
                String hokenryou = '';
                // 【個人保険特約】.[保険料]
                if(covpf.COLI_INSTPREM__c!=null){
                    hokenryou = String.valueOf((covpf.COLI_INSTPREM__c).format());
                }
                
                System.debug('=====COLIpdfデータテーブルに値を追加=====');
                extension.dataTableCOVPF.add(
                    new E_COVPF__c(
                        // 保障内容
                        coli_text255_1__c = hosyouNaiyou,
                        // 保険種類
                        coli_text255_2__c = covpf.COLI_ZCOVRNAMES__c,
                        // 保険期間(歳/年)
                        COMM_ZRCESDSC__c = covpf.COMM_ZRCESDSC__c,
                        // 保険料払込期間(歳/年)
                        COMM_ZPCESDSC__c = covpf.COMM_ZPCESDSC__c,
                        //　保険金額/入院給付金日額/年金（月）額 :加入時
                        coli_text255_3__c = kanyuHoken,
                        // 保険金額/入院給付金日額/年金（月）額 :現在
                        coli_text255_4__c = genzaiHoken,
                        // 保険料(※1)(円)
                        coli_text255_5__c = hokenryou
                    )
                );
            }
            
            // 主契約の指定代理請求特約表示フラグ情報がTrueのとき      
            if(getDesignationSubstitutionFlg()){
                System.debug('=====COLIpdf指定代理請求特約=====');
                // 指定代理請求特約のレコードをデータテーブルの最下部に追加
                extension.dataTableCOVPF.add(
                    new E_COVPF__c(
                        // 【個人保険特約】.[保障内容]
                        coli_text255_1__c = '特約',
                        // 【個人保険特約】.[保険種類(結合)]
                        coli_text255_2__c = '指定代理請求特約',  
                        // 【個人保険特約】.[保険期間]
                        COMM_ZRCESDSC__c = '-',
                        // 【個人保険特約】.[払込期間]
                        COMM_ZPCESDSC__c = '-',
                        // 【個人保険特約】.[加入時保険金額]
                        coli_text255_3__c = '-',
                        // 【個人保険特約】.[主契約現在保険金額(数式)]
                        coli_text255_4__c = '-',
                        // 【個人保険特約】.[保険料]
                        coli_text255_5__c = '-'
                    )
                );
            }
        }
        //合計行追加
        extension.dataTableCOVPF.add(
            new E_COVPF__c(
                coli_text255_1__c = '合計',
                coli_text255_5__c = getPremiumValueSum().format()
            )
        );

        System.debug('=====COLITableEnd=====');
    }
    
    /**
     * 戻るボタン
     */
    public override PageReference doReturn(){
        return new PageReference(E_CookieHandler.getCookieRefererPolicy());
    }
}