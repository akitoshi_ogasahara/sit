/**
 * Leadデータアクセスオブジェクト
 * CreatedDate 2016/08/10
 * @Author Terrasky
 */
public with sharing class E_MK_leadTMPDao {

	/*
	 * @param cid 対象キャンペーンターゲットID
	 * @return 関連しているMK_leadTMP__c数
	 */
	public static Integer getRecCountByCmpgnTrgt(ID cid){
		return [select count() from MK_leadTMP__c where MK_CampaignTarget__c = : cid];
	}
	/*
	 * @param id レコードID
	 * @return レコード
	 */
	public static MK_leadTMP__c getRecById(ID id){
		List<MK_leadTMP__c> recs = 	
					[select 
						id
						, Company__c
						, LastName__c
						, FirstName__c
						, LastNameKana__c
						, FirstNameKana__c
						, Email__c
						, MK_CampaignTarget__c 
						, CreateLeadFlag__c
					from 
						MK_leadTMP__c 
					where 
						id =: id
					];
		return (recs.isEmpty())?  null:recs[0];
	}
	/*
	 * @param ctid キャンペーンターゲットID
	 * @return 該当メールリスト
	 */
	public static Set<String> getEmailListByCmpgnTrgt(ID cid){
		Set<String> ret = new Set<String>();
		if(cid == null) return ret;
		for(MK_leadTMP__c rec :[select Email__c from MK_leadTMP__c where MK_CampaignTarget__c =: cid]){
			ret.add(rec.Email__c);
		}
		return ret;
	}
}