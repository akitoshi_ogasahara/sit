public with sharing class E_PieGraphInvestmentController extends E_AbstractPieGraph {
	
	//投信ヘッダID
	public String ithpfId {get;set;}
	//資産配分比率ラベル
	public String ratioLabel {get;set;}
	
	//グラフ作成
	public void createGraph() {
		
		//グラフ表示データ作成
		graphRecs = new List<GraphData>();
		//グラフデータテーブル作成
		graphTableRecs = new List<GraphDataTable>();
		
		ratioLabel = '買付時の資産配分比率';
		if(executeKind == E_Const.GRAPH_INVESTMENT_NOW){
			ratioLabel = '現在の資産配分比率';
		}
		
		//アセットクラス・投信ファンドを取得
		List<E_ITAPF__c> itapfList = E_ITAPFDao.getRecWithFundByflagAndHeaderId(ithpfId);
		
		//アセットクラス毎に集計
		Map<Id, Decimal> graphValueMap = new Map<Id, Decimal>();
		for(E_ITAPF__c itapf : itapfList){
			Decimal graphValue = 0;
			if(itapf.E_ITFPFs__r == null) continue;
			for(E_ITFPF__c itfpf : itapf.E_ITFPFs__r){
				if(executeKind == E_Const.GRAPH_INVESTMENT_BUY && itfpf.ZBRNDPER02__c != null){
					graphValue += itfpf.ZBRNDPER02__c;
				} else if(executeKind == E_Const.GRAPH_INVESTMENT_NOW && itfpf.ZBRNDPER04__c != null){
					graphValue += itfpf.ZBRNDPER04__c;
				}
			}
			graphValueMap.put(itapf.Id, graphValue);
		}
		
		//グラフデータ作成
		Decimal graphValueAmount = 0;
		for(E_ITAPF__c itapf : itapfList){
			Decimal graphValue = graphValueMap.get(itapf.Id);
			if(graphValue == null) graphValue = 0;
			graphValueAmount += graphValue;
			//グラフ表示データ作成
			GraphData graph = new GraphData(itapf.ZASSTNAM__c, graphValue);
			graphRecs.add(graph);
			//グラフデータテーブル作成
			GraphDataTable graphTab = new GraphDataTable(itapf.ZCOLORCD__c, itapf.ZASSTNAM__c, 0, graphValue);
			graphTableRecs.add(graphTab);
			//グラフの色設定
			colorList.add(itapf.ZCOLORCD__c);
		}
		
		//合計を追加
		GraphDataTable graphTab = new GraphDataTable('#FFFFFF', '合計', 0, graphValueAmount);
		graphTableRecs.add(graphTab);
		//colorList.add('#FFFFFF');
		System.debug(colorList);
		colors = String.join(colorList, ',');
		//colors = sortDescColor(colorList);
	}
}