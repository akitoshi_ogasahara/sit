public with sharing class AGC_AgencyInfo_Ctrl {

	//代理店基本情報
	private AGC_DairitenKihonjoho__c kihon;
	//代理店補足情報
	private AGC_DairitenJimushoHosokujoho__c hosoku;
	//支社マスタ
	private AGC_Shisha_Mst__c shishaMst;
	//担当者マスタ
	private AGC_ShishaMr_Mst__c mrMst;
	//Daoクラス
	private AGC_Common_Dao commonDao = new AGC_Common_Dao();
	
	
	public AGC_AgencyInfo_Ctrl(ApexPages.StandardController controller) {
		//リクエストIDを取得する
		ID id = ApexPages.currentPage().getParameters().get('id');
		
		try{
			kihon = commonDao.getKihonJoho(id);//代理店基本情報を取得する。
			hosoku = commonDao.getDairitenHosokujohoBykihonId(id);	 //代理店補足情報を取得する。
		}catch(System.QueryException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, '本機能の使用方法が誤っているか、データに不整合が発生しています。問題が解決しない場合、システム管理者までご連絡ください。');
			ApexPages.addMessage(msg);
			return;
		}
		
		try{
			shishaMst = commonDao.getShishaMstById(kihon.BranchCodeRef__c);	 //支社マスタを取得する。
			mrMst = commonDao.getShishaMrMstById(kihon.MRCodeRef__c);
		}catch(System.QueryException e){
			//マスタデータが取得できない場合は無視する。
		}
	}

	//代理店基本情報
	public AGC_DairitenKihonjoho__c getKihon(){
		return kihon;
	}
	
	//補足情報
	public void setHosoku(AGC_DairitenJimushoHosokujoho__c value){
		hosoku = value;
	}
	public AGC_DairitenJimushoHosokujoho__c getHosoku(){
		return hosoku;
	} 
	
	//支社マスタ
	public AGC_Shisha_Mst__c getShishaMst(){
		return shishaMst;
	}
	
	public AGC_ShishaMr_Mst__c getMrMst(){
		return mrMst;
	}
	
	public PageReference saveHosokuInfo(){
		//新規レコードを編集済みとして、新規フラグをfalseへ変更
		hosoku.NewRecord__c = false;
		upsert hosoku;
		return (new ApexPages.StandardController(kihon)).view();
	}
}