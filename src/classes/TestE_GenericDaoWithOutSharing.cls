@isTest
private class TestE_GenericDaoWithOutSharing {
	
	/**
	 * void insertRecord(sObject record)
	 */
	@isTest static void insertRecord_test001() {
		sObject sObj = new Account(Name='testaccount');

		Test.startTest();
		E_GenericDaoWithOutSharing.insertRecord(sObj);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name = 'testaccount'];
		System.assertEquals(1, results.size());
	}

	/**
	 * void insertRecord(List<sObject> record)
	 */
	@isTest static void insertRecord_test002() {
		List<sObject> sObjs = new List<sObject>();
		sObjs.add(new Account(Name='testaccount1'));
		sObjs.add(new Account(Name='testaccount2'));

		Test.startTest();
		E_GenericDaoWithOutSharing.insertRecord(sObjs);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name Like 'testaccount%'];
		System.assertEquals(2, results.size());
	}

	/**
	 * void updateRecord(sObject record)
	 */
	@isTest static void updateRecord_test001() {
		Account acc = new Account(Name='testaccount');
		insert acc;

		acc.Name = 'updaccount';
		sObject sObj = acc;

		Test.startTest();
		E_GenericDaoWithOutSharing.updateRecord(sObj);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name = 'updaccount'];
		System.assertEquals(1, results.size());
	}

	/**
	 * void updateRecord(List<sObject> record)
	 */
	@isTest static void updateRecord_test002() {
		List<Account> accList = new List<Account>();
		accList.add(new Account(Name='testaccount1'));
		accList.add(new Account(Name='testaccount2'));
		insert accList;

		accList[0].Name = 'upd' + accList[0].Name;
		accList[1].Name = 'upd' + accList[1].Name;

		List<sObject> sObjs = accList;

		Test.startTest();
		E_GenericDaoWithOutSharing.updateRecord(sObjs);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name Like 'upd%'];
		System.assertEquals(2, results.size());
	}

	/**
	 * void upsertRecord(sObject record)
	 */
	@isTest static void upsertRecord_test001() {
		Account acc = new Account(Name='testaccount', E_Upsertkey__c='ups01');
		insert acc;

		acc.Name = 'upsaccount';
		sObject sObj = acc;

		Test.startTest();
		E_GenericDaoWithOutSharing.upsertRecord(sObj);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name = 'upsaccount'];
		System.assertEquals(1, results.size());
	}

	/**
	 * void upsertRecord(List<sObject> record)
	 */
	@isTest static void upsertRecord_test002() {
		List<Account> accList = new List<Account>();
		accList.add(new Account(Name='testaccount1', E_Upsertkey__c='ups01'));
		accList.add(new Account(Name='testaccount2', E_Upsertkey__c='ups02'));
		insert accList;

		accList[0].Name = 'ups' + accList[0].Name;
		accList[1].Name = 'ups' + accList[1].Name;

		List<sObject> sObjs = accList;

		Test.startTest();
		E_GenericDaoWithOutSharing.upsertRecord(sObjs);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name Like 'ups%'];
		System.assertEquals(2, results.size());
	}

	/**
	 * void deleteRecord(sObject record)
	 */
	@isTest static void deleteRecord_test001() {
		Account acc = new Account(Name='testaccount', E_Upsertkey__c='ups01');
		insert acc;

		sObject sObj = acc;

		Test.startTest();
		E_GenericDaoWithOutSharing.deleteRecord(sObj);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name = 'testaccount'];
		System.assertEquals(0, results.size());
	}

	/**
	 * void deleteRecord(List<sObject> record)
	 */
	@isTest static void deleteRecord_test002() {
		List<Account> accList = new List<Account>();
		accList.add(new Account(Name='testaccount1', E_Upsertkey__c='ups01'));
		accList.add(new Account(Name='testaccount2', E_Upsertkey__c='ups02'));
		insert accList;

		List<sObject> sObjs = accList;

		Test.startTest();
		E_GenericDaoWithOutSharing.deleteRecord(sObjs);
		Test.stopTest();

		List<Account> results = [Select Id From Account Where Name Like 'testaccount%'];
		System.assertEquals(0, results.size());
	}
}