@isTest
private class TestI_SurveyController {

	/*
	 * アンケート登録 募集人
	 */
	static testMethod void doConfirmTest01() {
		//データ作成
		User us = createAgentUser();
		I_Survey__c survey = createSurveyRec(us.Id, true);
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		//テスト実施
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			controller.doConfirm();
			Test.stopTest();
			//テスト検証
			I_SurveyQuestion__c questinon = [SELECT Id, Selection__c FROM I_SurveyQuestion__c WHERE Id =: survey.I_SurveyQuestions__r[0].Id];
			I_SurveyAnswer__c answer = [SELECT id, SurveyTargetName__c, SurveyTarget__c, SurveyQuestion__c, Answer__c
										FROM I_SurveyAnswer__c 
										WHERE SurveyQuestion__c =: questinon.id
									   ];
			System.assertEquals(questinon.Selection__c, answer.Answer__c);
		}
	}

	/*
	 * アンケート登録 代理店
	 */
	static testMethod void doConfirmTest02() {
		//データ作成
		User us = createAgentUser();
		I_Survey__c survey = createSurveyRec(us.id, false);
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		//テスト実行
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			controller.pageAction();
			controller.doConfirm();
			//テスト検証
			I_SurveyQuestion__c questinon = [SELECT Id, Selection__c FROM I_SurveyQuestion__c WHERE Id =: survey.I_SurveyQuestions__r[0].Id];
			I_SurveyAnswer__c answer = [SELECT id, SurveyTargetName__c, SurveyTarget__c, SurveyQuestion__c, Answer__c
										FROM I_SurveyAnswer__c 
										WHERE SurveyQuestion__c =: questinon.id
									   ];
			System.assertEquals(questinon.Selection__c, answer.Answer__c);
		}
	}

	/*
	 * 添付ファイル取得
	 */
	static testMethod void getAttachmentTest(){
		//データ作成
		User us = createAgentUser();
		I_Survey__c survey = createSurveyRec(us.Id, true);
		// アンケートに紐づく添付ファイルレコードを作成
		Attachment att = TestI_TestUtil.createAttachment(true, survey.Id);
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		//テスト実施
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			String url = controller.getAttUrl();
			Test.stopTest();
			//テスト検証
			I_Survey__c surveyRec = [SELECT id, (SELECT Id, Name FROM Attachments) FROM I_Survey__c WHERE Id =: survey.id];
			System.assertEquals(att.Name, surveyRec.Attachments[0].Name);
			System.assertNotEquals(null,url);

		}
	}

	/*
	 * アンケート登録 Exception
	 */
	static testMethod void doConfirmException(){
		//データ作成
		User us = createAgentUser();
		//設問が紐づいていないアンケート作成
		I_Survey__c survey = new I_Survey__c();
		survey.Name = 'テストアンケート';
		survey.OverView__c = 'テスト概要';
		survey.UpsertKey__c = 'survey01';
		survey.PeriodFrom__c = Date.today();
		survey.PeriodTo__c = Date.today().addDays(5);
		insert survey;
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		//テスト実行
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			controller.doConfirm();
			Test.stopTest();
			System.assertEquals(true, controller.getPageMessages().getHasMessages());
		}
	}

	/*
	 * アンケート回答済み
	 */
	static testMethod void registeredSurveyRecord(){
		//データ作成
		User us = createAgentUser();
		//アンケートに対象者を紐づけて回答済みにする
		I_Survey__c survey = createSurveyRec(us.Id, true);
		I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
		sTarget.Ownerid = us.id;
		sTarget.Survey__c = survey.id;
		sTarget.Contact__c = us.contactId;
		insert sTarget;
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			Test.stopTest();
			System.assertEquals(true, controller.getPageMessages().getHasMessages());
		}
	}

	/*
	 * アンケート取得失敗
	 */
	static testMethod void faileSurveyRecord(){
		//データ作成
		User us = createAgentUser();
		I_Survey__c survey = createSurveyRec(us.id, true);
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', 'テスト');
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			Test.stopTest();
			System.assertEquals(true, controller.getPageMessages().getHasMessages());
		}
	}
	/*
	 * メールアドレス未登録
	 */
	static testMethod void noEmail(){
		//データ作成
		User us = createAgentUser();
		us.Email = 'testtest@test.com';
		User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
		System.runAs(thisUser){
			update us;
		}
		System.runAs(us){
			I_Survey__c survey = createSurveyRec(us.id, true);
			PageReference pageRef = Page.Iris_Survey;
			Test.setCurrentPageReference(pageRef);
			ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			Test.stopTest();
			System.assertEquals(true, controller.getPageMessages().getHasMessages());
		}
	}

	/*
	 * アンケート期間外
	 */
	static testMethod void failePeriod(){
		//データ作成
		User us = createAgentUser();
		I_Survey__c survey = createSurveyRec(us.id, false);
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		//テスト実行
		System.runAs(us){
			survey.PeriodTo__c = Date.today().addDays(-5);
			update survey;

			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			Test.stopTest();
			System.assertEquals(true, controller.getPageMessages().getHasMessages());
		}
	}

	/*
	 * アンケート対象外
	 */
	static testMethod void NotAgent(){
		//データ作成
		User us = TestI_TestUtil.createUser(true, 'Test' , 'ＭＲ');
		I_Survey__c survey = createSurveyRec(us.id, false);
		PageReference pageRef = Page.Iris_Survey;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('skey', survey.UpsertKey__c);
		//テスト実行
		System.runAs(us){
			I_SurveyController controller = new I_SurveyController();
			Test.startTest();
			controller.pageAction();
			controller.getIsEntryEmail();
			Test.stopTest();
			System.assertEquals(true, controller.getPageMessages().getHasMessages());
		}
	}


	/*
	 * アンケート登録 募集人
	 * @param isAgent： whether Agent
	 * @return I_Survey__c： アンケート
	 */
	private static I_Survey__c createSurveyRec(Id userId, boolean isAgent){
		//アンケート作成
		I_Survey__c survey = new I_Survey__c();
		survey.Name = 'テストアンケート';
		survey.OverView__c = 'テスト概要';
		survey.UpsertKey__c = 'survey01';
		survey.PeriodFrom__c = Date.today();
		survey.PeriodTo__c = Date.today().addDays(5);
		if(isAgent){
			survey.SurveyTarget__c = '募集人';
		}else{
			survey.SurveyTarget__c = '代理店';
		}
		insert survey;
		//アンケートに紐づく対象者作成
		I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
		sTarget.Ownerid = userId;
		sTarget.Survey__c = survey.id;
		insert sTarget;
		//アンケートに紐づく設問作成
		I_SurveyQuestion__c question = new I_SurveyQuestion__c();
		question.Name = 'テスト設問';
		question.Selection__c = '参加';
		question.Survey__c = survey.Id;
		insert question;

		survey = [SELECT Id, UpsertKey__c, (SELECT Id FROM I_SurveyTargets__r), (SELECT Id FROM I_SurveyQuestions__r) FROM I_Survey__c WHERE Id =: survey.Id];
		return survey;
	}

	/*
	 * 代理店ユーザー作成
	 * @return User: ユーザー
	 */
	private static User createAgentUser(){
		User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
		User us = new User();
		System.runAs(thisUser){
			Account parentAcc = TestI_TestUtil.createAccount(true, null);
			Account acc = TestI_TestUtil.createAccount(true, parentAcc);
			Contact con = TestI_TestUtil.createContact(true, acc.Id, 'testCon');
			us = TestI_TestUtil.createAgentUser(true, 'testUser', E_Const.PROFILE_E_PARTNERCOMMUNITY, con.Id);
			TestI_TestUtil.createAccountShare(acc.Id, us.Id);
			//代理店権限を付与
			TestI_TestUtil.createBasePermissions(us.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, us.Id, 'AY');
		}
		return us;
	}
}