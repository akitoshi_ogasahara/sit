global with sharing class E_GroupAgencyBizLogicBatch extends E_AbstractCommonBatch {
	
	//BaseSOQL
	public static String BASE_SOQL = 
		'Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c From Account Where E_IsAgency__c = true';

	/**
	 * BatchApexの開始処理です
	 * @param ctx
	 * @return Database.QueryLocator 
	 */
	global Database.QueryLocator start(Database.BatchableContext ctx){
		return Database.getQueryLocator(BASE_SOQL);
	}
}