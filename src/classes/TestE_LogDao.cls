@isTest
private class TestE_LogDao {
	
	@isTest static void getSearchLogTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		//TestI_TestUtil.createIDCPF(true, actUser.Id, null);
		List<E_Log__c> testLog = new List<E_Log__c>();
		for (Integer i = 0, j = 30; i < j; i++) {
			testLog.add(new E_Log__c(Name = 'testLog' + i, AccessUser__c = actUser.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage',isDisplay__c = true,ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_SEARCH));
		}
		insert testLog;
		List<E_Log__c> logs;

		Test.startTest();
		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);
			logs = E_LogDao.getSearchLog('testLog');
		}
		Test.stopTest();
		System.assertEquals(logs.size(),30);
	}
	
	@isTest static void getRecAccessLogTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		List<E_Log__c> testLog = new List<E_Log__c>();
		for (Integer i = 0, j = 30; i < j; i++) {
			testLog.add(new E_Log__c(Name = 'testLog' + i, AccessUser__c = actUser.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage',isDisplay__c = true,Detail__c = 'testLog' + i));
		}
		insert testLog;
		List<E_Log__c> logs;
		Test.startTest();
		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);
			logs = E_LogDao.getRecAccessLog(actUser.id);
		}
		Test.stopTest();
		System.assertEquals(logs[0].Detail__c,'testLog0');

	
	}
	
	@isTest static void getRecAccessLogListTest(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		List<E_Log__c> testLog = new List<E_Log__c>();
		for (Integer i = 0, j = 30; i < j; i++) {
			testLog.add(new E_Log__c(Name = 'testLog' + i, AccessUser__c = actUser.Id, AccessDatetime__c = datetime.now(), AccessPage__c = 'testPage',isDisplay__c = true));
		}
		insert testLog;

		Test.startTest();
		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);
			Set<Id> ids = new Set<Id>();
			ids.add(actUser.id);
			List<AggregateResult> result = E_LogDao.getRecAccessLogList(ids);
			System.assertEquals(result.size(),1);
		}
		Test.stopTest();

	}
}