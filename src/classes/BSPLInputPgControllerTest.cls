/**
* クラス名	:  BSPLInputPgControllerTest
* クラス概要	:  ＢＳ・ＰＬ入力画面(テスト)
* @created	:  2
* @modified	:   
*/
@isTest
private class BSPLInputPgControllerTest {

	/**
	* testMethodBSPLInputPgController()
	* ページ初期化処理（テスト）
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodBSPLInputPgController() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);
		// 業界平均マスタを作成
		//createIndustryAverage();
		// 法人税定義を作成
		//createTaxDefinition();
		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_FIID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		
		// 前々期ファイナンス情報を作成
		Finance__c fi2 = createFinance(989, customs.Id, 1);

		// テスト実行
		Test.startTest();
		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();
		Test.stopTest();

		System.assertEquals(990, ct.finance.Term__c);

		System.assertEquals(989, ct.finance02.Term__c);
	}
	/**
	* testMethodBSPLInputPgController2()
	* ページ初期化処理（テスト）
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodBSPLInputPgController2() {
		
		Test.startTest();
		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_CustomerCheck));
	}
	/**
	* testMethodBSPLinputStart()
	* ファイナンス情報読込処理（テスト）
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodBSPLinputStart() {
		
		/* カレントページにパラメータをセットする*/
		System.currentPageReference().getParameters().put('BS_flg', '1');
		System.currentPageReference().getParameters().put('PL_flg', '1');

		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();
		
		// 入力期をセット
		ct.inputTerm ='990';
		ct.dispSwitch = 'InputTerm';
				
		// テスト実行
		Test.startTest();
		ct.inputStart();
		Test.stopTest();

		System.assertEquals(0, ct.finance.BS_Cash__c);

	}

	/**
	* testMethodCheckInput1
	* 入力チェック(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodCheckInput1() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);
		// 法人税定義を作成
		createTaxDefinition();
		// 業界平均マスタを作成
		createIndustryAverage();
		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_FIID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		
		// 前々期ファイナンス情報を作成
		Finance__c fi2 = createFinance(989, customs.Id, 1);

		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();

		// ファイナンス情報をセット
		ct.finance = fi;
		ct.finance02 = fi2;

		ct.outputBS_ASTT = '100000';
		ct.outputBS_LNAT = '100000';

		ct.outputBS_ASTT02 = '100000';
		ct.outputBS_LNAT02 = '100000';

		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();

		System.assertEquals(1000, ct.finance.BS_Cash__c);
	}

	/**
	* testMethodCheckInput3
	* 入力チェック(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodCheckInput3() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// 業界平均マスタを作成
		//createIndustryAverage();
		// 法人税定義を作成
		//createTaxDefinition();
		/* カレントページにパラメータをセットする*/
		//ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		//ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_FIID, fi.Id);
		//ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		
		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();

		// ファイナンス情報をセット
		ct.finance = new Finance__c();
		ct.finance02 = new Finance__c();

		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();

		//String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		//System.assertEquals(true, strMsgError.contains(Label.Msg_Error_H_NotInput));

		System.assertEquals(null, ct.finance.BS_Cash__c);
	}

	/**
	* testMethodCheckInput2
	* 入力チェック(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodCheckInput2() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, -1);

		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_FIID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		
		// 前々期ファイナンス情報を作成
		Finance__c fi2 = createFinance(989, customs.Id, -1);

		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();

		// ファイナンス情報をセット
		ct.finance = fi;
		ct.finance02 = fi2;

		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_H_Minus));
	}

	/**
	* testMethodCancel
	* キャンセルボタン押下時の処理(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodCancel() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, -1);

		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_FIID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		
		// 前々期ファイナンス情報を作成
		Finance__c fi2 = createFinance(989, customs.Id, -1);
		
		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();
		
		// テスト実行
		Test.startTest();
		// コントローラを作成
		Pagereference pageHome = ct.cancel();
		Test.stopTest();
		// URLパラメータマップを取得
		String urlHome = URL.getSalesforceBaseUrl().toExternalForm();
		System.assert(true, pageHome.getUrl().contains(urlHome));
	}




	/**
	* testMethodClearData
	* 「BS情報をクリア」リンク処理(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodClearBSData(){
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);
		
		// 前々期ファイナンス情報を作成
		Finance__c fi2 = createFinance(989, customs.Id, 1);

		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();

		// テスト実行
		Test.startTest();
		ct.ClearBSData();
		Test.stopTest();

		System.assertEquals(0, ct.finance.BS_Cash__c);
		System.assertEquals(0, ct.finance.BS_NP__c);
}
	/**
	* testMethodClearData
	* 「PL情報をクリア」リンク処理(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodClearPLData(){
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);
		
		// 前々期ファイナンス情報を作成
		Finance__c fi2 = createFinance(989, customs.Id, 1);

		// コントローラを作成
		BSPLInputPgController ct = new BSPLInputPgController();

		// テスト実行
		Test.startTest();
		ct.ClearPLData();
		Test.stopTest();

		System.assertEquals(0, ct.finance.PL_SV__c);
}

	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 
	*/
	static Customer__c createCustomer(String name){
		// 顧客
		Customer__c customs = new Customer__c();
		// 顧客名
		customs.Name = name;
		// 代理店
		//  代理店【INGAgency】を作成
		AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
		agcINGA.Name = 'BSPLInput-test-test';
		insert agcINGA;
		customs.AgencyForINGAgency__c = agcINGA.Id;
		
		// 業界平均マスタ
		customs.Industry__c = 'BSPLInput-test';
		return customs;
	}
	/**
	* createFinance()
	* ファイナンス情報を作成
	* @return: なし
	* @created: 
	*/
	static Finance__c createFinance(integer opTerm, string cuid, Integer intOption){
		// 顧客を作成
		//Customer__c customs = createCustomer('BSPLInput-test');
		//insert customs;
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		
		finance.Term__c = opTerm;
		finance.BS_Cash__c = 1000 * intOption;
		finance.BS_NR__c = 2000 * intOption;
		finance.BS_AR__c = 3000 * intOption;
		finance.BS_PROD__c = 4000 * intOption;
		finance.BS_WIPRM__c = 5000 * intOption;
		finance.BS_SE__c = 6000 * intOption;
		finance.BS_STLR__c = 7000 * intOption;
		finance.BS_PRP__c = 8000 * intOption;
		finance.BS_PRC__c = 9000 * intOption;
		finance.BS_TA__c = 10000 * intOption;
		finance.BS_OCA__c = 11000 * intOption;
		finance.BS_LAC__c = 12000 * intOption;
		//outputBS_CATT = '0' * intOption;
		finance.BS_DP__c = 13000 * intOption;
		finance.BS_Land__c = 14000 * intOption;
		finance.BS_IFA__c = 15000 * intOption;
		finance.BS_IS__c = 16000 * intOption;
		finance.BS_LTLR__c = 17000 * intOption;
		finance.BS_DE__c = 18000 * intOption;
		finance.BS_PR__c = 19000 * intOption;
		finance.BS_OFA__c = 20000 * intOption;
		finance.BS_LAF__c = 21000 * intOption;
		//outputBS_FATT = '0' * intOption;
		finance.BS_DA__c = 22000 * intOption;
		//outputBS_ASTT = '0' * intOption;
										
		finance.BS_F_Cash__c = 1000 * intOption;
		finance.BS_F_NR__c = 2000 * intOption;
		finance.BS_F_AR__c = 3000 * intOption;
		finance.BS_F_PROD__c = 4000 * intOption;
		finance.BS_F_WIPRM__c = 5000 * intOption;
		finance.BS_F_SE__c = 6000 * intOption;
		finance.BS_F_STLR__c = 7000 * intOption;
		finance.BS_F_PRP__c = 8000 * intOption;
		finance.BS_F_PRC__c = 9000 * intOption;
		finance.BS_F_TA__c = 10000 * intOption;
		finance.BS_F_OCA__c = 11000 * intOption;
		finance.BS_F_LAC__c = 12000 * intOption;
		finance.BS_F_CATT__c = 13000 * intOption;
		finance.BS_F_DP__c = 14000 * intOption;
		finance.BS_F_Land__c = 15000 * intOption;
		finance.BS_F_IFA__c = 16000 * intOption;
		finance.BS_F_IS__c = 17000 * intOption;
		finance.BS_F_LTLR__c = 18000 * intOption;
		finance.BS_F_DE__c = 19000 * intOption;
		finance.BS_F_PR__c = 20000 * intOption;
		finance.BS_F_OFA__c = 21000 * intOption;
		finance.BS_F_LAF__c = 22000 * intOption;
		finance.BS_F_FATT__c = 23000 * intOption;
		finance.BS_F_DA__c = 24000 * intOption;
		finance.BS_F_ASTT__c = 25000 * intOption;
		
		finance.BS_NP__c = 1000 * intOption;
		finance.BS_AP__c = 2000 * intOption;
		finance.BS_STD__c = 3000 * intOption;
		finance.BS_OAP__c = 4000 * intOption;
		finance.BS_AE__c = 5000 * intOption;
		finance.BS_ADR__c = 6000 * intOption;
		finance.BS_DR__c = 7000 * intOption;
		finance.BS_ITP__c = 8000 * intOption;
		finance.BS_CTP__c = 9000 * intOption;
		finance.BS_AC__c = 10000 * intOption;
		finance.BS_OCL__c = 11000 * intOption;
		//outputBS_CLTT = '0' * intOption;
		finance.BS_LTD__c = 12000 * intOption;
		finance.BS_Bond__c = 13000 * intOption;
		finance.BS_AF__c = 14000 * intOption;
		finance.BS_OLTL__c = 15000 * intOption;
		//outputBS_LLTT = '0' * intOption;
		//outputBS_LBTT = '0' * intOption;
		finance.BS_CAP__c = 16000 * intOption;
		finance.BS_CS__c = 17000 * intOption;
		finance.BS_RE__c = 18000 * intOption;
		finance.BS_TS__c = 19000 * intOption;
		finance.BS_Other__c = 66000 * intOption;
		//outputBS_NATT = '0' * intOption;
		//outputBS_LNAT = '0' * intOption;
		
		finance.BS_F_NP__c = 3100 * intOption;
		finance.BS_F_AP__c = 3200 * intOption;
		finance.BS_F_STD__c = 3300 * intOption;
		finance.BS_F_OAP__c = 3400 * intOption;
		finance.BS_F_AE__c = 3500 * intOption;
		finance.BS_F_ADR__c = 3600 * intOption;
		finance.BS_F_DR__c = 3700 * intOption;
		finance.BS_F_ITP__c = 3800 * intOption;
		finance.BS_F_CTP__c = 3900 * intOption;
		finance.BS_F_AC__c = 4000 * intOption;
		finance.BS_F_OCL__c = 4100 * intOption;
		finance.BS_F_CLTT__c = 4200 * intOption;
		finance.BS_F_LTD__c = 4300 * intOption;
		finance.BS_F_Bond__c = 4400 * intOption;
		finance.BS_F_AF__c = 4500 * intOption;
		finance.BS_F_OLTL__c = 4600 * intOption;
		finance.BS_F_LLTT__c = 4700 * intOption;
		finance.BS_F_LBTT__c = 4800 * intOption;
		finance.BS_F_CAP__c = 4900 * intOption;
		finance.BS_F_CS__c = 5000 * intOption;
		finance.BS_F_RE__c = 5100 * intOption;
		finance.BS_F_TS__c = 5200 * intOption;
		finance.BS_F_Other__c = 5300 * intOption;
		finance.BS_F_NATT__c = 5400 * intOption;
		finance.BS_F_LNAT__c = 5500 * intOption;

					
		finance.BS_C_Cash_P__c  = '0％';
		finance.BS_C_NR_P__c    = '0％';
		finance.BS_C_AR_P__c    = '0％';
		finance.BS_C_PROD_P__c  = '0％';
		finance.BS_C_WIPRM_P__c = '0％';
		finance.BS_C_SE_P__c    = '0％';
		finance.BS_C_STLR_P__c  = '0％';
		finance.BS_C_PRP_P__c   = '0％';
		finance.BS_C_PRC_P__c   = '0％';
		finance.BS_C_TA_P__c    = '0％';
		finance.BS_C_OCA_P__c   = '0％';
		finance.BS_C_DP_P__c    = '0％';
		finance.BS_C_Land_P__c  = '0％';
		finance.BS_C_IFA_P__c   = '0％';
		finance.BS_C_IS_P__c    = '0％';
		finance.BS_C_LTLR_P__c  = '0％';
		finance.BS_C_DE_P__c    = '0％';
		finance.BS_C_PR_P__c    = '0％';
		finance.BS_C_OFA_P__c   = '0％';
		finance.BS_C_DA_P__c    = '0％';

		finance.PL_SV__c = 10 * intOption;
		finance.PL_COS__c = 20 * intOption;
		//outputPL_GP = '0';
		finance.PL_SGAAE__c = 30 * intOption;
		//outputPL_SI = '0';
		finance.PL_NSI__c = 40 * intOption;
		finance.PL_NSE__c = 50 * intOption;
		//outputPL_OI = '0';
		finance.PL_EI__c = 60 * intOption;
		finance.PL_EL__c = 70 * intOption;
		//outputPL_IBIT = '0';
		finance.PL_Tax__c = 80 * intOption;
		//outputPL_IBIT = '0';
		//outputPL_CI = '0';
		finance.PL_DE__c = 500 * intOption;
		finance.PL_IED__c = 300 * intOption;
		finance.PL_DIS__c = 100 * intOption;
					

		finance.Customer__c = cuid; // 顧客名
		insert finance;
	
		return finance;

	}

	/**
	* createIndustryAverage()
	* 業界平均マスタを作成
	* @return: 業界平均マスタ
	* @created: 
	*/
	static IndustryAverage__c createIndustryAverage(){
		IndustryAverage__c industry = new IndustryAverage__c();
		industry.Name__c = 'BSPLInput-test';
		industry.ROA__c = 10;	// 業界平均マスタ.ROA（総資産経常利益率）
		industry.ROE__c = 10;	// 業界平均マスタ.ROE(自己資本当期純利益率）
		industry.ROS__c = 10;	// 業界平均マスタ.ROS（売上高当期純利益率）
		industry.ICR__c = 10;	// 業界平均マスタ.インタレスト・カバレッジ・レシオ
		industry.DR__c = 10;	// 業界平均マスタ.債務償還年数
		industry.CAR__c = 10;	// 業界平均マスタ.自己資本比率
		industry.LR__c = 10;	// 業界平均マスタ.手元流動性比率
		industry.ITO__c = 10;	// 業界平均マスタ.棚卸資産回転率
		industry.QD__c = 10;	// 業界平均マスタ.当座比率
		industry.APTO__c = 10;	// 業界平均マスタ.買入債務回転率
		industry.RTO__c = 10;	// 業界平均マスタ.売上債権回転率
		insert industry;
		return industry;
	}
	/**
	* createCustomSetting()
	* カスタム設定。法人税定義を作成
	* @return: カスタム設定。法人税定義
	* @created: 
	*/
	static TaxDefinition__c createTaxDefinition(){
		TaxDefinition__c taxDf = new TaxDefinition__c();
		taxDf.From__c = Date.valueOf('2014-01-20');	// 法人税定義.施行日
		taxDf.NRate__c = 10;	// 法人税定義.必要倍率	
		taxDf.TaxRate__c = 10;	// 法人税定義.法人税率
		insert taxDf;
		return taxDf;
	}

}