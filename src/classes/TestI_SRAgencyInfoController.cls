@isTest
private class TestI_SRAgencyInfoController {

	/**
	 * AHのテスト
	 */
	@isTest static void viewTypeAHTest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 法人代表者/責任者の作成
		List<E_Representative__c> representative = TestE_RepresentativeDao.createRepresentative(agencySalesResults.Id);
		insert representative;

		// 乗合保険会社の作成
		List<E_VansInsurance__c> vansInsurances = TestE_VansInsuranceDao.createVansInsurance(agencySalesResults.Id);
		insert vansInsurances;

		Test.startTest();

		I_SRAgencyInfoController srInfoController = new I_SRAgencyInfoController();
		srInfoController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID
		srInfoController.paramViewType			= 'AH';						// 表示タイプ AH（代理店を情報を表示）AY（事務所情報を表示）AT（募集人情報を表示）

		// JS用値設定の取得
		srInfoController.getParameter();
		System.assertEquals(agencySalesResults.Id, srInfoController.agencySalesResults.id);

	}

	/**
	 * AYのテスト
	 */
	@isTest static void viewTypeAYTest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		Test.startTest();

		I_SRAgencyInfoController srInfoController = new I_SRAgencyInfoController();
		srInfoController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID
		srInfoController.paramViewType			= 'AY';						// 表示タイプ AH（代理店を情報を表示）AY（事務所情報を表示）AT（募集人情報を表示）

		// JS用値設定の取得
		srInfoController.getParameter();
		System.assertEquals(agencySalesResults.Id, srInfoController.agencySalesResults.id);

	}

	/**
	 * ATのテスト
	 */
	@isTest static void viewTypeATTest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 法人代表者/責任者の作成
		List<E_Representative__c> representative = TestE_RepresentativeDao.createRepresentative(agencySalesResults.Id);
		insert representative;

		Test.startTest();

		I_SRAgencyInfoController srInfoController = new I_SRAgencyInfoController();
		srInfoController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID
		srInfoController.paramViewType			= 'AT';						// 表示タイプ AH（代理店を情報を表示）AY（事務所情報を表示）AT（募集人情報を表示）

		// JS用値設定の取得
		srInfoController.getParameter();
		System.assertEquals(agencySalesResults.Id, srInfoController.agencySalesResults.id);

	}

}