@isTest
private class TestI_FAQController
{

	//CMSコンテンツ名:お問い合せ
	static testMethod void getFAQContentsIndexesTest1(){

		//IRISメニュー
		I_MenuMaster__c iMenu = new I_MenuMaster__c();
		iMenu.Name = 'テスト用';
		iMenu.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_FAQ;
		insert iMenu;

		//IRISページ
		I_PageMaster__c iPage = new I_PageMaster__c();
		iPage.Name = 'テスト用';
		iPage.Menu__c = iMenu.id;
		insert iPage;

		//IRIS　CMSコンテンツ(親)
		I_ContentMaster__c iCms = new I_ContentMaster__c();
		iCms.Name = 'お問い合せ';
		iCms.Page__c = iPage.id;
		insert iCms;

		//IRIS　CMSコンテンツ(子)
		I_ContentMaster__c cCms = new I_ContentMaster__c();
		cCms.ParentContent__c = iCms.Id;
		insert cCms;

		// ページ表示
		PageReference pr = Page.IRIS_FAQ;
		Test.setCurrentPage(pr);
		Id menuId = [SELECT Id, MainCategory__c FROM I_MenuMaster__c WHERE MainCategory__c = :I_Const.MENU_MAIN_CATEGORY_FAQ].Id;
		ApexPages.currentPage().getParameters().put('irismn', menuId);

		Test.startTest();
			I_FAQController faqcon = new I_FAQController();
			I_ContentMaster__c icon = new I_ContentMaster__c();
			icon.Name = iCms.Name;
			faqcon.getFAQContentsIndexes();
			System.assertEquals(icon.Name,iCms.Name);
		Test.stopTest();

	}

	//CMSコンテンツ名:テスト用
	static testMethod void getFAQContentsIndexesTest2(){

		//IRISメニュー
		I_MenuMaster__c iMenu = new I_MenuMaster__c();
		iMenu.Name = 'テスト用';
		iMenu.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_FAQ;
		insert iMenu;

		//IRISページ
		I_PageMaster__c iPage = new I_PageMaster__c();
		iPage.Name = 'テスト用';
		iPage.Menu__c = iMenu.id;
		insert iPage;

		//IRIS　CMSコンテンツ
		I_ContentMaster__c iCms = new I_ContentMaster__c();
		iCms.Name = 'テスト用';
		iCms.Page__c = iPage.id;
		insert iCms;

		I_ContentMaster__c iCmsChild = new I_ContentMaster__c();
		iCmsChild.Name = 'テスト用子コンテンツ';
		iCmsChild.ParentContent__c = iCms.Id;
		insert iCmsChild;

		// ページ表示
		PageReference pr = Page.IRIS_FAQ;
		Test.setCurrentPage(pr);
		Id menuId = [SELECT Id, MainCategory__c FROM I_MenuMaster__c WHERE MainCategory__c = :I_Const.MENU_MAIN_CATEGORY_FAQ].Id;
		ApexPages.currentPage().getParameters().put('irismn', menuId);

		Test.startTest();
			I_FAQController faqcon = new I_FAQController();
			List<I_CMSSection> ret = new List<I_CMSSection>();
			ret = faqcon.getFAQContentsIndexes();
			System.assertEquals(ret[0].record.Name,iCms.Name);
			System.assertEquals(ret[0].record.Page__c,iCms.Page__c);
		Test.stopTest();

	}

	//表示件数
	static testMethod void getVisibleRowsTest(){

		// ページ表示
		PageReference pr = Page.IRIS_FAQ;
		Test.setCurrentPage(pr);

		Test.startTest();
			I_FAQController faqcon = new I_FAQController();
			faqcon.getVisibleRows();
			System.assertEquals(faqcon.getVisibleRows(), 3);
		Test.stopTest();

	}

	static testMethod void getSumiseiParamTest(){
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'システム管理者');
		I_FAQController ctrl =  new I_FAQController();
		ApexPages.currentPage().getParameters().put('isSMTM', '1');

		System.runAs(actUser){
			Test.startTest();
				System.assertEquals('&isSMTM=1', ctrl.getSumiseiParam());
			Test.stopTest();
		}
	}

}