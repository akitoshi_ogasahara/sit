public with sharing class I_UIFrameController {

	//住生判定フラグ
	public Boolean isSumiseiFlag {get;set;}
	//mainタグのスタイルクラス
	public String mainStyle {get;set;}
	//mainタグ内のDiv1スタイルクラス
	public String mainDivStyle {get;set;}

	public I_UIFrameController() {
		E_AccessController access = E_AccessController.getInstance();
		isSumiseiFlag = false;
		if(access.canViewSumiseiIRIS()){
			isSumiseiFlag = true;
		}

		styleConfiguration();
	}

	//利用ユーザーによってスタイルの出しわけを行う
	public void styleConfiguration(){
		if(isSumiseiFlag){
			mainStyle = 'sumisei-page__content sumisei-page__content--fixed clearfix';
			mainDivStyle = 'sumisei-page__content-wrapper';
		}else{
			mainStyle = 'iris-content iris-layout-panel';
			mainDivStyle = 'iris-content-wrapper';
		}
	}
}