public with sharing class ASS_UnitSummaryController {
	//管理者プロファイル
	public final String SYSTEM_ADMINISTRATOR 	= 'システム管理者';
	public final String AREAMANAGER 			= 'エリア部長';
	//営業日報エンハンス20180709 START
	public final String SALESPROMOTION 			= '営業推進部';
	//営業日報エンハンス20180709 END

	//コンストラクタ
	public ASS_UnitSummaryController() {
	}

	//リダイレクト処理
	public PageReference redirectToMRSummary(){
		//プロファイルがシステム管理者または、エリア部長以外の場合、MR一覧にリダイレクト
		if(checkUserProfile() == false){
			PageReference pr = Page.ASS_MRSummary;
			pr.setRedirect(true);
			return pr;
		}
		return null;
	}

	//ユーザ判定
	private boolean checkUserProfile(){
		Profile profile = ASS_ProfileDao.getProfileById(UserInfo.getProfileId());
		if(profile != null){
			//プロファイルがシステム管理者または、エリア部長または、営業推進部の場合
			if(profile.Name.equals(SYSTEM_ADMINISTRATOR) 
				|| profile.Name.equals(AREAMANAGER) 
				//営業日報エンハンス20180709 START
				|| profile.Name.equals(SALESPROMOTION)){
				//営業日報エンハンス20180709 END
				return true;
			}
		}
		return false;
	}

	//画面遷移
	public PageReference moveToMRSummary(){
		PageReference pr = Page.ASS_MRSummary;
		pr.getParameters().put('unitId',ApexPages.currentPage().getParameters().get('unitId'));
		pr.setRedirect(true);
		return pr;
	}

	//営業部一覧取得
	public List<E_Unit__c> getUnitSummary(){
		return ASS_E_UnitDaoWithout.getUnits();
	}
}