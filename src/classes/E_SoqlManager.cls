/**
 * SOQL自動生成クラス
 */
public with sharing class E_SoqlManager {
	//=
	public final String eq = '{0} = {1}';
	//<>
	public final String ne = '{0} <> {1}';
	//<
	public final String lt = '{0} < {1}';
	//<=
	public final String gt = '{0} > {1}';
	//<=
	public final String le = '{0} <= {1}';
	//>=
	public final String ge = '{0} >= {1}';
	//like
	public final String co = '{0} like {1}';
	//not like
	public final String nc = 'not {0} like {1}';
	//in
	public final String inn = '{0} in {1}';
	//not it
	public final String nnn = '{0} not in {1}';
	
	//(
	public final String pStart = '(';
	//)
	public final String pEnd = ')';
	
	private final String cAnd = ' and ';
	private final String cOr = ' or ';
	
	private List<String> whereList = null;
	private List<String> pWhereList = null;
	
	/**
	 * Constract
	 */
	public E_SoqlManager() {
		whereList = new List<String>();
	}
	
	public void addString(String str) {
		whereList.add(str);
	}
	
	/**
	 * And条件
	 */
	public void addAndWhere(String key, Object value, String whereOperator) {
		List<String> values = new List<String>{key, escapeSingleQuotesString(value)};
		String formatted = String.format(whereOperator, values);
		if (whereList.size() > 0) {
			if (whereList.size() == 1 && whereList.get(0).equals(pStart)) {
				formatted = formatted;
			} else {
				formatted = cAnd + formatted;
			}
		}
		whereList.add(formatted);
	}
	
	/**
	 * Or条件
	 */
	public void addOrWhere(String key, Object value, String whereOperator) {
		List<String> values = new List<String>{key, escapeSingleQuotesString(value)};
		String formatted = String.format(whereOperator, values);
		if (whereList.size() > 0) {
			if (whereList.size() == 1 && whereList.get(0).equals(pStart)) {
				formatted = formatted;
			} else {
				formatted = cOr + formatted;
			}
		}
		whereList.add(formatted);
	}
	
	/**
	 * And条件（key whereOperator value And key whereOperator value)
	 */
	public void addAndParenthesesAndWhere(String key, Object value, String whereOperator, String parenthesis) {
		List<String> values = new List<String>{key, escapeSingleQuotesString(value)};
		String formatted = String.format(whereOperator, values);
		if (parenthesis.equals(pStart)) {
			System.debug('@wherelist@= '+ whereList);
			if (whereList.size() > 0) {
				if (whereList.size() == 1 && whereList.get(0).equals(pStart)) {
					formatted = parenthesis + formatted + cAnd;
				} else {
					formatted = cAnd + parenthesis + formatted + cAnd;
				}
			} else {
				formatted = parenthesis + formatted + cAnd;
			}
		} else if (parenthesis.equals(pEnd)) {
			formatted = formatted + parenthesis;
		}
		whereList.add(formatted);
	}
	
	/**
	 * And条件（key whereOperator value Or key whereOperator value)
	 */
	public void addAndParenthesesOrWhere(String key, Object value, String whereOperator, String parenthesis) {
		List<String> values = new List<String>{key, escapeSingleQuotesString(value)};
		String formatted = String.format(whereOperator, values);
		if (parenthesis.equals(pStart)) {
			System.debug(whereList);
			if (whereList.size() > 0) {
				if (whereList.size() == 1 && whereList.get(0).equals(pStart)) {
					formatted = parenthesis + formatted + cOr;
				} else {
					formatted = cAnd + parenthesis + formatted + cOr;
				}
			} else {
				formatted = parenthesis + formatted + cOr;
			}
		} else if (parenthesis.equals(pEnd)) {
			formatted = formatted + parenthesis;
		}
		whereList.add(formatted);
	}
	
	/**
	 * Or条件（key whereOperator value And key whereOperator value)
	 */
	public void addOrParenthesesAndWhere(String key, Object value, String whereOperator, String parenthesis) {
		List<String> values = new List<String>{key, escapeSingleQuotesString(value)};
		String formatted = String.format(whereOperator, values);
		if (parenthesis.equals(pStart)) {
			System.debug(whereList);
			if (whereList.size() > 0) {
				if (whereList.size() == 1 && whereList.get(0).equals(pStart)) {
					System.debug(1);
					formatted = parenthesis + formatted + cAnd;
				} else {
					System.debug(2);
					formatted = cOr + parenthesis + formatted + cAnd;
				}
			} else {
				System.debug(3);
				formatted = parenthesis + formatted + cAnd;
			}
		} else if (parenthesis.equals(pEnd)) {
			System.debug(4);
			formatted = formatted + parenthesis;
		}
		whereList.add(formatted);
	}
	
	/**
	 * where句を取得
	 */
	public String getWhere() {
		String soql = '';
		if (whereList.size() > 0) {
			soql += 'Where ';
			soql += String.join(whereList, ' ');
		}
		return soql;
	}
	
	/**
	 * Where句の条件をエスケープ処理
	 * @param obj Object
	 */
	private String escapeSingleQuotesString(Object obj) {
		if (obj == null) {
			return '';
		}
		String str = '';
		if (obj instanceOf String) {
			str = String.valueOf(obj);
			str = '\'' + str + '\'';
		} else if (obj instanceOf Integer) {
			str = String.valueOf(obj);
		} else if (obj instanceOf List<String>) {
			str = listToString((List<String>)obj);
		}
		return str;
	}
	
	/**
	 * List型をStringに型に変換
	 * @param lists List<String>
	 */
	private String listToString(List<String> lists) {
		List<String> escapeList = new List<String>();
		for (String str : lists) {
			escapeList.add(escapeSingleQuotesString(str));
		}
		String startStr ='(';
		String joinStr = String.join(escapeList, ',');
		String endStr =')';
		return startStr + joinStr + endStr;
	}
}