@isTest
private class TestSC_WebAnswerTriggerHandler {

	public static String fiscalYear = '2018';

	//Web回答作成
	@isTest static void insertTest01() {
		List<SC_WebAnswerExhibit__c> webAnsExhibitList = [select id from SC_WebAnswerExhibit__c];
		System.assertEquals(0,webAnsExhibitList.size());
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = createContact(true, accAgency.Id, actUserLastName);
		SC_Office__c office = createOffice(accAgency);

		Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
		List<SC_WebAnswer__c> ansList = new List<SC_WebAnswer__c>();
		ansList.add(updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON)));
		ansList.add(updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN)));

		webAnsExhibitList = [select id from SC_WebAnswerExhibit__c];
		System.assertEquals(1,webAnsExhibitList.size());
	}

	//Web回答更新
	@isTest static void udpateTest01() {
		List<SC_WebAnswerExhibit__c> webAnsExhibitList = [select id from SC_WebAnswerExhibit__c];
		System.assertEquals(0,webAnsExhibitList.size());
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = createContact(true, accAgency.Id, actUserLastName);
		SC_Office__c office = createOffice(accAgency);

		Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
		List<SC_WebAnswer__c> ansList = new List<SC_WebAnswer__c>();
		ansList.add(updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON)));
		ansList.add(updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN)));

		webAnsExhibitList = [select id from SC_WebAnswerExhibit__c];
		System.assertEquals(1,webAnsExhibitList.size());

		updateWebAnswer(ansList[0]);

		webAnsExhibitList = [select id from SC_WebAnswerExhibit__c];
		System.assertEquals(2,webAnsExhibitList.size());
	}


	public static SC_Office__c createOffice(Account acc){
		SC_Office__c office = new SC_Office__c();
		office.LimeSurveyTokenCommon__c = '';
		office.LimeSurveyTokenAdd__c = '';
		office.FiscalYear__c = fiscalYear;
		office.CMDRemandDate__c = DateTime.now();
		Insert office;
		return office;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 */
	public static Contact createContact(Boolean isInsert, Id accId, String lastName) {
		Contact src = new Contact(
			  AccountId = accId
			, LastName = lastName
			, E_CL3PF_ZAGMANGR__c = 'Y'
		);
		if(isInsert){
			insert src;
			src = [Select Id, Account.Name, LastName, E_AccParentCord__c From Contact Where Id =: src.Id];
		}
		return src;
	}

	/**
	 * 自主点検レコード取得
	 *
	 */
	public static Map<String, SC_SelfCompliance__c> getSelfCompMap(ID officeid) {
		Map<String, SC_SelfCompliance__c> tmpMap = new Map<String, SC_SelfCompliance__c>();
		for (SC_SelfCompliance__c record : SC_SelfComplianceDao.getRecsByOfficeId(officeid)) {
			tmpMap.put(record.Kind__c, record);
		}
		return tmpMap;
	}

	/*
	 * 自主点検レコード更新
	 * 提出方法（PDF、Web）によって紐づきを分ける
	 */
	public static SC_WebAnswer__c updateSelfComp(SC_SelfCompliance__c sc){
		sc.TypeSubmit__c = 'WEB';
		SC_WebAnswer__c ans = createWebAnswer(sc);
		update sc;
		return ans;
	}

	//Web回答作成
	public static SC_WebAnswer__c createWebAnswer(SC_SelfCompliance__c sc){
		SC_WebAnswer__c ans = new SC_WebAnswer__c();
		ans.SC_SelfCompliance__c = sc.Id;
		ans.Q00202__c = 'Y';
		insert ans;
		return ans;
	}

	//Web回答更新
	public static void updateWebAnswer(SC_WebAnswer__c ans){
		ans.Q00302__c = 'Y';
		update ans;
	}
}