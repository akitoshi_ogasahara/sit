@isTest
private class TestE_AgencySalesResultsShare {
	private static final String SHARE_ROWCAUSE_NNLink = 'NNLinkSharingRule__c';
	
	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static E_AgencySalesResults__c asr;

	private static testMethod void executeAH_test001(){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';
		
		// 公開グループ
		createGroup('L1' + CODE_DISTRIBUTE, 'L2' + CODE_OFFICE, 'BR' + CODE_BRANCH);
		createGroup('L1' + CODE_DISTRIBUTE_SUB, 'L2' + CODE_OFFICE_SUB, 'BR' + CODE_BRANCH_SUB);

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// Account 事務所
		office = new Account(Name = 'テスト事務所01',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
		insert office;
		suboffice = new Account(Name = 'テスト事務所02',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
		insert suboffice;
		
		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH'
		);
		insert asr;
		//数式の評価のため再取得
		asr = [SELECT Id ,brCode__c ,parentAccountCode__c ,AccountCode__c ,AgentCode__c ,Hierarchy__c ,ParentAccount__c From E_AgencySalesResults__c Where id =: asr.id];
		
		// SObject Set
		Set<SObject> sobjSet = new Set<SObject>();
		sobjSet.add(asr);
		
		// Test
		Test.startTest();
		
		E_AgencySalesResultsShare base = new E_AgencySalesResultsShare(sobjSet);
		base.execute();
		
		Test.stopTest();
		
		Map<String, E_AgencySalesResults__Share> resultShareMap = new Map<String, E_AgencySalesResults__Share>();
		List<E_AgencySalesResults__Share> shares = [Select Id, ParentId, AccessLevel, RowCause, UserOrGroupId, UserOrGroup.Name From E_AgencySalesResults__Share Where ROWCause = :SHARE_ROWCAUSE_NNLink];

		for(E_AgencySalesResults__Share share : shares){
			resultShareMap.put(share.UserOrGroup.Name, share);
		}
		System.debug(resultShareMap);
		
		// assertion
		System.assertEquals(3, shares.size());
		System.assert(resultShareMap.get('L1' + CODE_DISTRIBUTE) != null);
//		System.assert(resultShareMap.get('L2' + CODE_OFFICE) != null);
		System.assert(resultShareMap.get('BR' + CODE_BRANCH) != null);
//		System.assert(resultShareMap.get('L1' + CODE_DISTRIBUTE_SUB) != null);
//		System.assert(resultShareMap.get('L2' + CODE_OFFICE_SUB) != null);
		System.assert(resultShareMap.get('BR' + CODE_BRANCH_SUB) != null);

	}
	
	/**
	 * 公開グループ準備
	 */
	private static void createGroup(String d, String o, String b){
		// 既存の公開グループ
		Map<String, Group> grpMap = new Map<String, Group>();
		for(Group grp : [Select Id, DeveloperName From Group Where DeveloperName In (:d, :o, :b)]){
			grpMap.put(grp.DeveloperName, grp);
		}
		
		// 既存に存在しない公開グループ作成
		List<Group> grps = new List<Group>();
		if(grpMap.get(d) == null){
			grps.add(new Group(Name=d, DeveloperName=d, Type='Regular'));
		}
		if(grpMap.get(o) == null){
			grps.add(new Group(Name=o, DeveloperName=o, Type='Regular'));
		}
		if(grpMap.get(b) == null){
			grps.add(new Group(Name=b, DeveloperName=b, Type='Regular'));
		}
		if(!grps.isEmpty()){
			insert grps;
		}
	}
}