public without sharing class E_SetPasswordWithout {
	
	/*public E_SetPasswordWithout() {
		
	}*/

	/**
	 * System.setPassword用メソッド
	 * @param userId:ユーザＩＤ
	 * @param enpass:新パスワード
	 */
	public static void setPassword(String userId,String enpass){

		System.setPassword(userId, enpass);

	}
}