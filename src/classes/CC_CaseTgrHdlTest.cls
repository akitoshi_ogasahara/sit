/*
 * CC_CaseTgrHdl
 * Test class of CC_CaseTgrHdl
 * created  : Accenture 2018/7/12
 * modified :
 */

@isTest
private class CC_CaseTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account newAccount = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert newAccount;

			//Create Contact
			RecordType recordTypeOfContact = [SELECT Id FROM RecordType where SobjectType = 'Contact' and Name = '顧客' limit 1];
			Contact newContact = CC_TestDataFactory.getContactSkel();
			newContact.RecordTypeId = recordTypeOfContact.Id;
			newContact.AccountId = newAccount.Id;
			insert newContact;

			//Create SRTypeMaster
			CC_SRTypeMaster__c newSRTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
			insert newSRTypeMaster;

		}
	}

	/**
	* Test onBeforeInsert()
	*/
	static testMethod void onBeforeInsert() {
		System.runAs ( testUser ) {
			try{
				//Create Case data
				CC_SRTypeMaster__c newSRtypeMaster = [Select Id from CC_SRTypeMaster__c where Name = '10001'];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				newCase.CMN_Source__c = null;
				newCase.CC_SRTypeId__c = null;
				newCase.Status = 'クローズ';
				datetime myDate = datetime.now();
				newCase.CMN_ReceptionDateTime__c = myDate;

				Test.startTest();
				insert newCase;
				Test.stopTest();
			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test onBeforeUpdate()
	*/
	static testMethod void onBeforeUpdate() {
		System.runAs ( testUser ) {
			try{
				//Create Case data
				CC_SRTypeMaster__c newSRtypeMaster = [Select Id from CC_SRTypeMaster__c where Name = '10001'];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				insert newCase;
				//Update Case data
				newCase.Status = 'クローズ';

				//Create SRActivity data
				CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
				insert newSRActivity;
				newSRActivity.CC_Status__c = '開始前';
				update newSRActivity;

				Test.startTest();
				update newCase;
				Test.stopTest();
			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

}