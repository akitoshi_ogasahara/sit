/*
 * CC_SRPolicyValid
 * Validation class for CC_CaseEdit OmniScript and CC_SRPolicy__c Trigger
 * created  : Accenture 2018/9/3
 * modified :
 */

public with sharing class CC_SRPolicyValid{
	/**
	 * set error and output message map
	 * SR screen used only
	 * newCase： new/updated SR data
	 * outputValidMap： SR Data Error Map
	 * innerJSONMap: 'SR作成:帳票出力:Inner' JSON整理後のMap
	*/
	public static Map<String, Object> errorCheckAndOutput(Case newCase, Map<String,Object> outputValidMap, Map<String,Object> innerJSONMap){
		String inputAlertMessage = '';
		Map<String, Object> errorMessageMap = new Map<String, Object>();
		Map<String, String> dataErrorMap = new Map<String, String>();
		Boolean showValidationAlert = (Boolean)outputValidMap.get('showValidationAlert');
		Boolean showValidationError = (Boolean)outputValidMap.get('showValidationError');
		if(showValidationAlert){
			inputAlertMessage = (String)outputValidMap.get('inputAlertMessage');
		}
		if(outputValidMap.get('inputErrorMessage') != null){
			errorMessageMap = (Map<String, Object>)outputValidMap.get('inputErrorMessage');
			if(errorMessageMap.get('dataErrorMap') != null){
				dataErrorMap = (Map<String, String>)errorMessageMap.get('dataErrorMap');
			}
		}
		String sysNotInputCommonMsg = System.Label.CC_SRPolicyValid_ErrMsg_NotInputCommonMsg;
		String sysInvalidCommonMsg = System.Label.CC_SRPolicyValid_ErrMsg_InvalidCommonMsg;
		Boolean policyHasError = false;

		//証券番号、請求書1、請求書2
		Map<Integer, List<Object>> allPolicyMap = (Map<Integer, List<Object>>)innerJSONMap.get('allPolicyMap');
		Set<String> allPolicySet = (Set<String>)innerJSONMap.get('allPolicySet');
		List<E_Policy__c> listEPolicy = (List<E_Policy__c>)innerJSONMap.get('listEPolicy');
		Set<String> allFormMasterSet = (Set<String>)innerJSONMap.get('allFormMasterSet');
		List<CC_FormMaster__c> formMasterList = (List<CC_FormMaster__c>)innerJSONMap.get('FORMMASTERList');

		//証券番号
		if(allPolicySet != null && allPolicySet.size() > 0 ){
			for(String strPolicy : allPolicySet){
				Boolean policyErr = true;
				if(listEPolicy != null && listEPolicy.size() > 0){
					for(E_Policy__c ePolicyObj : listEPolicy){
						if(ePolicyObj.COMM_CHDRNUM__c.equals(strPolicy)){
							policyErr = false;
							Break;
						}
					}
				}

				if(policyErr){
					policyHasError = true;
					showValidationError = true;
					dataErrorMap.put('PolicyNumber' + strPolicy, sysInvalidCommonMsg.replace('{0}', '証券番号'));
				}
			}
		}

		//請求書1、請求書2
		if(allFormMasterSet != null && allFormMasterSet.size() > 0 ){
			for(String strForm : allFormMasterSet){
				Boolean formMatersErr = true;
				if(formMasterList != null && formMasterList.size() > 0){
					for(CC_FormMaster__c formMasterObj : formMasterList){
						if(formMasterObj.CC_FormName__c.equals(strForm)){
							formMatersErr = false;
							Break;
						}
					}
				}

				if(formMatersErr){
					showValidationError = true;
					dataErrorMap.put('FormMaster' + strForm, sysInvalidCommonMsg.replace('{0}', '請求書'));
				}
			}

		}

		//帳票出力ボタン押下する場合
		if(newCase.CC_IsSRFormPrint__c){
			//請求書1
			//請求書出力がチェックされる場合
			if(newCase.CC_IsInvoiceOutput__c){
				Map<Integer, String> allFormMaster1Map = (Map<Integer, String>)innerJSONMap.get('allFormMaster1Map');
				//確認書出力がチェックされる場合
				if(newCase.CC_IsConfirmationFormOutput__c){
					//1件目のみnull check
					if(!allFormMaster1Map.isEmpty() && allFormMaster1Map.get(1).equals('blank')){
						showValidationError = true;
						dataErrorMap.put('FormMaster1FirstNull', sysNotInputCommonMsg.replace('{0}', '請求書'));
					}
				}else{
					//すべてもnull check
					if(!allFormMaster1Map.isEmpty()){
						for(Integer i : allFormMaster1Map.keySet()){
							if(allFormMaster1Map.get(i).equals('blank')){
								showValidationError = true;
								dataErrorMap.put('FormMaster1AllNull', sysNotInputCommonMsg.replace('{0}', '請求書'));
								break;
							}
						}
					}
				}
			}

			//証券番号
			Boolean isPolicyHasNullValue = (Boolean)innerJSONMap.get('isPolicyHasNullValue');
			//請求書出力や確認書出力がチェックされる場合
			if(newCase.CC_IsInvoiceOutput__c || newCase.CC_IsConfirmationFormOutput__c){
				//入力されていない証券番号がある場合
				if(innerJSONMap == null || isPolicyHasNullValue){
					policyHasError = true;
					showValidationError = true;
					dataErrorMap.put('PolicyNumberNull', sysNotInputCommonMsg.replace('{0}', '証券番号'));
				}

				//帳票出力対象外の証券が選択されている場合
				if(listEPolicy != null && listEPolicy.size() > 0){
					for(E_Policy__c ePolicyObj : listEPolicy){
						if(!ePolicyObj.RecordType.Name.equals('個人保険ヘッダ')){
							String strPolicy = ePolicyObj.COMM_CHDRNUM__c;
							//マスタに存在する値である場合、該当エラーを表示する
							if(dataErrorMap.get('PolicyNumber' + strPolicy) == null){
								policyHasError = true;
								showValidationError = true;
								dataErrorMap.put('PolicyNumber' + strPolicy, System.Label.CC_SRPolicyValid_ErrMsg_InvalidPolicyNumber);
							}
						}
					}
				}
			}
			else if(!newCase.CC_IsInvoiceOutput__c && !newCase.CC_IsConfirmationFormOutput__c
					 && newCase.CMN_ShippingChoices__c != null && !newCase.CMN_ShippingChoices__c.equals('')){
					String shipAddress = newCase.CMN_ShippingChoices__c;
					if(shipAddress.equals('代理店事務所（主）') || shipAddress.equals('代理店事務所（従）')){
						if(isPolicyHasNullValue){
							showValidationAlert = true;
							if(inputAlertMessage != ''){
								inputAlertMessage += '\n' + '・' + sysNotInputCommonMsg.replace('{0}', '証券番号');
							}else{
								inputAlertMessage = '・' + sysNotInputCommonMsg.replace('{0}', '証券番号');
							}
						}
				}
			}

			//確認書出力
			//証券番号にはエラーない場合のみ確認書出力エラーを設定
			if(dataErrorMap.get('ConfirmationForm') != null && policyHasError){
				dataErrorMap.remove('ConfirmationForm');
			}
		}

		//output error&alert
		//error
		if(showValidationError){
			outputValidMap.put('showValidationError', true);
			if(!dataErrorMap.isEmpty()){
				errorMessageMap.put('dataErrorMap', (Object)dataErrorMap);
			}
			outputValidMap.put('inputErrorMessage', (Object)errorMessageMap);
		}else{
			outputValidMap.put('showValidationError', false);
		}

		//alert
		if(!inputAlertMessage.equals('')){
			outputValidMap.put('showValidationAlert', true);
			outputValidMap.put('inputAlertMessage', (Object)inputAlertMessage);
		}else{
			outputValidMap.put('showValidationAlert', false);
		}

		System.debug('CC_SRPolicyValid->errorCheckAndOutput: '+outputValidMap);
		return outputValidMap;
	}

	/**
	 * set validation
	 * trigger used only
	 */
	public static void setValidation(List<CC_SRPolicy__c> newList, Map<Id, CC_SRPolicy__c> newMap, Map<Id, CC_SRPolicy__c> oldMap){
		String sysNotInputCommonMsg = System.Label.CC_SRPolicyValid_ErrMsg_NotInputCommonMsg;
		String sysInvalidCommonMsg = System.Label.CC_SRPolicyValid_ErrMsg_InvalidCommonMsg;

		//get master data
		Map<Id, Case> caseMap = new Map<Id, Case>();
		Map<Id, E_Policy__c> ePolicyMap = new Map<Id, E_Policy__c>();
		Map<Id, CC_FormMaster__c> formMasterMap = new Map<Id, CC_FormMaster__c>();
		Set<Id> caseIdList = new Set<Id>();
		Set<Id> ePolicyIdList = new Set<Id>();
		Set<Id> formMasterIdList = new Set<Id>();

		for(CC_SRPolicy__c srPolicyObj : newList){

			caseIdList.add(srPolicyObj.CC_CaseId__c);

			if(srPolicyObj.CC_PolicyID1__c != null){
				ePolicyIdList.add(srPolicyObj.CC_PolicyID1__c);
			}
			if(srPolicyObj.CC_PolicyID2__c != null){
				ePolicyIdList.add(srPolicyObj.CC_PolicyID2__c);
			}
			if(srPolicyObj.CC_PolicyID3__c != null){
				ePolicyIdList.add(srPolicyObj.CC_PolicyID3__c);
			}
			if(srPolicyObj.CC_PolicyID4__c != null){
				ePolicyIdList.add(srPolicyObj.CC_PolicyID4__c);
			}
			if(srPolicyObj.CC_FormMaster1__c != null){
				formMasterIdList.add(srPolicyObj.CC_FormMaster1__c);
			}
			if(srPolicyObj.CC_FormMaster2__c != null){
				formMasterIdList.add(srPolicyObj.CC_FormMaster2__c);
			}
		}

		if(caseIdList.size() > 0){
			caseMap = CC_SRPolicyValidDao.getCaseMapByIdList(caseIdList);
		}
		if(ePolicyIdList.size() > 0){
			ePolicyMap = CC_SRPolicyValidDao.getEPolicyMapByIdSet(ePolicyIdList);
		}
		if(formMasterIdList.size() > 0){
			formMasterMap = CC_SRPolicyValidDao.getFormMasterMapByIdSet(formMasterIdList);
		}

		for(CC_SRPolicy__c srPolicyObj : newList){
			String errorMessage = ' ';
			Case caseObj = caseMap.get(srPolicyObj.CC_CaseId__c);

			if(caseObj != null){
				if(caseObj.CMN_ContactName__c != null){
					if(srPolicyObj.CC_PolicyID1__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID1__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID1__c).Contractor__c != caseObj.CMN_ContactName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID1__c).AccessibleCustomer__c != caseObj.CMN_ContactName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID1__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_PolicyID2__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID2__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID2__c).Contractor__c != caseObj.CMN_ContactName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID2__c).AccessibleCustomer__c != caseObj.CMN_ContactName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID2__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_PolicyID3__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID3__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID3__c).Contractor__c != caseObj.CMN_ContactName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID3__c).AccessibleCustomer__c != caseObj.CMN_ContactName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID3__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_PolicyID4__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID4__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID4__c).Contractor__c != caseObj.CMN_ContactName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID4__c).AccessibleCustomer__c != caseObj.CMN_ContactName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID4__c.getDescribe().getLabel()) + '<br>';
					}
				}
				else if(caseObj.CMN_CorporateName__c != null){

					if(srPolicyObj.CC_PolicyID1__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID1__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID1__c).Contractor__r.AccountId != caseObj.CMN_CorporateName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID1__c).AccessibleCustomer__r.AccountId != caseObj.CMN_CorporateName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID1__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_PolicyID2__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID2__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID2__c).Contractor__r.AccountId != caseObj.CMN_CorporateName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID2__c).AccessibleCustomer__r.AccountId != caseObj.CMN_CorporateName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID2__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_PolicyID3__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID3__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID3__c).Contractor__r.AccountId != caseObj.CMN_CorporateName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID3__c).AccessibleCustomer__r.AccountId != caseObj.CMN_CorporateName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID3__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_PolicyID4__c != null && ePolicyMap.get(srPolicyObj.CC_PolicyID4__c) != null &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID4__c).Contractor__r.AccountId != caseObj.CMN_CorporateName__c &&
					ePolicyMap.get(srPolicyObj.CC_PolicyID4__c).AccessibleCustomer__r.AccountId != caseObj.CMN_CorporateName__c
					){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_PolicyID4__c.getDescribe().getLabel()) + '<br>';
					}

				}

				if(caseObj.CC_SRTypeId__c != null){
					if(srPolicyObj.CC_FormMaster1__c != null && formMasterMap.get(srPolicyObj.CC_FormMaster1__c) != null &&
					((formMasterMap.get(srPolicyObj.CC_FormMaster1__c).CC_SRTypeNo__c != null &&
					  !formMasterMap.get(srPolicyObj.CC_FormMaster1__c).CC_SRTypeNo__c.equals(caseObj.CC_SRTypeId__r.Name)) ||
					 (formMasterMap.get(srPolicyObj.CC_FormMaster1__c).CC_FormType__c != null &&
					  !formMasterMap.get(srPolicyObj.CC_FormMaster1__c).CC_FormType__c.equals('請求書')))){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_FormMaster1__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_FormMaster2__c != null && formMasterMap.get(srPolicyObj.CC_FormMaster2__c) != null &&
					((formMasterMap.get(srPolicyObj.CC_FormMaster2__c).CC_SRTypeNo__c != null &&
					  !formMasterMap.get(srPolicyObj.CC_FormMaster2__c).CC_SRTypeNo__c.equals(caseObj.CC_SRTypeId__r.Name)) ||
					 (formMasterMap.get(srPolicyObj.CC_FormMaster2__c).CC_FormType__c != null &&
					  !formMasterMap.get(srPolicyObj.CC_FormMaster2__c).CC_FormType__c.equals('請求書')))){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_FormMaster2__c.getDescribe().getLabel()) + '<br>';
					}
				}
				else{
					if(srPolicyObj.CC_FormMaster1__c != null && formMasterMap.get(srPolicyObj.CC_FormMaster1__c) != null &&
					formMasterMap.get(srPolicyObj.CC_FormMaster1__c).CC_FormType__c != null &&
					!formMasterMap.get(srPolicyObj.CC_FormMaster1__c).CC_FormType__c.equals('請求書')){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_FormMaster1__c.getDescribe().getLabel()) + '<br>';
					}
					if(srPolicyObj.CC_FormMaster2__c != null && formMasterMap.get(srPolicyObj.CC_FormMaster2__c) != null &&
					formMasterMap.get(srPolicyObj.CC_FormMaster2__c).CC_FormType__c != null &&
					!formMasterMap.get(srPolicyObj.CC_FormMaster2__c).CC_FormType__c.equals('請求書')){
						errorMessage += sysInvalidCommonMsg.replace('{0}', CC_SRPolicy__c.CC_FormMaster2__c.getDescribe().getLabel()) + '<br>';
					}

				}
			}

			System.debug('CC_SRPolicyValid->setValidation: '+errorMessage);
			if(!errorMessage.equals(' ')){
				errorMessage = errorMessage.removeEnd('<br>');
				srPolicyObj.addError(errorMessage, false);
			}
		}
	}

}