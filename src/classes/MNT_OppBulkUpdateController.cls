global with sharing class MNT_OppBulkUpdateController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public Opportunity record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component161_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component161_to{get;set;}	
			
		public Opportunity Component26_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component26_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component26_op{get;set;}	
		public List<SelectOption> valueOptions_Opportunity_StageName_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component165_val {get;set;}	
		public SkyEditor2.TextHolder Component165_op{get;set;}	
			
	public String recordTypeRecordsJSON_Opportunity {get; private set;}
	public String defaultRecordTypeId_Opportunity {get; private set;}
	public String metadataJSON_Opportunity {get; private set;}
	{
	setApiVersion(31.0);
	}
		public MNT_OppBulkUpdateController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = Opportunity.fields.StageName;
		f = Opportunity.fields.IsOwner__c;
		f = Opportunity.fields.OwnerId;
		f = Opportunity.fields.AccountId;
		f = Opportunity.fields.Channel__c;
		f = Opportunity.fields.AssistanceMR__c;
		f = Opportunity.fields.Name;
		f = Opportunity.fields.Field1__c;
		f = Opportunity.fields.Count__c;
		f = Opportunity.fields.InsuranceType__c;
		f = Opportunity.fields.WAPE__c;
		f = Opportunity.fields.GAPE__c;
		f = Opportunity.fields.CloseDate;
		f = Opportunity.fields.CloseFlg__c;
		f = Opportunity.fields.ClosingMounth__c;
		f = Opportunity.fields.Note__c;
 f = Opportunity.fields.CloseDate;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = Opportunity.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component161_from = new SkyEditor2__SkyEditorDummy__c();	
				Component161_to = new SkyEditor2__SkyEditorDummy__c();	
					
				Component26_val = new Opportunity();	
				Component26_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component26_op = new SkyEditor2.TextHolder('inx');	
				valueOptions_Opportunity_StageName_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : Opportunity.StageName.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_Opportunity_StageName_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component165_val = new SkyEditor2__SkyEditorDummy__c();	
				Component165_op = new SkyEditor2.TextHolder('eq');	
					

				addInheritParameter(Component26_val_dummy,'SkyEditor2__Text__c', 'StageName');
				addInheritParameterOP(Component26_op,'StageName_op');
				addInheritParameter(Component165_val,'SkyEditor2__Checkbox__c', 'IsOwner__c');
				addInheritParameterOP(Component165_op,'IsOwner__c_op');
				applyInheritParameters();

				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('Opportunity')
						.addFieldAsOutput('OwnerId')
						.addFieldAsOutput('AccountId')
						.addFieldAsOutput('Channel__c')
						.addFieldAsOutput('AssistanceMR__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('Field1__c')
						.addFieldAsOutput('Count__c')
						.addFieldAsOutput('InsuranceType__c')
						.addFieldAsOutput('WAPE__c')
						.addFieldAsOutput('GAPE__c')
						.addField('StageName')
						.addField('CloseDate')
						.addFieldAsOutput('CloseFlg__c')
						.addFieldAsOutput('ClosingMounth__c')
						.addFieldAsOutput('Note__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component161_from, 'SkyEditor2__Date__c', 'CloseDate', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component161_to, 'SkyEditor2__Date__c', 'CloseDate', new SkyEditor2.TextHolder('le'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component26_val_dummy, 'SkyEditor2__Text__c','StageName', Component26_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component165_val, 'SkyEditor2__Checkbox__c', 'IsOwner__c', Component165_op, true, 0, false ))
						.addWhere(' ( StageName != \'S:予算\')')
.addSort('CloseDate',False,False)
				);	
					
					Component3 = new Component3(new List<Opportunity>(), new List<Component3Item>(), new List<Opportunity>(), null);
				 Component3.setPageItems(new List<Component3Item>());
				 Component3.setPageSize(100);
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(Opportunity.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = true;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_Opportunity_StageName_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_Opportunity_IsOwner_c() { 
			return getOperatorOptions('Opportunity', 'IsOwner__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public Opportunity record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, Opportunity record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.PagingList {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (Opportunity)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<Component3Item> getViewItems() {            return (List<Component3Item>) getPageItems();        }
	}

	public Opportunity Component3_table_Conversion { get { return new Opportunity();}}
	
	public String Component3_table_selectval { get; set; }
	
	
			
	}