@isTest
private class TestE_AnnuitantExtender{

	private static testMethod void TestE_AnnuitantExtender1(){
		Contact cntNenkin = TestE_TestUtil.createContact(false, null, 'テスト年金', '12345', null);
		cntNenkin.FirstName = '受取人';
		cntNenkin.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
		cntNenkin.E_CLTPF_DOB__c = '20141111';
		cntNenkin.E_CLTPF_CLTPCODE__c = '1234567';
		cntNenkin.E_CLTPF_ZCLKADDR__c = 'テスト住所';
		cntNenkin.E_CLTPF_CLTPHONE01__c = '111-1111';
		cntNenkin.E_CLTPF_CLTPHONE02__c = '222-2222';
		cntNenkin.E_CLTPF_FAXNO__c = '333-3333';
		insert cntNenkin;

		Contact cntHiho = TestE_TestUtil.createContact(false, null, 'テスト被保険者', null, '67890');
		cntHiho.LastName = 'テスト苗字';
		cntHiho.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
		cntHiho.E_CLTPF_DOB__c = '20141111';
		insert cntHiho;

		// テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		//結果画面
		PageReference resultPage;
		//結果郵便番号
		String actualPostalCode;

		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();
			E_Policy__c epoli = new E_Policy__c(Annuitant__c = cntNenkin.id,
												Insured__c = cntHiho.Id);
			insert epoli;
			//必要な場合、画面情報を設定
			PageReference testpage = Page.E_Annuitant;
			testpage.getParameters().put('id', epoli.Id);
			Test.setCurrentPage(testpage);
			
			Apexpages.Standardcontroller sdcon = new Apexpages.Standardcontroller(epoli);
			E_AnnuitantController attcon = new E_AnnuitantController(sdcon);
			E_AnnuitantExtender attex = new E_AnnuitantExtender(attcon);
			
			attex.init();
			
			//ページアクションの戻り値確認
			resultPage = attex.pageAction();
			//郵便番号の値確認
			actualPostalCode = attcon.record.Annuitant__r.E_CLTPF_CLTPCODE__c;
			//テスト終了
			Test.stopTest();
		}
		//※正常処理ならこれを確認↓
		system.assertEquals(null, resultPage);
		// 郵便番号が7桁
		System.assertEquals('1234567', actualPostalCode);
	}

	private static testMethod void TestE_AnnuitantExtender2(){
		Contact cntNenkin = TestE_TestUtil.createContact(false, null, 'テスト年金', '12345', null);
		cntNenkin.FirstName = '受取人';
		cntNenkin.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
		cntNenkin.E_CLTPF_DOB__c = '20141111';
		cntNenkin.E_CLTPF_CLTPCODE__c = '123';
		cntNenkin.E_CLTPF_ZCLKADDR__c = 'テスト住所';
		cntNenkin.E_CLTPF_CLTPHONE01__c = '111-1111';
		cntNenkin.E_CLTPF_CLTPHONE02__c = '222-2222';
		cntNenkin.E_CLTPF_FAXNO__c = '333-3333';
		insert cntNenkin;
		
		Contact cntHiho = TestE_TestUtil.createContact(false, null, 'テスト被保険者', null, '67890');
		cntHiho.FirstName = 'テスト苗字';
		cntHiho.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
		cntHiho.E_CLTPF_DOB__c = '20141111';
		insert cntHiho;

		// テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		//結果画面
		PageReference resultPage;
		//結果郵便番号
		String actualPostalCode;

		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();
			E_Policy__c epoli = new E_Policy__c(Annuitant__c = cntNenkin.id,
												Insured__c = cntHiho.Id);
			insert epoli;
			//必要な場合、画面情報を設定
			PageReference testpage = Page.E_Annuitant;
			testpage.getParameters().put('id', epoli.Id);
			Test.setCurrentPage(testpage);
			
			Apexpages.Standardcontroller sdcon = new	Apexpages.Standardcontroller(epoli);
			E_AnnuitantController attcon = new E_AnnuitantController(sdcon);
			E_AnnuitantExtender attex = new E_AnnuitantExtender(attcon);
			
			attex.init();
			
			//ページアクションの戻り値確認
			resultPage = attex.pageAction();
			//郵便番号の値確認
			actualPostalCode = attcon.record.Annuitant__r.E_CLTPF_CLTPCODE__c;
			//テスト終了
			Test.stopTest();
		}
		//※正常処理ならこれを確認↓
		system.assertEquals(null, resultPage);
		// 郵便番号が3桁
		System.assertEquals('123', actualPostalCode);

	}

}