/*
 * CC_CaseEditVOIDao
 * Data access class for CC_CaseEditOmniScript OmniScript
 * created  : Accenture 2018/4/18
 * modified :
 */

public with sharing class CC_CaseEditVOIDao{

	/**
	 * Object: CC_SRTypeMaster__c
	 * Parameter: Name (SR作成:SRタイプ番号SR用件)
	 * return: CC_SRTypeMaster__c
	 */
	public static CC_SRTypeMaster__c getSRTypeMasterByName(String Name){
		List<CC_SRTypeMaster__c> srTypeMasterObjList = [
							SELECT Id, Name, CC_IsComplaintSR__c, CC_SRTypeNameandSubject__c
							  FROM CC_SRTypeMaster__c
							 WHERE Name =: Name
								OR CC_SRTypeNameandSubject__c =: Name
							 LIMIT 1];

		return srTypeMasterObjList.isEmpty() ? new CC_SRTypeMaster__c() : srTypeMasterObjList[0];
	}

	/**
	 * Object: Contact
	 * Parameter: E_CL3PF_AGNTNUM__c (SR作成:募集人コード)
	 * return: Contact (募集人)
	 */
	public static Contact getAgentByAgentCode(String agentCode){
		List<Contact> contactList = [
							SELECT Id, AccountId, Account.E_CL2PF_ZAGCYNUM__c, Account.E_CL1PF_ZHEADAY__c, Account.E_IsAgency__c,
								   Account.ZubsMain__c, Account.Z1OFFING__c, Account.Parent.Z1OFFING__c, Account.Parent.E_CL1PF_ZHEADAY__c,
								   Account.Parent.E_CL2PF_ZAGCYNUM__c, Account.Parent.E_IsAgency__c, Account.Parent.ZubsMain__c
							  FROM Contact
							 WHERE E_CL3PF_AGNTNUM__c =: agentCode
							   AND RecordType.Name = '募集人'
							 LIMIT 1];

		return contactList.isEmpty() ? new Contact() : contactList[0];
	}

	/**
	 * Object: CC_FormMaster__c
	 * Parameter: CC_FormName__c (SR作成:送付状)
	 * return: CC_FormMaster__c (送付状)
	 */
	public static CC_FormMaster__c getCoverLetterByFormName(String formName){
		List<CC_FormMaster__c> formMasterList = [
							SELECT Id
							  FROM CC_FormMaster__c
							 WHERE CC_FormName__c =: formName
							   AND CC_FormType__c = '送付状'
							 LIMIT 1];

		return formMasterList.isEmpty() ? new CC_FormMaster__c() : formMasterList[0];
	}

	/**
	 * Object: CC_FormMaster__c
	 * Parameter: Set of CC_FormName__c (SR作成:請求書1、請求書2)
	 * return: List<CC_FormMaster__c>
	 */
	public static List<CC_FormMaster__c> getFormMasterListByFormNameSet(Set<string>allFormMasterSet , String srTypeNo){
		List<CC_FormMaster__c> formMasterList = new List<CC_FormMaster__c>();
		if(srTypeNo == null || srTypeNo.equals('')){
			formMasterList = [
							SELECT Id, CC_FormName__c
							  FROM CC_FormMaster__c
							 WHERE CC_FormName__c IN: allFormMasterSet
							   AND CC_FormType__c = '請求書'];
		}else{
			formMasterList = [
							SELECT Id, CC_FormName__c
							  FROM CC_FormMaster__c
							 WHERE CC_FormName__c IN: allFormMasterSet
							   AND CC_FormType__c = '請求書'
							   AND CC_SRTypeNo__c =: srTypeNo];
		}

		return formMasterList;
	}

	/**
	 * Object: CC_SRPolicy__c
	 * Parameter: Case Id
	 * return: List<CC_SRPolicy__c>
	 */
	public static List<CC_SRPolicy__c> getSRPolicyListByCaseId(String caseId){
		List<CC_SRPolicy__c> srPolicyList = [
							SELECT Id,
								   CC_PolicyNumber1__c, CC_PolicyNumber2__c,
								   CC_PolicyNumber3__c, CC_PolicyNumber4__c,
								   CC_FormMaster1__r.CC_FormName__c, CC_FormMaster2__r.CC_FormName__c
							  FROM CC_SRPolicy__c
							 WHERE CC_CaseId__c =: caseId];

		return srPolicyList;
	}

	/**
	 * Object: E_Policy__c
	 * Parameter: Set of COMM_CHDRNUM__c (SR作成:証券番号)
	 * return: List<E_Policy__c>
	 */
	public static List<E_Policy__c> getEPolicyListByCHDRNUMSet(Set<string> allPolicySet, String currentContactId, String currentCorporateId, String currentTargetId){
		List<E_Policy__c> ePolicyList = new List<E_Policy__c>();
		Id targetId;
		if(currentTargetId != null && currentTargetId != ''){
			targetId = currentTargetId;
		}else if(currentCorporateId != null && currentCorporateId != ''){
			targetId = currentCorporateId;
		}else if(currentContactId != null && currentContactId != ''){
			targetId = currentContactId;
		}

		ePolicyList = [
				SELECT Id, COMM_CHDRNUM__c, SubAgentAccount__c, RecordType.Name, MainAgentAccount__c
					FROM E_Policy__c
					WHERE COMM_CHDRNUM__c IN: allPolicySet
					AND (
						Contractor__c =: targetId
						OR (Contractor__r.AccountId =: targetId
							AND Contractor__r.E_CLTPF_CLTTYPE__c = 'C' )
						OR AccessibleCustomer__c = :targetId
						OR (AccessibleCustomer__r.AccountId =  :targetId
							AND AccessibleCustomer__r.E_CLTPF_CLTTYPE__c = 'C')
						)
				ORDER BY COMM_CHDRNUM__c ];

		return ePolicyList;
	}

	/**
	 * Object: E_Policy__c
	 * Parameter: List of Id
	 * return: List<E_Policy__c>
	 */
	public static List<E_Policy__c> getEPolicyListByIdList(List<Id> listPolicyId){
		List<E_Policy__c> ePolicyList = [
							SELECT COMM_CHDRNUM__c
							  FROM E_Policy__c
							 WHERE Id IN: listPolicyId
							 ORDER BY COMM_CHDRNUM__c];

		return ePolicyList;
	}

	/**
	 * Object: Account
	 * Parameter: Id
	 * return: Account
	 */
	public static Account getAccountById(Id accountId){
		List<Account> accountList = [
							SELECT Id, Name, CMN_IsClaimed__c, CLTPCODE__c, E_COMM_ZCLADDR__c, E_CL2PF_ZAGCYNUM__c, CMN_SYSAddress__c
							  FROM Account
							 WHERE Id =: accountId];

		return accountList.isEmpty() ? new Account() : accountList[0];
	}

	/**
	 * Object: Account
	 * Parameter: E_CL2PF_ZAGCYNUM__c
	 * return: Account
	 */
	public static Account getAccountByAgencyCode(String agencyCode){
		List<Account> accountList = [
							SELECT Id, Name, IsMotherOffice__c, E_IsAgency__c, E_CL1PF_ZHEADAY__c
							  FROM Account
							 WHERE E_CL2PF_ZAGCYNUM__c =: agencyCode
							 LIMIT 1];

		return accountList.isEmpty() ? new Account() : accountList[0];
	}

	/**
	 * Object: Case
	 * Parameter: Id
	 * return: Case
	 */
	public static Case getCaseById(Id caseId){
		List<Case> caseList = [
						SELECT Id, CMN_ContactName__c, CMN_CorporateName__c, CMN_AgencyName__c, ContactId, CC_SRTypeId__c, CC_SRTypeName__c,
								CC_IsConfirmationFormOutput__c, AccountId, CaseNumber, CMN_ReceptionDateTime__c, ClosedDate, CMN_Comment1__c,
								CMN_Comment2__c, CC_SubStatus__c, Status, CMN_FreeSpace1__c, CMN_FreeSpace2__c, RecruiterCD__c, CMN_Source__c,
								CMN_ClientName__c, CMN_ClientRelation__c, CMN_ShippingMethod__c, CMN_ShippingTargetDate__c, CMN_IsMultipleAgency__c,
								CC_IsInvoiceOutput__c, CMN_ShippingChoices__c, CMN_SendingEmail__c, CC_AddressForPrint__c, CC_ContactNameForPrint__c,
								CMN_ShippingNameTitle__c, CC_ZipcodeForPrint__c, CC_IsCoverLetterOutput__c, CMN_AgentName__c, CC_NoteForCoverletter__c,
								Contact.AccountId, CC_SRTypeId__r.Name, CC_SRTypeId__r.CC_SRTypeName__c, Owner.Name, CC_CoverLetter__r.CC_FormName__c,
								CMN_ContactName__r.Name, CMN_ContactName__r.CMN_SYSAddress__c, CMN_ContactName__r.CMN_FormattedPostalcode__c,
								CMN_CorporateName__r.Name, CMN_CorporateName__r.E_COMM_ZCLADDR__c, CMN_CorporateName__r.CLTPCODE__c, CMN_CorporateName__r.CMN_SYSAddress__c,
								CMN_AgentName__r.Name, CMN_AgentName__r.CMN_SYSAgencyName__c, CMN_AgentName__r.CMN_SYSAgentName__c, CMN_AgencyName__r.E_COMM_ZCLADDR__c,
								CMN_AgentName__r.Account.Z1OFFING__c, CMN_AgentName__r.Account.Parent.Z1OFFING__c,
								CMN_AgencyName__r.Name, CMN_AgencyName__r.CLTPCODE__c, CMN_AgencyName__r.E_CL2PF_ZAGCYNUM__c, CMN_AgencyName__r.CMN_SYSAddress__c
						  FROM Case
						 WHERE Id =: caseId];

		return caseList.isEmpty() ? new Case() : caseList[0];
	}

	/**
	 * Object: Contact
	 * Parameter: Id
	 * return: Contact
	 */
	public static Contact getContactById(Id contactId){
		List<Contact> contactList = [
						SELECT Id, Name, RecordType.Name, E_CL3PF_AGNTNUM__c, CMN_FormattedPostalcode__c, CMN_SYSAddress__c,
							   CMN_SYSAgencyName__c, CMN_SYSAgentName__c, Account.Name, Account.E_COMM_ZCLADDR__c, Account.CLTPCODE__c,
							   Account.E_CL2PF_ZAGCYNUM__c, Account.CMN_SYSAddress__c
						  FROM Contact
						 WHERE Id =: contactId];

		return contactList.isEmpty() ? new Contact() : contactList[0];
	}

	/**
	 * Object: CC_SRActivity__c
	 * Parameter: Case Id
	 * return: List<CC_SRActivity__c>
	 */
	public static List<CC_SRActivity__c> getSRActivityList(String caseId){
		List<CC_SRActivity__c> srActivityList = [
						SELECT Id, CC_SRNo__c, CC_Status__c
						  FROM CC_SRActivity__c
						 WHERE CC_SRNo__c =: caseId];

		return srActivityList;
	}

	/**
	 * Object: Account
	 * Parameter: Parent.E_CL1PF_ZHEADAY__c, Id
	 * return: List<Account>
	 */
	public static List<Account> getAgencyNumList(String parentZHEADAY, String ZHEADAY, Id id){
		String query = 'SELECT Id, Name, E_CL2PF_ZAGCYNUM__c FROM Account WHERE E_CL2PF_ZAGCYNUM__c != \'\' AND ( ';
		query += ' ( Z1OFFING__c = \'Y\' AND Id =: id )';
		if(String.isNotEmpty(parentZHEADAY)) query += ' OR ( Parent.Z1OFFING__c = \'Y\' AND Parent.E_CL1PF_ZHEADAY__c =: parentZHEADAY )';
		if(String.isNotEmpty(ZHEADAY)) query += ' OR ( Parent.Z1OFFING__c = \'Y\' AND Parent.E_CL1PF_ZHEADAY__c =: ZHEADAY )';
		query += ' ) ORDER BY E_CL2PF_ZAGCYNUM__c';

		List<Account> accountList = Database.query(query);

		return accountList;
	}

}