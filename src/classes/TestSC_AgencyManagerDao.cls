@isTest
private class TestSC_AgencyManagerDao {

	private static List<SC_AgencyManager__c> createData(Integer recSize, Boolean isInsert){
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		List<Contact> conList = new List<Contact>();
		for(Integer i=1; i<=recSize; i++){
			conList.add(TestSC_TestUtil.createContact(false, accAgency.Id, 'TestUser'+i));
		}
		insert conList;
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(true, accAgency.Id, null);
		// 自主点検管理責任者を作成
		List<SC_AgencyManager__c> records = new List<SC_AgencyManager__c>();
		for(Contact con : conList){
			records.add(new SC_AgencyManager__c(
				 User__c = UserInfo.getUserId()
				,HasUserEmail__c = true
				,SC_Office__c = scOffice.Id
				,Contact__c = con.Id)
			);
		}
		if(isInsert) {
			insert records;
		}
		return records;
	}

	@isTest static void getRecsByContactIds_test001() {
		List<SC_AgencyManager__c> managers = createData(3, true);
		Set<Id> ids = new Set<Id>();
		ids.add(managers[0].Contact__c);
		ids.add(managers[1].Contact__c);

		Test.startTest();
			List<SC_AgencyManager__c> result = SC_AgencyManagerDao.getRecsByContactIds(ids);
		Test.stopTest();
		System.assertEquals(2, result.size());
	}

	@isTest static void getRecsByContactIds_test002() {
		List<SC_AgencyManager__c> managers = createData(1, true);

		Test.startTest();
			List<SC_AgencyManager__c> result = SC_AgencyManagerDao.getRecsByContactIds(new Set<Id>());
		Test.stopTest();
		System.assertEquals(0, result.size());
	}
}