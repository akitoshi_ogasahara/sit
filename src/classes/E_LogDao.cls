public with sharing class E_LogDao {

	/**
	 * IRIS検索ボックス用
	 * 最近検索されたキーワード表示用
	 * @param String:検索カテゴリ
	 * @return List<E_Log__c>
	 *
	 */
	public static List<E_Log__c> getSearchLog(String searchCategory){
		String forwardmatch = searchCategory + '%';
		return [SELECT Id
					 , Name
					 , Detail__c
				FROM E_Log__c
				WHERE AccessUser__c = :userInfo.getUserId()
				AND   ActionType__c = :E_LogUtil.NN_LOG_ACTIONTYPE_SEARCH
				AND   Name like :forwardmatch
				AND   AccessDateTime__c >= :DateTime.now().addMonths(-1)
				ORDER BY LastModifiedDate DESC
				LIMIT 100];
	}

	/**
	 * IRIS 利用履歴画面
	 * 利用履歴画面で使用するレコードを取得する
	 * @param String:User.Id
	 * 
	 */
	public static List<E_Log__c> getRecAccessLog(String agentId) {
		List<E_Log__c> result = new List<E_Log__c>();
		result = [SELECT Id
					   , ActionType__c
					   , AccessDatetime__c
					   , DisplayURL__c
					   , DisplayName__c
					   //, TubeDisplayURL__c
					   , Detail__c
				  FROM E_Log__c
				  WHERE AccessDateTime__c >= :DateTime.now().addMonths(-1)
				  AND   AccessUser__c = :agentId
				  AND   isDisplay__c = true
				  ORDER BY AccessDateTime__c DESC
				  LIMIT :I_Const.LIST_MAX_ROWS];

		return result;
	}

	/**
	 * 募集人一覧画面
	 * 過去1ヶ月のIRIS利用履歴の有無を判定するためのレコードを取得
	 * @param String:User.Id
	 * 
	 */
	public static List<AggregateResult> getRecAccessLogList(Set<Id> agentId) {
		List<AggregateResult> result = new List<AggregateResult>();
		result = [SELECT AccessUser__c
				  FROM E_Log__c
				  WHERE AccessDateTime__c >= :DateTime.now().addMonths(-1)
				  AND   AccessUser__c in :agentId
				  AND   isDisplay__c = true
				  GROUP BY AccessUser__c];
		return result;
	}
	/**
	 * NNLinkLogIDからレコードを取得
	 * @param String elogId
	 */
	public static E_Log__c getRecByLogId(String elogId) {
		List<E_Log__c> logs = new List<E_Log__c>();
		logs = [Select 
					Id
					, Detail__c 
				from 
					E_log__c 
				where 
					Id = :elogId
				];
		if(logs.size()>0){
			return logs[0];
		}
		return null;
	}

	// P16-0003 Atria対応開発
	public static E_Log__c getLatestRecByCreatedAndActType(Id uId, String aType) {
		E_Log__c result = new E_Log__c();
		
		List<E_Log__c> logs =
			[
				Select
					Id, ActionType__c, TargetUser__c
				From
					E_Log__c
				Where
					CreatedById =: uId
				And
					ActionType__c =: aType
				Order by CreatedDate Desc
				Limit 1
			];

		if(!logs.isEmpty()) {
			result = logs.get(0);
		}
		
		return result;
	}
}