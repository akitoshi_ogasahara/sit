/**
 * 顧客検索画面コントローラクラス
 * CreatedDate 2014/12/15
 * @Author Terrasky
 */
public with sharing class E_CustomerSearchController extends E_AbstractController{

    /**検索区分**/
    //契約者,被保険者
    public String selectKind {get; set;}
    //代理店,代理店事務所,募集人
    public String selectTerm {get; set;}

    /**検索条件**/
    //証券番号
    public String iPolicyNo {get; set;}
    //顧客番号
    public String iCustomerNo {get; set;}
    //カナ氏名
    public String iCustomerKana {get; set;}
    //漢字氏名
    public String iCustomerKanji {get; set;}
    //代理店ルックアップ
    public Contact iDistoributor {get; set;}
    //代理店事務所ルックアップ
    public Contact iOffice {get; set;}
    //募集人ルックアップ
    public E_Policy__c iAgent {get; set;}

    /**検索結果**/
    //取得結果一覧
    public transient List<SearchResult> searchResult {get;set;}
    //取得結果一覧（表示用）
    public transient List<SearchResult> searchResultDisply {get;set;}

    /**制御**/
    //検索前後フラグ
    public Boolean isSearched {get; set;}
    //検索区分設定エリア非表示フラグ(true=非表示)
    public Boolean isHideSelectArea {get; set;}

    /**エラーメッセージ**/
    //検索前後フラグ
    public String errMsg {get; set;}

    /**定数**/
    public String getCS_CONTRACTOR() {
        return E_Const.CS_CONTRACTOR;
    }

    public String getCS_INSURED() {
        return E_Const.CS_INSURED;
    }

    public String getCS_DISTORIBUTOR() {
        return E_Const.CS_DISTORIBUTOR;
    }

    public String getCS_OFFICE() {
        return E_Const.CS_OFFICE;
    }

    public String getCS_AGENT() {
        return E_Const.CS_AGENT;
    }

    public boolean getIsEmployee() {
        return access.isEmployee();
    }

    public Integer getMax() {
        return maxCount;
    }

    private final Integer maxCount = 100;

    /**
     * Constractor
     */
    public E_CustomerSearchController() {
        init();
        pgTitle = '顧客検索';
    }

    /**
     * init
     */
    public void init() {
        pageRef = doAuth(E_Const.ID_KIND.CUSTOMER_SEARCH, null);
        if (pageRef == null) {
            initialize();
        }
    }

    /**
     * pageAction
     */
    public Pagereference pageAction(){
        return E_Util.toErrorPage(pageRef, null);
    }

    /**
     * ラジオボタン押下処理
     */
    public Pagereference reDraw(){
        iDistoributor.accountid = null;
        iOffice.accountid = null;
        iAgent.MainAgent__c = null;
        return null;
    }

    /**
     * 検索処理
     */
    public Pagereference doSearch(){
        E_PageMessagesHolder pageMessages = getPageMessages();
        try{
            //入力チェック
            errMsg = isCheck();
            if (errMsg != null) {
                pageMessages.addErrorMessage(errMsg);
                return null;
            }
    
            //初期化
            isSearched = false;
            searchResult = null;
            searchResultDisply = null;
    
            //検索結果取得
            List<E_Policy__c> srdao = E_PolicyDao.getEPoliCustomerResearch(createSoql());
            if(srdao.size() > 0){
                searchResult = getResultByPolicys(srdao);
                searchResultDisply = new List<SearchResult>();
                //検索結果件数判定
                if(searchResult.size() > maxCount){
                    //100件までのリストとする
                    for(Integer i = 0; i<maxCount; i++ ){
                        searchResultDisply.Add(searchResult[i]);
                    }
                } else {
                    searchResultDisply.addAll(searchResult);
                }
                //ソート処理
                searchResultDisply.sort();
            } else {
                searchResult = new List<SearchResult>();
            }
            //検索後
            isSearched = true;
            return null;
        }catch(Exception e){
            //ApexPages.addMessages(e);
            pageMessages.addErrorMessage(e.getMessage());
            return null;
        }
    }

    /**
     * ラジオボタン押下処理
     */
    public void doCancel(){
        initialize();
    }

    /**
     * 変数初期化
     */
    private void initialize() {
        //初期化
        selectKind = E_Const.CS_CONTRACTOR;
        selectTerm = E_Const.CS_DISTORIBUTOR;
        isSearched = false;
        iDistoributor = new Contact();
        iOffice = new Contact();
        iAgent = new E_Policy__c();
        iPolicyNo = null;
        iCustomerNo = null;
        iCustomerKana = null;
        iCustomerKanji = null;
        searchResult = null;
        searchResultDisply = null;
        errMsg = null;
        isHideSelectArea = false;

        // IRIS 検索条件セット
        if(iris.getIsIRIS() && iris.isSetPolicyCondition()){
            // フィルタ項目と値を取得
            String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
            String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

            if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
                // 代理店フィルタ
                if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
                    selectTerm = E_Const.CS_DISTORIBUTOR;
                    iDistoributor.AccountId = conditionValue;
                // 事務所フィルタ
                } else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
                    selectTerm = E_Const.CS_OFFICE;
                    iOffice.AccountId = conditionValue;
                // 募集人フィルタ
                } else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
                    //修正　山田
                    String agentId = E_Util.getMainAccountByAgentId(conditionValue);
                    selectTerm = E_Const.CS_AGENT;
                      iAgent.MainAgent__c = agentId;
                }

                isHideSelectArea = true;
            }
        }
    }

    /**
     * 入力チェック
     */
    private String isCheck() {
        errMsg = null;
        //検索条件なし
        if (iDistoributor.AccountId == null &&
            iOffice.AccountId == null &&
            iAgent.MainAgent__c == null &&
            (iPolicyNo == null || String.isEmpty(iPolicyNo)) &&
            (iCustomerNo == null || String.isEmpty(iCustomerNo)) &&
            (iCustomerKana == null || String.isEmpty(iCustomerKana)) &&
            (iCustomerKanji == null || String.isEmpty(iCustomerKanji))) {
                return getMSG().get('CSH|001');
            }
        //拠点権限の社員は代理店件は必須（BR_C03-1_001）
        if (access.isEmployee()) {
            if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL)) {
                if (iDistoributor.AccountId == null &&
                    iOffice.AccountId == null &&
                    iAgent.MainAgent__c == null) {
                    return getMSG().get('CSH|005');
                }
            }
        }
        //証券番号
        if (iPolicyNo != null && !String.isEmpty(iPolicyNo)) {
            if (!E_Util.isEnNum(iPolicyNo)) {
                return getMSG().get('CSH|004');
            }
            if (iPolicyNo.length() != 8) {
                return getMSG().get('CSH|004');
            }
        }
        return null;
    }

    /**
     * 拡張クラスリストへつめなおし
     */
    private List<SearchResult> getResultByPolicys(List<E_Policy__c> policys){
        Set<Id> contactIds = new Set<Id>();
        //重複除去したContactIdを取得
        if(selectKind.equals(E_Const.CS_CONTRACTOR)){
            for(E_Policy__c policy : policys){
                //契約者検索
                //if(policy.Contractor__c != null){
                    contactIds.add(policy.Contractor__c);
                    //ret.put(policy.Contractor__c, new SearchResult(policy, selectKind));
                //}
            }
        } else {
            for(E_Policy__c policy : policys){
                //被保険者検索
                //if(policy.Insured__c != null){
                    contactIds.add(policy.Insured__c);
                //}
            }
        }
        
        policys.clear();
        contactIds.remove(null);
        
        //Contactレコードの取得と詰め直し
        List<SearchResult> ret = new List<SearchResult>();
        for(Contact con: E_ContactDaoWithout.getRecordsByIds(contactIds)){
            ret.add(new SearchResult(con, selectKind));
        }
        return ret;
    }

    /**
     * SOQL生成
     */
    private String createSoql(){
        
        Map<String, Id> recordTypePolicy = E_RecordTypeDao.getRecordTypeBySobjectType('E_Policy__c');
        
        //取得項目
        String soql = 'SELECT Id,';
        soql += ' Contractor__c, Insured__c ';
/*
        soql += ' Contractor__r.NAME, Contractor__r.E_COMM_ZCLADDR__c, ';
        soql += ' Contractor__r.E_CLTPF_DOB__c, Contractor__r.E_CLTPF_ZKNJSEX__c, ';
        soql += ' Contractor__r.E_CLTPF_ZCLKNAME__c, Contractor__r.E_CLTPF_CLNTNUM__c, ';
        soql += ' Insured__r.NAME, Insured__r.E_COMM_ZCLADDR__c, ';
        soql += ' Insured__r.E_CLTPF_DOB__c, Insured__r.E_CLTPF_ZKNJSEX__c, ';
        soql += ' Insured__r.E_CLTPF_ZCLKNAME__c, Insured__r.E_CLTPF_CLNTNUM__c
*/
        soql += ' FROM E_Policy__c ';

        //上限
        String limitSoql = ' limit 5000';

        E_SoqlManager manager = new E_SoqlManager();
        manager.addString(manager.pStart);
        //manager.addOrWhere('RecordTypeId', recordTypePolicy.get('ESPHPF'), manager.eq);
        
        manager.addOrParenthesesAndWhere('RecordTypeId', recordTypePolicy.get('ESPHPF'), manager.eq, manager.pStart);
        manager.addOrParenthesesAndWhere('SPVA_ZANNSTAT__c', new List<String>{E_Const.SPVA_ZANNSTAT_C}, manager.nnn, manager.pEnd);
        
        manager.addOrParenthesesAndWhere('RecordTypeId', recordTypePolicy.get('ECHDPF'), manager.eq, manager.pStart);
        manager.addOrParenthesesAndWhere('COMM_STATCODE__c', new List<String>{E_Const.COLI_STATCODE_A,E_Const.COLI_STATCODE_J,E_Const.COLI_STATCODE_K}, manager.nnn, manager.pEnd);
        manager.addString(manager.pEnd);
        //代理店(社員のみ利用 __rの使用可能)
        if(selectTerm.equals(E_Const.CS_DISTORIBUTOR) && iDistoributor.AccountId != null){
            String esIDistoributor = String.escapeSingleQuotes(iDistoributor.AccountId);
            manager.addAndParenthesesOrWhere('MainAgent__r.Account.parentId', esIDistoributor, manager.eq, manager.pStart);
            manager.addAndParenthesesOrWhere('SubAgent__r.Account.parentId', esIDistoributor, manager.eq, manager.pEnd);
        }
        //代理店事務所(社員のみ利用)
        if(selectTerm.equals(E_Const.CS_OFFICE) && iOffice.AccountId != null){
            String esIOffice = String.escapeSingleQuotes(iOffice.AccountId);
            manager.addAndParenthesesOrWhere('MainAgent__r.AccountId', esIOffice, manager.eq, manager.pStart);
            manager.addAndParenthesesOrWhere('SubAgent__r.AccountId', esIOffice, manager.eq, manager.pEnd);
        }

        //募集人(社員のみ利用)
        if(selectTerm.equals(E_Const.CS_AGENT) && iAgent.MainAgent__c != null){
            String esIAgent = String.escapeSingleQuotes(iAgent.MainAgent__c);
            manager.addAndParenthesesOrWhere('MainAgent__c', esIAgent, manager.eq, manager.pStart);
            manager.addAndParenthesesOrWhere('SubAgent__c', esIAgent, manager.eq, manager.pEnd);
        }

        //契約者証券番号
        if(E_Util.isNotNull(iPolicyNo)){
            manager.addAndWhere('COMM_CHDRNUM__c', String.escapeSingleQuotes(iPolicyNo), manager.eq);
        }
        
        //---
        //  検索条件の追加　Contactへのアクセス権がないユーザでも可能とするため、数式項目を利用する
        //---
        //契約者顧客番号
        if(E_Util.isNotNull(iCustomerNo)){
            if(selectKind.equals(E_Const.CS_CONTRACTOR)){
                //manager.addAndWhere('Contractor__r.E_CLTPF_CLNTNUM__c', String.escapeSingleQuotes(iCustomerNo), manager.eq);
                manager.addAndWhere('ContractorCLTPF_CLNTNUM__c', String.escapeSingleQuotes(iCustomerNo), manager.eq);
            } else if (selectKind.equals(E_Const.CS_INSURED)) {
                //manager.addAndWhere('Insured__r.E_CLTPF_CLNTNUM__c', String.escapeSingleQuotes(iCustomerNo), manager.eq);
                manager.addAndWhere('InsuredCLTPF_CLNTNUM__c', String.escapeSingleQuotes(iCustomerNo), manager.eq);
            }
        }
        //契約者カナ名
        if(E_Util.isNotNull(iCustomerKana)){
            if (!E_Util.isNotNull(iPolicyNo) || !E_Util.isNotNull(iCustomerKanji)) {
                iCustomerKana = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(iCustomerKana))));
                String esICustomerKana = String.escapeSingleQuotes(iCustomerKana);
                esICustomerKana = '%' + esICustomerKana + '%';
                //esICustomerKana = E_Util.enKatakanaToEm(esICustomerKana);
                if(selectKind.equals(E_Const.CS_CONTRACTOR)){
                    //manager.addAndWhere('Contractor__r.E_CLTPF_ZCLKNAME__c', esICustomerKana, manager.co);
                    manager.addAndWhere('ContractorE_CLTPF_ZCLKNAME__c', esICustomerKana, manager.co);
                } else if (selectKind.equals(E_Const.CS_INSURED)) {
                    //manager.addAndWhere('Insured__r.E_CLTPF_ZCLKNAME__c', esICustomerKana, manager.co);
                    manager.addAndWhere('InsuredE_CLTPF_ZCLKNAME__c', esICustomerKana, manager.co);
                }
            }
        }
        //契約者氏名漢字
        if(E_Util.isNotNull(iCustomerKanji)){
            if (!E_Util.isNotNull(iPolicyNo) || !E_Util.isNotNull(iCustomerKana)) {
                String esICustomerKanji = String.escapeSingleQuotes(iCustomerKanji);
                esICustomerKanji = '%' + esICustomerKanji + '%';
                if(selectKind.equals(E_Const.CS_CONTRACTOR)){
                    //manager.addAndWhere('Contractor__r.NAME', esICustomerKanji, manager.co);
                    manager.addAndWhere('ContractorName__c', esICustomerKanji, manager.co);
                } else if (selectKind.equals(E_Const.CS_INSURED)) {
                    //manager.addAndWhere('Insured__r.NAME', esICustomerKanji, manager.co);
                    manager.addAndWhere('InsuredName__c', esICustomerKanji, manager.co);
                }
            }
        }
System.debug('*************************E_CustomerSearchController sql=' + soql + manager.getWhere() + limitSoql);
        return soql + manager.getWhere() + limitSoql;
    }

    /** =====================================================
     *      検索結果一覧内部クラス
     *          2015.05.15 オブジェクトをE_Policy__cからContactに変更   
     */
    public class SearchResult implements Comparable {
        //保険契約オブジェクト
        //public E_Policy__c record{get;set;}
        
        //取引先責任者
        public Contact record{get;set;}
        //契約者,被保険者
        private String selectKind;

        /**
         * Constractor
         */
        public SearchResult(Contact rec, String selectKind){
            this.record = rec;
            this.selectKind = selectKind;
        }

        /**
         * ソート処理：カナ名が五十音昇順
         */
        public Integer compareTo(Object compareTo){
            SearchResult result = (SearchResult)compareTo;
            //カナ名同列時
            Integer returnValue = 0;
            if( this.record.E_CLTPF_ZCLKNAME__c > result.record.E_CLTPF_ZCLKNAME__c ){
                returnValue = 1;
            }
            if( this.record.E_CLTPF_ZCLKNAME__c < result.record.E_CLTPF_ZCLKNAME__c ){
                returnValue = -1;
            }
/*
            ///契約者検索結果の場合
            if (this.selectKind.equals(E_Const.CS_CONTRACTOR)){
                if( this.record.Contractor__r.E_CLTPF_ZCLKNAME__c > result.record.Contractor__r.E_CLTPF_ZCLKNAME__c ){
                    returnValue = 1;
                }
                if( this.record.Contractor__r.E_CLTPF_ZCLKNAME__c < result.record.Contractor__r.E_CLTPF_ZCLKNAME__c ){
                    returnValue = -1;
                }
            }
            //被保険者検索結果の場合
            if (this.selectKind.equals(E_Const.CS_INSURED)){
                if( this.record.Insured__r.E_CLTPF_ZCLKNAME__c > result.record.Insured__r.E_CLTPF_ZCLKNAME__c ){
                    returnValue = 1;
                }
                if( this.record.Insured__r.E_CLTPF_ZCLKNAME__c < result.record.Insured__r.E_CLTPF_ZCLKNAME__c ){
                    returnValue = -1;
                }
            }
*/
            //結果
            return returnValue;
        }
    }
}