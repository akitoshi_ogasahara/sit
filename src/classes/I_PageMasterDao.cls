public with sharing class I_PageMasterDao {

	/**
	 * 指定したSFIDのIRISページレコードを取得
	 * @param String: SalesforceID
	 * @return I_PageMaster__c: IRISページレコード
	 * @throws なし
	 */
	public static I_PageMaster__c getRecordById(String pid){
		List<I_PageMaster__c> results = [
				SELECT
					Id,
					Name,
					page_unique_key__c,
					Menu__c,
					Description__c,
					isHTML__c,
					CloseButton__c,
					BackButton__c,
					Menu__r.Name,
					Menu__r.MainCategory__c,
					Menu__r.MainCategoryAlias__c,
					Menu__r.MainCategoryStyleClass__c,
					Menu__r.SubCategory__c,
					Menu__r.ParentMenu__c,
					//Menu__r.ClickAction__c,
					Menu__r.Style__c,
					//Menu__r.Menu__c,
					Menu__r.MenuKey__c,
					Menu__r.LinkURL__c,
					Menu__r.DestURL__c
				FROM
					I_PageMaster__c
				WHERE
					Id =: pid
		];
		return (results.size() > 0 ? results.get(0) : null);
	}

	/**
	 * 指定したPageKeyのIRISページレコードを取得
	 * @param String: page_unique_key
	 * @return I_PageMaster__c: IRISページレコード
	 * @throws なし
	 */
	public static I_PageMaster__c getRecordByKey(String pkey){
		List<I_PageMaster__c> results = [
				SELECT
					Id,
					Name,
					page_unique_key__c,
					Menu__c,
					Description__c,
					isHTML__c,
					FeatureDisplayFrom__c,
					FeatureDisplayTo__c,
					CloseButton__c,
					BackButton__c,
					Menu__r.Name,
					Menu__r.MainCategory__c,
					Menu__r.MainCategoryAlias__c,
					Menu__r.MainCategoryStyleClass__c,
					Menu__r.SubCategory__c,
					Menu__r.ParentMenu__c,
					//Menu__r.ClickAction__c,
					Menu__r.Style__c,
					//Menu__r.Menu__c,
					Menu__r.MenuKey__c,
					Menu__r.LinkURL__c,
					Menu__r.DestURL__c
				FROM
					I_PageMaster__c
				WHERE
					page_unique_key__c = :pKey
		];
		return (results.size() > 0 ? results.get(0) : null);
	}

	/**
	 * メインカテゴリからデフォルト指定されているページレコードを取得する。
	 * @param mainCategory メインカテゴリ
	 * @return List<I_PageMaster__c> :IRISページレコード
	 * @throws なし
	 */
	public static List<I_PageMaster__c> getRecordsByDefault() {
		return [
				SELECT
					Id,
					Name,
					Menu__c,
					Description__c,
					isHTML__c,
					CloseButton__c,
					BackButton__c,
					Menu__r.Name,
					Menu__r.MainCategory__c,
					Menu__r.SubCategory__c,
					Menu__r.ParentMenu__c,
					//Menu__r.ClickAction__c,
					Menu__r.Style__c,
					//Menu__r.Menu__c,
					Menu__r.MenuKey__c,
					Menu__r.LinkURL__c,
					Menu__r.DestURL__c
				FROM
					I_PageMaster__c
				WHERE
					Default__c = true
		];
	}

	/**
	 * メインカテゴリからページ情報を取得する。
	 * @return List<I_PageMaster__c> :IRISページレコード
	 * @throws なし
	 */
	public static List<I_PageMaster__c> getRecordsByCategory(String mainCategory, String subCategory) {
		return [
				SELECT
					Id,
					Name,
					//ContentType__c,
					Menu__c,
					//PageName__c,
					Description__c,
					isHTML__c,
					CloseButton__c,
					BackButton__c,
					Menu__r.Name,
					Menu__r.MainCategory__c,
					Menu__r.SubCategory__c,
					Menu__r.ParentMenu__c,
					//Menu__r.ClickAction__c,
					Menu__r.Style__c,
					//Menu__r.Menu__c,
					Menu__r.MenuKey__c,
					Menu__r.LinkURL__c,
					Menu__r.DestURL__c
				FROM
					I_PageMaster__c
				WHERE
					Menu__r.MainCategory__c =: mainCategory
					AND
					Menu__r.SubCategory__c =: subCategory
				ORDER BY
					Menu__r.DisplayOrder__c ASC
		];
	}
}