public with sharing class I_DColiController extends I_AbstractPIPController {
	private static final String PAGE_TITLE = '保険契約概要';
	
	// 保険契約ヘッダId
	//private String policyId;

	// 証券番号
	public String chdrnum {get; set;}
	// 個人契約ヘッダ
	public E_Policy__c policy {get; private set;}
	// ダウンロード履歴
	public E_DownloadHistorry__c dh {get; set;}
	// 個人保険特約リスト
	public List<E_COVPF__c> covpfs {get; set;}
	// PDFコメント
	public String pdfComment {get; set;}
	
	/**
	 * 表示募集人取得
	 */
	public Boolean getIsViewMainAgent(){
		Boolean isViewMainAgent = true;

		return isViewMainAgent;
	}
	
	/**
	 * 担当者一覧
	 */
	public I_COLIDatas.Charge getCharge(){
		I_COLIDatas.Charge charge = I_ColiUtil.getChargeEHDCPF(this.policy);
		return charge;
	}

	/**
	 * 保障内容
	 */
	public List<I_COLIDatas.Cover> getCovers(){
		return I_ColiUtil.getCoversEHDCPF(this.covpfs);
	}
	
	/** 
	 * 契約者
	 */
	public List<I_COLIDatas.Contractor> getContractors(){
		List<I_COLIDatas.Contractor> contractors = new List<I_COLIDatas.Contractor>();
		contractors.add(I_ColiUtil.getContractorEHDCPF(this.policy));
		return contractors;
	}
	
	/** 
	 * 被保険者（主たる被保険者） 
	 */
	public List<I_COLIDatas.Insured> getInsureds(){
		List<I_COLIDatas.Insured> insureds = new List<I_COLIDatas.Insured>();
		insureds.add(I_ColiUtil.getInsuredEHDCPF(this.policy));
		return insureds;
	}

	/**
	 * Constructor
	 */
	public I_DColiController(){
		super();
		pageMessages = getPageMessages();
		pgTitle = PAGE_TITLE;
		
		this.policyId = ApexPages.CurrentPage().getParameters().get('id');
		this.covpfs = new List<E_COVPF__c>();

	}
	
	/**
	 * init
	 */
	protected override PageReference init(){
		// LogName
		pageAccessLog.Name = PAGE_TITLE;
		
		// アクセス判定
		kind = E_Const.ID_KIND.POLICY;
		authTargetId = this.policyId;
		
		PageReference pr = super.init();
		if(pr != null){
			return pr;
			
		}else{
			// 保険契約ヘッダ取得
			this.policy = E_PolicyDao.getRecByIdForIRISColi(this.policyId);
			
			// 証券番号からパイプを削除
			this.chdrnum = I_ColiUtil.delPipeCdhrnum(this.policy.COMM_CHDRNUM__c);
			
			// 個人保険特約取得
			this.covpfs = E_COVPFDao.getRecsByPolicyIdAndRtdevname(this.policyId, E_Const.COVPF_RECORDTYPE_EHDCPF);

			return null;
		}
	}

	/**
	 * PDFダウンロードボタン押下
	 */
	public PageReference doDownloadPDF(){
		try{
			pageMessages.clearMessages();

			// 事務所と募集人の設定
			I_COLIDatas.Charge charge = getCharge();
			String officeName = '';
			String agentName = '';
			if(charge.hasMainMr){
				officeName = charge.mainAccountName;
				agentName = charge.mainAgentName;
			}else{
				officeName = charge.subAccountName;
				agentName = charge.subAgentName;
			}
			
			// JSON作成
			String json = I_ColiUtil.createDlJson(this.policyId, officeName, agentName, null, getDataSyncDateHead(), getIsIss(), E_Const.POLICY_RECORDTYPE_DCOLI, new List<E_DownloadNoticeDTO.Notice>());

			// ダウンロード履歴Insert
			this.dh = I_ColiUtil.createDownloadHistorry(json, this.pdfComment);
			// 文字切り捨て
			Database.DMLOptions dml = new Database.DMLOptions();
			dml.allowFieldTruncation = true;
			this.dh.setOptions(dml);
            insert this.dh;

		}catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
		}

		// コメント初期化
		this.pdfComment = '';

		return null;
	}	
}