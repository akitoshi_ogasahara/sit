public without sharing class I_SurveyAnswerDaoWithout {

	public static void insertRec(I_SurveyAnswer__c answer){
		if(answer != null){
			insert answer;
		}
	}
}