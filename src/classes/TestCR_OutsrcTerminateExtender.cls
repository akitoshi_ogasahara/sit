@isTest
private class TestCR_OutsrcTerminateExtender {
	
	// コンストラクタテスト
  	private static testMethod void CR_OutsrcTerminateTest001(){
    	CR_OutsrcTerminateController controller;
        CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
        System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_OUTSRC);
        System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_OUTSRC_Terminate);
  	}
	
  	// initテスト (外部委託参照権限なしエラー)
	private static testMethod void CR_OutsrcTerminateTest002(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
	        	PageReference pref = Page.E_CROutsrcTerminate;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutsrcTerminateController controller = new CR_OutsrcTerminateController(standardcontroller);
	        	CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest();
        }
	}

  	// initテスト２ (レコードId、またはレコード自体が存在しない場合)
	private static testMethod void CR_OutsrcTerminateTest003(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){
        	Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = New CR_Outsrc__c();
				
	        	PageReference pref = Page.E_CROutsrcTerminate;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutsrcTerminateController controller = new CR_OutsrcTerminateController(standardcontroller);
	        	CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest(); 	
        } 
	}

	// initテスト３
	private static testMethod void CR_OutsrcTerminateTest004(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutsrcTerminate;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutsrcTerminateController controller = new CR_OutsrcTerminateController(standardcontroller);
	        	CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
	        	target.init();
				
				System.assertEquals(target.isSuccessOfInitValidate, true);
	        Test.stopTest();
		}
	}

	// 参照画面遷移
	private static testMethod void CR_OutsrcTerminateTest005(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutsrcTerminate;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutsrcTerminateController controller = new CR_OutsrcTerminateController(standardcontroller);
	        	CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
        	
        		System.assertEquals(target.getUrlforView(), '/apex/e_croutsrcview?id=' + outsrc.Id);

	        Test.stopTest();
		} 
	}

	// 保存処理 (入力規則エラー)
	private static testMethod void CR_OutsrcTerminateTest006(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcEndDate(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutsrcTerminate;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutsrcTerminateController controller = new CR_OutsrcTerminateController(standardcontroller);
	        	CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
	        	        	
        		System.assertEquals(target.doSaveEx().getUrl(), '/apex/e_croutsrcview?id=' + outsrc.Id);

	        Test.stopTest();
		} 
	}	
	
	// 保存処理 (入力規則エラー)
	private static testMethod void CR_OutsrcTerminateTest007(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutsrcTerminate;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutsrcTerminateController controller = new CR_OutsrcTerminateController(standardcontroller);
	        	CR_OutsrcTerminateExtender target = new CR_OutsrcTerminateExtender(controller);
	        	        	
        		System.assertEquals(target.doSaveEx(), null);

	        Test.stopTest();
		} 
	}	
}