/**
 * 基底となるBatchApexの抽象クラスです
 */
global abstract with sharing class E_AbstractCommonBatch implements Database.Batchable<SObject>, Database.Stateful {
	
	//バッチ正常終了/異常終了フラグ
	protected Boolean isSuccess;
	//BatchApexクラス名
	protected String batchClassName;
	//主Id
	protected String id;
	//公開グループ取得(BRxx)
	protected Map<Id,Group> zbusbrIdMap;
	//key:母店支社コード, value:Group.Id
	protected Map<String, Group> zbusbrCdMap;
	//実行ログ
	private String executelog = '';

	/**
	 * コンストラクタ
	 */
	public E_AbstractCommonBatch() {
		initData();
	}
	
	/**
	 * BatchApexクラスをセットします
	 * @param batchClassName
	 * @return
	 */
	public void setup(String batchClassName) {
		this.batchClassName = batchClassName;
		System.assertNotEquals(null, batchClassName, 'Batchクラス名が指定されていません。');
	}
	
	/**
	 * 主IDをセットします
	 * @param id
	 * @return
	 */
	public void setId(String id) {
		this.id = id;
		System.assertNotEquals(null, id, 'Idが指定されていません。');
	}
	
	/**
	 * BatchApexの処理実行します
	 * @param ctx
	 * @param records
	 * @return
	 */
	global void execute(Database.BatchableContext ctx, List<Sobject> records) {
		String operationName = batchClassName + 'Operation';
		Type t = Type.forName(operationName);
		
		E_AbstractCommonBatchOperation operation = (E_AbstractCommonBatchOperation)t.NewInstance();
		operation.setId(id);
		operation.setZbusbrIdMap(zbusbrIdMap);
		operation.setZbusbrCdMap(zbusbrCdMap);
		try {
			if(operation.init(records)) {
				operation.execute();
			}
		} catch (Exception e) {
			throw e;
			
		} finally {
			String oplogger = operation.getLogger();
			if (oplogger.length() > 0) {
				setLogger(oplogger);
			}
		}
	}
	
	/**
	 * BatchApexの後処理
	 * @param ctx
	 * @return
	 */
	global virtual void finish(Database.BatchableContext ctx) {
    	updateRecords(id, executelog);
	}

	/**
	 *
	 */
	private void initData() {
		//公開グループ取得(BRxx)
		zbusbrIdMap = new Map<Id,Group>([Select Id, Name, Type, DeveloperName From Group where type = 'Regular' and developerName like 'BR%']);
		//key:母店支社コード, value:Group.Id
		zbusbrCdMap = new Map<String, Group>();
		for (Group gp : zbusbrIdMap.values()) {
			zbusbrCdMap.put(gp.developername, gp);
		}
	}
	
	/**
	 * 
	 */
	private void updateRecords(Id recordId, String executelog) {
		E_BizDataSyncLog__c ebizlog = new E_BizDataSyncLog__c();
		ebizlog.id = recordId;
		if (executelog != null) {
			ebizlog.log__c = executelog; 
		}
		
		ebizlog.datasyncenddate__c = Datetime.now();
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		ebizlog.setOptions(dml);
		update ebizlog;
	}
	
    /**
     * 実行ログをセットします。
     */
	private void setLogger(String addlog) {
		if (executelog.length() > 0) {
			executelog += ',';
		} 
		executelog += addlog;
	}
}