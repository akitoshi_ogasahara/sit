global with sharing class CR_RuleSubmitExtender extends CR_AbstractExtender{
    @TestVisible
    private CR_RuleSubmitController controller;
    @TestVisible
    private String agencyId;
    public Attachment file{get;set;}
    
    public CR_RuleSubmitExtender(CR_RuleSubmitController extension){
        super();
        this.controller = extension;
        this.agencyId = ApexPages.currentPage().getParameters().get('parentId');
        this.file = new Attachment();
        
        //ヘルプ設定
        helpMenukey = CR_Const.MENU_HELP_RULE;
        //新規アップロードか規定種別変更かで遷移先を変更する
        if(getIsNew()){
            helpPageJumpTo = CR_Const.MENU_HELP_RULE_Submit_New;    
        }else{
            helpPageJumpTo = CR_Const.MENU_HELP_RULE_Submit_Upd;
        }
    }

    public override void init(){
        if(agencyId == null || String.isBlank(agencyId)){
            isSuccessOfInitValidate = false;
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            this.viewingAgencyCode = this.controller.record.CR_Agency__r.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }       
    }

    public Boolean getIsNew(){
        //Idがない場合は新規とする。
        return controller.record.Id == null;
    }

    public PageReference doSaveEx(){
        pageMessages.clearMessages();

        if(getIsNew() &&
            (this.file==null||this.file.Body==null||String.isBlank(this.file.Name))
        ){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG02_REQUIRED_FILE);
            return null;
        }
        
        Savepoint sp = Database.setSavepoint();
        Boolean saveResult = false;
        try{
            if(getIsNew()){
                controller.record.CR_Agency__c = agencyId;
                saveResult = E_GenericDao.saveViaPage(controller.record);//insert controller.record;
                if(saveResult){
                    this.file.parentId = controller.record.Id;
                    this.file.ContentType = CR_Const.CONTENTTYPE_FOR_DOWNLOAD;		//強制的にファイルダウンロードを実施させる
                    insert file;
                }
            }else{
                saveResult = E_GenericDao.saveViaPage(controller.record); //update controller.record;
            }
            
            if(saveResult){
                return backToRulesPage();
            }
        }catch(Exception e){
            Database.Rollback(sp);
            pageMessages.addErrorMessage('ファイルアップロードエラー：'+e.getMessage());
        }
        return null;
    }

    public PageReference doSaveAndNew(){
        PageReference pr = doSaveEx();
        if(pr != null){
            PageReference prNew = Page.E_CRRuleSubmit;
            prNew.getParameters().put('parentId', controller.record.CR_Agency__c);
            prNew.setRedirect(true);
            return prNew;
        }
        return null;
    }
    
    public PageReference backToRulesPage(){
        PageReference pr = Page.E_CRRules;
        pr.getParameters().put('id', agencyId);
        return pr;
    }   
    
}