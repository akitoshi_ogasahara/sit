public with sharing class E_SUPPFDao {
    
    /**
     * 保険契約IDをキーにステップアップ情報を取得する
     * 複数件存在する場合には、1件目を返却する。
     * @param policyId: 保険契約ID
     * @return E_SUPPF__c: ステップアップ情報
     */
    public static E_SUPPF__c getRecByPolicyId(Id policyId){

        E_SUPPF__c rec = null;
        List<E_SUPPF__c> recs = [
            Select 
            ZSTUPDAY01__c,
            ZSTUPDAY02__c,
            ZSTUPDAY03__c,
            ZSTUPDAY04__c,
            ZSTUPDAY05__c,
            ZSTUPDAY06__c,
            ZSTUPSUM01__c,
            ZSTUPSUM02__c,
            ZSTUPSUM03__c,
            ZSTUPSUM04__c,
            ZSTUPSUM05__c,
            ZSTUPSUM06__c,
            ZATTMRAT01__c,
            ZATTMRAT02__c,
            ZATTMRAT03__c,
            ZATTMRAT04__c,
            ZATTMRAT05__c,
            ZATTMRAT06__c,
            ZATTMRAT07__c,
            ZATTMRAT08__c,
            ZATTMRAT09__c,
            ZATTMRAT10__c,
            ZATTMRAT11__c,
            ZATTMRAT12__c,
            ZATTMRAT13__c,
            ZATTMRAT14__c,
            ZATTMRAT15__c,
            ZATTMRAT16__c,
            ZATTMRAT17__c,
            ZATTMRAT18__c,
            ZATTMRAT19__c,
            ZATTMRAT20__c,
            ZSTUPRAT01__c,
            ZSTUPRAT02__c,
            ZSTUPRAT03__c,
            ZSTUPRAT04__c,
            ZSTUPRAT05__c,
            ZSTUPRAT06__c
            From 
                E_SUPPF__c 
            Where 
                E_Policy__c = :policyId
               ];
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
    }
}