public with sharing class SC_SelfCompliances_IrisController extends I_AbstractController{
	// 点検表タイトル key: 種別, value:タイトル
	private static final Map<String, String> titles = new Map<String, String>{
															SC_Const.SC_COMP_TYPE_COMMON => SC_Const.SC_TITLE_COMMON,
															SC_Const.SC_COMP_TYPE_NN => SC_Const.SC_TITLE_NN,
															SC_Const.SC_COMP_TYPE_TRAIL => SC_Const.SC_TITLE_TRAIL
														  };
	// 自主点検　種別Map
	private Map<String, SC_SelfCompliance__c> selfCompMap;
	
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// アクセス管理クラス
	public SC_AuthorityConfirmation authority {get;set;}
	// ステータス管理クラス
	public SC_StatusManager_Iris stManager {get;set;}
	
	//自主点検事務所ID
	public String officeid {get;set;}
	//　自主点検事務所
	public SC_Office__c office {get;set;}
	// 追加コメント
	public String addComment {get; set;}
	// 追加問い合わせ
	public String addInquiry {get; set;}
	//提出書類
	public List<submitListRow> submitList {get;set;}
	//ブラウザ判定
	public Boolean isOldIE {get; set;}
	//削除対象となるファイルID
	public String targetFileId {get; set;}
	// Modal window関連
	public String modalMessage{get; private set;}
	public Boolean isModalBtn{get; private set;}
	public Boolean isDispModal{get; private set;}
	//提出方法更 新用
	public String typeSubmit {get; set;}
	public String targetId {get; set;}
	public String targetPDFId {get;set;}

	// ダウンロード履歴
	public E_DownloadHistorry__c dh {get; set;}
	//共通点検表のID
	public Id commonId {get;set;}

	//PDFファイルダウンロード文言フラグ
	public Boolean pdfWordingFlag {get;set;}
	//ファイルダウンロード文言フラグ
	public Boolean fileWordingFlag {get;set;}
	//コメント追記ボタン表示フラグ
	public Boolean commentFlag {get;set;}
	//基本情報 ステータス
	public String displaystatus {get;set;}

	//保留モーダル関連
	public String holdMessage {get;set;}
	public String commonToken {get;set;}
	public String commonSurveyId {get;set;}
	public Id commonSelfCompId {get;set;}
	public String nnToken {get;set;}
	public String nnSurveyId {get;set;}
	public Id nnSelfCompId {get;set;}

	//回答変更時モーダル判定
	public Boolean commonAnswerChange {get;set;}
	public Boolean nnAnswerChange {get;set;}
	public Boolean bothChanges {get;set;}
	public String commonChangeMessage {get;set;}
	public String nnChangeMessage {get;set;}
	public String bothChangesMessage {get;set;}
	public Attachment commonAtt {get;set;}
	public Attachment nnAtt {get;set;}
	public PageReference commonpdfUpLink {get;set;}
	public PageReference nnpdfUpLink {get;set;}
	public Id modalAttId {get;set;}
	public String modalAttName {get;set;}

	// 前年度点検
	public String prevSCURL {get; set;}
	public Boolean getHasPrevSCOffice() {
		return String.isNotBlank(prevSCURL);
	}

	// 次年度点検
	public String retSCURL {get; set;}
	public Boolean getHasRetSCOffice() {
		return String.isNotBlank(retSCURL);
	}
	
	// 点検実施の手引きIRISページ取得
	public String getManualId(){
		String uniqueKey = SC_Const.SC_MANUAL_PREFIX + office.FiscalYear__c;
		I_PageMaster__c pageMaster = [Select Id From I_PageMaster__c Where page_unique_key__c = :uniqueKey];
		return pageMaster == null ? '' : pageMaster.Id; 
	}

	// 操作説明PDF取得
	public Id getHelpId(){
		Id helpFileId = null;
		I_ContentMaster__c cms = I_ContentMasterDao.getRecordByUpsertKey('コンポーネント|'+ office.FiscalYear__c + SC_Const.SC_OPERETION_EXPLANATION);
		if(cms != null){
			if(cms.ClickAction__c == I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT && !cms.Attachments.isEmpty() ){
			//クリックアクションがDL(Attachment)の場合、DLファイル格納先とファイル名が一致する最新のAttachment
				for(Attachment att : cms.Attachments){
					if(att.Name == cms.FilePath__c){
						helpFileId = att.Id;
						break;
					}
				}
			}else if(cms.ClickAction__c == I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK){
			//クリックアクションがDL(NNLink)の場合、DLファイル格納先の外部IDを持つファイル情報の最新のAttachment
				E_CMSFile__c cmsfile = E_AttachmentDao.getRecByUpsertKey(cms.FilePath__c);
				if(cmsfile != null && !cmsfile.Attachments.isEmpty()){
					helpFileId = cmsfile.Attachments[0].Id;
				}
			}
		}
		return helpFileId;

	}

	// TODO　要検討
	public String getSCStatus02(){
		return SC_Const.AMSOFFICE_STATUS_02_AG;
	}

	// TODO　要検討
	public String getSCStatus05(){
		return SC_Const.AMSOFFICE_STATUS_05_AG;
	}

	/**
	 * Constructor
	 */
	public SC_SelfCompliances_IrisController() {
		pageMessages = getPageMessages();
	}

	//メニューキー
	protected override String getLinkMenuKey() {
		if(access.isEmployee()){
			return SC_Const.MENU_KEY_SC_SEARCH_EMPLOYEE;
		}else{
			return SC_Const.MENU_EKY_SC_SEARCH_AGENT;
		}

	}

	/**
	 * GW経由判定
	 */
	public Boolean getIsGW(){
		return access.isCommonGateway();
	}

	/**
	 * init
	 */
	protected override pageReference init() {
		try{
			// アクセスログのName項目の設定
			this.pageAccessLog.Name = this.activePage.Name;
			if (pageAccessLog.Name==null) {
				pageAccessLog.Name = this.menu.Name;
			}
			
			// NNLink使用の場合エラー画面へリダイレクト
			if(access.isUseNNLink()){
				PageReference pr = Page.E_Info;
				pr.getParameters().put('menu', 'sclinkng');
				return pr;
			}
			
			// 自主点検事務所レコード取得
			officeid = ApexPages.currentPage().getParameters().get('id');
			office = SC_OfficeDao.getRecbyId(officeid);

			//文言フラグ初期化
			pdfWordingFlag = false;
			fileWordingFlag = false;
			commentFlag = false;

			//保留文言セット
			holdMessage = getMSG().get('SCC|M|004');

			// アクセス管理クラスのインスタンス化
			authority = new SC_AuthorityConfirmation(office);
			
			// ステータス管理クラスのインスタンス化
			stManager = new SC_StatusManager_Iris(office, access);
			
			// 前年度、次年度のURLセット
			setOtherYearUrl();

			//自主点検レコード取得
			selfCompMap = getSelfCompMap();

			// 提出書類一覧生成
			createSubmitList();

			//モーダル初期化
			modalMessage = '';
			isModalBtn = false;
			commonAnswerChange = false;
			nnAnswerChange = false;
			bothChanges = false;
			commonChangeMessage = '';
			nnChangeMessage = '';
			bothChangesMessage = '';

		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}

		return null;
	}

	/**
	 * 自主点検レコード取得
	 *
	 */
	private Map<String, SC_SelfCompliance__c> getSelfCompMap() {
		Map<String, SC_SelfCompliance__c> tmpMap = new Map<String, SC_SelfCompliance__c>();
		for (SC_SelfCompliance__c record : SC_SelfComplianceDao.getRecsByOfficeId(officeid)) {
			tmpMap.put(record.Kind__c, record);
		}
		return tmpMap;
	}

	/**
	 * 提出書類一覧生成
	 */
	private void createSubmitList() {
		getLinkMenuKey();
		//自主点検レコード再取得
		selfCompMap = getSelfCompMap();

		//基本情報 ステータス
		if(stManager.getisAG() && (office.Status__c == SC_Const.AMSOFFICE_STATUS_02||office.Status__c == SC_Const.AMSOFFICE_STATUS_04)){
			displaystatus = SC_Const.AMSOFFICE_STATUS_02_AG;
		}else{
			displaystatus = office.Status__c;
		}
		
		// 提出書類クラスのリスト
		submitList = new List<submitListRow>();

		// 提出一覧作成
		makeTable(SC_Const.SC_COMP_TYPE_COMMON);
		makeTable(SC_Const.SC_COMP_TYPE_NN);
		makeEvidenceMaterial(SC_Const.SC_COMP_TYPE_TRAIL);

		if(stManager.getIsAG() && authority.getAuthority(authority.TYPE_DOWNLOAD_PDF)){
			pdfWordingFlag = true;
		}
		if(stManager.getIsAG() && !authority.getAuthority(authority.TYPE_DOWNLOAD)){
			fileWordingFlag = true;
		}
		commentFlag = authority.getAuthority(authority.TYPE_COMMENT);

	}

	/**
	 * 提出一覧作成　共通、追加
	 */
	private  void makeTable(String kind) {
		if (selfCompMap.containsKey(kind)) {
			//共通点検表項目の初期化
			SubmitListRow submitRow = new SubmitListRow();

			// 対象の自主点検レコード取得
			SC_SelfCompliance__c record = selfCompMap.get(kind);
			
			// LimeSurvey情報
			submitRow.token = record.LimeSurveyToken__c;
			submitRow.surveyId = record.LimeSurveyId__c;

			// 提出種別
			String title = '';
			if (titles.containsKey(record.Kind__c)) title = titles.get(record.Kind__c);

			submitRow.submitType = record.FiscalYear__c + '年度 ' + title; 

			// ボタン ラベル
			submitRow.btnLabel = SC_Const.SC_BTNLABEL_WEB_ANSWER_CONFIRM;
			// 代理店 かつ 未提出 かつ Web回答なし かつ 回答権限あり
			if (stManager.getIsAG() && office.Status__c == SC_Const.AMSOFFICE_STATUS_01 && record.SC_WebAnswers__r.size() == 0 && record.Attachments.size() == 0 && authority.getAuthority(authority.TYPE_SUBMISSION_REMAND)) {
				submitRow.btnLabel = SC_Const.SC_BTNLABEL_WEB_ANSWER;
			} else if (stManager.getIsAG() && office.Status__c == SC_Const.AMSOFFICE_STATUS_01 && record.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_PDF && authority.getAuthority(authority.TYPE_SUBMISSION_REMAND)) {
				submitRow.btnLabel = SC_Const.SC_BTNLABEL_WEB_ANSWER_CHANGE;
			}

			// リンク ラベル
			submitRow.linkLabel = SC_Const.SC_BTNLABEL_PDF_UPLOAD;
			// Web回答あり
			if (record.TypeSubmit__c != SC_Const.SC_TYPE_SUBMIT_PDF && record.SC_WebAnswers__r.size() > 0){
				submitRow.linkLabel = SC_Const.SC_BTNLABEL_PDF_CHANGE;
				submitRow.pdfChangeFlag = true;
			}
			else if (record.Attachments.size() > 0 && stManager.getIsAG()) submitRow.linkLabel = SC_Const.SC_BTNLABEL_PDF_REUPLOAD;
			else if (record.Attachments.size() > 0 && !stManager.getIsAG()) submitRow.linkLabel = SC_Const.SC_BTNLABEL_REUPLOAD;

			// 提出方法　PDF
			if (record.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_PDF) {
				List<Attachment> attachment = record.Attachments;
				if(attachment.size() > 0 && record.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_PDF){
					submitRow.att = attachment[0];
					submitRow.modifiedName = E_Util.null2Blank(submitRow.att.LastmodifiedBy.LastName) + E_Util.null2Blank(submitRow.att.LastmodifiedBy.FirstName);
					submitRow.modifiedDate = E_Util.toString(submitRow.att.LastmodifiedDate);
					//ボタン表示
					submitRow.downloadFlag = authority.getAuthority(authority.TYPE_DOWNLOAD);
					submitRow.deleteFlag = authority.getAuthority(authority.TYPE_DELETE);
					submitRow.uploadFlag = authority.getAuthority(authority.TYPE_UPLOAD);

					//モーダルボタン用に、情報を格納
					if(record.Kind__c == SC_Const.SC_COMP_TYPE_COMMON){
						commonAtt = attachment[0];
					}else{
						nnAtt = attachment[0];
					}

				}
				// ボタンリンク表示
				submitRow.webChangeFlag = authority.getAuthority(authority.TYPE_CHANGE_WEB);
				submitRow.isDispBtn = authority.getAuthority(authority.TYPE_CHANGE_WEB);
				submitRow.isDispLink = authority.getAuthority(authority.TYPE_UPLOAD);
				if (!stManager.getIsAG()) {
					submitRow.styleClassReupload = 'iris-sm-btn d-inline-block iris-text-white iris-button-gray';
				}

			// 提出方法　WEB
			} else  {
				// WebAnser 最終更新
				if (record.SC_WebAnswers__r.size() > 0) {
					for (SC_WebAnswer__c webAnswer : record.SC_WebAnswers__r) {
						submitRow.modifiedName = webAnswer.LastmodifiedBy__c;
						submitRow.modifiedDate = E_Util.toString(webAnswer.LastmodifiedDate);
					}
				}
				// ボタンリンク表示
				submitRow.isDispBtn = authority.getAuthority(authority.TYPE_WEB_READ);
				submitRow.isDispLink = authority.getAuthority(authority.TYPE_CHANGE_PDF);
				if(record.Kind__c == SC_Const.SC_COMP_TYPE_COMMON){ //共通点検表の時のみ、PDFダウンロード
					submitRow.pdfdownloadFlag = authority.getAuthority(authority.TYPE_DOWNLOAD_PDF);
				}

				// 保留チェック
				if(record.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_WEB && record.holdCount__c > 0) {
					submitRow.holdFlag = true;
				}
			}

			submitRow.selfComplianceId = selfCompMap.get(kind).id;

			PageReference pr = Page.IRIS_SCSelfComplianceSubmit;
			pr.getParameters().put('id',submitRow.selfComplianceId);
			pr.getParameters().put('type',kind);
			pr.getParameters().put('isSingle','true');
			pr.getParameters().put('isOldIE',String.valueOf(isOldIE));
			pr.getParameters().put('year',record.FiscalYear__c);
			pr.getParameters().put('irismn',menu.Id);
			submitRow.pdfUpLink = pr;

			//モーダルボタン用に、情報を格納
			if(record.Kind__c == SC_Const.SC_COMP_TYPE_COMMON){
				submitRow.commonFlag = true;
				commonToken = record.LimeSurveyToken__c;
				commonSurveyId = record.LimeSurveyId__c;
				commonSelfCompId = selfCompMap.get(SC_Const.SC_COMP_TYPE_COMMON).id;
				commonpdfUpLink = submitRow.pdfUpLink;
			}else{
				submitRow.nnFlag = true;
				nnToken = record.LimeSurveyToken__c;
				nnSurveyId = record.LimeSurveyId__c;
				nnSelfCompId = selfCompMap.get(SC_Const.SC_COMP_TYPE_NN).id;
				nnpdfUpLink = submitRow.pdfUpLink;
			}

			submitList.add(submitRow);
		}
	}

	/**
	 * 提出一覧作成　証跡資料
	 */
	private void makeEvidenceMaterial(String kind){
		SubmitListRow evidenceRow = new SubmitListRow();
		SC_SelfCompliance__c record = selfCompMap.get(kind);
		//証跡資料ファイルの取得
		List<Attachment> attachment = record.Attachments;

		// ボタンリンク表示
		evidenceRow.isDispBtn = authority.getAuthority(authority.TYPE_UPLOAD);
		
		evidenceRow.btnLabel = SC_Const.SC_BTNLABEL_ADDUPLOAD;
		if (office.Status__c == SC_Const.AMSOFFICE_STATUS_01 && attachment.size() == 0) {
			evidenceRow.btnLabel = SC_Const.SC_BTNLABEL_UPLOAD;
		}

		for(Attachment att : attachment) {
			if (evidenceRow.att == null) {
				evidenceRow.att = att;
				evidenceRow.modifiedName = E_Util.null2Blank(evidenceRow.att.LastmodifiedBy.LastName) + E_Util.null2Blank(evidenceRow.att.LastmodifiedBy.FirstName);
				evidenceRow.modifiedDate = E_Util.toString(evidenceRow.att.LastmodifiedDate);
				//ボタン表示
				evidenceRow.downloadFlag = authority.getAuthority(authority.TYPE_DOWNLOAD);
				evidenceRow.deleteFlag = authority.getAuthority(authority.TYPE_DELETE);
				evidenceRow.uploadFlag = authority.getAuthority(authority.TYPE_UPLOAD);
			} else {
				TrailAttachmentListRow trailrow = new TrailAttachmentListRow();

				trailrow.att = att;
				trailrow.modifiedDate = E_Util.toString(trailrow.att.LastmodifiedDate);
				trailrow.deleteFlag = evidenceRow.deleteFlag;
				trailrow.downloadFlag = evidenceRow.downloadFlag;
				evidenceRow.trailList.add(trailrow);
			}
		}

		evidenceRow.submitType = SC_Const.SC_TITLE_TRAIL;
		PageReference pr = Page.IRIS_SCSelfComplianceSubmit;
		pr.getParameters().put('id',selfCompMap.get(kind).id);
		pr.getParameters().put('type',kind);
		pr.getParameters().put('isSingle','false');
		pr.getParameters().put('isOldIE',String.valueOf(isOldIE));
		pr.getParameters().put('year',record.FiscalYear__c);
		pr.getParameters().put('irismn',menu.Id);
		evidenceRow.pdfUpLink = pr;
		evidenceRow.trailFlag = true;

		submitList.add(evidenceRow);
	}

	/**
	 * コメント保存処理
	 */
	public void doSaveComment(){
		try{
			// コメント未入力の場合は処理終了
			if(String.isBlank(addComment)){
				return;
			}

			// コメントの再取得
			List<SC_Office__c> officeNow = SC_OfficeDao.getRefreshSCOffice(office.Id);
			if(officeNow == null || officeNow.isEmpty()){
				return;
			}

			office.Comment__c = officeNow.get(0).Comment__c;
			if(String.isBlank(office.Comment__c)){
				office.Comment__c = '';
			}

			// 投稿者情報を作成
			String bodyInfo = createBody(addComment);
			office.Comment__c = bodyInfo + office.Comment__c;

			// Update
			update office;

			// 追加コメントを初期化
			addComment = '';

		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}
		
		return;
	}

	/**
	 * 問い合わせ保存処理
	 */
	public void doSaveInquiry(){
		try{
			// 問い合わせ未入力の場合は処理終了
			if(String.isBlank(addInquiry)){
				return;
			}

			// 問い合わせの再取得
			List<SC_Office__c> officeNow = SC_OfficeDao.getRefreshSCOffice(office.Id);
			if(officeNow == null || officeNow.isEmpty()){
				return;
			}

			office.Inquiry__c = officeNow.get(0).Inquiry__c;
			if(String.isBlank(office.Inquiry__c)){
				office.Inquiry__c = '';
			}

			// 投稿者情報を作成
			String bodyInfo = createBody(addInquiry);
			office.Inquiry__c = bodyInfo + office.Inquiry__c;
			
			// 代理店 => CMD
			if(stManager.getIsAG()){
				SC_MailManager.sendMail(SC_MailManager.MT_INQUIRY_AGCMD, office, new List<SC_AgencyManager__c>());
				office.InquiryAgCmd__c = true;

			// MR/拠点長 => 代理店
			}else if(stManager.getIsMR() || stManager.getIsMGR()){
				SC_MailManager.sendMail(SC_MailManager.MT_INQUIRY_MRAG, office, office.SC_AgencyManagers__r);
				office.InquiryMrAg__c = true;

			// CMD => 代理店
			}else if(stManager.getIsCMD()){
				SC_MailManager.sendMail(SC_MailManager.MT_INQUIRY_CMDAG, office, office.SC_AgencyManagers__r);
				office.InquiryCmdAg__c = true;
			}

			// Update
			update office;

			// 問い合わせを初期化
			addInquiry = '';

		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}
		
		return;
	}
	
	/**
	 * 投稿者情報を作成
	 */
	private String createBody(String body){
		String bodyInfo = '';
		bodyInfo += '-----';
		bodyInfo += System.now().format();
		bodyInfo += '  ';
		bodyInfo += UserInfo.getName();
		bodyInfo += '-----';
		bodyInfo += '\n';
		bodyInfo += body;
		bodyInfo += '\n\n';
		return bodyInfo;
	}

	/**
	 * 画面再描画
	 */
	public PageReference rerenderBrowser(){
		isOldIE = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isOldIE'));
		createSubmitList();
		return null;
	}

	/*
	 * ファイル削除
	 *
	 */
	public PageReference deleteFile() {
		//system.debug('debug:targetFileId='+ targetFileId);
		//targetFileId = System.currentPageReference().getParameters().get('targetFileId');
		//try {
		//	Attachment record = new Attachment();
		//	record.id = id.valueOf(targetFileId);
		//	E_GenericDaoWithOutSharing.deleteRecord(record);

		//	// リスト作成し直し
		//	createSubmitList();

		//}catch(Exception e){
		//	pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		//}


		//return null;

		//system.debug('debug:targetFileId='+ targetFileId);
		//targetFileId = System.currentPageReference().getParameters().get('targetFileId');
		try {
			Attachment record = new Attachment();
			record.id = id.valueOf(modalAttId);
			E_GenericDaoWithOutSharing.deleteRecord(record);

			// リスト作成し直し
			createSubmitList();

		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}


		return null;
	}



	/**
	 * 提出種別更新
	 */
	public void updateTypeSubmit() {
		try{
			SC_SelfCompliance__c record = new SC_SelfCompliance__c();
			record.id = targetId;
			record.TypeSubmit__c = typeSubmit;
			update record;

			//点検表Web回答無効化処理
			//List<SC_WebAnswer__c> ansList = [select id,SC_SelfCompliance__c from SC_WebAnswer__c where SC_SelfCompliance__c =: record.Id];
			//System.debug(ansList);
			//if(typeSubmit == 'PDF' && ansList.size() > 0){
			//	E_GenericDaoWithOutSharing.deleteRecord(ansList);
			//}
			createSubmitList();
		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}

		//return null;
	}

	/*
	 * PDFファイル削除
	 *
	 */
	public void deletePDFFile() {
		try {
			Attachment record = new Attachment();
			record.id = targetPDFId;
			E_GenericDaoWithOutSharing.deleteRecord(record);

			// リスト作成し直し
			createSubmitList();

		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}
	}

	/**
	 * 提出ボタン 無効判定
	 */
	public Boolean getIsApplyDisabled() {
		try{
			SC_SelfCompliance__c common = selfCompMap.get(SC_Const.SC_COMP_TYPE_COMMON);
			SC_SelfCompliance__c additional = selfCompMap.get(SC_Const.SC_COMP_TYPE_NN);
			SC_SelfCompliance__c evidence = selfCompMap.get(SC_Const.SC_COMP_TYPE_TRAIL);

			Boolean isDisabled = true;

			// 権限チェック
			if ((stManager.getIsAG() && stManager.getIsAGMGR()) 
				|| stManager.getIsMR() || stManager.getIsMGR() || stManager.getIsCMD()) {
				isDisabled = false;
			}

			// 状況チェック
			if ((common.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_PDF && common.Attachments.size() == 0)
			 || (common.TypeSubmit__c != SC_Const.SC_TYPE_SUBMIT_PDF && common.SC_WebAnswers__r.size() == 0)
			 || (additional.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_PDF && additional.Attachments.size() == 0)
			 || (additional.TypeSubmit__c != SC_Const.SC_TYPE_SUBMIT_PDF && additional.SC_WebAnswers__r.size() == 0)
			 || (common.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_WEB && common.holdCount__c > 0)
			 || (additional.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_WEB && additional.holdCount__c > 0)) {
				isDisabled = true;
			}



			return isDisabled;

		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
			return true;
		}
	}

	/**
	 * 申請ボタン（エヌエヌ生命提出, 本社へ提出, 完了） 
	 */
	public pageReference doAction() {
		try{
			modalMessage = '';
			isModalBtn = false;
			isDispModal = false;
			commonAnswerChange = false;
			nnAnswerChange = false;
			bothChanges = false;

			//自主点検レコード再取得
			selfCompMap = getSelfCompMap();

			if (stManager.getNextActionLabel() == SC_StatusManager_Iris.scActionLabel.get(SC_StatusManager_Iris.ScAction.APPLY)) {
				// エヌエヌ生命に提出
				isModalBtn = true;
				isDispModal = true;
				modalMessage = getMSG().get('SCC|M|001');

				SC_SelfCompliance__c common = selfCompMap.get(SC_Const.SC_COMP_TYPE_COMMON);
				SC_SelfCompliance__c additional = selfCompMap.get(SC_Const.SC_COMP_TYPE_NN);
				SC_SelfCompliance__c evidence = selfCompMap.get(SC_Const.SC_COMP_TYPE_TRAIL);

				List<SC_SelfCompliance__c> records = new List<SC_SelfCompliance__c>{additional, common};	// エラーメッセージの関係上、追加からチェックを行う

				// 証跡チェック
				if (evidence.Attachments.size() == 0) {
					modalMessage = getMSG().get('SCC|M|002');
				}

				// 更新日チェック
				String lsURL = system.Label.SC_LimeSurveyURL + system.Label.SC_LimeSurveyURL_Plugins;
				for (SC_SelfCompliance__c record : records) {
					
					String status = '';
					if (record.TypeSubmit__c == SC_Const.SC_TYPE_SUBMIT_WEB ) {
						Http http = new Http();
						HttpRequest request = new HttpRequest();
						request.setMethod('GET');
						String lsEndpoint = '';
						for (SC_WebAnswer__c rec : record.SC_WebAnswers__r) {
							List<String> s = rec.submitDate__c.split('[-\\s:]');
							Datetime d = Datetime.newInstanceGMT(Integer.valueOf(s[0]),Integer.valueOf(s[1]),Integer.valueOf(s[2]),Integer.valueOf(s[3]),Integer.valueOf(s[4]),Integer.valueOf(s[5]));
							// -9時間
                            d = d.addHours(-9);
							String submitDate = d.format('yyyy-MM-dd\'T\'HH:mm:ss+09:00');
							List<String> fillers = new String[]{record.LimeSurveyToken__c, record.LimeSurveyId__c, EncodingUtil.urlEncode(submitDate, 'UTF-8')};
							lsEndpoint = String.format(lsURL, fillers);
							system.debug('debug:submitDate__c=' + rec.submitDate__c + ', datetime=' + d + ', submitDate=' + submitDate);
						}
						system.debug('debug:LS Endpoint=' + lsEndpoint);
						request.setEndpoint(lsEndpoint);

						HttpResponse response = http.send(request);
						Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
						status = String.valueOf(results.get('status'));
						System.debug('response status='+status);

						if (status == 'NG') {
							isModalBtn = false;
							if(record.Kind__c == SC_Const.SC_COMP_TYPE_COMMON){
								commonAnswerChange = true;
								commonChangeMessage = getMSG().get('SCC|M|003');
							}else if(record.Kind__c == SC_Const.SC_COMP_TYPE_NN){
								nnAnswerChange = true;
								nnChangeMessage = getMSG().get('SCC|M|005');
							}
							
							if(commonAnswerChange && nnAnswerChange){
								bothChangesMessage = getMSG().get('SCC|M|006');
								bothChanges = true;
							}
						}
					}

				}

			} else {
				// 本社へ提出, 完了
				stManager.doNextAction();
			}


		}catch(Exception e){
			pageMessages.addErrorMessage('[エラーが発生しました] ' + e.getMessage());
		}
		createSubmitList();

		return null;
	}

	/**
	 * Modal Window のメッセージセット
	 *
	 * @param kind 		種別
	 * @param message	メッセージ
	 */
	private void setModalMessage(String kind, String message) {
		if (modalMessage.contains(message)) {
			modalMessage = '【' + titles.get(kind) + '】' + modalMessage;
		} else {
			modalMessage = '【' + titles.get(kind) + '】' + message;
		}
	}

	/**
	 * reload
	 *
	 * actionFunctionからのリロード用
	 */
	public pageReference reload() {
		return null;
	}

	/**
	 * 提出書類　一覧クラス
	 */
	public class SubmitListRow{
		public String submitType {get;set;}			//提出種別
		public Attachment att{get;set;}				// 添付ファイル
		//public String fileName {get;set;}			//ファイル名
		//public String fileId {get;set;}				//ファイルID
		public String modifiedName {get;set;}		//更新者
		public String modifiedDate {get;set;}		//更新日
		public Boolean deleteFlag {get;set;}		//削除ボタン表示フラグ
		public Boolean downloadFlag {get;set;}		//ダウンロードボタン表示フラグ
		public Boolean pdfdownloadFlag {get;set;}		//PDFダウンロードボタン表示フラグ
		public Boolean uploadFlag {get;set;}		//アップロードリンク表示フラグ
		public String webLink {get;set;}			//Webで回答ボタンリンク先
		public PageReference pdfUpLink {get;set;}	//PDFでアップロードリンク先
		public Boolean trailFlag {get;set;}			//証跡判定フラグ
		public Id selfComplianceId {get; set;}		//自主点検レコードID
		public String token{get;set;}				//LimeSurvey token
		public String surveyId{get;set;}			//LimeSurvey id
		public String btnLabel{get;set;}			//ボタン ラベル
		public String linkLabel{get;set;}			//リンク ラベル
		public Boolean pdfChangeFlag {get;set;}		//PDFアップロードに変更フラグ
		public Boolean isDispBtn{get;set;}			//ボタン表示
		public Boolean webChangeFlag {get;set;}		//WEBで回答に変更フラグ
		public Boolean isDispLink{get;set;}			//リンク表示
		public String styleClassReupload{get;set;}	//再アップロードStyle
		public Boolean holdFlag {get;set;}			//保留フラグ
		public Boolean commonFlag {get;set;}		//保留共通フラグ
		public Boolean nnFlag {get;set;}			//保留nn追加フラグ
		public List<TrailAttachmentListRow> trailList {get;set;}

		//コンストラクタ
		public SubmitListRow(){
			submitType = '';
			att = null;
			//fileName = '';
			//fileId = '';
			modifiedName = '';
			modifiedDate = '';
			deleteFlag = false;
			downloadFlag = false;
			pdfdownloadFlag = false;
			uploadFlag = false;
			webLink = '';
			pdfUpLink = null;
			trailFlag = false;
			selfComplianceId = null;
			token = '';
			surveyId = '';
			btnLabel = '';
			linkLabel = '';
			pdfChangeFlag = false;
			webChangeFlag = false;
			isDispBtn = false;
			isDispLink = false;
			styleClassReupload = '';
			holdFlag = false;
			commonFlag = false;
			nnFlag = false;
			trailList = new List<TrailAttachmentListRow>();
		}
	}

	/**
	 * 提出書類　証跡資料一覧クラス
	 */
	public class TrailAttachmentListRow{
		//public String fileName {get;set;}			//ファイル名
		//public String fileId {get;set;}				//ファイルID
		//public String modifiedName {get;set;}		//更新者
		public String modifiedDate {get;set;}		//更新日時
		public Attachment att{get;set;}
		public Boolean deleteFlag {get;set;}		//削除ボタン表示フラグ
		public Boolean downloadFlag {get;set;}		//ダウンロードボタン表示フラグ

		//コンストラクタ
		public TrailAttachmentListRow(){
			//fileName = '';
			//fileId = '';
			//modifiedName = '';
			modifiedDate = '';
			att = null;
			deleteFlag = false;
			downloadFlag = false;
		}
	}

	/**
	 * 前年度、次年度のURLセット
	 */
	private void setOtherYearUrl(){
		if(office.FiscalYearNum__c == null){
			return;
		}
		
		// 前年度
		Decimal prevYear = office.FiscalYearNum__c - 1;
		// 次年度
		Decimal retYear = office.FiscalYearNum__c + 1;
		
		// 前年度、次年度の自主点検事務所取得
		Set<Decimal> years = new Set<Decimal>{prevYear, retYear};
		years.remove(null);
		for(SC_Office__c rec : [Select Id, FiscalYearNum__c From SC_Office__c Where AccZAGCYNUM__c = :office.AccZAGCYNUM__c And FiscalYearNum__c In :years]){
			// 2018年判定
			String scUrl = '';
			if(rec.FiscalYearNum__c < 2018){
				scUrl = Page.E_SCSelfCompliances.getUrl();
			}else{
				scUrl = Page.IRIS_SCSelfCompliances.getUrl();
			}
			
			// 前年度
			if(rec.FiscalYearNum__c == prevYear){
				prevSCURL = scUrl + '?id=' + rec.Id;
			
			// 次年度
			}else if(rec.FiscalYearNum__c == retYear){
				retSCURL = scUrl + '?id=' + rec.Id;
			}
		}
	}

	/**
	 * 履歴 種別表示用
	 */
	public Map<String, String> getTitles() {
		return titles;
	}

	/*
	 * 変更履歴取得
	 */
	public List<SC_WebAnswerHistory__c> getWebAnswerHistories() {
		return [select
				  id
				, ChangeDatetime__c
				, CreatedBy.Name
				, ChangedBy__c
				, ItemNumber__c
				, SC_WebAnswer__r.SC_SelfCompliance__r.Kind__c
				from SC_WebAnswerHistory__c 
				where SC_WebAnswer__r.SC_SelfCompliance__r.SC_Office__c = :officeid
				order by ChangeDatetime__c desc];
	}

	/**
	 * PDFダウンロードボタン押下
	 */
	public PageReference doDownloadPDF(){
		try{
			pageMessages.clearMessages();
			//共通点検表に紐づくWeb回答を取得
			SC_WebAnswer__c webAnswer = [SELECT id FROM SC_WebAnswer__c WHERE SC_SelfCompliance__c =: commonId ORDER BY CreatedDate desc limit 1];
			if(webAnswer.Id != null){
				// ダウンロード履歴Insert
				this.dh = new E_DownloadHistorry__c();
				this.dh.FormNo__c = E_Const.DH_FORMNO_SELFCOMPLIANCE;
				this.dh.RecordId__c = webAnswer.Id;
				this.dh.outputType__c = E_Const.DH_OUTPUTTYPE_PDF;
				// 文字切り捨て
				Database.DMLOptions dml = new Database.DMLOptions();
				dml.allowFieldTruncation = true;
				this.dh.setOptions(dml);
				insert this.dh;
			}

		}catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
		}

		return null;
	}

	//nextAction 後、リスト作成し直し
	public PageReference doNextActionAndRerender(){
		stManager.doNextAction();

		createSubmitList();

		return null;

	}

	//nextAction 後、リスト作成し直し
	public PageReference doRejectActionCmdAndRerender(){
		stManager.doRejectActionCmd();

		createSubmitList();

		return null;

	}

	//nextAction 後、リスト作成し直し
	public PageReference doRejectActionAndRerender(){
		stManager.doRejectAction();

		createSubmitList();

		return null;

	}

	public void createModalData(){
		modalAttId = System.currentPageReference().getParameters().get('modalAttId');
		modalAttName = System.currentPageReference().getParameters().get('modalAttName');
	}
}