public virtual class I_MenuItem{
	public I_MenuMaster__c record{get;set;}
	public Boolean rendered{get;set;}		//参照権限を持つメニューの場合True AccessControllerと比較して値を設定する。

	/*	Constructor	*/
	public I_MenuItem(){}
	public I_MenuItem(I_MenuMaster__c rec){
		this.record = rec;

		//メニュー表示設定
		E_AccessController access = E_AccessController.getInstance();

		if(access.isGuest() || access.idcpf == null){
			//Guestユーザはメニュー非表示でOK
			// ID管理が存在しない場合はメニュー非表示
			this.rendered = false;
		}else{
			//Guestユーザ以外は権限の検証を行う
			this.rendered = true;
			Set<String> required;

			//ID管理　フラグ項目検証
			if(String.isNotBlank(record.Required_EIDC_Flag__c)){
				this.rendered = false;		//フラグ検証を行うためFalseに設定
				required = new Set<String>(record.Required_EIDC_Flag__c.split(';'));
				for(String s:required){
					String sFlag = s.split(':')[0];	//:で分けた前半を取得
					if(String.valueOf(access.idcpf.get(sFlag)) == '1'){
						this.rendered = true;	//フラグが立っている場合にTrueに設定
						break;
					}
				}
			}

			//ID種別検証
			if(this.rendered){
				if(String.isNotBlank(record.Required_ZIDTYPE__c)){
					required = new Set<String>(record.Required_ZIDTYPE__c.split(';'));
					if(required.contains(access.idcpf.ZIDTYPE__c)==false){
						//ログインユーザのID種別が設定されていない場合は表示対象に追加しない
						this.rendered = false;
					}
				}
			}

			//権限セット検証
			if(this.rendered){
				if(String.isNotBlank(record.RequiredPermissionSet__c)){
					required = new Set<String>(record.RequiredPermissionSet__c.split(';'));
					if(access.hasPermission(required)==false){
						//指定の権限セットをどれも付与されていないときは表示対象に追加しない
						this.rendered = false;
					}
				}
			}

			//発送申込可能ユーザ検証
			if(this.rendered && String.isNotBlank(record.UpsertKey__c)){
				if(record.UpsertKey__c.contains('IRIS_DocumentRequest')){
					if(access.user.Contact.Account.CannotBeOrder__c){
						//資料発送申込ボタンは、チャネル='SK'かつソート='BK'のユーザには表示しない
						this.rendered = false;
					}
				}
			}
		}
	}

	//遷移先URLを返す
	public virtual String getDestUrl(){
		//表示権限がない場合はNullを返す
		if(this.rendered == false){
			return null;
		}

		String retUrl = record.DestUrl__c;
		if(record.requiredDefaultPageId__c){
			retUrl += '?' + I_Const.URL_PARAM_PAGE + '=';	//'?page=';
			if(record.pages__r.size()>0){
				retUrl += record.pages__r[0].Id;
			}
		}

		//メニューキーの追加
		retUrl += retUrl.contains('?')?'&':'?';		//?がない場合に?を追加する。ある場合は&を追加
		retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(record.Id);

		if(String.isNotBlank(record.urlParameters__c)){
			retUrl += retUrl.contains('?')?'&':'?';
			retUrl += record.urlParameters__c;
		}
		return retUrl;
	}
}