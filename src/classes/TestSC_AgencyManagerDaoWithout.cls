@isTest
private class TestSC_AgencyManagerDaoWithout {

	// 自主点検事務所
	private static SC_Office__c office;
	//
	private static Account accParent;
	//
	private static Account accAgency;
	// 
	private static List<SC_AgencyManager__c> agManagerList;

	/**
	 * List<SC_AgencyManager__c> getRecsByUserIds(Set<Id> userIds)
	 */
	@isTest static void getRecsByUserIds_test001() {
		// TestData
		createTestData();
		createAgManagerList(1);

		Set<Id> ids = new Set<Id>();
		ids.add(agManagerList[0].User__c);

		Test.startTest();
		List<SC_AgencyManager__c> result = SC_AgencyManagerDaoWithout.getRecsByUserIds(ids);
		Test.stopTest();

		System.assertEquals(1, result.size());
	}

	@isTest static void getRecsByContactIds_test002() {
		Test.startTest();
		List<SC_AgencyManager__c> result = SC_AgencyManagerDaoWithout.getRecsByUserIds(new Set<Id>());
		Test.stopTest();

		System.assertEquals(0, result.size());
	}

	/**
	 * TestData 自主点検事務所
	 */
	private static void createTestData(){
		/* 実行ユーザ作成 */
		// 1. 取引先（格）を作成
		accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'SCTestUser001';
		Contact con = TestSC_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestSC_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestSC_TestUtil.createIDCPF(true, actUser.Id, 'AH');

		/* テストデータ作成 */
		// 6. 自主点検事務所を更新
		office = TestSC_TestUtil.createSCOfficeByFiscalYear(true, accAgency.Id, null, '2018');
		// 事務所
		office.Account__c = accAgency.Id;
		// 拠点長
		User mrmngr = TestSC_TestUtil.createUser(true, 'sc_mrmngr', '拠点長');
		office.MRManager__c = mrmngr.Id;
		update office;

		office = [Select Id, FiscalYear__c, AccZAGCYNUM__c, AccZEAYNAM__c, OwnerMRUnitName__c
							, DomainInternet__c, DomainGw__c, DomainEmployee__c, MRManager__c
					From SC_Office__c
					Where Id = :office.Id];

		// 7. レコードを共有
		TestSC_TestUtil.createSCOfficeShare(office.Id, actUser.Id);
		TestSC_TestUtil.createAccountShare(accAgency.Id, actUser.Id);
		TestSC_TestUtil.createAccountShare(accParent.Id, actUser.Id);
	}

	/**
	 * TestData 自主点検業務管理責任者
	 */
	private static void createAgManagerList(Integer cnt){
		agManagerList = new List<SC_AgencyManager__c>();

		// 業務管理責任者（SC_AgencyManager__c）
		for(Integer i = 0; i < cnt; i++){
			// 取引先責任者を作成
			String actUserLastNameAm = 'SCTestUserAmr' + cnt;
			Contact conAm = TestSC_TestUtil.createContact(true, accAgency.Id, actUserLastNameAm);
			// ユーザを作成
			User actUserAm = TestSC_TestUtil.createAgentUser(true, actUserLastNameAm, 'E_PartnerCommunity', conAm.Id);
			// ID管理を作成
			E_IDCPF__c idMngAm = TestSC_TestUtil.createIDCPF(true, actUserAm.Id, 'AT');
			// 自主点検管理責任者
			SC_AgencyManager__c agManager = new SC_AgencyManager__c();
			agManager.SC_Office__c = office.Id;
			agManager.Contact__c = conAm.Id;
			agManager.User__c = actUserAm.Id;
			agManagerList.add(agManager);
		}
		insert agManagerList;
	}
}