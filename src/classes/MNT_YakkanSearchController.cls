global with sharing class MNT_YakkanSearchController extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public I_ContentMaster__c record{get;set;}
	public MNT_YakkanSearchExtender getExtender() {return (MNT_YakkanSearchExtender)extender;}
	public ResultList ResultList {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component26_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component26_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component9_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component9_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}
	public SkyEditor2.TextHolder Component8_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c agencyCodeText_val {get;set;}
	public SkyEditor2.TextHolder agencyCodeText_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component50_val {get;set;}
	public SkyEditor2.TextHolder Component50_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component29_val {get;set;}
	public SkyEditor2.TextHolder Component29_op{get;set;}
	public List<SelectOption> valueOptions_I_ContentMaster_c_GuideAgreeDiv_c {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component31_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component31_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component31_op{get;set;}
	public List<SelectOption> valueOptions_I_ContentMaster_c_InsType_c {get;set;}
	public String recordTypeRecordsJSON_I_ContentMaster_c {get; private set;}
	public String defaultRecordTypeId_I_ContentMaster_c {get; private set;}
	public String metadataJSON_I_ContentMaster_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public MNT_YakkanSearchController(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.AgencyCodeText__c;
		f = I_ContentMaster__c.fields.FormNoWoHyphen__c;
		f = I_ContentMaster__c.fields.GuideAgreeDiv__c;
		f = I_ContentMaster__c.fields.InsType__c;
		f = I_ContentMaster__c.fields.isSitePublishFlg__c;
		f = I_ContentMaster__c.fields.ContractDateFrom__c;
		f = I_ContentMaster__c.fields.ContractDateTo__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.AgencyCode__c;
 f = I_ContentMaster__c.fields.ContractDateFrom__c;
 f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.IsYakkanParentFlg__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = I_ContentMaster__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component26_from = new SkyEditor2__SkyEditorDummy__c();
			Component26_to = new SkyEditor2__SkyEditorDummy__c();
			Component9_from = new SkyEditor2__SkyEditorDummy__c();
			Component9_to = new SkyEditor2__SkyEditorDummy__c();
			Component8_val = new SkyEditor2__SkyEditorDummy__c();
			Component8_op = new SkyEditor2.TextHolder('co');
			agencyCodeText_val = new SkyEditor2__SkyEditorDummy__c();
			agencyCodeText_op = new SkyEditor2.TextHolder('co');
			Component50_val = new SkyEditor2__SkyEditorDummy__c();
			Component50_op = new SkyEditor2.TextHolder('co');
			Component29_val = new SkyEditor2__SkyEditorDummy__c();
			Component29_op = new SkyEditor2.TextHolder('eq');
			valueOptions_I_ContentMaster_c_GuideAgreeDiv_c = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : I_ContentMaster__c.GuideAgreeDiv__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_I_ContentMaster_c_GuideAgreeDiv_c.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component31_val = new SkyEditor2__SkyEditorDummy__c();
			Component31_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component31_op = new SkyEditor2.TextHolder('eq');
			valueOptions_I_ContentMaster_c_InsType_c = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : I_ContentMaster__c.InsType__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_I_ContentMaster_c_InsType_c.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			queryMap.put(
				'ResultList',
				new SkyEditor2.Query('I_ContentMaster__c')
					.addFieldAsOutput('isSitePublishFlg__c')
					.addFieldAsOutput('FormNo__c')
					.addFieldAsOutput('InsType__c')
					.addFieldAsOutput('ContractDateFrom__c')
					.addFieldAsOutput('ContractDateTo__c')
					.addFieldAsOutput('DisplayFrom__c')
					.addFieldAsOutput('ValidTo__c')
					.addFieldAsOutput('AgencyCode__c')
					.addFieldAsOutput('GuideAgreeDiv__c')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component26_from, 'SkyEditor2__Date__c', 'ContractDateFrom__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component26_to, 'SkyEditor2__Date__c', 'ContractDateFrom__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component9_from, 'SkyEditor2__Date__c', 'DisplayFrom__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component9_to, 'SkyEditor2__Date__c', 'DisplayFrom__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'FormNo__c', Component8_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(agencyCodeText_val, 'SkyEditor2__Text__c', 'AgencyCodeText__c', agencyCodeText_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component50_val, 'SkyEditor2__Text__c', 'FormNoWoHyphen__c', Component50_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component29_val, 'SkyEditor2__Text__c', 'GuideAgreeDiv__c', Component29_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component31_val_dummy, 'SkyEditor2__Text__c','InsType__c', Component31_op, true, 0, true ))
					.addWhere(' ( IsYakkanParentFlg__c = true)')
				);
			ResultList = new ResultList(new List<I_ContentMaster__c>(), new List<ResultListItem>(), new List<I_ContentMaster__c>(), null);
			ResultList.ignoredOnSave = true;
			listItemHolders.put('ResultList', ResultList);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_ContentMaster__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_YakkanSearchExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			ResultList.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_FormNo_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'FormNo__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_AgencyCodeText_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'AgencyCodeText__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_FormNoWoHyphen_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'FormNoWoHyphen__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_GuideAgreeDiv_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'GuideAgreeDiv__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_InsType_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'InsType__c');
	}
	global with sharing class ResultListItem extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		ResultListItem(ResultList holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class ResultList extends SkyEditor2.ListItemHolder {
		public List<ResultListItem> items{get; private set;}
		@TestVisible
			ResultList(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<ResultListItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new ResultListItem(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

}