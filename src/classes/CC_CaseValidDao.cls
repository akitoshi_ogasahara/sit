/*
 * CC_CaseValidDao
 * Data access class for  CC_CaseValid class
 * created  : Accenture 2018/10/22
 * modified :
 */

public with sharing class CC_CaseValidDao{

	/**
	 * Object: CC_SRActivity__c
	 * Parameter: Case Id set
	 * return: List<CC_SRActivity__c>
	 */
	public static List<CC_SRActivity__c> getSRActivityListByCaseIdList(Set<Id> caseIdSet){
		List<CC_SRActivity__c> srActivityList = [
						SELECT Id, CC_SRNo__c, CC_Status__c
						  FROM CC_SRActivity__c
						 WHERE CC_SRNo__c IN: caseIdSet
					  ORDER BY CC_SRNo__c];

		return srActivityList;
	}

	/**
	 * Object: Account
	 * Parameter: list of Id
	 * return: List<Account>
	 */
	public static List<Account> getAccountListByIdList(Set<Id> IdSet){
		List<Account> accountList = [
							SELECT Id, Name, CMN_IsClaimed__c, E_CL2PF_ZAGCYNUM__c, IsMotherOffice__c, E_IsAgency__c, ZubsMain__c,
									Parent.E_CL1PF_ZHEADAY__c
							  FROM Account
							 WHERE Id IN: IdSet];

		return accountList;
	}

	/**
	 * Object: Contact
	 * Parameter: E_CL3PF_AGNTNUM__c セット (募集人コードセット)
	 * return: List<Contact> (募集人リスト)
	 */
	public static List<Contact> getAgentListByAgentCodeSet(Set<String> agentCodeSet){
		List<Contact> contactList = [
							SELECT Id, AccountId, E_CL3PF_AGNTNUM__c, Account.E_CL2PF_ZAGCYNUM__c, Account.E_CL1PF_ZHEADAY__c, Account.E_IsAgency__c,
								   Account.ZubsMain__c, Account.Z1OFFING__c, Account.Parent.Z1OFFING__c, Account.Parent.E_CL1PF_ZHEADAY__c,
								   Account.Parent.E_CL2PF_ZAGCYNUM__c, Account.Parent.E_IsAgency__c, Account.Parent.ZubsMain__c
							  FROM Contact
							 WHERE RecordType.Name = '募集人'
							   AND E_CL3PF_AGNTNUM__c IN: agentCodeSet];

		return contactList;
	}

	/**
	 * Object: CC_SRTypeMaster__c
	 * Parameter: Id set
	 * return: Map<Id, CC_SRTypeMaster__c>
	 */
	public static Map<Id, CC_SRTypeMaster__c> getSRTypeMasterMapByIdList(Set<Id> srTypeIdSet){
		Map<Id, CC_SRTypeMaster__c> srTypeMasterMap = new Map<Id, CC_SRTypeMaster__c>([
							SELECT Id, Name, CC_SRTypeNameandSubject__c, CC_IsInformedClosedSRFlag__c
							  FROM CC_SRTypeMaster__c
							 WHERE Id IN: srTypeIdSet]);

		return srTypeMasterMap;
	}

	/**
	 * Object: CC_FormMaster__c
	 * Parameter: CC_SRTypeNo__c セット
	 * return: Map<Id, CC_FormMaster__c>
	 */
	public static List<CC_FormMaster__c> getFormMasterListBySRTypeSet(Set<String> srTypeNameSet){
		List<CC_FormMaster__c> formMasterList = new List<CC_FormMaster__c>([
							SELECT Id, CC_FormName__c, CC_SRTypeNo__c, CC_FormType__c
							  FROM CC_FormMaster__c
							 WHERE CC_SRTypeNo__c IN: srTypeNameSet]);

		return formMasterList;
	}

}