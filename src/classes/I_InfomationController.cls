public with sharing class I_InfomationController extends I_CMSController{

	//取得リスト行数
//	public Integer listRows {get; private set;}
	public Integer getlistRows(){
		if(this.CMSSections.size()==0 || this.CMSSections[0].cmsComps==null){return 0;}
		return this.CMSSections[0].cmsComps.size();
	}

	// リスト行数
	public Integer rowCount {get; set;}
	//最大表示件数
	public Integer listMaxRows{get; set;}


	public I_InfomationController(){
		//デフォルト行数定義
		rowCount = 10;

		//最大表示件数1000件
		listMaxRows = I_Const.LIST_MAX_ROWS;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		//現在の表示行数+20
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		//現在の表示行数が1000より大きくなった場合、表示行数は1000
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	//コンテンツ取得
	protected virtual override List<I_ContentMaster__c> getContentMasterRecords(){
		String activeRecId = getActiveRecordId();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getRecordsInfoContent(activeRecId);

		if(recs.size() == 0) {
			return new List<I_ContentMaster__c>();
		}

		//20170705 NN_IRIS-495対応 START
		for(I_ContentMaster__c child : recs.get(0).ChildContents__r){
			if(!String.isBlank(child.tubeIds__c)){
				//お知らせ動画Idに値が入っていた場合
				Set<String> tubeIds = new Set<String>(child.tubeIds__c.split(','));
				//動画URLを生成する
				List<String> tubeURLs = new List<String>();
				I_TubeConnectHelper helper = new I_TubeConnectHelper();
				for(String tubeId : tubeIds){
					tubeURLs.add(helper.getTubeTopUrl() +'?'+ helper.getParams_MovieType() +'&mid='+tubeId);
				}

				//固定文字列を動画URLに置き換える
				for(Integer i=1; i <= tubeURLs.size(); i++){
					if(child.Content__c.contains('douga'+i)) child.Content__c = child.Content__c.replace('douga'+i, tubeURLs[i-1]);
				}
			}
		}
		//20170705 NN_IRIS-495対応 END

		//system.assert(recs.size()==1, 'お知らせセクションが複数発見されました。');

		//I_ContentMaster__c rec = recs.get(0);

		//子コンテンツをlistLowsに
		//listRows = rec.ChildContents__r.size();
		return recs;
	}

	//現在、特定されているレコードId　設定されている場合
	public String getActiveRecordId(){
		return ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_INFOID);
	}

	//メニューIdを設定
	protected virtual override Id getIrisMenuId(){
		I_MenuMaster__c menu = I_MenuMasterDao.getRecByName(I_Const.IRIS_MENU_NAME_INFOMATION);
		if(menu != null){
			return menu.id;
		}
		return null;
	}
}