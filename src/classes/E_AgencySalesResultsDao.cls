public with sharing class E_AgencySalesResultsDao {

	/**
	 * 代理店挙積情報IDをキーに、指定した代理店挙積情報を取得する。
	 * 使用箇所
	 *  I_AgencySRInfoController
	 * @param asrId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecTypeAById(Id asrId) {
		List<E_AgencySalesResults__c> recs = [
				Select
					Id											// カスタムオブジェクト ID
					,YYYYMM__c									// 年月
					,NBCANP_YTD__c								// 新契約係数ANP（当年YTD）
					,NBCANP_LYYTD__c							// 新契約係数ANP（前年YTD）
					,NBCANP_SpInsTypeYTD__c						// 新契約係数ANP 特定保険種類（当年YTD）
					,NBCANP_SpInsTypeLYYTD__c					// 新契約係数ANP 特定保険種類（前年YTD）
					,IANP__c									// 保有ANP（当年）
					,IANP_LY__c									// 保有ANP（前年）
					,IQA__c										// IQA継続率（当年）
					,IQA_LY__c									// IQA継続率（前年）
					,MOF13__c									// MOF13継続率（当年）
					,MOF13_LY__c								// MOF13継続率（前年）
					,MOF25__c									// MOF25継続率（当年）
					,MOF25_LY__c								// MOF25継続率（前年）
					,DefactRate__c								// 単純不備率（当年YTD）
					,DefactRate_LY__c							// 単純不備率（前年YTD）
					,XHAH_AGCLS__c								// 現在資格
					,QualifDemoSuspension__c					// 降格猶予（現在資格）
					,QualifSim__c								// 次回予測資格
					,QualifSimDemoSuspension__c					// 降格猶予（次回予測資格）
					,QualifNextYYYYMM__c						// 次回資格査定年月
					,QualifSimPeriodNBCANP__c					// 査定対象期間　新契約係数ANP
					,QualifSimNBCANP__c							// 資格予測　新契約係数ANP
					,QualifSimPeriodNBCANP_SpInsType__c			// 査定対象期間　新契約係数ANP　特定保険種類
					,QualifSimNBCANPSpInsType__c				// 資格予測　新契約係数ANP 特定保険種類
					,QualifSimMonthEndIANP__c					// 査定対象期間　保有ANP
					,QualifSimIANP__c							// 資格予測　保有ANP
					,QualifSimPeriodActiveMonth__c				// 査定対象期間　稼働月数
					,QualifSimActiveMonth__c					// 資格予測　稼動月数
					,QualifSimMonthEndIQA__c					// 査定対象期間　IQA継続率
					,QualifSimIQA__c							// 資格予測　IQA継続率
					,QualifSimPeriodNBCANP_Formula__c			//.査定対象期間　新契約係数ANP
					,QualifSimPeriodNBCANP_SpInsType_Formula__c	//.査定対象期間　新契約係数ANP　特定保険種類
					,QualifSimMonthEndIANP_Formula__c			//.査定対象期間　保有ANP
					,QualifSimPeriodActiveMonth_Formula__c		//.査定対象期間　稼働月数
					,QualifSimMonthEndIQA_Formula__c			//.査定対象期間　IQA継続率
					, BusinessDate_10th__c						// 第10営業日
					, BusinessDate_10th_Formula__c				// 第10営業日（数式）
					, BusinessDate_11th__c						// 第11営業日
					, BusinessDate_11th_Formula__c				// 第11営業日（数式）
					, QualifNextYYYYMM_Formula__c 				// 次回資格査定年月（数式）
					, ParentAccountNm__c 						// 代理店格名
					, AccountNm__c 								// 事務所名
					, AgentNm__c 								// 募集人名
					, IQA_NNAverage__c 							// IQA継続率（当社平均）
					, MOF13_NNAverage__c 						// MOF13継続率（当社平均）
					, MOF25_NNAverage__c 						// MOF25継続率（当社平均）
					, DefactRate_NNAverage__c 					// 単純不備率（当社平均）
					, QualifSim_Formula__c 						// 次回予測資格（数式）
				From
					E_AgencySalesResults__c
				Where
					Id = :asrId
			   ];
		return (recs.size() > 0) ? recs[0] : new E_AgencySalesResults__c();
	}

	/**
	 * 代理店挙積情報IDをキーに、指定した代理店挙積情報を取得する。
	 * 使用箇所
	 *  I_LicenseAssessmentController
	 * @param asrId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecTypeBById(Id asrId) {
		List<E_AgencySalesResults__c> recs = [
				Select
					Id					// カスタムオブジェクト ID
					,XHAH_AGCLS__c		// 現在資格
				From
					E_AgencySalesResults__c
				Where
					Id = :asrId
			   ];
		return (recs.size() > 0) ? recs[0] : new E_AgencySalesResults__c();
	}

	/**
	 * 代理店挙積情報IDをキーに、指定した代理店挙積情報を取得する。
	 * 使用箇所
	 *  I_ChartNBCANPController
	 * @param asrId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecTypeC1ById(Id asrId) {
		List<E_AgencySalesResults__c> recs = [
				Select
					Id										// カスタムオブジェクト ID
					,YYYYMM__c								// 年月
					,NBCANP_Jan__c							// 新契約係数ANP　当年1月
					,NBCANP_Feb__c							// 新契約係数ANP　当年2月
					,NBCANP_Mar__c							// 新契約係数ANP　当年3月
					,NBCANP_Apr__c							// 新契約係数ANP　当年4月
					,NBCANP_May__c							// 新契約係数ANP　当年5月
					,NBCANP_Jun__c							// 新契約係数ANP　当年6月
					,NBCANP_Jul__c							// 新契約係数ANP　当年7月
					,NBCANP_Aug__c							// 新契約係数ANP　当年8月
					,NBCANP_Sep__c							// 新契約係数ANP　当年9月
					,NBCANP_Oct__c							// 新契約係数ANP　当年10月
					,NBCANP_Nov__c							// 新契約係数ANP　当年11月
					,NBCANP_Dec__c							// 新契約係数ANP　当年12月
					,NBCANP_LYJan__c						// 新契約係数ANP　前年1月
					,NBCANP_LYFeb__c						// 新契約係数ANP　前年2月
					,NBCANP_LYMar__c						// 新契約係数ANP　前年3月
					,NBCANP_LYApr__c						// 新契約係数ANP　前年4月
					,NBCANP_LYMay__c						// 新契約係数ANP　前年5月
					,NBCANP_LYJun__c						// 新契約係数ANP　前年6月
					,NBCANP_LYJul__c						// 新契約係数ANP　前年7月
					,NBCANP_LYAug__c						// 新契約係数ANP　前年8月
					,NBCANP_LYSep__c						// 新契約係数ANP　前年9月
					,NBCANP_LYOct__c						// 新契約係数ANP　前年10月
					,NBCANP_LYNov__c						// 新契約係数ANP　前年11月
					,NBCANP_LYDec__c						// 新契約係数ANP　前年12月
					,NBCANP_B2YJan__c						// 新契約係数ANP　2年前1月
					,NBCANP_B2YFeb__c						// 新契約係数ANP　2年前2月
					,NBCANP_B2YMar__c						// 新契約係数ANP　2年前3月
					,NBCANP_B2YApr__c						// 新契約係数ANP　2年前4月
					,NBCANP_B2YJun__c						// 新契約係数ANP　2年前6月
					,NBCANP_B2YMay__c						// 新契約係数ANP　2年前5月
					,NBCANP_B2YJul__c						// 新契約係数ANP　2年前7月
					,NBCANP_B2YAug__c						// 新契約係数ANP　2年前8月
					,NBCANP_B2YSep__c						// 新契約係数ANP　2年前9月
					,NBCANP_B2YOct__c						// 新契約係数ANP　2年前10月
					,NBCANP_B2YNov__c						// 新契約係数ANP　2年前11月
					,NBCANP_B2YDec__c						// 新契約係数ANP　2年前12月
					,NBCANP_1Q__c							// 新契約係数ANP　当年1Q
					,NBCANP_2Q__c							// 新契約係数ANP　当年2Q
					,NBCANP_3Q__c							// 新契約係数ANP　当年3Q
					,NBCANP_4Q__c							// 新契約係数ANP　当年4Q
					,NBCANP_LY1Q__c							// 新契約係数ANP　前年1Q
					,NBCANP_LY2Q__c							// 新契約係数ANP　前年2Q
					,NBCANP_LY3Q__c							// 新契約係数ANP　前年3Q
					,NBCANP_LY4Q__c							// 新契約係数ANP　前年4Q
					,NBCANP_B2Y1Q__c						// 新契約係数ANP　2年前1Q
					,NBCANP_B2Y2Q__c						// 新契約係数ANP　2年前2Q
					,NBCANP_B2Y3Q__c						// 新契約係数ANP　2年前3Q
					,NBCANP_B2Y4Q__c						// 新契約係数ANP　2年前4Q
					,NBCANP_Progress__c						// 新契約係数ANP　当年対前年進捗率
					,NBCANP_ProgressLY__c					// 新契約係数ANP　前年対前年進捗率
					,NBCANP_YTD__c							// 新契約係数ANP（当年YTD）
					,NBCANP_LYYTD__c						// 新契約係数ANP（前年YTD）
				From
					E_AgencySalesResults__c
				Where
					Id = :asrId
			   ];
		return (recs.size() > 0) ? recs[0] : new E_AgencySalesResults__c();
	}
	/**
	 * 代理店挙積情報IDをキーに、指定した代理店挙積情報を取得する。
	 * 使用箇所
	 *  I_ChartNBCANPController
	 * @param asrId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecTypeC2ById(Id asrId) {
		List<E_AgencySalesResults__c> recs = [
				Select
					Id										// カスタムオブジェクト ID
					,YYYYMM__c								// 年月
					,NBCANP_SpInsTypeJan__c					// 新契約係数ANP　特定保険種類　当年1月
					,NBCANP_SpInsTypeFeb__c					// 新契約係数ANP　特定保険種類　当年2月
					,NBCANP_SpInsTypeMar__c					// 新契約係数ANP　特定保険種類　当年3月
					,NBCANP_SpInsTypeApr__c					// 新契約係数ANP　特定保険種類　当年4月
					,NBCANP_SpInsTypeMay__c					// 新契約係数ANP　特定保険種類　当年5月
					,NBCANP_SpInsTypeJun__c					// 新契約係数ANP　特定保険種類　当年6月
					,NBCANP_SpInsTypeJul__c					// 新契約係数ANP　特定保険種類　当年7月
					,NBCANP_SpInsTypeAug__c					// 新契約係数ANP　特定保険種類　当年8月
					,NBCANP_SpInsTypeSep__c					// 新契約係数ANP　特定保険種類　当年9月
					,NBCANP_SpInsTypeOct__c					// 新契約係数ANP　特定保険種類　当年10月
					,NBCANP_SpInsTypeNov__c					// 新契約係数ANP　特定保険種類　当年11月
					,NBCANP_SpInsTypeDec__c					// 新契約係数ANP　特定保険種類　当年12月
					,NBCANP_SpInsTypeLYJan__c				// 新契約係数ANP　特定保険種類　前年1月
					,NBCANP_SpInsTypeLYFeb__c				// 新契約係数ANP　特定保険種類　前年2月
					,NBCANP_SpInsTypeLYMar__c				// 新契約係数ANP　特定保険種類　前年3月
					,NBCANP_SpInsTypeLYApr__c				// 新契約係数ANP　特定保険種類　前年4月
					,NBCANP_SpInsTypeLYMay__c				// 新契約係数ANP　特定保険種類　前年5月
					,NBCANP_SpInsTypeLYJun__c				// 新契約係数ANP　特定保険種類　前年6月
					,NBCANP_SpInsTypeLYJul__c				// 新契約係数ANP　特定保険種類　前年7月
					,NBCANP_SpInsTypeLYAug__c				// 新契約係数ANP　特定保険種類　前年8月
					,NBCANP_SpInsTypeLYSep__c				// 新契約係数ANP　特定保険種類　前年9月
					,NBCANP_SpInsTypeLYOct__c				// 新契約係数ANP　特定保険種類　前年10月
					,NBCANP_SpInsTypeLYNov__c				// 新契約係数ANP　特定保険種類　前年11月
					,NBCANP_SpInsTypeLYDec__c				// 新契約係数ANP　特定保険種類　前年12月
					,NBCANP_SpInsTypeB2YJan__c				// 新契約係数ANP　特定保険種類　2年前1月
					,NBCANP_SpInsTypeB2YFeb__c				// 新契約係数ANP　特定保険種類　2年前2月
					,NBCANP_SpInsTypeB2YMar__c				// 新契約係数ANP　特定保険種類　2年前3月
					,NBCANP_SpInsTypeB2YApr__c				// 新契約係数ANP　特定保険種類　2年前4月
					,NBCANP_SpInsTypeB2YMay__c				// 新契約係数ANP　特定保険種類　2年前5月
					,NBCANP_SpInsTypeB2YJun__c				// 新契約係数ANP　特定保険種類　2年前6月
					,NBCANP_SpInsTypeB2YJul__c				// 新契約係数ANP　特定保険種類　2年前7月
					,NBCANP_SpInsTypeB2YAug__c				// 新契約係数ANP　特定保険種類　2年前8月
					,NBCANP_SpInsTypeB2YSep__c				// 新契約係数ANP　特定保険種類　2年前9月
					,NBCANP_SpInsTypeB2YOct__c				// 新契約係数ANP　特定保険種類　2年前10月
					,NBCANP_SpInsTypeB2YNov__c				// 新契約係数ANP　特定保険種類　2年前11月
					,NBCANP_SpInsTypeB2YDec__c				// 新契約係数ANP　特定保険種類　2年前12月
					,NBCANP_SpInsType1Q__c					// 新契約係数ANP　特定保険種類　当年1Q
					,NBCANP_SpInsType2Q__c					// 新契約係数ANP　特定保険種類　当年2Q
					,NBCANP_SpInsType3Q__c					// 新契約係数ANP　特定保険種類　当年3Q
					,NBCANP_SpInsType4Q__c					// 新契約係数ANP　特定保険種類　当年4Q
					,NBCANP_SpInsTypeLY1Q__c				// 新契約係数ANP　特定保険種類　前年1Q
					,NBCANP_SpInsTypeLY2Q__c				// 新契約係数ANP　特定保険種類　前年2Q
					,NBCANP_SpInsTypeLY3Q__c				// 新契約係数ANP　特定保険種類　前年3Q
					,NBCANP_SpInsTypeLY4Q__c				// 新契約係数ANP　特定保険種類　前年4Q
					,NBCANP_SpInsTypeB2Y1Q__c				// 新契約係数ANP　特定保険種類　2年前1Q
					,NBCANP_SpInsTypeB2Y2Q__c				// 新契約係数ANP　特定保険種類　2年前2Q
					,NBCANP_SpInsTypeB2Y3Q__c				// 新契約係数ANP　特定保険種類　2年前3Q
					,NBCANP_SpInsTypeB2Y4Q__c				// 新契約係数ANP　特定保険種類　2年前4Q
					,NBCANP_SpInsTypeProgress__c			// 新契約係数ANP　特定保険種類　当年対前年進捗率
					,NBCANP_SpInsTypeLY__c					// 新契約係数ANP　特定保険種類　前年対前年進捗率
					,NBCANP_SpInsTypeYTD__c					// 新契約係数ANP 特定保険種類（当年YTD）
					,NBCANP_SpInsTypeLYYTD__c				// 新契約係数ANP 特定保険種類（前年YTD）
				From
					E_AgencySalesResults__c
				Where
					Id = :asrId
			   ];
		return (recs.size() > 0) ? recs[0] : new E_AgencySalesResults__c();
	}

	/**
	 * 代理店挙積情報IDをキーに、指定した代理店挙積情報を取得する。
	 * 使用箇所
	 *  I_ChartPaymentController
	 * @param asrId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecTypeDById(Id asrId) {
		List<E_AgencySalesResults__c> recs = [
				Select
					Id										// カスタムオブジェクト ID
					,YYYYMM__c								// 年月
					,ClawBack__c							// 戻入未収金残高
					,LastModifiedDate						// 最終更新日
					,Payment_Jan__c							// 振込手数料額　当年1月
					,Payment_Feb__c							// 振込手数料額　当年2月
					,Payment_Mar__c							// 振込手数料額　当年3月
					,Payment_Apr__c							// 振込手数料額　当年4月
					,Payment_May__c							// 振込手数料額　当年5月
					,Payment_Jun__c							// 振込手数料額　当年6月
					,Payment_Jul__c							// 振込手数料額　当年7月
					,Payment_Aug__c							// 振込手数料額　当年8月
					,Payment_Sep__c							// 振込手数料額　当年9月
					,Payment_Oct__c							// 振込手数料額　当年10月
					,Payment_Nov__c							// 振込手数料額　当年11月
					,Payment_Dec__c							// 振込手数料額　当年12月
					,FY_Jan__c								// 初年度手数料額　当年1月
					,FY_Feb__c								// 初年度手数料額　当年2月
					,FY_Mar__c								// 初年度手数料額　当年3月
					,FY_Apr__c								// 初年度手数料額　当年4月
					,FY_May__c								// 初年度手数料額　当年5月
					,FY_Jun__c								// 初年度手数料額　当年6月
					,FY_Jul__c								// 初年度手数料額　当年7月
					,FY_Aug__c								// 初年度手数料額　当年8月
					,FY_Sep__c								// 初年度手数料額　当年9月
					,FY_Oct__c								// 初年度手数料額　当年10月
					,FY_Nov__c								// 初年度手数料額　当年11月
					,FY_Dec__c								// 初年度手数料額　当年12月
					,ASY_Jan__c								// 次年度以降手数料額　当年1月
					,ASY_Feb__c								// 次年度以降手数料額　当年2月
					,ASY_Mar__c								// 次年度以降手数料額　当年3月
					,ASY_Apr__c								// 次年度以降手数料額　当年4月
					,ASY_May__c								// 次年度以降手数料額　当年5月
					,ASY_Jun__c								// 次年度以降手数料額　当年6月
					,ASY_Jul__c								// 次年度以降手数料額　当年7月
					,ASY_Aug__c								// 次年度以降手数料額　当年8月
					,ASY_Sep__c								// 次年度以降手数料額　当年9月
					,ASY_Oct__c								// 次年度以降手数料額　当年10月
					,ASY_Nov__c								// 次年度以降手数料額　当年11月
					,ASY_Dec__c								// 次年度以降手数料額　当年12月
					,OT_Jan__c								// その他手数料額　当年1月
					,OT_Feb__c								// その他手数料額　当年2月
					,OT_Mar__c								// その他手数料額　当年3月
					,OT_Apr__c								// その他手数料額　当年4月
					,OT_May__c								// その他手数料額　当年5月
					,OT_Jun__c								// その他手数料額　当年6月
					,OT_Jul__c								// その他手数料額　当年7月
					,OT_Aug__c								// その他手数料額　当年8月
					,OT_Sep__c								// その他手数料額　当年9月
					,OT_Oct__c								// その他手数料額　当年10月
					,OT_Nov__c								// その他手数料額　当年11月
					,OT_Dec__c								// その他手数料額　当年12月
					,CB_Jan__c								// 戻入金額　当年1月
					,CB_Feb__c								// 戻入金額　当年2月
					,CB_Mar__c								// 戻入金額　当年3月
					,CB_Apr__c								// 戻入金額　当年4月
					,CB_May__c								// 戻入金額　当年5月
					,CB_Jun__c								// 戻入金額　当年6月
					,CB_Jul__c								// 戻入金額　当年7月
					,CB_Aug__c								// 戻入金額　当年8月
					,CB_Sep__c								// 戻入金額　当年9月
					,CB_Oct__c								// 戻入金額　当年10月
					,CB_Nov__c								// 戻入金額　当年11月
					,CB_Dec__c								// 戻入金額　当年12月
				From
					E_AgencySalesResults__c
				Where
					Id = :asrId
			   ];
		return (recs.size() > 0) ? recs[0] : new E_AgencySalesResults__c();
	}

	/**
	 * 代理店挙積情報IDをキーに、指定した代理店挙積情報を取得する。
	 * 使用箇所
	 *  I_ChartNBCANPController
	 * @param asrId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecTypeEById(Id asrId) {
		List<E_AgencySalesResults__c> recs = [
				Select
					Id										// カスタムオブジェクト ID
					,CLTPHONE01__c							// 事務所電話番号
					,AccountCode__c							// 事務所コード
					,Address__c								// 本店所在地
					,AGED_CAP__c							// 専門資格(合格年月/合格番号)
					,AgencyName__c							// 代理店名
					,AgentCode__c							// 募集人コード
					,AgentName__c							// 募集人名
					,AgentType__c							// 登録区分
					,BelongOfficeName__c					// 所属事務所名
					,BusinessDate__c						// 営業日
					,CnvsLicense__c							// 当社商品の募集資格
					,CnvsLicenseVariableIns__c				// 当社での変額資格
					,CnvsStartDate__c						// 当社募集開始日
					,MotherAdress__c						// 母店　所在地
					,MotherOfficeName__c					// 母店　事務所名
					,MRNm__c								// MR名
					,OfficeAddress__c						// 事務所　所在地
					,OfficeName__c							// 事務所名
					,ParentAccount__c						// 代理店格
					,ParentAccountCode__c					// 代理店格コード
					,Proxy__c								// 代申／非代申
					,Segment__c								// セグメント
					,UnitNm__c								// 営業部名
					,XHAH_AGCLS__c							// 現在資格
					,XHAH_KCHANNEL__c						// チャネル
					,XHAH_KMOFCODE__c						// 代理店登録番号
					,XHAH_ZBUSAGCY__c						// 母店　事務所コード
					,XHAT_KMOFCODE__c						// 募集人登録番号
					,XHAY_AGNTBR__c							// 事務所　担当営業部
					,XHAY_ZMRCODE__c						// 事務所　担当MR
					,ComStartDate__c						// 当社募集開始日
					,MotherBranchName__c					// 母店　担当営業部
					,MotherMR__c							// 母店　担当MR
				From
					E_AgencySalesResults__c
				Where
					Id = :asrId
			   ];
		return (recs.size() > 0) ? recs[0] : new E_AgencySalesResults__c();
	}

	/**
	 * 代理店格・代理店事務所・募集人のIDをキーに、代理店挙績情報のIdを取得する
	 * 使用箇所
	 *  I_AgencySalesResultsController
	 * @param soqlWhere: 代理店格・代理店事務所・募集人ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static E_AgencySalesResults__c getRecBysWhere(String soqlWhere) {
		String soql = ' SELECT Id'
					+ ', ParentAccount__r.Owner.Unit__c '
					+ ', ParentAccount__r.Owner.Name '
					+ ', UnitNm__c '
					+ ', MRNm__c '
					+ ', ParentAccountNm__c '
					+ ', AccountNm__c '
					+ ', AgentNm__c '
					+ ', XHAH_AGCLS__c '
					+ ',EvaCancellConsigContract__c '
					+ ',BusDate__c '
					+ ',ParentAccountId__c '
					+ ',AccountId__c '
					+ ',QualifSim__c'
					+ ', BusinessDate_10th__c'						// 第10営業日
					+ ', BusinessDate_10th_Formula__c'				// 第10営業日（数式）
					+ ', BusinessDate_11th__c'						// 第11営業日
					+ ', BusinessDate_11th_Formula__c'				// 第11営業日（数式）
					+ ', QualifSim_Formula__c'						// 次回予測資格（数式）
					+ ',(SELECT id FROM E_ProductBreakdowns__r LIMIT 1)'
					+ ' FROM E_AgencySalesResults__c '
					+ 'WHERE '
					+ soqlWhere
					+ ' ORDER BY '
					+ ' CreatedDate desc '
					+ ' LIMIT 1 ';

		System.debug('[SOQL]' + soql);
		List<E_AgencySalesResults__c> recs = Database.query(soql);
		return recs.size() == 0 ? null : recs[0];
	}

	/**
	 * 代理店格のSFIDに紐づく代理店挙績情報の募集人を取得する。
	 * 使用箇所
	 *  I_SRAgentController
	 * @param agencyId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static List<E_AgencySalesResults__c> getSRAgentRecs(String soqlWhere) {
		return getSRAgentRecs(soqlWhere,false);
	}
	public static List<E_AgencySalesResults__c> getSRAgentRecs(String soqlWhere,Boolean isCsv) {
		String soql = ' SELECT '
					+ 'AgentCode__c '
					+ ',XHAT_KMOFCODE__c '
					+ ',AgentNm__c '
					+ ',AgentType__c '
					+ ',BelongOfficeName__c '
					+ ',CnvsLicense_IRIS__c '
					+ ',CnvsStartDate__c '
					+ ',ComStartDate__c '
					+ ',CnvsLicenseVariableIns__c '
					+ ',AGED_KPCDT__c '
					+ ',AGED_KPCDTNum__c '
					+ ',AGED_KPCLC__c '
					+ ',AGED_CAP__c '
					+ ',BusDate__c '
					+ ',ParentAccountId__c '
					+ 'FROM E_AgencySalesResults__c '
					+ 'WHERE '
					+ soqlWhere
					+ ' ORDER BY '
					+ ' XHAT_AGTYPE__c '
					+ ' ,AccountCode__c '
					+ ' ,ComStartDate__c NULLS LAST ';

		if(!isCsv){
			soql += ' LIMIT 1000 ';
		}

		return Database.query(soql);
	}

	/**
	 * 指定された階層と営業日の代理店挙積情報リストを取得する。
	 * 使用箇所
	 *  E_BizDataSyncBatchOperation_SalesPlan
	 * @param String hrchy: 階層
	 * @param String bizDate: 営業日
	 * @return List<E_AgencySalesResults__c>: 代理店挙積情報リスト
	 */
	public static List<E_AgencySalesResults__c> getRecordsByHierarchyAndBizDate(String hrchy, String bizDate) {
		return [SELECT
					Id
					,ParentAccount__c
				 From
					E_AgencySalesResults__c
				 Where
					Hierarchy__c = :hrchy
				 And
					BusinessDate__c = :bizDate
				 ORDER BY UpsertKey__c];
	}

}