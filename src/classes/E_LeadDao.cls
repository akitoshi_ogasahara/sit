/**
 * Leadデータアクセスオブジェクト
 * CreatedDate 2016/08/10
 * @Author Terrasky
 */
 public with sharing class E_LeadDao {
	
	/*
	 * @param email 対象メールアドレス
	 * @return 該当数
	 */
	public static Integer getRecCountByEmail(String email){
		return [select count() from Lead where Email =: email];
	}
	/*
	 * @param cid 対象キャンペーンターゲットID
	 * @return 関連しているLead数
	 *
	public static Integer getRecCountByCmpgnTrgt(ID cid){
		return [select count() from Lead where MK_CampaignTarget__c = : cid];
	}*/
}