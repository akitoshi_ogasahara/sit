public with sharing class E_SiteErrorPage extends E_AbstractUnitPriceController{

    //エラーコード
    public String errorMsgCode;

    //ページタイトル
    private static final String PAGE_TITLE = 'エラーページ';
    private static final String DEFAULT_ERROR_MSG = 'エラーが発生しました。';

    //ページ名
    public String pageName;

    public E_SiteErrorPage () {
        //Sitesエラーページ一覧
        final String SITES_ERROR_PAGE_E_UNAUTHORIZED = getPageName(Page.E_Unauthorized);
        final String SITES_ERROR_PAGE_E_BANDWIDTHEXCEEDED = getPageName(Page.E_BandwidthExceeded);
        final String SITES_ERROR_PAGE_E_INMAINTENANCE = getPageName(Page.E_InMaintenance);
        final String SITES_ERROR_PAGE_E_FILENOTFOUND = getPageName(Page.E_FileNotFound);
        final String SITES_ERROR_PAGE_E_EXCEPTION = getPageName(Page.E_Exception);
        final String PAGE_NULL = 'System.PageReference[null]';
        this.pgtitle = PAGE_TITLE;
        //ページ名取得
        if(!String.valueof(ApexPages.currentPage()).equals(PAGE_NULL)){
         	pageName =  ApexPages.currentPage().getUrl().toUpperCase();
        }
         System.debug('@取得ページ名@'+pageName);
         System.debug('@該当の名前@'+ getPageName(Page.E_Unauthorized));
         //エラーコードセット
         if(String.valueof(ApexPages.currentPage()).equals(PAGE_NULL)){
         	errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_999;
         }else if(pageName.indexof(SITES_ERROR_PAGE_E_UNAUTHORIZED) != -1){
	            errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_401;
	      }else if(pageName.indexof(SITES_ERROR_PAGE_E_BANDWIDTHEXCEEDED) != -1){
	            errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_509;
	       }else if(pageName.indexof(SITES_ERROR_PAGE_E_INMAINTENANCE) != -1){
	            errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_500_503;
	       }else if(pageName.indexof(SITES_ERROR_PAGE_E_FILENOTFOUND) != -1){
	            errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_404;
	       }else if(pageName.indexof(SITES_ERROR_PAGE_E_EXCEPTION) != -1){
	            errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_999;
	       }else{
	         	errorMsgCode = E_Const.SITES_ERROR_PAGE_CODE_999;
	        }
         getPageMessages().addErrorMessage(getMSG().get(errorMsgCode ));
    }

    //ページ名取得
    public String getPageName(PageReference prf) {
        String refPg;
        refPg = String.valueof(prf).replace('/apex/','');
        refPg = refPg.replace('System.PageReference[','');
        refPg = refPg.replace(']','');
        refPg = refPg.toUpperCase();
        return refPg;
    }

}