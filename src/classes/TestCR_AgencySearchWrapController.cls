@isTest
private class TestCR_AgencySearchWrapController {
    
    // コンストラクタ
    private static testMethod void AgencySearchTest001(){
        
        CR_AgencySearchWrapController target = new CR_AgencySearchWrapController();
            System.assertEquals(target.getPageTitle(), '代理店検索（規程）');        
    }
    
    // 社員ユーザの場合
    private static testMethod void AgencySearchTest002(){
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		System.runAs(usr){
			Test.startTest();
			PageReference pageRef = Page.E_CRAgencySearch;
			Test.setCurrentPage(pageRef);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchWrapController target = new CR_AgencySearchWrapController();
						
			// 実行結果確認
			System.assertEquals(target.pageAction(), null);
						
			Test.stopTest();			
		}
	}
	
	// 代理店ユーザの場合
    private static testMethod void AgencySearchTest003(){
    	Account acc = TestCR_TestUtil.createAccount(true);
   		Contact con = TestCR_TestUtil.createContact(true, acc.Id, 'TestUser002');
    	// 代理店ユーザ作成
		User usr = TestCR_TestUtil.createAgentUser(true, 'TestUser002', 'E_PartnerCommunity', con.Id);
		
		System.runAs(usr){
			TestCR_TestUtil.createAgencyPermissions(usr.Id);
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_RULE);
			
			Test.startTest();
			PageReference pageRef = Page.E_CRAgencySearch;
			Test.setCurrentPage(pageRef);
			
			// 拡張クラスを生成
			CR_AgencySearchWrapController target = new CR_AgencySearchWrapController();
			target.viewingAgencyCode = agny.AgencyCode__c;
			
			// 実行結果確認
			System.assertEquals(target.pageAction().getUrl(),'/apex/e_crrules?id=' + agny.Id);
						
			Test.stopTest();			
		}
	}	
}