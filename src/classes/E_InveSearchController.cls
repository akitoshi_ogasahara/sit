global with sharing class E_InveSearchController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_ITHPF__c record{get;set;}	
			
	public E_InveSearchExtender getExtender() {return (E_InveSearchExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component5_val {get;set;}	
		public SkyEditor2.TextHolder Component5_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component42_val {get;set;}	
		public SkyEditor2.TextHolder Component42_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c NameKana_val {get;set;}	
		public SkyEditor2.TextHolder NameKana_op{get;set;}	
			
	public String recordTypeRecordsJSON_E_ITHPF_c {get; private set;}
	public String defaultRecordTypeId_E_ITHPF_c {get; private set;}
	public String metadataJSON_E_ITHPF_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public E_InveSearchController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_ITHPF__c.fields.ZCLNTCDE__c;
		f = E_ITHPF__c.fields.ContractorCLTPF_CLNTNUM__c;
		f = Contact.fields.E_CLTPF_ZCLKNAME__c;
		f = E_ITHPF__c.fields.ZCLNTCDE_LINK__c;
		f = E_ITHPF__c.fields.ContractorName__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_ITHPF__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component5_val = new SkyEditor2__SkyEditorDummy__c();	
				Component5_op = new SkyEditor2.TextHolder('eq');	
					
				Component42_val = new SkyEditor2__SkyEditorDummy__c();	
				Component42_op = new SkyEditor2.TextHolder('eq');	
					
				NameKana_val = new SkyEditor2__SkyEditorDummy__c();	
				NameKana_op = new SkyEditor2.TextHolder('co');	
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('E_ITHPF__c')
						.addFieldAsOutput('ZCLNTCDE_LINK__c')
						.addFieldAsOutput('ContractorName__c')
						.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c')
						.limitRecords(100)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component5_val, 'SkyEditor2__Text__c', 'ZCLNTCDE__c', Component5_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component42_val, 'SkyEditor2__Text__c', 'ContractorCLTPF_CLNTNUM__c', Component42_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(NameKana_val, 'SkyEditor2__Text__c', 'Contractor__r.E_CLTPF_ZCLKNAME__c',Contact.fields.E_CLTPF_ZCLKNAME__c, NameKana_op, true, false,true,0,true,'Contractor__c',E_ITHPF__c.fields.Contractor__c )) 
.addSort('ZCLNTCDE__c',True,False)
				);	
					
					resultTable = new resultTable(new List<E_ITHPF__c>(), new List<resultTableItem>(), new List<E_ITHPF__c>(), null);
				 resultTable.setPageItems(new List<resultTableItem>());
				 resultTable.setPageSize(100);
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_ITHPF__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_InveSearchExtender(this);
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_E_ITHPF_c_ZCLNTCDE_c() { 
			return getOperatorOptions('E_ITHPF__c', 'ZCLNTCDE__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_ITHPF_c_ContractorCLTPF_CLNTNUM_c() { 
			return getOperatorOptions('E_ITHPF__c', 'ContractorCLTPF_CLNTNUM__c');	
		}	
		public List<SelectOption> getOperatorOptions_Contact_E_CLTPF_ZCLKNAME_c() { 
			return getOperatorOptions('Contact', 'E_CLTPF_ZCLKNAME__c');	
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public E_ITHPF__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, E_ITHPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.PagingList {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (E_ITHPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<resultTableItem> getViewItems() {            return (List<resultTableItem>) getPageItems();        }
	}

			
	}