global with sharing class MNT_IrisPageLookup extends SkyEditor2.SkyEditorPageBaseWithSharing{
	
	public I_PageMaster__c record{get;set;}
	public Component3 Component3 {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component4{get;set;}
	public List<SelectOption> Component4_options {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component7{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component10{get;set;}
	{
	setApiVersion(31.0);
	}
	public MNT_IrisPageLookup(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = I_PageMaster__c.fields.Name;
		f = I_PageMaster__c.fields.MainCategory__c;
		f = I_PageMaster__c.fields.Menu__c;
		f = I_PageMaster__c.fields.LastModifiedDate;
		f = I_PageMaster__c.fields.LastModifiedById;
        f = I_PageMaster__c.fields.Name;

		try {
			mainRecord = null;
			mainSObjectType = I_PageMaster__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempProductLookup_01;
			
			Component4 = new SkyEditor2__SkyEditorDummy__c();
			Component7 = new SkyEditor2__SkyEditorDummy__c();
			Component10 = new SkyEditor2__SkyEditorDummy__c();
			
			queryMap.put(
				'Component3',
				new SkyEditor2.Query('I_PageMaster__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('MainCategory__c')
					.addFieldAsOutput('Menu__c')
					.addFieldAsOutput('LastModifiedDate')
					.addFieldAsOutput('LastModifiedById')
					.addField('Name')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component4, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('eq'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component7, 'SkyEditor2__Text__c', 'Name', new SkyEditor2.TextHolder('co'), false, true, false))
					.addListener(new SkyEditor2.QueryWhereRegister(Component10, 'SkyEditor2__Text__c', 'MainCategory__c', new SkyEditor2.TextHolder('co'), false, true, false))
			);
			
			Component3 = new Component3(new List<I_PageMaster__c>(), new List<Component3Item>(), new List<I_PageMaster__c>(), null);
			listItemHolders.put('Component3', Component3);
			Component3.ignoredOnSave = true;
			
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_PageMaster__c.SObjectType);
			
			p_showHeader = false;
			p_sidebar = false;
			presetSystemParams();
			update_Component4_options();
			initSearch();
			
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e){
			e.setMessagesToPage();
		} catch (SkyEditor2.Errors.PricebookNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
			hidePageBody = true;
		}
	}

	public List<SelectOption> getOperatorOptions_I_PageMaster_c_Name() {
		return getOperatorOptions('I_PageMaster__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_I_PageMaster_c_MainCategory_c() {
		return getOperatorOptions('I_PageMaster__c', 'MainCategory__c');
	}
	
	
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public I_PageMaster__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, I_PageMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (I_PageMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	
	public void update_Component4_options() {
		Component4_options = new SelectOption[]{ new SelectOption('', label.none) };
			SObject[] results;
			String soql = 'SELECT Name FROM I_PageMaster__c WHERE Name != null ';
			soql += 'GROUP BY Name ORDER BY Name LIMIT 999';
			results = Database.query(soql);
			for (SObject r : results) {
				String value = (String)(r.get('Name'));
				Component4_options.add(new SelectOption(value, value));
			}
	}
	
}