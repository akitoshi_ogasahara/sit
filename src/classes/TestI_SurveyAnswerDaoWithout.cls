@isTest
private class TestI_SurveyAnswerDaoWithout {

	@isTest static void insertRecTest(){
		//データ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us);

		I_SurveyAnswer__c answer = new I_SurveyAnswer__c();
		answer.SurveyTarget__c = survey.I_SurveyTargets__r[0].id;
		answer.SurveyQuestion__c = survey.I_SurveyQuestions__r[0].id;
		System.runAs(us){
		//テスト実行
			Test.startTest();
			I_SurveyAnswerDaoWithout.insertRec(answer);
			Test.stopTest();
		}
		//テスト検証
		I_SurveyAnswer__c answerRec = [SELECT Id, SurveyTarget__c, SurveyQuestion__c FROM I_SurveyAnswer__c WHERE SurveyQuestion__r.Survey__c =: survey.Id];
		System.assertEquals(survey.I_SurveyTargets__r[0].id, answer.SurveyTarget__c);
		System.assertEquals(survey.I_SurveyQuestions__r[0].id, answer.SurveyQuestion__c);

	}

	/*
	 * 代理店ユーザー作成
	 * @return User: ユーザー
	 */
	private static User createUser(){
		User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
		User us = new User();
		System.runAs(thisUser){
			Account parentAcc = TestI_TestUtil.createAccount(true, null);
			Account acc = TestI_TestUtil.createAccount(true, parentAcc);
			Contact con = TestI_TestUtil.createContact(true, acc.Id, 'testCon');
			us = TestI_TestUtil.createAgentUser(true, 'testUser', E_Const.PROFILE_E_PARTNERCOMMUNITY, con.Id);
		}
		return us;
	}
	/*
	 * アンケート登録 募集人
	 * @param user
	 * @return I_Survey__c： アンケート
	 */
	private static I_Survey__c createSurveyRec(User us){
		//アンケート作成
		I_Survey__c survey = new I_Survey__c();
		survey.Name = 'テストアンケート';
		survey.OverView__c = 'テスト概要';
		survey.UpsertKey__c = 'survey01';
		survey.PeriodFrom__c = Date.today();
		survey.PeriodTo__c = Date.today().addDays(5);
		survey.SurveyTarget__c = '代理店';
		insert survey;
		//アンケートに紐づく対象者作成
		I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
		sTarget.Ownerid = us.Id;
		sTarget.Survey__c = survey.id;
		sTarget.ParentAccount__c = us.Contact.Account.ParentId;
		insert sTarget;
		//アンケートに紐づく設問作成
		I_SurveyQuestion__c question = new I_SurveyQuestion__c();
		question.Name = 'テスト設問';
		question.Selection__c = '参加';
		question.Survey__c = survey.Id;
		insert question;

		survey = [SELECT Id, (SELECT Id FROM I_SurveyTargets__r), (SELECT Id FROM I_SurveyQuestions__r) FROM I_Survey__c WHERE Id =: survey.Id];

		return survey;
	}
}