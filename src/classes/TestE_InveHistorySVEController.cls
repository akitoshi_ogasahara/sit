@isTest
private with sharing class TestE_InveHistorySVEController{
		private static testMethod void testPageMethods() {	
			E_InveHistorySVEController page = new E_InveHistorySVEController(new ApexPages.StandardController(new E_ITTPF__c()));	
			System.assert(true);
		}	
			
	private static testMethod void testdataTable() {
		E_InveHistorySVEController.dataTable dataTable = new E_InveHistorySVEController.dataTable(new List<E_ITTPF__c>(), new List<E_InveHistorySVEController.dataTableItem>(), new List<E_ITTPF__c>(), null);
		dataTable.setPageItems(new List<E_InveHistorySVEController.dataTableItem>());
		dataTable.create(new E_ITTPF__c());
		dataTable.doDeleteSelectedItems();
		dataTable.setPagesize(10);		dataTable.doFirst();
		dataTable.doPrevious();
		dataTable.doNext();
		dataTable.doLast();
		dataTable.doSort();
		System.assert(true);
	}
	
}