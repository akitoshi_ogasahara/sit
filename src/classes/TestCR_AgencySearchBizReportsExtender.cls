/**
 * 
 */
@isTest
private class TestCR_AgencySearchBizReportsExtender {

    static testMethod void test_ExportCSV01() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizReportsPermissions(usr.Id);
		
		System.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZREPORTS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			CR_AgencySearchBizReportsSVEController controller = new CR_AgencySearchBizReportsSVEController(stdController);
			CR_AgencySearchBizReportsExtender extender = new CR_AgencySearchBizReportsExtender(controller);
			PageReference resultPref = extender.ExportCSV();
			
			Test.stopTest();
			
			PageReference targetPref = Page.E_CRExportAgencyBizReports;
			String soql = controller.queryMap.get('resultTable').toSoql();
			String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
			sWhere = E_SoqlUtil.trimLimitClause(sWhere);
			
			String resultParam = resultPref.getParameters().get(E_CSVExportController.SOQL_WHERE);
			
			// 実行結果確認
			System.assertEquals(sWhere, E_EncryptUtil.getDecryptedString(resultParam));
		}
    }
}