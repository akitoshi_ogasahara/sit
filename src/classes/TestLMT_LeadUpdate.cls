/**
@version 1.0
@author PwC
LMT_LeadUpdateTriggerHandlerテストクラス
*/
@isTest
private class TestLMT_LeadUpdate {
	@testSetup public static void setup() {

		//経営企画部のグループメンバーの準備
    	List<User> strUserGroup = [SELECT Id
    								FROM User
    								WHERE IsActive = true
    								AND profile.UserLicense.name = 'Salesforce'
    								AND UserType='Standard'
    							 	LIMIT 2];

    	//代理店データの準備
		//代理店格
    	Account testParentAccount = new Account(Name = 'PAccount', E_CL1PF_ZHEADAY__c='00001');
    	Insert testParentAccount;
		List<Account> acList = new List<Account>();
		//本部
		Account testHqAccount = new Account(Name = 'PHQAccount', ParentId=testParentAccount.Id);
		acList.add(testHqAccount);

		//支店
		Account testAccount = new Account(Name = 'PAccount', ParentId=testParentAccount.Id);
		acList.add(testAccount);
		Insert acList;

		Id HqAcId = null;
		Id bacId = null;
		for(Account ac :[SELECT Id,Name,E_ParentZHEADAY__c FROM Account WHERE Name IN ('PHQAccount','PAccount')]){
			if(ac.Name.equals('PHQAccount')){
				HqAcId = ac.Id;
			}else{
				bacId = ac.Id;
			}
		}


		List<Contact> contactList = new List<Contact>();
		//本部募集人データの準備
		Contact testContactHq01 = new Contact(LastName ='testHqCon01',AccountId = HqAcId);
		contactList.add(testContactHq01);
		Contact testContactHq02 = new Contact(LastName ='testHqCon02',AccountId = HqAcId);
		contactList.add(testContactHq02);

		//支店募集人データの準備
		//支店長
		Contact testContact01 = new Contact(LastName ='testCon01',AccountId = bacId);
		contactList.add(testContact01);
		//クラーク
		Contact testContact02 = new Contact(LastName ='testCon02',AccountId = bacId);
		contactList.add(testContact02);
		//募集人01
		Contact testContact03 = new Contact(LastName ='testCon03',AccountId = bacId);
		contactList.add(testContact03);
		//募集人02
		Contact testContact04 = new Contact(LastName ='testCon04',AccountId = bacId);
		contactList.add(testContact04);
        //募集人03
        Contact testContact05 = new Contact(LastName ='testCon05',AccountId = bacId);
		contactList.add(testContact05);
        //募集人04
        Contact testContact06 = new Contact(LastName ='testCon06',AccountId = bacId);
		contactList.add(testContact06);
        //募集人05
        Contact testContact07 = new Contact(LastName ='testCon07',AccountId = bacId);
		contactList.add(testContact07);
        Insert contactList;

        //代理店プロファイル
        Id pProfile = [SELECT Id,
        						UserType
    							FROM Profile
    							WHERE Name='E_PartnerCommunity_IA'
    							AND UserType='PowerPartner'].Id;

		List<User> partnerUserList = new List<User>();
        //本部
        User partnerHQ01 = new User(Alias = 'tHq01', Email='tHq01@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TESTHQ01', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='tHq01@test.nnlife.co.jp', ContactId = testContactHq01.Id ,IsPrmSuperUser=false);
		partnerUserList.add(partnerHQ01);

		User partnerHQ02 = new User(Alias = 'tHq02', Email='tHq02@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TESTHQ02', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='tHq02@test.nnlife.co.jp', ContactId = testContactHq02.Id ,IsPrmSuperUser=false);

        partnerUserList.add(partnerHQ02);

		//支店長
		User partner01 = new User(Alias = 't01', Email='t01@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST01', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t01@test.nnlife.co.jp', ContactId = testContact01.Id ,IsPrmSuperUser=true);
      	partnerUserList.add(partner01);

		//クラーク
		User partner02 = new User(Alias = 't02', Email='t02@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST02', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t02@test.nnlife.co.jp', ContactId = testContact02.Id  ,IsPrmSuperUser=true);
      	partnerUserList.add(partner02);

		//募集人01
      	User partner03 = new User(Alias = 't03', Email='t03@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST03', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t03@test.nnlife.co.jp', ContactId = testContact03.Id ,IsPrmSuperUser=false);
		partnerUserList.add(partner03);

		//募集人02
		User partner04 = new User(Alias = 't04', Email='t04@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST04', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t04@test.nnlife.co.jp', ContactId = testContact04.Id ,IsPrmSuperUser=false);
        partnerUserList.add(partner04);

        //募集人03
        User partner05 = new User(Alias = 't05', Email='t05@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST05', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t05@test.nnlife.co.jp', ContactId = testContact05.Id ,IsPrmSuperUser=false);
        partnerUserList.add(partner05);

        //募集人04
        User partner06 = new User(Alias = 't06', Email='t06@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST06', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t06@test.nnlife.co.jp', ContactId = testContact06.Id ,IsPrmSuperUser=false);
        partnerUserList.add(partner06);

        //募集人05
        User partner07 = new User(Alias = 't07', Email='t07@test.nnlife.co.jp',
            EmailEncodingKey='UTF-8', LastName='TEST07', LanguageLocaleKey='ja',
            LocaleSidKey='ja_JP', ProfileId = pProfile,
            TimeZoneSidKey='Asia/Tokyo', UserName='t07@test.nnlife.co.jp', ContactId = testContact07.Id ,IsPrmSuperUser=false);
        partnerUserList.add(partner07);

        Insert partnerUserList;

        //公開グループデータの準備
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
	        system.runAs(thisUser){
	        	Group publicGroup01 = new Group();
		    	publicGroup01.DeveloperName = 'LMT_00001';
		    	publicGroup01.Name='テスト代理店本社';
		    	Insert publicGroup01;

		    	Group publicGroup02 = [SELECT Id FROM Group WHERE DeveloperName = 'LMT_Strategy_Div'];
		    	//Group publicGroup03 = [SELECT Id FROM Group WHERE DeveloperName = 'StrategyDiv_Queue'];

				List<GroupMember> gmemList = new List<GroupMember>();
				System.runAs(thisUser){
				GroupMember GMHQ00 = new GroupMember();
		        GMHQ00.GroupId = publicGroup01.Id;
		        GMHQ00.UserOrGroupId = partnerHQ01.Id;
		        gmemList.add(GMHQ00);

		        GroupMember GMHQ01 = new GroupMember();
		        GMHQ01.GroupId = publicGroup01.Id;
		        GMHQ01.UserOrGroupId = partnerHQ02.Id;
		        gmemList.add(GMHQ01);

		    	GroupMember GM01 = new GroupMember();
		        GM01.GroupId = publicGroup02.Id;
		        GM01.UserOrGroupId = strUserGroup[0].Id;
		        gmemList.add(GM01);

		        GroupMember GM02 = new GroupMember();
		        GM02.GroupId = publicGroup02.Id;
		        GM02.UserOrGroupId = strUserGroup[1].Id;
		        gmemList.add(GM02);

		    	Insert gmemList;
	        }

		}

    	//LMT_メール通知管理のデータの準備
    	List<LMT_EmailAddressManagement__c> mailList = new List<LMT_EmailAddressManagement__c>();
		//Toが募集人01
		LMT_EmailAddressManagement__c eam01 = new LMT_EmailAddressManagement__c();
		eam01.User1__c = partner03.Id;
		eam01.User2__c = partner04.Id;
		eam01.User3__c = partner05.Id;
		eam01.User4__c = partner06.Id;
		eam01.User5__c = partner07.Id;
		eam01.User6__c = partner02.Id;
		mailList.add(eam01);

		//Toが支店長
		LMT_EmailAddressManagement__c eam02 = new LMT_EmailAddressManagement__c();
		eam02.User1__c = partner01.Id;
		eam02.User2__c = partner02.Id;
		eam02.User3__c = partner03.Id;
		eam02.User4__c = partner04.Id;
		eam02.User5__c = partner05.Id;
		eam02.User6__c = partner06.Id;
		mailList.add(eam02);

		//Toがクラーク
		LMT_EmailAddressManagement__c eam03 = new LMT_EmailAddressManagement__c();
		eam03.User1__c = partner02.Id;
		eam03.User2__c = partner01.Id;
		eam03.User3__c = partner03.Id;
		eam03.User4__c = partner04.Id;
		eam03.User5__c = partner05.Id;
		eam03.User6__c = partner06.Id;
		mailList.add(eam03);

		Insert mailList;

		//見込顧客データの準備
		List<Lead> leadList = new List<Lead>();
		Lead lead00 = new Lead();
		lead00.AssignmentStatus__c = '00';
		lead00.Status = '代理店連携後';
		lead00.Company = '会社00';
		lead00.LastName ='TEST00';
		leadList.add(lead00);

		Lead lead01 = new Lead();
		lead01.AssignmentStatus__c = '01';
		lead01.Status = '代理店連携後';
		lead01.Company = '会社01';
		lead01.LastName ='TEST01';
		leadList.add(lead01);

		Lead lead02 = new Lead();
		lead02.AssignmentStatus__c = '02';
		lead02.Status = '代理店連携後';
		lead02.Company = '会社02';
		lead02.LastName ='TEST02';
		leadList.add(lead02);

		Lead lead03 = new Lead();
		lead03.AssignmentStatus__c = '03';
		lead03.Status = '代理店連携後';
		lead03.Company = '会社03';
		lead03.LastName ='TEST03';
		leadList.add(lead03);

		Lead lead04 = new Lead();
		lead04.AssignmentStatus__c = '02';
		lead04.Status = '代理店連携後';
		lead04.Company = '会社04';
		lead04.LastName ='TEST04';
		leadList.add(lead04);

		Lead lead05 = new Lead();
		lead05.AssignmentStatus__c = '01';
		lead05.Status = '代理店連携後';
		lead05.Company = '会社05';
		lead05.LastName ='TEST05';
		leadList.add(lead05);

		Lead lead06 = new Lead();
		lead06.AssignmentStatus__c = '01';
		lead06.Status = '代理店連携後';
		lead06.Company = '会社06';
		lead06.LastName ='TEST06';
		leadList.add(lead06);

		Insert leadList;

		List<Lead> createdLeadList = [SELECT Id,
											Company,
											AssignmentStatus__c,
											LatestCampMemUrl__c,
											OwnerId
										FROM Lead
										WHERE Company LIKE '会社0%'
										ORDER BY Company];

		createdLeadList[0].OwnerId = partner02.Id;
		createdLeadList[1].OwnerId = partnerHQ01.Id;
		createdLeadList[2].OwnerId = partner01.Id;
		createdLeadList[3].OwnerId = partner03.Id;
		createdLeadList[4].OwnerId = partner01.Id;
		createdLeadList[5].OwnerId = partnerHQ01.Id;
		Update createdLeadList;
	}
	@isTest
    private static void testMethodForAuthority() {
    	List<Lead> updateLeadList = [SELECT Id,
											Company,
											AssignmentStatus__c,
											LatestCampMemUrl__c,
											OwnerId
										FROM Lead
										WHERE Company LIKE '会社0%'
										ORDER BY Company];
    	List<User> puserList = [SELECT Id,
										Email
									FROM User
									WHERE Email IN ('thq01@test.nnlife.co.jp',
													'thq02@test.nnlife.co.jp',
													't01@test.nnlife.co.jp',
													't02@test.nnlife.co.jp',
													't03@test.nnlife.co.jp',
													't04@test.nnlife.co.jp',
													't05@test.nnlife.co.jp',
													't06@test.nnlife.co.jp',
													't07@test.nnlife.co.jp')];
		Group publicGroup03 = [SELECT Id FROM Group WHERE DeveloperName = 'StrategyDiv_Queue'];
		User testAgent = new User();
		User assignedSAgent = new User();
		User assignedAgent = new User();
		for(User puser : puserList){
				if(puser.Email.equals('t03@test.nnlife.co.jp')){
					testAgent.Id =puser.Id;
				}
				if(puser.Email.equals('t01@test.nnlife.co.jp')){
					assignedSAgent.Id =puser.Id;
				}
				if(puser.Email.equals('t04@test.nnlife.co.jp')){
					assignedAgent.Id =puser.Id;
				}
		}
		system.runAs(testAgent){
			try{
				updateLeadList[3].OwnerId = assignedSAgent.Id;
				update updateLeadList[3];
			}catch(Exception e){
				System.assert(e.getMessage().contains(Label.Msg_Error_LMT_LeadAssignmentAuthority));
			}

		}


    }
    @isTest
    private static void testMethodForAssignmentStatus() {
		//LMT_LeadUpdateTrigerHelper.isFirstRun = true;
		List<Lead> updateLeadList = [SELECT Id,
											Company,
											AssignmentStatus__c,
											LatestCampMemUrl__c,
											OwnerId
										FROM Lead
										WHERE Company LIKE '会社0%'
										ORDER BY Company];

		List<User> puserList = [SELECT Id,
										Email
									FROM User
									WHERE Email IN ('thq01@test.nnlife.co.jp',
													'thq02@test.nnlife.co.jp',
													't01@test.nnlife.co.jp',
													't02@test.nnlife.co.jp',
													't03@test.nnlife.co.jp',
													't04@test.nnlife.co.jp',
													't05@test.nnlife.co.jp',
													't06@test.nnlife.co.jp',
													't07@test.nnlife.co.jp')];
		Group publicGroup03 = [SELECT Id FROM Group WHERE DeveloperName = 'StrategyDiv_Queue'];
		List<Lead> updateList = new List<Lead>();
		Test.startTest();
		try{
			for(User puser : puserList){
				if(puser.Email.equals('t03@test.nnlife.co.jp')){
					//クラークから募集人へ割当
					updateLeadList[0].OwnerId = puser.Id;
					updateList.add(updateLeadList[0]);
				}
				if(puser.Email.equals('t01@test.nnlife.co.jp')){
					//本部から支店長へ割当
					updateLeadList[1].OwnerId = puser.Id;
					updateList.add(updateLeadList[1]);
					//募集人から支店長へ割当
					updateLeadList[3].OwnerId = puser.Id;
					updateList.add(updateLeadList[3]);
				}
				if(puser.Email.equals('thq01@test.nnlife.co.jp')){
					//支店長から本部へ割当
					updateLeadList[2].OwnerId = puser.Id;
					updateList.add(updateLeadList[2]);
				}
				if(puser.Email.equals('t04@test.nnlife.co.jp')){
					//支店長からメール通知管理登録外の募集人へ割当
					updateLeadList[4].OwnerId = puser.Id;
					updateList.add(updateLeadList[4]);
				}

			}

			//本部から施策事務局へ割当
			updateLeadList[5].OwnerId = publicGroup03.Id;
			updateList.add(updateLeadList[5]);
			update updateLeadList;

		}catch(Exception e){

		}
		List<Lead> hqTestLead = [SELECT Id,
									Company,
									AssignmentStatus__c,
									LatestCampMemUrl__c,
									OwnerId
								FROM Lead
								WHERE Company LIKE '会社0%'
								ORDER BY Company];
		System.assertEquals('03', hqTestLead[0].AssignmentStatus__c);
		System.assertEquals('02', hqTestLead[1].AssignmentStatus__c);
		System.assertEquals('01', hqTestLead[2].AssignmentStatus__c);
		System.assertEquals('02', hqTestLead[3].AssignmentStatus__c);
		System.assertEquals('03', hqTestLead[4].AssignmentStatus__c);
		System.assertEquals('00', hqTestLead[5].AssignmentStatus__c);
		Test.stopTest();

    }
}