public without sharing class E_GroupMemberDaoWithout {

	/**
	 * @parram ids UserOrGroupIdのSet
	 * @return List<GroupMember>
	 */
	public static List<GroupMember> getRecByUserORGroupId(Set<ID> ids){
		return [SELECT Id, GroupId, UserOrGroupId FROM GroupMember Where UserOrGroupId in: ids];
	}

	public static List<Database.DeleteResult> doDelete(List<GroupMember> deleteGms){
		return Database.Delete(deleteGms, false);
	}
}