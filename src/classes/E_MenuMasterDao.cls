/**
 * メニューマスタデータアクセスオブジェクト
 * CreatedDate 2014/12/02
 * @Author Terrasky
 */
public class E_MenuMasterDao {
    
    /**
     * メニューマスタデータをメニュー種類Idから取得する
     * @param MenukndId: メニュー種類Id
     * @return List<E_MenuMaster__c>: メニューマスタリスト
     */
    public static List<E_MenuMaster__c> getMenuMstsByMenukndId(Id menuKndId){
        E_AccessController accCon = E_AccessController.getInstance();
        
        boolean isStandardONly = accCon.isEmployee();						//社員ユーザのみ
        boolean isPartnerONly = accCon.isAgent();							//代理店ユーザのみ
        boolean isCommonGW = accCon.isCommonGateway();						//共同GWから
        boolean isInternet = !accCon.isCommonGateway();						//インターネットから
        if (isStandardONly) {
        	if (String.isEmpty(E_CookieHandler.getCookieAccessKind())) {
        		isCommonGW = false;
        		isInternet = true;
        	} else {
        		isCommonGW = E_CookieHandler.getCookieAccessKind().equals('1') ? true : false;
        		isInternet = E_CookieHandler.getCookieAccessKind().equals('1') ? false : true;
        	}
        }
        
        //ID管理フラグチェック
    	List<String> authList = new List<String>();
    	String CUSTOMER_SEARCH = '顧客検索';
    	String SPVA = 'SPVA';
    	String INVESTMENT = '投信照会';
    	String CUSTOMER_INFO = '契約照会';
    	String DOWNLOAD = 'ダウンロード';
    	
        //顧客検索フラグ付与時
        if(accCon.canCustomerSearch()) authList.add(CUSTOMER_SEARCH);
        //SPVAフラグ付与時
        if(accCon.canSpva()) authList.add(SPVA);
        //要投信照会フラグ付与時
        if(accCon.canInvestment()) authList.add(INVESTMENT);
        //契約照会：個人保険照会フラグ、SPVA照会フラグ、投信照会フラグのいずれか付与時
        if(accCon.canColi() || accCon.canSpva() || accCon.canInvestment()) authList.add(CUSTOMER_INFO);
    	//ダウンロード：手数料明細書照会フラグ、手続の履歴フラグ、個人保険照会フラグ、SPVA照会フラグのいずれか付与時
        if(accCon.canDownloads()) authList.add(DOWNLOAD);
        
        return [
            SELECT
                  Id
                , Name
//              , Recordtype.Name
//              , SortNumberAll__c
                ,(
                    SELECT
                          Id
                        , Name
//                      , Recordtype.Name
//                      , ParentName__c
//                      , SortNumberAll__c
                        , MenuMasterKey__c
//                      , IsExternalUrl__c
                        , Url__c
                        , UrlInput__c
//                        , IsParam_MenuMasterId__c
//                        , IsParam_LoginUserId__c
//                        , IsParam_ContactID__c
                        , AddParameters__c
//                      , UserContactId__c
                        , WindowTarget__c
                        , IsWindowOpen__c
                        , PostParam__c
                        , IsStandardUserOnly__c
                        , IsPartnerUserOnly__c
                        , IsHideMenu__c
                        , IsShowCloseBtn__c
//                      , IsViaCommonGW__c
                        , SelectedMenuKey__c
                        , UseCommonGW__c
                        , UseInternet__c
                    FROM E_MenuMasters__r
                    WHERE Id IN (
                            SELECT
                                  MenuMaster__c
                            FROM E_SelectedMenu__c
                            WHERE MenuKind__c = :menuKndId
                              AND isShowMenu__c = true				/*	表示期間中のもののみ表示　2015/11/30	*/
                        )
                         AND (IsStandardUserOnly__c = false
                          OR  IsStandardUserOnly__c = :isStandardONly)
                         AND (IsPartnerUserOnly__c = false
                          OR  IsPartnerUserOnly__c = :isPartnerONly)
                         AND (
                         		(UseCommonGW__c = true AND UseCommonGW__c = :isCommonGW)
                         	 OR (UseInternet__c = true AND UseInternet__c = :isInternet)
                            )
                         AND (
                         		RequiredAuthList__c = null
                         	 OR RequiredAuthList__c IN :authList
                         	)
				        //権限セット条件追加 2015.10　追加
				        AND (
				        		RequiredPermissionSetName__c = null
				        	 OR RequiredPermissionSetName__c in :accCon.getAssignedPermissionSetNames()
				        	)
                    ORDER BY SortNumberAll__c
                )
            FROM E_MenuMaster__c
            WHERE Recordtype.DeveloperName = :E_Const.MENU_RECORDTYPE_TOPMENU
            ORDER BY SortNumberAll__c
        ];
    }


    /**
     * メニューマスタレコードをIdに取得
     * @param MenukndId: メニューId
     * @return E_MenuMaster__c
     */
/*
    public static E_MenuMaster__c getMenuMasterRecById(Id menuId){
        for(E_MenuMaster__c menu: [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c,IsHideMenu__c, IsShowCloseBtn__c 
                                     FROM E_MenuMaster__c 
                                    WHERE Id = :menuId
                                    Order By Id]){
            return menu;
        }
        return null;
    }
*/
    /**
     * メニューマスタレコードをKeyに取得
     * @param menuKey: メニューKey
     * @return E_MenuMaster__c
     */
/*
    public static E_MenuMaster__c getMenuMasterRecByKey(String menukey){
        for(E_MenuMaster__c menu: [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c,IsHideMenu__c, IsShowCloseBtn__c 
                                     FROM E_MenuMaster__c 
                                    WHERE MenuMasterKey__c = :menukey
                                    Order By Id]){
            return menu;
        }
        return null;
    }
*/
    /**
     * メニューをKeyにそのサブページを取得取得
     * @param menuKey: メニューKey
     * @return List<E_MenuMaster__c>
     */
/*
    public static List<E_MenuMaster__c> getSubPagesByKey(String menukey){
        return [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c , IconsUrl__c,IsHideMenu__c, IsShowCloseBtn__c
                                     FROM E_MenuMaster__c 
                                    WHERE SelectedMenuKey__c = :menukey
                                      AND MenuMasterKey__c != :menukey      //サブページなので自分自身は取得しない
                                    Order By SortNumberAll__c];
    }
*/
}