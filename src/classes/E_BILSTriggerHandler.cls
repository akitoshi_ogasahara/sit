public with sharing class E_BILSTriggerHandler {
	public void onBeforeInsert (List<E_BILS__c> newList){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList);
	}
	public void onBeforeUpdate (List<E_BILS__c> newList,Map<Id,E_BILS__c> oldMap){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList,oldMap);
	}
}