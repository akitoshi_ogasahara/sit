public with sharing class E_BILATriggerHandler {
	public void onBeforeInsert (List<E_BILA__c> newList){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList);
	}
	public void onBeforeUpdate (List<E_BILA__c> newList ,Map<Id,E_BILA__c> oldMap){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList,oldMap);
	}
}