@isTest
private class TestE_DownloadNoticeDTO {
	/**
	 * 3ヶ月取得
	 */
	@isTest static void test_method_one() {
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = new E_IDCPF__c(User__c = u.Id, OwnerId = u.Id, ZINQUIRR__c = 'BR**');
		insert idcpf;
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'22222' );
		E_Policy__c plcy = TestE_TestUtil.createPolicy(true , con.id , 'ECHDPF', '33333');

		List<E_AGPO__c> ins = new List<E_AGPO__c>();
		Date d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
		for(Integer i = 1;i < 4;i++){
			E_AGPO__c agpo = new E_AGPO__c(
									YYYY__c = d.year()
									, MM__c = d.month()
									, LIMITDT__c = '2000010' + String.valueOf(i)
									, E_Policy__c = plcy.id
									);
			ins.add(agpo);
			d = d.addMonths(-1);
		}
		insert ins;
		system.debug('ins=' + ins);
		System.runAs(u){
		//コンストラクタ
			E_DownloadNoticeDTO dto = new E_DownloadNoticeDTO(plcy.Id);
system.debug('dto.noticeList=' + dto.noticeList);
/*		system.assertEquals(dto.noticeList.size() ,3);
		system.assertEquals(dto.noticeList[0].LIMITDT ,'2000/01/01');
		system.assertEquals(dto.noticeList[1].LIMITDT ,'2000/01/02');
		system.assertEquals(dto.noticeList[2].LIMITDT ,'2000/01/03');*/
		}
	}
	/**
	 * 帳票ソート
	 */
	@isTest static void test_method_two() {
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = new E_IDCPF__c(User__c = u.Id, OwnerId = u.Id, ZINQUIRR__c = 'BR**');
		insert idcpf;

		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'22222' );
		E_Policy__c plcy = TestE_TestUtil.createPolicy(true , con.id , 'ECHDPF', '33333');

		List<E_AGPO__c> ins = new List<E_AGPO__c>();
		Date d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
		E_AGPO__c agpo = new E_AGPO__c(
							YYYY__c = d.year()
							, MM__c = d.month()
							, E_Policy__c = plcy.id
							);
		insert agpo;
System.debug(agpo);
		d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA);
		E_BILA__c bila = new E_BILA__c(
							YYYY__c = d.year()
							, MM__c = d.month()
							, E_Policy__c = plcy.id
							);
		insert bila;
		d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS);
		E_BILS__c bils = new E_BILS__c(
							YYYY__c = d.year()
							, MM__c = d.month()
							, E_Policy__c = plcy.id
							);
		insert bils;
		d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
		E_NCOL__c ncol = new E_NCOL__c(
							YYYY__c = d.year()
							, MM__c = d.month()
							, E_Policy__c = plcy.id
							);
		insert ncol;
List<E_NCOL__c> ncolList = [Select Id From E_NCOL__c Where E_Policy__c =:plcy.Id];
System.debug(ncolList);
List<E_Policy__c> plcyList = [Select Id, (Select id From E_NCOLs__r) From E_Policy__c Where Id =:plcy.Id];
System.debug(plcyList);
		System.runAs(u){
		//コンストラクタ
			E_DownloadNoticeDTO dto = new E_DownloadNoticeDTO(plcy.Id);
System.debug('dto.noticeList=' + dto.noticeList);
/*			system.assertEquals(dto.noticeList.size() ,4);
			Integer i = 0;
			for(String key :E_DownloadNoticeConst.FORMATNO_MAP.keySet()){
				key = dto.noticeList[i].formatName;
				i++;
		}
		system.assert(dto.getIsNotice());*/
		}
	}
	@isTest static void forCoverage() {
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'22222' );
		E_Policy__c plcy = TestE_TestUtil.createPolicy(true , con.id , 'ECHDPF', '33333');

		E_DownloadNoticeDTO dto = new E_DownloadNoticeDTO(plcy.Id);
		dto.getIsNotice();
	}
	private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}