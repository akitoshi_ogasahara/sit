public with sharing class SC_SearchWrapController extends AMS_AbstractExtender{
	public String selectedIds {get; set;}
	public String templateDevName {get; set;}
    public String subject {get; set;}
    public String mailBody {get; set;}
    public Boolean isCmd {get; set;}
    public String sendMsg {get; set;}
    
    /**
     * Constructor
     */
    public SC_SearchWrapController(){
		pgTitle = '代理店事務所検索（代理店自己点検）';
    	subject = '';
	    mailBody = '';
	    isCmd = access.hasSCAdminPermission();
    }
    
    /**
     * PageAction
     */
    public pageReference pageAction(){
        return null;
    }

    /**
     * メールテンプレートのセット
     */
    public void setMailTemplate(){
    	// メールテンプレート取得
    	EmailTemplate eMailTemplate = E_EmailTemplateDao.getRecByDeveloper(templateDevName);
    	if(eMailTemplate != null){
	    	subject = eMailTemplate.Subject;
	    	mailBody = eMailTemplate.Body;
    	}
    }

	/**
	 * メール送信ボタン押下処理
	 *   メール送信処理は自主点検メール送信履歴のワークフローにて実装
	 *   代理店  →  業務管理責任者、MR、拠点長
	 *   MR    →  業務管理責任者、拠点長 
	 */
	public void doSend(){
		try{
			if(String.isBlank(selectedIds)){
				return;
			}
			
			// 選択されたIdセット作成
			Set<Id> ids = new Set<Id>();
			for(String str : selectedIds.split(',')){
				ids.add(str);
			}
			ids.remove(null);

			// 選択された自主点検事務所より、送信対象ユーザを取得
			List<SC_Office__c> officeList = SC_OfficeDao.getRecsForRemindMail(ids);
			
			// 自主点検メール送信履歴の登録
			List<SC_MailHistory__c> mailHistoryList = new List<SC_MailHistory__c>();
			for(SC_Office__c office : officeList){
				List<Id> userIds = new List<Id>();

				// 自主点検メール送信履歴
				SC_MailHistory__c mailHistory = new SC_MailHistory__c();
				mailHistory.SC_Office__c = office.Id;
				mailHistory.Subject__c = SC_MailManager.replaceTemplate(office, subject, SC_MailManager.replaceListRemind);
				mailHistory.Body__c = SC_MailManager.replaceTemplate(office, mailBody, SC_MailManager.replaceListRemind);
	
				// 拠点長 => SendUser1__c
				mailHistory.SendUser1__c = office.MRManager__r.Id;
				
				// MR => SendUser2__c
				mailHistory.SendUser2__c = office.Account__r.OwnerId;
				// MRのアドレス　本社営業 => GM, 本社営業以外 => MRのUserアドレス
				mailHistory.SendAddress2__c = office.IsHeadSection__c ? System.Label.SC_HQRep_Gr_EMail : office.Account__r.Owner.Email;
				
				// 業務管理責任者 => SendUser3~7__c
				Integer userNo = 3;
				if(templateDevName == SC_MailManager.MT_REMIND_BEFORE_AG
								|| templateDevName == SC_MailManager.MT_REMIND_AFTER_AG
								|| templateDevName == SC_MailManager.MT_REMIND_DEFECT_AG){
					for(SC_AgencyManager__c agManager : office.SC_AgencyManagers__r){
						mailHistory.put('SendUser' + userNo + '__c', agManager.User__r.Id);
						userNo++;
					}
				}
	
				mailHistoryList.add(mailHistory);
			}
			system.debug('** mailHistoryList **' + mailHistoryList);
			
			// Insert
			insert mailHistoryList;
			
			// Msg
			sendMsg = '指定した宛先にメールを送信しました。';
		
    	}catch(Exception e){
    		system.debug('** <setMailTemplate> exception **' + e.getMessage());
    		// Msg
			sendMsg = 'メール送信でエラーが発生しました。<br/>' + e.getMessage();
    	}
    }
}