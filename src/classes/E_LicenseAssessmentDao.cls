public with sharing class E_LicenseAssessmentDao {

	/**
	 * 代理店挙積情報IDをキーに、指定した資格査定詳細情報を取得する。
	 * @param asrId: 代理店挙積情報ID
	 * @return List<E_LicenseAssessment__c>: 資格査定詳細情報リスト
	 */
	public static List<E_LicenseAssessment__c> getRecsByASRId(Id asrId) {
		return [
				Select
					Id								// カスタムオブジェクト ID
					//,Name							// 資格査定詳細情報名
					//,OwnerId						// 所有者 ID
					//,IsDeleted					// 削除
					//,CreatedDate					// 作成日
					//,CreatedById					// 作成者 ID
					//,LastModifiedDate				// 最終更新日
					//,LastModifiedById				// 最終更新者 ID
					//,SystemModstamp				// System Modstamp
					//,AddParentAccountCode__c		// 合算先代理店コード
					,E_AgencySalesResults__c		// 代理店挙積情報
					//,E_Unit__c						// 営業部コード
					,Hierarchy__c					// 階層
					//,MR__c							// MRコード
					//,ParentAccount__c				// 代理店コード
					//,SectionCode__c					// 課コード
					,Standard_ActiveMonth__c		// 基準　稼動月数
					,Standard_IANP__c				// 基準　保有ANP
					,Standard_IQA__c				// 基準　IQA継続率
					,Standard_NBCANP_SpInsType__c	// 基準　新契約係数ANP 特定保険種類
					,Standard_NBCANP__c				// 基準　新契約係数ANP
					,State_ActiveMonth__c			// 状況　稼動月数
					,State_IANP__c					// 状況　保有ANP
					,State_IQA__c					// 状況　IQA継続率
					,State_NBCANP_SpInsType__c		// 状況　新契約係数ANP 特定保険種類
					,State_NBCANP__c				// 状況　新契約係数ANP
					,Qualification__c				// 資格
					,SEQ__c							// SEQ
				From
					E_LicenseAssessment__c
				Where
					E_AgencySalesResults__c = :asrId	// 代理店挙積情報
				Order By
					Qualification__c				// 資格
					,SEQ__c							// SEQ
			   ];
	}
	/**
	 * IDと資格情報からレコードを取得する処理
	 * @param asrId: 代理店挙積情報ID
	 * @param String qualificationType: 資格
	 * @return List<E_LicenseAssessment__c>: 資格査定詳細情報リスト
	 */

	public static List<E_LicenseAssessment__c> getRecsByQualification(Id asrId, String qualificationType) {
		return [
				Select
					Id								// カスタムオブジェクト ID
					,E_AgencySalesResults__c		// 代理店挙積情報
					,Hierarchy__c					// 階層
					,Standard_ActiveMonth__c		// 基準　稼動月数
					,Standard_IANP__c				// 基準　保有ANP
					,Standard_IQA__c				// 基準　IQA継続率
					,Standard_NBCANP_SpInsType__c	// 基準　新契約係数ANP 特定保険種類
					,Standard_NBCANP__c				// 基準　新契約係数ANP
					,State_ActiveMonth__c			// 状況　稼動月数
					,State_IANP__c					// 状況　保有ANP
					,State_IQA__c					// 状況　IQA継続率
					,State_NBCANP_SpInsType__c		// 状況　新契約係数ANP 特定保険種類
					,State_NBCANP__c				// 状況　新契約係数ANP
					,Qualification__c				// 資格
					,SEQ__c							// SEQ
				From
					E_LicenseAssessment__c
				Where
					E_AgencySalesResults__c = :asrId	// 代理店挙積情報
				AND 
					Qualification__c = :qualificationType //資格
				Order By 
					SEQ__c							// SEQ
			   ];
	}

}