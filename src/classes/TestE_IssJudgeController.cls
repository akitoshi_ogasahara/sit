@isTest
private class TestE_IssJudgeController {

    /*
     * SP種別がCOLIの時。
     */
    static testMethod void testIssJudge() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(false, 'keiyakusha', E_Const.SYSTEM_ADMINISTRATOR);
        TestE_TestUtil.createMessageMaster_Messages(True);
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        u.FederationIdentifier = 'saml83';
        insert u;

        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        insert idcpf;
        
        //SP種別がCOLIの時
        Contact con  = TestE_TestUtil.createContact(true,null,'ebi','0802','ebi');
        E_Policy__c policy = TestE_TestUtil.createPolicy(true, con.Id ,E_Const.POLICY_RECORDTYPE_COLI,'12903171');

        PageReference resultPage;

        //テストユーザで機能実行開始
        System.runAs(u){
            E_IssJudgeController ctr = new E_IssJudgeController();
            Test.startTest();
            resultPage = ctr.init();
            Test.stopTest();
        }
        //System.assert(resultPage.getUrl().indexOf(Page.E_IssReference.getUrl()) > -1);
    }

    // 所有者作成メソッド
    private static User createOwner(){
        User thisUser = TestE_TestUtil.createUser(true, 'thisUser', E_Const.SYSTEM_ADMINISTRATOR);
        //所有者ユーザ作成(共通クラスのメソッド呼び出し)
        User ownerUser = TestE_TestUtil.createUser(false, 'shoyusya', E_Const.SYSTEM_ADMINISTRATOR);
        System.runAs(thisUser){
            UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
            ownerUser.userroleid = uRole.id;
        }
        insert ownerUser;
        return ownerUser;
    }

}