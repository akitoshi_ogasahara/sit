@isTest
private class TestI_MenuItem {
	// null確認 - 初期処理
	static testMethod void ConstructorNullTest() {
		Test.startTest();
			I_MenuItem imenu = new I_MenuItem();

			system.assertEquals(imenu.record, null);
			system.assertEquals(imenu.rendered, null);
		Test.stopTest();
	}

	// Guestユーザ - 初期処理
	static testMethod void ConstructorGuestUserTest() {
		// 実行ユーザ作成
		String userName = 'testUser@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = 'testUser'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		insert actUser;

		I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', IRISPageName__c = 'IRIS_Top');
		insert m;

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuItem imenu = new I_MenuItem(m);

				system.assertEquals(imenu.record, m);
				system.assertEquals(imenu.rendered, false);
				system.assertEquals(imenu.getDestUrl(), null);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - 初期処理
	static testMethod void ConstructorTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', IRISPageName__c = 'IRIS_Top');
				insert m;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, UpsertKey__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c ];
				I_MenuItem imenu = new I_MenuItem(mm);

				String retUrl = mm.DestUrl__c;
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);

				system.assertEquals(imenu.record, mm);
				system.assertEquals(imenu.rendered, true);
				system.assertEquals(imenu.getDestUrl(), retUrl);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - ID管理 フラグ項目検証 - ture
	static testMethod void ConstructorEidcFlgTrueTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c userIdcpf = new E_IDCPF__c(
					 User__c = actUser.Id
					,OwnerId = actUser.Id
					,ZSTATUS01__c = '1'
					,ZINQUIRR__c = 'L167890'
					,ZDSPFLAG01__c = '1'
					,FLAG01__c = '1'
					,FLAG02__c = '1'
					,FLAG04__c = '1'
					,FLAG06__c = '1'
					,AppMode__c = I_Const.APP_MODE_NNLINK
				);
		insert userIdcpf;

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.Required_EIDC_Flag__c.getDescribe().getPicklistValues();

				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', Required_EIDC_Flag__c = entries[0].getValue());
				insert m;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, UpsertKey__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c ];
				I_MenuItem imenu = new I_MenuItem(mm);

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);


				system.assertEquals(imenu.record, mm);
				system.assertEquals(imenu.rendered, true);
				system.assertEquals(imenu.getDestUrl(), retUrl);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - ID管理 フラグ項目検証 - false
	static testMethod void ConstructorEidcFlgFalseTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c userIdcpf = new E_IDCPF__c(
					 User__c = actUser.Id
					,OwnerId = actUser.Id
					,ZSTATUS01__c = '1'
					,ZINQUIRR__c = 'L167890'
					,ZDSPFLAG01__c = '0'
					,FLAG01__c = '0'
					,FLAG02__c = '0'
					,FLAG04__c = '0'
					,FLAG06__c = '0'
					,AppMode__c = I_Const.APP_MODE_NNLINK
				);
		insert userIdcpf;

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.Required_EIDC_Flag__c.getDescribe().getPicklistValues();

				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', Required_EIDC_Flag__c = entries[0].getValue());
				I_MenuItem imenu = new I_MenuItem(m);

				system.assertEquals(imenu.record, m);
				system.assertEquals(imenu.rendered, false);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - ID種別検証 - true
	static testMethod void ConstructorZidTypeTrueTest1() {
		// 実行ユーザ作成
		// 1. 取引先（格）を作成
		Account accParent = TestI_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', Required_ZIDTYPE__c = 'AY', UrlParameters__c = 'testParameter');
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, UpsertKey__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c ];
				I_MenuItem imenu = new I_MenuItem(mm);

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += mm.pages__r[0].Id;
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += mm.UrlParameters__c;

				system.assertEquals(imenu.record, mm);
				system.assertEquals(imenu.rendered, true);
				system.assertEquals(imenu.getDestUrl(), retUrl);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - ID種別検証 - true
	static testMethod void ConstructorZidTypeTrueTest2() {
		// 実行ユーザ作成
		// 1. 取引先（格）を作成
		Account accParent = TestI_TestUtil.createAccount(false, null);
		accParent.E_CL1PF_KCHANNEL__c = 'BK';
		insert accParent;
		// 2. 取引先（事務所）を作成
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testDocumentMenu', Required_ZIDTYPE__c = 'AY', UrlParameters__c = 'testParameter', IRISPageName__c='IRIS_DocumentRequest');
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, UpsertKey__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c ];
				I_MenuItem imenu = new I_MenuItem(mm);

				system.assertEquals(imenu.record, mm);
				system.assertEquals(imenu.rendered, true);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - ID種別検証 - false
	static testMethod void ConstructorZidTypeFalseTest() {
		// 実行ユーザ作成
		// 1. 取引先（格）を作成
		Account accParent = TestI_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', Required_ZIDTYPE__c = 'AH');
				I_MenuItem imenu = new I_MenuItem(m);

				system.assertEquals(imenu.record, m);
				system.assertEquals(imenu.rendered, false);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - 権限セット検証 - true
	static testMethod void ConstructorPermissionSetTureTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', RequiredPermissionSet__c = E_Const.PERM_BASE);
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, UpsertKey__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c ];
				I_MenuItem imenu = new I_MenuItem(mm);

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += mm.pages__r[0].Id;
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);

				system.assertEquals(imenu.record, mm);
				system.assertEquals(imenu.rendered, true);
				system.assertEquals(imenu.getDestUrl(), retUrl);
			Test.stopTest();
		}
	}

	// Guestユーザ以外 - 権限セット検証 - false
	static testMethod void ConstructorPermissionSetFalseTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', RequiredPermissionSet__c = E_Const.PERM_INVE);
				I_MenuItem imenu = new I_MenuItem(m);

				system.assertEquals(imenu.record, m);
				system.assertEquals(imenu.rendered, false);
			Test.stopTest();
		}
	}
}