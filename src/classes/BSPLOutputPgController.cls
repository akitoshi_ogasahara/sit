/**
* クラス名	:  BSPLOutputPgController
* クラス概要	:  ＢＳ・ＰＬ情報詳細画面(コントローラ)
* @created	: 
* @modified	: 2015/12/03 KSVC Thao Doan Phuong
*/
public with sharing class BSPLOutputPgController {

  public string dispSwitch { get; set;}

	// ファイナンス情報.決算期(前期)
	public String strTerm{get; set;}

	// ファイナンス情報
	public Finance__c finance{ get; set; }
	
	public Finance__c finance02{ get; set; }
	
	//表題ラベル
	public string BS_BeforePrevious { get; set; }
	public string BP_Previous { get; set; }
	/* 2015/12/08 KSVC THAO DOAN PHUONG ADD START */
	// タイムアウト回避の詳細
	public string	strFCTimeOut { get; set; }
	/* 2015/12/08 KSVC THAO DOAN PHUONG ADD END */
	// 顧客
	private Customer__c customer{get; set;}
	
	/**
	* ＢＳ・ＰＬOutputPgController
	* ページ初期化処理
	* @param 	: なし
	* @return	: なし
	* @created  : 
	* @modified :   
	*/
	public BSPLOutputPgController(ApexPages.StandardController controller){
	
		// ファイナンス情報obj.カスタムオブジェクトID
		String strFinanceId = Apexpages.currentPage().getParameters().get(CommonConst.STR_PARAMETER_ID);
		if (strFinanceId == null) {
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_BP_NotBSPL));		
			return;
		}
		
		try {
			/* 2015/12/08 KSVC THAO DOAN PHUONG ADD START */
			// タイムアウト回避の詳細
			resetSessionTimeout();
			/* 2015/12/08 KSVC THAO DOAN PHUONG ADD END */
			
			// 前期ファイナンス情報を取得
			finance = (Finance__c)controller.getRecord();
			
			// 画面にファイナンス情報.決算期(前期)値を表示
			strTerm = String.format(Label.BP_Term_Out, new List<String>{String.valueOf(finance.Term__c)});
			
			BP_Previous = String.format(Label.BP_Previous, new List<String>{String.valueOf(finance.Term__c)});
			
			BS_BeforePrevious = String.format(Label.BS_BeforePrevious, new List<String>{String.valueOf(finance.Term__c - 1)});
			
			List<Finance__c> finc02 = new List<Finance__c>();
			finc02 = [SELECT Id,
											Term__c,
											BS_Cash__c,
											BS_NR__c,
											BS_AR__c,
											BS_PROD__c,
											BS_WIPRM__c,
											BS_SE__c,
											BS_STLR__c,
											BS_PRP__c,
											BS_PRC__c,
											BS_TA__c,
											BS_OCA__c,
											BS_LAC__c,
											BS_CATT__c,																		
											BS_DP__c,
											BS_Land__c,
											BS_IFA__c,
											BS_IS__c,
											BS_LTLR__c,
											BS_DE__c,
											BS_PR__c,
											BS_OFA__c,
											BS_LAF__c,
											BS_FATT__c,
											BS_DA__c,
											BS_ASTT__c,
														
											BS_NP__c,
											BS_AP__c,
											BS_STD__c,
											BS_OAP__c,
											BS_AE__c,
											BS_ADR__c,
											BS_DR__c,
											BS_ITP__c,
											BS_CTP__c,
											BS_AC__c,
											BS_OCL__c,
											BS_CLTT__c,
											BS_LTD__c,
											BS_Bond__c,
											BS_AF__c,
											BS_OLTL__c,
											BS_LLTT__c,
											BS_LBTT__c,
											BS_CAP__c,
											BS_CS__c,
											BS_RE__c,
											BS_TS__c,
											BS_Other__c,
											BS_NATT__c,
											BS_LNAT__c,
														
											BS_PLFlg__c,
											Customer_Name__c,	// 顧客.顧客名
											Customer__c				// 顧客名
														
								FROM Finance__c
								WHERE Term__c = :finance.Term__c - 1
									AND Customer__c = :finance.Customer__c
			];
			
			if (!finc02.isEmpty()) {			
				finance02 = finc02[0];
			}

			
		} catch (exception ex){
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));		
			return;
		}
		
		dispSwitch = 'DispBSPL';
		
	}
	/**
	* clickLinkKokyakuShousaiJouhou
	* 「顧客詳細情報へ」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 
	* @modified :   
	*/
	public Pagereference clickLinkKokyakuShousaiJouhou(){
		try{
			// 顧客を取得 
			customer = HearingDAO.getCustomerById(finance.Customer__c);
			// 顧客レコードが取得出来ない場合
			if(customer == null || String.isBlank(String.valueOf(customer.Id))){
				// エラーを表示
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,
																Label.Msg_Error_H_NotCustomer));
				return null;
			}
			Pagereference pageKokyaku = new Pagereference('/' + customer.Id);
			pageKokyaku.setRedirect(true);
			return pageKokyaku;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
		return null;
	}
	/**
	* actionButton
	* 「ＢＳ・ＰＬ入力画面へ」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 
	* @modified :   
	*/
	public Pagereference actionButton(){

		try{
			// ＢＳ・ＰＬ入力画面へ遷移
			Pagereference BSPLInputPage= Page.BSPLInputPage;
			// 現在のページURLを取得
			String strUrl = Apexpages.currentPage().getUrl();
			// 現在のページURLをセット
			BSPLInputPage.getParameters().put(CommonConst.STR_PARAMETER_RETURL, strUrl);
			// URLパラメータにファイナンス情報obj.レコードIDをセット
			BSPLInputPage.getParameters().put(CommonConst.STR_PARAMETER_ID, finance.Id);
			// URLパラメータにファイナンス情報obj.顧客obj.カスタムオブジェクトIDをセット
			BSPLInputPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, finance.Customer__c);
			BSPLInputPage.setRedirect(true);
			return BSPLInputPage;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}

		return null;
	}

	/**
	* actionButton
	* 「ヒヤリング情報入力へ」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 
	* @modified :   
	*/
	public Pagereference hearringInput(){
		try{
			// ヒアリング入力画面へ遷移
			Pagereference hearingInputPage= Page.HearingInputPage;
			// 現在のページURLを取得
			String strUrl = Apexpages.currentPage().getUrl();
system.debug('@@@@@@@@@@@@@@@@@@@@@ strUrl = ' + strUrl);
			// 現在のページURLをセット
			hearingInputPage.getParameters().put(CommonConst.STR_PARAMETER_RETURL, strUrl);
			// URLパラメータにファイナンス情報obj.レコードIDをセット
			hearingInputPage.getParameters().put(CommonConst.STR_PARAMETER_ID, finance.Id);
			// URLパラメータにファイナンス情報obj.顧客obj.カスタムオブジェクトIDをセット
			hearingInputPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, finance.Customer__c);
			hearingInputPage.setRedirect(true);
			return hearingInputPage;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
		return null;
	}

	/**
	* actionButton
	* 「ヒヤリング情報詳細へ」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 
	* @modified :   
	*/
	public Pagereference hearringOutput(){
		try{
			// ヒアリング情報詳細へ遷移
			Pagereference hearingOutputPage= Page.HearingOutputPage;
			// 現在のページURLを取得
			String strUrl = Apexpages.currentPage().getUrl();
			// 現在のページURLをセット
			hearingOutputPage.getParameters().put(CommonConst.STR_PARAMETER_RETURL, strUrl);
			// URLパラメータにファイナンス情報obj.レコードIDをセット
			hearingOutputPage.getParameters().put(CommonConst.STR_PARAMETER_ID, finance.Id);
			// URLパラメータにファイナンス情報obj.顧客obj.カスタムオブジェクトIDをセット
			hearingOutputPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, finance.Customer__c);
			hearingOutputPage.setRedirect(true);
			return hearingOutputPage;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
		return null;
	}
	/**
	* resetSessionTimeout
	* カスタム設定を取得する
	* @param 	: なし
	* @return	: なし
	* @created  : 2015/12/03 KSVC Thao Doan Phuong
	* @modified : 2015/12/08 KSVC THAO DOAN PHUONG Deleted
	*/
	/*public void resetSessionTimeout(){
		List<FC_TIMEOUT__c> fcTimeoutList =  [select FC_TIMEOUT__c From FC_TIMEOUT__c];
	}*/
	/**
	* resetSessionTimeout
	* PERSONAタイムアウト回避
	* @param 	: なし
	* @return	: なし
	* @created  : 2015/12/08 KSVC THAO DOAN PHUONG
	* @modified : 
	*/
	public void resetSessionTimeout(){
		try{
			FC_TIMEOUT__c fcTimeOut = HearingDAO.getFcTimeOut();
			strFCTimeOut = fcTimeOut == null || fcTimeOut.FC_TIMEOUT__c == null ? '0' : String.valueOf(fcTimeOut.FC_TIMEOUT__c);
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
	}
}