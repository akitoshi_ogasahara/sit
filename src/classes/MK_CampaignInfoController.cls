public with sharing class MK_CampaignInfoController{

	public CampaignManagement__c record {get;set;}
	public Boolean contractantFlag {get;set;}

	public class IncentiveImage{
		public ID AttID {get;set;}
		public String AttStyle {get;set;}

		public IncentiveImage(Id attid,String attStyle){
			this.attId = attId;
			this.attStyle = attStyle;
		}
	}

	public MK_CampaignInfoController(){
		CntstRgltnUrl = '';
	}
	//インセンティブ画像URL
	private List<IncentiveImage> IncentiveImageUrls;
	public List<IncentiveImage> getIncentiveImageUrls(){
		//空の場合も存在するため、null比較
		if(IncentiveImageUrls == null){
			IncentiveImageUrls = new List<IncentiveImage>();
			if(record == null) return IncentiveImageUrls;
			if(!String.isBlank(record.Id)){
				for(Attachment att: E_AttachmentDao.getRecsByParentId(record.id)){
					if(att.Name.mid(2,1) <> 'C'){
						IncentiveImageUrls.add(new IncentiveImage(att.Id,'baton-incentive-image'));
					}else if(contractantFlag){
						IncentiveImageUrls.add(new IncentiveImage(att.Id,'baton-incentive-image-c'));
					}
				}
			}
		}
		return IncentiveImageUrls;
	}
	//Attachment URLPrefix
	private String fileURLPre;
	private String getfileURLPre(){
		if(String.isEmpty(fileURLPre)) fileURLPre = URL.getSalesforceBaseUrl().toExternalForm() + '/' + Site.getName() + '/servlet/servlet.FileDownload?file=';
		return fileURLPre;
	}
	private String CntstRgltnUrl;
	public String getCntstRgltnUrl(){
		system.debug('★start');
		if(String.isBlank(CntstRgltnUrl)){
			system.debug('★1 record.E_MessageMaster_UpsertKey__c='+record.E_MessageMaster_UpsertKey__c);
			if(record == null) return CntstRgltnUrl;
			if(String.isBlank(record.E_MessageMaster_UpsertKey__c)) return CntstRgltnUrl;
			//toDo 既存Daoが使えないか検討
			List<E_MessageMaster__c> emm = [Select CMSPreviewURL__c From E_MessageMaster__c Where UpsertKey__c =: record.E_MessageMaster_UpsertKey__c limit 2];
			system.debug('★2 emm.size()='+emm.size());
			if(emm.size() == 1) CntstRgltnUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + Site.getName() + emm[0].CMSPreviewURL__c.replaceAll('apex/','');
			system.debug('★3 CntstRgltnUrl='+CntstRgltnUrl);
		}
		return CntstRgltnUrl;
	}
	public String getBaseUrl(){
		return URL.getSalesforceBaseUrl().toExternalForm() + '/' + Site.getName();
	}

}