@isTest(SeeAllData=false)
public with sharing class TestE_AnnuitySearchExtender {

    private static testMethod void testAnnuitySearch() {
		E_Policy__c policy = new E_Policy__c();
		Account acc = new Account(Name ='test' );
        insert acc;
        Contact con = new Contact( LastName='test',AccountId=acc.Id );
        insert con;

        // テストユーザ設定
        User u = TestE_TestUtil.createUser(true, 'TESTUSER', 'システム管理者');
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false, u.Id);
        idcpf.FLAG02__c = '1';
        idcpf.FLAG04__c = '1';
        insert idcpf;

        PageReference resultPage;

        System.runAs(u) {
        	// テスト開始
        	Test.startTest();

	    	PageReference pref = Page.E_AnnuitySearchSVE;
            pref.getParameters().put('cId', con.id);
	    	Test.setCurrentPage(pref);

	    	Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	    	E_AnnuitySearchController controller = new E_AnnuitySearchController(standardcontroller);
	    	E_AnnuitySearchExtender extender = controller.getExtender();

	    	controller.NameKana_val.SkyEditor2__Text__c = 'ﾃｽﾄ';

			extender.preSearch();
	    	resultPage = extender.pageAction();

	    	System.assertEquals(controller.NameKana_val.SkyEditor2__Text__c, 'テスト');

	    	// テスト終了
	    	Test.stopTest();
        }

    	// 確認
        System.assertEquals(null, resultPage);
    }
}