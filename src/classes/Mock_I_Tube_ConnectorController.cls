public with sharing class Mock_I_Tube_ConnectorController {
	
	public String getReqType() {
		if('https://www.nntube.jp' == Label.E_DOMAIN_NN_TUBE){
			return '';
		}
		
		return 'st';
	}
	
	public String getMetaUrl() {
		return Label.E_DOMAIN_NN_TUBE + '/api/get_mediainfo_forlink.aspx';
	}
	
	public String getSsoUrl() {
		return Label.E_DOMAIN_NN_TUBE + '/auth.aspx';
	}
	
	public String getUserId() {
		
		String uId = UserInfo.getUserId();
		
		System.debug('■Mock_I_Tube_ConnectorController.getUserId():' + uId);
		
		return uId;
	}
	
	public String getSfDomain() {
		final String PARAM_IRIS_DOMAIN = 'domain';
		
		String siteName = Site.getName();
		String path = String.isNotBlank(siteName) ? '/' + siteName : '';
		String domain = URL.getSalesforceBaseUrl().toExternalForm() + path;
		
		System.debug('■Mock_I_Tube_ConnectorController.getSfDomain():' + domain);
		
		return domain;
	}
}