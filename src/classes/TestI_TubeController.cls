@isTest
private class TestI_TubeController {
    
    static testMethod void initTest() {
        Test.startTest();
            PageReference pageRef = Page.IRIS_Tube;
            Test.setCurrentPage(pageRef);
            //pageRef.getParameters().put('from','1');

            I_TubeController tubeCon = new I_TubeController();
            PageReference pr = tubeCon.pageAction();
        Test.stopTest();
        //System.assertEquals(null, pr);
    }
    
    @isTest static void initTest2() {
        Test.startTest();
            PageReference pageRef = Page.IRIS_Tube;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put(I_TubeController.PARAM_TUBE_URL,I_TubeController.PARAM_VALUE_NNTUBE);

            I_TubeController tubeCon = new I_TubeController();
            PageReference pr = tubeCon.pageAction();
        Test.stopTest();
        System.assertNotEquals(null, pr);
    }

    @isTest static void initTest3() {
        Test.startTest();
            PageReference pageRef = Page.IRIS_Tube;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put(I_Const.URL_PARAM_CATEGORY,I_Const.MENU_CATEGORY_ALIAS_LIB_TOOL);

            I_TubeController tubeCon = new I_TubeController();
            PageReference pr = tubeCon.pageAction();
        Test.stopTest();
        System.assertNotEquals(null, pr);
    }

    @isTest static void initTest4() {
        Test.startTest();
            PageReference pageRef = Page.IRIS_Tube;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put(I_Const.URL_PARAM_CATEGORY,I_Const.MENU_CATEGORY_ALIAS_LIB_MNT);

            I_TubeController tubeCon = new I_TubeController();
            PageReference pr = tubeCon.pageAction();
        Test.stopTest();
        System.assertEquals(null, pr);
    }
    @isTest static void initTest5() {
        Test.startTest();
            PageReference pageRef = Page.IRIS_Tube;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put(I_Const.URL_PARAM_CATEGORY,I_Const.MENU_CATEGORY_ALIAS_NEW_OPP);

            I_MenuMaster__c menu = new I_MenuMaster__c(MainCategory__c=I_Const.MENU_CATEGORY_ALIAS_NEW_OPP);
            insert menu;

            I_TubeController tubeCon = new I_TubeController();
            PageReference pr = tubeCon.pageAction();
        Test.stopTest();
        System.assertNotEquals(null, pr);
    }
    @isTest static void initTest6() {
        String docNum = '111-111';
        PageReference pageRef = Page.IRIS_Tube;
        Test.setCurrentPage(pageRef);
        Test.startTest();
            ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_CATEGORY,I_Const.MENU_CATEGORY_ALIAS_LIB_TOOL);
            Apexpages.currentPage().getParameters().put(I_Const.URL_PARAM_DOC_NUM,docNum);

            I_TubeController tubeCon = new I_TubeController();
            PageReference pr = tubeCon.pageAction();
        Test.stopTest();
        System.assertNotEquals(null, pr);
    }
    @isTest static void getURLValuesTest() {
        Test.startTest();
            PageReference pageRef = Page.IRIS_Tube;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put(I_Const.URL_PARAM_CATEGORY,I_Const.MENU_CATEGORY_ALIAS_NEW_OPP);

            I_TubeController tubeCon = new I_TubeController();
            tubeCon.pageAction();
            String urlValues=tubeCon.getURLValues();
        Test.stopTest();
        System.assertNotEquals(null, urlValues);
    }
}