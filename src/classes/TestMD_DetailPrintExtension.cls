/**
 * 
 */
@isTest
private class TestMD_DetailPrintExtension {

	/**
	 * void init()
	 * 正常
	 */
	static testMethod void initTest01() {
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		Test.setCurrentPage(pr);

		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailPrintExtension ext = new MD_DetailPrintExtension(stdController);
		String zoom = ext.getZoom();
		ext.init();

		Test.stopTest();

		// [IRIS] ログをコントローラーで取得しないように変更したため削除
		// assert
		//List<E_Log__c> logList = [Select Id From E_Log__c];
		//System.assertEquals(1, logList.size());
	}
	
	/**
	 * void init()
	 * Exception
	 */
	static testMethod void initTest02() {
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		Test.setCurrentPage(pr);

		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailPrintExtension ext = new MD_DetailPrintExtension(stdController);
		ext.logCondition.headAccept = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
		ext.logCondition.headAccept += '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
		ext.logCondition.headAccept += '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
		ext.init();

		Test.stopTest();

		// [IRIS] ログをコントローラーで取得しないように変更したため削除
		// assert
		//List<E_Log__c> logList = [Select Id, IsError__c, RecordId__c From E_Log__c];
		//System.assertEquals(1, logList.size());
		//System.assertEquals(true, logList[0].IsError__c);
		//System.assertEquals(doctor.Id, logList[0].RecordId__c);
	}

	/* Test Data　位置情報固定 */
	private static MD_Doctor__c createDoctor(String name, String hosName) {
		MD_Doctor__c doc = new MD_Doctor__c();
		doc.Name = name;
		doc.HospitalName__c = hosName;
		doc.ParticularDoctor__c = 'Y';
		doc.Blood__c = '有';
		doc.Electrocardiogram__c = '有';
		doc.State__c = '東京都';
		doc.Address__c = '中央区日本橋1-1-1';
		doc.Phone__c = '03-1234-5678';
		doc.ConsultationTime__c = '10:00 ~ 18:00';
		doc.Note__c = 'test memo';
		doc.DoctorNumber__c = '99999';
		return doc;
	}
}