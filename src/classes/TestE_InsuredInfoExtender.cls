@isTest(seealldata=false)
public with sharing class TestE_InsuredInfoExtender {
    
    // test data
    private static User user;
    private static E_IDCPF__c idcpf;
    private static Account account;
    private static Contact contact;
    private static RecordType recordType_COLI;
    private static RecordType recordType_SPVA;
    private static List<E_Policy__c> epolicyList;		// E_Policy_c(保険契約ヘッダ)
    private static List<E_COVPF__c> covpfList;			// E_COVPF__c(個人保険特約)
    
    //　正常、coliレコードが存在する場合個人保険特約レコードを参照出来ているかの確認
    static testMethod void testPageAction01() {
        
        createDataCommon();
        PageReference pref = Page.E_InsuredInfo;
        pref.getParameters().put('cId', contact.Id);
        pref.getParameters().put('cName', 'lastName firstName');
        Test.setCurrentPage(pref);
        
        Test.startTest();
        
        system.runAs(user){
            
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(contact);
            E_InsuredInfocontroller controller	= new E_InsuredInfocontroller(standardcontroller);
            E_InsuredInfoExtender extender		= controller.getExtender();
            
            extender.init();
            
            List<E_InsuredInfoController.InsuredListItem> coliInsuredList = new List<E_InsuredInfoController.InsuredListItem>();
            List<E_InsuredInfoController.InsuredListItem> spvaInsuredList = new List<E_InsuredInfoController.InsuredListItem>();
            for(E_InsuredInfoController.InsuredListItem item : extender.extension.InsuredList.Items) {
                if (item.record.RecordTypeId == recordType_COLI.Id) {
                    coliInsuredList.add(item);
                } else {
                    spvaInsuredList.add(item);
                }
            }
            
            system.assertEquals(2, extender.extension.InsuredList.Items.size());
            system.assertEquals(true, extender.checkConsistency());
            system.assertEquals('一時払変額終身保険Ⅰ型', coliInsuredList.get(0).record.COMM_ZCOVRNAM__c);
            system.assertEquals('支払中', coliInsuredList.get(0).record.COMM_ZRSTDESC__c);
            system.assertEquals('一時払変額年金保険 (08) Ａ型', spvaInsuredList.get(0).record.COMM_ZCOVRNAM__c);
            system.assertEquals('契約中', spvaInsuredList.get(0).record.COMM_ZRSTDESC__c);
            
        }
        
        Test.stopTest();
        
        
    }
    
    //　異常、不正パラメータが渡ってきた場合
    static testMethod void testPageAction02() {
        
        createDataCommon();
        PageReference pref = Page.E_InsuredInfo;
        pref.getParameters().put('cId', contact.Id);
        pref.getParameters().put('cName', 'パラメタ不正');
        Test.setCurrentPage(pref);
        
        Test.startTest();
        
        system.runAs(user){
            
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(contact);
            E_InsuredInfocontroller controller	= new E_InsuredInfocontroller(standardcontroller);
            E_InsuredInfoExtender extender		= controller.getExtender();
            
            extender.init();
            system.assertEquals(false, extender.checkConsistency());
        }    	
        
        Test.stopTest();
        
    }
    
    static void createDataCommon() {
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            // User
            Profile profile = [Select Id, Name From Profile Where Name = 'システム管理者' Limit 1];
            user = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
            );
            insert user;
            
            // ID管理DB
            idcpf = new E_IDCPF__c(
                FLAG01__c = '1',
                FLAG02__c = '1',
                FLAG04__c = '1',
                User__c = user.Id
            );
            insert idcpf;
            
            // Account
            account = new Account(Name = 'testAccount');
            insert account;
            
            // Contact
            contact = new Contact(	LastName = 'lastName', 
                                  FirstName = 'firstName',
                                  AccountId = account.Id, 
                                  E_COMM_ZCLADDR__c = '東京駅　日本橋駅近く  アンティークビアバル  まるで豪華客船の様な店内は必見',
                                  E_CLTPF_ZKNJSEX__c = '男性',
                                  E_CLTPF_DOB__c = '19800723');
            insert contact;
            
            recordType_COLI = [Select Id From RecordType Where DeveloperName = :E_Const.POLICY_RECORDTYPE_COLI LIMIT 1];
            recordType_SPVA = [Select Id From RecordType Where DeveloperName = :E_Const.POLICY_RECORDTYPE_SPVA LIMIT 1];
            
            // 保険契約ヘッダ
            epolicyList = new List<E_Policy__c>();
            E_Policy__c policy1 = new E_Policy__c(RecordTypeId = recordType_COLI.Id,
                                                  Insured__c = contact.id,
                                                  COMM_CHDRNUM__c = '000123',
                                                  COMM_OCCDATE__c = '20140123',
                                                  COMM_STATCODE__c = 'C');
            epolicyList.add(policy1);
            E_Policy__c policy2 = new E_Policy__c(RecordTypeId = recordType_SPVA.Id,
                                                  Insured__c = contact.id,
                                                  COMM_CHDRNUM__c = '000125',
                                                  COMM_OCCDATE__c = '20141223',
                                                  COMM_ZCOVRNAM__c = '一時払変額年金保険 (08) Ａ型',
                                                  COMM_ZRSTDESC__c = '契約中');
            epolicyList.add(policy2);
            insert epolicyList;
            policy1 = [select id from E_Policy__c where RecordTypeId =: recordType_COLI.Id];
            
            // 個人保険特約
            covpfList = new List<E_COVPF__c>();
            E_COVPF__c cov1 = new E_COVPF__c(E_Policy__c = policy1.id, 
                                             COMM_ZCOVRNAM__c = '一時払変額終身保険Ⅰ型',
                                             COLI_ZPSTDESC__c = '支払中',
                                             COLI_ZCRIND__c = 'C');
            covpfList.add(cov1);
            E_COVPF__c cov2 = new E_COVPF__c(E_Policy__c = policy1.id, 
                                             COMM_ZCOVRNAM__c = '一時払変額年金保険 (04) B型',
                                             COLI_ZPSTDESC__c = '契約中',
                                             COLI_ZCRIND__c = 'A');
            covpfList.add(cov2);
            insert covpfList;
        }
        insertE_bizDataSyncLog();
    }
    
    private static void insertE_bizDataSyncLog(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
            insert log;
        }    
    }
    
    
}