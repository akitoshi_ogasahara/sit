@isTest
private class TestWY_WebYakkanSearchController {
	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 * 正常(コード桁数：10桁)
	 */
	@isTest static void doSearchCode_Test001() {
		createData();
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		E_Util.systemToday_forTest = Date.newInstance(2018, 11, 01);
		controller.searchCode = 'TP20181101';

		Test.startTest();
		controller.getPreparateLogJSON();
		controller.doSearchCode();
		Test.stopTest();

		System.assertEquals(false, controller.isTop);
		System.assertEquals(true, controller.isCodeResult);
		System.assertEquals(false, controller.hasCodeSearchError);
		System.assertNotEquals(null, controller.searchResult);
		System.assertEquals(3, controller.searchResult.childfileList.size());
	}
	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 * 正常(コード桁数：15桁)
	 */
	@isTest static void doSearchCode_Test002() {
		createData();
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		E_Util.systemToday_forTest = Date.newInstance(2018, 11, 01);
		controller.searchCode = 'TL201811013C8WM';

		Test.startTest();
		controller.doSearchCode();
		Test.stopTest();

		System.assertEquals(false, controller.isTop);
		System.assertEquals(true, controller.isCodeResult);
		System.assertEquals(false, controller.hasCodeSearchError);
		System.assertNotEquals(null, controller.searchResult);
		System.assertEquals(3, controller.searchResult.childfileList.size());
	}
	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 * コード不正（コード値：空）
	 */
	@isTest static void doSearchCode_Test003() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchCode = '';

		Test.startTest();
		controller.doSearchCode();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(true, controller.hasCodeSearchError);
	}
	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 * コード不正（コード桁数：9桁）
	 */
	@isTest static void doSearchCode_Test004() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchCode = 'TP2018';

		Test.startTest();
		controller.doSearchCode();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(true, controller.hasCodeSearchError);
	}
	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 * コード不正（契約日不正）
	 */
	@isTest static void doSearchCode_Test005() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchCode = 'TPyyyymmdd';

		Test.startTest();
		controller.doSearchCode();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(true, controller.hasCodeSearchError);
	}
	/**
	 * 検索コードから｢ご契約のしおり･約款｣を検索
	 * データ不備（該当データなし）
	 */
	@isTest static void doSearchCode_Test006() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchCode = 'TP20181101';

		Test.startTest();
		controller.doSearchCode();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(true, controller.hasCodeSearchError);
	}

	/**
	 * ご契約日/更新日･特約中途付加日から｢ご契約のしおり･約款｣を検索
	 * 正常
	 */
	@isTest static void doSearchContract_Test001() {
		createData();
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		E_Util.systemToday_forTest = Date.newInstance(2018, 11, 01);
		controller.searchContractDate = '2018/11/01';

		Test.startTest();
		controller.doSearchContract();
		Test.stopTest();

		System.assertEquals(false, controller.isTop);
		System.assertEquals(true, controller.isContractResult);
		System.assertEquals(false, controller.hasContractSearchError);
	}
	/**
	 * ご契約日/更新日･特約中途付加日から｢ご契約のしおり･約款｣を検索
	 * 正常（未来日付入力）
	 */
	@isTest static void doSearchContract_Test002() {
		createData();
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		E_Util.systemToday_forTest = Date.newInstance(2018, 11, 01);
		controller.searchContractDate = '2100/11/01';

		Test.startTest();
		controller.doSearchContract();
		Test.stopTest();

		System.assertEquals(false, controller.isTop);
		System.assertEquals(true, controller.isContractResult);
		System.assertEquals(false, controller.hasContractSearchError);
	}
	/**
	 * ご契約日/更新日･特約中途付加日から｢ご契約のしおり･約款｣を検索
	 * 日付不備（空）
	 */
	@isTest static void doSearchContract_Test003() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchContractDate = '';

		Test.startTest();
		controller.doSearchContract();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(false, controller.isContractResult);
		System.assertEquals(true, controller.hasContractSearchError);
	}
	/**
	 * ご契約日/更新日･特約中途付加日から｢ご契約のしおり･約款｣を検索
	 * 日付不備（日付不正）
	 */
	@isTest static void doSearchContract_Test004() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchContractDate = 'yyyy/mm/dd';

		Test.startTest();
		controller.doSearchContract();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(false, controller.isContractResult);
		System.assertEquals(true, controller.hasContractSearchError);
	}
	/**
	 * ご契約日/更新日･特約中途付加日から｢ご契約のしおり･約款｣を検索
	 * 日付不備（2018/11/01以前の日付）
	 */
	@isTest static void doSearchContract_Test005() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		controller.searchContractDate = '2015/11/01';

		Test.startTest();
		controller.doSearchContract();
		Test.stopTest();

		System.assertEquals(true, controller.isTop);
		System.assertEquals(false, controller.isContractResult);
		System.assertEquals(true, controller.hasContractSearchError);
	}

	/**
	 * 検索(最新の｢ご契約のしおり･約款｣を検索する)
	 * 正常
	 */
	@isTest static void doSearchNew_Test001() {
		createData();
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		WY_WebYakkanSearchController controller = new WY_WebYakkanSearchController();
		E_Util.systemToday_forTest = Date.newInstance(2018, 11, 01);

		Test.startTest();
		controller.doSearchNew();
		Test.stopTest();

		System.assertEquals(false, controller.isTop);
		System.assertEquals(true, controller.isLatestResult);
	}


	// テストデータ作成
	private static void createData(){
		List<String> insTypeList = new List<String>{'TP','TL','TN','IA','IF'};
		List<String> agencyCodeList = new List<String>{'汎用','3C8WM','78355','1N20M','1N20J'};

		// 親レコード
		List<I_ContentMaster__c> parents = new List<I_ContentMaster__c>();
		for(Integer i = 0; i < 5; i++){
			I_ContentMaster__c parent = new I_ContentMaster__c();
			parent.Name = 'ご契約のしおり・約款' + String.valueOf(i);
			parent.InsType__c = insTypeList[i];
			parent.AgencyCode__c = agencyCodeList[i];
			if(math.mod(i, 2) == 0){
				parent.GuideAgreeDiv__c = '新契約用';
			}else{
				parent.GuideAgreeDiv__c = '更新・特約中途付加用';
			}
			E_Util.systemToday_forTest = Date.newInstance(2018, 11, 01);
			parent.ContractDateFrom__c = E_Util.SYSTEM_TODAY();
			parent.ContractDateTo__c = E_Util.SYSTEM_TODAY().addMonths(i + 1);
			parent.DisplayFrom__c = E_Util.SYSTEM_TODAY();
			parent.ValidTo__c = E_Util.SYSTEM_TODAY().addMonths(i + 1);
			parent.IsYakkanParentFlg__c = true;
			parent.isSitePublishFlg__c = true;
			parents.add(parent);
		}
		insert parents;

		//　子レコード
		List<String> yakkanTypeList = new List<String>{'本編','別添1','別添2'};
		List<I_ContentMaster__c> children = new List<I_ContentMaster__c>();
		for(I_ContentMaster__c parent : parents){
			for(String yType : yakkanTypeList){
				I_ContentMaster__c child = new I_ContentMaster__c();
				child.ParentContent__c = parent.Id;
				child.Name = parent.Name + yType;
				child.YakkanType__c = yType;
				child.IsYakkanParentFlg__c = false;
				child.isSitePublishFlg__c = true;
				children.add(child);
			}
		}
		insert children;

		//　添付ファイル
		List<Attachment> attachments = new List<Attachment>();
		Integer count = 0;
		for(I_ContentMaster__c child : children){
			Attachment att = new Attachment();
			if(count != 3){
				att.Name = child.YakkanType__c + '.pdf';
				att.ParentId = child.Id;
				if(count == 0){
					Integer byteSize = (Integer)Math.pow(10, 6) + 1;
					String s = ('0').repeat(byteSize);
					Blob myBlob = Blob.valueOf(s);
					att.body = myBlob;
				}else{
					att.Body = Blob.valueOf('testAttachment');
				}
				attachments.add(att);
			}
			count ++;
		}
		insert attachments;
	}
}