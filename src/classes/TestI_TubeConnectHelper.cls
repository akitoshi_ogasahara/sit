@isTest
private class TestI_TubeConnectHelper {
	
	static testMethod void getInstanceTest() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			System.assertNotEquals(null, instance);
		Test.stopTest();
	}
	
	static testMethod void getUrlParamMapForNNTubeTest() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			Map<String,String> paramMap = instance.getUrlParamMapForNNTube();
		
			String siteName = Site.getName();
			String path = String.isNotBlank(siteName) ? '/' + siteName : '';
			String domain = URL.getSalesforceBaseUrl().toExternalForm() + path;

			System.assertNotEquals(null, paramMap);
			System.assertEquals(domain, paramMap.get(I_TubeConnectHelper.PARAM_IRIS_DOMAIN));

			System.debug(paramMap);
		Test.stopTest();
	}

	static testMethod void getUrlParamsForNNTubeTest() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			Map<String,String> paramMap = instance.getUrlParamMapForNNTube();
			String param = instance.getUrlParamsForNNTube();
			System.assertEquals(paramMap.size()-1, param.countMatches('&'));
			System.debug(param);
		Test.stopTest();
	}
	static testMethod void getUrlParamsForNNTubeTest2() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			Map<String,String> paramMap = instance.getUrlParamMapForNNTube();
			String param = instance.getUrlParamsForNNTube();
			System.assertEquals(paramMap.size()-1, param.countMatches('&'));
			param = instance.getUrlParamsForNNTube('type=123');
			System.assertEquals(paramMap.size(), param.countMatches('&'));
			System.debug(param);
		Test.stopTest();
	}

	static testMethod void getTubekunImgUrlTest() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			String tubekun = instance.getTubekunImgUrl();
			System.assertEquals(true, tubekun.contains(Label.E_DOMAIN_NN_TUBE + I_TubeConnectHelper.Tube_tubekun_img));
			System.debug('tubekun:'+tubekun);
		Test.stopTest();
	}

	static testMethod void getTubeUrlTest() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			String tubeMetaUrl = instance.getTubeMetaUrl();
			String tubeTopUrl = instance.getTubeTopUrl();
			String movieType = instance.getParams_MovieType();
			String favoriteUrl = instance.getTubeFavoriteUrl();
			String historyUrl = instance.getTubeHistoryUrl();
			String tubeLogMetaUrl = instance.getTubeLogMetaUrl();

			System.assertEquals(Label.E_DOMAIN_NN_TUBE + I_TubeConnectHelper.TUBE_ENDPOINT_META, tubeMetaUrl);
			System.assertEquals(Label.E_DOMAIN_NN_TUBE + I_TubeConnectHelper.TUBE_ENDPOINT_META_LOG, tubeLogMetaUrl);
			System.assertEquals(Label.E_DOMAIN_NN_TUBE + I_TubeConnectHelper.TUBE_ENDPOINT, tubeTopUrl);
			System.assertEquals(true, movieType.contains(I_TubeConnectHelper.TUBE_REQUESTTYPE_MOVIE));
			System.assertEquals(true, favoriteUrl.contains(I_TubeConnectHelper.TUBE_REQUESTTYPE_FAVORITE));
			System.assertEquals(true, historyUrl.contains(I_TubeConnectHelper.TUBE_REQUESTTYPE_HISTORY));
		Test.stopTest();
	}
	//環境依存の機能のためassert省略
	static testMethod void callOnlyTest() {
		I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
		instance.getChannelCode();
		instance.getSessionId();
		instance.getUserId();
		instance.getTubeEnvType();
	}
	static testMethod void cannotConnectTest() {
		Test.startTest();
			I_TubeConnectHelper instance = I_TubeConnectHelper.getInstance();
			String msg = instance.getCANNOT_CONNECT_TUBE_MSG();
			System.assertEquals('ご利用の環境では動画は視聴できません。', msg);
		Test.stopTest();
	}
}