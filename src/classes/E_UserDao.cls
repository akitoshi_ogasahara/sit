/**
 * ユーザデータアクセスオブジェクト  Permissionアクセスオブジェクト
 * CreatedDate 2014/12/04
 * @Author Terrasky
 */
public class E_UserDao {

	/**
	 * ユーザデータをユーザIdから取得する
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static User getUserRecByUserId(Id userId){
		return [
			SELECT
				Name,
				AccountId,
				ContactId,
                IsActive,
				UserType,
				Email,
				Username,
				UserRole.DeveloperName,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				//20170526
				Unit__c,
				UnitName__c,
				//20170526
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.E_AccParentCord__c,
				Contact.Account.ParentId,
				Contact.AccountId,
				Contact.Account.E_CL2PF_ZAGCYNUM__c,
				Contact.E_MenuKindId__c,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.Account.canSelfRegist__c
			FROM User
			WHERE Id = :userId
		];
	}

	/**
	 * ユーザデータをユーザIdから取得する（権限セット付）
	 *												2015.10 追加
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static User getUserWithPermissionsByUserId(Id userId){
		return [
			SELECT
				AccountId,
				ContactId,
				UserType,
				Email,
				Username,
				Department,
				UserRole.DeveloperName,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_ZWEBID__c,
				LastPasswordChangeDate,
				Unit__c,
				MRCD__c,
				IsChangeMailaddress__c,
				IsActive,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.E_AccParentCord__c,
				Contact.E_CL3PF_AGTYPE__c,	//20180528 IRISforSUMISEI
				Contact.Account.ParentId,
				Contact.Account.E_DoCheckIP__c,	//20171106追加 銀行代理店解放
				//20180214 資料発送申込対応 START
				Contact.Account.CannotBeOrder__c,
				Contact.Account.Z1OFFING__c,
				Contact.Account.ParentZ1OFFING__c,
				Contact.Account.E_CL2PF_ZEAYNAM__c,
				//20180214 資料発送申込対応 END
				Contact.AccountId,
				Contact.E_MenuKindId__c,
				Contact.Account.E_CL2PF_ZAGCYNUM__c,
				Contact.E_AccZSHRMAIN__c,
				Contact.E_AccParentName__c,
				Contact.E_Account__c,
				Contact.E_KCHANNEL__c,
				Contact.AgentNum5__c

				// P16-0003 Atria対応開発
				,Name,
				Contact.E_CL3PF_VALIDFLAG__c,
				Contact.E_CL3PF_KQUALFLG__c,
				Contact.E_CL3PF_ZSUBTRM__c,
				Contact.E_CL3PF_ZHEADAY__c,
				Contact.E_CL3PF_ZAGCYNUM__c,
				Contact.Account.Name,
                Contact.Account.E_CL2PF_ZAYKNJNM__c,
				Contact.Account.CLTPCODE__c,
				Contact.Account.E_COMM_ZCLADDR__c,
				Contact.Account.CLTPHONE01__c,
				Contact.Account.E_CL2PF_RCMTAB__c,
				Contact.Account.ZMRCODE__c,
				Contact.Account.Owner.Name,
				Contact.Account.Parent.E_CL1PF_ZBKCODE__c,
				Contact.Account.Parent.E_CL1PF_KMOFCODE__c,
				Contact.Account.Parent.E_CL1PF_ZHEADAY__c,
				Contact.Account.Parent.Name,
				Contact.Account.Parent.Z1OFFING__c,
				Contact.Account.Parent.E_CL1PF_AGCLS__c,
				Contact.Account.Parent.E_CL1PF_KCHANNEL__c,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.E_CL3PF_BRANCH__c,
				Contact.E_CLTPF_CLTPCODE__c,
				Contact.Account.AGNTBR_NM__c,
				Contact.Account.E_CL2PF_ZAYSECT__c,
				Contact.E_CL3PF_ZMEMNUM__c

				//付与された権限セット
				,(Select Id
						, PermissionSetId
						, PermissionSet.Name
						, AssigneeId
				  From PermissionSetAssignments
				  Where PermissionSet.ProfileId = null		//ProfileIdがNullのものが権限セット
					//AND PermissionSet.Name in :E_Const.PERMISSIONSET_NAMES		//NNLink以外もすべて取得
				)

				// P16-0003 Atria対応開発
				,(
					Select
						Id,
						ZIDTYPE__c,
						ZINQUIRR__c,
						AtriaCommissionAuthorityEditable__c,
						CLNTNUM__c
					From
						E_IDCPFs__r
				)
			FROM User
			WHERE Id = :userId
		];
	}
	/**
	 * ユーザデータをユーザIdから取得する
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static Map<Id, User> getUserByIds(Set<Id> uIds){
		return new Map<Id, User>(
							[
			SELECT
				AccountId,
				ContactId,
				UserType,
				Email,
				Username,
				UserRole.DeveloperName,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.E_AccParentCord__c,
				Contact.Account.ParentId,
				Contact.AccountId,
				Contact.Account.E_CL2PF_ZAGCYNUM__c,
				Contact.E_MenuKindId__c,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.Account.canSelfRegist__c
							 FROM User
							 WHERE Id in :uIds
							]);
	}

	/**
	 * ユーザデータを権限セット付でユーザIdから取得する
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static Map<Id, User> getUserWithPermissions(Set<Id> uIds){
		return new Map<Id, User>(
							[SELECT Id
									,Name
									,isActive
									,(Select Id
											, PermissionSetId
											, PermissionSet.Name
											, AssigneeId
									  From PermissionSetAssignments
									  Where PermissionSet.ProfileId = null		//ProfileIdがNullのものが権限セット
										AND PermissionSet.Name in :E_Const.PERMISSIONSET_NAMES)
							 FROM User
							 WHERE Id in :uIds
							]);
	}

	/**
	 *		getPermissionSetForNNLink
	 *			NNLink関連の権限セットのMap
	 */
	private static Map<String, PermissionSet> NNLinkPermissionSets;
	public static Map<String, PermissionSet> getPermissionSetForNNLink(){
		if(NNLinkPermissionSets==null){
			NNLinkPermissionSets = new Map<String, PermissionSet>();
			List<PermissionSet> perSet = [SELECT Id,Name,Label FROM PermissionSet
									WHERE Name in :E_Const.PERMISSIONSET_NAMES];
			for(PermissionSet ps: perSet){
				NNLinkPermissionSets.put(ps.Name, ps);
			}
			System.assert(NNLinkPermissionSets.size() == E_Const.PERMISSIONSET_NAMES.size()
							, 'PermissionSetレコード取得件数が不足しています。');
		}
		return NNLinkPermissionSets;
	}

	/**
	 *		MRユーザ取得メソッド
	 */
	//MR取得 key:MRコード, value:ユーザId
	private static Map<String, Id> mrUserMap;
	public static Map<String, Id> getMrUserMap() {
		if (mrUserMap == null) {
			mrUserMap = new  Map<String, Id>();
			for (User u : [Select Id, MRCD__c, UserRole.ParentRoleId
							From User
							where userType = 'Standard'
								and MRCD__c != null
								and isActive = true]) {
				mrUserMap.put(u.MRCD__c, u.Id);
			}
		}
		return mrUserMap;
	}

	/**
	 *		MRユーザ取得メソッド
	 */
	//MR取得 key:MRコード, value:ユーザ
	private static Map<String, User> mrUserRecordMap;
	public static Map<String, User> getMrUserRecordMap() {
		if (mrUserMap == null) {
			mrUserRecordMap = new  Map<String, User>();
			for (User u : [Select Id, MRCD__c, UserRole.ParentRoleId
							From User
							where userType = 'Standard'
								and MRCD__c != null
								and isActive = true]) {
				mrUserRecordMap.put(u.MRCD__c, u);
			}
		}
		return mrUserRecordMap;
	}

	//MRコードからUserIDの取得
	public static Id getActiveUserIdByMRCD(String mrCD){
		Map<String, Id> userMap = new Map<String, Id>();
		userMap = getMrUserMap();

		if(userMap.containsKey(mrCD)){
			return userMap.get(mrCD);
		}

		return null;
	}

	/**
	 *		拠点長ユーザ取得メソッド
	 */
	//MR取得 key:ロールID, value:ユーザID
	private static Map<String, Id> mrManagerMap;
	public static Map<String, Id> getMrManagerMap() {
		if(mrManagerMap == null){
			mrManagerMap = new Map<String, Id>();
			for(User u : [Select Id, UserRoleId From User
						  Where UserType = 'standard'
						   and  UserRole.Name like '%拠点長%'
						   and  isActive = true]){
				mrManagerMap.put(u.UserRoleId, u.Id);
			}
		}
		return mrManagerMap;
	}

	/**
	 *		本社営業部拠点長ユーザ取得メソッド
	 */
	public static ID getHQManagerId() {
		ID result = [Select Id, UserRoleId From User
					 Where UserType = 'standard'
					  and  UserRole.DeveloperName =: E_Const.ROLE_DEVNAME_HQ_MANAGER
					  and  isActive = true].get(0).Id;

		return result;
	}

/*
	//MR取得 key:MRコード, value:ユーザID
	private static Map<String, Id> mrUserMap;
	public static Map<String, Id> getMrUserMap() {
		if (mrUserMap == null) {
			mrUserMap = new  Map<String, Id>();
			for (User u : [Select Id, MRCD__c From User
							where userType = 'Standard'
								and MRCD__c != null
								and isActive = true]) {
				mrUserMap.put(u.MRCD__c, u.id);
			}
		}
		return mrUserMap;
	}

	//MRコードからUserIDの取得
	public static Id getActiveUserIdByMRCD(String mrCD){
		return getMrUserMap().get(mrCD);
	}

	/**
	 * ユーザデータをMR番号から取得する
	 * @param mrCode: MR番号
	 * @return User: ユーザ情報
	 */
	public static User getUserRecByMRCode(String mrCode){
		return [
			SELECT
				AccountId,
				ContactId,
				UserType,
				Email,
				Username,
				Phone,
				UserRole.DeveloperName,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c
			FROM User
			WHERE MRCD__c = :mrCode
		];
	}

	/*
	 * @param cid コンタクトＩＤ
	 * @return Userレコード
	 */
	public static id getRecByContactId(ID cid){
		if(cid == null) return null;
		List<User> recs = [SELECT Id FROM User Where ContactId =: cid];
		return (recs.size() == 1)? recs[0].id:null;
	}

	/**
	 * ID発行時ユーザ情報更新
	 * @param userRec [ユーザレコード]
	 * @param nnId    [NNLink10桁ID]
	 */
	public static void updateUserRec(User userRec){
		//ユーザ情報をupdate
		update userRec;

	}

	/**
	 * ユーザデータをMR番号から取得する
	 * @param mrCode: MR番号
	 * @return UserList: ユーザ情報
	 */
	public static List<User> getUserRecsByMRCode(List<String> mrCodeList){
		return [
			SELECT
				AccountId,
				ContactId,
				UserType,
				Email,
				Username,
				Phone,
				UserRole.DeveloperName,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				MRCD__c,
				Name
			FROM User
			WHERE MRCD__c IN :mrCodeList
		];
	}

	/**
	 * ユーザ無効化
	 * @param uIds: ユーザIDのセット
	 */
	@future
	public static void updateUserRecsActiveFalse(Set<Id> uIds){
		List<User> recs = [SELECT Id, isActive FROM User Where Id in :uIds];
		for(User rec : recs){
			rec.isActive = false;
		}
		update recs;
	}

	/**
	 * 募集人IDに紐付くユーザを取得
	 */
	public static List<User> getRecsAgentId(Set<Id> contactIdset) {
		List<User> userList = [SELECT
									Id,
									Contact.Id,
									IsActive,
									LastLoginDate,
									LastModifiedDate,
									ContactId,
									Email
								FROM
									User
								WHERE
									Contact.Id in :contactIdset];
		return userList;
	}

	/**
	 * 無効化対象ユーザの項目を取得
	 * @param  Set<Id> 無効化対象ユーザのID
	 * @return  List<User>
	 */
	public static List<User> getFreezeUserListByIds(Set<Id> userIds){
		List<User> userList = [SELECT
									Id,
									UserName,
									LastLoginDate,
									LastModifiedDate,
									IsActive,
									IsPortalEnabled,
									(SELECT
											Id,
											ZSTATUS01__c,
											ZWEBID__c,
											ZIDTYPE__c,
											ZIDOWNER__c,
											ZINQUIRR__c
										FROM E_IDCPFs__r
										WHERE ZSTATUS01__c = :E_Const.PW_STATUS_OK
										  AND ZDSPFLAG02__c = :E_Const.ZDSPFLAG02_IRIS) //TODO ID適正化
								FROM User
								WHERE id in :userIds];
		return userList;
	}

	/**
	 * ライセンスがPowerPartnerでMRが担当支社となっている事務所の募集人一覧を取得
	 * @return  List<User>
	 */
	public static List<User> getPartnerUserRecByUserType(String sortField, Boolean sortIsAsc) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', LastLoginDate'
					+ ', Contact.Account.Name'
					+ ', UserType'
					+ ', Profile.Name'
					+ ' FROM User'
					+ ' WHERE UserType = \'PowerPartner\''
					+ ' AND Contact.Account.ParentId != \'\'';

		if (String.isNotBlank(sortField)) {
			soql += ' ORDER BY ' + sortField;

			if (!sortIsAsc) {
				soql += ' DESC NULLS LAST';
			}
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	/**
	 * 募集人IDに紐付くユーザをID管理とともに取得
	 * JIRA_#51 170208
	 */
	public static List<User> getRecsAgentIdWithEIDC(Set<Id> contactIdset) {
		List<User> userList = [SELECT
									Id,
									Contact.Id,
									IsActive,
									LastLoginDate,
									LastModifiedDate,
									(SELECT Id, ZSTATUS01__c FROM E_IDCPFs__r)
								FROM
									User
								WHERE
									Contact.Id in :contactIdset];
		return userList;
	}

	 /**
	 * [挙積情報] 営業部支社名と一致するユーザと、それに紐づく挙績情報を取得
	 * @param  brName 営業部支社名
	 * @return List<User>
	 */
	public static List<User> getUserRecsWithEmployeeSales(String sWhere,String busdate){
	 	String soql = 'SELECT'
				+' Id'
				+ ' ,Name'
				+ ' ,MemberNo__c'
				+ ',(SELECT Id'
					+ ' ,NBWANP_FSYTD__c'
					+ ' ,AchiveRate_FSYTD__c'
					+ ' ,NBWANP_ProtectionYTD__c'
					+ ' ,AchiveRate_ProtectionYTD__c'
					+ ' ,NBWANP_BACOLIYTD__c'
					+ ' ,AchiveRate_BACOLIYTD__c'
					+ ' ,NBWANP_TotalYTD__c'
					+ ' ,AchiveRate_TotalYTD__c'
					+ ' ,ActiveAgency__c'
					+ ' ,ActiveAgent__c'
					+ ' ,MR__c'
				+ ' FROM E_EmployeeSalesResults__r'
				+ ' Where BusinessDate__c =: busdate)'
				+ ' FROM User'
				+ ' WHERE IsActive = true'
				+ sWhere;
		return Database.query(soql);
	}

	/**
	 * ユーザデータをユーザIdから取得する
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static Map<Id, User> getUserByIdsWithBatch(Set<Id> uIds){
		return new Map<Id, User>([SELECT
				Id								// ユーザ ID
				,Username						// ユーザ名
				,ContactId						// 取引先責任者 ID
				,Contact.Name					// 取引先責任者 ID,名前
				,Contact.AccountId				// 取引先責任者 ID,取引先 ID
				,Contact.Account.Name			// 取引先責任者 ID,取引先 ID.代理店事務所名
				,Contact.Account.ParentId		// 取引先責任者 ID,取引先 ID.親取引先 ID
				,Contact.Account.Parent.Name	// 取引先責任者 ID,取引先 ID.親取引先 ID.代理店事務所名
			FROM User
			WHERE Id in :uIds
		]);
	}
}