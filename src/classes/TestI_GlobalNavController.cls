@isTest
private class TestI_GlobalNavController {
	static testMethod void employeeTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);
		//テストデータ作成
		createMenuMaster();

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_GlobalNavController ign = new I_GlobalNavController();

				system.assertEquals(ign.getIrisHandler(), E_IRISHandler.getInstance());
				system.assertEquals(ign.getURL_AppSwitch2NNLink(), 'E_AppSwitch?'+I_Const.APPSWITCH_URL_PARAM_APP + '=' + I_Const.APP_MODE_NNLINK);
				system.assertEquals(ign.getIsEmployee(), true);
				system.assertEquals(ign.getPageTitle(), null);
				system.assertEquals(ign.getIsCGW(), false);
				system.assertEquals(ign.getIsGuestUser(), false);
				system.assertEquals(ign.getNameSupported(), null);
				//system.assertEquals(ign.FilterOff(), null);
				//system.assertEquals(ign.getRequiredFormTag(), true);
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				//system.assertEquals(ign.getgNavItems().size(), entries.size()-1);
				 system.assertEquals(ign.getgNavItems().size(), entries.size());
			Test.stopTest();
		}
	}

	static testMethod void guestUserTest() {
		// 実行ユーザ作成
		String userName = 'testUser@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = 'testUser'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		insert actUser;

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);
		pr.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				Account acc = new Account(Name = 'test');
				insert acc;
				E_IRISHandler.getInstance().setPolicyCondition('agency', acc.Name, acc.Id, acc.Name);

				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[0].getValue(), MenuKey__c = 'testMenuKey');
				insert m;
				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);

				I_GlobalNavController ign = new I_GlobalNavController();

				system.assertEquals(ign.getIsEmployee(), false);
				system.assertEquals(ign.getPageTitle(), m.Name);
				system.assertEquals(ign.getIsCGW(), true);
				system.assertEquals(ign.getIsGuestUser(), true);
				system.assertEquals(ign.getNameSupported(), null);
			Test.stopTest();
		}
	}

	static testMethod void getNameSupportedTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				Account acc = new Account(Name = 'test');
				insert acc;
				E_IRISHandler.getInstance().setPolicyCondition('agency', acc.Name, acc.Id, acc.Name);

				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey');
				insert m;
				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);

				I_GlobalNavController ign = new I_GlobalNavController();

				system.assertEquals(ign.getNameSupported(), null);
				//system.assertEquals(ign.getNameSupported(), acc.Name);
			Test.stopTest();
		}
	}

	static testMethod void getgNavItemsTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				Account acc = new Account(Name = 'test');
				insert acc;
				E_IRISHandler.getInstance().setPolicyCondition('agency', acc.Name, acc.Id, acc.Name);

				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey', Default__c = true);
				insert m;
				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);

				I_GlobalNavController ign = new I_GlobalNavController();

				// TODO 3/3までに解除
				//system.assertEquals(ign.getgNavItems().size(), 1);
				//system.assertEquals(ign.getgNavItems().size(), 4);
				// system.assertEquals(ign.getgNavItems().size(), 5);
			Test.stopTest();
		}
	}

	static testMethod void getgNavItemsTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey', Default__c = false);
				insert m;
				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);

				I_GlobalNavController ign = new I_GlobalNavController();

				// TODO 3/3までに解除
				//system.assertEquals(ign.getgNavItems().size(), 1);
				//system.assertEquals(ign.getgNavItems().size(), 4);
				// system.assertEquals(ign.getgNavItems().size(), 5);
			Test.stopTest();
		}
	}

	static testMethod void getHasSubNavTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '5.よくあるご質問', MenuKey__c = 'testMenuKey', Default__c = false);
				insert m;
				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);

				I_GlobalNavController ign = new I_GlobalNavController();
				Boolean subNav = ign.getHasSubNav();

				system.assertEquals(subNav, false);
				//system.assertEquals(ign.getgNavItems().size(), 4);
				// system.assertEquals(ign.getgNavItems().size(), 5);
			Test.stopTest();
		}
	}

	static testMethod void gNavItemTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[0].getValue(), MenuKey__c = 'testMenuKey', Default__c = true);
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];
				I_MenuProvider.getInstance().setAcitveMenuId(mm.Id);

				I_GlobalNavController.gNavItem ni = new I_GlobalNavController.gNavItem();
				ni.record = mm;

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += mm.pages__r[0].Id;
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);

				system.assertEquals(ni.getShowForMainCategory(),true);
				system.assertEquals(ni.isActive, true);
				system.assertEquals(ni.getTopMenu(), mm);
				system.assertEquals(ni.getCategoryLabel(), mm.MainCategoryAlias__c);
				system.assertEquals(ni.getCategoryTopUrl(), retUrl);
			Test.stopTest();
		}
	}

	//メインカテゴリがよくあるご質問の場合
	static testMethod void gNavItemTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = I_Const.MENU_MAIN_CATEGORY_FAQ, MenuKey__c = 'testMenuKey', Default__c = true);
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];
				I_MenuProvider.getInstance().setAcitveMenuId(mm.Id);

				I_GlobalNavController.gNavItem ni = new I_GlobalNavController.gNavItem();
				ni.record = mm;

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += mm.pages__r[0].Id;
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);

				system.assertEquals(ni.getShowForMainCategory(),false);
				system.assertEquals(ni.isActive, true);
				system.assertEquals(ni.getTopMenu(), mm);
				system.assertEquals(ni.getCategoryLabel(), mm.MainCategoryAlias__c);
				system.assertEquals(ni.getCategoryTopUrl(), retUrl);
			Test.stopTest();
		}
	}

	//社員以外のユーザ
	static testMethod void gNavItemTest03() {
		// 実行ユーザ作成
		String userName = 'testUser@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = 'testUser'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		insert actUser;

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LOG, MenuKey__c = 'testMenuKey', Default__c = true);
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];
				I_MenuProvider.getInstance().setAcitveMenuId(mm.Id);

				I_GlobalNavController.gNavItem ni = new I_GlobalNavController.gNavItem();
				ni.record = mm;

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += mm.pages__r[0].Id;
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);

				system.assertEquals(ni.getShowForMainCategory(),false);
				system.assertEquals(ni.isActive, true);
				system.assertEquals(ni.getTopMenu(), mm);
				system.assertEquals(ni.getCategoryLabel(), mm.MainCategoryAlias__c);
				system.assertEquals(ni.getCategoryTopUrl(), retUrl);
			Test.stopTest();
		}
	}

/*
	static testMethod void gNavItemTest03() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[0].getValue(), MenuKey__c = 'testMenuKey', Default__c = true);
				insert m;

				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];
				I_MenuProvider.getInstance().setAcitveMenuId(mm.Id);
				I_MenuProvider.getInstance().menusByCategory.get(entries[1].getValue());

				I_GlobalNavController.gNavItem ni = new I_GlobalNavController.gNavItem();
				//ni.record = mm;

				system.assertEquals(ni.getCategoryTopUrl(), null);
			Test.stopTest();
		}
	}
*/

	static testMethod void UrlConverterTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey', Default__c = false);
				insert m;

				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);
				I_GlobalNavController.UrlConverter urlCon = new I_GlobalNavController.UrlConverter();

				system.assertEquals(urlCon.canShow(), true);
			Test.stopTest();
		}
	}

	//インターネット用　代理店ユーザ　ページURLのテスト
	static testMethod void getUrl_via_InternetTesst() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey', Default__c = false);
				insert m;

				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);
				I_GlobalNavController.UrlConverter urlCon = new I_GlobalNavController.UrlConverter();
				String intURL = E_Util.getUrlForNNLinkage();

				system.assertEquals(urlCon.getUrl_via_Internet(),intURL);
			Test.stopTest();
		}
	}

	//共同GW用　代理店ユーザ　ページURLのテスト
	static testMethod void getUrl_via_GWTesst() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey', Default__c = false);
				insert m;

				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);
				I_GlobalNavController.UrlConverter urlCon = new I_GlobalNavController.UrlConverter();
				String gwURL = E_Util.getURLForGWTB(urlCon.getUrl_via_Internet());

				system.assertEquals(urlCon.getUrl_via_GW(),gwURL);
			Test.stopTest();
		}
	}

	static testMethod void getFAQNavItemTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = '2.保有契約', MenuKey__c = 'testMenuKey', Default__c = false);
				insert m;

				I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c);
				I_GlobalNavController ign = new I_GlobalNavController();

				system.assertEquals(ign.getFAQNavItem(),null);
			Test.stopTest();
		}
	}

	//テストデータ作成
	static void createMenuMaster(){
		List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
		List<I_MenuMaster__c> mmList = new List<I_MenuMaster__c>();

		for(Integer num = 0; num < entries.size() ; num ++){
			I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[num].getValue(), MenuKey__c = 'testMenuKey'); 
			mmList.add(m);
		}

		insert mmList;
	}
}