public with sharing class I_MeritTableEntity {
	// calcServiseからのアウトプットJSONパラメータ

	public String keikaNensu {get; set;}
	public String hokenNendo {get; set;}
	public String age {get; set;}
	public String shibouKoudoShogaiSManyen {get; set;}
	public String shibouSeikatsuShogaiSManyen {get; set;}
	public String judaishippeiSManyen {get; set;}
	public String shibouKyufuKin {get; set;}
	public String kijunSManyen {get; set;}
	public String saigaiSManyen {get; set;}
	public String ganNyuinKyufuKinNichigaku {get; set;}
	public String nenkinGetsugaku {get; set;}
	public String uketoriKaisu {get; set;}
	public String uketoriSogaku {get; set;}
	public String nenkinGenka {get; set;}
	public String haraikataBetsuP {get; set;}
	public String nenkanHaraikomiP {get; set;}
	public String pRuikei {get; set;}
	public String mikeikaP {get; set;}
	public String cv {get; set;}
	public String mankiSRuikei {get; set;}
	public String kaiyakuUketoriKin {get; set;}
	public String uketoriKinRuikeiGaku {get; set;}
	public String simpleHenreiRt {get; set;}
	public String sonkinP {get; set;}
	public String koukaGaku {get; set;}
	public String koukaGakuRuikei {get; set;}
	public String jisshitsuFutanGaku {get; set;}
	public String jisshitsuHenreiRt {get; set;}
	public String shisanKeijoGaku {get; set;}



}