global with sharing class E_ContactInfoExtender extends E_AbstractEditExtender{
    private static final String PAGE_TITLE = '登録内容確認';
    
    E_ContactInfoController extension;
    public Date dateItem{get;set;}
    public String contactName{get;set;}
    
    /* コンストラクタ */
    public E_ContactInfoExtender(E_ContactInfoController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }
    
    /* init */
    global override void init(){
        
        pageRef = doAuth(E_Const.ID_KIND.Contact, this.extension.record.id);
        if(pageRef == null){
            // お客様名に「様」を付与
            this.contactName = this.extension.record.Name + '様';
        }
    }
    
    /* pageAction */
    public PageReference pageAction () {
        return E_Util.toErrorPage(pageRef, null);
    }
    
    /* 住所変更ボタン押下処理 */
    public Pagereference doChangeAddress(){
        Pagereference pref = Page.E_ADRPFChange;
        pref.getParameters().put('conid', this.extension.record.Id);
        return pref;
    }
    
    /* メールアドレス変更ボタン押下処理 */
    public Pagereference doChangeEmail(){
        Pagereference pref = Page.E_EMailAddressChange;
        //pref.getParameters().put('conid', this.extension.record.Id);
        return pref;
    }
    
    /* 基本パスワード変更ボタン押下処理 */
    public Pagereference doChangeStandardPw(){
        Pagereference pref = Page.E_StandardPasswordChange;
        //pref.getParameters().put('conid', this.extension.record.Id);
        return pref;
    }
    
    /* 手続パスワード変更ボタン押下処理 */
    public Pagereference doChangeProceduralPw(){
        Pagereference pref = Page.E_ProceduralPasswordChange;
        pref.getParameters().put('conid', this.extension.record.Id);
        return pref;
    }    
}