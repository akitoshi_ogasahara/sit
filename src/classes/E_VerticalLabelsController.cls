public with sharing class E_VerticalLabelsController {
	public List<String> dataStrings{get;set;}
	public Integer rowSize{get;set;}
	
	public List<List<String>> getStringList(){
		if(rowsize < 1){
			rowSize = 5;
		}
		
		Map<Integer, List<String>> src = new Map<Integer, List<String>>();
		
		for(Integer i=0;i<dataStrings.size();i++){
			// Integer同士の割り算で商を求める
			Integer idx = i/rowSize;
			
			if(!src.containsKey(idx)){
				src.put(idx, new List<String>());
				for(Integer j=0;j<rowSize;j++){
					src.get(idx).add('');
				}
			}
			src.get(idx)[Math.mod(i, rowsize)] = dataStrings[i];
		}
		return src.values();
	}
}