/**
 * IDリクエストデータアクセスオブジェクト(Without版)
 * CreatedDate 2016/10/11
 * @Author Terrasky
 */
public without sharing class E_IDRequestDaoWithout {
	/**
	 * IDリクエストデータリストをIDリクエストレコードIDから取得する
	 * @param  eidrId :IDリクエストのレコードID
	 * @return IDリクエスト情報
	 */
	public static E_IDRequest__c getRecById(String eidrId){
		List<E_IDRequest__c> recs = [
				SELECT
					  Id                    //カスタムオブジェクト ID 
					, Name                  //IDリクエストNo
					, ZEMAILAD__c           //メールアドレス
					, E_IDCPF__c            //ID管理
					, ZIDTYPE__c            //ID種別
					, ZWEBID__c             //ID番号
					, ZUPDFLAG__c           //リクエスト種別
					, ZUPDNO__c             //更新No
					, ZIDOWNER__c           //所有者コード
					, ZINQUIRR__c           //照会者コード
					, dataSyncDate__c       //変更受付日時
					, E_IDCPF__r.User__c
				FROM E_IDRequest__c
				WHERE ID = :eidrId
		];
		return recs.isEmpty() ? null : recs[0];
	}

	/**
	 * ID管理のSFIDからIDリクエストを取得する
	 * @param ID:IDリクエストのSFID
	 */
	public static List<E_IDRequest__c> getRecsByIDCPFId(ID idcpfId){
		List<E_IDRequest__c> recs = [
				SELECT
					  Id                    //カスタムオブジェクト ID 
				FROM E_IDRequest__c
				WHERE E_IDCPF__r.Id = :idcpfId
				And ZUPDFLAG__c = 'I'
		];
		return recs;
	}


	/**
	 * IDリクエストの変更受付日時を更新する
	 * @param eidrId IDリクエストのレコードID
	 */
	@future
	public static void updateEidr(String eidrId,String beforeEmail){
		E_IDRequest__c eidrRec = getRecById(eidrId);

		eidrRec.dataSyncDate__c = System.now();
		eidrRec.note__c = beforeEmail;
		update eidrRec;
	}

	/**
	 * IDリクエストデータを作成(リクエスト種別:E)
	 * @param userName  ユーザのユーザ名
	 * @param userMail  ユーザの変更後メールアドレス
	 * @return E_IDRequest__c
	 */
	public static E_IDRequest__c insEIdrRecWithMail(E_IDCPF__c eidc, String mail){
		//User userRec = E_UserDaoWithout.getUserRecByUserName(userName);
		//E_IDCPF__c eidc = E_IDCPFDaoWithout.getRecsByContactId(userRec.Contact.Id);
		E_IDRequest__c eidr = new E_IDRequest__c(
			E_IDCPF__c = eidc.Id                //ID管理
			,ZUPDFLAG__c = 'E'                  //リクエスト種別
			,ZWEBID__c = eidc.ZWEBID__c         //ID番号
			,ZEMAILAD__c = mail                 //E-MAILアドレス
			,dataSyncDate__c = null             //変更受付日時
		);
		System.debug(eidr);

		insert eidr;
		return eidr;
	}

	/**
	 * IDリクエストレコードを作成する
	 * @param  eidr IDリクエストレコード
	 */
	public static void insertRec(E_IDRequest__c eidr){
		insert eidr;
	}

	/**
	 * 募集人IDに紐付くユーザのNNLinkIDステータスを取得
	 */
	public static List<E_IDRequest__c> getRecsByAgentUserId(Set<Id> userIdset) {
		List<E_IDRequest__c> idrList = [SELECT
											Id, ZUPDFLAG__c, E_IDCPF__r.ZSTATUS01__c, E_IDCPF__r.User__r.Id
										FROM
											E_IDRequest__c
										WHERE
											E_IDCPF__r.User__r.Id in :userIdset
										ORDER BY
											LastModifiedDate DESC];
		return idrList;
	}
	public static List<E_IDRequest__c> getRecByUserName(String userName) {
		List<E_IDRequest__c> idrList = [SELECT
											Id, ZUPDFLAG__c, E_IDCPF__r.ZSTATUS01__c, E_IDCPF__r.User__r.Id,E_IDCPF__r.User__r.IsActive
										FROM
											E_IDRequest__c
										WHERE
											E_IDCPF__r.User__r.userName Like :userName
										ORDER BY
											LastModifiedDate DESC];
		return idrList;
	}

	/**
	 * IDリクエストデータを作成(リクエスト種別:ES)
	 * @param E_IDCPF__c eidc リクエスト対象のID管理レコード
	 * @return E_IDRequest__c
	 */
	public static E_IDRequest__c insEIdrRecES(E_IDCPF__c eidc){
		E_IDRequest__c eidr = new E_IDRequest__c(
			E_IDCPF__c = eidc.Id 				//ID管理
			,ZUPDFLAG__c = 'ES' 				//リクエスト種別
			,ZWEBID__c = eidc.ZWEBID__c 		//ID番号
			,dataSyncDate__c = null 			//変更受付日時
		);
		System.debug(eidr);

		insert eidr;
		return eidr;
	}
}