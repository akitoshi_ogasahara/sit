public with sharing class E_PTNPFDao {

	/**
	 * 保険契約ヘッダIdをキーにデータ取得
	 * @param Id 保険契約ヘッダID
	 * @return List<E_PTNPF__c>
	 */
	public static List<E_PTNPF__c> getRecsByPolicyid(Id policyId){
		return [Select 
						Id, TRANDATE__c, ZTRANDSC__c
					From
						E_PTNPF__c
					Where
						E_Policy__r.Id = :policyId
					Order by
						TRANDATE__c desc, CHDRNUM__c asc, TRANNO__c desc
		];
	}
}