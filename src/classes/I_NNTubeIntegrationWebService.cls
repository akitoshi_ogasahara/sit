global with sharing class I_NNTubeIntegrationWebService {
// 共通
	// 名前
	public static final String JSON_KEY_USER_NAME	= 'userName';
	// SalesforceID(ユーザ)
	public static final String JSON_KEY_SFID		 = 'sfid';
	// ユーザタイプ
	public static final String JSON_KEY_USER_TYPE	= 'userType';
	// MR番号
	public static final String JSON_KEY_MR_CODE	  = 'MRCode';
	// 支社コード
	public static final String JSON_KEY_BRUNCH_CODE  = 'brunchCode';
	
	// 支社名
	public static final String JSON_KEY_BRUNCH_NAME  = 'brunchName';
	
	// チャネルコード
	public static final String JSON_KEY_CHANNEL_CODE = 'channelCode';
	// チャネル名
	public static final String JSON_KEY_CHANNEL_NAME = 'channelName';

// 代理店ユーザのみ
	// 募集人コード
	public static final String JSON_KEY_AGENT_CODE  = 'agentCode';
	// 誕生日
	public static final String JSON_KEY_BIRTH_DAY   = 'birthDay';
	// 代理店コード
	public static final String JSON_KEY_AGENCY_CODE = 'agencyCode';
	
	// 代理店名
	public static final String JSON_KEY_AGENCY_NAME = 'agencyName';
	
	// 事務所コード
	public static final String JSON_KEY_OFFICE_CODE = 'officeCode';
	
	// 事務所名
	public static final String JSON_KEY_OFFICE_NAME = 'officeName';
	

// 社員のみ
	// 社員番号
	public static final String JSON_KEY_EMPLOYEE_CODE = 'employeeCode';

	webService static String getUserInfo(String userId) {
		Map<String, String> jsonMap = new Map<String, String>();

		jsonMap.put(JSON_KEY_USER_NAME, '');
		jsonMap.put(JSON_KEY_SFID, '');
		jsonMap.put(JSON_KEY_USER_TYPE, '');
		jsonMap.put(JSON_KEY_MR_CODE, '');
		//jsonMap.put(JSON_KEY_AREA_CODE, '');
		jsonMap.put(JSON_KEY_BRUNCH_CODE, '');
		//jsonMap.put(JSON_KEY_AREA_NAME, '');
		jsonMap.put(JSON_KEY_BRUNCH_NAME, '');
		jsonMap.put(JSON_KEY_CHANNEL_CODE, '');
		jsonMap.put(JSON_KEY_CHANNEL_NAME, '');
		jsonMap.put(JSON_KEY_AGENT_CODE, '');
		jsonMap.put(JSON_KEY_BIRTH_DAY, '');
		jsonMap.put(JSON_KEY_AGENCY_CODE, '');
		jsonMap.put(JSON_KEY_AGENCY_NAME, '');
		jsonMap.put(JSON_KEY_OFFICE_CODE, '');
		jsonMap.put(JSON_KEY_OFFICE_NAME, '');
		jsonMap.put(JSON_KEY_EMPLOYEE_CODE, '');

		// ユーザを取得
		List<User> users = [SELECT Id, Name
								 , ContactId
								 , UserType
								 , MemberNo__c
								 , MRCD__c
							FROM USER
							WHERE Id = :userId
							AND isActive = true];
		if(users != null && !users.isEmpty()){
			User user = users.get(0);
			Account office;

			// 名前
			jsonMap.put(JSON_KEY_USER_NAME, user.Name);
			// SFID
			jsonMap.put(JSON_KEY_SFID, user.Id);

			// ID管理を取得
			List<E_IDCPF__c> idcpf = [SELECT ZIDTYPE__c FROM E_IDCPF__c WHERE User__r.Id = :userId];

			if(idcpf != null && !idcpf.isEmpty()){
				// ユーザタイプ
				jsonMap.put(JSON_KEY_USER_TYPE, E_Util.null2Blank(idcpf.get(0).ZIDTYPE__c));
			}

			// 代理店ユーザ
			if(user.UserType == E_Const.USERTYPE_POWERPARTNER){
				// 募集人を取得
				List<Contact> con = [SELECT   E_Account__c
											, E_CL3PF_AGNTNUM__c
											, E_CLTPF_DOB__c
											, E_AccParentCord__c
											, E_AccParentName__c
											, E_CL3PF_ZAGCYNUM__c
											, E_CL3PF_BRANCH__c
											, E_KCHANNEL__c
											, E_CL3PF_CLNTNUM__c
											, Account.Id
									FROM Contact
									WHERE Id = :user.ContactId];

				if(con != null && !con.isEmpty()){
					Contact agent = con.get(0);

					// 募集人コード 「|」より前の値を渡す
					final String PIPE = '|';
					String aCode = E_Util.null2Blank(agent.E_CL3PF_AGNTNUM__c);
					Integer pos = aCode.indexOf(PIPE);
					
					if(pos != -1) {
						aCode = aCode.substring(0, pos);
					}
					
					jsonMap.put(JSON_KEY_AGENT_CODE, aCode);
					
					// 代理店コード
					jsonMap.put(JSON_KEY_AGENCY_CODE, E_Util.null2Blank(agent.E_AccParentCord__c));
					// 代理店名
					jsonMap.put(JSON_KEY_AGENCY_NAME, E_Util.null2Blank(agent.E_AccParentName__c));
					// 事務所コード
					jsonMap.put(JSON_KEY_OFFICE_CODE, E_Util.null2Blank(agent.E_CL3PF_ZAGCYNUM__c));
					// 事務所名
					jsonMap.put(JSON_KEY_OFFICE_NAME, E_Util.null2Blank(agent.E_Account__c));
					// 支社コード
					jsonMap.put(JSON_KEY_BRUNCH_CODE, E_Util.null2Blank(agent.E_CL3PF_BRANCH__c));
					// チャネルコード
					jsonMap.put(JSON_KEY_CHANNEL_CODE, E_Util.null2Blank(agent.E_KCHANNEL__c));

					// 募集人（顧客レコード）を取得
					List<Contact> ai = [SELECT E_CLTPF_DOB__c FROM Contact WHERE E_CLTPF_CLNTNUM__c = :agent.E_CL3PF_CLNTNUM__c LIMIT 1];
					if(ai != null && !ai.isEmpty()){
						Contact agentInfo = ai.get(0);
						// 生年月日
						jsonMap.put(JSON_KEY_BIRTH_DAY, E_Util.null2Blank(agentInfo.E_CLTPF_DOB__c));
					}
	
					// 事務所を取得
					List<Account> acc = [SELECT E_CL1PF_KCHANNEL__c, ZMRCODE__c, Agency__r.Id FROM Account WHERE Id = :agent.Account.Id];
					if(acc != null && !acc.isEmpty()){
						office = acc.get(0);
						// チャネルコード	チャネルコードは代理店格から取得する必要あるため、数式経由で取得
						//jsonMap.put(JSON_KEY_CHANNEL_CODE, E_Util.null2Blank(office.E_CL1PF_KCHANNEL__c));
						// MR番号
						jsonMap.put(JSON_KEY_MR_CODE, E_Util.null2Blank(office.ZMRCODE__c));
					}
				}

			// 社員ユーザ
			} else {
				// MR番号
				jsonMap.put(JSON_KEY_MR_CODE, E_Util.null2Blank(user.MRCD__c));
				// 社員番号
				jsonMap.put(JSON_KEY_EMPLOYEE_CODE, E_Util.null2Blank(user.MemberNo__c));
				// チャネルコード
				jsonMap.put(JSON_KEY_CHANNEL_CODE, 'EP');

				// 事務所を取得
				List<Account> acc = [SELECT E_ParentZHEADAY__c
										  , Parent.Name
										  , E_CL2PF_ZAGCYNUM__c
										  , E_CL2PF_BRANCH__c
										  , E_CL1PF_KCHANNEL__c
										  , ZMRCODE__c
										  , Agency__r.Id
									 FROM Account
									 WHERE ZMRCODE__c = :user.MRCD__c
									 LIMIT 1];
				if(acc != null && !acc.isEmpty()){
					office = acc.get(0);
					// 支社コード
					jsonMap.put(JSON_KEY_BRUNCH_CODE, E_Util.null2Blank(office.E_CL2PF_BRANCH__c));
					// チャネルコード
					//jsonMap.put(JSON_KEY_CHANNEL_CODE, E_Util.null2Blank(office.E_CL1PF_KCHANNEL__c));
				}
			}
			

			// 代理店を取得
			if(office != null){
				List<Agency__c> agency = [SELECT AGNTBR__c, KCHANNEL_NM__c FROM Agency__c WHERE Id = :office.Agency__r.Id];
				if(agency != null && !agency.isEmpty()){
					// 拠点名
					jsonMap.put(JSON_KEY_BRUNCH_NAME, E_Util.null2Blank(agency.get(0).AGNTBR__c));
					// チャネル名
					jsonMap.put(JSON_KEY_CHANNEL_NAME, E_Util.null2Blank(agency.get(0).KCHANNEL_NM__c));
				}
			}
		}

		return JSON.serialize(jsonMap);
	}
}