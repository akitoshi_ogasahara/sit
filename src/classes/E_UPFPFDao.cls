/**
 * ユニットプライス表示用ファンドDAO
 */
public with sharing class E_UPFPFDao {
	/**
     * 特別勘定IDをキーにユニットプライス表示用ファンドリストと
     * 特別勘定ID毎に番古いユニットプライスの基準日を1件取得する
     * @param svcpfIdSet: 特別勘定ID SET
     * @return List<E_UPFPF__c>: ユニットプライス表示用ファンド リスト
	 */
	public static List<E_UPFPF__c> getRecBySvcpfId (Set<Id> svcpfIdSet) {
        List<E_UPFPF__c> recs = [
            Select 
            Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
            SystemModstamp, /*LastViewedDate, LastReferencedDate,*/ ZSFUNDCD__c, ZCOLORCD__c, ZDSPFLAG__c, 
            ZFNDNAME01__c, ZFNDNAME02__c, ZHPLNK__c, ZIVTNAME01__c, ZIVTNAME02__c, ZIVTNAME03__c, 
            ZIVTNAME04__c, ZLINCOLO__c, ZLINTYPE__c, dataSyncDate__c, ZSTDTFRM__c, ZSTDTTO__c, 
            ZSTTMFRM__c, ZSTTMTO__c, ZFUNDFLG__c, SVCPF__c, ZFNDNAMEUnion__c, ZIVTNAMEUnion__c,
            ZNAMEUnion__c,
             (Select ZFUNDDTE__c from E_UPXPFs__r order by ZFUNDDTE__c limit 1) 
        	From E_UPFPF__c
            Where SVCPF__c in : svcpfIdSet
        ];
        return recs;
	}

	/** 
	 * ユニットプライス　ファンド別のインデックス値を取得
     * @param eupfpfIds: ユニットプライス表示用ファンドID
     *			fromdt: 抽出期間From
     *			todt:   抽出期間To
     * @return List<E_UPFPF__c>: ユニットプライス基本情報　(@paramのファンドID順に並び替えて返す)
	 */	
	public static List<E_UPFPF__c> getIndexesBySvcpfIds(List<String> eupfpfIds, String fromDt, String toDt){
		List<E_UPFPF__c> ret = new List<E_UPFPF__c>();
		
		if(String.isBlank(fromDt)){
			fromDt = '0';
		}
		if(String.isBlank(toDt)){
			toDt = '99999999';
		}
		Map<String, E_UPFPF__c> funds = new Map<String, E_UPFPF__c>(
															[Select	ZIVTNAMEUnion__c
																	,ZFNDNAMEUnion__c
																	,ZNAMEUnion__c
																	,Name
																	,SVCPF__c
																	,Id
																	,ZFUNDFLG__c
																	, (Select ZFUNDDTE__c, ZVINDEX__c 
																		From E_UPXPFs__r
																		where ZFUNDDTE__c >= :fromDt
																		  And ZFUNDDTE__c <= :toDt
																		order by ZFUNDDTE__c) 
															From E_UPFPF__c
															where Id in :eupfpfIds]
														);
		
		//　パラメータのId列順にSort
		for(Integer i=0;i<eupfpfIds.size();i++){
			//レコードが取得できている場合のみ追加
			if(funds.containsKey(eupfpfIds[i])){
				ret.add(funds.get(eupfpfIds[i]));
			}
		}
		return ret;
	}
	
	/** 
	 * ユニットプライス　ファンド別のインデックス値を取得 （1件のみ）
     * @param eupfpfIds: ユニットプライス表示用ファンドID
     *			fromdt: 抽出期間From
     *			todt:   抽出期間To
     * @return List<E_UPFPF__c>: ユニットプライス基本情報　(@paramのファンドID順に並び替えて返す)
	 */	
	public static List<E_UPFPF__c> getIndexLimit1BySvcpfIds(List<String> eupfpfIds, String fromDt, String toDt){
		List<E_UPFPF__c> ret = new List<E_UPFPF__c>();
		
		if(String.isBlank(fromDt)){
			fromDt = '0';
		}
		if(String.isBlank(toDt)){
			toDt = '99999999';
		}
		Map<String, E_UPFPF__c> funds = new Map<String, E_UPFPF__c>(
															[Select	ZIVTNAMEUnion__c
																	,ZFNDNAMEUnion__c
																	,ZNAMEUnion__c
																	,Name
																	,SVCPF__c
																	,Id
																	,ZFUNDFLG__c
																	, (Select ZFUNDDTE__c, ZVINDEX__c 
																		From E_UPXPFs__r
																		where ZFUNDDTE__c >= :fromDt
																		  And ZFUNDDTE__c <= :toDt
																		order by ZFUNDDTE__c
																		Limit 1) 
															From E_UPFPF__c
															where Id in :eupfpfIds]
														);
		
		//　パラメータのId列順にSort
		for(Integer i=0;i<eupfpfIds.size();i++){
			//レコードが取得できている場合のみ追加
			if(funds.containsKey(eupfpfIds[i])){
				ret.add(funds.get(eupfpfIds[i]));
			}
		}
		return ret;
	}
	

	/** 
	 * 指定日付のユニットプライス情報を取得（指定日付が存在しない場合、直前日付を取得）
	 * @param  targetDate:指定日付
	 * @param  ids:ユニットプライス表示用ファンドID
	 * @param  isFrom:開始日(true)　終了日(false)
	 * @return Map<Id, E_UPFPF__c>
	 */
	public static Map<Id, E_UPFPF__c> getIndexBySvcpfIds(List<String> ids, String targetDate, Boolean isFrom){
		if(isFrom && String.isBlank(targetDate)){
			targetDate = '0';
		}
		if(!isFrom && String.isBlank(targetDate)){
			targetDate = '99999999';
		}
		return new Map<Id, E_UPFPF__c>([
			Select
				Id, Name, ZIVTNAMEUnion__c, ZFNDNAMEUnion__c, ZNAMEUnion__c, ZFUNDFLG__c, SVCPF__c,
				(Select ZFUNDDTE__c, ZVINDEX__c From E_UPXPFs__r Where ZFUNDDTE__c = :targetDate Order By ZFUNDDTE__c  Desc Limit 1)
			From
				E_UPFPF__c
			Where
				Id In :ids 
		]);	
	}
    
	/** 
	 * 特別勘定IDと、ファンドコードからユニットプライス表示用ファンドを取得する
	 * @param  tokubetuKanjouID:特別勘定ID
	 * @param  fndCode:ファンドコード
	 * @return list<E_UPFPF__c>
	 */
    public static list<E_UPFPF__c> getRecByfndCode(List<ID> tokubetuKanjouID,String fndCode,String baseDate){
        String nowtime =  E_Util.getSysDate('yyyyMMddHHmmss');    
		//ユニットプライス表示用ファンド情報取得
        return [
            Select 
            ZFNDNAMEUnion__c
            ,ZIVTNAMEUnion__c
            ,ZFUNDFLG__c
            ,ZHPLNK__c
            ,SVCPF__c
            ,ZSFUNDCD__c
            ,(
                Select 
                ZVINDEX__c
                ,ZFUNDDTE__c 
                From E_UPXPFs__r 
                where ZFUNDDTE__c <= :baseDate
                order by ZFUNDDTE__c DESC nulls last 
                limit 5
            )
            From E_UPFPF__c 
        	where SVCPF__c in :tokubetuKanjouID 
            AND STDTTMFRM__c < :nowtime 
            AND  STDTTMTO__c > :nowtime 
            AND ZDSPFLAG__c = '1' 
            AND ZSFUNDCD__c = :fndCode
            order by ZSFUNDCD__c ASC
        ];        
    }
	/** 
	 * 特別勘定IDでユニットプライス表示用ファンドを取得する
	 * @param  tokubetuKanjouID:特別勘定ID
	 * @return list<E_UPFPF__c>
	 */
    public static list<E_UPFPF__c> getRecBySvcpfId_Time(List<ID> tokubetuKanjouID,String baseDate){
        String nowtime =  E_Util.getSysDate('yyyyMMddHHmmss');    
		//ユニットプライス表示用ファンド情報取得
        return [
            Select 
            ZFNDNAMEUnion__c
            ,ZIVTNAMEUnion__c
            ,ZFUNDFLG__c
            ,ZHPLNK__c
            ,SVCPF__c
            ,ZSFUNDCD__c
            ,(
                Select 
                ZVINDEX__c
                ,ZFUNDDTE__c 
                From E_UPXPFs__r 
                where ZFUNDDTE__c <= :baseDate
                order by ZFUNDDTE__c DESC nulls last 
                limit 5
            )
            From E_UPFPF__c 
        	where SVCPF__c in :tokubetuKanjouID 
            AND STDTTMFRM__c < :nowtime 
            AND  STDTTMTO__c > :nowtime 
            AND ZDSPFLAG__c = '1' 
            order by ZSFUNDCD__c ASC
            ];        
    }
	
}