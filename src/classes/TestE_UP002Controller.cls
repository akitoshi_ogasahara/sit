@isTest(SeeAllData=false)
public class TestE_UP002Controller {
	// testData
	private static E_UPBPF__c upbpf;
    private static String urlZprdCd;
    private static String urlZfndCd;
    private static String ZGFUNDCD;
    private static List<String> ZSFUNDCD;
    private static String ZPRDCD;
    private static Date baseDate;
    private static boolean ZFUNDFLG;
    private static integer EUPFPFSize = 1;
    
    private static void init(){
        createdata();
        insertMessage();
        
    }
    //ダイレクト型商品のとき
    private static testMethod void testDirect() {
        ZPRDCD = 'S0105';
        ZSFUNDCD = new LIST<String>{'A098'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        urlZfndCd = E_EncryptUtil.prdCodeConversion(ZSFUNDCD[0], true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        testpage.getParameters().put('Zfndcd',urlZfndCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        system.assertequals(null,controller.childinit());
        system.assert(controller.openUP003().getURL().contains(Page.UP003.getURL()));
        
        
    }
    //通常商品のとき
    private static testMethod void testStandard() {
        ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A008'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assertequals(null,controller.childinit());
        LIST<string> baseDateList = controller.getBaseDateList();
        //assertionは、m/d
        system.assertequals(baseDate.month() + '/' + baseDate.day(),baseDateList[0]);
        system.assertequals(baseDate.addDays(-1 * 1).month() + '/' + baseDate.addDays(-1 * 1).day(),baseDateList[1]);
        system.assertequals(baseDate.addDays(-1 * 2).month() + '/' + baseDate.addDays(-1 * 2).day(),baseDateList[2]);
        system.assertequals(baseDate.addDays(-1 * 3).month() + '/' + baseDate.addDays(-1 * 3).day(),baseDateList[3]);
        system.assertequals(baseDate.addDays(-1 * 4).month() + '/' + baseDate.addDays(-1 * 4).day(),baseDateList[4]);
        system.assertequals(false,controller.getisVariable());
        system.assertequals('up2|001',controller.getUpperMessage());
        
    }
    //コンポジットファンドのとき
    private static testMethod void testComposit() {
        ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A008'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = true;
        init();
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assertequals(null,controller.childinit());
        String toushiShintaku;
        for(integer i = 0 ; i < controller.recordDataList.size();i++){
            if(controller.recordDataList[i].isCompositURL){
                toushiShintaku = controller.recordDataList[i].toushiShintaku;
            }
        }
        system.assert(String.isnotBlank(toushiShintaku));        
    }
    //バリアブルアニュイティの時
    private static testMethod void testValiable() {
        EUPFPFSize = 5;
        ZPRDCD = 'V9999';
        ZSFUNDCD = new LIST<String>{'V001','V002','V003','V004','V005'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assertequals(null,controller.childinit());
        system.assertequals('up2|002',controller.getUpperMessage());
        system.assert(controller.getisVariable());
    }
    
    //Z98ファンド群のデータは選択可能ファンドと表示用ファンドがマッチングしないケース。
    private static testMethod void testNoMatch() {
        EUPFPFSize = 3;
        ZPRDCD = 'S0025';
        ZSFUNDCD = new LIST<String>{'A001','A002','A003'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'Z98';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();

        // ユニットプライス表示用ファンド(E_UPFPF__c)
        E_UPFPF__c upfpf = [ SELECT Id, ZDSPFLAG__c FROM E_UPFPF__c LIMIT 1 ];
        upfpf.ZDSPFLAG__c = '0';
        update upfpf;        
        
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assertequals(null,controller.childinit());
        LIST<string> baseDateList = controller.getBaseDateList();
        //assertionは、m/d
        system.assertequals(baseDate.month() + '/' + baseDate.day(),baseDateList[0]);
        system.assertequals(baseDate.addDays(-1 * 1).month() + '/' + baseDate.addDays(-1 * 1).day(),baseDateList[1]);
        system.assertequals(baseDate.addDays(-1 * 2).month() + '/' + baseDate.addDays(-1 * 2).day(),baseDateList[2]);
        system.assertequals(baseDate.addDays(-1 * 3).month() + '/' + baseDate.addDays(-1 * 3).day(),baseDateList[3]);
        system.assertequals(baseDate.addDays(-1 * 4).month() + '/' + baseDate.addDays(-1 * 4).day(),baseDateList[4]);
        system.assertequals(false,controller.getisVariable());
        system.assertequals('up2|001',controller.getUpperMessage());
        
    }

    //URLパラメータエラーのとき
    private static testMethod void testErrorURL() {
        ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A008'};
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        //インスタンス
        Pagereference  testpage = Page.up002;
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assert(controller.childinit().getURL().contains(Page.up999.getURL()));
        
    }


    //ユニットプライス基本情報が存在しないとき
    private static testMethod void testNonUPBPF() {
        ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A008'};
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        urlZprdCd = E_EncryptUtil.prdCodeConversion('S0040', true);
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assert(controller.childinit().getURL().contains(Page.up999.getURL()));
        
    }


    //選択可能ファンドが取得できなかったとき
    private static testMethod void testNonSFGPF() {
        ZPRDCD = 'S0046';
        ZSFUNDCD = new LIST<String>{'A008'};
        ZGFUNDCD = 'G04';
        baseDate = date.today();
        ZFUNDFLG = false;
        init();
        ZPRDCD = 'S0001';
        // ユニットプライス基本情報(E_UPBPF__c)
        upbpf = new E_UPBPF__c(
            ZPETNAME__c = 'アイエヌジーマイプラン年金',
            ZGFUNDCD__c = ZGFUNDCD,
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZIDXDATE01__c = '20150101',
            ZPRDCD__c = ZprdCd,
            ZQLFLG__c = true,
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '080000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        );
        insert upbpf;
        urlZprdCd = E_EncryptUtil.prdCodeConversion(ZPRDCD, true);
        //インスタンス
        Pagereference  testpage = Page.up002;
        testpage.getParameters().put('zprdcd',urlZprdCd);
        E_MenuMaster__c menu = createMenuMaster(true, 'メニューマスタ', 'UP' + ZprdCd ,'E_UnitPrice', true);
        E_MessageMaster__c message = createMessageMaster(true, 'メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
        Test.setCurrentPage(testpage);
        E_UP002Controller controller = new  E_UP002Controller();
        
        system.assert(controller.childinit().getURL().contains(Page.up999.getURL()));
        
    }

    
    // testDataCreate
    static void createData(){
        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        
        // ファンド群マスタ(E_FundMaster__c)
        E_FundMaster__c fundMaster = new E_FundMaster__c(
            CURRFROM__c = 0,
            CURRTO__c = 99999999,
            ZGFUNDCD__c = ZGFUNDCD,
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZGRPFND01__c = 'アイエヌジーマイプラン年金',
            ZGRPFND02__c = '',
            ZGRPFND03__c = ''
        );
        insert fundMaster;
        
        // 特別勘定(E_SVCPF__c)
        LIST<E_SVCPF__c> svcpfList = new LIST<E_SVCPF__c>();
        for(integer i = 0 ; i < EUPFPFSize;i++){
        E_SVCPF__c svcpf  = new E_SVCPF__c(
            ZFSDFROM__c = '20150101',
            ZFSHFROM__c = '10',
            ZFSMFROM__c = '10',
            ZFSDTO__c = '99999999',
            ZFSHTO__c = '99',
            ZFSMTO__c = '99',
            ZEFUNDCD__c = ZSFUNDCD[i],
            ZCATFLAG__c = '20',
            ZFUNDNAM01__c = '世界債券型（TS1）',
            ZFUNDNAM02__c = 'ＶＡ外国債券ファンド',
            ZFUNDNAM03__c = '（適格機関投資家専用）',
            ZFUNDNAM04__c = '',
            CURRFROM__c = '20150101',
            CURRTO__c = '99999999'
        );
            svcpfList.add(svcpf);
        }
        insert svcpfList;
        
        // 選択可能ファンド(E_SFGPF__c)
        LIST<E_SFGPF__c> sfgpfList = new LIST<E_SFGPF__c>();
        for(integer i = 0 ; i < svcpfList.size();i++){
        E_SFGPF__c sfgpf = new E_SFGPF__c(
            FundMaster__c = fundMaster.Id,
            E_SVCPF__c = svcpfList[i].Id,
            E_SFGPFCode__c = ZGFUNDCD + '|' + ZSFUNDCD[i]
        );
            sfgpfList.add(sfgpf);
        }
        insert sfgpfList;
        
        String[] indexDate = new String[9];
        
        for(integer i = 0;i<9;i++){
            indexDate[i] = string.valueOf(baseDate.addDays(-1 * i)).remove('-');
        }
        system.debug(indexDate);
        
        // ユニットプライス基本情報(E_UPBPF__c)
        upbpf = new E_UPBPF__c(
            E_FundMaster__c = fundMaster.Id,
            ZPETNAME__c = 'アイエヌジーマイプラン年金',
            ZGFUNDCD__c = ZGFUNDCD,
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZIDXDATE01__c = indexDate[0],
            ZIDXDATE02__c = indexDate[1],
            ZIDXDATE03__c = indexDate[2],
            ZIDXDATE04__c = indexDate[3],
            ZIDXDATE05__c = indexDate[4],
            ZPRDCD__c = ZprdCd,
            ZQLFLG__c = true,
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '080000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        );
        insert upbpf;
        
        LIST<E_UPFPF__c> upfpfList = new LIST<E_UPFPF__c>();
        for(integer i = 0 ; i < svcpfList.size();i++){
            // ユニットプライス表示用ファンド(E_UPFPF__c)
            E_UPFPF__c upfpf = new E_UPFPF__c(
                SVCPF__c = svcpfList[i].Id,
                ZFUNDFLG__c = ZFUNDFLG,
                ZSFUNDCD__c = ZSFUNDCD[i],
                ZFNDNAME01__c = '世界債券型',
                ZFNDNAME02__c = '（TS1）',
                ZIVTNAME01__c = 'ＶＡ外国債券ファンド',
                ZIVTNAME02__c = '（適格機関投資家専用）',
                ZIVTNAME03__c = '',
                ZIVTNAME04__c = '',
                ZDSPFLAG__c = '1',
                ZSTDTFRM__c = '00000000',
                ZSTTMFRM__c = '000000',
                ZSTDTTO__c = '99999999',
                ZSTTMTO__c = '999999'
            );
            upfpfList.add(upfpf);
        }
        insert upfpfList;
        
        // ユニットプライス(E_UPXPF__c)
        List<E_UPXPF__c> upxpfList = new List<E_UPXPF__c>();
        string fndcd;
        for(E_UPFPF__c item: upfpfList){
            fndcd = item.ZSFUNDCD__c;
            for(Integer i = 0; i < 9; i++){
                E_UPXPF__c upxpf = new E_UPXPF__c(
                    E_UPFPF__c = item.Id,
                    ZSFUNDCD__c = fndcd,
                    ZVINDEX__c = 100.000000000,
                    ZFUNDDTE__c = indexDate[i]
                );
                upxpfList.add(upxpf);
            }
        }
        insert upxpfList;
        
        List<E_VariableAnnuity__c> evaList = new List<E_VariableAnnuity__c>();
        evaList.add(new E_VariableAnnuity__c(Name = 'V001', InvName__c = 'エヌエヌ・日本債券オープン'));
        evaList.add(new E_VariableAnnuity__c(Name = 'V002', InvName__c = '投資信託は利用していません'));
        evaList.add(new E_VariableAnnuity__c(Name = 'V003|001', InvName__c = 'エヌエヌ・日本株式オープン'));
        evaList.add(new E_VariableAnnuity__c(Name = 'V003|002', InvName__c = 'エヌエヌ・海外株式オープン'));
        evaList.add(new E_VariableAnnuity__c(Name = 'V004', InvName__c = 'エヌエヌ・日本株式オープン'));
        evaList.add(new E_VariableAnnuity__c(Name = 'V005|001', InvName__c = 'エヌエヌ・日本債券オープン'));
        evaList.add(new E_VariableAnnuity__c(Name = 'V005|002', InvName__c = 'ベアリング外国債券ファンドVA1 （適格機関投資家専用）'));
        insert evaList;
    }
    	/**
	 * メニューマスタ作成
	 * @param isInsert: whether to insert
	 * @param name: メニューマスタ名
	 * @param Key: メニューマスタキー
	 * @param recTypeDevName: レコードタイプ名
	 * @param showCloseBtn: 閉じるボタンを表示
	 * @return E_MenuMaster__c: メニューマスタレコード
	 */
	public static E_MenuMaster__c createMenuMaster(Boolean isInsert, String name, String Key, String recTypeDevName, Boolean showCloseBtn) {
		Id recTypeId = getRecTypeIdMap().get('E_MenuMaster__c').get(recTypeDevName);
		E_MenuMaster__c src = new E_MenuMaster__c(
					Name = name,
					RecordTypeId = recTypeId,
					MenuMasterKey__c = Key,
					IsShowCloseBtn__c = showCloseBtn
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
	
	/**
	 * メッセージマスタ作成
	 * @param isInsert: whether to insert
	 * @param name: メッセージマスタ名
	 * @param mId: メニューマスタのレコードID
	 * @param recTypeDevName: レコードタイプ名
	 * @param type: 種別
	 * @param key: Key/並び順
	 * @return E_MessageMaster__c: メッセージマスタレコード
	 */
	public static E_MessageMaster__c createMessageMaster(Boolean isInsert, String name, Id mId, String recTypeDevName, String type, String key) {
		Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get(recTypeDevName);
		E_MessageMaster__c src = new E_MessageMaster__c(
					Name = name,
					RecordTypeId = recTypeId,
					Menu__c = mId,
					Type__c = type,
					Key__c = key
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
    
    
    /**
	 * RecordTypeマップ取得
	 * @return Map<String, Map<String, Id>>: Map(sObjectType, Map(RecTypeDevName,RecTypeID)
	 */
	private static Map<String, Map<String, Id>> rMap;
	private static Map<String, Map<String, Id>> getRecTypeIdMap(){
		if(rMap != null) return rMap;
		rMap = new Map<String, Map<String, Id>>();
		for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
			if(rMap.containsKey(rt.sObjectType)){
				rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
			}else{
				rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
			}
		}
		return rMap;
	}

    private static void insertMessage(){
        LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
        String type = 'ディスクレイマー';
        String param = 'up2|001';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|002';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|003';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|004';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|005';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        param = 'up2|006';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        insert insMessage;
    }

    

}