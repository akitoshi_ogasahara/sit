@isTest
private with sharing class TestE_PortfolioController{
	private static testMethod void testPageMethods() {		E_PortfolioController extension = new E_PortfolioController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testdtSVFPUnitPrice() {
		E_PortfolioController.dtSVFPUnitPrice dtSVFPUnitPrice = new E_PortfolioController.dtSVFPUnitPrice(new List<E_SVFPF__c>(), new List<E_PortfolioController.dtSVFPUnitPriceItem>(), new List<E_SVFPF__c>(), null);
		dtSVFPUnitPrice.create(new E_SVFPF__c());
		System.assert(true);
	}
	
	private static testMethod void testdtCurrentUnitPrice() {
		E_PortfolioController.dtCurrentUnitPrice dtCurrentUnitPrice = new E_PortfolioController.dtCurrentUnitPrice(new List<E_SVFPF__c>(), new List<E_PortfolioController.dtCurrentUnitPriceItem>(), new List<E_SVFPF__c>(), null);
		dtCurrentUnitPrice.create(new E_SVFPF__c());
		System.assert(true);
	}
	
	private static testMethod void testdtPrtHsry() {
		E_PortfolioController.dtPrtHsry dtPrtHsry = new E_PortfolioController.dtPrtHsry(new List<E_SVPPF__c>(), new List<E_PortfolioController.dtPrtHsryItem>(), new List<E_SVPPF__c>(), null);
		dtPrtHsry.create(new E_SVPPF__c());
		System.assert(true);
	}
	
}