/**
 * 
 */
@isTest
private class TestE_DownloadFeeDataSearchExtension {
	
	//ProfileName
	private static String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	private static User user;
	private static User thisUser = [select id from user where id =:system.UserInfo.getUserId()];
	private static User communityUser;
	private static Account kakuAcc;
	private static List<Account> offAccList;
	private static Contact agentCon;
	private static List<E_AGSPF__c> agspf;
	private static List<E_SASPF__c> saspf;

	/** 
	 * getter 
	 */
    static testMethod void testGeter01() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'coli');	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);
		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)');

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	
			//system.assertEquals('2015年01月', extension.getStartYearMonth());
			//system.assertEquals('2015年02月', extension.getEndYearMonth());
			system.assert(extension.getNewestOrRangeList().size() == 2);
			system.assertEquals('生保手数料明細データ出力条件設定', extension.getTitle());
    	}

    	Test.stopTest();
    }

	/** 
	 * pageAction
	 */
    static testMethod void testPageAction01() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'coli');	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	
			Pagereference resutlPref = extension.pageAction();
			system.assertEquals(null, resutlPref);
    	}

    	Test.stopTest();
    }
	
	/** 
	 * doDownloadCsv 
	 * COLI
	 */
    static testMethod void testDoDownloadCsv01() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'coli');	// COLI
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);
		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)');

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	extension.selOfficeList = new String[]{}; 
	    	extension.selOfficeList.add(kakuAcc.Id);
	    	
	    	extension.doDownloadCsv();
	    	
	    	E_DownloadHistorry__c resultDh = [select outputType__c,formno__c,Conditions__c from E_DownloadHistorry__c where Id = :controller.dh.Id];
    		E_DownloadCondition.CommissionDownloadCondi dlCondi = (E_DownloadCondition.CommissionDownloadCondi)JSON.Deserialize(resultDh.Conditions__c, E_DownloadCondition.CommissionDownloadCondi.class);
    		system.assertEquals(kakuAcc.Id, dlCondi.distributorCode);
    		//system.assertEquals('2015', dlCondi.fromyyyy);
    		//system.assertEquals('02', dlCondi.fromMM);
    		//system.assertEquals('2015', dlCondi.toyyyy);
    		//system.assertEquals('02', dlCondi.toMM);
    		system.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, controller.dh.outputType__c);
    		system.assertEquals(E_Const.DH_FORMNO_FEEDATA_AGS, controller.dh.formno__c);
    	}

    	Test.stopTest();
    }
   
	/** 
	 * doDownloadPDF 
	 * SPVA

    static testMethod void testDoDownloadPDF01() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'spva');	// SPVA
        pref.getParameters().put('oid', offAccList[0].Id);	
        Test.setCurrentPage(pref);

    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	extension.selOfficeList = new String[]{}; 
	    	extension.selOfficeList.add(offAccList[0].Id);
	    	
	    	extension.doDownloadPDF();
	    	
	    	E_DownloadHistorry__c resultDh = [select outputType__c,formno__c,Conditions__c from E_DownloadHistorry__c where Id = :controller.dh.Id];
    		E_DownloadCondition.CommissionDownloadCondi dlCondi = (E_DownloadCondition.CommissionDownloadCondi)JSON.Deserialize(resultDh.Conditions__c, E_DownloadCondition.CommissionDownloadCondi.class);
    		system.assertEquals(kakuAcc.Id, dlCondi.distributorCode);
    		system.assertEquals('2015', dlCondi.fromyyyy);
    		system.assertEquals('04', dlCondi.fromMM);
    		system.assertEquals('2015', dlCondi.toyyyy);
    		system.assertEquals('04', dlCondi.toMM);
    		system.assertEquals(E_Const.DH_OUTPUTTYPE_PDF, controller.dh.outputType__c);
    		system.assertEquals(E_Const.DH_FORMNO_FEEDATA_SPVA, controller.dh.formno__c);
    	}

    	Test.stopTest();
    }
    
	/** 
	 * checkInputForm 
	 * BONUS
	 * エラー：代理店格の時、代理店事務所未選択
	 */
    static testMethod void testCheckInputForm01() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'bospf');	// BONUS
        pref.getParameters().put('did', kakuAcc.Id);	
        Test.setCurrentPage(pref);
		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');
		
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	extension.selOfficeList = new String[]{}; 
	    	extension.doDownloadCsv();
	    	
	    	system.assert(controller.pageMessages.hasMessages() != null );
    	}

    	Test.stopTest();
    }
    
	/** 
	 * checkInputForm 
	 * SPVA
	 * エラー：成立年月チェック
	 */
    static testMethod void testCheckInputForm02() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'spva');	// SPVA
        pref.getParameters().put('oid', offAccList[0].Id);	
        Test.setCurrentPage(pref);
		
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	extension.selOfficeList = new String[]{}; 
	    	extension.selOfficeList.add(kakuAcc.Id);
	    	extension.selTerm = 'designateTerm';
	    	extension.doDownloadPDF();
	    	
	    	system.assert(controller.pageMessages.hasMessages() != null );
    	}

    	Test.stopTest();
    }
    
	/** 
	 * checkInputForm 
	 * SPVA
	 * エラー：単月指定チェック(PDFのみ)
	 */
    static testMethod void testCheckInputForm03() {
    	// User
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	// ページ情報
    	PageReference pref = Page.E_DownloadFeeDataSearch;
        pref.getParameters().put('pt', 'spva');	// SPVA
        pref.getParameters().put('oid', offAccList[0].Id);	
        Test.setCurrentPage(pref);
		
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadFeeDataSearchExtension extension = new E_DownloadFeeDataSearchExtension(controller);
	    	extension.selOfficeList = new String[]{}; 
	    	extension.selOfficeList.add(kakuAcc.Id);
	    	extension.selTerm = 'designateTerm';
	    	extension.selFromDate = '2015/01';
	    	extension.selToDate = '2015/02';
	    	
	    	agspf[1].ACCTMONTH__c = 1;
	    	update agspf;
	    	
	    	extension.doDownloadPDF();
	    	
	    	system.assert(controller.pageMessages.hasMessages() != null );
    	}

    	Test.stopTest();
    }

    
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */    
    
    /** User作成 */
    private static void createUser(String profileName){
        system.runAs(thisuser){
            // User
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            String userName = 'test@terrasky.ingtesting';
            Profile p = [Select Id From Profile Where Name = :profileName];
            user = new User(
                Lastname = 'test'
                , Username = userName
                , Email = userName
                , ProfileId = p.Id
                , Alias = 'test'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , UserRoleId = portalRole.Id
            );
            insert user;
        }
    }
    
    /** コミュニティユーザ作成 */
    private static void createCommunityUser(){
        system.runAs(thisuser){
            // Account
            Account account = new Account(
                Name = 'testAccount'
                ,parentId = kakuAcc.Id
                ,ownerid = user.id
            );
            insert account;
            
            // Contact
            Contact contact = new Contact(
                LastName = 'fstest'
                ,FirstName = 'firstName'
                ,AccountId = account.Id
                ,email = 'fstest@terrasky.ingtesting'
                ,ownerid = user.Id
            );
            insert contact;
            
            Profile p = [Select Id From Profile Where Name = :PF_PARTNER];
            communityUser = new User(
                Lastname = 'fstest'
                ,Username = 'fstest@terrasky.ingtesting'
                ,Email = 'fstest@terrasky.ingtesting'
                ,ProfileId = p.Id
                ,Alias = 'fstest'
                ,TimeZoneSidKey = UserInfo.getTimeZone().getID()
                ,LocaleSidKey = UserInfo.getLocale()
                ,EmailEncodingKey = 'UTF-8'
                ,LanguageLocaleKey = 'ja'
                ,CommunityNickName='tuser1'
                ,contactID = contact.Id
            );
            insert communityUser;
        }
    }
    
    
    /** データアクセス系作成 */
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG02__c = '1'
                ,FLAG06__c = '1'
                ,ZDSPFLAG01__c = '1'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }
    
    /** Data作成 */
    private static void createData(Boolean isCreateOffice){
        system.runAs(thisuser){
            // Account 代理店格
            kakuAcc = new Account(Name = 'kakuAccount',E_CL1PF_ZHEADAY__c = 'kaku0');
            insert kakuAcc;
            
            if(isCreateOffice){
                offAccList = new List<Account>();
                for(Integer i = 1; i < 4; i++){
                    Account acc = new Account(
                        Name = 'office' + i
                        ,ParentId = kakuAcc.Id
                    );
                    offAccList.add(acc);
                }
                insert offAccList;
            }
            
            if(!offAccList.isEmpty()){
                agentCon = new Contact(
                    LastName = 'test'
                    ,AccountId = offAccList[0].Id
                );
                insert agentCon;
            }
            
            // 代理店手数料生保明細CSVダウンロードデータ用
            agspf = new List<E_AGSPF__c>();
            agspf.add(new E_AGSPF__c(ParentAccount__c = kakuAcc.Id, ACCTYEAR__c = 2015, ACCTMONTH__c = 1));
            agspf.add(new E_AGSPF__c(ParentAccount__c = kakuAcc.Id, ACCTYEAR__c = 2015, ACCTMONTH__c = 2));
            agspf.add(new E_AGSPF__c(Account__c = offAccList[0].Id, ACCTYEAR__c = 2015, ACCTMONTH__c = 3));
            agspf.add(new E_AGSPF__c(Account__c = offAccList[0].Id, ACCTYEAR__c = 2015, ACCTMONTH__c = 4));
            insert agspf;
            
            // 代理店手数料生保明細CSVダウンロードデータ用
            saspf = new List<E_SASPF__c>();
            saspf.add(new E_SASPF__c(Account__c = offAccList[0].Id, ACCTYEAR__c = 2015, ACCTMONTH__c = 3));
            saspf.add(new E_SASPF__c(Account__c = offAccList[0].Id, ACCTYEAR__c = 2015, ACCTMONTH__c = 4));
            insert saspf;
			
	        // Ebiz連携ログ(E_BizDataSyncLog__c)
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
			
        }
    }
}