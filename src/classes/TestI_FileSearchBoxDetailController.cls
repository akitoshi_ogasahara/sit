@isTest
private class TestI_FileSearchBoxDetailController {

	private static testMethod void test() {

		Test.startTest();
		I_FileSearchBoxDetailController ctrl = new I_FileSearchBoxDetailController();
		System.assertEquals(I_Const.LIST_MAX_ROWS, ctrl.getListMaxRows());
		System.assertEquals(null, ctrl.getFileRows());
		List<I_FileSearchResult> rows = new List<I_FileSearchResult>();
		ctrl.setFileRows(rows);
		System.assertEquals(rows, ctrl.getSortedRows());

		Integer rowCount = I_FileSearchBoxDetailController.DEFAULT_DISPLAY_ROW_COUNT;
		System.assertEquals(rowCount, ctrl.rowCount);
		ctrl.addRows();
		rowCount = rowCount + I_FileSearchBoxDetailController.ADDITIONAL_ROW_COUNT;
		System.assertEquals(rowCount, ctrl.rowCount);

		Integer loopCount = (I_Const.LIST_MAX_ROWS - rowCount / I_FileSearchBoxDetailController.ADDITIONAL_ROW_COUNT) + 1;
		for (Integer i = 0; i < loopCount; i++) {
			ctrl.addRows();
		}
		System.assertEquals(I_Const.LIST_MAX_ROWS, ctrl.rowCount);
		
		// ソート
		PageReference ref = Page.IRIS_Top;
		Map<String, String> params = ref.getParameters();
		params.put('st', I_FileSearchResult.SORT_TYPE_FILE_NAME);
		// 降順ソート
		Test.setCurrentPageReference(ref);
		ctrl.sortRows();

		// 対象を変更し、昇順ソート
		params.put('st', I_FileSearchResult.SORT_TYPE_PAGE_NAME);
		ctrl.sortRows();

		Test.stopTest();
	}
}