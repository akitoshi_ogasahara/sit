public with sharing class I_TopSumiseiController extends I_AbstractController{

	//緊急メンテナンス
	public I_ContentMaster__c maintenanceInfo {get; private set;}
	//定期メンテナンスリンク
	public String maintenanceURL {get; private set;}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = 'IRISfor住友TOP画面';

		maintenanceInfo = I_ContentMasterDao.getTextByUpsertKey('セクション|住友生命用_Atriaメンテナンス情報|');

		I_PageMaster__c FAQ_Other = I_PageMasterDao.getRecordByKey('iris_FAQ_Sumisei_8');
		maintenanceURL = '';
		if(FAQ_Other!=null){
			maintenanceURL = '/apex/IRIS?page=' + FAQ_Other.Id;
			if(access.isEmployee()) maintenanceURL += '&isSMTM=1';
			maintenanceURL += '#iris-cms-sumisei8-08';
		}

		if(getIsEmployee()){
			ApexPages.currentPage().getParameters().put(I_Const.APPSWITCH_URL_PARAM_APP, 'IRIS');
			E_AppSwitchController appswitch = new E_AppSwitchController();
			appswitch.pageAction();
			return null;
		}

		// 住生IRISユーザでない場合、各TOPへリダイレクト
		if(iris.getIsIRIS() && !access.isSumiseiIRISUser()){
			return Page.E_Home;
		}

		return pr;
	}

	public Map<String, I_MenuItem> getTopMenus(){
		return iris.irisMenus.MenuItemsByKey;
	}

	public Boolean getIsEmployee(){
		return access.isEmployee();
	}

}