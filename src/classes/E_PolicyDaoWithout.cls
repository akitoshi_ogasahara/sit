public without sharing class E_PolicyDaoWithout {
	
	//　解約請求書DL契約レコード作成用レコード取得
	public static E_Policy__c getRecForDLHistoryById(String policyId){
	
		for(E_Policy__c po:[SELECT Id
									,Name
									,ContractorName__c	//ZCLNAME__c;    		//契約者名     テキスト(100)
									,COMM_OCCDATE__c	//OCCDATE__c;    		//契約日     テキスト(8)
									,SPVA_ZREFAMT__c	//ZREFAMT__c;    		//現時点での解約返戻金     数値(16、0)
									,SPVA_ZACTSUM__c	//ZACTSUM__c;    		//現時点での死亡給付金額     数値(16、0)
									,SPVA_AGNTNUM02__c							//共同募集人コード
									,ContractorCLTPF_CLNTNUM__c					//CLNTNUM__c;    //顧客番号     テキスト(8)
									,COMM_CHDRNUM__c	//CHDRNUM__c;    		//証券番号     テキスト(8)

									//代理店格
									,MainAgent__r.Account.E_IsAgency__c
									,MainAgent__r.Account.E_CL1PF_ZHEADAY__c			//ZHEADAY__c;    //代理店コード     テキスト(5)
									,MainAgent__r.Account.E_CL1PF_KCHANNEL__c			//KCHANNEL__c;    //代理店チャネル     テキスト(2)
									,MainAgent__r.Account.Parent.E_CL1PF_KCHANNEL__c	//KCHANNEL__c;    //代理店チャネル     テキスト(2)
									,MainAgent__r.Account.Name							//ZAHNAME__c;    //代理店名     テキスト(100)
									,MainAgent__r.Account.Parent.Name					//ZAHNAME__c;    //代理店名     テキスト(100)

									//代理店事務所
									,MainAgent__r.Account.E_CL2PF_ZAGCYNUM__c	//ZAGCYNUM__c;    //代理店事務所コード     テキスト(5)
									,MainAgent__r.Account.E_CL2PF_ZEAYNAM__c	//ZEAYNAM__c;    //代理店事務所名     テキスト(100)

									,COMM_ZTOTPREM__c							//ZTOTPREM__c;    //払込保険料     数値(18、0)
									,COMM_CRTABLE__c							//CRTABLE__c;    //保険種類コード     テキスト(4)
									,COMM_ZCOVRNAM__c							//ZCOVRNAM__c;    //保険種類名     テキスト(30)
									,MainAgent__r.E_CL3PF_AGNTNUM__c			//AGNTNUM__c;    //募集人番号     テキスト(5)
									,MainAgent__r.Name							//ZEATNAM__c;    //募集人名     テキスト(100)
									,SPVA_ZPRDCD2__c 
									,COMM_CRTABLE2__c
							FROM E_Policy__c
							WHERE Id = :policyId]){
			return po;
		}
		return null;
	}
}