/**
 * 値変更_不備ステータスコンポーネント
 */
public with sharing class SC_ChangeValue_DefectController {
	// 対象レコード
	public SC_Office__c record {get; set;}
	
	//参照権限
	public Boolean canView {get; set;}
	//編集権限
	public Boolean canEdit {get; set;}
	// 編集モードフラグ
	public Boolean isEdit {get; set;}
	// 変更前値
	public String beforeValue;

	/**
	 * Constractor
	 */
	public SC_ChangeValue_DefectController(){
		// 初期化
		isEdit = false;
		beforeValue = '';
	}

	/*
	 * 編集状態を切り替え
	 */
	public PageReference doChangeDisplay(){
		isEdit = !isEdit;
		beforeValue = record.Defect__c;
		if(!isEdit){
			PageReference pr = Page.IRIS_SCSelfCompliances;
			pr.getParameters().put('id',record.Id);
			return pr.setRedirect(true);
		}
		return null;
	}

	/**
	 * 保存ボタン押下
	 */
	public PageReference doSave(){
		//不備が変更された際は履歴を更新
		if(!E_Util.null2Blank(beforeValue).equals(E_Util.null2Blank(record.Defect__c))){
			createHistoryByDefect(beforeValue);
		}
		update record;
		
		// フラグの初期化
		isEdit = false;
		PageReference pr = Page.IRIS_SCSelfCompliances;
		pr.getParameters().put('id',record.Id);
		return pr.setRedirect(true);
	}

	/**
	 * ステータスがNullの際の表示文言
	 */
	private String getDefectStatus01(){
		return SC_Const.DEFECT_STATUS_01;
	}
	
	/**
	 * 表示用ステータス
	 */
	public String getDefectStatus(){
		return getDispDefect(record.Defect__c);
	}
	
	/**
	 * 表示用ステータス作成
	 */
	private String getDispDefect(String defect){
		return String.isBlank(defect)? getDefectStatus01() : defect ;
	}
	
	/***************************************************
	 * 不備ステータス更新
	 ***************************************************/
	/**
	 *　不備ステータス更新時の履歴作成
	 */
	private void createHistoryByDefect(String beforeDefect){
		String str = '';
		beforeDefect = getDispDefect(beforeDefect) ;
		String beforeHistory = record.StatusHistory__c;
		String afterDefect = getDispDefect(record.Defect__c);
			
		// 不備ステータス履歴
		//str += '----------------------------------------------------------------------------------------';
		//str += '\n';
		//str += '- ';
		str += System.now().format();
		str += '   ';
		str += UserInfo.getName();
		str += '   [';
		str += beforeDefect;
		str += ' => ';
		str += afterDefect;
		str += ']';
		str += '\n';
		str += '-------------------------------------------------------------------------------------';
		str += '\n';
		
		str += E_Util.null2Blank(beforeHistory);
		record.StatusHistory__c = str;
	}
}