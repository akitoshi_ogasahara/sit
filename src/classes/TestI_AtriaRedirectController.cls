// P16-0003 Atria対応開発
@isTest
private class TestI_AtriaRedirectController {

	static testMethod void myUnitTest01() {
		I_AtriaRedirectController con = new I_AtriaRedirectController();
		
		Test.startTest();
		
		con.init();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest02() {
		I_AtriaRedirectController con = new I_AtriaRedirectController();
		
		con.init();
		
		Test.startTest();
		
		con.doRedirect();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest03() {
		I_AtriaRedirectController con = new I_AtriaRedirectController();
		
		PageReference pr = Page.IRIS_AtriaRedirect;
		pr.getParameters().put('atriaEpKey', 'AtriaSIT1');
		Test.setCurrentPage(pr);
		
		con.init();
		
		Test.startTest();
		
		con.doRedirect();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest04() {
		I_AtriaRedirectController con = new I_AtriaRedirectController();
		
		PageReference pr = Page.IRIS_AtriaRedirect;
		pr.getParameters().put('atriaEpKey', 'domainkeynotfound');
		Test.setCurrentPage(pr);
		
		con.init();
		
		Test.startTest();
		
		con.doRedirect();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest05() {
		I_AtriaRedirectController con = new I_AtriaRedirectController();
		
		con.init();
		
		Test.startTest();
		
		Integer domainSize = con.getAtriaDomainSize();
		
		Test.stopTest();
		
		System.assertNotEquals(0, domainSize);
	}
}