@isTest
private class TestI_GroupPolicyController {
	public static Date today = Date.newInstance(Date.Today().year(), Date.Today().month()+1,1);


	//保険用ヘッダが0件エラー
	@isTest static void makePolicyListTest() {
		createMessageData('LIS|002','該当する契約がありません');

		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			System.assertEquals(lpc.pageMessages.getWarningMessages()[0].summary,'該当する契約がありません');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約応答月
	@isTest static void sortRowsPolicyResponseDateTest() {
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'polResSort');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			String strAssert;
			if(Integer.valueOf(today.month())<10){
				strAssert = String.valueOf(today.year()) + '/0' + String.valueOf(today.month()) + '/01';
			}else{
				strAssert = String.valueOf(today.year()) + '/' + String.valueOf(today.month()) + '/01';
			}
			System.assertEquals(lpc.policyList[0].polResSort,strAssert);	//TODO Assertionエラー　
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険料
	@isTest static void sortRowsFeeTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyFee');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].policyFee,Decimal.valueOf(10000).format());
			System.assertEquals(lpc.policyList[0].policyFee,'350,000');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険金額
	@isTest static void sortRowsMoneyTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyAmount');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].policyAmount,Decimal.valueOf(100000).format());
			System.assertEquals(lpc.policyList[0].policyAmount,Decimal.valueOf(30000).format());
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険種類
	@isTest static void sortRowsTypeTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyType');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].policyType,'');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-被保険者名
	@isTest static void sortRowsinsuredTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'insured');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].insured,'テストテスト8');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-証券番号
	@isTest static void sortRowsNumTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'policyNum');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].policyNum,'Test0002');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-解約返戻金
	@isTest static void sortRowsCancelTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'cancellRef');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			//System.assertEquals(lpc.policyList[0].cancellRef,'10,000');
			System.assertEquals(lpc.policyList[0].cancellRef,'1,000');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約日
	@isTest static void sortRowsDateTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'contractDay');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			String strAssert;
			if(Integer.valueOf(today.month())<10){
				strAssert = '0' + String.valueOf(today.month()) + '/01';
			}else{
				strAssert = String.valueOf(today.month()) + '/01';
			}
			System.assertEquals(lpc.policyList[0].polResDate,strAssert);
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約者
	@isTest static void sortRowsNameTest() {
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getParameters().put('st', 'contractorName');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			conNameList.sort();
			//System.assertEquals(lpc.policyList[0].contractorName,conNameList[0]);
			System.assertEquals(lpc.policyList[0].contractorName,'テストテスト8');
			Test.stopTest();
		}
	}

	//getIsTotalRowsのテスト
	@isTest static void getIsTotalRowsTest(){
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			String rows = lpc.policyList.size().format();
			System.assertEquals(rows,'2');
			Test.stopTest();
		}
	}

	//前のページへ
	@isTest static void PreviousPageTest(){
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.previous();
			System.assertEquals(lpc.pageRef,null);
			Test.stopTest();
		}
	}

	//次のページへ
	@isTest static void NextPageTest(){
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.next();
			System.assertEquals(lpc.pageRef,null);
			Test.stopTest();
		}
	}

	//最後のページへ
	@isTest static void LastPageTest(){
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		createPolicy4(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_GroupPolicy;
		pageRef.getParameters().put('group','12345678');
		pageRef.getParameters().put('from','1');
		pageRef.getHeaders().put('User-Agent', 'policy');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);
			Test.startTest();
			I_GroupPolicyController lpc = new I_GroupPolicyController();
			lpc.pageAction();
			lpc.last();
			System.assertEquals(lpc.pageRef,null);
			Test.stopTest();
		}
	}

	//データ作成
	static Integer num = 0;
	static List<String> conNameList = new List<String>();
	static List<String> usNameList = new List<String>();
	static void clear(){
		num = 0;
		conNameList = new List<String>();
		usNameList = new List<String>();
	}
	//ログイン判定ユーザ
	static User runAsUser(String sfid){
		User thisUser = [ select Id,ContactId from User where Id = :sfid ];
		return thisUser;
	}

	//保険契約ヘッダ
	static void createPolicy(Boolean choice,User us,String conId){
		Account acc = createAccountOwner(us);
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0004';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = 15000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = conId;
		po.SubAgent__c = con.Id;
		//po.PolicyInCharge__c = true;
		po.OwnerId = us.Id;
		insert po;
	}
	
	static E_Policy__c createPolicy1(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0002';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.SPVA_ZNOWSUM__c = 30000;
		po.COMM_ZTOTPREM__c = 350000;
		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		po.COLI_ZCSHVAL__c = 10000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.OwnerId = userId;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
		 				  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_SPVA];
		po.RecordTypeId = rec.Id;
		insert po;
		return po;
	}
	
	static void createPolicy2(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20171110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10000;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0001';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		po.COLI_ZCSHVAL__c = 1000;
		po.OwnerId = userId;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con2.Id;
		insert po;
	}
	//個人
	static void createPolicy3(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.DCOLI_ZNAME40A01__c = '3テストタイプ';
		po.DCOLI_ZNAME40A02__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0003';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = 20000;
		po.OwnerId = userId;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;
		insert po;
	}

	static void createPolicy4(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.DCOLI_ZNAME40A01__c = '3テストタイプ';
		po.DCOLI_ZNAME40A02__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10;
		po.COMM_CHDRNUM__c = 'Test0005';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = 20000;
		po.OwnerId = userId;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con2.Id;
		insert po;
	}

	//個人保険特約
	static void createCOV(E_Policy__c po){
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		insert cov;
	}
	//募集人（取引先責任者)
	static Contact cretateContact(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//募集人（取引先責任者)
	static Contact cretateContact2(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//親取引先
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//親取引先2
	static Account createParentAccount2(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//取引先
	static Account createAccount(){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先2
	static Account createAccount2(){
		Account acc = createParentAccount2();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先3
	static Account createAccountOwner(User us){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		acp.OwnerId = us.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//ユーザ
	static void createUser(Contact con,Account acc){
		User us = new User();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
	}
	//ユーザ（拠点長)
	static User createUserKyotentyo(){
		User us = new User();
		us.Username = 'kyotentyo@test.com@sfdc.com';
		us.Alias = '拠点長';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('拠点長');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		//us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		//us.ContactId = con.Id;
		insert us;
		return us;
	}

	//ID管理
	static void createIDManage(User us){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.User__c = us.Id;
		ic.AppMode__c = 'IRIS';
		ic.ZINQUIRR__c = 'BR00';
		insert ic;
	}

	//メッセージマスタ
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

}