global with sharing class MNT_DocumentSearch extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public I_ContentMaster__c record{get;set;}
	public MNT_DocumentSearchExtender getExtender() {return (MNT_DocumentSearchExtender)extender;}
	public ResultList ResultList {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component6_val {get;set;}
	public SkyEditor2.TextHolder Component6_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c ConditionCategory_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c ConditionCategory_val_dummy {get;set;}
	public SkyEditor2.TextHolder ConditionCategory_op{get;set;}
	public List<SelectOption> valueOptions_I_ContentMaster_c_DocumentCategory_c_multi {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}
	public SkyEditor2.TextHolder Component17_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component19_val {get;set;}
	public SkyEditor2.TextHolder Component19_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component61_val {get;set;}
	public SkyEditor2.TextHolder Component61_op{get;set;}
	public String recordTypeRecordsJSON_I_ContentMaster_c {get; private set;}
	public String defaultRecordTypeId_I_ContentMaster_c {get; private set;}
	public String metadataJSON_I_ContentMaster_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public MNT_DocumentSearch(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DocumentCategory__c;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.CannotBeOrder__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.MaxCopies__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = I_ContentMaster__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component6_val = new SkyEditor2__SkyEditorDummy__c();
			Component6_op = new SkyEditor2.TextHolder('co');
			ConditionCategory_val = new SkyEditor2__SkyEditorDummy__c();
			ConditionCategory_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			ConditionCategory_op = new SkyEditor2.TextHolder('inx');
			valueOptions_I_ContentMaster_c_DocumentCategory_c_multi = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : I_ContentMaster__c.DocumentCategory__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_I_ContentMaster_c_DocumentCategory_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component17_val = new SkyEditor2__SkyEditorDummy__c();
			Component17_op = new SkyEditor2.TextHolder('co');
			Component19_val = new SkyEditor2__SkyEditorDummy__c();
			Component19_op = new SkyEditor2.TextHolder('eq');
			Component61_val = new SkyEditor2__SkyEditorDummy__c();
			Component61_op = new SkyEditor2.TextHolder('ge');
			queryMap.put(
				'ResultList',
				new SkyEditor2.Query('I_ContentMaster__c')
					.addFieldAsOutput('FormNo__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('DocumentCategory__c')
					.addFieldAsOutput('MaxCopies__c')
					.addFieldAsOutput('DisplayFrom__c')
					.addFieldAsOutput('CannotBeOrder__c')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'SkyEditor2__Text__c', 'Name', Component6_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(ConditionCategory_val_dummy, 'SkyEditor2__Text__c','DocumentCategory__c', ConditionCategory_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component17_val, 'SkyEditor2__Text__c', 'FormNo__c', Component17_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component19_val, 'SkyEditor2__Checkbox__c', 'CannotBeOrder__c', Component19_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component61_val, 'SkyEditor2__Date__c', 'DisplayFrom__c', Component61_op, true, 0, true ))
				);
			ResultList = new ResultList(new List<I_ContentMaster__c>(), new List<ResultListItem>(), new List<I_ContentMaster__c>(), null);
			ResultList.ignoredOnSave = true;
			 ResultList.setPageItems(new List<ResultListItem>());
			 ResultList.setPageSize(25);
			listItemHolders.put('ResultList', ResultList);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_ContentMaster__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_DocumentSearchExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			ResultList.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Name() { 
		return getOperatorOptions('I_ContentMaster__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_DocumentCategory_c_multi() { 
		return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_FormNo_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'FormNo__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_CannotBeOrder_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'CannotBeOrder__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_DisplayFrom_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'DisplayFrom__c');
	}
	global with sharing class ResultListItem extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		ResultListItem(ResultList holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class ResultList extends SkyEditor2.PagingList {
		public List<ResultListItem> items{get; private set;}
		@TestVisible
			ResultList(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<ResultListItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new ResultListItem(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<ResultListItem> getViewItems() {            return (List<ResultListItem>) getPageItems();        }
	}

}