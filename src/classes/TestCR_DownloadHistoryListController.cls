/**
 * CR_DownloadHistoryListController テストクラス
 */
@isTest
private class TestCR_DownloadHistoryListController {

	/**
	 * getHistoryWrapList テスト01
	 * List<historyWrap> getHistoryWrapList()
	 */
	static testMethod void test_getHistoryWrapList01() {
		// Test Data
		List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
		dhList.add(createDownloadHistory('02', '201603', ''));
		dhList.add(createDownloadHistory('99', '201604', 'メッセージ'));
		insert dhList;
		
		Attachment att = new Attachment();
		att.Name = 'Test File';
		Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
		att.body = bodyBlob;
		att.parentId = dhList[0].Id;
		insert att;
		
		Test.startTest();
		
		CR_DownloadHistoryListController controller = new CR_DownloadHistoryListController();
		controller.historyList = [Select Id, Name, RequestDateTime__c, Status__c, OutputRecordYM__c, Message__c, StatusLabel__c, Conditions__c, (Select Id, Name From Attachments) From E_DownloadHistorry__c];
		
		List<CR_DownloadHistoryListController.historyWrap> resultList = controller.getHistoryWrapList();
		
		Test.stopTest();
		
		// 1件目
		CR_DownloadHistoryListController.historyWrap resultWrap1 = resultList[0];
		System.assertEquals(dhList[0].Id, resultWrap1.history.Id);
		System.assert(resultWrap1.file != null);
		System.assertEquals(1, resultWrap1.no);
		System.assertEquals(true, resultWrap1.isDl);
		System.assertEquals(dhList[0].OutputRecordYM__c.substring(0,4) + '/' + dhList[0].OutputRecordYM__c.substring(4,6), resultWrap1.dlOutputYm);

		// 2件目
		CR_DownloadHistoryListController.historyWrap resultWrap2 = resultList[1];
		System.assertEquals(dhList[1].Id, resultWrap2.history.Id);
		System.assert(resultWrap2.file == null);
		System.assertEquals(2, resultWrap2.no);
		System.assertEquals(false, resultWrap2.isDl);
		System.assertEquals(dhList[1].OutputRecordYM__c.substring(0,4) + '/' + dhList[1].OutputRecordYM__c.substring(4,6), resultWrap2.dlOutputYm);
			
	}
	
    /** Create E_DownloadHistorry__c */
    private static E_DownloadHistorry__c createDownloadHistory(String status, String ym, String msg){
    	E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
    	dh.RequestDateTime__c = Datetime.now();	// データ依頼日時
    	dh.Status__c = status;						// ステータス
    	dh.OutputRecordYM__c = ym;				// 出力基準年月
    	dh.Message__c = msg;						// メッセージ
    	dh.Conditions__c = '{"ZHEADAY":"10101","reportType":"0"}';
    	return dh;
    }
}