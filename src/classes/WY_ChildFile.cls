public with sharing class WY_ChildFile {
	public String yakkanType {get; set;}
	public I_ContentMaster__c child {get; set;}
	public String attName {get; set;}
	public Id attId {get; set;}
	public String fileSize {get; set;}

	public WY_ChildFile(String yType, I_ContentMaster__c content, List<Attachment> attach) {
		yakkanType = yType;
		child = content;

		if(!attach.isEmpty() && attach.size() > 0){
			attName = attach[0].Name;
			attId = attach[0].Id;
			fileSize = getAttachmentSize(attach[0]);
		}else{
			attName = '';
			attId = null;
			fileSize = '';
		}
	}

	private String getAttachmentSize(Attachment att) {
		
		Decimal d = 0;
			
		if(att.BodyLength > 1000000){
			d = (Decimal)att.BodyLength/1024/1024;
			
			return String.valueOf(E_Util.roundDecimal(d, 2)) + 'MB';
		} else {
			d = (Decimal)att.BodyLength/1024;
			
			return String.valueOf(E_Util.roundDecimal(d, 0)) + 'KB';
		}
	}
}