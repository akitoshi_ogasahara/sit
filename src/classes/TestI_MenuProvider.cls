@isTest
private class TestI_MenuProvider {
	// 初期処理
	static testMethod void ConstructorTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();

				system.assertEquals(I_MenuProvider.getInstance().activeMenu, null);
				system.assertEquals(I_MenuProvider.getInstance().menusByCategory.get(entries[0].getValue()).size(), 0);
				system.assertEquals(I_MenuProvider.getInstance().menuItemsByKey.size(), 0);
				system.assertEquals(I_MenuProvider.getInstance().getActiveCategoryMenus(), null);
			Test.stopTest();
		}
	}

	static testMethod void ConstructorAddMenuTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[1].getValue(), MenuKey__c = 'testMenuKey'); 
				insert m;

				I_MenuItem imenu = new I_MenuItem(m);

				system.assertEquals(I_MenuProvider.getInstance().menuItemsByKey.get(m.MenuKey__c) != null, true);
				system.assertEquals(I_MenuProvider.getInstance().menusByCategory.get(m.MainCategory__c).size() > 0, true);
			Test.stopTest();
		}
	}

	static testMethod void setAcitveMenuIdTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[2].getValue(), MenuKey__c = 'testMenuKey');
				insert m;

				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];

				system.assertEquals(I_MenuProvider.getInstance().activeMenu, mm);
				//system.assertEquals(I_MenuProvider.getInstance().getActiveCategoryMenus(), I_MenuProvider.getInstance().menusByCategory.get(mm.MainCategory__c));
			Test.stopTest();
		}
	}

	static testMethod void setAcitveMenuKeyTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[2].getValue(), MenuKey__c = 'testMenuKey');
				insert m;

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				system.assertEquals(I_MenuProvider.getInstance().activeMenu, mm);
			Test.stopTest();
		}
	}

	static testMethod void setAcitveMenuKeyTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[3].getValue(), MenuKey__c = 'testMenuKey');
				insert m;

				PageReference pr = Page.IRIS_Top;
				Test.setCurrentPage(pr);
				pr.getParameters().put(I_Const.URL_PARAM_MENUID, m.Id);

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				system.assertEquals(I_MenuProvider.getInstance().activeMenu, mm);
			Test.stopTest();
		}
	}



	static testMethod void getActiveSubCateIsMntTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[3].getValue(), MenuKey__c = 'testMenuKey',SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_INFO);
				insert m;

				PageReference pr = Page.IRIS_Top;
				Test.setCurrentPage(pr);
				pr.getParameters().put(I_Const.URL_PARAM_MENUID, m.Id);

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				I_MenuProvider.LibSubNavItemsHolder libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();
				System.assertEquals(String.isNotBlank(libSubNavItems.getUrlForNNTube()),true);
				System.assertEquals(String.isNotBlank(libSubNavItems.getUrlForNNTubeFavorite()),true);
				System.assertEquals(String.isNotBlank(libSubNavItems.getUrlForNNTubeHistory()),true);
				libSubNavItems.getLibInfoNavItem();
				libSubNavItems.getLibManualNavItem();
				libSubNavItems.getLibOtherNavItem();
				System.assertEquals(libSubNavItems.getActiveSubCateIsMnt(),true);
			Test.stopTest();
		}
	}

	static testMethod void getActiveSubCateIsToolTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[3].getValue(), MenuKey__c = 'testMenuKey',SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL);
				insert m;

				PageReference pr = Page.IRIS_Top;
				Test.setCurrentPage(pr);
				pr.getParameters().put(I_Const.URL_PARAM_MENUID, m.Id);

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				I_MenuProvider.LibSubNavItemsHolder libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();

				System.assertEquals(libSubNavItems.getActiveSubCateIsTool(),true);
			Test.stopTest();
		}
	}

	static testMethod void getActiveSubCateIsFeatTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[3].getValue(), MenuKey__c = 'testMenuKey',SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE);
				insert m;

				PageReference pr = Page.IRIS_Top;
				Test.setCurrentPage(pr);
				pr.getParameters().put(I_Const.URL_PARAM_MENUID, m.Id);

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				I_MenuProvider.LibSubNavItemsHolder libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();

				System.assertEquals(libSubNavItems.getActiveSubCateIsFeat(),true);
				libSubNavItems.getActiveSubCateIsManual();
				libSubNavItems.getActiveSubCateIsOther();
			Test.stopTest();
		}
	}

	static testMethod void getActiveSubCateIsManualTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[3].getValue(), MenuKey__c = 'testMenuKey',SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_MANUAL);
				insert m;

				PageReference pr = Page.IRIS_Top;
				Test.setCurrentPage(pr);
				pr.getParameters().put(I_Const.URL_PARAM_MENUID, m.Id);

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				I_MenuProvider.LibSubNavItemsHolder libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();

				System.assertEquals(libSubNavItems.getActiveSubCateIsManual(),true);
			Test.stopTest();
		}
	}

	static testMethod void getActiveSubCateIsOtherTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[3].getValue(), MenuKey__c = 'testMenuKey',SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_OTHER);
				insert m;

				PageReference pr = Page.IRIS_Top;
				Test.setCurrentPage(pr);
				pr.getParameters().put(I_Const.URL_PARAM_MENUID, m.Id);

				I_MenuProvider.getInstance().setAcitveMenuKey(m.MenuKey__c);

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
											, CanFiltered__c
											, DestURL__c
											, ParentMenu__r.Name
										FROM I_MenuMaster__c
										WHERE MenuKey__c LIKE: m.MenuKey__c
										ORDER BY DisplayOrder__c ASC
										LIMIT 1 ];

				I_MenuProvider.LibSubNavItemsHolder libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();

				System.assertEquals(libSubNavItems.getActiveSubCateIsOther(),true);
			Test.stopTest();
		}
	}
}