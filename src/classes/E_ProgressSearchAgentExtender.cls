/**
 * 新契約申込進捗照会(JP) 募集人検索 Extenderクラス
 */
global with sharing class E_ProgressSearchAgentExtender extends E_AbstractSearchExtender {
	
	//extension
	E_ProgressSearchAgentController extension;
	
	public E_ProgressSearchAgentExtender(E_ProgressSearchAgentController extension) {
		super();
		this.extension = extension;
		pageRef = doAuth(E_Const.ID_KIND.SPECIAL_FEATURES, null);
		if (pageRef == null) {
			SkyEditor2.Query tableQuery1 = extension.queryMap.get('progressTable');
			tableQuery1.addWhere('ZHEADAY__c', getAccountCode(), SkyEditor2.WhereOperator.Eq);
			extension.queryMap.put('progressTable',tableQuery1);
		}
	}
	
	global override void init() {
		//pageRef = doAuth(E_Const.ID_KIND.SPECIAL_FEATURES, null);
	}
	
	public Pagereference pageAction (){
		return E_Util.toErrorPage(pageRef, null);
	}
	
	public PageReference myRecordSearch(){
		User user = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		if (user != null && user.ContactId != null) {
			extension.iAgent_val.SkyEditor2__Text__c = user.Contact.Name;
			extension.doSearch();			
		} else {
			extension.iAgent_val.SkyEditor2__Text__c = null;
		}
		return null;
	}
	
	public String getAccountCode() {
		String accountCode = null;
		String menuKind = E_CookieHandler.getCookieMenuKind();
		if (menuKind != null) {
			if (menuKind.equals(E_Const.MK_EXID_JAPAN_POST)) {
				accountCode = E_Const.ZHEADAY_JPLIFE;
			} else if (menuKind.equals(E_Const.MK_EXID_KAMPO)) {
				accountCode = E_Const.ZHEADAY_KAMPO;
			}
		}
		System.debug('aacccccccc=' + accountCode);
		return accountCode;
	}
}