public with sharing class I_FeatureListController extends I_CMSController{
	//セクション表示用日付項目
	public Date displayDate{get;set;}

	// ページ：ライブラリ配下のIRIS CMSコンテンツ群
	public List<I_ContentMaster__c> sections{get;set;}
	//セクション表示判定用 Booleanリスト
	public List<Boolean> cpmDisplayFlag{get;set;}

	public I_FeatureListController() {
		displayDate = E_Util.SYSTEM_TODAY();
		cpmDisplayFlag = new List<Boolean>();
		try{
			this.sections = new List<I_ContentMaster__c>();
			Integer cmpSize = 0;
			Integer i;

			for(I_CMSSection s : CMSSections) {
				cmpSize = s.cmsComps.size();
				i = 1;
				for(I_CMSComponentController cmp : s.cmsComps){
					if(cmp.getFeatureDisplay()){
						cpmDisplayFlag.add(true);
						break;
					}
					if(cmpSize == i){
						cpmDisplayFlag.add(false);
					}
					i++;
				}
				this.sections.add(s.record);
			}

		}catch(Exception e){
			getPageMessages().addErrorMessage(e.getMessage(), e.getStackTraceString());
		}
	}

	protected virtual override Id getIrisMenuId(){
		// 「ライブラリトップ」のIRISメニュー取得
		I_MenuMaster__c menu = I_MenuMasterDao.getRecByName('特集');

		//デフォルトチェックのついているページ（特集一覧）の格納先を取得
		if(menu != null) {
			Integer pageCnt = menu.Pages__r.size();
			Integer pageNum = 0;
			if(pageCnt > 0){
				for(Integer i = 0; i < pageCnt ; i++){
					if(menu.Pages__r.get(i).Default__c){
						pageNum = i;
					}
				}
			}
			String pageId = menu.Pages__r.size() > 0 ? menu.Pages__r.get(pageNum).Id : '';
			ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_PAGE, pageId);
			ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_MENUID, menu.Id);
		}
		
		return super.getIrisMenuId();
	}

}