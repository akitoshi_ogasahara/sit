public with sharing class E_NewPolicyDao {

	/**
	 * 新契約を取得
	 */
	public static List<E_NewPolicy__c> getNewPolicy (String condition) {
		String soql = 'Select ';
		soql += 'id, ';
		soql += 'InsuranceType__c, ';   //保険種類（主契約）
		soql += 'Status__c, ';
		soql += 'STATCODE__c, ';
		soql += 'StatusDate__c, ';
		soql += 'COMM_ZCLNAME__c, ';
		soql += 'ContractorE_CLTPF_ZCLKNAME__c, ';
		soql += 'KFRAMEIND__c, ';
		soql += 'GroupKey__c, ';
		soql += 'GroupParentFlag__c, ';
		soql += 'GroupStatusDate__c, ';
		soql += 'GRUPKEY__c, ';
		soql += 'W50Count__c, ';
		soql += 'defectCount__c, ';
		soql += 'CHDRNUM__c, ';
		soql += 'INSTTOT01__c, ';
		soql += 'ParentAccount__r.Name, ';
		soql += 'AgentAccountName__c,';
		soql += 'Account__r.owner.Name, ';
		soql += 'Agent_Name__c, ';
		soql += 'BATCACTMN__c,';
		soql += 'BATCACTYR__c,';
		soql += 'SubParentAccount__r.Name, ';
		soql += 'SubAccount__r.Name, ';
		soql += 'SubAccount__r.owner.Name, ';
		soql += 'SubAgent__r.Name, ';
		soql += 'COMM_ZINSNAM__c, ';
		soql += 'InsuredE_CLTPF_ZCLKNAME__c ';
		//外字対応
		soql += ', ContractorName__c, ';
		soql += 'InsuredName__c, ';
		soql += 'ContractorNameKana__c, ';
		soql += 'InsuredNameKana__c ';
		soql += 'From ';
		soql += 'E_NewPolicy__c ';
		soql += condition;
		soql += 'ORDER BY ';
		soql += 'StatusDate__c desc NULLS LAST, ';
		soql += 'ParentAccount__r.Name desc NULLS LAST, ';
		soql += 'Account__r.Name desc NULLS LAST ';

		return Database.query(soql);
	}
	/**
	 * @param pid : 新契約ID
	 * @return E_NewPolicy__c : 新契約レコード
	 */
	public static List<E_NewPolicy__c> getNewPolicyById (Id pid) {
		List<E_NewPolicy__c> newPlcys = [Select
					id,
					InsuranceType__c,   //保険種類（主契約）
					Status__c,
					STATCODE__c,
					StatusDate__c,  //ステータス配信日
					COMM_ZCLNAME__c,//契約者
					ContractorE_CLTPF_ZCLKNAME__c,
					KFRAMEIND__c,   //団体タイプ
					GroupKey__c,
					GRUPKEY__c,
					W50Count__c,
					defectCount__c,
					CHDRNUM__c,
					INSTTOT01__c,
					BATCACTMN__c,
					BATCACTYR__c,
					COMM_ZINSNAM__c,
					InsuredE_CLTPF_ZCLKNAME__c,
					ParentAccount__r.Name,
					AgentAccountName__c,
					Account__r.owner.Name,
					Agent_Name__c,
					SubParentAccount__r.Name,
					SubAccount__r.Name,
					SubAccount__r.owner.Name,
					SubAgent__r.Name,
					//外字対応
					ContractorName__c,
					InsuredName__c,
					ContractorNameKana__c,
					InsuredNameKana__c,
					(Select
						FUPCDE__c,
						FUPSTAT__c
					From
						E_NewPolicyDefects__r
					Where
						FUPSTAT__c <> '')
				From
					E_NewPolicy__c
				Where
					id = :pid];
		if(!newPlcys.isEmpty() && newPlcys.get(0).KFRAMEIND__c == 'Y') {
			newPlcys = [Select
					id,
					InsuranceType__c,   //保険種類（主契約）
					Status__c,
					STATCODE__c,
					StatusDate__c,  //ステータス配信日
					COMM_ZCLNAME__c,//契約者
					ContractorE_CLTPF_ZCLKNAME__c,
					KFRAMEIND__c,   //団体タイプ
					GroupKey__c,
					GRUPKEY__c,
					W50Count__c,
					defectCount__c,
					CHDRNUM__c,
					INSTTOT01__c,
					BATCACTMN__c,
					BATCACTYR__c,
					COMM_ZINSNAM__c,
					InsuredE_CLTPF_ZCLKNAME__c,
					ParentAccount__r.Name,
					AgentAccountName__c,
					Account__r.owner.Name,
					Agent_Name__c,
					SubParentAccount__r.Name,
					SubAccount__r.Name,
					SubAccount__r.owner.Name,
					SubAgent__r.Name,
					//外字対応
					ContractorName__c,
					InsuredName__c,
					ContractorNameKana__c,
					InsuredNameKana__c,
					(Select
						FUPCDE__c,
						FUPSTAT__c
					From
						E_NewPolicyDefects__r
					Where
						FUPSTAT__c <> '')
				From
					E_NewPolicy__c
				Where
					GroupKey__c = :newPlcys.get(0).GroupKey__c];
		}
		return newPlcys;
	}
	/**
	 * 新契約をx件取得
	 */
	public static List<E_NewPolicy__c> getNewPolicysWithNumberOfLimit(Integer numberOfLimit) {
/*		//最終 ステータス
		Set<String> statuses = I_NewPolicyConst.finalStatus;
		// 期間（最終ステータス　=>　前月～今日、　以外　=>　期間制御なし）
		Date ago = Date.today().addMonths(-1);
		Date d = Date.newInstance(ago.year(), ago.month(), 1);

		return [SELECT
				id,
				InsuranceType__c,   //保険種類（主契約）
				Status__c,
				STATCODE__c,
				StatusDate__c,  //ステータス配信日
				COMM_ZCLNAME__c,//契約者
				ContractorE_CLTPF_ZCLKNAME__c,
				KFRAMEIND__c,   //団体タイプ
				GroupKey__c,
				GRUPKEY__c,
				W50Count__c,
				defectCount__c,
				CHDRNUM__c,
				INSTTOT01__c,
				COMM_ZINSNAM__c,
				InsuredE_CLTPF_ZCLKNAME__c,
				ParentAccount__r.Name,
				AgentAccountName__c,
				Account__r.owner.Name,
				Agent_Name__c,
				SubParentAccount__r.Name,
				SubAccount__r.Name,
				SubAccount__r.owner.Name,
				SubAgent__r.Name
			FROM
				E_NewPolicy__c
			WHERE
				KFRAMEIND__c <> 'Y'
			AND
				((Status__c In :statuses And StatusDate__c >= :d) Or (Status__c Not In :statuses))
			ORDER BY
				StatusDate__c desc NULLS LAST,
				ParentAccount__r.Name NULLS LAST,
				Account__r.Name NULLS LAST
			LIMIT :numberOfLimit];*/
			return E_NewPolicyDao.getNewPolicysWithNumberOfLimit(numberOfLimit,'');
	}
	/**
	 * 新契約をx件取得
	 */
	public static List<E_NewPolicy__c> getNewPolicysWithNumberOfLimit(Integer numberOfLimit,String sWhereAnd) {
		//最終 ステータス
		Set<String> statuses = I_NewPolicyConst.finalStatus;
		// 期間（最終ステータス　=>　前月～今日、　以外　=>　期間制御なし）
		Date ago = Date.today().addMonths(-1);
		Date d = Date.newInstance(ago.year(), ago.month(), 1);

		String soql = ' SELECT';
		soql +=' id,';
		soql +=' InsuranceType__c,';   //保険種類（主契約）
		soql +=' Status__c,';
		soql +=' STATCODE__c,';
		soql +=' StatusDate__c,';  //ステータス配信日
		soql +=' COMM_ZCLNAME__c,';//契約者';
		soql +=' ContractorE_CLTPF_ZCLKNAME__c,';
		soql +=' KFRAMEIND__c,';   //団体タイプ';
		soql +=' GroupKey__c,';
		soql +=' GRUPKEY__c,';
		soql +=' W50Count__c,';
		soql +=' defectCount__c,';
		soql +=' BATCACTMN__c,';
		soql +=' BATCACTYR__c,';
		soql +=' CHDRNUM__c,';
		soql +=' INSTTOT01__c,';
		soql +=' COMM_ZINSNAM__c,';
		soql +=' InsuredE_CLTPF_ZCLKNAME__c,';
		soql +=' ParentAccount__r.Name,';
		soql +=' AgentAccountName__c,';
		soql +=' Account__r.owner.Name,';
		soql +=' Agent_Name__c,';
		soql +=' SubParentAccount__r.Name,';
		soql +=' SubAccount__r.Name,';
		soql +=' SubAccount__r.owner.Name,';
		soql +=' SubAgent__r.Name';
		//外字対応
		soql += ', ContractorName__c,';
		soql += ' InsuredName__c, ';
		soql += ' ContractorNameKana__c, ';
		soql += ' InsuredNameKana__c ';
		soql +=' FROM';
		soql +=' E_NewPolicy__c';
		soql +=' WHERE';
		soql +=' KFRAMEIND__c <> \'Y\' ';
		soql +=' AND ';
		soql +=' ((Status__c In :statuses And StatusDate__c >= :d) Or (Status__c Not In :statuses))';
		soql += sWhereAnd;
		soql +=' ORDER BY ';
		soql +=' StatusDate__c desc NULLS LAST,';
		soql +=' ParentAccount__r.Name NULLS LAST,';
		soql +=' Account__r.Name NULLS LAST ';
		soql +=' LIMIT :numberOfLimit';

		return Database.query(soql);
	}

	/**
	 * CSV用データ取得
	 * @param
	 * @return List<E_NewPolicy__c>
	 */
	public static List<E_NewPolicy__c> getRecsForCsv(String distributeId, String officeId, String agentId){
		// 事務所
		Set<String> offices = new Set<String>();
		if(String.isNotBlank(officeId)){
			for(String office : officeId.split(',')){
				offices.add(office);
			}
		}
		system.debug('[offices]' + offices);

		//最終 ステータス
		Set<String> statuses = I_NewPolicyConst.finalStatus;
		// 不備ステータス
		Set<String> defStatus = I_NewPolicyConst.defStatus;


		// 期間（最終ステータス　=>　前月～今日、　以外　=>　期間制御なし）
		Date ago = Date.today().addMonths(-1);
		Date d = Date.newInstance(ago.year(), ago.month(), 1);

		String soql = ' Select ';
		// 新契約情報
		soql += ' Id ';
		soql += ' ,ZPROPNUM__c';			// 申込書番号（9桁）
		soql += ' ,CHDRNUM__c';				// 証券番号
		soql += ',EntryDate__c';			//契約データ入力日
		soql += ' ,PTRN_TRDT__c';			// 契約データ入力日
		soql += ' ,StatusDate__c';			// ステータス発信日
		soql += ' ,Status__c';				// ステータス
		soql += ' ,EFFDATE__c';				// 入金日
		soql += ' ,BATCACTYR__c';			// 成立Ａ／Ｃ （年）
		soql += ' ,BATCACTMN__c';			// 成立Ａ／Ｃ（月）
		soql += ' ,AGNTNUM__c';				// 募集人コード（主）
		soql += ' ,ZAYSECT__c';				// 支部コード
		soql += ' ,ZBKCLCDE__c';			// 個人コード
		soql += ' ,AgentName__c';			// 取扱者名
		soql += ' ,SubAGNTNUM__c';			// 募集人コード（従）
		soql += ' ,SubZJAYSECT__c';			// 支部コード（従）
		soql += ' ,SubZBKCLCDE__c';			// 個人コード（従）
		soql += ' ,SubAgentName__c';		// 取扱者名（従）
		soql += ' ,COMM_ZCLNAME__c';		// 契約者
		soql += ' ,ContractorE_CLTPF_ZCLKNAME__c';  // 契約者（カナ）
		soql += ' ,COMM_ZINSNAM__c';		// 被保険者
		soql += ' ,InsuredE_CLTPF_ZCLKNAME__c'; // 被契約者（カナ）
		soql += ' ,InsuranceType__c';		   // 保険種類（主契約）
		soql += ' ,INSTTOT01__c';			// 1P合計保険料
		soql += ' ,GRUPKEY__c';				// 団体番号
		soql += ' ,SyncDate__c';			// データ発信日
		soql += ' ,FLUP_TRDT__c';		   //最新不備処理日
		//外字対応
		soql += ' ,ContractorName__c';		//契約者名（数式）
		soql += ' ,InsuredName__c';		   //被保険者名（数式）
		soql += ' ,ContractorNameKana__c';		//契約者カナ名（数式）
		soql += ' ,InsuredNameKana__c';		   //被保険者カナ名（数式）
		soql += ' ,ZANNPREM__c';			//ANP
		soql += ' ,SUMINS__c';				//保険金額
		soql += ' ,PAYEDPREM__c';			//入金済保険料金額
		soql += ' ,DIFFPREM__c';			//入金済保険料金額との差額
		soql += ' ,STATEMENTDATE__c';		//告知日
		soql += ' ,ANBATCCD__c';			//契約時年齢
		soql += ' ,BILLFREQ__c';			//払方
		soql += ' ,BILLCHNL__c';			//払込経路
		soql += ' ,ZAYKNJ__c';				//支社名（事務所名）
		soql += ' ,SUBZAYKNJ__c';			//支社名（事務所名）（従）
		soql += ' ,SPECIAPREM__c';			//特別保険料
		soql += ' ,STMPDTYDTE__c';			//申込日
		soql += ' ,LIFESEX__c';				//被保険者性別
		soql += ' ,LIFEDOB__c';				//被保険者生年月日
		soql += ' ,SELECTION__c';			//診査方法
		// 不備情報
		soql += ' ,(Select Id';
		soql += ' ,FULP_FUPDT__c';		  // 不備発信日
		soql += ' ,FUPSTAT__c';			 // 不備ステータス
		soql += ' ,FUPCDE__c';			  // 不備コード
		soql += ' ,T5661_LONGDESC__c';	  // 不備内容
		soql += ' ,ZFUPRMK__c';			 // 不備内容詳細（テキストエリア）
		soql += ' ,TL735_LONGDESC__c';	  // 不備処理方法1
		soql += ' ,TL735_LONGDESC2__c';	 // 不備処理方法2
		soql += ' ,TL735_LONGDESC3__c';	 // 不備処理方法3
		soql += ' From';
		soql += ' E_NewPolicyDefects__r';
		soql += ' Where';
		soql += ' FUPSTAT__c In :defStatus';
		soql += ' Order by';
		soql += ' FUPSTAT__c desc';		 // 不備ステータス（O→F）
		soql += ' ,FULP_FUPDT__c desc';	 // 不備発信日
		soql += ' ,FUPCDE__c asc';		  // 不備コード
		soql += ' ,UpsertKey__c asc';	   //Upsertkey(SEQコードの代用)
		soql += ' Limit 10)';

		soql += ' From';
		soql += ' E_NewPolicy__c';

		soql += ' Where';
		// 期間
		soql += ' ((Status__c In :statuses And StatusDate__c >= :d) Or (Status__c Not In :statuses))';
		// 事務所
		if(String.isNotBlank(officeId)){
			soql += ' And Account__c In :offices';
		// 募集人
		}else if(String.isNotBlank(agentId)){
			soql += 'And Agent__c = :agentId';
		// 代理店格
		}else if(String.isNotBlank(distributeId)){
			soql += ' And ParentAccount__c = :distributeId';
		}

		soql += ' Order by';
		soql += ' StatusDate__c desc NULLS LAST, ';
		soql += ' AgentParentAccountName__c desc NULLS LAST, ';
		soql += ' AgentAccountName__c desc NULLS LAST, ';
		soql += ' CHDRNUM__c asc';

		System.debug(soql);
		System.debug(Database.query(soql));
		return Database.query(soql);
	}

	/**
	 * ログ加工用Map作成
	 * @param Set<Id>
	 * @return Map<Id, E_NewPolicy__c>
	 */
	public static Map<Id, E_NewPolicy__c> getRecsForLog(Set<Id> ids){
		Map<Id, E_NewPolicy__c> result = new Map<Id, E_NewPolicy__c>();
		List<E_NewPolicy__c> npols = [SELECT Id, COMM_ZCLNAME__c, InsuranceType__c, CHDRNUM__c
									  FROM E_NewPolicy__c
									  WHERE Id IN :ids];

		for(E_NewPolicy__c np : npols){
			result.put(np.Id, np);
		}

		return result;
	}
	/**
	 * 団体親レコードの取得
	 * @return List<E_NewPolicy__c>
	 */
	public static List<E_NewPolicy__c> getGroupParentRecs(){
		return [Select id,GroupKey__c From E_NewPolicy__c Where GroupParentFlag__c = true];
	}
	/**
	 * 団体キーからレコードを取得
	 * @parem gSet: 団体キーSet
	 * @return List<E_NewPolicy__c>
	 */
	public static List<E_NewPolicy__c> getRecsByGroupKeys(Set<String> gSet){
		return [Select id,GroupParentFlag__c,GroupKey__c,Status__c,SystemStatus__c,GroupStatusDate__c,SyncDate__c,GroupStatus__c From E_NewPolicy__c Where GroupKey__c in: gSet];
	}

    /**
     * 団体サマリーキーで集計した情報の取得
     * @param Set<String> groupKeys 団体サマリーキー
     * @param Set<String> statusNames ステータス
     * @return List<AggregateResult>
     */
    public static List<AggregateResult> getAggregatesByGroupKey(Set<String> groupKeys, Set<String> statusNames){
    	return [Select
    				GRUPKEY__c, COUNT(Id) cnt, SUM(INSTTOT01__c) fee, MIN(CHDRNUM__c) chdrnum
    			From
    				E_NewPolicy__c
    			Where
    				GroupKey__c In :groupKeys
    			And
    				 Status__c In :statusNames
    			Group By
    				GroupKeyText__c, GRUPKEY__c];
    }

    /**
     * 証券番号をキーにレコード取得
     * @param Set<String> nums 証券番号
     * @return List<E_NewPolicy__c>
     *
     */
    public static List<E_NewPolicy__c> getRecsByCHDRNUMForPush(Set<String> nums){
    	return [Select
    				 Id, CHDRNUM__c, COMM_ZCLNAME__c, COMM_ZINSNAM__c, InsuranceType__c,
    				 Agent_Name__c, AgentAccountName__c, MainAgentNum5__c, SubAgentNum5__c,
    				 ContractorE_CLTPF_ZCLKNAME__c, InsuredE_CLTPF_ZCLKNAME__c,
    				 Agent__r.Id, SubAgent__r.Id,
    				 Account__r.OwnerId, SubAccount__r.OwnerId
    				 //外字対応
    				 ,ContractorName__c,InsuredName__c,ContractorNameKana__c,InsuredNameKana__c
    			From
    				E_NewPolicy__c
    			Where
    				CHDRNUM__c In :nums];
    }
	/**
	 * 新契約ステータスを個人集計用に取得する。
	 * 使用箇所
	 *  I_TotalController
	 * @param condition : 追加条件
	 * @return List<AggregateResult> : 集計結果
	 */
	public static List<AggregateResult> getNewPolicyForCount(String condition){
		// 実行当月の1日
		Date d = Date.today().addMonths(-1);
		String dt = Datetime.newInstance(d.year(), d.month(), 1).formatGMT('yyyy-MM-dd HH:mm:ss');
		dt = dt.replaceAll(' ', 'T') + '.000Z';

		String soql = '';
		soql = 'SELECT ';
		soql += 'SystemStatus__c ';			// 発信日ステータス
		soql += ',count(id) cnt ';			// カウント
		soql += 'FROM E_NewPolicy__c ';
		soql += 'WHERE (KFRAMEIND__c <> \'Y\') ';
		soql += '  AND ((STATCODE__c = \'P\') ';
		soql += '   OR  (STATCODE__c <> \'P\' AND statusDate__c >= ' + dt + ')) ';
		soql += condition;
		soql += 'GROUP BY SystemStatus__c ';
		system.debug(soql);
		return Database.query(soql);
	}
	/**
	 * 新契約ステータスを団体集計用に取得する。
	 * 使用箇所
	 *  I_TotalController
	 * @param condition : 追加条件
	 * @return List<AggregateResult> : 集計結果
	 */
	public static List<AggregateResult> getNewPolicyForGroupCount(String condition){
		// 実行当月の1日
		Date d = Date.today().addMonths(-1);
		String dt = Datetime.newInstance(d.year(), d.month(), 1).formatGMT('yyyy-MM-dd HH:mm:ss');
		dt = dt.replaceAll(' ', 'T') + '.000Z';

		String soql = '';
		soql = 'SELECT ';
		soql += 'GroupKeyText__c ';			// 団体サマリーキー（テキスト）
		soql += ',GroupStatus__c ';			// 団体ステータス
		soql += 'FROM E_NewPolicy__c ';
		soql += 'WHERE (KFRAMEIND__c = \'Y\' AND GroupParentFlag__c = true) ';
		soql += '  AND ((STATCODE__c = \'P\') ';
		soql += '   OR  (STATCODE__c <> \'P\' AND statusDate__c >= ' + dt + ')) ';
		soql += condition;
		soql += 'GROUP BY GroupKeyText__c, GroupStatus__c ';
		system.debug(soql);
		return Database.query(soql);
	}


}