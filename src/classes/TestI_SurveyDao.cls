@isTest
private class TestI_SurveyDao {

	/*
	 * データ1件取得成功
	 */
	@isTest static void getSurveyBySurveyKeyTest01(){
		//データ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, true);
		Attachment att = TestI_TestUtil.createAttachment(true, survey.Id);
		Test.startTest();
		I_Survey__c surveyRec = I_SurveyDao.getSurveyBySurveyKey(survey.UpsertKey__c);
		Test.stopTest();

		System.assertEquals(survey.Id ,surveyRec.Id);
		System.assertEquals(att.Id, surveyRec.Attachments[0].Id);
	}

	/*
	 * データ1件取得失敗
	 */
	@isTest static void getSurveyBySurveyKeyTest02(){
		//データ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, true);
		Attachment att = TestI_TestUtil.createAttachment(true, survey.Id);
		//テスト実行
		Test.startTest();
		I_Survey__c surveyRec = I_SurveyDao.getSurveyBySurveyKey('test');
		Test.stopTest();
		//テスト検証
		System.assertEquals(null, surveyRec.Id);
	}

	/*
	 * アンケート登録 募集人
	 * @param isAgent： whether Agent
	 * @return I_Survey__c： アンケート
	 */
	private static I_Survey__c createSurveyRec(Id userId, boolean isAgent){
		//アンケート作成
		I_Survey__c survey = new I_Survey__c();
		survey.Name = 'テストアンケート';
		survey.OverView__c = 'テスト概要';
		survey.UpsertKey__c = 'survey01';
		survey.PeriodFrom__c = Date.today();
		survey.PeriodTo__c = Date.today().addDays(5);
		if(isAgent){
			survey.SurveyTarget__c = '募集人';
		}else{
			survey.SurveyTarget__c = '代理店';
		}
		insert survey;
		//アンケートに紐づく対象者作成
		I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
		sTarget.Ownerid = userId;
		sTarget.Survey__c = survey.id;
		insert sTarget;
		//アンケートに紐づく設問作成
		I_SurveyQuestion__c question = new I_SurveyQuestion__c();
		question.Name = 'テスト設問';
		question.Selection__c = '参加';
		question.Survey__c = survey.Id;
		insert question;

		return survey;
	}

	/*
	 * 代理店ユーザー作成
	 * @return User: ユーザー
	 */
	private static User createUser(){
		User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
		User us = new User();
		System.runAs(thisUser){
			Account parentAcc = TestI_TestUtil.createAccount(true, null);
			Account acc = TestI_TestUtil.createAccount(true, parentAcc);
			Contact con = TestI_TestUtil.createContact(true, acc.Id, 'testCon');
			us = TestI_TestUtil.createAgentUser(true, 'testUser', E_Const.PROFILE_E_PARTNERCOMMUNITY, con.Id);
			TestI_TestUtil.createAccountShare(acc.Id, us.Id);
			//代理店権限を付与
			TestI_TestUtil.createBasePermissions(us.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, us.Id, 'AY');
		}
		return us;
	}
}