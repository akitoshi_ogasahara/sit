@isTest
private class TestE_DownloadCondition {
	
	/**
	 * 期間自由選択,特別勘定ID:無し
	 */
	private static testMethod void testDownloadCondi001() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){

			//テスト開始
			Test.startTest();

			E_DownloadCondition.DownloadCondi downloadCondi = new E_DownloadCondition.DownloadCondi();
			downloadCondi.distributorCode = '111';
			downloadCondi.officeCodes = new List<String>{'111111', '222222'};
			downloadCondi.ZIDTYPE = 'AY';
			downloadCondi.toJSON();

			E_DownloadCondition.CommissionDownloadCondi commissionDownloadCondi = new E_DownloadCondition.CommissionDownloadCondi();
			commissionDownloadCondi.fromyyyy = '2014';
			commissionDownloadCondi.fromMM = '10';
			commissionDownloadCondi.toyyyy = '2015';
			commissionDownloadCondi.toMM = '12';
			//20170110追加 医的引受の目安 リリースのため↓
			commissionDownloadCondi.ZHEADAY = 'ZHEADAY';
			commissionDownloadCondi.ZHEADNAME = 'ZHEADNAME';
			//20170110追加 医的引受の目安 リリースのため↑

			E_DownloadCondition.SPVADownloadCondi spvaDownloadCondi = new E_DownloadCondition.SPVADownloadCondi();
			spvaDownloadCondi.agentCode = '003xxxxxxxxxxxx';
			spvaDownloadCondi.fromZACCTYM = 'fromZACCTYM';
			spvaDownloadCondi.toZACCTYM = 'toZACCTYM';
			spvaDownloadCondi.fromOCCDATE = 'fromOCCDATE';
			spvaDownloadCondi.toOCCDATE = 'toOCCDATE';	
			spvaDownloadCondi.ZCNTRDSC = 'ZCNTRDSC';	
			spvaDownloadCondi.ZANNSTFLG = 'ZANNSTFLG';

			E_DownloadCondition.COLIDownloadCondi coliDownloadCondi = new E_DownloadCondition.COLIDownloadCondi();
			coliDownloadCondi.groupby = 'groupby';

			E_DownloadCondition.HistoryDownloadCondi historyDownloadCondi = new E_DownloadCondition.HistoryDownloadCondi();
			historyDownloadCondi.agentCode = '003xxxxxxxxxxxx';
			historyDownloadCondi.fromTRANDATE = 'fromTRANDATE';
			historyDownloadCondi.toTRANDATE = 'toTRANDATE';

			E_DownloadCondition.UnitPriceDownloadCondi unitPriceDownloadCondi = new E_DownloadCondition.UnitPriceDownloadCondi();
			unitPriceDownloadCondi.EUPBPF = 'EUPBPF';
			unitPriceDownloadCondi.NAMEKBN = 'NAMEKBN';
			unitPriceDownloadCondi.ZSFUNDCD = new List<String>{'ZSFUNDCD1', 'ZSFUNDCD2'};
			unitPriceDownloadCondi.fromTerm = 'fromTerm';
			unitPriceDownloadCondi.toTerm = 'toTerm';
			unitPriceDownloadCondi.getInstanceByJSON(JSON.serialize(unitPriceDownloadCondi));
			unitPriceDownloadCondi.clone(unitPriceDownloadCondi);

			E_DownloadCondition.NoticeDownloadCondi noticeDownloadCondi = new E_DownloadCondition.NoticeDownloadCondi();
			noticeDownloadCondi.agentCode = '11111';
			noticeDownloadCondi.fromyyyyMM = '201712';
			noticeDownloadCondi.toyyyyMM = '201801';
			noticeDownloadCondi.policyTypes = new List<String>{'契約種類'};
			noticeDownloadCondi.csvType = 'csv';

			E_DownloadCondition.NoticeIrisDownloadCondi noticeIrisDownloadCondi = new E_DownloadCondition.NoticeIrisDownloadCondi();
			noticeIrisDownloadCondi.agentCode = '11111';
			noticeIrisDownloadCondi.fromyyyyMM = '201712';
			noticeIrisDownloadCondi.toyyyyMM = '201801';
			noticeIrisDownloadCondi.policyTypes = new List<String>{'契約種類'};
			noticeIrisDownloadCondi.distributorCodes = new List<String>{'csv'};

			E_DownloadCondition.NotificationInfo notificationInfo = new E_DownloadCondition.NotificationInfo();
			notificationInfo.type = '1';
			notificationInfo.month = '12';
			notificationInfo.zduedate = '20171201';
			notificationInfo.limitdt = '10';
			notificationInfo.content = 'test';

			E_DownloadCondition.ICOLIDownloadCondi icoliDownloadCondi = new E_DownloadCondition.ICOLIDownloadCondi();
			icoliDownloadCondi.policyTypes = new List<String>{'個人保険ヘッダ','個人保険ヘッダ'};
			icoliDownloadCondi.policyId = '123456789';
			icoliDownloadCondi.inquirydate = '2017/12/01';
			icoliDownloadCondi.isIss = 'true';
			//icoliDownloadCondi.comment = 'テストコメント';
			icoliDownloadCondi.notificationInfos = new List<E_DownloadCondition.NotificationInfo>{notificationInfo};

			//20170110追加 医的引受の目安 リリースのため↓
			icoliDownloadCondi.officeName = 'officeName';
			icoliDownloadCondi.agentName = 'agentName';
			icoliDownloadCondi.groupName = 'groupName';
			icoliDownloadCondi.toJSON();

			E_DownloadCondition.BizBooksDownloadCondi bizBooksDLCondi = new E_DownloadCondition.BizBooksDownloadCondi();
			bizBooksDLCondi.officeCodes = new List<String>{'officeCode1','officeCode2'};

			E_DownloadCondition.BizReportsDownloadCondi bizReportsDLCondi = new E_DownloadCondition.BizReportsDownloadCondi();
			bizReportsDLCondi.reportType = 'reportType';
			bizReportsDLCondi.ZHEADAY = 'ZHEADAY';
			bizReportsDLCondi.toJSON();

			E_DownloadCondition.DiseaseNumberInfo diseaseNumberInfo = new E_DownloadCondition.DiseaseNumberInfo();
			diseaseNumberInfo.csvType = 'csvType';
			diseaseNumberInfo.diseaseNumber = '1';
			diseaseNumberInfo.toJSON();
			//20170110追加 医的引受の目安 リリースのため↑

			
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(null, resultPage);
		
	}


	/**
	 * ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	private static User createUser(Boolean isInsert, String LastName, String profileDevName){
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}
	private static Map<String, Id> pMap;
	private static Map<String, Id> getProfileIdMap(){
		if(pMap != null){
			return pMap;
		}
		pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}
}