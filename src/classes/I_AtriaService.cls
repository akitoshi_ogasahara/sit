public with sharing class I_AtriaService {

	public String nnlinkid {get; set;}
	@TestVisible private String guid{get; set;}
	
    public List<Object> getHihokensyaMeritInfo(String inJson){
		String atriaJson = createAtriaJson(inJson);
		System.debug(atriajson);
		String resStr = '';

		List<Object> lst;
		if( atriajson != null ) resStr = calloutAtriaService(atriajson);
		if( resStr != null && resStr != '' ){
			Map<String,Object> resMap = (Map<String,Object>)JSON.deserializeUntyped(resStr);
			String statusCode = String.valueOf((Integer)resMap.get('statusCode'));
			if(statusCode == '200'){
				try{
					Map<String,Object> dataMap = (Map<String,Object>)resMap.get('data');
					Map<String,Object> meritHyoMap = (Map<String,Object>)dataMap.get('meritHyo');
					List<Object> hihokenshaMeritList = (List<Object>)meritHyoMap.get('hihokenshaMeritList');
					Map<String,Object> hihokenshaMeritMap = (Map<String,Object>)hihokenshaMeritList[0];
					lst = (List<Object>)hihokenshaMeritMap.get('hihokenshaMeritInfo');
					insertCallAtriaLog( resStr );
					if(lst.size() != 0) return lst;
				}catch (Exception e){
					//Atriaからのレスポンスボディが期待したデータ構造でない場合
					throw new I_PIPException(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_INVALID_RESPONSE, 'guid:' + guid + '\r\nStackTrace:'+ e.getStackTraceString()+'\r\nresStr:'+resStr);
				}
				
			}else{
				//AtriaからのレスポンスのStatusコードが200以外の場合
				throw new I_PIPException(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_INVALID_RESPONSE,resStr);
			}
		} else {
			//Atriaからのレスポンスボディがnull又は空の場合
			throw new I_PIPException(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_INVALID_RESPONSE,resStr);
		}

    	return lst;
    }

	@TestVisible private String createAtriaJson( String inJson ){
		String timeStamp = DateTime.now().format('yyyy-MM-dd HH:mm:ss.SSS');
		String timeStampForguid = DateTime.now().format('yyyyMMddHHmmss');
		guid = 'P-' + nnlinkid + '-' + timeStampForguid;

		String strToEncode = nnlinkid + timeStamp + inJson;
		Blob hash = Crypto.generateDigest('SHA-256',Blob.valueOf(strToEncode));
		String encodedInput = EncodingUtil.base64Encode(hash);

		String signature = createSignature(encodedInput);

		String atriajson = '{ "userId":"' + nnlinkid + '",';
		atriajson += '"guid":"' + guid + '",';
		atriajson += '"encodedImput":"' + encodedInput + '",';
		atriajson += '"requestTimeStr":"' + timeStamp + '",';
		atriajson += '"signature":"' + signature + '",';
		atriajson += '"atriaRt":' + inJson + '}';

		return atriaJson;
	}

    @TestVisible private String createSignature(String strToSign){

    	Blob targetBlob = Blob.valueOf(strToSign);
    	Blob signature = System.Crypto.signWithCertificate('RSA-SHA256', targetBlob, 'PIPCert');
    	String base64Signature = EncodingUtil.base64Encode(signature);

    	return base64Signature;

    }

	@TestVisible private String calloutAtriaService(String reqBody){
		Http http = new HTTP();
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint('callout:Atria/web/atriaWebService/calService');
		//req.setEndpoint('https://atria-uat.nnlink.jp:11444/atria-webapp/web/atriaWebService/calService');
		req.setHeader('Content-Type','application/json');
		req.setBody(reqBody);
		system.debug('rec.Endpoint' + req.getEndPoint() );

		HttpResponse res = http.send(req);
		system.debug( 'getHeader(key)' + res.getHeader('Content-Type') );
		system.debug( 'getHeaderKeys()' + res.getHeaderKeys() );
		system.debug( 'StatusCode = ' + res.getStatusCode() );
		system.debug( 'resStr = '  + res.getBody() );
		if(res.getStatusCode() == 200){
			System.debug('☆☆☆Atria Response Body' + res.getBody());
			return res.getBody();
		} else if(res.getStatusCode() == 404) {
			System.debug('☆☆☆Atria Response Body' + res.getBody());
			throw new I_PIPException(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_404,res.getBody());
		} else {
			throw new I_PIPException(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_OTHER,res.getBody());
		}
	}

	@TestVisible private void insertCallAtriaLog( String logDetail ){
		String detail = 'guid:' + guid + '\r\ndetail:' + logDetail.abbreviate(30000); //上限を超えないよう、30000文字以降は...で表示する
		E_log__c log = E_LogUtil.getLogRec('IRIS_MeritTableChart' , detail );
		log.Name = '推移表呼び出し';

		insert log;
	}

}