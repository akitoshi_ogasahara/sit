global with sharing class E_CustomerInfoController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Contact record {get{return (Contact)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_CustomerInfoExtender getExtender() {return (E_CustomerInfoExtender)extender;}
	
	
	public coliTab coliTab {get; private set;}
	public spvaTab spvaTab {get; private set;}
	public anTab anTab {get; private set;}
	public tiTab tiTab {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_CustomerInfoController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1716');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1716',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','手続履歴');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_ProcessHistory?cid={!extender.cid}&cno={!Extender.ContractorCLNTNUM}&cname={!URLENCODE(Extender.ContractorName)}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1717');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1717',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_CustomerInfo');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','90');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1723');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1723',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','100件以上の表示はこちら');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{! coliTab.items.size >= 100 }');
        tmpPropMap.put('p_value','E_ColiSearch?cId={!extender.cId}&disId={!extender.disId}&offId={!extender.offId}&ageId={!extender.ageId}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','144');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1724');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1724',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!coliTab_item.record.E_Policy__r.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','73');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1710');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1710',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','100件以上の表示はこちら');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{! spvaTab.items.size >= 100 }');
        tmpPropMap.put('p_value','E_SpvaSearch?cId={!extender.cId}&disId={!extender.disId}&offId={!extender.offId}&ageId={!extender.ageId}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','146');
		tmpPropMap.put('Component__Height','17');
		tmpPropMap.put('Component__id','Component1725');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1725',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!spvaTab_item.record.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','21');
		tmpPropMap.put('Component__id','Component1711');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1711',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','100件以上の表示はこちら');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{! anTab.items.size >= 100 }');
        tmpPropMap.put('p_value','E_AnnuitySearch?cId={!extender.cId}&disId={!extender.disId}&offId={!extender.offId}&ageId={!extender.ageId}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','138');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1726');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1726',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!anTab_item.record.SPVA_ZANNFROM__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1797');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1797',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1722');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1722',tmpPropMap);


		SObjectField f;

		f = E_COVPF__c.fields.COMM_CHDRNUM__c;
		f = E_COVPF__c.fields.COLI_ZPSTDESC__c;
		f = E_COVPF__c.fields.InsuredName__c;
		f = E_COVPF__c.fields.COLI_ZCOVRNAMES__c;
		f = E_COVPF__c.fields.COLI_SUMINSF__c;
		f = E_Policy__c.fields.COMM_SINSTAMT__c;
		f = E_Policy__c.fields.COMM_CHDRNUM_LINK__c;
		f = E_Policy__c.fields.COMM_ZRSTDESC__c;
		f = E_Policy__c.fields.InsuredName__c;
		f = E_Policy__c.fields.COMM_ZCOVRNAM__c;
		f = E_Policy__c.fields.FinancialReserve__c;
		f = E_Policy__c.fields.COMM_ZTOTPREM__c;
		f = E_Policy__c.fields.SPVA_ZANNSTATD__c;
		f = E_Policy__c.fields.SPVA_ZANNDSCA01__c;
		f = E_ITHPF__c.fields.ZCLNTCDE_LINK__c;
		f = E_ITHPF__c.fields.ContractorName__c;
		f = E_ITHPF__c.fields.ContractorCLTPF_CLNTNUM__c;
		f = E_COVPF__c.fields.COLI_PSTATCODE__c;
		f = E_COVPF__c.fields.COLI_ZCRIND__c;
		f = E_COVPF__c.fields.RecordTypeId;
		f = E_Policy__c.fields.RecordTypeId;
		f = E_Policy__c.fields.SpClassification__c;
		f = E_Policy__c.fields.COMM_OCCDATE__c;
		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.SPVA_ZANNSTAT__c;
		f = E_ITHPF__c.fields.ZCLNTCDE__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Contact.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('Contact');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			coliTab = new coliTab(new List<E_COVPF__c>(), new List<coliTabItem>(), new List<E_COVPF__c>(), null);
			listItemHolders.put('coliTab', coliTab);
			query = new SkyEditor2.Query('E_COVPF__c');
			query.addFieldAsOutput('COMM_CHDRNUM__c');
			query.addFieldAsOutput('COLI_ZPSTDESC__c');
			query.addFieldAsOutput('InsuredName__c');
			query.addFieldAsOutput('COLI_ZCOVRNAMES__c');
			query.addFieldAsOutput('COLI_SUMINSF__c');
			query.addFieldAsOutput('E_Policy__r.COMM_SINSTAMT__c');
			query.addFieldAsOutput('E_Policy__r.Contractor__r.E_CLTPF_CLNTNUM__c');
			query.addFieldAsOutput('E_Policy__r.Contractor__r.Name');
			query.addFieldAsOutput('E_Policy__r.Contractor__r.FirstName');
			query.addFieldAsOutput('E_Policy__r.COMM_OCCDATE__c');
			query.addFieldAsOutput('E_Policy__r.ContractorCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('E_Policy__r.ContractorName__c');
			query.addFieldAsOutput('E_Policy__r.AnnuitantCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('E_Policy__r.AnnuitantName__c');
			query.addFieldAsOutput('E_Policy__r.COMM_CHDRNUM_LINK__c');
			query.addFieldAsOutput('E_Policy__r.COMM_CHDRNUM_LINK_I__c');
			query.addFieldAsOutput('RecordTypeId');
			coliTab.queryRelatedEvent = False;
			query.limitRecords(100);
			queryMap.put('coliTab', query);
			
			spvaTab = new spvaTab(new List<E_Policy__c>(), new List<spvaTabItem>(), new List<E_Policy__c>(), null);
			listItemHolders.put('spvaTab', spvaTab);
			query = new SkyEditor2.Query('E_Policy__c');
			query.addFieldAsOutput('COMM_CHDRNUM_LINK__c');
			query.addFieldAsOutput('COMM_ZRSTDESC__c');
			query.addFieldAsOutput('InsuredName__c');
			query.addFieldAsOutput('COMM_ZCOVRNAM__c');
			query.addFieldAsOutput('FinancialReserve__c');
			query.addFieldAsOutput('COMM_ZTOTPREM__c');
			query.addFieldAsOutput('Id');
			query.addFieldAsOutput('SPVA_ZREVAMT01__c');
			query.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('ContractorName__c');
			query.addFieldAsOutput('COMM_OCCDATE__c');
			query.addFieldAsOutput('COMM_CHDRNUM__c');
			query.addFieldAsOutput('AnnuitantName__c');
			query.addFieldAsOutput('AnnuitantCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('RecordTypeId');
			spvaTab.queryRelatedEvent = False;
			query.limitRecords(100);
			queryMap.put('spvaTab', query);
			
			anTab = new anTab(new List<E_Policy__c>(), new List<anTabItem>(), new List<E_Policy__c>(), null);
			listItemHolders.put('anTab', anTab);
			query = new SkyEditor2.Query('E_Policy__c');
			query.addFieldAsOutput('COMM_CHDRNUM_LINK__c');
			query.addFieldAsOutput('SPVA_ZANNSTATD__c');
			query.addFieldAsOutput('InsuredName__c');
			query.addFieldAsOutput('COMM_ZCOVRNAM__c');
			query.addFieldAsOutput('SPVA_ZANNDSCA01__c');
			query.addFieldAsOutput('Id');
			query.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('ContractorName__c');
			query.addFieldAsOutput('COMM_CHDRNUM__c');
			query.addFieldAsOutput('SPVA_ZANNFROM__c');
			query.addFieldAsOutput('AnnuitantName__c');
			query.addFieldAsOutput('AnnuitantCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('RecordTypeId');
			anTab.queryRelatedEvent = False;
			query.limitRecords(100);
			queryMap.put('anTab', query);
			
			tiTab = new tiTab(new List<E_ITHPF__c>(), new List<tiTabItem>(), new List<E_ITHPF__c>(), null);
			listItemHolders.put('tiTab', tiTab);
			query = new SkyEditor2.Query('E_ITHPF__c');
			query.addFieldAsOutput('ZCLNTCDE_LINK__c');
			query.addFieldAsOutput('ContractorName__c');
			query.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c');
			query.addFieldAsOutput('Id');
			query.addFieldAsOutput('ZCLNTCDE__c');
			tiTab.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('tiTab', query);
			
			
			SkyEditor2.Query coliTabQuery = queryMap.get('coliTab');
			coliTabQuery.addWhereIfNotFirst('AND');
			coliTabQuery.addWhere('( COLI_PSTATCODE__c != \'A\' AND COLI_PSTATCODE__c != \'J\' AND COLI_ZCRIND__c = \'C\' AND COLI_PSTATCODE__c != \'K\' AND RecordType.DeveloperName = \'ECOVPF\')');
			SkyEditor2.Query spvaTabQuery = queryMap.get('spvaTab');
			spvaTabQuery.addWhereIfNotFirst('AND');
			spvaTabQuery.addWhere('( RecordType.DeveloperName = \'ESPHPF\' AND  ( SpClassification__c = \'SPVA\' OR SpClassification__c = \'SPVWL\' OR SpClassification__c = \'SPVA&ANNUITY\' ) )');
			spvaTabQuery.addSort('COMM_OCCDATE__c',True,False).addSort('COMM_CHDRNUM__c',True,False);
			SkyEditor2.Query anTabQuery = queryMap.get('anTab');
			anTabQuery.addWhereIfNotFirst('AND');
			anTabQuery.addWhere('( RecordType.DeveloperName = \'ESPHPF\' AND SpClassification__c = \'ANNUITY\' AND SPVA_ZANNSTAT__c != \'C\')');
			anTabQuery.addSort('COMM_OCCDATE__c',True,False).addSort('COMM_CHDRNUM__c',True,False);
			SkyEditor2.Query tiTabQuery = queryMap.get('tiTab');
			tiTabQuery.addSort('ZCLNTCDE__c',True,False);
			p_showHeader = false;
			p_sidebar = false;
			sve_ClassName = 'E_CustomerInfoController';
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_CustomerInfoExtender(this);
			init();
			
			coliTab.extender = this.extender;
			spvaTab.extender = this.extender;
			anTab.extender = this.extender;
			tiTab.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class coliTabItem extends SkyEditor2.ListItem {
		public E_COVPF__c record{get; private set;}
		@TestVisible
		coliTabItem(coliTab holder, E_COVPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class coliTab extends SkyEditor2.ListItemHolder {
		public List<coliTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			coliTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<coliTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new coliTabItem(this, (E_COVPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class spvaTabItem extends SkyEditor2.ListItem {
		public E_Policy__c record{get; private set;}
		@TestVisible
		spvaTabItem(spvaTab holder, E_Policy__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class spvaTab extends SkyEditor2.ListItemHolder {
		public List<spvaTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			spvaTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<spvaTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new spvaTabItem(this, (E_Policy__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class anTabItem extends SkyEditor2.ListItem {
		public E_Policy__c record{get; private set;}
		@TestVisible
		anTabItem(anTab holder, E_Policy__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class anTab extends SkyEditor2.ListItemHolder {
		public List<anTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			anTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<anTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new anTabItem(this, (E_Policy__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class tiTabItem extends SkyEditor2.ListItem {
		public E_ITHPF__c record{get; private set;}
		@TestVisible
		tiTabItem(tiTab holder, E_ITHPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class tiTab extends SkyEditor2.ListItemHolder {
		public List<tiTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			tiTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<tiTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new tiTabItem(this, (E_ITHPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}