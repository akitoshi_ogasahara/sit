public with sharing class E_CHTPFDao {
    /**
     * @param keys 証券番号+募集人コードSet
     * @return List<E_CHTPF__c>
     */
    public static List<E_CHTPF__c> getRecsByCHDRAndAGNT (Set<String> keys){
        if(keys.isEmpty()) return new List<E_CHTPF__c>();
        return [Select id ,E_CHTPFKey__c From E_CHTPF__c Where E_CHTPFKey__c in :keys];
    }
    /**
     * @param keys 契約ヘッダIDSet
     * @return List<E_CHTPF__c>
     */
    public static List<E_CHTPF__c> getRecsByPlcyIds (Set<Id> keys){
        if(keys.isEmpty()) return new List<E_CHTPF__c>();
        return [Select E_Policy__c,AGNTNUM__c,ZATSEQNO__c From E_CHTPF__c Where E_Policy__c in :keys];
    }
}