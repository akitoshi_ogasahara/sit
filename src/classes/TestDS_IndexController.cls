@isTest
private class TestDS_IndexController {

/*
* init()
*/
	@isTest static void init_test01() {
		PageReference pageRef = Page.E_DSIndex;
		Test.setCurrentPageReference(pageRef);
		DS_IndexController ctrl =  new DS_IndexController();

		Test.startTest();
		ctrl.pageAction();
		Test.stopTest();
		System.assertEquals(DS_Const.PAGE_NAME_DS_INDEX, ctrl.getPageName());
	}

	/*
	* 観点: indexDsMap_getter 正常系
	* 条件: 傷病名カナ頭文字(KanaInitial__c)に全角カナを入力
	* 検証: DS_Const.INITIAL_MAPに沿った順で、傷病名カナ頭文字(KanaInitial__c)を基準にレコードがMapingされること
	*/
	@isTest static void getIndexDsMap_test01() {
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');

		PageReference pageRef = Page.E_DSIndex;
		Test.setCurrentPageReference(pageRef);
		DS_IndexController ctrl =  new DS_IndexController();

		Test.startTest();
		ctrl.showIndex = 'カ';
		Test.stopTest();

		System.assertEquals(5, ctrl.indexDsMap.get(ctrl.showIndex).size());
	}

	@isTest static void getSumiseiParam_test01() {
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'システム管理者');
		DS_IndexController ctrl =  new DS_IndexController();
		ApexPages.currentPage().getParameters().put('isSMTM', '1');

		System.runAs(actUser){
			Test.startTest();
				System.assertEquals('&isSMTM=1', ctrl.getSumiseiParam());
			Test.stopTest();
		}
	}
}