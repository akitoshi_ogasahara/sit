public without sharing class SC_AppManager {
	public class SCAppManagerException extends Exception{}

	public SC_Office__c record{get; private set;}
	public SC_SelfCompliance__c parent{get; private set;}

	public Enum StatusCode {SCE001,SCE002,SCE003,SCE004,SCE005,SCE006}
	public Map<StatusCode, String> messages = new Map<StatusCode, String>{
														StatusCode.SCE001 => 'SCE001::tokenがありません。エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE001】',
														StatusCode.SCE002 => 'SCE002::survey idがありません。エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE002】',
														StatusCode.SCE003 => 'SCE003::tokenが無効です。token:{0}エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE003】',
														StatusCode.SCE004 => 'SCE004::tokenまたはsurvey idが無効です。token:{0} / survey id:{1}エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE004】',
														StatusCode.SCE005 => 'SCE005::提出方法が添付ファイルに変更されています。エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE005】',
														StatusCode.SCE006 => 'SCE006::ステータスが未提出以外に変更されています。エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE006】'
													};

	/*
	 * コンストラクタ(GET)
	 *
	 */
	public SC_AppManager(String token) {
		if (String.isEmpty(token)) {
			throw new SCAppManagerException(messages.get(StatusCode.SCE001)); 
		}
		try {
			record = [select
						  id
						, AccZEAYNAM__c
						, AccZAGCYNUM__c
						, OwnerMRUnitName__c
						, OwnerMRName__c
						, Status__c
						, DispDefect__c
						, OfficeType__c
						, FiscalYear__c
						, ZAGCYNUM__c
						, ZEAYNAM__c
						, OwnerMRCode__c
						, CMDRemandDate__c
						, Defect__c
						, Inquiry__c
						, Comment__c
						, StatusHistory__c
						, RemandSource__c
						, MRManager__c
						, SendInquiryMailCMD__c
						, InquiryAgCmd__c
						, InquiryCmdAg__c
						, InquiryMrAg__c
					  from SC_Office__c
					  where LimeSurveyTokenCommon__c = :token
					  or LimeSurveyTokenAdd__c = :token];
		} catch (Exception e) {
			List<String> fillers = new String[]{token};
			String errMsg = String.format(messages.get(StatusCode.SCE003), fillers);
			throw new SCAppManagerException(errMsg);
		}
	}

	/*
	 * コンストラクタ(POST)
	 *
	 */
	public SC_AppManager(String token, Integer surveyid) {
		if (String.isEmpty(token)) {
			throw new SCAppManagerException(messages.get(StatusCode.SCE001)); 
		}
		if (surveyid == null) {
			throw new SCAppManagerException(messages.get(StatusCode.SCE002)); 
		}
		try {

			parent = [select id, Kind__c, TypeSubmit__c, SC_Office__r.Status__c from SC_SelfCompliance__c
					  where LimeSurveyToken__c = :token
					  and LimeSurveyId__c = :String.valueOf(surveyid) Limit 1];

			system.debug('debug:parent=' + [select id, Kind__c, TypeSubmit__c, SC_Office__r.Status__c from SC_SelfCompliance__c where LimeSurveyToken__c = :token and LimeSurveyId__c = :String.valueOf(surveyid)]);
		} catch (Exception e) {
			List<String> fillers = new String[]{token, String.valueOf(surveyid)};
			String errMsg = String.format(messages.get(StatusCode.SCE004), fillers);
			throw new SCAppManagerException(errMsg);
		}
	}

	public void validate() {
		if (parent.TypeSubmit__c == 'PDF') {
			throw new SCAppManagerException(messages.get(StatusCode.SCE005));
		}

		if (parent.SC_Office__r.Status__c != '未提出') {
			throw new SCAppManagerException(messages.get(StatusCode.SCE006));
		}
	}

	public String getAccess() {
		String access = 'no access';

		SC_AuthorityConfirmation ac = new SC_AuthorityConfirmation(record);
		if (ac.getAuthority(ac.TYPE_WEB_ANSWER)) access = 'edit';
		else if (ac.getAuthority(ac.TYPE_WEB_READ)) access = 'read';

		return access;
	}

	public String getUserType() {
		User us = [select id, contactId from User where id = :UserInfo.getUserId()];
		if (us.contactId != null) {
			return 'agent';
		}
		return 'employee';
	}

}