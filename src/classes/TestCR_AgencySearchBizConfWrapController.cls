/**
 * 
 */
@isTest
private class TestCR_AgencySearchBizConfWrapController {

	/**
	 * Constructor
	 */
    static testMethod void test_constructor01() {
        CR_AgencySearchBizConfWrapController target = new CR_AgencySearchBizConfWrapController();
        System.assertEquals(target.getPageTitle(), '大規模代理店権限設定');  
    }
    
    /**
     * pageAction
     */
    private static testMethod void test_pageAction01(){
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		System.runAs(usr){
			Test.startTest();
			PageReference pageRef = Page.E_CRAgencySearchBizConf;
			Test.setCurrentPage(pageRef);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizConfWrapController target = new CR_AgencySearchBizConfWrapController();
						
			// 実行結果確認
			System.assertEquals(target.pageAction(), null);
						
			Test.stopTest();			
		}
	}
}