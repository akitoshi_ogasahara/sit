/**
 * 新契約ステータスダウンロードクラス
 */
public with sharing class E_DownloadNewPolicyExtension extends E_CSVExportController{
	// Controller
	E_DownloadController controller;

	// URLパラメータ
	@TestVisible private ID distributeId;
	@TestVisible private ID officeId;
	@TestVisible private ID agentId;
	@TestVisible private String paramOfficeId = '';

	// 検索条件表示フラグ
	public Boolean isDispSearchArea {get; private set;}

	// 事務所選択値
	public String[] selOfficeList {get; set;}

	// タイトル
	public String getTitle(){
		return '新契約ステータスダウンロード';
	}

	// 不備件数上限値　E_ExportNewPolicyControllerから移設
	private static Integer MAX_CNT_DEFECTS = 10;
	//E_ExportNewPolicyControllerから移設
	private Set<String> getFlupStatus(){
		return new Set<String>{I_NewPolicyConst.STATUS_NAME_P2,I_NewPolicyConst.STATUS_NAME_P3};
	}

	/**
	* Constructor
	*/
	public E_DownloadNewPolicyExtension(E_DownloadController controller) {
		if(this.controller==null){
			this.controller = controller;
		}
	}

/**
   * PageAction
   */
	public PageReference pageAction(){
		try{
			isDispSearchArea = true;
			
			// パラメータによる実行者情報取得
			controller.isValidate = controller.getActor();
			
			// IDセット
			distributeId = controller.distributor == null ? null :  controller.distributor.Id;
			officeId = controller.office == null ? null :  controller.office.Id;
			agentId = controller.agent == null ? null :  controller.agent.Id;
			
			// 新契約ステータス取得
			List<E_NewPolicy__c> policys = E_NewPolicyDao.getRecsForCsv(distributeId, officeId, agentId);

			// 対象データ存在チェック
			if(policys.isEmpty()){
				getPageMessages().addErrorMessage(controller.getMSG().get('NPD|001'));
				isDispSearchArea = false;
			}
		}catch(Exception e){
				isDispSearchArea = false;
				getPageMessages().addErrorMessage(e.getMessage());
		}

		return null;
	}

	/**
	* CSVダウンロードボタン押下
	*/
	public PageReference doDownloadCsv(){
		try{
			//String paramOfficeId = '';
			String paramAgentId = '';
			// 実行権限：AH
			if(distributeId != null){
			// 事務所選択チェック
			if(selOfficeList.size() == 0){
				system.debug('err');
				getPageMessages().addErrorMessage(controller.getMSG().get('NPD|002'));
				return null;
			}
			// 事務所セット
			paramOfficeId = String.join(selOfficeList, ',');

			// 実行権限：AY
			}else if(officeId != null){
				paramOfficeId = officeId;
			// 実行権限：AT
			}else if(agentId != null){
				paramAgentId = agentId;
			// それ以外
			}else{
				system.debug('err');
				getPageMessages().addErrorMessage(controller.getMSG().get('NPD|003'));
				return null;
			}
			//PageReference pr = Page.E_ExportNewPolicy;
			//pr.getParameters().put('oid', paramOfficeId);
			//pr.getParameters().put('aid', paramAgentId);
			//return pr;
			return Page.E_ExportNewPolicy.setRedirect(false);
		}catch(Exception e){
			isDispSearchArea = false;
			getPageMessages().addErrorMessage(e.getMessage());
			return null;
		}
	}

	//E_ExportNewPolicyControllerから移設
	public override String mainSObjectAPIName(){
		return Schema.SObjectType.Account.Name;
	}

	/**
	* ヘッダー　E_ExportNewPolicyControllerから移設
	*/
	public override String getCSVHeader(){
		List<String> header = new List<String>();
		header.add('申込番号');
		header.add('証券番号');
		header.add('契約データ入力日');
		header.add('ステータス更新日');
		header.add('新契約ステータス');
		header.add('入金日');
		header.add('成立処理日');
		header.add('募集年');
		header.add('募集月');
		header.add('募集人コード');
		header.add('支部コード');
		header.add('個人コード');
		header.add('取扱者漢字名');
		header.add('募集人コード（従）');
		header.add('支部コード（従）');
		header.add('個人コード（従）');
		header.add('取扱者漢字名（従）');
		header.add('契約者名（漢字）');
		header.add('契約者名（カナ）');
		header.add('被保険者名（漢字）');
		header.add('被保険者名（カナ）');
		header.add('保険種類（主契約）');
		header.add('1P合計保険料（主契約＋特約）');    
		// 不備情報　10件
		for(Integer i = 1; i < (MAX_CNT_DEFECTS + 1); i++){
			header.add('不備発信日' + i);
			header.add('不備ステータス' + i);
			header.add('不備コード' + i);
			header.add('不備内容' + i);
			header.add('不備内容詳細' + i);
			header.add('不備処理方法' + i + '-1');
			header.add('不備処理方法' + i + '-2');
			header.add('不備処理方法' + i + '-3');
		}
		header.add('団体番号');
		header.add('データ配信日時');
		header.add('支社名（事務所名）');
		header.add('支社名（事務所名）(従)');
		header.add('申込日');
		header.add('告知日');
		header.add('診査方法');
		header.add('保険金額');
		header.add('被保険者生年月日');
		header.add('被保険者性別');
		header.add('契約時年齢');
		header.add('払方');
		header.add('払込経路');
		header.add('特別保険料');
		header.add('入金済保険料金額');
		header.add('入金済保険料金額との差額');
		header.add('ANP');
		system.debug(String.join(header, CSV_SEPARATOR()) + CSV_BREAKELINE());

		return String.join(header, CSV_SEPARATOR()) + CSV_BREAKELINE();
	}
	/**
	* データ行作成　E_ExportNewPolicyControllerから移設
	*/
	public List<String> getLines(){
System.debug('getLines start');
		try{
			List<String> lines = new List<String>();
			// データ取得
			List<E_NewPolicy__c> policys = E_NewPolicyDao.getRecsForCsv(distributeId, paramOfficeId, agentId);
			// CSVデータ作成
			for(E_NewPolicy__c policy : policys){
				List<String> ln = new List<String>();
				
				ln.add(E_Util.getEscapedCSVString(policy.ZPROPNUM__c));              // 申込書番号（9桁）
				ln.add(E_Util.getEscapedCSVString(policy.CHDRNUM__c));              // 証券番号  
				ln.add(E_Util.getEscapedCSVString(getString(policy.EntryDate__c)));        // 契約データ入力日
				/* 住友対応：不備対応中の場合は、最新の不備情報処理日を表示 */
				if(getFlupStatus().contains(policy.Status__c)){
				  ln.add(E_Util.getEscapedCSVString(getString(policy.FLUP_TRDT__c)));              // 最新不備処理日
				}else{
				  ln.add(E_Util.getEscapedCSVString(policy.StatusDate__c.format('yyyyMMdd')));  // ステータス発信日
				}
				ln.add(E_Util.getEscapedCSVString(policy.Status__c));              // ステータス
				ln.add(E_Util.getEscapedCSVString(getString(policy.EFFDATE__c)));        // 入金日
				ln.add(E_Util.getEscapedCSVString(getString(policy.PTRN_TRDT__c)));        // 成立日
				ln.add(E_Util.getEscapedCSVString(getString(policy.BATCACTYR__c)));        // 成立Ａ／Ｃ （年）
				ln.add(E_Util.getEscapedCSVString(numLpad(policy.BATCACTMN__c)));        // 成立Ａ／Ｃ（月）
				ln.add(E_Util.getEscapedCSVString(policy.AGNTNUM__c));              // 募集人コード（主）
				ln.add(E_Util.getEscapedCSVString(policy.ZAYSECT__c));              // 支部コード
				ln.add(E_Util.getEscapedCSVString(policy.ZBKCLCDE__c));              // 個人コード
				ln.add(E_Util.getEscapedCSVString(policy.AgentName__c));            // 取扱者名
				ln.add(E_Util.getEscapedCSVString(policy.SubAGNTNUM__c));            // 募集人コード（従）
				ln.add(E_Util.getEscapedCSVString(policy.SubZJAYSECT__c));            // 支部コード（従）
				ln.add(E_Util.getEscapedCSVString(policy.SubZBKCLCDE__c));            // 個人コード（従）
				ln.add(E_Util.getEscapedCSVString(policy.SubAgentName__c));            // 取扱者名（従）
				//ln.add(E_Util.getEscapedCSVString(policy.COMM_ZCLNAME__c));					// 契約者
				//ln.add(E_Util.getEscapedCSVString(policy.ContractorE_CLTPF_ZCLKNAME__c));		// 契約者（カナ）
				//ln.add(E_Util.getEscapedCSVString(policy.COMM_ZINSNAM__c));						// 被保険者
				//ln.add(E_Util.getEscapedCSVString(policy.InsuredE_CLTPF_ZCLKNAME__c));			// 被契約者（カナ）
				//外字対応
				ln.add(E_Util.getEscapedCSVString(policy.ContractorName__c));					// 契約者
				//ln.add(E_Util.getEscapedCSVString(policy.ContractorNameKana__c));		// 契約者（カナ）
				ln.add(E_Util.getEscapedCSVString(policy.ContractorE_CLTPF_ZCLKNAME__c));		// 契約者（カナ）
				ln.add(E_Util.getEscapedCSVString(policy.InsuredName__c));						// 被保険者
				//ln.add(E_Util.getEscapedCSVString(policy.InsuredNameKana__c));			// 被契約者（カナ）
				ln.add(E_Util.getEscapedCSVString(policy.InsuredE_CLTPF_ZCLKNAME__c));			// 被契約者（カナ）
				ln.add(E_Util.getEscapedCSVString(policy.InsuranceType__c));							// 保険種類（主契約）
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.INSTTOT01__c)));    // 1P合計保険料
				
				// 不備情報セット
				Integer defCnt = 0;
				if(I_NewPolicyConst.finalStatus.contains(policy.Status__c) == false){
					defCnt = policy.E_NewPolicyDefects__r.size();
					for(E_NewPolicyDefect__c def : policy.E_NewPolicyDefects__r){
						ln.add(E_Util.getEscapedCSVString(String.valueOf(def.FULP_FUPDT__c)));    // 不備発信日
						ln.add(E_Util.getEscapedCSVString(def.FUPSTAT__c));              // 不備ステータス
						ln.add(E_Util.getEscapedCSVString(def.FUPCDE__c));              // 不備コード
						ln.add(E_Util.getEscapedCSVString(def.T5661_LONGDESC__c));          // 不備内容
						ln.add(E_Util.getEscapedCSVString(def.ZFUPRMK__c));              // 不備内容詳細
						ln.add(E_Util.getEscapedCSVString(def.TL735_LONGDESC__c));          // 不備処理方法1
						ln.add(E_Util.getEscapedCSVString(def.TL735_LONGDESC2__c));          // 不備処理方法2
						ln.add(E_Util.getEscapedCSVString(def.TL735_LONGDESC3__c));          // 不備処理方法3
					}
				}

				// 不備情報の不足分追加（10まで）
				if(defCnt < 10){
					Integer addCnt = MAX_CNT_DEFECTS - defCnt;
					for(Integer i = 0; i < addCnt; i++){
						ln.add('');    // 不備発信日
						ln.add('');    // 不備ステータス
						ln.add('');    // 不備コード
						ln.add('');    // 不備内容
						ln.add('');    // 不備内容詳細
						ln.add('');    // 不備処理方法1
						ln.add('');    // 不備処理方法2
						ln.add('');    // 不備処理方法3
					}
				}
				
				ln.add(E_Util.getEscapedCSVString(policy.GRUPKEY__c));                // 団体番号
				ln.add(E_Util.getEscapedCSVString(policy.SyncDate__c.format('yyyyMMddHHmmss')));  // データ発信日

				//csv項目住友生命リクエスト追加分
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.ZAYKNJ__c)));		// 支社名（事務所）
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.SUBZAYKNJ__c)));		// 支社名（事務所）（従）
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.STMPDTYDTE__c)));		// 申込日
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.STATEMENTDATE__c)));		// 告知日
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.SELECTION__c)));		// 診査方法
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.SUMINS__c)));		// 保険金額
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.LIFEDOB__c)));		// 被保険者生年月日
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.LIFESEX__c)));		// 被保険者性別
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.ANBATCCD__c)));		// 契約時年齢
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.BILLFREQ__c)));		// 払方
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.BILLCHNL__c)));		// 払込経路
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.SPECIAPREM__c)));	// 特別保険料
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.PAYEDPREM__c)));		// 入金済保険料金額
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.DIFFPREM__c)));		// 入金済保険料金額との差額
				ln.add(E_Util.getEscapedCSVString(String.valueOf(policy.ZANNPREM__c)));		// ANP
				
				lines.add(String.join(ln, CSV_SEPARATOR()) + CSV_BREAKELINE());
			}
		System.debug('lines:'+lines);
			return lines;
		}catch(Exception e){
			return null;
		}
	}

	/**
	*数値が0の時空白をかえす　E_ExportNewPolicyControllerから移設
	*/
	public String getString(Decimal d){
		String s;
		if(d == null ||d == 0){
			s = '';
		}
		else{
			s = String.valueOf(d);
		}
		return s;
	}

	/**
	* 左0埋め　2桁　E_ExportNewPolicyControllerから移設
	*/
	private String numLpad(Decimal dec){
		String str = getString(dec);
		
		if(String.isNotBlank(str)){
			str = E_Util.leftPad(str, 2, '0');
		}
		return str;
	}
}