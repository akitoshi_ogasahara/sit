public with sharing class I_TamotsukunIntroductionController extends I_CMSController {
	public String pageTitle;
	public I_CMSSection captureSection;
	public I_CMSSection downLoadSection;

	public I_TamotsukunIntroductionController(){
		super();
	}

	protected virtual override Id getIrisMenuId(){
		String pkey='iris_tamotsukun';
        this.activePage = I_PageMasterDao.getRecordByKey(pkey);
        //pgkeyがあるのに取得できない場合エラーとする
        if (this.activePage == null) {
          throw new GeneralException('ページが存在しません['+ ApexPages.currentPage().getUrl() +']');
        }

        return this.activePage.Menu__c;
	}

	protected virtual override String getLinkMenuKey(){
		return 'tamotsukunIntroduction';
	}

	public I_CMSSection getCaputureSection(){
		for( Integer i = 0; i < CMSSections.size() ; i++  ){
			if( CMSSections[i].record.Name == 'たもつくんキャプチャ' ){
				captureSection = CMSSections.remove(i);//キャプチャ部分はセクションリストから削除する
				return captureSection;
			}
		}
		return null;
	}

	public I_CMSSection getdownLoadSection(){
		for( Integer i = 0; i < CMSSections.size() ; i++ ){
			if( CMSSections[i].record.Name == 'たもつくんダウンロード' ){
				downLoadSection = CMSSections.remove(i);//ダウンロード部分はセクションリストから削除する
				return downLoadSection;
			}
		}
		return null;
	}

}