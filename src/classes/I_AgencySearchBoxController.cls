public with sharing class I_AgencySearchBoxController extends I_AbstractSearchBoxController{
// 出力
	// 検索結果リスト
	public List<I_AgencySearchBoxRow> agencyRows {get; set;}

	//IRISエンハンス IRIS-751 IRIS-752 IRIS-753 ここから
	//ラジオボタン取得変数
	public String selection{get; set;}
	//IRISエンハンス IRIS-751 IRIS-752 IRIS-753 ここまで

// 制御
	// 表示制御 - 募集人リストを表示
	public Boolean getIsDisplayAgentList() {
		return exeSearchType == I_Const.SEARCH_TYPE_AGENT;
	}

	// 表示制御 - 事務所リストを表示
	public Boolean getIsDisplayOfficeList() {
		return exeSearchType == I_Const.SEARCH_TYPE_OFFICE;
	}

	// 表示制御 - 代理店リストを表示
	public Boolean getIsDisplayAgencyList() {
		return exeSearchType == I_Const.SEARCH_TYPE_AGENCY;
	}

// ソート用
	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

// ローカル定数
	private static final String SORT_TYPE_AGENCY_NAME = 'agencyName';
	private static final String SORT_TYPE_AGENCY_CODE = 'agencyCode';
	private static final String SORT_TYPE_OFFICE_NAME = 'officeName';
	private static final String SORT_TYPE_OFFICE_CODE = 'officeCode';
	private static final String SORT_TYPE_ADDRESS     = 'address';
	private static final String SORT_TYPE_AGENT_NAME  = 'agentName';
	private static final String SORT_TYPE_AGENT_CODE  = 'agentCode';

	public String getSearchCategoryLabel(){
		return I_Const.SEARCH_LABEL_CATEGORY_AGENCY;
	}

	//IRISエンハンス IRIS-751 IRIS-752 IRIS-753 ここから
	//ラジオボタン作成
	public List<SelectOption> getSearchOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('and', 'and検索'));
		options.add(new SelectOption('or', 'or検索'));
		return options;
	}
	//IRISエンハンス IRIS-751 IRIS-752 IRIS-753 ここまで

	// コンストラクタ
	public I_AgencySearchBoxController(){
		super();
		agencyRows = new List<I_AgencySearchBoxRow>();

		// 初期値をセット
		searchCategory = I_Const.SEARCH_LABEL_CATEGORY_AGENCY;
		rowCount = I_Const.LIST_DEFAULT_ROWS;

		//IRISエンハンス IRIS-751 IRIS-752 IRIS-753 ここから
		//ラジオボタン初期値
		selection = 'and';
		//IRISエンハンス IRIS-751 IRIS-752 IRIS-753 ここまで

		searchTypeItems = new List<SelectOption>();
		// AY
		if(access.isAuthAY()){
			searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_AGENT, I_Const.SEARCH_TYPE_LABEL_AGENT));
		}
		// AH
		if(access.isAuthAH()){
			searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_AGENT, I_Const.SEARCH_TYPE_LABEL_AGENT));
			searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_OFFICE, I_Const.SEARCH_TYPE_LABEL_OFFICE));
		}
		// 社員
		if(access.isEmployee()){
			searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_AGENT, I_Const.SEARCH_TYPE_LABEL_AGENT));
			searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_OFFICE, I_Const.SEARCH_TYPE_LABEL_OFFICE));
			searchTypeItems.add(new SelectOption(I_Const.SEARCH_TYPE_AGENCY, I_Const.SEARCH_TYPE_LABEL_AGENCY));
		}

		flush();
	}

	public void flush() {
		keyword = '';
		if(String.isBlank(searchType)){
			searchType = I_Const.SEARCH_TYPE_AGENT;
			searchTypeName = I_Const.SEARCH_TYPE_LABEL_AGENT;
		}
		isSearch = false;
		agencyRows.clear();
		searchMessages.clear();
		//JIRA_#63 170331
//		getSeachBoxLogRecord(searchType);
		getSeachBoxLogRecord(searchTypeName);
	}

	// 検索処理
	public void doSearch(){
		searchMessages.clear();

		// 入力チェック
		if(!isInputValidate()){
			return;
		}

		// 変数の初期化
		agencyRows = new List<I_AgencySearchBoxRow>();
		exeSearchType = searchType;
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		isSearch = false;
		// 検索ログを再取得
		//JIRA_#63 170331
//		getSeachBoxLogRecord(exeSearchType);
		getSeachBoxLogRecord(searchTypeName);
		// 募集人検索
		if(getIsDisplayAgentList()){
			sortType = SORT_TYPE_AGENT_NAME;
			sortIsAsc = true;
			doSearchAgent();

		// 事務所検索
		} else if(getIsDisplayOfficeList()){
			sortType = SORT_TYPE_OFFICE_NAME;
			sortIsAsc = true;
			doSearchOffice();

		// 代理店検索
		} else if(getIsDisplayAgencyList()){
			sortType = SORT_TYPE_AGENCY_NAME;
			sortIsAsc = true;
			doSearchAgency();
		}

		return;
	}

	// 代理店検索
	private void doSearchAgency(){
		String soqlWhere = '';
		// 支社コードを設定
		String brunchCode = access.idcpf.ZINQUIRR__c.right(2);
		if(brunchCode != '**'){
			soqlWhere = 'AND (E_CL2PF_BRANCH__c = \'' + brunchCode + '\' '
					  + 'OR E_CL2PF_ZBUSBR__c = \'' + brunchCode + '\') ';
		}

		List<Account> agencys = new List<Account>();
		//IRISエンハンス IRIS-753 ここから
		agencys = E_AccountDaoWithout.getRecsIRISAgencySearchBoxAgency(soqlWhere, keyword,selection);
		//IRISエンハンス IRIS-753 ここまで

		// 検索結果がゼロ件の場合は処理終了
		if(agencys == null || agencys.isEmpty()){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return;
		}

		// 検索結果が上限の場合は処理終了
		if(agencys.size() >= I_Const.LIST_MAX_ROWS){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return;
		}

		// 重複チェック用のIDセット
		Set<Id> agencyIdSet = new Set<Id>();
		for(Account acc : agencys){
			if(!agencyIdSet.contains(acc.Parent.Id)) {
				I_AgencySearchBoxRow row = new I_AgencySearchBoxRow();

				row.agencyName = acc.E_ParentName__c;
				row.agencyNameKana = acc.Parent.E_CL1PF_ZAHKNAME__c;
				row.agencyCode = acc.E_ParentZHEADAY__c;
				row.address = acc.Parent.E_COMM_ZCLADDR__c;
				/* 挙積情報対応 */
/*				row.url = 'IRIS_Agency?type=agency&id=' + acc.ParentId
						+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_AMS;
*/
				row.url = 'IRIS_AgencySalesResults?type=agency&id=' + acc.ParentId
						+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_AMS;

				agencyRows.add(row);
				if(agencyRows.size() == I_Const.LIST_MAX_ROWS){
					searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
					return;
				}
				agencyIdSet.add(acc.Parent.Id);
			}
		}

		// ソート
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;
		agencyRows.sort();

		isSearch = true;

		return;
	}

	// 事務所検索
	private void doSearchOffice(){
		// 代理店ユーザの場合は代理店は固定
		String soqlWhere = '';
		if(access.isAgent()){
			soqlWhere += 'AND ParentId = \'' + access.user.Contact.Account.ParentId + '\' ';
		}

		// 社員の場合は支社コードを設定
		if(access.isEmployee()){
			String brunchCode = access.idcpf.ZINQUIRR__c.right(2);
			if(brunchCode != '**'){
				soqlWhere = 'AND (E_CL2PF_BRANCH__c = \'' + brunchCode + '\' '
						  + 'OR E_CL2PF_ZBUSBR__c = \'' + brunchCode + '\') ';
			}
		}

		List<Account> offices = new List<Account>();
		//IRISエンハンス IRIS-752 ここから
		offices = E_AccountDaoWithout.getRecsIRISAgencySearchBoxOffice(soqlWhere, keyword,selection);
		//IRISエンハンス IRIS-752 ここまで

		// 検索結果がゼロ件の場合は処理終了
		if(offices == null || offices.isEmpty()){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return;
		}

		//最大件数以上の場合は警告を表示
		if(offices.size() >= I_Const.LIST_MAX_ROWS){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return;
		}

		for(Account acc : offices){
			I_AgencySearchBoxRow row = new I_AgencySearchBoxRow();

			row.officeName = acc.Name;
			row.officeNameKana = acc.E_CL2PF_ZEAYKNAM__c;
			row.officeCode = acc.E_CL2PF_ZAGCYNUM__c;
			row.address = acc.E_COMM_ZCLADDR__c;
			//row.agencyName = acc.E_ParentName__c;
			//row.agencyNameKana = acc.Parent.E_CL1PF_ZAHKNAME__c;
			row.agencyCode = acc.E_ParentZHEADAY__c;
			/* 挙積情報対応 */
/*			row.url = 'IRIS_Agency?type=office&id=' + acc.Id
					+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_AMS;
*/
			row.url = 'IRIS_AgencySalesResults?type=office&id=' + acc.Id
					+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_AMS;

			agencyRows.add(row);
			if(agencyRows.size() >= I_Const.LIST_MAX_ROWS){
				searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
				return;
			}
		}

		// ソート
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;
		agencyRows.sort();

		isSearch = true;

		return;
	}

	// 募集人検索
	private void doSearchAgent(){
		String soqlWhere = '';
		// 検索条件セット - 社員
		if(access.isEmployee()){
			String brunchCode = access.idcpf.ZINQUIRR__c.right(2);
			// 担当する事務所の募集人のみを抽出
			if(brunchCode != '**'){
				soqlWhere = 'AND (Account.E_CL2PF_BRANCH__c = \'' + brunchCode + '\' '
						  + 'OR Account.E_CL2PF_ZBUSBR__c = \'' + brunchCode + '\') ';
			}
		}

		// 検索条件セット - AH
		if(access.isAuthAH()){
			// 自代理店に所属する募集人のみを抽出
			soqlWhere = 'AND Account.ParentId = \'' + access.user.Contact.Account.ParentId + '\' ';
		}

		// 検索条件セット - AY
		if(access.isAuthAY()){
			// 自事務所に所属する募集人のみを抽出
			soqlWhere = 'AND Account.Id = \'' + access.user.Contact.AccountId + '\' ';
		}

		List<Contact> agents = new List<Contact>();
		//IRISエンハンス IRIS-751 ここまで
		agents = E_ContactDaoWithout.getRecsIRISAgencySearchBoxAgent(soqlWhere, keyword,selection);
		//IRISエンハンス IRIS-751 ここまで

		// 検索結果がゼロ件の場合は処理終了
		if(agents == null || agents.isEmpty()){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return;
		}

		//最大件数以上の場合は警告を表示
		if(agents.size() >= I_Const.LIST_MAX_ROWS){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return;
		}

// 2017/03/14 ADD START
		// ユーザ情報取得
		Set<ID> conIds = new Set<ID>();
		for(Contact con : agents){
			conIds.add(con.Id);
		}

		List<User> userList = E_UserDaoWithout.getRecsAgentIdWithEIDC(conIds);
		Map<ID, Boolean> userActiveMap = new Map<ID, Boolean>();
		Map<String, Boolean> agentActiveMap = new Map<String, Boolean>();
		for(User u : userList){
			userActiveMap.put(u.Contact.Id, u.isActive);

			if(!agentActiveMap.containskey(u.AgentCode__c) || (!agentActiveMap.get(u.AgentCode__c) && u.isActive)){
				agentActiveMap.put(u.AgentCode__c, u.isActive);
			}
		}
// 2017/03.14 ADD END

		for(Contact con : agents){
// 2017/03/14 ADD START
			Boolean isDisplay = false;
			String agentCode = con.E_CL3PF_AGNTNUM__c.substringBefore('|');

			// ユーザ未作成 または ユーザ無効
			if(!userActiveMap.containskey(con.Id) || !userActiveMap.get(con.Id) ){
				// 同一募集人コードに有効ユーザがいない、かつ募集人コードに'|'が含まれていない
				if((!agentActiveMap.containskey(agentCode) || !agentActiveMap.get(agentCode))
				&& con.E_CL3PF_AGNTNUM__c.indexOf('|') == -1){
					isDisplay = true;
				}

			// ユーザが有効
			} else {
				isDisplay = true;
			}

			// 表示判定
			if(!isDisplay){
				continue;
			}
// 2017/03.14 ADD END

			I_AgencySearchBoxRow row = new I_AgencySearchBoxRow();

			row.agentName = con.Name;
			row.agentNameKana = con.E_CL3PF_ZEATKNAM__c;
			row.agentCode = agentCode;
			row.agencyCode = con.E_AccParentCord__c;
			row.officeName = con.E_Account__c;
			row.officeNameKana = con.Account.E_CL2PF_ZEAYKNAM__c;
			row.officeCode = con.Account.E_CL2PF_ZAGCYNUM__c;
			/* 挙積情報対応 */
/*			row.url = 'IRIS_Agent?agent=' + row.agentCode
					+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_AMS;
*/
			row.url = 'IRIS_AgencySalesResults?agent=' + con.id
					+ '&' + I_Const.URL_PARAM_CATEGORY + '=' + I_Const.MENU_CATEGORY_ALIAS_AMS;

			agencyRows.add(row);
		}

		// ソート
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;
		agencyRows.sort();

		isSearch = true;

		return;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return agencyRows == null ? 0 : agencyRows.size();
	}

	/**
	 * ソート
	 */
	public void sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		if(sortType != sType){
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;

		agencyRows.sort();

		return;
	}

	/**
	 * 「最近検索したキーワード」から検索
	 */
	public void doSearchLog(){
		searchType = ApexPages.currentPage().getParameters().get('searchType');
		keyword = ApexPages.currentPage().getParameters().get('keyword');

		doSearch();
	}

	public class I_AgencySearchBoxRow implements Comparable{
		// 代理店名
		public String agencyName {get; set;}
		// 代理店名カナ
		public String agencyNameKana {get; set;}
		// 代理店コード
		public String agencyCode {get; set;}
		// 事務所名
		public String officeName {get; set;}
		// 事務所名カナ
		public String officeNameKana {get; set;}
		// 事務所コード
		public String officeCode {get; set;}
		// 住所
		public String address {get; set;}
		// 募集人名
		public String agentName {get; set;}
		// 募集人名カナ
		public String agentNameKana {get; set;}
		// 募集人コード
		public String agentCode {get; set;}
		// リンクURL
		public String url {get; set;}

		public I_AgencySearchBoxRow(){
			agencyName = '';
			agencyNameKana = '';
			agencyCode = '';
			officeName = '';
			officeNameKana = '';
			officeCode = '';
			address = '';
			agentName = '';
			agentNameKana = '';
			agentCode = '';
			url = '';
		}

		// ソート
		public Integer compareTo(Object compareTo){
			I_agencySearchBoxRow compareToRow = (I_agencySearchBoxRow)compareTo;
			Integer result = 0;

			// 代理店名（カナ）
			if(list_sortType == SORT_TYPE_AGENCY_NAME){
				result = I_Util.compareToString(agencyNameKana, compareToRow.agencyNameKana);
			// 代理店コード
			} else if(list_sortType == SORT_TYPE_AGENCY_CODE){
				result = I_Util.compareToString(agencyCode, compareToRow.agencyCode);
			// 事務所名（カナ）
			} else if(list_sortType == SORT_TYPE_OFFICE_NAME){
				result = I_Util.compareToString(officeNameKana, compareToRow.officeNameKana);
			// 事務所コード
			} else if(list_sortType == SORT_TYPE_OFFICE_CODE){
				result = I_Util.compareToString(officeCode, compareToRow.officeCode);
			// 住所
			} else if(list_sortType == SORT_TYPE_ADDRESS){
				result = I_Util.compareToString(address, compareToRow.address);
			// 募集人名（カナ）
			} else if(list_sortType == SORT_TYPE_AGENT_NAME){
				result = I_Util.compareToString(agentNameKana, compareToRow.agentNameKana);
			// 募集人コード
			} else if(list_sortType == SORT_TYPE_AGENT_CODE){
				result = I_Util.compareToString(agentCode, compareToRow.agentCode);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	//JIRA_#63 170331
	public void getSeachBoxLog(){
		getSeachBoxLogRecord(searchTypeName);
		return;
	}
}