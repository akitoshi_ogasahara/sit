@isTest
public class TestDS_TestUtil {

	// 医的な引受の目安のテストデータ作成----------------------------------------------------------------

	// 分類の値
	private static final List<String> CATEGORY_LIST = new List<String>{'外傷', '感覚器', '感染症', '血液', '膠原病類縁疾患', '呼吸器', '循環器', '消化器', '新生物', '性・排尿物', '精神神経系', '代謝', '内分泌', '妊娠及び分娩', '皮膚', '骨・運動器', '健診指摘', 'その他'};


	// 傷病名となる文字列作成用

	// 傷病名List
	private static List<String> nameList;
	//　傷病名かなList
	private static List<String> hiraganaList;
	//　傷病名カナList
	private static List<String> katakanaList;

	// ア段のList
	private static final List<String> A_COLUM_KATAKANA_LIST = new List<String>{'ア', 'カ', 'サ', 'タ', 'ナ', 'ハ', 'マ', 'ヤ', 'ラ', 'ワ'};
	private static final List<String> A_COLUM_HIRAGANA_LIST = new List<String>{'あ', 'か', 'さ', 'た', 'な', 'は', 'ま', 'や', 'ら', 'わ'};
	// ア行のList
	private static final List<String> A_ROW_KATAKANA_LIST = new List<String>{'ア', 'イ', 'ウ', 'エ', 'オ'};
	private static final List<String> A_ROW_HIRAGANA_LIST = new List<String>{'あ', 'い', 'う', 'え', 'お'};

	// 検索画面のテストケースに合わせて作成

	// アルファベット
	private static final List<String> ALPHABET_LIST = new List<String>{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
	private static final List<String> ALPHABET_KATAKANA_LIST = new List<String>{'エー', 'ビー', 'シー', 'ディー', 'イー', 'エフ', 'ジー', 'エイチ', 'アイ', 'ジェー'};
	private static final List<String> ALPHABET_HIRAGANA_LIST = new List<String>{'えー', 'びー', 'しー', 'でぃー', 'いー', 'えふ', 'じー', 'えいち', 'あい', 'じぇー'};

	// 一覧画面のテストケースに合わせて作成

	// 値のないデータ作成用
	private static final List<String> BLANK_LIST = new List<String>{'', 'ア', 'カ', 'サ', 'タ', 'ナ', 'ハ', 'マ', 'ヤ', 'ラ'};
	// 異常系データ作成用
	private static final List<String> UNEXPECT_LIST = new List<String>{'＊', 'ア', 'カ', 'サ', 'タ', 'ナ', 'ハ', 'マ', 'ヤ', 'ラ'};



	// 保険種類名選択リスト値
	private static final List<String> INSURANCETYPENAME_LIST = new List<String>{'普通死亡', '生活障害定期保険', '重大疾病保障保険', 'ガン保険', '長期傷害保険'};

	/*
	 * 保険種類引受条件情報のデータ各種パターン作成用Map
	 * 普通死亡　　　　　　 | 状況あり、但し書きあり
	 * 生活障害定期保険 | 状況あり、但し書きなし
	 * 　　　　　　　　　　　　 | 状況なし、但し書きなし
	 * 重大疾病保障保険 | 状況あり、但し書きあり
	 * 　　　　　　　　　　　　 | 状況なし、但し書きあり
	 * ガン保険　　　　    | 状況あり、但し書きなし
	 * 長期傷害保険　　　 | 状況なし、但し書きあり
	 * 　　　　　　　　　　　　 | 状況あり、但し書きなし
	 */
	private static final Map<String, List<Boolean>> INSTYPE_MAP = new Map<String, List<Boolean>>{
																'普通死亡' => new List<Boolean>{true, true}
																, '生活障害定期保険' => new List<Boolean>{true, false, false, false}
																, '重大疾病保障保険' => new List<Boolean>{true, false, true, true}
																, 'ガン保険' => new List<Boolean>{false, true}
																, '長期傷害保険' => new List<Boolean>{false, true, true, false}
																};


	/*
	 * 傷病Noマスタのデータ作成
	 * @diseaseNumberCount ： 作成する傷病のNoの数
	 */
	public static List<E_DiseaseNumber__c> createDiseaseNumberData(Integer diseaseNumberCount){
		Map<String, Integer> categoryNumberMap = new Map<String, Integer>();
		Integer categoryCount = 1;
		for(String category : CATEGORY_LIST){
			categoryNumberMap.put(category, categoryCount);
			categoryCount++;
		}
		List<E_DiseaseNumber__c> diseaseNumberList = new List<E_DiseaseNumber__c>();
		Integer count = 1;
		for(String category : CATEGORY_LIST){
			for(Integer i = 0; i < categoryNumberMap.get(category); i++){
				E_DiseaseNumber__c dNo = createDiseaseNumberDetail(String.valueOf(count), category);
				diseaseNumberList.add(dNo);
				if(count == diseaseNumberCount) {
					break;
				}
				count++;
			}
			if(count == diseaseNumberCount) {
				break;
			}
		}
		insert diseaseNumberList;
		return diseaseNumberList;
	}

	/*
	 * 傷病Noマスタの項目値設定
	 * @diseaseNo ： 傷病のNo
	 * @category ： 傷病のNoの分類
	 */
	private static E_DiseaseNumber__c createDiseaseNumberDetail(String diseaseNo, String category){
		E_DiseaseNumber__c diseaseNumber = new E_DiseaseNumber__c(
			//分類検索と一覧画面への適用を考慮して自由に値を設定可能にする
			Name = diseaseNo
			, Category__c = category
			//実データは、値の有/無による表示/非表示があるが、一律で値を設定する
			, Tips__c = '告知のポイント' + diseaseNo
			, Symptoms__c = '障がいの部位' + diseaseNo
			, Period__c = '入院期間' + diseaseNo
			, Extent__c = '障がいの程度' + diseaseNo
			, Situation__c = '現在の状況' + diseaseNo
			, Notice__c = '必要な告知' + diseaseNo
			, NoticeSumisei__c = '必要な告知(住生)' + diseaseNo
			, Document__c = '医的資料' + diseaseNo
			, DocumentSumisei__c = '医的資料(住生)' + diseaseNo
			, Information__c = '参考情報' + diseaseNo
			, InformationSumisei__c = '参考情報(住生)' + diseaseNo
		);
		return diseaseNumber;
	}


	/*
	 * 傷病マスタのデータ作成
	 * @diseaseNumberList ： 紐づける傷病のNoのList
	 * @testType ： テストの種類
	 */
	public static List<E_Diseases__c> createDiseaseData(List<E_DiseaseNumber__c> diseaseNumberList, String testType){
		nameList = new List<String>();
		hiraganaList = new List<String>();
		katakanaList = new List<String>();

		//傷病Noに対して作成予定の傷病数を算出（1~nまでの和）
		Integer numberSize = diseaseNumberList.size();
		Integer diseaseCount = numberSize * (numberSize + 1) / 2;

		if(testType == 'normal') {
			createNameList(diseaseCount, nameList, A_COLUM_KATAKANA_LIST, A_ROW_HIRAGANA_LIST, 'の傷病');
			createNameList(diseaseCount, katakanaList, A_COLUM_KATAKANA_LIST, A_ROW_KATAKANA_LIST, 'ノショウビョウ');
			createNameList(diseaseCount, hiraganaList, A_COLUM_HIRAGANA_LIST, A_ROW_HIRAGANA_LIST, 'のしょうびょう');
		} else if(testType == 'alphabet') {
			//検索画面のテストケースに合わせて作成
			createNameList(diseaseCount, nameList, ALPHABET_LIST, A_ROW_HIRAGANA_LIST, 'の傷病');
			createNameList(diseaseCount, katakanaList, ALPHABET_KATAKANA_LIST, A_ROW_KATAKANA_LIST, 'ノショウビョウ');
			createNameList(diseaseCount, hiraganaList, ALPHABET_HIRAGANA_LIST, A_ROW_HIRAGANA_LIST, 'のしょうびょう');
		} else if(testType == 'half') {
			//一覧画面のテストケースに合わせて作成
			createNameList(diseaseCount, nameList, A_COLUM_KATAKANA_LIST, A_ROW_HIRAGANA_LIST, 'の傷病');
			createNameList(diseaseCount, katakanaList, createHalfChar(A_COLUM_KATAKANA_LIST), createHalfChar(A_ROW_KATAKANA_LIST), 'ﾉｼｮｳﾋﾞｮｳ');
			createNameList(diseaseCount, hiraganaList, A_COLUM_HIRAGANA_LIST, A_ROW_HIRAGANA_LIST, 'のしょうびょう');
		} else if(testType == 'blank') {
			//一覧画面のテストケースに合わせて作成
			createNameList(diseaseCount, nameList, A_COLUM_KATAKANA_LIST, A_ROW_HIRAGANA_LIST, 'の傷病');
			createNameList(diseaseCount, katakanaList, BLANK_LIST, BLANK_LIST, '');
			createNameList(diseaseCount, hiraganaList, A_COLUM_HIRAGANA_LIST, A_ROW_HIRAGANA_LIST, 'のしょうびょう');
		} else if(testType == 'unExpect') {
			//一覧画面のテストケースに合わせて作成
			createNameList(diseaseCount, nameList, A_COLUM_KATAKANA_LIST, A_ROW_HIRAGANA_LIST, 'の傷病');
			createNameList(diseaseCount, katakanaList, UNEXPECT_LIST, UNEXPECT_LIST, 'ノショウビョウ');
			createNameList(diseaseCount, hiraganaList, A_COLUM_HIRAGANA_LIST, A_ROW_HIRAGANA_LIST, 'のしょうびょう');
		}

		List<E_Diseases__c> diseaseList = new List<E_Diseases__c>();
		Integer i = 0;
		for(E_DiseaseNumber__c diseaseNumber : diseaseNumberList){
			for(Integer j = 0; j < Integer.valueOf(diseaseNumber.Name); j++){
				diseaseList.add(createDiseaseDetail(diseaseNumber.Id, nameList[i], hiraganaList[i], katakanaList[i]));
				i++;
			}
		}
		insert diseaseList;
		return diseaseList;
	}

	/*
	 * 傷病マスタの項目値設定
	 * @diseaseNumberId ： 紐づける傷病NoのId
	 * @name ：
	 */
	private static E_Diseases__c createDiseaseDetail(Id diseaseNumberId, String name, String hiragana, String katakana){
		E_Diseases__c disease = new E_Diseases__c(
			DiseaseNumber__c = diseaseNumberId,
			Name = name
			, Hiragana__c = hiragana
			, Katakana__c = katakana
		);
		return disease;
	}

	/*
	 * 保険種類引受条件のデータ作成
	 * @diseaseNumberList ： 紐づける傷病のNoのList
	 */
	public static List<E_InsType_UnderWriting__c> createUnderWritingData(List<E_DiseaseNumber__c> diseaseNumberList){
		List<E_InsType_UnderWriting__c> underWritingList = new List<E_InsType_UnderWriting__c>();
		//傷病No
		for(E_DiseaseNumber__c diseaseNumber : diseaseNumberList){
			for(String insType: INSURANCETYPENAME_LIST){
				List<Boolean> decisions = INSTYPE_MAP.get(insType);
				underWritingList.add(createUnderWritingDetail(diseaseNumber.Id, insType, 1, decisions[0], decisions[1]));
				if(decisions.size() == 4){
					underWritingList.add(createUnderWritingDetail(diseaseNumber.Id, insType, 2, decisions[2], decisions[3]));
				}
			}
		}
		insert underWritingList;
		return underWritingList;
	}

	/*
	 * 保険種類引受条件の項目値設定
	 * @diseaseNumberId ： 紐づける傷病NoのId
	 * @insType ： 保険種類名
	 * @insTypeOrder ： 状況表示順
	 * @hasStatus ： 状況の値の有/無
	 * @hasProviso ： 但し書きの値の有/無
	 */
	private static E_InsType_UnderWriting__c createUnderWritingDetail(Id diseaseNumberId, String insType, Integer insTypeOrder, Boolean hasStatus, Boolean hasProviso){
		E_InsType_UnderWriting__c underWriting = new E_InsType_UnderWriting__c(
			DiseaseNumber__c = diseaseNumberId
			, InsuranceType__c = insType
			, TreatmentStatusOrder__c = insTypeOrder
			//保険種類名を含む場合があり、住生の場合出し分けが必要
			, Judgement__c = insType + 'の判定'
		);
		if(hasStatus){
			underWriting.TreatmentStatus__c = '状況';
		}
		if(hasProviso){
			underWriting.Proviso__c = '但し書き';
		}
		return underWriting;
	}

	/*
	 * 傷病名の文字列List作成
	 * @diseaseCount ： 作成したい傷病数
	 * @columList ： ア段のList
	 * @rowList ： ア行のList
	 * @charStr ： 固定文字（’の傷病’など）
	 */
	private static void createNameList(Integer diseaseCount, List<String> nameCharList, List<String> columList, List<String> rowList, String charStr){
		Integer count = 0;
		for(String columChar : columList) {
			for(String rowChar : rowList) {
				nameCharList.add(columChar + rowChar + charStr);
				count++;
				if(count == diseaseCount) {
					break;
				}
			}
			if(count == diseaseCount) {
				break;
			}
		}
	}

	/*
	 * 半角カナのデータ作成用
	 * @emList ： 半角化したい全角カナのList
	 */
	private static List<String> createHalfChar(List<String> emList){
		List<String> halfList = new List<String>();
		for(String emChar: emList){
			halfList.add(E_Util.emKatakanaToEn(emChar));
		}
		return halfList;
	}

	//-------------------------------------------------------------------------------------------

	//住生ユーザデータ作成
	public static User createSumiseiUser(Boolean isInsert){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		Profile pro = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY);
		system.runAs(thisUser){
			Account parentAccount = new Account(
				  Name = 'parentAcc'
				 ,E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI
			);
			insert parentAccount;

			Account acc = new Account(
				  ParentId = parentAccount.Id
				  ,Name = 'TestAcc'
				 ,E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI
			);
			insert acc;

			Contact con = new Contact(
				  FirstName = '花子'
				 ,LastName = 'テストテスト'
				 ,E_CL3PF_AGNTNUM__c = 'test01'
				 ,E_CL3PF_KMOFCODE__c = 'test01'
				 ,E_CLTPF_DOB__c = '20161017'
				 ,E_CL3PF_VALIDFLAG__c = '1'
				 ,AccountId = acc.Id
			);
			insert con;

			us = new User(
				  Username = 'ts.nn.iris.test01'+ System.Label.E_USERNAME_SUFFIX
				 ,Alias = 'テストユーザー'
				 ,Email = 'test01@test.com'
				 ,EmailEncodingKey = 'UTF-8'
				 ,LanguageLocaleKey = 'ja'
				 ,LastName = 'テスト'
				 ,LocaleSidKey = 'ja_JP'
				 ,ProfileId = pro.Id
				 ,ContactId = con.Id
				 ,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
			);

			if(isInsert){
				insert us;
				TestI_TestUtil.createAccountShare(acc.Id, us.Id);
				//代理店権限を付与
				TestI_TestUtil.createBasePermissions(us.Id);
				E_IDCPFTriggerHandler.isSkipTriggerActions = true;
				E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, us.Id, 'AY');
			}
		}
		return us;
	}
	//住生ユーザデータ作成
	public static User createSumiseiIRISUser(Boolean isInsert){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		Profile pro = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS);
		system.runAs(thisUser){
			Account parentAccount = new Account(
				  Name = 'parentAcc'
				 ,E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI
			);
			insert parentAccount;

			Account acc = new Account(
				  ParentId = parentAccount.Id
				  ,Name = 'TestAcc'
				 ,E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI
			);
			insert acc;

			Contact con = new Contact(
				  FirstName = '花子'
				 ,LastName = 'テストテスト'
				 ,E_CL3PF_AGNTNUM__c = 'test01'
				 ,E_CL3PF_KMOFCODE__c = 'test01'
				 ,E_CLTPF_DOB__c = '20161017'
				 ,E_CL3PF_VALIDFLAG__c = '1'
				 ,AccountId = acc.Id
				 ,E_CL3PF_AGTYPE__c = '02'
			);
			insert con;

			us = new User(
				  Username = 'ts.nn.iris.test01'+ System.Label.E_USERNAME_SUFFIX
				 ,Alias = 'テストユーザー'
				 ,Email = 'test01@test.com'
				 ,EmailEncodingKey = 'UTF-8'
				 ,LanguageLocaleKey = 'ja'
				 ,LastName = 'テスト'
				 ,LocaleSidKey = 'ja_JP'
				 ,ProfileId = pro.Id
				 ,ContactId = con.Id
				 ,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
			);

			if(isInsert){
				insert us;
				TestI_TestUtil.createAccountShare(acc.Id, us.Id);
				//代理店権限を付与
				TestI_TestUtil.createBasePermissions(us.Id);
				E_IDCPFTriggerHandler.isSkipTriggerActions = true;
				E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, us.Id, 'AY');
			}
		}
		return us;
	}
}