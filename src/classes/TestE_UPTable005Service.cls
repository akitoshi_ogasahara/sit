@istest
public class TestE_UPTable005Service {
	private static E_UPFPF__c upfpf;
    private static E_UPBPF__c eupbpf;
    private static LIST<E_UPFPF__c> upfpfList;        
    @testSetup
    private static void setup() {
        // Create common test Message
        Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get('MSG_DISCLAIMER');
        final String TYPE_MESSAGE = 'メッセージ';
        final String MSG_NAME = 'name';
        List<E_MessageMaster__c> src = new List<E_MessageMaster__c>{
              new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|006', Value__c='開始日には、{0}（特別勘定の設定開始日）以前の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|007', Value__c='終了日には、{0} 以降の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|008', Value__c='20年を超えて期間を選択することはできません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|009', Value__c='終了日には、{0} 以降の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|010', Value__c='終了日には、{0} 以前の日付は選択できません。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|011', Value__c='[{0}]は必須です。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|012', Value__c='[{0}]はyyyy/MM/dd 形式で入力して下さい。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|014', Value__c='選択期間を選択してください。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UPS|018', Value__c='一定時間操作がありませんでした。「ユニットプライス/騰落率トップ画面」を閉じて再度アクセスして下さい。')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|002', Value__c='ERR|002')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='UP5|E01', Value__c='UP5|E01')
        };
        insert src;
    }
    private static Map<String, Map<String, Id>> rMap;
    private static Map<String, Map<String, Id>> getRecTypeIdMap(){
        if(rMap != null) return rMap;
        rMap = new Map<String, Map<String, Id>>();
        for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
            if(rMap.containsKey(rt.sObjectType)){
                rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
            }else{
                rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
            }
        }
        return rMap;
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    public static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
    
    /**
     * 期間自由選択,特別勘定ID:無し
     */
    private static testMethod void testInit005() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        integer pageNo;
        //テストユーザで機能実行開始
        System.runAs(u){
            PageReference pref = Page.up005;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);

            createData();
            // ユニットプライスの登録
            LIST<E_UPXPF__c> upxpfList = new LIST<E_UPXPF__c>();
            DateTime fundDate = system.Datetime.newInstance(2014, 4, 4);
            integer size = upfpfList.size();
            for(integer i=0;i < 1000;i++){
                
                E_UPXPF__c upxpf = new E_UPXPF__c(
                    E_UPFPF__c = upfpfList[system.math.mod(i, size)].Id,
                    ZFUNDDTE__c = fundDate.addDays(i).format('yyyyMMdd'),
                    dataSyncDate__c = Date.valueOf('2014-04-02'),
                    ZVINDEX__c = 10.0989 + i/10             
                );
                upxpfList.add(upxpf);
            }
            insert upxpfList;
            
            

            // ユニットプライス基本情報ID
            ApexPages.currentPage().getParameters().put('id', eupbpf.Id);
            // 特別勘定ID
            ApexPages.currentPage().getParameters().put('spid', null);
            E_UPTerm003Controller controller = new E_UPTerm003Controller();
            // ラジオボタンの初期値確認
            system.assertEquals(controller.getRADIO_VAL_FREE(),controller.selectType);
            controller.init();
            // 期間選択ラジオボタン
            controller.selectType = '0';
            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.freeSelectStartDate = '2013/01/01';
            controller.freeSelectEndDate = '2023/12/31';
            controller.changeSelectType();
            controller.getTermItems();
            controller.getRADIO_VAL_FREE();
            controller.getRADIO_VAL_EASY();
            controller.doCheckboxAllNotChecked();
            controller.doCheckboxAllChecked();
            for(E_UPTerm003Controller.EUPFPFSelectOption selectOption : controller.fundSelectOptions){
                selectOption.getLabel();
                selectOption.getStartyyyyMMdd();
            }

            //ApexPages.currentPage().getParameters().put('selectRadio', controller.getRADIO_VAL_FREE());
            controller.doTransition004();
            controller.doTransition005();
            controller.doTransition006();
            controller.validate();
            E_UPTable005Extension Extension = new E_UPTable005Extension(controller);
            resultPage = Extension.init005();
            E_UPTable005Service service = new E_UPTable005Service(
                E_UPFPFDao.getIndexesBySvcpfIds(controller.dlCondi.ZSFUNDCD
                                                , controller.dlCondi.fromTerm
                                                , controller.dlCondi.toTerm)
                ,controller.dlCondi.NAMEKBN
            );
             List<E_UPTable005Service.UPTablePage>  UPTablePageList = service.getUPTablegetUPTablePagesPages();
            UPTablePageList[0].compareto(UPTablePageList[0]);
            UPTablePageList[0].getIndexTableHTML(controller.dlCondi.fromTerm,controller.dlCondi.toTerm);
            system.assertEquals(null,service.getIndexTableHTML(3, controller.dlCondi.fromTerm,controller.dlCondi.toTerm));
            //テスト終了
            Test.stopTest();
            
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }

    private static void createData(){
        E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
        log.DataSyncStartDate__c = Date.valueOf('2014-10-01');
        log.DataSyncEndDate__c = Date.valueOf('2014-10-01');
        log.InquiryDate__c = Date.valueOf('2014-12-31');
        insert log;
        // ############### テストデータの登録 ###############
        // 特別勘定の登録
        
        LIST<E_SVCPF__c> svcpfList = new LIST<E_SVCPF__c>{
            new E_SVCPF__c(),new E_SVCPF__c(),new E_SVCPF__c(),new E_SVCPF__c(),new E_SVCPF__c()              
        };
        insert svcpfList;
        // ファンド群マスタの登録
        E_FundMaster__c fundMaster = new E_FundMaster__c();
        insert fundMaster;
        // 選択可能ファンドの登録
        LIST<E_SFGPF__c> sfgpfList = new LIST<E_SFGPF__c>();        
        E_SFGPF__c sfgpf;
        for(E_SVCPF__c item : svcpfList){
            sfgpf = new E_SFGPF__c(
                E_SVCPF__c = item.Id
                ,FundMaster__c = fundMaster.Id
                
            );
            sfgpfList.add(sfgpf);
        }
        insert sfgpfList;
        // ユニットプライス基本情報の作成
        eupbpf = new E_UPBPF__c();
        eupbpf.ZPRDCD__c = 'V9999';
        eupbpf.ZDSPFLAG__c = '1';
        eupbpf.E_FundMaster__c = fundMaster.Id;
        insert eupbpf;
        // ユニットプライス表示用ファンドの登録
        upfpfList = new LIST<E_UPFPF__c>();        
        for(E_SVCPF__c item : svcpfList){
            upfpf = new E_UPFPF__c();
            upfpf.SVCPF__c = item.Id; 
            upfpfList.add(upfpf);
        }
        insert upfpfList;
        
    }   
}