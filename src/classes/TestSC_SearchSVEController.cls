@isTest
private with sharing class TestSC_SearchSVEController{
		private static testMethod void testPageMethods() {	
			SC_SearchSVEController page = new SC_SearchSVEController(new ApexPages.StandardController(new SC_Office__c()));	
			page.getOperatorOptions_SC_Office_c_FiscalYear_c_multi();	
			page.getOperatorOptions_SC_Office_c_LimitFixed_c();	
			page.getOperatorOptions_SC_Office_c_IsActiveOffice_c();	
			page.getOperatorOptions_SC_Office_c_Status_c();	
			page.getOperatorOptions_SC_Office_c_OwnerMRUnitName_c();	
			page.getOperatorOptions_SC_Office_c_IsHeadSection_c();	
			page.getOperatorOptions_SC_Office_c_OwnerMRName_c();	
			page.getOperatorOptions_SC_Office_c_OwnerMRCode_c();	
			page.getOperatorOptions_SC_Office_c_HasAgManagerMail_c();	
			page.getOperatorOptions_SC_Office_c_ZEAYNAM_c();	
			page.getOperatorOptions_SC_Office_c_ZAGCYNUM_c();	
			page.getOperatorOptions_SC_Office_c_SCPlan_c_multi();	
			page.getOperatorOptions_SC_Office_c_Defect_c_multi();	
			System.assert(true);
		}	
			
	private static testMethod void testresultTable() {
		SC_SearchSVEController.resultTable resultTable = new SC_SearchSVEController.resultTable(new List<SC_Office__c>(), new List<SC_SearchSVEController.resultTableItem>(), new List<SC_Office__c>(), null);
		resultTable.create(new SC_Office__c());
		resultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
}