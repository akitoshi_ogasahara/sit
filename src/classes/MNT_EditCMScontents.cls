global with sharing class MNT_EditCMScontents extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public MNT_EditCMScontentsExtender getExtender() {return (MNT_EditCMScontentsExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public MNT_EditCMScontents(ApexPages.StandardController controller) {
		super(controller);

		registTransitionControl='/apex/MNT_ViewCMScontents?id={ID}';
		editTransitionControl='/apex/MNT_ViewCMScontents?id={ID}';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.HiddenContentSubject__c;
		f = I_ContentMaster__c.fields.DisplayOrder__c;
		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.DOM_Id__c;
		f = I_ContentMaster__c.fields.DocumentNo__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.ForCustomers__c;
		f = I_ContentMaster__c.fields.isRecommend__c;
		f = I_ContentMaster__c.fields.CompanyLimited__c;
		f = I_ContentMaster__c.fields.ForAgency__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.NewValidTo__c;
		f = I_ContentMaster__c.fields.createIndex__c;
		f = I_ContentMaster__c.fields.Style__c;
		f = I_ContentMaster__c.fields.SelectFrom__c;
		f = I_ContentMaster__c.fields.SelectLimit__c;
		f = I_ContentMaster__c.fields.SelectCondition__c;
		f = I_ContentMaster__c.fields.Content__c;
		f = I_ContentMaster__c.fields.ContentEmployee__c;
		f = I_ContentMaster__c.fields.isHTML__c;
		f = I_ContentMaster__c.fields.CanDisplayVia__c;
		f = I_ContentMaster__c.fields.LinkTitle__c;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.LabelforDate__c;
		f = I_ContentMaster__c.fields.DateStr__c;
		f = I_ContentMaster__c.fields.InfoLabel__c;
		f = I_ContentMaster__c.fields.inquirySubject__c;
		f = I_ContentMaster__c.fields.inquiryTo__c;
		f = I_ContentMaster__c.fields.Category__c;
		f = I_ContentMaster__c.fields.Management__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.filePath__c;
		f = I_ContentMaster__c.fields.LinkURL__c;
		f = I_ContentMaster__c.fields.WindowTarget__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addField('Name');
			mainQuery.addField('HiddenContentSubject__c');
			mainQuery.addField('DisplayOrder__c');
			mainQuery.addField('ParentContent__c');
			mainQuery.addField('DOM_Id__c');
			mainQuery.addField('DocumentNo__c');
			mainQuery.addField('ApprovalNo__c');
			mainQuery.addField('ForCustomers__c');
			mainQuery.addField('isRecommend__c');
			mainQuery.addField('CompanyLimited__c');
			mainQuery.addField('ForAgency__c');
			mainQuery.addField('DisplayFrom__c');
			mainQuery.addField('ValidTo__c');
			mainQuery.addField('NewValidTo__c');
			mainQuery.addField('createIndex__c');
			mainQuery.addField('Style__c');
			mainQuery.addField('SelectFrom__c');
			mainQuery.addField('SelectLimit__c');
			mainQuery.addField('SelectCondition__c');
			mainQuery.addField('Content__c');
			mainQuery.addField('ContentEmployee__c');
			mainQuery.addField('isHTML__c');
			mainQuery.addField('CanDisplayVia__c');
			mainQuery.addField('LinkTitle__c');
			mainQuery.addField('FormNo__c');
			mainQuery.addField('LabelforDate__c');
			mainQuery.addField('DateStr__c');
			mainQuery.addField('InfoLabel__c');
			mainQuery.addField('inquirySubject__c');
			mainQuery.addField('inquiryTo__c');
			mainQuery.addField('Category__c');
			mainQuery.addField('Management__c');
			mainQuery.addField('ClickAction__c');
			mainQuery.addField('filePath__c');
			mainQuery.addField('LinkURL__c');
			mainQuery.addField('WindowTarget__c');
			mainQuery.addFieldAsOutput('ParentContent__c');
			mainQuery.addFieldAsOutput('UpsertKey__c');
			mainQuery.addFieldAsOutput('ParentContent__r.Name');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = true;
			p_sidebar = true;
			sve_ClassName = 'MNT_EditCMScontents';
			addInheritParameter('ParentContent__c', 'ParentContent__c');
			extender = new MNT_EditCMScontentsExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}