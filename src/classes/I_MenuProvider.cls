public with sharing class I_MenuProvider {

	private E_CookieHandler.CookieItem activeMenuCookieItem;

	private static I_MenuProvider menuProvider = null;
	public static I_MenuProvider getInstance(){
		if(menuProvider == null){
			menuProvider = new I_MenuProvider();
		}
		return menuProvider;
	}

	public I_MenuMaster__c activeMenu{get; private set;}
	public Map<String, Map<String, List<I_MenuMaster__c>>> menusByCategory{get;set;}
	
	//Topページのメニューでメニューキーを指定すれば、メニューマスタから遷移先を返しています。
	public Map<String, I_MenuItem> menuItemsByKey{get;set;}
	public Map<Id, I_MenuItem> menusById {get; set;}
	private I_MenuProvider(){
		this.activeMenuCookieItem  = new E_CookieHandler.CookieItem(I_Const.COOKIE_ACTIVE_MENU);
		
		//メインカテゴリは選択リスト値から取得する。
		List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
		List<String> categorys = new List<String>();
		for (Schema.PicklistEntry entry: entries) {
			if (entry.isActive()) {
				categorys.add(entry.getValue());
			}
		}
		categorys.sort();
		
		//カテゴリのソート結果でMap順を確定
		menusByCategory = new Map<String, Map<String,List<I_MenuMaster__c>>>();
		for(String cate:categorys){
			menusByCategory.put(cate, new Map<String,List<I_MenuMaster__c>>());
		}
		
		menuItemsByKey = new Map<String, I_MenuItem>();
		menusById = new Map<Id, I_MenuItem>();

		//メニューマスタをカテゴリ別に格納&menuItemのMAPを生成
		for(I_MenuMaster__c menu:I_MenuMasterDao.getSortedRecords()){
			I_menuItem mnitem = new I_menuItem(menu);
			menuItemsByKey.put(menu.MenuKey__c, mnitem);		//TODO ﾘﾌｧｸﾀ　メニューキーが重複するパターンがある　→　IRIS_TopのCMS化の際に実装方式の見直し
			menusById.put(menu.Id, mnitem);
			//メニューを追加
			if(mnItem.rendered){
				String subCate = E_Util.null2Blank(menu.SubCategory__c);
				if(menusByCategory.get(menu.MainCategory__c).containsKey(subCate)==false){
					menusByCategory.get(menu.MainCategory__c).put(subCate, new List<I_MenuMaster__c>());
				}
				menusByCategory.get(menu.MainCategory__c).get(subCate).add(menu);
			}
		}
	}
	
	//現在選択されているメニュー	Idを設定
	public void setAcitveMenuId(Id mnId){
		if(activeMenu==null||activeMenu.Id!=mnId){
			this.activeMenu = I_MenuMasterDao.getRecordById(mnId);
			activeMenuCookieItem.setCookie(mnId);
		}
	}

	//現在選択されているメニュー	メニューキーを設定	E_系ページからはこちらが設定される	
	public void setAcitveMenuKey(String mnKey){
		//URLにてメニューIdが指定されている場合はそちらを優先
		String urlMenuId = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_MENUID); 
		if(String.isNotBlank(urlMenuId)&&(urlMenuId instanceof Id)){
			setAcitveMenuId(Id.valueOf(urlMenuId));
			return; 
		}

		//Cookieに設定されている値を取得
		String cookieMenuId = activeMenuCookieItem.getCookieValue();
		
		//メニューキーが指定されている場合
		if(String.isNotBlank(mnKey)){
			List<I_MenuMaster__c> menus = I_MenuMasterDao.getRecordsByLinkMenuKey(mnKey);
			for(I_MenuMaster__c mn:menus){
				if(mn.Id == cookieMenuId){
					this.activeMenu = mn; 
					return;			//Cookieの設定値は変更不要
				}
			}
			//Cookieと一致するものがなければ並び順先頭のメニューを返す
			if(menus.size()>0){
				this.activeMenu = menus[0];		//TODO リスト取得できていない場合
				activeMenuCookieItem.setCookie(this.activeMenu.Id);
				return;
			}	
		}
		
		//メニューキーが指定されていない場合はcategoryの設定を確認
		String urlCate = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_CATEGORY);
		if(this.activeMenu==null&&String.isNotBlank(urlCate)){
			String mainCateName = I_Const.CATEGORY_ALIAS_NAME.get(urlCate);
			I_MenuMaster__c cateMenu = extractDefaultMenuRecord(mainCateName, null);
			if(cateMenu!=null){
				setAcitveMenuId(cateMenu.Id);
				return;
			}
		}
		
		//メニューキーによるAciveMenuが決まらない場合Cookieの値から設定
		if(String.isNotBlank(cookieMenuId)&&(cookieMenuId instanceof Id)){
			setAcitveMenuId(Id.valueOf(cookieMenuId));
			return;
		}

		//URLのmenuId　＞　メニューキー　＞　Cookieから取得ができない場合は、Activeが設定されない。
	}

	//現在選択されているメニューをクリア
	public void clearAcitveMenu(){
		this.activeMenu = null;
		activeMenuCookieItem.deleteCookie();
	}

	//現在選択されているカテゴリのメニューリスト		
	public List<I_MenuMaster__c> getActiveCategoryMenus(){
		if(activeMenu!=null){
			//return menusByCategory.get(activeMenu.MainCategory__c);
			String subCate = E_Util.null2Blank(activeMenu.SubCategory__c);
			return menusByCategory.get(activeMenu.MainCategory__c).get(subCate);
		}
		//return 	new List<I_MenuMaster__c>();
		return null;
	}
	
	//メインカテゴリとサブカテゴリを指定してメニューリストを取得
	public List<I_MenuMaster__c> getCategoryMenus(String mainCate, String subCate){
		if(menusByCategory.containsKey(mainCate) == false){
			return null;
		}
		
		return menusByCategory.get(mainCate).get(E_Util.null2Blank(subCate));
	}

	//カテゴリ内のメニューレコードリストからデフォルトメニューを返す
	public I_MenuMaster__c extractDefaultMenuRecord(String mainCate, String subCate){
		return I_MenuProvider.extractDefaultMenuRecord(getCategoryMenus(mainCate,subCate));
	}

	//	staticメソッド
	public static I_MenuMaster__c extractDefaultMenuRecord(List<I_MenuMaster__c> menus){
		if(menus==null){
			return null;
		}
		
		for(I_MenuMaster__c mn:menus){
			if(mn.Default__c){
				return mn;
			}
		}
		//デフォルトフラグがない場合は1件目のメニューをセット
		if(menus!=null&&menus.size()>0){
			return menus[0];
		}
		return null;
	}
	
	
	/**	=====================================
	 *		ライブラリ　サブナビ　メニュー
	 */
	public class LibSubNavItemsHolder{
		//Constractor
		public LibSubNavItemsHolder(){	}

		//NNTube Top
		public String getUrlForNNTube(){
			return tubeConnectHelper.getTubeTopUrl() + '?' + tubeConnectHelper.getUrlParamsForNNTube(); 
		}

		//NNTube お気に入り
		public String getUrlForNNTubeFavorite(){
			return tubeConnectHelper.getTubeFavoriteUrl();
		}

		//NNTube 閲覧履歴
		public String getUrlForNNTubeHistory(){
			return tubeConnectHelper.getTubeHistoryUrl();
		}
		
		//ライブラリ　保全手続資料のURL
		public I_GlobalNavController.gNavItem getLibInfoNavItem(){
			I_GlobalNavController.gNavItem item = new I_GlobalNavController.gNavItem();
			E_IRISHandler iris = E_IRISHandler.getInstance();
			item.record = iris.irisMenus.extractDefaultMenuRecord(I_Const.MENU_MAIN_CATEGORY_LIBLARY
																			, I_Const.MENU_SUB_CATEGORY_LIBLARY_INFO);
			return item;
		}

		//ライブラリ　マニュアルのURL
		public I_GlobalNavController.gNavItem getLibManualNavItem(){
			I_GlobalNavController.gNavItem item = new I_GlobalNavController.gNavItem();
			E_IRISHandler iris = E_IRISHandler.getInstance();
			item.record = iris.irisMenus.extractDefaultMenuRecord(I_Const.MENU_MAIN_CATEGORY_LIBLARY
																			, I_Const.MENU_SUB_CATEGORY_LIBLARY_MANUAL);
			return item;
		}

		//ライブラリ　その他のURL
		public I_GlobalNavController.gNavItem getLibOtherNavItem(){
			I_GlobalNavController.gNavItem item = new I_GlobalNavController.gNavItem();
			E_IRISHandler iris = E_IRISHandler.getInstance();
			item.record = iris.irisMenus.extractDefaultMenuRecord(I_Const.MENU_MAIN_CATEGORY_LIBLARY
																			, I_Const.MENU_SUB_CATEGORY_LIBLARY_OTHER);
			return item;
		}
		
		//アクティブなサブカテゴリ名
		public String getActiveSubCateName(){
			E_IRISHandler iris = E_IRISHandler.getInstance();
			if(String.isNotBlank(iris.irisMenus.activeMenu.SubCategory__c)){
				return I_Const.LIBRARY_SUB_CATE_NAME_ALIAS.get(iris.irisMenus.activeMenu.SubCategory__c);
			}
			return null;
		}

		//アクティブサブカテゴリが　保全手続書類の場合
		public Boolean getActiveSubCateIsMnt(){
			return getActiveSubCateName() == I_Const.MENU_CATEGORY_ALIAS_LIB_MNT;	//'lib_mnt';
		}

		//アクティブサブカテゴリが　セールスツールの場合
		public Boolean getActiveSubCateIsTool(){
			return getActiveSubCateName() == I_Const.MENU_CATEGORY_ALIAS_LIB_TOOL;	//'lib_tool';
		}

		//アクティブサブカテゴリが　パンフレット等の場合
		public Boolean getActiveSubCateIsPamph(){
			return getActiveSubCateName() == I_Const.MENU_CATEGORY_ALIAS_LIB_PAMPH;	//'lib_pamph';
		}

		//特集改修 ここから
		//アクティブサブカテゴリが　特集の場合
		public Boolean getActiveSubCateIsFeat(){
			return getActiveSubCateName() == I_Const.MENU_CATEGORY_ALIAS_LIB_FEAT;	//'lib_feat';
		}
		//特集改修 ここまで

		//アクティブサブカテゴリが　マニュアルの場合
		public Boolean getActiveSubCateIsManual(){
			return getActiveSubCateName() == I_Const.MENU_CATEGORY_ALIAS_LIB_MANUAL;	//'lib_manual';
		}

		//アクティブサブカテゴリが　その他の場合
		public Boolean getActiveSubCateIsOther(){
			return getActiveSubCateName() == I_Const.MENU_CATEGORY_ALIAS_LIB_OTHER;	//'lib_other';
		}
		/**
		 * I_TubeConnectHelperインスタンスを返す.
		 *
		 * @return I_TubeConnectHelper
		 */
		public I_TubeConnectHelper tubeConnectHelper{
														get{
															if(tubeConnectHelper==null){
																tubeConnectHelper = I_TubeConnectHelper.getInstance();
															}
															return tubeConnectHelper;
														}
														private set;
													}
	}

}