/**
 * グラフのインターフェース
 */
public Interface E_IGraph {
    //初期処理
    void init();
    //グラフ生成
    void createGraph();
}