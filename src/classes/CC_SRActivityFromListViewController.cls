/**
 * terrasky 2018/05/01
 * 一覧表表示クラス
 */
public with sharing class CC_SRActivityFromListViewController extends E_AbstractController {

	//内部定数
	private final String PARAM_KEY_BATCHKEY = 'bkey';
	private final String PARAM_KEY_TYPE = 'type';
	private final String AGENCY_BRCODE_HEAD_OFFICE = '03';
	private final Integer MORE_DISP_DATE = 60;

	//メンバ変数
	private User accsessUser;
	private String batchKey;
	public String targetDateStr {get;private set;}
	public String getTargetDay(){
		return targetDateStr.substring(0,10);
	}

	//フラグ
	//手数料関連表示フラグ
	public Boolean isDispCommission {get; set;}
	//保全ダイヤル表示フラグ
	public Boolean isDispConservation {get; set;}
	//過去60日みる画面チェックボックス
	public Boolean isChangeMoreDisp {get; set;}
	//過去60日表示中フラグ
	public Boolean isMoreDisp {get; private set;}

	//一覧表示関連
	public List<CC_SRActivityFromListViewUtils.MRSection> mrSecList {get; set;}

	public CC_SRActivityFromListViewController() {
		//値の初期化
		isDispCommission = false;
		isDispConservation = false;
	}

	/**
	 * 個別でチェック判定
	 */
	protected override boolean isValidate() {
		//社員のみ利用可能
		return access.isEmployee();
	}

	/**
	 * 画面アクション（初期処理）
	 */
	public PageReference init(){
		try{
					//Auth処理
			super.doAuth(null, null);
			accsessUser = [SELECT Name FROM User WHERE id =: UserInfo.getUserId()];

			//キーの取得
			batchKey = ApexPages.currentPage().getParameters().get(PARAM_KEY_BATCHKEY);
			if(String.isBlank(batchKey) || !Pattern.matches('[0-9]{8}_[0-9]{4}',batchKey)){
				getPageMessages().addErrorMessage('パラメータが不正です。');
				return null;
			}
			String activeKind = ApexPages.currentPage().getParameters().get(PARAM_KEY_TYPE);
			targetDateStr = batchKey.substring(0, 4) + '/'
						+ batchKey.substring(4, 6) + '/'
						+ batchKey.substring(6, 8) + ' '
						+ batchKey.substring(9, 11) + ':'
						+ batchKey.substring(11, 13);
			if(String.isNotBlank(activeKind)){
				if(activeKind == '1'){
					isDispCommission = true;
				}else if(activeKind == '2'){
					isDispConservation = true;
				}
			}
			String sWhere = 'WHERE CC_MRBatchKey__c = \'' + batchKey + '\'';
			//表示内容取得
			createDispList(sWhere);
		}catch(Exception e){
			getPageMessages().addErrorMessage(e.getMessage());
		}

		return null;
	}

	/**
	 * MRCheckボックスの更新処理を実施
	 *　対象項目以外のUpdateはさせない
	 */
	public PageReference doSave(){
		try{
			Map<Id,Case> upds = new Map<Id,Case>();
			String now = E_Util.getSysDate('yyyy/MM/dd HH:mm');
			for(CC_SRActivityFromListViewUtils.MRSection mrSec :mrSecList){
				for(CC_SRActivityFromListViewUtils.SRSection srSec :mrSec.srList){
					if(srSec.isCheck == srSec.record.CC_MRCheckBox__c){
						continue;
					}
					Case upd = new Case();
					upd.id = srSec.record.id;
					upd.CC_MRCheckBox__c = srSec.record.CC_MRCheckBox__c;
					srSec.isCheck = upd.CC_MRCheckBox__c;
					srSec.record.CC_MRLastModified__c = accsessUser.Name + ' ' + now;
					upd.CC_MRLastModified__c = srSec.record.CC_MRLastModified__c;
					upds.put(upd.id,upd);
				}
			}
			//更新対象があれば更新して再取得
			if(!upds.isEmpty()){
				update upds.values();
			}
		}catch(Exception e){
			getPageMessages().addErrorMessage(e.getMessage());
		}
		return null;
	}

	/**
	 * 画面再描画を実施
	 */
	public void doRefresh(){
		try{
			if(isChangeMoreDisp){
				doDispMore();
				isMoreDisp = true;
			}else{
				isMoreDisp = false;
				String sWhere = 'WHERE CC_MRBatchKey__c = \'' + batchKey + '\'';
				//表示内容取得
				createDispList(sWhere);
			}
		}catch(Exception e){
			getPageMessages().addErrorMessage(e.getMessage());
		}
		return;
	}

	public PageReference doDispMore(){
		String startDate = E_Util.addDaysyyyyMMdd(batchKey.substring(0, 8),-MORE_DISP_DATE);
		String sWhere = 'WHERE CC_MRBatchKey__c <= \'' + batchKey + '\' AND CC_MRBatchKey__c >= \'' + startDate + '\'';
		createDispList(sWhere);
		return null;
	}

	/**
	 * 表示対象作成クラス
	 */
	private void createDispList(String condition){
		Boolean isHead = access.ZINQUIRR_is_BRAll();
		String branchNo = (isHead)? AGENCY_BRCODE_HEAD_OFFICE:access.getAccessKindFromThree();

		//リストの初期化
		mrSecList = new List<CC_SRActivityFromListViewUtils.MRSection>();
		//取得対象のSRを特定
		Set<Id> targetIds = new Set<Id>();
		Map<Id,List<CC_SRActivity__c>> targetActMap = new Map<Id,List<CC_SRActivity__c>>();
		for(CC_SRActivity__c srActive : CC_SRActivityFromListViewDao.getRecsByBatchKey(condition)){
			targetIds.add(srActive.CC_SRNo__c);
			String key = srActive.CC_SRNo__c;
			if(!targetActMap.containsKey(key)){
				targetActMap.put(key,new List<CC_SRActivity__c>());
			}
			targetActMap.get(key).add(srActive);
		}

		//取得対象SRかつ表示対象支社のSRPolicyを取得
		String sWhereFormat = 'AND({0})';
		List<String> sWhereParamOR = new List<String>();
		String operator = (isHead)? '=' : '!=';

		//契約1～4まで対象
		for(Integer i = 1;i <= 4;i++){
			//主募集人
			List<String> conditions = new List<String>();
			conditions.add('CC_PolicyID'+ String.valueOf(i) + '__r.MainAgent__r.Account.E_CL2PF_BRANCH__c = \'' + branchNo + '\'');
			conditions.add('CC_PolicyID'+ String.valueOf(i) + '__r.MainAgent__r.Account.KSECTION__c ' + operator + ' \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\'');
			if(isHead){
				sWhereParamOR.addAll(conditions);
			}else{
				sWhereParamOR.add('(' + String.join(conditions,' AND ') + ')');
			}
			//従募集人
			conditions = new List<String>();
			conditions.add('CC_PolicyID'+ String.valueOf(i) + '__r.SubAgent__r.Account.E_CL2PF_BRANCH__c = \'' + branchNo + '\'');
			conditions.add('CC_PolicyID'+ String.valueOf(i) + '__r.SubAgent__r.Account.KSECTION__c ' + operator + ' \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\'');
			if(isHead){
				sWhereParamOR.addAll(conditions);
			}else{
				sWhereParamOR.add('(' + String.join(conditions,' AND ') + ')');
			}
		}
		String sWhere = String.format(sWhereFormat,new List<String>{String.join(sWhereParamOR,' OR ')});

		//画面表示対象の判定
		if(isDispCommission && isDispConservation){
			sWhere += ' AND (CC_CaseId__r.CC_IsCommissionFlag__c = true'
					+ ' OR CC_CaseId__r.CC_IsConservationFlag__c = true)';

		}else if(isDispCommission){
			sWhere += ' AND CC_CaseId__r.CC_IsCommissionFlag__c = true';
		}else if(isDispConservation){
			sWhere += ' AND CC_CaseId__r.CC_IsConservationFlag__c = true';
		}else{
			//条件無しの場合は後続処理なし
			return;
		}

		//ヘルパークラスの初期化
		CC_SRActivityFromListViewUtils helper = new CC_SRActivityFromListViewUtils();

		//Rowの作成
		for(CC_SRPolicy__c srPolicy : CC_SRActivityFromListViewDao.getRecsByCaseIdsWithsWhere(targetActMap.keySet(),sWhere)){
			for(CC_SRActivity__c srActive :targetActMap.get(srPolicy.CC_CaseId__r.id)){
				helper.setRowBySRPolicy(srPolicy,srActive,branchNo,isHead);
			}
		}
		mrSecList.addAll(helper.mrMap.values());
		mrSecList.sort();
	}
}