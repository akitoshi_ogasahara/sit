@isTest
private with sharing class TestE_InveController{
	private static testMethod void testPageMethods() {		E_InveController extension = new E_InveController(new ApexPages.StandardController(new E_ITHPF__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testdataTableITFPF() {
		E_InveController.dataTableITFPF dataTableITFPF = new E_InveController.dataTableITFPF(new List<E_ITFPF__c>(), new List<E_InveController.dataTableITFPFItem>(), new List<E_ITFPF__c>(), null);
		dataTableITFPF.create(new E_ITFPF__c());
		System.assert(true);
	}
	
}