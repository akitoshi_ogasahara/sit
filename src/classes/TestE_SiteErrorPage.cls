@isTest
public with sharing class TestE_SiteErrorPage {
    
    
    static testMethod void testE_UnauthorizedMessage() {
        //テストデータ作成
        createMessageMaster_Messages(true);
        
        //setCurrentPage
        PageReference nowPage = Page.E_Unauthorized;
        system.debug('@nowPage@= '+ nowPage);
        Test.setCurrentPage(nowPage);
        
        //テスト実行開始
        E_SiteErrorPage ctr = new E_SiteErrorPage();
        
        Test.startTest();
        
        String UnauthorizedMsg= ctr.getMSG().get(E_Const.SITES_ERROR_PAGE_CODE_401);
        String actualMsg =  ctr.getPageMessages().getErrorMessages().get(0).summary;
        String UrlName = ctr.getPageName(nowPage);
        
        Test.stopTest();
        
        system.debug('@CurrntPage@= ' + ApexPages.currentPage().getUrl());
        system.debug('@UnauthorizedMsg@= ' + UnauthorizedMsg);
        
        system.assertEquals(UnauthorizedMsg, actualMsg);
        system.assertEquals('E_UNAUTHORIZED',UrlName);
        
    }
    
	static testMethod void testE_FileNotFoundMessage() {
        //テストデータ作成
        createMessageMaster_Messages(true);
        
        //setCurrentPage
        PageReference nowPage = Page.E_FileNotFound;
        
        system.debug('@nowPage@= '+ nowPage);
        Test.setCurrentPage(nowPage);
        
        //テスト実行開始
        E_SiteErrorPage ctr = new E_SiteErrorPage();
        
        Test.startTest();
        
        String FileNotFoundMsg= ctr.getMSG().get(E_Const.SITES_ERROR_PAGE_CODE_404);
        String actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
        String UrlName = ctr.getPageName(nowPage);
        
        Test.stopTest();
        
        system.debug('@CurrntPage@= ' + ApexPages.currentPage().getUrl());
        system.debug('@FileNotFoundMsg@= ' + FileNotFoundMsg);
        
        system.assertEquals(FileNotFoundMsg, actualMsg);
        system.assertEquals('E_FILENOTFOUND',UrlName);
        
    
    }
    
    static testMethod void testE_InMaintenanceMessage() {
        //テストデータ作成
        createMessageMaster_Messages(true);
        
        //setCurrentPage
        PageReference nowPage = Page.E_InMaintenance;
        system.debug('@nowPage@= '+ nowPage);
        Test.setCurrentPage(nowPage);
        
        //テスト実行開始
        E_SiteErrorPage ctr = new E_SiteErrorPage();
        
        Test.startTest();
        
        String InMaintenanceMsg= ctr.getMSG().get(E_Const.SITES_ERROR_PAGE_CODE_500_503);
        String actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
        String UrlName = ctr.getPageName(nowPage);
        
        Test.stopTest();
        
        system.debug('@InMaintenanceMsg@= ' + InMaintenanceMsg);
        system.debug('@CurrntPage@= ' + ApexPages.currentPage().getUrl());
        
        system.assertEquals(InMaintenanceMsg, actualMsg);
        system.assertEquals('E_INMAINTENANCE',UrlName);
    
    }
    
    static testMethod void testE_BandwidthExceededMessage() {
        //テストデータ作成
        createMessageMaster_Messages(true);
        
        //setCurrentPage
        PageReference nowPage = Page.E_BandwidthExceeded;
        system.debug('@nowPage@= '+ nowPage);
        Test.setCurrentPage(nowPage);
        
        //テスト実行開始
        E_SiteErrorPage ctr = new E_SiteErrorPage();
        
        Test.startTest();
        
        String BandwidthExceededMsg= ctr.getMSG().get(E_Const.SITES_ERROR_PAGE_CODE_509);
        String actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
        String UrlName = ctr.getPageName(nowPage);
        
        Test.stopTest();
        
        system.debug('@CurrntPage@= ' + ApexPages.currentPage().getUrl());
        system.debug('@BandwidthExceededMsg@= ' + BandwidthExceededMsg);
        
        
        system.assertEquals(BandwidthExceededMsg, actualMsg);
        system.assertEquals('E_BANDWIDTHEXCEEDED',UrlName);
        
    
    }
    
    static testMethod void testE_ExceptionMessage() {
        //テストデータ作成
        createMessageMaster_Messages(true);
        
        //setCurrentPage
        PageReference nowPage = Page.E_Exception;
        system.debug('@nowPage@= '+ nowPage);
        Test.setCurrentPage(nowPage);
        
        //テスト実行開始
        E_SiteErrorPage ctr = new E_SiteErrorPage();
        
        Test.startTest();
        
        String ExceptionMsg= ctr.getMSG().get(E_Const.SITES_ERROR_PAGE_CODE_999);
        String actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
        String UrlName = ctr.getPageName(nowPage);
        
        Test.stopTest();
        system.debug('@CurrntPage@= ' + ApexPages.currentPage().getUrl());
        system.debug('@ExceptionMsg@= ' + ExceptionMsg);
        
        system.assertEquals(ExceptionMsg, actualMsg);
        system.assertEquals('E_EXCEPTION',UrlName);
    
    }
    
    static testMethod void testE_OtherMessage() {
        //テストデータ作成
        createMessageMaster_Messages(true);
        
        //テスト実行開始
        E_SiteErrorPage ctr = new E_SiteErrorPage();
        
        Test.startTest();
        
        String ExceptionMsg= ctr.getMSG().get(E_Const.SITES_ERROR_PAGE_CODE_999);
        String actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
//        String UrlName = ctr.getPageName(nowPage);
        
        Test.stopTest();
        system.debug('@CurrntPage@= ' + ApexPages.currentPage().getUrl());
        system.debug('@ExceptionMsg@= ' + ExceptionMsg);
        
        system.assertEquals(ExceptionMsg, actualMsg);
 //       system.assertEquals('E_EXCEPTION',UrlName);
        
    
    }
    
    
    
    
    
    
    
    
 /*********************************************************************************************************************
 * テストデータ作成用
 *********************************************************************************************************************/
    /**
     * RecordTypeマップ取得
     * @return Map<String, Map<String, Id>>: Map(sObjectType, Map(RecTypeDevName,RecTypeID)
     */
    private static Map<String, Map<String, Id>> rMap;
    private static Map<String, Map<String, Id>> getRecTypeIdMap(){
        if(rMap != null) return rMap;
        rMap = new Map<String, Map<String, Id>>();
        for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
            if(rMap.containsKey(rt.sObjectType)){
                rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
            }else{
                rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
            }
        }
        return rMap;
    }
    

    private static E_MessageMaster__c[] createMessageMaster_Messages(Boolean isInsert) {
        Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get('MSG_DISCLAIMER');
        final String TYPE_MESSAGE = 'メッセージ';
        final String TYPE_DTSCRIMER = 'ディスクレイマー';
        final String MSG_NAME = 'name';
        List<E_MessageMaster__c> src = new List<E_MessageMaster__c>{
              new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= E_Const.SITES_ERROR_PAGE_CODE_401, Value__c='E_Unauthorizedエラーです')            
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= E_Const.SITES_ERROR_PAGE_CODE_404, Value__c='E_FileNotFoundエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= E_Const.SITES_ERROR_PAGE_CODE_500_503, Value__c='E_InMaintenanceエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= E_Const.SITES_ERROR_PAGE_CODE_509, Value__c='E_BandwidthExceededエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c= E_Const.SITES_ERROR_PAGE_CODE_999, Value__c='E_Exceptionエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_DTSCRIMER, Key__c= E_Const.SITES_ERROR_PAGE_CODE_401, Value__c='テストE_Unauthorizedエラーです')            
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_DTSCRIMER, Key__c= E_Const.SITES_ERROR_PAGE_CODE_404, Value__c='テストE_FileNotFoundエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_DTSCRIMER, Key__c= E_Const.SITES_ERROR_PAGE_CODE_500_503, Value__c='テストE_InMaintenanceエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_DTSCRIMER, Key__c= E_Const.SITES_ERROR_PAGE_CODE_509, Value__c='テストE_BandwidthExceededエラーです')
            , new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_DTSCRIMER, Key__c= E_Const.SITES_ERROR_PAGE_CODE_999, Value__c='テストE_Exceptionエラーです')
           
            
        };
        if(isInsert){
            insert src;
        }
        return src;
    }
    
    

}