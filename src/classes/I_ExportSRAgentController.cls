public with sharing class I_ExportSRAgentController extends E_CSVExportController{
	private List<E_AgencySalesResults__c> salesResults;
	private String sWhere;
	private String message;

	public override String mainSObjectAPIName(){
		return Schema.SObjectType.E_AgencySalesResults__c.Name;
	}

	private String getBizDate(){
		return E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
	}
	/**
	 *  Constructor
	 */
	public I_ExportSRAgentController(){
		super();
		message = '';

		//パラメータ取得
		sWhere = ApexPages.currentPage().getParameters().get('soqlWhere');
		
		// データ取得
		salesResults = E_AgencySalesResultsDaoWithout.getSRAgentRecs(sWhere,true);
		if(salesResults.size() == 10001){
			message = '募集人が表示上限の10000件を超えております。お手数ですが、サポートセンターにご確認ください';
			salesResults.remove(10000);
		}
	}
	/**
	 * タイトル
	 */
	public String getCSVTitle(){
		List<String> titles = new List<String>();
		E_AgencySalesResults__c sr = E_AgencySalesResultsDaoWithout.getAHRecByAccId(salesResults[0].ParentAccountId__c,getBizDate());
		titles.add(sr.AgencyName__c + '　募集人一覧');
		titles.add('');	//表示位置の調整
		titles.add('データ基準日　　'+sr.BusDate__c);	//営業日
		if(String.isBlank(message)){
			return String.join(titles, CSV_SEPARATOR()) + CSV_BREAKELINE();
		}else{
			return message + CSV_BREAKELINE() + String.join(titles, CSV_SEPARATOR()) + CSV_BREAKELINE();
		}

	}
	/**
	 * ヘッダー
	 */
	public override String getCSVHeader(){
		List<String> header = new List<String>();
		header.add('募集人コード');
		header.add('募集人登録番号');
		header.add('募集人名');
		header.add('登録区分');
		header.add('所属事務所名');
		header.add('当社商品の募集資格');
		header.add('当社募集開始日');
		header.add('当社での変額資格');
		header.add('専門資格(合格年月/合格番号)');
		return String.join(header, CSV_SEPARATOR()) + CSV_BREAKELINE();
	}
	/**
	 * 行
	 */
	public List<String> getLines(){
		try{
			List<String> lines = new List<String>();
			
			// CSVデータ作成
			for(E_AgencySalesResults__c src : salesResults){
				List<String> ln = new List<String>();

				// 募集人コード
				ln.add(E_Util.getEscapedCSVString(src.AgentCode__c));
				// 募集人登録番号
				ln.add(E_Util.getEscapedCSVString(src.XHAT_KMOFCODE__c));
				//募集人名
				ln.add(E_Util.getEscapedCSVString(src.AgentNm__c));
				//登録区分
				ln.add(E_Util.getEscapedCSVString(src.AgentType__c));
				//所属事務所名
				ln.add(E_Util.getEscapedCSVString(src.BelongOfficeName__c));
				//当社商品の募集資格
				ln.add(E_Util.getEscapedCSVString(src.CnvsLicense_IRIS__c));
				//当社募集開始日
				ln.add(E_Util.getEscapedCSVString(src.ComStartDate__c));
				//当社での変額資格
				ln.add(E_Util.getEscapedCSVString(src.CnvsLicenseVariableIns__c));
				//専門資格(合格年月/合格番号)
				ln.add(E_Util.getEscapedCSVString(src.AGED_CAP__c));

				lines.add(String.join(ln, CSV_SEPARATOR()) + CSV_BREAKELINE());
			}
			return lines;
		}catch(Exception e){
			return null;
		}
	}
}