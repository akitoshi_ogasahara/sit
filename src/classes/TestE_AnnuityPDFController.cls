@isTest
private with sharing class TestE_AnnuityPDFController{
	private static testMethod void testPageMethods() {		E_AnnuityPDFController extension = new E_AnnuityPDFController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testdataTableSTEP() {
		E_AnnuityPDFController.dataTableSTEP dataTableSTEP = new E_AnnuityPDFController.dataTableSTEP(new List<E_TransitionWK__c>(), new List<E_AnnuityPDFController.dataTableSTEPItem>(), new List<E_TransitionWK__c>(), null);
		dataTableSTEP.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
}