/**
 * ユニットプライス基本情報DAO
 */
public with sharing class E_UPBPFDao {
	/**
     * ユニットプライス基本情報IDをキーにユニットプライス基本情報を取得する
     * 複数件存在する場合には、1件目を返却する。
     * @param upbpfId: ユニットプライス基本情報ID
     * @return E_UPBPF__c: ユニットプライス基本情報
	 */
	public static E_UPBPF__c getRecById (String upbpfId) {
		E_UPBPF__c rec = null;
		List<E_UPBPF__c> recs = [
			Select 
			SystemModstamp, OwnerId, Name, Id, CreatedDate, CreatedById,JOBNM__c, ZPETNAME__c, DATIME__c, USRPRF__c, ZGFUNDCD__c, E_FundMaster__c,E_FundMaster__r.ZGFUNDNM__c,
			ZGFUNDNM__c, ZHDCLOR__c, ZIDXDATE01__c, ZIDXDATE02__c, ZIDXDATE03__c, ZIDXDATE04__c, ZIDXDATE05__c, ZUNTGIF__c, ZUNTLNK__c, ZRPTGIF01__c, 
			ZRPTLNK__c, ZBKCHNL__c, ZPRDCD__c, ZPRDGIF01__c, ZPRDGIF02__c, ZPRDLNK__c, ZPRDNAME__c, ZPRDNMCD__c, ZCHANEL__c, Account__c, ZHPLNK__c, 
			ZHEADAY__c, ZAYNAME01__c, ZAYNAME02__c, ZQLFLG__c, ZDSPFLAG__c, ZSTTMFRM__c, ZSTTMTO__c, ZSTDTFRM__c, ZSTDTTO__c, ZSEQNUM__c, NameClass__c,
			dataSyncDate__c
			From E_UPBPF__c
			Where id =: upbpfId
			And ZDSPFLAG__c in : E_Const.UP_VISIBLE_ZDSPFLAG
		];
		if (recs.size() > 0) {
		    rec = recs.get(0);
		}
		return rec;
	}

	/**
     * ユニットプライス基本情報の商品名コードと、表示/非表示フラグをキーにユニットプライス基本情報を取得する
     * 複数件存在する場合には、1件目を返却する。
     * @param prdCode: 商品名コード	
     * @param flg: 表示/非表示フラグ
     * @return E_UPBPF__c: ユニットプライス基本情報
	 */
    
    public static E_UPBPF__c getRecByZPRDCD(String prdCode){
        String nowtime =  E_Util.getSysDate('yyyyMMddHHmmss');
        //ユニットプライス基本情報データ取得
        LIST<E_UPBPF__c> retE_UPBPF = [
            Select E_FundMaster__c
            ,ZIDXDATE01__c 
            ,ZQLFLG__c
            ,ZPETNAME__c
            ,ZGFUNDNM__c
            ,ZGFUNDCD__c
            ,E_FundMaster__r.ZGFUNDCD__c
            From E_UPBPF__c  
            where ZDSPFLAG__c in : E_Const.UP_VISIBLE_ZDSPFLAG
            AND ZPRDCD__c = :prdCode 
            AND STDTTMFRM__c <= :nowtime 
            limit 1
        ];
        if(retE_UPBPF.isEmpty()){
            return null;
        }else{
            return retE_UPBPF[0];
        }
    }

}