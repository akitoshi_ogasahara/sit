public without sharing class ASS_ASSMemoDaoWithout {
	
	/**
	 * 営業日報メモ取得
	 * @param officeId: 事務所ID 
	 * @return List<ASS_Memo__c>: メモリスト
	 */
	public static List<ASS_Memo__c> getMemosByOfficeId(String officeId) {
		return [SELECT 
					Id
					, Name
					, Contents__c
					, Account__c
					, LastModifiedDate
					,LastModifiedBy.LastName
					, LastModifiedBy.FirstName 
				FROM 
					ASS_Memo__c 
				WHERE 
					Account__c = :officeId 
				Order By 
					LastModifiedDate DESC NULLS LAST];
	}


	/**
	 * 営業日報メモ削除処理
	 * @param memoList: メモのリスト
	 * @return 
	 */
	public static void deleteMemo(List<ASS_Memo__c> memoList) {
		delete memoList;
	}
}