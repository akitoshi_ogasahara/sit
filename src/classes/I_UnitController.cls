public with sharing class I_UnitController extends I_AbstractController {
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// リスト行数
	public Integer rowCount {get; set;}

// 営業部一覧
	// 営業部リスト
	public List<UnitListRow> UnitRows {get; set;}

	//営業部名
	public String uName {get; set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}

	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE		 = 'st';
	// URLパラメータ名 - フィルタID
	public static final String URL_PARAM_FILTER_ID		 = 'id';
	//ソートキー
	public static final String SORT_TYPE_UNITNAME    = 'unitName';
	public static final String SORT_TYPE_UNITAREA    = 'unitArea';
	public static final String SORT_TYPE_UNITADDRESS = 'unitAddress';
	public static final String SORT_TYPE_PNUM        = 'phoneNum';
	public static final String SORT_TYPE_DORDER		 = 'displayOrder';


	// コンストラクタ
	public I_UnitController() {
		pageMessages = getPageMessages();

		// デフォルトソート - 並び順・昇順
		sortType = SORT_TYPE_DORDER;
		sortIsAsc = true;

		// URLパラメータから繰り返し行数を取得
		//rowCount = I_Const.LIST_DEFAULT_ROWS;
		//表示対象が少ないため、常に全件表示
		rowCount = I_Const.LIST_MAX_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
		}
	}

	//メニューキー（後ほど修正）
	protected override String getLinkMenuKey() {
		return 'agencyList';
	}

	//初期処理
	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}

		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}
		getUnitList();
		// ソート
/*		I_UnitController.list_sortType  = sortType;
		I_UnitController.list_sortIsAsc = sortIsAsc;
		UnitRows.sort();*/
		return null;
	}

	/**
	 * 個別でチェック判定
	 */
	protected override boolean isValidate() {
		//BR**以外はエラーとする
		return access.ZINQUIRR_is_BRAll();
	}

	/**
	 * 営業部一覧作成
	 */
	private void getUnitList() {

		//営業部表示用リスト
		UnitRows = new List<UnitListRow>();

		//取得用リスト
		List<E_Unit__c> units = E_UnitDao.getUnitRec();

		//0件の時エラー
		if(units.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('LIS|002'));
			return;
		}

		//最大件数以上の場合は警告を表示
		if(units.size() >= I_Const.LIST_MAX_ROWS){
			pageMessages.addWarningMessage(getMSG().get('LIS|004'));
		}

		for (E_Unit__c uRec : units) {
			UnitListRow row = new UnitListRow();
				row.id   		= uRec.Id;
				row.unitName    = uRec.Name;
				row.unitArea    = uRec.Area__r.Name;
				row.unitAddress = uRec.E_COMM_ZCLADDR__c;
				row.phoneNum    = uRec.E_CLTPF_CLTPHONE01__c;
				row.unitCode    = uRec.BRANCH__c;
				row.disOrder    = uRec.DisplayOrder__c;

			UnitRows.add(row);

			if(UnitRows.size() >= 999){
				break;
			}
		}
		UnitListRow row = new UnitListRow();

//		row.id   		= uRec.Id;
		row.unitName    = '本社営業部';
		row.unitArea    = '本社';
		row.unitCode    = '00';
		row.disOrder    = '1';

		UnitRows.add(row);

	}


	/**
	 * ソート
	 */
	public PageReference sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}

		I_UnitController.list_sortType  = sortType;
		I_UnitController.list_sortIsAsc = sortIsAsc;
		UnitRows.sort();

		return null;
	}

	//契約者一覧へ遷移
	public PageReference moveToOwnerList() {
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);
		E_Unit__c UnitRec = E_UnitDao.getUnitRecById(id);
		String branch = UnitRec.BRANCH__c;
		String field = I_Const.CONDITION_FIELD_UNIT;

		E_IRISHandler.setPolicyCondition(field, branch);

		PageReference pr = Page.IRIS_Owner;
		pr.getParameters().put(I_Const.URL_PARAM_MENUID, iris.irisMenus.menuItemsByKey.get('ownerList').record.Id);
		pr.setRedirect(true);

		return pr;
	}

	
	//新契約一覧へ遷移
	public PageReference moveToNewPolicyList() {
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);

		E_Unit__c UnitRec = E_UnitDao.getUnitRecById(id);
		String branch = UnitRec.BRANCH__c;

		String field = I_Const.CONDITION_FIELD_UNIT;
		E_IRISHandler.setPolicyCondition(field, branch);

		PageReference pr = Page.IRIS_NewPolicy;
		pr.setRedirect(true);

		return pr;
	}


	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	//総行数取得
	public Integer getTotalRows(){
		Integer totalRow = 0;

		totalRow = UnitRows  == null ? 0 : UnitRows.size();
		return totalRow;
	}


	/**
	 * 内部クラス
	 */
	public class UnitListRow implements Comparable {
		//SFID
		public String id {get; set;}
		//営業部名
		public String unitName {get; set;}
		//エリア
		public String unitArea {get; set;}
		//住所
		public String unitAddress {get; set;}
		//電話番号
		public String phoneNum {get; set;}
		//支社コード
		public String unitCode {get; set;}
		//表示順
		public String disOrder {get; set;}

		//コンストラクタ
		public UnitListRow(){
			id = '';
			unitName = '';
			unitArea = '';
			unitAddress = '';
			phoneNum = '';
			unitCode = '';
			disOrder = '';
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			UnitListRow compareToRow = (UnitListRow)compareTo;
			System.debug('[UnitListRow]' + compareToRow);
			Integer result = 0;

			//営業部名
			if(list_sortType == SORT_TYPE_UNITNAME){
				result = I_Util.compareToString(unitName, compareToRow.unitName);
			//エリア
			}else if(list_sortType == SORT_TYPE_UNITAREA){
				result = I_Util.compareToString(unitArea, compareToRow.unitArea);
			//住所
			}else if(list_sortType == SORT_TYPE_UNITADDRESS){
				result = I_Util.compareToString(unitAddress, compareToRow.unitAddress);
			//電話番号
			}else if(list_sortType == SORT_TYPE_PNUM){
				result = I_Util.compareToString(phoneNum, compareToRow.phoneNum);
			//表示順（デフォルトソート）
			}else if(list_sortType == SORT_TYPE_DORDER){
				result = I_Util.compareToString(disOrder, compareToRow.disOrder);
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}

}