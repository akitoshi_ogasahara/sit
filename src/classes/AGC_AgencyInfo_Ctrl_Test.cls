@isTest(SeeAllData=false)
private class AGC_AgencyInfo_Ctrl_Test{

	private static testMethod void testRegistAgencyInfo(){

		AGC_TestCommon common = new AGC_TestCommon();
		AGC_DairitenKihonJoho__c kihon;
		AGC_DairitenJimushoHosokuJoho__c hosoku;
		AGC_ShishaMr_Mst__c tanto;
		AGC_Shisha_Mst__c sisha;
		AGC_Common_Dao dao = new AGC_Common_Dao();
		AGC_ContactRireki__c rireki;
		Integer nowCount = 0;
		
		common.getCustomSetting();
		User apiUser = common.createApiUser();
		insert apiUser;		
		
		System.RunAs(apiUser){
			//準備
			sisha = common.createShishaMst();
			insert sisha;
			tanto = common.createTantoMst();
			insert tanto;
			kihon = common.createKihonInfo(sisha.Id,tanto.Id);
			insert kihon;
			hosoku = dao.getDairitenHosokujohoBykihonId(kihon.Id);
		}
		//create vf
		PageReference pageRef = Page.AGC_AgencyInfoRegist_Page;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',kihon.Id);

		//create controller
		ApexPages.StandardController standard = new ApexPages.StandardController(kihon);
		AGC_AgencyInfo_Ctrl controller = new AGC_AgencyInfo_Ctrl(standard);

		User u =common.createEigyoUser();
		insert u;
		System.RunAs(u){
			
			controller.getKihon();
			controller.getHosoku();
			
			hosoku.MailAddr1__c = 'アドレス１';
			hosoku.MailAddr2__c = 'アドレス２';
			hosoku.MailAddr3__c = 'アドレス３';
			hosoku.MailOwner1__c = 'メール１';
			hosoku.MailOwner2__c = 'メール２';
			hosoku.MailOwner3__c = 'メール３';
			hosoku.MobileNumber2__c = '番号２';
			hosoku.MobileNumber1__c = '番号１';
			hosoku.MobileOwner1__c = 'モバイル１';
			hosoku.MobileOwner2__c = 'モバイル２';
			hosoku.Segment__c = 'L';
			hosoku.Status__c = 'L';
			hosoku.SendMethod__c = '1';
			hosoku.JoinStatus__c = 'Y';
			hosoku.Competitor__c = 'Y';
			hosoku.AgencyInfo__c = '基本情報';
		
			controller.setHosoku(hosoku);
			controller.saveHosokuInfo();
		
			AGC_DairitenJimushoHosokuJoho__c result = [select MailAddr1__c
					,MailAddr2__c
					,MailAddr3__c
					,MailOwner1__c
					,MailOwner2__c
					,MailOwner3__c
					,MobileNumber2__c
					,MobileNumber1__c
					,MobileOwner1__c
					,MobileOwner2__c
					,Segment__c
					,Status__c
					,SendMethod__c
					,JoinStatus__c
					,Competitor__c
					,AgencyInfo__c 
				from AGC_DairitenJimushoHosokuJoho__c
				where Id = :hosoku.Id
			];
		
		
			System.assertEquals('アドレス１',result.MailAddr1__c);
			System.assertEquals('アドレス２',result.MailAddr2__c);
			System.assertEquals('アドレス３',result.MailAddr3__c);
			System.assertEquals('メール１',result.MailOwner1__c);
			System.assertEquals('メール２',result.MailOwner2__c);
			System.assertEquals('メール３',result.MailOwner3__c);
			System.assertEquals('番号２',result.MobileNumber2__c);
			System.assertEquals('番号１',result.MobileNumber1__c);
			System.assertEquals('モバイル１',result.MobileOwner1__c);
			System.assertEquals('モバイル２',result.MobileOwner2__c);
			System.assertEquals('L',result.Segment__c);
			System.assertEquals('L',result.Status__c);
			System.assertEquals('1',result.SendMethod__c);
			System.assertEquals('Y',result.JoinStatus__c);
			System.assertEquals('Y',result.Competitor__c);
			System.assertEquals('基本情報',result.AgencyInfo__c);
		
			System.assertEquals('担当者',controller.getMrMst().Name);
			System.assertEquals('01',controller.getMrMst().MRCode__c);
			System.assertEquals('テスト部署名　担当者 *',controller.getMrMst().DisplayName__c);

			System.assertEquals('A1',controller.getShishaMst().ShishaCd__c);
			System.assertEquals('テスト支社名',controller.getShishaMst().Name);
		}
	}
}