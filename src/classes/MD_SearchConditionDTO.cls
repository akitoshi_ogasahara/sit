public with sharing class MD_SearchConditionDTO {
	/* 選択値 */
	// 選択値 性別
	public String selectedGender {get; set;}
	// 選択値 血液型
	public String selectedBlood {get; set;}
	// 選択値 特定医
	// public String selectedParticularDoctor {get; set;}
	// 選択値 心電図
	public String selectedElectrocardiogram {get; set;}
	// 選択値 年齢
	public String selectedAge {get; set;}
	// 選択値 指定位置からの距離
	public String selectedDistance {get; set;}

	/* 検索値 */
	// 検索値 性別
	public String searchGender {get; set;}
	// 検索値 血液型
	public String searchBlood {get; set;}
	// 検索値 特定医
	// public String searchParticularDoctor {get; set;}
	// 検索値 心電図
	public String searchElectrocardiogram {get; set;}
	// 検索値 年齢
	public String searchAge {get; set;}
	// 検索値 指定位置からの距離
	public String searchDistance {get; set;}

	// 緯度
	public Double latitude {get; set;}
	// 経度
	public Double longitude {get; set;}
	// 縮尺
	public Double zoom {get; set;}

	// 画面表示用
	// 選択値 性別 ラベル
	public String selectedGenderLabel {get; set;}
	// 選択値 血液型 ラベル
	public String selectedBloodLabel {get; set;}
	// 選択値 特定医 ラベル
	// public String selectedParticularDoctorLabel {get; set;}
	// 選択値 心電図 ラベル
	public String selectedElectrocardiogramLabel {get; set;}
	// 選択値 年齢 ラベル
	public String selectedAgeLabel {get; set;}
	// 選択値 指定位置からの距離 ラベル
	public String selectedDistanceLabel {get; set;}
	// 検索種別(遷移元検索画面 )
	public String selectedKind {get; set;}

	// 年齢
	public List<SelectOption> ageOptions {get; set;}
	public List<String> selectedAges {get; set;}
	public List<String> searchAges {get; set;}

	// 住所・駅検索 条件値
	public String addrStation {get; set;}
	public String addrStationText {get; set;}

	// エリア検索 条件値
	public String areaCode {get; set;}
	public String areaText {get; set;}

	/**
	 * Constractor
	 */
	public MD_SearchConditionDTO() {
		// 選択リスト値設定: 年齢
		ageOptions = createAgeOptions();

		// 初期値セット
		setDefaultValues();
	}

	/** 縮尺取得 */
	public void setZoom(Double z) {
		if (z == 0.5) {
			zoom = 10;
		} else if(z == 1) {
			zoom = 9;
		} else if(z == 3) {
			zoom = 7;
		} else if (z == 5) {
			zoom = 6;
		} else if(z == 10) {
			zoom = 5;
		} else if(z == 20) {
			zoom = 4;
		} else if(z == 50) {
			zoom = 3;
		} else if(z == 100) {
			zoom = 2;
		}
	}

	/** 選択リスト取得 性別 */
	public List<SelectOption> getGenderOptions() {
		return createGenderOptions();
	}

	/** 選択リスト取得 血液型 */
	public List<SelectOption> getBloodOptions() {
		return createFieldOptions(MD_Const.DOCTOR_COL_API_BLOOD);
	}

	/** 選択リスト取得 特定医 */
	// public List<SelectOption> getParticularDoctorOptions() {
	// 	return createParticularOptions();
	// }

	/** 選択リスト取得 心電図 */
	public List<SelectOption> getElectrocardiogramOptions() {
		return createFieldOptions(MD_Const.DOCTOR_COL_API_ELECTROCARDIOGRAM);
	}

	/** 選択リスト取得 年齢
	public List<SelectOption> getAgeOptions() {
		return createAgeOptions();
	}
	 */

	/** 選択リスト取得 指定位置からの距離 */
	public List<SelectOption> getDistanceOptions() {
		return createDistanceOptions();
	}

	/**
	 * オブジェクト項目から選択リスト作成
	 */
	public List<SelectOption> createFieldOptions(String fname) {
		List<SelectOption> options = new List<SelectOption>();

		DescribeFieldResult f = SObjectType.MD_Doctor__c.fields.getMap().get(fname).getDescribe();
		List<PicklistEntry> entries = f.getPicklistValues();
		for (PicklistEntry e : entries) {
			if (e.isActive()) {
				options.add(new SelectOption(fname + ' = \'' + e.getValue() + '\'', e.getLabel()));
			}
		}

		// 「指定なし」を追加
		options.add(createNoneSelect());

		return options;
	}

	/**
	 * 性別選択リスト作成
	 */
	public List<SelectOption> createGenderOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(MD_Const.DOCTOR_COL_API_GENDER + ' = \'M\'', '男性'));
		options.add(new SelectOption(MD_Const.DOCTOR_COL_API_GENDER + ' = \'F\'', '女性'));
		options.add(createNoneSelect());

		return options;
	}

	/**
	 * 特定医選択リスト作成
	 */
	// public List<SelectOption> createParticularOptions() {
	// 	List<SelectOption> options = new List<SelectOption>();
	// 	options.add(new SelectOption(MD_Const.DOCTOR_COL_API_PARTICULARDOCTOR + ' = \'Y\'', '該当'));
	// 	options.add(new SelectOption(MD_Const.DOCTOR_COL_API_PARTICULARDOCTOR + ' = \'N\'', '非該当'));
	// 	options.add(createNoneSelect());
	//
	// 	return options;
	// }

	/**
	 * 年齢選択リスト作成
	 */
	public List<SelectOption> createAgeOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('(' + MD_Const.DOCTOR_COL_API_AGE + ' < 40)','30歳代まで'));
		options.add(new SelectOption('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 40 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 50)', '40歳代'));
		options.add(new SelectOption('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 50 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 60)', '50歳代'));
		options.add(new SelectOption('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 60 AND ' + MD_Const.DOCTOR_COL_API_AGE + ' < 70)', '60歳代'));
		options.add(new SelectOption('(' + MD_Const.DOCTOR_COL_API_AGE + ' >= 70)', '70歳代以上'));

		return options;
	}

	/**
	 * 指定位置からの距離選択リスト作成
	 */
	public List<SelectOption> createDistanceOptions() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('0.5','500m'));
		options.add(new SelectOption('1','1Km'));
		options.add(new SelectOption('3','3Km'));
		options.add(new SelectOption('5','5Km'));
		options.add(new SelectOption('10','10Km'));
		options.add(new SelectOption('20','20Km'));
		options.add(new SelectOption('50','50Km'));
		options.add(new SelectOption('100','100Km'));

		return options;
	}

	/**
	 * 選択肢に「指定なし」追加
	 */
	private SelectOption createNoneSelect() {
		return new SelectOption(MD_Const.SELECTED_NONE_VALUE, MD_Const.SELECTED_NONE_LABEL);
	}

	/**
	 * 選択リストのラベル取得
	 */
	public void getSelectOptionLabel() {
		// 性別
		selectedGenderLabel = changeSelectOptionValue(searchGender, getGenderOptions());
		// 血液型
		selectedBloodLabel = changeSelectOptionValue(searchBlood, getBloodOptions());
		// 特定医
		// selectedParticularDoctorLabel = changeSelectOptionValue(searchParticularDoctor, getParticularDoctorOptions());
		// 心電図
		selectedElectrocardiogramLabel = changeSelectOptionValue(searchElectrocardiogram, getElectrocardiogramOptions());
		// 年齢
		selectedAgeLabel = changeSelectOptionValueCheckbox(searchAges, ageOptions);
		// 指定位置からの距離
		selectedDistanceLabel = changeSelectOptionValue(searchDistance, getDistanceOptions());

		return;
	}

	/**
	 * 選択リストの値(ラジオ) → ラベル変換
	 */
	private String changeSelectOptionValue(String target, List<SelectOption> selectList) {
		String result = MD_Const.SELECTED_NONE_LABEL;

		for (SelectOption so : selectList) {
			if (target == so.getValue()) {
				result = so.getLabel();
				break;
			}
		}

		return result;
	}

	/**
	 * 選択リストの値(チェックボックス) → ラベル変換
	 */
	private String changeSelectOptionValueCheckbox(List<String> targetList, List<SelectOption> selectList) {
		if (targetList.isEmpty()) {
			return MD_Const.SELECTED_NONE_LABEL;
		}

		// 選択リストのマップ(Value,Label)
		Map<String, String> listMap = new Map<String, String>();
		for (SelectOption s : selectList) {
			listMap.put(s.getValue(), s.getLabel());
		}

		// Labelの取得
		String result = '';
		for (String t : targetList) {
			String label = listMap.get(t);

			if (String.isBlank(result)) {
				result = label;
			} else {
				result += ',' + label;
			}
		}

		return result;
	}

	/**
	 * 選択リスト初期値セット
	 */
	private void setDefaultValues() {
		selectedGender = MD_Const.SELECTED_NONE_VALUE;
		selectedBlood = MD_Const.SELECTED_NONE_VALUE;
		// selectedParticularDoctor = MD_Const.SELECTED_NONE_VALUE;
		selectedElectrocardiogram = MD_Const.SELECTED_NONE_VALUE;
		selectedAge = MD_Const.SELECTED_NONE_VALUE;
		selectedDistance = '3';

		// 年齢（全選択）
		system.debug('■［start］' + ageOptions);
		selectedAges = new List<String>();
		for(SelectOption op : ageOptions){
			selectedAges.add(op.getValue());
		}
		system.debug('■［end］' + ageOptions);
	}
}