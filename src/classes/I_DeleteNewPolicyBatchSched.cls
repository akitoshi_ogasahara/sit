/**
 * 不要な新契約ステータスレコード削除バッチを起動するスケジューラクラス
 */
global class I_DeleteNewPolicyBatchSched implements Schedulable {

	global void execute(SchedulableContext sc){
		// E_Util.SYSTEM_TODAY を使って起動
		Database.executeBatch(new I_DeleteNewPolicyBatch());
	}
}