public with sharing class E_BizDataSyncLogDao {
    
    /*
     * 最新の連携開始日時を持つレコードの取得（DataSyncEndDate__cがnullかはE_Utilにて判別）
     * [連携中かの判定用]
     */
    public static E_BizDataSyncLog__c getRecByLast1(){
        List<E_BizDataSyncLog__c> records =
            [SELECT
                  Id
                , Name
                , Kind__c
                , KindName__c
                , InquiryDate__c
                , DataSyncStartDate__c
                , DataSyncEndDate__c
            FROM
                E_BizDataSyncLog__c
            WHERE
                DataSyncStartDate__c != null
            AND
                (Kind__c = '1'
                OR
                Kind__c = '2')
            ORDER BY DataSyncStartDate__c DESC LIMIT 1 
        ];
        if (records.size() > 0) {
            return records.get(0);
        }
        return null;
    }
    
    private static E_BizDataSyncLog__c bizDataSyncLog;
    
    /*
     * 最新の連携終了日時を持つレコードの取得
     */
    public static E_BizDataSyncLog__c getNotBlankEndDateByLast1(){
        if(bizDataSyncLog != null){
            return bizDataSyncLog;
        }
        List<E_BizDataSyncLog__c> records =
            [SELECT
                  Id
                , Name
                , Kind__c
                , KindName__c
                , InquiryDate__c
                , DataSyncStartDate__c
                , DataSyncEndDate__c
            FROM
                E_BizDataSyncLog__c
            WHERE
                DataSyncStartDate__c != null
            AND
                DataSyncEndDate__c != null
            AND
                Kind__c = '1'
                /*(Kind__c = '1'
                OR
                Kind__c = '2')*/
            ORDER BY DataSyncEndDate__c DESC LIMIT 1 
        ];
        if (records.size() > 0) {
            bizDataSyncLog = records.get(0);
            return bizDataSyncLog;
        }
        return null;
    }

    /*
     * 最新の連携終了日時を持つレコードの取得（ユニットプライス用）
     */
    public static E_BizDataSyncLog__c getNotBlankEndDateByLast1_forUp(){
        List<E_BizDataSyncLog__c> records =
            [SELECT
                  Id
                , Name
                , Kind__c
                , KindName__c
                , InquiryDate__c
                , DataSyncStartDate__c
                , DataSyncEndDate__c
            FROM
                E_BizDataSyncLog__c
            WHERE
                DataSyncStartDate__c != null
            AND
                DataSyncEndDate__c != null
            AND
                Kind__c = '0'
            ORDER BY DataSyncEndDate__c DESC LIMIT 1 
        ];
        if (records.size() > 0) {
            return records.get(0);
        }
        return null;
    }
    
    public static E_BizDataSyncLog__c getRecById(Id recId){
        List<E_BizDataSyncLog__c> records =
            [SELECT
                  Id
                , Name
                , Kind__c
                , KindName__c
                , InquiryDate__c
                , DataSyncStartDate__c
                , DataSyncEndDate__c
                , OperationClassName__c
                , Log__c
                , NotificationKind__c
                , NotificationKindName__c
                , CreatedDate
            FROM
                E_BizDataSyncLog__c
            WHERE
                Id = :recId 
        ];
        if (records.size() > 0) {
            return records.get(0);
        }
        return null;
    }
    public static List<E_BizDataSyncLog__c> getRecsByKindAndCreatedDate(String kind ,Date d){
        if(String.isBlank(kind) || d == null) return new List<E_BizDataSyncLog__c>();
        return [Select id From E_BizDataSyncLog__c Where Kind__c =: kind And CreatedDate >=: d Limit 1];
    }
    
    /**
     * 通知区分より前回実施されたPush通知ログを取得
     * @param Id logId 今回実施の連携ログID
     * @param String nkind 通知区分
     * @return E_BizDataSyncLog__c
     */
    public static E_BizDataSyncLog__c getRecAgoByNotificationKindForPush(Id logId, String nkind){
    	return [
    		Select
    			Id, DataSyncStartDate__c 
    		From
    			E_BizDataSyncLog__c
    		Where
    			Kind__c = :I_Const.EBIZDATASYNC_KBN_PUSHOUT
    		And
    			NotificationKind__c = :nkind
    		And
    			Id <> :logId
    		Order by 
    			DataSyncStartDate__c desc
    		Limit 1
    	];
    }
    /**
     * 挙積情報表示レコード判定用
     */
    public static E_BizDataSyncLog__c getRecKindSRNotBlankBatchEndDateByLast1(){
        return [Select id,InquiryDate__c,CreatedDate From E_BizDataSyncLog__c Where kind__c =: 'F' AND BatchEndDate__c != null ORDER BY CreatedDate desc limit 1];
    }
}