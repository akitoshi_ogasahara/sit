public with sharing class SC_SearchExtender extends SkyEditor2.Extender{
    
    private SC_SearchSVEController controller;
    
    // 選択された結果レコード
    public String selectedIds {get; set;}
    // CMD判定
    public Boolean isCmd {get; set;}

    /**
     * Constructor
     */
    public SC_SearchExtender(SC_SearchSVEController extension){
        this.controller = extension;
	}

    /**
     * init override
     */
    public override void init(){
		// 年度の初期値セット
		this.controller.Component53_val.SkyEditor2__Text__c = system.Label.SC_FiscalYear;
		this.controller.Component53_val_dummy.SkyEditor2__Text__c = '["' + system.Label.SC_FiscalYear + '"]';
		
		// CMD判定
		E_AccessController access = E_AccessController.getInstance();
		isCmd = access.hasSCAdminPermission();
    }
    
    public override void preSearch() {
        system.debug('* queryMap * ' + controller.queryMap.get('resultTable'));
    }

    /** ［リマインド］　代理店向け＿締切前 */
    public String getRemindBeforeAg(){
    	return SC_MailManager.MT_REMIND_BEFORE_AG;
    }
    
    /** ［リマインド］　代理店向け＿締切後 */
    public String getRemindAfterAg(){
    	return SC_MailManager.MT_REMIND_AFTER_AG;
    }

    /** ［リマインド］　代理店向け＿不備対応中 */
    public String getRemindDefectAg(){
    	return SC_MailManager.MT_REMIND_DEFECT_AG;
    }
    
    /** ［リマインド］　MR向け＿締切前 */
    public String getRemindBeforeMr(){
    	return SC_MailManager.MT_REMIND_BEFORE_MR;
    }
    
    /** ［リマインド］　MR向け＿締切後 */
    public String getRemindAfterMr(){
    	return SC_MailManager.MT_REMIND_AFTER_MR;
    }
    
    /** ［リマインド］　MR向け＿不備対応中 */
    public String getRemindDefectMr(){
    	return SC_MailManager.MT_REMIND_DEFECT_MR;
    }
    
    /**
     * リマインドメールボタン押下処理
     */ 
    public void doRemaind(){
		// 選択されたIdをセット
    	List<String> ids = new List<String>();
    	for(SC_SearchSVEController.resultTableItem i : this.controller.resultTable.items){
			if(i.selected){
				ids.add(i.record.id);
			}
		}
		selectedIds = String.join(ids, ',');
    }
     
	/**
	 * CSVExport
	 */
	public PageReference ExportCSV(){
		PageReference pr = Page.E_SCExportSearch;
		String soql = controller.queryMap.get('resultTable').toSoql();
		String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
		sWhere = E_SoqlUtil.trimLimitClause(sWhere);
		pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
								E_EncryptUtil.getEncryptedString(sWhere));  
		return pr;
	}
    
    /**
     * レポートID取得
     */
    public String getReportId(){
    	String reportId = '';
    	
    	// レポート取得
    	Report rep = [Select Id From Report Where DeveloperName = 'RPT_ScOfficeList'];
    	if(rep != null){
    		reportId = rep.Id;
    	}
    	
    	return reportId;
    }


}