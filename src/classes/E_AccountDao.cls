/**
 * Accountデータアクセスオブジェクト
 * CreatedDate 2015/03/18
 * @Author Terrasky
 */
public with sharing class E_AccountDao {

	public static Account getRecById(String accountId){
		Account rec = null;
		List<Account> recs = [
			Select
				Id
				, Name                      //代理店格名/代理店事務所名
				, ParentId                  //Id（親取引先）
				, Parent.Name               //代理店格名（親取引先）
				, E_ParentName__c			//代理店格名（親取引先）※親の参照権限に依存しない
				, Parent.E_CL1PF_ZHEADAY__c //代理店格コード（親取引先）
				, E_CL2PF_ZAGCYNUM__c       //代理店事務所コード
				, E_CL1PF_ZHEADAY__c        //代理店格コード
				, ZHEADAY__c                //代理店番号
				, E_ParentZHEADAY__c
			From
				Account
			Where
				Id = :accountId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	public static List<Account> getRecsByParentId(Id accountId){
		return [
			Select
				Id
				, Name                   //代理店格名/代理店事務所名
				, E_CL2PF_ZAGCYNUM__c    //代理店事務所コード
			From
				Account
			Where
				ParentId = :accountId
				Order by E_CL2PF_ZAGCYNUM__c
		];
	}

	/**
	 * 代理店格のAccountレコードを取得
	 */
	public static List<Account> getRecByZHEADAY() {
		return [
			Select
				Id
				,E_CL1PF_ZHEADAY__c
				,E_CL1PF_ZBUSBR__c
			From
				Account
			Where
				E_IsAgency__c = true
		];
	}

	/**
	 * ログインユーザの照会者コード下二桁と一致する支店コードを持つAccountレコードを取得
	 */
	public static List<Account> getRecsByMRCode(String brunchCode) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', Parent.Id'
					+ ', Parent.Name'
					+ ', Parent.E_COMM_ZCLADDR__c'
					+ ', Parent.E_CL2PF_BRANCH__c'
					+ ', Parent.E_CL1PF_ZHEADAY__c'
					+ ', E_CL1PF_ZBUSBR__c'
					+ ', E_CL2PF_BRANCH__c '
					+ ', Parent.E_CL1PF_ZAHKNAME__c '		//代理店格カナ名--スプリントバックログ9(2017/1/18)追記
					+ 'FROM Account '
					+ 'WHERE Parent.Id != null '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';


		if (brunchCode.left(2) != '**') {
			soql += ' AND E_CL2PF_BRANCH__c = :brunchCode';
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS_AGGREGATE;

		return Database.query(soql);
	}

	/**
	 * 指定した代理店の事務所 かつ 担当拠点が一致 かつ 母店が含まれるチェックする
	 * @param String id:代理店のSFID
	 * @param String brunchCode:支社コード
	 */
	public static Integer getCountIsMotherOffice(String id, String brunchCode) {
		String soql = 'SELECT count() '
					+ 'FROM Account '
					+ 'WHERE Parent.Id = :id '
					+ 'AND E_CL2PF_BRANCH__c = :brunchCode '
					+ 'AND isMotherOffice__c = true ';

		return Database.countQuery(soql);
	}

	/**
	 * 事務所Accountレコードを取得
	 */
	public static List<Account> getRecsByAgency(String id, String brunchCode, Boolean isAllOffice, String sortField, Boolean sortIsAsc) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL2PF_ZAGCYNUM__c'
					+ ', E_COMM_ZCLADDR__c'
					+ ', CLTPHONE01__c'
					+ ', AGNTBR_NM__c'
					+ ', Owner.Name'
					+ ', Parent.Name '
					+ 'FROM Account '
					+ 'WHERE Parent.Id = :id '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';

		if(!isAllOffice){
			soql += 'AND E_CL2PF_BRANCH__c = :brunchCode';
		}

		if (String.isNotBlank(sortField)) {
			soql += ' ORDER BY ' + sortField;

			if (!sortIsAsc) {
				soql += ' DESC NULLS LAST';
			}
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	// IRIS Day2
	/**
	 * 検索ボックス（代理店）
	 */
	public static List<Account> getRecsIRISAgencySearchBox(String soqlWhere, String keyword){
		List<Account> result = new List<Account>();
		String soql = 'SELECT Id '
					+ ', Parent.Id '
					+ ', E_ParentName__c '
					+ ', E_ParentZHEADAY__c '
					+ ', Parent.E_COMM_ZCLADDR__c '
					+ 'FROM Account '
					// 事務所レコードのみ対象
					+ 'WHERE Parent.Id != null '
					// 有効な事務所のみ対象
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' '
					// 代理店名（部分一致）
					+ 'AND (E_ParentName__c like \'%' + String.escapeSingleQuotes(keyword) + '%\' '
					// 代理店コード（前方一致）
					+ 'OR E_ParentZHEADAY__c like \'' + String.escapeSingleQuotes(keyword) + '%\') '
					+ soqlWhere;
		system.debug(soql);
		result = Database.query(soql);

		return result;
	}

	/**
	 * MRIDと所有者が一致するAccountレコードを取得
	 */
	public static List<Account> getRecsByMRId(String mrId ,String brunchCode) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', Parent.Id'
					+ ', Parent.Name'
					+ ', Parent.E_COMM_ZCLADDR__c'
					+ ', Parent.E_CL2PF_BRANCH__c'
					+ ', Parent.E_CL1PF_ZHEADAY__c'
					+ ', E_CL1PF_ZBUSBR__c'
					+ ', E_CL2PF_BRANCH__c '
					+ ', Parent.E_CL1PF_ZAHKNAME__c '		//代理店格カナ名--スプリントバックログ9(2017/1/18)追記
					+ ', KSECTION__c '
					+ ', isMOffice__c '		//本社営業部担当(20170824)追記
					+ 'FROM Account '
					+ 'WHERE Parent.Id != null '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';
		if(String.isNotBlank(mrid)){
			soql += ' AND Owner.id =: mrid';
		}
		if (brunchCode.left(2) != '**') {
			soql += ' AND E_CL2PF_BRANCH__c = :brunchCode';
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS_AGGREGATE;

		return Database.query(soql);
	}

	/**
	 * 代理店・事務所取得(I_UpdateLogBatch)
	 * @param  Set<ID>:IDのリスト
	 * @return Map<Id, Account>:パラメータのIDとAccountレコード
	 */
	public static Map<Id, Account> getRecsByIds(Set<Id> Ids){
		Map<Id, Account> result = new Map<Id, Account>();
		for(Account acc : [SELECT
							Id
							, Name                      //代理店格名/代理店事務所名
							, ParentId                  //Id（親取引先）
							, Parent.Name               //代理店格名（親取引先）
							FROM Account
						   WHERE Id in :Ids]){
			result.put(acc.Id, acc);
		}
		return result;
	}

}