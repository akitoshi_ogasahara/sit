public with sharing class E_RecordCountController {
	public String executerName{get;set;}
	private static recordCountExecuter executer(String clsName){
								clsName = clsName.replace('+','.');		//内部クラスの場合+で連結されて受け取る
								Type t = Type.forName(clsName);
								return (recordCountExecuter)t.newInstance(); 
	}
	
	public List<String> getQueryIndicators(){
		return executer(executerName).getQueryIndicators();
	}
	@readOnly
	@RemoteAction	
	public static String getRecordSize(String executer_nm, String key){
		try{
			return executer(executer_nm).getRecordSize(key);
			
			//以下ガバナ制限チェック用 debug
			//executer(executer_nm).getRecordSize(key);
			//return 'key:' + string.valueOF(Limits.getQueryRows()) + '/' + string.valueOf(Limits.getLimitQueryRows());
		}catch(Exception e){
			return 'レコード件数取得処理でエラーが発生しました。   ' + e.getMessage();
		}
	}
	
	
	/*
	 *	recordCountExecuter
	 */
	public abstract class recordCountExecuter{
		//レコード件数問合せクエリ取得用Indicator　複数のクエリを投げる場合にそれぞれのクエリの識別子リストを返す
		public abstract List<String> getQueryIndicators();
		
		//レコード件数問合せ実施　レコード件数を返す（但し文字列として） パラメータでgetQueryIndicators()の文字列を受け取る
		public abstract String getRecordSize(String key);
	}
}