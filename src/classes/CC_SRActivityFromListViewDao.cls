public with sharing class CC_SRActivityFromListViewDao  {

	/**
	 * バッチキーが一致する活動の一覧を取得
	 * @param batchKey バッチキー
	 * @return List<CC_SRActivity__c>
	 */
	public static List<CC_SRActivity__c> getRecsByBatchKey(String sWhere){
		//パラメータ不正はnewを返す
		if(String.isBlank(sWhere)){return new List<CC_SRActivity__c>();}

		String query = 'SELECT '
					+ 'id, '
					+ 'CC_SRNo__c, '
					+ 'CC_MRBatchKey__c '
					+ 'FROM '
					+ 'CC_SRActivity__c ';
		query += sWhere;

		return Database.query(query);
	}

	/**
	 * Caseに紐づくSRPolicyを支社コードを考慮して取得
	 * @param cIds CaseIDのSet
	 * @param branchNo 支社コード
	 * @return List<CC_SRPolicy__c>
	 */
	public static List<CC_SRPolicy__c> getRecsByCaseIdsWithsWhere(Set<Id> cIds, String sWhere){
		//パラメータ不正はnewを返す
		if(cIds == null || cIds.isEmpty()){return new List<CC_SRPolicy__c>();}

		List<String> fieldNms = new List<String>{
												'id',
												'CC_CaseId__r.id',
												'CC_CaseId__r.CaseNumber',
												'CC_CaseId__r.CMN_Source__c',
												'CC_CaseId__r.CC_SRTypeName__c',
												'CC_CaseId__r.CMN_ClientRelation__c',
												'CC_CaseId__r.CMN_ClientName__c',
												'CC_CaseId__r.CC_MRCheckBox__c',
												'CC_CaseId__r.CMN_ReceptionDateTime__c',
												'CC_CaseId__r.CMN_ShippingTargetDate__c',
												'CC_CaseId__r.CMN_ShippingMethod__c',
												'CC_CaseId__r.CC_MRLastModified__c',
												'CC_CaseId__r.CC_IsConservationFlag__c',
												'CC_CaseId__r.CC_IsCommissionFlag__c'
												};

		for(Integer i = 1;i <= 4;i++){
			String pol = 'CC_PolicyID' + String.valueOf(i) + '__r.';
			fieldNms.add(pol + 'id');
			fieldNms.add(pol + 'COMM_CHDRNUM__c');
			fieldNms.add(pol + 'COMM_CHDRNUM_LINK_I__c');
			fieldNms.add(pol + 'Contractor__r.Name');

			fieldNms.add(pol + 'MainAgent__r.Name');
			fieldNms.add(pol + 'MainAgent__r.Account.Name');
			fieldNms.add(pol + 'MainAgent__r.Account.E_CL2PF_BRANCH__c');
			fieldNms.add(pol + 'MainAgent__r.Account.KSECTION__c');
			fieldNms.add(pol + 'MainAgent__r.Account.OwnerId');
			fieldNms.add(pol + 'MainAgent__r.Account.Owner.Name');

			fieldNms.add(pol + 'SubAgent__r.Name');
			fieldNms.add(pol + 'SubAgent__r.Account.Name');
			fieldNms.add(pol + 'SubAgent__r.Account.E_CL2PF_BRANCH__c');
			fieldNms.add(pol + 'SubAgent__r.Account.KSECTION__c');
			fieldNms.add(pol + 'SubAgent__r.Account.OwnerId');
			fieldNms.add(pol + 'SubAgent__r.Account.Owner.Name');
		}

		String query = 'SELECT ';
		query += String.join(fieldNms, ',') + ' ';
		query += 'FROM CC_SRPolicy__c ';
		query += 'WHERE CC_CaseId__r.id in: cIds ';
		query += sWhere;

		return Database.query(query);
	}
}