global with sharing class MNT_YakkanViewExtender extends SkyEditor2.Extender{
	
	// インスタンス
	public MNT_YakkanViewController extension;
	private I_ContentMaster__c record;
	
	/**
	 * コンストラクタ.
	 *
	 * @param MNT_YakkanViewController コントローラインスタンス
	 */
	public MNT_YakkanViewExtender(MNT_YakkanViewController extension){
		
		this.extension = extension;
	}
	
	/**
	 * 初期処理.
	 */
	global override void init(){
		
		this.record = extension.record;
	}
	
	/**
	 * レコード削除処理.
	 *
	 * @return PageReference 削除後ページ
	 */
	public PageReference doDelete() {
		
		List<I_ContentMaster__c> delRows = new List<I_ContentMaster__c>();

		// 子供と親をまとめて削除
		for(MNT_YakkanViewController.yakkanFileViewListItem item : this.extension.yakkanFileViewList.items){
			delRows.add(item.record);
		}
		
		delRows.add(this.record);
		
		try {
			delete delRows;
		} catch(Exception e) {
			
			System.debug(e.getMessage());
			
			SkyEditor2.ExtenderException exp = new SkyEditor2.ExtenderException();
			exp.addMessage('削除時にエラーが発生しました。' + e.getMessage());
			throw exp;
		}
		
		return null;
	}
}