public with sharing class MK_FriendRegistController extends E_AbstractController {
	// 友達登録部表示フラグ
	public Boolean isConfirmed {get; set;}
	//キャンペーン管理
	public CampaignManagement__c campaignManagement {get;set;}
	//キャンペーンターゲット
	public MK_CampaignTarget__c campaignTarget {get; set;}
	//リードTMP
	public List<MK_leadTMPDTO> friends {get; set;}
	//インサート用
	public List<MK_leadTMP__c> insfriends {get;private set;}
	//同意確認
	public Boolean Agree{get; set;}
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;private set;}
	//変更ボタン押下フラグ
	public Boolean isChecked {get;set;}
	//契約者メール再確認項目
	public String campaignTarget_RecomfirmEmail {get;set;}
	//契約者オリジナルメール項目
	private String campaignTarget_OriginalEmail;

	public class MK_leadTMPDTO{
		//リードテンプ
		public MK_leadTMP__c mk_leadtmp {get;set;}
		//メール再確認(友人)
		public String RecomfirmEmail {get; set;}
		//レコード番号
		public Integer recNum {get;private set;}

		public MK_leadTMPDTO(Id ctid,integer recNum){
			this.mk_leadtmp = new MK_leadTMP__c(MK_CampaignTarget__c = ctid);
			this.RecomfirmEmail = '';
			this.recNum = recNum;
		}
	}
	/**
	 * Constructor
	 */
	public MK_FriendRegistController() {
		system.debug('■[start Constructor]');
		// 友達登録部表示フラグの初期化
		isConfirmed = false;
		//キャンペーン管理
		campaignManagement = E_CampaignManagementDao.getActiveRecByBaton();
		// キャンペーンターゲットの初期化
		campaignTarget = new MK_CampaignTarget__c();
		// リードTMPの初期化
		friends = new List<MK_leadTMPDTO>();
		//変数初期化
		Agree = false;
		isChecked = false;
		pageMessages = getPageMessages();
		pageMessages.addErrorMessage(MK_Const.ERR_MSG09_URLPARAM_ID);
		insfriends = new List<MK_leadTMP__c>();
	}

	/**
	 * init
	 */
	public void init(){
		pageMessages.clearMessages();
		if(campaignManagement.id == null){
			pageMessages.addErrorMessage(MK_Const.ERR_MSG13_IS_NOT_ACTIVE_CAMPAIGN);
		}
	}

	/**
	 * doCheckCustomer
	 */
	public PageReference doCheckCustomer(){
		pageMessages = getPageMessages();
		try{
			if(isConfirmed) return null;
			//Nullチェック		ToDo 認証用文字列の長さは1文字以上で良いか確認
			if(String.isBlank(campaignTarget.CLTPF_CLNTNUM__c)
			|| String.isBlank(campaignTarget.ContractantKana__c)
			|| String.isBlank(campaignTarget.Name)){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG02_IS_NOT_ENTRY);
				return null;
			}
			//入力値整形
			String reqStr = clnsngStr(campaignTarget.ContractantKana__c);
			system.debug('■[reqStr]' + reqStr);
			if(String.isBlank(reqStr)){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG01_IS_NOT_CONFIRM);
				return null;
			}
			//禁則文字チェック
			if(E_Util.isNoSendString(campaignTarget.CLTPF_CLNTNUM__c) || E_Util.isNoSendString(reqStr)){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG10_IS_NO_SEND);
				return null;
			}
			//認証処理
			MK_CampaignTarget__c temp_campaignTarget = E_MK_CampaignTargetDao.getActiveRecByCLTPF_CLNTNUM(campaignTarget.CLTPF_CLNTNUM__c);
			if(temp_campaignTarget <> null){
				system.debug('■[doCheckCustomer1]');
				if(clnsngStr(temp_campaignTarget.ContractantKana__c) <> reqStr){
					pageMessages.addErrorMessage(MK_Const.ERR_MSG01_IS_NOT_CONFIRM);
					return null;
				}
				// 友達登録部表示フラグをtrue
				isConfirmed = true;
				campaignTarget = temp_campaignTarget;
				//取得したアドレスが空ならtrueをセット
				isChecked = String.isBlank(campaignTarget.ContractantMailAddress__c);
				campaignTarget_OriginalEmail = campaignTarget.ContractantMailAddress__c;
				//リスト作成
				for(integer i = 1;i <= 3;i++){
					friends.add(new MK_leadTMPDTO(campaignTarget.id,i));
				}

			} else {
				pageMessages.addErrorMessage(MK_Const.ERR_MSG01_IS_NOT_CONFIRM);
				system.debug('■[doCheckCustomer2]' + pageMessages);
			}
		} catch(Exception e){
			system.debug('■[doCheckCustomer3]' + e.getMessage());
		}
		system.debug('■[doCheckCustomer4]' + pageMessages + isConfirmed);
		return null;
	}

	/**
	 * doConfirm
	 */
	public PageReference doConfirm(){
		pageMessages = getPageMessages();
		try{
			if(String.isNotBlank(ApexPages.CurrentPage().getParameters().get('isChecked'))){
				isChecked = Boolean.valueOf(ApexPages.CurrentPage().getParameters().get('isChecked'));
			}
			insfriends = new List<MK_leadTMP__c>();
			Set<String> emailList = E_MK_leadTMPDao.getEmailListByCmpgnTrgt(campaignTarget.id);
			for(MK_leadTMPDTO friend:friends){
				if(friend.recNum <> 1
				&& String.isBlank(friend.mk_leadtmp.Company__c)
				&& String.isBlank(friend.mk_leadtmp.LastName__c)
				&& String.isBlank(friend.mk_leadtmp.FirstName__c)
				&& String.isBlank(friend.mk_leadtmp.Email__c)
				&& String.isBlank(friend.mk_leadtmp.LastNameKana__c)
				&& String.isBlank(friend.mk_leadtmp.FirstNameKana__c)
				&& String.isBlank(friend.RecomfirmEmail)){
					//最初のレコードでない かつ 未入力の際は、処理を行わない
				}else{
					//Nullチェック
					if(String.isBlank(friend.mk_leadtmp.Company__c)
					 || String.isBlank(friend.mk_leadtmp.LastName__c)
					 || String.isBlank(friend.mk_leadtmp.FirstName__c)
					 || String.isBlank(friend.mk_leadtmp.Email__c)
					 || String.isBlank(friend.mk_leadtmp.LastNameKana__c)
					 || String.isBlank(friend.mk_leadtmp.FirstNameKana__c)
					 || String.isBlank(friend.RecomfirmEmail)){
						pageMessages.addErrorMessage(MK_Const.ERR_MSG02_IS_NOT_ENTRY);
						return null;
					}
					//既契約者メール必須チェック
					if(isChecked
					&& (String.isBlank(campaignTarget.ContractantMailAddress__c) || String.isBlank(campaignTarget_RecomfirmEmail))){
						if(campaignTarget_OriginalEmail <> campaignTarget.ContractantMailAddress__c){
							pageMessages.addErrorMessage(MK_Const.ERR_MSG02_IS_NOT_ENTRY);
							return null;
						}
					}
					//既契約者メール再入力
					if(isChecked
					&& String.isNotEmpty(campaignTarget_RecomfirmEmail)){
						if(campaignTarget.ContractantMailAddress__c <> campaignTarget_RecomfirmEmail){
							pageMessages.addErrorMessage(MK_Const.ERR_MSG05_IS_NOT_RECON_EMAIL);
							return null;
						}
					}
					//カナ文字判定
					//ハイフンは許容する
					if(E_Util.isKatakana(friend.mk_leadtmp.LastNameKana__c.replace('ｰ', '').replace('ー', '')) && E_Util.isKatakana(friend.mk_leadtmp.FirstNameKana__c.replace('ｰ', '').replace('ー', ''))){
						//全角に統一する
						friend.mk_leadtmp.LastNameKana__c = E_Util.enKatakanaToEm(friend.mk_leadtmp.LastNameKana__c);
						friend.mk_leadtmp.FirstNameKana__c = E_Util.enKatakanaToEm(friend.mk_leadtmp.FirstNameKana__c);
					}else{
						pageMessages.addErrorMessage(MK_Const.ERR_MSG03_IS_NOT_KATAKANA);
						return null;
					}
					//禁則文字チェック メール項目は対象外とする
					if(E_Util.isNoSendString(friend.mk_leadtmp.Company__c)
					 || E_Util.isNoSendString(friend.mk_leadtmp.LastName__c)
					 || E_Util.isNoSendString(friend.mk_leadtmp.FirstName__c)
					 || E_Util.isNoSendString(friend.mk_leadtmp.LastNameKana__c)
					 || E_Util.isNoSendString(friend.mk_leadtmp.FirstNameKana__c)){
						pageMessages.addErrorMessage(MK_Const.ERR_MSG10_IS_NO_SEND);
						return null;
					}
					//メール再入力確認
					if(friend.mk_leadtmp.Email__c <> friend.RecomfirmEmail){
						pageMessages.addErrorMessage(MK_Const.ERR_MSG05_IS_NOT_RECON_EMAIL);
						return null;
					}

					//同一メールアドレス有無チェック
					if(emailList.contains(friend.mk_leadtmp.Email__c)){
						pageMessages.addErrorMessage(MK_Const.ERR_MSG06_IS_NOT_SAME_EMAIL);
						return null;
					}
					emailList.add(friend.mk_leadtmp.Email__c);
					//インサート対象
					insfriends.add(friend.mk_leadtmp);
					system.debug('■[doConfirm1]' + friend);
				}
			}
			//同一既契約者の友達登録上限数チェック
			if(E_MK_leadTMPDao.getRecCountByCmpgnTrgt(campaignTarget.id) + insfriends.size()
				> integer.valueOf(system.label.E_MK_BATON_FRIEND_REGIST_MAX)){
				pageMessages.addErrorMessage(String.format(MK_Const.ERR_MSG08_MAX_BATON, new String[]{system.label.E_MK_BATON_FRIEND_REGIST_MAX}));
				return null;
			}
		} catch(Exception e){
			system.debug('■[doConfirm2]' + e.getMessage());
			return null;
		}
		if(pageMessages.hasMessages()) return null;
		PageReference pref1 = Page.MK_FriendConfirm;
		pref1.setRedirect(false);
		return pref1;
	}
	/**
	 * doEdit
	 */
	public PageReference doEdit(){
		pageMessages = getPageMessages();
		PageReference pref1 = Page.MK_FriendRegist;
		pref1.setRedirect(false);
		return pref1;
	}

	/**
	 * doSave
	 */
	public PageReference doSave(){
		pageMessages = getPageMessages();
		Savepoint sp = Database.setSavePoint();
		try{
			//同意確認
			if(!Agree){
				pageMessages.addErrorMessage(MK_Const.ERR_MSG07_IS_NOT_AGREE);
				return null;
			}
			Set<String> emailList = E_MK_leadTMPDao.getEmailListByCmpgnTrgt(campaignTarget.id);
			system.debug('emailList=' + emailList);
			for(MK_leadTMP__c friend:insfriends){
				//同一メールアドレス有無チェック
				if(emailList.contains(friend.Email__c)){
					pageMessages.addErrorMessage(MK_Const.ERR_MSG06_IS_NOT_SAME_EMAIL);
					return null;
				}
				emailList.add(friend.Email__c);
			}
			if(E_MK_leadTMPDao.getRecCountByCmpgnTrgt(campaignTarget.id) + insfriends.size()
					> integer.valueOf(system.label.E_MK_BATON_FRIEND_REGIST_MAX)){
				system.debug('■[doSaveErr2]');
				pageMessages.addErrorMessage(String.format(MK_Const.ERR_MSG08_MAX_BATON, new String[]{system.label.E_MK_BATON_FRIEND_REGIST_MAX}));
				return null;
			}
			//リードTMPの登録
			system.debug('■[doSave1]' + insfriends);
			//ロールバック時にIDが付与されるのを避けるため、クローンしたものをコミットする
			List<MK_leadTMP__c> commitFriends = insfriends.clone();

			insert commitFriends;
			update campaignTarget;

			//URL作成
			for(MK_leadTMP__c friend:commitFriends){
				friend.DomainURL__c = Site.getBaseUrl() + '/MK_FriendDetailRegist?request=' + EncodingUtil.urlEncode(E_EncryptUtil.getEncryptedString(friend.Id), 'UTF-8');
			}

			update commitFriends;
		} catch(Exception e){
			Database.rollback(sp);
			system.debug('■[doSave2]' + e.getMessage());
			return null;
		}
		system.debug('■[doSave3]');
		if(pageMessages.hasMessages()) return null;
		system.debug('■[doSave4]');
		PageReference pref = Page.MK_ThanksRegist;
		pref.setRedirect(true);
		return pref;
	}
	/*
	 * @param 	s 基準文字列
	 * @return	照合、記号をtrim 全角へ統一
	 */
	private String  clnsngStr(String s){
		return E_Util.trimSign(E_Util.trimSymbolKana(E_Util.trimSymbol(E_Util.enToEm(s))));
	}
}