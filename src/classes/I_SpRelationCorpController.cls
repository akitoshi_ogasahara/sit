public with sharing class I_SpRelationCorpController extends I_AbstractSalesResultsController{

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// リスト行数
	public Integer rowCount {get; set;}

// 特定関係法人一覧
	// 特定関係法人リスト
	public List<CorpListRow> corpRows {get;private set;}
	//営業日
	public String businessDate {get;private set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	// ダウンロード
	public String agency{get; set;}

	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}

	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE			= 'st';
	// URLパラメータ名 - フィルタID
	public static final String URL_PARAM_FILTER_ID			= 'id';
	//ソートキー
	public static final String SORT_TYPE_CORPNAME			= 'corpName';
	public static final String SORT_TYPE_CORPRELATION		= 'corpRelation';
	public static final String SORT_TYPE_CORPCODE			= 'corpCode';


	//コンストラクタ
	public I_SpRelationCorpController() {
		pageMessages = getPageMessages();

		// デフォルトソート - 関係コード・昇順
		sortType = SORT_TYPE_CORPCODE;
		sortIsAsc = true;
		agency = '';

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
		}
	}

	//メニューキー
	protected override String getLinkMenuKey() {
		return 'agencyList';
	}

	//初期処理
	public override pageReference init() {
		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}
		getCorpList();
		// ソート
		I_SpRelationCorpController.list_sortType  = sortType;
		I_SpRelationCorpController.list_sortIsAsc = sortIsAsc;
		corpRows.sort();
		return null;
	}



	//特定関係法人取得
	private void getCorpList(){
		//営業日
		String bizStr = getBizDate();
		businessDate = bizStr.subString(0,4) + '/' + bizStr.subString(4,6) + '/' + bizStr.subString(6,8);

		//特定関係法人表示用リスト
		corpRows = new List<CorpListRow>();

		//URLパラメータ受取
		agency = apexPages.currentPage().getParameters().get('agency');

		//取得用リスト
		//List<E_AgencySalesResults__c> salesResults = [Select BusDate__c,(Select SpRelationCorpName__c,Relation__c,AGCO_AGNTREL__c From E_SpRelationCorps__r ORDER BY AGCO_AGNTREL__c asc ,AGCO_CLNTNUM__c asc) From E_AgencySalesResults__c Where ParentAccountId__c =: agency AND Hierarchy__c = 'AH' AND BusinessDate__c =: getBizDate() limit 1];
		List<E_AgencySalesResults__c> salesResults = E_AgencySalesResultsDaoWithout.getSpRelationCorp(agency, getBizDate());

		//0件の時エラー
		if(salesResults.isEmpty() || salesResults[0].E_SpRelationCorps__r.isEmpty()){
			//pageMessages.addWarningMessage(getMSG().get('LIS|002'));
			pageMessages.addWarningMessage(getMSG().get('ISS|005'));
			return;
		}
		E_AgencySalesResults__c salesResult = salesResults[0];
		//最大件数以上の場合は警告を表示
		if(salesResult.E_SpRelationCorps__r.size() >= I_Const.LIST_MAX_ROWS){
			pageMessages.addWarningMessage(getMSG().get('LIS|004'));
		}

		for(E_SpRelationCorp__c corpRec : salesResult.E_SpRelationCorps__r){
			CorpListRow row = new CorpListRow();

			row.corpName 	 = corpRec.SpRelationCorpName__c;
			row.corpRelation = corpRec.Relation__c;
			row.corpCode 	 = corpRec.AGCO_AGNTREL__c;

			corpRows.add(row);

			if(corpRows.size() >= 1000){
				break;
			}
		}
	}

	/**
	 * ソート
	 */
	public PageReference sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}

		I_SpRelationCorpController.list_sortType  = sortType;
		I_SpRelationCorpController.list_sortIsAsc = sortIsAsc;
		corpRows.sort();

		return null;
	}


	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}


	//総行数取得
	public Integer getTotalRows(){
		Integer totalRow = 0;

		totalRow = corpRows  == null ? 0 : corpRows.size();
		return totalRow;
	}

	//CSVエクスポート
	//代理店格に対応する代理店挙績情報に関連する特定関係法人
	//public PageReference ExportCSV(){
	//	PageReference pr = Page.IRIS_ExportSpRelationCorp;

	//	pr.getParameters().put('agency',agency);
	//	return pr;

	//}


	/**
	 * 内部クラス
	 */
	public class CorpListRow implements Comparable {
		//特定関係法人名
		public String corpName {get; set;}
		//関係
		public String corpRelation {get; set;}
		//関係コード
		public String corpCode {get; set;}

		//コンストラクタ
		public CorpListRow(){
			corpCode 		= '';
			corpRelation 	= '';
			corpCode 		= '';
		}

		//ソート
		public Integer compareTo(Object compareTo) {
			CorpListRow compareToRow = (CorpListRow)compareTo;
			Integer result = 0;

			//特定関係法人名
			if(list_sortType == SORT_TYPE_CORPNAME){
				result = I_Util.compareToString(corpName, compareToRow.corpName);
			//関係
			}else if(list_sortType == SORT_TYPE_CORPRELATION){
				result = I_Util.compareToString(corpRelation, compareToRow.corpRelation);
			//関係コード(デフォルトソート)
			}else if(list_sortType == SORT_TYPE_CORPRELATION){
				result = I_Util.compareToString(corpCode, compareToRow.corpCode);
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}

}