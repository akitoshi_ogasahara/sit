public with sharing class E_EmployeeSalesResultsDao {

	/**
	 * 社内挙積情報IDをキーに、指定した社内挙積情報を取得する。
	 * @param id: 社内挙積情報ID
	 * @return E_EmployeeSalesResults__c: 社内挙積情報
	 */
	public static E_EmployeeSalesResults__c getRecById(Id id) {
		List<E_EmployeeSalesResults__c> recs = [
				Select
					Id								// カスタムオブジェクト ID
//					,OwnerId						// 所有者 ID
//					,IsDeleted						// 削除
//					,Name							// 社内挙積情報名
//					,CreatedDate					// 作成日
//					,CreatedById					// 作成者 ID
//					,LastModifiedDate				// 最終更新日
//					,LastModifiedById				// 最終更新者 ID
//					,SystemModstamp					// System Modstamp
					,MRCode__c						// MRコード
					,E_Unit__c						// 営業部
					,SectionCode__c					// 課コード
					,Hierarchy__c					// 階層
					,YYYYMM__c						// 年月
					,MR__c							// MR
					,AchiveRate_FS__c				// 達成率 FS
					,Budget_FS__c					// 予算 FS
					,NBWANP_FS__c					// 新契約換算ANP　FS
					,AchiveRate_Protection__c		// 達成率 Protection
					,Budget_Protection__c			// 予算 Protection
					,NBWANP_Protection__c			// 新契約換算ANP　Protection
					,AchiveRate_BACOLI__c			// 達成率 BA-COLI
					,Budget_BACOLI__c				// 予算 BA-COLI
					,NBWANP_BACOLI__c				// 新契約換算ANP　BA-COLI
					,AchiveRate_Total__c			// 達成率 Total
					,Budget_Total__c				// 予算 Total
					,NBWANP_Total__c				// 新契約換算ANP　Total
					,AchiveRate_FSYTD__c			// 達成率（YTD） FS
					,Budget_FSYTD__c				// 予算（YTD） FS
					,NBWANP_FSYTD__c				// 新契約換算ANP（YTD）　FS
					,AchiveRate_ProtectionYTD__c	// 達成率（YTD） Protection
					,Budget_ProtectionYTD__c		// 予算（YTD） Protection
					,NBWANP_ProtectionYTD__c		// 新契約換算ANP（YTD）　Protection
					,AchiveRate_BACOLIYTD__c		// 達成率（YTD） BA-COLI
					,Budget_BACOLIYTD__c			// 予算（YTD） BA-COLI
					,NBWANP_BACOLIYTD__c			// 新契約換算ANP（YTD）　BA-COLI
					,AchiveRate_TotalYTD__c			// 達成率（YTD） Total
					,Budget_TotalYTD__c				// 予算（YTD） Total
					,NBWANP_TotalYTD__c				// 新契約換算ANP（YTD）　Total
					,ActiveAgency__c				// 稼動事務所数
					,ActiveAgent__c					// 稼働募集人数
					,IQA__c							// IQA継続率
					,IQA_Numerator__c				// IQA継続率 分子
					,IQA_Denominator__c				// IQA継続率 分母
					,MOF13__c						// MOF13継続率
					,MOF13_Numerator__c				// MOF13継続率 分子
					,MOF13_Denominator__c			// MOF13継続率 分母
					,MOF25__c						// MOF25継続率
					,MOF25_Numerator__c				// MOF25継続率 分子
					,MOF25_Denominator__c			// MOF25継続率 分母
					,DefactRate__c					// 単純不備率
					,DefactRate_Numerator__c		// 単純不備率 単純不備数
					,DefactRate_Denominator__c		// 単純不備率 総数
					,IQA_NNAverage__c 				// IQA継続率（全社平均）
					,MOF13_NNAverage__c 			// MOF13継続率（全社平均）
					,MOF25_NNAverage__c 			// MOF25継続率（全社平均）
					,DefactRate_NNAverage__c 		// 単純不備率（全社平均）
				From
					E_EmployeeSalesResults__c
				Where
					Id = :id
			   ];
		return (recs.size() > 0) ? recs[0] : new E_EmployeeSalesResults__c();
	}

}