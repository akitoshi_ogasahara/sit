/**
 * ユニットプライス用抽象コントローラクラス.
 */
public abstract class E_AbstractUnitPriceController {

	public E_PageMessagesHolder getPageMessages(){
			return E_PageMessagesHolder.getInstance();
	}
	
	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}
	
	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}
	
	/**
	 * 照会日時取得（文字列）
	 */
	public String getDataSyncDate() {
		date d = getDataSyncDate_DateType();
		return d == null? '': DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0)).format('yyyy/MM/dd');
	}
	
	/**
	 * 照会日時取得（日付）
	 */
	public Date getDataSyncDate_DateType() {
		E_BizDataSyncLog__c ebizlog = E_bizDataSyncLogDao.getNotBlankEndDateByLast1_forUp();
		return ebizlog == null? null:  ebizlog.InquiryDate__c;
	}
	
	/**
	 * ページタイトル
	 */
	public String pgTitle; 					//サブクラスにて設定してください。
	public String getPageTitle() {
		String sTitle = '';					//['+ E_Const.SYSTEM_NAME +'] -  SitesはNNLinkを付けない
		if(String.isNotBlank(pgTitle)){
			sTitle += pgTitle;
		}
		return sTitle;
	}
	
	/**
	 * 商品が『バリアブルアニュイティ』かを判定
	 */
	public Boolean isValiableAnnuity(String prdCode) {
        return prdCode.equals(E_Const.UP_PRDCD_VALIABLE);
	}
    
	/**
	 * ターゲット型の判定
	 */
	public Boolean isTarget(String prdCode) {
        return prdCode.equals(E_Const.UP_PRDCD_DIRECT);
	}
	
	/**
	 * HELPページへ遷移
	 */
    public PageReference openHELP(){
    	return Page.up007;
    }
    
    /**
     *　データ連携中チェック
     */
     /*20150610除外
	public PageReference isServiceHourMenu() {
		List<String> menus = System.Label.E_SERVICE_HOUR_MENU.split(',', 0);
		for (String value : menus) {
			if (value.trim().equals(E_Const.ID_KIND.UNITPRICE.name())) {
				if (!E_Util.isSyncEndData()){
					PageReference pageRef = Page.up999;
					pageRef.getParameters().put('code', E_Const.ERROR_MSG_SERVICEHOUR);
					return pageRef;
				}
			}
		}
		return null;
	}
	*/
}