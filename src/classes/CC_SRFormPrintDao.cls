public with sharing class CC_SRFormPrintDao {

	/**
	 * ケース取得
	 * @param srid: Id
	 * @return Case: Case(SR)情報
	 */
	public static Case getCaseById(Id srid) {
		return [select id,
						CC_IsCoverLetterOutput__c,
						CC_CoverLetter__c,
						CC_IsInvoiceOutput__c,
						CC_IsConfirmationFormOutput__c,
						CC_SRTypeId__r.Name
					from
						Case
					where Id = :srid
				];
	}

	/**
	 * 帳票マスタデータ取得
	 * @return List<CC_FormMaster__c>: 帳票情報一覧
	 */
	public static List<CC_FormMaster__c> getFormMasters() {
		return      [
		select Id
				, CC_FormNo__c
				, CC_FormName__c
				, CC_FormType__c
				, CC_SRTypeNo__c
				, CC_SvfFormButtonName__c   // SVFのボタン名を取得
			FROM CC_FormMaster__c
			where CC_IsNotValid__c = false
		];
	}

	/**
	 * Caseに紐づくSRPolicyを取得
	 * @param caseId CaseID
	 * @return List<CC_SRPolicy__c>
	 */
	public static List<CC_SRPolicy__c> getCCSRPolicyByCaseId(Id caseId) {
		//条件不備の場合には空を返す
		if(caseId == null){
			return new List<CC_SRPolicy__c>();
		}
		return [SELECT  id,
					CC_PolicyID1__r.RecordType.Name,
					CC_PolicyID1__r.COMM_CHDRNUM__c,
					CC_PolicyID1__r.COMM_ZINSNAM__c,
					CC_PolicyID1__r.InsuredName__c,
					CC_PolicyID1__r.COMM_SINSTAMT__c,
					CC_PolicyID1__r.DCOLI_SUMINS__c,
					CC_PolicyID1__r.COLI_ZEPLPRN__c,
					CC_PolicyID1__c,

					CC_PolicyID2__r.RecordType.Name,
					CC_PolicyID2__r.COMM_CHDRNUM__c,
					CC_PolicyID2__r.COMM_ZINSNAM__c,
					CC_PolicyID2__r.InsuredName__c,
					CC_PolicyID2__r.COMM_SINSTAMT__c,
					CC_PolicyID2__r.DCOLI_SUMINS__c,
					CC_PolicyID2__r.COLI_ZEPLPRN__c,
					CC_PolicyID2__c,

					CC_PolicyID3__r.RecordType.Name,
					CC_PolicyID3__r.COMM_CHDRNUM__c,
					CC_PolicyID3__r.COMM_ZINSNAM__c,
					CC_PolicyID3__r.InsuredName__c,
					CC_PolicyID3__r.COMM_SINSTAMT__c,
					CC_PolicyID3__r.DCOLI_SUMINS__c,
					CC_PolicyID3__r.COLI_ZEPLPRN__c,
					CC_PolicyID3__c,

					CC_PolicyID4__r.RecordType.Name,
					CC_PolicyID4__r.COMM_CHDRNUM__c,
					CC_PolicyID4__r.COMM_ZINSNAM__c,
					CC_PolicyID4__r.InsuredName__c,
					CC_PolicyID4__r.COMM_SINSTAMT__c,
					CC_PolicyID4__r.DCOLI_SUMINS__c,
					CC_PolicyID4__r.COLI_ZEPLPRN__c,
					CC_PolicyID4__c,

					CC_FormMaster1__r.CC_FormType__c,
					CC_FormMaster1__r.CC_FormNo__c,
					CC_FormMaster1__r.CC_FormName__c,
					CC_FormMaster1__r.CC_SRTypeNo__c,
					CC_FormMaster1__r.CC_CorporatePersonClassification__c,
					CC_FormMaster1__c,
					CC_FormMaster2__r.CC_FormType__c,
					CC_FormMaster2__r.CC_FormNo__c,
					CC_FormMaster2__r.CC_FormName__c,
					CC_FormMaster2__r.CC_SRTypeNo__c,
					CC_FormMaster2__r.CC_CorporatePersonClassification__c,
					CC_FormMaster2__c,

					CC_CaseId__r.CC_IsConfirmationFormOutput__c,

					CC_CaseId__r.CC_IsCoverLetterOutput__c,
					CC_CaseId__r.CC_CoverLetter__c,
					CC_CaseId__r.CC_IsInvoiceOutput__c,

					CC_SYSNumberOfPolicies__c
				FROM
					CC_SRPolicy__c
				WHERE
					CC_CaseId__c = :caseId
				ORDER BY CreatedDate ASC
				];
	}

	/**
	 * SR契約連結の契約1～4を集約し、紐づく特約の一覧を取得
	 * @param policyIds E_PolicyIDのList
	 * @return List<E_COVPF__c>
	 */
	public static List<E_COVPF__c> getECovpfByPolicyIds(List<Id> policyIds) {
		//条件不備の場合には空を返す
		if(policyIds.size() == 0){
			return new List<E_COVPF__c>();
		}
		return [select Id,
					Name,
					E_Policy__c,
					COLI_ZCOVRNAMES__c
				from
					E_COVPF__c
				where
					E_Policy__c in : policyIds
				order by
					E_Policy__c
		];
	}

	/**
	 * 帳票情報取得
	 * @param srId, formTypeConditionList(手続き分類)
	 * @return List<CC_FormDetail__c>
	 */
	public static List<CC_FormDetail__c> getFormDetailBySRFormtypeForDel(Id srId, List<String> formTypeConditionList) {

		return [select
					id
				from
					CC_FormDetail__c
				where
					CC_Case__c = :srId
					and CC_FormMasterId__r.CC_FormType__c IN :formTypeConditionList
		];

	}

}