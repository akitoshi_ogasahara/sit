public without sharing class E_AccountDaoWithout {

	public static List<Account> getRecsByParentId(Id accountId){
		return [
			Select
				Id
				, Name                   //代理店格名/代理店事務所名
				, E_CL2PF_ZAGCYNUM__c    //代理店事務所コード
				, E_CL2PF_BRANCH__c      //支社コード
			From
				Account
			Where
				ParentId = :accountId
			And
				(E_COMM_VALIDFLAG__c = '1' OR E_COMM_VALIDFLAG__c = '2')
			Order by E_CL2PF_ZAGCYNUM__c
		];
	}

	public static List<Account> getRecsByParentIdIsActive(Id accountId){
		return [
			Select
				Id
				, Name                   //代理店格名/代理店事務所名
				, E_CL2PF_ZAGCYNUM__c    //代理店事務所コード
				, E_CL2PF_BRANCH__c      //支社コード
			From
				Account
			Where
				ParentId = :accountId
			And
				E_COMM_VALIDFLAG__c = '1'
			Order by E_CL2PF_ZAGCYNUM__c
		];
	}

	/**
	 * 代理店事務所レコードを取得（MRが自支社担当事務所のみを取得する）
			@param      parentId    代理店格AccountId
						brcd        支店コード(2桁 BRがつかない)
						validflgs   有効フラグ
	 */
	public static List<Account> getOfficesByParentId_BRCD(Id parentId, String brcd, Set<String> validFlgs){
		String soql = 'SELECT Id, Name ';
		soql += ' ,E_CL2PF_ZAGCYNUM__c';
		soql += ' ,E_CL2PF_BRANCH__c';
		soql += ' FROM Account';
		soql += ' WHERE ParentId = \'' + parentId + '\'';

		if(brcd==null){
			brcd = '';
		}
		//支社コードの設定(BR**以外の場合)
		if(brcd != E_Const.USER_ACCESS_KIND_BRALL){
			brcd = brcd.right(2);       //みぎ桁が支社コード
			soql += ' AND (E_CL2PF_BRANCH__c = \'' + brcd + '\'';
			soql += ' OR parent.E_CL1PF_ZBUSBR__c = \'' + brcd + '\')';

		}

		//有効フラグの設定
		List<String> soql_validflg = new List<String>();
		for(String flg:validflgs){
			soql_validflg.add(' E_COMM_VALIDFLAG__c = \'' + flg + '\'');
		}
		if(!soql_validflg.isEmpty()){
			soql += ' AND (' + String.join(soql_validflg, ' OR ') + ')';
		}

		soql += ' Order by E_CL2PF_ZAGCYNUM__c';
		return Database.query(soql);
	}

// IRIS
	/**
	 * 事務所Accountレコードを取得
	 */
	public static List<Account> getRecsByAgency(String id, String brunchCode, Boolean isAllOffice, String sortField, Boolean sortIsAsc) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL2PF_ZAGCYNUM__c'
					+ ', E_COMM_ZCLADDR__c'
					+ ', CLTPHONE01__c'
					+ ', AGNTBR_NM__c'
					+ ', Owner.Name'
					+ ', Parent.Name '
					+ ', isMotherOffice__c '
					//挙積対応 20170804追加 Start
					+ ', E_CL2PF_BRANCH__c '
					+ ', OwnerId '
					//挙積対応 20170804追加 End
					+ ', isMOffice__c ' //挙績対応 20170816
					+ ', E_CL1PF_ZBUSBR__c '    //挙績対応 20170907
					+ ', zubsMain__c '  //挙績対応 20170907
					+ 'FROM Account '
					+ 'WHERE Parent.Id = :id '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';

		if(!isAllOffice){
			soql += 'AND E_CL2PF_BRANCH__c = :brunchCode';
		}

		if (String.isNotBlank(sortField)) {
			soql += ' ORDER BY ' + sortField;

			if (!sortIsAsc) {
				soql += ' DESC NULLS LAST';
			}
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	/**
	 * 事務所Accountレコードと挙績情報を取得
	 */
	public static List<Account> getRecsByAgency(String brunchCode, Boolean isAllOffice, String sortField, Boolean sortIsAsc, String wheresoql) {
		String businessDate = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL2PF_ZAGCYNUM__c'
					+ ', E_COMM_ZCLADDR__c'
					+ ', CLTPHONE01__c'
					+ ', AGNTBR_NM__c'
					+ ', Owner.Name'
					+ ', Parent.Name '
					+ ', isMotherOffice__c '
					+ ', E_CL2PF_BRANCH__c '
					+ ', OwnerId '
					+ ', isMOffice__c '
					+ ', E_CL1PF_ZBUSBR__c '
					+ ', zubsMain__c '
					+ ', (SELECT'
					+ '     Id'
					+ '     , NBCANP_YTD__c'
					+ '     , NBCANP_SpInsTypeYTD__c'
					+ '     , IANP__c'
					+ '     , IQA__c'
					+ '     , NBWANP_TotalYTD__c'
					+ '     , NBWANP_FSYTD__c'
					+ '     , NBWANP_ProtectionYTD__c'
					+ '     , ActiveAgent__c'
					+ '   FROM E_AgencySalesResults__r'
					+ '   WHERE BusinessDate__c =: businessDate'
					+ '     AND Hierarchy__c = \'' + E_Const.ZIDTYPE_AY + '\''
					+ '   LIMIT 1 ) '
					+ 'FROM Account '
					+ 'WHERE E_COMM_VALIDFLAG__c = \'1\' '
					+ wheresoql;

		if(!isAllOffice){
			  soql += ' AND E_CL2PF_BRANCH__c = :brunchCode';
		}

		if (String.isNotBlank(sortField)) {
			  soql += ' ORDER BY ' + sortField;

			if (!sortIsAsc) {
			  soql += ' DESC NULLS LAST';
			}
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	public static Account getRecById(String accountId){
		Account rec = null;
		List<Account> recs = [
			Select
				Id
				, Name                      //代理店格名/代理店事務所名
				, ParentId                  //Id（親取引先）
				, Parent.Name               //代理店格名（親取引先）
				, E_ParentName__c           //代理店格名（親取引先）※親の参照権限に依存しない
				, Parent.E_CL1PF_ZHEADAY__c //代理店格コード（親取引先）
				, E_CL2PF_ZAGCYNUM__c       //代理店事務所コード
				, E_CL1PF_ZHEADAY__c        //代理店格コード
				, ZHEADAY__c                //代理店番号
				, E_ParentZHEADAY__c        //代理店格コード（数式）
				, E_COMM_ZCLADDR__c         //漢字住所
				, CLTPHONE01__c             //電話番号
				, AGNTBR_NM__c              //担当拠点
				, isMotherOffice__c         //母店担当フラグ
				, Owner.Name                //所有者.Name
				, E_CL1PF_ZAHKNAME__c       //代理店格カナ名--スプリントバックログ9(2017/1/18)追記
				, Z1OFFING__c               //一事務所登録フラグ
			From
				Account
			Where
				Id = :accountId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	// IRIS Day2
	/**
	 * 代理店検索（検索ボックス）
	 */
	public static List<Account> getRecsIRISAgencySearchBoxAgency(String soqlWhere, String keyword){
		// キーワード分割
		List<String> partialmatch = new List<String>();
		List<String> forwardmatch = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			partialmatch.add('%' + String.escapeSingleQuotes(text) + '%');
			forwardmatch.add(String.escapeSingleQuotes(text) + '%');
		}

		List<Account> result = new List<Account>();
		String soql = 'SELECT Id '
					+ ', Parent.Id '
					+ ', E_ParentName__c '
					+ ', E_ParentZHEADAY__c '
					+ ', Parent.E_CL1PF_ZAHKNAME__c '
					+ ', Parent.E_COMM_ZCLADDR__c '
					+ ', Parent.E_CL1PF_ZHEADAY__c '    //20180214 資料発送申込対応
					+ 'FROM Account '
					+ 'WHERE Parent.Id != null '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' '
					+ 'AND (E_ParentName__c like :partialmatch OR Parent.E_CL1PF_ZAHKNAME__c like :partialmatch '
					+ 'OR E_ParentZHEADAY__c like :forwardmatch) '
					+ soqlWhere;
		system.debug(soql);
		result = Database.query(soql);

		return result;
	}

	// IRIS Day2
	/**
	 * 代理店検索（検索ボックス）
	 */
	//IRISエンハンス IRIS-753 ここから
	public static List<Account> getRecsIRISAgencySearchBoxAgency(String soqlWhere, String keyword,String selection){
		String soqlkeyword = '';
		// キーワード分割
		List<String> partialmatch = new List<String>();
		List<String> forwardmatch = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			partialmatch.add('%' + String.escapeSingleQuotes(text) + '%');
			forwardmatch.add(String.escapeSingleQuotes(text) + '%');
		}

		//代理店格名（親取引先名）項目 検索条件文
		String nameKeywords = '';
		nameKeywords = E_Util.createAndSearchQuery(keyword,'E_ParentName__c');

		//代理店格カナ名項目 検索条件文
		String kanaKeywords = '';
		kanaKeywords = E_Util.createAndSearchQuery(keyword,'Parent.E_CL1PF_ZAHKNAME__c');

		//代理店格コード（数式）項目 検索条件文
		String zheadayKeywords = '';
		for(String text : keyword.replaceAll('　', ' ').split(' ')){
			//where句追記初回
			if(String.isBlank(zheadayKeywords)){
				zheadayKeywords = 'E_ParentZHEADAY__c like ' + '\'%' + text.replaceAll('(%|_)', '\\\\$1') + '%\'';
			}else{
				zheadayKeywords = zheadayKeywords + ' AND E_ParentZHEADAY__c like ' + '\'%' + text.replaceAll('(%|_)', '\\\\$1') + '%\'';
			}
		}
		//AND検索
		if(selection == 'and'){
			soqlkeyword = 'AND (((' + nameKeywords + ') OR (' + kanaKeywords + ')) OR (' + zheadayKeywords + '))';
		//OR検索
		}else{
			soqlkeyword = 'AND (E_ParentName__c like :partialmatch OR Parent.E_CL1PF_ZAHKNAME__c like :partialmatch '
					+ 'OR E_ParentZHEADAY__c like :forwardmatch) ';
		}

		List<Account> result = new List<Account>();
		String soql = 'SELECT Id '
					+ ', Parent.Id '
					+ ', E_ParentName__c '
					+ ', E_ParentZHEADAY__c '
					+ ', Parent.E_CL1PF_ZAHKNAME__c '
					+ ', Parent.E_COMM_ZCLADDR__c '
					+ ', Parent.E_CL1PF_ZHEADAY__c '    //20180214 資料発送申込対応
					+ 'FROM Account '
					+ 'WHERE Parent.Id != null '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' '
					+ soqlkeyword
					+ soqlWhere;
		system.debug(soql);
		result = Database.query(soql);

		return result;
	}
	//IRISエンハンス IRIS-753 ここまで

	/**
	 * 事務所検索（検索ボックス）
	 */
	public static List<Account> getRecsIRISAgencySearchBoxOffice(String soqlWhere, String keyword){
		// キーワード分割
		List<String> partialmatch = new List<String>();
		List<String> forwardmatch = new List<String>();
		if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
			for(String text : I_Util.createSearchkeywordList(keyword)){
				partialmatch.add('%' + String.escapeSingleQuotes(text) + '%');
				forwardmatch.add(String.escapeSingleQuotes(text) + '%');
			}
		}

		List<Account> result = new List<Account>();
		String soql = 'SELECT Id '
					+ ', Name '
					+ ', E_CL2PF_ZEAYKNAM__c '
					+ ', E_CL2PF_ZAGCYNUM__c '
					+ ', E_COMM_ZCLADDR__c '
					+ ', E_ParentName__c '
					+ ', Parent.E_CL1PF_ZAHKNAME__c '
					+ ', E_ParentZHEADAY__c '
					//20180214 資料発送申込対応 START
					+ ', CLTPCODE__c '
					+ ', CLTPHONE01__c '
					+ ', E_CL2PF_ZEAYNAM__c '
					//20180214 資料発送申込対応 END
					+ 'FROM Account '
					+ 'WHERE Parent.Id != null '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';
			if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
			  soql += 'AND (Name like :partialmatch OR E_CL2PF_ZEAYKNAM__c like :partialmatch '
					+ 'OR E_CL2PF_ZAGCYNUM__c like :forwardmatch) ';
			}
			  soql += soqlWhere
					+ 'LIMIT ' + I_Const.LIST_MAX_ROWS;

		// 検索実行
		system.debug(soql);
		result = Database.query(soql);

		return result;
	}

	/**
	 * 事務所検索（検索ボックス）
	 */
	//IRISエンハンス IRIS-752 ここから
	public static List<Account> getRecsIRISAgencySearchBoxOffice(String soqlWhere, String keyword,String selection){
		String soqlkeyword = '';
		// キーワード分割
		List<String> partialmatch = new List<String>();
		List<String> forwardmatch = new List<String>();
		if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
			for(String text : I_Util.createSearchkeywordList(keyword)){
				partialmatch.add('%' + String.escapeSingleQuotes(text) + '%');
				forwardmatch.add(String.escapeSingleQuotes(text) + '%');
			}
		}

		//取引先名）項目 検索条件文
		String nameKeywords = '';
		nameKeywords = E_Util.createAndSearchQuery(keyword,'Name');

		//代理店事務所カナ名項目 検索条件文
		String kanaKeywords = '';
		kanaKeywords = E_Util.createAndSearchQuery(keyword,'E_CL2PF_ZEAYKNAM__c');

		//代理店事務所コード項目 検索条件文
		String zagcynumKeywords = '';
		for(String text : keyword.replaceAll('　', ' ').split(' ')){
			//where句追記初回
			if(String.isBlank(zagcynumKeywords)){
				zagcynumKeywords = 'E_CL2PF_ZAGCYNUM__c like ' + '\'%' + text.replaceAll('(%|_)', '\\\\$1') + '%\'';
			}else{
				zagcynumKeywords = zagcynumKeywords + ' AND E_CL2PF_ZAGCYNUM__c like ' + '\'%' + text.replaceAll('(%|_)', '\\\\$1') + '%\'';
			}
		}
		//AND検索
		if(selection == 'and'){
			soqlkeyword = 'AND (((' + nameKeywords + ') OR (' + kanaKeywords + ')) OR (' + zagcynumKeywords + '))';
		//OR検索
		}else{
			soqlkeyword = 'AND (Name like :partialmatch OR E_CL2PF_ZEAYKNAM__c like :partialmatch '
					+ 'OR E_CL2PF_ZAGCYNUM__c like :forwardmatch) ';
		}

		List<Account> result = new List<Account>();
		String soql = 'SELECT Id '
					+ ', Name '
					+ ', E_CL2PF_ZEAYKNAM__c '
					+ ', E_CL2PF_ZAGCYNUM__c '
					+ ', E_COMM_ZCLADDR__c '
					+ ', E_ParentName__c '
					+ ', Parent.E_CL1PF_ZAHKNAME__c '
					+ ', E_ParentZHEADAY__c '
					//20180214 資料発送申込対応 START
					+ ', CLTPCODE__c '
					+ ', CLTPHONE01__c '
					+ ', E_CL2PF_ZEAYNAM__c '
					//20180214 資料発送申込対応 END
					+ 'FROM Account '
					+ 'WHERE Parent.Id != null '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';
			if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
			  soql += soqlkeyword;
			}
			  soql += soqlWhere
					+ 'LIMIT ' + I_Const.LIST_MAX_ROWS;

		// 検索実行
		system.debug(soql);
		result = Database.query(soql);

		return result;
	}
	//IRISエンハンス IRIS-752 ここまで
	/**
	 * 事務所コードから事務所情報を取得
	 */
	public static Account getRecByAccCode(String accCode){
		List<Account> recs = [Select id,Name From Account Where E_CL2PF_ZAGCYNUM__c =: accCode limit 1];
		if(recs.size() > 0){
			return recs[0];
		}
		return null;
	}

	/**
	 * IPアドレス確認
	 */
	public static Account getParentAcc(Id accid){
		return [
			SELECT
				  Id
				, E_WhiteIPAddress__c
			FROM Account
			WHERE Id = :accid
		];
	}

	 /**
	 * P16-0003 Atria対応開発
	 * 代理店格レコードを取得
	 * ownerIdが入っている場合→ownerIdをキーに自身の所有する代理店事務所のみを取得
	 * BranchCodeが入っている場合→照会者コードをキーに自身所属営業部代理店事務所を取得
	 * どちらも入っていない場合→全代理店格を取得
	 */
	public static List<Account> getRecsIRISAuthorityAgencySearch( String ownerId , String brunchCode, String keyword ){
		// キーワード分割
		List<String> partialmatch = new List<String>();
		List<String> forwardmatch = new List<String>();
		if( keyword !='' ){
			for(String text : I_Util.createSearchkeywordList(keyword)){
				partialmatch.add('%' + String.escapeSingleQuotes(text) + '%');
				forwardmatch.add(String.escapeSingleQuotes(text) + '%');
			}
		}

		List<Account> result = new List<Account>();
		String soql='SELECT Id '
				+ ',KSECTION__c '
				+ ',Parent.Id '
				+ ',Parent.Name '
				+ ',Parent.E_CL1PF_ZHEADAY__c '
				+ ',Parent.E_COMM_ZCLADDR__c '
				+ ',Parent.E_CL1PF_ZAHKNAME__c '
				+ 'FROM Account '
				+ 'WHERE Parent.Id!=null '//事務所レコードのみ対象
				+ 'AND E_COMM_VALIDFLAG__c = \'1\' '; //有効な事務所が紐づいているもののみ対象
		if(keyword != '' ){
			soql += 'AND ( E_ParentName__c  like :partialmatch OR  Parent.E_CL1PF_ZAHKNAME__c like :partialmatch ';
			soql += 'OR E_ParentZHEADAY__c like :forwardmatch ) ';
		}
		if( ownerId != '' ){
			soql += ' AND Owner.Id = :ownerId ';
			//事務所の課コードが88の場合には取得しない
			soql += ' AND KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\'';
		}
		if ( brunchCode != '') {
			soql += ' AND E_CL2PF_BRANCH__c = :brunchCode ';
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS_AGGREGATE;

		return Database.query(soql);

	}

	/**
	 * P16-0003 Atria対応開発
	 * 代理店格コードを受取り、代理店格Idを返す、
	 */
	public static Account getParentRecByAccCode(String accCode){
		List<Account> recs = [Select id From Account Where E_CL1PF_ZHEADAY__c =: accCode AND E_IsAgency__c = true limit 1];
		if(recs.size() > 0){
			return recs[0];
		}
		return null;
	}

	/**
	 * P16-0003 Atria対応開発
	 * 代理店格Idによって取得できる代理店事務所件数のカウントを返す
	 */
	public static Integer getRecsByAgencyCount( String agencyId ){
		return [Select Count() from Account Where Parent.Id = : agencyId AND E_COMM_VALIDFLAG__c ='1' ];
	}

	/**
	 * P16-0003 Atria対応開発
	 * 代理店格Idによって代理店事務所を取得する。コードが渡されている場合、コードと一致するものを設定する
	 */
	public static List<Account> getRecsByAgency_Atria(String id, String code, String sortField, Boolean sortIsAsc) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL2PF_ZAGCYNUM__c'
					+ ', E_COMM_ZCLADDR__c'
					+ ', CLTPHONE01__c'
					+ ', AGNTBR_NM__c'
					+ ', Owner.Name'
					+ ', Parent.Name '
					+ ', isMotherOffice__c '
					+ ', Parent.Z1OFFING__c '
					+ ', AccountNameKANA__c '
					+ ', E_CL2PF_BRANCH__c '
					+ 'FROM Account '
					+ 'WHERE Parent.Id = :id '
					+ 'AND E_COMM_VALIDFLAG__c = \'1\' ';
		if(String.isNotBlank(code)){
			soql += 'AND ( E_CL2PF_BRANCH__c = :code ';
			soql += 'OR E_CL2PF_ZBUSBR__c = :code)';
		}

		if (String.isNotBlank(sortField)) {
			soql += ' ORDER BY ' + sortField;
			if (!sortIsAsc) {
				soql += ' DESC NULLS LAST';
			}
		}

		soql += ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	/**
	 * 所有者プロファイルId取得
	 */
	public static Account getOwner(Id accid){
		return [
			SELECT
				  Id
				  ,OwnerId
				  ,Owner.ProfileId
			FROM Account
			WHERE Id = :accid
			LIMIT 1
		];
	}
	
}