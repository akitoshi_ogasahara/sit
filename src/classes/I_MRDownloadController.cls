public with sharing class I_MRDownloadController extends I_AbstractController{
	// MenuKey
	protected override String getLinkMenuKey(){
		return MenuMap.get(dlFormNo);
	}
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	//MR選択フラグ
	public Boolean isSelectMR {get;private set;}
	//エリアリスト
	public List<AreaCheckBox> areaList {get; set;}
	//MRリスト
	public List<MRCheckBox> mrList {get; set;}
	//ダウンロード履歴
	public E_DownloadHistorry__c dlhistory {get;set;}
	//ダウンロード種別
	public String dlFormNo {
		get{
			if(dlFormNo==null){
				try{
					dlFormNo = ApexPages.currentPage().getParameters().get('dNo');					
				}catch(Exception e){

				}
				if(String.isBlank(dlFormNo)) dlFormNo = defaltDlFormNo;
			}
			return dlFormNo;
		}private set;
	}
	/**
	 * 契約の主担当リスト
	 */
	public Boolean isUnit{get;set;}
	public Boolean isReal{get;set;}
	public Boolean isHaed{get;set;}

	//ページタイトル
	public String getPageName(){
		return '営業部・MR毎 帳票ダウンロード';
	}

	/**
	 * 初期表示切替
	 */
	public List<E_Area__c> getRec( List<String> conditions ){
		String soqlFormat = 'Select Name ,(Select Name ,BRANCH__c From E_Units__r Where IsActive__c = \'\'1\'\'{0} Order by DisplayOrder__c Nulls Last,BRANCH__c Nulls Last) '
					+ 'From E_Area__c '
					+ 'Where IsActive__c = \'\'1\'\'{1} Order by SortNo__c';
		String query = String.format(soqlFormat, conditions);

		return Database.query(query);
	}
	/**
	 * ダウンロード期間(AGPO)
	 */
	public List<SelectOption> getAGPOPeriod(){
		return E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO, false);
	}
	/**
	 * ダウンロード期間(BILA)
	 */
	public List<SelectOption> getBILAPeriod(){
		return E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA, false);
	}
	/**
	 * ダウンロード期間(BILS)
	 */
	public List<SelectOption> getBILSPeriod(){
		return E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS, false);
	}
	/**
	 * ダウンロード期間(NCOL)
	 */
	public List<SelectOption> getNCOLPeriod(){
		return E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL, false);
	}
	//OPRO呼び出しURL生成
	public string getDownloadId(){
		if(dlhistory == null || dlhistory.Id == null){
			return '';
		}
		return dlhistory.Id;
	}

	//選択された支社コード
	private List<String> selectedUnits;
	//選択された営業部名
	private String selectedUnitName;
	//選択されたMR
	private List<User> selectedMRs;
	//選択された期間
	private String periodValue;
	//デフォルト値
	private String defaltDlFormNo = E_DownloadNoticeConst.DH_FORMNO_NOTICE_NCOL;
	//帳票種別 => メニューKey
	private Map<String,String> MenuMap = new Map<String,String>{
		E_DownloadNoticeConst.DH_FORMNO_NOTICE_NCOL => I_NoticeConst.MNKEY_NCOL
		,E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA => I_NoticeConst.MNKEY_BILA
		,E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILS => I_NoticeConst.MNKEY_BILS
		,E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO => I_NoticeConst.MNKEY_AGPO
	};

	//コンストラクタ
	public I_MRDownloadController(){
		super();
		pageMessages = getPageMessages();
		isSelectMR = false;
		selectedUnits = new List<String>();
		areaList = new List<AreaCheckBox>();
		isUnit = access.isAuthMR() || access.isAuthManager() || access.isAreaManager();
		isReal = false;
		isHaed = false;
		selectedUnitName = '';
		try{
			dlFormNo = ApexPages.currentPage().getParameters().get('dNo');
		}catch(Exception e){
		}
		if(String.isBlank(dlFormNo)) dlFormNo = defaltDlFormNo;
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		try{
			areaList = createAreaList();
			pageAccessLog.Name = getPageName();
			if(access.isAuthMR() || access.isAuthManager()){
				doChangeDisplay();
			}
		}catch(MRDLException e){
			pageMessages.addWarningMessage(e.getMessage());
		}catch(Exception e){
			pageMessages.addWarningMessage('[システムエラー ]'+e.getMessage());
		}
		if(pr==null){

		}

		return pr;
	}

	/**
	 * DLアクション
	 */
	public PageReference doDL() {
		pageMessages.clearMessages();
		if(!isUnit && !isReal && !isHaed){
			pageMessages.addWarningMessage(getMSG().get('MRDL|001'));
			return null;
		}
		try{
			dlFormNo = ApexPages.currentPage().getParameters().get('dNo');
			dlhistory = new E_DownloadHistorry__c();

			dlhistory.FormNo__c = dlFormNo;
			dlhistory.Conditions__c = createDLCondition().toJSON();
			dlhistory.outputType__c = E_Const.DH_OUTPUTTYPE_CSV;
			insert dlhistory;
		}catch(MRDLException e){
			pageMessages.addWarningMessage(e.getMessage());
		}catch(Exception e){
			pageMessages.addWarningMessage('[システムエラー ]'+e.getMessage());
		}

		return null;
	}
	
	/**
	 * 拠点選択⇔MR選択
	 */
	 public PageReference doChangeDisplay(){
	 	pageMessages.clearMessages();
	 	try{
		 	//MR選択モードでなければMR選択リスト作成
		 	if(!isSelectMR){
		 		createSelectedUnits();
				if(selectedUnits.size()!=1){
					pageMessages.addWarningMessage(getMSG().get('MRDL|002'));
					return null;
				}
				mrList = new List<MRCheckBox>();
				for(User user :[Select Name,Unit__c From User Where Unit__c =: selectedUnitName AND IsActive = true Order By Profile.Name desc ,MemberNo__c]){
					MRCheckBox mRCheckBox = new MRCheckBox(user);
					mrList.add(mRCheckBox);
				}
				if(mrList.isEmpty()){
					throw new MRDLException(getMSG().get('MRDL|003'));
				}
			}
		 	//フラグ反転
		 	isSelectMR = !isSelectMR;
		}catch(MRDLException e){
			pageMessages.addWarningMessage(e.getMessage());
		}catch(Exception e){
			pageMessages.addWarningMessage('[システムエラー ]'+e.getMessage());
		}

	 	return null;
	 }

	/**
	 * Areaリスト作成
	 */
	private List<AreaCheckBox> createAreaList(){
		areaList = new List<AreaCheckBox>();
		String conditions = '';
		String childConditions = '';
		Boolean isMR = false;
		User user = E_UserDao.getUserRecByUserId(UserInfo.getUserId());

		//社員フラグがfalse
		if(!access.isEmployee()){
			//エラー
			pageMessages.addWarningMessage(getMSG().get('MRDL|004'));
		//MRor拠点長の場合
		}else if(access.isAuthMR() || access.isAuthManager()){
			childConditions += ' And ';
			childConditions += ' Name = \'' + user.Unit__c + '\'';
			conditions += ' And ';
			conditions += ' Name = \'' + user.UnitName__c + '\'';
			isMR = true;
		//エリア部長の場合
		}else if(access.isAreaManager()){
			conditions += ' And ';
			conditions += ' Name = \'' + user.UnitName__c + '\'';
		}else{
		}
		for(E_Area__c areaRec: getRec( new List<String>{childConditions,conditions} )){
			AreaCheckBox area = new AreaCheckBox(areaRec, isMR);
			for(E_Unit__c unitRec :areaRec.E_Units__r){
				area.units.add(new UnitCheckBox(unitRec, isMR));
			}
			areaList.add(area);
		}
		return areaList;
	}
	/**
	 * MR選択・拠点選択ボタンの表示切替フラグ
	 */
	public Boolean getUnitSpecific(){
		return !(access.isAuthMR() || access.isAuthManager());
	}
	/**
	 * すべての拠点選択の表示切替フラグ
	 */
	public Boolean getAllArea(){
		if(access.isAuthMR() || access.isAuthManager() || access.isAreaManager()){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * 選択された営業部リスト作成
	 */
	private void createSelectedUnits(){
		isAll = true;
	 	selectedUnits = new List<String>();
		for(AreaCheckBox areaCheckBox :areaList){
			for(UnitCheckBox unitCheckBox :areaCheckBox.units){
				if(unitCheckBox.isChecked){
					selectedUnits.add(unitCheckBox.unit.BRANCH__c);
					selectedUnitName = unitCheckBox.unit.Name;
				}else{
					isAll = false;
				}
			}
		}
	}

	/**
	 * 選択されたMRリスト作成
	 */
	private void createSelectedMRs(){
		selectedMRs = new List<User>();
		for(MRCheckBox mRCheckBox:mrList){
			if(mRCheckBox.isChecked){
				selectedMRs.add(mRCheckBox.mr);
			}
		}
	}

	/**
	 * DLCondition作成
	 */
	private E_DownloadCondition.NoticeDownloadCondi createDLCondition(){		
		// E_DownloadConditionの生成
		E_DownloadCondition.NoticeDownloadCondi dlCondi = new E_DownloadCondition.NoticeDownloadCondi();
					
		// Condition
		//事務所コード
		dlCondi.officeCodes = new List<String>(getOffices());
		//ID種別
		dlCondi.ZIDTYPE = E_Const.ZIDTYPE_EP;
		//期間FromTo
		List<String> periodList = periodValue.split(',');
		//単月指定
		if(periodList.size() == 1){
			dlCondi.fromyyyyMM = periodValue;
			dlCondi.toyyyyMM = periodValue;
		}
		//複数月指定
		else{
			dlCondi.fromyyyyMM = periodList.get(2);
			dlCondi.toyyyyMM = periodList.get(0);
		}
		//レコードタイプMap作成
		Map<String, Id> recTypes = E_RecordTypeDao.getRecordTypeBySobjectType('E_Policy__c');

		dlCondi.policyTypes = new List<String>{(String)recTypes.get(E_Const.POLICY_RECORDTYPE_COLI)};
		//異動契約はSPVAと消滅契約も対象とする
		if(dlFormNo == E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO){
			dlCondi.policyTypes.add((String)recTypes.get(E_Const.POLICY_RECORDTYPE_DCOLI));
			dlCondi.policyTypes.add((String)recTypes.get(E_Const.POLICY_RECORDTYPE_SPVA));
			dlCondi.policyTypes.add((String)recTypes.get(E_Const.POLICY_RECORDTYPE_DSPVA));
		}

		return dlCondi;
	}
	/**
	 * 事務所リスト作成
	 */
	private Boolean isAll;
	private Set<String> getOffices(){
		isAll = false;
		//全選択（異動通知3ヶ月 かつ 全拠点 かつ 全課コード）の際はエラーを返す
		String objNm = E_DownloadNoticeConst.FNO_OBJNM_MAP.get(dlFormNo);
		try{
			periodValue = ApexPages.currentPage().getParameters().get('period'+dlFormNo);
		}catch(Exception e){
			throw new MRDLException(getMSG().get('MRDL|005'));
		}
		List<String> periodList = periodValue.split(',');
		//Query作成
		String query = 'Select Account__c From ' + objNm + ' Where Account__r.E_COMM_VALIDFLAG__c = \'1\' AND Account__r.E_IsAgency__c = false AND YYYYMM__C in: periodList';

		//MR選択モードの際は担当者を指定
		if(isSelectMR){
			createSelectedMRs();
			if(selectedMRs.isEmpty()){
				throw new MRDLException(getMSG().get('MRDL|006'));
			}
			//全件選択時
			if(mrList.size() == selectedMRs.size()){
				query += ' AND (Account__r.E_CL2PF_BRANCH__c in: selectedUnits OR Account__r.E_CL2PF_ZBUSBR__c in: selectedUnits)';
			}else{
				query += ' AND Account__r.OwnerId in: selectedMRs';
			}
		}
		//営業部選択時は支社コード、母店支社コードを指定
		else{
			createSelectedUnits();
			if(selectedUnits.isEmpty()){
				throw new MRDLException(getMSG().get('MRDL|007'));
			}
			query += ' AND (Account__r.E_CL2PF_BRANCH__c in: selectedUnits OR Account__r.E_CL2PF_ZBUSBR__c in: selectedUnits)';
		}
		//全件選択時は課コードの指定は行わない
		if(!(isHaed && isReal && isUnit)){
			//本社担当分選択時はその他を取得
			if(isHaed){
				Set<String> notChargeSet  = new Set<String>();
				if(!isReal) notChargeSet.add(E_DownloadNoticeConst.KSECTION88);
				if(!isUnit) notChargeSet.add(E_DownloadNoticeConst.KSECTION00);
				query += ' AND ( Account__r.KSECTION__c NOT in: notChargeSet)';
			}
			//本社担当分未選択時は選択課コードを指定
			else{
				Set<String> chargeSet = new Set<String>();
				if(isReal) chargeSet.add(E_DownloadNoticeConst.KSECTION88);
				if(isUnit) chargeSet.add(E_DownloadNoticeConst.KSECTION00);
				query += ' AND Account__r.KSECTION__c in: chargeSet';
			}
		}
		if(System.Label.E_INL_EXC_CHDRNUM.length() >= 10){
			query += ' AND E_Policy__r.COMM_CHDRNUM__c  Not In (' + System.Label.E_INL_EXC_CHDRNUM + ')';
		}
		query += ' limit 40000';
		system.debug(query);
		if(getIsAllErr(periodList)){
			throw new MRDLException(getMSG().get('MRDL|009'));
		}
		Set<String> officeSet = new Set<String>();

		for(sObject sob :Database.query(query)){
			officeSet.add((String)sob.get('Account__c'));
		}

		if(officeSet.isEmpty()){
			throw new MRDLException(getMSG().get('MRDL|008'));
		}
		return officeSet;
	}
	/**
	 * 全選択判定
	 */
	private Boolean getIsAllErr(List<String> periodList){
		//異動通知以外はOK
		if(dlFormNo <> E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO){
			return false;
		}
		//全選択時以外はOK
		if(!(isHaed && isReal && isUnit)){
			return false;
		}
		//期間が3ヶ月以外はOK
		if(periodList.size() == 1){
			return false;
		}
		if(isSelectMR){
			return false;
		}
		if(!getAllArea() || !isAll){
			return false;
		}
		return true;
	}

	/* チェックボックス用ラッパークラス */
	//拠点
	public class AreaCheckBox{
		public E_Area__c area{get;set;}
		public Boolean isChecked{get;set;}
		public List<UnitCheckBox> units{get;set;}

		public AreaCheckBox(E_Area__c area, Boolean isMR){
			this.area = area;
			this.isChecked = isMR;
			this.units = new List<UnitCheckBox>();
		}
	}
	//営業部
	public class UnitCheckBox{
		public E_Unit__c unit{get;set;}
		public Boolean isChecked{get;set;}

		public UnitCheckBox(E_Unit__c unit, Boolean isMR){
			this.unit = unit;
			this.isChecked = isMR;
		}
	}
	//MR
	public class MRCheckBox{
		public User mr{get;set;}
		public Boolean isChecked{get;set;}

		public MRCheckBox(User mr){
			this.mr = mr;
			this.isChecked = mr.id == UserInfo.getUserId();
		}
	}

	//Exceptionクラス
	private class MRDLException extends Exception {}
}