public with sharing class CC_BranchConfigDao {

	public static List<CC_BranchConfig__c> getRecsByBRCodes(Set<String> brSet){
		if(brSet == null || brSet.isEmpty()){
			return new List<CC_BranchConfig__c>();
		}
		return [SELECT CC_SharedMailAdd__c FROM CC_BranchConfig__c WHERE CC_BranchCode__c in: brSet];
	}
}