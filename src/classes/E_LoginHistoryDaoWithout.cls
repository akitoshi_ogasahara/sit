public without sharing class E_LoginHistoryDaoWithout{

	public static AggregateResult[] countLastMonthLoginHistory(Set<ID> ids){
		return [SELECT UserId, count(id) loginCount 
				FROM LoginHistory
				WHERE LoginTime >= :Date.today().addmonths(-1)
				AND UserId IN :ids
				GROUP BY UserId];
	}
}