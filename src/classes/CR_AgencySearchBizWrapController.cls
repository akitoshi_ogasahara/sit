public with sharing class CR_AgencySearchBizWrapController extends CR_AbstractExtender{

	private String url;
	private String bizType;	

	// 事業報告書判定
	public Boolean isReports {get; set;}
	// 帳簿書類判定
	public Boolean isBooks {get; set;}
	// メニューNo
	public String menuNo {get; set;}
	
	/**
	 * Constractor
	 */
	public CR_AgencySearchBizWrapController() {
		bizType = ApexPages.currentPage().getParameters().get(CR_Const.URL_PARAM_TYPE_BIZ);
	}
	
	/**
	 * 初期処理
	 */
	public pageReference pageAction(){
		isSuccessOfInitValidate = true;
		isReports = false;
		isBooks = false;
		
		// [種別]事業報告書
		if(bizType == CR_Const.TYPE_BIZREPORTS){
			isReports = true;
			url = getUrlforReports();
			pgTitle = '代理店検索(事業報告書)';
			menuNo = CR_Const.MENU_REPORTS;
			
			// 権限チェック
			if(!(getHasReportsPermission())){
				pageMessages.addErrorMessage(CR_Const.ERR_MSG04_PH2_REQUIRED_PERMISSION_REPORTS); 
				isSuccessOfInitValidate = false;
			}
			
		// [種別]帳簿書類
		}else if(bizType == CR_Const.TYPE_BIZBOOKS){ 
			isBooks = true;
			url = getUrlforBooks();
			pgTitle = '代理店検索(帳簿書類)';
			menuNo = CR_Const.MENU_BOOKS;
			
			// 権限チェック
			if(!(getHasBooksPermission())){
				pageMessages.addErrorMessage(CR_Const.ERR_MSG05_PH2_REQUIRED_PERMISSION_BOOKS); 
				isSuccessOfInitValidate = false;
			}
			
		// URLパラメータ取得エラー
		}else{
			pgTitle = 'エラー';
			menuNo = '';
			pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);
			isSuccessOfInitValidate = false;
			
		}
		
		// 社員以外はリダイレクト(チェックOK、社員、遷移先URLが存在)
		if(isSuccessOfInitValidate && access.isEmployee() == false && String.isNotBlank(url)){
			return new PageReference(url);
        }
        		
		return null;
	}
}