@IsTest public with sharing class CC_SRFormPrintControllerTest {
	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest001() {
		// Caseレコード作成ほか
		CC_SRFormPrintController con = new CC_SRFormPrintController(new ApexPages.StandardController(new Case()));

		// ヘッダ、明細が初期化
		system.assertEquals(false, con.isConfirmationFormOutput);
	}

	static CC_SRTypeMaster__c insSRType() {

		return insSRType('0310004');
	}

	static CC_SRTypeMaster__c insSRType(String srTypeName) {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = new CC_SRTypeMaster__c();
		srType.name=srTypeName;
		srType.CC_SRTypeName__c = 'SRタイプ名001';
		srType.CC_Department__c = 'COLI';
		insert srType;

		return srType;
	}

	static E_Policy__c insPolicy() {
		E_Policy__c pol = new E_Policy__c();
		insert pol;

		return pol;
	}

	static List<E_Policy__c> insPolicies() {
		List<E_Policy__c> pols = new List<E_Policy__c>();
		for (Integer i = 0; i < 4; i++) {
			E_Policy__c pol = insPolicy();

			pols.add(pol);
		}

		return pols;
	}

	static Case insSR(Id srTypeId) {

		return insSR(srTypeId, false, false);
	}

	static Case insSR(Id srTypeId, Boolean isCoverLetter, Boolean isConfirmationForm) {
		Case cs = new Case();
		cs.CC_SRTypeId__c = srTypeId;
		cs.CC_IsCoverLetterOutput__c = isCoverLetter;
		cs.CC_IsConfirmationFormOutput__c = isConfirmationForm;
		cs.CMN_Source__c = '電話';
		insert cs;

		return cs;
	}

	static E_COVPF__c insCOVPF(Id polId) {
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = polId;
		cov.COMM_ZCOVRNAM__c = '終身保険';
		insert cov;

		return cov;
	}

	static List<E_COVPF__c> insCOVPF(List<E_Policy__c> pols) {
		List<E_COVPF__c> covs = new List<E_COVPF__c>();
		for (Integer i = 0; i < 2; i++) {
			E_COVPF__c cov = new E_COVPF__c();
			if (i == 0) {
				cov.E_Policy__c = pols[0].Id;
				cov.COMM_ZCOVRNAM__c = '終身保険';
			} else {
				cov.COMM_ZCOVRNAM__c = '医療保険';
				cov.E_Policy__c = pols[1].Id;
			}
			insert cov;
			covs.add(cov);
		}

		return covs;
	}

	static CC_SRFormPrintController setPage(Case cs) {

		return setPage(cs, null, null);
	}

	static CC_SRFormPrintController setPage(Case cs, String tabval, String optval) {

		Pagereference pageref = Page.CC_SRFormPrint;
		pageref.getParameters().put('srid', (String)cs.Id);
		if (tabval != null) {
			pageref.getParameters().put('tabvalue', tabval);
		}
		if (optval != null) {
			pageref.getParameters().put('optionValue', optval);
		}
		Test.setCurrentPage(pageRef);
		CC_SRFormPrintController con = new CC_SRFormPrintController(new ApexPages.StandardController(cs));

		return con;
	}

	static CC_SRPolicy__c insSRPolicy(Id csId, Id polId, Id formId) {
		CC_SRPolicy__c srp = new CC_SRPolicy__c();
		srp.CC_PolicyID1__c = polId;
		srp.CC_FormMaster1__c = formId;
		srp.CC_CaseId__c = csId;
		insert srp;

		return srp;
	}

	static CC_SRPolicy__c insSRPolicy(Id csId, List<E_Policy__c> pols, List<CC_FormMaster__c> fms) {
		CC_SRPolicy__c srp = new CC_SRPolicy__c();

		srp.CC_PolicyID1__c = pols[0].Id;
		srp.CC_PolicyID2__c = pols[1].Id;
		srp.CC_PolicyID3__c = pols[2].Id;
		srp.CC_PolicyID4__c = pols[3].Id;
		srp.CC_FormMaster1__c = fms[0].Id;
		srp.CC_FormMaster2__c = fms[1].Id;
		srp.CC_CaseId__c = csId;
		insert srp;

		return srp;
	}

	static List<CC_FormMaster__c> insForms() {

		return insForms('COLI-POS 121', null);
	}

	static List<CC_FormMaster__c> insForms(String form1, String form2) {
		List<CC_FormMaster__c> fms = new List<CC_FormMaster__c>();
		for (Integer i = 0; i < 2; i++) {
			CC_FormMaster__c fm = new CC_FormMaster__c();
			if (i == 0) {
				fm.CC_FormNo__c = form1;
			} else {
				fm.CC_FormNo__c = form2;
			}
			insert fm;
			fms.add(fm);
		}

		return fms;
	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest002() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType();

		Case cs = insSR(srType.Id);

		CC_SRFormPrintController con = setPage(cs);

		// ヘッダ、明細が初期化
		system.assertEquals(false, con.isConfirmationFormOutput);
	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest003() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType();

		E_Policy__c pol = insPolicy();

		Case cs = insSR(srType.Id);



		CC_SRFormPrintController con = setPage(cs);

		// ヘッダ、明細が初期化
		system.assertEquals(false, con.isConfirmationFormOutput);
	}

		/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest004() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType();

		E_Policy__c pol = insPolicy();

		E_COVPF__c cov = insCOVPF(pol.Id);

		Case cs = insSR(srType.Id);

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pol.Id, null);

		CC_SRFormPrintController con = setPage(cs);

		// ヘッダ、明細が初期化
		system.assertEquals(false, con.isConfirmationFormOutput);
	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest005() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType();

		E_Policy__c pol = insPolicy();

		E_COVPF__c cov = insCOVPF(pol.Id);

		Case cs = insSR(srType.Id);

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pol.Id, null);


		CC_SRFormPrintController con = setPage(cs);

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());

		system.assertEquals(10, con.bulkTab.size());
		con.addBulkPage();
		system.assertEquals(20, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		system.assertEquals(false, con.isConfirmationFormOutput);
	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest006() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType('0310005');

		E_Policy__c pol = insPolicy();

		E_COVPF__c cov = insCOVPF(pol.Id);

		Case cs = insSR(srType.Id, true, false);

		CC_FormMaster__c fm = new CC_FormMaster__c();
		fm.CC_IsNotValid__c =false;
		fm.CC_FormType__c = '送付状';
		fm.CC_SVFFormButtonName__c = 'SVFxxxxxxxxxx';
		insert fm;
		cs.CC_IsCoverLetterOutput__c = true;
		cs.CC_CoverLetter__c = fm.Id;
		update cs;

		// CC_SRPolicy__c srp = insSRPolicy(cs.Id, pol.Id, fm.Id);

		CC_SRFormPrintController con = setPage(cs, '一括', '解約');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());

		system.assertEquals(10, con.bulkTab.size());
		con.addBulkPage();
		system.assertEquals(20, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		system.assertEquals(false, con.isConfirmationFormOutput);

		con.save();

	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest007() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType('0310005');


		List<E_Policy__c> pols = insPolicies();

		E_COVPF__c cov = insCOVPF(pols[0].Id);

		Case cs = insSR(srType.Id, false, true);

		List<CC_FormMaster__c> fms = insForms();

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pols, fms);

		CC_SRFormPrintController con = setPage(cs, '一括', '解約');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());

		List<SelectOption> unitOpt = con.unitTab[0].getRiders();

		Boolean isUnitMaxDisp = con.unitTab[0].maxAmountDisp;

		Boolean isBulkMaxdisp = con.bulkTab[0].maxAmountDisp;

		system.assertEquals(10, con.bulkTab.size());
		con.addBulkPage();
		system.assertEquals(20, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		con.isConfirmationFormOutput = true;
		system.assertEquals(true, con.isConfirmationFormOutput);

		con.save();
	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest008() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType('0310005');


		List<E_Policy__c> pols = insPolicies();

		List<E_COVPF__c> covs = insCOVPF(pols);

		Case cs = insSR(srType.Id, false, false);

		List<CC_FormMaster__c> fms =insForms();

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pols, fms);

		CC_SRFormPrintController con = setPage(cs, '一括', '解約');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());

		//List<SelectOption> unitOpt = con.unitTab[0].getRiders();

		system.assertEquals(10, con.bulkTab.size());
		con.addBulkPage();
		system.assertEquals(20, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		con.isConfirmationFormOutput = true;
		system.assertEquals(true, con.isConfirmationFormOutput);

		con.save();

	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest009() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType('0310005');


		List<E_Policy__c> pols = insPolicies();

		List<E_COVPF__c> covs = insCOVPF(pols);

		Case cs = insSR(srType.Id, false, false);

		List<CC_FormMaster__c> fms = insForms('COLI-POS 122-03', 'COLI-POS 122-03');

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pols, fms);

		CC_SRFormPrintController con = setPage(cs, '個別', '解約');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());

		//List<SelectOption> unitOpt = con.unitTab[0].getRiders();

		system.assertEquals(10, con.bulkTab.size());
		con.addBulkPage();
		system.assertEquals(20, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		con.isConfirmationFormOutput = true;
		system.assertEquals(true, con.isConfirmationFormOutput);

		con.save();

	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest010() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType('0310005');


		List<E_Policy__c> pols = insPolicies();

		List<E_COVPF__c> covs = insCOVPF(pols);

		Case cs = insSR(srType.Id, false, true);

		List<CC_FormMaster__c> fms = insForms('COLI-POS 122-03', 'COLI-POS 122-03');

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pols, fms);

		CC_SRFormPrintController con = setPage(cs, '個別', '解約');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());

		// List<SelectOption> unitOpt = con.unitTab[0].getRiders();

		system.assertEquals(10, con.bulkTab.size());
		con.addBulkPage();
		system.assertEquals(20, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		con.delBulkPage();
		system.assertEquals(10, con.bulkTab.size());

		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		con.isConfirmationFormOutput = true;
		system.assertEquals(true, con.isConfirmationFormOutput);

		con.save();

	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest011() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType('0310002');


		List<E_Policy__c> pols = insPolicies();

		List<E_COVPF__c> covs = insCOVPF(pols);

		Case cs = insSR(srType.Id, false, true);

		List<CC_FormMaster__c> fms = insForms('COLI-POS 322-03', 'COLI-POS 322-03');

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pols, fms);

		CC_SRFormPrintController con = setPage(cs, '個別', '減額');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());


		system.assertEquals(null, con.printStr);

		// ヘッダ、明細が初期化
		con.isConfirmationFormOutput = true;
		system.assertEquals(true, con.isConfirmationFormOutput);

		con.save();

	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest012() {
		// Caseレコード作成ほか
		CC_SRTypeMaster__c srType = insSRType();

		List<E_Policy__c> pols = insPolicies();

		List<E_COVPF__c> covs = insCOVPF(pols);

		Case cs = insSR(srType.Id, false, true);

		List<CC_FormMaster__c> fms = insForms('COLI-POS 222-03', 'COLI-POS 222-03');

		CC_SRPolicy__c srp = insSRPolicy(cs.Id, pols, fms);

		CC_SRFormPrintController con = setPage(cs, '個別', '契約者貸付');

		List<SelectOption> opt = con.getActClasses();
		system.assertEquals(3, opt.size());


		system.assertEquals(null, con.printStr);

		String maxDisp = con.maxDisp;

		// ヘッダ、明細が初期化
		con.isConfirmationFormOutput = true;
		system.assertEquals(true, con.isConfirmationFormOutput);

		con.save();

	}

	/** コンポーネントコントローラ初期化テスト_新規 */
	@isTest static void test_constructorTest013() {

		List<CC_SRPolicy__c> srp = CC_SRFormPrintDao.getCCSRPolicyByCaseId(null);

		List<E_COVPF__c> cov = CC_SRFormPrintDao.getECovpfByPolicyIds(new List<Id>());



	}

}