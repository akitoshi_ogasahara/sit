@isTest(seealldata = false)
public with sharing class TestE_ZHLDPERCController {
    private static boolean isEditable;
    //正常系テスト
    private static testmethod void standardTest(){
        E_policy__c policy = new E_Policy__c();
        insert policy;
        E_SVCPF__c SVCPF = new E_SVCPF__c(ZFSDFROM__c = '19020101',ZFSHFROM__c = '00',ZFSMFROM__c = '00',ZFSDTO__c = '99990101',ZFSHTO__c = '00',ZFSMTO__c = '00',FLAG02__c = true);
        insert SVCPF;
        E_SVFPF__c SVFPF = new E_SVFPF__c(E_policy__c = policy.id,E_SVCPF__c = SVCPF.id);
        insert SVFPF;
        SVFPF = [select E_SVCPF__r.ZFSDFROM__c,E_SVCPF__r.ZFSHFROM__c,E_SVCPF__r.ZFSMFROM__c,E_SVCPF__r.ZFSDTO__c,E_SVCPF__r.ZFSHTO__c,E_SVCPF__r.ZFSMTO__c,E_SVCPF__r.FLAG02__c from E_SVFPF__c where id =:svfpf.id];
        E_ZHLDPERCController controller = new E_ZHLDPERCController();
        controller.SVFPF = SVFPF;
        isEditable = controller.getIsEditable();
        system.assert(isEditable);
    }
    
    //異常系テストパラメータ不正
    private static testmethod void eroorParamTest(){
        E_policy__c policy = new E_Policy__c();
        insert policy;
        E_SVCPF__c SVCPF = new E_SVCPF__c(ZFSDFROM__c = '19020101',ZFSHFROM__c = '00',ZFSMFROM__c = '00',ZFSDTO__c = '99990101',ZFSHTO__c = '00',ZFSMTO__c = '00',FLAG02__c = false);
        insert SVCPF;
        E_SVFPF__c SVFPF = new E_SVFPF__c(E_policy__c = policy.id,E_SVCPF__c = SVCPF.id);
        insert SVFPF;
        SVFPF = [select E_SVCPF__r.ZFSDFROM__c,E_SVCPF__r.ZFSHFROM__c,E_SVCPF__r.ZFSMFROM__c,E_SVCPF__r.ZFSDTO__c,E_SVCPF__r.ZFSHTO__c,E_SVCPF__r.ZFSMTO__c,E_SVCPF__r.FLAG02__c from E_SVFPF__c where id =:svfpf.id];
        E_ZHLDPERCController controller = new E_ZHLDPERCController();
        controller.SVFPF = SVFPF;
        isEditable = controller.getIsEditable();
        system.assert(!isEditable);
    }

    //異常系テストException
    private static testmethod void eroorExceptionTest(){
        String errMsg;
        E_policy__c policy = new E_Policy__c();
        insert policy;
        E_SVCPF__c SVCPF = new E_SVCPF__c(ZFSDFROM__c = '19020101',ZFSHFROM__c = '00',ZFSMFROM__c = '00',ZFSDTO__c = '99990101',ZFSHTO__c = '00',ZFSMTO__c = '00',FLAG02__c = false);
        insert SVCPF;
        E_SVFPF__c SVFPF = new E_SVFPF__c(E_policy__c = policy.id,E_SVCPF__c = SVCPF.id);
        insert SVFPF;
        E_ZHLDPERCController controller = new E_ZHLDPERCController();
        controller.SVFPF = SVFPF;
        try{
            isEditable = controller.getIsEditable();
        }catch(SkyEditor2.ExtenderException e){
            errMsg = e.getMessage();
        }
        system.assertequals('GMTへの変換に失敗しました。null null:null',errMSG);
    }


}