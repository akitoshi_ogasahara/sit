@istest
private class TestSC_Search_agWrapController {
    // コンストラクタ
    private static testMethod void SearchagWrapTest001() {
        Test.startTest();
            SC_Search_agWrapController controller = new SC_Search_agWrapController();
        Test.stopTest();  

        System.assertEquals(controller.getPageTitle(), '代理店事務所検索（代理店自己点検）');
        System.assertEquals(controller.isViewSearch, false);
    }

    // pageAction:（AY権限）
    private static testMethod void SearchagWrapTest002() {
		// テストデータ作成
		SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null);
		User actUser = TestSC_TestUtil.createTestDataSet('AY', office);

        PageReference pr;
        SC_Search_agWrapController controller;

        System.runAs(actUser){
            TestSC_TestUtil.createBasePermissions(actUser.Id);
        TestSC_TestUtil.createAgencyPermissions(actUser.Id);

            Test.startTest();
                controller = new SC_Search_agWrapController();
                pr = controller.pageAction();
            Test.stopTest();
        }

		//System.assertEquals(pr.getUrl(), Page.E_SCSelfCompliances.getUrl() + '?id=' + office.Id);
        System.assertEquals(controller.isViewSearch, true);
    }

    // pageAction:（AH権限）
    private static testMethod void SearchagWrapTest003() {
		// テストデータ作成
		SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null);
		User actUser = TestSC_TestUtil.createTestDataSet('AH', office);

        PageReference pr;
        SC_Search_agWrapController controller;

        System.runAs(actUser){
            TestSC_TestUtil.createBasePermissions(actUser.Id);

            Test.startTest();
                controller = new SC_Search_agWrapController();
                pr = controller.pageAction();
            Test.stopTest();
        }

        System.assertEquals(pr, null);
        System.assertEquals(controller.isViewSearch, true);
    }

    // pageAction:（AY権限）：自主点検事務所なし
    private static testMethod void SearchagWrapTest004() {
		// テストデータ作成
		SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null);
		User actUser = TestSC_TestUtil.createTestDataSet('AY', office);
		delete office;

        PageReference pr;
        SC_Search_agWrapController controller;

        System.runAs(actUser){
            TestSC_TestUtil.createBasePermissions(actUser.Id);
        TestSC_TestUtil.createAgencyPermissions(actUser.Id);

            Test.startTest();
                controller = new SC_Search_agWrapController();
                pr = controller.pageAction();
            Test.stopTest();
        }

		System.assertEquals(pr, null);
		System.assertEquals(controller.isViewSearch, true);
        //System.assertEquals(controller.pageMessages.getErrorMessages().get(0).summary, SC_Const.ERR_MSG05_SCOFFICE_NONE);
    }
}