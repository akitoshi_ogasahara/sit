public with sharing class I_MeritTableDTO {

    public string keikaNensu {get; set;}
    public string age {get; set;}
    public Decimal S {get; set;}
    public Decimal shibouKyufuKin {get; set;}
    public Decimal pRuikei {get; set;}
    public Decimal cv {get; set;}
    public Decimal simpleHenreiRt {get; set;}
    public Decimal sonkinP {get; set;}
    public Decimal koukaGaku {get; set;}
    public Decimal koukaGakuRuikei {get; set;}
    public Decimal jisshitsuFutanGaku {get; set;}
    public Decimal jisshitsuHenreiRt {get; set;}
    public Decimal shisanKeijoGaku {get; set;}

}