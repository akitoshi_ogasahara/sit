public with sharing class MD_DetailExtension extends MD_AbstractController {
	// ドクター
	private MD_Doctor__c doctor;
	// 詳細情報（ログ）
	private String detail; 

	public String url {get; set;}

	/**
	 * Constructor
	 */
	public MD_DetailExtension(ApexPages.StandardController stdController) {
		this.doctor = (MD_Doctor__c)stdController.getRecord();
		// 詳細情報
		detail = '[嘱託医番号: ';
		detail += E_Util.null2Blank(this.doctor.DoctorNumber__c);
		detail += ']';
	}

	/**
	 * init
	 */
	public PageReference init() {
		// IRIS Day2 ページアクセスログは E_Logging コンポーネントで取得するように変更
		return null;
		/*
		try {
			// アクセスログ登録
			logCondition.recId = doctor.Id;
			logCondition.detail = detail;
			return insertAccessLog();

		} catch (Exception e) {
			// アクセスログ登録、 エラーページへ
			logCondition.recId = doctor.Id;
			return insertErrorLog(e.getMessage());
		}
		*/
	}

	/**
	 * メール送信ボタン
	 */
	public void doSendMail() {
		// アクセスログ登録
		logCondition = new E_LogCondition.DoctorCondi();
		logCondition.accessPage = 'MD_Detail SendMail';
		logCondition.recId = doctor.Id;
		logCondition.detail = detail;
		insertAccessLog();

		url = getMailtoLink();
	}

	/**
	 * [メール送信]ボタンへのmailtoリンクを返す
	 * 詳細画面の項目構成、及びgoogleMapへの住所リンク
	 */
	public String getMailtoLink() {
		// 地図リンク作成
		String linkUrl = '';

		MD_Doctor__c d = [select Location_WGS__Latitude__s, Location_WGS__Longitude__s from MD_Doctor__c where id = :doctor.Id];

		linkUrl += 'http://maps.google.co.jp/maps?q=' + string.valueOf(d.Location_WGS__Latitude__s) + ',' + string.valueOf(d.Location_WGS__Longitude__s);

		String mailToLinkPrefix = 'mailto:?body=';
		String mailToLinkBody =  + '\r\n\r\n\r\n'
								 + setSumiBrackets(Schema.SObjectType.MD_Doctor__c.fields.HospitalName__c.label)
								 + E_Util.null2Blank(doctor.HospitalName__c)
								 + '\r\n'
								 + setSumiBrackets(Schema.SObjectType.MD_Doctor__c.fields.Name.label)
								 + E_Util.null2Blank(doctor.Name)
								 + '\r\n'
								 + '【郵便番号】'
								 + E_Util.null2Blank(doctor.PostalCode__c)
								 + '\r\n'
								 + '【住所】'
								 + E_Util.null2Blank(doctor.State__c + doctor.Address__c)
								 + '\r\n'
								 + setSumiBrackets('地図へのリンク')
								 + linkUrl
								 + '\r\n'
								 + setSumiBrackets(Schema.SObjectType.MD_Doctor__c.fields.Phone__c.label)
								 + E_Util.null2Blank(doctor.Phone__c)
								 + '\r\n'
								 + '\r\n'
								 + E_Util.null2Blank(E_Message.getDisclaimerMap().get('MDDE|001')) //共通ディスクレーマー
								 ;

		//body部分には全角文字が含まれるためURLエンコードして返す
		return mailToLinkPrefix + EncodingUtil.urlEncode(mailToLinkBody,'UTF-8').replace('+', '%20');
	}

	//隅括弧付与
	private String setSumiBrackets(String taget) {
		return '【' + taget + '】';
	}
}