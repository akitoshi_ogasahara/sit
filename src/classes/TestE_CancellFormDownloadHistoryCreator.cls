@isTest
private class TestE_CancellFormDownloadHistoryCreator {
	
	// TestData
	private static E_Policy__c policy;

	/**
	 * E_CancellFormDownloadHistory__c createLog(Id policyId, String DLFileName, String IDType)
	 */
	@isTest static void createLog_test001() {
		E_Policy__c policy = createData();
		String fileName = 'test file';
		String idType = 'AH';

		Test.startTest();

		E_CancellFormDownloadHistoryCreator creator = new E_CancellFormDownloadHistoryCreator();
		E_CancellFormDownloadHistory__c result = creator.createLog(policy.Id, fileName, idType);

		Test.stopTest();

		// assert
		System.assertEquals(policy.Id, result.E_Policy__c);
		System.assertEquals(fileName, result.DLPDFNM__c);
		System.assertEquals(idType, result.ZIDTYPE__c);
	}
	
	/**
	 * E_CancellFormDownloadHistory__c createCancellationDLLog(E_Policy__c policy, E_COVPF__c mainCovpf, String DLFileName)
	 * E_CancellFormDownloadHistory__c createCancellationDLLog(E_Policy__c policy, E_COVPF__c mainCovpf, String DLFileName, boolean toNotifyFlag, User DLUser)
	 */
	@isTest static void createCancellationDLLog_test001() {
		E_Policy__c policy = createData();
		E_COVPF__c maincov = createCOV(policy);
		String fileName = 'test file';

		Test.startTest();

		E_CancellFormDownloadHistoryCreator creator = new E_CancellFormDownloadHistoryCreator();
		E_CancellFormDownloadHistory__c result = creator.createCancellationDLLog(policy, maincov, fileName, true, new User(), null);

		Test.stopTest();

		// assert
		System.assertEquals(policy.Id, result.E_Policy__c);
		System.assertEquals(fileName, result.DLPDFNM__c);
	}

	/** Test Data */
	private static E_Policy__c createData(){
		// 代理店格
		Account ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
		insert ahAcc;

		// 事務所
		Account ayAcc = new Account(
				Name = 'office01'
				,ParentId = ahAcc.Id
				,E_CL2PF_ZAGCYNUM__c = 'ay001'
				,E_COMM_VALIDFLAG__c = '1'
				,AGNTBR_NM__c = '東京'
		);
		insert ayAcc;

		// 保険契約ヘッダ
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(E_Const.POLICY_RECORDTYPE_COLI);
		E_Policy__c policy = new E_Policy__c();
		policy.RecordTypeId = recTypeId;
		policy.COMM_CHDRNUM__c = '12345678';
		policy.MainAgentAccount__c = ayAcc.Id;
		insert policy;

		return policy;
	}

	private static E_COVPF__c createCOV(E_Policy__c po){
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);

		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_STATDATE__c = '20161111';
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		cov.COLI_ZCRIND__c = 'C';
		cov.COMM_ZCOVRNAM__c = '4テストタイプ';
		cov.COLI_ZDPTYDSP__c = true;
		cov.COMM_CHDRNUM__c = '99999999';
		cov.DCOLI_ZCLNAME__c = '契約者A';
		cov.DCOLI_ZINSNAM__c = '被保険者A';
		cov.COMM_SUMINS__c = 987654321;
		cov.DCOLI_SINSTAMT__c = 123456789;
		cov.DCOLI_OCCDATE__c = '20171027';
		cov.COMM_ZRCESDSC__c = '終身';
		cov.COMM_ZPCESDSC__c = '---------';
		cov.RecordTypeId = recTypeId;
		cov.COMM_CRTABLE__c = E_Const.COVPF_CRTABLE2_JA;
		insert cov;

		cov = [Select Id, COMM_CRTABLE2__c, COMM_ZCOVRNAMFIX__c From E_COVPF__c Where Id = :cov.Id];
		
		return cov; 
	}
}