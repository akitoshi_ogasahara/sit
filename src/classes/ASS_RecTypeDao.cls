/**
 * 営業日報データアクセスオブジェクト
 * CreatedDate 2018/05/07
 * @Author Terrasky
 */

public class ASS_RecTypeDao {
	/**
	 * レコードタイプリスト取得
	 * @param sobType :　オブジェクトタイプ
	 * @param devName :　レコードタイプ名
	 * @return List<RecordType>: レコードタイプリスト
	 */
	public static RecordType getRecTypeByDevName(String subType, String devName) {
		return [SELECT 
					Id 
					, Name
				FROM 
					RecordType 
				WHERE 
					SobjectType = :subType
				AND 
					DeveloperName = :devName
				];
	}

	/**
	 * レコードタイプリスト取得
	 * @param sobType :　オブジェクトタイプ
	 * @return List<RecordType>: レコードタイプリスト
	 */
	public static List<RecordType> getRecTypesBySubType(String sobType) {
		return [SELECT 
					id
					, DeveloperName 
				FROM 
					RecordType 
				WHERE 
					SobjectType = :sobType
				];
	}
}