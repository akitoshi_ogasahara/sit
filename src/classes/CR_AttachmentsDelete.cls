public with sharing class CR_AttachmentsDelete{

	//　ファイルの削除とステータスの変更
	@future
	public static void asyncDelete(Id logId){
		List<Attachment> delFiles = new List<Attachment>();
		List<E_DownloadHistorry__c> dlRecs = new List<E_DownloadHistorry__c>();

		String logMsg = null;
		Savepoint sp = Database.setSavepoint();
		try{
			for(E_DownloadHistorry__c rec:E_DownloadHistoryDaoWithout.getRecsAndFilesByfmNoAndDate(
																					  new Set<String>{CR_Const.TYPE_BIZREPORTS, CR_Const.TYPE_BIZBOOKS}
																					, E_Util.getDayAgo(CR_Const.CR_ATTACHMENTS_EXPIRE_DAYS)
																				)){
				//ダウンロードログのステータス変更
				rec.Status__c = CR_Const.DL_STATUS_DELETED;
				dlRecs.add(rec);
			
				//添付ファイルの削除
				delFiles.addAll(rec.attachments);
			}
		
			delete delFiles;
			update dlRecs;
			
			logMsg = '---Success---\n';
			logMsg += '削除済ダウンロード履歴件数:' + String.valueOf(dlRecs.size()) + '\n';
			logMsg += '削除ファイル件数:' + String.valueOf(delFiles.size());
		}catch(Exception e){
			Database.rollback(sp);
			logMsg = e.getMessage() + '@@@' + e.getStackTraceString();
		}
	
		//EBizログの更新
		E_BizDataSyncLog__c log = E_BizDataSyncLogDao.getRecById(logId);
		if (String.isNotBlank(logMsg)){
			log.log__c = logMsg; 
		}
		
		log.datasyncenddate__c = Datetime.now();
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		log.setOptions(dml);
		update log;
	}
	
}