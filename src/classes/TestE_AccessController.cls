/**
 * E_AccessController テストメソッド
 */
@isTest
private class TestE_AccessController {

	private static User testuser;
	private static E_IDCPF__c idcpf1;
	private static E_IDRequest__c eidr;
	private static Account account;
	private static Contact contact;
	private static RecordType recordType;
	private static E_MenuKind__c menuKind;


	static testMethod void allUnitTest() {
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '2';
		idcpf.FLAG06__c = '1';
		idcpf.ZDSPFLAG01__c = '1';
		idcpf.TRCDE01__c = 'TD05';
		idcpf.TRCDE10__c = 'TD06';
		idcpf.TRCDE02__c = 'TDB0';
		idcpf.ZSTATUS01__c  = '1';
		insert idcpf;

		E_AccessController access;
		System.runAs(u){
			Test.startTest();
			access = E_AccessController.getInstance();
			System.assert(!access.isComplianceView());
			access.switchEIDC_appMode();
			idcpf.appMode__c  = 'IRIS';
			update idcpf;
			access.switchEIDC_appMode();

			access.switchEIDC_appMode('IRIS');
			System.assertEquals(idcpf.appMode__c,'IRIS');

			Test.stopTest();
		}
		system.assert(access.canColi());
		system.assert(access.canProcessHistory());
		system.assert(access.canFeeDetail());
		system.assert(!access.canSpva());
		system.assert(!access.canCustomerSearch());
		system.assert(!access.canInvestment());
		system.assert(access.isAccessiblePolicy());
		system.assert(access.isAccessiblePolicyHistory());
		system.assert(access.isAccessibleInvestment());
		system.assert(access.isAccessibleDownload());
		system.assert(access.isAccessibleCustomer());
		system.assert(access.isAccessibleSpecialFeaturesJp());
		system.assert(access.isAccessibleSpecialFeaturesBank());
		system.assert(access.isEmployee());
		system.assert(!access.isAgent());
		system.assert(!access.isCustomer());
		system.assert(access.isSystemAdministrator());
		system.assert(access.isTransactionCode('TD05'));
		system.assert(access.isTransactionCode('TD06'));
		system.assert(access.isTransactionCode('TDB0'));

		//1220追加
		System.assert(access.hasAuthDistributor());
		System.assert(access.hasAuthOffice());
		System.assert(access.hasAuthAgent());
		System.assert(!access.isCallCenter());
		System.assert(!access.isGuest());
		System.assert(!access.isCommonGateway());
		System.assert(access.isViaInternet());
		System.assert(!access.isInitAndReissuePassword());
		System.assertEquals(access.isLoginControl(null),null);
		System.assertEquals(access.isServiceHourMenu(null),null);
		System.assertEquals(access.isServiceHourMenu(E_Const.ID_KIND.COMPLIANCE),null);
		System.assertEquals(access.getAccountCode(),null);
		System.assertEquals(access.getOfficeCode(),null);
		System.assertEquals(access.getAccZSHRMAIN(),null);
		System.assert(access.canDisplayFeeGuidance());
		System.assert(access.canDisplayFeeData());
		System.assert(access.canDisplayHoldPolicyColi());
		System.assert(!access.canDisplayHoldPolicySpva ());
		System.assert(access.canDisplayPolicyHisotry ());
		System.assert(access.canDownloads());
		System.assert(access.isAccessKind(null));
		System.assertEquals(access.getAccessKindFromThree(),null);
		System.assert(!access.hasCRApprovalPermission());
		System.assert(!access.hasCROutsrcViewPermission());
		System.assert(access.isCRModifyAllProfileUser());
		System.assert(!access.hasCRDpaPermission());
		System.assert(!access.hasCRReportsPermission());
		System.assert(!access.hasCRBooksPermission());
		System.assert(!access.hasSCAGPermission());
		System.assert(!access.hasSCNNPermission());
		System.assert(!access.hasSCDirectorPermission());
		System.assert(!access.hasSCAdminPermission());
		System.assert(!access.ZINQUIRR_is_BRAll());
		System.assert(!access.isAuthAH());
		System.assert(!access.isAuthAY());
		System.assert(!access.isAuthAT());
		System.assert(!access.isAuthMR());
		System.assert(!access.isAuthManager());
		System.assert(!access.isSelfCreatedUser());
		System.assert(access.isUseIRIS());
		System.assert(!access.isUseNNLink());
		System.assert(!access.isEmailSuggestUser());
		System.assertEquals(access.getOfficeMRCode(),null);
		System.assertEquals(access.getReferer(null),'');
		System.assertEquals(access.hasPermission(''),false);
	}

	static testMethod void notIdcpfUnitTest() {
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		E_AccessController access;
		System.runAs(u){
			Test.startTest();

			access = E_AccessController.getInstance();

			Test.stopTest();
		}
		system.assert(!access.canColi());
		system.assert(!access.canSpva());
		system.assert(!access.canCustomerSearch());
		system.assert(!access.canProcessHistory());
		system.assert(!access.canFeeDetail());
		system.assert(!access.canInvestment());
		system.assert(access.isAccessiblePolicy());
		system.assert(access.isAccessiblePolicyHistory());
		system.assert(access.isAccessibleInvestment());
		system.assert(access.isAccessibleDownload());
		system.assert(access.isAccessibleCustomer());
		system.assert(access.isAccessibleSpecialFeaturesJp());
		system.assert(access.isAccessibleSpecialFeaturesBank());
		system.assert(access.isEmployee());
		system.assert(!access.isAgent());
		system.assert(!access.isCustomer());
		system.assert(access.isSystemAdministrator());
		system.assert(!access.isTransactionCode('TD05'));
		system.assert(!access.isTransactionCode('TD06'));
		system.assert(!access.isTransactionCode('TDB0'));

		System.assert(access.hasAuthDistributor());
		System.assert(access.hasAuthOffice());
		System.assert(access.hasAuthAgent());
	}

	//代理店テスト
	static testMethod void partnerCommunityUnitTest(){
		User u = createPartnerDataCommon('E_PartnerCommunity');
		E_AccessController access;
		System.runAs(u){
			Test.startTest();
			access = E_AccessController.getInstance();
			System.assert(!access.isComplianceView());
			System.assertEquals(access.isLoginControl(null).getURL(),Page.E_Compliance.getURL());

			//IRIS NNLink 切り替えテスト
			access.switchEIDC_appMode();
			idcpf1.appMode__c  = 'IRIS';
			update idcpf1;
			access.switchEIDC_appMode();
			access.switchEIDC_appMode('IRIS');
			System.assertEquals(idcpf1.appMode__c,'IRIS');

			PageReference pageRef = Page.E_CustomerInfo;
			pageRef.getHeaders().put('Referer', Page.E_CustomerInfo.geturl());
			Test.setCurrentPage(pageRef);

			access.setReferer(E_Const.ID_KIND.CONTACT, new List<PageReference>{Page.E_CustomerInfo}, E_CookieHandler.REFERER_SEARCH);
			Test.stopTest();
		}
		//System.assertEquals(access.isLoginControl(null),null);
		System.assertEquals(access.getOfficeMRCode(),null);
		System.assert(access.isEmailSuggestUser());
		System.assertEquals(access.getAccountCode(),null);
		System.assert(access.hasAuthOffice());
		System.assertEquals(access.getOfficeCode(),null);
		System.assert(!access.getAccZSHRMAIN());
		System.assert(access.isAccessKind('L3'));
		System.assert(!access.hasPermission(new Set<String>{'E_PermissionSet_Base'}));
		System.assert(access.isAuthAH());
		System.assert(!access.isSuggestPWChange());
	}

	//契約者テスト
	static testMethod void customerCommunityUnitTest(){
		User u = createPartnerDataCommon('E_CustomerCommunity');
		E_AccessController access;
		System.runAs(u){
			Test.startTest();
			access = E_AccessController.getInstance();
			Test.stopTest();
		}
		System.assert(access.isCustomer());
	}

	//住生IRISテスト
	static testMethod void sumiseiUserTest01(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);
		}

		System.runAs(uc){
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			E_AccessController controller;
			controller = E_AccessController.getInstance();
			System.assertEquals(controller.isSumiseiIRISUser(),true);
			Test.stopTest();
		}
	}

	//住生IRISテスト2
	static testMethod void sumiseiUserTest02(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',false);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);
		}

		System.runAs(uc){
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			E_AccessController controller;
			controller = E_AccessController.getInstance();
			System.assertEquals(controller.isSumiseiIRISUser(),false);
			Test.stopTest();
		}
	}

	//住生IRISテスト3
	static testMethod void sumiseiUserTest03(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);
		}

		System.runAs(uc){
			PageReference pageRef = Page.IRIS;
			pageRef.getParameters().put('isSMTM','1');
			Test.setCurrentPage(pageRef);
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			E_AccessController controller;
			controller = E_AccessController.getInstance();
			System.assertEquals(controller.canViewSumiseiIRIS(),true);
			Test.stopTest();
		}
	}

	//住生IRISテスト4
	static testMethod void sumiseiUserTest04(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',false);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);
		}

		System.runAs(uc){
			PageReference pageRef = Page.IRIS;
			pageRef.getParameters().put('isSMTM','1');
			Test.setCurrentPage(pageRef);
			TestI_TestUtil.createIRISSumiseiPermissions(uc.Id);
			Test.startTest();
			E_AccessController controller;
			controller = E_AccessController.getInstance();
			System.assertEquals(controller.canViewSumiseiIRIS(),false);
			Test.stopTest();
		}
	}


	/* test data 作成*/
	static User createPartnerDataCommon(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			userrole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
			//having role user
			User roleUser = new User(
				Lastname = 'roleuser'
				, Username = 'roleuser@terrasky.ingtesting'
				, Email = 'roleuser2@terrasky.ingtesting'
				, ProfileId = profile.Id
				, UserRoleId = portalRole.id
				, Alias = 'roleuser'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='roleuser'
			);
			insert roleuser;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '一般代理店（インターネット）');
			insert menuKind;

			// Account
			account = new Account(Name = 'testAccount',ownerid = roleuser.id, E_MenuKind__c = menuKind.Id);
			insert account;

			// Contact
			contact = new Contact(LastName = 'fstest', FirstName = 'firstName', AccountId = account.Id,email = 'fstest@terrasky.ingtesting',ownerid = roleuser.id);
			insert contact;

			// User
			if(profilename != null){
				profile = [Select Id, Name,usertype From Profile Where Name = :profileName Limit 1];
			}
			testuser = new User(
				Lastname = 'fstest'
				, FirstName = 'firstName'
				, Username = 'fstest2@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
				, contactID = contact.id
			);
			insert testuser;

			idcpf1 = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				User__c = testuser.Id,
				ZSTATUS01__c = '1',
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
				,ownerid = testuser.id,
				ZINQUIRR__c = 'L3',
				ZDSPFLAG02__c = '0',
				ZIDTYPE__c  = 'AH'
			);
			insert idcpf1;

			eidr = new E_IDRequest__c(
				E_IDCPF__c = idcpf1.ID              //ID管理
				,ZUPDFLAG__c = E_Const.ZUPDFLAG_I   //リクエスト種別
				,ZWEBID__c = '5012345678'           //ID番号
				,ZIDTYPE__c = 'AT'      //ID種別
				,ZIDOWNER__c = 'AG'+idcpf1.ZIDOWNER__c      //所有者コード
				,ZINQUIRR__c = idcpf1.ZINQUIRR__c       //照会者コード
			);
			insert eidr;
		}
		return testuser;
	}

	//accessIRIS()テスト
	static testMethod void IPCheckTest(){
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		User partner = createPartnerDataCommon('E_PartnerCommunity');
		TestI_TestUtil.createAgencyPermissions(partner.Id);
		System.runAs(u){
			Account parentAcc = new Account(Name = 'testParent', E_CheckIPFlg__c = TRUE, E_WhiteIPAddress__c = null);
			insert parentAcc;
			account.ParentId = parentAcc.Id;
			update account;
			TestI_TestUtil.createAccountShare(account.Id, partner.Id);
		}

		System.runAs(partner){
			Test.startTest();
			E_AccessController access = E_AccessController.getInstance();
			Boolean result = access.accessIRIS();
			Test.stopTest();

			System.assertEquals(FALSE, result);
		}

	}

}