@isTest(SeeAllData=false)
public with sharing class TestE_DistributorLkupExtender {

	private static testMethod void testDistributorLkup() {
		//テストデータ作成
		Account acc = new Account();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'test', 'システム管理者');

		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();

			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(acc);
			E_DistributorLkupController controller = new E_DistributorLkupController(standardcontroller);
			E_DistributorLkupExtender extender = new E_DistributorLkupExtender(controller);
			controller.ipVal_dairitenkana.SkyEditor2__Text__c = 'ﾃｽﾄ';
			extender.presearch();
			//確認
			system.assertEquals(controller.ipVal_dairitenkana.SkyEditor2__Text__c, 'テスト');
			//テスト終了
			Test.stopTest();
		}
	}
}