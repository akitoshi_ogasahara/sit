/**
 * 保有契約異動通知ダウンロードクラス
 */
public with sharing class E_DownloadNoticeAGPOExtension {
	// Controller
	E_DownloadController controller;
	
	// 検索条件表示フラグ
	public Boolean isDispSearchArea {get; private set;}
	
	// 期間対象 ※ToDo 確認 : 既存ディスクレーマーで使用　不要の場合は削除
	public String startYearMonth {get; private set;}
	public String endYearMonth {get; private set;}
	
	// 事務所選択値
	public String[] selOfficeList {get; set;}
	
	// 期間選択リスト
	public String periodValue {get; set;}
	public List<SelectOption> periodOptions {get; set;}
	
	// タイトル
	public String getTitle(){
		return E_DownloadNoticeConst.E_AGPONM + 'ダウンロード';
	}
	
	/**
	 * Constructor
	 */
	public E_DownloadNoticeAGPOExtension(E_DownloadController controller){
		this.controller = controller;
	}
	
	/**
	 * PageAction
	 */
	public PageReference pageAction(){
		try{
			isDispSearchArea = true;
			
			//帳票設定
			controller.formNo = E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO;
			
			// パラメータによる実行者情報取得
			controller.isValidate = controller.getActor();
			
			// 期間選択リスト生成
			periodOptions = E_DownloadNoticeUtil.createPeriodOptions(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
/*			
			// 対象データ存在チェック
			isDispSearchArea = true;
			if(E_DownloadNoticeUtil.hasDownloadDatas(
										E_DownloadNoticeConst.OBJ_API_E_AGPO, 
										periodOptions[0].getValue(), 
										controller.distributor, 
										controller.office, 
										controller.agent
									) == false){
				controller.pageMessages.addErrorMessage(controller.getMSG().get('DND|002'));
				isDispSearchArea = false;
			}
*/	
			// 表示文言で使用
			startYearMonth = periodOptions[1].getValue();
			endYearMonth = periodOptions[3].getValue();
			
			// エラー判定
			if(controller.pageMessages.hasMessages()){
				isDispSearchArea = false;
			}
			
			return null;
			
		}catch(Exception e){
			isDispSearchArea = false;
			controller.pageMessages.addErrorMessage(e.getMessage());
			return null;
		}
	}
	
	/**
	 * CSVダウンロードボタン押下
	 */
	public PageReference doDownloadCsv(){
		String csvType = System.currentPageReference().getParameters().get('csvType');
		controller.pageMessages.clearMessages();
		//初期化
		controller.dh = new E_DownloadHistorry__c();
		
		// チェック処理
		if(isValidateSuccess(true) == false){
			return null;
		}

		controller = E_DownloadNoticeUtil.createDlHistory(controller ,selOfficeList ,periodValue ,true ,csvType);
		
		return null;
	}
	
	/**
	 * PDFダウンロードボタン押下
	 */
	public PageReference doDownloadPDF(){
		controller.pageMessages.clearMessages();
		//初期化
		controller.dh = new E_DownloadHistorry__c();
		
		// チェック処理
		if(isValidateSuccess(false) == false){
			return null;
		}
		
		controller = E_DownloadNoticeUtil.createDlHistory(controller ,selOfficeList ,periodValue ,false);

		return null;
	}

	/**
	 * チェック処理
	 * @param Boolean true:CSV, false:PDF
	 * @return Boolean true:エラーなし, false:エラーあり
	 */
	private Boolean isValidateSuccess(Boolean isCsv){
		Boolean hasErr = false;
		
		// 単月指定チェック
		if(isCsv == false){
			E_DownloadNoticeUtil.checkPeriodPDF(controller ,periodValue);
		}
		
		// エラー判定
		if(controller.pageMessages.hasMessages()){
			return false;
		}else{
			return true;
		}
	}
}