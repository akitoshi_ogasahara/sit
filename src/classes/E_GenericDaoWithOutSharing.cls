public without sharing class E_GenericDaoWithOutSharing {

	public static void insertRecord(sObject record) {
		insert record;
	}
	public static void insertRecord(List<sObject> record) {
		insert record;
	}

	public static void updateRecord(sObject record) {
		update record;
	}
	public static void updateRecord(List<sObject> record) {
		update record;
	}
	
	public static void upsertRecord(sObject record) {
		upsert record;
	}
	public static void upsertRecord(List<sObject> record) {
		upsert record;
	}
	
	public static void deleteRecord(sObject record) {
		delete record;
	}
	public static void deleteRecord(List<sObject> record) {
		delete record;
	}
}