@isTest
private class TestI_Util {
	static testMethod void compareToStringTest() {
		Test.startTest();
			system.assertEquals(I_Util.compareToString('', 'test'), 1);
			system.assertEquals(I_Util.compareToString('test', 'test'), 0);
			system.assertEquals(I_Util.compareToString('test', ''), -1);
		Test.stopTest();
	}

	static testMethod void compareToIntegerTest() {
		Test.startTest();
			system.assertEquals(I_Util.compareToInteger(10, 10), 0);
			system.assertEquals(I_Util.compareToInteger(10, 5), 1);
			system.assertEquals(I_Util.compareToInteger(5, 10), -1);
		Test.stopTest();
	}

	static testMethod void compareToDecimalTest() {
		Test.startTest();
			system.assertEquals(I_Util.compareToDecimal(1.5, 1.5), 0);
			system.assertEquals(I_Util.compareToDecimal(1.5, 1.0), 1);
			system.assertEquals(I_Util.compareToDecimal(1.0, 1.5), -1);
		Test.stopTest();
	}

	static testMethod void compareToDateTest() {
		Test.startTest();
			system.assertEquals(I_Util.compareToDate(Date.newInstance(2017, 4, 10), Date.newInstance(2017, 4, 10)),0);
			system.assertEquals(I_Util.compareToDate(Date.newInstance(2017, 4, 11), Date.newInstance(2017, 4, 10)), 1);
			system.assertEquals(I_Util.compareToDate(Date.newInstance(2017, 4, 10), Date.newInstance(2017, 4, 11)), -1);
		Test.stopTest();
	}


	static testMethod void compareToDateTimeTest() {
		Test.startTest();
			system.assertEquals(I_Util.compareToDateTime(Datetime.newInstance(2017, 4, 10), Datetime.newInstance(2017, 4, 10)),0);
			system.assertEquals(I_Util.compareToDateTime(Datetime.newInstance(2017, 4, 11), Datetime.newInstance(2017, 4, 10)), 1);
			system.assertEquals(I_Util.compareToDateTime(Datetime.newInstance(2017, 4, 10), Datetime.newInstance(2017, 4, 11)), -1);
		Test.stopTest();
	}

	static testMethod void getAutoSearchAgencyIdTest() {
		Test.startTest();
			system.assertEquals(I_Util.getAutoSearchAgencyId(), E_CookieHandler.getCookieAutoSearchAgency());
		Test.stopTest();
	}

	static testMethod void getAutoSearchOfficeIdTest() {
		Test.startTest();
			system.assertEquals(I_Util.getAutoSearchOfficeId(), E_CookieHandler.getCookieAutoSearchOffice());
		Test.stopTest();
	}

	static testMethod void getAutoSearchAgentIdTest() {
		Test.startTest();
			system.assertEquals(I_Util.getAutoSearchAgentId(), E_CookieHandler.getCookieAutoSearchAgent());
		Test.stopTest();
	}

	static testMethod void isRefineSearchFlaseTest() {
		Test.startTest();
			system.assertEquals(I_Util.isRefineSearch(), false);
		Test.stopTest();
	}

	static testMethod void isRefineSearchTrueTest() {
		Test.startTest();
			E_CookieHandler.setCookieAutoSearchAgency('test');
			system.assertEquals(I_Util.isRefineSearch(), true);
		Test.stopTest();
	}

	static testMethod void splitStringSpaceTest() {
		Test.startTest();
			List<String> testList = I_Util.splitStringSpace('test1 test2　test3');
			system.assertEquals(testList[0], 'test1');
			system.assertEquals(testList[1], 'test2');
			system.assertEquals(testList[2], 'test3');
		Test.stopTest();
	}

	static testMethod void createSearchkeywordListTest() {
		Test.startTest();
			List<String> testList = I_Util.createSearchkeywordList('テ ｽ　ト');
			system.assertEquals(testList[0], 'テ');
			system.assertEquals(testList[1], 'ﾃ');
			system.assertEquals(testList[2], 'ｽ');
			system.assertEquals(testList[3], 'ス');
			system.assertEquals(testList[4], 'ト');
			system.assertEquals(testList[5], 'ﾄ');
		Test.stopTest();
	}

	static testMethod void toStrTest() {
		String temp		= '';
		String v1		= null;
		Integer v2		= null;
		Long v3			= null;
		Double v4		= null;
		Decimal v5		= null;
		Boolean v6		= null;
		Date v7			= null;
		DateTime v8		= null;
		Id v9			= null;

		Test.startTest();
			temp = I_Util.toStr(v1);
			system.assertEquals(temp, '');
			temp = I_Util.toStr(v2);
			system.assertEquals(temp, '0');
			temp = I_Util.toStr(v3);
			system.assertEquals(temp, '0');
			temp = I_Util.toStr(v4);
			system.assertEquals(temp, '0');
			temp = I_Util.toStr(v5);
			system.assertEquals(temp, '0');
			temp = I_Util.toStr(v6);
			system.assertEquals(temp, 'false');
			temp = I_Util.toStr(v7);
			system.assertEquals(temp, '');
			temp = I_Util.toStr(v8);
			system.assertEquals(temp, '');
			temp = I_Util.toStr(v9);
			system.assertEquals(temp, '');

			v7	= Date.newInstance(1999, 12, 31);
			temp = I_Util.toStr(v7);
			system.assertEquals(temp, '1999/12/31');

			v8	= DateTime.newInstance(1999, 12, 31, 23, 58, 59);
			temp = I_Util.toStr(v8);
			system.assertEquals(temp, '1999/12/31 23:58:59');
		Test.stopTest();
	}

	static testMethod void toDecStrTest() {
		String temp		= '';
		Double v4		= null;
		Decimal v5		= null;

		Test.startTest();
			temp = I_Util.toDecStr(v4, 2);
			system.assertEquals(temp, '0.00');
			temp = I_Util.toDecStr(v5, 2);
			system.assertEquals(temp, '0.00');
		Test.stopTest();
	}

	static testMethod void toFormatStrTest() {
		String temp		= '';
		Integer v2		= 1000;
		Long v3			= 1000000;
		Double v4		= 1000000000;
		Decimal v5		= 1000000000000L;

		Test.startTest();
			temp = I_Util.toFormatStr(v2);
			system.assertEquals(temp, '1,000');
			temp = I_Util.toFormatStr(v3);
			system.assertEquals(temp, '1,000,000');
			temp = I_Util.toFormatStr(v4);
			system.assertEquals(temp, '1,000,000,000');
			temp = I_Util.toFormatStr(v5);
			system.assertEquals(temp, '1,000,000,000,000');
		Test.stopTest();
	}

}