/***
 *	  当ページはE_LoginControllerをベースに作成するため、I_AbstractControllerを継承していません
 *	  住生のログインはE_LoginSumisei　が使われる
 */
public with sharing class I_LoginController extends E_InfoController {
	private static final String MENU_KEY_AGENT = 'login_agent';
	private static final String MENU_KEY_CUSTOMER = 'login_customer';
	private static final String MENU_KEY_AGENT_INAPPROPRIATE = 'login_agent_notrecommended';


	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}

	//ブラウザ判定Handler
	public E_BrowserJudgeHandler browserHandler{get;set;}

	//ユーザ名
	public String userId {get;set;}
	//メールアドレス
	public String userMail {get;set;}
	//パスワード
	public String password {get;set;}
	//メニューキー
	public String menuKey {get;set;}
	// ハッシュ
	public String anchor {get; set;}

	/**
	 * ID自動発行フラグ
	 * true:仮PWログイン,ログイン時にID自動発行・10桁IDの付番を行う
	 * false:通常ログイン
	 */
//  private Boolean selfRegistrationFlg;
	public Boolean selfRegistrationFlg {get; set;}
	//ID自動発行時メールアドレス
	public String idUserMail {get;set;}
	//IRIS対応ブラウザフラグ
	public Boolean useIRISBrowserFlag {get; set;}

	/* スプリントバックログ16 ID管理_ログイン 170120 */
	//契約者ユーザロック
	public Boolean clientUserLock {get;set;}
	//契約者ユーザ以外ロック
	public Boolean otherUserLock {get;set;}
	/* 改修ここまで */

	//長期未仕様による非アクティブ
	public Boolean isInActive {get;set;}
	//90日未ログイン
	public Boolean isPassed90Days {get;set;}

	public I_LoginController() {
//	  System.debug('**************************IRIS_LoginController コンストラクタ');
		pgTitle = 'ログイン';
		pageMessages = getPageMessages();
		//ブラウザ
		useIRISBrowserFlag = iris.IRISRecommendBrowser();

		//仮PW発行後のメールのURLから遷移した場合、パラメータ'maillogin'が付与される
		String mailLogincheck = ApexPages.currentPage().getParameters().get('maillogin');

		//パラメータが有効(='1')の場合、ID自動発行判定
		if(mailLogincheck =='1'){
			selfRegistrationFlg = true;
		}else{
			selfRegistrationFlg = false;
		}
		clientUserLock = false;
		otherUserLock = false;
		isInActive =false;
		isPassed90Days=false;
	}

	//ログインフォーム表示時にTrue
	public Boolean canLogin{get; private set;}
	//ページアクション
	public PageReference pageAction(){
		canLogin = true;
		//ブラウザ判定クラス
		browserHandler = new E_BrowserJudgeHandler(getLoginPage());

		//たもつくんアクセス時はログインページを表示させる。
		if(access.isViaMobileSDK()){
			String startUrl = ApexPages.currentPage().getParameters().get('startUrl');
			if(String.isBlank(startUrl)){
				canLogin = false;
				pageMessages.addErrorMessage(getMSG().get('TMT|login06E'));
			}
			return null;
		}

		//非互換ブラウザならエラー画面へ
		if(E_Util.isUnsupportedBrowser()){
			return page.E_BrowserErr;
		}

		pageRef = doAuth(E_Const.ID_KIND.LOGIN, userinfo.getUserId());

		//doAuthの結果問題ない　かつ　住生IPアドレスからのアクセスの場合は、住生専用のログインページへ遷移させる		2017.10 たもつ君経由時はそのままIRISのログインページとする
		if(pageRef==null && E_Util.isIncludedIpRange(System.Label.E_SUMISEI_IP_WHITELIST) && (access.isViaMobileSDK()==false)){
			return Page.E_LoginSumisei;
		}
		return E_Util.toErrorPage(pageRef, null);
	}

	//[ログイン]ボタン
	public PageReference doLogin(){

		PageReference ret = null;
		pageMessages.clearMessages();

		//レコード取得用
		User userRec = null;

		/* スプリントバックログ16 ID管理_ログイン 170120 */
		clientUserLock = false;
		otherUserLock = false;
		/* 改修ここまで */
		isInActive =false;
		isPassed90Days=false;

		try {
			E_CookieHandler.clearCookieCompliance();

			//ログインパスワード
			String loginPassword = password;
			//ログインユーザ名
			String loginName = '';

			//ユーザレコード取得
			//メールアドレスとユーザIDが、片方もしくは両方入力されているときの処理
			if(String.isNotBlank(userId)){
				//ユーザIDにサフィックスをつける
				loginName = userId + System.Label.E_USERNAME_SUFFIX;
				//決定したログインユーザ名でユーザレコード取得
				userRec =  E_UserDaoWithout.getUserRecByUserName(loginName);
			}else if(selfRegistrationFlg){	  //ID自動発行メールから遷移したとき
				//Userオブジェクト取得query(childRelationshipでID管理取得)
				loginName = idUserMail + System.Label.E_USERNAME_SUFFIX;
				//リレーションでID管理オブジェクトとともに、ユーザレコードを取得
				userRec = E_UserDaoWithout.getUserRecByUserNameWithEIDC(loginName);
			}

			//メールアドレス及びユーザＩＤでユーザレコードが取得できなかった時のエラー処理
			if(userRec == null){
				// 長期未使用による無効常態かチェック
				if(isInActiveUser(loginName)){
					isInActive=true;
					//pageMessages.addErrorMessage(getMSG().get('LGE|008'));
					return Page.E_LoginAgent.setRedirect(false);
				}

				pageMessages.addErrorMessage(getMSG().get('LGE|004'));
				return Page.E_LoginAgent.setRedirect(false);
			}else{
				//ユーザロックチェック
				if (isLocked(userRec)) {
					return null;
				}
				if(!userRec.E_IDCPFs__r.isEmpty() && userRec.E_IDCPFs__r[0].ZSTATUS01__c == E_Const.PW_STATUS_EMERGENCYSTOP){
					pageMessages.addErrorMessage(getMSG().get('LGE|004'));
					return Page.E_LoginAgent.setRedirect(false);
				}
			}
			//N日間未ログインチェック 90日を想定  2017/1 PWは任意変更へ
			//保君の際は90日経過時も使用させる　2017.12 たもつ君
			if(isPassedDays(userRec) && !access.isViaMobileSDK()){
				//PageReference pr = Page.IRIS_ReissuePassword;
				//String loginPassDays = E_Const.PREFIX_TEMP_PASSWORD+E_Util.getRandomPassword(10);
				//E_SetPasswordWithout.setPassword(userRec.Id, loginPassDays);
				//pr.getParameters().put('isPassedDays', '1');
				//return pr;
				//　90日未ログインでエラーとしてログイン画面へ返るよう修正 2017-2-14
				//pageMessages.addErrorMessage(getMSG().get('LGE|007'));
				isPassed90Days=true;
				return Page.E_LoginAgent.setRedirect(false);
			}

			// 20171101 銀行代理店開放 START
			//インターネット経由
			if(!userRec.UserType.equals(E_Const.USERTYPE_STANDARD) && access.isViaInternet()){
				//モバイル経由ではない
				if(!access.isViaMobileSDK()){
					//銀行ユーザ or IPアドレスチェックフラグ:true
					Account office = userRec.Contact.Account;
					if(office.E_DoCheckIP__c && office.ParentId!=null){
						//IPアドレス判定
						if(!E_Util.ismatchIpRange(E_AccountDaoWithout.getParentAcc(office.ParentId).E_WhiteIPAddress__c)){
							pageMessages.addErrorMessage(getMSG().get('LGE|011'));
							return Page.E_LoginAgent.setRedirect(false);
						}
					}
				}
			}
			// 20171101 銀行代理店開放 END

			//MobileSDK経由のログインはEIDCがあるユーザのみ可能とする　　2017.12 たもつ君
			//  仮PWフラグTrueの場合もログイン不可とする。
			if(access.isViaMobileSDK()){
				if(!isExistsEIDC(userRec) || userRec.E_UseTempBasicPW__c){
					pageMessages.addErrorMessage(getMSG().get('TMT|login02E'));	//NN生命ホームページよりIRISへログインしたのち、再度たもつくんにてログインを実施してください。
					return Page.E_LoginAgent.setRedirect(false);
				}
			}

			//初回ログインの場合
			if(userRec.E_UseTempBasicPW__c){
				try{
					//ログイン仮パスワードにプレフィックスをつけ暗号化
					loginPassword = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, userRec.E_TempBasicPWModifiedDate__c);
				} catch(Exception e) {
					//暗号化エラーでもロック回数をカウントさせるためログイン処理をする
					ret = Site.login(loginName, loginPassword, Page.E_Compliance.getUrl());
					//5回ミスでロックの場合
					if (isLocked(userRec)) return Page.E_LoginAgent.setRedirect(false);

					//暗号化でエラー失敗　PWの入力ミスなのでログイン失敗のエラーメッセージを出力
					pageMessages.addErrorMessage(getMSG().get('LGE|004'));
					return Page.E_LoginAgent.setRedirect(false);
				}
			}else if(userRec.E_UseExistingBasicPW__c){
				//既存ユーザの場合
				//ログインパスワードにプレフィックスをつけ暗号化
				loginPassword = E_EncryptUtil.encrypt(password);
			}

			//ログイン  startUrlがある場合そちらのページへ遷移
			String startUrl = ApexPages.currentPage().getParameters().get('startUrl');
			if(String.isBlank(startUrl)){
				startUrl = Page.E_Home.getUrl();
			}
			ret = Site.login(loginName, loginPassword, startUrl);

			System.debug(ret);

			//ログイン後に再度ユーザロックチェック
			if (isLocked(userRec)) {
				return null;
			}

			//ログイン失敗　ID、パス不正
			if(ret==null){
				// 長期未使用による無効常態かチェック
				if(isInActiveUser(loginName)){
					isInActive=true;
					//pageMessages.addErrorMessage(getMSG().get('LGE|008'));
					return Page.E_LoginAgent.setRedirect(false);
				}
				//通常のログイン失敗
				pageMessages.addErrorMessage(getMSG().get('LGE|004'));
				return Page.E_LoginAgent.setRedirect(false);
			} else {

				String pageUrl = ret.getParameters().get('retURL');
				if (String.isNotBlank(pageUrl) && String.isNotBlank(this.anchor)) {
					try {
						// ハッシュが存在していなかった場合遷移先として保持しておいたアンカーを付与する
						if (pageUrl.indexOf('#') < 0) {
							pageUrl = pageUrl + this.anchor;
							ret.getParameters().put('retURL', pageUrl);
						}
					} catch(Exception e) {}
				}
			}

			//エラーメッセージをNNLinkコンポーネントに移します。
			System.debug('ApexPages=====' + ApexPages.getMessages());
			if(ApexPages.hasMessages()){
				pageMessages.addMessages(ApexPages.getMessages());
				return null;
			}

			//メールアドレス変更ページ遷移チェック
			//E_HomeControllerでチェック
/*
			if(String.isNotBlank(userId) && !E_Util.isMailAddress(userId.toUpperCase())) {
				//パラメータ作成
				String param = 'userID='+ userId +'&pass='+ passWord;
				String cryptoParam = E_EncryptUtil.getEncryptedString(param);
				ret = Page.E_updateEmail;
				ret.getParameters().put('param', cryptoParam);
			}
*/
			// 仮パスメールのURLから来ていない仮パスユーザが通常ログインしてEIDCがなければエラー
			if(!selfRegistrationFlg && userRec.E_UseTempBasicPW__c){
				if(!isExistsEIDC(userRec)){
					pageMessages.addErrorMessage(getMSG().get('LGE|005'));
					return null;
				}
			}
			//ID自動発行メールから遷移したとき、NNLinkIDを発行 発行完了したらPW変更画面へ自動で遷移
			if(selfRegistrationFlg && !isCreateNNLinkID(userRec)){
				return Page.E_LoginAgent.setRedirect(false);
			}
			//90日パスワード変更提示のチェック
			//E_AbstractController に移譲
//		  if(isSuggestPWChange(userRec)){
				//Site.Loginのreturn値のURL(https://[site-domain]/secur/frontdoor.jsp)にリダイレクトしないと、ログイン判定にならない
//			  return Site.login(loginName, loginPassword, Page.IRIS_SuggestChangePW.getUrl());
//		  }

			//たもつ君用ロジック　uuidをEIDCに更新する　2017.12
			if(access.isViaMobileSDK()){
				TMT_AppManager tmtApp = new TMT_AppManager(userRec.Id);
				tmtApp.putDeviceID();			//update EIDC
			}

		} catch(Exception e) {
			System.debug('Exception=====' + e);
//			pageMessages.addErrorMessage(e.getMessage() + e.getStackTraceString());
			pageMessages.addErrorMessage(e.getMessage());
			ret = null;
		}

		return ret;
	}

	//[パスワードをお忘れの方はこちら]ボタン
	public PageReference doReissue(){
		return Page.IRIS_ReissuePassword;
	}

	//ログインページを取得　　ブラウザチェック後の戻り先とする。
	private PageReference getLoginPage(){
		if(getMenuKey()==MENU_KEY_AGENT){
			return Page.E_LoginAgent;
		}else if(getMenuKey()==MENU_KEY_CUSTOMER){
			return Page.E_LoginCustomer;
		}else{
			return null;
		}
	}

	//メニューキーを返却
	protected override String getMenuKey(){
		if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_CLI)) {
			return MENU_KEY_CUSTOMER;   //'login_customer';
		} else if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_AGE)) {
			if(useIRISBrowserFlag){
				return MENU_KEY_AGENT;	  //'login_agent';
			}
			return MENU_KEY_AGENT_INAPPROPRIATE;
		}
		return '';
	}

	//ユーザロックチェック
	private Boolean isLocked(User userRec) {
		UserLogin userLoginRecBefore = E_UserLoginDaoWithout.getRecByUserId(userRec.id);
		if(userLoginRecBefore == null){
			String pathPrefix = Site.getPathPrefix();
			if (!String.isEmpty(pathPrefix)) {
				/* スプリントバックログ16 ID管理_ログイン 170120
				if (pathPrefix.indexOf(E_Const.NNLINK_CLI) > 0) {   //契約者
					pageMessages.addErrorMessage(getMSG().get('LGE|002'));
				} else {
					pageMessages.addErrorMessage(getMSG().get('LGE|003'));
				}
				*/
				/*
				if (pathPrefix.indexOf(E_Const.NNLINK_CLI) > 0) {   //契約者
					pageMessages.addErrorMessage(getMSG().get('LGE|002'));
					return true;
				}
				if (E_Util.isUserAgentIPhone()) {   //モバイル画面
					pageMessages.addErrorMessage(getMSG().get('LGE|003'));
				} else {							//PC画面
					pageMessages.addErrorMessage(getMSG().get('LGE|006'));
				}
				*/
				/* スプリントバックログ16 */

				/* スプリントバックログ16 ID管理_ログイン 170206 */
				if (pathPrefix.indexOf(E_Const.NNLINK_CLI) > 0) {   //契約者
					clientUserLock = true;
				} else {
					otherUserLock = true;
				}
				/* 改修ここまで */
				return true;
			}
		}
		return false;
	}

	/**
	 * N日未ログインチェックのメソッド(90日を想定)
	 * @param userRec ユーザレコード
	 * @return Boolean true:N日以上経っている、false:経っていない
	 */
	private Boolean isPassedDays(User userRec){
		Integer criteriaDays = 1000;
		try{
			criteriaDays = Integer.valueOf(System.Label.E_LASTLOGINDATE_LAPSEDDAYS);
		}catch(Exception e){
			//デフォルト1000日にしておく
		}

		//クライテリアの日数以上の時にTrue
		return E_Util.days_after_lastLogin(userRec) > criteriaDays;
	}

	/**
	 *	  PW変更を提示するかどうか
	 *	  E_AccessController に移譲
	 **/
	//public Boolean isSuggestPWChange(User userRec){
	//  //一般代理店ユーザ以外の場合、PW変更定時しない
	//  if(isNormalAgent(userRec)==false){
	//	  return false;
	//  }
	//  //前回変更日がnull
	//  if(userRec.LastPasswordChangeDate == null){
	//	  return false;
	//  }
	//  //カスタム表示ラベルよりパスワードの有効期間を取得
	//  Integer limitDays;
	//  try{
	//	  limitDays = Integer.valueOf(System.Label.E_PWLmitDays);
	//  }catch(Exception e){
	//	  return false;//数値変換できない場合（'-'など）は無期限とみなす
	//  }
	//  //前回変更日が90日以上前
	//  if(userRec.LastPasswordChangeDate.addDays(limitDays) <= datetime.now()){
	//	  return true;
	//  }
	//  return false;
	//}

	//private Boolean isNormalAgent(User user){
	//  E_IDCPF__c idcpf = E_IDCPFDaoWithout.getRecsByUserID(user.Id);
	//  return (isAgent(user)
	//		  && idcpf.ZSTATUS01__c == E_Const.ZSTATUS01_ENABLE
	//		  && String.isBlank(user.contact.E_MenuKindId__c));
	//}

	// EIDCが存在するか　存在する：true 存在しない：false
	@TestVisible
	private Boolean isExistsEIDC(User userRec){
		return E_IDCPFDaoWithout.getRecsByUserID(userRec.Id) != null;
	}

	@TestVisible
	private Boolean isInActiveUser(String loginName){
		List<E_IDRequest__c> eidrs = E_IDRequestDaoWithout.getRecByUserName(loginName+'%');
		if(eidrs.isEmpty()){return false;}

		if(eidrs[0].E_IDCPF__r.User__r.IsActive==False 
			&& (eidrs[0].E_IDCPF__r.ZSTATUS01__c==E_Const.PW_STATUS_UNUSEDSTOP 
				|| (eidrs[0].ZUPDFLAG__c==E_Const.ZUPDFLAG_D
				&& eidrs[0].E_IDCPF__r.ZSTATUS01__c==E_Const.PW_STATUS_OK))){
			return true;
		}

		return false;
	}

	/**
	 * 代理店（ユーザタイプ）フラグ
	 */
	public boolean isAgent(User user){
		if (user != null && user.UserType.equals(E_Const.USERTYPE_POWERPARTNER)) {
			return true;
		}
		return false;
	}

	/**
	 * NNLinkIDを発行
	 * ユーザを更新、ID管理・IDリクエストを作成
	 * @param  userRec ユーザレコード
	 * @return Boolean true:成功
	 */
	@TestVisible
	private Boolean isCreateNNLinkID(User userRec){
		/**
		 * ID自動発行→ユーザ更新のループ
		 * ID管理・IDリクエストの作成もループ内で行う
		 * (ID管理.ID番号はID管理内で一意,insert失敗時にロールバックするため)
		 */
		//ID管理レコードが既に存在する場合はエラー
		if(isExistsEIDC(userRec)){
			pageMessages.addErrorMessage(getMSG().get('CID|006'));
			return false;
		}

		//セーブポイント
		Savepoint sp = Database.setSavepoint();
		//失敗したらリトライ（10回までとする）
		for(Integer i=0; i<10; i++){
			//10桁ID生成
			String tenId = E_createNNLinkID.createNNLinkID();
			System.debug('チェック回数='+i);
			try{
				//ユーザ情報を更新
				userRec.CommunityNickname = String.valueOf(userRec.Id); //ニックネーム
				userRec.E_ZWEBID__c = tenId;	//NNLinkユーザID

				//ID管理を新規登録
				E_IDCPF__c eidc = createEidcRec(userRec);   //ID管理を作成
				E_IDCPFDaoWithout.insertRec(eidc);

				//代理店の自動契約移管判定フラグ="Y"のときは、IDリクエストを作成しない
				if(userRec.Contact.Account.ParentID!=null){
					//if(userRec.Contact.Account.Parent.ZAUTOTRF__c != E_Const.ZAUTOTRF_Y){	//20171208 EIDR作成ロジック修正のため、コメントアウト
						//IDリクエストを新規登録
						E_IDRequest__c eidr = createEidrRec(eidc);  //IDリクエストを作成
						E_IDRequestDaoWithout.insertRec(eidr);
					//}
				}

				//usertriggerhandlerで一般代理店のプロファイルを更新する際に、一般代理店判定でID管理を使用するため、
				//ID管理を作成してからユーザ情報を更新する
				E_UserDaoWithout.updateRec(userRec);

				break;	  //成功したら抜ける
			}catch(DmlException e){
				System.debug(e);
				Database.rollback(sp);  //ロールバック
				//処理失敗時にリトライ（10回目のエラーはThrow）
				if(i < 9 && e.getMessage().contains('duplicate value found: E_ZWEBID__c')) continue;
				throw e;
			}
		}
		//更新が完了したらtrueを返す
		return true;
	}

	/**
	 * ID管理を作成
	 * @param userRec [ユーザレコード]
	 */
	private E_IDCPF__c createEidcRec(User userRec){
		E_IDCPF__c eidc = new E_IDCPF__c(
			User__c = userRec.Id									//ユーザ
			,ZEMAILAD__c = userRec.EMAIL							//E-MAILアドレス
			,ZIDOWNER__c = E_Const.ZIDOWNER_AG+userRec.Contact.E_CL3PF_AGNTNUM__c   //所有者コード
			,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
			,ZINQUIRR__c = E_Const.ZINQUIRR_L3+userRec.Contact.E_CL3PF_AGNTNUM__c   //照会者コード
			,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
			,ZWEBID__c = userRec.E_ZWEBID__c						//ID番号
			,OwnerId = userRec.ID									//所有者ID
			,FLAG04__c = '1'										//顧客検索フラグ
			,FLAG01__c = '1'								//ID適正化
			,ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS				//利用システムフラグ

			//ID適正化 初期値設定
			,ZDSPFLAG01__c = '0'
			,ZDSPFLAG03__c = '0'
			,ZDSPFLAG04__c = '0'
			,ZDSPFLAG05__c = '0'
			,ZDSPFLAG06__c = '0'
			,ZDSPFLAG07__c = '0'
			,ZDSPFLAG08__c = '0'
			,ZDSPFLAG09__c = '0'
			,ZDSPFLAG10__c = '0'		);
		if(useIRISBrowserFlag){
			eidc.AppMode__c = I_Const.APP_MODE_IRIS;
			//setUseIRIS();
		}else{
			eidc.AppMode__c = I_Const.APP_MODE_NNLINK;
			//setUseNNLink();
		}
		return eidc;
	}

	/**
	 * IDリクエストを作成
	 * @param eidc [ID管理レコード]
	 */
	@TestVisible
	private E_IDRequest__c createEidrRec(E_IDCPF__c eidc){
		E_IDRequest__c eidr = new E_IDRequest__c(
			E_IDCPF__c = eidc.ID				//ID管理
			,ZUPDFLAG__c = E_Const.ZUPDFLAG_I   //リクエスト種別
			,ZWEBID__c = eidc.ZWEBID__c		 //ID番号
			,ZIDTYPE__c = eidc.ZIDTYPE__c	   //ID種別
			,ZIDOWNER__c = eidc.ZIDOWNER__c	 //所有者コード
			,ZINQUIRR__c = eidc.ZINQUIRR__c	 //照会者コード
			,ZEMAILAD__c = eidc.ZEMAILAD__c //E-MAILアドレス
		);
		return eidr;
	}


	//============	2017.12 たもつくん対応 START	=============//
	public Boolean getViaMobileSDK(){
		//return access.isViaMobileSDK() || ApexPages.currentPage().getParameters().get('tmtstyle')=='1';			//ブラウザでたもつくんUI確認する場合は、?tmtstyle=1を追加
		return access.isViaMobileSDK();
	}
	//============	2017.12 たもつくん対応  END	=============//


	/**
	 *		Debug String
	 *			Page情報を取得
	 */
	public String getPageParameterStrings(){
/*
		String ret = '【Headers】<br/>';
		ret += JSon.serialize(ApexPages.currentPage().getHeaders());
		ret += '<br/><br/>【Parameters】<br/>';
		ret += JSon.serialize(ApexPages.currentPage().getParameters());

		return ret;
*/
		return null;
	}
}