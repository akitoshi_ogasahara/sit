@isTest
private class TestI_LicenseAssessmentController {

	/**
	 * TypeAのテスト
	 */
	@isTest static void typeATest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 資格査定詳細情報の作成
		E_LicenseAssessment__c licenseAssessment = TestE_LicenseAssessmentDao.createLicenseAssessment(agencySalesResults.Id);
		insert licenseAssessment;

		Test.startTest();

		I_LicenseAssessmentController licenseAssessmentController = new I_LicenseAssessmentController();
		licenseAssessmentController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID
		licenseAssessmentController.paramQualif	= agencySalesResults.XHAH_AGCLS__c;	// 現在資格

		// JS用値設定の取得
		licenseAssessmentController.getJSParameter();
		System.assertEquals('license-assessment-acha', licenseAssessmentController.filteringType);

		// JS用値設定の取得(リモートアクション)
		String json = I_LicenseAssessmentController.getJSParam(licenseAssessmentController.paramSalesResultsId, licenseAssessmentController.paramQualif);
		System.assertNotEquals(0, json.length());

		// 表示判定
		System.assertEquals(true, licenseAssessmentController.isShow);

		// 選択テーブルID
		System.assertEquals('license-assessment-acha', licenseAssessmentController.targetId);

		// 表示ディスクレーマ(表示用)
		Map<String, String> disclaimers = licenseAssessmentController.disclaimers;
		System.assertEquals('', disclaimers.get('AMS|SR|004'));

		// 全角変換
		licenseAssessmentController.paramQualif = '優績ｓ';
		System.assertEquals('優績S', licenseAssessmentController.paramQualif);

	}

	/**
	 * getNumberTextのテスト
	 */
	@isTest static void getNumberTextTest() {

		// 基準/状況 文字列へ変換
		List<String> temps	= new List<String>();
		Decimal standard	= 0;
		Decimal state		= 0;
		String temp			= '';

		Test.startTest();

		I_LicenseAssessmentController licenseAssessmentController = new I_LicenseAssessmentController();

		standard	= 1000000000000L;
		state		= -900000000000L;
		temps = licenseAssessmentController.getNumberText(0, standard, state);
		System.assertEquals('1兆円以上', temps[0]);
		System.assertEquals('あと900,000,000,000円', temps[1]);

		standard	= 100000000000L;
		state		= 150000000000L;
		temps = licenseAssessmentController.getNumberText(0, standard, state);
		System.assertEquals('1,000億円以上', temps[0]);
		System.assertEquals('達成', temps[1]);

		standard	= 10000000L;
		state		= -1L;
		temps = licenseAssessmentController.getNumberText(0, standard, state);
		System.assertEquals('1,000万円以上', temps[0]);
		System.assertEquals('あと1円', temps[1]);

		standard	= 6;
		state		= 9;
		temps = licenseAssessmentController.getNumberText(2, standard, state);
		System.assertEquals('6ヵ月以上', temps[0]);
		System.assertEquals('達成', temps[1]);

		standard	= 9;
		state		= -6;
		temps = licenseAssessmentController.getNumberText(2, standard, state);
		System.assertEquals('9ヵ月以上', temps[0]);
		System.assertEquals('あと6ヵ月', temps[1]);

		standard	= 90;
		state		= -1.1;
		temps = licenseAssessmentController.getNumberText(3, standard, state);
		System.assertEquals('90.0%以上', temps[0]);
		System.assertEquals('- 1.1%', temps[1]);

		standard	= 50;
		state		= 30;
		temps = licenseAssessmentController.getNumberText(3, standard, state);
		System.assertEquals('50.0%以上', temps[0]);
		System.assertEquals('達成', temps[1]);

		temp =  licenseAssessmentController.round(123.45, 0);
		System.assertEquals('+ 123', temp);

		Test.stopTest();

	}

}