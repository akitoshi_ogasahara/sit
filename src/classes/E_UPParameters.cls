public class E_UPParameters {
	
	/**
	 * 商品コード暗号化
	 * @param prdCode: 商品コード（暗号化前）
	 * @return String 暗号化した商品コード
	 */
	public static String prdCodeEncrypt(String prdCode) {
		return E_EncryptUtil.prdCodeConversion(prdCode, true);
	}
	
	/**
	 * 商品コード複合化
	 * @param encryptedPrdCode: 暗号化されている商品コード
	 * @return String 複合化した商品コード
	 */
	public static String prdCodeDecrypt(String encryptedPrdCode) {
		return E_EncryptUtil.prdCodeConversion(encryptedPrdCode, false);
	}
	
	

	/**	******************
	 * ページ遷移時のパラメータ
	 *	****************** */
	private static final String UP_PARAM_KEY = 'upterms';

	/**
	 * ページ遷移時のパラメータ（検索条件情報）の追加  Headerに追加してリクエストする
	 * @param 	pageReference: 遷移先PageReference型
	 *			UnitPriceDownloadCondi:　検索条件情報
	 * @return パラメータ追加されたPageReference
	 */
	public static PageReference putDownloadCondi(PageReference pg
												, E_DownloadCondition.UnitPriceDownloadCondi dlCondi){
		pg.getParameters().put(UP_PARAM_KEY, dlCondi.toJSON());
		
		return pg;
	}

	/**
	 * ページ遷移時のパラメータ（検索条件情報）の取得
	 * @param
	 * @return UnitPriceDownloadCondi
	 */
	public static E_DownloadCondition.UnitPriceDownloadCondi getUnitPriceDownloadCondi(){
		E_DownloadCondition.UnitPriceDownloadCondi dlCondi = new E_DownloadCondition.UnitPriceDownloadCondi();
		return dlCondi.getInstanceByJSON(ApexPages.currentPage().getParameters().get(UP_PARAM_KEY));
	}
}