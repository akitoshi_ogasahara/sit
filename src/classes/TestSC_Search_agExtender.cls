@isTest
private class TestSC_Search_agExtender {
	// コンストラクタ
	private static testMethod void SearchAgTest001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			Test.setCurrentPage(Page.E_SCSearch_ag);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
			SC_Search_agSVEController controller = new SC_Search_agSVEController(standardcontroller);
			SC_Search_agExtender extender = new SC_Search_agExtender(controller);
		Test.stopTest();
	}

	// init
	private static testMethod void SearchAgTest002(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			Test.setCurrentPage(Page.E_SCSearch_ag);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
			SC_Search_agSVEController controller = new SC_Search_agSVEController(standardcontroller);
			SC_Search_agExtender extender = new SC_Search_agExtender(controller);

			extender.init();
		Test.stopTest();
	}
}