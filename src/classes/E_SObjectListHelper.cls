/**
 *	SObjectのリスト系VF Componentのコントローラクラス
 */
public with sharing class E_SObjectListHelper {

	/**
	 *		削除　処理
	 */
	// 削除ターゲットレコード(apex:param経由で取得) 
	public String delRecordId{get;set;}
	public String delSObjectName{get;set;}

	public PageReference doDelete(){
		try{
			String soql = 'select id from ';
			soql += delSObjectName;
			soql += ' WHERE Id = :delRecordId ';
			SObject rec = Database.query(soql);
			system.assert(rec!=null, rec);
			
			delete rec;
		}catch(Exception e){
			E_PageMessagesHolder.getInstance().addErrorMessage('レコードの削除に失敗しました。  ' + e.getMessage());
		}

		//ページ遷移なし　サーバサイドRedirectとする
		PageReference pr = ApexPages.currentPage();
		pr.setRedirect(true);
		return pr;
	}
}