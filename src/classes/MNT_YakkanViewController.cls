global with sharing class MNT_YakkanViewController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	public MNT_YakkanViewExtender getExtender() {return (MNT_YakkanViewExtender)extender;}
	public yakkanFileViewList yakkanFileViewList {get; private set;}
	{
	setApiVersion(42.0);
	}
	public MNT_YakkanViewController(ApexPages.StandardController controller) {
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.InsType__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.AgencyCode__c;
		f = I_ContentMaster__c.fields.CrCode__c;
		f = I_ContentMaster__c.fields.ContractDateFrom__c;
		f = I_ContentMaster__c.fields.GuideAgreeDiv__c;
		f = I_ContentMaster__c.fields.ContractDateTo__c;
		f = I_ContentMaster__c.fields.isSitePublishFlg__c;
		f = I_ContentMaster__c.fields.YakkanType__c;
		f = I_ContentMaster__c.fields.filePath__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_YakkanViewController';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('FormNo__c');
			mainQuery.addFieldAsOutput('DisplayFrom__c');
			mainQuery.addFieldAsOutput('InsType__c');
			mainQuery.addFieldAsOutput('ValidTo__c');
			mainQuery.addFieldAsOutput('AgencyCode__c');
			mainQuery.addFieldAsOutput('CrCode__c');
			mainQuery.addFieldAsOutput('ContractDateFrom__c');
			mainQuery.addFieldAsOutput('GuideAgreeDiv__c');
			mainQuery.addFieldAsOutput('ContractDateTo__c');
			mainQuery.addFieldAsOutput('isSitePublishFlg__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			yakkanFileViewList = new yakkanFileViewList(new List<I_ContentMaster__c>(), new List<yakkanFileViewListItem>(), new List<I_ContentMaster__c>(), null);
			listItemHolders.put('yakkanFileViewList', yakkanFileViewList);
			query = new SkyEditor2.Query('I_ContentMaster__c');
			query.addFieldAsOutput('YakkanType__c');
			query.addFieldAsOutput('filePath__c');
			query.addWhere('ParentContent__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('yakkanFileViewList', 'ParentContent__c');
			yakkanFileViewList.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('yakkanFileViewList', query);
			registRelatedList('ChildContents__r', 'yakkanFileViewList');
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_YakkanViewExtender(this);
			init();
			yakkanFileViewList.extender = this.extender;
			if (record.Id == null) {
				saveOldValues();
			}

			extender.init();
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class yakkanFileViewListItem extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		yakkanFileViewListItem(yakkanFileViewList holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class yakkanFileViewList extends SkyEditor2.ListItemHolder {
		public List<yakkanFileViewListItem> items{get; private set;}
		@TestVisible
			yakkanFileViewList(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<yakkanFileViewListItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new yakkanFileViewListItem(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}