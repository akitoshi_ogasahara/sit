@isTest
public class TestE_RepresentativeDaoWithout {

	/**
	 * getRecsByAsrIdのテスト
	 */
	@isTest static void getRecsByAsrIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 法人代表者/責任者の作成
		List<E_Representative__c> representative = createRepresentative(agencySalesResults.Id);
		insert representative;

		// テスト
		Test.startTest();
		List<E_Representative__c> recs = E_RepresentativeDaoWithout.getRecsByAsrId(agencySalesResults.Id);
		System.assertEquals(agencySalesResults.Id, recs[0].E_AgencySalesResults__c);
		Test.stopTest();

	}


	/**
	 * 法人代表者/責任者の作成
	 */
	public static List<E_Representative__c> createRepresentative(Id asrId){

		List<E_Representative__c> representative = new List<E_Representative__c>();

		E_Representative__c rec = new E_Representative__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
//		rec.Hierarchy__c				= '法人代表者 01';		// 階層
		rec.Name						= '法人代表者 01';		// 法人代表者/責任者No
		rec.Category__c					= '法人代表者';			// カテゴリー
		rec.CnvsLicense__c				= '合';					// 募集資格
		rec.XHAT_AGTYPE__c				= '';					// 募集人区分
		rec.XHAT_KQUALFLG__c			= '';					// 募集資格コード
		rec.XHAT_ZAGMANGR__c			= '';					// 業務管理責任者コード
		rec.XHAT_ZAGEDUCT__c			= '';					// 教育責任者コード
		rec.Agent__c					= '1234';				// 募集人コード
		representative.add(rec);

		rec = new E_Representative__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
//		rec.Hierarchy__c				= '法人代表者 02';		// 階層
		rec.Name						= '法人代表者 02';		// 法人代表者/責任者No
		rec.Category__c					= '法人代表者';			// カテゴリー
		rec.CnvsLicense__c				= '合';					// 募集資格
		rec.XHAT_AGTYPE__c				= '';					// 募集人区分
		rec.XHAT_KQUALFLG__c			= '';					// 募集資格コード
		rec.XHAT_ZAGMANGR__c			= '';					// 業務管理責任者コード
		rec.XHAT_ZAGEDUCT__c			= '';					// 教育責任者コード
		rec.Agent__c					= '1235';				// 募集人コード
		representative.add(rec);

		rec = new E_Representative__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
//		rec.Hierarchy__c				= '営業管理責任者 01';	// 階層
		rec.Name						= '営業管理責任者 01';	// 法人代表者/責任者No
		rec.Category__c					= '営業管理責任者';		// カテゴリー
		rec.CnvsLicense__c				= '合';					// 募集資格
		rec.XHAT_AGTYPE__c				= '';					// 募集人区分
		rec.XHAT_KQUALFLG__c			= '';					// 募集資格コード
		rec.XHAT_ZAGMANGR__c			= '';					// 業務管理責任者コード
		rec.XHAT_ZAGEDUCT__c			= '';					// 教育責任者コード
		rec.Agent__c					= '2345';				// 募集人コード
		representative.add(rec);

		rec = new E_Representative__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
//		rec.Hierarchy__c				= '教育管理責任者 01';	// 階層
		rec.Name						= '教育管理責任者 01';	// 法人代表者/責任者No
		rec.Category__c					= '教育管理責任者';		// カテゴリー
		rec.CnvsLicense__c				= '合';					// 募集資格
		rec.XHAT_AGTYPE__c				= '';					// 募集人区分
		rec.XHAT_KQUALFLG__c			= '';					// 募集資格コード
		rec.XHAT_ZAGMANGR__c			= '';					// 業務管理責任者コード
		rec.XHAT_ZAGEDUCT__c			= '';					// 教育責任者コード
		rec.Agent__c					= '3456';				// 募集人コード
		representative.add(rec);

		return representative;
	}

}