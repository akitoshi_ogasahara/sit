@isTest(SeeAllData=false)
public with sharing class TestE_StandardPasswordChangeExtender {

	private static User testuser;
	private static E_IDCPF__c idcpf;
	private static Account account;
	private static Contact contact;
	private static RecordType recordType;
	private static E_MenuKind__c menuKind;
	private static datetime nowDateTime = system.now();
	private static String pass = 'Pass0001';
	static void init(){
		insertE_bizDataSyncLog();
		insertMessage();
		insertEmail();
		system.debug('this is init');
	}

	//レコード1件の正常系
	private static testMethod void testStandard() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		system.setPassword(testuser.id, pass);
		String disclaimer;
		String pageType;
		String servicePolicy;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//disclaimer = extender.getHeadDisclaimer();
			pageType = extender.getPageType();
			servicePolicy = extender.getInternetServicePolicy();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'pass0002@test.co.jp';
			extender.newConfirmMail = 'pass0002@test.co.jp';
			extender.doSave();
			pref = extender.pageaction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//system.assertEquals('LGN|202', disclaimer);
		system.assertEquals('', pageType);
		system.assertEquals('internet_service_policy_contractant', servicePolicy);
		System.assertEquals(null, pref);
	}

	//doSaveテスト
	//エラー確認 パスワード入力値なし
	private static testMethod void testDoSave01() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		system.setPassword(testuser.id, 'Pass0001');
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//入力値なしの時
			pref = extender.doSave();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//doSaveテスト
	//エラー確認 現在パスワード誤り
	private static testMethod void testDoSave02() {
		init();
		createPartnerDataCommon('PERSONAコミュニティユーザ') ;
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		Test.startTest();
		system.runAs(testuser){
			//system.setPassword(testuser.id, 'pass0001');
			system.debug('change=====>' + Apexpages.getMessages());
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//現在のパスワードを間違えているとき
			extender.nowPassword = 'failpass';
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			pref = extender.doSave();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
/*
		System.assertEquals(null, pref);
*/

	}

	//doSaveテスト
	//エラー確認 新規パスと新規パス（確認）が不一致
	private static testMethod void testDoSave03() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		system.setPassword(testuser.id, pass);
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//確認パスワードが一致せず、かつパスワードが変更されていないとき
			extender.nowPassword = pass;
			extender.newPassword = pass;
			extender.newconfirmpassword = 'pass0003';
			pref = extender.doSave();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//doSaveテスト
	//エラー確認 パスワードが全角
	private static testMethod void testDoSave04() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		system.setPassword(testuser.id, pass);
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//確認パスワードが一致せず、かつパスワードが変更されていないとき
			extender.nowPassword = pass;
			extender.newPassword = 'あああああああ９';
			extender.newconfirmpassword = 'あああああああ９';
			pref = extender.doSave();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//doSaveテスト
	//通常モード 非契約者ユーザ
	//既存基本PW使用
	private static testMethod void testDoSave05() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		system.setPassword(testuser.id, pass);
		//既存基本PW使用フラグを有効化
		testuser.E_UseExistingBasicPW__c = true;
		update testUser;
		String disclaimer;
		String pageType;
		String servicePolicy;
		Test.startTest();
		PageReference pr;
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//disclaimer = extender.getHeadDisclaimer();
			pageType = extender.getPageType();
			servicePolicy = extender.getInternetServicePolicy();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'pass0002@test.co.jp';
			extender.newConfirmMail = 'pass0002@test.co.jp';
			pr = extender.doSave();
//			pref = extender.pageaction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//system.assertEquals('LGN|202', disclaimer);
//		System.assertEquals('/apex/E_AcceptanceCompletion?type=ACC%7C004&conid='+ testuser.Contact.Id, pr.getUrl());
		system.assertEquals('', pageType);
		system.assertEquals('internet_service_policy_contractant', servicePolicy);
//		System.assertEquals(null, pref);
	}

	//doSaveテスト
	//ApexPage.Messageにメッセージがある
	private static testMethod void testDoSave06() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		system.setPassword(testuser.id, pass);
		String disclaimer;
		String pageType;
		String servicePolicy;
		Test.startTest();
		PageReference pr;
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//disclaimer = extender.getHeadDisclaimer();
			pageType = extender.getPageType();
			servicePolicy = extender.getInternetServicePolicy();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'pass0002@test.co.jp';
			extender.newConfirmMail = 'pass0002@test.co.jp';
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Your Password cannot equal or contain your user name.'));
			pr = extender.doSave();
//			pref = extender.pageaction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//system.assertEquals('LGN|202', disclaimer);
		System.assertEquals(null, pr);
		system.assertEquals('', pageType);
		system.assertEquals('internet_service_policy_contractant', servicePolicy);
//		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//初回モード 非代理店ユーザ
	private static testMethod void testPageAction01() {
		pass = '2345678901';
		init();
		createDataCommon();
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'fstest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//初回モード 代理店ユーザ
	private static testMethod void testPageAction02() {
		pass = '2345678901';
		init();
//		createDataCommon();
		createPartnerDataCommon('E_PartnerCommunity');
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'fstest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//初回モード 代理店ユーザ
	private static testMethod void testPageAction03() {
		pass = '2345678901';
		init();
//		createDataCommon();
		createPartnerDataCommon('E_PartnerCommunity');
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'fstest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//有効期限切れモード
	private static testMethod void testPageAction04() {
		pass = '2345678901';
		init();
		createDataCommon();
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			//有効期限切れモードを有効にする
			extender.isInitialMode = false;
			extender.isExpiredMode = true;
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'fstest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals('expiredLogin', pageType);
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//通常モード以外 入力チェック1
	private static testMethod void testPageAction05() {
		pass = '2345678901';
		init();
		createDataCommon();
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.doSave();
			pref = extender.pageaction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//通常モード以外 入力チェック2
	private static testMethod void testPageAction06() {
		pass = '2345678901';
		init();
		createDataCommon();
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'あass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'stest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageaction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//ID自動発行テスト 仮基本PW使用
	//初回モード 非代理店ユーザ
	private static testMethod void testPageAction07(){
		pass = '2345678901';
		init();
		createDataIDCommon();
		//仮パスワード設定
		String tempPass = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(pass, idcpf.ZPASSWD02DATE__c);
		system.setPassword(testuser.id, tempPass);
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		//testuser = E_UserDao.getUserRecByUserId('005p00000010hd6');
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'Pass0002';
			extender.newconfirmpassword = 'Pass0002';
			extender.newMail = 'fstest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			System.debug('testCreateIDvalidation');
		}
		Test.stopTest();
//		System.assertEquals('initAutoCrIDLogin', pageType);
		System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//ID自動発行テスト 仮基本PW使用
	//初回モード 非代理店ユーザ
	//仮PW認証失敗エラー
	private static testMethod void testPageAction08(){
		pass = '2345678901';
		init();
		createDataIDCommon();
		//仮パスワード設定
		String tempPass = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(pass, idcpf.ZPASSWD02DATE__c);
		system.setPassword(testuser.id, tempPass);
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		//testuser = E_UserDao.getUserRecByUserId('005p00000010hd6');
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			pass = '1234567890';
			extender.nowPassword = pass;
			extender.newPassword = 'Pass0002';
			extender.newconfirmpassword = 'Pass0002';
			extender.newMail = 'fstest@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@terrasky.ingtesting';
			extender.doSave();
			System.debug('testCreateIDvalidation');
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
//		System.assertEquals('initAutoCrIDLogin', pageType);
		System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}

	//pageActionテスト
	//初回モード 非代理店ユーザ
	//メールアドレスの形式が不正
	private static testMethod void testPageAction09() {
		pass = '2345678901';
		init();
		createDataCommon();
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'fstest@test@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@test@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}
/*
	//pageActionテスト
	//初回モード 非代理店ユーザ
	//パスワードが
	private static testMethod void testPageAction10() {
		pass = '2345678901';
		init();
		createDataCommon();
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pageType = extender.getPageType();
			extender.nowPassword = pass;
			extender.newPassword = 'pass0002';
			extender.newconfirmpassword = 'pass0002';
			extender.newMail = 'fstest@test@terrasky.ingtesting';
			extender.newConfirmMail = 'fstest@test@terrasky.ingtesting';
			extender.doSave();
			pref = extender.pageAction();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		//System.assertEquals('initLogin', pageType);
		System.assertEquals(null, pref);
	}
*/
	//キャンセルボタン
	//通常モード 非契約者ユーザ
	//レコード1件の正常系
	private static testMethod void testDoReturn01() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();

			pref = extender.doReturn();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(Page.E_Home.getURL(), pref.getURL());
	}

	//キャンセルボタン
	//通常モード 契約者ユーザ
	private static testMethod void testDoReturn02() {
		pass = '2345678901';
		init();
//		createDataCommon();
		createPartnerDataCommon('E_CustomerCommunity');
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
//		testuser.E_UseTempBasicPW__c = true;
//		update testuser;
		String pageType;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			pref = extender.doReturn();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals('/apex/e_contactinfo?id='+ contact.Id, pref.getURL());
	}

	//キャンセルボタン
	//初回モード 代理店ユーザ
	private static testMethod void testDoReturn03() {
		pass = '2345678901';
		init();
		createPartnerDataCommon('E_PartnerCommunity');
		//インスタンス
		PageReference pref = Page.E_StandardPasswordChange;
		Test.setCurrentPage(pref);
		testuser.E_UseTempBasicPW__c = true;
		update testuser;
		String pageType;
		String servicePolicy;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_StandardPasswordChangeController controller = new E_StandardPasswordChangeController(standardcontroller);
			E_StandardPasswordChangeExtender extender = controller.getExtender();
			servicePolicy = extender.getInternetServicePolicy();
			pref = extender.doReturn();
			//pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		system.assertEquals('internet_service_policy_agency', servicePolicy);
	}




	/* test data */
	static void createDataCommon(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){

			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
			);
			insert testuser;
			// Account
			account = new Account(Name = 'testAccount');
			insert account;

			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id);
			insert contact;

			idcpf = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = testuser.Id,
				ZPASSWD02DATE__c = '99999999',
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207', // terrasky
				ZDSPFLAG02__c = '0'
			);
			insert idcpf;


		}
	}

	/* partner test data */
	static void createPartnerDataCommon(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			userrole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
			//having role user
			User roleUser = new User(
				Lastname = 'roleuser'
				, Username = 'roleuser@terrasky.ingtesting'
				, Email = 'roleuser@terrasky.ingtesting'
				, ProfileId = profile.Id
				, UserRoleId = portalRole.id
				, Alias = 'roleuser'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='roleuser'
			);
			insert roleuser;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '一般代理店（インターネット）');
			insert menuKind;

			// Account
//			account = new Account(Name = 'testAccount',ownerid = roleuser.id);
			account = new Account(Name = 'testAccount',ownerid = roleuser.id, E_MenuKind__c = menuKind.Id);
			insert account;

			// Contact
			contact = new Contact(LastName = 'fstest', FirstName = 'firstName', AccountId = account.Id,email = 'fstest@terrasky.ingtesting',ownerid = roleuser.id);
			insert contact;

			// User
			if(profilename != null){
				profile = [Select Id, Name,usertype From Profile Where Name = :profileName Limit 1];
			}
			testuser = new User(
				Lastname = 'fstest'
				, FirstName = 'firstName'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
				, contactID = contact.id
			);
			insert testuser;

			idcpf = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = testuser.Id,
				ZSTATUS01__c = '1',
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207', // terrasky
				ownerid = testuser.id,
				ZDSPFLAG02__c = '0'
			);
			insert idcpf;
		}
	}

	/* ID発行test data */
	static void createDataIDCommon(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
				,E_ZWEBID__c = '5000000000'
			);
			insert testuser;
			// Account
			account = new Account(Name = 'testAccount');
			insert account;

			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id,E_CL3PF_AGNTNUM__c = 'Z1111');
			insert contact;
/*
			E_IDCPF__c eidc = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = 'AG'+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
				,ZINQUIRR__c = 'L3'+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,ZPASSWD02DATE__c = '20161201'							//仮手続パスワード変更日
			);
			insert eidc;

			E_IDRequest__c eidr = new E_IDRequest__c(
				E_IDCPF__c = eidc.ID				//ID管理
				,ZUPDFLAG__c = E_Const.ZUPDFLAG_I	//リクエスト種別
				,ZWEBID__c = eidc.ZWEBID__c			//ID番号
				,ZIDTYPE__c = eidc.ZIDTYPE__c		//ID種別
				,ZIDOWNER__c = eidc.ZIDOWNER__c		//所有者コード
				,ZINQUIRR__c = eidc.ZINQUIRR__c		//照会者コード
			);
			insert eidr;
*/

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = 'AG'+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
				,ZINQUIRR__c = 'L3'+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,ZPASSWD02DATE__c = '20161201'							//仮手続パスワード変更日
				,ZDSPFLAG02__c = '0'									//利用システムフラグ
			);
			insert idcpf;

			E_IDRequest__c eidr = new E_IDRequest__c(
				E_IDCPF__c = idcpf.ID				//ID管理
				,ZUPDFLAG__c = E_Const.ZUPDFLAG_I	//リクエスト種別
				,ZWEBID__c = idcpf.ZWEBID__c			//ID番号
				,ZIDTYPE__c = idcpf.ZIDTYPE__c		//ID種別
				,ZIDOWNER__c = idcpf.ZIDOWNER__c		//所有者コード
				,ZINQUIRR__c = idcpf.ZINQUIRR__c		//照会者コード
			);
			insert eidr;

			//E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
			//bizLogic.PermissionSetAssign();
			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add('E_PermissionSet_Base');//基本セット　全ユーザ対象
			//permissionNames.add('E_PermissionSet_Content');

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}
		}
	}



	private static void insertMessage(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
			String type = 'メッセージ';
			String param = 'SPW|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|002';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|003';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			type = 'ディスクレイマー';
			param = 'SPW|004';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|005';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|006';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|007';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'SPW|008';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'LGN|201';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'LGN|202';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			insert insMessage;
		}
	}

	private static void insertEmail(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_Email__c eMail = new E_Email__c(NAME = 'E_101',Email__c = 'xxxx@xx.xx');
			insert eMail;
		}
	}
	private static void insertE_bizDataSyncLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
			insert log;
		}
	}

}