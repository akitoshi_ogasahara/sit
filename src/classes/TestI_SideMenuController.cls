@isTest
private class TestI_SideMenuController {
	static testMethod void SideMenuControllerTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_SideMenuController ism = new I_SideMenuController();

				system.assertEquals(ism.getMenuItems(), null);
				system.assertEquals(ism.mainCategoryLabel, null);
				system.assertEquals(ism.mainCategoryStyleClass, null);
			Test.stopTest();
		}
	}

	static testMethod void getMenuItemsTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				List<Schema.PicklistEntry> subentries = I_MenuMaster__c.SubCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[1].getValue(),SubCategory__c = subentries[1].getValue(), MenuKey__c = 'testMenuKey');
				insert m;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];

				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);
				I_SideMenuController ism = new I_SideMenuController();

				system.assertEquals(ism.getMenuItems().size() > 0, true);
				system.assertEquals(ism.mainCategoryLabel, mm.MainCategoryAlias__c);
				system.assertEquals(ism.mainCategoryStyleClass, mm.MainCategoryStyleClass__c);
				system.assertEquals(ism.subCategoryLabel, mm.SubCategory__c.mid(2, mm.SubCategory__c.length()));
			Test.stopTest();
		}
	}

	static testMethod void getMenuItemsTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c pm = new I_MenuMaster__c(Name = 'testParentMenu', MainCategory__c = entries[1].getValue(), MenuKey__c = 'testParentMenuKey');
				insert pm;
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[1].getValue(), MenuKey__c = 'testMenuKey', ParentMenu__c = pm.Id);
				insert m;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];

				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);
				I_SideMenuController ism = new I_SideMenuController();

				system.assertEquals(ism.getMenuItems()[0].children.size() > 0, true);
			Test.stopTest();
		}
	}

	static testMethod void getMenuItemsTest03() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c pm = new I_MenuMaster__c(Name = 'testParentMenu', MainCategory__c = entries[1].getValue(), MenuKey__c = 'testParentMenuKey');
				insert pm;
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu01', MainCategory__c = entries[0].getValue(), MenuKey__c = 'testMenuKey', ParentMenu__c = pm.Id);
				insert m;

				I_MenuMaster__c mm = [ SELECT Name
											, MainCategory__c
											, MainCategoryAlias__c
											, MainCategoryStyleClass__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, Default__c
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];

				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);
				I_SideMenuController ism = new I_SideMenuController();

				system.assertEquals(ism.getMenuItems().size(), 0);
			Test.stopTest();
		}
	}

	static testMethod void sideMenuItemTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
			List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', MainCategory__c = entries[0].getValue(), MenuKey__c = 'testMenuKey', IRISPageName__c = 'IRIS_Top'); 
				insert m;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];
				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);

				I_SideMenuController.sideMenuItem smi = new I_SideMenuController.sideMenuItem();

				system.assertEquals(smi.isActive, false);
				system.assertEquals(smi.hasActivatedChild, false);
				system.assertEquals(smi.children.size(), 0);
				// system.assertEquals(smi.getDestUrl(), retUrl);
			Test.stopTest();
		}
	}

	static testMethod void sideMenuItemTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
			List<Schema.PicklistEntry> entries = I_MenuMaster__c.MainCategory__c.getDescribe().getPicklistValues();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu01', MainCategory__c = entries[0].getValue(), MenuKey__c = 'testMenuKey');
				insert m;

				I_MenuMaster__c mm = [ SELECT Id
											, Name
											, MainCategory__c
											, MainCategoryAlias__c
											, SubCategory__c
											, ParentMenu__c
											, Style__c
											, MenuKey__c
											, LinkURL__c
											, IRISPageName__c
											, DestUrl__c
											, UrlParameters__c
											, requiredDefaultPageId__c
											, WindowTarget__c
											, Default__c
											, Required_ZIDTYPE__c
											, RequiredPermissionSet__c
											, Required_EIDC_Flag__c
											, ShowOnTop__c
											, (select id,name from Pages__r limit 1)
										FROM I_MenuMaster__c
										WHERE Id = :m.Id ];
				I_MenuProvider.getInstance().setAcitveMenuId(m.Id);

				I_SideMenuController.sideMenuItem smi = new I_SideMenuController.sideMenuItem();
				smi.record = mm;

				String retUrl = mm.DestUrl__c;
				retUrl += '?page=';
				retUrl += retUrl.contains('?')?'&':'?';
				retUrl += I_Const.URL_PARAM_MENUID + '=' + String.valueOf(mm.Id);

				system.assertEquals(smi.isActive, true);
				//system.assertEquals(smi.hasActivatedChild, true);
				//system.assertEquals(smi.children.size() > 0, true);
				system.assertEquals(smi.getDestUrl(), retUrl);
			Test.stopTest();
		}
	}
}