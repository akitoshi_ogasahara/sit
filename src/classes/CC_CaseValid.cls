/*
 * CC_CaseValid
 * Validation class for CC_CaseEdit OmniScript and Case Trigger
 * created  : Accenture 2018/8/30
 * modified :
 */

public with sharing class CC_CaseValid{

	/**
	 * set alert
	 * SR screen used only
	*/
	public static String alertCheck(Case newCase){
		String inputAlertMessage = '';
		String sysNotInputCommonMsg = System.Label.CC_CaseValid_ErrMsg_NotInputCommonMsg;

		/* [Step2][Sprint1#4]申出人＆申出人種別のアラートを削除する
		//申出人
		if(newCase.CMN_ClientName__c == null || newCase.CMN_ClientName__c.equals('')){
			inputAlertMessage = '・' + sysNotInputCommonMsg.replace('{0}', '申出人');
		}

		//申出人種別
		if(newCase.CMN_ClientRelation__c == null || newCase.CMN_ClientRelation__c.equals('')){
			String msg = '・' + sysNotInputCommonMsg.replace('{0}', Case.CMN_ClientRelation__c.getDescribe().getLabel());
			if(inputAlertMessage != ''){
				inputAlertMessage += '\n' + msg;
			}else{
				inputAlertMessage = msg;
			}
		}
		*/

		//帳票出力ボタン押下する場合
		if(newCase.CC_IsSRFormPrint__c){
			//送付状出力チェックされる場合
			if(newCase.CC_CoverLetter__c != null && newCase.CC_IsCoverLetterOutput__c == true){
				//送付先敬称
				if(newCase.CMN_ShippingNameTitle__c == null || newCase.CMN_ShippingNameTitle__c.equals('')){
					String msg = '・' + System.Label.CC_CaseValid_ErrMsg_NotSelectCommonMsg.replace('{0}', Case.CMN_ShippingNameTitle__c.getDescribe().getLabel());
					if(inputAlertMessage != ''){
						inputAlertMessage += '\n' + msg;
					}else{
						inputAlertMessage = msg;
					}
				}
			}
		}

		System.debug('CC_CaseValid->alertCheck: '+inputAlertMessage);
		return inputAlertMessage;
	}

	/**
	 * set error and output message map
	 * used for SR screen and case trigger
	*/
	public static Map<String, Object> errorCheckAndOutput(List<Case> newList, Map<Id,Case> newMap, Boolean triggerFlag, String inputAlertMessage){
		System.debug('CC_CaseValid->errorCheckAndOutput->newList: '+newList);

		Map<String, Object> outputValidMap = new Map<String, Object>();

		RecordType recordTypeOfCase = [SELECT Id FROM RecordType where SobjectType = 'Case' and Name = 'サービスリクエスト' limit 1];

		//get master data
		Set<Id> accountIdSet = new Set<Id>();
		Set<String> agentCodeSet = new Set<String>();
		Set<Id> srTypeIdSet = new Set<Id>();
		for(Case newCase : newList){
			if(newCase.RecordTypeId == recordTypeOfCase.Id){
				if(newCase.CMN_AgencyName__c != null){
					accountIdSet.add(newCase.CMN_AgencyName__c);
				}
				if(newCase.RecruiterCD__c != null && !newCase.RecruiterCD__c.equals('')){
					agentCodeSet.add(newCase.RecruiterCD__c);
				}
				if(newCase.CC_SRTypeId__c != null){
					srTypeIdSet.add(newCase.CC_SRTypeId__c);
				}
			}
		}
		List<Account> accountList = CC_CaseValidDao.getAccountListByIdList(accountIdSet);
		List<Contact> agentList = CC_CaseValidDao.getAgentListByAgentCodeSet(agentCodeSet);
		Map<Id, CC_SRTypeMaster__c> srTypeMasterMap = CC_CaseValidDao.getSRTypeMasterMapByIdList(srTypeIdSet);
		Set<String> srTypeNameSet = new Set<String>();
		for(CC_SRTypeMaster__c srType : srTypeMasterMap.values()){
			srTypeNameSet.add(srType.Name);
		}
		List<CC_FormMaster__c> formMasterList = CC_CaseValidDao.getFormMasterListBySRTypeSet(srTypeNameSet);

		List<CC_SRActivity__c> srActivityList = new List<CC_SRActivity__c>();
		if(!newMap.isEmpty()){
			Set<Id> caseIdSet = newMap.keySet();
			srActivityList = CC_CaseValidDao.getSRActivityListByCaseIdList(caseIdSet);
		}

		for(Case newCase : newList){
			//共通変数
			String sysNotInputCommonMsg = System.Label.CC_CaseValid_ErrMsg_NotInputCommonMsg;
			String sysInvalidCommonMsg = System.Label.CC_CaseValid_ErrMsg_InvalidCommonMsg;
			//SR画面用変数
			Boolean showValidationError = false;
			Map<String, Object> errorMessageMap = new Map<String, Object>();
			Map<String, String> dataErrorMap = new Map<String, String>();
			String inputRequiredMessage = sysNotInputCommonMsg.replace('{0}', '');
			//トリガ用変数
			String errorMessage = ' ';

			if(newCase.RecordTypeId == recordTypeOfCase.Id){

				//ステータス
				//when update/delete
				if(!newMap.isEmpty()){
					if(newCase.Status.equals('クローズ') && srActivityList.size() > 0){
						for(CC_SRActivity__c srActivityObj : srActivityList){
							if(srActivityObj.CC_SRNo__c == newCase.Id &&
							!srActivityObj.CC_Status__c.equals('終了') && !srActivityObj.CC_Status__c.equals('作業不要')){
								String msg = System.Label.CC_CaseValid_ErrMsg_InvalidStatus;
								errorMessage += msg + '<br>';
								showValidationError = true;
								dataErrorMap.put('Status', msg);
								Break;
							}
						}
					}
				}

				//情報源
				if((newCase.CMN_Source__c == null || newCase.CMN_Source__c.equals(''))
				&& (newCase.CMN_ReceptionDateTime__c != null || !triggerFlag)){
					errorMessage += sysNotInputCommonMsg.replace('{0}', Case.CMN_Source__c.getDescribe().getLabel()) + '<br>';
					showValidationError = true;
					errorMessageMap.put('inputRequiredMessage', (Object)inputRequiredMessage);
				}

				//SRタイプ番号
				String srTypeIdLabel = Case.CC_SRTypeId__c.getDescribe().getLabel();
				if(newCase.CC_SRTypeId__c == null && (newCase.CMN_ReceptionDateTime__c != null || !triggerFlag)){
					errorMessage += sysNotInputCommonMsg.replace('{0}', srTypeIdLabel) + '<br>';
					showValidationError = true;
					errorMessageMap.put('inputRequiredMessage', (Object)inputRequiredMessage);
				}else if(newCase.CC_SRTypeId__c == '000000000kariId'){
					showValidationError = true;
					dataErrorMap.put('SRTypeNameAndSubject', sysInvalidCommonMsg.replace('{0}', srTypeIdLabel));
				}

				//募集人コード&代理店事務所コード
				if(newCase.RecruiterCD__c != null && newCase.RecruiterCD__c != '' && newCase.RecruiterCD__c != 'kariCode'){
					Contact contactObj;
					for(Contact agent: agentList){
						if(agent.E_CL3PF_AGNTNUM__c.equals(newCase.RecruiterCD__c)){
							contactObj = agent;
							newCase.CMN_AgentName__c = agent.Id;
							break;
						}else{
							newCase.CMN_AgentName__c = null;
						}
					}

					if(newCase.CMN_AgentName__c == null){
						String msg = sysInvalidCommonMsg.replace('{0}', Case.CMN_AgentCD__c.getDescribe().getLabel());
						errorMessage += msg + '<br>';
						showValidationError = true;
						dataErrorMap.put('AgentCode', msg);
					}else{
						//代理店事務所コード
						if((contactObj.Account.Z1OFFING__c != null && contactObj.Account.Z1OFFING__c.equals('Y')) ||
						(contactObj.Account.Parent.Z1OFFING__c != null && contactObj.Account.Parent.Z1OFFING__c.equals('Y'))){
							String agencyCodeLabel = Case.CMN_AgencyOfficeCD__c.getDescribe().getLabel();

							if(newCase.CMN_AgencyName__c == null){
								String msg = sysNotInputCommonMsg.replace('{0}', agencyCodeLabel);
								errorMessage += msg + '<br>';
								showValidationError = true;
								dataErrorMap.put('AgencyCode', msg);
							}
							else{
								String invalidMsg = sysInvalidCommonMsg.replace('{0}', agencyCodeLabel);

								if(newCase.CMN_AgencyName__c == '000000000kariId'){
									showValidationError = true;
									dataErrorMap.put('AgencyCode', invalidMsg);
								}else{
									Boolean eIsAgency = false;
									Decimal parentIsMotherOffice = 1;
									Boolean inputDataError = true;

									if(newCase.CMN_AgencyName__c == contactObj.AccountId){
										eIsAgency = contactObj.Account.E_IsAgency__c;
										parentIsMotherOffice = contactObj.Account.ZubsMain__c;
										inputDataError = false;
									}
									else{
										for(Account acc : accountList){
											if(((contactObj.Account.E_CL1PF_ZHEADAY__c != null && contactObj.Account.E_CL1PF_ZHEADAY__c.equals(acc.Parent.E_CL1PF_ZHEADAY__c))
												|| (contactObj.Account.Parent.E_CL1PF_ZHEADAY__c != null && contactObj.Account.Parent.E_CL1PF_ZHEADAY__c.equals(acc.Parent.E_CL1PF_ZHEADAY__c)))
											&& newCase.CMN_AgencyName__c == acc.Id){
												eIsAgency = acc.E_IsAgency__c;
												parentIsMotherOffice = acc.ZubsMain__c;
												inputDataError = false;
												break;
											}
										}
									}

									if(inputDataError){
										errorMessage += invalidMsg + '<br>';
										showValidationError = true;
										dataErrorMap.put('AgencyCode', invalidMsg);
									}else{
										String errMsg = System.Label.CC_CaseValid_ErrMsg_InvalidAgencyCode;
										if(eIsAgency){
											errorMessage += errMsg + '<br>';
											showValidationError = true;
											dataErrorMap.put('AgencyCode', errMsg);
										}else if(parentIsMotherOffice == 0){
											String alert = System.Label.CC_CaseValid_Msg_AlertAgencyCode;
											if(!inputAlertMessage.equals('')){
												inputAlertMessage += '\n' + alert;
											}else{
												inputAlertMessage = alert;
											}
										}
									}
								}
							}
						}
					}
				}
				else if(newCase.RecruiterCD__c == 'kariCode' ||
						((newCase.RecruiterCD__c == null || newCase.RecruiterCD__c == '') && newCase.CMN_AgencyName__c != null)){
					String msg = System.Label.CC_CaseValid_ErrMsg_NotInputAgentCode;
					errorMessage += msg + '<br>';
					showValidationError = true;
					dataErrorMap.put('AgentCode', msg);
				}

				//送付状
				if(newCase.CC_CoverLetter__c == '000000000kariId'){
					showValidationError = true;
					dataErrorMap.put('CoverLetter', sysInvalidCommonMsg.replace('{0}', Case.CC_CoverLetter__c.getDescribe().getLabel()));
				}

				//帳票出力ボタン押下する場合
				if(newCase.CC_IsSRFormPrint__c){

					//送付状
					if(newCase.CC_IsCoverLetterOutput__c == true && newCase.CC_CoverLetter__c == null){
						String msg = sysNotInputCommonMsg.replace('{0}', Case.CC_CoverLetter__c.getDescribe().getLabel());
						errorMessage += msg + '<br>';
						showValidationError = true;
						dataErrorMap.put('CoverLetter', msg);
					}

					//送付状出力チェックされる場合
					if(newCase.CC_CoverLetter__c != null && newCase.CC_IsCoverLetterOutput__c == true){
						//送付先
						if(newCase.CMN_ShippingChoices__c == null || newCase.CMN_ShippingChoices__c == ''){
							String msg = System.Label.CC_CaseValid_ErrMsg_NotSelectCommonMsg.replace('{0}', '送付先');
							errorMessage += msg + '<br>';
							showValidationError = true;
							dataErrorMap.put('ShippingChoices', msg);
						}

						//送付先宛名
						if(newCase.CC_ContactNameForPrint__c == null || newCase.CC_ContactNameForPrint__c == ''){
							String msg = sysNotInputCommonMsg.replace('{0}', Case.CC_ContactNameForPrint__c.getDescribe().getLabel());
							errorMessage += msg + '<br>';
							showValidationError = true;
							dataErrorMap.put('ContactNameForPrint', msg);
						}

						//送付先郵便番号
						if(newCase.CC_ZipcodeForPrint__c == null || newCase.CC_ZipcodeForPrint__c == ''){
							String msg = sysNotInputCommonMsg.replace('{0}', Case.CC_ZipcodeForPrint__c.getDescribe().getLabel());
							errorMessage += msg + '<br>';
							showValidationError = true;
							dataErrorMap.put('ZipcodeForPrint', msg);
						}

						//送付先住所
						if(newCase.CC_AddressForPrint__c == null || newCase.CC_AddressForPrint__c == ''){
							String msg = sysNotInputCommonMsg.replace('{0}', '送付先住所');
							errorMessage += msg + '<br>';
							showValidationError = true;
							dataErrorMap.put('AddressForPrint', msg);
						}
					}

					//確認書出力（かつ証券番号にはエラーなし）の場合
					if(newCase.CC_IsConfirmationFormOutput__c
					&& newCase.CC_SRTypeId__c != null && newCase.CC_SRTypeId__c != '000000000kariId'){
						String srTypeMasterName = srTypeMasterMap.get(newCase.CC_SRTypeId__c).Name;
						Boolean errorFlag = false;

						if(formMasterList.size() > 0){
							for(CC_FormMaster__c formMasterObj : formMasterList){
								if(formMasterObj.CC_SRTypeNo__c.equals(srTypeMasterName) && formMasterObj.CC_FormType__c.equals('確認書')){
									errorFlag = false;
									break;
								}else{
									errorFlag = true;
								}
							}
						}else{
							errorFlag = true;
						}

						if(errorFlag){
							String msg = System.Label.CC_CaseValid_ErrMsg_InvalidConfirmationForm;
							errorMessage += msg + '<br>';
							showValidationError = true;
							dataErrorMap.put('ConfirmationForm', msg);
						}
					}
				}
			}

			//output error for trigger(multiple Case)
			if(triggerFlag){
				if(!errorMessage.equals(' ')){
					errorMessage = errorMessage.removeEnd('<br>');
					outputValidMap.put((String)newCase.Id, (Object)errorMessage);
				}
			}
			//output error&alert for SR screen(only one Case)
			else{
				//error
				if(showValidationError){
					outputValidMap.put('showValidationError', true);
					if(!dataErrorMap.isEmpty()){
						errorMessageMap.put('dataErrorMap', (Object)dataErrorMap);
					}
					outputValidMap.put('inputErrorMessage', (Object)errorMessageMap);
				}else{
					outputValidMap.put('showValidationError', false);
				}

				//alert
				if(!inputAlertMessage.equals('')){
					outputValidMap.put('showValidationAlert', true);
					outputValidMap.put('inputAlertMessage', (Object)inputAlertMessage);
				}else{
					outputValidMap.put('showValidationAlert', false);
				}
			}

		}

		System.debug('CC_CaseValid->errorCheckAndOutput: '+outputValidMap);
		return outputValidMap;
	}

}