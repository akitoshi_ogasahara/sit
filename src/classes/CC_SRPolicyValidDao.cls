/*
 * CC_SRPolicyValidDao
 * Data access class for CC_SRPolicyValid class
 * created  : Accenture 2018/10/26
 * modified :
 */

public with sharing class CC_SRPolicyValidDao{

	/**
	 * Object: Case
	 * Parameter: Id
	 * return: Map<Id, Case>
	 */
	public static Map<Id, Case> getCaseMapByIdList(Set<Id> caseIdSet){
		Map<Id, Case> caseMap = new Map<Id, Case>([
						SELECT Id, CMN_ContactName__c, CMN_CorporateName__c, CMN_AgencyName__c, ContactId, CC_SRTypeId__c, CC_SRTypeId__r.Name
						  FROM Case
						 WHERE Id IN: caseIdSet
						 	AND RecordType.Name = 'サービスリクエスト']);

		return caseMap;
	}

	/**
	 * Object: E_Policy__c
	 * Parameter: Id set
	 * return: Map<Id, E_Policy__c>
	 */
	public static Map<Id, E_Policy__c> getEPolicyMapByIdSet(Set<Id> listPolicyId){
		Map<Id, E_Policy__c> ePolicyMap = new Map<Id, E_Policy__c>([
							SELECT Id, COMM_CHDRNUM__c, Contractor__c, Contractor__r.AccountId, AccessibleCustomer__c, AccessibleCustomer__r.AccountId
							  FROM E_Policy__c
							 WHERE Id IN: listPolicyId ]);

		return ePolicyMap;
	}

	/**
	 * Object: CC_FormMaster__c
	 * Parameter: Id set
	 * return: Map<Id, CC_FormMaster__c>
	 */
	public static Map<Id, CC_FormMaster__c> getFormMasterMapByIdSet(Set<Id> formMasterIdSet){
		Map<Id, CC_FormMaster__c> formMasterMap = new Map<Id, CC_FormMaster__c>([
							SELECT Id, CC_FormName__c, CC_SRTypeNo__c, CC_FormType__c
							  FROM CC_FormMaster__c
							 WHERE Id IN: formMasterIdSet ]);

		return formMasterMap;
	}

}