/**
 * 定数を定義するクラス
 * CreatedDate 2014/12/03
 * @Author Terrasky
 */
public class E_Const {
	//システム名
	public static final String SYSTEM_NAME = Label.E_NNLink_SYS_NAME;   //'NNLink';

	//ContentのダウンロードUrl
	public static final String CONTENT_DOWNLOAD_URL = '/sfc/servlet.shepherd/version/download/{0}?aspdf=false';


	//User.UserType
	//社員
	public static final String USERTYPE_STANDARD = 'Standard';
	//代理店コミュニティ
	public static final String USERTYPE_POWERPARTNER = 'PowerPartner';
	//契約者コミュニティ
	public static final String USERTYPE_CSPLITEPORTAL = 'CspLitePortal';
	//Guest
	public static final String USERTYPE_GUEST = 'Guest';

	//特定代理店格コード
	//BTMU
	public static final String ZHEADAY_BTMU = '1CHH0';
	//かんぽ
	public static final String ZHEADAY_KAMPO = '1N20J';
	//JP
	public static final String ZHEADAY_JPLIFE = '1N20M';
	//住友生命
	public static final String ZHEADAY_SUMISEI = '3C8WM';

	//Policy.RecordType
	//個人保険
	public static final String POLICY_RECORDTYPE_COLI = 'ECHDPF';
	//SPVA
	public static final String POLICY_RECORDTYPE_SPVA = 'ESPHPF';
	//個人保険消滅
	public static final String POLICY_RECORDTYPE_DCOLI = 'EHLDPF';
	//SPVA消滅
	public static final String POLICY_RECORDTYPE_DSPVA = 'EHDSPF';

	//照会不可：0
	public static final String IS_INQUIRY_0 = '0';
	//照会可能：1
	public static final String IS_INQUIRY_1 = '1';
	//一時照会不可：2
	public static final String IS_INQUIRY_2 = '2';

	//共通クラスの引数のID区分
	public enum ID_KIND {CONTACT, POLICY, USER, INVESTMENT, DOWNLOAD, SPECIAL_FEATURES, CUSTOMER_SEARCH, COMPLIANCE, LOGIN, INFO, UNITPRICE}

	//エラーメッセージ
	//オブジェクトアクセス権なし
	public static final String ERROR_MSG_OBJECT = 'ERR|001';
	//パラメータ不正
	public static final String ERROR_MSG_PARAMETER = 'ERR|002';
	//機能制限
	public static final String ERROR_MSG_FUNCTION = 'ERR|003';
	//Service Hour
	public static final String ERROR_MSG_SERVICEHOUR = 'ERR|007';
	//該当する情報はございません。
	public static final String ERROR_MSG_NODATA = 'ERR|009';
	//投資信託機能削除
	public static final String ERROR_MSG_NOFUNCTION = 'ERR|010';
	//ID管理が存在しないユーザ
	public static final String ERROR_MSG_NO_IDCPF = 'ERR|011';
	/* 20170710追加 メッセージのフレンドリー化 */
	//ページ読み込み時にエラーが発生した際の共通メッセージ
	public static final String ERROR_MSG_NOTALLOW = 'ERR|012';
	/* 20171101追加 銀行代理店開放 */
	//IPアドレス不正
	public static final String ERROR_MSG_IPADDRESS = 'ERR|013';
	/* 20180724追加 IRISfor住友 */
	//ユーザ情報不正
	public static final String ERROR_MSG_ACTIVEUSER = 'ERR|014';

	//ファンド区分
	//個人保険
	public static final String FUND_KIND_COLI = '10';
	//SPVA
	public static final String FUND_KIND_SPVA = '20';

	//ファンドコード
	//NOT個人保険ファンド
	public static final String NOT_FUND_COLI = 'V002';
	//ポートフォリオ 指定の保険種類コード（先頭２文字）のみ変額保険ファンドを抽出
	public static final String PORTFOLIO_FUND_POLICYKIND = 'SU';

	//SP種別
	//年金
	public static final String SP_CLASS_ANNUITY = 'ANNUITY';
	//
	public static final String SP_CLASS_SPVAANNUITY = 'SPVA&ANNUITY';
	//
	public static final String SP_CLASS_SPVA = 'SPVA';
	//
	public static final String SP_CLASS_SPVWL = 'SPVWL';


	//契約関連機能
	//代理店
	public static final String CS_DISTORIBUTOR = 'DISTORIBUTOR';
	//代理店事務所
	public static final String CS_OFFICE = 'OFFICE';
	//募集人
	public static final String CS_AGENT = 'AGENT';
	//契約者
	public static final String CS_CONTRACTOR = 'CONTRACTOR';
	//被保険者
	public static final String CS_INSURED = 'INSURED';
	//エラーページ用コード
	public static final String CS_ERROR_PAGE_CODE = 'CSH|001';

	//みずほ銀行の代理店格コード
	public static final String E_CL3PF_ZHEADAY_2VSL3= '2VSL3';


	//ID管理　トランザクションコード
	//繰入比率変更
	public static final String TRCDE_TD05 = 'TD05';
	//積立金の移転
	public static final String TRCDE_TD06 = 'TD06';
	//住所変更受付
	public static final String TRCDE_TDB0 = 'TDB0';

	//ID管理　ID種別
	//代理店募集人
	public static final String ZIDTYPE_AT = 'AT';
	//代理店事務所
	public static final String ZIDTYPE_AY = 'AY';
	//代理店格
	public static final String ZIDTYPE_AH = 'AH';
	//社員
	public static final String ZIDTYPE_EP = 'EP';

	//契約関連の円グラフ（機能別）
	//積立金
	public static final String GRAPH_POLICY_FUNDTRANSFER = '1';
	//繰入比率
	public static final String GRAPH_POLICY_FUNDRATIO = '2';
	//ポートフォリオ
	public static final String GRAPH_POLICY_PORTFOLIO = '3';

	//契約関連の円グラフ（機能の画面別）
	//申込 or 参照
	public static final String GRAPH_POLICY_ENTRY = 'entry';
	//確認
	public static final String GRAPH_POLICY_CONFIRM = 'confirm';
	//完了
	public static final String GRAPH_POLICY_COMPLETE = 'complete';

	//投資信託関連の円グラフ
	//買付時の資産配分比率
	public static final String GRAPH_INVESTMENT_BUY = 'buy';
	//現在の資産配分比率
	public static final String GRAPH_INVESTMENT_NOW = 'now';

	//パスワードステータス
	//長期未使用
	public static final String PW_STATUS_LONGTERMUNUSED = '0';
	//有効
	public static final String PW_STATUS_OK = '1';
	//無効
	public static final String PW_STATUS_NG = '2';
	//緊急停止
	public static final String PW_STATUS_EMERGENCYSTOP = '5';
	//長期未使用
	public static final String PW_STATUS_UNUSEDSTOP = '0';

	//メール
	//メール本文用スペース
	public static final String EMAIL_SPACE = '　　　　';
	// メールタイプ：住所変更
	public static final String MAIL_TYPE_AD = 'AD';
	// メールタイプ：メール変更
	public static final String MAIL_TYPE_EM = 'EM';
	// メールタイプ：基本パスワード変更
	public static final String MAIL_TYPE_1P = '1P';
	// メールタイプ：手続きパスワード変更
	public static final String MAIL_TYPE_2P = '2P';

	//顧客変更受付履歴区分
	//住所変更
	public static final String KBN_ADD = '住所変更';
	//メールアドレス変更
	public static final String KBN_MAIL = 'メールアドレス変更';
	//基本パスワード変更
	public static final String KBN_1PPW = '基本パスワード変更';
	//手続パスワード変更
	public static final String KBN_2PPW = '手続パスワード変更';

	//ラベル
	//手続パスワード：現在
	public static final String LABEL_2P_OLD = '現在の手続サービス用パスワード';
	//手続パスワード：新
	public static final String LABEL_2P_NEW = '新規手続サービス用パスワード';
	//手続パスワード：確認
	public static final String LABEL_2P_VER = '新規手続サービス用パスワード(確認)';


	//個人保険
	//現在の状況コード(A)
	public static final String COLI_STATCODE_A = 'A';
	//現在の状況コード(J)
	public static final String COLI_STATCODE_J = 'J';
	//現在の状況コード(K)
	public static final String COLI_STATCODE_K = 'K';

	//年金ステータス(C)
	public static final String SPVA_ZANNSTAT_C = 'C';

	//カスタム設定(メールアドレス：E_Email__c)
	//担当者メールアドレス（登録内容変更）
	public static final String EMAIL_CHARGE_CHANGECONTACT = 'E_101';
	//担当者メールアドレス（積立&繰入)
	public static final String EMAIL_CHARGE_TRANSFER = 'E_201';

	//system administrator
	public static final String SYSTEM_ADMINISTRATOR = 'システム管理者';

	//新契約申込進捗照会(JP)  商品コード
	public static final String PROGRESS_JP_PRODUCTCD = '02';

	//Menu.RecordType
	//1.トップメニュー
	public static final String MENU_RECORDTYPE_TOPMENU = 'E_TopMenu';
	//2.メニュー
	public static final String MENU_RECORDTYPE_MENU = 'E_Menu';
	//3.スクリーン
	public static final String MENU_RECORDTYPE_SCREEN = 'E_Screen';

	//保険種類
	public static final String COMM_CRTABLE_SZ = 'SZ';
	public static final String COMM_CRTABLE_SY = 'SY';
	public static final String COMM_CRTABLE_SX = 'SX';

	//MenuKind外部ID
	public static final String MENUKIND_EXID_CUSTOMER = 'MK_Customer';

	//積立&繰入：同意画面非経由時
	public static final String FUND_TRANSFER_NOT_AGREED = 'FEE|000';
	public static final String INWARD_TRANSFER_NOT_AGREED = 'IND|000';

	//顧客ロール
	public static final String CLRRROLE_OW = 'OW';
	public static final String CLRRROLE_LA = 'LA';
	public static final String CLRRROLE_BN = 'BN';
	public static final String CLRRROLE_GR = 'GR';
	public static final String CLRRROLE_NE = 'NE';

	public static final String CLRRROLE_NE_LABEL = '有';

	//受取人種別
	public static final String BNYTYP_01 = '01';
	public static final String BNYTYP_02 = '02';
	public static final String BNYTYP_03 = '03';
	public static final String BNYTYP_04 = '04';
	public static final String BNYTYP_05 = '05';
	public static final String BNYTYP_06 = '06';
	public static final String BNYTYP_07 = '07';
	public static final String BNYTYP_08 = '08';
	public static final String BNYTYP_09 = '09';
	public static final String BNYTYP_10 = '10';
	public static final String BNYTYP_11 = '11';
	public static final String BNYTYP_40 = '40';
	public static final String BNYTYP_50 = '50';
	public static final String BNYTYP_51 = '51';
	public static final String BNYTYP_53 = '53';

	//有効FLG
	public static final String VALIDFLAG_3 = '3';

	//契約者区分
	public static final String COMM_ZCNTRDSC_P = 'P';
	public static final String COMM_ZCNTRDSC_C = 'C';

	//ファンドスイッチング更新：区分
	//積立金
	public static final String KBN_FUND = '積立金';
	//繰入
	public static final String KBN_INWARD = '繰入';

	//権限セット　Name
	public static final String PERM_BASE				= 'E_PermissionSet_Base';
	public static final String PERM_INVE				= 'E_PermissionSet_Inve';
	public static final String PERM_VIEWContent		 	= 'E_PermissionSet_Content';
	public static final String PERM_ProceduralHistory   = 'E_PermissionSet_ProceduralHistory';
	public static final String PERM_Commision		    = 'E_PermissionSet_Commision';
	public static final String PERM_EMPloyee			= 'E_PermissionSet_Employee';
	public static final String PERM_BTMU				= 'E_PermissionSet_BTMU';
	public static final String PERM_JPLife			  	= 'E_PermissionSet_JPLife';
	public static final String PERM_Maintenance			= 'E_PermissionSet_Maintenance';
	//public static final String PERM_Super				 = 'E_PermissionSet_Super';	  Super権限は自動割当を行わない
	public static final String PERM_SREmployee 			= 'I_PermissionSet_SREmployee'; //挙積対応
	public static final String PERM_ATRAuthRead			= 'ATR_AuthMngRead';//P16-0003 Atria対応開発
	public static final String PERM_ATRAuthUpd			= 'ATR_AuthMngUpd';//P16-0003 Atria対応開発
	public static final String PERM_DocMaintenance 		= 'I_PermissionSet_DocMaintenance'; //資料メンテナンス
	public static final String PERM_SUMISEI_IRIS 		= 'I_PermissionSet_Sumisei';	// Iris for Sumisei

	//NNLinkで利用する権限セットNameのSet
	public static final Set<String> PERMISSIONSET_NAMES = new Set<String>{
														  E_Const.PERM_Base
														, E_Const.PERM_Inve
														, E_Const.PERM_VIEWContent
														, E_Const.PERM_ProceduralHistory
														, E_Const.PERM_Commision
														, E_Const.PERM_Employee
														, E_Const.PERM_BTMU
														, E_Const.PERM_JPLife
														, E_Const.PERM_Maintenance
														//, E_Const.PERM_Super
														, E_Const.PERM_SREmployee 	//挙積対応
														, E_Const.PERM_ATRAuthRead		//P16-0003 Atria対応開発
														, E_Const.PERM_ATRAuthUpd		//P16-0003 Atria対応開発
														, E_Const.PERM_DocMaintenance	//資料メンテナンス
														, E_Const.PERM_SUMISEI_IRIS		// IRIS for Sumitomo
														, CR_Const.PERMISSIONSET_EMPLOYEE
														//, CR_PermissionSet_Approve			//承認権限は運用にて設定するため自動付与しない →　ID管理の項目を元にシステム付与
														, CR_Const.PERMISSIONSET_APPROVE
														, CR_Const.PERMISSIONSET_DPA
														, CR_Const.PERMISSIONSET_OutSrc
														, CR_Const.PERMISSIONSET_Agency
														, CR_Const.PERMISSIONSET_REPORTS
														, CR_Const.PERMISSIONSET_BOOKS
														, SC_Const.PERMISSIONSET_SC_NN
														, SC_Const.PERMISSIONSET_SC_Director
														, SC_Const.PERMISSIONSET_SC_Agent
														, SC_Const.PERMISSIONSET_SC_Admin
														, MK_Const.PERMISSIONSET_MK_Agent
													};

	//遷移元情報取得
	public static final String FROM_ANNUITY = 'annuity';
	public static final String FROM_SPVA = 'spva';
	public static final String FROM_COLI = 'coli';
	public static final String FROM_PORTFOLIO = 'portfolio';
	public static final String FROM_SPECIALACCOUNT = 'specialaccount';

	//メッセージマスタ
	public static final String E_MEGTYPE_DISCLAIMER = 'ディスクレイマー';   //メッセージマスタ種別：ディスクレイマー
	public static final String E_MEGTYPE_MEG = 'メッセージ';			 //メッセージマスタ種別：メッセージ
	public static final String E_MEGTYPE_CMS = 'CMS';			   //メッセージマスタ種別：CMS

	//ID管理.パスワードステータス
	public static final String ZSTATUS01_URGENT  = '0';
	public static final String ZSTATUS01_ENABLE  = '1';
	public static final String ZSTATUS01_DISABLE = '2';
	public static final String ZSTATUS01_ENTRY   = '3';
	public static final String ZSTATUS01_PAUSE   = '4';
	public static final String ZSTATUS01_ERROR   = '5';
	public static final String ZSTATUS01_CGW	 = '6';

	//ID管理.利用システムフラグ
	public static final String ZDSPFLAG02_IRIS	= '0';	//IRIS
	public static final String ZDSPFLAG02_BANK	= '1';	//銀行代理店用NN Link

	//ユニットプライス商品コード・ファンドコード
	//ターゲット型
	public static final String UP_PRDCD_DIRECT = 'S0105';
	public static final String UP_GFNDCD_DIRECT = 'G29';	//ファンド群コード
	public static final String UP_FNDCD_DIRECT35 = 'A098';
	public static final String UP_FNDCD_DIRECT50 = 'A099';
	//たしかなおくりもの
	//public static final String UP_PRDCD_PRESENT_UNDER70 = 'S0213';
	public static final String UP_GFNDCD_PRESENT_UNDER70 = 'G44';//ファンド群コード
	//public static final String UP_PRDCD_PRESENT_OVER71 = 'S0226';
	public static final String UP_GFNDCD_PRESENT_OVER71 = 'G45';//ファンド群コード
	//バリアブルアニュイティ
	public static final String UP_PRDCD_VALIABLE = 'V9999';

	//たしかなおくりもの遷移先パラメータ
	public static Map<String, List<String>> UP_URLPARAM_PRESENT = new Map<String, List<String>>{
		'S0212' => new List<String>{'S0212', 'S0225'},
		'S0225' => new List<String>{'S0212', 'S0225'},
		'S0213' => new List<String>{'S0213', 'S0226'},
		'S0226' => new List<String>{'S0213', 'S0226'},
		'S0214' => new List<String>{'S0214', 'S0227'},
		'S0227' => new List<String>{'S0214', 'S0227'}
	};



	//コンプライアンス
	public static final String MENU_KEY_COMPLIANCE_AGENCY = 'compliance_agency';
	public static final String MENU_KEY_COMPLIANCE_CONTRACTANT = 'compliance_contractant';

	//ユニットプライス基本情報の表示対象パラメータ
	public static final SET<String> UP_VISIBLE_ZDSPFLAG = new SET<String>{'1'};

	//ブラウザ
	public static final String BROWSER_MSIE = 'MSIE';
	public static final String BROWSER_TRIDENT = 'Trident';
	public static final String BROWSER_FIREFOX = 'Firefox';
	public static final String BROWSER_CHROME = 'Chrome';
	public static final String BROWSER_SAFARI = 'Safari';

	//OS
	public static final String OS_WINDOWS = 'Windows';
	public static final String OS_MACINTOSH = 'Macintosh';
	public static final String OS_IPHONE = 'iPhone';

	//ダウンロード履歴.出力形式
	public static final String DH_OUTPUTTYPE_CSV = 'CSV';
	public static final String DH_OUTPUTTYPE_PDF = 'PDF';
	public static final String DH_OUTPUTTYPE_GRAPH = 'GRAPH';

	//ダウンロード履歴.帳票種別
	public static final String DH_FORMNO_FEEDATA_AGS = '1';	 //手数料明細(生保)
	public static final String DH_FORMNO_FEEDATA_SPVA = '2';	//手数料明細(変額商品)
	public static final String DH_FORMNO_FEEDATA_BOS = '3';	 //手数料明細(ボーナス)
	public static final String DH_FORMNO_HOLDPOLI_COLI = '4';   //保有契約（個人保険）
	public static final String DH_FORMNO_HOLDPOLI_SPVA = '5';   //保有契約（個人保険）
	public static final String DH_FORMNO_POLIHISTORY = '6';	 //生命保険
	public static final String DH_FORMNO_NOTICE = '未設定';	 //４帳票
	public static final String DH_FORMNO_IRIS_COLI = 'E';	 // 保険契約概要
	public static final String DH_FORMNO_SELFCOMPLIANCE = 'F';	//自己点検
	public static final String DH_FORMNO_DISEASE = 'G';	 // 医的な引受の目安
	//ダウンロード履歴.帳票種別 set
	public static final Set<String> DH_FORMNOS = new Set<String>{
													E_Const.DH_FORMNO_FEEDATA_AGS,
													E_Const.DH_FORMNO_FEEDATA_SPVA,
													E_Const.DH_FORMNO_FEEDATA_BOS,
													E_Const.DH_FORMNO_HOLDPOLI_COLI,
													E_Const.DH_FORMNO_HOLDPOLI_SPVA,
													E_Const.DH_FORMNO_POLIHISTORY,
													E_Const.DH_FORMNO_IRIS_COLI,
													E_Const.DH_FORMNO_DISEASE,
													E_Const.DH_FORMNO_SELFCOMPLIANCE,
													'A','B','C','D'
	};

	//仮Pwのprefix
	public static final String PREFIX_TEMP_PASSWORD = 'Ebiz_';

	//Sitesエラーコード一覧
	public static final String SITES_ERROR_PAGE_CODE_401 = 'ERR|401';
	public static final String SITES_ERROR_PAGE_CODE_509 = 'ERR|509';
	public static final String SITES_ERROR_PAGE_CODE_500_503 = 'ERR|500';
	public static final String SITES_ERROR_PAGE_CODE_404 = 'ERR|404';
	public static final String SITES_ERROR_PAGE_CODE_999 = 'ERR|999';


	//コミュニティサブディレクトリ名
	public static final String NNLINK_CLI = 'nnlinkcli';
	public static final String NNLINK_AGE = 'nnlinkage';
	public static final String NNLINK_CGWAGE = 'nnlinkcgwage';
	public static final String NNLINK_NN = 'nn';

	public static final String INVALID_EMAIL_ADDRESS = 'INVALID_EMAIL_ADDRESS';
	public static final String INVALID_NEW_PASSWORD = 'INVALID_NEW_PASSWORD';

	//全社権限（BR**）
	public static final String USER_ACCESS_KIND_BRALL = 'BR**';

	//年金ステータス
	public static final String SPVA_ZANNSTAT_A = 'A';

	//パスワード変更時のエラー文言
	public static Map<string,string> SFDC_EXCEPTION_MSG_HACK = new Map<String,String>{
		'Your password must have a mix of numbers and uppercase and lowercase letters'  => 'アルファベットの大文字、小文字、数字の3種類すべてを組み合わせて設定してください。'
		,'Your password must be at least 8 characters long' => '8文字以上のパスワードを設定してください。'
		,'invalid repeated password' => '過去使用したパスワードは設定できません。'
		,'パスワードは、数字、大文字、小文字のすべてを含む必要があります' => 'アルファベットの大文字、小文字、数字の3種類すべてを組み合わせて設定してください。'
		,'パスワードは 8 文字以上で、数字、大文字、小文字を組み合わせて指定する必要があります。' => '8文字以上のパスワードを設定してください。'
		,'エラー: 過去使用したパスワードは設定できません。' => '過去使用したパスワードは設定できません。'
		,'Your Password cannot equal or contain your user name.' => 'ログインIDに含まれるパスワードは設定できません。'
	};

	//メニュー種類外部ID
	public static final String MK_EXID_AGENCY = 'MK_Agency';				//一般代理店
	public static final String MK_EXID_BANK_AGENCY = 'MK_BankAgency';	   //銀行代理店
	public static final String MK_EXID_JAPAN_POST = 'MK_JapanPost';		 //郵便局
	public static final String MK_EXID_KAMPO = 'MK_Kampo';				  //かんぽ
	public static final String MK_EXID_SUMITOMO = 'MK_Sumitomo';		 //住友生命

	//Ebiz連携ログ 区分
	public static final String EBIZ_ADD_GROUP_AGENCY = '4';	 // 公開グループに母店支社設定
	public static final String EBIZ_SHARE_NEWPOLICY = 'E';	  // 新契約ステータス（E_NewPolicy__c）のShare設定

	// 本社営業部長ロール
	public static final String ROLE_DEVNAME_HQ_MANAGER = 'DeveloperName71077';

	// 10桁IDの先頭2桁
	public static final String ZWEBID_TOP_50 = '50';

	//ユーザ作成時固定内容
	public static final String USER_TIMEZONESIDKEY_ASIATOKYO = 'Asia/Tokyo';		  //タイムゾーン
	public static final String USER_EMAILENCODINGKEY_UTF8 = 'UTF-8';			 //メールの文字コード
	public static final String USER_LOCALESIDKRY_JA_JP = 'ja_JP';				 //地域
	public static final String USER_LANGUAGELOCLEKEY_JA = 'ja';						 //言語
	public static final String USER_PORTALROlE_WORKER = 'Worker';					   //ポータルロール

	//IDリクエスト ID種別
	//IDの新規作成
	public static final String ZUPDFLAG_I = 'I';
	//IDの無効化
	public static final String ZUPDFLAG_D = 'D';
	//IDの権限変更
	public static final String ZUPDFLAG_M = 'M';
	//Email変更
	public static final String ZUPDFLAG_E = 'E';
	//パスワード変更のキャンセル
	public static final String ZUPDFLAG_PW = 'PW_C';
	//IDの緊急停止
	public static final String ZUPDFLAG_ES = 'ES';
	//ユーザ無効化種別Set
	public static final Set<String> INVALID_ZUPDFLAGS = new Set<String>{ZUPDFLAG_D,ZUPDFLAG_ES};

	//ID管理 所有者コード
	//契約者用ID
	public static final String ZIDOWNER_OW = 'OW';
	//代理店用ID
	public static final String ZIDOWNER_AG = 'AG';
	//LDA／内勤社員用ID
	public static final String ZIDOWNER_EP = 'EP';

	//ID管理 照会者コード
	//契約者権限
	public static final String ZINQUIRR_OW = 'OW';
	//被保険者権限
	public static final String ZINQUIRR_LF = 'LF';
	//代理店格権限、支社長権限
	public static final String ZINQUIRR_L1 = 'L1';
	//代理店事務所権限、営業所長権限
	public static final String ZINQUIRR_L2 = 'L2';
	//募集人権限、LDA権限
	public static final String ZINQUIRR_L3 = 'L3';
	//支社権限、全社権限
	public static final String ZINQUIRR_BR = 'BR';

	//取引先自動契約移管判定フラグ
	public static final String ZAUTOTRF_Y = 'Y';

	//プロファイルName
	//E_PartnerCommunity
	public static final String PROFILE_E_PARTNERCOMMUNITY = 'E_PartnerCommunity';
	////E_PartnerCommunity_IA
	//public static final String PROFILE_E_PARTNERCOMMUNITY_IA = 'E_PartnerCommunity_IA';
	////E_PartnerCommunity_Sumisei
	//public static final String PROFILE_E_PARTNERCOMMUNITY_Sumisei = 'E_PartnerCommunity_Sumisei';

	//Profile差し替え対応 ここから
	//E_PartnerCommunity_IA
	public static final String PROFILE_E_PARTNERCOMMUNITY_IA = 'E_PartnerCommunity_IA';
	public static final String PROFILE_E_PARTNERCOMMUNITY_IA2 = 'E_PartnerCommunity_IA2';
	//E_PartnerCommunity_Sumisei
	public static final String PROFILE_E_PARTNERCOMMUNITY_Sumisei = 'E_PartnerCommunity_Sumisei';
	public static final String PROFILE_E_PARTNERCOMMUNITY_Sumisei2 = 'E_PartnerCommunity_Sumisei2';
	public static final String PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS = 'E_PartnerCommunity_SumiseiIris';
	public static final String PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2 = 'E_PartnerCommunity_SumiseiIris2';

	public static final Set<String> PROFILE_NAMES = new Set<String>{
													 E_Const.PROFILE_E_PARTNERCOMMUNITY
													,E_Const.PROFILE_E_PARTNERCOMMUNITY_IA
													,E_Const.PROFILE_E_PARTNERCOMMUNITY_IA2
													,E_Const.PROFILE_E_PARTNERCOMMUNITY_Sumisei
													,E_Const.PROFILE_E_PARTNERCOMMUNITY_Sumisei2
												};

	public static final Set<String> PROFILE_NAMES_IA = new Set<String>{
														 E_Const.PROFILE_E_PARTNERCOMMUNITY_IA
														,E_Const.PROFILE_E_PARTNERCOMMUNITY_IA2
													};
	//Profile差し替え対応 ここまで


	//組織メールアドレス（IL-JP-sc_reg-change@nnlife.co.jp）
	public static final String EMAIL_NAME_ILJPSCREGCHANGE = 'IL-JP-sc_reg-change@nnlife.co.jp';

	// Share アクセスレベル
	public static final String SHARE_ACCESS_LEVEL_READ = 'Read';
	public static final String SHARE_ACCESS_LEVEL_EDIT = 'Edit';

	// 本社課コード
	public static final String AGENCY_KSECTION_HEAD_OFFICE = '88';

	//ID種別 Set
	public static final Set<String> EIDC_ZIDTYPES = new Set<String>{
													E_Const.ZIDTYPE_AT
													, E_Const.ZIDTYPE_AY
													, E_Const.ZIDTYPE_AH
												};

	//Contact.RecordType
	//募集人
	public static final String CONTACT_RECORDTYPE_AGENT = 'ECL3';
	//顧客
	public static final String CONTACT_RECORDTYPE_CLIENT = 'ECLT';

	// E_COVPF__c.RecordType
	// 個人保険特約
	public static final String COVPF_RECORDTYPE_ECOVPF = 'ECOVPF';
	// 個人保険特約消滅
	public static final String COVPF_RECORDTYPE_EHDCPF = 'EHDCPF';

	// 特約.保険種類
	public static final String COVPF_CRTABLE2_JA = 'JA';
	public static final String COVPF_CRTABLE2_FA = 'FA';

	//自主点検事務所　法人個人区分
	public static final String SCOFFICE_CLTTYPE_C = 'C';
	public static final String SCOFFICE_CLTTYPE_P = 'P';
	public static final Map<String,String> SCOFFICE_CLTTYPEMAP = new Map<String,String>{
		SCOFFICE_CLTTYPE_C => '法人'
		,SCOFFICE_CLTTYPE_P => '個人'
	};
}