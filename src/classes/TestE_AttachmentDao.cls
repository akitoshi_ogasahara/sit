@isTest
/**
 * E_AttachmentDaoテストクラス
 * 部分リリース用　正常系のみ
 */
private class TestE_AttachmentDao {

/*
* 観点:getRecById　メソッド正常系
* 条件:Attachmentが添付された親のIDを引数へ渡す
* 検証:親に紐づくAttachmentが1件返される
*/
	@isTest static void getRecById_test001() {
		E_CMSFile__c parent = setTestData(1,1)[0];

		Test.startTest();
		Attachment att = E_AttachmentDao.getRecById(parent.Id);
		Test.stopTest();

		System.assertEquals(parent.Attachments[0].Id, att.Id );
	}

/*
* 観点:getRecById　メソッド異常系
* 条件:Attachmentのない親のIDを引数へ渡す
* 検証:nullが返される
*/
	@isTest static void getRecById_test002() {
		E_CMSFile__c parent = setTestData(1,0)[0];

		Test.startTest();
		Attachment att = E_AttachmentDao.getRecById(parent.Id);
		Test.stopTest();

		System.assertEquals(null, att);
	}

/*
* 観点:getRecById　メソッド異常系
* 条件:引数へ誤ったIdを渡す
* 検証:nullが返される
*/
	@isTest static void getRecById_test003() {
		Test.startTest();
		Attachment att = E_AttachmentDao.getRecById(null);
		Test.stopTest();

		System.assertEquals(null, att);
	}

/*
* 観点:getRecsById　メソッド正常系
* 条件:Attachmentが添付された親のIDを引数へ渡す
* 検証:親に紐づくAttachmentが全件返されること
*/
	@isTest static void getRecsById_test001() {
		E_CMSFile__c parent = setTestData(1,2)[0];

		Test.startTest();
		List<Attachment> atts = E_AttachmentDao.getRecsById(parent.Id);
		Test.stopTest();

		System.assertEquals(parent.Attachments.size(),atts.size());
	}

/*
* 観点:getRecsById　メソッド異常系
* 条件:Attachmentがない親のIDを引数へ渡す
* 検証:0件のリストが返されること
*/
	@isTest static void getRecsById_test002() {
		E_CMSFile__c parent = setTestData(1,0)[0];

		Test.startTest();
		List<Attachment> atts = E_AttachmentDao.getRecsById(parent.Id);
		Test.stopTest();

		System.assertEquals(0 ,atts.size());
	}

/*
* 観点:getRecsById　メソッド異常系
* 条件:誤ったIDを引数へ渡す
* 検証:0件のリストが返されること
*/
	@isTest static void getRecsById_test003() {
		Test.startTest();
		List<Attachment> atts = E_AttachmentDao.getRecsById(null);
		Test.stopTest();

		System.assertEquals(0 ,atts.size());
	}

/*
* 観点:getRecsByParentId　メソッド正常系
* 条件:Attachmentが添付された親のIDを引数へ渡す
* 検証:親に紐づくAttachmentが全件Nameの降順で返されること
*/
	@isTest static void getRecsByParentId_test001() {
		E_CMSFile__c testParent = setTestData(1,2)[0];

		Test.startTest();
		List<Attachment> atts = E_AttachmentDao.getRecsByParentId(testParent.Id);
		Test.stopTest();

		E_CMSFile__c parent = [SELECT Id
									,(SELECT Id FROM Attachments ORDER BY Name)
								FROM E_CMSFile__c
								WHERE Id =: testParent.Id];

		System.assertEquals(parent.Attachments.size(),atts.size());
		for(Integer i=0;i<atts.size();i++){
			System.assertEquals(parent.Attachments[i].Id ,atts[i].Id);
		}
	}

/*
* 観点:getRecsByParentId　メソッド異常系
* 条件:Attachmentがない親のIDを引数へ渡す
* 検証:0件のリストが返されること
*/
	@isTest static void getRecsByParentId_test002() {
		E_CMSFile__c testParent = setTestData(1,0)[0];

		Test.startTest();
		List<Attachment> atts = E_AttachmentDao.getRecsByParentId(testParent.Id);
		Test.stopTest();

		System.assertEquals(0 ,atts.size());
	}

/*
* 観点:getRecsByParentId　メソッド異常系
* 条件:誤ったIDを引数へ渡す
* 検証:0件のリストが返されること
*/
	@isTest static void getRecsByParentId_test003() {
		Test.startTest();
		List<Attachment> atts = E_AttachmentDao.getRecsByParentId(null);
		Test.stopTest();

		System.assertEquals(0 ,atts.size());
	}

/*
* 観点:getRecsByParentIds　メソッド正常系
* 条件:Attachmentが添付された親のIDのSetを引数へ渡す
* 検証:引数に渡したIDに紐づくAttachmentのリストが返されること
*/
	@isTest static void getRecsByParentIds_test001(){
		List<E_CMSFile__c> parents = setTestData(2,2);
		Set<Id> parentId = new Set<Id>();
		for(E_CMSFile__c prt : parents){
			parentId.add(prt.Id);
		}

		Test.startTest();
		Map<Id,List<Attachment>> recs = E_AttachmentDao.getRecsByParentIds(parentId);
		Test.stopTest();

		System.assertEquals(parents.size(),recs.size());
		for(E_CMSFile__c parent :parents){
			System.assertEquals(parent.Attachments.size(),recs.get(parent.Id).size());
		}
	}

/*
* 観点:getRecsByParentIds　メソッド異常系
* 条件:Attachmentがない親のIDのSetを引数へ渡す
* 検証:0件のリストが返されること
*/
	@isTest static void getRecsByParentIds_test002(){
		List<E_CMSFile__c> parents = setTestData(2,0);
		Set<Id> parentId = new Set<Id>();
		for(E_CMSFile__c prt : parents){
			parentId.add(prt.Id);
		}

		Test.startTest();
		Map<Id,List<Attachment>> recs = E_AttachmentDao.getRecsByParentIds(parentId);
		Test.stopTest();

		System.assertEquals(0,recs.size());
	}

/*
* 観点:getRecsByParentIds　メソッド異常系
* 条件:引数へnullを渡す
* 検証:0件のリストが返されること
*/
	@isTest static void getRecsByParentIds_test003(){
		Test.startTest();
		Map<Id,List<Attachment>> recs = E_AttachmentDao.getRecsByParentIds(null);
		Test.stopTest();

		System.assertEquals(0 ,recs.size());
	}

/*
* 観点:getRecsByUpsertKey　メソッド正常系
* 条件:Attachmentが添付されたファイル情報の外部ID（UpsertKey__c）を引数へ渡す
* 検証:指定した外部ID（UpsertKey__c）のファイル情報がAttachmentを持った状態で返されること
*/
	@isTest static void getRecByUpsertKey_test001(){
		E_CMSFile__c parent = setTestData(1,2)[0];

		Test.startTest();
		E_CMSFile__c rec = E_AttachmentDao.getRecByUpsertKey(parent.UpsertKey__c);
		Test.stopTest();

		System.assertEquals(parent.Id ,rec.Id);
		System.assertEquals(parent.Attachments.size() ,rec.Attachments.size());
	}

/*
* 観点:getRecsByUpsertKey　メソッド異常系
* 条件:Attachmentがないファイル情報の外部ID（UpsertKey__c）を引数へ渡す
* 検証:指定した外部ID（UpsertKey__c）のファイル情報が0件のAttachmentで返されること
*/
	@isTest static void getRecByUpsertKey_test002(){
		E_CMSFile__c parent = setTestData(1,0)[0];

		Test.startTest();
		E_CMSFile__c rec = E_AttachmentDao.getRecByUpsertKey(parent.UpsertKey__c);
		Test.stopTest();

		System.assertEquals(0 ,rec.Attachments.size());
	}

/*
* 観点:getRecsByUpsertKey　メソッド異常系
* 条件:引数へnullを渡す
* 検証:nullが返されること
*/
	@isTest static void getRecByUpsertKey_test003(){
		E_CMSFile__c parent = setTestData(1,0)[0];

		Test.startTest();
		E_CMSFile__c rec = E_AttachmentDao.getRecByUpsertKey(null);
		Test.stopTest();

		System.assertEquals(null ,rec);
	}

/*
* 観点:getRecsByUpsertKey　メソッド異常系
* 条件:引数へ空文字を渡す
* 検証:nullが返されること
*/
	@isTest static void getRecByUpsertKey_test004(){
		E_CMSFile__c parent = setTestData(1,0)[0];

		Test.startTest();
		E_CMSFile__c rec = E_AttachmentDao.getRecByUpsertKey('');
		Test.stopTest();

		System.assertEquals(null ,rec);
	}

/*
* 観点:getRecsByParentAndDescription　メソッド正常系
* 条件:Attachmentが添付されたファイル情報のIDと内容を引数へ渡す
* 検証:指定したIDと内容のAttachmentが返されること
*/
	@isTest static void getRecsByParentAndDescription_test001(){
		List<E_CMSFile__c> parents = setTestData(2,3);
		update new List<Attachment>{
			  new Attachment(Id = parents[0].Attachments[0].Id, Description = 'type1')
			, new Attachment(Id = parents[1].Attachments[0].Id, Description = 'type1')
			, new Attachment(Id = parents[1].Attachments[1].Id, Description = 'type2')
			, new Attachment(Id = parents[1].Attachments[2].Id, Description = 'type1')
		};
		delete new Attachment(Id = parents[1].Attachments[0].Id);

		Test.startTest();
		List<Attachment> rec = E_AttachmentDao.getRecsByParentAndDescription(parents[1].Id, 'type1');
		Test.stopTest();

		System.assertEquals(1, rec.size());
		System.assertEquals(parents[1].Attachments[2].Id, rec[0].Id);
	}

/*
* 観点:getRecsByParentAndDescription　メソッド異常系
* 条件:Attachmentが添付されたファイル情報のIDと内容を引数へ渡す
* 検証:指定したIDと内容のAttachmentが返されること
*/
	@isTest static void getRecsByParentAndDescription_test002(){
		E_CMSFile__c parent = setTestData(1,1)[0];
		update new Attachment(Id = parent.Attachments[0].Id, Description = 'type1');
		delete new Attachment(Id = parent.Attachments[0].Id);

		Test.startTest();
		List<Attachment> rec = E_AttachmentDao.getRecsByParentAndDescription(parent.Id, 'type1');
		Test.stopTest();

		System.assertEquals(0, rec.size());
	}

/*
* 観点:getRecsByParentAndDescription　メソッド異常系
* 条件:Attachmentが添付されたファイル情報のIDと内容を引数へ渡す
* 検証:指定したIDと内容のAttachmentが返されること
*/
	@isTest static void getRecsByParentAndDescription_test003(){
		E_CMSFile__c parent = setTestData(1,1)[0];
		update new Attachment(Id = parent.Attachments[0].Id, Description = 'type1');

		Test.startTest();
		List<Attachment> rec = E_AttachmentDao.getRecsByParentAndDescription(parent.Id, '');
		Test.stopTest();

		System.assertEquals(0, rec.size());
	}

/*
* 観点:getRecsByNotIdAndDescription　メソッド正常系
* 条件:Attachmentが添付されたファイル情報のIDと内容を引数へ渡す
* 検証:指定したIDと内容のAttachmentが返されること
*/
	@isTest static void getRecsByNotIdAndDescription_test001(){
		List<E_CMSFile__c> parents = setTestData(2,4);
		update new List<Attachment>{
			  new Attachment(Id = parents[0].Attachments[0].Id, Description = 'type1')
			, new Attachment(Id = parents[1].Attachments[0].Id, Description = 'type1')
			, new Attachment(Id = parents[1].Attachments[1].Id, Description = 'type2')
			, new Attachment(Id = parents[1].Attachments[2].Id, Description = 'type1')
			, new Attachment(Id = parents[1].Attachments[3].Id, Description = 'type1')
		};
		delete new Attachment(Id = parents[1].Attachments[0].Id);

		Test.startTest();
		List<Attachment> rec = E_AttachmentDao.getRecsByNotIdAndDescription(parents[1].Attachments[3].Id, parents[1].Id, 'type1');
		Test.stopTest();

		System.assertEquals(1, rec.size());
		System.assertEquals(parents[1].Attachments[2].Id, rec[0].Id);
	}

/*
* 観点:getRecsByNotIdAndDescription　メソッド異常系
* 条件:Attachmentが添付されたファイル情報のIDと内容を引数へ渡す
* 検証:指定したIDと内容のAttachmentが返されること
*/
	@isTest static void getRecsByNotIdAndDescription_test002(){
		E_CMSFile__c parent = setTestData(1,1)[0];
		update new Attachment(Id = parent.Attachments[0].Id, Description = 'type1');

		Test.startTest();
		List<Attachment> rec = E_AttachmentDao.getRecsByNotIdAndDescription(parent.Attachments[0].Id, parent.Id, 'type1');
		Test.stopTest();

		System.assertEquals(0, rec.size());
	}

/*
* 観点:getRecsByNotIdAndDescription　メソッド異常系
* 条件:Attachmentが添付されたファイル情報のIDと内容を引数へ渡す
* 検証:指定したIDと内容のAttachmentが返されること
*/
	@isTest static void getRecsByNotIdAndDescription_test003(){
		E_CMSFile__c parent = setTestData(1,1)[0];
		update new Attachment(Id = parent.Attachments[0].Id, Description = 'type1');

		Test.startTest();
		List<Attachment> rec = E_AttachmentDao.getRecsByNotIdAndDescription(parent.Attachments[0].Id, parent.Id, '');
		Test.stopTest();

		System.assertEquals(0, rec.size());
	}

/*
* テストデータ作成
* @param parentNum Attachmentの親(ファイル情報)の数
* @param attNum　1つの親に対するAttachmentの数
* @return 親となるファイル情報のリスト
*/
	private static List<E_CMSFile__c> setTestData(Integer parentNum,Integer attNum){
		//親メッセージマスタ
		E_MessageMaster__c msg = TestE_TestUtil.createMessageMaster(true,'メッセージマスタ',null,'CMS','CMS','1');
		//ファイル情報
		List<E_CMSFile__c> parents = new List<E_CMSFile__c>();
		for(Integer i=0;i < parentNum;i++){
			E_CMSFile__c cmsfile = TestI_TestUtil.createCMSFile(false,'ファイル情報00'+i, msg.Id,'pattern02');
			cmsfile.SortOrder__c = i;
			parents.add(cmsfile);
		}
		insert parents;

		//添付ファイル
		List<Attachment> atts = new List<Attachment>();
		for(E_CMSFile__c parent : parents){
			for(Integer i = 0; i < attNum;i++){
				Attachment att = TestI_TestUtil.createAttachment(false, parent.Id);
				att.Name = '添付0'+ i;
				atts.add(att);
			}
		}
		insert atts;

		return [SELECT Id
					,Name
					,UpsertKey__c
					,(SELECT Id, Name FROM Attachments ORDER BY LastmodifiedDate Desc)
				FROM E_CMSFile__c
				WHERE Id In: parents];
	}
}