@isTest
private class TestE_InveExtender {
	
	//グラフコンポーネントの引数
	static testMethod void getExecuteKindTest001(){
		
		E_InveExtender extender = createExtender();
		
		Test.startTest();
		String result = extender.getExecuteKind();
		Test.stopTest();
		
		System.assertEquals(E_Const.GRAPH_INVESTMENT_BUY, result);
	}
	
	//コンストラクタ
	static testMethod void constructorTest001(){
		
		E_ITHPF__c record = new E_ITHPF__c();
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveController extension = new E_InveController(controller);
		
		Test.startTest();
		E_InveExtender extender = new E_InveExtender(extension);
		Test.stopTest();
		
	}
	
	//init
	//doAuth成功
	static testMethod void initTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
			
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			system.assertNotEquals(null, extender.pageRef);
			/*
			system.assertEquals('3,333', extender.zinvamtAmount);
			system.assertEquals('0', extender.zbrndper01Amount);
			*/
		}
	}
	
	//init
	//doAuth失敗
	static testMethod void initTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
		
			system.assertEquals(null, extender.zinvamtAmount);
			system.assertEquals(null, extender.zbrndper01Amount);
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			system.assertNotEquals(null, result);
		}
	}
	
	//ページアクション
	//doAuth失敗
	static testMethod void pageActionTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			system.assertNotEquals(null, result);
		}
	}
	
	//テストユーザ作成
	static User createUser(String flag03){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG03__c = flag03;
		insert idcpf;
		return u;
	}
	
	//Extender作成
	static E_InveExtender createExtender(){
		//レコード作成
		E_ITHPF__c record = new E_ITHPF__c();
		insert record;
		E_ITAPF__c itapf = new E_ITAPF__c();
		insert itapf;
		List<E_ITFPF__c> itfpfList = new List<E_ITFPF__c>();
		for(Integer i=0; i<2;i++){
			E_ITFPF__c itfpf = new E_ITFPF__c();
			itfpf.E_ITHPF__c = record.Id;
			itfpf.E_ITAPF__c = itapf.Id;
			itfpfList.add(itfpf);
		}
		itfpfList[0].ZINVAMT__c = 1111;
		itfpfList[1].ZINVAMT__c = 2222;
		insert itfpfList;
		
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveController extension = new E_InveController(controller);
		E_InveExtender extender = new E_InveExtender(extension);
		return extender;
	}
}