public abstract class DS_AbstractController extends E_AbstractController {

	// ダウンロード履歴
	private E_DownloadHistorry__c dlHistory;

	// ページアクセスログの取得
	protected E_Log__c pageAccessLog;
	// ダウンロード帳票種別
	public String dlFormNo {get{return DS_Const.DH_FORMNO_DISEASE;} set;}

	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	
	//利用アプリケーション『医的な引受の目安』
	public String getAppName(){
		return DS_Const.APPNAME;
	}

	/**
	 * Construnctor
	 */
	public DS_AbstractController(){
		pageMessages = getPageMessages();
	}

	/**
	 * init
	 */
	protected virtual PageReference init(){	
		PageReference pr = super.doAuth(null, null);
		if(pr!=null){
			return pr;
		}

		//IRIS選択でない場合は、E_Homeへ遷移（GuestユーザもI_Abstractページの表示OKとする）
		if(!iris.getIsIRIS() && !access.isGuest()){
			pr = Page.E_AppSwitch;
			pr.getParameters().put(I_Const.APPSWITCH_URL_PARAM_APP, I_Const.APP_MODE_NNLINK);		//?app=nnlink
			return pr;
		}

		return null;
	}
	
	//ページアクセスログをController内で出力しない場合　overrideしてTrueにしてください。readOnly=trueの場合など
	protected virtual Boolean preventWritePageAccessLog(){
		return false;
	}
	
	/**
	 * PageAction
	 */
	public PageReference PageAction(){
		PageReference pr = null;
		//pageMessages= getPageMessages();

		try{
			if(pageAccessLog == null){
				pageAccessLog = E_LogUtil.createLog(getAppName());
				pageAccessLog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_PAGE;
				//サブクラスでの初期処理
				if(!pageMessages.getHasErrorMessages()){
					pr = init();		//エラーが発生していない状態のみ、init()を実施
					if(pr!=null){
						pageMessages.addInfoMessage('[リダイレクト] ' + pr.getUrl());
					}
				}	
				createDSLog();
			}
			
		}catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage(),e.getStackTraceString());
		}
		return pr;
	}

	/**
	 * アクセスログ登録
	 */
	 public void createDSLog(){
		if(E_LogUtil.requiredLogging()){
			try{
				pageAccessLog.isError__c = pageMessages.getHasErrorMessages();
				List<String> logDetails = new List<String>();
				if(String.isNotBlank(pageAccessLog.detail__c)){		//既に設定してあるものを設定しておく
					logDetails.add(pageAccessLog.detail__c);
				}
				
				String msg = E_LogUtil.getLogDetailString(pageMessages.getMessages());
				if(String.isNotBlank(msg)){
					logDetails.add(msg);
				}
				if(logDetails.size()>0){
					pageAccessLog.detail__c = String.join(logDetails, '\r\n');
				}
				
				//文字数オーバーの場合TruncateしてUpsert PageAccessLog許可の場合
				if(!preventWritePageAccessLog()){
					Database.DMLOptions dml = new Database.DMLOptions();
					dml.allowFieldTruncation = true;
					pageAccessLog.setOptions(dml);
					upsert pageAccessLog;
				}
			}catch(Exception e){
				pageMessages.addErrorMessage('★★★アクセスログの更新に失敗しました。★★★' + e.getMessage());
			}	
		}		
	}


	/**
	 * PDF出力時ダウンロード履歴、OPROパラメータ生成（OPROには、isSumiseiのフラグで項目の値を出し分けてもらう作りにする、アクションタイプはNNLinkLogの「PDF出力」にする）
	 */
	//DLCondition作成
	protected virtual E_DownloadCondition.DiseaseNumberInfo createDLCondition(){ return new E_DownloadCondition.DiseaseNumberInfo(); }

	//PDFダウンロード
	public PageReference doDownloadPdf(){
		try{
			 //E_DownloadConditionの生成
			E_DownloadCondition.DiseaseNumberInfo dlCondi= createDLCondition();
			
			// ダウンロード履歴の登録
			dlHistory = new E_DownloadHistorry__c();
			dlHistory.FormNo__c = dlFormNo;
			//出力形式:PDF
			dlHistory.outputType__c = E_Const.DH_OUTPUTTYPE_PDF;
			dlHistory.Conditions__c = dlCondi.toJSON();
			insert dlHistory;
			
		}catch(Exception e){
			pageMessages.clearMessages();
			pageMessages.addErrorMessage('［システムエラー］ ' + e.getMessage());
		}
		
		return null;
	}

	//OPRO呼び出しURL生成
	public string getDownloadId(){
		if(dlHistory == null || dlHistory.Id == null){
			return '';
		}
		return dlHistory.Id;
	}
}