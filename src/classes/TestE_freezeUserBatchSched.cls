@isTest
private class TestE_freezeUserBatchSched {
	@isTest static void executeTest() {

		Test.startTest();
		E_freezeUserBatchSched scheduled = new E_freezeUserBatchSched();
		String jobId = System.schedule('Freeze User job', '0 30 18 21 11 ? 2030', scheduled);

		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
		System.assertEquals(0, ct.TimesTriggered);			//実施回数
		System.assertEquals('2030-11-21 18:30:00',String.valueOf(ct.NextFireTime));		//次回実行日時
		Test.stopTest();
	}
}