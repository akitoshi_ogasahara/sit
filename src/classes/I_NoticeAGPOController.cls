public with sharing class I_NoticeAGPOController extends I_NoticeAbstractController{

	public String filteringType {get; set;}
	public String disclaimer {get; set;}
	public String discFilter{get; set;}
	public String discTransdate{get;set;}
	public Boolean filterFlag{get;set;}
	
	// MenuKey
	protected override String getLinkMenuKey(){
		return I_NoticeConst.MNKEY_AGPO;
	}
	private final String infoPageKey = 'agpo_info';
	private List<String> getInfoURLs(){
		List<I_ContentMaster__c> cms = [Select id From I_ContentMaster__c Where Page__r.page_unique_key__c =: infoPageKey AND Style__c != 'アンカー' Order By DisplayOrder__c];
		return new List<String>{'IRIS?pgkey=' + infoPageKey + '#iris-cms-' + cms[0].id,'IRIS?pgkey=' + infoPageKey + '#iris-cms-' + cms[1].id};
	}
	
	/**
	 * Constructor
	 */
	public I_NoticeAGPOController() {
		super();
		
		// ソートデフォルト設定
		sortIsAsc = false;
		sortType = 'response';
		
		// 帳票種別
		dlFormNo = E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO;
	}

	/**
	 * init
	 */
	protected override PageReference init(){
		this.filteringType = this.getFilteringOptions().get(0).getValue();
		PageReference pr = super.init();
		pageAccessLog.Name = '保有契約異動通知';
		rowCnt = dtos.size();
		disclaimer = string.format(getDisclaimer().get('DNI|008'),getInfoURLs());

		return pr;
	}

	/**
	 * 表示リスト作成 
	 */
	protected override void createList(){
		dtos = new List<I_NoticeDTO>();
		discFilter = '';
		discTransdate = '';
		filterFlag = false;
		String conditions = '';
		if(filteringType != I_NoticeConst.AGPO_TITLE01){
			conditions += ' And ';
			conditions += ' TITLE__c = \'' ;
			conditions += (filteringType == I_NoticeConst.AGPO_TITLE05)? '' :filteringType;
			conditions += '\'';
		}
		conditions += createNoticeCondition();
		for(E_AGPO__c rec : E_AGPODao.getRec(conditions)){
			I_NoticeDTO agpo = new I_NoticeDTO(rec);
			dtos.add(agpo);
		}
		if(dtos.size() != 0){
			discTransdate = '通知発信日：' + dtos[0].transmissionDate.left(4) + '年' + dtos[0].transmissionDate.mid(5,2) + '月' + dtos[0].transmissionDate.right(2) + '日';
		}
		if(filteringType != I_NoticeConst.AGPO_TITLE01){
			String recSize = (dtos.size() > 1000)? '1000' : String.valueOf(dtos.size());
			discFilter = string.format(getDisclaimer().get('DNI|009'),new List<String>{recSize});
			filterFlag = true;
		}

	}

	public override Boolean getIsViewDl(){
		Boolean isView = true;
		// 対象件数が0件の場合は非表示
		if(rowCnt == 0){
			return false;
		}
		
		// 社員ユーザ
		if(access.isEmployee()){
			// フィルターモードの場合は表示
			if(isFilterAh || isFilterAy || isFilterAt){
				isView = true;
			}else{
				isView = false;
			}
		}
		return isView;
	}


	/**
	 * 絞り込みを行う値の候補リスト
	 */
	public List<SelectOption> getFilteringOptions() {
		return new List<SelectOption>{
			new SelectOption(I_NoticeConst.AGPO_TITLE01, I_NoticeConst.AGPO_TITLE01),
			new SelectOption(I_NoticeConst.AGPO_TITLE02, I_NoticeConst.AGPO_TITLE02),
			new SelectOption(I_NoticeConst.AGPO_TITLE03, I_NoticeConst.AGPO_TITLE03),
			new SelectOption(I_NoticeConst.AGPO_TITLE04, I_NoticeConst.AGPO_TITLE04),
			new SelectOption(I_NoticeConst.AGPO_TITLE05, I_NoticeConst.AGPO_TITLE05)
		};
	}


	/**
	 * 再描画を行う前に専用の処理が必要があれば実行する。
	 */
	public PageReference changeFilteringType() {
		pageMessages.clearMessages();
		try{
		// 表示リスト作成
		createList();
		// 2017/06/19 表示件数初期化処理追加対応 Start
		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		// End
		// 件数チェック
		checkSize();
		// ソートデフォルト設定
		sortIsAsc = false;
		sortType = 'response';

		}catch(Exception e){
			pageMessages.clearMessages();
			pageMessages.addErrorMessage('［システムエラー］ ' + e.getMessage());
			this.hasError = true;
		}
		return null;
	}

	protected override List<String> getRecordTypeDevName(){
				return new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI),
								(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_SPVA),		
								(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_DCOLI),		
								(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_DSPVA)		
							};
	}
}