@isTest
private class TestI_ContentSearchResult {
	
	private static testMethod void should_build_instance_with_marker_when_receive_isnerted_menu_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getMenuByName('enabled menu'));
			I_KeywordMarker marker = new I_KeywordMarker();
			marker.addKeyword('menu');
			result.setMarker(marker);
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(true, result.isMatched());
			System.assertEquals('enabled <mark>menu</mark>', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_created_menu_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new I_MenuMaster__c());
			System.assertEquals(false, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_page_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getPageByName('enabled page'));
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled page', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_feature_page_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getPageByName('feature page'));
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('feature page', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_created_page_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new I_PageMaster__c());
			System.assertEquals(false, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_url_content_record1() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('enabled content all'));
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled content all', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_url_content_record2() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('enabled content other than employee'));
			System.assertEquals(false, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled content other than employee', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_url_content_record3() {
		System.runAs(getAgent()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('enabled content other than agency'));
			System.assertEquals(false, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled content other than agency', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_url_content_record4() {
		User guest = getGuest();
		if (guest != null) {
			System.runAs(guest) {
				I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('enabled content other than internet'));
				System.assertEquals(false, result.hasAccess());
				System.assertEquals(false, result.isMatched());
				System.assertEquals('enabled content other than internet', result.getMarkedName());
				System.assertEquals(null, result.getMarkedContext());
			}
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_url_content_record5() {
		User guest = getGuest();
		if (guest != null) {
			PageReference ref = Page.IRIS_Top;
			ref.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
			Test.setCurrentPageReference(ref);
			System.runAs(guest) {
				I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('enabled content other than gw'));
				System.assertEquals(false, result.hasAccess());
				System.assertEquals(false, result.isMatched());
				System.assertEquals('enabled content other than gw', result.getMarkedName());
				System.assertEquals(null, result.getMarkedContext());
			}
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_info_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('enabled info'));
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('enabled info', result.getMarkedName());
			System.assertEquals('お知らせ', result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_qa_record1() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('qa content'));
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('qa content', result.getMarkedName());
			System.assertEquals('ご質問', result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_inserted_qa_record2() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Retriever().getContentByName('qa content dom'));
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('qa content dom', result.getMarkedName());
			System.assertEquals('ご質問', result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_created_content_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new I_ContentMaster__c());
			System.assertEquals(true, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals('', result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_build_instance_when_receive_unsupported_record() {
		System.runAs(getAdmin()) {
			I_ContentSearchResult result = I_ContentSearchResult.build(new Account());
			System.assertEquals(false, result.hasAccess());
			System.assertEquals(false, result.isMatched());
			System.assertEquals(null, result.getMarkedName());
			System.assertEquals(null, result.getMarkedContext());
		}
	}

	private static testMethod void should_sort_result_list_with_sort_option() {
		System.runAs(getAdmin()) {
			List<I_ContentSearchResult> results = new List<I_ContentSearchResult>{
				new I_ContentSearchResult(), new I_ContentSearchResult(), new I_ContentSearchResult()
			};
			I_ContentSearchResult.setSortOption(I_ContentSearchResult.SORT_TYPE_PAGE_NAME, true);
			System.assertEquals(true, I_ContentSearchResult.sortBy(I_ContentSearchResult.SORT_TYPE_PAGE_NAME));
			System.assertEquals(true, I_ContentSearchResult.isAsc());
			results.sort();

			I_ContentSearchResult.setSortOption(I_ContentSearchResult.SORT_TYPE_PAGE_TYPE, false);
			System.assertEquals(true, I_ContentSearchResult.sortBy(I_ContentSearchResult.SORT_TYPE_PAGE_TYPE));
			System.assertEquals(false, I_ContentSearchResult.isAsc());
			results.sort();
		}
	}

	private static User getAdmin() {
		List<User> users = [SELECT id FROM User WHERE LastName =: 'fstest'];
		return users.size() > 0 ? users.get(0) : null;
	}

	private static User getAgent() {
		List<User> users = [SELECT id FROM User WHERE LastName =: 'fsagent'];
		return users.size() > 0 ? users.get(0) : null;
	}

	private static User getGuest() {
		List<User> users = [SELECT id FROM User WHERE UserType =: E_Const.USERTYPE_GUEST AND Profile.Name LIKE '%nnlinkage%' AND IsActive = true];
		return users.size() > 0 ? users.get(0) : null;
	}

	@testSetup
	public static void setup() {
		TestE_TestUtil.createIDCPF(true, TestI_TestUtil.createUser(true, 'fstest', 'システム管理者').Id);
		TestE_TestUtil.createIDCPF(true, TestI_TestUtil.createAgentUser(true, 'fsagent', 'E_PartnerCommunity', TestI_TestUtil.createContact(true, TestI_TestUtil.createAccount(true, null).Id, 'fsagent').Id).Id);

		Map<String, I_MenuMaster__c> menusByKey = new Map<String, I_MenuMaster__c>();
		{
			I_MenuMaster__c menu;
			menu = new I_MenuMaster__c(
				name = 'enabled menu',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = 'お知らせ',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_INFO
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = 'qa menu',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_FAQ
			);
			menusByKey.put(menu.name, menu);

			menu = new I_MenuMaster__c(
				name = 'feature menu',
				MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY,
				SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
			);
			menusByKey.put(menu.name, menu);
		}
		insert menusByKey.values();
		
		Map<String, I_PageMaster__c> pagesByKey = new Map<String, I_PageMaster__c>();
		{
			I_PageMaster__c page;
			page = new I_PageMaster__c(
				name = 'enabled page',
				Menu__c = menusByKey.get('enabled menu').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = 'qa page',
				Menu__c = menusByKey.get('qa menu').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = 'feature page',
				Menu__c = menusByKey.get('feature menu').Id
			);
			pagesByKey.put(page.name, page);

			page = new I_PageMaster__c(
				name = 'お知らせ',
				Menu__c = menusByKey.get('お知らせ').Id
			);
			pagesByKey.put(page.name, page);
		}
		insert pagesByKey.values();

		Map<String, I_ContentMaster__c> sectionsByKey = new Map<String, I_ContentMaster__c>();
		{
			I_ContentMaster__c section;
			section = new I_ContentMaster__c(
				name = 'enabled section',
				Page__c = pagesByKey.get('enabled page').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = 'お知らせ',
				Page__c = pagesByKey.get('お知らせ').Id
			);
			sectionsByKey.put(section.name, section);

			section = new I_ContentMaster__c(
				name = 'qa section',
				Page__c = pagesByKey.get('qa page').Id
			);
			sectionsByKey.put(section.name, section);
		}
		insert sectionsByKey.values();

		Map<String, I_ContentMaster__c> componentsByKey = new Map<String, I_ContentMaster__c>();
		{
			I_ContentMaster__c component;

			component = new I_ContentMaster__c(
				name = 'enabled content all',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'URL',
				CanDisplayVia__c = String.join(new List<String>{
					I_Const.DISP_RIGHT_EMPLOYEE, I_Const.DISP_RIGHT_AGENCY, I_Const.ACCESS_VIA_INTERNET, I_Const.ACCESS_VIA_CMN_GW
				}, ';')
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled content other than employee',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'URL',
				CanDisplayVia__c = String.join(new List<String>{
					I_Const.DISP_RIGHT_AGENCY, I_Const.ACCESS_VIA_INTERNET, I_Const.ACCESS_VIA_CMN_GW
				}, ';')
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled content other than agency',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'URL',
				CanDisplayVia__c = String.join(new List<String>{
					I_Const.DISP_RIGHT_EMPLOYEE, I_Const.ACCESS_VIA_INTERNET, I_Const.ACCESS_VIA_CMN_GW
				}, ';')
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled content other than internet',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'URL',
				CanDisplayVia__c = String.join(new List<String>{
					I_Const.DISP_RIGHT_EMPLOYEE, I_Const.DISP_RIGHT_AGENCY, I_Const.ACCESS_VIA_CMN_GW
				}, ';')
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled content other than gw',
				ParentContent__c = sectionsByKey.get('enabled section').Id,
				ClickAction__c = 'URL',
				CanDisplayVia__c = String.join(new List<String>{
					I_Const.DISP_RIGHT_EMPLOYEE, I_Const.DISP_RIGHT_AGENCY, I_Const.ACCESS_VIA_INTERNET
				}, ';')
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'enabled info',
				ParentContent__c = sectionsByKey.get('お知らせ').Id,
				ClickAction__c = 'URL',
				Content__c = 'お知らせ'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'qa content',
				ParentContent__c = sectionsByKey.get('qa section').Id,
				ClickAction__c = 'URL',
				Content__c = 'ご質問'
			);
			componentsByKey.put(component.name, component);

			component = new I_ContentMaster__c(
				name = 'qa content dom',
				ParentContent__c = sectionsByKey.get('qa section').Id,
				ClickAction__c = 'URL',
				Content__c = 'ご質問',
				Dom_Id__c = 'dom-dom'
			);
			componentsByKey.put(component.name, component);
		}
		insert componentsByKey.values();
	}

	private without sharing class Retriever {
		
		public I_MenuMaster__c getMenuByName(String name) {
			I_MenuMaster__c result;
			for (I_MenuMaster__c record: [SELECT name FROM I_MenuMaster__c WHERE name =: name]) {
				result = record;
				break;
			}
			return result;
		}
	
		public I_PageMaster__c getPageByName(String name) {
			I_PageMaster__c result;
			for (I_PageMaster__c record: [SELECT name, Menu__c, page_unique_key__c, Default__c  FROM I_PageMaster__c WHERE name =: name]) {
				result = record;
				break;
			}
			return result;
		}
	
		public I_ContentMaster__c getContentByName(String name) {
			I_ContentMaster__c result;
			for (I_ContentMaster__c record: [
				SELECT
					Name,
					Content__c,
					LinkURL__c,
					ClickAction__c,
					CanDisplayVia__c,
					Dom_Id__c,
					Category__c,
					PamphletCategory__c,
					ParentContent__c,
					ParentContent__r.Page__c,
					ParentContent__r.Page__r.Menu__c,
					ParentContent__r.Page__r.Menu__r.Name,
					ParentContent__r.Page__r.Menu__r.DestUrl__c,
					ParentContent__r.Page__r.Menu__r.MainCategoryAlias__c
				FROM
					I_ContentMaster__c
				WHERE
					name =: name
			]) {
				result = record;
				break;
			}
			return result;
		}
	}

}