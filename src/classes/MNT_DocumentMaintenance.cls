global with sharing class MNT_DocumentMaintenance extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public MNT_DocumentMaintenanceExtender getExtender() {return (MNT_DocumentMaintenanceExtender)extender;}
	{
	setApiVersion(42.0);
	}
	public MNT_DocumentMaintenance(ApexPages.StandardController controller) {
		super(controller);
		appComponentProperty = new Map<String, Map<String, Object>>();
		appComponentProperty.put('Component112',new Map<String,Object>{'id'=>'','top'=>'','left'=>'','width'=>'','height'=>'','SIncludeOrder'=>'System.Object','p_rendered'=>'{!extender.isUpload && record.ParentContent__c == null}','p_newFile'=>'{!extender.uploadFile}','Component__Width'=>'200','Component__Height'=>'30','Component__id'=>'Component112','Component__Name'=>'EUploadAttachment','Component__NameSpace'=>'','Component__Top'=>'0','Component__Left'=>'0'});
		registTransitionControl='/apex/MNT_DocumentView?id={ID}';
		editTransitionControl='/apex/MNT_DocumentView?id={ID}';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.filePath__c;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.DOM_Id__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.NewValidTo__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.DocumentCategory__c;
		f = I_ContentMaster__c.fields.MaxCopies__c;
		f = I_ContentMaster__c.fields.CannotBeOrder__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_DocumentMaintenance';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addField('Name');
			mainQuery.addField('filePath__c');
			mainQuery.addField('FormNo__c');
			mainQuery.addField('DOM_Id__c');
			mainQuery.addField('DisplayFrom__c');
			mainQuery.addField('ValidTo__c');
			mainQuery.addField('NewValidTo__c');
			mainQuery.addField('ClickAction__c');
			mainQuery.addField('MaxCopies__c');
			mainQuery.addField('CannotBeOrder__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('DocumentCategory__c');
			mainQuery.addFieldAsOutput('FormNo__c');
			mainQuery.addFieldAsOutput('DOM_Id__c');
			mainQuery.addFieldAsOutput('DisplayFrom__c');
			mainQuery.addFieldAsOutput('ValidTo__c');
			mainQuery.addFieldAsOutput('NewValidTo__c');
			mainQuery.addFieldAsOutput('ClickAction__c');
			mainQuery.addFieldAsOutput('filePath__c');
			mainQuery.addFieldAsOutput('ParentContent__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_DocumentMaintenanceExtender(this);
			init();
			if (record.Id == null) {
				saveOldValues();
			}

			extender.init();
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}