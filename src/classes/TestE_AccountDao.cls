@isTest
private class TestE_AccountDao {
    //--- getRecByIdメソッド ---
    static testMethod void getRecByIdTest(){
        //テストデータ作成
        String expected = 'テストアカウント';
        Account a = new Account(Name=expected);
        INSERT a;
        
        String accountId = a.Id;
        Account rec = E_AccountDao.getRecById(accountId); //テスト対象メソッド
        System.assertEquals(expected, rec.Name);
    }

    
    //--- getRecsByParentIdメソッド ---
    static testMethod void getRecsByParentIdTest(){
        //テストデータ作成
        String expectedName = 'テストgetRecsByParentIdTest';
        Account a = new Account(Name=expectedName);
        INSERT a;
        
        String accountId = a.Id;
        List<Account> returned = E_AccountDao.getRecsByParentId(accountId); //テスト対象メソッド 
        System.assertNotEquals(null, returned);
    }

    
    //--- getRecByZHEADAYメソッド ---
    static testMethod void getRecByZHEADAYTest(){      
        List<Account> returned = E_AccountDao.getRecByZHEADAY(); //テスト対象メソッド
        System.assertNotEquals(null, returned);
    }
    
    
    //--- getRecsByMRCodeメソッド ---
    static testMethod void getRecsByMRCodeTest(){
        String brunchCode = 'XX';	//if (brunchCode.left(2) != '**') -> true にする
        List<Account> returned = E_AccountDao.getRecsByMRCode(brunchCode); //テスト対象メソッド
        
        System.assertNotEquals(null, returned);
    }

    
    //--- getCountIsMotherOfficeメソッド ---
    static testMethod void getCountIsMotherOfficeTest(){
        //テストデータ作成
        Account a = new Account(Name='テストgetCountIsMotherOfficeTest', E_CL2PF_BRANCH__c='XX');
        INSERT a;
        
        String id = a.Id;
        String brunchCode = a.E_CL2PF_BRANCH__c;
        Integer actual = E_AccountDao.getCountIsMotherOffice(id, brunchCode); //テスト対象メソッド
        
        Integer expected = 0;
        System.assertEquals(expected, actual);
    }

    
    //--- getRecsByAgencyメソッド ---
    static testMethod void getRecsByAgencyTest(){
        //テストデータ作成
        Account a = new Account(Name='getRecsByAgencyTest', E_CL2PF_BRANCH__c='XX');
        INSERT a;
        
        String id = a.Id;
        String brunchCode = a.E_CL2PF_BRANCH__c;
        Boolean isAllOffice = false;
        String sortField ='Name';
        Boolean sortIsAsc = false;
        List<Account> returned = E_AccountDao.getRecsByAgency(id, brunchCode, isAllOffice, sortField, sortIsAsc); //テスト対象メソッド
    	
		System.assertNotEquals(null, returned);
    }

    static testMethod void getRecsIRISAgencySearchBoxTest(){
        //テストデータ作成
        Account a = new Account(Name='テストgetRecsIRISAgencySearchBox', E_CL1PF_ZHEADAY__c='テスト1T');
        INSERT a;
        String keyword = 'テスト';
        String soqlWhere = '';
        List<Account> returned = E_AccountDao.getRecsIRISAgencySearchBox(soqlWhere, keyword);

        System.assertNotEquals(null, returned);
    }
    static testMethod void getRecsByMRIdTest(){
        Account pa = new Account(Name='テストParent', E_CL1PF_ZHEADAY__c='テスト1T');
        INSERT pa;
        Account a = new Account(Name='テストgetRecsByMRId', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
        INSERT a;
        String mrId = a.Owner.Id;
        String brunchCode = a.E_CL2PF_BRANCH__c;
        List<Account> returned = E_AccountDao.getRecsByMRId(mrId, brunchCode);

        System.assertNotEquals(null, returned);
    }
}