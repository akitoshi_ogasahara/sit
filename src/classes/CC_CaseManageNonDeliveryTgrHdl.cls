public with sharing class CC_CaseManageNonDeliveryTgrHdl{

	/**
	 * Trigger Handler実行確認用変数
	 */
	private static Set<String> runTgrSet = new Set<String>();

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){
		if(!runTgrSet.contains('onBeforeInsert')){
			runTgrSet.add('onBeforeInsert');

			//不達郵便物管理
			manageNonDelivery(newList, new Map<Id,Case>(), new Map<Id,Case>(), 'onBeforeInsert');
		}
	}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){
		if(!runTgrSet.contains('onAfterInsert')){
			runTgrSet.add('onAfterInsert');

			//不達郵便物管理
			manageNonDelivery(newList, newMap, new Map<Id,Case>(), 'onAfterInsert');
		}
	}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		if(!runTgrSet.contains('onBeforeUpdate')){
			runTgrSet.add('onBeforeUpdate');

			//不達郵便物管理
			manageNonDelivery(newList, newMap, oldMap, 'onBeforeUpdate');
		}
	}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}


	/**
	 *　不達郵便物管理
	 */
	public static void manageNonDelivery(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap, String triggerStatus){
		//マスタデータ取得
		CC_SRTypeMaster__c sTypeMaster = getSRTypeMasterByName('0124007');  //0124007/配達不能郵便物/管理
		Set<Id> accountIdSet = new Set<Id>();
		Set<Id> contactIdSet = new Set<Id>();
		for(Case caseObj : newList){
			if(caseObj.CMN_ContactName__c != null){
				contactIdSet.add(caseObj.CMN_ContactName__c);
			}
			if(caseObj.CMN_CorporateName__c != null){
				accountIdSet.add(caseObj.CMN_CorporateName__c);
			}
		}
		Map<Id, Contact> contactMap = getContactMapByIdList(contactIdSet);
		for(Contact contact : contactMap.values()){
			if(contact.AccountId != null) accountIdSet.add(contact.AccountId);
		}
		Map<Id, Account> accountMap = getAccountMapByIdList(accountIdSet);
		List<Account> accountList = accountMap.values();


		if(triggerStatus.equals('onBeforeInsert') || triggerStatus.equals('onBeforeUpdate')){
			//通知物種類のリスト値取得
			List<Schema.PicklistEntry> picklistOfDPSItem = Schema.SObjectType.Case.fields.CC_DPSItemID__c.getPicklistValues();

			//データ設定
			for(Case caseObj : newList){
				//Insertのみ場合
				if(triggerStatus.equals('onBeforeInsert')){
					//受付日時
					caseObj.CMN_ReceptionDateTime__c = Datetime.Now();
				}

				//すべて不達SRの情報を設定
				if(caseObj.CC_isNonDeliveryManage__c){
					if(sTypeMaster != null){
						caseObj.CC_SRTypeId__c = sTypeMaster.Id;
						caseObj.CMN_Source__c = '郵便';

						//コメント1：郵送時期+通知物種類+不着事由
						String comment1 = Case.CC_DPSPostingTime__c.getDescribe().getLabel() + '：';
						if(caseObj.CC_DPSPostingTime__c != null) comment1 += caseObj.CC_DPSPostingTime__c.format();
						comment1 += '\n' + Case.CC_DPSItemID__c.getDescribe().getLabel() + '：';
						if(caseObj.CC_DPSItemID__c != null){
							for(Schema.PicklistEntry ple : picklistOfDPSItem){
								if(ple.getValue().equals(caseObj.CC_DPSItemID__c)){
									comment1 += ple.getLabel();
									break;
								}
							}
						}
						comment1 += '\n' + Case.CC_NonDeliveryReason__c.getDescribe().getLabel() + '：';
						comment1 += caseObj.CC_NonDeliveryReason__c == null ? '' : caseObj.CC_NonDeliveryReason__c;
						caseObj.CMN_Comment1__c = comment1;
					}
				}

				//権利者設定
				if(caseObj.CMN_ContactName__c != null && contactMap.get(caseObj.CMN_ContactName__c) != null){
					if(contactMap.get(caseObj.CMN_ContactName__c).E_CLTPF_CLTTYPE__c != null &&
						contactMap.get(caseObj.CMN_ContactName__c).E_CLTPF_CLTTYPE__c.equals('C')){
						for(Account acc : accountList){
							if(contactMap.get(caseObj.CMN_ContactName__c).AccountId == acc.Id){
								caseObj.CMN_ContactName__c = null;
								caseObj.CMN_CorporateName__c = acc.Id;
								break;
							}
						}
					}
				}

			}

			//SRクローズ処理(住所更新確認及び初回不明者判定)
			//newListをフィルターする
			Map<String, Object> returnMap = filterNonDelivery(newList, newMap);
			List<Case> lastDPSPostTimeSRList = (List<Case>)returnMap.get('lastDPSPostTimeSRList');  //トリガ内最新郵送時期持つSRリスト
			List<Case> closeTriCaseList = (List<Case>)returnMap.get('closeTriCaseList');  //トリガ内クローズ対象となるSRリスト
			Map<Id, Case> closeExsitCaseMap = (Map<Id, Case>)returnMap.get('closeExsitCaseMap');  //トリガ外クローズ対象となるSRマップ
			Map<Id, Case> allCloseCaseMap = (Map<Id, Case>)returnMap.get('allCloseCaseMap'); //すべてクローズ対象となるSRマップ

			for(Case caseObj : lastDPSPostTimeSRList){
				if(caseObj.CC_isNonDeliveryManage__c){
					Boolean closeSRFlag = false;
					Contact contact = new Contact();
					Account account = new Account();

					//住所更新確認
					//個人顧客の場合
					if(caseObj.CMN_ContactName__c != null){
						contact = contactMap.get(caseObj.CMN_ContactName__c);
						if(contact.CC_LastUpdateDateForAddress__c != null && contact.CC_LastUpdateDateForAddress__c >= caseObj.CC_DPSPostingTime__c){
							closeSRFlag = true;
						}
					}
					//法人顧客の場合
					else if(caseObj.CMN_CorporateName__c != null){
						account = accountMap.get(caseObj.CMN_CorporateName__c);
						if(account.CC_LastUpdateDateForAddress__c != null && account.CC_LastUpdateDateForAddress__c >= caseObj.CC_DPSPostingTime__c){
							closeSRFlag = true;
						}
					}

					if(closeSRFlag){
						closeTriCaseList.add(caseObj);
						allCloseCaseMap.put(caseObj.Id, caseObj);
					}
				}
			}

			System.debug('lastDPSPostTimeSRList' +lastDPSPostTimeSRList);
			System.debug('closeTriCaseList' +closeTriCaseList.size());
			System.debug('closeExsitCaseMap' +closeExsitCaseMap);

			//クローズ実行
			//クローズ対象となる既存SRに関連する活動を先にクローズする
			if(allCloseCaseMap != null && allCloseCaseMap.size() > 0) deleteSRActivity(allCloseCaseMap.keySet());

			//トリガ内最新郵送時期持つSRをクローズする
			if(closeTriCaseList != null && closeTriCaseList.size() > 0){
				for(Case closeTriCase : closeTriCaseList){
					closeTriCase.Status = 'クローズ';
					closeTriCase.CC_SubStatus__c = '解決済み';
				}
			}

			//それ以外の既存SRをクローズする
			if(closeExsitCaseMap != null && closeExsitCaseMap.size() > 0){
				for(Case closeCase : closeExsitCaseMap.values()){
					closeCase.Status = 'クローズ';
					closeCase.CC_SubStatus__c = '解決済み';
				}
				Update closeExsitCaseMap.values();
			}

		}

		//after Insertの場合
		else if(triggerStatus.equals('onAfterInsert')){
			//権利者の住所不明フラグを更新
			//変更対象の変数
			Map<Id, Account> updAccountMap = new Map<Id, Account>();
			Map<Id, Contact> updContactMap = new Map<Id, Contact>();

			for(Case caseObj : newList){
				if(caseObj.Status.equals('オープン') && caseObj.CC_SRTypeId__c == sTypeMaster.Id && caseObj.CMN_Source__c.equals('郵便')){
					//個人顧客の場合
					if(caseObj.CMN_ContactName__c != null){
						Contact contact = contactMap.get(caseObj.CMN_ContactName__c);
						if(!contact.CMN_isAddressUnknown__c && updContactMap.get(caseObj.CMN_ContactName__c) == null){
							contact.CMN_isAddressUnknown__c = true;
							updContactMap.put(contact.Id, contact);
						}

						if(contact.E_CLTPF_CLTTYPE__c.equals('C')){
							//代理店の住所不明フラグも更新
							if(!contact.Account.CMN_isAddressUnknown__c && updAccountMap.get(contact.AccountId) == null){
								Account account = accountMap.get(contact.AccountId);
								account.CMN_isAddressUnknown__c = true;
								updAccountMap.put(account.Id, account);
							}
						}

					}

					//法人顧客の場合
					else if(caseObj.CMN_CorporateName__c != null){
						Account account = accountMap.get(caseObj.CMN_CorporateName__c);
						if(!account.CMN_isAddressUnknown__c && updAccountMap.get(caseObj.CMN_CorporateName__c) == null){
							account.CMN_isAddressUnknown__c = true;
							updAccountMap.put(account.Id, account);
						}
					}
				}
			}

			//法人に紐づく個人Contactも更新
			List<Contact> contactList = getContactListByIdandAccountIdList(new Set<Id>(), updAccountMap.keySet());
			for(Contact contactObj : contactList){
				contactObj.CMN_isAddressUnknown__c = true;
				updContactMap.put(contactObj.Id, contactObj);
			}

			//更新実行
			if(updContactMap != null){
				Update updContactMap.values();
			}
			if(updAccountMap != null){
				Update updAccountMap.values();
			}
		}

	}

	/**
	 * 不達郵便物管理SRをフィルターする
	 */
	public static Map<String, Object> filterNonDelivery(List<Case> newList, Map<Id,Case> newMap){
		Map<String, Object> returnMap = new Map<String, Object>();
		List<Case> lastDPSPostTimeSRList = new List<Case>();
		List<Case> closeTriCaseList = new List<Case>();
		Map<Id, Case> allCloseCaseMap = new Map<Id, Case>();
		Map<Id, Case> contactKeyMap = new Map<Id, Case>();
		Map<Id, Case> accountKeyMap = new Map<Id, Case>();

		//同じ顧客に紐付く不達SRをフィルターする
		for(Case caseObj : newList){
			if(caseObj.CMN_ContactName__c != null){
				if(contactKeyMap.get(caseObj.CMN_ContactName__c) == null){
					contactKeyMap.put(caseObj.CMN_ContactName__c, caseObj);
				}else{
					if(caseObj.CC_DPSPostingTime__c > contactKeyMap.get(caseObj.CMN_ContactName__c).CC_DPSPostingTime__c){
						//最新郵送時期を持つSRを保持する
						contactKeyMap.put(caseObj.CMN_ContactName__c, caseObj);
					}else{
						//クローズ対象
						closeTriCaseList.add(caseObj);
						allCloseCaseMap.put(caseObj.Id, caseObj);
					}
				}
			}else if(caseObj.CMN_CorporateName__c != null){
				if(accountKeyMap.get(caseObj.CMN_CorporateName__c) == null){
					accountKeyMap.put(caseObj.CMN_CorporateName__c, caseObj);
				}else{
					if(caseObj.CC_DPSPostingTime__c > accountKeyMap.get(caseObj.CMN_CorporateName__c).CC_DPSPostingTime__c){
						//最新郵送時期を持つSRを保持する
						accountKeyMap.put(caseObj.CMN_CorporateName__c, caseObj);
					}else{
						//クローズ対象
						closeTriCaseList.add(caseObj);
						allCloseCaseMap.put(caseObj.Id, caseObj);
					}
				}
			}
		}

		Set<Id> accContactIdSet = new Set<Id>();
		if(!contactKeyMap.isEmpty()){
			accContactIdSet.addAll(contactKeyMap.keySet());
			lastDPSPostTimeSRList.addAll(contactKeyMap.values());
		}
		if(!accountKeyMap.isEmpty()){
			accContactIdSet.addAll(accountKeyMap.keySet());
			lastDPSPostTimeSRList.addAll(accountKeyMap.values());
		}

		//既存オープンの不達SRを取得
		Map<Id, Case> closeExsitCaseMap = getOpenNonDeliveryCaseMap(accContactIdSet, newMap.keySet());
		if(closeExsitCaseMap != null){
			for(Id id : closeExsitCaseMap.keySet()){
				allCloseCaseMap.put(id, closeExsitCaseMap.get(id));
			}
		}

		returnMap.put('closeTriCaseList', (Object)closeTriCaseList);
		returnMap.put('lastDPSPostTimeSRList', (Object)lastDPSPostTimeSRList);
		returnMap.put('closeExsitCaseMap', (Object)closeExsitCaseMap);
		returnMap.put('allCloseCaseMap', (Object)allCloseCaseMap);

		return returnMap;
	}


	/**
	 * クローズSR関連活動を削除
	 */
	public static void deleteSRActivity(Set<Id> caseIdSet){
		List<CC_SRActivity__c> deleteSRActivityList = getOpenSRActListByCaseIdList(caseIdSet);

		if(deleteSRActivityList != null && deleteSRActivityList.size() > 0){
			Delete deleteSRActivityList;
			Database.emptyRecycleBin(deleteSRActivityList);
		}
	}

	/**
	 * Object: CC_SRTypeMaster__c
	 * Parameter: Name (SR作成:SRタイプ番号SR用件)
	 * return: CC_SRTypeMaster__c
	 */
	public static CC_SRTypeMaster__c getSRTypeMasterByName(String Name){
		List<CC_SRTypeMaster__c> srTypeMasterObjList = [
							SELECT Id, Name, CC_IsComplaintSR__c, CC_SRTypeNameandSubject__c
							  FROM CC_SRTypeMaster__c
							 WHERE Name =: Name
								OR CC_SRTypeNameandSubject__c =: Name
							 LIMIT 1];

		return srTypeMasterObjList.isEmpty() ? new CC_SRTypeMaster__c() : srTypeMasterObjList[0];
	}

	/**
	 * Object: Contact
	 * Parameter: なし
	 * return: Map<Id, Contact>
	 */
	public static Map<Id, Contact> getContactMapByIdList(Set<Id> idSet){
		Map<Id, Contact> contactMap = new Map<Id, Contact>([
							SELECT Id, AccountId, E_CL3PF_AGNTNUM__c, E_CLTPF_CLTTYPE__c, CMN_isAddressUnknown__c, CC_LastUpdateDateForAddress__c,
								   Account.CMN_isAddressUnknown__c
							  FROM Contact
							 WHERE Id IN: idSet]);

		return contactMap;
	}

	/**
	 * Object: Account
	 * Parameter: Id set
	 * return: Map<Id, Account>
	 */
	public static Map<Id, Account> getAccountMapByIdList(Set<Id> idSet){
		Map<Id, Account> accountMap = new Map<Id, Account>([
						SELECT Id, Name, E_CL2PF_BRANCH__c, KSECTION__c, E_CL2PF_ZAGCYNUM__c, CMN_ClientNumber__c,
								CMN_isAddressUnknown__c, CC_LastUpdateDateForAddress__c
						  FROM Account
						 WHERE Id in: idSet]);

		return accountMap;
	}

	/**
	 * Object: Contact
	 * Parameter: list of Id and AccountId
	 * return: List<Contact>
	 */
	public static List<Contact> getContactListByIdandAccountIdList(Set<Id> IdSet, Set<Id> accountIdSet){
		List<Contact> contactList = [
							SELECT Id, CMN_IsClaimed__c, AccountId, CMN_isAddressUnknown__c
							  FROM Contact
							 WHERE Id IN: IdSet
								OR (AccountId IN: accountIdSet
							   AND E_CLTPF_CLTTYPE__c = 'C')];

		return contactList;
	}

	/**
	 * Object: Case
	 * Parameter: なし
	 * return: Map<Id, Case>
	 */
	public static Map<Id, Case> getOpenNonDeliveryCaseMap(Set<Id> accContactIdSet, Set<Id> newCaseIdSet){
		Map<Id, Case> caseMap = new Map<Id, Case>([
						SELECT Id, CMN_ContactName__c, CMN_CorporateName__c, CC_SRTypeId__c, CC_SRTypeId__r.Name, CMN_ReceptionDateTime__c,
							   CC_isNonDeliveryManage__c, Status, CMN_Source__c, CC_DPSPostingTime__c
						  FROM Case
						 WHERE Status = 'オープン'
						   AND CC_SRTypeId__r.Name = '0124007'
						   AND CMN_Source__c = '郵便'
						   AND Id NOT IN : newCaseIdSet
						   AND (CMN_ContactName__c IN : accContactIdSet
						    OR CMN_CorporateName__c IN : accContactIdSet)
						   AND RecordType.Name = 'サービスリクエスト']);

		return caseMap;
	}

	/**
	 * Object: CC_SRActivity__c
	 * Parameter: Case Id set
	 * return: List<CC_SRActivity__c>（ステータス:終了＆作業不要以外）
	 */
	public static List<CC_SRActivity__c> getOpenSRActListByCaseIdList(Set<Id> caseIdSet){
		List<CC_SRActivity__c> srActivityList = [
						SELECT Id, CC_SRNo__c, CC_Status__c, OwnerId, Owner.Name, CC_Worker__c
						  FROM CC_SRActivity__c
						 WHERE CC_SRNo__c IN: caseIdSet
						   AND CC_Status__c NOT IN ('終了', '作業不要')
					  ORDER BY CC_SRNo__c];

		return srActivityList;
	}

}