public with sharing class I_ChangePWController extends E_AbstractController{
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get; set;}

	//タイトル
	public String title{get;set;}

	//新規基本サービス用パスワード
	public String newPassword{get;set;}
	//新規基本サービス用パスワード(確認)
	public String newConfirmPassword{get;set;}

	//true:完了画面
	public Boolean compFlag {get; set;}
	//ページリダイレクト用
	public String prm {get; set;}
	//初回ログインか
	public Boolean firstLoginFlag {get; set;}
	//パスワード再発行か
	//public Boolean reissuePassFlag {get; set;}


	public I_ChangePWController() {
		pageMessages = getPageMessages();
		title = 'パスワード変更';

		//パラメータで変更画面と完了画面を切り替え
		prm = ApexPages.currentPage().getParameters().get('after');
		//パラメータが1のときは完了画面
		if(String.isNotBlank(prm)&&prm=='1'){
			compFlag = true;
		}else{
			compFlag = false;
		}

		//初回ログインフラグを立てる
		firstLoginFlag = access.user.E_UseTempBasicPW__c;

		//reissuePassFlag = ;
	}

	//ページアクション
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}

	//IRISブラウザ判定
	public Boolean getIsIrisPage(){
		return iris.getIsIRIS();
	}

	//戻るボタン押下
	public override PageReference doReturn(){
		return Page.IRIS_UserConfig;
	}

	//実行
	public Pagereference doSave(){
		pageMessages.clearMessages();
		try{
			//入力チェック
			if(!validatePassword()){
				return null;
			}
			//ユーザレコードを取得
			User userRec = access.user;

			//パスワード変更
			System.setPassword(UserInfo.getUserId(), newPassword);
			
			if(ApexPages.hasMessages()){
				//ChangeMessasgeの失敗はApexPagesのメッセージに出力されている。
				//パスワード登録に失敗した場合は、処理を中断する。
				//テストメソッドはここを通ることはないので、通過していなくても良しとする
				ApexPages.Message errMsg = ApexPages.getMessages().get(0);
				for(string s:E_Const.SFDC_EXCEPTION_MSG_HACK.keyset()){
					if(errMsg.getDetail().contains(s)){
						addExceptionToPage(E_Const.SFDC_EXCEPTION_MSG_HACK.get(s));
						return null;
					}
				}
				//エラーメッセージをNNLinkコンポーネントに移します。
				addExceptionToPage(ApexPages.getMessages());
				System.debug('ApexPages.getMessages()=============' +ApexPages.getMessages());
				return null;
			}

			//暗号化されたパスワードを変更した場合、フラグを降ろす
			if(userRec.E_UseTempBasicPW__c){
				userRec.E_UseTempBasicPW__c = false;
				update userRec;
			}else if(userRec.E_UseExistingBasicPW__c){
				userRec.E_UseExistingBasicPW__c = false;
				update userRec;
			}

			//リダイレクト先(完了画面)へパラメータをつけて渡す
			PageReference ref = Page.IRIS_ChangePW;
			ref.getParameters().put('after','1');
			ref.setRedirect(true);
			return ref;

		}catch(DMLException ex) {
			System.debug('ex=============' +ex);
			pageMessages.addErrorMessage(ex.getMessage());
			return null;
		}catch(Exception e){
			System.debug('e=============' +e);
			string errMsg = e.getMessage();

			for(string s:E_Const.SFDC_EXCEPTION_MSG_HACK.keyset()){
				if(errMsg.contains(s)){
					addExceptionToPage(E_Const.SFDC_EXCEPTION_MSG_HACK.get(s));
					return null;
				}
			}

			addExceptionToPage(e.getMessage());
			return null;
		}
	}

	//入力チェック
	private boolean validatePassword(){
		boolean isOK = false;
		//新しいパスワードが入力されていない場合
		if(String.isblank(newPassword)){
			//新規パスワードを入力して下さい。
			addExceptionToPage(getMSG().get('SPW|101'));
		}
		//新しいパスワード（確認）が入力されていない場合
		if(String.isblank(newConfirmPassword)){
			//新規パスワード（確認）を入力して下さい。
			addExceptionToPage(getMSG().get('SPW|102'));
		}

		if(!pageMessages.hasMessages()){
			// パスワードに使用できる文字(半角英小文字及び数字のみ)以外の入力がある
			if(!E_Util.isEnAlphaNum(newPassword)){
				//半角アルファベットの大文字、小文字、数字の3種類すべてを組み合わせて設定してください。
				addExceptionToPage(getMSG().get('SPW|010'));
			//パスワードポリシー(アルファベットの大文字、小文字、数字の３種類すべてを組み合わせて、8文字以上のパスワードが設定されているか)チェック
			}else if(!E_util.passwordPolicyCheck(newPassword)){
				addExceptionToPage(getMSG().get('SPW|010'));
			}
			//パスワードが8文字以下の場合
			if(newPassword.length()<8){
				addExceptionToPage(getMSG().get('SPW|110'));
			}

			//新しいパスワードと新しいパスワード（確認）が違っている場合
			if(!newPassword.equals(newConfirmPassword)){
				//新パスワードと新パスワード(確認）が相違しています。
				addExceptionToPage(getMSG().get('SPW|109'));
			}

		}
		if(!pageMessages.hasMessages()) isOk = true;
		return isOK;
	}

	//エラーメッセージ追加
	private void addExceptionToPage(String str){
		pageMessages.addErrorMessage(str);
	}
	private void addExceptionToPage(ApexPages.Message[] pgMsgs){
		pageMessages.addMessages(pgMsgs);
	}

	//ホーム画面へ
	public Pagereference goHome(){
		return Page.E_Home;
	}
}