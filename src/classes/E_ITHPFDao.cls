public with sharing class E_ITHPFDao {

	public static E_ITHPF__c getRecById (Id investmentId) {
		E_ITHPF__c rec = null;
		List<E_ITHPF__c> recs = [
			Select 
				id, Name, ContractorName__c, ZCLNTCDE__c From E_ITHPF__c
			Where 
				id =: investmentId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}
	
}