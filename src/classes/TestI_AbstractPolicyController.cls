@isTest
private class TestI_AbstractPolicyController {

	//---フィールドの setter, getter テスト---
	static testMethod void coverFieldsSetterGetter(){
		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();

		target.rowCount = 1;
		System.assertEquals(1, target.rowCount);
		target.sortType = 'asc';
		System.assertEquals('asc', target.sortType);

		target.sortIsAsc = true;
		System.assert(target.sortIsAsc);

		target.contactFrag = true;
		System.assert(target.contactFrag);
		target.agencyFlag = true;
		System.assert(target.agencyFlag);
		target.officeFlag = true;
		System.assert(target.officeFlag);
		target.manegeMRFlag = true;
		System.assert(target.manegeMRFlag);
	}

	//---getListMaxRowsメソッドテスト---
	static testMethod void getListMaxRowsTest(){
		I_OwnerController target = new I_OwnerController(); //テスト対象インスタンス
		Test.startTest();
		System.assertEquals(1000, target.getListMaxRows());
		Test.stopTest();
	}



	//---createPolicyConditionメソッド---
	// 代理店フィルタ
	static testMethod void createPolicyConditionTest_passIfClause(){
		String conditionValue = 'agency';
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		E_IRISHandler iris = E_IRISHandler.getInstance();
		iris.setPolicyCondition('agency', '代理店名', 'agency', 'breadCrumb');

		System.runAs(actUser){
			I_OwnerController target = new I_OwnerController();	//テスト対象インスタンス
			Test.startTest();
				String returned = target.createPolicyCondition();
			Test.stopTest();

			String expected =  ' AND ( MainAgentAccountParentId__c = \'' + conditionValue + '\' '
							   + ' OR SubAgentAccountParent__c = \''  + conditionValue + '\' ) ';
			System.assertEquals(expected, returned);
		}
	}
	// 事務所フィルタ
	static testMethod void createPolicyConditionTest_pass_fisrtElseIfClause(){
		String conditionValue =  'office';
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		E_IRISHandler iris = E_IRISHandler.getInstance();
		iris.setPolicyCondition('office', '事務所名', 'office', 'breadCrumb');

		System.runAs(actUser){
			I_OwnerController target = new I_OwnerController();	//テスト対象インスタンス
			Test.startTest();
				String returned = target.createPolicyCondition();
			Test.stopTest();

			String expected =  ' AND ( MainAgentAccount__c = \'' + conditionValue + '\' '
						   + ' OR SubAgentAccount__c = \''  + conditionValue + '\' ) ';
			System.assertEquals(expected, returned);
		}
	}
	// 募集人フィルタ
	static testMethod void createPolicyConditionTest_pass_secondElseIfClause(){
		//String conditionValue = 'agent';
		//代理店作成
		Account acc = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,acc);
		//募集人作成
		Contact con = TestI_TestUtil.createContact(false,office.Id,'test');
		//募集人コード
		con.E_CL3PF_AGNTNUM__c = '12345';
		insert con;


		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			I_OwnerController target = new I_OwnerController();	//テスト対象インスタンス

			Test.startTest();
				E_IRISHandler iris = E_IRISHandler.getInstance();
				iris.setPolicyCondition('agent', '募集人名', con.Id, 'breadCrumb');
				//iris.setPolicyCondition('agent', '募集人名', 'agent', 'breadCrumb');

				String returned = target.createPolicyCondition();
			Test.stopTest();

			String expected =  ' AND ( MainAgent__c = \'' + con.Id + '\' '
							   + ' OR SubAgent__c = \''  + con.Id + '\' ) ';
			//String expected =  ' AND ( MainAgent__c = \'' + conditionValue + '\' '
			//				   + ' OR SubAgent__c = \''  + conditionValue + '\' ) ';
			System.assertEquals(expected, returned);
		}
	}
	// 営業部フィルタ
	static testMethod void createPolicyConditionTest_pass_thirdElseIfClause(){
		String conditionValue = 'unit';
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			I_OwnerController target = new I_OwnerController();	//テスト対象インスタンス

			Test.startTest();
				E_IRISHandler iris = E_IRISHandler.getInstance();
				iris.setPolicyCondition('unit', '営業部名', 'unit', 'breadCrumb');

				String returned = target.createPolicyCondition();
			Test.stopTest();

			String expected =  ' AND ( MainAgentAccount__r.E_CL2PF_BRANCH__c = \''+conditionValue+'\''
							  + 'OR MainAgentAccount__r.E_CL2PF_ZBUSBR__c = \''+conditionValue+'\''
							  + 'OR SubAgentAccount__r.E_CL2PF_BRANCH__c = \''+conditionValue+'\''
							  + 'OR SubAgentAccount__r.E_CL2PF_ZBUSBR__c = \''+conditionValue+'\' ) ';
			System.assertEquals(expected, returned);
		}
	}
	// MRフィルタ
	static testMethod void createPolicyConditionTest_pass_fourthElseIfClause(){
		String conditionValue = 'mr';
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			I_OwnerController target = new I_OwnerController();	//テスト対象インスタンス

			Test.startTest();
				E_IRISHandler iris = E_IRISHandler.getInstance();
				iris.setPolicyCondition('mr', 'MR名', 'mr', 'breadCrumb');

				String returned = target.createPolicyCondition();
			Test.stopTest();

			String expected =  ' AND ( MainAgentAccount__r.OwnerId = \''+conditionValue+'\' '
							  +' OR SubAgentAccount__r.OwnerId = \''+conditionValue+'\' )';
			System.assertEquals(expected, returned);
		}
	}

	// フィルタ無しの場合  MR または 拠点長 または 代理店ユーザの場合
	static testMethod void createPolicyConditionTest_passElseClause(){
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			I_OwnerController target = new I_OwnerController();	//テスト対象インスタンス
			Test.startTest();
			String returned = target.createPolicyCondition();
			Test.stopTest();
			String expected = ' AND PolicyInCharge__c = True ';
			System.assertEquals(expected, returned);
		}
	}

	//---crateCustomerInfoParameterメソッド---
   //代理店
	static testMethod void 	crateCustomerInfoParameterTest_agency(){
		String conditionValue = 'agency';
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		String returned;
		System.runAs(actUser){
			E_AccessController access = E_AccessController.getInstance();
			E_IRISHandler.getInstance().setPolicyCondition('agency', '代理店名', 'agency', 'breadCrumb');

		  Test.startTest();
			I_OwnerController target = new I_OwnerController();	 //テスト対象インスタンス
			returned = target.crateCustomerInfoParameter();
		  Test.stopTest();
		}
		String expected = '&disid=' + conditionValue + '&offId=&ageId=';
		System.assertEquals(expected, returned);
	}
	//事務所
	static testMethod void crateCustomerInfoParameterTest_office(){
		String conditionValue = 'office';
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		String returned;
		System.runAs(actUser){
			E_AccessController access = E_AccessController.getInstance();
			E_IRISHandler.getInstance().setPolicyCondition('office', '代理店名', 'office', 'breadCrumb');

		  Test.startTest();
			I_OwnerController target = new I_OwnerController();	 //テスト対象インスタンス
			returned = target.crateCustomerInfoParameter();
		  Test.stopTest();
		}
		String expected = '&disid=&offId=' + conditionValue + '&ageId=';
		System.assertEquals(expected, returned);
	}
	//募集人
	static testMethod void crateCustomerInfoParameterTest_agent(){
		//String conditionValue = 'agent';

		//代理店作成
		Account acc = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,acc);
		//募集人作成
		Contact con = TestI_TestUtil.createContact(false,office.Id,'test');
		//募集人コード
		con.E_CL3PF_AGNTNUM__c = '12345';
		insert con;

		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		String returned;
		System.runAs(actUser){
			E_AccessController access = E_AccessController.getInstance();
			E_IRISHandler.getInstance().setPolicyCondition('agent', '代理店名', con.Id, 'breadCrumb');

		  Test.startTest();
			I_OwnerController target = new I_OwnerController();	 //テスト対象インスタンス
			returned = target.crateCustomerInfoParameter();
		  Test.stopTest();
		}
		String expected = '&disid=&offId=&ageId=' + con.Id;
		//String expected = '&disid=&offId=&ageId=' + conditionValue;
		System.assertEquals(expected, returned);
	}
	//フィルタなし 募集人
	 static testMethod void crateCustomerInfoParameterTest_withoutFilter_agent(){
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		String returned;
		System.runAs(actUser){
			E_AccessController access = E_AccessController.getInstance();

		  Test.startTest();
			I_OwnerController target = new I_OwnerController();	 //テスト対象インスタンス
			returned = target.crateCustomerInfoParameter();
		  Test.stopTest();
		}
		String expected = '&disid=&offId=&ageId=';// + access.user.ContactId;
		System.assertEquals(expected, returned);
	}
	//フィルタなし その他
	static testMethod void crateCustomerInfoParameterTest_withoutFilter_other1(){
		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		String returned;
		System.runAs(actUser){
			E_AccessController access = E_AccessController.getInstance();

		  Test.startTest();
			I_OwnerController target = new I_OwnerController();	 //テスト対象インスタンス
			returned = target.crateCustomerInfoParameter();
		  Test.stopTest();
		}
		String expected = '&disid=&offId=&ageId=';
		System.assertEquals(expected, returned);
	}
	//フィルタなし その他
	static testMethod void crateCustomerInfoParameterTest_withoutFilter_other2(){
		//実行ユーザー作成
		//User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		Account acc = TestCR_TestUtil.createAccount(true);
    	Contact con = TestCR_TestUtil.createContact(true, acc.Id, 'TestUser002');

    	// 代理店ユーザ作成
		User actUser = TestCR_TestUtil.createAgentUser(true, 'TestUser002', 'E_PartnerCommunity', con.Id);
		TestCR_TestUtil.createContactShare(con.id, actUser.id);
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		String returned;
		System.runAs(actUser){
			E_AccessController access = E_AccessController.getInstance();

		  Test.startTest();
			I_OwnerController target = new I_OwnerController();	 //テスト対象インスタンス
			returned = target.crateCustomerInfoParameter();
		  Test.stopTest();
		}
		String expected = '&disid=&offId=&ageId='+con.Id;
		System.assertEquals(expected, returned);
	}


	//---getIRISMenuParameterメソッド---
	static testMethod void getIRISMenuParameterTest(){
		I_OwnerController target = new I_OwnerController(); //テスト対象インスタンス
		String retStr = target.getIRISMenuParameter();

		Test.startTest();
		System.assertEquals('irismn=' + ApexPages.currentPage().getParameters().get('irismn'), retStr);
		Test.stopTest();
	}

	//---getCancellationRefundメソッド---
	//if文通過
	static testMethod void  getCancellationRefundTest_passIfClause(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 0;
		pol.COLI_ZEAPLTOT__c = 0;
		pol.COLI_ZEPLTOT__c = 0;
		pol.COLI_ZEADVPRM__c = 0;
		pol.COLI_ZUNPREM__c = 0;
		pol.COLI_ZCVDCF__c = true;
		pol.COLI_ZUNPCF__c = true;

		I_OwnerController target = new I_OwnerController(); //テスト対象インスタンス
		Test.startTest();
		System.assertEquals(-1, target.getCancellationRefund(pol));
		Test.stopTest();
	}
	//else if文通過
	static testMethod void getCancellationRefundTest_passElseIfClause(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 500;
		pol.COLI_ZEAPLTOT__c = 50;
		pol.COLI_ZEPLTOT__c = 70;
		pol.COLI_ZEADVPRM__c = 0;
		pol.COLI_ZUNPREM__c = 0;
		//if文に入らないために falseを設定
		pol.COLI_ZCVDCF__c = false;
		pol.COLI_ZUNPCF__c = false;
		//else if文に入るために falseを設定
		//pol.COLI_ZUNPCF__c と pol.COLI_ZUNPDCF__c はデフォルトがfalse

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();

		Decimal actual = target.getCancellationRefund(pol);
		Decimal expected = 380;
		Test.startTest();
		System.assertEquals(expected, actual);
		Test.stopTest();
	}
	//else文通過
	static testMethod void getCancellationRefundTest_passElseClause(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 300;
		pol.COLI_ZEAPLTOT__c = 100;
		pol.COLI_ZEPLTOT__c = 200;
		pol.COLI_ZEADVPRM__c = 150;
		pol.COLI_ZUNPREM__c = 250;
		//if文に入らないために falseを設定
		pol.COLI_ZCVDCF__c = false;
		pol.COLI_ZUNPCF__c = false;
		//else文に入るために trueを設定
		pol.COLI_ZUNPDCF__c = true;

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();

		Decimal actual= target.getCancellationRefund(pol);
		Decimal expected = 400;
		Test.startTest();
		System.assertEquals(expected, actual);
		Test.stopTest();
	}

	static testMethod void getCreateCovpfCondition01(){
		Account acc = new Account(Name = 'test');
		insert acc;

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();
		target.iris.setPolicyCondition('agency', acc.Name, acc.Id, acc.Name);
		String actual = target.createCovpfCondition();
	}

	static testMethod void getCreateCovpfCondition02(){
		Account acc = new Account(Name = 'test');
		insert acc;

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();
		target.iris.setPolicyCondition('office', acc.Name, acc.Id, acc.Name);
		String actual = target.createCovpfCondition();
	}

	static testMethod void getCreateCovpfCondition03(){
		Account acc = new Account(Name = 'test');
		insert acc;

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();
		target.iris.setPolicyCondition('agent', acc.Name, acc.Id, acc.Name);
		String actual = target.createCovpfCondition();
	}

	static testMethod void getCreateCovpfCondition04(){
		Account acc = new Account(Name = 'test');
		insert acc;

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();
		target.iris.setPolicyCondition('unit', acc.Name, acc.Id, acc.Name);
		String actual = target.createCovpfCondition();
	}

	static testMethod void getCreateCovpfCondition05(){
		Account acc = new Account(Name = 'test');
		insert acc;

		//テスト対象インスタンス生成
		I_OwnerController target = new I_OwnerController();
		target.iris.setPolicyCondition('mr', acc.Name, acc.Id, acc.Name);
		String actual = target.createCovpfCondition();
	}

	static testMethod void getCreateCovpfCondition06(){
		Account acc = new Account(Name = 'test');
		insert acc;

		//実行ユーザー作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			//テスト対象インスタンス生成
			I_OwnerController target = new I_OwnerController();
			target.iris.setPolicyCondition('', acc.Name, acc.Id, acc.Name);
			String actual = target.createCovpfCondition();
		}
	}

	static testMethod void test_createCovpfConditionByManager(){
		//実行ユーザー作成
		User actUser = TestE_TestUtil.createUser(true, 'testName001', '拠点長');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){

			//テスト対象インスタンス生成
			I_LapsedPolicyController target = new I_LapsedPolicyController();
			target.pageAction();

			Test.startTest();
				String result = target.createCovpfConditionByManager();
				String expected = ' AND E_Policy__r.MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\''
								+ ' AND E_Policy__r.MainAgent__r.Account.E_CL2PF_BRANCH__c = \'90\'';
				System.assertEquals(expected, result);

				target.searchType = I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE;
				expected = ' AND (E_Policy__r.MainAgent__r.Account.KSECTION__c = \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' '
								+ ' OR (E_Policy__r.MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' '
								+ ' AND E_Policy__r.MainAgent__r.Account.E_CL2PF_BRANCH__c != \'90\')) ';
				 result = target.createCovpfConditionByManager();
				System.assertEquals(expected, result);
			Test.stopTest();

		}
	}

	static testMethod void test_createPolicyConditionByManager(){
		//実行ユーザー作成
		User actUser = TestE_TestUtil.createUser(true, 'testName001', '拠点長');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){

			//テスト対象インスタンス生成
			I_OwnerController target = new I_OwnerController();
			target.pageAction();

			Test.startTest();
				String result = target.createPolicyConditionByManager();
				String expected = ' AND MainAgent__r.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\''
								+ ' AND MainAgent__r.Account.E_CL2PF_BRANCH__c = \'90\'';
				System.assertEquals(expected, result);

				target.searchType = I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE;
				expected = ' AND (MainAgent__r.Account.KSECTION__c = \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' '
						 + ' OR (MainAgent__r.Account.KSECTION__c !=  \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' '
						 + ' AND MainAgent__r.Account.E_CL2PF_BRANCH__c != \'90\'))';
				 result = target.createPolicyConditionByManager();
				System.assertEquals(expected, result);
			Test.stopTest();

		}
	}

}