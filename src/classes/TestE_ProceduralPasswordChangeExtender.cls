/**
* 
*/
@isTest
private class TestE_ProceduralPasswordChangeExtender {
    
    // test data
    private static User user;
    private static E_IDCPF__c idcpf;
    private static List<E_MessageMaster__c> msgList;
    
    /*
* init()
*/
    static testMethod void testInit01() {
        // createData
        createDataCommon();
        
        Test.startTest();
        system.runAs(user){
            
            E_ProceduralPasswordChangeController controller = new E_ProceduralPasswordChangeController(new ApexPages.StandardController(idcpf));
            E_ProceduralPasswordChangeExtender extender = new E_ProceduralPasswordChangeExtender(controller);
            
            extender.init();
            
            system.assertEquals(controller.record.Id, idcpf.Id);
            system.assert(!extender.isInit);
        }
        Test.stopTest();
    }
    
    /*
* pageAction()
*/
    static testMethod void testPageAction01() {
        // createData
        createDataCommon();
        
        Test.startTest();
        system.runAs(user){
            
            E_ProceduralPasswordChangeController controller = new E_ProceduralPasswordChangeController(new ApexPages.StandardController(idcpf));
            E_ProceduralPasswordChangeExtender extender = new E_ProceduralPasswordChangeExtender(controller);
            
            extender.init();
            Pagereference pref = extender.pageAction();
            
            system.assertEquals(pref, null);
        }
        Test.stopTest();
    }
    
    /*
* doExecute()
* 仮パスワード以外
*/
    static testMethod void testDoExecute01() {
        // createData
        createDataCommon();
        
        Test.startTest();
        system.runAs(user){
            
            E_ProceduralPasswordChangeController controller = new E_ProceduralPasswordChangeController(new ApexPages.StandardController(idcpf));
            E_ProceduralPasswordChangeExtender extender = new E_ProceduralPasswordChangeExtender(controller);
            
            extender.init();
            
            extender.oldPassword = 'terrask1';
            extender.newPassword = 'terrask2';
            extender.verPassword = 'terrask2';
            Pagereference resultPref = extender.doExecute();
            // エラーが発生していないこと
            System.assert(!extender.pageMessages.getHasErrorMessages());
            Pagereference pref = Page.E_AcceptanceCompletion;
            pref.getParameters().put('type', 'ACC|005');
            system.assertNotEquals(resultPref, pref);
            
            E_IDCPF__c resultIdcpf = [Select RegularProcedurePW__c From E_IDCPF__c Where Id = :controller.record.Id Limit 1];
            system.assertEquals(resultIdcpf.RegularProcedurePW__c, E_EncryptUtil.encrypt('terrask2'));
            
            E_ADRPF__c adrpf = [Select E_IDCPF__c From E_ADRPF__c Limit 1];
            system.assertEquals(adrpf.E_IDCPF__c, idcpf.Id);
        }
        Test.stopTest();
    }
    
    /*
* doExecute()
* 仮パスワード
*/
    static testMethod void testDoExecute02() {
        // createData
        createDataCommon();
        
        Test.startTest();
        system.runAs(user){
            idcpf.UseTempProcedurePW__c = true;
            idcpf.ZPASSWD02__c = E_EncryptUtil.encryptInitPass('1234567801', E_Util.getSysDate(null));
            //idcpf.RegularProcedurePW__c = E_EncryptUtil.encryptInitPass('1234567801', E_Util.getSysDate(null)); // terrask1
            update idcpf;
            
            E_ProceduralPasswordChangeController controller = new E_ProceduralPasswordChangeController(new ApexPages.StandardController(idcpf));
            E_ProceduralPasswordChangeExtender extender = new E_ProceduralPasswordChangeExtender(controller);
            
            extender.init();
            
            extender.oldPassword = '1234567801';
            extender.newPassword = 'terrask2';
            extender.verPassword = 'terrask2';
            Pagereference resultPref = extender.doExecute();
            
            Pagereference pref = Page.E_AcceptanceCompletion;
            pref.getParameters().put('type', 'ACC|005');
            system.assertNotEquals(resultPref, pref);

            // エラーが発生していないこと
            //System.debug('@error:'+ extender.pageMessages.getErrorMessages().get(0).summary);
            //System.assert(!extender.pageMessages.getHasErrorMessages());
            E_IDCPF__c resultIdcpf = [Select RegularProcedurePW__c From E_IDCPF__c Where Id = :controller.record.Id Limit 1];
            system.assertEquals(resultIdcpf.RegularProcedurePW__c, E_EncryptUtil.encrypt('terrask1'));
            
            //List<E_ADRPF__c> adrpfList = [Select E_IDCPF__c From E_ADRPF__c];
            //system.assert(!adrpfList.isEmpty());
        }
        Test.stopTest();
    }
    
    /*
* doExecute()
* 入力エラー
*/
    static testMethod void testDoExecute03() {
        // createData
        createDataCommon();
        
        Test.startTest();
        system.runAs(user){
            E_ProceduralPasswordChangeController controller = new E_ProceduralPasswordChangeController(new ApexPages.StandardController(idcpf));
            E_ProceduralPasswordChangeExtender extender = new E_ProceduralPasswordChangeExtender(controller);
            
            extender.init();
            
            extender.oldPassword = 'terrask1';
            extender.newPassword = 'terrask2';
            extender.verPassword = 'terrask3';
            Pagereference resultPref = extender.doExecute();
            
            system.assertEquals(resultPref, null);
        }
        Test.stopTest();
    }
    
    /*
* doExecute()
* エラーページ遷移
*/
    static testMethod void testDoExecute04() {
        // createData
        createDataCommon();
        Test.startTest();
        system.runAs(user){
            E_Email__c eMail = [select id,name from E_Email__c where name = :E_Const.EMAIL_CHARGE_CHANGECONTACT][0];
            email.Name = 'test';
            update eMail;
            
            E_ProceduralPasswordChangeController controller = new E_ProceduralPasswordChangeController(new ApexPages.StandardController(idcpf));
            E_ProceduralPasswordChangeExtender extender = new E_ProceduralPasswordChangeExtender(controller);
            
            extender.init();
            
            extender.oldPassword = 'terrask1';
            extender.newPassword = 'terrask2';
            extender.verPassword = 'terrask2';
            Pagereference resultPref = extender.doExecute();
            
            system.assertEquals(resultPref, null);
        }
        Test.stopTest();
    }
    
    /* test data */
    static void createDataCommon(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            // User
            Profile profile = [Select Id, Name From Profile Where Name = 'システム管理者' Limit 1];
            user = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
            );
            insert user;
            
            // ID管理
            idcpf = new E_IDCPF__c(
                TRCDE01__c = 'TDB0',
                User__c = user.Id,
                ZSTATUS02__c = '1',
                ZPASSWD02__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b', // terrask1
                RegularProcedurePW__c = 'eaaddc40d5d09f656e62370d8047fe8d15fab85eed6845405736ae453b5781048d19d7cd121a48b66f113e1ea03d18df3dd32c5efde343afd8cff7198020f04b', // terrask1
                ZPASSWD02DATE__c = '20150101'
            );
            insert idcpf;
            
            // E_MessageMaster__c
            RecordType rtype = [Select Id From RecordType Where DeveloperName = 'MSG_DISCLAIMER' Limit 1];
            msgList = new List<E_MessageMaster__c>();
            for(Integer i = 1; i < 8; i++){
                // メッセージ
                msgList.add(new E_MessageMaster__c(
                    Name = 'Err' + String.valueOf(i),
                    RecordTypeId = rtype.Id,
                    Type__c = 'メッセージ',
                    Key__c = 'ACE|00' + String.valueOf(i),
                    Value__c = 'ACE|00' + String.valueOf(i)
                ));
            }
            insert msgList;
            E_Email__c eMail = new E_Email__c(NAME = 'E_101',Email__c = 'xxxx@xx.xx');
            insert eMail;            
        }
        insertE_bizDataSyncLog();
    }
    
    private static void insertE_bizDataSyncLog(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
            insert log;
        }    
    }
    
}