/**
 * ユニットプライス/騰落率表示、ユニットプライスグラフ表示、ユニットプライス表示、騰落率表示 共通コントローラ
 */
public with sharing class E_UPTerm003Controller extends E_AbstractUnitPriceController {
    /** ユニットプライスタイトルMap */
	public static Map<String,String> UNITPRICE_PAGENAMES = new Map<String,String>{
																			 '003' => 'ユニットプライス/騰落率表示'
																			,'004' => 'ユニットプライスグラフ表示'
																			,'005' => 'ユニットプライス表示'
																			,'006' => '騰落率表示'
																			,'CSV' => 'ユニットプライスダウンロード' 
																	};
	public Map<String,String> getUNITPRICE_PAGENAMES(){
		return UNITPRICE_PAGENAMES;
	}

    /** ユニットプライス基本情報ID */
    private String uid {get; set;}
    /** 特別勘定ID */
    private String spid {get; set;}
    /** ユニットプライス基本情報 */
    public E_UPBPF__c eupbpf {get; set;}
    /** 選択可能ファンド */
    private List<E_SFGPF__c> sList {get; set;}
    /** 特別勘定チェックボックス */
    public List<EUPFPFSelectOption> fundSelectOptions{get;set;}

    /** 自由選択ラジオボタン設定値 */
    private final String RADIO_VAL_FREE = '0';
    public String getRADIO_VAL_FREE(){
        return RADIO_VAL_FREE;
    } 
    /** 簡易選択ラジオボタン設定値 */
    private final String RADIO_VAL_EASY = '1';
    public String getRADIO_VAL_EASY(){
        return RADIO_VAL_EASY;
    }
	/** 期間選択方式　変更時 */
	public PageReference changeSelectType(){
		return null;
	}
	/** 期間選択方式　選択値 */
	public String selectType{get{
									if(selectType==null){
										selectType = RADIO_VAL_FREE;
									}
									return selectType;
							}
							set;}

    /** DatePicker開始年度 */
    public String datePickerStartYear {get; set;}
    /** DatePicker終了年度 */
    public String datePickerEndYear {get; set;}
    /** バリアブルアニュイティの判定 */
    public boolean isVariableAnnuity {get; set;}
    /** コンポジット判定 */
    public Boolean isCompositeFund {get; set;}
    
    //ページ遷移系　Properties
    /** 各リンクの表示判定 */
    public Boolean showPageLink{get;set;}  

    //####################### 呼出し条件 #######################
    /** PDF,CSVダウンロード時のOPROサービス呼出し時パラメータ管理クラス */
    public E_DownloadCondition.UnitPriceDownloadCondi dlCondi{get; private set;}
    /** 自由選択開始日(yyyy/MM/dd) */
    public String freeSelectStartDate {get; set;}
    private String originFreeSelectStartDate;
    /** 自由選択終了日(yyyy/MM/dd) */
    public String freeSelectEndDate {get; set;}
    private String originFreeSelectEndDate;
    /** 期間簡易選択終了日(yyyy/MM/dd) */
    public String easySelectEndDate {get; set;}
    private String originEasySelectEndDate;
    /** 簡易選択 選択期間 */
    public String easySelectTerm {get; set;}

    //####################### 実装 #######################    
    /**
     *      Constructor
     */
    public E_UPTerm003Controller() {
        super();
        showPageLink = false;
        this.pgTitle = UNITPRICE_PAGENAMES.get('003');		//ユニットプライス/騰落率
    }
    /* init */
    public Pagereference init(){
    	
    	/*
		//service hour menu
		PageReference pageRef = isServiceHourMenu();
		if (pageRef != null) {
			return pageRef;
		}
		*/
    	
        // ユニットプライス基本情報IDの取得
        this.uid = ApexPages.currentPage().getParameters().get('id');
        // 特別勘定IDの取得
        this.spid = ApexPages.currentPage().getParameters().get('spid');

        // ユニットプライス基本情報の取得
        if(String.isNotBlank(this.uid)){
            this.eupbpf = E_UPBPFDao.getRecById(this.uid);
              
        }else{
            // URLパラメータが不正な場合
            return E_Util.toUpErrorPage('ERR|002');
        }

        if(this.eupbpf == null){
            // パラメーター[id]で【ユニットプライス基本情報】が取得できない場合
            return E_Util.toUpErrorPage('UP2|006');
        }
        // バリアブルアニュイティの判定
        this.isVariableAnnuity = isValiableAnnuity(this.eupbpf.ZPRDCD__c);

        // 選択可能ファンド一覧の取得
        if(String.isNotBlank(this.spid)){
            // ターゲット型（特別勘定IDが指定されている）の場合
            this.sList = E_SFGPFDao.getRecByFundMasterId(this.eupbpf.E_FundMaster__c,this.spid);
        }else{
            // たしかなおくりものの場合
            this.sList = E_SFGPFDao.getRecByFundMasterId(this.eupbpf.E_FundMaster__c);
        }

        // 【ユニットプライス基本情報】から特別勘定が1件も紐づかない場合エラー
        if(this.sList == null || this.sList.size() < 1){
            return E_Util.toUpErrorPage('UPS|013');
        }

        // 初期値の設定
        setDefaultValue();
        
        // コンポジット判定
        isCompositeFund = (eupbpf.NameClass__c == '0' && isCompositeFund == false) ? false : true;
        system.debug(isCompositeFund);
        return null;
    }
    /** 期間簡易選択選択リスト */
    private List<SelectOption> easyTermOptions;
    public List<SelectOption> getTermItems() {
    	if(easyTermOptions==null){
            // value値は期間簡易選択時の開始日の算出に利用 @See #getCalcEasySelectStartDate
	        easyTermOptions = new List<SelectOption>();
	        easyTermOptions.add(new SelectOption('',    '期間を選択してください'));
	        easyTermOptions.add(new SelectOption('-1',  '過去１ヶ月'         ));
	        easyTermOptions.add(new SelectOption('-3',  '過去３ヶ月'         ));
	        easyTermOptions.add(new SelectOption('-6',  '過去６ヶ月'         ));
	        easyTermOptions.add(new SelectOption('-12', '過去１年'          ));
	        easyTermOptions.add(new SelectOption('-36', '過去３年'          ));
	        easyTermOptions.add(new SelectOption('-60', '過去５年'          ));
	        easyTermOptions.add(new SelectOption('0',   '設定来'           ));
    	}
    	return easyTermOptions;
    }

    /**
     * 期間簡易選択の選択リスト値から、期間簡易選択の開始日を算出し、yyyyMMdd形式で返却する
     * 期間簡易選択かつ、期間が「期間を選択してください」以外の場合に実施
     *
     * @param diffMonthStr 期間簡易選択の選択リスト値
     */
    private String getCalcEasySelectStartDate(String diffMonthStr){
        Integer diffMonth = String.isNotBlank(diffMonthStr) ? Integer.valueOf(diffMonthStr) : null;
        Date castedEasyEndDate = E_Util.formattedString2DateyyyyMMdd(this.easySelectEndDate);
        Date ret;
        if(diffMonth==0){
            //　設定来の場合
            Date castedOriginFreeSelectStartDate = E_Util.formattedString2DateyyyyMMdd(this.originFreeSelectStartDate);
            // 20年前を取得
            Date castedEasyEndDate20yo = castedEasyEndDate.addYears(-20);
            // 設定来(設定開始日)が20年より前の日付の場合、20年前を開始日とする
            if(castedEasyEndDate20yo > castedOriginFreeSelectStartDate){
                ret = castedEasyEndDate20yo;
            }else{
                ret = castedOriginFreeSelectStartDate;
            }
        }else{
        	if(Math.abs(diffMonth)<12){
        		ret = castedEasyEndDate.addMonths(diffMonth);
        	}else{
        		ret = castedEasyEndDate.addYears(diffMonth/12);
        	}	
        }

        return E_Util.date2YYYYMMDD(ret);
    }

    /**
     * ユニットプライス推移グラフへ遷移します
     */
    public Pagereference doTransition004(){
    	showPageLink = false;
        //遷移先ページへのパラメータ生成
        dlCondi = null;
        dlCondi = getDLCondition();
        
        return page.up004.setRedirect(false);
    }

    /**
     * ユニットプライスへ遷移します
     */
    public Pagereference doTransition005(){
    	showPageLink = false;
        dlCondi = null;
        //遷移先ページへのパラメータ生成
        dlCondi = getDLCondition();
        
        return page.up005.setRedirect(false);
	}

    /**
     * 騰落率表示へ遷移します
     */
    public Pagereference doTransition006(){
    	showPageLink = false;
        dlCondi = null;
        //遷移先ページへのパラメータ生成
        dlCondi = getDLCondition();
        
        return page.up006.setRedirect(false);
    }

    /**
     * 特別勘定チェックボックスを全てチェックする
     */
    public Pagereference doCheckboxAllChecked(){
        for(EUPFPFSelectOption option : this.fundSelectOptions){
            option.isSelected = true;
        }
        return null;
    }

    /**
     * 特別勘定チェックボックスを全て未チェックにする
     */
    public Pagereference doCheckboxAllNotChecked(){
        for(EUPFPFSelectOption option : this.fundSelectOptions){
            option.isSelected = false;
        }
        return null;
    }
	
    /**
     * 入力値チェック
     */
	public PageReference validate() {
		showPageLink = false;
        getPageMessages().clearMessages();

        // 期間選択か自由選択かで分岐を入れる
        if(selectType == RADIO_VAL_FREE){
        	// 自由選択の場合
            // 必須チェック
            if(String.isBlank(this.freeSelectStartDate)){
				getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|011'),new String[]{'期間自由選択の開始日'}));
            }
            if(String.isBlank(this.freeSelectEndDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|011'),new String[]{'期間自由選択の終了日'}));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }

            // 日付形式チェック
            // [期間自由選択の開始日]がyyyy/MM/dd形式でない場合
            if(!E_Util.isYyyyMMdd(this.freeSelectStartDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|012'),new String[]{'期間自由選択の開始日'}));
            }
            //[期間自由選択の終了日]がyyyy/MM/dd形式でない場合
            if(!E_Util.isYyyyMMdd(this.freeSelectEndDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|012'),new String[]{'期間自由選択の終了日'}));
            }
            if(getPageMessages().hasMessages()){
                return null;
            }
            // 日付存在チェック
            if(!E_Util.isExistsYyyyMMdd(this.freeSelectStartDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|016'),new String[]{'期間自由選択の開始日'}));
            }
            if(!E_Util.isExistsYyyyMMdd(this.freeSelectEndDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|016'),new String[]{'期間自由選択の終了日'}));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }
	        // [期間自由選択の開始日] ＜【特別勘定】.[設定日]の場合
	        if(this.freeSelectStartDate < this.originFreeSelectStartDate){
	         	getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|006'),new String[]{this.originFreeSelectStartDate}));
	        }
	        // [期間自由選択の終了日] ＞ [終了日]の場合
	        if(this.originFreeSelectEndDate < this.freeSelectEndDate){
	         	getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|007'),new String[]{this.originFreeSelectEndDate}));
	        }

            // [期間自由選択の開始日] ＞　[期間自由選択の終了日]の場合
            if(this.freeSelectStartDate > this.freeSelectEndDate){
                getPageMessages().addErrorMessage(getMSG().get('UPS|017'));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }
        	// 「期間自由選択の開始日」と「期間自由選択の終了日」の差が20年以上ある場合
            Date castedFreeSelectStartDate = E_Util.formattedString2DateyyyyMMdd(this.freeSelectStartDate);
            Date castedFreeSelectEndDate = E_Util.formattedString2DateyyyyMMdd(this.freeSelectEndDate);
            // 20年前の日付を取得
            Date castedFreeSelectEndDate20yo = castedFreeSelectEndDate.addYears(-20);
            if(castedFreeSelectEndDate20yo > castedFreeSelectStartDate){
                getPageMessages().addErrorMessage(getMSG().get('UPS|008'));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }

    	}else{
    		// 簡易選択の場合
            // 必須チェック
            if(String.isBlank(easySelectEndDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|011'),new String[]{'期間簡易選択の終了日'}));
            }
    		// [期間選択ラジオボタン]が「期間簡易選択」かつ[期間簡易選択の選択期間プルダウン]が「期間を選択して下さい」の場合
            if(String.isBlank(this.easySelectTerm)){
                getPageMessages().addErrorMessage(getMSG().get('UPS|014'));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }

            // 日付形式チェック
            //[期間簡易選択の終了日]がyyyy/MM/dd形式でない場合 
            if(!E_Util.isYyyyMMdd(easySelectEndDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|012'),new String[]{'期間簡易選択の終了日'}));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }
            // 日付存在チェック
            if(!E_Util.isExistsYyyyMMdd(easySelectEndDate)){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|016'),new String[]{'期間簡易選択の終了日'}));
            }
            if(getPageMessages().hasMessages()){
                return null;
            }

	        // 「期間簡易選択の終了日」＞最も新しい終了日
            if(this.originEasySelectEndDate < this.easySelectEndDate){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|009'),new String[]{this.originEasySelectEndDate}));
            }
        	// 「期間簡易選択の終了日」＜最も古い開始日
            if(this.easySelectEndDate < this.originFreeSelectStartDate){
                getPageMessages().addErrorMessage(String.format(getMSG().get('UPS|010'),new String[]{this.originFreeSelectStartDate}));
            }
            if(getPageMessages().hasMessages()){
            	return null;
            }
    	}

        // 特別勘定選択チェックボックスが１つも選択されずにボタンが押下された場合
        boolean isSelectedVar = false;
        if(fundSelectOptions != null && fundSelectOptions.size() > 0){
            for(EUPFPFSelectOption selectOption : fundSelectOptions){
                if(selectOption.isSelected){
                    isSelectedVar = selectOption.isSelected;
                    break;
                }
            }
        }
        if(!isSelectedVar){
			getPageMessages().addErrorMessage(getMSG().get('UPS|015'));
        }

		if(getPageMessages().hasMessages()==false){
			showPageLink = true;
		}

		return null;
    }

    /**
     * CSV出力用入力値チェック
     * 入力チェックが正常の場合はCSV出力用データを作成する
     */
    public PageReference validateAndTransitionCsv() {
        // 入力値チェック
        PageReference pageReference = validate();
        if(!getPageMessages().hasMessages()){
            dlCondi = null;
            //CSV出力用パラメータ生成
            dlCondi = getDLCondition();
        }
        return pageReference;
    }

    /**
     * 初期値の設定
     */
    private void setDefaultValue(){
    	this.isCompositeFund = false;
    	
        //　特別勘定ID
        Set<Id> svcpfIdSet = new Set<Id>();
        for(E_SFGPF__c sfgpf : this.sList){
            svcpfIdSet.add(sfgpf.E_SVCPF__c);
        }
        // 選択可能ファンドからファンド選択用のOptionリストを作成
        this.fundSelectOptions = new List<EUPFPFSelectOption>();
        List<E_UPFPF__c> upfpfList = E_UPFPFDao.getRecBySvcpfId(svcpfIdSet);
        // 並び順毎にチェックボックスの設定
        for(E_SFGPF__c sfgpf : this.sList){
            for(E_UPFPF__c rec : upfpfList){
                if(sfgpf.E_SVCPF__c == rec.SVCPF__c){
                  this.fundSelectOptions.add(new EUPFPFSelectOption(rec, eupbpf.NameClass__c));
                }
             
                if(this.isCompositeFund == false && rec.ZFUNDFLG__c == true){
                	this.isCompositeFund = true;
                }
            }
        }

		/* ファンドの期間情報の取得 */
		// ファンド群のインデックス値から最大最少の日付を取得
		E_UPXPFDao.min_max_Date minmaxDate = E_UPXPFDao.getMinMaxDateBySvcpfId(svcpfIdSet);
        // Ebiz連携ログテーブルから照会日を取得
        //String inquiryDate = E_Util.date2YYYYMMDD(E_bizDataSyncLogDao.getRecByLast1().InquiryDate__c); 修正前
        String inquiryDate = E_Util.date2YYYYMMDD(getDataSyncDate_DateType());
system.debug('@@@inquiryDate:'+inquiryDate);
        if(String.isNotBlank(inquiryDate) && minmaxDate.maxDate > inquiryDate){
            // Ebiz連携ログテーブルに設定されている値を上限値とする
			minmaxDate.maxDate = inquiryDate;
		}

        // 日付項目保持
        this.freeSelectEndDate = E_Util.getFormatDate(minmaxDate.maxDate,'yyyy/MM/dd');
        this.easySelectEndDate = E_Util.getFormatDate(minmaxDate.maxDate,'yyyy/MM/dd');
        this.originFreeSelectEndDate = E_Util.getFormatDate(minmaxDate.maxDate,'yyyy/MM/dd');
        this.originEasySelectEndDate = E_Util.getFormatDate(minmaxDate.maxDate,'yyyy/MM/dd');

        // DatePicker終了年度の取得
        if(minmaxDate.maxDate != null && minmaxDate.maxDate.length() == 8){
            this.datePickerEndYear = minmaxDate.maxDate.left(4);
        }

        this.freeSelectStartDate = E_Util.getFormatDate(minmaxDate.minDate,'yyyy/MM/dd');
        this.originFreeSelectStartDate = E_Util.getFormatDate(minmaxDate.minDate,'yyyy/MM/dd');
        // DatePicker終了年度の取得
        if(minmaxDate.minDate != null && minmaxDate.minDate.length() == 8){
            this.datePickerStartYear = minmaxDate.minDate.left(4);
        }
    }

    /**
     * 遷移先ページへのパラメータ生成
     */
    private E_DownloadCondition.UnitPriceDownloadCondi getDLCondition(){
        E_DownloadCondition.UnitPriceDownloadCondi dc = new E_DownloadCondition.UnitPriceDownloadCondi();

		if(selectType == RADIO_VAL_FREE){
            // 期間自由選択
            dc.fromTerm = E_Util.slashTrim(freeSelectStartDate);     // yyyyMMdd
            dc.toTerm = E_Util.slashTrim(freeSelectEndDate);         // yyyyMMdd
        }else{
            // 期間簡易選択
            dc.fromTerm = getCalcEasySelectStartDate(easySelectTerm);// yyyyMMdd
            dc.toTerm = E_Util.slashTrim(easySelectEndDate);         // yyyyMMdd
        }
        dc.EUPBPF = this.uid;
        dc.NAMEKBN = eupbpf.NameClass__c;
        dc.ZSFUNDCD = new List<String>();
        for(EUPFPFSelectOption opt:fundSelectOptions){
            if(opt.isSelected){
                dc.ZSFUNDCD.add(opt.record.Id);
            }
        }
        return dc;
    }
    
	/* ******************* 特別勘定一覧　表示用リスト *********************** */
    /** 特別勘定一覧　表示用リスト(1リスト：3ファンド) */
    public List<List<EUPFPFSelectOption>> getFundSelectOptionsList(){

        List<List<EUPFPFSelectOption>> rowList = new List<List<EUPFPFSelectOption>>();
        List<EUPFPFSelectOption> dataList = new List<EUPFPFSelectOption>();
        Integer cnt = 0;
        
        for(EUPFPFSelectOption fund : this.fundSelectOptions){
        	// 3ファンド　
        	if(cnt < 3){
        		dataList.add(fund);
        		cnt += 1;
        	}else{
        		rowList.add(dataList);
        		dataList = new List<EUPFPFSelectOption>();
        		dataList.add(fund);
        		cnt = 1;
        	}
        }
        if(!dataList.isEmpty()){
        	rowList.add(dataList);
        }
        
        return rowList;
    }
	/* **************************************************************** */

    //####################### 内部クラス #######################
    /** 特別勘定選択Option */
    public class EUPFPFSelectOption{
        /** チェックボックスの有無 */
        public Boolean isSelected{get;set;}
        /** ユニットプライス表示用ファンド */
        public E_UPFPF__c record{get; private set;}
        /** 名称区分 */
        private String NAMEKBN;
        /** Constructor */
        public EUPFPFSelectOption(E_UPFPF__c rec, String nmKbn){
            this.record = rec;
            this.NAMEKBN = nmKbn;
            this.isSelected = false;
        }
        /** 表示用特別勘定名の取得 */
        public String getLabel(){
            return getLabel(record,NAMEKBN);
            /*
            if(NAMEKBN == '0' && !record.ZFUNDFLG__c){
                // ファンド投資信託名
                return record.ZNAMEUnion__c;
            }else if(NAMEKBN == '0' && record.ZFUNDFLG__c){
                // 投資信託名
                return record.ZIVTNAMEUnion__c;
            }else{
                // ファンド名
                return record.ZFNDNAMEUnion__c;
            }
			*/
        }
        /** 設定開始日の取得 */
        public String getStartyyyyMMdd(){
            if(this.record.E_UPXPFs__r.size()==0) return null;
            return this.record.E_UPXPFs__r[0].ZFUNDDTE__c;
        }
    }
    
    /** 表示用特別勘定名の取得 */
    public static String getLabel(E_UPFPF__c UPFPF,String NAMEKBN){
        if(NAMEKBN == '0' && !UPFPF.ZFUNDFLG__c){
            // ファンド投資信託名
            return UPFPF.ZNAMEUnion__c;
        }else if(NAMEKBN == '0' && UPFPF.ZFUNDFLG__c){
            // 投資信託名
            return UPFPF.ZIVTNAMEUnion__c;
        }else{
            // ファンド名
            return UPFPF.ZFNDNAMEUnion__c;
        }
    }
    
    
}