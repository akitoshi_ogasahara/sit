/*
 * CC_SRPolicyValidTgrHdlTest
 * Test class of CC_SRPolicyValidTgrHdl
 * created  : Accenture 2018/6/26
 * modified :
 */

@isTest
private class CC_SRPolicyValidTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create Account test data(Account0,Account1)
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '代理店' limit 1];
			List<Account> accountList = CC_TestDataFactory.getAccountSkelList(recordTypeOfAccount.Id);
			insert accountList;
			//Create Contact test data(Contact0,Contact1)
			List<Contact> contactList = CC_TestDataFactory.getContactSkelList();
			for(Contact contact : contactList){
				contact.AccountId = accountList.get(0).Id;
			}
			insert contactList;
			//Create EPolicy test data(with Contact0)
			E_Policy__c newEPolicy = CC_TestDataFactory.getEPolicySkel(accountList.get(0).Id, contactList.get(0).Id);
			newEPolicy.Contractor__c = contactList.get(0).Id;
			insert newEPolicy;
			//Create FormMaster test data
			CC_FormMaster__c newFormMaster1 = CC_TestDataFactory.getFormMasterSkel();
			insert newFormMaster1;
			//Create FormMaster test data
			CC_FormMaster__c newFormMaster2 = CC_TestDataFactory.getCoverLetterSkel();
			insert newFormMaster2;
			//Create SRTypeMaster test data
			CC_SRTypeMaster__c newSRTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
			insert newSRTypeMaster;

		}
	}

	/**
	* Test Before Insert01
	* CMN_ContactName__c != null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeInsert01() {
		System.runAs ( testUser ) {
			try{

				//Prepare Case test data(with Contact0)
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				Contact contact0 = [select Id from Contact where LastName = 'Contact0'];
				newCase.CMN_ContactName__c = contact0.Id;
				insert newCase;
				//Prepeare EPolicy test data(with Contact0)
				E_Policy__c newEPolicy = [select Id from E_Policy__c where COMM_CHDRNUM__c = '10001'];
				//Update Case test data(with Contact1)
				Contact contact1 = [select Id from Contact where LastName = 'Contact1'];
				newCase.CMN_ContactName__c = contact1.Id;
				update newCase;
				//Prepeare SRPolicy test data(with Contact0)
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_PolicyID1__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID2__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID3__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID4__c = newEPolicy.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				insert newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Insert02
	* CMN_CorporateName__c != null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeInsert02() {
		System.runAs ( testUser ) {
		try{

				//Prepare Case test data(with Account0)
				Account account0 = [select Id from Account where Name = 'testAccount0'];
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				newCase.CMN_CorporateName__c = account0.Id;
				insert newCase;
				//Prepeare EPolicy test data(with Account0)
				E_Policy__c newEPolicy = [select Id from E_Policy__c where COMM_CHDRNUM__c = '10001'];
				//Update Case test data(with Account1)
				Account account1 = [select Id from Account where Name = 'testAccount1'];
				newCase.CMN_CorporateName__c = account1.Id;
				update newCase;
				//Prepeare SRPolicy data & EPolicy data(with Contact0, Account0)
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_PolicyID1__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID2__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID3__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID4__c = newEPolicy.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				insert newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Insert03
	* CC_SRTypeId__c != null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeInsert03() {
		System.runAs ( testUser ) {
		try{

				//Prepare Case test data
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				insert newCase;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster = [select Id from CC_FormMaster__c where CC_FormNo__c = '200001'];
				newFormMaster.CC_SRTypeNo__c = '200001';
				update newFormMaster;
				//Prepeare SRPolicy test data
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_FormMaster1__c = newFormMaster.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				insert newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Insert04
	* CC_SRTypeId__c == null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeInsert04() {
		System.runAs ( testUser ) {
			try{

				//Prepare Case test data
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				newCase.CC_SRTypeId__c = null;
				insert newCase;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster = [select Id from CC_FormMaster__c where CC_FormNo__c = '200001'];
				//Prepeare SRPolicy test data
				CC_SRPolicy__c newSRPolicy = new CC_SRPolicy__c();
				newSRPolicy.CC_FormMaster1__c = newFormMaster.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				insert newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Insert05
	* CC_SRTypeId__c != null CC_FormType__c
	*/
	static testMethod void srPolicyTgrHdlTestBeforeInsert05() {
		System.runAs ( testUser ) {
			try{

				//Prepare Case test data
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				insert newCase;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster = [select Id from CC_FormMaster__c where CC_FormNo__c = '200001'];
				//Prepeare SRPolicy test data
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_FormMaster1__c = newFormMaster.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				insert newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update01
	* CMN_ContactName__c != null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeUpdate01() {
		System.runAs ( testUser ) {
			try{

				//Create Case test data(with Contact0)
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				Contact contact0 = [select Id from Contact where LastName = 'Contact0'];
				newCase.CMN_ContactName__c = contact0.Id;
				insert newCase;
				//Update EPolicy data(with Contact0)
				E_Policy__c newEPolicy = [select Id from E_Policy__c where COMM_CHDRNUM__c = '10001'];
				newEPolicy.Contractor__c = contact0.Id;
				update newEPolicy;
				//Prepeare SRPolicy data(with Contact0)
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_PolicyID1__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID2__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID3__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID4__c = newEPolicy.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;
				insert newSRPolicy;
				//Update Case test data(with Contact1)
				Contact contact1 = [select Id from Contact where LastName = 'Contact1'];
				newCase.CMN_ContactName__c = contact1.Id;
				update newCase;
				//Update SRPolicy test data(with Contact1,Account1)
				Account account1 = [select Id from Account where Name = 'testAccount1'];
				E_Policy__c newEPolicy1 = CC_TestDataFactory.getEPolicySkel(account1.Id, contact1.Id);
				insert newEPolicy1;
				newSRPolicy.CC_PolicyID1__c = newEPolicy1.Id;
				newSRPolicy.CC_PolicyID2__c = newEPolicy1.Id;
				newSRPolicy.CC_PolicyID3__c = newEPolicy1.Id;
				newSRPolicy.CC_PolicyID4__c = newEPolicy1.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				update newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update02
	* CMN_CorporateName__c != null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeUpdate02() {
		System.runAs ( testUser ) {
			try{

				//Create Case test data(with Account0)
				Account account0 = [select Id from Account where Name = 'testAccount0'];
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				newCase.CMN_CorporateName__c = account0.Id;
				insert newCase;
				//Prepeare SRPolicy data & EPolicy data(with Contact0, Account0)
				E_Policy__c newEPolicy = [select Id from E_Policy__c where COMM_CHDRNUM__c = '10001'];
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_PolicyID1__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID2__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID3__c = newEPolicy.Id;
				newSRPolicy.CC_PolicyID4__c = newEPolicy.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;
				insert newSRPolicy;
				//Prepeare SRPolicy data(with Contact1)
				Account account1 = [select Id from Account where Name = 'testAccount1'];
				Contact contact1 = [select Id from Contact where LastName = 'Contact1'];
				E_Policy__c newEPolicy1 = CC_TestDataFactory.getEPolicySkel(account1.Id, contact1.Id);
				insert newEPolicy1;
				newSRPolicy.CC_PolicyID1__c = newEPolicy1.Id;
				newSRPolicy.CC_PolicyID2__c = newEPolicy1.Id;
				newSRPolicy.CC_PolicyID3__c = newEPolicy1.Id;
				newSRPolicy.CC_PolicyID4__c = newEPolicy1.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;

				Test.startTest();
				update newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update03
	* CMN_CorporateName__c != null CC_SRTypeNo__c
	*/
	static testMethod void srPolicyTgrHdlTestBeforeUpdate03() {
		System.runAs ( testUser ) {
			try{

				//Prepare Case test data
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				insert newCase;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster1 = [select Id from CC_FormMaster__c where CC_FormNo__c = '100001'];
				newFormMaster1.CC_SRTypeNo__c = '200001';
				update newFormMaster1;
				//Create SRPolicy test data
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_FormMaster1__c = newFormMaster1.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster1.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;
				insert newSRPolicy;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster2 = [select Id from CC_FormMaster__c where CC_FormNo__c = '200001'];
				newFormMaster2.CC_SRTypeNo__c = '200001';
				update newFormMaster2;
				//Update SRPolicy test data
				newSRPolicy.CC_FormMaster1__c = newFormMaster2.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster2.Id;

				Test.startTest();
				update newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update04
	* CC_SRTypeId__c == null
	*/
	static testMethod void srPolicyTgrHdlTestBeforeUpdate04() {
		System.runAs ( testUser ) {
			try{

				//Prepare Case test data
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				newCase.CC_SRTypeId__c = null;
				insert newCase;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster1 = [select Id from CC_FormMaster__c where CC_FormNo__c = '100001'];
				newFormMaster1.CC_FormType__c = '請求書';
				update newFormMaster1;
				//Prepeare SRPolicy test data
				CC_SRPolicy__c newSRPolicy = new CC_SRPolicy__c();
				newSRPolicy.CC_FormMaster1__c = newFormMaster1.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster1.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;
				insert newSRPolicy;
				//Prepare another FormMaster test data
				CC_FormMaster__c newFormMaster2 = [select Id from CC_FormMaster__c where CC_FormNo__c = '200001'];
				newFormMaster2.CC_FormType__c = '確認書';
				update newFormMaster2;
				//Update SRPolicy test data
				newSRPolicy.CC_FormMaster1__c = newFormMaster2.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster2.Id;

				Test.startTest();
				update newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update05
	* CMN_CorporateName__c != null CC_FormType__c
	*/
	static testMethod void srPolicyTgrHdlTestBeforeUpdate05() {
		System.runAs ( testUser ) {
			try{

				//Prepare Case test data
				CC_SRTypeMaster__c newSRTypeMaster = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c = 'testSRTypeMaster' limit 1];
				Case newCase = CC_TestDataFactory.getCaseSkel(newSRTypeMaster.Id);
				insert newCase;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster1 = [select Id from CC_FormMaster__c where CC_FormNo__c = '100001'];
				newFormMaster1.CC_SRTypeNo__c = '200001';
				update newFormMaster1;
				//Create SRPolicy test data
				CC_SRPolicy__c newSRPolicy = new  CC_SRPolicy__c();
				newSRPolicy.CC_FormMaster1__c = newFormMaster1.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster1.Id;
				newSRPolicy.CC_CaseId__c = newCase.Id;
				insert newSRPolicy;
				//Update FormMaster test data
				CC_FormMaster__c newFormMaster2 = [select Id from CC_FormMaster__c where CC_FormNo__c = '200001'];
				update newFormMaster2;
				//Update SRPolicy test data
				newSRPolicy.CC_FormMaster1__c = newFormMaster2.Id;
				newSRPolicy.CC_FormMaster2__c = newFormMaster2.Id;

				Test.startTest();
				update newSRPolicy;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}
}