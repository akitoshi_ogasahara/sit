@isTest
private class TestASS_RecTypeDao {
	//エリア部長で実行
	private static testMethod void getRecTypeByDevNameTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			RecordType retRecType = ASS_RecTypeDao.getRecTypeByDevName('Event','DailyReport');
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retRecType.Name,'営業日報');
		}
	
	}

	private static testMethod void getRecTypesBySubTypeTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<RecordType> retRecTypeList = ASS_RecTypeDao.getRecTypesBySubType('Event');
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retRecTypeList.size()>0,true);
		}
	
	}

	//MRで実行
	private static testMethod void getRecTypeByDevNameMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			RecordType retRecType = ASS_RecTypeDao.getRecTypeByDevName('Event','DailyReport');
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retRecType.Name,'営業日報');
		}
	
	}

	private static testMethod void getRecTypesBySubTypeMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<RecordType> retRecTypeList = ASS_RecTypeDao.getRecTypesBySubType('Event');
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retRecTypeList.size()>0,true);
		}
	
	}
}