@isTest
private class TestI_SalesPlanPDFController {
	private static Account acc;
	private static E_AgencySalesResults__c asr;
	private static E_SalesPlan__c sp;
	private static Attachment att;

	// 未入力
	@isTest static void createPDFAndShowTest01() {
		createData();
		Pagereference testpage = Page.IRIS_SalesPlanPDF;
		testpage.getParameters().put('planId', sp.Id);
		I_SalesPlanPDFController controller = new I_SalesPlanPDFController();
		testpage.getParameters().put('isClear', 'true');
		Test.startTest();
		Test.setCurrentPage(testpage);
		PageReference result = controller.createPDFAndShow();
		Test.stopTest();
	}
	// PDF出力
	@isTest static void createPDFAndShowTest02() {
		createData();
		Pagereference testpage = Page.IRIS_SalesPlanPDF;
		testpage.getParameters().put('planId', sp.Id);
		I_SalesPlanPDFController controller = new I_SalesPlanPDFController();
		testpage.getParameters().put('isClear', 'false');
		Test.startTest();
		Test.setCurrentPage(testpage);
		PageReference result = controller.createPDFAndShow();
		Test.stopTest();
	}
	
	// テストデータ作成
	private static void createData(){

		acc = new Account(Name = 'テスト代理店格'
						, E_CL1PF_ZHEADAY__c = '10001');
		insert acc;

		asr = new E_AgencySalesResults__c(ParentAccount__c = acc.Id
										, BusinessDate__c = '20000101'
										, Hierarchy__c = 'AH'
										, QualifSim__c = '優績S');
		insert asr;

		sp = new E_SalesPlan__c(ParentAccount__c = acc.Id
								, SalesResult__c = asr.Id);
		insert sp;
	}
}