public with sharing class E_UPError999Controller extends E_AbstractUnitPriceController{
	
	private String errorMsgCode; 

   	//ページタイトル
	private static final String PAGE_TITLE = 'エラーページ';
	private static final String DEFAULT_ERROR_MSG = 'エラーが発生しました。';

    
	public E_UPError999Controller () {
		errorMsgCode = ApexPages.currentPage().getParameters().get('code');
        this.pgTitle = PAGE_TITLE;
	}
	
	public PageReference init() {
		E_MessageMaster__c msgMaster = E_MessageMasterDao.getRecByCode(errorMsgCode);
		if (msgMaster != null) {
			List<String> params = new List<String>();
			
			//String.formatに埋め込む値を最大3つ受取可能とする。
			for(Integer i=1;i<=3;i++){
				String pNM = 'p' + String.valueOf(i);
				if(ApexPages.currentPage().getParameters().containsKey(pNM)){
					params.add(ApexPages.currentPage().getParameters().get(pNM));
				}
			}
			
			getPageMessages().addErrorMessage(String.format(msgMaster.Value__c, params));
			
		}else{
			getPageMessages().addErrorMessage(DEFAULT_ERROR_MSG + '[' +errorMsgCode + ']');
		}
		return null;
	}
}