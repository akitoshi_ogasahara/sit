// CMSセクション管理情報クラス
public class I_CMSSection {
	// コンポーネント
	public I_ContentMaster__c record {get; set;}
	// コンテンツリスト
	public List<I_CMSComponentController> cmsComps{get;set;}

	public I_CMSSection(I_ContentMaster__c record){
		this.record = record;
		this.cmsComps  = new List<I_CMSComponentController>();
	}

/* Day2にてChatterFileを利用するため引数追加
		public void addCMSComp(I_ContentMaster__c cmRec, List<Attachment> attchs) {
			this.cmsComps.add(new I_CMSComponentController(cmRec, attchs));
		}
*/
	public void addCMSComp(I_ContentMaster__c cmRec, List<Attachment> attchs, Id contentId) {
		this.cmsComps.add(new I_CMSComponentController(cmRec, attchs, contentId));
	}

	/**
	 *		getDomId
	 *			Domに出力するIDを返す ()
	 */
	public String getDomId(){
		String DOM_ID_PREFIX = 'iris-cms-';
		if(String.isBlank(this.record.Dom_Id__c)){
			return DOM_ID_PREFIX + String.valueOf(this.record.id);
		}else{
			return DOM_ID_PREFIX + this.record.Dom_id__c;
		}
	}
	
	//NNTubeピックアップのセクションの場合True
	public Boolean getIsNNTubePickUps(){
		return record.SelectFrom__c == 'NNTube'; 
	}
	
	//IRISコンテンツのピックアップのセクションの場合True
	public Boolean getIsIRISPickups(){
		return record.SelectFrom__c == 'CMSコンテンツ'; 
	}
}