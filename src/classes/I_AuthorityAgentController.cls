public with sharing class I_AuthorityAgentController extends I_AbstractController {

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get; private set;}
	//変更確定モーダル用メッセージ
	public String  confirmationMessage {get; set;}
	// リスト行数
	public Integer rowCount {get; private set;}

	// 募集人一覧(表示用)
	public List<I_AuthorityAgentRowManager.AgentListRow> agentRows {get; set; }

	// 募集人一覧取得件数
	public Integer getAgentCount() {
		return agentRows == null ? 0 : agentRows.size();
	}
	//事務所一覧（モーダル用）
	public List<OfficeListRow> officeRows {get; set;}

	//事務所一覧取得件数
	public Integer getOfficeRowsCount(){
		return officeRows == null ? 0 : officeRows.size();
	}
	//初期表示時事務所件数
	public Integer initOfficeCount {get; private set;}

	//現在選択している事務所件数
	public Integer currentOfficeCount {get; private set;}

	// 代理店名
	public String agency {get; private set;}

	//代理店格Id
	public String searchId{get; private set;}

	//画面表示制御用変数
	//一事務所登録フラグ
	public boolean is1Offing {get; private set;}

	//全事務所検索フラグ
	public Boolean isAllSearch {get; private set;}

	//検索Boxの値
	public String searchText {get; set;}

	//事務所選択モーダル全件選択チェックボックス
	public Boolean isAllCheck{get; set;}

	// ソートキー(募集人)
	public String sortType {get; set;}
	// ソート順(募集人)
	public Boolean sortIsAsc {get; set;}
	//ソートキー(代理店事務所)
	public String officeSortType {get; set;}
	//ソート順(代理店事務所)
	public Boolean officeSortIsAsc {get; set;}


	// 募集人一覧：ソート実行用：項目
	private static String agentList_sortType;
	// 募集人一覧：ソート実行用：昇順 OR 降順
	private static Boolean agentList_sortIsAsc;

	// 事務所一覧モーダル：ソート実行用：項目
	private static String officeList_sortType;
	// 事務所一覧モーダル：ソート実行用：昇順 OR 降順
	private static Boolean officeList_sortIsAsc;

	//変更確定モーダル用ID管理
	public E_IDCPF__c authorityEidcpf {get;set;}
	//変更確定モーダル用ID管理ID
	public String eidcpfId {get;set;}
	//募集人1000件以上フラグ
	public Boolean isOverView {get; private set;}
	//変更確定モーダル用募集人種別
	public String authorityAgType {get; private set;}

	// 内部定数
	// URLパラメータ名 - ソート項目
	private static final String URL_PARAM_SORT_TYPE	 = 'st';
	// URLパラメータ名 - フィルタID
	private static final String URL_PARAM_FILTER_ID	 = 'id';
	// ソートキー
	//募集人一覧・変更確定モーダル用ソートキー
	public static final String SORT_TYPE_AGENT	= 'agentName';
	public static final String SORT_TYPE_AGENTCODE	= 'agentCode';
	public static final String SORT_TYPE_IDSTATUS		= 'idStatus';
	public static final String SORT_TYPE_AGTYPE		= 'agtype';
	//public static final String SORT_TYPE_USERID		= 'userId';
	public static final String SORT_TYPE_EMAIL		= 'email';
	public static final String SORT_TYPE_OFFICE		= 'office';
	public static final String SORT_TYPE_COMMISSION	= 'commission';

	//事務所選択モーダル
	public static final String SORT_TYPE_OFFICENAME  = 'officeName';
	public static final String SORT_TYPE_OFFICECODE	= 'officeCode';
	public static final String SORT_TYPE_UNIT			= 'unit';
	public static final String SORT_TYPE_OWNER		= 'owner';

	//管理責任者用
	private static final String OPERATIONS_MANAGER = '業管';

	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}

	/**
	 * コンストラクタ
	 */
	public I_AuthorityAgentController() {
		pageMessages = getPageMessages();

		//メンバ変数の初期化
		is1Offing = true;
		isAllSearch = true;
		isAllCheck = true;
		rowCount = 0;
		initOfficeCount = 0;
		currentOfficeCount = 0;
		searchText = '';
		eidcpfId = '';
		isOverView = false;

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	protected override String getLinkMenuKey() {
		return 'authorityManage';
	}

	//Atria権限管理用の権限セットがついていなければエラーページへ遷移
	protected override Boolean isValidate(){
		return access.hasATRAuthMngRead();
	}

	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}

		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}

		searchId = '';
		List<Account> officeList = new List<Account>();

		//社員ならば、遷移元の代理店格Idを取得
		if( access.isEmployee() ){
			searchId = apexPages.currentPage().getParameters().get('accid');
			//accIdがない場合には代理店一覧ページへ遷移
			if (String.isBlank(searchId)) {
				return redirectAgencyPage();
			}
		} else {
			//社員でなければ、自分の所属する代理店格コードから代理店格Idを取得
			String accCode = access.getAccountCode();
			searchId = E_AccountDaoWithout.getParentRecByAccCode( accCode ).Id;
		}

		//is1Offingを判定する処理を追加
		if(E_AccountDaoWithout.getRecById(searchId).Z1OFFING__c != 'Y'){
			Is1Offing = false;
		}

		officeList = getOfficeList(searchId);

		if ( officeList.isEmpty() ){
			initOfficeCount = 0;
			currentOfficeCount = initOfficeCount;
		} else {
			//事務所リストが空でない場合は、代理店名と一事務所登録フラグを判定
			agency = officeList[0].Parent.Name ;
			if ( officeList[0].Parent.Z1OFFING__c != 'Y' ){
				Is1Offing = false;
			}
			//1000行未満であれば取得した事務所件数を初期事務所件数と現在の事務所件数の両方に設定する
			if( officeList.size() < I_Const.LIST_MAX_ROWS )	{
				initOfficeCount = officeList.size();
				currentOfficeCount = initOfficeCount;
			} else {
			//1000行以上の場合、エラーメッセージの分岐のため、条件に当てはまる事務所の正確な件数を取得する必要がある
			//現在の表示行数は最大値で固定
				initOfficeCount = overLimitofficeCount( searchId );
				currentOfficeCount = Integer.valueOf ( I_Const.LIST_MAX_ROWS );
			}

		}

		// デフォルトのソート：事務所コード : 昇順
		officeSortType = SORT_TYPE_OFFICECODE;
		officeSortIsAsc = true;

		//事務所一覧行作成
		createOfficeRows(officeList);

		//MRまたは拠点長 かつ 母店担当でない かつ 一事務所登録でなければ、初期表示は自分の担当している拠点分のみとする。
		if( ( access.isAuthMR() || access.isAuthManager() ) && !access.ZINQUIRR_is_BRAll() && !is1Offing ){
			String ownerId = UserInfo.getUserId();
			for( officeListRow row : officeRows ){
				//営業の場合初期表示時は、自分の担当している代理店事務所のみをチェックする
				if( row.ownerId != ownerId ){
					row.IsSelect = false;
					row.IsSelectBefore = false;
					currentOfficeCount--;//現在チェックをしている件数を設定する
					//isAllsearch = false;
					isAllCheck = false;
				}
			}
		}

		//募集人一覧行作成
		List<Id> officeIdList = new List<Id>();
		for ( officeListRow row : officeRows){
			if(row.IsSelect){
				officeIdList.add( row.id );
			}
		}
		I_AuthorityAgentRowManager rowMng = new I_AuthorityAgentRowManager();
		//デフォルトのソート順/方向
		sortType = SORT_TYPE_AGENT;
		sortIsAsc = true;
		I_AuthorityAgentRowManager.agentList_sortType	= sortType;
		I_AuthorityAgentRowManager.agentList_sortIsAsc = sortIsAsc;

		createAgentRow( searchId , officeIdList , searchText);

		return null;
	}


	/** 画面アクション*/

	/**
	 * 「事務所選択」ボタン押下時の処理：確定されずにモーダルが閉じた場合、画面初期表示または最後に確定した状態に
	 *チェック状態を戻す。
	 */
	public void openOfficeModal(){
		Integer checkedNum = 0;
		for ( officeListRow row : officeRows ){
			row.isSelect = row.isSelectBefore; // チェック状態を以前の状態に戻す
			if(row.isSelect){
				checkedNum++;			//チェック済みの件数をカウントする
			}
		}
		 isAllCheck = ( checkedNum == initOfficeCount || checkedNum == I_Const.LIST_MAX_ROWS ) ? true : false;

	}

	/**
	 * 事務所選択モーダルの「確定」ボタン押下時の処理：絞り込んだ事務所で募集人一覧行を再作成
	 */
	public PageReference officeSelect(){
		agentRows = new List<I_AuthorityAgentRowManager.AgentListRow>();

		searchText = '';
		//IsChangeAuthorityFlag = false;
		//チェックのついている事務所の一覧を取得
		List<String> officeIdList =new List<String>();
		for ( OfficeListRow row : OfficeRows ){
			if( row.isSelect ){
				officeIdList.add( row.Id );
			}
			//チェックボックスが変更されていれば、その内容を次にモーダルを開いた時のために保持
			if( row.isSelect != row.isSelectBefore ){
				row.isSelectBefore = row.isSelect;
			}
		}
		//絞り込み判定のため、現在選択中の事務所件数を設定
		currentOfficeCount = officeIdList.size();
		//事務所の初期表示件数 = 現在選択件数 もしくは、現在選択件数が行の最大値であれば「全件表示中」とする
		//isAllSearch = ( I_Const.LIST_MAX_ROWS == currentOfficeCount || initOfficeCount == currentOfficeCount  ) ? true: false;
		isAllCheck = ( I_Const.LIST_MAX_ROWS == currentOfficeCount || initOfficeCount == currentOfficeCount  ) ? true: false;
		//募集人一覧の作成
		createAgentRow( searchId, officeIdList, searchText );


		return null;

	}

	/**
	 * 「検索」ボタン押下時の処理：事務所選択で選ばれている内容＋検索ボックスの内容で募集人一覧を再作成する
	 */
	 public PageReference searchAgent(){
		//IsChangeAuthorityFlag = false;//変更確定ボタンを非活性にする

		//チェックのついている事務所の一覧を取得
		List<String> officeIdList =new List<String>();
		for ( OfficeListRow row : OfficeRows ){
			if( row.isSelectBefore ){
				officeIdList.add( row.Id );
			}
		}
		createAgentRow( searchId, officeIdList, searchText );

		return null;
	 }

	/**
	 * ログを作成し、csvページへ遷移する
	 */
	 public PageReference doDownloadCSV(){

	 	E_Log__c elog = E_LogUtil.createLog();
	 	//必要な情報をjsonにする
	 	JSONGenerator gen = JSON.createGenerator(true);
	 	gen.writeStartobject();

	 	gen.writeStringField('SearchText', searchText ); //検索ワード
	 	gen.writeBooleanField('isAllSearch' , isAllSearch); //全件検索のフラグ
	 	//一事務所登録でなく、母店担当でないMRまたは拠点長の場合は拠点コードを渡す
		if( ( access.isAuthMR() || access.isAuthManager() ) && !access.ZINQUIRR_is_BRAll() && !is1Offing ){
	 		gen.writeStringField( 'branchCode', access.getAccessKindFromThree() );//拠点コード
		} else {
	 		gen.writeStringField( 'branchCode', '' );//拠点コード
		}
		if( access.isEmployee() ){
			//社員の場合はURLパラメータから取得
	 		gen.writeStringField('accId', apexPages.currentPage().getParameters().get('accid')  ); //代理店格Id
		} else {
			//社員以外の場合は自身の代理店格コードより取得
			String accCode = access.getAccountCode();
	 		gen.writeStringField('accId', E_AccountDaoWithout.getParentRecByAccCode( accCode ).Id  ); //代理店格Id
		}
	 	gen.writeFieldName('SelectOffice'); //選択された事務所の一覧
	 	gen.writeStartArray();
	 	for( OfficeListRow row :officeRows ){
	 		if( row.isSelect ){
	 			gen.writeString( row.Id );
	 		}
	 	}
	 	gen.writeEndArray();
	 	gen.writeStringField('SortType' , sortType ); //ソート項目
	 	gen.writeBooleanField( 'SortIsAsc' , sortIsAsc ); //ソート方向

	 	gen.writeEndObject();

	 	elog.detail__c = gen.getAsString();
	 	//ログ情報を設定してinsertする
	 	PageReference pr = Page.IRIS_ExportAuthorityAgent;
	 	elog.AccessPage__c =  pr.getUrl();//遷移先のページを設定
	 	elog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_DOWNLOAD;
	 	elog.Name = 'csvダウンロード';

	 	insert elog;

		//遷移先を指定し,作成したelogのIdを渡してページ遷移
	 	pr.getParameters().put('id' , elog.Id);
	 	return pr;
	 }

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}

		return null;
	}

	/*
	 * 変更ボタン押下時の処理
	 */
	public void changeBtnAction(){
		//変更ボタンが押下された募集人のID管理のIdを取得
		eidcpfId = System.currentPageReference().getParameters().get('eidcpfId');
		authorityEidcpf = E_IDCPFDaoWithout.getRecsByZWEBID(eidcpfId);

		Map<String,String> AGTYPE_MAP = new Map<String,String>();
		{
			AGTYPE_MAP.put('AH','代理店権限');
			AGTYPE_MAP.put('AY','事務所権限');
			AGTYPE_MAP.put('AT','募集人権限');
		}

		if(authorityEidcpf.User__r.Contact.E_CL3PF_ZAGMANGR__c == 'Y'){
			authorityAgType = OPERATIONS_MANAGER +'('+ AGTYPE_MAP.get(authorityEidcpf.ZIDTYPE__c) + ')';
		}else{
			authorityAgType = AGTYPE_MAP.get(authorityEidcpf.ZIDTYPE__c);
		}
	}

	/**
	 *変更確定モーダルの「確定」ボタン押下時の処理：ID管理を更新する
	 */
	 public void updateAuthority(){
	 	//更新用リスト
	 	List<E_IDCPF__c> upIdcpfList = new List<E_IDCPF__c>();
	 	//ID管理レコード
	 	E_IDCPF__c upIdcpf = new E_IDCPF__c();

	 	//Id
	 	upIdcpf.Id = authorityEidcpf.Id;
	 	//Atria手数料試算権限編集可能フラグ
	 	upIdcpf.AtriaCommissionAuthorityEditable__c = authorityEidcpf.AtriaCommissionAuthorityEditable__c;
	 	upIdcpfList.add(upIdcpf);

	 	for(I_AuthorityAgentRowManager.AgentListRow ar : agentRows){
	 		if(ar.E_IDCPFId == eidcpfId){
	 			ar.commissionTxt = (authorityEidcpf.AtriaCommissionAuthorityEditable__c) ? '有':'無';
	 			ar.commissionSort = (authorityEidcpf.AtriaCommissionAuthorityEditable__c) ? '1':'3';
	 		}
	 	}
		if( upIdcpfList.size() > 0 ){
			E_IDCPFDaoWithout.updateRecs(upIdcpfList);
		}
	 }

	/** 画面アクション ここまで*/

	/**
	 * 代理店に紐づく事務所の一覧を取得
	 */
	private List<Account> getOfficeList( String searchKey ){

	    String sortField = '';
		List<account> offices = new List<Account>();
		String code = '';
		//一事務所登録でない かつ 母店担当でないMR/拠点長の際には、照会者コードの一致する事務所のみを取得する。
		if( ( access.isAuthMR() || access.isAuthManager() ) && !access.ZINQUIRR_is_BRAll() && !is1Offing ){
			code = access.getAccessKindFromThree();
		}

		// 代理店格IDで事務所を取得
		offices = E_AccountDaoWithout.getRecsByAgency_Atria(searchKey, code, sortField, sortIsAsc);

		return offices;
	}

	/**
	 * 3000件以上でエラーメッセージを分岐する必要があるため、事務所件数が1000件以上の場合は正確な事務所件数を取得する
	 */
	private Integer overLimitOfficeCount( String searchKey ){
		// 代理店格IDで事務所を取得
		return  E_AccountDaoWithout.getRecsByAgencyCount( searchKey );
	}

	/**
	 * 募集人一覧行作成クラス
	 */
	private void createAgentRow(String accId, List<String>  officeIdList, String searchText){
		AgentRows = new List<I_AuthorityAgentRowManager.AgentListRow>();
		I_AuthorityAgentRowManager rowMng = new I_AuthorityAgentRowManager();
		I_AuthorityAgentRowManager.agentList_sortType	= sortType;
		I_AuthorityAgentRowManager.agentList_sortIsAsc = sortIsAsc;

		String branchCode = '';
		//一事務所登録でなく、母店担当のMRでも拠点長でないユーザが全件選択している時以外は事務所リストでの取得を行う、
		if( !is1Offing && ( !isAllCheck || ( !access.ZINQUIRR_is_BRAll() && ( access.isAuthMR() || access.isAuthManager() ) ) ) ){
			branchCode = access.getAccessKindFromThree();
			isAllsearch = false;
		}
		AgentRows = rowMng.createAgentRows( isAllsearch , searchId , branchCode , officeIdList , searchText , false  );

		ErrorMeessageSetting( searchId, officeIdList, searchText );
	}
	/**
	 * エラーメッセージの初期化および、事務所、募集人のエラーを画面を再読み込みするたびに設定する
	 */

	private void ErrorMeessageSetting( String searchId, List<Id> offIdList, String keyword ){
		//初期取得時の件数で 事務所が1001件以上の場合のエラーメッセージを設定する。
		pageMessages.clearMessages();
		//事務所の上限値超のエラーは不要
		/*
		if( initOfficeCount > I_Const.LIST_MAX_ROWS){
			pageMessages.addWarningMessage(getMSG().get('LIS|005'));//事務所件数が1000件を超えている旨のエラー
		}
		*/
		//共同GW時には、ダウンロードに関するメッセージを表示する
		if( access.isCommonGateway() ){
			pageMessages.addWarningMessage(getMSG().get('LIS|011'));
		}

		//募集人件数が上限に達している場合、3000件を超えているかどうかを判定する必要がある
		if( agentRows.size() == 0 ){
				isOverView = false;
				if(String.isBlank(searchText)){
					pageMessages.addWarningMessage(getMSG().get('LIS|010'));//検索時以外で募集人件数が0件のエラー
				}else {
					pageMessages.addWarningMessage(getMSG().get('IRIS|SEARCH|001'));//検索時募集人件数が0件のエラー
				}
		} else {
			Integer rowCountNum = 0;
			if(isAllSearch){
				rowCountNum = E_ContactDaoWithout.getRecCountByAgencyId( searchId, keyword );
			} else {
				rowCountNum = E_ContactDaoWithout.getRecCountByOfficeIds( offIdList, keyword );
			}
			if(rowCountNum > I_Const.LIST_MAX_ROWS){
				isOverView = true;
				if(rowCountNum <= I_Const.LIST_DL_MAX_ROWS){
					pageMessages.addWarningMessage(getMSG().get('LIS|006'));//募集人件数が1001件以上のエラー
				} else {
					pageMessages.addWarningMessage(getMSG().get('LIS|008'));//募集人件数が3001件以上のエラー
				}
			} else {
				isOverView = false;
			}
		}
	}


	/**
	 * 事務所一覧(モーダル用)作成
	 */
	private void createOfficeRows( List<Account> officeList  ){
		officeRows = new List<OfficeListRow>();
		//画面に表示する事務所レコード
		OfficeListRow row;

		for( Account ac : officeList){
			row = new OfficeListRow();
			row.id = ac.Id;
			row.name = ac.Name;
			row.code = ac.E_CL2PF_ZAGCYNUM__c;//代理店事務所コード
			row.unit = ac.AGNTBR_NM__c;//担当拠点
			row.owner = ac.Owner.Name;//担当MR
			row.ownerId = ac.OwnerId;
			row.nameSort = ac.AccountNameKANA__c;//代理店事務所名（カナ）
			row.branch = ac.E_CL2PF_BRANCH__c; //支社コード
			officeRows.add( row );
		}
		I_AuthorityAgentController.officeList_sortType	= officeSortType;
		I_AuthorityAgentController.officeList_sortIsAsc = officeSortIsAsc;

		officeRows.sort();

	}

	/**
	 * ソート
	 */
	 //募集人一覧用ソート
	public void sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if (sortType != sType) {
			sortType	= sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		I_AuthorityAgentRowManager.agentList_sortType = sortType;
		I_AuthorityAgentRowManager.agentList_sortIsAsc = sortIsAsc;

		AgentRows = I_AuthorityAgentRowManager.sortRows( AgentRows );

		return;
	}
	//事務所モーダル用ソート
	public void officeModalSortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if (officeSortType != sType) {
			officeSortType	= sType;
			officeSortIsAsc = true;
		} else {
			officeSortIsAsc = !officeSortIsAsc;
		}

		I_AuthorityAgentController.officeList_sortType	= officeSortType;
		I_AuthorityAgentController.officeList_sortIsAsc = officeSortIsAsc;

		officeRows.sort();

		return;
	}


	/**
	 * 代理店一覧へ遷移
	 */
	public PageReference redirectAgencyPage() {
		PageReference pr = Page.IRIS_AuthorityAgency;
		pr.setRedirect(true);
		return pr;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return agentRows == null ? 0 : agentRows.size();
	}

	/**
	 * 内部クラスでモーダル用事務所リストを作成
	 */
	public class OfficeListRow implements Comparable {
		public Boolean isSelect {get; set;}
		private Boolean isSelectBefore;
		private String id;
		public String name {get; private set;}
		public String code {get; private set;}
		public String unit {get; private set;}
		public String owner{get; private set;}
		private String ownerId;
		private String nameSort;
		private String branch;


		// コンストラクタ
		public OfficeListRow() {
			isSelect = true;
			isSelectBefore = true;
			id = '';
			name = '';
			code = '';
			unit = '';
			owner = '';
			ownerId = '';
			nameSort = '';// カナソート用変数
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			OfficeListRow compareToRow = (OfficeListRow)compareTo;
			Integer result = 0;

			// 事務所名
			if (officeList_sortType == SORT_TYPE_OFFICENAME) {
				result = I_Util.compareToString(nameSort, compareToRow.nameSort);
			//事務所コード
			} else if(officeList_sortType == SORT_TYPE_OFFICECODE){
				result = I_Util.compareToString(code, compareToRow.code);
			//担当拠点名
			} else if(officeList_sortType == SORT_TYPE_UNIT){
				result = I_Util.compareToString(unit, compareToRow.unit);
			//担当MR
			} else if(officeList_sortType == SORT_TYPE_OWNER){
				result = I_Util.compareToString(owner, compareToRow.owner);
			}

			return officeList_sortIsAsc ? result : result * (-1);
		}

	}

	/**
	 * Atria手数料変更前後管理用クラス
	 */
	 public class AtriaCommissionCheck{

		public Boolean authorityFlag {get; set;}
	 	private String commissionSort {get; private set;} //ソート用文言 1:有(トグル) 2:有(テキスト) 3:無(トグル) 4:無(テキスト) 5:-

		//コンストラクタ
		public AtriaCommissionCheck() {
			commissionSort = '';
			authorityFlag = false;
		}
	 }
}