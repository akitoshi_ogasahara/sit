public class E_ShareUtil {

	/**
	 * L1+[代理店コード]Shareレコード取得
	 * @param Id ダウンロード履歴ID
	 * @param Set<String> 代理店コード
	 * @return List<E_DownloadHistorry__Share>
	 */
	public static List<E_DownloadHistorry__Share> getDownloadHistoryShareL1(Id recId, Set<String> agencyCodes){
		// 公開グループ取得(L1＋代理店コード)
		Map<String, Group> groupMap = getZhedayGroups(agencyCodes);
		
		// Shareレコード作成
		List<E_DownloadHistorry__Share> dhShareList = getShareByGroup(recId, 'L1', agencyCodes, groupMap);
		return dhShareList;
	}
	
	/**
	 * L2+[事務所コード],BR+[支社コード]Shareレコード取得
	 * @param Id ダウンロード履歴ID
	 * @param Set<String> 事務所コード
	 * @param Set<String> 支社コード
	 * @return List<E_DownloadHistorry__Share>
	 */
	public static List<E_DownloadHistorry__Share> getDownloadHistoryShareL2(Id recId, Set<String> officeCodes, Set<String> branchCodes){
		List<E_DownloadHistorry__Share> dhShareList = new List<E_DownloadHistorry__Share>();

		// 公開グループ取得(L2＋事務所コード)
		if(!officeCodes.isEmpty()){
			Map<String, Group> groupMapL2 = getZagcynumGroups(officeCodes);  
			dhShareList.addAll(getShareByGroup(recId, 'L2', officeCodes, groupMapL2));
		}

	// 公開グループ取得(BR＋支社コード)
	if(!branchCodes.isEmpty()){
		Map<String, Group> groupMapBR = getBranchGroups(branchCodes); 
		dhShareList.addAll(getShareByGroup(recId, 'BR', branchCodes, groupMapBR));
	}
		
		return dhShareList;
	}

	/**
	 * BR+[支社コード]Shareレコード取得
	 * @param Id ダウンロード履歴ID
	 * @param Set<String> 支社コード
	 * @return List<E_DownloadHistorry__Share>
	 */
	public static List<E_DownloadHistorry__Share> getDownloadHistoryShareBR(Id recId, Set<String> brCds){
		// 公開グループ取得(BR＋支社コード)
		Map<String, Group> groupMap = getBranchGroups(brCds);
		
		// Shareレコード作成
		List<E_DownloadHistorry__Share> dhShareList = getShareByGroup(recId, 'BR', brCds, groupMap);
		return dhShareList;
	}


	/**
	 * 代理店格の公開グループを取得
	 * 引数の代理店格コードリストより公開グループ(L1 + ZHEADAY)を取得
	 * @param Set<String>
	 * @return Group
	 */
	public static Map<String, Group> getZhedayGroups(Set<String> agencyCodes){
		Set<String> cds = new Set<String>();
		for (String acd : agencyCodes) {
			cds.add('L1' + acd);
		}
		return getGroupMap(cds);
	}
	
	/**
	 * 代理店事務所の公開グループを取得
	 * 引数の代理店事務所コードリストより公開グループ(L2 + ZAGCYNUM)を取得
	 * @param Set<String>
	 * @return Map<String, Group>
	 */
	public static Map<String, Group> getZagcynumGroups(Set<String> officeCodes){
		Set<String> cds = new Set<String>();
		for (String ocd : officeCodes) {
			cds.add('L2' + ocd);
		}
		return getGroupMap(cds);
	}
	
	/**
	 * 代理店事務所の公開グループを取得
	 * 引数の支社コードリストより公開グループ(BR +   E_CL2PF_BRANCH__c)を取得
	 * @param Set<String>
	 * @return Map<String, Group>
	 */
	public static Map<String, Group> getBranchGroups(Set<String> branchCodes){
		Set<String> cds = new Set<String>();
		for (String bcd : branchCodes) {
			cds.add('BR' + bcd);
		}
		return getGroupMap(cds);
	}
	
	/**
	 * 営業母店支社コードの公開グループを取得
	 * 引数のSalesforceIDリストより営業母店支社コード公開グループ(BR + ZBUSBR)を取得
	 * @param Set<String>
	 * @return Map<String, Group>
	 */
	public static Map<String, Group> getZbusbrGroup(Set<Id> ids){
		List<Account> accounts = [Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c, E_CL1PF_ZBUSAGCY__c
									,(select id, ZMRCODE__c, E_CL2PF_ZAGCYNUM__c, E_CL2PF_ZBUSBR__c from ChildAccounts)
								From Account
									where id in :ids];
	
		Set<String> cds = new Set<String>();
		for (Account parentAcc : accounts) {
			for (Account childAcc : parentAcc.ChildAccounts) {
				if (parentAcc.E_CL1PF_ZBUSAGCY__c.equals(childAcc.E_CL2PF_ZAGCYNUM__c)) {
					cds.add('BR' + childAcc.E_CL2PF_ZBUSBR__c);
					break;
				}
			}
		}
		return getGroupMap(cds);
	} 

	/**
	 * 引数のDeveloperNameリストを元に公開グループを取得する
	 * @param Set<String> 
	 * @return Map<String, Group>        TODO クエリの発行回数を制限する必要あり
	 *
	 */
	private static Map<String, Group> getGroupMap(Set<String> cds){
		if (cds == null) {
			return null;
		}
		Map<String, Group> groupMap = new Map<String, Group>();
		for (Group grp : [Select Id, Name, Type, DeveloperName From Group where type = 'Regular' and developerName in : cds]) {
			groupMap.put(grp.DeveloperName, grp);
		}
		return groupMap;
	}
	
	/**
	 * 対象のコードをキーとした公開グループを取得
	 */
	private static List<E_DownloadHistorry__Share> getShareByGroup(Id recId, String grpType, Set<String> cds, Map<String, Group> grpMap){
		// Shareレコード作成
		List<E_DownloadHistorry__Share> dhShareList = new List<E_DownloadHistorry__Share>();
		for(String cd : cds){
			Group grp = grpMap.get(grpType + cd);
			if(grp != null){
				dhShareList.add(getDownloadHistoryShare(recId, grp.Id));
			}
		}
		return dhShareList;
	}
		
	/**
	 * Shareレコード作成
	 */
	private static E_DownloadHistorry__Share getDownloadHistoryShare(Id recId, Id grpId){
			E_DownloadHistorry__Share dhShare = new E_DownloadHistorry__Share();
			dhShare.ParentId = recId;
			dhShare.UserOrGroupId = grpId;
			dhShare.AccessLevel = 'Read';
			dhShare.RowCause = Schema.E_DownloadHistorry__Share.RowCause.Manual;
			return dhShare;
	}

	/**
	 * 代理店格コードをキーとしたID管理（業務管理責任者）リストのMap取得
	 */
	public static Map<String, List<E_IDCPF__c>> getIdcpfMap(Set<String> parentCodes){
		Map<String, List<E_IDCPF__c>> idcpfMap = new Map<String, List<E_IDCPF__c>>();

		// 代理店格に関連する募集人（業務管理責任者）のID管理を取得し、代理店格コードをキーとしたID管理リストMap生成
		String pCode = '';
		List<E_IDCPF__c> idcpfList;
		for(E_IDCPF__c idcpf : E_IDCPFDao.getRecsByParentCodes(parentCodes)){
			// 新しい代理店格コード
			if(pCode != idcpf.User__r.Contact.E_AccParentCord__c){
				pCode = idcpf.User__r.Contact.E_AccParentCord__c;
				idcpfList = new List<E_IDCPF__c>();
			// 既にMapのキーとして存在する代理店格コード
			}else{
				idcpfList = idcpfMap.get(pCode);
			}

			// ID管理リストにAdd
			idcpfList.add(idcpf);
			idcpfMap.put(pCode, idcpfList);
		}

		return idcpfMap;
	}

	//　2016.07  AMSプロジェクトにて追加    TODO　上記　E_DownloadHistory特化してるメソッドは別クラスへ移行する。
	
	// NNLinkの共有ルール名（各オブジェクトのApex共有の理由に設定しておくこと）　　NNLink共有：NNLinkSharingRule
	public static final String SHARE_ROWCAUSE_NNLink = 'NNLinkSharingRule__c';

	/**
	 *  createNNLinkShares
	 *      NNLinkの標準アクセスコントロールモデルに基づいたShareレコードを設定する。
	 *        所有者を事務所の担当MR、L1公開グループ、L2公開グループ、BR支社公開グル―プを設定する。
	 *        ※将来的にはoverrideされた関数で各設定の要否を選択可能とする。（例えば所有者を設定しないとか・・・）      TODO 複数オブジェクト不可にしてしまう　deleteと合わせる
	 */
	public static List<String> createNNLinkShares(Map<SObject, Account> recs){
		List<String> errMsgs = new List<String>();

		//公開グループの対象抽出
		Set<String> groupNames = new Set<String>();

		// 代理店格コードのSet
		Set<String> parentCodes = new Set<String>();
		
		//代理店格顧客コードのSetを作成
		Set<String> agencyCustomerCodes = new Set<String>();

		// 自主点検事務所の代理店格コードをキーとした、処理対象の事務所のリストMap
		Map<String, List<Account>> relatedOfficeListMap = new Map<String, List<Account>>();
		Set<String> relatedParentCodes = new Set<String>();
		for(SObject sobj : recs.keySet()){
			Account office = recs.get(sobj);

			// 事務所、または代理店格コードがNullの場合はSkip
			if(office == null || String.isBlank(office.E_ParentZHEADAY__c)) continue;

			// 自主点検事務所レコードの事務所単位でのアクセス権付与対象（～2017年度）
			if(SC_Const.SC_FISCALYEAR_BEFORE2017.contains(String.valueOf(sobj.get('FiscalYear__c')))){
				relatedOfficeListMap.put(office.E_ParentZHEADAY__c, new List<Account>{office});

			// 代理店格配下の事務所単位でのアクセス権付与対象（2018年度～）
			}else{
				relatedParentCodes.add(office.E_ParentZHEADAY__c);
			}
		}

		if(!relatedParentCodes.isEmpty()){
			for(Account related : [Select Id, E_ParentZHEADAY__c, E_CL2PF_ZAGCYNUM__c, E_CL2PF_BRANCH__c From Account Where E_ParentZHEADAY__c In :relatedParentCodes]){
				if(related.E_ParentZHEADAY__c == null) continue;

				List<Account> relatedList = relatedOfficeListMap.get(related.E_ParentZHEADAY__c);
				if(relatedList == null){
					relatedList = new List<Account>();
				}

				relatedList.add(related);
				relatedOfficeListMap.put(related.E_ParentZHEADAY__c, relatedList);
			}
		}

		for(Account acc:recs.values()){
			//事務所Accountレコードが取得できない場合は、Skipする。
			if(acc == null) continue;
			
			// 顧客番号
			if(acc.Parent != null && String.isNotBlank(acc.Parent.E_CL1PF_CLNTNUM__c)){
				agencyCustomerCodes.add(acc.Parent.E_CL1PF_CLNTNUM__c);
			}
			//L1
			if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
				groupNames.add('L1' + acc.E_ParentZHEADAY__c);
				parentCodes.add(acc.E_ParentZHEADAY__c);
			}

			for(Account relatedOffice : relatedOfficeListMap.get(acc.E_ParentZHEADAY__c)){
				//L2
				if(String.isNotBlank(relatedOffice.E_CL2PF_ZAGCYNUM__c)){
					groupNames.add('L2' + relatedOffice.E_CL2PF_ZAGCYNUM__c);
				}
				//支社コード
				if(String.isNotBlank(relatedOffice.E_CL2PF_BRANCH__c)){
					groupNames.add('BR' + relatedOffice.E_CL2PF_BRANCH__c);
				}
			}
		}


/*
		for(Account acc:recs.values()){
			//事務所Accountレコードが取得できない場合は、Skipする。
			if(acc==null)continue;
			
			//L1
			if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
				groupNames.add('L1' + acc.E_ParentZHEADAY__c);
				parentCodes.add(acc.E_ParentZHEADAY__c);
			}
			//L2
			if(String.isNotBlank(acc.E_CL2PF_ZAGCYNUM__c)){
				groupNames.add('L2' + acc.E_CL2PF_ZAGCYNUM__c);
			}
			//支社コード
			if(String.isNotBlank(acc.E_CL2PF_BRANCH__c)){
				groupNames.add('BR' + acc.E_CL2PF_BRANCH__c);
			}

			if(acc.Parent != null && String.isNotBlank(acc.Parent.E_CL1PF_CLNTNUM__c)){
				agencyCustomerCodes.add(acc.Parent.E_CL1PF_CLNTNUM__c);
			}
		}
*/
		Map<String, Group> grMap = getGroupMap(groupNames);

		// 代理店格コードをキーとしたID管理リストのMap
		Map<String, List<E_IDCPF__c>> idcpfMap = getIdcpfMap(parentCodes);

		//顧客コードと個人法人区分のマップ作成
		Map<String,String> corporateIndividualMap = E_ContactDao.getCustomerCodeMap(agencyCustomerCodes);
		
		//Shareレコードの作成
		List<SObject> updateParents = new List<SObject>();
		List<SObject> shares = new List<SObject>();
		String sObjName;        //メインオブジェクトのAPI名
		Schema.SObjectType shareObjType;    ////ShareオブジェクトType
		String lastRecPrefix = null;  //前処理のレコードPrefix
		// MRマップ
		Map<String, User> mrUserMap = E_UserDao.getMrUserRecordMap();
		// 拠点長マップ
		Map<String, Id> mrManagerMap = E_UserDao.getMrManagerMap();
		// 本社営業部拠点長
		ID HQManagerId = E_UserDao.getHQManagerId();
system.debug('* chech 00 *');
		for(SObject parentRec:recs.keyset()){
			Account acc = recs.get(parentRec);
			//事務所Accountレコードが取得できない場合は、Skipする。
			if(acc==null)continue;
			
			//ShareオブジェクトのAPI名取得    prefixが変更された場合のみDescribeを呼び出す。
			if(lastRecPrefix == null || lastRecPrefix != String.valueOf(parentRec.Id).subString(0,3)){
				sObjName = parentRec.Id.getSObjectType().getDescribe().getName();
				String shareObjName = sObjName.replace('__c','__share');
				shareObjType = Schema.getGlobalDescribe().get(shareObjName);
				lastRecPrefix = String.valueOf(parentRec.Id).subString(0,3);
			}
			
system.debug('* chech 01 *');
			//担当MRを所有者に設定
			User mr = mrUserMap.get(acc.ZMRCODE__c);
			Boolean isUpdate = false;
system.debug('* chech 02 *' + mr + ' => ' + acc.ZMRCODE__c);
			if(mr!=null && parentRec.get('ownerId') != mr.Id){
system.debug('* chech 03 *');
				parentRec.put('ownerId', mr.Id);
				isUpdate = true;
			}

			// 拠点長を設定
			if(Boolean.valueOf(parentRec.get('isHeadSection__c'))){
				if(parentRec.get('ownerId') != HQManagerId){
					parentRec.put('MRManager__c', HQManagerId);
					isUpdate = true;
				}
			} else if(mrManagerMap != null && mr != null && mrManagerMap.containskey(mr.userRole.ParentRoleId)){
				ID managerId = mrManagerMap.get(mr.userRole.ParentRoleId);
				if(parentRec.get('ownerId') != managerId){
					parentRec.put('MRManager__c', managerId);
					isUpdate = true;
				}
			} else {
				parentRec.put('MRManager__c', '');
				isUpdate = true;
			}

			//代理店格顧客コード
			String clntnum = acc.Parent.E_CL1PF_CLNTNUM__c;
			if(corporateIndividualMap != null && String.isNotBlank(clntnum) && corporateIndividualMap.containsKey(clntnum)){

				//全角アルファベットを半角に変換
				String clttype = corporateIndividualMap.get(clntnum);

				//変換後の法人個人区分
				String agencyType =E_Const.SCOFFICE_CLTTYPEMAP.get(clttype);

				//法人/個人区分が'C'または'P'の時
				if(String.isNotBlank(agencyType)){
					//法人/個人区分の値が変更された場合のみ上書き
					if(parentRec.get('OfficeType__c') != agencyType){
						parentRec.put('OfficeType__c',agencyType);
						isUpdate = true;
					}
				}
			}
	
			if(isUpdate){
				updateParents.add(parentRec);
			}

			//L1　公開グループ設定
			if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
				group grL1 = grMap.get('L1'+acc.E_ParentZHEADAY__c);
				if(grL1!=null){
					SObject shareL1 = shareObjType.newSObject();
					shareL1.put('parentId', parentRec.Id);
					shareL1.put('AccessLevel', 'Edit');
					shareL1.put('RowCause', SHARE_ROWCAUSE_NNLink);
					shareL1.put('UserOrGroupId',grL1.Id);
					shares.add(shareL1);
				}
			}

			for(Account relatedOffice : relatedOfficeListMap.get(acc.E_ParentZHEADAY__c)){
				//L2　公開グループ設定
				if(String.isNotBlank(relatedOffice.E_CL2PF_ZAGCYNUM__c)){
					group grL2 = grMap.get('L2'+relatedOffice.E_CL2PF_ZAGCYNUM__c);
					if(grL2!=null){
						SObject shareL2 = shareObjType.newSObject();
						shareL2.put('parentId', parentRec.Id);
						shareL2.put('AccessLevel', 'Edit');
						shareL2.put('RowCause', SHARE_ROWCAUSE_NNLink);    //'NNLinkSharingRule__c'
						shareL2.put('UserOrGroupId',grL2.Id);
						shares.add(shareL2);
					}
				}    
				//BR　公開グループ設定
				if(String.isNotBlank(relatedOffice.E_CL2PF_BRANCH__c)){
					group grBR = grMap.get('BR'+relatedOffice.E_CL2PF_BRANCH__c);
					if(grBR!=null){
						SObject shareBR = shareObjType.newSObject();
						shareBR.put('parentId', parentRec.Id);
						shareBR.put('AccessLevel', 'Edit');
						shareBR.put('RowCause',SHARE_ROWCAUSE_NNLink);    //'NNLinkSharingRule__c
						shareBR.put('UserOrGroupId',grBR.Id);
						shares.add(shareBR);
					}  
				}
			}
/*
			//L2　公開グループ設定
			if(String.isNotBlank(acc.E_CL2PF_ZAGCYNUM__c)){
				group grL2 = grMap.get('L2'+acc.E_CL2PF_ZAGCYNUM__c);
				if(grL2!=null){
					SObject shareL2 = shareObjType.newSObject();
					shareL2.put('parentId', parentRec.Id);
					shareL2.put('AccessLevel', 'Edit');
					shareL2.put('RowCause', SHARE_ROWCAUSE_NNLink);    //'NNLinkSharingRule__c'
					shareL2.put('UserOrGroupId',grL2.Id);
					shares.add(shareL2);
				}
			}    
			//BR　公開グループ設定
			if(String.isNotBlank(acc.E_CL2PF_BRANCH__c)){
				group grBR = grMap.get('BR'+acc.E_CL2PF_BRANCH__c);
				if(grBR!=null){
					SObject shareBR = shareObjType.newSObject();
					shareBR.put('parentId', parentRec.Id);
					shareBR.put('AccessLevel', 'Edit');
					shareBR.put('RowCause',SHARE_ROWCAUSE_NNLink);    //'NNLinkSharingRule__c
					shareBR.put('UserOrGroupId',grBR.Id);
					shares.add(shareBR);
				}  
			}
*/
			// 2018年度以降
			if(Boolean.valueOf(parentRec.get('IsIris__c'))){
				// 代理店格配下の募集人（業務管理責任者）設定 
				if(idcpfMap.containsKey(acc.E_ParentZHEADAY__c)){
					List<E_IDCPF__c> idcpfs = idcpfMap.get(acc.E_ParentZHEADAY__c);
					// 代理店格配下の募集人（業務管理責任者）にレコードアクセス権を付与
					for(E_IDCPF__c idcpf : idcpfs){
						SObject shareID = shareObjType.newSObject();
						shareID.put('parentId', parentRec.Id);
						shareID.put('AccessLevel', 'Edit');
						shareID.put('RowCause',SHARE_ROWCAUSE_NNLink);    //'NNLinkSharingRule__c
						shareID.put('UserOrGroupId',idcpf.OwnerId);
						shares.add(shareID);
					}
				}
			}
		}

		try{
			List<Database.Saveresult> results = new List<Database.Saveresult>();
			// 自主点検事務所の更新
			if(updateParents.isEmpty()==false){
				results = Database.update(updateParents, false);

				// Errorレコード判定
				for(Integer i = 0; i < results.size(); i++){
					if(!results[i].isSuccess()) {
						Database.Error err = results[i].getErrors().get(0);
						errMsgs.add('[' + updateParents[i].Id + ':' + err.getMessage() + ']');
					}
				}
			}
			// 自主点検事務所の共有登録
			insert shares;

		}catch(Exception e){
			errMsgs.add(e.getMessage());
		}

		return errMsgs;
	}

	/**
	 * deleteNNLinkShares
	 * 共有の理由が『NNLinkSharingRule』であるレコードを削除する。
	 */
	public static void deleteNNLinkShares(Set<SObject> recs){
		if(recs.isEmpty()) return;
		
		List<SObject> recsList = new List<SObject>(recs);
		String sObjPrefix = String.valueOf(recsList[0].Id).subString(0,3);
		String shareObjName = recsList[0].Id.getSObjectType().getDescribe().getName().replace('__c','__share');

		for(SObject rec :recsList){
			system.assertEquals(String.valueOf(rec.Id).subString(0,3), sObjPrefix
									, '複数のオブジェクトをまとめて処理できません。オブジェクト単位で呼び出してください。');
		}

		String soql = 'SELECT Id, ParentId';
		soql += ' FROM ' + shareObjName;
		soql += ' WHERE parentId in :recs ';
		soql += ' AND ROWCause = :SHARE_ROWCAUSE_NNLink';

		List<SObject> shares = Database.query(soql);
		delete shares;
	}
}