public with sharing class SC_SelfCompliancesExtender extends AMS_AbstractExtender{

    @TestVisible
    private SC_SelfCompliancesController controller;

	public SC_StatusManager manager {get; set;}

	// 前回点検URL
	public String prevSCURL {get; set;}
	public Boolean getHasPrevSCOffice() {
		return String.isNotBlank(prevSCURL);
	}

	// 戻るURL
	public String retSCURL {get; set;}
	public Boolean getHasRetSCOffice() {
		return String.isNotBlank(retSCURL);
	}

    /**
     * 提出書類一覧
     */
    public List<SC_SelfCompliance__c> selfCompliances{
    	get{
    		return SC_SelfComplianceDao.getAttachmentsByOfficeId(this.controller.record.Id);
    	}
    	private set;
    }
    
    /**
     * AY権限ユーザ判定
     */
    public Boolean getIsAy(){
    	return access.isAuthAY();
    }

    /**
     * 参照のみユーザ判定
     */
    public Boolean getIsReadOnly(){
    	return access.hasSCDirectorPermission();
    }

    /**
     * メニューキー
     */
    public String getMenuKey(){
    	return access.isEmployee() == true ? SC_Const.MENU_KEY_SC_SEARCH_EMPLOYEE : SC_Const.MENU_EKY_SC_SEARCH_AGENT;
    }
    
    /**
     * sp2:ステータス「未提出」判定
     */
    public Boolean getIsStatus01(){
    	return (this.controller.record.Status__c == SC_Const.AMSOFFICE_STATUS_01)
    			&& (   this.manager.getIsAG()  || this.manager.getIsMR() || this.manager.getIsCMD()
    			); 
    }

    public String getSCStatus02(){
    	return SC_Const.AMSOFFICE_STATUS_02;
    }

    public String getSCStatus05(){
        return SC_Const.AMSOFFICE_STATUS_05;
    }

    /**
     * Constructor
     */
    public SC_SelfCompliancesExtender(SC_SelfCompliancesController extension){
        super();
        this.controller = extension;
        
        // ヘルプページ（操作説明）
        this.helpMenukey = SC_Const.MENU_HELP_SELFCOMPLIANCE;
        this.helpPageJumpTo = SC_Const.MENU_HELP_SELFCOMPLIANCE_VIEW;
        
        // ヘルプページ（各種資料ダウンロード）
		this.guideMenukey = SC_Const.MENU_GUIDE_SELFCOMPLIANCE;
		//this.guidePageJumpTo = SC_Const.MENU_GUIDE_SELFCOMPLIANCE_GUIDE;
    }
    
    /**
     * init
     */
    public override void init(){
        if(this.controller.record == null 
                || String.isBlank(this.controller.record.ZAGCYNUM__c)){
            isSuccessOfInitValidate = false;
            pageMessages.addErrorMessage(SC_Const.ERR_MSG01_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            isSuccessOfInitValidate = true;
        }       

		// 前年度、次年度のURLセット
		setOtherYearUrl();
		/*
		// sp2:前回実施点検URL生成
		prevSCURL = '';
		List<SC_Office__c> prevOffice = SC_OfficeDao.getPrevSCOffice(controller.record.FiscalYearNum__c, controller.record.AccZAGCYNUM__c);
		if(prevOffice != null && !prevOffice.isEmpty()){
			prevSCURL = Page.E_SCSelfCompliances.getUrl() + '?id=' + prevOffice.get(0).Id;
		}

		// sp2:次回実施点検URL生成
		retSCURL = '';
		List<SC_Office__c> retOffice = SC_OfficeDao.getNextSCOffice(controller.record.FiscalYearNum__c, controller.record.AccZAGCYNUM__c);
		if(retOffice != null && !retOffice.isEmpty()){
			retSCURL = Page.E_SCSelfCompliances.getUrl() + '?id=' + retOffice.get(0).Id;
		}
		*/

		// sp2:ステータスマネージャを作成
        this.manager = new SC_StatusManager(this.controller.record, this.access);

		return;
    }
    
    /**
     * sp2:ネクストアクション処理
     */
    public PageReference doNextAction(){
    	PageReference pr;
    	
    	// メッセージ初期化
    	pageMessages.clearMessages();
    	
    	// アップロードファイル件数チェック
    	if(selfCompliances.isEmpty()){
    		pageMessages.addErrorMessage(SC_Const.ERR_MSG02_NOT_UPLOAD_FILES);
    		return null;
    	}
    	
    	// 現在のボタン名
    	String actionLabel = manager.getNextActionLabel();
    	
    	// ネクストアクション
    	pr = manager.doNextAction();
    	
    	// 処理完了メッセージ
    	if(String.isNotBlank(manager.msgActionComplete)){
    		pageMessages.addErrorMessage(manager.msgActionComplete);
    	}

    	return null;
    }

	/*
	 * 提出書類アップロードボタン押下時
	 */ 
	public PageReference toUploadPage(){
		PageReference pr = Page.E_SCSelfComplianceSubmit;
		pr.getParameters().put('parentid', controller.record.Id);
		return pr;
	}
	
	/**
	 * 前年度、次年度のURLセット
	 */
	private void setOtherYearUrl(){
		SC_Office__c office = this.controller.record;
		
		if(office.FiscalYearNum__c == null){
			return;
		}
		
		// 前年度
		Decimal prevYear = office.FiscalYearNum__c - 1;
		// 次年度
		Decimal retYear = office.FiscalYearNum__c + 1;
		
		// 前年度、次年度の自主点検事務所取得
		Set<Decimal> years = new Set<Decimal>{prevYear, retYear};
		years.remove(null);
		for(SC_Office__c rec : [Select Id, FiscalYearNum__c From SC_Office__c Where AccZAGCYNUM__c = :office.AccZAGCYNUM__c And FiscalYearNum__c In :years]){
			// 2018年判定
			String scUrl = '';
			if(rec.FiscalYearNum__c < 2018){
				scUrl = Page.E_SCSelfCompliances.getUrl();
			}else{
				scUrl = Page.IRIS_SCSelfCompliances.getUrl();
			}
			
			// 前年度
			if(rec.FiscalYearNum__c == prevYear){
				prevSCURL = scUrl + '?id=' + rec.Id;
			
			// 次年度
			}else if(rec.FiscalYearNum__c == retYear){
				retSCURL = scUrl + '?id=' + rec.Id;
			}
		}
	}
}