public with sharing class I_FooterController {

	private E_AccessController access;
	public I_FooterController(){
		access = E_AccessController.getInstance();
	}

	public String getPrivacyPolicyPageKey(){
		//return 'PrivacyPolicy';
		String key = '';
		if(access.canViewSumiseiIRIS()){
			key = 'PrivacyPolicySumisei';
			if(access.isEmployee()){
				key += '&isSMTM=1';
			}
		}else{
			key = 'PrivacyPolicy';
		}
		return key;
	}

	public String getInternetPolicyPageKey(){
		//return access.isCommonGateWay()?'GWUsePolicy':'InternetUsePolicy';
		String key;
		if(access.isCommonGateWay()){
			key = 'GWUsePolicy';
		}else{
			if(access.canViewSumiseiIRIS()){
				key = 'InternetUsePolicySumisei';
				if(access.isEmployee()){
					key += '&isSMTM=1';
				}
			}else{
				key = 'InternetUsePolicy';
			}
		}
		return key;
	}

	//CMSページ名を返します。
	public String getIRISCMSPageName(){
		//ログイン前はIRISPublicです。
		return access.isGuest()?'IRISPublic':'IRIS';
	}
	public Boolean getIsGuest(){
		return access.isGuest();
	}

	//============	2017.12 たもつくん対応 START	=============//
	public Boolean getViaMobileSDK(){
		return access.isViaMobileSDK();
	}
	//============	2017.12 たもつくん対応  END	=============//

}