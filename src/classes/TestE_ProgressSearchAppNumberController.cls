@isTest
private with sharing class TestE_ProgressSearchAppNumberController{
		private static testMethod void testPageMethods() {	
			E_ProgressSearchAppNumberController page = new E_ProgressSearchAppNumberController(new ApexPages.StandardController(new E_CPCPF__c()));	
			page.getOperatorOptions_E_CPCPF_c_ZWEBCNTT_c();	
			page.getOperatorOptions_E_CPCPF_c_PROPNUM_c();	
			System.assert(true);
		}	
			
	private static testMethod void testsearchResult() {
		E_ProgressSearchAppNumberController.searchResult searchResult = new E_ProgressSearchAppNumberController.searchResult(new List<E_CPCPF__c>(), new List<E_ProgressSearchAppNumberController.searchResultItem>(), new List<E_CPCPF__c>(), null);
		searchResult.setPageItems(new List<E_ProgressSearchAppNumberController.searchResultItem>());
		searchResult.create(new E_CPCPF__c());
		searchResult.doDeleteSelectedItems();
		searchResult.setPagesize(10);		searchResult.doFirst();
		searchResult.doPrevious();
		searchResult.doNext();
		searchResult.doLast();
		searchResult.doSort();
		System.assert(true);
	}
	
}