public with sharing class E_EmployeeSalesResultsShare extends E_ShareBase {

	private Set<SObject> records;
    
	/**
	 * Constructor
	 */
	public E_EmployeeSalesResultsShare(Set<SObject> records){
		this.records = records;
	}

	/**
	 * execute
	*/
	public void execute(){
		// Share削除
		//Insertのみのため削除は行わない
//		deleteNNLinkShares(records);
		
		// Share登録
		createSharesNNLink(records, E_Const.SHARE_ACCESS_LEVEL_READ);
	}

	/*
	 * @param records: 代理店挙積情報
	 * @param level: アクセスレベル
	 */
	private List<SObject> createSharesNNLink(Set<SObject> records, String level){
		shares = new List<SObject>();
		
		// AccessLevel
		accessLevel = level;
		
		// Prefix
		lastRecPrefix = null;

		// ShareBizLogic
		createSharesBizLogic(records);
		
		// 正常レコードのみInsert、エラーレコードは連携ログの詳細を更新
		if(shares.isEmpty() == false){
			// Insert
			List<Database.SaveResult> insrList = Database.insert(shares,false);
			
			// エラーレコードのエラー内容をセット
			errMsg = '';
			for(Integer i = 0; i< shares.size(); i++){
				if(insrList != null && !insrList.get(i).isSuccess()){
					Database.Error err = insrList.get(i).getErrors().get(0);
					//errList.add('[Id: ' + shares.get(i).Id + ', エラー: ' +err.getmessage() + ']\n');
					errMsg += '[' + shares.get(i).get('parentId') + '/' +err.getmessage() + ']\n';
				}				
			}
			if(String.isNotBlank(errMsg)){
				throw new ShareBaseException(errMsg);
			}
		}
		return shares;
	}

	/**
	 * Shareロジック
	 * 支社コードに共有を設定
	 */
	private void createSharesBizLogic(Set<SObject> records){
		//支社コードリスト
		Set<String> brs = new Set<String>();

		// レコードの件数分、処理を行う
		for(SObject parentRec : records){
			//代理店挙績情報の支社コードから対象の公開グループセット
			brs.add('BR' + (String)parentRec.get('BrCode__c'));
		}

		// 支社コード毎の公開グループより、GroupIdを取得
		Map<String, Group> grMap = getGroupIds(brs);

		for(SObject parentRec : records){
			//ShareオブジェクトのAPI名取得		prefixが変更された場合のみDescribeを呼び出す。
			if(lastRecPrefix == null || lastRecPrefix != String.valueOf(parentRec.Id).subString(0,3)){
				sObjName = parentRec.Id.getSObjectType().getDescribe().getName();
				shareObjName = sObjName.replace('__c','__share');
				shareObjType = Schema.getGlobalDescribe().get(shareObjName);
				lastRecPrefix = String.valueOf(parentRec.Id).subString(0,3);
			}
			createShares(parentRec, grMap);
		}
	}
	/**
	 * Shareの作成 
	 */
	private void createShares(SObject parentRec, Map<String, Group> grMap){
		String brCode = (String)parentRec.get('brCode__c');

		//BR　公開グループ設定
		if(String.isNotBlank(brCode)){
			Group grBR = grMap.get('BR'+brCode);
			if(grBR != null){
				shares.add(createShare(shareObjType, parentRec.Id, grBR.Id));
			}	
		}
	}

	/**
	 * 支社コードの公開グループより、GroupIdを取得
	 */
	private Map<String, Group> getGroupIds(Set<String> brs){

		// 支社コード毎の公開グループを設定（BR）
		Set<String> groupNames = new Set<String>(brs);
		// Groupの取得
		Map<String, Group> grMap = getGroupMap(groupNames);
		
		return grMap;
	}
	
}