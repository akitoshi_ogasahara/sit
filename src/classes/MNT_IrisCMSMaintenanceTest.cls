@isTest
private with sharing class MNT_IrisCMSMaintenanceTest{
		private static testMethod void testPageMethods() {	
			MNT_IrisCMSMaintenance page = new MNT_IrisCMSMaintenance(new ApexPages.StandardController(new I_ContentMaster__c()));	
			page.getOperatorOptions_I_ContentMaster_c_ParentContent_c();	
			page.getOperatorOptions_I_ContentMaster_c_Page_c();	
			page.getOperatorOptions_I_ContentMaster_c_Name();	
			page.getOperatorOptions_I_ContentMaster_c_SelectFrom_c();	
			page.getOperatorOptions_I_ContentMaster_c_Category_c();	
			System.assert(true);
		}	
			
	private static testMethod void testComponent3() {
		MNT_IrisCMSMaintenance.Component3 Component3 = new MNT_IrisCMSMaintenance.Component3(new List<I_ContentMaster__c>(), new List<MNT_IrisCMSMaintenance.Component3Item>(), new List<I_ContentMaster__c>(), null);
		Component3.create(new I_ContentMaster__c());
		Component3.doDeleteSelectedItems();
		System.assert(true);
	}
	
}