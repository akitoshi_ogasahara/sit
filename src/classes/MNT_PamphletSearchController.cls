global with sharing class MNT_PamphletSearchController extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public I_ContentMaster__c record{get;set;}
	public MNT_PamphletSearchExtender getExtender() {return (MNT_PamphletSearchExtender)extender;}
	public PamphletResultTable PamphletResultTable {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component46_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component46_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component48_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component48_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}
	public SkyEditor2.TextHolder Component10_op{get;set;}
	public I_ContentMaster__c parentCMSId_val {get;set;}
	public SkyEditor2.TextHolder parentCMSId_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component45_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component45_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component45_op{get;set;}
	public List<SelectOption> valueOptions_I_ContentMaster_c_PamphletCategory_c {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component51_val {get;set;}
	public SkyEditor2.TextHolder Component51_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component53_val {get;set;}
	public SkyEditor2.TextHolder Component53_op{get;set;}
	public String recordTypeRecordsJSON_I_ContentMaster_c {get; private set;}
	public String defaultRecordTypeId_I_ContentMaster_c {get; private set;}
	public String metadataJSON_I_ContentMaster_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public MNT_PamphletSearchController(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.PamphletCategory__c;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.LastModifiedDate;
		f = I_ContentMaster__c.fields.CreatedDepartment__c;
		f = I_ContentMaster__c.fields.Jurisdiction__c;
		f = I_ContentMaster__c.fields.Reason__c;
 f = I_ContentMaster__c.fields.DisplayFrom__c;
 f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.PamphletOrderBy__c;
		f = I_ContentMaster__c.fields.DisplayOrder__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = I_ContentMaster__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component46_from = new SkyEditor2__SkyEditorDummy__c();
			Component46_to = new SkyEditor2__SkyEditorDummy__c();
			Component48_from = new SkyEditor2__SkyEditorDummy__c();
			Component48_to = new SkyEditor2__SkyEditorDummy__c();
			I_ContentMaster__c lookupObjComponent35 = new I_ContentMaster__c();
			Component10_val = new SkyEditor2__SkyEditorDummy__c();
			Component10_op = new SkyEditor2.TextHolder('co');
			parentCMSId_val = lookupObjComponent35;
			parentCMSId_op = new SkyEditor2.TextHolder('eq');
			Component45_val = new SkyEditor2__SkyEditorDummy__c();
			Component45_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component45_op = new SkyEditor2.TextHolder('inc');
			valueOptions_I_ContentMaster_c_PamphletCategory_c = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : I_ContentMaster__c.PamphletCategory__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_I_ContentMaster_c_PamphletCategory_c.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component51_val = new SkyEditor2__SkyEditorDummy__c();
			Component51_op = new SkyEditor2.TextHolder('co');
			Component53_val = new SkyEditor2__SkyEditorDummy__c();
			Component53_op = new SkyEditor2.TextHolder('co');
			queryMap.put(
				'PamphletResultTable',
				new SkyEditor2.Query('I_ContentMaster__c')
					.addFieldAsOutput('Name')
					.addFieldAsOutput('ParentContent__c')
					.addField('FormNo__c')
					.addField('ApprovalNo__c')
					.addField('DisplayFrom__c')
					.addField('ValidTo__c')
					.addFieldAsOutput('LastModifiedDate')
					.addFieldAsOutput('CreatedDepartment__c')
					.addFieldAsOutput('Jurisdiction__c')
					.addFieldAsOutput('Reason__c')
					.limitRecords(500)
					.addListener(new SkyEditor2.QueryWhereRegister(Component46_from, 'SkyEditor2__Date__c', 'DisplayFrom__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component46_to, 'SkyEditor2__Date__c', 'DisplayFrom__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component48_from, 'SkyEditor2__Date__c', 'ValidTo__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component48_to, 'SkyEditor2__Date__c', 'ValidTo__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'Name', Component10_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(parentCMSId_val, 'ParentContent__c', 'ParentContent__c', parentCMSId_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component45_val_dummy, 'SkyEditor2__Text__c','PamphletCategory__c', Component45_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component51_val, 'SkyEditor2__Text__c', 'FormNo__c', Component51_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component53_val, 'SkyEditor2__Text__c', 'ApprovalNo__c', Component53_op, true, 0, true ))
					.addWhere(' ( ParentContent__r.Name = \'パンフレット/特に重要なお知らせ\' OR ParentContent__r.Name = \'ご契約のしおり・約款\' OR ParentContent__r.Name = \'ニード喚起資料等\')')
.addSort('ParentContent__c',True,True).addSort('PamphletOrderBy__c',True,True).addSort('DisplayOrder__c',True,False).addSort('DisplayFrom__c',False,True)
				);
			PamphletResultTable = new PamphletResultTable(new List<I_ContentMaster__c>(), new List<PamphletResultTableItem>(), new List<I_ContentMaster__c>(), null);
			listItemHolders.put('PamphletResultTable', PamphletResultTable);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_ContentMaster__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_PamphletSearchExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			PamphletResultTable.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Name() { 
		return getOperatorOptions('I_ContentMaster__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_ParentContent_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'ParentContent__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_PamphletCategory_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'PamphletCategory__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_FormNo_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'FormNo__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_ApprovalNo_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'ApprovalNo__c');
	}
	global with sharing class PamphletResultTableItem extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		PamphletResultTableItem(PamphletResultTable holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class PamphletResultTable extends SkyEditor2.ListItemHolder {
		public List<PamphletResultTableItem> items{get; private set;}
		@TestVisible
			PamphletResultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<PamphletResultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new PamphletResultTableItem(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public I_ContentMaster__c PamphletResultTable_table_Conversion { get { return new I_ContentMaster__c();}}
	
	public String PamphletResultTable_table_selectval { get; set; }
	
	
}