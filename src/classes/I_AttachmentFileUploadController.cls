public with sharing class I_AttachmentFileUploadController extends I_AbstractController{
	public sObject record {get; set;}
	public Boolean isSingle {get; set;}
	public String targetFileId {get; set;}
	public String pageType{get; set;}
	public E_PageMessagesHolder pageMessages {get;set;}

	//ポップアップ用
	public Id modalAttId {get;set;}
	public String modalAttName {get;set;}

	// IE9 以下用
	public transient Blob fileBody {get; set;}
	public String fileName {get; set;}
	public String contentType {get; set;}
	public Integer fileSize {get; set;}

	//インターネット用　代理店ユーザ　ページURL
	public String getUrl_via_Internet(){
		return E_Util.getDomainNNlinkage();
	}

	//共同GW用　代理店ユーザ　ページURL
	public String getUrl_via_GW(){
		return E_Util.getURLForGWTB(getUrl_via_Internet());
	}

	/*
	 * コンストラクタ
	 *
	 */
	public I_AttachmentFileUploadController() {
		system.debug('debug:コンストラクタ:record=' + record);
		//pageMessages = getPageMessages();
	}

	public List<AttachmentRec> getAttachments() {
		system.debug('debug:getAttachments:record=' + record);
		system.debug('debug:pageType=' + pageType);

		List<AttachmentRec> lists = new List<AttachmentRec>();
		for (Attachment record : E_AttachmentDao.getRecsByParentAndDescription(record.Id, pageType)) {
			lists.add(new AttachmentRec(record));
		}

		return lists;
	}

	/*
	 * reload
	 *
	 */
	public PageReference reload() {
		system.debug('debug:reload');
		return null;
	}

	/*
	 * ファイル削除
	 *
	 */
	public PageReference deleteFile() {
		system.debug('debug:targetFileId='+ targetFileId);

		try {
			Attachment record = new Attachment();
			//record.id = id.valueOf(targetFileId);
			record.id = id.valueOf(modalAttId);
			E_GenericDaoWithOutSharing.deleteRecord(record);
		} catch (Exception e) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
		}
		pageMessages = getPageMessages();
		return null;
	}

	/*
	 * ファイルアップロード 旧IE
	 *
	 */
	public PageReference fileUploadOldIE() {
		system.debug('debug:fileUploadOldIE');
		Savepoint sp = Database.setSavepoint();

		try {
			if(!Apexpages.hasMessages(ApexPages.severity.ERROR)) {
				system.debug('debug: fileName=' + fileName);
				system.debug('debug: contentType=' + contentType);

				// 単一アップロード有効時は Delete / Insert
				if (isSingle) {
					List<Attachment> records = E_AttachmentDao.getRecsByParentAndDescription(record.Id, pageType);
					E_GenericDaoWithOutSharing.deleteRecord(records);
				}
		  
				Attachment att = new Attachment();
				att.Name = fileName;
				att.Body = fileBody;
				att.ContentType = contentType;
				att.ParentId = record.Id;
				att.Description = pageType;
				insert att;

				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, fileName + ' アップロード完了'));
				setProcessedMessage();

			}
		} catch (Exception e) {
			system.debug('error:'+e);
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
			Database.rollback(sp);
		}
		return null;
	}

	@RemoteAction
	public static FileUploadResponse fileDeleteById(String attachmentId) {
		system.debug('debug:attachmentId=' + attachmentId);
		FileUploadResponse result = new FileUploadResponse();
		Savepoint sp = Database.setSavepoint();

		try {
			Attachment record = new Attachment();
			record.id = id.valueOf(attachmentId);
			delete record;

		} catch (Exception e) {
			system.debug('error : ' + e);
			result.errorMessage = e.getMessage();
			Database.rollback(sp);
		}
		return result;
	}


	@RemoteAction
	public static FileUploadResponse fileDelete(FileUploadRequest request) {
		system.debug('debug:request=' + request);
		FileUploadResponse result = new FileUploadResponse();
		Savepoint sp = Database.setSavepoint();
		
		try {
			List<Attachment> records = E_AttachmentDao.getRecsByNotIdAndDescription(request.fileId, request.parentId, request.pageType);
			E_GenericDaoWithOutSharing.deleteRecord(records);
		} catch (Exception e) {
			system.debug('error : ' + e);
			result.errorMessage = e.getMessage();
			Database.rollback(sp);
		}
		return result;
	}

	public class FileUploadRequest {
		public String parentId {get; set;}
		public String fileId {get; set;}
		public Boolean isSingle {get; set;}
		public String pageType{get; set;}

		public FileUploadRequest() {
			this.parentId = '';
			this.fileId = '';
			this.isSingle = false;
			this.pageType = '';
		}
	}
	
	/*
	 * response
	 */
	public class FileUploadResponse {
		public String recordId {get; set;}
		public String errorMessage {get; set;}

		public FileUploadResponse() {
			this.recordId = '';
			this.errorMessage = '';
		}
	}

	/*
	 * 表示用
	 */
	public class AttachmentRec {
		public Attachment record {get; private set;}
		public String fileSize {get; private set;}
		public String fileName {get; private set;}

		public AttachmentRec(Attachment record) {
			this.record = record;

			if(record.Name.length() < 26){
				this.fileName = record.Name;
			}else{
				String fname = record.Name.substring(0, 25);
				this.fileName = fname + '...';
			}

			if (record.BodyLength < 1024) {
				this.fileSize = String.valueOf(record.BodyLength) + ' Byte';
			} else if (record.BodyLength < 1048576) {
				Decimal size = Decimal.valueOf(record.BodyLength).divide(1024, 2);
				 this.fileSize = String.valueOf(size) + ' KB';
			} else {
				Decimal size = Decimal.valueOf(record.BodyLength).divide(1048576, 2);
				this.fileSize = String.valueOf(size) + ' MB';
			}
		}
	}

	public PageReference setProcessedMessage(){
		pageMessages = getPageMessages();
		// 既にメッセージが登録されていない場合に、メッセージセット
		if(!pageMessages.hasMessages()){
			pageMessages.addErrorMessage('ファイルのアップロードが完了しました。');
		}
		System.debug(pageMessages);
		return null;
	}

	public void createModalData(){
		modalAttId = System.currentPageReference().getParameters().get('modalAttId');
		modalAttName = System.currentPageReference().getParameters().get('modalAttName');
	}
}