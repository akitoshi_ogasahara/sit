public with sharing class MD_DoctorTriggerHandler {
    private MD_DoctorTriggerBizLogic bizLogic;

    /** Constructor */
    public MD_DoctorTriggerHandler() {
        bizLogic = new MD_DoctorTriggerBizLogic();
    }

    /** before insert */
    public void onBeforeInsert(List<MD_Doctor__c> newList, Map<Id, MD_Doctor__c> oldMap) {
        // 検索用テキストの設定（カタカナ、英語、数字、記号を全角から半角へ変換
        bizLogic.createSearchTextEmToEn(newList);
    }

    /** before update */
    public void onBeforeUpdate(List<MD_Doctor__c> newList, Map<Id, MD_Doctor__c> oldMap) {
        // 検索用テキストの設定（カタカナ、英語、数字、記号を全角から半角へ変換
        bizLogic.createSearchTextEmToEn(newList);
        
        // 住所情報が変更された場合、緯度経度をブランク更新する
        bizLogic.updateLocationBlank(newList, oldMap);
    }
}