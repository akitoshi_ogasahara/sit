/**
 * 
 */
@isTest
private class TestCR_BizBooksExtender {

	/**
	 * Constructor
	 */
	static testMethod void test_CR_BizBooksExtender01() {
		CR_BizBooksController controller;
		CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
		
		// assert
		System.assertEquals(extender.helpMenukey, CR_Const.MENU_HELP_Books);
	}
	
	/**
	 * init
	 * 社員ユーザ / 正常
	 */
	static testMethod void test_init01(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		system.runAs(usr){
			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				
				// assert確認用
				DescribeFieldResult f = SObjectType.CR_Agency__c.fields.ReportType__c;
				List<PicklistEntry> entries = f.getPicklistValues();
				
				Date d = Date.today();
				DateTime dt = DateTime.newInstance(d.year(), d.month(),d.day());

				// assert
				System.assertEquals(extender.isSuccessOfInitValidate, true);
				System.assertEquals(extender.outputRecordYm, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT));
				
			Test.stopTest();
		}
	}
	
	/**
	 * init
	 * 社員ユーザ / エラー：レコードが存在しない
	 */
	static testMethod void test_init02(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		system.runAs(usr){
			Test.startTest();
				// データ作成
				CR_Agency__c agny = new CR_Agency__c();
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				
				// assert
				System.assertEquals(extender.isSuccessOfInitValidate, false);
				
			Test.stopTest();
		}
	}

	/**
	 * init
	 * 社員ユーザ / エラー：代理店コードがNULL
	 */
	static testMethod void test_init03(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		system.runAs(usr){
			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店コードをNULLで更新
				acc.E_CL1PF_ZHEADAY__c = null;
				update acc;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				
				// assert
				System.assertEquals(extender.isSuccessOfInitValidate, false);
				
			Test.stopTest();
		}
	}
	
	/**
	 * init
	 * 社員ユーザ / エラー：権限なし
	 */
	static testMethod void test_init04(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createPermissions(usr.Id);
		
		system.runAs(usr){
			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				
				// assert
				System.assertEquals(extender.isSuccessOfInitValidate, false);
				
			Test.stopTest();
		}
	}
	
	/**
	 * List<E_DownloadHistorry__c> getHistoryList()
	 * 社員ユーザ / 正常
	 */
	static testMethod void test_getHistoryList01(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		system.runAs(usr){
			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
				// ダウンロード履歴の登録(事業x1、帳簿x2)
				List<E_DownloadHistorry__c> dhList = TestCR_TestUtil.createDlHistoryList(agny.Id);
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				List<E_DownloadHistorry__c> resultList = extender.getHistoryList();

				// assert
				System.assertEquals(2, resultList.size());
				
			Test.stopTest();
		}
	}
	
	/**
	 * public List<SelectOption> getOfficeOptions()
	 */
	static testMethod void test_getOfficeOptions01(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		system.runAs(usr){
			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, '99');
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				List<SelectOption> resultList = extender.getOfficeOptions();

				// assert
				System.assertEquals(1, resultList.size());
				
			Test.stopTest();
		}
	}	

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / 正常
	 */
	static testMethod void test_doRequestCreateData01(){
		String officeCode = '12345';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);

		//Group
		Group grp = new Group(Name = 'L2' + officeCode);
		insert grp;

		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;

			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.LastRequestBook__c = Datetime.now();
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();
				
				extender.outputRecordYm = '2016/09';
				extender.selOfficeList = new List<String>();
				extender.selOfficeList.add(office.E_CL2PF_ZAGCYNUM__c);
				
				PageReference resultPref = extender.doRequestCreateData();

				// assert
				CR_Agency__c resultRec = [Select Id, LastRequestBook__c From CR_Agency__c Where Id = :controller.record.Id];
				System.assert(resultRec.LastRequestBook__c != agny.LastRequestBook__c);
				
				List<E_DownloadHistorry__c> resultDhList = [Select Id From E_DownloadHistorry__c Where CR_Agency__c = :agny.Id];
				System.assert(resultDhList.size() == 1);
				
				PageReference assertPref = Page.E_CRBizBooks;
				assertPref.getParameters().put('id', agny.Id);
				System.assertEquals(resultPref.getUrl(), assertPref.getUrl());
				
				/*
				List<E_DownloadHistorry__Share> sl = [Select ParentId, UserOrGroupId From E_DownloadHistorry__Share];
				Set<Id> ids = new Set<Id>();
				for(E_DownloadHistorry__Share s : sl){
					String str = (String)s.UserOrGroupId;
					if(str.substring(0,3) == '00G'){
						ids.add(s.UserOrGroupId);
					}
				}
				system.debug([select Name From Group Where Id in :ids]);
				*/
				
			Test.stopTest();
		}
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：出力基準年月
	 */
	static testMethod void test_doRequestCreateData02(){
		String officeCode = '12345';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		//Group
		Group grp = new Group(Name = 'L2' + officeCode);
		insert grp;
		
		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;

			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.LastRequestBook__c = Datetime.now();
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();

				extender.selOfficeList = new List<String>();
				extender.selOfficeList.add(office.E_CL2PF_ZAGCYNUM__c);
				
				PageReference resultPref;
				
				// 出力基準年月に空白をセット
				extender.outputRecordYm = null;
				
				// 実行
				resultPref = extender.doRequestCreateData();
				// assert
				System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG01_PH2_FRAUD_OUTPUTYM);
				extender.pageMessages.clearMessages();
				
				// 出力基準年月に文字列をセット
				extender.outputRecordYm = 'テスト';
				// 実行
				resultPref = extender.doRequestCreateData();
				// assert
				System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG01_PH2_FRAUD_OUTPUTYM);
				extender.pageMessages.clearMessages();
							
			Test.stopTest();
		}
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：期間
	 */
	static testMethod void test_doRequestCreateData03(){
		String officeCode = '12345';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);

		//Group
		Group grp = new Group(Name = 'L2' + officeCode);
		insert grp;
		
		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;

			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.LastRequestBook__c = Datetime.now();
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();

				extender.selOfficeList = new List<String>();
				extender.selOfficeList.add(office.E_CL2PF_ZAGCYNUM__c);
				
				// 出力基準年月に期限開始前をセット
				extender.outputRecordYm = TestCR_TestUtil.getOutputYmLast(CR_Const.TYPE_BIZBOOKS);
				
				// 実行
				PageReference resultPref = extender.doRequestCreateData();
				
				// assert
				System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG07_PH2_FROM_OUTPUTYM);
				extender.pageMessages.clearMessages();
				
			Test.stopTest();
		}
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：同条件
	 */
	static testMethod void test_doRequestCreateData04(){
		String officeCode = '12345';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);

		//Group
		Group grp = new Group(Name = 'L2' + officeCode);
		insert grp;
		
		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;

			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.LastRequestBook__c = Datetime.now();
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();

				extender.selOfficeList = new List<String>();
				extender.selOfficeList.add(office.E_CL2PF_ZAGCYNUM__c);
				
				PageReference resultFirstPref = extender.doRequestCreateData();
				
				PageReference resultSecondPref = extender.doRequestCreateData();

				// assert
				System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG02_PH2_SAME_CONDITION);
				
			Test.stopTest();
		}
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：回数
		 */
	static testMethod void test_doRequestCreateData05(){
		String officeCode = '12345';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);

		//Group
		Group grp = new Group(Name = 'L2' + officeCode);
		insert grp;
		
		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;

			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.LastRequestBook__c = Datetime.now();
				update agny;
				
				// ダウンロード履歴登録
				Date d = Date.today();
				DateTime dt = DateTime.newInstance(d.year(), d.month(),d.day());
				List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
				dhList.add(TestCR_TestUtil.createDlHistory(agny.Id, CR_Const.TYPE_BIZBooks, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT), 'condition1'));
				dhList.add(TestCR_TestUtil.createDlHistory(agny.Id, CR_Const.TYPE_BIZBooks, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT), 'condition2'));
				dhList.add(TestCR_TestUtil.createDlHistory(agny.Id, CR_Const.TYPE_BIZBooks, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT), 'condition3'));
				insert dhList;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();

				extender.selOfficeList = new List<String>();
				extender.selOfficeList.add(office.E_CL2PF_ZAGCYNUM__c);

				PageReference resultPref = extender.doRequestCreateData();

				// assert
				System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG03_PH2_LIMIT_REQUEST);
				
			Test.stopTest();
		}
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：事務所未選択
	 */
	static testMethod void test_doRequestCreateData06(){
		String officeCode = '12345';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		//Group
		Group grp = new Group(Name = 'L2' + officeCode);
		insert grp;
		
		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = E_Const.USER_ACCESS_KIND_BRALL;
			insert eidc;

			Test.startTest();
				// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.LastRequestBook__c = Datetime.now();
				update agny;
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();

				extender.selOfficeList = new List<String>();

				PageReference resultFirstPref = extender.doRequestCreateData();
				
				PageReference resultSecondPref = extender.doRequestCreateData();

				// assert
				System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG06_PH2_REQUIRED_OFFICES);
				
			Test.stopTest();
		}
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / BR99  支社担当MR
	 */
	static testMethod void test_doRequestCreateDataShare(){
		String officeCode = '12345';
		String branchCode = 'BR99';
		
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		//Group
		List<Group> grps = new List<Group>();
		Group grpL2 = new Group(Name = 'L2' + officeCode);
		grps.add(grpL2);
		Group grpBR = new Group(Name = branchCode);
		grps.add(grpBR);
		insert grps;

		GroupMember gm = new GroupMember(groupId = grpBR.Id, UserOrGroupId = usr.Id);
		insert gm;
		
		CR_Agency__c agny;
		system.runAs(usr){
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, usr.Id);
			eidc.ZINQUIRR__c = branchCode;
			insert eidc;
		
			// データ作成
			Account acc = TestCR_TestUtil.createAccount(true);
			Account office = TestCR_TestUtil.createOffice(true, acc.Id, officeCode);
			agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
			// 代理店提出の「大規模代理店」をTrueで更新
			agny.LargeAgency__c = true;
			agny.LastRequestBook__c = Datetime.now();
			update agny;
	
			Test.startTest();
				
				PageReference pref = Page.E_CRBizBooks;
				Test.setCurrentPage(pref);
				
				Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizBooksController controller = new CR_BizBooksController(standardcontroller);
				CR_BizBooksExtender extender = new CR_BizBooksExtender(controller);
				extender.init();
				extender.getOfficeOptions();
				
				extender.outputRecordYm = '2016/09';
				extender.selOfficeList = new List<String>();
				extender.selOfficeList.add(office.E_CL2PF_ZAGCYNUM__c);
				
				PageReference resultPref = extender.doRequestCreateData();

				// assert
				CR_Agency__c resultRec = [Select Id, LastRequestBook__c From CR_Agency__c Where Id = :controller.record.Id];
				System.assert(resultRec.LastRequestBook__c != agny.LastRequestBook__c);
			Test.stopTest();
		}

		List<E_DownloadHistorry__c> resultDhList = [Select Id 
														,(select id,UserOrGroupId,UserOrGroup.Name from Shares)
													From E_DownloadHistorry__c 
													Where CR_Agency__c = :agny.Id];
		System.assert(resultDhList.size() == 1);
		Set<String> groupNames = new Set<String>();
		for(E_DownloadHistorry__share share:resultDhList[0].shares){
			groupNames.add(share.userOrGroup.Name);
		}
		
		//BR99が含まれている
		System.assert(groupNames.contains(branchCode));
	}

}