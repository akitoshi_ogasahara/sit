public with sharing class I_PushOutBatchOperation_Notice extends I_AbstractPushOutBatchOperation{
    
    /**
     * Constructor
     */
    public I_PushOutBatchOperation_Notice(){
		System.debug('*** I_PushOutBatchOperation_Notice ***');
    }
    
    /**
     * setQueryCondition
     */
    public override void setQueryCondition(){
    	String convertedDate = ebizLog.CreatedDate.addDays(-1).format('yyyy-MM-dd\'T\'hh:mm:ss.SSS\'Z\'');
    	queryCondition = ' Where ';
    	queryCondition += ' CreatedDate > ' + convertedDate;
    	system.debug(queryCondition);
    }
    
    /**
     * execute
     * 代理店ユーザのレコードのみ登録
     * 社員ユーザはfinishにて登録
     */
    public override List<I_PushOut__c> execute(List<Sobject> records){
    	System.debug('*** I_PushOutBatchOperation_Notice execute ***');
    	List<I_PushOut__c> pushRecs = new List<I_PushOut__c>();
    	List<I_PushOut__c> mergeRecs = new List<I_PushOut__c>();
    	
		// 代理店ユーザ
		for(E_IDCPF__c agentIdcpf : getAgentIdcpfs(records)){
			// ID管理よりPushOut通知作成
			pushRecs.add(createRecordByIdcpf(agentIdcpf, pType, false, 'yyyyMMdd'));
		}
		System.debug('*[代理店ユーザのPushRecs.size]' + pushRecs.size());
		
		// UpsertKeyでレコードマージ
		mergeRecs = mergeRecords(pushRecs);

		// PushOut通知レコード登録
		upsert mergeRecs UpsertKey__c;
		
		return mergeRecs;
    }
    
    /**
     * finish
     */
    public override void finish(List<I_PushOut__c> records){
    	List<I_PushOut__c> empRecs = new List<I_PushOut__c>();
    	List<I_PushOut__c> mergeRecs = new List<I_PushOut__c>();
    	
    	// 社員ユーザのPushOut通知レコード登録
    	empRecs = getEmployeePushOuts();
		if(empRecs.size() > 0){
			// UpsertKeyでレコードマージ
			mergeRecs = mergeRecords(empRecs);
			upsert mergeRecs UpsertKey__c;
		}
    }
    
    /**
     * 社内ユーザのPushOut通知レコード取得
     */
    private List<I_PushOut__c> getEmployeePushOuts(){
    	List<I_PushOut__c> empRecs = new List<I_PushOut__c>();
    	
		// 社内ユーザ
		for(E_IDCPF__c empIdcpf : getEmployeeIdcpfs()){
			// ID管理よりPushOut通知作成
			empRecs.add(createRecordByIdcpf(empIdcpf, pType, true, 'yyyyMMdd'));
		}
		
		return empRecs;
    }
    
    /**
     * 送信ユーザの取得
     */
	private Set<E_IDCPF__c> getAgentIdcpfs(List<SObject> records){
		System.debug('*** I_PushOutBatchOperation_Notice getAgentIdcpfs ***');
		Set<E_IDCPF__c> agentIdcpfs = new Set<E_IDCPF__c>();
		Set<String> conIds = new Set<String>();
		Set<String> groupCodes = new Set<String>();
		
		// 代理店ユーザ(レコードから取得)
		for(SObject rec : records){
			// レコード値
			Boolean isZ1Offing = (Boolean)rec.get('PolicyZ1OFFING__c');		// 一事務所登録フラグ
			Boolean isZAutoTrf = (Boolean)rec.get('PolicyZAUTOTRF__c');		// 自動契約移管判定フラグ
			String conId = (String)rec.get('PolicyContactId__c');			// 募集人ID
			String accountCode = (String)rec.get('PolicyAccountCode__c');	// 代理店格コード
			String officeCode = (String)rec.get('PolicyOfficeCode__c');		// 事務所コード

			// AT(自動移管、一事務所の場合は対象外)
			if(isZ1Offing == false && isZAutoTrf == false){
				if(String.isNotBlank(conId)){
					conIds.add(conId);
				}
			}
			
			// AH(L1公開グループ)
			if(String.isNotBlank(accountCode)){
				groupCodes.add('L1' + accountCode);
			}

			// AY(L2公開グループ)
			if(String.isNotBlank(officeCode)){
				groupCodes.add('L2' + officeCode);
			}
		}
		
		// 募集人ユーザの追加
		agentIdcpfs.addAll(getAgentIdcpfs(conIds));
		
		// L1、L2公開グループユーザの追加
		agentIdcpfs.addAll(getGroupIdcpfs(groupCodes));
		
		return agentIdcpfs;
	}
}