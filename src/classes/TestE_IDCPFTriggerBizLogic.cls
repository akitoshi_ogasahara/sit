/**
			E_IDCPFTriggerBizLogicとE_IDCPFUpdateByUserTriggerのテストクラス
*/

@isTest
private class TestE_IDCPFTriggerBizLogic {

	private static User testuser;
	private static E_IDCPF__c idcpf;
	private static Account account;
	private static Contact contact;
	private static RecordType recordType;

	private static testMethod void testE_IDCPFTriggerBizLogicSet() {
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){

			Test.startTest();

			//ID管理
			idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '0';
			idcpf.FLAG02__c = '0';
			idcpf.IsMaintenanceCharge__c = true;
			idcpf.IsCrApprove__c = true;
			idcpf.IsCrDpa__c = true;
			idcpf.IsScDirector__c = true;
			idcpf.IsScAdmin__c = true;
			idcpf.IsShowSREmployee__c = true;
			insert idcpf;
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			SET<ID> idcpfIds = new SET<ID>{idcpf.id};
			LIST<E_IDCPF__c> idcpfList = E_IDCPFDao.getActiveUsersRecsByIDs(idcpfIds);
			bizLogic.addTargetUsers(idcpfList);
			bizLogic.PermissionSetAssign();
		}

	}
	private static testMethod void testE_IDCPFTriggerBizLogicSet2() {
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){

			Test.startTest();

			//ID管理
			idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '0';
			idcpf.FLAG02__c = '0';
			//投信照会フラグ
			idcpf.FLAG03__c = E_Const.IS_INQUIRY_1;
			//手続の履歴
			idcpf.FLAG06__c =  E_Const.IS_INQUIRY_1;
			//手数料明細書照会フラグ
			idcpf.ZDSPFLAG01__c =  E_Const.IS_INQUIRY_1;
			//メンテナンス        連携されない項目で判定
			idcpf.IsMaintenanceCharge__c = true;
			insert idcpf;
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			SET<ID> idcpfIds = new SET<ID>{idcpf.id};
			LIST<E_IDCPF__c> idcpfList = E_IDCPFDao.getActiveUsersRecsByIDs(idcpfIds);
			bizLogic.addTargetUsers(idcpfList);
			bizLogic.PermissionSetAssign();
		}

	}
	private static testMethod void testE_IDCPFTriggerBizLogicSet3() {
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		User partner = createTestUser();

		System.runAs(u){

			Test.startTest();

			//ID管理
			idcpf = TestE_TestUtil.createIDCPF(false,partner.Id);
			idcpf.FLAG01__c = '0';
			idcpf.FLAG02__c = '0';
			idcpf.IsMaintenanceCharge__c = true;
			idcpf.IsCrApprove__c = true;
			idcpf.IsCrDpa__c = true;
			idcpf.IsScDirector__c = true;
			idcpf.IsScAdmin__c = true;
			idcpf.IsShowSREmployee__c = true;
			idcpf.ZSTATUS01__c =  CR_Const.PW_STATUS_AGENCY;
			idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
			insert idcpf;
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			SET<ID> idcpfIds = new SET<ID>{idcpf.id};
			LIST<E_IDCPF__c> idcpfList = E_IDCPFDao.getActiveUsersRecsByIDs(idcpfIds);
			bizLogic.addTargetUsers(idcpfList);
			bizLogic.PermissionSetAssign();
		}

	}
	private static testMethod void testE_IDCPFTriggerBizLogicDel() {
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){

			Test.startTest();

			//ID管理
			idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '0';
			idcpf.FLAG02__c = '0';
			insert idcpf;
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			SET<ID> idcpfIds = new SET<ID>{idcpf.id};
			LIST<E_IDCPF__c> idcpfList = E_IDCPFDao.getActiveUsersRecsByIDs(idcpfIds);
		   system.debug(idcpflist);
			bizLogic.addTargetUsers(idcpfList);
			bizLogic.PermissionSetAssign();
			bizLogic.PermissionSetDelete(idcpfList);
		}

	}

/*      非同期処理のAssertionがテストメソッドでエラーとなるためコメントアウト
	//E_IDCPF削除時テスト
	private static testMethod void testE_IDCPF_Delete() {
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		insert idcpf;

		Test.startTest();

		System.runAs(u){
			u = E_UserDao.getUserWithPermissions(new Set<Id>{u.Id}).get(u.Id);
			System.assert(u.PermissionSetAssignments.size()>0);
			delete idcpf;
		}
		Test.stopTest();

		u = E_UserDao.getUserWithPermissions(new Set<Id>{u.Id}).get(u.Id);
		System.assert(u.PermissionSetAssignments.size()==0);
	}
*/
	private static testMethod void testE_IDCPFTriggerBizLogicDMLerror() {
		PermissionSet ps;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		User u2 = TestE_TestUtil.createUser(true, 'keiyakusha2', 'システム管理者');
		E_bizDataSyncLog__c EBIZlog = new E_bizDataSyncLog__c(Kind__c = '1');
		insert EBizlog;

		System.runAs(u){

			Test.startTest();

			//ID管理
			idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '0';
			idcpf.FLAG02__c = '0';
			insert idcpf;
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			SET<ID> idcpfIds = new SET<ID>{idcpf.id};
			LIST<E_IDCPF__c> idcpfList = E_IDCPFDao.getActiveUsersRecsByIDs(idcpfIds);
		   system.debug(idcpflist);
			bizLogic.addTargetUsers(idcpfList);
			ps = [Select Id,Name From PermissionSet Where Name =:E_Const.PERM_Employee];
			bizLogic.PermissionSetAssign();
			ps.name = 'test';
			String ermsg;
			try{
				//update ps;
			}catch (Exception e){
				ermsg = e.getMessage();
			}
			test.stopTest();
		}
	}
	/**
	 *      ユーザオブジェクトの更新によって権限セットを付与するパターン
	 */
	private static testMethod void testE_IDCPFUpdateByUserTrigger() {
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		insert idcpf;

		System.runAs(u){
			Test.startTest();
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizLogic.addTargetUsers(E_IDCPFDao.getActiveUsersRecsByIDs(new Set<Id>{idcpf.Id}));
			bizLogic.PermissionSetAssign();
			User usr = [Select Name, Id
								, (Select Id, PermissionSetId From PermissionSetAssignments)
						From User
						WHERE Id = :u.Id];

			//権限セットが適切に付与されていることを確認
			System.assertEquals(6, usr.PermissionSetAssignments.size());    //プロファイル＋2つの権限セット
			Set<Id> permIds = new Set<Id>();
			for(PermissionSetAssignment ps:usr.PermissionSetAssignments){
				permIds.add(ps.PermissionSetId);
			}
			for(PermissionSet ps:[select id,name from PermissionSet WHERE Id in :permIds]){
				System.debug('■■■'+ps.Name);
			}
			Test.stopTest();
		}
   }

    /**
	 * ユーザオブジェクトの更新によって権限セットを付与するパターン
	 * 狙い：ユーザのプロファイルが「コールセンター部」のとき、権限セット「Atria管理権限（参照のみ）」が付与されること
	 */
	private static testMethod void testE_IDCPFUpdateByCallCenterUserTrigger() {
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'callcenter', 'コールセンター部');
		//ID管理
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		insert idcpf;

		System.runAs(u){
			Test.startTest();
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizLogic.addTargetUsers(E_IDCPFDao.getActiveUsersRecsByIDs(new Set<Id>{idcpf.Id}));
			bizLogic.PermissionSetAssign();
			User usr = [Select Name, Id
								, (Select Id, PermissionSetId From PermissionSetAssignments)
						From User
						WHERE Id = :u.Id];

			//権限セットが適切に付与されていることを確認
			Set<Id> permIds = new Set<Id>();
			for(PermissionSetAssignment ps:usr.PermissionSetAssignments){
				permIds.add(ps.PermissionSetId);
			}
			Boolean isExistsAtrAuthRead = false;
			for(PermissionSet ps:[select id,name from PermissionSet WHERE Id in :permIds]){
				isExistsAtrAuthRead = ps.Name == E_Const.PERM_ATRAuthRead;
				if (isExistsAtrAuthRead) break;
			}
			System.assertEquals(true, isExistsAtrAuthRead);    //権限セット「Atria管理権限（参照のみ）」が付与される

			Test.stopTest();
		}
}
  

	private static testMethod void testE_IDCPFUpdateByUserTrigger_userActivate() {
		//ユーザ
		User admin = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		//User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		User u = createTestUser();
		//ID管理
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		insert idcpf;
		//ユーザ無効化
		System.runAs(admin){
			u.isActive = false;
			update u;
		}
		E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
		bizLogic.addTargetUsers(E_IDCPFDao.getActiveUsersRecsByIDs(new Set<Id>{idcpf.Id}));
		//権限セット付与
		bizLogic.PermissionSetAssign();
		User usr = [Select Name, Id
							, (Select Id, PermissionSetId From PermissionSetAssignments)
					From User
					WHERE Id = :u.Id];
		//無効ユーザなので権限セットは付与されない
		System.assertEquals(1, usr.PermissionSetAssignments.size());    //プロファイル

		Test.startTest();
		//ユーザ有効化
		System.runAs(admin){
			u.isActive = true;
			update u;

			usr = [Select Name, Id
								, (Select Id, PermissionSetId From PermissionSetAssignments)
						From User
						WHERE Id = :u.Id];
			//System.assertEquals(5, usr.PermissionSetAssignments.size());    //プロファイル＋2つの権限セット
			System.assertEquals(3, usr.PermissionSetAssignments.size());    //プロファイル＋2つの権限セット
		}
		Test.stopTest();
   }

	private static testMethod void testupdateEIDCContact(){
		//ユーザ作成
		User admin = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User u = createTestUser2();

		//ID管理のリスト作成
		List<E_IDCPF__c> eidcList = new List<E_IDCPF__c>();

		//ID管理作成
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.User__c = null;
		idcpf.ZIDTYPE__c = 'AH';
		idcpf.Contact__c = null;
		idcpf.ZIDOWNER__c = 'AGT0000';
		insert idcpf;

		eidcList.add(idcpf);

		System.runAs(admin){
			Test.startTest();
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.updateEIDCContact(eidcList);

			String str = idcpf.ZIDOWNER__c.replaceAll('AG','');
			Contact con =[Select Id FROM Contact WHERE E_CL3PF_AGNTNUM__c =:str];

			System.assertEquals(eidcList[0].Contact__c,con.Id);
			Test.stopTest();
		}
	}

	//insert
	private static testMethod void testupdateEidcpf(){
		//ユーザ作成
		User admin = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User u = createTestUser2();

		//ID管理のリスト作成
		List<E_IDCPF__c> eidcList = new List<E_IDCPF__c>();

		//ID管理作成
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.User__c = null;
		idcpf.ZIDTYPE__c = 'AH';
		idcpf.Contact__c = null;
		idcpf.ZIDOWNER__c = 'AGT0000';
		insert idcpf;

		eidcList.add(idcpf);

		System.runAs(admin){
			Test.startTest();
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.updateEidcpf(eidcList);

			String str = idcpf.ZIDOWNER__c.replaceAll('AG','');
			List<E_IDRequest__c> idrpf = [Select Id FROM E_IDRequest__c WHERE E_IDCPF__c =:idcpf.id];

			System.assertEquals(idrpf.size(),0);
			Test.stopTest();
		}
	}

	//update
	private static testMethod void testupdateEidcpf2(){
		//ユーザ作成
		User admin = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User u = createTestUser2();

		//ID管理のリスト作成
		List<E_IDCPF__c> eidcList = new List<E_IDCPF__c>();

		//ID管理作成
		idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.User__c = null;
		idcpf.ZIDTYPE__c = 'AH';
		idcpf.Contact__c = null;
		idcpf.ZIDOWNER__c = 'AGT0000';
		idcpf.ZDSPFLAG01__c = '1';
		idcpf.ZDSPFLAG02__c = '1';
		idcpf.ZDSPFLAG03__c = '1';
		idcpf.ZDSPFLAG04__c = '1';
		idcpf.ZDSPFLAG05__c = '1';
		idcpf.ZDSPFLAG06__c = '1';
		idcpf.ZDSPFLAG07__c = '1';
		idcpf.ZDSPFLAG08__c = '1';
		idcpf.ZDSPFLAG09__c = '1';
		idcpf.ZDSPFLAG10__c = '1';
		idcpf.dataSyncDate__c = Datetime.now();
		insert idcpf;

		E_IDCPF__c oldidcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		oldidcpf.User__c = null;
		oldidcpf.ZIDTYPE__c = 'AH';
		oldidcpf.Contact__c = null;
		oldidcpf.ZIDOWNER__c = 'AGT0000';
		oldidcpf.ZDSPFLAG01__c = '1';
		oldidcpf.ZDSPFLAG02__c = '1';
		oldidcpf.ZDSPFLAG03__c = '1';
		oldidcpf.ZDSPFLAG04__c = '1';
		oldidcpf.ZDSPFLAG05__c = '1';
		oldidcpf.ZDSPFLAG06__c = '1';
		oldidcpf.ZDSPFLAG07__c = '1';
		oldidcpf.ZDSPFLAG08__c = '1';
		oldidcpf.ZDSPFLAG09__c = '1';
		oldidcpf.ZDSPFLAG10__c = '0';
		oldidcpf.dataSyncDate__c = idcpf.dataSyncDate__c;
		insert oldidcpf;
		Map<ID,E_IDCPF__c> oldMap = new Map<ID,E_IDCPF__c>();
		oldMap.put(idcpf.Id, oldidcpf);
		eidcList.add(idcpf);

		System.runAs(admin){
			Test.startTest();
			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.updateEidcpf(eidcList,oldMap);

			//String str = newidcp.ZIDOWNER__c.replaceAll('AG','');
			List<E_IDRequest__c> idrpf = [Select Id FROM E_IDRequest__c WHERE E_IDCPF__c =:idcpf.id];

			//System.assertEquals(idrpf.size(),1);
			Test.stopTest();
		}
	}

	/**
	 * class userPermission
	 * 	住生ユーザ：IRIS（募集人種別　02）
	 * 	IRIS権限セット（住友生命）のみが付与対象となること
	 */
	private static testMethod void userPermission_test01(){
		User u = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User partner = createTestUser3('E_PartnerCommunity_SumiseiIris', E_Const.ZHEADAY_SUMISEI, '02');
		E_IDCPF__c idcpf = createTestIdcpf(false, partner.Id);

		System.runAs(u){
			Test.startTest();

			Set<Id> ids = new Set<Id>{idcpf.Id};
			List<E_IDCPF__c> idcpfs = E_IDCPFDao.getActiveUsersRecsByIDs(ids);

			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.addTargetUsers(idcpfs);
			bizlogic.PermissionSetAssign();

			Test.stopTest();

			Map<String, String> resultMap = new Map<String, String>();
			Map<Id, User> resultUserPerMap = E_UserDao.getUserWithPermissions(new Set<Id>{partner.Id});
			for(Id key : resultUserPerMap.keySet()){
				User ru = resultUserPerMap.get(key);

				for(PermissionSetAssignment psa : ru.PermissionSetAssignments){
					resultMap.put(psa.PermissionSet.Name, psa.PermissionSet.Name);
				}
			}

			// assertion
			System.assertEquals(1, resultMap.size());
			System.assertEquals(true, resultMap.containsKey(E_Const.PERM_SUMISEI_IRIS));
		}
	}

	/**
	 * class userPermission
	 * 	住生ユーザ：IRIS（募集人種別　03）
	 * 	IRIS権限セット（住友生命）のみが付与対象となること
	 */
	private static testMethod void userPermission_test02(){
		User u = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User partner = createTestUser3('E_PartnerCommunity_SumiseiIris', E_Const.ZHEADAY_SUMISEI, '03');
		E_IDCPF__c idcpf = createTestIdcpf(false, partner.Id);

		System.runAs(u){
			Test.startTest();

			Set<Id> ids = new Set<Id>{idcpf.Id};
			List<E_IDCPF__c> idcpfs = E_IDCPFDao.getActiveUsersRecsByIDs(ids);

			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.addTargetUsers(idcpfs);
			bizlogic.PermissionSetAssign();

			Test.stopTest();

			Map<String, String> resultMap = new Map<String, String>();
			Map<Id, User> resultUserPerMap = E_UserDao.getUserWithPermissions(new Set<Id>{partner.Id});
			for(Id key : resultUserPerMap.keySet()){
				User ru = resultUserPerMap.get(key);

				for(PermissionSetAssignment psa : ru.PermissionSetAssignments){
					resultMap.put(psa.PermissionSet.Name, psa.PermissionSet.Name);
				}
			}

			// assertion
			System.assertEquals(1, resultMap.size());
			System.assertEquals(true, resultMap.containsKey(E_Const.PERM_SUMISEI_IRIS));
		}
	}

	/**
	 * class userPermission
	 * 	住生ユーザ：ダミー（募集人種別　09）
	 * 	IRIS権限セット（住友生命）が付与対象となっていないこと
	 */
	private static testMethod void userPermission_test03(){
		User u = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User partner = createTestUser3('E_PartnerCommunity_SumiseiIris', E_Const.ZHEADAY_SUMISEI, '09');
		E_IDCPF__c idcpf = createTestIdcpf(false, partner.Id);

		System.runAs(u){
			Test.startTest();

			Set<Id> ids = new Set<Id>{idcpf.Id};
			List<E_IDCPF__c> idcpfs = E_IDCPFDao.getActiveUsersRecsByIDs(ids);

			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.addTargetUsers(idcpfs);
			bizlogic.PermissionSetAssign();

			Test.stopTest();

			Map<String, String> resultMap = new Map<String, String>();
			Map<Id, User> resultUserPerMap = E_UserDao.getUserWithPermissions(new Set<Id>{partner.Id});
			for(Id key : resultUserPerMap.keySet()){
				User ru = resultUserPerMap.get(key);

				for(PermissionSetAssignment psa : ru.PermissionSetAssignments){
					resultMap.put(psa.PermissionSet.Name, psa.PermissionSet.Name);
				}
			}

			// assertion
			System.assert(1 < resultMap.size());
			System.assertEquals(false, resultMap.containsKey(E_Const.PERM_SUMISEI_IRIS));
		}
	}

	/**
	 * class userPermission
	 * 	住生以外ユーザ
	 * 	IRIS権限セット（住友生命）が付与対象となっていないこと
	 */
	private static testMethod void userPermission_test04(){
		User u = TestE_TestUtil.createUser(true, 'admin', 'システム管理者');
		User partner = createTestUser3('E_PartnerCommunity_IA', '12345', '03');
		E_IDCPF__c idcpf = createTestIdcpf(false, partner.Id);

		System.runAs(u){
			Test.startTest();

			Set<Id> ids = new Set<Id>{idcpf.Id};
			List<E_IDCPF__c> idcpfs = E_IDCPFDao.getActiveUsersRecsByIDs(ids);

			E_IDCPFTriggerBizLogic bizlogic = new E_IDCPFTriggerBizLogic();
			bizlogic.addTargetUsers(idcpfs);
			bizlogic.PermissionSetAssign();

			Test.stopTest();

			Map<String, String> resultMap = new Map<String, String>();
			Map<Id, User> resultUserPerMap = E_UserDao.getUserWithPermissions(new Set<Id>{partner.Id});
			for(Id key : resultUserPerMap.keySet()){
				User ru = resultUserPerMap.get(key);

				for(PermissionSetAssignment psa : ru.PermissionSetAssignments){
					resultMap.put(psa.PermissionSet.Name, psa.PermissionSet.Name);
				}
			}

			// assertion
			System.assert(1 < resultMap.size());
			System.assertEquals(false, resultMap.containsKey(E_Const.PERM_SUMISEI_IRIS));
		}
	}

   //ユーザ
	static User createTestUser(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, '契約者', 'Test000', 'Test000');
		User us = new User();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = '契約者';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity_IA');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
		return us;
	}

   //ユーザ2(募集人コードが5桁の場合)
	static User createTestUser2(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, '契約者', 'Test000', 'T0000');
		User us = new User();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = '契約者';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity_IA');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
		return us;
	}

	/**
	 * 
	 */
	static User createTestUser3(String profileName, String zheaday, String agType){
		Profile pro = E_ProfileDaoWithout.getRecByName(profileName);

		Account parentAcc = TestE_TestUtil.createAccount(false);
		//parentAcc = [Select Id, E_CL1PF_ZHEADAY__c, OwnerId From Account Where Id = :parentAcc.Id];
		parentAcc.E_CL1PF_ZHEADAY__c = zheaday;
		insert parentAcc;

		Account acc = TestE_TestUtil.createAccount(false);
		//acc = [Select Id, ParentId From Account Where Id = :acc.Id];
		acc.ParentId = parentAcc.Id;
		insert acc;

		Contact con = TestE_TestUtil.createContact(false, acc.Id, 'テスト募集人', 'Test000', 'Test000');
		//con = [Select Id, E_CL3PF_AGTYPE__c From Contact Where Id = :con.Id];
		con.E_CL3PF_AGTYPE__c = agType;
		insert con;

		User u = new User();
		u.Username = 'test@test.com@sfdc.com';
		u.Alias = 'テスト募集人';
		u.Email = 'test@test.com';
		u.EmailEncodingKey = 'UTF-8';
		u.LanguageLocaleKey = 'ja';
		u.LastName = 'テスト募集人';
		u.LocaleSidKey = 'ja_JP';
		u.ProfileId = pro.Id;
		u.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		u.IsActive = true;
		u.ContactId = con.Id;
		insert u;

		return u;
	}

	static E_IDCPF__c createTestIdcpf(Boolean isIns, Id userId){
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(isIns, userId);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.IsMaintenanceCharge__c = true;
		idcpf.IsCrApprove__c = true;
		idcpf.IsCrDpa__c = true;
		idcpf.IsScDirector__c = true;
		idcpf.IsScAdmin__c = true;
		idcpf.IsShowSREmployee__c = true;
		idcpf.ZSTATUS01__c =  CR_Const.PW_STATUS_AGENCY;
		idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
		insert idcpf;

		return idcpf;
	}

}