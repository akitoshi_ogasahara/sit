@isTest
private class TestE_PTNPFDao {
	
	@isTest static void getRecsByPolicyidTest01() {
		E_Policy__c pol = createPolicy();
		createPtnpf(pol);
		Test.startTest();
		List<E_PTNPF__c> ptnpf = E_PTNPFDao.getRecsByPolicyid(pol.Id);
		Test.stopTest();

		System.assertEquals(ptnpf[0].ZTRANDSC__c,'テストクラス作成');
	}


	private static E_Policy__c createPolicy(){
		E_Policy__c pol = new E_Policy__c();
		pol.COMM_CHDRNUM__c = '99999999';

		insert pol;
		return pol;
	}

	//保険取引履歴
	private static void createPtnpf(E_Policy__c policy){
		E_PTNPF__c ptn = new E_PTNPF__c();

		ptn.E_Policy__c = policy.Id;
		ptn.TRANDATE__c = '20171027';
		ptn.ZTRANDSC__c = 'テストクラス作成';

		insert ptn;
	}
}