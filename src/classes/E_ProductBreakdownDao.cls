public with sharing class E_ProductBreakdownDao {

	/**
	 * 代理店挙積情報IDをキーに、指定した商品内訳を取得する。
	 * @param asrId: 代理店挙積情報ID
	 * @return List<E_ProductBreakdown__c>: 商品内訳
	 */
	public static List<E_ProductBreakdown__c> getRecsByAsrId(Id asrId) {
		return [Select
					Id							// カスタムオブジェクト ID
//					,IsDeleted					// 削除
					,Name						// 商品内訳No
//					,CreatedDate				// 作成日
//					,CreatedById				// 作成者 ID
//					,LastModifiedDate			// 最終更新日
//					,LastModifiedById			// 最終更新者 ID
//					,SystemModstamp				// System Modstamp
					,E_AgencySalesResults__c	// 代理店挙積情報
					,E_AgencySalesResults__r.YYYYMM__c	// 代理店挙積情報.年月
					,Hierarchy__c				// 階層
					,NBCANP__c					// 新契約係数ANP　当年
					,NBCANP_LY__c				// 新契約係数ANP　前年
					,NBCANP_B2Y__c				// 新契約係数ANP　2年前
//					,ParentAccount__c			// 代理店コード
					,ProductName__c				// 商品名　当年
				From
					E_ProductBreakdown__c
				Where
					E_AgencySalesResults__c	= :asrId	// 代理店挙積情報
				Order By
					NBCANP__c DESC 				// 新契約係数ANP　当年
		];
	}

}