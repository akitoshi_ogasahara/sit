/**
 * NNLinkの標準アクセスコントロールモデルに基づいたShareレコードを設定する
 */
public abstract class E_ShareBase { 
	/* 定数 */
	// NNLinkの共有ルール名（各オブジェクトのApex共有の理由に設定しておくこと）　　NNLink共有：NNLinkSharingRule
	private static final String SHARE_ROWCAUSE_NNLink = 'NNLinkSharingRule__c';

	/* 変数 */
	/* 挙積変更 */
//	private String accessLevel;
	protected String accessLevel;
	protected String sObjName;					// メインオブジェクトのAPI名
	protected String shareObjName;				// メインオブジェクトのAPI名
	protected Schema.SObjectType shareObjType;	// ShareオブジェクトType
	protected String lastRecPrefix;				// 前処理のレコードPrefix
	protected List<SObject> shares;
	protected List<String> errList;
	
	/* Exception */
	protected String errMsg;
	public class ShareBaseException extends Exception{}

	/**
	 *	deleteNNLinkShares
	 *	共有の理由が『NNLinkSharingRule』であるレコードを削除する。
	 */
	public void deleteNNLinkShares(Set<SObject> recs){
		if(recs.isEmpty()) return;
		
		List<SObject> recsList = new List<SObject>(recs);
		String sObjPrefix = String.valueOf(recsList[0].Id).subString(0,3);
		String shareObjName = recsList[0].Id.getSObjectType().getDescribe().getName().replace('__c','__share');

		for(SObject rec :recsList){
			system.assertEquals(String.valueOf(rec.Id).subString(0,3), sObjPrefix
									, '複数のオブジェクトをまとめて処理できません。オブジェクト単位で呼び出してください。');
		}

		String soql = 'SELECT Id, ParentId';
		soql += ' FROM ' + shareObjName;
		soql += ' WHERE parentId in :recs ';
		soql += ' AND ROWCause = :SHARE_ROWCAUSE_NNLink';

		List<SObject> shares = Database.query(soql);
		delete shares;
	}

	/**
     *	NNLinkの標準アクセスコントロールモデルに基づいたShareレコードを設定する
     *	L1公開グループ、L2公開グループ、BR支社公開グル―プを設定する
	 */
	public List<SObject> createSharesNNLink(Map<SObject, Account> recs, String level){
		shares = new List<SObject>();
		
		// AccessLevel
		accessLevel = level;
		
		// Prefix
		lastRecPrefix = null;

		// ShareBizLogic
		createSharesBizLogic(recs);
		
		// 正常レコードのみInsert、エラーレコードは連携ログの詳細を更新
		if(shares.isEmpty() == false){
			// Insert
			List<Database.SaveResult> insrList = Database.insert(shares,false);
			
			// エラーレコードのエラー内容をセット
			errMsg = '';
			for(Integer i = 0; i< shares.size(); i++){
				if(insrList != null && !insrList.get(i).isSuccess()){
					Database.Error err = insrList.get(i).getErrors().get(0);
					//errList.add('[Id: ' + shares.get(i).Id + ', エラー: ' +err.getmessage() + ']\n');
					errMsg += '[' + shares.get(i).get('parentId') + '/' +err.getmessage() + ']\n';
				}				
			}
			if(String.isNotBlank(errMsg)){
				throw new ShareBaseException(errMsg);
			}
		}

		return shares;
	}
	
	/**
	 * Shareロジック
	 */
	protected virtual void createSharesBizLogic(Map<SObject, Account> recs){
		// 事務所毎の公開グループより、GroupIdを取得
		Map<String, Group> grMap = getGroupIds(recs);
		
		// 事務所の件数分、処理を行う
		for(SObject parentRec : recs.keyset()){
			// 事務所レコード取得
			Account acc = recs.get(parentRec);
		
			// 事務所レコードが取得できない場合は、Skipする。
			if(acc == null) continue;
			
			//ShareオブジェクトのAPI名取得		prefixが変更された場合のみDescribeを呼び出す。
			if(lastRecPrefix == null || lastRecPrefix != String.valueOf(parentRec.Id).subString(0,3)){
				sObjName = parentRec.Id.getSObjectType().getDescribe().getName();
				shareObjName = sObjName.replace('__c','__share');
				shareObjType = Schema.getGlobalDescribe().get(shareObjName);
				lastRecPrefix = String.valueOf(parentRec.Id).subString(0,3);
			}
			
			// Shareの作成
			createShares(acc, parentRec, grMap);
		}
	}
	
	/**
	 * Shareの作成 
	 */
	protected virtual void createShares(Account acc, SObject parentRec, Map<String, Group> grMap){
		//L1　公開グループ設定
		if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
			Group grL1 = grMap.get('L1'+acc.E_ParentZHEADAY__c);
			if(grL1 != null){
				shares.add(createShare(shareObjType, parentRec.Id, grL1.Id));
			}
		}
		//L2　公開グループ設定
		if(String.isNotBlank(acc.E_CL2PF_ZAGCYNUM__c)){
			Group grL2 = grMap.get('L2'+acc.E_CL2PF_ZAGCYNUM__c);
			if(grL2 != null){
				shares.add(createShare(shareObjType, parentRec.Id, grL2.Id));
			}
		}
		//BR　公開グループ設定
		if(String.isNotBlank(acc.E_CL2PF_BRANCH__c)){
			Group grBR = grMap.get('BR'+acc.E_CL2PF_BRANCH__c);
			if(grBR != null){
				shares.add(createShare(shareObjType, parentRec.Id, grBR.Id));
			}	
		}
	}
	
	/**
	 * Shareの作成
	 */
	protected SObject createShare(Schema.SObjectType objType, Id parentId, Id grpId){
		SObject share = objType.newSObject();
		share.put('parentId', parentId);
		share.put('AccessLevel', accessLevel);
		share.put('RowCause', SHARE_ROWCAUSE_NNLink);
		share.put('UserOrGroupId', grpId);
		return share;
	}

	/**
	 * 事務所毎の公開グループより、GroupIdを取得
	 */
	protected Map<String, Group> getGroupIds(Map<SObject, Account> recs){
		// 事務所毎の公開グループを設定（L1、L2、BR）
		Set<String> groupNames = getGroupNames(recs);
		
		// Groupの取得
		Map<String, Group> grMap = getGroupMap(groupNames);
		
		return grMap;
	}
	
	/**
	 * 事務所毎の公開グループ取得
	 * @param Map<SObject, Account> 
	 * @return Set<String>
	 */
	protected virtual Set<String> getGroupNames(Map<SObject, Account> recs){
		Set<String> groupNames = new Set<String>();
		
		// 事務所毎の公開グループを設定（L1、L2、BR）
		for(Account acc:recs.values()){
			// 事務所レコードが取得できない場合は、Skipする。
			if(acc == null) continue;
			
			/* 公開グループ設定 */
			// L1（代理店格コード）
			if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
				groupNames.add('L1' + acc.E_ParentZHEADAY__c);
			}
			// L2（代理店事務所コード）
			if(String.isNotBlank(acc.E_CL2PF_ZAGCYNUM__c)){
				groupNames.add('L2' + acc.E_CL2PF_ZAGCYNUM__c);
			}
			// BR（支社コード）
			if(String.isNotBlank(acc.E_CL2PF_BRANCH__c)){
				groupNames.add('BR' + acc.E_CL2PF_BRANCH__c);
			}
		}
		
		return groupNames;
	}
	
	/**
	 * 引数のDeveloperNameリストを元に公開グループを取得する
	 * @param Set<String> 
	 * @return Map<String, Group>				TODO クエリの発行回数を制限する必要あり
	 *
	 */
	protected Map<String, Group> getGroupMap(Set<String> cds){
		if (cds == null) {
			return null;
		}
		Map<String, Group> groupMap = new Map<String, Group>();
		for (Group grp : [Select Id, Name, Type, DeveloperName From Group where type = 'Regular' and developerName in : cds]) {
			groupMap.put(grp.DeveloperName, grp);
		}
		return groupMap;
	}
}