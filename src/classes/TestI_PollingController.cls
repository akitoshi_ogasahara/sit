@isTest
private class TestI_PollingController {
	
	@isTest static void getPollingCountTest01() {
		Test.startTest();
			I_PollingController controller = new I_PollingController();
			Integer cnt = controller.getPollingCount();
		Test.stopTest();
		System.assertEquals(cnt,0);
	}

	@isTest static void getPollingCountTest02() {
		Test.startTest();
			I_PollingController controller = new I_PollingController();
			controller.polling();
			Integer cnt = controller.getPollingCount();
		Test.stopTest();
		System.assertEquals(cnt,1);
	}
}