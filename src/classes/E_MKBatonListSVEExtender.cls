global with sharing class E_MKBatonListSVEExtender extends E_AbstractViewExtender{

    private static final String PAGE_TITLE = 'バトンプロジェクト';

    /*
     * Constructor
     */
    public E_MKBatonListSVEExtender (E_MKBatonListSVEController controller){
        super();
        this.pgTitle = PAGE_TITLE;
    }
}