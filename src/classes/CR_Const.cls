public class CR_Const {

    public static final String RECTYPE_DEVNAME_RULE = 'CR_Rule';
    public static final String RECTYPE_DEVNAME_OUTSRC = 'CR_Outsrc';
    public static final String RECTYPE_DEVNAME_BIZ = 'CR_Biz';
    
    // URLパラメータ
    public static final String URL_PARAM_TYPE_BIZ = 'type';
    
    /*  権限セット名  */
    public static final String PERMISSIONSET_APPROVE = 'CR_PermissionSet_Approve';
    public static final String PERMISSIONSET_OUTSRC  = 'CR_PermissionSet_OutSrc';
    public static final String PERMISSIONSET_EMPLOYEE = 'CR_PermissionSet_Employee';
    public static final String PERMISSIONSET_AGENCY   = 'CR_PermissionSet_Agency';
    //Phase2
    public static final String PERMISSIONSET_DPA   = 'CR_PermissionSet_DPA';
    public static final String PERMISSIONSET_REPORTS   = 'CR_PermissionSet_Reports';
    public static final String PERMISSIONSET_BOOKS   = 'CR_PermissionSet_Books';
    
    public static final String ERR_MSG01_CANNOT_GET_RULE_RECORD = '代理店レコード（規程管理）の取得に失敗しました。';
    public static final String ERR_MSG02_REQUIRED_FILE = '添付ファイルを選択してください。';
    public static final String ERR_MSG02b_REQUIRED_FILE_REASON = '添付ファイルを選択するか、添付いただけない理由を記載してください。';
    public static final String ERR_MSG03_INVALID_URL = 'Urlが不正です。';
    public static final String ERR_MSG04_REQUIRED_FILES = 'ファイルをアップロードいただくか、アップロードいただけない理由をファイルアップロードページから記載ください。';
    public static final String ERR_MSG05_REQUIRED_PERMISSION_OUTSRC = '外部委託参照権限がありません。';
    //Phase2
    public static final String ERR_MSG01_PH2_FRAUD_OUTPUTYM = '正しい出力基準年月を入力してください。';
    public static final String ERR_MSG02_PH2_SAME_CONDITION = '同条件で既にデータ作成依頼済みです。';
    public static final String ERR_MSG03_PH2_LIMIT_REQUEST = '12時（正午）から翌日の12時（正午）間で、データ作成依頼は最大3回までとなります。';
    public static final String ERR_MSG04_PH2_REQUIRED_PERMISSION_REPORTS = '事業報告書権限がありません。';
    public static final String ERR_MSG05_PH2_REQUIRED_PERMISSION_BOOKS = '帳簿書類権限がありません。';
    public static final String ERR_MSG06_PH2_REQUIRED_OFFICES = '代理店事務所は1つ以上選択して下さい。';
    public static final String ERR_MSG07_PH2_FROM_OUTPUTYM = '出力基準年月を2016年5月以降としてください。';

    /* 外部委託　ステータス一覧 */
    public static final String OUTSRC_STATUS_01  = '01.作成中';
    public static final String OUTSRC_STATUS_02a = '02.確認中';
    public static final String OUTSRC_STATUS_02b = '02.報告済';
    public static final String OUTSRC_STATUS_03  = '03.承認済';
    public static final String OUTSRC_STATUS_09  = '09.委託終了';
    
    /* HELP Menu名*/
    //管理規程
    public static final String MENU_HELP_RULE = 'Help_CR_Rule';
    //管理規程一覧　タグ
    public static final String MENU_HELP_RULE_View = 'cms_contents_001';
    //ファイルアップロードページ　タグ（新規アップロード時）
    public static final String MENU_HELP_RULE_Submit_New = 'cms_contents_002';
    //ファイルアップロードページ　タグ（規程種別変更時）
    public static final String MENU_HELP_RULE_Submit_Upd = 'cms_contents_003';
    
    //外部委託
    public static final String MENU_HELP_OUTSRC = 'Help_CR_Outsrc';
    //外部委託一覧　タグ
    public static final String MENU_HELP_OUTSRC_List = 'cms_contents_001';
    //外部委託参照　タグ
    public static final String MENU_HELP_OUTSRC_View = 'cms_contents_002';
    //外部委託編集　タグ
    public static final String MENU_HELP_OUTSRC_Edit = 'cms_contents_003';
    //ファイルアップロード　タグ
    public static final String MENU_HELP_OUTSRC_Submit = 'cms_contents_004';
    //ファイルアップロード　タグ
    public static final String MENU_HELP_OUTSRC_Terminate = 'cms_contents_005';
    
    // 事業報告書
    public static final String MENU_REPORTS = 'business_report';
    public static final String MENU_HELP_REPORTS = 'Help_CR_Reports';
    
    // 帳簿書類
    public static final String MENU_BOOKS = 'business_book';
    public static final String MENU_HELP_BOOKS = 'Help_CR_Books';

    // EBiz連携ログ　区分
    //  代理店提出レコード作成、レコードへのアクセスコントロール設定
    public static final String EBIZ_RULE_AND_OUTSRC_AGENCY = '5';
    //  業法改正関連ログに添付されたファイルの削除
    public static final String EBIZ_LOGKBN_ATTACHMENTS_DELETE = '6';
    
    //      添付ファイル保持期間（デフォルト90日）
    public static Integer CR_ATTACHMENTS_EXPIRE_DAYS{
                                    get{
                                        try{
                                            if(CR_ATTACHMENTS_EXPIRE_DAYS == null){
                                                CR_ATTACHMENTS_EXPIRE_DAYS = Integer.valueOf(Label.CR_ATTACHMENTS_EXPIRE_DAYS);
                                            }
                                            return CR_ATTACHMENTS_EXPIRE_DAYS;
                                        }catch(Exception e){
                                            //初期値は90
                                            return 90;
                                        }
                                    }
                                    set;
                    }
    
    //ID管理.パスワードステータス=1(一般代理店)  
    public static final String PW_STATUS_AGENCY = '1';
    
    //ファイルアップロード時のContentType：強制的にダウンロードさせる
    public static final String CONTENTTYPE_FOR_DOWNLOAD = 'application/octet-stream';
    
    /* ダウンロード履歴　ステータス一覧 */
    public static final String DL_STATUS_WAIT = '00';
    public static final String DL_STATUS_PROCESSING = '01';
    public static final String DL_STATUS_SUCCESS = '02';
    public static final String DL_STATUS_DELETED = '09';
    public static final String DL_STATUS_ERROR = '99';
    
    /* ダウンロード履歴　種別 */
    public static final String TYPE_BIZREPORTS = '8';
    public static final String TYPE_BIZBOOKS = '9';
    
    // 出力基準年月フォーマット
    public static final String OUTPUT_RECORD_YM_FORMAT = 'yyyy/MM';
    
    public static final String OUTPUT_RECORD_YM_REPORTS_FROM = '2016/05';
    public static final String OUTPUT_RECORD_YM_REPORTS_TO = '2050/12';
    public static final String OUTPUT_RECORD_YM_BOOKS_FROM = '2016/05';
    public static final String OUTPUT_RECORD_YM_BOOKS_TO = '2050/12';
    
    
    // 事業報告書・帳簿書類 1日のリクエストカウント
    public static final Integer REQUEST_CNT_LIMIT = 3;

}