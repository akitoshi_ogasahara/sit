public with sharing class E_BizDataSyncBatchOperation_NewPolicy extends E_BizDataSyncBatchOperation {
    
	/**
	 * getQueryLocator for Batch start method
	 */
	public override Database.QueryLocator getQueryLocator(){
//		errList = new List<String>();
		//最終ステータスセット
		Set<String> statuses = I_NewPolicyConst.finalStatus;
		// 期間（最終ステータス　=>　前月～今日、　以外　=>　期間制御なし）
		Date ago = Date.today().addMonths(-1);
		Date d = Date.newInstance(ago.year(), ago.month(), 1);

		return Database.getQueryLocator(
					[Select Id
						,Name
						,Agent__c							// 募集人（主）
						,AGNTNUM__c							// 募集人コード（主）
						,MainAgentNum5__c
						,Account__c
						,Account__r.E_ParentZHEADAY__c		// 代理店格（主）
						,Account__r.E_CL2PF_ZAGCYNUM__c		// 事務所（主）
						,Account__r.E_CL2PF_BRANCH__c		// 支社（主）
						,SubAgent__c						// 募集人（従）
						,SubAGNTNUM__c						// 募集人コード（従）
						,SubAgentNum5__c
						,SubAccount__c
						,SubAccount__r.E_ParentZHEADAY__c	// 代理店格（従）
						,SubAccount__r.E_CL2PF_ZAGCYNUM__c	// 事務所（従）
						,SubAccount__r.E_CL2PF_BRANCH__c	// 支社（従）
					From 
						E_NewPolicy__c
					Where
						(Status__c In :statuses And StatusDate__c >= :d) 
					Or 
						Status__c Not In :statuses]);
	}
    
	/**
	 * execute
	 * 		batchのExecuteの本体ロジック
	 */
    public override void execute(List<Sobject> records){
    	// 事務所のマップ作成
    	Map<SObject, Account> accountMap = new Map<SObject, Account>();
    	Map<SObject, Account> subAccountMap = new Map<SObject, Account>();
		for(SObject rec : records){
			E_NewPolicy__c policy = (E_NewPolicy__c)rec;
			accountMap.put(policy, policy.Account__r);
			subAccountMap.put(policy, policy.subAccount__r);
		}
    	
    	// Shareクラス生成
		E_NewPolicyShare npShare = new E_NewPolicyShare(accountMap, subAccountMap);
		
		// Share設定
		npShare.execute();
    }
}