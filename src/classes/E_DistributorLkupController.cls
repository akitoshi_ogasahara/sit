global with sharing class E_DistributorLkupController extends SkyEditor2.SkyEditorPageBaseWithSharing{
    
    public Account record{get;set;}
    public E_DistributorLkupExtender getExtender() {return (E_DistributorLkupExtender)extender;}
    public searchDataTable searchDataTable {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c ipVal_distributorCode{get;set;}
    public SkyEditor2__SkyEditorDummy__c ipVal_dairitenkana{get;set;}
    public E_DistributorLkupController(ApexPages.StandardController controller){
        super(controller);

        SObjectField f;
        f = Account.fields.E_CL1PF_ZHEADAY__c;
        f = Account.fields.E_CL1PF_ZAHKNAME__c;
        f = Account.fields.Name;
        f = Account.fields.E_COMM_ZCLADDR__c;
        f = Account.fields.E_COMM_VALIDFLAG__c;
        f = Account.fields.E_IsAgency__c;

        try {
            mainRecord = null;
            mainSObjectType = Account.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            
            ipVal_distributorCode = new SkyEditor2__SkyEditorDummy__c();
            ipVal_dairitenkana = new SkyEditor2__SkyEditorDummy__c();
            
            queryMap.put(
                'searchDataTable',
                new SkyEditor2.Query('Account')
                    .addFieldAsOutput('E_CL1PF_ZHEADAY__c')
                    .addFieldAsOutput('Name')
                    .addFieldAsOutput('E_COMM_ZCLADDR__c')
                    .addField('Name')
                    .limitRecords(100)
                    .addListener(new SkyEditor2.QueryWhereRegister(ipVal_distributorCode, 'SkyEditor2__Text__c', 'E_CL1PF_ZHEADAY__c', new SkyEditor2.TextHolder('eq'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(ipVal_dairitenkana, 'SkyEditor2__Text__c', 'E_CL1PF_ZAHKNAME__c', new SkyEditor2.TextHolder('co'), false, true, false))
                     .addWhere(' ( E_COMM_VALIDFLAG__c = \'1\' AND E_IsAgency__c = true)')
.addSort('E_CL1PF_ZAHKNAME__c',True,True)
            );
            
            searchDataTable = new searchDataTable(new List<Account>(), new List<searchDataTableItem>(), new List<Account>(), null);
            listItemHolders.put('searchDataTable', searchDataTable);
            
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(Account.SObjectType);
            
            p_showHeader = false;
            p_sidebar = false;
            presetSystemParams();
            extender = new E_DistributorLkupExtender(this);
            initSearch();
            
            extender.init();
            
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }

    public List<SelectOption> getOperatorOptions_Account_E_CL1PF_ZHEADAY_c() {
        return getOperatorOptions('Account', 'E_CL1PF_ZHEADAY__c');
    }
    public List<SelectOption> getOperatorOptions_Account_E_CL1PF_ZAHKNAME_c() {
        return getOperatorOptions('Account', 'E_CL1PF_ZAHKNAME__c');
    }
    
    global with sharing class searchDataTableItem extends SkyEditor2.ListItem {
        public Account record{get; private set;}
        @TestVisible
        searchDataTableItem(searchDataTable holder, Account record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class searchDataTable extends SkyEditor2.ListItemHolder {
        public List<searchDataTableItem> items{get; private set;}
        @TestVisible
        searchDataTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<searchDataTableItem>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new searchDataTableItem(this, (Account)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
}