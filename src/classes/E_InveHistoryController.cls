public with sharing class E_InveHistoryController extends E_AbstractController {
	
	public E_ITHPF__c record {get;set;}
	private String ithpfId = null; 
	
	/** コンストラクタ */
	public E_InveHistoryController(){
		super();
		ithpfId = ApexPages.CurrentPage().getParameters().get('ithpfId');
		if(String.isBlank(ithpfId)){
			ithpfId = null;
		}
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, ithpfId);
		if(pageRef == null){
			if(String.isNotBlank(ithpfId)){
				record = E_ITHPFDao.getRecById(ithpfId);
			}
		}
	}
	
	/** ページアクション */
	public PageReference pageAction(){
		ApexPages.CurrentPage().getParameters().put('ithpfId', ithpfId);
		return E_Util.toErrorPage(pageRef, null);
	}
	
    /** 戻るボタン押下処理 */
    public Pagereference doBack(){
    	Pagereference pref = page.E_Inve;
		pref.getParameters().put('id', ithpfId);
    	return pref;
    }
}