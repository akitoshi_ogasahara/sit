/**
		SandBox リフレッシュタスク自動化
			＜自動化タスク＞
				1.マスク処理
				
			＜手動設定を必要に応じて実施＞
				1.メールアクセス権の設定
				2.Community/Sitesの外部サイトの無効化
				3.TrustBindの変換テーブルの調整（ドメイン変更の場合など）
				4.ワークフロールールの無効化（PartialSandBoxのみ　有効化状態ではリレーションが不完全なためInternal　Server　Errorが発生します。）
 */
global class MNT_SandBoxRefresh implements SandboxPostCopy{

	/**
	 *	  SandBoxリフレッシュ時組み込み用メソッド
	 */
	global void runApexClass(SandboxContext context) {
		String firstObjNm = '';
		for(String nm:MNT_SandBoxRefresh.REFRESH_JOBs.keyset()){
			firstObjNm = nm;
			break;
		}
		
		//1件目を走らせる
		if(String.isNotBlank(firstObjNm)){
			MNT_RefreshJobBatch batch = new MNT_RefreshJobBatch(firstObjNm);
			batch.executeNextJob = true;
			Database.executeBatch(batch, 2000);
		}
	}


	// マスク方法
	public Enum MASKTYPE {NAME, EMAIL, ADDRESS, PHONE, MaskByID}

	/**
	 *	  マスク対象項目の特定
	 *			  Updatableな項目のみ指定してください
	 */
	public static Map<String, Map<String, MASKTYPE>> REFRESH_JOBs = new Map<String, Map<String, MASKTYPE>>{
							'User' => new Map<String, MASKTYPE>{
											'Alias' => MASKTYPE.Name
											,'CommunityNickname' => MASKTYPE.MaskByID
											,'Email' => MASKTYPE.Email
											,'FirstName' => MASKTYPE.Name
											,'LastName' => MASKTYPE.Name
											//,'Name' => MASKTYPE.Name
									}
							,
							'Account' => new Map<String, MASKTYPE>{			 
											//'AgencyName__c' => MASKTYPE.Name			is not editable
											'E_CL2PF_ZEAYNAM__c' => MASKTYPE.Name
											,'E_CL2PF_ZEAYKNAM__c' => MASKTYPE.Name
											,'E_COMM_ZCLADDR__c' => MASKTYPE.Address
											//,'E_ParentName__c' => MASKTYPE.Name	   E_ParentName__c is not editable
											,'KJADDR01__c' => MASKTYPE.Address
											,'Name' => MASKTYPE.Name
									}
							,'Contact' => new Map<String, MASKTYPE>{
											'E_CLTPF_ZCLKADDR__c' => MASKTYPE.Address
											,'E_CLTPF_ZCLKNAME__c' => MASKTYPE.Name
											,'E_CLTPF_ZNCLTADR01__c' => MASKTYPE.Address
											,'E_CLTPF_ZNKJADDR01__c' => MASKTYPE.Address
											,'E_COMM_ZCLADDR__c' => MASKTYPE.Address
											,'FirstName' => MASKTYPE.Name
											,'LastName' => MASKTYPE.Name
											//,'Name' => MASKTYPE.Name
											,'E_CLTPF_CLTPHONE01__c' => MASKTYPE.Phone
											,'E_CLTPF_CLTPHONE02__c' => MASKTYPE.Phone
											,'E_CLTPF_FAXNO__c' => MASKTYPE.Phone
									}
							,'E_CRLPF__c' => new Map<String, MASKTYPE>{
											'ZCLADDR__c' => MASKTYPE.Address
											,'ZCLKADDR__c' => MASKTYPE.Address
											,'ZCLKNAME__c' => MASKTYPE.Name
											,'ZCLNAME__c' => MASKTYPE.Name
									}
							,'E_BOSPF__c' => new Map<String, MASKTYPE>{
											'ZEATNAM__c' => MASKTYPE.Name
											,'ZEAYNAM__c' => MASKTYPE.Name
											,'ZEGRUPNAME__c' => MASKTYPE.Name
											,'ZELIFENAM__c' => MASKTYPE.Name
											,'ZEOWNNAME__c' => MASKTYPE.Name
											//,'ZISSUEATName__c' => MASKTYPE.Name		ZISSUEATName__c is not editable
									}
							,'E_AGSPF__c' => new Map<String, MASKTYPE>{
											'ZEATNAM__c' => MASKTYPE.Name
											,'ZEAYNAM__c' => MASKTYPE.Name
											,'ZELIFENAM__c' => MASKTYPE.Name
											,'ZEOWNNAME__c' => MASKTYPE.Name
									}
/*
							,'CR_Agency__c' => new Map<String, MASKTYPE>{
											//'AgencyNameKana__c' => MASKTYPE.Name	  AgencyNameKana__c is not editable
											//,'AgencyName__c' => MASKTYPE.Name		 AgencyName__c is not editable
											//,'MRName__c' => MASKTYPE.Name			  MRName__c is not editable
									}
*/
							,'CR_Outsrc__c' => new Map<String, MASKTYPE>{
											'Address__c' => MASKTYPE.Address
											//,'AgencyName__c' => MASKTYPE.Name		 AgencyName__c is not editable
											,'MRMail__c' => MASKTYPE.Email
											//,'OutsrcLink__c' => MASKTYPE.Name		 OutsrcLink__c is not editable
											,'Representative__c' => MASKTYPE.Name
									}
							,'CR_Rule__c' => new Map<String, MASKTYPE>{
											//'AgencyName__c' => MASKTYPE.Name		  AgencyName__c is not editable
											'MRMail__c' => MASKTYPE.Email
											//,'MRName__c' => MASKTYPE.Name			 MRName__c is not editable
									}
							,'E_IDCPF__c' => new Map<String, MASKTYPE>{
											'ZEMAILAD__c' => MASKTYPE.Email
									}
									
									
				/*	=====	レコード削除対象オブジェクト	（項目リストなしは削除対象とする。）	=====	*/		
							,'E_SPSPFEmail__c' => new Map<String, MASKTYPE>{/* Records Delete */}			   
							,'E_ADRPF__c' => new Map<String, MASKTYPE>{/* Records Delete */}				
							,'E_CancellFormDownloadHistory__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'Agency__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'SupportMR__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'WS__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'DefectInfoHeader__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'DefectInfo__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'Opportunity' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'Case' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'AGC_DairitenJimushoHosokuJoho__c' => new Map<String, MASKTYPE>{/* Records Delete */}
							,'AGC_DairitenKihonJoho__c' => new Map<String, MASKTYPE>{/* Records Delete */}
	};
}