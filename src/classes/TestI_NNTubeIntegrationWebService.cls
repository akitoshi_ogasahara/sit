/**
 * I_NNTubeIntegrationWebServiceのテストクラス.
 * CreatedDate 2016/12/21
 * @Author Terrasky
 */
@isTest
private class TestI_NNTubeIntegrationWebService {
	
	private static User u;
	private static Contact con;
	private static Account acc;
	
	static void setUpForNN() {
		acc = TestE_TestUtil.createAccount(false);
		acc.ZMRCODE__c = '11';
		insert acc;
		
		u = TestE_TestUtil.createUser(false, 'Test', 'システム管理者');
		u.MRCD__c = '11';
		insert u;
	}
	
	static void setUpForPartner() {
		acc = TestE_TestUtil.createAccount(true);
		con = TestE_TestUtil.createContact(true, acc.Id, 'Test', 'cc123', '12345|0001');
		u = TestE_TestUtil.createUser(false, 'Test', 'E_PartnerCommunity');
		u.ContactId = con.Id;
		insert u;
		
		con.OwnerId = u.Id;
		update con;
	}
	
	static testMethod void myUnitTest01() {
		
		setUpForNN();
		
		Test.startTest();
		
		System.runAs(u) {
			I_NNTubeIntegrationWebService.getUserInfo(u.Id);
		}
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest02() {
		
		setUpForPartner();
		
		Test.startTest();
		
		System.runAs(u) {
			I_NNTubeIntegrationWebService.getUserInfo(u.Id);
		}
		
		Test.stopTest();
	}
}