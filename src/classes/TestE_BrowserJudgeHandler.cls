/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestE_BrowserJudgeHandler {

	//24時間以内に確認されている状態のテスト
    static testMethod void test_isAlreadyConfirmed01() {
		PageReference pr = Page.E_LoginAgent;
		pr.getParameters().put(E_BrowserJudgeHandler.PARAM_SKIP_CONFIRM, String.valueOf(System.now().getTime()));
		
		Test.setCurrentPage(pr);
		Test.startTest();
		
		E_BrowserJudgeHandler handler = new E_BrowserJudgeHandler(Page.E_LoginAgent);
		
		System.assertEquals(true, handler.isAlreadyConfirmed);
		
		Test.stopTest();
    }

	//24時間以内に確認されている状態のテスト
    static testMethod void test_isAlreadyConfirmed02() {
		PageReference pr = Page.E_LoginAgent;
		
		//25時間前に確認している
		pr.getParameters().put(E_BrowserJudgeHandler.PARAM_SKIP_CONFIRM, String.valueOf(System.now().addHours(-25).getTime()));
		
		Test.setCurrentPage(pr);
		Test.startTest();
		
		E_BrowserJudgeHandler handler = new E_BrowserJudgeHandler(Page.E_LoginAgent);
		
		System.assertEquals(false, handler.isAlreadyConfirmed);		//25時間前なのでアウト
		
		Test.stopTest();
    }

    static testMethod void test_forCoverage() {
		E_BrowserJudgeHandler handler = new E_BrowserJudgeHandler(Page.E_LoginAgent);
		System.assertEquals(0, handler.getMSG().size());
		System.assertEquals(0, handler.getDISCLAIMER().size());
		System.assertEquals(false, handler.getIsUnsupportedBrowser());
		System.assertEquals(false, handler.getIsCompatibilityMode());
		System.assertEquals(false, handler.getShowAlert());

		System.assert(handler.doSkipConfirm().getUrl().toLowercase().contains(E_BrowserJudgeHandler.PARAM_SKIP_CONFIRM));
		System.assert(handler.toNextPage().getUrl().toLowercase().contains('e_loginagent'));
    }

}