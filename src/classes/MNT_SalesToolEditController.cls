global with sharing class MNT_SalesToolEditController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	public MNT_SalesToolEditExtender getExtender() {return (MNT_SalesToolEditExtender)extender;}
	{
	setApiVersion(42.0);
	}
	public MNT_SalesToolEditController(ApexPages.StandardController controller) {
		super(controller);
		registTransitionControl='/apex/MNT_SalesToolView?id={ID}';
		editTransitionControl='/apex/MNT_SalesToolView?id={ID}';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DocumentNo__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.ForCustomers__c;
		f = I_ContentMaster__c.fields.isRecommend__c;
		f = I_ContentMaster__c.fields.CompanyLimited__c;
		f = I_ContentMaster__c.fields.ForAgency__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.NewValidTo__c;
		f = I_ContentMaster__c.fields.Content__c;
		f = I_ContentMaster__c.fields.ContentEmployee__c;
		f = I_ContentMaster__c.fields.isHTML__c;
		f = I_ContentMaster__c.fields.Category__c;
		f = I_ContentMaster__c.fields.Management__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.LinkURL__c;
		f = I_ContentMaster__c.fields.filePath__c;
		f = I_ContentMaster__c.fields.WindowTarget__c;
		f = I_ContentMaster__c.fields.ParentContent__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_SalesToolEditController';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addField('Name');
			mainQuery.addField('DocumentNo__c');
			mainQuery.addField('ApprovalNo__c');
			mainQuery.addField('ForCustomers__c');
			mainQuery.addField('isRecommend__c');
			mainQuery.addField('CompanyLimited__c');
			mainQuery.addField('ForAgency__c');
			mainQuery.addField('DisplayFrom__c');
			mainQuery.addField('ValidTo__c');
			mainQuery.addField('NewValidTo__c');
			mainQuery.addField('Content__c');
			mainQuery.addField('ContentEmployee__c');
			mainQuery.addField('isHTML__c');
			mainQuery.addField('Category__c');
			mainQuery.addField('Management__c');
			mainQuery.addField('ClickAction__c');
			mainQuery.addField('LinkURL__c');
			mainQuery.addField('filePath__c');
			mainQuery.addField('WindowTarget__c');
			mainQuery.addFieldAsOutput('ParentContent__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_SalesToolEditExtender(this);
			init();
			if (record.Id == null) {
				saveOldValues();
			}

			extender.init();
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}