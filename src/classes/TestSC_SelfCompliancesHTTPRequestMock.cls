@isTest
public class TestSC_SelfCompliancesHTTPRequestMock implements HttpCalloutMock{
	protected Integer code;
	protected String status;
	protected String body;
	protected Map<String, String> responseHeaders;
	public Boolean errorFlg {get;set;}

	public TestSC_SelfCompliancesHTTPRequestMock(Boolean errFlg) {
		this.errorFlg = errFlg;
	}

	public HTTPResponse respond(HTTPRequest req) {
		System.assertEquals('GET',req.getMethod());

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String body = '';
		if(this.errorFlg){
			body = '{"status":"NG", "surveyid":"","token":"" }';
			status = 'OK';
			res.setBody(body);
			res.setStatusCode(200);
			res.setStatus(status);
			return res;
		}
		body = '{"status":"OK", "surveyid":"","token":"" }';
		status = 'OK';
		res.setBody(body);
		res.setStatusCode(200);
		res.setStatus(status);
		return res;
	}
}