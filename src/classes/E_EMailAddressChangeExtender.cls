global with sharing class E_EMailAddressChangeExtender extends E_AbstractEditExtender{
  private static final String PAGE_TITLE = '電子メールアドレス変更';
	
	public E_EMailAddressChangeController extension;
	String UserType = UserInfo.getUserType();
	String ret;
	Pagereference pref = Page.E_Home;    
	String contactId;
	public String mailAddress{get;set;}
	public String mailAddressConfirm{get;set;}
	public String getInfoMessage(){
		ret = null;
		//ユーザ種別ごとに表示するメッセージを変える
		if(String.isnotBlank(UserType) && getIsAgent()){
			ret = getDISCLAIMER().get('MAC|011');
		}else{
			ret = getDISCLAIMER().get('MAC|009');
		}
		return ret;
	}
	
	/* コンストラクタ */
	public E_EMailAddressChangeExtender(E_EMailAddressChangeController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
		this.contactId = E_AccessController.getInstance().user.ContactId;
	}
	
	/* init */
	global override void init(){
		// アクセスチェック
		pageRef = doAuth(E_Const.ID_KIND.USER, USERINFO.getUSerID());
	}
	
	/* pageAction */
	public PageReference pageAction () {
		System.debug('************************pageRef = ' + pageRef);
		System.debug('************************Page.E_Home = ' + Page.E_Home);
		return E_Util.toErrorPage(pageRef, null);
	}
	
	public override pagereference doReturn(){
		if(getIsCustomer()){
			pref = Page.E_ContactInfo;
			pref.getParameters().put('id', contactId);
		}
		//代理店ユーザのキャンセルの場合はEIDRを作成する
		if(access.isNormalAgent()){
			//IDリクエストレコード作成
			E_IDRequest__c eidr = new E_IDRequest__c();
			E_IDCPF__c eidcRec = E_IDCPFDao.getRecsById(UserInfo.getUserId());  //ID管理検索
			eidr.E_IDCPF__c = eidcRec.Id;                   //ID管理
			eidr.ZUPDFLAG__c = E_Const.ZUPDFLAG_E;          //リクエスト種別="E"
			eidr.ZEMAILAD__c = null;                        //メールアドレス=空欄
			eidr.note__c = '変更キャンセル';                   //備考="変更キャンセル"
			//IDリクエストを登録
			insert eidr;
		}
		return pref;
	}
	
	
	/* 実行ボタン押下処理 */
	public pagereference saveMailAddress(){
		presave();        
		if(pageMessages.hasMessages()){
			return null;
		}else{
			try{
				insMailObj(USERINFO.getUSerID());
			}catch(DMLException e) {
				if (e.getDmlStatusCode(0).equals(E_Const.INVALID_EMAIL_ADDRESS)) {
					pageMessages.addErrorMessage(getMSG().get('MAC|005'));
					return null;
				}
				pageMessages.addErrorMessage(e.getMessage());
				return null;
			}
			string key = 'ACC|';
			string type;
			if(getIsAgent()){
				type = '008';
			}else{
				type = '003';
			}
			
			return new pagereference('/apex/E_AcceptanceCompletion?type=' + key + type + '&conid=' + this.contactId );
		}
	}
	
	/* presave */
	global override void preSave(){
	  pageMessages.clearMessages();
		if(string.isBlank(mailAddress)){//メールアドレスが空白かチェック
			pageMessages.addErrorMessage(getMSG().get('MAC|001'));
		}
		
		if(string.isBlank(mailAddressConfirm)){//メールアドレス(確認)が空白かチェック
			pageMessages.addErrorMessage(getMSG().get('MAC|002'));
		}
		
		if(apexpages.hasMessages()) return;

		if(mailAddress != mailAddressConfirm){//メールアドレスが違っていないかチェック
			pageMessages.addErrorMessage(getMSG().get('MAC|003'));
		}
		
		if(mailAddress.equals(UserInfo.getUserEmail())
			&& UserInfo.getUserName().equals(mailAddress + System.Label.E_USERNAME_SUFFIX)){//以前のメールアドレスと同じかチェック

			pageMessages.addErrorMessage(getMSG().get('MAC|004'));
		}

		if(access.isNormalAgent()){
			//新規メールアドレス重複チェック
			if(isOverlapUserName()){
				//エラーメッセージの表示
				pageMessages.addErrorMessage(getMSG().get('CID|008'));
				return;
			}
		}
	}
	
	public void clearMailAddress(){
		mailAddress = null;
		mailAddressConfirm = null;
	}
	
	private void insMailObj(ID userID){
		String username = mailAddress + System.Label.E_USERNAME_SUFFIX;

		//一般代理店ユーザの場合、IDリクエストレコードを作成して確認メール送信　代理店は必ずUserNameも変更する
		if(access.isNormalAgent()){
			//Userレコード取得
			User userRec = E_UserDao.getUserRecByUserId(userID);

			username = UserInfo.getUserName();
			//IDリクエストレコード作成
			E_IDRequest__c idReqest = insIDRequest(username);
			//メールアドレス確認メール送信
			sendConfirmEmail(idReqest);
			return;
		}
		//一般代理店ユーザ以外のとき、メールアドレスのみ更新 一般代理店ユーザでもメアド!=UserName以外はメアドンのみ更新
		User updUserItem = new USER(
			ID = userID
			,EMAIL = mailAddress
		);
		update updUserItem;

		String ReceptionDatetime = System.now().format('yyyy年MM月dd日 HH時mm分');      
		User userContact = E_UserDao.getUserRecByUserId(userID);
		ID E_IDCPFID = E_IDCPFDao.getRecsById(userID).ID;
		
		E_ADRPF__c insE_ADRPFItem = new E_ADRPF__c(
			Type__c = E_Const.MAIL_TYPE_EM
			,Kbn__c = E_Const.KBN_MAIL
			,ReceptionDatetime__c = ReceptionDatetime
			,E_IDCPF__c = E_IDCPFID
			,ContactNameKana__c = userContact.contact.E_CLTPF_ZCLKNAME__c
			,ContactName__c = userContact.contact.name
			,EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c
			,CLNTNUM__c = userContact.contact.E_CLTPF_CLNTNUM__c
		);
		insert insE_ADRPFItem;
		
	}

	/**
	 * ログインユーザ名と入力メールアドレスから
	 * IDリクエストレコードを作成する
	 * @param String username ログインユーザのユーザ名
	 * @return  E_IDRequest__c IDリクエストレコード
	 */
	private E_IDRequest__c insIDRequest(String username){
		User userRec = E_UserDaoWithout.getUserRecByUserName(username);
		E_IDCPF__c eidc = E_IDCPFDaoWithout.getRecsByContactId(userRec.Contact.Id);
		E_IDRequest__c idReqest = E_IDRequestDaoWithout.insEIdrRecWithMail(eidc, mailAddress);

		return idReqest;
	}

	//メールアドレス確認メールの送信
	private void sendConfirmEmail(E_IDRequest__c idReqest){
		//IDリクエストオブジェクトのレコードId
		String recId = 'recId=' + idReqest.Id;

		//メールアドレス変更サイトのURL
		//現在のサイトのドメインを取得
		String baseUrl = Site.getDomain();
		if(String.isEmpty(baseUrl)){
			baseUrl = '';
		}
		System.debug(baseUrl);
		////現在のユーザのサイト名を取得
		String siteName = Site.getName();
		if(String.isEmpty(siteName)){
			siteName = '';
		}

		System.debug(siteName);


		////////インターネットログインユーザのメールアドレス変更
		//パラメータを暗号化
		String cryptoParam = E_EncryptUtil.getEncryptedString(recId);

		//Site'nn' と ユーザのSiteレコードを取得
		List<Site> userSite = [SELECT GuestUserId, Name, Subdomain, UrlPathPrefix,Description,ID FROM Site WHERE Name = :siteName];
		List<Site> nnSite = [SELECT GuestUserId, Name, Subdomain, UrlPathPrefix FROM Site WHERE Name = :E_Const.NNLINK_NN];

		//>>>>>>>>>userSite がnull のとき空にする>>>>>>>>>>>>
		String userSubDom = '';
		if(!userSite.isEmpty()){
			userSubDom = userSite[0].Subdomain;
		}

		//それぞれのSubDomainを格納
		//String userSubDom = userSite[0].Subdomain;
		String nnSubDom = nnSite[0].Subdomain;

		//カスタム表示ラベルから、secureサブドメイン値を取得
		String secureDomein = System.Label.I_SecureSubDomain;
		if(secureDomein != '.secure'){
			secureDomein ='';
		}
		//SubDomainを置き換える
		//String siteFullUrl = baseUrl.replace(userSubDom, nnSubDom);
		String siteFullUrl = baseUrl.replace(userSubDom, nnSubDom + secureDomein);

		//Domain以外の部分を結合
		String siteNN = 'https://' + siteFullUrl + '/'+ E_Const.NNLINK_NN;
		//遷移先ページと暗号化したパラメータを結合
		String confPage = siteNN +'/IRIS_ChangeUserIDCmpl?Param='+ cryptoParam;


		///////////GWログインユーザのメールアドレス変更
		//パラメータを暗号化
		String cryptoParamGW = E_EncryptUtil.getEncryptedString(recId);
		String confPageGW = E_Util.getURLForGWTB(siteNN) + '/IRIS_ChangeUserIDCmpl?Param='+ cryptoParamGW;

		//メール本文作成
		User userRec = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		String contactName = userRec.Contact.Name;

		String body = '';

		body = String.format(getMSG().get('MAB|001'),new String[]{contactName, confPage, confPageGW});

		//IRIS_ChangeUserIDCmplにパラメータを付与してメール送信
		//メール送信先、送信元、件名、本文を設定して送信
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[] { mailAddress });		//送信先アドレス
		//送信元アドレス設定
		OrgWideEmailAddress senderAdress = E_OrgWideEmailAddressDao.getOrgWideEmailAddress('IL-JP-sc_reg-change@nnlife.co.jp');
		mail.setOrgWideEmailAddressId(senderAdress.Id);
//		mail.setSenderDisplayName('NNLink');					//送信元名前
		mail.setSubject(System.Label.E_MailTitle_ChangeUserID);		//件名
		mail.setHtmlBody(body);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });		//送信
	}

	/**
	 * 新規メールアドレス重複チェック
	 * @return Boolean true:重複  false:重複していない
	 */
	private Boolean isOverlapUserName(){
		//メールアドレスの末尾にsuffixを付けてUsername完全一致検索
		User userRec = E_UserDaoWithout.getUserRecByUserName(mailAddress + System.Label.E_USERNAME_SUFFIX);

		//一致したユーザがあった場合、重複エラー
		if(userRec != null){
			return true;
		}
		return false;
	}
}