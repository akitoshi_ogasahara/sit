@isTest
private class TestE_AbstractExtender {

	/**
	 * E_AbstractExtenderの網羅テスト01
	 * doAuth
	 */
	static testMethod void testAbstractExtender01() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;

		PageReference pref = Page.E_ProcessHistory;

		// Ebiz連携ログ(E_BizDataSyncLog__c)
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DColiController controller = new E_DColiController(standardcontroller);
			E_DColiExtender extender = controller.getExtender();

			E_PageMessagesHolder msg = extender.pageMessages;
			Map<String, String> resultMap;
			resultMap = extender.getMSG();
			resultMap = extender.getDISCLAIMER();

			PageReference reference;
			reference = extender.doAuth(E_Const.ID_KIND.POLICY, policy.id);
			System.assertNotEquals(null, reference);
			reference = extender.doAuth(E_Const.ID_KIND.CONTACT, policy.id);
			System.assertNotEquals(null, reference);
			reference = extender.doAuth(E_Const.ID_KIND.USER, policy.id);
			System.assertNotEquals(null, reference);
			reference = extender.doAuth(E_Const.ID_KIND.INVESTMENT, policy.id);
			System.assertNotEquals(null, reference);
			reference = extender.doAuth(E_Const.ID_KIND.DOWNLOAD, policy.id);
			System.assertEquals(null, reference);
			reference = extender.doAuth(E_Const.ID_KIND.SPECIAL_FEATURES, policy.id);
			System.assertNotEquals(null, reference);
			reference = extender.doAuth(E_Const.ID_KIND.LOGIN, u.id);
			System.assertEquals(null, reference);

			Test.stopTest();
		}
	}

	/**
	 * E_AbstractExtenderの網羅テスト02
	 */
	static testMethod void testAbstractExtender02() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;

		PageReference pref = Page.E_ProcessHistory;

		// Ebiz連携ログ(E_BizDataSyncLog__c)
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DColiController controller = new E_DColiController(standardcontroller);
			E_DColiExtender extender = controller.getExtender();

			String resultStr;
			resultStr = extender.getPageTitle();
			System.assertEquals(resultStr, '契約内容個別照会(個人保険)');
			resultStr = extender.getDataSyncDate();
			System.assertEquals(resultStr, E_Util.formatDataSyncDate()  + '　' + '現在の契約内容');
			resultStr = extender.getDataSyncDateRegist();
			System.assertEquals(resultStr, E_Util.formatDataSyncDate()  + '　' + '現在のご登録内容');
			resultStr = extender.getDataSyncDateCustomer();
			System.assertEquals(resultStr, E_Util.formatDataSyncDate()  + '　' + '現在の契約/取引内容');
			resultStr = extender.getDataSyncDateInvestment();
			System.assertEquals(resultStr, E_Util.formatDataSyncDate()  + '　' + '現在');
			resultStr = extender.getWelcome();
			System.assertEquals(resultStr, E_Util.getWelcome());

			Boolean resultFlg;
			resultFlg = extender.getIsIss();
			System.assertEquals(resultFlg, E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId()));
			resultFlg = extender.getIsTrCodeFundRatio();
			System.assertEquals(resultFlg, extender.access.isTransactionCode(E_Const.TRCDE_TD05));
			resultFlg = extender.getIsTrCodeFundTransfer();
			System.assertEquals(resultFlg, extender.access.isTransactionCode(E_Const.TRCDE_TD06));
			resultFlg = extender.getIsTrCodeAddress();
			System.assertEquals(resultFlg, extender.access.isTransactionCode(E_Const.TRCDE_TDB0));

			Test.stopTest();
		}
	}

	/**
	 * E_AbstractExtenderの網羅テスト03
	 */
	static testMethod void testAbstractExtender03() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG04__c = '0';
		insert idcpf;
		E_Policy__c policy = new E_Policy__c();
		insert policy;

		PageReference pref = Page.E_ProcessHistory;

		// Ebiz連携ログ(E_BizDataSyncLog__c)
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;

		System.runAs(u){

			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_ADRPFConrtoller controller = new E_ADRPFConrtoller(standardcontroller);
			E_ADRPFExtender extender = controller.getExtender();

			E_DownloadController selfCon = new E_DownloadController();

			Boolean resultFlg;
			resultFlg = extender.getIsEmployee();
			System.assertEquals(true, resultFlg);
			resultFlg = extender.getIsAgent();
			System.assertEquals(false, resultFlg);
			resultFlg = extender.getIsCustomer();
			System.assertEquals(false, resultFlg);
			resultFlg = extender.getIsCallCenter();
			System.assertEquals(false, resultFlg);
			resultFlg = extender.getIsFundTransfer();
			System.assertEquals(false, resultFlg);
			resultFlg = extender.getIsInwardTransfer();
			System.assertEquals(false, resultFlg);
			resultFlg = extender.getIsContactInfo();
			System.assertEquals(false, resultFlg);

			selfCon.doReturn();
			selfCon.isValidate();

			String resultStr = extender.getCloseBackLabel();
			System.assertEquals(System.Label.E_BTN_BACK, resultStr);

			Test.stopTest();
		}
	}

}