public with sharing class E_IDCPFTriggerBizLogic {

	/*	###############################################################
	 *			例外クラス
	 *	############################################################### */
	public class E_PermissionSettingException extends Exception{}

	/*	###############################################################
	 *			ユーザ毎のPermission設定クラス
	 *	############################################################### */
	public class userPermission{
		private String userId;
		//最新のEIDCから『設定すべき』権限セットリスト
		public Set<String> permissionNames{get;set;}
	
		/*	Constructor */
		public userPermission(E_IDCPF__c eidc, Boolean isLargeAgency){
			this.userId = eidc.User__c;
			this.permissionNames = new Set<String>();

			// 住生IRIS
			// 代理店格コードが 『3C8WM』：User.Contact.E_AccParentCord__c
			// 募集人種別が 『02』 or 『03』：User.Contact.E_CL3PF_AGTYPE__c
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER)
					&&eidc.User__r.Contact.E_AccParentCord__c == E_Const.ZHEADAY_SUMISEI
					&&(eidc.User__r.Contact.E_CL3PF_AGTYPE__c == '02' || eidc.User__r.Contact.E_CL3PF_AGTYPE__c == '03')){
				this.permissionNames.add(E_Const.PERM_SUMISEI_IRIS);
				return;
			}

			//Base
			this.permissionNames.add(E_Const.PERM_BASE);
			//投信照会フラグ
			if(eidc.FLAG03__c == E_Const.IS_INQUIRY_1){	 
				this.permissionNames.add(E_Const.PERM_INVE);
			}
			//手続の履歴
			if(eidc.FLAG06__c ==	E_Const.IS_INQUIRY_1){		
				this.permissionNames.add(E_Const.PERM_ProceduralHistory);
			}
			//手数料明細書照会フラグ
			if(eidc.ZDSPFLAG01__c ==	E_Const.IS_INQUIRY_1){
				this.permissionNames.add(E_Const.PERM_Commision);
			}
			
			//PartnerCommunityユーザの場合にContent権限を付与	 ※CustomerCommunityには付与しない
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER)){
				this.permissionNames.add(E_Const.PERM_VIEWContent);
			}
			
			//ING社員
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_STANDARD)){	// 社員
				this.permissionNames.add(E_Const.PERM_Employee);
			}	 

			//BTMU
			if(eidc.user__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c == E_Const.ZHEADAY_BTMU){
				this.permissionNames.add(E_Const.PERM_BTMU);
			}

			//かんぽ
			if(eidc.user__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c == E_Const.ZHEADAY_KAMPO){
				this.permissionNames.add(E_Const.PERM_JPLife);
			}

			//JP
			if(eidc.user__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c == E_Const.ZHEADAY_JPLIFE){
				this.permissionNames.add(E_Const.PERM_JPLife);
			}
			
			/* setManualPermissions へ移動。　手動設定
			//メンテナンス	連携されない項目で判定
			if(eidc.IsMaintenanceCharge__c ){
			this.permissionNames.add(E_Const.PERM_Maintenance);
			}
			*/
			
			//---	代理店規定管理機能 start
			//代理店ユーザ用基本権限セット
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER) 
					&& eidc.ZSTATUS01__c ==	CR_Const.PW_STATUS_AGENCY
								&& eidc.ZDSPFLAG02__c == E_Const.ZDSPFLAG02_IRIS){	//TODO ID適正化
				this.permissionNames.add(CR_Const.PERMISSIONSET_AGENCY);
			
				//大規模代理店
				if(isLargeAgency){
					if(eidc.ZIDTYPE__c == E_Const.ZIDTYPE_AH){
						this.permissionNames.add(CR_Const.PERMISSIONSET_REPORTS);
						this.permissionNames.add(CR_Const.PERMISSIONSET_BOOKS);
					}else if(eidc.ZIDTYPE__c == E_Const.ZIDTYPE_AY){
						this.permissionNames.add(CR_Const.PERMISSIONSET_BOOKS);
					}
				}
			}
			
			//代理店規定管理（社員＆代理店ユーザ　外部委託）権限セット
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER) 
					&& eidc.ZSTATUS01__c ==	CR_Const.PW_STATUS_AGENCY			 //TODO ﾘﾌｧｸﾀ　　一般代理店ン判断ロジックを移行
					&& eidc.ZDSPFLAG02__c == E_Const.ZDSPFLAG02_IRIS	//TODO ID適正化
					&& eidc.ZIDTYPE__c != E_Const.ZIDTYPE_AT){
				this.permissionNames.add(CR_Const.PERMISSIONSET_OUTSRC);
			}
			
			//社員基本権限セット
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_STANDARD)){
				this.permissionNames.add(CR_Const.PERMISSIONSET_EMPLOYEE);
				this.permissionNames.add(CR_Const.PERMISSIONSET_OUTSRC);
				
				//手数料権限がある場合に、事業報告書と帳簿書類の権限を付与
				if(eidc.ZDSPFLAG01__c ==	E_Const.IS_INQUIRY_1){
					this.permissionNames.add(CR_Const.PERMISSIONSET_REPORTS);
					this.permissionNames.add(CR_Const.PERMISSIONSET_BOOKS);
				}
			}
			//---	代理店規定管理機能 end

			//---	AMS　自主点検　機能 start
			//一般代理店ユーザ（AT以外）
			//ユーザタイプ：PowerPartnerかつパスワードステータス：1かつID種別：ATでない又は、ID種別がATかつ管理責任者がYの場合に付与
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER) 
					&& eidc.ZSTATUS01__c ==	CR_Const.PW_STATUS_AGENCY			 //TODO ﾘﾌｧｸﾀ　　一般代理店ン判断ロジックを移行
					&& eidc.ZDSPFLAG02__c == E_Const.ZDSPFLAG02_IRIS	//TODO ID適正化
					&& ( eidc.ZIDTYPE__c != E_Const.ZIDTYPE_AT
					|| eidc.User__r.Contact.E_CL3PF_ZAGMANGR__c == 'Y')){
				this.permissionNames.add(SC_Const.PERMISSIONSET_SC_Agent);
			}

			//NN社員（拠点MR、拠点長、本社営業部MR、本社営業部拠点長）
			if(E_ProfileDao.isMR(eidc.user__r.profileId)
					||E_ProfileDao.isMRManager(eidc.user__r.profileId)
					||E_ProfileDao.isHQMR(eidc.user__r.profileId)
					||E_ProfileDao.isHQMRManager(eidc.user__r.profileId)){
				this.permissionNames.add(SC_Const.PERMISSIONSET_SC_NN);
			}

			//NN社員（エリア部長）	参照のみ
			if(E_ProfileDao.isAreaManager(eidc.user__r.profileId)){
				this.permissionNames.add(SC_Const.PERMISSIONSET_SC_Director);
			}
			
			//DPA,本社営業推進部,コンプライアンス部は運用管理者が手動で権限セットを付与する。
			//--- Atria権限管理 start
			//参照のみ（コールセンター部、営業推進部、本社営業関連部、営業推進部長）
			if(E_ProfileDao.isCallCenter(eidc.user__r.profileId)
					||E_ProfileDao.isSalesPromotion(eidc.user__r.profileId)
					||E_ProfileDao.isHOSDivision(eidc.user__r.profileId)
					||E_ProfileDao.isHQEManager(eidc.user__r.profileId)){
				this.permissionNames.add(E_Const.PERM_ATRAuthRead);
			}

			//更新可能（拠点長、ＭＲ、本社営業部拠点長、本社営業部MR、システム管理者）
			// 上記プロファイル 或いは(パートナーユーザ かつ 「管理責任者」が"Y"）
			if(E_ProfileDao.isMRManager(eidc.user__r.profileId)
					||E_ProfileDao.isMR(eidc.user__r.profileId)
					||E_ProfileDao.isHQMRManager(eidc.user__r.profileId)
					||E_ProfileDao.isHQMR(eidc.user__r.profileId)
					||E_ProfileDao.isSystemAdmin(eidc.user__r.profileId)
					|| (eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER)
						&& E_Util.null2Blank(eidc.User__r.Contact.E_CL3PF_ZAGMANGR__c).equals('Y'))) {
				this.permissionNames.add(E_Const.PERM_ATRAuthUpd);
			}
			//--- Atria権限管理 end
			
			//---	AMS　自主点検　機能 end

			//---	マーケティング　機能 start
			//一般代理店ユーザ
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_POWERPARTNER)
					&& eidc.ZSTATUS01__c ==	CR_Const.PW_STATUS_AGENCY
					&& eidc.ZDSPFLAG02__c == E_Const.ZDSPFLAG02_IRIS){	//TODO ID適正化
				this.permissionNames.add(MK_Const.PERMISSIONSET_MK_Agent);
			}
			//---	マーケティング　機能 end

			//---	挙績情報　機能 start
			//NN社員（拠点MR、拠点長、営業企画管理部、営業推進部、エリア部長）
			if(E_ProfileDao.isMR(eidc.user__r.profileId)
					||E_ProfileDao.isMRManager(eidc.user__r.profileId)
					||E_ProfileDao.isAreaManager(eidc.user__r.profileId)
					||E_ProfileDao.isPlanningManagement(eidc.user__r.profileId)
					||E_ProfileDao.isSalesPromotion(eidc.user__r.profileId)){
				this.permissionNames.add(E_Const.PERM_SREmployee);
			}
			//本社ユーザの場合は手動で付与
			if(eidc.IsShowSREmployee__c ){
				this.permissionNames.add(E_Const.PERM_SREmployee);
			}

			//---	挙績情報　機能 end
			
			//---	手動付与の権限セット　 start
			//社員ユーザ
			if(eidc.User__r.UserType.equals(E_Const.USERTYPE_STANDARD)){
				setManualPermissions(eidc);
			}
			//---	手動付与の権限セット　 end
		}
		
		/**
		 * ID管理の情報（各権限付与フラグ）を元に、手動で付与された権限セットを対象に追加
		 */
		private void setManualPermissions(E_IDCPF__c eidc){
			//メンテナンス	連携されない項目で判定
			if(eidc.IsMaintenanceCharge__c ){
				this.permissionNames.add(E_Const.PERM_Maintenance);
			}
			
			// 代理店規定管理（承認権限セット）
			if(eidc.IsCrApprove__c ){
				this.permissionNames.add(CR_Const.PERMISSIONSET_APPROVE);
			}
			
			// 代理店規程管理（DPA権限セット)
			if(eidc.IsCrDpa__c ){
				this.permissionNames.add(CR_Const.PERMISSIONSET_DPA);
			}
			
			// 自主点検（参照のみ）
			if(eidc.IsScDirector__c ){
				this.permissionNames.add(SC_Const.PERMISSIONSET_SC_Director);
			}
			
			// 自主点検（コンプラ）
			if(eidc.IsScAdmin__c ){
				this.permissionNames.add(SC_Const.PERMISSIONSET_SC_Admin);
			}

			//20180214 資料発送申込対応 START
			// 資料メンテナンス
			if(eidc.IsDocMaint__c ){
				this.permissionNames.add(E_Const.PERM_DocMaintenance);
			}
			//20180214 資料発送申込対応 END
		}

		/*	追加対象のPermissionAssignment	 reAssignメソッド内で設定される	 */
		public List<PermissionSetAssignment> additionalAssignments{get; private set;}

		/*	削除対象のPermissionAssignment	 reAssignメソッド内で設定される	 */
		public List<PermissionSetAssignment> deleteAssignments{get; private set;}

		/**
		 *	reAssign
		 */
		public void reAssign(List<PermissionSetAssignment> permAssigns){
			additionalAssignments = new List<PermissionSetAssignment>();
			deleteAssignments = new List<PermissionSetAssignment>();
			
			for(PermissionSetAssignment pa:permAssigns){
				if(this.permissionNames.contains(pa.PermissionSet.Name)){
					//設定すべき権限セットリスト(permissionNames)にすでに含まれている場合は、設定すべきリストから削除
					this.permissionNames.remove(pa.PermissionSet.Name);
				}else{	
					//設定すべき権限セットリスト(permissionNames)に含まれていない場合に削除対象とする。
					deleteAssignments.add(pa);
				}
			}
			
			for(String permName:this.permissionNames){
				Id permId = E_UserDao.getPermissionSetForNNLink().get(permName).Id;
				additionalAssignments.add(new PermissionSetAssignment(
											PermissionSetId = permId
											, AssigneeId = this.userId
											));
			}
		}
	}
	

	/*	###############################################################
	 *			権限セット設定　ビジネスロジック
	 *	############################################################### */
	private Map<Id, userPermission> userPermissionMap;
	private Map<Id, Boolean> agencyTypeMap;

	/*	Constructor */
	public E_IDCPFTriggerBizLogic(){
		userPermissionMap = new Map<Id, userPermission>();
		agencyTypeMap = new Map<Id, Boolean>();
	}
	
	/**	 
	 *	addTargetUsers	
	 *		権限セット付与のターゲットユーザリストの作成
	 */
	public void addTargetUsers(List<E_IDCPF__c> EIDCPFs){
		//大規模代理店判定内容を取得
		for(E_IDCPF__c rec:EIDCPFs){
			agencyTypeMap.put(rec.User__r.AccountId, false);
			agencyTypeMap.put(rec.User__r.Contact.Account.Parentid, false);
		}
		//大規模代理店指定のCR_Agencyレコードを取得できた場合にTrueにする。
		for(CR_Agency__c ag: CR_AgencyDao.getLargeAgencysByAccountIds(agencyTypeMap.keyset())){
			agencyTypeMap.put(ag.Account__c, true);
		}
		//Permissionセットの作成
		for(E_IDCPF__c rec:EIDCPFs){
			Boolean isLargeAgency = agencyTypeMap.get(rec.User__r.AccountId);
			if(!isLargeAgency){
				isLargeAgency = agencyTypeMap.get(rec.User__r.Contact.Account.ParentId);
				userPermissionMap.put(rec.User__c, new userPermission(rec, isLargeAgency));
			}
		}
	}
	
	/** +++++++++++++++++++++++++++++++++++++
	 *	PermissionSetAssign 
	 *		権限セット付与
	 */
	public void PermissionSetAssign(){
		List<PermissionSetAssignment> createTargets = new List<PermissionSetAssignment>();
		List<PermissionSetAssignment> deleteTargets = new List<PermissionSetAssignment>();
		
		try{
			Map<Id, User> users = E_UserDao.getUserWithPermissions(userPermissionMap.keyset());

			for(String uid:userPermissionMap.keyset()){
				User usr = users.get(uid);
				userPermission usrPerm = userPermissionMap.get(uid);
			
				usrPerm.reAssign(usr.PermissionSetAssignments);
				
				createTargets.addAll(usrPerm.additionalAssignments);
				deleteTargets.addAll(usrPerm.deleteAssignments);
				}

				//不要権限の削除
				if(!deleteTargets.isEmpty()){
				deletePermissionSets(deleteTargets);
			}

			//権限の付与
			if(!createTargets.isEmpty()){
				insertPermissionSets(createTargets);
			}
		}catch(Exception e){
			system.debug('確認' + e.getMessage() + '\n' + e.getStackTraceString());
			throw new E_PermissionSettingException(e.getMessage() + '\n' + e.getStackTraceString());
		}
	}
	
	/** +++++++++++++++++++++++++++++++++++++
	 *	PermissionSetDelete
	 *		権限セットをまとめて削除
	 */
	public void PermissionSetDelete(List<E_IDCPF__c> EIDCPFs){
		Set<Id> uIds = new Set<Id>();
		for(E_IDCPF__c rec:EIDCPFs){
			uIds.add(rec.User__c);
		}
	
		List<PermissionSetAssignment> deleteTargets = new List<PermissionSetAssignment>();	
		for(User usr:E_UserDao.getUserWithPermissions(uIds).values()){
			deleteTargets.addAll(usr.PermissionSetAssignments);
		}	
		if(!deleteTargets.isEmpty()){
			deletePermissionSets(deleteTargets);
		}
	}


	//PermissionSetAssignment　のInsert
	private void insertPermissionSets(List<PermissionSetAssignment> insPSAs){
		//GenericDao INSERT
		Map<SObject, Database.Error[]> insResult = E_GenericDao.insertPartial(insPSAs);

		if(insResult.size()>0){
			//関連先EBiz連携ログ
			E_bizDataSyncLog__c log = getLastDataSyncLog();
			createErrorNote(log.Id, E_GenericDao.DMLMODE_INS, insResult);
		}
	}
		
	//PermissionSetAssignment　のDelete
	private void deletePermissionSets(List<PermissionSetAssignment> delPSAs){
		//GenericDao Delete
		Map<SObject, Database.Error[]> delResult = E_GenericDao.deletePartial(delPSAs);

		if(delResult.size()>0){
			//関連先EBiz連携ログ
			E_bizDataSyncLog__c log = getLastDataSyncLog();
			createErrorNote(log.Id, E_GenericDao.DMLMODE_INS, delResult);
		}
	}
	

	/** ---------------------------------------------
	 *	createErrorNote
	 *		エラーログの生成（メモに出力はしない。Apexジョブへログを出力）
	 */
	private String createErrorNote(String parentId, String mode, Map<SObject, Database.Error[]> dmlResults){
		//Title
		String title = '【権限セット設定エラー】';
		
		//body
		String body = (mode==E_GenericDao.DMLMODE_INS?'追加エラー':'削除エラー');
		body += '\n';
		body += 'AssigneeId	,' + 'PermissionId	,' + 'エラーメッセージ';
		body += '\n';
		body += '---------------------------------------------------------------------';
		body += '\n';
		
		for(SObject rec:dmlResults.keyset()){
			PermissionSetAssignment psa = (PermissionSetAssignment)rec;
			body += psa.AssigneeId + ',';
			body += psa.PermissionSetId + ',';
			body += dmlResults.get(rec)[0];
			body += '\n';
		}
	
/*
	//メモレコードの作成	 ⇒MIXED_DMLが発生するので、メモにレコードは作成しない
	Note errorLog = new Note(	 parentId = parentId
					, title = title
					, body = body
					, isPrivate = false
				);
	//insert errorLog;
*/	
	throw new E_PermissionSettingException(title + '\n' + body);
	}
	
	
	/** ---------------------------------------------
	 *	getLastDataSyncLog
	 *		最新の Life/J→SFDC連携のログを取得
	 */
	private static E_bizDataSyncLog__c getLastDataSyncLog(){
		for(E_bizDataSyncLog__c log:[
					SELECT Id,Name
					FROM E_bizDataSyncLog__c
					WHERE Kind__c = '1'	 //区分=1　：　Life/J→SFDC
					ORDER By DataSyncStartDate__c DESC
					Limit 1]){
			return log;
		}
		return null;
	}

	private Map<String,Contact> conMap;
	private Map<Id,User> uMap;

	public void updateEIDCContact(List<E_IDCPF__c> newList){
		updateEIDCContact(newList, new Map<Id,E_IDCPF__c>() );
	}

	//ID管理更新・作成時、募集人(Contact__c)に値を入れる
	public void updateEIDCContact(List<E_IDCPF__c> newList, Map<Id,E_IDCPF__c> oldMap){

		//ユーザID
		Set<Id> uId = new Set<Id>();
		//所有者コード
		Set<String> zcode = new Set<String>();

		for(E_IDCPF__c eidc : newList){
			//処理対象外の場合処理しない
			if(!isTarget(eidc.ZIDTYPE__c, eidc.Contact__c)){
				continue;
			}
			//該当ユーザが存在する場合はユーザに紐づくContactIdを入れる
			if(eidc.User__c != null){
				uId.add(eidc.User__c);
			//ユーザが存在しない場合、ID管理の所有者コードをSet
			}else if(codeCheck(eidc)){
				String code = eidc.ZIDOWNER__c.replaceAll(E_Const.ZIDOWNER_AG,'');
				zcode.add(code);
			}
		}

		if(uId.isEmpty() && zcode.isEmpty()){
			return;
		}

		//所有者コードをキーにして一致するContactIdを取得
		//Map<String,Id> conMap = E_ContactDaoWithout.getRecsContactByCode(zcode);
		conMap = E_ContactDaoWithout.getRecsContactByCode(zcode);
		//UserIdをキーにして一致するContactIdを取得
		//Map<Id,Id> uMap = E_UserDaoWithout.getRecsContactById(uId);
		uMap = E_UserDaoWithout.getRecsContactById(uId);

		for(E_IDCPF__c eid : newList){
			setFieldHistoryTracking(eid);
			//募集人Idの設定 処理対象であればContactIdを設定
			if( isTarget(eid.ZIDTYPE__c, eid.Contact__c) ){
				eid.Contact__c = setContactId(eid); 
			}
			//ID管理の新規作成時には、手数料試算フラグのデフォルト値を設定			 
			if( oldMap.isEmpty() ){
				eid.AtriaCommissionAuthorityEditable__c = setAuthorityFlag(eid);
			}
		}
	}

	private Id setContactId( E_IDCPF__c rec ){
		//ユーザが存在する場合は、UserIdをキーにして一致するContactIdを入れる
		if(rec.User__c != null){
			return uMap.get(rec.User__c).Contact.Id;
		//ユーザが存在しない場合は、所有者コード-AGと一致するContactIdを入れる
		}else if(codeCheck(rec)){
			String agentcode = rec.ZIDOWNER__c.replaceAll(E_Const.ZIDOWNER_AG,'');
			return conMap.get(agentcode).Id;
		}
		return null;
	}

	//R1.1対応 手数料試算権限の初期値を設定する
	private Boolean setAuthorityFlag( E_IDCPF__c rec ){
		if(rec.User__c != null && uMap.containsKey( rec.User__c )){
			return initialAuthority( rec.ZDSPFLAG01__c, uMap.get(rec.User__c).Contact.E_CL3PF_ZAGMANGR__c );
		}else if(codeCheck(rec)){
			String agentcode = rec.ZIDOWNER__c.replaceAll(E_Const.ZIDOWNER_AG,'');
			return initialAuthority( rec.ZDSPFLAG01__c, conMap.get(agentcode).E_CL3PF_ZAGMANGR__c);
		}
		return false;	 
	}

	//手数料明細フラグ または 管理責任者フラグがYのときにtrueを返す
	private Boolean initialAuthority( String ZDSPFLAG, String ZAGMANGR){
		return ( ZDSPFLAG == '1' || ZAGMANGR == 'Y' );
	}

	//ID管理の所有者コード判定
	//所有者コードに値があり、前2桁がAGかつ7桁の場合に処理する
	private Boolean codeCheck(E_IDCPF__c ecode){
		if(ecode.ZIDOWNER__c != null && ecode.ZIDOWNER__c.substring(0,2) == E_Const.ZIDOWNER_AG && ecode.ZIDOWNER__c.length() == 7){
			return true;
		}
		return false;
	}

	//処理対象か判定する
	//ID種別：AH,AY,ATかつ募集人がnullならtrueを返す
	private Boolean isTarget(String idType, Id contactId){
		if(E_Const.EIDC_ZIDTYPES.contains(idType) && contactId == null){
			return true;
		}
		return false;
	}

	//1205作成
	//項目履歴管理用
	private final String EIDC_CUSTOMITEMSUFFIX = '__c';

	public void updateEidcpf(List<E_IDCPF__c> newList){
		updateEidcpf(newList,new Map<ID,E_IDCPF__c>());
	}
	public void updateEidcpf(List<E_IDCPF__c> newList,Map<ID,E_IDCPF__c> oldMap){
	//コメントアウト　ETLの準備が整うまでは有効化しない

		//対象eidcid
 /*		 Set<ID> eidcIds = new Set<ID>();

		//条件作成用ループ
		for(E_IDCPF__c newRec : newList){
			E_IDCPF__c oldRec = oldMap.get(newRec.id);
			//EIDR作成対象ID取得
			if(isCreateEIDR(newRec,oldRec)){
				eidcIds.add(newRec.id);
			}
		}

		//EIDCIDをキーとして未連携のEIDRを取得
		Map<Id,E_IDRequest__c> eidrMap = new Map<Id, E_IDRequest__c>();
		for(E_IDRequest__c eidr : [SELECT id ,E_IDCPF__c FROM E_IDRequest__c WHERE E_IDCPF__c in: eidcIds AND dataSyncDate__c = null AND ZUPDFLAG__c = 'M']){
			eidrMap.put(eidr.E_IDCPF__c,eidr);
		}

		List<E_IDRequest__c> idrs = new List<E_IDRequest__c>();
		//レコード設定用ループ
		for(E_IDCPF__c newRec : newList){
			E_IDCPF__c oldRec = oldMap.get(newRec.id);
			if(!isCreateEIDR(newRec, oldRec)){
		continue;
		}

		idrs.add(createEIDR(newRec, oldRec, eidrMap.get(newRec.id)));
		}
		upsert idrs;*/
	}

	/**
	 * EIDR作成
	 *
	private E_IDRequest__c createEIDR(E_IDCPF__c newRec, E_IDCPF__c oldRec, E_IDRequest__c eidr){
		//対象外は処理終了
		if(eidr == null || eidr.id == null){
			eidr = new E_IDRequest__c();
			eidr.E_IDCPF__c = newRec.id;
		}
		eidr.ZIDTYPE__c = newRec.ZIDTYPE__c;
		eidr.ZWEBID__c = newRec.ZWEBID__c;
		eidr.ZUPDFLAG__c = 'M';
		eidr.ZIDOWNER__c = newRec.ZIDOWNER__c;
		eidr.ZINQUIRR__c = newRec.ZINQUIRR__c;
		eidr.note__c = '予備フラグ更新';

		return eidr;
	}

	private String defaultStr = '0';
	private String null2Default(String value){
		if(String.isBlank(value)){
			value = defaultStr;
		}
		return value;
	}

	/**
	 * EIDR作成判定
	 *
	private Boolean isCreateEIDR(E_IDCPF__c newRec, E_IDCPF__c oldRec){

		if(oldRec != null && newRec.dataSyncDate__c == oldRec.dataSyncDate__c){
			if(newRec.ZDSPFLAG01__c != oldRec.ZDSPFLAG01__c
				|| newRec.ZDSPFLAG02__c != oldRec.ZDSPFLAG02__c
				|| newRec.ZDSPFLAG03__c != oldRec.ZDSPFLAG03__c
				|| newRec.ZDSPFLAG04__c != oldRec.ZDSPFLAG04__c
				|| newRec.ZDSPFLAG05__c != oldRec.ZDSPFLAG05__c
				|| newRec.ZDSPFLAG06__c != oldRec.ZDSPFLAG06__c
				|| newRec.ZDSPFLAG07__c != oldRec.ZDSPFLAG07__c
				|| newRec.ZDSPFLAG08__c != oldRec.ZDSPFLAG08__c
				|| newRec.ZDSPFLAG09__c != oldRec.ZDSPFLAG09__c
				|| newRec.ZDSPFLAG10__c != oldRec.ZDSPFLAG10__c){
				return true;
			}
		}
		return false;
	}
	/**
	 * 項目履歴監視項目の設定
	 * @param newRec EIDCレコード
	 */
	private void setFieldHistoryTracking(E_IDCPF__c newRec){
		//項目セットのループ
		for(String key : getFsMap().keyset()){
			//fhtの前方一致のみ処理
			if(key.indexOf('fht')!=0) continue;
			List<String> itemList = new List<String>();
			//項目セットの項目のループ
			for(Schema.FieldSetMember f : getFsMap().get(key).getFields()){
				itemList.add(f.getLabel() + ':' + E_Util.null2Blank(String.valueOf(newRec.get(f.getFieldPath()))));
			}
			newRec.put(key + EIDC_CUSTOMITEMSUFFIX,String.join(itemList, '|'));
		}
	}
	private Map<String, Schema.FieldSet> fsMap;
	private Map<String, Schema.FieldSet> getFsMap(){
		if(fsMap == null){
			fsMap = Schema.SObjectType.E_IDCPF__c.fieldSets.getMap();
		}
		return fsMap;
	}

	/*	###############################################################
	 *					ID管理-更新項目ログ出力　ビジネスロジック
	 *	############################################################### */

	//更新項目ログ出力対象項目API参照名リスト
	private static final List<String> auditFields = new List<String>{
		'AtriaCommissionAuthorityEditable__c'
	};

	//ログ出力文
	private static final String logTemp = '{0}が{1}の以下の項目を変更しました。';

	/** ID管理の項目マップ */
	private Map<String, Schema.SObjectField> fieldMap{
		get{
			if(fieldMap == null){
				fieldMap = Schema.getGlobalDescribe().get('E_IDCPF__c').getDescribe().fields.getMap();
			}
			return fieldMap;
		}
		private set;
	}

	//変更があった項目の情報をログ管理へ登録する
	public void outputItemChangedLog(Map<Id, E_IDCPF__c> oldMap, Map<Id, E_IDCPF__c> newMap){
		String userName = UserInfo.getName();
		List<String> changeFieldLabel = new List<String>();
		List<E_Log__c> outputLogs = new List<E_Log__c>();
		system.debug( 'newMap.keySet =' + newMap.keySet() );

		Map<Id, E_IDCPF__c> eidcMap = new Map<Id, E_IDCPF__c>(E_IDCPFDaoWithout.getUserNameByIds(newMap.keySet()));
		system.debug('eidcMap = ' + eidcMap);

		for (Id idcpfId : newMap.keySet()){
			changeFieldLabel = getChangeFieldLabel(oldMap.get(idcpfId), newMap.get(idcpfId));
			if (!changeFieldLabel.isEmpty()){
				E_Log__c elog = E_LogUtil.createLog();

				List<String> logDetail = new List<String>();
				Integer FieldSize = changeFieldLabel.size();
				for( Integer i = 0; i < FieldSize; i++ ){
					logDetail.add('項目名： ' + changeFieldLabel[i]);
					logDetail.add('変更前： ' + String.valueOf( oldMap.get(idcpfId).get(auditFields[i]) ));
					logDetail.add('変更後： ' + String.valueOf( newMap.get(idcpfId).get(auditFields[i]) ));
				}

				if( elog.AccessPage__c != null && elog.AccessPage__c.contains('&') ){
					elog.AccessPage__c = elog.AccessPage__c.left( elog.AccessPage__c.indexOf('&') ); //必要な部分のみを設定する
				}
				elog.ActionType__c = I_Const.ACTION_TYPE_AUTHORITY_CHANGE;
				elog.TargetUser__c = eidcMap.get(idcpfId).User__c;
				elog.Detail__c = String.join(logDetail, '\r\n');
				elog.Name = I_Const.ACTION_TYPE_AUTHORITY_CHANGE;
				//項目が上限を超える場合の切り捨てオプションを設定
				Database.DMLOptions dml = new Database.DMLOptions();
				dml.allowFieldTruncation = true;
				elog.setOptions(dml);

				outputLogs.add(elog);
			}

		}
		if (!outputLogs.isEmpty()) insert outputLogs;
	}

	//値に変更があった項目のラベル名を取得する
	private List<String> getChangeFieldLabel(E_IDCPF__c oldObj, E_IDCPF__c newObj){
		List<String> changeFieldLabel = new List<String>();
		for (String fieldApiName : auditFields){
			if (oldObj.get(fieldApiName) != newObj.get(fieldApiName)){
				changeFieldLabel.add(fieldMap.get(fieldApiName).getDescribe().getLabel());
			}
		}
		return changeFieldLabel;
	}

}