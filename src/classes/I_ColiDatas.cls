/**
 * IRIS 個人契約参照画面に表示するデータ管理クラス
 */
public with sharing class I_ColiDatas {

    /* 担当者一覧 */
    public class Charge{
    	public Boolean hasMainMr {get; set;}			// 主募集人存在フラグ
    	public String mainAgentName {get; set;}			// 主募集人名
    	public String mainAccountName {get; set;}		// 主事務所名
    	public String mainSplitc {get; set;}			// 主割合
    	public String mainMr {get; set;}				// 主担当MR
    	public String mainBr {get; set;}				// 主担当支社
    	public Boolean hasSubMr {get; set;}				// 従募集人存在フラグ
    	public String subAgentName {get; set;}			// 従募集人名
    	public String subAccountName {get; set;}		// 従事務所名
    	public String subSplitc {get; set;}				// 従割合
    	public String subMr {get; set;}					// 従担当MR
    	public String subBr {get; set;}					// 従担当支社

    }

    /* 保障内容 */
    public class Cover{
        public E_COVPF__c covpf {get; set;}    			// 個人保険特約
        public String hosyouNaiyou {get; set;}          // 保障内容
        public String hokenSyurui {get; set;}           // 保険種類
        public String genzaiHoken {get; set;}           // 保険金額/入院給付金日額/年金（月）額 :現在
        public String kanyuHoken {get; set;}            // 保険金額/入院給付金日額/年金（月）額 :加入時
        public String hokenryou {get; set;}             // 保険料(※1)(円)
        public String hokenKikan {get; set;}            // 保険期間
        public String haraikomiKikan {get; set;}        // 保険払込期間
        public String occdate {get; set;}				// 契約日
        public String chdrnum {get; set;}				// 証券番号
        public String zclname {get; set;}				// 契約者
        public String zinsname {get; set;}				// 被保険者
        public Boolean isMain {get; set;}				// 主契約判定
        public Boolean isTotal {get; set;}				// 合計行判定
        public Boolean isDesignation {get; set;}		// 指定代理人判定
        public Boolean isCondition {get; set;}			// 特約行判定

    }

    /* 契約者 */
    public class Contractor{
        public String name {get; set;}      // 契約者名
        public String nameKana {get; set;}  // 契約者名（カナ）
        public String clntnum {get; set;}   // 顧客番号
        public String address {get; set;}   // 契約者住所
        public String phone {get; set;}     // 電話番号
        public String birth {get; set;}     // 生年月日
        public Decimal age {get; set;}      // 年齢
		public String altAge {get; set;}    // 年齢（文字列）
    }

    /* 被保険者（主たる被保険者） */
    public class Insured{
        public String name {get; set;}      // 被保険者名
        public String nameKana {get; set;}  // 被保険者名（カナ）
        public String clntnum {get; set;}   // 顧客番号
        public String sex {get; set;}       // 性別
        public String birth {get; set;}     // 生年月日
        public Decimal age {get; set;}      // 現在年齢
        public Decimal anbccd {get; set;}   // 加入時年齢
        public String address {get; set;}   // 住所
        public String phone {get; set;}     // 電話番号
		public String altAge {get; set;}    // 現在年齢（文字列）
        public String altAnbccd {get; set;} // 加入時年齢（文字列）

    }

    /* 受取人 */
    public class Beneficiary{
        public String bnytp {get; set;}     // 受取人種別
        public String bnytpName {get; set;} // 受取人種別名
        public String clntnum {get; set;}   // 顧客番号
        public String name {get; set;}      // 受取人
        public String sex {get; set;}       // 性別
        public Decimal age {get; set;}      // 現在年齢
        public Decimal bnypc {get; set;}    // 受取割合
        public String altAge {get; set;}    // 現在年齢（文字列）
        public String altBnypc {get; set;}    // 受取割合（文字列）

    }

    /* 払込・口座情報 */
    public class Bank{
        public String zefreq {get; set;}        // 払込方法
        public String zadvdesc {get; set;}      // 前納
        public String zechinl {get; set;}       // 払込経路
        public String ptdate {get; set;}        // 次回払込
        public String pcesdte {get; set;}       // 最終払込
        public String zebknm {get; set;}        // 金融機関
        public String zebkbrnm {get; set;}      // 支店
        public String zebkType {get; set;}      // 預金種目/通帳記号
        public String bankacckey {get; set;}    // 口座番号/通帳番号
        public String zebkacow {get; set;}      // 口座名義人

    }

    /* 解約時受取金額 */
    public class Surrender{
        public Decimal surrenderValueSum {get; set;}    // 合計金額（円）
        public Decimal zcshval {get; set;}              // 解約返戻金（円）
        public Decimal zloantot {get; set;}             // 貸付金等精算額（円）
        public Decimal zeadvprm {get; set;}             // 前納未経過保険料（円）
        public Decimal zunprem {get; set;}              // 未経過期間に対応する額（円）
        public Boolean isViewZunprem {get; set;}        // [表示制御]未経過期間に対応する額（円）
        public String altCharSurrenderValueSum {get; set;}
        public String altCharZcshval {get; set;}
        public String altCharZunprem {get; set;}

    }

    /* 前納 */
    public class Advance{
        public String rcptdt {get; set;}      		// 前納保険料入金日
        public Decimal avprmrcv {get; set;}      	// 前納保険料額（円）
        public Decimal remnbann {get; set;}      	// 前納保険料の残回数
        public Boolean isViewAdvance {get; set;}    // 前納表示フラグ

    }

    /* 質権 */
    public class Pledge{
        public String haspledge {get; set;}      	// 質権
        public String zclname {get; set;}      		// 質権名

    }

    /* 契約者貸付 */
    public class Lone{
        public Decimal zepltot {get; set;}      // 契約者貸付合計(円)
        public Decimal zeplprn {get; set;}      // 契約者貸付元金(円)
        public Decimal zeplint {get; set;}      // 契約者貸付利息(円)
        public String zplcapt {get; set;}       // 契約者貸付繰入期間
        public Decimal zplrate {get; set;}      // 契約者貸付利率
        public Boolean isViewLone {get; set;}   // [表示制御]契約者貸付表示フラグ

    }

    /* 保険料自動振替貸付 */
    public class AutoLone{
        public Decimal zeapltot {get; set;}         // 保険料振替貸付合計(円)
        public Decimal zeaplprn {get; set;}         // 保険料振替貸付元金(円)
        public Decimal zeaplint {get; set;}         // 保険料振替貸付利息(円)
        public String zaplcapt {get; set;}          // 保険料振替貸付繰入期間
        public Decimal zaplrate {get; set;}         // 保険料振替貸付利率
        public Boolean isViewAutoLone {get; set;}   // [表示制御]振替貸付表示フラグ

    }

    /* 契約に関する通知 */
    public class Notification{
        public E_DownloadNoticeDTO dto {get; set;}
    }

    /* 手続履歴 */
    public class ProcessHistory{
        public String trandate {get; set;}		// 手続完了日
        public String ztrandsc {get; set;}		// 申し出内容
    }

    /* 特別条件割増削減 */
    public class Condition{
        public String MortalityClassKenkotaiYuryotai {get; set;}		//健康体
        public String MortalityClassWarimashi {get; set;}		//割増
        public String LienCondition {get; set;}		// 削減
        public Boolean TeikicoverFlg {get; set;}		// 定期特約判定
        public Boolean DDALDFlg {get; set;}		// ALD or DD判定
        public Boolean IsStandard {get; set;} //通常体フラグ
        public List<String> errorMsg {get; set;} //エラーの内容
        public Condition(){
            this.TeikicoverFlg = false;
            this.DDALDFlg = false;
            this.IsStandard = false;
            this.errorMsg = new List<String>();
        }
    }


}