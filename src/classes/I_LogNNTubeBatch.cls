global class I_LogNNTubeBatch implements Database.Batchable<sObject> ,Database.Stateful,Database.AllowsCallouts {
	
	private I_LogNNTubeResult result;
	private List<String> errorMessages;

	/**
	 * Constructor
	 */
	global I_LogNNTubeBatch() {
		
	}

	/**
	 * start
	 * 	アクションタイプが『動画』の最新のNNLinkLogを取得
	 */
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([Select 
												Id, AccessDatetime__c, BatchDate__c 
											From 
												E_Log__c 
											Where 
												ActionType__c = '動画' 
											And 
												BatchDate__c <> null 
											Order By 
												BatchDate__c DESC
											Limit 1]);
	}

	/**
	　*　execute
	　*/
	global void execute(Database.BatchableContext BC, List<E_Log__c> scope) {
		Http httpProtocol = new Http();
		this.result = new I_LogNNTubeResult();
		this.errorMessages = new List<String>();

		// アクセス日時が最新の動画のログ
		E_Log__c latestLog = scope[0];
		
		// 現在時刻
		Datetime now_dt = Datetime.now();
		
		// EndPoint取得
		String endpoint = getEndpoint(latestLog.batchDate__c, now_dt);

		// HttpRequest: NNTubeApi呼び出し
		HttpRequest request = new HttpRequest();
		request.setEndPoint(endpoint);
		request.setMethod('GET');

		// HttpResponse: 返却結果
		HttpResponse response = httpProtocol.send(request);

		// 結果JSON文字列
		String nntubeJsonResult = response.getBody();
		System.debug('■[nntubeJsonResult] ' + nntubeJsonResult);

		// 処理結果が正常か判定
		if(isSuccess(nntubeJsonResult)){
			try{
				// 返却されたJSONより、NNLinkLogを作成
				List<E_Log__c> movielogs = createLog(this.result.movieRows, now_dt);

				// Upsert
				List<Database.UpsertResult> saveResult;
				Schema.DescribeFieldResult F = E_Log__c.TubeLogSeq__c.getDescribe();
				Schema.sObjectField T = F.getSObjectField();
				saveResult = Database.upsert(movielogs, T, false);

				// エラー情報
				for(Integer i = 0; i < saveResult.size(); i++){
					if(!saveResult[i].isSuccess()){
						errorMessages.add('[' + movielogs[i].TubeLogSeq__c + ':' + saveResult[i].getErrors() + ']');
					}
				}
			}catch(Exception e){
				System.debug('Exception=' + e);
				errorMessages.add(String.valueOf(e));
			}
		}

		// エラーログレコード登録
		if(errorMessages.size()>0){
			E_log__c errLog = new E_log__c();
			errlog.Name = '動画ログエラー:' + getDatetime(System.Label.E_TUBELOG_FROMDATETIME, now_dt);
			errlog.IsError__c = true;
			errlog.batchDate__c = now_dt;
			errLog.Detail__c = String.join(errorMessages, ',');
			insert errlog;
		}
	}

	/**
	 * finish
	 */
	global void finish(Database.BatchableContext BC) {
		
	}

	/**
	 * EndPoint取得
	 */
	private String getEndpoint(Datetime latestLog_dt, Datetime now_dt){
		// ログメタ情報取得API　URL
		I_TubeConnectHelper connect = I_TubeConnectHelper.getInstance();
		String endpoint = connect.getTubeLogMetaUrl();
		
		// Parameter 『fromdatetime』 の取得　※カスタム表示ラベル 『E_TUBELOG_FROMDATETIME』 での値の設定可能
		String fromdatetime = getDatetime(System.Label.E_TUBELOG_FROMDATETIME, latestLog_dt);
		// Parameter 『todatetime』 の取得　※カスタム表示ラベル 『E_TUBELOG_FROMDATETIME』 での値の設定可能
		String todatetime = getDatetime(System.Label.E_TUBELOG_TODATETIME, now_dt);

		//fromdatetime と todatetime の時間のみ取り出し
		Integer fromhour = Integer.valueOf(fromdatetime.substring(8,10));
		Integer tohour = Integer.valueOf(todatetime.substring(8,10));
		System.debug(fromhour);
		System.debug(tohour);

		// Parameter
		Map<String,String> params = new Map<String,String>();
		params.put(I_Const.TUBE_PARAM_FROMDATETIME, fromdatetime);
		params.put(I_Const.TUBE_PARAM_TODATETIME, todatetime);
		
		// Hash生成
		String digestStr = createHash(System.label.E_TUBELOG_DIGEST_KEYWORD + params.get(I_Const.TUBE_PARAM_FROMDATETIME) + params.get(I_Const.TUBE_PARAM_TODATETIME));

		// 期間指定フラグ取得
		String termflg = getTermflg(fromhour, tohour);
		System.debug(termflg);

		//リクエスト情報のJSON作成
		String jsonInput = '{';
		jsonInput += '"digest":"' + digestStr + '"';
		jsonInput += ',"' + I_Const.TUBE_PARAM_FROMDATETIME + '":"' + fromdatetime + '"';
		jsonInput += ',"' + I_Const.TUBE_PARAM_TODATETIME + '":"' + todatetime + '"';
		jsonInput += ',"termflg":"' + termflg + '"';
		jsonInput += '}';

		//URLエンコード
		String encordUrl = EncodingUtil.urlEncode(jsonInput, 'UTF-8');
		endpoint = endpoint + '?info=' + encordUrl;
		
		return endpoint;
	}
	
	/**
	 * 対象日付の取得
	 * 	カスタム表示ラベルの値がセットされているかによって、日付を設定する
	 */
	private String getDatetime(String label, Datetime dt){
		String dt_str = '';
		if(label == '-'){
			dt_str = String.valueOf(dt.format('yyyyMMddHHmmss'));
		}else{
			dt_str = label;
		}
		return dt_str;
	}
	
	/**
	 * Hash生成
	 */
	private String createHash(String str){
		// Digest設定
		Blob b = Blob.valueOf(str);

		// アルゴリズム「SHA256」でハッシュ化 
		Blob digest = Crypto.generateDigest(I_Const.TUBE_DIGEST_ALGORITHM, b);

		// HTTP ダイジェスト認証のためにクライアント応答を計算
		return EncodingUtil.convertToHex(digest);
	}
	
	/**
	 * 期間指定フラグ取得
	 */
	private String getTermflg(Integer from_h, Integer to_h){
		String flg = '';
		
		// バッチ正常動作確認
		Integer hourComp = (to_h - from_h);

		// 日付を跨ぐときのパターン
		if(from_h == I_Const.BATCH_END_HOUR && hourComp < 0){
			flg = '0';
		
		// 通常パターン
		}else{ 
			if(hourComp == 0 || hourComp == 1){
				flg = '0';
			}else{
				flg = '1';
			}
		}
		
		return flg;
	}
	
	/**
	 * 処理結果が正常か判定
	 */
	private Boolean isSuccess(String tubereq){
		Boolean isSuccess = true;
		
		try{
			this.result.jsonResult = (I_LogNNTubeResult.NNTubeLogJsonResult)JSON.deserialize(tubereq, I_LogNNTubeResult.NNTubeLogJsonResult.class);
			System.debug(this.result.jsonResult);
			
			// ［エラー判定］ ResultCodeが正常でない
			if (this.result.jsonResult.resultcode != I_Const.TUBE_RESULTCODE_SUCCESS) {
				throw new ClassException(this.result.jsonResult.resultcode);
			}
			
			// ［エラー判定］ 取得件数0件
			if (this.result.jsonResult.data == null){
				throw new ClassException('動画ログが0件です。');
			}
			
			// ログレコード生成
			this.result.createMedias(this.result.jsonResult);
			
		}catch (ClassException e) {
			errorMessages.add(e.getMessage());
			isSuccess = false;
		} catch (Exception e) {
			this.result = null;
			isSuccess = false;
		}
		return isSuccess;
	}

	/**
	 * NNLinkLogレコード作成
	 */
	public List<E_Log__c> createLog(List<I_LogNNTubeResult.NNTubeMedia> movielog, Datetime todate){
		List<E_Log__c> logList = new List<E_Log__c>();
		
		for(I_LogNNTubeResult.NNTubeMedia logs : movielog){
			E_Log__c log = new E_Log__c();
			//ページ名
			log.Name = logs.tubeLogSeq;
			//一覧用ログ名
			log.SummaryName__c = logs.summaryName;
			// ログインユーザ
			if(String.isNotBlank(logs.accessUser)){
				log.AccessUser__c = logs.accessUser;
			}
			// アクセスページ
			log.AccessPage__c = logs.accessPage;
			// アクセス日時
			log.AccessDatetime__c = Datetime.valueOf(logs.accessDatetime);
			//アクションタイプ
			log.ActionType__c = logs.actionType;
			//動画一覧用URL
			//log.TubeDisplayURL__c = logs.displayUrl;
			log.Detail__c = logs.displayUrl;
			//利用アプリケーション
			log.AppName__c = logs.appName;
			//NNTubeログSEQ
			log.TubeLogSeq__c = logs.tubeLogSeq;
			//一覧表示フラグ
			log.isDisplay__c = true;
			//バッチ処理日付
			log.batchDate__c = todate;

			logList.add(log);
		}

		return logList;
	}
	
	/**
	 * Exception
	 */
	private class ClassException extends Exception {}
}