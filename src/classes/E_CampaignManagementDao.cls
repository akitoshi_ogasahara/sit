public with sharing class E_CampaignManagementDao {
	
	public static CampaignManagement__c getActiveRecByBaton(){
		List<CampaignManagement__c> recs = [Select id,
									Incentive__c,
									Information__c,
									Overview__c,
									Introduction__c,
									ContractantIncentive__c,
									title__c,
									E_MessageMaster_UpsertKey__c,
									IncentiveImportant__c
								From CampaignManagement__c
								Where FromDate__c <= toDay
								AND ToDate__c >= toDay
								Order by FromDate__c
								limit 1];
		return (recs.isEmpty())? new CampaignManagement__c(): recs[0];
	}
}