/**
 * PushOut通知のBachApexの抽象クラス
 */
public abstract class I_AbstractPushOutBatchOperation {
	// 
	protected Datetime dt;
	// 
	protected String queryCondition;
	// 通知対象レコードリスト
	protected List<SObject> records;
	// EBiz連携ログ
	public E_BizDataSyncLog__c ebizLog {get; set;}
	// 通知タイプ
	public String pType {get; set;}
	// サイトのドメイン
	public String siteDomain {get; set;}

	// クエリ条件
	public virtual void setQueryCondition(){}
    
    /** Database.QueryLocator */
    public virtual Database.QueryLocator getQueryLocator(){
    	String soql = '';
    	
    	// 通知タイプのクエリ作成
    	String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(pType);
    	Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(pType);
    	if(columns.isEmpty() == false){
	    	soql = ' Select Id';
	    	for(String col :columns){
	    		soql += ', ' + col;
	    	}
	    	soql += ' From ' + objApi;
	    	
	    	// Where句
	    	if(String.isNotBlank(queryCondition)){
	    		soql += queryCondition;
	    	}
    	}
    	system.debug(soql);
    	
    	return Database.getQueryLocator(soql);
    }
    
    /** 
     * execute
     */
    public abstract List<I_PushOut__c> execute(List<Sobject> records);

	/**
	 * finish
	 */
	public virtual void finish(List<I_PushOut__c> recs){}
	
	/**
	 * PushOut通知　メール送信バッチ呼び出し
	 */
	public virtual void callSendBatch(E_BizDataSyncLog__c ebizlog){
		I_PushOutSendBatch sendBatch = new I_PushOutSendBatch(ebizlog);
		Database.executeBatch(sendBatch, 200);
	}
       
    /**
     * ID管理よりPushOut通知レコード生成
     */
    protected virtual I_PushOut__c createRecordByIdcpf(E_IDCPF__c idcpf, String noticeType, Boolean isEmployee, String formatType){
    	// 共通情報セット
    	I_PushOut__c record = setCommonInfo(idcpf.User__r.Id, noticeType, isEmployee, idcpf.User__r.Email);

    	// UserIdをキーに外部IDを生成
    	if(isEmployee == false){
    		record.UpsertKey__c = noticeType + '|' + ebizLog.CreatedDate.format(formatType) + '|' + idcpf.User__r.Id + '|0';
    	}else{
    		record.UpsertKey__c = noticeType + '|' + ebizLog.CreatedDate.format(formatType) + '|' + idcpf.User__r.Id + '|1';
    	}
/*
    	// メールアドレスをキーに外部IDを生成
    	if(isEmployee == false){
    		record.UpsertKey__c = noticeType + '|' + ebizLog.CreatedDate.format(formatType) + '|' + idcpf.User__r.Email + '|0';
    	}else{
    		record.UpsertKey__c = noticeType + '|' + ebizLog.CreatedDate.format(formatType) + '|' + idcpf.User__r.Email + '|1';
    	}
*/
    	return record;
    }
    
    /** 
     * 共通情報セット
     */
    private I_PushOut__c setCommonInfo(Id userId, String noticeType, Boolean isEmployee, String email){
    	I_PushOut__c record = new I_PushOut__c();
    	record.Type__c = noticeType;
    	record.User__c = userId;
    	record.IsEmployee__c = isEmployee;
    	record.SiteDomain__c = siteDomain;
    	record.IsSended__c = false;
    	record.IsSendTarget__c = false;
    	record.SendEmail__c = email;
    	return record;
    }
     
    /**
     * PushOut通知レコードのマージ（UpsertKey）
     */
    protected virtual List<I_PushOut__c> mergeRecords(List<I_PushOut__c> recs){
		List<I_PushOut__c> mergeRecords = new List<I_PushOut__c>();
		
		Map<String, I_PushOut__c> upsertKeyMap = new Map<String, I_PushOut__c>();
		for(I_PushOut__c rec : recs){
			upsertKeyMap.put(rec.UpsertKey__c, rec);
		}
		
		for(String key : upsertKeyMap.keySet()){
			mergeRecords.add(upsertKeyMap.get(key));
		}
				
		return mergeRecords;
    }
    
	/**
	 * 送信ユーザ取得　Contact
	 * ID管理のUserより取得　※個人保険照会フラグが「1」
	 * @param Set<E_IDCPF__c> ContactId
	 * @return Set<User>
	 */
	protected Set<E_IDCPF__c> getAgentIdcpfs(Set<String> conIds){
		Set<E_IDCPF__c> agentIdcpfs = new Set<E_IDCPF__c>();
		
		if(conIds.isEmpty() == false){
			for(E_IDCPF__c idcpf : E_IDCPFDao.getNotificationRecsByUserContactId(conIds)){
				agentIdcpfs.add(idcpf);
			}
		}

		return agentIdcpfs;
	}
	
	/**
	 * 送信ユーザ取得　公開グループ
	 * ID管理の照会者コードより取得
	 * @param Set<String> 公開グループ
	 * @return Set<E_IDCPF__c>
	 */
	protected Set<E_IDCPF__c> getGroupIdcpfs(Set<String> grpCode){
		Set<E_IDCPF__c> groupIdcpfs = new Set<E_IDCPF__c>();
		
		if(grpCode.isEmpty() == false){
			for(E_IDCPF__c idcpf : E_IDCPFDao.getNotificationRecsByZINQUIRR(grpCode)){
				groupIdcpfs.add(idcpf);
			}
		}
		
		return groupIdcpfs;
	}
	
	/**
	 * 送信ユーザ取得　社内ユーザ
	 * プロファイルより取得
	 * @param
	 * @return Set<User>
	 */
	protected Set<E_IDCPF__c> getEmployeeIdcpfs(){
		Set<E_IDCPF__c> employeeIdcpfs = new Set<E_IDCPF__c>();
		
		for(E_IDCPF__c idcpf : E_IDCPFDao.getRecsByProfileNames(I_Const.EMPLOYEE_PROFILE_NAMES)){
			employeeIdcpfs.add(idcpf);
		}
		
		return employeeIdcpfs;
	}
}