public with sharing class I_TotalController {

	public String paramFilterType {get; set;}					// フィルタタイプ
	public String paramFilterValue {get; set;}					// フィルタ値
	public Boolean paramIsEmployee {get; set;}					// 社員フラグ
	public Boolean paramIsSelf {get; set;}						// 自身の情報参照フラグ

	public Map<String, I_MenuItem> topMenus {					// トップメニュー情報
		get{
			if(topMenus == null){
				I_TopController topController = new I_TopController();
				topMenus = topController.getTopMenus();
			}
			return topMenus;
		}
		set;
	}
	//データ基準日
	public String bizDate{
		get{
			if(bizDate != null) return this.bizDate;
			String inquiryD = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
			String year = inquiryD.left(4);
			String month = inquiryD.mid(4, 1)+inquiryD.mid(5, 1);
			String day = inquiryD.mid(6, 1)+inquiryD.mid(7, 1);
			return year+'/'+month+'/'+day;
		} set;
	}

	/**
	 * コンストラクタ
	 */
	public I_TotalController(){
		this.paramfilterType		= '';											// フィルタタイプ
		this.paramfilterValue		= '';											// フィルタ値
		this.paramIsEmployee		= false;										// 社員フラグ
		this.paramIsSelf			= false;										// 自身の情報参照フラグ
	}

	/**
	 * レコード情報取得(リモートアクション)
	 */
	@readOnly
	@RemoteAction
	public static String getRecordInfo(String getType, String filterType, String filterValue, Boolean isSelf){

		if((getType == 'New')){					// 新契約

			return I_TotalController.getNewAgreement(getType, filterType, filterValue);

		}else if((getType == 'NCOL')			// 未入金通知
			  || (getType == 'AGPO')			// 請求通知
			  || (getType == 'BILS')			// 予告通知
			  || (getType == 'BILA')){			// 異動通知

			return I_TotalController.getPossessionAgreement(getType, filterType, filterValue);

		}else if((getType == 'CounterM1')		// 契約応当日(1～3ヶ月後)
			  || (getType == 'CounterM4')){		// 契約応当日(4～6ヶ月後)

			return I_TotalController.getPossessionAgreementMonth(getType, filterType, filterValue, isSelf);

		}else if(getType == 'Ranking'){			// ランキング

			return I_TotalController.getRanking(getType, filterType, filterValue);

		}else if(getType == 'UpdateTime'){		// データ更新日時
			E_BizDataSyncLog__c biz = [SELECT DataSyncEndDate__c FROM E_BizDataSyncLog__c WHERE Kind__c = 'E' ORDER BY CreatedDate desc LIMIT 1];
			if(biz.DataSyncEndDate__c == null){
				return 'データ更新中';
			}
			return biz.DataSyncEndDate__c.format('yyyy/MM/dd HH:mm');

		}else if(getType == 'Period1'){			// 期間
			Date d = Date.today().addMonths(-1);
			String temp = Datetime.newInstance(d.year(), d.month(), 1).format('yyyy/MM/dd');
			temp += '～';
			d = Date.today();
			temp += Datetime.newInstance(d.year(), d.month(), d.day()).format('yyyy/MM/dd');
			return temp;

		}else if(getType == 'Period2'){			// 期間
			Date d	= E_DownloadNoticeUtil.getPeriodStartDateByObjApi(E_DownloadNoticeConst.OBJ_API_E_NCOL);
			d = Date.newInstance(d.year(), d.month(), 1);
			Date de	= d.addMonths(1).addDays(-1);
			return I_Util.toStr(d) + '～' + I_Util.toStr(de);
		}else if( getType == 'Period3'){
			Date d	= E_Util.SYSTEM_TODAY().toStartOfMonth().addMonths(1);
			Date de	= d.addMonths(6).addDays(-1);
			return I_Util.toStr(d) + '～' + I_Util.toStr(de);
		}

		return '';
	}

	/**
	 * 新契約の取得
	 */
	public static String getNewAgreement(String getType, String filterType, String filterValue){
		String conditions		= '';
		List<String> lines		= new List<String>();
		List<String> values		= new List<String>();

		// json項目名変換
		Map<String, String> setkeys = new Map<String, String>();
		setkeys.put(I_NewPolicyConst.STATUS_NAME_I, 'est');			// 成立
		setkeys.put(I_NewPolicyConst.STATUS_NAME_Z, 'ack');			// 謝絶
		setkeys.put(I_NewPolicyConst.STATUS_NAME_Q, 'wit');			// 取下げ
		setkeys.put(I_NewPolicyConst.STATUS_NAME_QZ,'ackwit');		// 謝絶/取下げ
		setkeys.put(I_NewPolicyConst.STATUS_NAME_P1,'arr');			// 申込書類到着済
		setkeys.put(I_NewPolicyConst.STATUS_NAME_P2,'def');			// 不備対応中
		setkeys.put(I_NewPolicyConst.STATUS_NAME_P3,'acc');			// 査定完了入金待ち
		setkeys.put(I_NewPolicyConst.STATUS_NAME_P4,'ass');			// 査定中
		// カウント集計
		Map<String, integer> counts = new Map<String, integer>();
		for(String Key : setkeys.keySet()){
			counts.put(setkeys.get(Key), 0);
		}

		// 追加条件
		if(filterType == I_Const.CONDITION_FIELD_UNIT){
			// 営業部
			conditions += ' AND (Account__r.E_CL2PF_BRANCH__c = \'' + filterValue + '\' ';
			conditions += '  OR  Account__r.E_CL2PF_ZBUSBR__c = \'' + filterValue + '\' ';
			conditions += '  OR  SubAccount__r.E_CL2PF_BRANCH__c = \'' + filterValue + '\' ';
			conditions += '  OR  SubAccount__r.E_CL2PF_ZBUSBR__c = \'' + filterValue + '\') ';
		}else if(filterType == I_Const.CONDITION_FIELD_MR){
			// MR
			conditions += ' AND (Account__r.OwnerId = \'' + filterValue + '\' ';
			conditions += '  OR  SubAccount__r.OwnerId = \'' + filterValue + '\') ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENCY){
			// 代理店格
			conditions += ' AND (ParentAccount__c = \'' + filterValue + '\' ';
			conditions += '  OR  SubParentAccount__c = \'' + filterValue + '\') ';
		}else if(filterType == I_Const.CONDITION_FIELD_OFFICE){
			// 代理店事務所
			conditions += ' AND (Account__c = \'' + filterValue + '\' ';
			conditions += '  OR  SubAccount__c = \'' + filterValue + '\') ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENT){
			// 募集人
			conditions += ' AND (Agent__c = \'' + filterValue + '\' ';
			conditions += '  OR  SubAgent__c = \'' + filterValue + '\') ';
		}

		// 個人の件数取得
		List<AggregateResult> recAggs = E_NewPolicyDao.getNewPolicyForCount(conditions);
		for(AggregateResult recAgg : recAggs){
			String status	= (String)recAgg.get('SystemStatus__c');	// 発信日ステータス
			Integer cnt		= (Integer)recAgg.get('cnt');				// カウント
			String key		= setkeys.get(status);
			if(counts.containsKey(key)){
				counts.put(key, counts.get(key) + cnt);
			}
		}

		// 団体の件数取得
		recAggs = E_NewPolicyDao.getNewPolicyForGroupCount(conditions);
		for(AggregateResult recAgg : recAggs){
			String status	= (String)recAgg.get('GroupStatus__c');		// 団体ステータス
			String key		= setkeys.get(status);
			if(counts.containsKey(key)){
				counts.put(key, counts.get(key) + 1);
			}
		}

		// JSONを作成
		for(String Key : counts.keySet()){
			values.add('"' + Key + '":' + I_Util.toStr(counts.get(Key)) + '');
		}
		return String.join(values,',');
	}

	/**
	 * 保有契約の取得
	 */
	public static String getPossessionAgreement(String getType, String filterType, String filterValue){
		Map<String, String> soqls	= new Map<String, String>();
		String soql			= '';
		String objectName	= '';
		String conditions	= '';
		Integer cnt			= 0;

		// 各種設定
		if(getType == 'NCOL'){
			// 未入金通知
			objectName = E_DownloadNoticeConst.OBJ_API_E_NCOL;

		}else if(getType == 'AGPO'){
			// 請求通知
			objectName = E_DownloadNoticeConst.OBJ_API_E_AGPO;

		}else if(getType == 'BILS'){
			// 予告通知
			objectName = E_DownloadNoticeConst.OBJ_API_E_BILS;

		}else if(getType == 'BILA'){
			// 異動通知
			objectName = E_DownloadNoticeConst.OBJ_API_E_BILA;
		}

		// 追加条件
		if(filterType == I_Const.CONDITION_FIELD_UNIT){
			// 営業部
			conditions += ' AND (SearchBRNO__c = \'' + filterValue + '\' ';
			conditions += '  OR  SearchMainBRNO__c = \'' + filterValue + '\') ';
		}else if(filterType == I_Const.CONDITION_FIELD_MR){
			// MR
			conditions += ' AND SearchOwnerId__c = \'' + filterValue + '\' ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENCY){
			// 代理店格
			conditions += ' AND SearchParentAccountId__c = \'' + filterValue + '\' ';
		}else if(filterType == I_Const.CONDITION_FIELD_OFFICE){
			// 代理店事務所
			conditions += ' AND SearchAccountId__c = \'' + filterValue + '\' ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENT){
			// 募集人
			conditions += ' AND SearchContactId__c = \'' + filterValue + '\' ';
		}

		if(conditions == '') conditions = I_NoticeAbstractController.getSoqlWhereByUser();
		soqls = I_NoticeTopController.getRECORD_SIZE_SOQL(objectName, conditions);

		// 件数取得処理
		for (String key : soqls.keySet()) {
			try{
				soql = soqls.get(key);
				List<AggregateResult> agResult = Database.query(soql);
				cnt += Integer.valueOf(agResult[0].get('expr0'));
			}catch(Exception e){
				return 'レコード件数取得処理でエラーが発生しました。   ' + e.getMessage();
			}
		}
		return String.valueOf(cnt);
	}
	/**
	 * 保有契約の取得(月後)
	 */
	public static String getPossessionAgreementMonth(String getType, String filterType, String filterValue, Boolean isSelf){
		Map<String, String> soqls		= new Map<String, String>();
		Map<String, String> conditions	= new Map<String, String>();
		String condition	= '';
		String soql			= '';
		Integer monthFrom	= 0;
		Integer cnt			= 0;

		// 各種設定
		if(getType == 'CounterM1'){
			// 契約応当日(1～3ヶ月後)
			monthFrom	= 1;

		}else if(getType == 'CounterM4'){
			// 契約応当日(4～6ヶ月後)
			monthFrom	= 4;
		}

		// 追加条件
		condition = '';
		if(filterType == I_Const.CONDITION_FIELD_UNIT){
			// 営業部
			condition += ' AND (E_Policy__r.MainAgentAccount__r.E_CL2PF_BRANCH__c = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.MainAgentAccount__r.E_CL2PF_ZBUSBR__c = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.SubAgentAccount__r.E_CL2PF_BRANCH__c = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.SubAgentAccount__r.E_CL2PF_ZBUSBR__c = \'' + filterValue + '\') ';
		}else if(filterType == I_Const.CONDITION_FIELD_MR){
			// MR
			condition += ' AND (E_Policy__r.MainAgentAccount__r.OwnerId = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.SubAgentAccount__r.OwnerId = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENCY){
			// 代理店格
			condition += ' AND (E_Policy__r.MainAgentAccountParentId__c = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.SubAgentAccountParent__c = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_OFFICE){
			// 代理店事務所
			condition += ' AND (E_Policy__r.MainAgentAccount__c = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.SubAgentAccount__c = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENT){
			// 募集人
			condition += ' AND (E_Policy__r.MainAgent__c = \'' + filterValue + '\' ';
			condition += '  OR  E_Policy__r.SubAgent__c = \'' + filterValue + '\' ) ';
		}
		// E_COVPF__c用条件 →(変換)→  E_Policy__c用条件
		conditions.put('POLICY_Group',condition);
		conditions.put('POLICY_Indivi',condition.replace('E_Policy__r.', ''));

		soqls = I_PolicysController.getRECORD_SIZE_SOQL(monthFrom);

		// 条件の削除・追加
		for (String key : soqls.keySet()) {
			soql = soqls.get(key);
			if(isSelf == false){
				soql = soql.replace('AND PolicyInCharge__c=true','');
				soql = soql.replace('AND E_Policy__r.PolicyInCharge__c=true','');
			}
			List<String> temp = soql.split('group by ');
			temp.set(0, temp.get(0) + ' ' + conditions.get(key));
			soqls.put(key, String.join(temp,'group by '));
			// system.debug('getPossessionAgreementMonth:' + soqls.get(key));
		}

		// 件数取得処理
		for (String key : soqls.keySet()) {
			try{
				soql = soqls.get(key);
				List<AggregateResult> agResult = Database.query(soql);
				if(key == 'POLICY_Group'){
					cnt += agResult.size();
				}else{
					cnt += Integer.valueOf(agResult[0].get('expr0'));
				}
			}catch(Exception e){
				return 'レコード件数取得処理でエラーが発生しました。   ' + e.getMessage();
			}
		}
		return String.valueOf(cnt);

	}
	/**
	 * 利用履歴の取得
	 */
	public static String getRanking(String getType, String filterType, String filterValue){
		String soql			= '';
		String conditions	= '';
		// 追加条件
		if(filterType == I_Const.CONDITION_FIELD_UNIT){
			// 営業部
			conditions = ' AND ( Contact.Account.E_CL2PF_BRANCH__c = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_MR){
			// MR
			conditions = ' AND ( Contact.Account.OwnerId = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENCY){
			// 代理店格
			conditions = ' AND ( Contact.Account.ParentId = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_OFFICE){
			// 代理店事務所
			conditions = ' AND ( Contact.AccountId = \'' + filterValue + '\' ) ';
		}else if(filterType == I_Const.CONDITION_FIELD_AGENT){
			// 募集人
			conditions = ' AND ( ContactId = \'' + filterValue + '\' ) ';
		}

		// 過去1ヶ月以内のログがある募集人リストを作成
		I_UsersController usersController	= new I_UsersController();
		usersController.rowCount			= 10;		// 表示行数
		usersController.sortType			= I_UsersController.SORT_TYPE_LOGIN_COUNT;		// ソートキー
		usersController.sortIsAsc			= false;	// ソート順
		usersController.searchType			= 'all';	// 検索タイプ
		usersController.createUserList(conditions);

		// JSONを作成
		List<String> values			= new List<String>();
		List<String> lines			= new List<String>();
		for(I_UsersController.UserListRow userListRow : usersController.userList){
			values			= new List<String>();
			values.add('"agentId":"' + userListRow.agentId + '"');			// 募集人ID
			values.add('"agentName":"' + userListRow.agentName + '"');		// 募集人名
			values.add('"officeName":"' + userListRow.officeName + '"');	// 事務所名
			values.add('"loginCount":"' + userListRow.loginCount + '"');	// 過去1ヶ月のログイン回数
			lines.add('{' + String.join(values,',') + '}');
		}
		return String.join(lines,',');
	}

	/**
	 * 指定ページへ遷移
	 */
	public PageReference moveToPage() {
		// 遷移用パラメータ取得
		String urlType		= ApexPages.currentPage().getParameters().get('next_urltype');
		String filterType	= ApexPages.currentPage().getParameters().get('next_filtertype');
		String filterValue	= ApexPages.currentPage().getParameters().get('next_filtervalue');

		// フィルターモード用 cookie設定
		E_IRISHandler.setPolicyCondition(filterType, filterValue);

		// 遷移先URL設定
		Pagereference pr = new PageReference(((String.isBlank(urlType))?'':'/apex/' + urlType));
		pr.setRedirect(true);

		if(urlType == 'IRIS_Users'){
			List<I_MenuMaster__c> menus = I_MenuMasterDao.getRecordsByLinkMenuKey('iris_log');
			if(menus.size() == 1) pr.getParameters().put(I_Const.URL_PARAM_MENUID, menus.get(0).id);
		}
		return pr;
	}
}