@isTest
private class TestE_ADRPFConrtoller {
	
	private static testMethod void testPageMethods() {
		E_ADRPFConrtoller extension = new E_ADRPFConrtoller(new ApexPages.StandardController(new E_ADRPF__c()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
        extension.getExtender();
    }
    @isTest(SeeAllData=true)
    public static void test_loadReferenceValues_zipcode() {
        String testReferenceId = '';
        E_ADRPFConrtoller extension = new E_ADRPFConrtoller(new ApexPages.StandardController(new E_ADRPF__c()));
        extension.loadReferenceValues_zipcode();

        if (testReferenceId == '') {
            try {
                SkyEditor2.TestData testdata = new SkyEditor2.TestData(E_CADPF__c.getSObjectType());
                SObject parent = testdata.newSObject();
                insert parent;
                testReferenceId = parent.Id;
            } catch (Exception e) {
                List<E_CADPF__c> parents = [SELECT Id FROM E_CADPF__c LIMIT 1];
                if (parents.size() == 0) {
                    throw new SkyEditor2.Errors.TestDataException(SkyEditor2.Messages.TEST_DATA_CREATION_FAILURE, e);
                } else {
                    testReferenceId = parents[0].Id;
                }
            }
        }
        E_CADPF__c parent = [SELECT Id,ZADRSKJ__c,Reserve__c,ZADRSKN__c,Name FROM E_CADPF__c WHERE Id = :testReferenceId];
        extension.record.E_CADPF__c = parent.Id;
        extension.loadReferenceValues_zipcode();

        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.ZNKJADDR__c)) {
            System.assertEquals(parent.ZADRSKJ__c, extension.record.ZNKJADDR__c);
        }
        System.assert(true);


        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.ZNKJADDR01__c)) {
            System.assertEquals(parent.Reserve__c, extension.record.ZNKJADDR01__c);
        }
        System.assert(true);


        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.ZNKJADDR02__c)) {
            System.assertEquals(parent.Reserve__c, extension.record.ZNKJADDR02__c);
        }
        System.assert(true);


        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.ZADRSKN__c)) {
            System.assertEquals(parent.ZADRSKN__c, extension.record.ZADRSKN__c);
        }
        System.assert(true);


        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.ZNCLTADR01__c)) {
            System.assertEquals(parent.Reserve__c, extension.record.ZNCLTADR01__c);
        }
        System.assert(true);


        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.ZNCLTADR02__c)) {
            System.assertEquals(parent.Reserve__c, extension.record.ZNCLTADR02__c);
        }
        System.assert(true);


        if (SkyEditor2.Util.isEditable(extension.record, E_ADRPF__c.fields.CLTPCODE__c)) {
            System.assertEquals(parent.Name, extension.record.CLTPCODE__c);
        }
        System.assert(true);
    }
}