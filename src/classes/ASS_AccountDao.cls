/**
 * 営業日報データアクセスオブジェクト
 * CreatedDate 2018/04/27
 * @Author Terrasky
 */

public class ASS_AccountDao {
	/**
	 * 代理店事務所取得
	 * @param officeId :事務所ID
	 * @return Account: 代理店事務所
	 */
	public static Account getOfficeById(String officeId) {
		return [SELECT 
					Name
					, CLTPHONE01__c
					, KJADDR01__c
				FROM 
					Account 
				WHERE 
					Id = :officeId
				];
	}

	/**
	 * 代理店事務所リスト取得
	 * @param mrId :ユーザID
	 * @param officeId :事務所ID
	 * @return List<Account>: 代理店事務所リスト
	 */
	public static List<Account> getOfficesById(String mrId, String officeId) {
		String soql = 'SELECT '
						+ 'Id '
						+ ', Name '
						+ ', E_CL2PF_ZEAYKNAM__c '
					+ ' FROM '
						+ 'Account'
					+ ' WHERE '
						+ 'OwnerID = :mrId'
					+ ' AND '
						+ 'E_COMM_VALIDFLAG__c = \'1\' ';

		if(String.isNotEmpty(officeId)){
			soql += ' AND Id = :officeId ';
		}
		return Database.query(soql);
	}
	/**
	 * 代理店事務所リスト取得
	 * @param MRId :MRID
	 * @param loginUsId :ログインユーザのID
	 * @return List<Account>: 代理店事務所リスト
	 */
	public static List<Account> getOfiicesByMRIdANDLoginUsId(String mrId, String loginUsId) {
		return  [SELECT 
					Name 
				FROM 
					Account 
				WHERE 
					(OwnerId = :mrId
				OR 
					OwnerId = :loginUsId)
				AND 
					E_COMM_VALIDFLAG__c = '1'
				];
	}
}