/*
 * CC_CaseEditVOIController
 * Controller class of CC_CaseEditOmniScript
 * created  : Accenture 2018/10/18
 * modified :
 */

global with sharing class CC_CaseEditVOIController implements Vlocity_ins.VlocityOpenInterface2{
	global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);

		Boolean success = true;
		try{
			//画面初期化処理
			if(methodName == 'initCase'){
				success = CC_CaseEditVOIBizLogic.initCase(inputs, output, options);

			//画面初期化時'帳票出力'セクションのデータ取得
			}else if(methodName == 'extractSRPolicy'){
				success = CC_CaseEditVOIBizLogic.extractSRPolicy(inputs, output, options);

			//画面から'保存'実施する場合
			}else if(methodName == 'saveCase'){
				options.put('methodName', (Object)methodName);
				success = CC_CaseEditVOIBizLogic.saveCase(inputs, output, options);

			//画面から'帳票出力'実施する場合
			}else if(methodName == 'SRFormPrint'){
				options.put('methodName', (Object)methodName);
				success = CC_CaseEditVOIBizLogic.saveCase(inputs, output, options);

			//画面から'SaveCaseConfirmed'実施する場合（バリデーション実行しない）
			}else if(methodName == 'saveCaseConfirmed'){
				options.put('methodName', (Object)methodName);
				success = CC_CaseEditVOIBizLogic.saveCase(inputs, output, options);

			//画面から'SRFormPrintConfirmed'実施する場合（バリデーション実行しない）
			}else if(methodName == 'SRFormPrintConfirmed'){
				options.put('methodName', (Object)methodName);
				success = CC_CaseEditVOIBizLogic.saveCase(inputs, output, options);

			//代理店事務所コードTypeAheadの取得
			}else if(methodName == 'getAgencyTypeAhead'){
				success = CC_CaseEditVOIBizLogic.getAgencyTypeAhead(inputs, output, options);
			}

			if(Test.isRunningTest()) throw new DMLException();

		}catch(Exception e){
			System.debug('Error invoke method: ' + methodName + ' with error: '+ e);
			output.put('error', e);
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}

}