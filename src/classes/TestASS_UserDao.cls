@isTest
private class TestASS_UserDao {

	private static testMethod void getUserByIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			User retUser = ASS_UserDao.getUserById(mr.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retUser.Id,mr.Id);
		}
	
	}

	private static testMethod void getOtherUsersByUnitNameTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<User> retUsers = ASS_UserDao.getOtherUsersByUnitName('テスト1営業部',actUser.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retUsers.size(),1);
			System.assertEquals(retUsers[0].Id,mr.Id);			
		}
	
	}
}