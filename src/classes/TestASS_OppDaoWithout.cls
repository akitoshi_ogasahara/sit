@isTest
private class TestASS_OppDaoWithout {
	//エリア部長で実行
	private static testMethod void getThisMonthOppsByOffIdSetTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Opportunity> retOppList = ASS_OppDaoWithout.getThisMonthOppsByOffIdSet(officeMap.keySet());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retOppList.size(),2);
		}
	}

	private static testMethod void getThisMonthOppsByOffIdSetAndLevelTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			//見込案件対象レベル
			String[] level = new String[]{'S','A','B','C','D'};
			List<Opportunity> retOppList = ASS_OppDaoWithout.getThisMonthOppsByOffIdSetAndLevel(officeMap.keySet(),level);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retOppList.size(),1);
		}
	}

	private static testMethod void getOppsByOfficeIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			//見込案件対象レベル
			String[] level = new String[]{'S','A','B','C','D'};
			List<Opportunity> retOppList = ASS_OppDaoWithout.getOppsByOfficeId(offList[0].Id,level);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retOppList.size(),1);
		}
	}

	//MRで実行
	private static testMethod void getThisMonthOppsByOffIdSetMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Opportunity> retOppList = ASS_OppDaoWithout.getThisMonthOppsByOffIdSet(officeMap.keySet());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retOppList.size(),2);
		}
	}

	private static testMethod void getThisMonthOppsByOffIdSetAndLevelMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			//見込案件対象レベル
			String[] level = new String[]{'S','A','B','C','D'};
			List<Opportunity> retOppList = ASS_OppDaoWithout.getThisMonthOppsByOffIdSetAndLevel(officeMap.keySet(),level);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retOppList.size(),1);
		}
	}

	private static testMethod void getOppsByOfficeIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = ptprod.Id, AccountId = offList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			//見込案件対象レベル
			String[] level = new String[]{'S','A','B','C','D'};
			List<Opportunity> retOppList = ASS_OppDaoWithout.getOppsByOfficeId(offList[0].Id,level);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retOppList.size(),1);
		}
	}
}