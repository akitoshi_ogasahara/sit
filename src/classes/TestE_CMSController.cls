@isTest
private class TestE_CMSController
{
	/**
     * E_CMSController 分岐網羅
     */
    private static testMethod void testCMSController1() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	//テストデータ作成
            E_MenuMaster__c menuMaster1 = new E_MenuMaster__c(MenuMasterKey__c='key1',SelectedMenuKey__c='key1');
            insert menuMaster1;

        	List<E_MessageMaster__c> insMessage = new List<E_MessageMaster__c>();
	        String type = 'メッセージ';
	        String param = 'CPL|001';

	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true,ShowHTMLByAttachment__c = true));
	        param = 'CPL|002';
	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        param = 'CPL|003';
	        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type,ShowSubPageLink__c = true,Menu__c = menuMaster1.Id,ShowPicsByAttachment__c = true));
	        insert insMessage;

	        RecordType rtype = [Select Id From RecordType Where DeveloperName = 'pattern03' Limit 1];
            E_CMSFile__c cmsFile = new E_CMSFile__c(ContentsName__c='ContentsName');
            cmsFile.RecordTypeId = rtype.Id;
            cmsFile.MessageMaster__c = insMessage.get(0).Id;
            insert cmsFile;
            
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf('<p>');
            attachment.Name = String.valueOf('test.html');
            attachment.ParentId = insMessage.get(0).Id;
            insert attachment;

            ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
            content.VersionData = Blob.valueOf('Testing base 64 encode');
            insert content;

            List<E_MessageMaster__c> messageMasterList = E_MessageMasterDao.getMessagesByMenuId(menuMaster1.Id);

            //テスト開始
            Test.startTest();
            E_CMSController cms = new E_CMSController();
            cms.cmsMsg = messageMasterList.get(0);
            cms.getSubPages();
            cms.getAttachments();
            cms.getFileList01();
            cms.getFileList02();
            cms.getFileList03();
            cms.getFileList04();
            cms.getFileList05();
            cms.getFileList06();
            cms.getFileList07();
            cms.getFileInfoMap();
            cms.getContents();

            cms = new E_CMSController();
            cms.cmsMsg = insMessage.get(0);
            cms.getFileInfoMap();

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}