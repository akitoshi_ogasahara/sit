global with sharing class E_BankExtender extends E_AbstractViewExtender{
    public E_BankController extension;
    public static final String PageTitle = '口座情報';
    
    public E_BankExtender(E_BankController extension){
        super();
        this.extension = extension;
        this.pgTitle = PageTitle;
    }
    
    global override void init(){
        String epoliId = ApexPages.CurrentPage().getParameters().get('id');
        pageRef = doAuth(E_Const.ID_KIND.POLICY, epoliId);
    }
    
    public PageReference pageAction () {
        return E_Util.toErrorPage(pageRef, null);
    }

    
}