@isTest
private class TestI_ChartNBCANPController {

	/**
	 * TypeAのテスト
	 */
	@isTest static void typeATest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		Test.startTest();

		I_ChartNBCANPController agencySRInfoController = new I_ChartNBCANPController();
		agencySRInfoController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID

		// JS用値設定の取得
		agencySRInfoController.paramViewType = '1';
		agencySRInfoController.getJSParameter();
		System.assertNotEquals(0, agencySRInfoController.json.length());

		// JS用値設定の取得(リモートアクション)
		String json = I_ChartNBCANPController.getJSParam(agencySRInfoController.paramSalesResultsId, agencySRInfoController.paramViewType);
		System.assertNotEquals(0, json.length());

		// タイトル
		System.assertEquals('全商品', agencySRInfoController.title);
	}

	/**
	 * TypeBのテスト
	 */
	@isTest static void typeBTest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		Test.startTest();

		I_ChartNBCANPController agencySRInfoController = new I_ChartNBCANPController();
		agencySRInfoController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID

		// JS用値設定の取得
		agencySRInfoController.paramViewType = '2';
		agencySRInfoController.getJSParameter();
		System.assertNotEquals(0, agencySRInfoController.json.length());

		// タイトル
		System.assertEquals('特定保険種類', agencySRInfoController.title);
	}

	/**
	 * TypeCのテスト
	 */
	@isTest static void typeCTest() {

		Test.startTest();

		I_ChartNBCANPController agencySRInfoController = new I_ChartNBCANPController();

		// タイトル
		System.assertEquals('', agencySRInfoController.title);
	}

	/**
	 * TypeDのテスト
	 */
	@isTest static void typeDTest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		agencySalesResults.YYYYMM__c = 'abcdef';
		insert agencySalesResults;

		Test.startTest();

		I_ChartNBCANPController agencySRInfoController = new I_ChartNBCANPController();
		agencySRInfoController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID

		// JS用値設定の取得
		agencySRInfoController.paramViewType = '1';
		agencySRInfoController.getJSParameter();
		System.assertEquals(0, agencySRInfoController.json.length());

		// JS用値設定の取得
		agencySRInfoController.paramViewType = '2';
		agencySRInfoController.getJSParameter();
		System.assertEquals(0, agencySRInfoController.json.length());

	}

}