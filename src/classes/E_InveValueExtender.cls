global with sharing class E_InveValueExtender extends E_AbstractViewExtender {
	private static final String PAGE_TITLE = '現在の評価金額';
	
	E_InveValueController extension;
	
	//時価合計金額
	public String zcurrprcAmount {get;set;}
	//ファンド別配分比率合計
	public String fundRatioAmount {get;set;}
	//投資金額合計
	public String zinvamtAmount {get;set;}
	//評価損益額
	public String strProfit {get;set;}
	//買付来騰落率
	public String strRatio {get;set;}
	
	//グラフコンポーネントの引数
	public String getExecuteKind(){
		return E_Const.GRAPH_INVESTMENT_NOW;
	}
	
	/** コンストラクタ */
	public E_InveValueExtender(E_InveValueController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
	}

	/** init */
	global override void init(){
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, extension.record.id);
		if (pageRef == null) {
			// 投資金額合計を設定
			initInvestmentAmount();
		}
	}

	/** ページアクション */
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}
	
	/** 投資金額合計を設定 */
	private void initInvestmentAmount(){
		Decimal zcurrprc = 0;
		Decimal fundRatio = 0;
		Decimal zinvamt = 0;
		Decimal profit = 0;
		Decimal ratio = 0;
		List<E_InveValueController.dataTableITFPFItem> items = extension.dataTableITFPF.items;
		if(items!=null){
	 		for(E_InveValueController.dataTableITFPFItem item :items){
	 			E_ITFPF__c obj = (E_ITFPF__c)item.record;
	 			if(obj.ZCURRPRC__c != null) zcurrprc += obj.ZCURRPRC__c;
	 			if(obj.ZBRNDPER03__c != null) fundRatio += obj.ZBRNDPER03__c;
	 			if(obj.ZINVAMT__c != null) zinvamt += obj.ZINVAMT__c;
	 		}
	 	}
	 	profit = zcurrprc - zinvamt;
	 	if(zinvamt != 0) ratio = 100 * profit / zinvamt;
	 	
	 	zcurrprcAmount = zcurrprc.format();
	 	fundRatioAmount = String.valueOf(fundRatio);
		zinvamtAmount = zinvamt.format();
		strProfit = profit.format();
		strRatio = STring.valueOf(ratio.setScale(1, System.RoundingMode.HALF_UP));
	}
	
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		PageReference pageRef = Page.E_Inve;
		pageRef.getParameters().put('id', extension.record.id);
		return pageRef;
	}
}