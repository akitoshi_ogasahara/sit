/**
 *      ContactDao :::without利用のため、取扱要注意:::
 *          保険契約ヘッダへの参照権限がある場合に限り当メソッドを利用してその取引先責任者レコードの値を取得する
 *              対象となるのは契約者、被保険者、年金受取人
 */
public without sharing class E_ContactDaoWithout {

	/**
	 *      getRecordsByIds
	 *          ContactIdのSetをKeyにレコードリストを取得
	 */
	public static List<Contact> getRecordsByIds(Set<Id> conIds){
		return [Select
					id, Name, E_CLTPF_ZCLKNAME__c, E_CLTPF_CLNTNUM__c,
					E_CLTPF_CLTPHONE01__c, E_CLTPF_CLTPHONE02__c, E_CLTPF_FAXNO__c,
					E_CLTPF_ZNKJADDR01__c, E_CLTPF_ZNKJADDR02__c, E_CLTPF_ZNCLTADR01__c, E_CLTPF_ZNCLTADR02__c,
					E_CADPF__c, E_CADPF__r.Name, E_CADPF__r.ZADRSKJ__c, E_CADPF__r.ZADRSKN__c,
					E_CL3PF_AGNTNUM__c, E_Account__c, E_CL3PF_ZAGCYNUM__c, E_AccParentName__c, E_CL3PF_ZHEADAY__c,
					AccountId, Account.Name, Account.ParentId, Account.Parent.Name, Account.Parent.E_CL1PF_ZHEADAY__c,
					E_AccParentCord__c, E_MenuKindId__c,E_CLTPF_DOB__c,E_COMM_ZCLADDR__c,E_CLTPF_ZKNJSEX__c,
					Account.E_IsAgency__c ,Account.E_CL1PF_ZAHKNAME__c, Account.E_CL1PF_ZHEADAY__c, FirstName,
					Account.E_CL1PF_KCHANNEL__c, Account.E_CL2PF_ZEAYNAM__c, Account.E_CL2PF_ZAGCYNUM__c,
					Account.Agency__c, LastName, Account.AgencyName__c, E_CLTPF_CLTPCODE__c
					, Account.canSelfRegist__c      //170117 UserTriggerHandler
				From
					Contact
				Where
					id in :conIds
				Order By E_CLTPF_ZCLKNAME__c];      //カナ順で取得
	}

	/**
	 *      PolicyレコードにContact情報を追加設定する。 ContactはWithoutSharingで取得
	　*          @param List<E_Policy__c> 保険契約ヘッダのリスト
	 *          return 追加設定後の保険契約ヘッダのリスト
	 */
	public static List<E_Policy__c> fillContactsToPolicys(List<E_Policy__c> policys
															, Boolean fillToContractor      //契約者　追加する場合True
															, Boolean fillToInsured         //被保険者
															, Boolean fillToAnnuitant       //年金受取人
															, Boolean fillToMainAgent       //主募集人
															, Boolean fillToSubAgent){      //従募集人
		//取得対象ContactId
		Set<Id> contactIds = new Set<Id>();
		for(E_Policy__c rec:policys){
			if(fillToContractor){
				contactIds.add(rec.contractor__c);
			}
			if(fillToInsured){
				contactIds.add(rec.insured__c);
			}
			if(fillToAnnuitant){
				contactIds.add(rec.annuitant__c);
			}
			if(fillToMainAgent){
				contactIds.add(rec.mainagent__c);
			}
			if(fillToSubAgent){
				contactIds.add(rec.subagent__c);
			}
		}

		//ContactのMap
		Map<Id,Contact> contactMap = new Map<Id,Contact>(getRecordsByIds(contactIds));
		//Policyレコードに詰めなおす
		for(E_Policy__c rec:policys){
			if(fillToContractor&&contactMap.containsKey(rec.contractor__c)){
				rec.putSObject('contractor__r', contactMap.get(rec.contractor__c));
			}
			if(fillToInsured&&contactMap.containsKey(rec.insured__c)){
				rec.putSObject('insured__r', contactMap.get(rec.insured__c));
			}
			if(fillToAnnuitant&&contactMap.containsKey(rec.annuitant__c)){
				rec.putSObject('annuitant__r', contactMap.get(rec.annuitant__c));
			}
			if(fillToMainAgent&&contactMap.containsKey(rec.mainagent__c)){
				rec.putSObject('mainagent__r', contactMap.get(rec.mainagent__c));
			}
			if(fillToSubAgent&&contactMap.containsKey(rec.subagent__c)){
				rec.putSObject('subagent__r', contactMap.get(rec.subagent__c));
			}
		}
		return policys;
	}

	/**
	 *      PolicyレコードにContact情報を追加設定する。 ContactはWithoutSharingで取得
	　*          @param E_Policy__c 保険契約ヘッダ1件
	 *          return 追加設定後の保険契約ヘッダ
	 */
	public static E_Policy__c fillContactsToPolicy(E_Policy__c policy
													, Boolean fillToContractor      //契約者　追加する場合True
													, Boolean fillToInsured         //被保険者
													, Boolean fillToAnnuitant       //年金受取人
													, Boolean fillToMainAgent       //主募集人
													, Boolean fillToSubAgent){      //従募集人
		return fillContactsToPolicys(new List<E_Policy__c>{policy}, fillToContractor, fillToInsured, fillToAnnuitant, fillToMainAgent, fillToSubAgent)[0];
	}

	public static Contact getRecByCustomerNumber (String customerNumber) {
		Contact rec = null;
		List<Contact> recs = [
			Select
				id, Name ,E_CLTPF_CLNTNUM__c From Contact
			Where
				E_CLTPF_CLNTNUM__c =: customerNumber
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}
/*
	public static Contact getRecByIdWithOA_Applications(Id cid){
		List<Contact> srcs =    [Select
						E_Account__c
						, E_CL3PF_ZAGCYNUM__c
						, Name
						, E_CL3PF_AGNTNUM__c
						, (Select
							 OA_Information_ExamDate__c
							, OA_Information_ExamMaster_Name__c
							, OA_Information_ExamPlaceMaster_Name__c
							, ResultStatus__c
						From
							OA_Applications__r
						ORDER BY
							OA_Information_ExamDate__c desc)

					From
						Contact
					Where
						id =: cid];
		return (srcs.isEmpty())? new Contact():srcs[0];
	}
*/
	public static Contact getRecById(Id cid){
		List<Contact> srcs = [Select
						Id, Name, FirstName, LastName, E_AccParentName__c, E_AccParentCord__c, E_Account__c,
						E_CL3PF_ZAGCYNUM__c, E_CL3PF_AGNTNUM__c, E_CL3PF_KMOFCODE__c, E_CLTPF_DOB__c,
						Account.E_ParentZHEADAY__c, Account.E_CL2PF_ZAGCYNUM__c, Account.E_CL2PF_BRANCH__c,
						E_CL3PF_ZEATKNAM__c
						//Atria対応開発 20171211
						,E_CL3PF_VALIDFLAG__c,E_CL3PF_KQUALFLG__c,E_CL3PF_ZSUBTRM__c
					From
						Contact
					Where
						Id = :cid];
		return (srcs.isEmpty())? new Contact():srcs[0];
	}

	/**
	 *  募集人コードをキーにレコード取得
	　*  @param agntNum 募集人コード
	 *  @return Contact　募集人レコード
	 */
	public static List<Contact> getRecByAgntNum (String agntNum) {
		String validFlag = '1';//有効フラグ
		//String autoTrFlg = E_Const.ZAUTOTRF_Y;//自動契約移管判定フラグ:Y
		List<Contact> recs = [
			Select
				id, Name ,FirstName, LastName,E_CL3PF_KMOFCODE__c,E_CLTPF_DOB__c,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c,Account.canSelfRegist__c,
				E_CL3PF_CLNTNUM__c,AgentNum5__c
			From Contact
			Where
				E_CL3PF_AGNTNUM__c =: agntNum
			And
				Account.canSelfRegist__c = true
			And E_CL3PF_VALIDFLAG__c=:validFlag
			And E_CL3PF_CLNTNUM__c != null
			//And Account.ZAUTOTRF__c !=:autoTrFlg

		];
		return recs;
	}
	/**
	 *  MoFコードをキーにレコード取得
	　*  @param mofCd MOFコード
	 *  @return Contact　募集人レコード
	 */
	public static List<Contact> getRecByMOF (String mofCd) {
		String validFlag = '1';//有効フラグ
		//String autoTrFlg = E_Const.ZAUTOTRF_Y;//自動契約移管判定フラグ:Y
		List<Contact> recs = [
			Select
				id, Name ,FirstName, LastName,E_CL3PF_KMOFCODE__c,E_CLTPF_DOB__c,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c,Account.canSelfRegist__c,
				E_CL3PF_CLNTNUM__c,AgentNum5__c
			From Contact
			Where
				E_CL3PF_KMOFCODE__c =: mofCd
			And
				Account.canSelfRegist__c = true
			And E_CL3PF_VALIDFLAG__c=:validFlag
			And E_CL3PF_CLNTNUM__c != null
			And SystemOriginFlag__c = true //20171215 ID適正化
			//And Account.ZAUTOTRF__c !=:autoTrFlg
		];
		return recs;
	}

	/**
	 * 募集人顧客コードをキーにレコード取得
	 * @param  contactClntNum        募集人顧客コード
	 * @param  dayofBirth 生年月日yyyyMMdd
	 * @return Contact 募集人レコード
	 */
	public static List<Contact> getRecByCLNTNUM(String contactClntNum ,String dayofBirth){
		List<Contact> recs = [
			Select
				id, Name ,FirstName, LastName,E_CL3PF_KMOFCODE__c,E_CLTPF_DOB__c,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c,Account.canSelfRegist__c,
				E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c
			From Contact
			Where
				E_CLTPF_CLNTNUM__c = :contactClntNum
			AND E_CLTPF_DOB__c = :dayofBirth
		];
		return recs;
	}

	/**
	 *募集人コードもしくはMOFコードが合致し、かつ生年月日が合致する場合レコード取得
	 *@param  agentNum 募集人コード
	 *@param  mofCd MOFコード
	 *@param  dayofBirth 生年月日yyyymmdd
	 *@return Contact 募集人レコード
	 */
	public static List<Contact> getRecsByCdANDDayofBirth(String agentNum,String mofCd,String dayofBirth){
		if(String.isBlank(agentNum)){
			agentNum = ' ';
		}else if(String.isBlank(mofCd)){
			mofCd = ' ';
		}
		String validFlag = '1';//有効フラグ

		List<Contact> recs = [
		Select
				id, Name ,FirstName, LastName,E_CL3PF_KMOFCODE__c,E_CLTPF_DOB__c,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c
		From
				Contact
		Where
				(E_CL3PF_AGNTNUM__c = :agentNum Or E_CL3PF_KMOFCODE__c =:mofCd) And E_CLTPF_DOB__c =: dayofBirth And E_CL3PF_VALIDFLAG__c=:validFlag
				And Account.ZAUTOTRF__c !=:E_Const.ZAUTOTRF_Y
		];
		return recs;
	}

	/**
	 *  募集人コード、またはMOFコードをキーにレコード取得
	　*  @param agOrMofcode 募集人　または　MOFコード
	　*  @param dayofBirth 生年月日yyyymmdd
	 *  @return Contact　募集人レコード
	 */
	public static List<Contact> getRecByAGorMOF (String agOrMofcode,String dayofBirth) {
		String validFlag = '1';//有効フラグ
		//String autoTrFlg = E_Const.ZAUTOTRF_Y;//自動契約移管判定フラグ:Y
		List<Contact> recs = [
			Select
				id, Name ,FirstName, LastName,E_CL3PF_KMOFCODE__c,E_CLTPF_DOB__c,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c,Account.canSelfRegist__c
			From Contact
			Where
				(E_CL3PF_KMOFCODE__c =: agOrMofcode
			OR
				E_CL3PF_AGNTNUM__c =: agOrMofcode)
			And
				E_CLTPF_DOB__c =: dayofBirth
			And
				Account.canSelfRegist__c = true
			And E_CL3PF_VALIDFLAG__c=:validFlag
			//And Account.ZAUTOTRF__c !=:autoTrFlg
		];
		return recs;
	}

	/**
	 * 事務所IDからContactレコードを取得
	 */
	public static List<Contact> getRecsByOfficeId(String accid) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL3PF_AGNTNUM__c'
					+ ', E_Account__c'
					+ ', E_AccParentName__c '
					+ ', E_CL3PF_ZEATKNAM__c '  //スプリントバックログ9(2017/1/18)
					+ 'FROM Contact '
					+ 'WHERE Account.Id = :accid'
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\''
					+ ' AND E_CL3PF_AGTYPE__c != \'01\''
					+ ' AND E_CL3PF_AGTYPE__c != \'07\''
					+ ' AND E_CL3PF_AGTYPE__c != \'08\''
					+ ' AND E_CL3PF_AGTYPE__c != \'99\''
					+ ' AND (NOT E_CL3PF_AGNTNUM__c LIKE \'%|%\')'
					+ ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	public static Contact getRecByIdWithOut (String contactId) {
		Contact rec = null;
		List<Contact> recs = [
			Select
				id, Name, E_CLTPF_ZCLKNAME__c, E_CLTPF_CLNTNUM__c,
				E_CLTPF_CLTPHONE01__c, E_CLTPF_CLTPHONE02__c, E_CLTPF_FAXNO__c,
				E_CLTPF_ZNKJADDR01__c, E_CLTPF_ZNKJADDR02__c, E_CLTPF_ZNCLTADR01__c, E_CLTPF_ZNCLTADR02__c,
				E_CADPF__c, E_CADPF__r.Name, E_CADPF__r.ZADRSKJ__c, E_CADPF__r.ZADRSKN__c,
				E_CL3PF_AGNTNUM__c, E_Account__c, E_CL3PF_ZAGCYNUM__c, E_AccParentName__c, E_CL3PF_ZHEADAY__c,
				AccountId, Account.Name, Account.ParentId, //Account.Parent.Name, Account.Parent.E_CL1PF_ZHEADAY__c,
				E_AccParentCord__c, E_MenuKindId__c, Account.E_CL2PF_ZAGCYNUM__c, Account.E_CL2PF_BRANCH__c
			From
				Contact
			Where
				id =: contactId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	// IRIS Day2
	/**
	 * 顧客番号から検索
	 */
	public static Contact getRecCustomerNo(String customerNo){
		List<Contact> cons = new List<Contact>();
		cons = [SELECT Id
					, Name
					, E_CLTPF_ZCLKNAME__c
					, E_CLTPF_CLNTNUM__c
					, E_COMM_ZCLADDR__c
					, E_CLTPF_FLAG01__c
					, E_CLTPF_FLAG02__c
				FROM Contact
				WHERE isDeleted = false
				AND E_CLTPF_CLNTNUM__c = :E_Util.LTrim(E_Util.RTrim(customerNo))
				AND E_isAgent__c = false];

		if(cons == null || cons.isEmpty()){
			return null;
		}

		return cons.get(0);
	}

	/**
	 * 募集人検索（検索ボックス）
	 */
	public static List<Contact> getRecsIRISAgencySearchBoxAgent(String soqlWhere, String keyword){
		// キーワード分割
		List<String> partialKeywords = new List<String>();
		List<String> forwardKeywords = new List<String>();
		if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
			for(String text : I_Util.createSearchkeywordList(keyword)){
				partialKeywords.add('%' + String.escapeSingleQuotes(text) + '%');
				forwardKeywords.add(String.escapeSingleQuotes(text) + '%');
			}
		}

		List<Contact> result = new List<Contact>();
		String soql = 'SELECT Id '
					+ ', Name '                         // 募集人名
					+ ', E_CL3PF_ZEATKNAM__c '          // 募集人名カナ
					+ ', E_CL3PF_AGNTNUM__c '           // 募集人コード
					+ ', E_AccParentName__c '           // 親代理店名
					+ ', Account.Parent.E_CL1PF_ZAHKNAME__c '   // 代理店名カナ
					+ ', E_AccParentCord__c '           // 代理店格コード
					+ ', E_Account__c '                 // 代理店事務所（数式）：事務所名
					+ ', Account.E_CL2PF_ZEAYKNAM__c '  // 事務所名カナ
					+ ', Account.E_CL2PF_ZAGCYNUM__c '  // 代理店事務所コード
					//20180214 資料発送申込対応 START
					+ ', Account.CLTPCODE__c '          // 代理店事務所郵便番号
					+ ', Account.E_COMM_ZCLADDR__c '    // 代理店事務所住所
					+ ', Account.CLTPHONE01__c '        // 代理店事務所電話番号
					+ ', Account.E_CL2PF_ZEAYNAM__c '   // 代理店事務所名
					//20180214 資料発送申込対応END
					+ 'FROM Contact '
					+ 'WHERE E_CL3PF_VALIDFLAG__c = \'1\' '
					+ 'AND E_CL3PF_AGNTNUM__c != null '
					+ 'AND E_CL3PF_AGTYPE__c != \'01\' '
					+ 'AND E_CL3PF_AGTYPE__c != \'07\' '
					+ 'AND E_CL3PF_AGTYPE__c != \'08\' '
					+ 'AND E_CL3PF_AGTYPE__c != \'99\' ';
			if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
				soql+='AND (Name like :partialKeywords OR E_CL3PF_ZEATKNAM__c like :partialKeywords '
					+ 'OR E_CL3PF_AGNTNUM__c like :forwardKeywords) ';
			}
				soql+=soqlWhere
					+ 'LIMIT ' + I_Const.LIST_MAX_ROWS;

		result = Database.query(soql);

		return result;
	}

	/**
	 * 募集人検索（検索ボックス）
	 */
	//IRISエンハンス IRIS-751 ここから
	public static List<Contact> getRecsIRISAgencySearchBoxAgent(String soqlWhere, String keyword,String selection){
		String soqlkeyword = '';
		// キーワード分割
		List<String> partialKeywords = new List<String>();
		List<String> forwardKeywords = new List<String>();
		if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
			for(String text : I_Util.createSearchkeywordList(keyword)){
				partialKeywords.add('%' + String.escapeSingleQuotes(text) + '%');
				forwardKeywords.add(String.escapeSingleQuotes(text) + '%');
			}
		}

		//名前項目 検索条件文
		String nameKeywords = '';
		nameKeywords = E_Util.createAndSearchQuery(keyword,'Name');

		//募集人カナ氏名項目 検索条件文
		String kanaKeywords = '';
		kanaKeywords = E_Util.createAndSearchQuery(keyword,'E_CL3PF_ZEATKNAM__c');

		//募集人コード項目 検索条件文
		String agntnumKeywords = '';
		for(String text : keyword.replaceAll('　', ' ').split(' ')){
			//where句追記初回
			if(String.isBlank(agntnumKeywords)){
				agntnumKeywords = 'E_CL3PF_AGNTNUM__c like ' + '\'%' + text.replaceAll('(%|_)', '\\\\$1') + '%\'';
			}else{
				agntnumKeywords = agntnumKeywords + ' AND E_CL3PF_AGNTNUM__c like ' + '\'%' + text.replaceAll('(%|_)', '\\\\$1') + '%\'';
			}
		}
		//AND検索
		if(selection == 'and'){
			soqlkeyword = 'AND (((' + nameKeywords + ') OR (' + kanaKeywords + ')) OR (' + agntnumKeywords + '))';
		//OR検索
		}else{
			soqlkeyword = 'AND (Name like :partialKeywords OR E_CL3PF_ZEATKNAM__c like :partialKeywords '
					+ 'OR E_CL3PF_AGNTNUM__c like :forwardKeywords) ';
		}

		List<Contact> result = new List<Contact>();
		String soql = 'SELECT Id '
					+ ', Name '                         // 募集人名
					+ ', E_CL3PF_ZEATKNAM__c '          // 募集人名カナ
					+ ', E_CL3PF_AGNTNUM__c '           // 募集人コード
					+ ', E_AccParentName__c '           // 親代理店名
					+ ', Account.Parent.E_CL1PF_ZAHKNAME__c '   // 代理店名カナ
					+ ', E_AccParentCord__c '           // 代理店格コード
					+ ', E_Account__c '                 // 代理店事務所（数式）：事務所名
					+ ', Account.E_CL2PF_ZEAYKNAM__c '  // 事務所名カナ
					+ ', Account.E_CL2PF_ZAGCYNUM__c '  // 代理店事務所コード
					//20180214 資料発送申込対応 START
					+ ', Account.CLTPCODE__c '          // 代理店事務所郵便番号
					+ ', Account.E_COMM_ZCLADDR__c '    // 代理店事務所住所
					+ ', Account.CLTPHONE01__c '        // 代理店事務所電話番号
					+ ', Account.E_CL2PF_ZEAYNAM__c '   // 代理店事務所名
					//20180214 資料発送申込対応END
					+ 'FROM Contact '
					+ 'WHERE E_CL3PF_VALIDFLAG__c = \'1\' '
					+ 'AND E_CL3PF_AGNTNUM__c != null '
					+ 'AND E_CL3PF_AGTYPE__c != \'01\' '
					+ 'AND E_CL3PF_AGTYPE__c != \'07\' '
					+ 'AND E_CL3PF_AGTYPE__c != \'08\' '
					+ 'AND E_CL3PF_AGTYPE__c != \'99\' ';
			if(String.isNotEmpty(keyword)){ //20180214 資料発送申込対応
				soql+=soqlkeyword;
			}
				soql+=soqlWhere
					+ 'LIMIT ' + I_Const.LIST_MAX_ROWS;

		result = Database.query(soql);

		return result;
	}
	//IRISエンハンス IRIS-751 ここまで

	/**
	 * 事務所IDからContactレコードを取得(募集人コードに'|'を含む)
	 * JIRA_#51 170207
	 */
	public static List<Contact> getRecsByAccountId(String accid) {
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL3PF_AGNTNUM__c'
					+ ', E_Account__c'
					+ ', E_AccParentName__c'
					+ ', E_CL3PF_ZEATKNAM__c'
					+ ', E_CL3PF_VALIDFLAG__c'
					+ ', E_CL3PF_KQUALFLG__c'//Atria　山田
					+ ', E_CL3PF_ZSUBTRM__c'//Atria　山田
					+ ' FROM Contact'
					+ ' WHERE Account.Id = :accid'
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\''
					+ ' AND E_CL3PF_AGNTNUM__c != null'
					+ ' AND E_CL3PF_AGTYPE__c != \'01\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'07\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'08\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'99\' '
					+ ' ORDER BY E_CL3PF_AGNTNUM__c'
					+ ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}
	/**
	 * 募集人コードの前方一致でContactレコードを取得
	 * JIRA_#51 170208
	 */
	public static List<Contact> getRecsByAgntNum(String agntNum) {
		String agntNumLike = agntNum+'%';
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL3PF_AGNTNUM__c'
					+ ', E_Account__c'
					+ ', E_AccParentName__c'
					+ ', E_CL3PF_ZEATKNAM__c'
					+ ', E_CL3PF_VALIDFLAG__c'//Atria　山田
					+ ', E_CL3PF_KQUALFLG__c'//Atria　山田
					+ ', E_CL3PF_ZSUBTRM__c'//Atria　山田
					+ ' FROM Contact'
					+ ' WHERE E_CL3PF_AGNTNUM__c like :agntNumLike'
					+ ' ORDER BY E_CL3PF_AGNTNUM__c'
					+ ' LIMIT ' + I_Const.LIST_MAX_ROWS;

		return Database.query(soql);
	}

	/**
	 * 募集人コードをキーにContactのMapを取得
	 * @param Set<Id> ids
	 * @return Map<Id, Contact>
	 */
	public static Map<Id, Contact> getRecsMapByIds(Set<Id> ids){
		return new Map<Id, Contact>(
			[
				SELECT
					Id,
					E_AccParentCord__c,
					E_CL3PF_AGTYPE__c,
					AccountId,
					Account.Parent.E_MenuKind__c,
					Account.Parent.E_CL1PF_KCHANNEL__c,
					Account.canSelfRegist__c,
					Account.ParentId
				 FROM Contact
				 WHERE Id in :ids
			]);
	}

	/**
	 * ID管理の所有者コードをキーにContactのMapを取得
	 * @param Set<String> code
	 * @return Map<String, Id>
	 */
	 /*
	 public static Map<String, Id> getRecsContactByCode(Set<String> code){
		Map<String,Id> agentMap = new Map<String,Id>();
			for(Contact con : [SELECT Id,E_CL3PF_AGNTNUM__c FROM Contact WHERE E_CL3PF_AGNTNUM__c in :code]){
				agentMap.put(con.E_CL3PF_AGNTNUM__c,con.Id);
			}
			return agentMap;
	} */
	// R1.1特別対応
	/**
	 * ID管理の所有者コードをキーにContactのMapを取得
	 * @param Set<String> code
	 * @return Map<String, Id>
	 */
	 public static Map<String, Contact> getRecsContactByCode(Set<String> code){
		Map<String,Contact> agentMap = new Map<String,Contact>();
			for(Contact con : [SELECT Id,E_CL3PF_AGNTNUM__c,E_CL3PF_ZAGMANGR__c FROM Contact WHERE E_CL3PF_AGNTNUM__c in :code]){
				agentMap.put(con.E_CL3PF_AGNTNUM__c,con);
			}
			return agentMap;
	}

	/**
	 * 顧客番号をキーに顧客番号、募集人顧客コードを検索しContactのListを取得
	　*  @param cltnum 顧客番号
	 *  @return List<Contact>　Contactリスト
	 */
	public static List<Contact> getRecsByCltnum (String cltnum) {
		List<Contact> recs = [
			Select
				 Id
				,Name
				,RecordType.Name
				,E_CLTPF_CLNTNUM__c
				,E_CL3PF_CLNTNUM__c
				,E_CL3PF_AGNTNUM__c
				,E_CL3PF_VALIDFLAG__c
				,E_IsAgent__c
				,CreatedDate
				,(select Id ,Name from E_PolicyContractors__r)
				,(select Id ,Name from E_PolicyInsureds__r)
				,(select Id ,Name from E_PolicyAnnuitants__r)
				,(select Id ,Name from E_PolicyMainAgents__r)
				,(select Id ,Name from E_PolicySubAgents__r)
				,(select Id ,Name from E_PolicyCorporates__r)
			From
				Contact
			Where
				E_CLTPF_CLNTNUM__c =: cltnum
			Or
				E_CL3PF_CLNTNUM__c =: cltnum
			Order by
				 E_CLTPF_CLNTNUM__c NULLS LAST
				,E_CL3PF_AGNTNUM__c NULLS LAST
				,E_CL3PF_AGNTNUM__c NULLS LAST
		];
		return recs;
	}

	/**
	 * 顧客番号or募集人顧客コードのセットをキーに顧客番号を検索し顧客ContactのListを取得
	　*  @param Set<String> cltnums 顧客番号セット
	 *  @return List<Contact>　顧客Contactリスト
	 */
	public static List<Contact> getClientRecsByCltnums (Set<String> cltnums) {
		List<Contact> recs = [
			Select
				 Id
				,Name
				,RecordType.Name
				,E_CLTPF_CLNTNUM__c
				,E_CL3PF_CLNTNUM__c
				,E_CL3PF_AGNTNUM__c
				,E_CL3PF_VALIDFLAG__c
				,E_IsAgent__c
				,CreatedDate
			From
				Contact
			Where
				E_CLTPF_CLNTNUM__c In :cltnums
			Order by
				E_CLTPF_CLNTNUM__c
		];
		return recs;
	}

	/**
	 * 顧客番号or募集人顧客コードのセットをキーに募集人顧客コードを検索し募集人ContactのListを取得
	　*  @param Set<String> cltnums 顧客番号セット
	 *  @return List<Contact>　募集人Contactリスト
	 */
	public static List<Contact> getAgentRecsByCltnums (Id recTypeId, Set<String> cltnums) {
		List<Contact> recs = [
			Select
				 Id
				,Name
				,RecordType.Name
				,E_CLTPF_CLNTNUM__c
				,E_CL3PF_CLNTNUM__c
				,E_CL3PF_AGNTNUM__c
				,E_CL3PF_VALIDFLAG__c
				,E_IsAgent__c
				,ParentECLT__c
				,CreatedDate
			From
				Contact
			Where
				RecordTypeId = :recTypeId
			And
				E_CL3PF_CLNTNUM__c In :cltnums
			Order by
				E_CL3PF_CLNTNUM__c
		];
		return recs;
	}


	public static List<Contact> getRecByAgentNum5(String agentNum){
		String validFlag = '1';//有効フラグ

		List<Contact> recs = [
			Select
				Id
				,SystemOriginFlag__c
			From
				Contact
			WHERE
				AgentNum5__c LIKE :agentNum
			And
				Account.canSelfRegist__c = true
			And
				E_CL3PF_VALIDFLAG__c=:validFlag
			And
				E_CL3PF_CLNTNUM__c != null
			ORDER BY SystemOriginFlag__c DESC
		];
		return recs;
	}
	/*
	 *募集人コードでContactを検索する
	 */

	public static Contact getRecByAgentNumber (String AgentNumber) {
		Contact rec = null;
		List<Contact> recs = [
			Select
				id, Name ,E_CL3PF_AGNTNUM__c,E_CL3PF_VALIDFLAG__c From Contact
			Where
				E_CL3PF_AGNTNUM__c =: AgentNumber
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 *  P16-0003 Atria対応開発
	 *  代理店格Id + 検索文言で取得できる募集人の件数を返す
	 */
	 public static Integer getRecCountByAgencyId( String accId, String keyword ){
		// キーワード分割
		List<String> partialKeywords = new List<String>();
		List<String> forwardKeywords = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			partialKeywords.add('%' + String.escapeSingleQuotes(text) + '%');
			forwardKeywords.add(String.escapeSingleQuotes(text) + '%');
		}
		String soql = 'SELECT Count()'
					+ ' FROM Contact'
					+ ' WHERE Account.Parent.Id = :accId'
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\''
					+ ' AND E_CL3PF_AGNTNUM__c != null'
					+ ' AND E_CL3PF_AGTYPE__c != \'01\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'07\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'08\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'99\' ';
		if( keyword != '' ){
			soql    += 'AND (Name like :partialKeywords OR E_CL3PF_ZEATKNAM__c like :partialKeywords ';
			soql    += ' OR E_CL3PF_AGNTNUM__c like :forwardKeywords) ';
		}

		return Integer.valueOf( Database.countQuery( soql ) );
	 }

	/**
	 *  P16-0003 Atria対応開発
	 *  絞り込んだ事務所のId + 検索文言で取得できる募集人の件数を返す
	 */
	 public static Integer getRecCountByOfficeIds(List<String> offIdList, String keyword ){
		// キーワード分割
		List<String> partialKeywords = new List<String>();
		List<String> forwardKeywords = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			partialKeywords.add('%' + String.escapeSingleQuotes(text) + '%');
			forwardKeywords.add(String.escapeSingleQuotes(text) + '%');
		}
		String soql = 'SELECT Count()'
					+ ' FROM Contact'
					+ ' WHERE Account.Id = :offidList'
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\''
					+ ' AND E_CL3PF_AGNTNUM__c != null'
					+ ' AND E_CL3PF_AGTYPE__c != \'01\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'07\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'08\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'99\' ';
		if( keyword != '' ){
			soql    += 'AND (Name like :partialKeywords OR E_CL3PF_ZEATKNAM__c like :partialKeywords ';
			soql    += ' OR E_CL3PF_AGNTNUM__c like :forwardKeywords) ';
		}

		return Integer.valueOf( Database.countQuery( soql ) );
	 }

	/**
	 *  P16-0003 Atria対応開発
	 *  代理店格Id + 検索文言で取得できる募集人の情報を返す
	 */
	public static List<Contact> getRecsByAgencyId ( String accId, String branchCode, String keyword, Boolean isCsv ){
		// キーワード分割
		List<String> partialKeywords = new List<String>();
		List<String> forwardKeywords = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			partialKeywords.add('%' + String.escapeSingleQuotes(text) + '%');
			forwardKeywords.add(String.escapeSingleQuotes(text) + '%');
		}

		List<Contact> result = new List<Contact>();
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL3PF_AGNTNUM__c'
					+ ', E_Account__c'
					+ ', E_AccParentName__c'
					+ ', E_CL3PF_ZEATKNAM__c'
					+ ', E_CL3PF_VALIDFLAG__c'
					+ ', E_CL3PF_AGTYPE__c'
					+ ', E_CL3PF_ZAGMANGR__c'
					+ ' FROM Contact'
					+ ' WHERE Account.Parent.Id = :accId '
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\' '
					+ ' AND E_CL3PF_AGNTNUM__c != null'
					+ ' AND E_CL3PF_AGTYPE__c != \'01\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'07\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'08\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'99\' ';
		if( branchCode != '' ){
			soql    += 'AND ( Account.E_CL2PF_BRANCH__c = :branchCode ';
			soql    += 'OR Account.E_CL2PF_ZBUSBR__c = :branchCode )';
		}
		if( keyword != '' ){
			soql    += ' AND (Name like :partialKeywords OR E_CL3PF_ZEATKNAM__c like :partialKeywords ';
			soql    += ' OR E_CL3PF_AGNTNUM__c like :forwardKeywords) ';
		}
			soql    += ' ORDER BY E_CL3PF_AGNTNUM__c';

		if( isCsv ){
			soql    += ' LIMIT ' + I_Const.LIST_DL_MAX_ROWS ;
		} else {
			soql    += ' LIMIT ' + I_Const.LIST_MAX_ROWS;
		}
		system.debug('SOQL****' + soql ) ;
		result = Database.query(soql);

		return result;
	}

	/**
	 *  P16-0003 Atria対応開発
	 *  絞り込んだ事務所のId + 検索文言で取得できる募集人の情報を返す
	 */
	public static  List<Contact> getRecsByOfficeIds( List<String> offIdList, String keyword, Boolean isCsv ){
		// キーワード分割
		List<String> partialKeywords = new List<String>();
		List<String> forwardKeywords = new List<String>();
		for(String text : I_Util.createSearchkeywordList(keyword)){
			partialKeywords.add('%' + String.escapeSingleQuotes(text) + '%');
			forwardKeywords.add(String.escapeSingleQuotes(text) + '%');
		}


		List<Contact> result = new List<Contact>();
		String soql = 'SELECT Id'
					+ ', Name'
					+ ', E_CL3PF_AGNTNUM__c'
					+ ', E_Account__c'
					+ ', E_AccParentName__c'
					+ ', E_CL3PF_ZEATKNAM__c'
					+ ', E_CL3PF_VALIDFLAG__c'
					+ ', E_CL3PF_AGTYPE__c'
					+ ', E_CL3PF_ZAGMANGR__c'
					+ ' FROM Contact'
					+ ' WHERE Account.Id = :offidList'
					+ ' AND E_CL3PF_VALIDFLAG__c = \'1\''
					+ ' AND E_CL3PF_AGNTNUM__c != null'
					+ ' AND E_CL3PF_AGTYPE__c != \'01\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'07\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'08\' '
					+ ' AND E_CL3PF_AGTYPE__c != \'99\' ';
		if( keyword != '' ){
			soql    += 'AND (Name like :partialKeywords OR E_CL3PF_ZEATKNAM__c like :partialKeywords ';
			soql    += ' OR E_CL3PF_AGNTNUM__c like :forwardKeywords) ';
		}
			soql    += ' ORDER BY E_CL3PF_AGNTNUM__c';
		if( isCsv ){
			soql    += ' LIMIT ' + I_Const.LIST_DL_MAX_ROWS;
		} else {
			soql    += ' LIMIT ' + I_Const.LIST_MAX_ROWS;
		}
		result = Database.query(soql);
		system.debug('soql' + soql);

		return result;

	}

}