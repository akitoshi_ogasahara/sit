/**
 * Attachmentデータアクセスオブジェクト
 * CreatedDate 2015/01/07
 * @Author Terrasky
 */
public with sharing class E_AttachmentDao {
	/**
	 * IDをキーに、指定したオブジェクトの添付ファイルを取得する
	 * @param targetId: 添付ファイルが紐づくオブジェクトのID
	 * @return Attachment: 添付ファイル
	 */
	public static Attachment getRecById (Id targetId) {
		Attachment rec = null;
		List<Attachment> recs = [
			Select 
				id, Name From Attachment
			Where 
				Parent.Id =: targetId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}
	
	/**
	 * IDをキーに、指定したオブジェクトの添付ファイルリストを取得する
	 * @param targetId: 添付ファイルが紐づくオブジェクトのID
	 * @return List<Attachment>: 添付ファイルリスト
	 */
	public static List<Attachment> getRecsById (Id targetId) {
		return 
			[Select 
				Id, Name, ParentId, LastModifiedDate
			From
				Attachment
			Where
				parent.Id =: targetId
			ORDER BY 
				LastModifiedDate desc
		];
	}

	/**
	 * @author Terrasky 
	 * CreatedDate 20160908
	 * @param parentid 親レコードID
	 * @return List<Attachment>: 添付ファイルリスト
	 */
	 public static List<Attachment> getRecsByParentId(ID pid){
		if(pid == null) return new List<Attachment>();
		return [Select id,Name,body From Attachment Where parentid =: pid Order by Name];
	}

	/**
	 * [IRIS]親IDのSetをキーに親IDと添付ファイルリストのマップを取得する
	 * @param  Set<ID>:IDのリスト
	 * @return Map<ID, List<Attachment>>:パラメータのIDと添付ファイルリストのマップ
	 */
	public static Map<Id, List<Attachment>> getRecsByParentIds(Set<Id> parentIds){
		Map<ID, List<Attachment>> ret = new Map<Id, List<Attachment>>();
		for(Attachment att : [SELECT Id, Name, ParentId, BodyLength
								FROM Attachment 
							   WHERE ParentId in :parentIds 
							   ORDER BY LastModifiedDate desc]){
			if(ret.containsKey(att.parentId)==false){
				ret.put(att.parentId, new List<Attachment>());
			}
			ret.get(att.parentId).add(att);
		}
		return ret;
	}

	/**
	* @author Terrasky
	* CreatedDate 2017/11/22
	* ファイル情報のUpsertKeyをキーに、ファイル情報を取得する
	* @param UpsertKey: 添付ファイルが紐づくファイル情報のUpsertKey
	* @return rec:指定されたファイル情報
	*/
	public static E_CMSFile__c getRecByUpsertKey(String upsertKey){
		E_CMSFile__c rec = null;
		List<E_CMSFile__c> recs = [SELECT Id
									,UpsertKey__c
									,(SELECT Id, Name FROM Attachments ORDER BY LastModifiedDate desc)
									FROM E_CMSFile__c
									WHERE UpsertKey__c =: upsertKey];
		if(!recs.isEmpty()){
			rec = recs[0];
			}
		return rec;
	}

	/**
	 * 内容をキーにレコードリスト取得
	 */
	public static List<Attachment> getRecsByParentAndDescription(Id pId, String des){
		return [Select
					Id, Name, BodyLength, CreatedDate, CreatedBy.Name
				From 
					Attachment
				Where IsDeleted = false
				And parentId = :pId
				And Description = :des
				Order by CreatedDate];
	}

	/**
	 * 内容をキーにレコードリスト取得
	 */
	public static List<Attachment> getRecsByNotIdAndDescription(Id recId, Id pId, String des){
		return [Select
					Id
				From 
					Attachment
				Where IsDeleted = false
				And parentId = :pId
				And Id != :recId
				And Description = :des];
	}
}