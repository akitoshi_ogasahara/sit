@isTest
private class TestE_AttachmentDaoWithout {
	
	@isTest static void doDeleteTest() {
		// テストデータ作成
		String CODE_DISTRIBUTE = '10001';
		String BUSINESS_DATE = '20000101';
		Account acc = new Account(Name = 'テスト代理店格', E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert acc;
		E_AgencySalesResults__c sr = new E_AgencySalesResults__c(ParentAccount__c = acc.Id
																, BusinessDate__c = BUSINESS_DATE
																, Hierarchy__c = 'AH'
																, QualifSim__c = '優績S');
		insert sr;
		E_SalesPlan__c sp = new E_SalesPlan__c(ParentAccount__c = acc.Id, SalesResult__c = sr.Id);
		insert sp;

		Attachment att = new Attachment(Name = 'テストファイル', ParentId = sp.Id, body = Blob.valueOf(sp.Id));
		insert att;
		
		E_AttachmentDaoWithout.doDelete(att);
	}
}