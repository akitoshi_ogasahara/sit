/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OppBudgetTotalizationControllerTest {

	static testMethod void procTotalizationTestCase1() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース１【0件更新の場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '2010/12';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース１【0件更新の場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase2() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース２【1件更新の場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '1995/12';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース２【1件更新の場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase3() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース３【見込案件の該当データ無しの場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '1990/12';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース３【見込案件の該当データ無しの場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase4() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース４【営業部予算の該当データ無しの場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '2009/12';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース４【営業部予算の該当データ無しの場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase5() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース５【異常の場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '9999/99';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース５【異常の場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase6() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース６【201件更新の場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '2008/01';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース６【201件更新の場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase7() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース７【2件更新及び未処理件数（両方）ありの場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '2007/02';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース７【2件更新及び未処理件数（両方）ありの場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase8() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース８【1件更新及び未処理件数（営業部予算）ありの場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '2004/08';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース８【1件更新及び未処理件数（営業部予算）ありの場合】の終了 --------------------------------');
    }

    static testMethod void procTotalizationTestCase9() {
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース９【1件更新及び未処理件数（見込案件）ありの場合】の開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        ctl.inpDate = '2003/09';
        ctl.procTotalization();
        system.debug('-------------------------------- 営業部予算への反映メソッドテストケース９【1件更新及び未処理件数（見込案件）ありの場合】の終了 --------------------------------');
    }

    static testMethod void getNowMonthTestCase() {
    	system.debug('-------------------------------- 現在年月取得メソッドテストの開始 --------------------------------');
        PageReference pageRef = Page.OppBudgetTotalization;
	    Test.setCurrentPage(pageRef);

        OppBudgetTotalizationController ctl = new OppBudgetTotalizationController();
        String yearMonth = ctl.getNowMonth();
        //system.assertEquals('2010/12', yearMonth);
        system.debug('-------------------------------- 現在年月取得メソッドテストの終了 --------------------------------');
    }
}