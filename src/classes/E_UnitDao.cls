/**
 * E_Unit(営業部)データアクセスオブジェクト
 * CreatedDate 2017/06/21
 * @Author Terrasky
 */
public with sharing class E_UnitDao {

	public static List<E_Unit__c> getUnitRec(){
		return[
			SELECT
				Id,
				Name,
				Area__r.Name,
				E_COMM_ZCLADDR__c,
				E_CLTPF_CLTPHONE01__c,
				BRANCH__c,
				DisplayOrder__c
			FROM
				E_Unit__c
			WHERE 
				Area__r.AreaCode__c != 'A9' 
			ORDER BY 
				Area__r.SortNo__c,BRANCH__c
		];
	}

	public static E_Unit__c getUnitRecById(String unitId){
		return[
			SELECT
				Id,
				Name,
				Area__r.Name,
				E_COMM_ZCLADDR__c,
				E_CLTPF_CLTPHONE01__c,
				BRANCH__c
			FROM
				E_Unit__c
			WHERE
				Id =: unitId
		];
	}
	/**
	 *
	 */
	public static E_Unit__c getUnitRecByBRCode(String brCode){
		return[
			SELECT
				Name
			FROM
				E_Unit__c
			WHERE
				BRANCH__c =: brCode
		];	
	}
}