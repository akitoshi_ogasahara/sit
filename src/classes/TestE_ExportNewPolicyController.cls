/**
 * 
 */
@isTest
private class TestE_ExportNewPolicyController {

	private static Account ahAcc;
	private static List<Account> ayAccList;
	private static List<Contact> atConList;
	private static List<E_NewPolicy__c> newpolicys;

	/**
	 * String mainSObjectAPIName()
	 */
	static testMethod void mainSObjectAPIName_test01(){
		Test.StartTest();
		
		E_ExportNewPolicyController controller = new E_ExportNewPolicyController();
		
		Test.stopTest();
		
		// assertion
		System.assertEquals('Account', controller.mainSObjectAPIName());
	}
	
	/**
	 * String getCSVHeader()
	 */
	static testMethod void getCSVHeader_test01(){
		Test.StartTest();
		
		E_ExportNewPolicyController controller = new E_ExportNewPolicyController();
		
		Test.stopTest();
		
		// 予想値
		List<String> header = new List<String>();
		header.add('申込番号');
		header.add('証券番号');
		header.add('契約データ入力日');
		header.add('ステータス更新日');
		header.add('新契約ステータス');
		header.add('入金日');
		header.add('成立処理日');
		header.add('募集年');
		header.add('募集月');
		header.add('募集人コード');
		header.add('支部コード');
		header.add('個人コード');
		header.add('取扱者漢字名');
		header.add('募集人コード（従）');
		header.add('支部コード（従）');
		header.add('個人コード（従）');
		header.add('取扱者漢字名（従）');
		header.add('契約者名（漢字）');
		header.add('契約者名（カナ）');
		header.add('被保険者名（漢字）');
		header.add('被保険者名（カナ）');
		header.add('保険種類（主契約）');
		header.add('1P合計保険料（主契約＋特約）');		
		// 不備情報　10件
		for(Integer i = 1; i < 11; i++){
			header.add('不備発信日' + i);
			header.add('不備ステータス' + i);
			header.add('不備コード' + i);
			header.add('不備内容' + i);
			header.add('不備内容詳細' + i);
			header.add('不備処理方法' + i + '-1');
			header.add('不備処理方法' + i + '-2');
			header.add('不備処理方法' + i + '-3');
		}
		header.add('団体番号');
		header.add('データ配信日時');
		header.add('支社名（事務所名）');
		header.add('支社名（事務所名）(従)');
		header.add('申込日');
		header.add('告知日');
		header.add('診査方法');
		header.add('保険金額');
		header.add('被保険者生年月日');
		header.add('被保険者性別');
		header.add('契約時年齢');
		header.add('払方');
		header.add('払込経路');
		header.add('特別保険料');
		header.add('入金済保険料金額');
		header.add('入金済保険料金額との差額');
		header.add('ANP');
		String expected =  String.join(header, ',') + '\n';
	
		// assertion
		System.assertEquals(expected, controller.getCSVHeader());
	}
	
	/**
	 * List<String> getLines()
	 * AH
	 */
	static testMethod void getLines_test01(){
		// TestData
		createData();
		
		// 不備情報登録
		createDefect(newpolicys[0].Id, 10);
		
		//ページ情報
		PageReference pref = Page.E_ExportNewPolicy;
		pref.getParameters().put('did', ahAcc.Id);
		Test.setCurrentPage(pref);
		
		List<E_NewPolicy__c> policys = E_NewPolicyDao.getRecsForCsv(ahAcc.Id, null, null);
		for(E_NewPolicy__c p : policys){
			system.debug(p.E_NewPolicyDefects__r.size());
		}
		
		Test.StartTest();
		
		E_ExportNewPolicyController controller = new E_ExportNewPolicyController();
		List<String> resultList = controller.getLines();
		
		Test.stopTest();
		
		// assertion
		System.assertEquals(2, resultList.size());
	}
	
	/**
	 * List<String> getLines()
	 * AY
	 */
	static testMethod void getLines_test02(){
		// TestData
		createData();
		
		//ページ情報
		PageReference pref = Page.E_ExportNewPolicy;
		pref.getParameters().put('oid', ayAccList[0].Id);
		Test.setCurrentPage(pref);
		
		Test.StartTest();
		
		E_ExportNewPolicyController controller = new E_ExportNewPolicyController();
		List<String> resultList = controller.getLines();
		
		Test.stopTest();
		
		// assertion
		System.assertEquals(1, resultList.size());
	}
	
	/**
	 * List<String> getLines()
	 * AT
	 */
	static testMethod void getLines_test03(){
		// TestData
		createData();
		
		//ページ情報
		PageReference pref = Page.E_ExportNewPolicy;
		pref.getParameters().put('aid', atConList[0].Id);
		Test.setCurrentPage(pref);
		
		Test.StartTest();
		
		E_ExportNewPolicyController controller = new E_ExportNewPolicyController();
		List<String> resultList = controller.getLines();
		
		Test.stopTest();
		
		// assertion
		System.assertEquals(1, resultList.size());
	}
	
    /** Data作成************************************************************************ */
    /**
     * - Kaku
     * -- Jimu1
     * --- Boshu1
     * -- Jimu2
     * --- Boshu2
     */
	private static void createData(){
		// Account 代理店格
		ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
		insert ahAcc;

		// Account 事務所
		ayAccList = new List<Account>();
		for(Integer i = 1; i < 3; i++){
			ayAccList.add(new Account(
				Name = 'office' + i
				,ParentId = ahAcc.Id
				,E_CL2PF_ZAGCYNUM__c = 'ay00' + i
				,E_COMM_VALIDFLAG__c = '1'
			));
		}
		insert ayAccList;

		// Contact 募集人
		atConList = new List<Contact>();
		for(Integer j = 0; j < ayAccList.size(); j++){
			Contact atCon = new Contact(LastName = 'test',AccountId = ayAccList[j].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[j].E_CL2PF_ZAGCYNUM__c);
			atCon.E_CL3PF_AGNTNUM__c = 'at00' + j;
			atCon.email = 'fstest@terrasky.ingtesting';
			atConList.add(atCon);
		}
		insert atConList;
			
		// 新契約ステータス
		newpolicys = new List<E_NewPolicy__c>();
		for(Integer p = 0; p < atConList.size(); p++){
			newpolicys.add(new E_NewPolicy__c(
				STATCODE__c = 'P'				// 新契約ステータス
				,CHDRNUM__c = '0000000' + p		// 証券番号
				,SyncDate__c = System.now()		// データ発信日
				,ParentAccount__c = ahAcc.Id
				,Account__c = atConList[p].AccountId
				,Agent__c = atConList[p].Id
			));
		}
		insert newpolicys;
	}
	
	/**
	 * 新契約不備情報作成
	 */
	private static void createDefect(Id parentRecId, Integer dataCnt){
		List<E_NewPolicyDefect__c> defects = new List<E_NewPolicyDefect__c>();
		for(Integer i = 0; i < dataCnt; i++){
			defects.add(new E_NewPolicyDefect__c(
				E_NewPolicy__c = parentRecId
				,FUPSTAT__c = 'F'
			));
		}
		insert defects;
	}
}