public with sharing class E_ScrollInfoController extends E_InfoController {
	
	public String scrollMenuMasterKey {get; set;}
	public boolean isRenderedDis {get;set;}
/*	
	//パラメータが渡された時点でinitを実行
	public void setMenuMasterKey(String key){
		menuMasterKey = key;
		System.debug('isRenderedDis============' + isRenderedDis);
		if (isRenderedDis) {
			isValidate();
		}
	}
*/

	
	//コンストラクタ
	public E_ScrollInfoController() {
		super();
	}
	
	//init内でmenuMasterKeyを取得
	protected override String getMenuKey(){
		if(isRenderedDis) return scrollMenuMasterKey;
		return '';
	}
}