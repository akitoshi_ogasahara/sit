/**
 * 不要な新契約ステータスレコードを削除するバッチ
 */
global with sharing class I_DeleteNewPolicyBatch implements Database.Batchable<SObject>, Database.Stateful {

	// 新契約ステータス/申し込み中
	private static final String STATCODE_PENDING = 'P';
	// 新契約ステータス/成立
	private static final String STATCODE_PASSED = 'I';
	// 新契約ステータス/謝絶
	private static final String STATCODE_REJECT = 'Z';
	// 新契約ステータス/取り下げ
	private static final String STATCODE_CANCEL = 'Q';
	// 団体タイプ/Y
	private static final String POLICY_TYPE_GROUP = 'Y';
	//「不要証券番号管理」の募集人コード
	private static final String UNNCESSARY_AGENTNUM = '61167';

	// 削除対象を割り出すための基準日
	private Date baseDate;
	// 削除対象を除外するグループキーの集合
	private Set<String> excludedGroupKeys;
	// DML実行時のエラーを集計するためのハンドラ
	private ErrorHandler errorHandler;

	/**
	 * コンストラクタ
	 */
	public I_DeleteNewPolicyBatch() {
		this(E_Util.SYSTEM_TODAY());
	}

	/**
	 * コンストラクタ
	 * @param baseDate - 削除対象を割り出すための基準日。この日付を-3か月した月の最終日が対象日付となる
	 */
	public I_DeleteNewPolicyBatch(Date baseDate) {
		this.excludedGroupKeys = new Set<String>();
		this.errorHandler = new ErrorHandler();
		this.baseDate = baseDate;
	}

	global Database.QueryLocator start(Database.BatchableContext bc) {
		return createQueryLocator(this.getReferenceNumber());
	}

	global void execute(Database.BatchableContext bc, List<E_NewPolicy__c> scope) {
		List<E_NewPolicy__c> deleteTargets = new List<E_NewPolicy__c>();
		Set<String> groupKeySet = new Set<String>();
		// 削除対象の選定
		for (E_NewPolicy__c policy: scope) {
			String groupKey = policy.GroupKey__c;
			Boolean isExcluded = (String.isNotBlank(groupKey) && excludedGroupKeys.contains(groupKey));
			if (isExcluded) {
				continue;
			}
			Boolean isGrouped = (policy.KFRAMEIND__c == POLICY_TYPE_GROUP);
			// グループされたレコードだった場合
			if (isGrouped) {
				groupKeySet.add(groupKey);
			}
			// 個人レコードだった場合
			else {
				deleteTargets.add(policy);
			}
		}
		// 削除対象となったグループキーが存在した場合
		if (groupKeySet.size() > 0) {
			Map<String, PolicyGroup> groupsByGroupKey = new Map<String, PolicyGroup>();
			// グループキーごとにレコードを振り分ける
			for (E_NewPolicy__c policy: getNewPoliciesByGroupKey(groupKeySet)) {
				String groupKey = policy.GroupKey__c;
				if (!groupsByGroupKey.containsKey(groupKey)) {
					groupsByGroupKey.put(groupKey, new PolicyGroup(groupKey));
				}
				groupsByGroupKey.get(groupKey).add(policy);
			}
			Integer referenceNumber = getReferenceNumber();
			for (PolicyGroup policyGroup: groupsByGroupKey.values()) {
				// グループにされたレコードがすべて削除可なレコードだった場合
				if (policyGroup.canDeleteFrom(referenceNumber)) {
					deleteTargets.addAll(policyGroup.getPolicies());
				}
			}
			// 選定を行ったグループキーを削除除外対象に追加
			excludedGroupKeys.addAll(groupsByGroupKey.keyset());
		}
		// 削除対象が存在した場合
		if (deleteTargets.size() > 0) {
			List<Database.DeleteResult> deleteResults = Database.delete(deleteTargets, false);
			// DML実施結果からエラーが発生しているレコードが存在していた場合情報を内部で保持する
			// DML実行リストと削除結果のリストの並びは一致するので添え字で取得を行う。
			// https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_methods_system_database_deleteresult.htm#apex_methods_system_database_deleteresult
			for (Integer i = 0; i < deleteTargets.size(); i++) {
				E_NewPolicy__c target = deleteTargets.get(i);
				Database.DeleteResult result = deleteResults.get(i);
				if (!result.isSuccess()) {
					this.errorHandler.addError(target, result.getErrors());
				}
			}
		}
	}

	global void finish(Database.BatchableContext bc) {
		// executeのDMLでエラーが発生していた場合、例外を発生させてメール通知を行う。
		if (this.errorHandler.hasError()) {
			throw new GeneralException(this.errorHandler.toString());
		}
	}

	/**
	 * 基準日より-3か月した月の最終日を数値として出力する。
	 * 例）2000/1/1 => 19991031
	 * @return Integer
	 */
	public Integer getReferenceNumber() {
		Date twoMonthAgo = this.baseDate.addMonths(-2);
		Date referenceDate = Date.newInstance(twoMonthAgo.year(), twoMonthAgo.month(), 0);
		String digit8String = E_Util.date2YYYYMMDD(referenceDate);
		return Integer.valueOf(digit8String);
	}

	/** 例外出力用内部クラス */
	private class GeneralException extends Exception {}

	
	/** DMLエラーをハンドリングするためのクラス */
	private class ErrorHandler {
		// key: E_NewPolicy__c.Id, value: エラー文言のリスト
		private Map<Id, List<String>> errorListsByPolicyId;

		/**
		 * コンストラクタ
		 */
		public ErrorHandler() {
			this.errorListsByPolicyId = new Map<Id, List<String>>();
		}

		/**
		 * 受け取った情報をもとにエラー文言を生成し、内部で保持する。1レコードにつき複数のエラー内容が出力される想定
		 * format: [000000000000000AAA: 証券番号] STATUS_CODE - エラーメッセージ\n
		 * @param policy - 新契約ステータスレコード
		 * @param errors - DML実行時のエラー
		 */
		public void addError(E_NewPolicy__c policy, List<Database.Error> errors) {
			List<String> builder = new List<String>();
			for (Database.Error error: errors) {
				builder.add(String.format('[{0}: {1}] {2} - {3}', new List<String>{
					policy.Id,
					policy.CHDRNUM__C,
					error.getStatusCode().name(),
					error.getMessage()
				}));
			}
			if (!this.errorListsByPolicyId.containsKey(policy.Id)) {
				this.errorListsByPolicyId.put(policy.Id, new List<String>());
			}
			this.errorListsByPolicyId.get(policy.Id).add(String.join(builder, '\n'));
		}

		/**
		 * 保持しているエラーの内容が1レコード以上存在していた場合TRUEを返却する。
		 * @return Boolean
		 */
		public Boolean hasError() {
			return this.errorListsByPolicyId.size() > 0;
		}

		/**
		 * 保持しているエラー内容を結合し、出力する。
		 */
		public override String toString() {
			List<String> builder = new List<String>();
			for (List<String> formatedErrors: this.errorListsByPolicyId.values()) {
				builder.addAll(formatedErrors);
			}
			return String.join(builder, '\n');
		}
	}

	/** グループされた新契約ステータスを管理する */
	private class PolicyGroup {
		// 新契約ステータスのグループキー/E_NewPolicy__c.GroupKey__c
		private String groupKey;
		// グループキーでまとめられる新契約ステータスレコードのリスト
		private List<E_NewPolicy__c> policies;
		
		/**
		 * コンストラクタ
		 * @param groupKey - 新契約ステータスのグループキー/E_NewPolicy__c.GroupKey__c
		 */
		public PolicyGroup(String groupKey) {
			this.groupKey = groupKey;
			this.policies = new List<E_NewPolicy__c>();
		}

		/**
		 * 新契約ステータスレコードをグループとして追加する。
		 * @param policy - 新契約ステータスレコード
		 */
		public void add(E_NewPolicy__c policy) {
			this.policies.add(policy);
		}

		/**
		 * 保持している新契約ステータスレコードを取得するためのgetter
		 * @return List<E_NewPolicy__c>
		 */
		public List<E_NewPolicy__c> getPolicies() {
			return this.policies.clone();
		}

		/**
		 * 日付をあらわした数値をもとに処理を行う。
		 * 保持している新契約ステータスレコードのすべてが削除可能である場合TRUEを返却する。
		 * 削除可能であるかの条件は下記の通り。
		 *
		 * - [ステータス/STATCODE__c]が(成立/I)であり、[成立処理日/PTRN_TRDT__c]が基準数値以下であるもの
		 * - [ステータス/STATCODE__c]が(謝絶/Z)であり、[謝絶、取下処理日/RejectDate__c]が基準数値以下であるもの
		 * - [ステータス/STATCODE__c]が(取り下げ/Q)であり、[謝絶、取下処理日/RejectDate__c]が基準数値以下であるもの
		 *
		 * @param referenceNumber - 削除対象レコードであるかを判断するための基準値。 => 例). 20000101
		 * @return Boolean
		 */
		public Boolean canDeleteFrom(Integer referenceNumber) {
			Boolean canDeleteFromAll = true;
			for (E_NewPolicy__c policy: this.policies) {
				Boolean canDelete = true;
				String status = policy.STATCODE__c;
				// ステータスが成立の場合
				if (status == I_DeleteNewPolicyBatch.STATCODE_PASSED) {
					canDelete = (policy.PTRN_TRDT__c <= referenceNumber);
				}
				// ステータスが謝絶もしくは取り下げである場合
				else if (status == I_DeleteNewPolicyBatch.STATCODE_REJECT || status == I_DeleteNewPolicyBatch.STATCODE_CANCEL) {
					canDelete = (policy.RejectDate__c <= referenceNumber);
				}
				// 上記ステータスのいずれにも一致しない場合
				else {
					canDelete = false;
				}
				// 削除できないレコードが一件でも存在した場合、その時点ですべてのレコードが削除対象でなくなるので処理をやめる。
				if (!canDelete) {
					canDeleteFromAll = false;
					break;
				}
			}
			return canDeleteFromAll;
		}
	}

	/**
	 * グループキーと一致する新契約ステータスレコードをすべて取得する。
	 * @param groupKeys - グループキーの集合
	 * @return List<E_NewPolicy__c>
	 */
	private static List<E_NewPolicy__c> getNewPoliciesByGroupKey(Set<String> groupKeys) {
		return [
			SELECT 
				CHDRNUM__C,
				ENTRYDATE__C,
				PTRN_TRDT__C,
				STATCODE__C,
				SYNCDATE__C,
				KFRAMEIND__c,
				RejectDate__c,
				GroupKey__c
			FROM
				E_NewPolicy__c
			WHERE
				GroupKey__c IN: groupKeys
		];
	}

	/**
	 * 削除対象日付をあらわした数値をもとに対象となるレコードをすべて取得するロケータを返却する。
	 * @param referenceNumber - 対象数値。 例). 20000101
	 * @return Database.QueryLocator
	 */
	private static Database.QueryLocator createQueryLocator(Integer referenceNumber) {
		return Database.getQueryLocator([
			SELECT
				CHDRNUM__C,
				ENTRYDATE__C,
				PTRN_TRDT__C,
				STATCODE__C,
				SYNCDATE__C,
				KFRAMEIND__c,
				RejectDate__c,
				GroupKey__c
			FROM
				E_NewPolicy__c
			WHERE
				// 成立していて3か月より古い[成立処理日]をもつもの
				(STATCODE__c =: STATCODE_PASSED AND PTRN_TRDT__C <=: referenceNumber)
				OR
				// 謝絶/取り下げ済みで3か月より古い[謝絶、取下処理日]をもつもの
				(STATCODE__c IN: new List<String>{STATCODE_REJECT, STATCODE_CANCEL} AND RejectDate__c <=: referenceNumber)
				OR
				// 「不要証券番号管理」募集人が持っていて、3か月以上前のもの
				((MainAgentNum5__c =: UNNCESSARY_AGENTNUM OR SubAgentNum5__c =: UNNCESSARY_AGENTNUM) AND EntryDate__c <=: referenceNumber)
		]);
	}
}