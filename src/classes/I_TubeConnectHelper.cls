public class I_TubeConnectHelper {
    public static final String PARAM_IRIS_SESSION = 'session';
    public static final String PARAM_IRIS_ZWEBID = 'user';
    public static final String PARAM_IRIS_USERID = 'sfid';
    public static final String PARAM_IRIS_DOMAIN = 'domain';
	
	public static final String TUBE_tubekun_img = '/tubekun.png';
	public static final String TUBE_ENDPOINT = '/auth.aspx';
	public static final String TUBE_ENDPOINT_META = '/api/get_mediainfo_forlink.aspx';
	public static final String TUBE_ENDPOINT_META_LOG = '/api/get_data.aspx';
	public static final String TUBE_REQUESTTYPE_MOVIE = 'type=3';
	public static final String TUBE_REQUESTTYPE_FAVORITE = 'type=4';
	public static final String TUBE_REQUESTTYPE_HISTORY = 'type=5';

	private E_AccessController access{
									get{
											if(access==null){
												access = E_AccessController.getInstance();
											}
											return access;
									}
									set;
	}
	/**
	 * インスタンス生成
	 */
	private static I_TubeConnectHelper instance = null;
	public static I_TubeConnectHelper getInstance() {
		if (instance == null) {
			instance = new I_TubeConnectHelper();
		}
		return instance;
	}


	/**
	 *	NNTubeへ渡す際に追加するパラメータ
	 */
	public Map<String,String> getUrlParamMapForNNTube(){
		Map<String, String> params = new Map<String,String>();
		params.put(PARAM_IRIS_SESSION, E_Util.getSessionId());
		params.put(PARAM_IRIS_ZWEBID, access.user.E_ZWEBID__c);
		params.put(PARAM_IRIS_USERID, String.valueOf(access.user.Id));
		//params.put(PARAM_IRIS_DOMAIN, URL.getSalesforceBaseUrl().toExternalForm());		//URLFOR('/'))と取得が異なる
		String siteName = Site.getName();
		String path = String.isNotBlank(siteName) ? '/' + siteName : '';
		String domain = URL.getSalesforceBaseUrl().toExternalForm() + path;
		params.put(PARAM_IRIS_DOMAIN, domain);
	
		return params;
	}
	public String getUrlParamsForNNTube(){
		Map<String,String> params = getUrlParamMapForNNTube();
		List<String> plist = new List<String>();
		for(String key:params.keyset()){
			plist.add(key + '=' + E_Util.null2Blank(params.get(key)));
		}
		return String.join(plist,'&');
	}
	//Typeを追加するパターン
	public String getUrlParamsForNNTube(String requestType){
		return getUrlParamsForNNTube() + '&' + requestType;
	}

	// Tube君　画像URL
	public String getTubekunImgUrl(){
		return Label.E_DOMAIN_NN_TUBE + Tube_tubekun_img + '?' + String.valueOf(System.now().getTime());		//画像のキャッシュ対策でtimestampつけておく
	}

	//メタ情報取得API　URL
	public String getTubeMetaUrl(){
		return Label.E_DOMAIN_NN_TUBE + TUBE_ENDPOINT_META;
	}

	//ログメタ情報取得API　URL
	public String getTubeLogMetaUrl(){
		return Label.E_DOMAIN_NN_TUBE + TUBE_ENDPOINT_META_LOG;
	}
	
	//TubeURL TOP
	public String getTubeTopUrl(){
		return Label.E_DOMAIN_NN_TUBE + TUBE_ENDPOINT;
	}
	//個別動画ページへ遷移 TYPE
	public String getParams_MovieType(){
		return getUrlParamsForNNTube(TUBE_REQUESTTYPE_MOVIE);		//'type=3';
	}
	//お気に入りページURL
	public String getTubeFavoriteUrl(){
		return getTubeTopUrl() + '?' + getUrlParamsForNNTube(TUBE_REQUESTTYPE_FAVORITE);		//type=4
	}
	//閲覧履歴ページURL
	public String getTubeHistoryUrl(){
		return getTubeTopUrl() + '?' + getUrlParamsForNNTube(TUBE_REQUESTTYPE_HISTORY);		//type=4
	}
	
	//チャネルコード
	public String getChannelCode(){
		return access.isEmployee()?'EP':access.user.contact.E_KCHANNEL__c;
	}
	//SessionId
	public String getSessionId(){
		return E_Util.getSessionId();
	}
	//sfId
	public String getUserId(){
		return String.valueOf(access.user.Id);
	}
	
	public String getTubeEnvType(){
		if('https://www.nntube.jp' == Label.E_DOMAIN_NN_TUBE){
			return '';
		}
		
		//カスタム表示ラベルに本番URL以外が設定されているときは "st"
		return 'st';
	}
/*
	//ZWEBID
	public String getZWEBID(){
		return access.user.E_ZWEBID__c;
	}
*/	
	
	//Tubeに接続できないときのメッセージ
	public String getCANNOT_CONNECT_TUBE_MSG(){
		return 'ご利用の環境では動画は視聴できません。';
	}



}