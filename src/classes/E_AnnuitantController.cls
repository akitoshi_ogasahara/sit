global with sharing class E_AnnuitantController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_AnnuitantExtender getExtender() {return (E_AnnuitantExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public E_AnnuitantController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1111');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1111',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_Annuitant');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1116');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1116',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.Annuitant__r.E_CLTPF_DOB__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component940');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component940',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_postalCode','{!record.Annuitant__r.E_CLTPF_CLTPCODE__c}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1110');
		tmpPropMap.put('Component__Name','EPostalCodeLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1110',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.Insured__r.E_CLTPF_DOB__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component939');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component939',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1115');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1115',tmpPropMap);


		SObjectField f;

		f = Contact.fields.Name;
		f = Contact.fields.E_CLTPF_ZCLKNAME__c;
		f = Contact.fields.E_COMM_ZCLADDR__c;
		f = Contact.fields.E_CLTPF_CLTPHONE01__c;
		f = Contact.fields.E_CLTPF_CLTPHONE02__c;
		f = Contact.fields.E_CLTPF_FAXNO__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Annuitant__r.Name');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_ZCLKNAME__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_COMM_ZCLADDR__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLTPHONE01__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLTPHONE02__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_FAXNO__c');
			mainQuery.addFieldAsOutput('Insured__r.Name');
			mainQuery.addFieldAsOutput('Insured__r.E_CLTPF_ZCLKNAME__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_DOB__c');
			mainQuery.addFieldAsOutput('Annuitant__r.E_CLTPF_CLTPCODE__c');
			mainQuery.addFieldAsOutput('Insured__r.E_CLTPF_DOB__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_AnnuitantExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}