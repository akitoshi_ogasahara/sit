global class AGC_SetDeleteFlag_Sched implements Schedulable{
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new AGC_SetDeleteFlag_Bch());
    }
}