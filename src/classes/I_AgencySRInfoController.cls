public with sharing class I_AgencySRInfoController {

	private Map<String,Integer> RANK_MAP = new Map<String,Integer>();	//2017/11追加
	{	
		RANK_MAP.put('優績SS',1);
		RANK_MAP.put('プロテクション優績特則',2);
		RANK_MAP.put('優績S',3);
		RANK_MAP.put('優績A',4);
		RANK_MAP.put('優績B',5);
		RANK_MAP.put('上級',6);
		RANK_MAP.put('普通',7);
	}
	//private Map<String,Integer> RANK_MAP = new Map<String,Integer>();	//2017/11追加
	//{
	//	RANK_MAP.put('優績S',1);
	//	RANK_MAP.put('優績A',2);
	//	RANK_MAP.put('優績B',3);
	//	RANK_MAP.put('上級',4);
	//	RANK_MAP.put('普通',5);
	//	RANK_MAP.put('優績SS',0);
	//	RANK_MAP.put('プロテクション優績特則',0);
		
	//}
	public Id paramSalesResultsId {get; set;}				// 代理店挙積情報ID
	public Integer year {get;set;}							//2017/11追加
	public String rankDetail {get;set;}						//2017/11追加
	public String dispJudgment {get;set;}					//2017/11追加
	transient public String[] paramDisclaimer {get; set;}	// 表示ディスクレーマ
	public Map<String, String> disclaimers{					// 表示ディスクレーマ(表示用)
		get{
			if(disclaimers == null){
				// パラメータよりMapを作成
				while(this.paramDisclaimer.size() < 6) {
					this.paramDisclaimer.add('');
				}
				disclaimers = new Map<String, String>();
				disclaimers.put('AMS|SR|001', this.paramDisclaimer[0]);
				disclaimers.put('AMS|SR|002', this.paramDisclaimer[1]);
				disclaimers.put('AMS|SR|006', this.paramDisclaimer[2]);
				disclaimers.put('AMS|SR|014', this.paramDisclaimer[3]);
				disclaimers.put('AMS|SR|015', this.paramDisclaimer[4]);
				disclaimers.put('AMS|SR|016', this.paramDisclaimer[5]);
			}
			return disclaimers;
		}
		set;
	}
	/**
	 * コンストラクタ
	 */
	public I_AgencySRInfoController(){
		this.paramSalesResultsId	= null;											// 代理店挙積情報ID
		this.paramDisclaimer		= new List<String>();							// 表示ディスクレーマ
	}

	//11/1追加
	public E_AgencySalesResults__c getSalesResult(){
		E_AgencySalesResults__c salesResults = E_AgencySalesResultsDao.getRecTypeAById(this.paramSalesResultsId);
		//年取得
		year = Integer.valueOf(salesResults.YYYYMM__c.left(4));		

		//ランク判定処理
		Integer bef = RANK_MAP.get(E_Util.emAlphabetToEn(salesResults.XHAH_AGCLS__c));
		Integer aft = RANK_MAP.get(E_Util.emAlphabetToEn(salesResults.QualifSim_Formula__c));
		if(bef != null && aft != null){
			if(bef > aft){
				rankDetail = 'rankup';
			}
			if(bef < aft){
				rankDetail = 'rankdown';
			}
		}
		if(aft == null){
			dispJudgment = 'Hide';
		}
		return salesResults;
	}
}