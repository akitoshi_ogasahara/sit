global with sharing class E_CustomerInfoExtender extends E_AbstractViewExtender {

	E_CustomerInfoController extension;

	/** ページタイトル */
	private static final String PAGE_TITLE = '契約者照会';
	public String ContractorCLNTNUM {get; private set;}
	public String ContractorName {get; private set;}
	public String AnnuitantCLNTNUM {get; private set;}
	public String AnnuitantName {get; private set;}
	public String cno {get; set;}
	public String cname {get; set;}
	
	public String cId{get;set;}
	public String disId{get;set;}
	public String offId{get;set;}
	public String ageId{get;set;}

	/* コンストラクタ */
	public E_CustomerInfoExtender(E_CustomerInfoController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
		cId = ApexPages.currentPage().getParameters().get('cId');
		disId = ApexPages.currentPage().getParameters().get('disId');
		offId = ApexPages.currentPage().getParameters().get('offId');
		ageId = ApexPages.currentPage().getParameters().get('ageId');

		if(String.isNotBlank(this.cid)){    //cidが取得できない場合はdoAuthでエラーとなる。
			/*各関連テーブルをcidに紐づいたものだけとする*/
			SkyEditor2.Query coliTabQuery = extension.queryMap.get('coliTab');
			coliTabQuery.addWhereIfNotFirst('AND');
			coliTabQuery.addWhere('( E_Policy__r.RecordType.DeveloperName = \'ECHDPF\' AND E_Policy__r.Contractor__c = \'' + cId + '\' )' );
			addWhereParameter(coliTabQuery, false);
			coliTabQuery.addSort('E_Policy__r.COMM_OCCDATE__c',True,True).addSort('COLI_ZPSTDESC__c',True,True).addSort('E_Policy__r.COMM_CHDRNUM__c',True,True);

			SkyEditor2.Query spvaTabQuery = extension.queryMap.get('spvaTab');
			spvaTabQuery.addWhereIfNotFirst('AND');
			spvaTabQuery.addWhere('( Contractor__c = \'' + cId + '\'  OR Annuitant__c  = \'' + cId + '\')' );
			addWhereParameter(spvaTabQuery, true);
	
			SkyEditor2.Query anTabQuery = extension.queryMap.get('anTab');
			anTabQuery.addWhereIfNotFirst('AND');
			anTabQuery.addWhere('( Contractor__c = \'' + cId + '\' OR Annuitant__c  = \'' + cId + '\')' );
			addWhereParameter(anTabQuery, true);
	
			SkyEditor2.Query tiTabQuery = extension.queryMap.get('tiTab');
			tiTabQuery.addWhereIfNotFirst('AND');
			tiTabQuery.addWhere('( Contractor__c = \'' + cId + '\')' ); 
		}
	}

	/* init */
	global override void init(){
system.debug('@@@init');
		//顧客名と顧客番号を変数に設定する
		setContractorName_Num();
		pageRef = doAuth(E_Const.ID_KIND.CONTACT, cId);
		
		// 証券番号リンクの生成　NNLink使用：E_Coli、IRIS使用：IRIS_Coli 
		Integer cnt = extension.coliTab.Items.size();
		for(Integer i = 0; i < cnt; i++){
			if(iris.getIsIRIS()){
				extension.coliTab.Items[i].record.COMM_CHDRNUM__c = extension.coliTab.Items[i].record.E_Policy__r.COMM_CHDRNUM_LINK_I__c;
			}else{
				extension.coliTab.Items[i].record.COMM_CHDRNUM__c = extension.coliTab.Items[i].record.E_Policy__r.COMM_CHDRNUM_LINK__c;
			}
		}
	}

	//個別でチェック判定上書き
	public override boolean isValidate() {
system.debug('@@@isValidate');
		// URLパラメータ整合性チェック
		return checkConsistency();
	}

	/* ページアクション */
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}

	///**
	// * 各子テーブルから１件目を取得し変数に設定する
	// */
	//private void setContractorName_Num(){
	//  if(!extension.spvaTab.Items.isEmpty()){
	//      checkPolicy(extension.spvaTab.Items[0].record);
	//  }else if(!extension.anTab.Items.isEmpty()){
	//      checkPolicy(extension.anTab.Items[0].record);
	//  }else if(!extension.tiTab.Items.isEmpty()){
	//      checkITHPF(extension.tiTab.Items[0].record);
	//  }
	//}

	/**
	 * 各子テーブルから１件目を取得し変数に設定する
	 */
	private void setContractorName_Num(){
		if(!extension.coliTab.Items.isEmpty()){
			checkE_COVPF(extension.coliTab.Items[0].record);
		}else if(!extension.spvaTab.Items.isEmpty()){
			checkPolicy(extension.spvaTab.Items[0].record);
		}else if(!extension.anTab.Items.isEmpty()){
			checkAnnuityPolicy(extension.anTab.Items[0].record);
		}else if(!extension.tiTab.Items.isEmpty()){
			checkITHPF(extension.tiTab.Items[0].record);
		}
	}

	/**
	 * 個人保険特約から顧客番号と顧客名を取得する
	 */
	private void checkE_COVPF(E_COVPF__c p){
system.debug('@@@checkE_COVPF');
		ContractorCLNTNUM = p.E_Policy__r.ContractorCLTPF_CLNTNUM__c;
		ContractorName = p.E_Policy__r.ContractorName__c;
		AnnuitantCLNTNUM = p.E_Policy__r.AnnuitantCLTPF_CLNTNUM__c;
		AnnuitantName = p.E_Policy__r.AnnuitantName__c;
	}

	/**
	 * 保険契約ヘッダから顧客番号と顧客名を取得する
	 */
	private void checkPolicy(E_Policy__c p){
system.debug('@@@checkPolicy');
system.debug('@@@checkE_COVPF=' + p);
		ContractorCLNTNUM = p.ContractorCLTPF_CLNTNUM__c;
		ContractorName = p.ContractorName__c;
		AnnuitantCLNTNUM = p.AnnuitantCLTPF_CLNTNUM__c;
		AnnuitantName = p.AnnuitantName__c;
	}

	/**
	 * 保険契約ヘッダから顧客番号と顧客名を取得する
	 * 社員と代理店ユーザの場合は、契約者の顧客番号と顧客名
	 * 契約者と年金受取人の場合は、年金受取人の顧客番号と顧客名
	 */
	private void checkAnnuityPolicy(E_Policy__c p){
system.debug('@@@checkAnnuityPolicy');
system.debug('@@@checkE_Policy=' + p);
system.debug('@@@checkcidy=' + cid);
		if(access.isCustomer()){
			ContractorCLNTNUM = p.AnnuitantCLTPF_CLNTNUM__c;
			ContractorName = p.AnnuitantName__c;
		} else {
			ContractorCLNTNUM = p.ContractorCLTPF_CLNTNUM__c;
			ContractorName = p.ContractorName__c;
		}
		AnnuitantCLNTNUM = p.AnnuitantCLTPF_CLNTNUM__c;
		AnnuitantName = p.AnnuitantName__c;
	}
	
	/**
	 * 投信ヘッダから顧客番号と顧客名を取得する
	 */
	private void checkITHPF(E_ITHPF__c i){
system.debug('@@@checkITHPF');
		ContractorCLNTNUM = i.ContractorCLTPF_CLNTNUM__c;
		ContractorName = i.ContractorName__c;
	}

	/**
	 * URLパラメータと取得データをチェックする整合性チェックする
	 * @param ---
	 * @return boolean： 一致してればTRUE
	 */
	private boolean checkConsistency(){
		//Customerコミュニティユーザはチェックしない
		if(access.isCustomer()){
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
		cno = ApexPages.currentPage().getParameters().get('cNo');
		cname = ApexPages.currentPage().getParameters().get('cName');
system.debug('@@@cno:'+ cno);
system.debug('@@@cname:'+ cname);
system.debug('@@@ContractorCLNTNUM:'+ ContractorCLNTNUM);
system.debug('@@@ContractorName:'+ ContractorName);
		if(String.isBlank(cno) || String.isBlank(cname)
				 || String.isBlank(ContractorCLNTNUM) || String.isBlank(ContractorName)){
			return false;
		}else{
			if (ContractorCLNTNUM.equals(cno) && ContractorName.equals(cname)) {
				return true;
			} else if (AnnuitantCLNTNUM.equals(cno) && AnnuitantName.equals(cname)) {
				ContractorCLNTNUM = AnnuitantCLNTNUM;
				ContractorName = AnnuitantName; 
				return true;
			} else {
				return false;
			}
		}
	}
	
	
	/**
	 *  CallCenterユーザ時に標準の取引先責任者ページに遷移させる
	 */
	public PageReference toContactStandardPage(){
		ApexPages.standardController contactController = new ApexPages.standardController(new Contact(id = cid));
		return contactController.view();
	}
	
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
System.debug('***********E_CustomerInfoExtender doReturn getCookieReferer:'+E_CookieHandler.getCookieReferer());
System.debug('***********E_CustomerInfoExtender doReturn getCookieRefererPolicy:'+E_CookieHandler.getCookieRefererPolicy());
System.debug('***********E_CustomerInfoExtender doReturn getCookieRefererContact:'+E_CookieHandler.getCookieRefererContact());
		/* スプリントバックログ 19対応　start  2017/1/20 */
		String pr = E_CookieHandler.getCookieRefererContact();
		E_CookieHandler.setCookieContactReferer(null);

		return new PageReference(pr);
		/* スプリントバックログ 19対応　end */
//		return new PageReference(E_CookieHandler.getCookieRefererPolicy());
//		return Page.E_CustomerSearch;
	}
	
	private void addWhereParameter(SkyEditor2.Query query, Boolean isPolicy) {
		if (getIsEmployee()) {
			if (!String.isBlank(disId)) {
				query.addWhereIfNotFirst('AND');
				if (isPolicy) {
					query.addWhere('( MainAgentAccount__r.ParentId = \'' + disId + '\' OR SubAgentAccount__r.ParentId = \'' + disId + '\' )' );
				} else {
					query.addWhere('( E_Policy__r.MainAgentAccount__r.ParentId = \'' + disId + '\' OR E_Policy__r.SubAgentAccount__r.ParentId = \'' + disId + '\' )' );
				}
				
			}
			if (!String.isBlank(offId)) {
				query.addWhereIfNotFirst('AND');
				if (isPolicy) {
					query.addWhere('( MainAgentAccount__c = \'' + offId + '\' OR SubAgentAccount__c = \'' + offId + '\' )' );
				} else {
					query.addWhere('( E_Policy__r.MainAgentAccount__c = \'' + offId + '\' OR E_Policy__r.SubAgentAccount__c = \'' + offId + '\' )' );
				}
			}
			if (!String.isBlank(ageId)) {
				query.addWhereIfNotFirst('AND');
				if (isPolicy) {
					query.addWhere('( MainAgent__c = \'' + ageId + '\' OR SubAgent__c = \'' + ageId + '\' )' );
				} else {
					query.addWhere('( E_Policy__r.MainAgent__c = \'' + ageId + '\' OR E_Policy__r.SubAgent__c = \'' + ageId + '\' )' );
				}
			}
		}
	}
}