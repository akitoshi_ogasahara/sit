@isTest
private class TestE_freezeUserBatch {
	public static Datetime nowDate;  //バッチ起動日時
	public static String usType;        //E_Const.USERTYPE_POWERPARTNER
	public static Datetime userFreezeDate;    //現在日時のN年+M日前

	static testMethod void test01() {
		nowDate = Datetime.now();
		User us = createUser();

		Profile partnerProfile = E_ProfileDaoWithout.getRecByName(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);
		Id partnerProfileId = partnerProfile.Id;

		Id userId = us.Id;

		String query =
			'select id from user ' +
			'where isActive = true ' +
			'and ProfileId = :partnerProfileId ' +
			'and Id = :userId ' +
			'and ((LastLoginDate < :userFreezeDate AND LastLoginDate != null) ' +
				'OR (LastModifiedDate < :userFreezeDate AND LastLoginDate = null))';

		userFreezeDate = nowDate.addYears(-Integer.valueOf(System.Label.E_FREEZEUSER_YEARS));
		userFreezeDate = userFreezeDate.addDays(-Integer.valueOf(System.Label.E_FREEZEUSER_DAYS));

		Test.startTest();

		E_freezeUserBatch frzBatch = new E_freezeUserBatch();
		Database.executeBatch(frzBatch, 200);

		Test.stopTest();


		Integer j =  Database.query(query).size();    //バッチ起動後
		System.assertEquals(j, 0);
		System.debug('userNum___'+ j);
	}

	static testMethod void test_createEIDR_for_Delete(){
		E_IDCPFTriggerHandler.isSkipTriggerActions = true;    //EIDCのトリガを実行しない MIXED_DML対策
		User us = createUser();
		E_IDCPFTriggerHandler.isSkipTriggerActions = false;

		//0315対応
		Set<Id> userIds = new Set<Id>{us.Id};

		Test.startTest();
		E_freezeUserBatch batch = new E_freezeUserBatch();
		//0315対応
		E_freezeUserBatch.createEidrQueueable createeidr = new E_freezeUserBatch.createEidrQueueable(userIds);

		//batch.createEIDR_for_Delete(new Set<Id>{us.Id});

		//0315対応
		System.enqueueJob(createeidr);
		Test.stopTest();

		 List<E_IDRequest__c> eidrs = E_IDRequestDaowithout.getRecsByAgentUserId(new Set<Id>{us.Id});
		 System.assertEquals(eidrs.size(), 1);
		 System.assertEquals(eidrs[0].ZUPDFLAG__c, E_Const.ZUPDFLAG_D);  //種別　D　で作成済
	}

	//システム日付との比較でユーザ凍結バッチ処理実行可否判断のテスト
	static testMethod void test_freezeUserBatchIsAvailable(){
		E_freezeUserBatch batch = new E_freezeUserBatch();
		Date startDate_Of_Freeze = I_Const.IRIS_GOLIVE_DAY.addYears(Integer.valueOf(System.Label.E_FREEZEUSER_YEARS))
															.addDays(Integer.valueOf(System.Label.E_FREEZEUSER_DAYS));

		E_Util.systemToday_forTest = startDate_Of_Freeze;
		System.assert(batch.freezeUserBatchIsAvailable(),false);

		E_Util.systemToday_forTest = startDate_Of_Freeze.addDays(1);
		System.assert(batch.freezeUserBatchIsAvailable(),true);
	}


	static testMethod void test_for_coverage(){
		E_IDCPFTriggerHandler.isSkipTriggerActions = true;    //EIDCのトリガを実行しない MIXED_DML対策
		User us = createUser();
		E_IDCPFTriggerHandler.isSkipTriggerActions = false;
			//システム日付を設定（Golive日とSystem.today()の大きい方を基準に考える）
			Date baseDate = I_Const.IRIS_GOLIVE_DAY>System.today()?I_Const.IRIS_GOLIVE_DAY:System.today();
			Date startDate_Of_Freeze = baseDate.addYears(Integer.valueOf(System.Label.E_FREEZEUSER_YEARS))
																.addDays(Integer.valueOf(System.Label.E_FREEZEUSER_DAYS));
			E_Util.systemToday_forTest = startDate_Of_Freeze.addDays(1);

			E_freezeUserBatch batch = new E_freezeUserBatch();
			Test.startTest();
			Database.executeBatch(batch, 200);
			Test.stopTest();
		
	}

	//処理対象レコード　0件の場合
	static testMethod void test_freezeIsNotAvailable(){
		E_IDCPFTriggerHandler.isSkipTriggerActions = true;    //EIDCのトリガを実行しない MIXED_DML対策
		User us = createUser();
		E_IDCPFTriggerHandler.isSkipTriggerActions = false;

		//システム日付を設定（Golive日とSystem.today()の大きい方を基準に考える）
		Date baseDate = I_Const.IRIS_GOLIVE_DAY>System.today()?I_Const.IRIS_GOLIVE_DAY:System.today();
		Date startDate_Of_Freeze = baseDate.addYears(Integer.valueOf(System.Label.E_FREEZEUSER_YEARS))
															.addDays(Integer.valueOf(System.Label.E_FREEZEUSER_DAYS));
		E_Util.systemToday_forTest = startDate_Of_Freeze.adddays(-1);          //処理対象外

		E_freezeUserBatch batch = new E_freezeUserBatch();
		Test.startTest();
		Database.executeBatch(batch, 200);
		Test.stopTest();
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.E_CL1PF_KCHANNEL__c = 'TEST';
		insert acc;
		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	//ユーザ作成
	static User createUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = con.Id;
			insert us;

			E_IDCPF__c idcpf = new E_IDCPF__c();
			idcpf.ZSTATUS01__c = '1';
			idcpf.User__c = us.Id;
			idcpf.OwnerId = us.Id;
			idcpf.ZDSPFLAG02__c = '0';
			insert idcpf;
		}
		return us;
	}
}