@isTest
private class TestE_UPError999Controller {

    static testMethod void testUPError_DefaultMessage() {
        createMessageMaster_Messages(true);
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', E_Const.SYSTEM_ADMINISTRATOR);
		
		PageReference nowPage = Page.up999;
		nowPage.getParameters().put('code', 'ERR|000');
		Test.setCurrentPage(nowPage);
		
		//エラーメッセージ
		String expectedMsg = 'エラーが発生しました。[ERR|000]';
		
		String actualUrl;
		String actualMsg;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			E_UPError999Controller ctr = new E_UPError999Controller();
			Test.startTest();
			ctr.init();
			actualUrl = ApexPages.currentPage().getUrl();
			actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
			Test.stopTest();
		}
		system.assertEquals(nowPage.getUrl(), actualUrl);
		system.assertEquals(expectedMsg, actualMsg);
    }
    
    static testMethod void testUPError_MessageOfMessageMaster() {
        createMessageMaster_Messages(true);
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', E_Const.SYSTEM_ADMINISTRATOR);
		
		PageReference nowPage = Page.up999;
		nowPage.getParameters().put('code', 'ERR|004');
		nowPage.getParameters().put('p1', 'testMenuId');
		nowPage.getParameters().put('p2', 'testMenu');
		Test.setCurrentPage(nowPage);
		
		String expectedMsg = '指定されたページは存在しません。([menuid:testMenuId /menu:testMenu])';
		String actualUrl;
		String actualMsg;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			E_UPError999Controller ctr = new E_UPError999Controller();
			Test.startTest();
			ctr.init();
			actualUrl = ApexPages.currentPage().getUrl();
			actualMsg = ctr.getPageMessages().getErrorMessages().get(0).summary;
			Test.stopTest();
		}
		system.assertEquals(nowPage.getUrl(), actualUrl);
		system.assertEquals(expectedMsg, actualMsg);
    }
    
    
/*********************************************************************************************************************
 * テストデータ作成用
 *********************************************************************************************************************/
    
    /**
	 * Profileマップ取得
	 * @return Map<String, Id>: Map of ProfileName & ProfileID
	 */
	private static Map<String, Id> pMap;
	private static Map<String, Id> getProfileIdMap(){
		if(pMap != null){
			return pMap;
		}
		pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}
    
    /**
	 * ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	private static User createUser(Boolean isInsert, String LastName, String profileDevName){
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}
    
	/**
	 * RecordTypeマップ取得
	 * @return Map<String, Map<String, Id>>: Map(sObjectType, Map(RecTypeDevName,RecTypeID)
	 */
	private static Map<String, Map<String, Id>> rMap;
	private static Map<String, Map<String, Id>> getRecTypeIdMap(){
		if(rMap != null) return rMap;
		rMap = new Map<String, Map<String, Id>>();
		for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
			if(rMap.containsKey(rt.sObjectType)){
				rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
			}else{
				rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
			}
		}
		return rMap;
	}
    
    private static E_MessageMaster__c[] createMessageMaster_Messages(Boolean isInsert) {
		Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get('MSG_DISCLAIMER');
		final String TYPE_MESSAGE = 'メッセージ';
		final String MSG_NAME = 'name';
		List<E_MessageMaster__c> src = new List<E_MessageMaster__c>{
			  new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|001', Value__c='ご利用いただきありがとうございます。<br>以下の変更を受付いたしました。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|002', Value__c='<h>お客様のご住所・電話番号などの変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>当日の受付日時が、当社営業日の09時00分以前のお申込については当日受付となり、原則として翌営業日に反映されます。</li> <li>なお、当社で受信が確認できましたら、ご登録の電子メールアドレスへ受付のご連絡をさせていただきます。</li> <li>ご不明な点はサービスセンター(フリーダイヤル0120-521-513)までお問い合せください</li> </ol>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|003', Value__c='<h>電子メールアドレスのご変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>いま現在、新メールアドレスに反映ずみです。</li> </ol>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|004', Value__c='<h>基本サービス用パスワードのご変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>いま現在、新パスワードに反映ずみです。</li> <li>なお、確認のため「お手続受付完了のお知らせ」メールを、ご登録の電子メールアドレス宛に送信いたします。</li> </ol>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|005', Value__c='<h>手続サービス用パスワードのご変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>いま現在、新パスワードに反映ずみです。</li> <li>なお、確認のため「お手続受付完了のお知らせ」メールを、ご登録の電子メールアドレス宛に送信いたします。</li> </ol>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|006', Value__c='<h>お客様のご住所・電話番号などの変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>当日の受付日時が、当社営業日の09時00分以前のお申込については当日受付となり、原則として翌営業日に反映されます。</li> <li>なお、当社で受信が確認できましたら、ご登録の電子メールアドレスへ受付のご連絡をさせていただきます。</li> <li>ご不明な点はサービスセンター(フリーダイヤル0120-521-513)までお問い合せください</li> </ol> <br> <h>手続サービス用パスワードのご変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>いま現在、新パスワードに反映ずみです</li> </ol>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACC|007', Value__c='<h>基本サービス用パスワードのご変更:</h> <ol style="list-style:none;margin:0 0 0 1em;padding:0;"> <li>パスワードの有効期間は90日となります。90日経過してログインされますと</li> <li>パスワード変更画面が表示されますので、その都度、ご自身でご変更いただきますようお願いいたします。</li> </ol>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|001', Value__c='住所の入力に処理できない文字が含まれています。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|002', Value__c='住所カナの入力に処理できない文字が含まれています。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|003', Value__c='電話番号は数字のみで入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|004', Value__c='電話の入力が完全でありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|005', Value__c='自宅電話番号と日中連絡先は、すくなくともどちらか一つご入力ください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|006', Value__c='正しい手続パスワードを入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACE|007', Value__c='入力された住所が長すぎます。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACM|001', Value__c='変更する部分をすべてご入力ください')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACM|002', Value__c='<span class="font_NNDarkOrange">住所：</span>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ACM|003', Value__c='<span class="font_NNDarkOrange">電話・FAX番号： ※自宅電話番号と日中連絡先は、すくなくともどちらか一つご入力ください。</span>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CID|009', Value__c='解約請求書ダウンロードをするには、同意確認をする必要があります。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CPL|001', Value__c='郵便番号は数字のみで入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CPL|002', Value__c='郵便番号を入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CPL|003', Value__c='入力された郵便番号では、住所は検索されませんでした。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CSH|001', Value__c='<span class="font_NNDarkOrange">検索キー指定フォームに何も入力されていません。</span>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CSH|002', Value__c='<span class="font_NNDarkOrange">条件に合致する参照可能な顧客情報が見つかりませんでした。</span>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CSH|003', Value__c='<span class="font_NNDarkOrange">検索可能なデータ数の上限を超えました。条件を再度設定の上、再検索をしてください。</span>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='CSH|004', Value__c='<span class="font_NNDarkOrange">証券番号/年金証書番号は８桁の数字で入力して下さい。</span>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|001', Value__c='<font size="5">データにアクセスする権限がありません。</font>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|002', Value__c='<font size="5">画面を表示する情報が不正です。</font>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|003', Value__c='<font size="5">画面を参照する権限がありません。</font>')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|004', Value__c='指定されたページは存在しません。([menuid:{0} /menu:{1}])')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ERR|005', Value__c='指定されたレポートは存在しません。([reportName:{0}])')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEC|000', Value__c='積立金の移転手続きを進めるには、同意確認をする必要があります。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEE|000', Value__c='積立金の移転手続きを進めるには、同意確認をする必要があります。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEE|001', Value__c='新しい積立金割合の合計値が100になるようにしてください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEE|002', Value__c='株式比率の合計が{0}%となり、規定の{1}%を超えています。新しい積立金割合を再度指定してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEE|003', Value__c='新しい積立金の割合の入力は半角の整数値に限ります。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEE|004', Value__c='積立金の割合が変更されていません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FEE|005', Value__c='現在の手続サービス用パスワードが正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='FES|006', Value__c='手続サービス用パスワードが入力されていません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='IER|000', Value__c='繰入比率の変更手続きを進めるには、同意確認をする必要があります')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='IER|001', Value__c='新しい繰入比率の合計値が100になっていません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='IER|003', Value__c='新しい繰入比率の割合の入力は半角の整数値に限ります。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='IER|004', Value__c='繰入比率の割合が変更されていません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='IER|005', Value__c='正しい手続パスワードを入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='IND|000', Value__c='繰入比率の変更手続きを進めるには、同意確認をする必要があります。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ISS|001', Value__c='認証情報が不正です。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ISS|002', Value__c='画面を参照する権限がありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ISS|003', Value__c='対象の契約情報がございません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='ISS|004', Value__c='対象のユーザは存在しません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='LGE|001', Value__c='入力されたユーザーIDは不正です。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='LRE|201', Value__c='不正な画面遷移です。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='MAC|001', Value__c='新規電子ﾒｰﾙアドレスを入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='MAC|002', Value__c='新規電子ﾒｰﾙアドレス（確認）を入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='MAC|003', Value__c='新規電子ﾒｰﾙアドレス（確認）が正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='MAC|004', Value__c='以前と同じメールアドレスへの変更は受け付けできません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='MAC|005', Value__c='入力したEメールアドレスが不正です。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGB|001', Value__c='取扱店番（支店コード）は、入力必須項目です。検索したい店番コードを数字４桁で入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGB|002', Value__c='対象店番は、数字４桁で入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGB|003', Value__c='ステータスを少なくとも１つ選択してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGJ|001', Value__c='検索する申込番号を入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGJ|002', Value__c='申込番号は半角英数9文字で入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGJ|003', Value__c='検索する商品コードを入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PGJ|004', Value__c='商品コードは半角英数2文字で入力してください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PPC|001', Value__c='現在の手続パスワードを入力して下さい')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PPC|002', Value__c='現在の手続サービス用パスワードが正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PPC|003', Value__c='変更内容が入力されていません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PPC|004', Value__c='入力規則に従ってパスワードを変更して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PPC|005', Value__c='変更内容（確認）が入力されていません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='PPC|006', Value__c='新規パスワード（確認）が正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPV|003', Value__c='* 左記表示日において解約した場合の金額を表示しておりま<br/> す。 <br/> <br/> * 契約日が2003年8月1日以前のご契約で定額年金のお客様は、年金支<br/> 払開始日まで２ヶ月以内の場合は、「解約返戻金」「死亡給付金」等の<br/> 金額に<br/>つきましては弊社にお問い合わせください。<br/> <br/> * 契約日が2003年8月1日以前で定額年金特則を適用しているご契約について<br/> 　年金支払開始日まで2ケ月以内の場合は、「解約返戻金」<br/> 「死亡給付金」等の金額につきましては弊社にお問い合わせください。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|004', Value__c='現在の基本サービス用パスワードが入力されていません')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|005', Value__c='新規基本サービス用パスワードを入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|006', Value__c='新規基本サービス用パスワード（確認）を入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|007', Value__c='以前と同じ基本サービス用パスワードへの変更は受け付けできません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|008', Value__c='新規基本サービス用パスワード（確認）が正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|101', Value__c='新規パスワードを入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|102', Value__c='新規パスワード（確認）を入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|103', Value__c='電子メールアドレスを入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|104', Value__c='電子メールアドレス（確認）を入力して下さい。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|105', Value__c='新規パスワード（確認）が正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|106', Value__c='電子メールアドレス（確認）が正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|107', Value__c='電子メールアドレスが正しくありません。')
			, new E_MessageMaster__c(Name=MSG_NAME, RecordTypeId=recTypeId, Type__c=TYPE_MESSAGE, Key__c='SPW|108', Value__c='現在のパスワードが入力されていません。')
		};
		if(isInsert){
			insert src;
		}
		return src;
	}
}