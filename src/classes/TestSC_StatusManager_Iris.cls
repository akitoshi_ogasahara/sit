@isTest
private class TestSC_StatusManager_Iris {
	
	/**
	 * Boolean getIsAGMGR() 業務管理責任者
	 */
	@isTest static void getIsAGMGR_test001() {
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 業務管理責任者
		SC_AgencyManager__c agManager = new SC_AgencyManager__c();
		agManager.SC_Office__c = record.Id;
		agManager.User__c = actUser.Id;
		insert agManager;

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	    	TestSC_TestUtil.createAgencyPermissions(actUser.Id);

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	Test.stopTest();
	    	
	    	// assert
	    	System.assertEquals(true, manager.getIsAG());
	    	System.assertEquals(true, manager.getIsAGMGR());
	    }
	}
	
	/**
	 * Boolean getIsAG() 自主点検（代理店） 権限セット
	 */
	@isTest static void getIsAG_test001() {
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	    	TestSC_TestUtil.createAgencyPermissions(actUser.Id);

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	Test.stopTest();
	    	
	    	// assert
	    	System.assertEquals(true, manager.getIsAG());
	    	System.assertEquals(false, manager.getIsAGMGR());
	    }
	}

	/**
	 * Boolean getIsMR() 自主点検（社員） 権限セット　かつ　（MRプロファイル　または　本社営業部MRプロファイル）
	 */
	@isTest static void getIsMR_test001() {
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'MR', 'ＭＲ');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	Test.stopTest();

			// assert
			System.assertEquals(true, manager.getIsMR());
		}
	}

	/**
	 * Boolean getIsMGR() 自主点検（社員） 権限セット　かつ　（拠点長プロファイル　または　本社営業部拠点長プロファイル）
	 */
	@isTest static void getIsMGR_test001() {
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'MRManager', '拠点長');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	Test.stopTest();

			// assert
			System.assertEquals(true, manager.getIsMGR());
		}
	}

	/**
	 * Boolean getIsCMD() 自主点検（コンプラ） 権限セット
	 */
	@isTest static void getIsCMD_test001() {
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	Test.stopTest();

			// assert
			System.assertEquals(true, manager.getIsCMD());
		}
	}

	/**
	 * Boolean getIsPermissionReadOnly() 自主点検（参照のみ）権限セット
	 */
	@isTest static void getIsPermissionReadOnly_test001() {
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'readOnly', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createReadOnlyPermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null, SC_Const.AMSOFFICE_STATUS_01, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	Test.stopTest();

			// assert
			System.assertEquals(true, manager.getIsPermissionReadOnly());
		}
	}

	/**
	 * PageReference doNextAction()
	 * PageReference doApply(Boolean isRemand)
	 */
	@isTest static void doNextAction_test001() {
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	    	TestSC_TestUtil.createAgencyPermissions(actUser.Id);

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doNextAction();
	    	Test.stopTest();
	    	
	    	// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_04, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    	System.assert(true == String.isBlank(resultRec.Defect__c));
	    	System.assert(true == String.isBlank(resultRec.RemandSource__c));
	    	System.assert(true == (resultRec.CMDRemandDate__c == null));
	    }
	}

	/**
	 * PageReference doNextAction()
	 * PageReference doApply(Boolean isRemand)
	 */
	@isTest static void doNextAction_test002() {
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
		record.RemandSource__c = 'CMD';

		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	    	TestSC_TestUtil.createAgencyPermissions(actUser.Id);

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doNextAction();
	    	Test.stopTest();
	    	
	    	// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_04, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    }
	}

	/**
	 * PageReference doNextAction()
	 * PageReference doApplyMR()
	 */
	@isTest static void doNextAction_test003() {
		// テストデータ作成
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
		record.RemandSource__c = 'MR';

		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', record);

		// 代理店ユーザで実行
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
	    	TestSC_TestUtil.createAgencyPermissions(actUser.Id);

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doNextAction();
	    	Test.stopTest();
	    	
	    	// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_02, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    }
	}

	/**
	 * PageReference doNextAction()
	 * PageReference doReportMR()
	 */
	@isTest static void doNextAction_test004() {
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'MR', 'ＭＲ');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_02, '2018');
			record.RemandSource__c = 'MR';

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doNextAction();
	    	Test.stopTest();

			// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_04, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    }
	}

	/**
	 * PageReference doNextAction()
	 * PageReference doApprove()
	 */
	@isTest static void doNextAction_test005() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_04, '2018');
			record.RemandSource__c = 'MR';

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doNextAction();
	    	Test.stopTest();

			// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_05, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    	System.assert(true == String.isBlank(resultRec.Defect__c));
	    }
	}

	/**
	 * PageReference doRejectAction()
	 * PageReference doRejectApproveAG()
	 */
	@isTest static void doRejectAction_test001() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_04, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doRejectAction();
	    	Test.stopTest();

			// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_01, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    	System.assertEquals(SC_Const.DEFECT_STATUS_02, resultRec.Defect__c);
	    	System.assertEquals('CMD', resultRec.RemandSource__c);
	    	System.assert(true == (resultRec.CMDRemandDate__c != null));
	    }
	}

	/**
	 * PageReference doRejectAction()
	 * PageReference doRejectReportMR()
	 */
	@isTest static void doRejectAction_test002() {
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'MR', 'ＭＲ');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_02, '2018');
			record.RemandSource__c = 'MR';
			record.CMDRemandDate__c = System.Now();

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doRejectAction();
	    	Test.stopTest();

			// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_01, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    	System.assertEquals(SC_Const.DEFECT_STATUS_02, resultRec.Defect__c);
	    	System.assertEquals('MR', resultRec.RemandSource__c);
	    	System.assert(true == (resultRec.CMDRemandDate__c != null));
	    }
	}

	/**
	 * PageReference doRejectAction()
	 * PageReference doRejectComplete()
	 */
	@isTest static void doRejectAction_test003() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_05, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doRejectAction();
	    	Test.stopTest();

			// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_04, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    }
	}

	/**
	 * PageReference doRejectActionCmd()
	 * PageReference doRejectApproveMR()
	 */
	@isTest static void doRejectActionCmd_test001() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);

			// テストデータ作成
			Account acc = TestSC_TestUtil.createAccount(true, null);
			SC_Office__c record = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_04, '2018');

	    	Test.startTest();
	    	SC_StatusManager_Iris manager = new SC_StatusManager_Iris(record, E_AccessController.getInstance());
	    	manager.doRejectActionCmd();
	    	Test.stopTest();

			// assert
	    	SC_Office__c resultRec = [Select Id, Status__c, StatusHistory__c, Defect__c, RemandSource__c, CMDRemandDate__c From SC_Office__c Where Id = :record.Id];
	    	System.assertEquals(SC_Const.AMSOFFICE_STATUS_02, resultRec.Status__c);
	    	System.assert(true == String.isNotBlank(resultRec.StatusHistory__c));
	    	System.assertEquals(SC_Const.DEFECT_STATUS_02, resultRec.Defect__c);
	    	System.assertEquals('CMD', resultRec.RemandSource__c);
	    	System.assert(true == (resultRec.CMDRemandDate__c != null));
	    }
	}
}