public with sharing class E_SPRPF_WeeklyDao {

	public static List<E_SPRPF_Weekly__c> getRecByPolicyId(Id policyId, String startdate) {
		return
			[Select 
				Id, 
				ZPRMSDTE__c, 
				ZREVAMT__c, 
				SUMINS__c,
				ZRCHTSUM__c,
				ZACTSUM__c,
				ZSTUPSUM__c,
				ZRCDCF__c,
				ZSTUPDSP__c
			From
				E_SPRPF_Weekly__c 
			Where
				E_Policy__c =: policyId
			And
				ZPRMSDTE__c >=: startdate
			Order by
				ZPRMSDTE__c asc
		];
	}
	
	public static List<E_SPRPF_Weekly__c> getRecByPolicyIdMax(Id policyId) {
		return
			[Select 
				Id, 
				ZPRMSDTE__c, 
				ZREVAMT__c, 
				SUMINS__c,
				ZRCHTSUM__c,
				ZACTSUM__c,
				ZSTUPSUM__c,
				ZRCDCF__c,
				ZSTUPDSP__c
			From
				E_SPRPF_Weekly__c 
			Where
				E_Policy__c =: policyId
			Order by
				ZPRMSDTE__c desc
			limit 1
		];
	}
}