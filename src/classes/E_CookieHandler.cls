/**
 * Cookieハンドラークラス
 * CreatedDate 2014/12/05
 * @Author Terrasky
 */
public class E_CookieHandler {
	/** 保持できる最大バイト数は4096 **/
	/** クッキーに保持可能なCookie項目数はドメインにより **/

	//ISSフラグ保持用CookieキーName　→未使用
	private static final String ISS_FLAG = 'issflag';

	//セッションID保持用CookieキーName　→未使用
	private static final String SESSION_ID = 'sessionid';

	//メニューコンポーネント-メニュー種別保持用CookieキーName
	private static final String MENU_KIND = 'menukind';

	//コンプライアンス経由保持用CookieキーName
	private static final String COMPLIANCE_SESSION_ID = 'compliance_sessionid';
	
	//ISS用セッションID保持用CookieキーName
	private static final String ISS_SESSION_ID = 'iss_sessionid';
	
	//メニューコンポーネント-アクセス種別保持用CookieキーName
	private static final String ACCESS_KIND = 'accesskind';
	
	//遷移元画面URL保持用CookieキーName
	private static final String REFERER = 'referer';
	
	//遷移元画面URL保持用CookieキーName(汎用)
	private static final String GENERAL_REFERER = 'generalReferer';
	
	//遷移元画面URL契約内容個別照会保持用CookieキーName
	public static final String REFERER_POLICY = 'refererPolicy';
	
	//遷移元画面URLポートフォリオ保持用CookieキーName
	public static final String REFERER_PORTFOLIO = 'refererPortfolio';
	
	//遷移元画面URL特別勘定保持用CookieキーName
	public static final String REFERER_INVE = 'refererIvne';
	
	//遷移元画面URL手続履歴保持用CookieキーName
	public static final String REFERER_HISTORY = 'refererHistory';
	
	//遷移元画面URL100件契約照会保持用CookieキーName
	public static final String REFERER_SEARCH = 'refererSearch';

	//[IRIS]IRIS UI 使用フラグCookieキーName
	public static final String USE_IRIS = 'useIRIS';

	//[IRIS]簡易代理ログイン中フラグ CookieキーName
	public static final String IS_AUTO_SEARCH = 'isAutoSearch';

	//[IRIS]簡易代理ログイン - 代理店SFID CookieキーName
	public static final String AUTO_SEARCH_AGENCY = 'autoSearchAgency';

	//[IRIS]簡易代理ログイン - 事務所SFID CookieキーName
	public static final String AUTO_SEARCH_OFFICE = 'autoSearchOffice';

	//[IRIS]簡易代理ログイン - 募集人SFID CookieキーName
	public static final String AUTO_SEARCH_AGENT  = 'autoSearchAgent';

/* スプリントバックログ 19対応　start  2017/1/20 */
//契約者一覧 
public static final String REFERER_CONTACT  = 'refererContact';
/* スプリントバックログ 19対応　end */
	/**
	 * ISSフラグ用Cookie登録　→未使用
	 */
	public static void setCookieIssFlag(String value){
		saveCookieValue(ISS_FLAG, value);
	}
	/**
	 * ISSフラグ用Cookie取得　→未使用
	 */
	public static String getCookieIssFlag(){
		return getCookieValue(ISS_FLAG);
	}

	/**
	 * ISSフラグ用Cookieクリア　→未使用
	 */
	public static void clearCookieIssFlag(){
		clearCookieValue(ISS_FLAG);
	}

	/**
	 * セッションID用Cookie登録　→未使用
	 */
	public static void setCookieSessionId(String value){
		saveCookieValue(SESSION_ID, value);
	}
	/**
	 * セッションID用Cookie取得　→未使用
	 */
	public static String getCookieSessionId(){
		return getCookieValue(SESSION_ID);
	}

	/**
	 * セッションID用Cookieクリア　→未使用
	 */
	public static void clearCookieSessionId(){
		clearCookieValue(SESSION_ID);
	}

	/**
	 * ISS用セッションIDのCookie登録
	 */
	public static void setCookieIssSessionId(){
		saveCookieValue(ISS_SESSION_ID, E_Util.getSessionId());
	}
	/**
	 * ISS用セッションIDのCookie取得
	 */
	public static String getCookieIssSessionId(){
		return getCookieValue(ISS_SESSION_ID);
	}
	
	/**
	 * メニューコンポーネント用Cookie登録
	 */
	public static void setCookieMenuKind(String value){
		saveCookieValue(MENU_KIND, value);
	}
	/**
	 * メニューコンポーネント用Cookie取得
	 */
	public static String getCookieMenuKind(){
		return getCookieValue(MENU_KIND);
	}
	
	/**
	 * コンプライアンス経由用Cookie登録
	 */
	public static void setCookieCompliance(){
		saveCookieValue(COMPLIANCE_SESSION_ID, E_Util.getSessionId());
	}
	/**
	 * コンプライアンス経由Cookie取得
	 */
	public static String getCookieCompliance(){
		return getCookieValue(COMPLIANCE_SESSION_ID);
	}
	/**
	 * コンプライアンス経由Cookieクリア
	 */
	public static void clearCookieCompliance(){
		clearCookieValue(COMPLIANCE_SESSION_ID);
	}

	/**
	 * メニューコンポーネント用アクセス種別Cookie登録
	 */
	public static void setCookieAccessKind(String value){
		saveCookieValue(ACCESS_KIND, value);
	}
	/**
	 * メニューコンポーネント用アクセス種別Cookie取得
	 */
	public static String getCookieAccessKind(){
		return getCookieValue(ACCESS_KIND);
	}
	
	/**
	 * 遷移元画面URL保持用CookieキーName登録
	 */
	public static void setCookieReferer(String value){
		saveCookieValue(REFERER, value);
	}
	/**
	 * 遷移元画面URL保持用CookieキーName取得
	 */
	public static String getCookieReferer(){
		return getCookieValue(REFERER);
	}

	/**
	 * 遷移元画面URL契約内容個別照会保持用CookieキーName登録
	 */
	public static void setCookieRefererPolicy(String value){
		saveCookieValue(REFERER_POLICY, value);
	}
	/**
	 * 遷移元画面URL契約内容個別照会保持用CookieキーName取得
	 */
	public static String getCookieRefererPolicy(){
		return getCookieValue(REFERER_POLICY);
	}

/* スプリントバックログ 19対応　start  2017/1/20 */
/**
 * 遷移元画面URL契約内容個別照会保持用CookieキーName取得
 */
public static String getCookieRefererContact(){
	return getCookieValue(REFERER_CONTACT);
}
/**
 * 遷移元画面URL末端画面保持用CookieキーName取得
 */
//public static String getCookieRefererEnd(String key){
//	return getCookieValue(key);
//}
/* スプリントバックログ 19対応　end */

	/**
	 * 遷移元画面URLポートフォリオ保持用CookieキーName登録
	 */
	public static void setCookieRefererPortfolio(String value){
		saveCookieValue(REFERER_PORTFOLIO, value);
	}
	/**
	 * 遷移元画面URLポートフォリオ保持用CookieキーName取得
	 */
	public static String getCookieRefererPortfolio(){
		return getCookieValue(REFERER_PORTFOLIO);
	}
	
	/**
	 * 遷移元画面URL投資信託保持用CookieキーName登録
	 */
	public static void setCookieRefererInve(String value){
		saveCookieValue(REFERER_INVE, value);
	}
	/**
	 * 遷移元画面URL投資信託保持用CookieキーName取得
	 */
	public static String getCookieRefererInve(){
		return getCookieValue(REFERER_INVE);
	}
	
	/**
	 * 遷移元画面URL手続履歴保持用CookieキーName登録
	 */
	public static void setCookieRefererHistory(String value){
		saveCookieValue(REFERER_HISTORY, value);
	}
	/**
	 * 遷移元画面URL手続履歴保持用CookieキーName取得
	 */
	public static String getCookieRefererHistory(){
		return getCookieValue(REFERER_HISTORY);
	}
	
	/**
	 * 遷移元画面URL保持用(汎用)CookieキーName登録
	 */
	public static void setCookieGeneralReferer(String value){
		saveCookieValue(GENERAL_REFERER, value);
	}

/* スプリントバックログ 19対応　start  2017/1/20 */
/**
 * 遷移元画面URL保持用(汎用)CookieキーName登録
 */
public static void setCookieContactReferer(String value){
	saveCookieValue(REFERER_CONTACT, value);
}
/**
 * 遷移元画面URL保持用(末端ページ汎用)CookieキーName登録
 */
//public static void setCookieEndReferer(String key,String value){
//	saveCookieValue(key,value);
//}
/* スプリントバックログ 19対応　end */

	/**
	 * 遷移元画面URL保持用(汎用)CookieキーName取得
	 */
	public static String getCookieGeneralReferer(){
		return getCookieValue(GENERAL_REFERER);
	}

	/**
	 * 遷移元画面URL保持用(汎用)CookieキーName取得
	 */
	public static String getCookieGeneralReferer(String value){
		String refererStr = getCookieValue(GENERAL_REFERER);
		if (!String.isEmpty(refererStr)) {
			Map<String, String> refererMap = (Map<String, String>)Json.deserialize(refererStr, Map<String, String>.class);
			return refererMap.get(value);
		}
		return null;
	}

	/**
	 * 指定したキー、値をCookieに保存
	 * @param key: 保存するCookieのキー
	 * @param value: 保存するCookieの値
	 * @return ---
	 */
	private static void saveCookieValue( String key, String value){
		Cookie ck = new Cookie(key, value, null , -1, false);
		ApexPages.currentPage().setCookies(new Cookie[]{ck});
	}

	/**
	 * Cookieから指定したキーの値を返す
	 * @param key :取得したいCookieのキー
	 * @return String Cookieから取得した値
	 */
	private static String getCookieValue( String key ){
		Cookie ck = ApexPages.currentPage().getCookies().get(key);
		if(ck != null){
			return ck.getValue();
		}
		return '';
	}

	/**
	 * 指定したキーに紐付く値をクッキーからクリアする
	 * @param key: 削除したいクッキーのキー
	 */
	private static void clearCookieValue(String key){
		Cookie ck = new Cookie(key, null, null , -1, false);
		ApexPages.currentPage().setCookies(new Cookie[]{ck});
	}

// [IRIS]
	/**
	 * [IRIS]自動検索条件設定中フラグCookie登録
	 */
	public static void setCookieIsAutoSearch(String value){
		saveCookieValue(IS_AUTO_SEARCH, value);
	}
	/**
	 * [IRIS]自動検索条件設定中フラグCookie取得
	 */
	public static String getCookieIsAutoSearch(){
		return getCookieValue(IS_AUTO_SEARCH);
	}


	/**
	 * [IRIS]自動検索条件-代理店SFID Cookie登録
	 */
	public static void setCookieAutoSearchAgency(String value){
		saveCookieValue(AUTO_SEARCH_AGENCY, value);
	}
	/**
	 * [IRIS]自動検索条件-代理店SFID Cookie取得
	 */
	public static String getCookieAutoSearchAgency(){
		return getCookieValue(AUTO_SEARCH_AGENCY);
	}
	/**
	 * [IRIS]自動検索条件-代理店SFID Cookie削除
	 */
	public static void clearCookieAutoSearchAgency(){
		clearCookieValue(AUTO_SEARCH_AGENCY);
	}


	/**
	 * [IRIS]自動検索条件-事務所SFID Cookie登録
	 */
	public static void setCookieAutoSearchOffice(String value){
		saveCookieValue(AUTO_SEARCH_OFFICE, value);
	}
	/**
	 * [IRIS]自動検索条件-事務所SFID Cookie取得
	 */
	public static String getCookieAutoSearchOffice(){
		return getCookieValue(AUTO_SEARCH_OFFICE);
	}
	/**
	 * [IRIS]自動検索条件-事務所SFID Cookie削除
	 */
	public static void clearCookieAutoSearchOffice(){
		clearCookieValue(AUTO_SEARCH_OFFICE);
	}


	/**
	 * [IRIS]自動検索条件-募集人SFID Cookie登録
	 */
	public static void setCookieAutoSearchAgent(String value){
		saveCookieValue(AUTO_SEARCH_AGENT, value);
	}
	/**
	 * [IRIS]自動検索条件-募集人SFID Cookie取得
	 */
	public static String getCookieAutoSearchAgent(){
		return getCookieValue(AUTO_SEARCH_AGENT);
	}
	/**
	 * [IRIS]自動検索条件-募集人SFID Cookie削除
	 */
	public static void clearCookieAutoSearchAgent(){
		clearCookieValue(AUTO_SEARCH_AGENT);
	}


	public class CookieItem {
		private String key;

		// コンストラクタ
		public CookieItem(String key){
			this.key = key;
		}

		/**
		 * Cookieセット処理
		 * @param String:値
		 */
		public void setCookie(String value){
			Cookie cookie = new Cookie(this.key, value, null , -1, false);
			ApexPages.currentPage().setCookies(new Cookie[]{cookie});

			return;
		}

		/**
		 * Cookie削除処理
		 */
		public void deleteCookie(){
			Cookie cookie = new Cookie(this.key, null, null , 0, false);
			ApexPages.currentPage().setCookies(new Cookie[]{cookie});

			return;
		}

		/**
		 * Cookie名取得
		 */
		public String getCookieName(){
			return this.key;
		}

		/**
		 * Cookie値取得
		 */
		public String getCookieValue(){
			Cookie cookie = getCookie();

			if(cookie != null){
				return cookie.getValue();
			}

			return null;
		}

		/**
		 * Cookie存在判定
		 */
		public Boolean isSetCookie(){
			Cookie cookie = getCookie();
			return (cookie != null);
		}

		/**
		 * Cookie取得
		 */
		private Cookie getCookie(){
			return ApexPages.currentPage().getCookies().get(this.key);
		}
	}
}