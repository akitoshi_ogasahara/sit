/**
 * I_ContentSearchBoxDetailController
 *
 * I_ContentSearchResultの表示を制御するためのコントローラクラス
 */
public with sharing class I_ContentSearchBoxDetailController {

	/** 初期表示件数 */
	public static Integer DEFAULT_DISPLAY_ROW_COUNT = 5;

	/** 加算件数 */
	public static Integer ADDITIONAL_ROW_COUNT = 5;

	/** 制御するコンテンツのリスト */
	private List<I_ContentSearchResult> rows;

	/** 現在設定されているソート項目 */
	public String sortType {get; set;}

	/** 昇順表示であった場合にTRUE */
	public Boolean sortIsAsc {get; set;}

	/** 現在表示可能となっている件数の値 */
	public Integer rowCount {get; set;}

	/**
	 * コンストラクタ
	 */
	public I_ContentSearchBoxDetailController() {
		this.rowCount = DEFAULT_DISPLAY_ROW_COUNT;
	}

	/**
	 * 最大表示数の数値を取得する
	 * @return Integer
	 */
	public Integer getListMaxRows() {
		return I_Const.LIST_MAX_ROWS;
	}

	/**
	 * rowsを取得するためのゲッターメソッド
	 * @return List<I_ContentSearchResult>
	 */
	public List<I_ContentSearchResult> getContentRows() {
		return this.rows;
	}

	/**
	 * rowsを設定するためのセッターメソッド
	 * すでに保持しているリストと受け取ったリストを比較し、別インスタンスであった場合のみ
	 * 設定が行われる。
	 * 設定を行う際は各クラス変数の初期化を行う
	 * @param List<I_ContentSearchResult> rows
	 */
	public void setContentRows(List<I_ContentSearchResult> rows) {
		// 参照が変わっていた場合初期化を行う
		if (this.rows != rows) {
			this.rowCount = DEFAULT_DISPLAY_ROW_COUNT;
			this.sortType = I_ContentSearchResult.SORT_TYPE_PAGE_NAME;
			this.sortIsAsc = true;
			this.rows = rows;
			I_ContentSearchResult.setSortOption(this.sortType, this.sortIsAsc);
			this.rows.sort();
		}
	}

	/**
	 * ソートが行われたリストを返却する
	 * @return List<I_ContentSearchResult>
	 */
	public List<I_ContentSearchResult> getSortedRows() {
		return this.rows;
	}

	/**
	 * 現在の表示可能件数を増加させる。
	 * 一定以上の値になった場合は、最大値を設定する。
	 */
	public PageReference addRows() {
		this.rowCount += ADDITIONAL_ROW_COUNT;
		if(this.rowCount > I_Const.LIST_MAX_ROWS){
			this.rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * ソート
	 */
	public PageReference sortRows() {
		String sType = ApexPages.currentPage().getParameters().get('st');
		if (sType == this.sortType) {
			this.sortIsAsc = !this.sortIsAsc;
		} else {
			this.sortType = sType;
			this.sortIsAsc = true;
		}
		I_ContentSearchResult.setSortOption(this.sortType, this.sortIsAsc);
		this.rows.sort();

		return null;
	}
}