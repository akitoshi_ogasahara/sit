public with sharing class I_TopController extends I_AbstractController {
	//特集バナー用コンテンツ群
	public List<I_ContentMaster__c> bannerContentList{get;set;}

	public List<I_CMSComponentController> cmsComps{get;set;}
	// コンストラクタ
	public I_TopController() {}

	// MenuIdを初期化する。
	public override Id getIrisMenuId(){
		//ActiveMenuクリア
		iris.irisMenus.clearAcitveMenu();

		return null;
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = 'IRISトップ画面';

		//Filterモード解除
		if(iris.isSetPolicyCondition()){
			iris.clearPolicyCondition();
		}

		// 社員の場合は強制的に IRIS_Top を表示
		if(access.isEmployee() && iris.getCanUseIRIS()){
			// 利用アプリを IRIS に更新する
			access.switchEIDC_appMode(I_Const.APP_MODE_IRIS);
			return null;
		}

		//IRIS住生ユーザーの場合、住生トップへ遷移
		if(access.isSumiseiIRISUser()){
			access.switchEIDC_appMode(I_Const.APP_MODE_IRIS);
			return Page.IRIS_TopSumisei;
		}

		return pr;
	}

	public Map<String, I_MenuItem> getTopMenus(){
		return iris.irisMenus.MenuItemsByKey;
	}

	/*  ライブラリのサブナビ  */
	public I_MenuProvider.LibSubNavItemsHolder libSubNavItems{get{
																	if(libSubNavItems==null){
																		libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();
																	}
																	return libSubNavItems;
																}
																private set;
															}

	public Boolean getIsEmployee(){
		return access.isEmployee();
	}

/*
		IRIS Phase1 Day2の実装方式変更に伴い削除

	/*  応答月による抽出によるレコード件数
	 *      件数取得用クエリ    I_PolicysControllerのメソッドを利用する
	 *
	 *
	public String getRECORD_SIZE_SOQL_FOR_RESTAPI_from1(){
		return I_PolicysController.getRECORD_SIZE_SOQL_FOR_RESTAPI(1);
	}
	public String getRECORD_SIZE_SOQL_FOR_RESTAPI_from4(){
		return I_PolicysController.getRECORD_SIZE_SOQL_FOR_RESTAPI(4);
	}

	/*  年月(数式)による抽出によるレコード件数
	 *      件数取得用クエリ    I_NoticeTopControllerのメソッドを利用する
	 *
	 *
	public String getRECORD_SIZE_SOQL_FOR_RESTAPI_NCOL(){
		return I_NoticeTopController.getRECORD_SIZE_SOQL_FOR_RESTAPI(E_DownloadNoticeConst.OBJ_API_E_NCOL);
	}

	public String getRECORD_SIZE_SOQL_FOR_RESTAPI_BILA(){
		return I_NoticeTopController.getRECORD_SIZE_SOQL_FOR_RESTAPI(E_DownloadNoticeConst.OBJ_API_E_BILA);
	}

	public String getRECORD_SIZE_SOQL_FOR_RESTAPI_BILS(){
		return I_NoticeTopController.getRECORD_SIZE_SOQL_FOR_RESTAPI(E_DownloadNoticeConst.OBJ_API_E_BILS);
	}
*/


	public List<I_CMSComponentController> getBannerContents(){

		this.bannerContentList = I_ContentMasterDao.getBannerContents();
		Set<Id> pageCompIds = new Set<Id>();

		for(I_ContentMaster__c secRec: this.bannerContentList){
			pageCompIds.add(secRec.Id);
		}

		// コンポーネントの添付ファイルを取得
		Map<ID, List<Attachment>> files = E_AttachmentDao.getRecsByParentIds(pageCompIds);

		//System.debug(files);

		// ChatterFileのIdを取得
		Map<Id,Id> chatterFileIds = I_ContentMasterDao.getChatterFileIdsByRecId(pageCompIds);

		this.cmsComps = new List<I_CMSComponentController>();

		for (I_ContentMaster__c cmpRec: this.bannerContentList) {
			this.cmsComps.add(new I_CMSComponentController(cmpRec, files.get(cmpRec.Id), chatterFileIds.get(cmpRec.Id)));
		}

		return this.cmsComps;
	}

	/**
		Method for "ファイナンシャルアライアンス" and "ソニックジャパン" agency for the current user
		@author PwC
		@date 2017-11-16
		@param なし
		@return FAまたはSJへの所属有無
	*/
	public Boolean getSJandFA(){
		Boolean isSJandFA = FALSE;
		User newuserinfo = [Select id, ContactID, name from User where id=:UserInfo.getUserId()];
		if(newuserinfo.ContactID != null){
			Contact contactPartnerData = [Select Account.E_ParentZHEADAY__c, id from Contact where id=:newuserinfo.ContactID];
			if(contactPartnerData != null ){
				String contactagencyCode = String.valueOf(contactPartnerData.Account.E_ParentZHEADAY__c);
				if(contactagencyCode != null){
					if(contactagencyCode.equals(System.Label.LMT_SonicJapan_AgencyCode_for_Apex)
					|| contactagencyCode.equals(System.Label.LMT_FA_AgencyCode_for_Apex)){
						isSJandFA = TRUE;
					}
				}
			}
		}
		return isSJandFA ;
	}

	/**
		Reference to Lead Record from IRIS page
		@author PwC
		@date 2017-11-16
		@param なし
		@return IRIS URL
	*/
	public PageReference getLeadPage(){
		Schema.DescribeSObjectResult LeadSObjectSchema = Lead.SObjectType.getDescribe();
		String objectIdPrefix = LeadSObjectSchema.getKeyPrefix();
		PageReference retURL = new PageReference(Site.getBaseUrl()+ '/'+ objectIdPrefix);
		retURL.setRedirect(true);
		return retURL;
	}
}