public with sharing class CR_AgencySearchOSWrapController extends CR_AbstractExtender{
    /**
     * Constractor
     */
    public CR_AgencySearchOSWrapController() {
        pgTitle = '代理店検索（外部委託）';
    }
    
    /**
     * 代理店検索（外部委託）一覧へのページ遷移判定
     */
    public pageReference pageAction(){
        String url = getUrlforOutsrcs();
        if(String.isNotBlank(url)){
            return new PageReference(url);
        } 
        return null;
    }
}