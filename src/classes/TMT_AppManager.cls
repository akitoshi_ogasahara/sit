/**
 *    たもつ君アプリケーション　管理クラス
 *              withoutでないとUser.LastPasswordChangeDateがInvalid Fieldになる。
 */
public without sharing class TMT_AppManager {
    public class AppManagerException extends Exception{}
    public static final String APPNAME = 'たもつくん';

    @TestVisible
    private E_IDCPF__c eidc;
    private UserLogin usrLogin;
    public TMT_Config__c config{get;private set;}
    

    /*  Constructor */
    public TMT_AppManager(Id uid){                              //useridを受け取ることとする　　E_IDCPFDaoWithout.getRecsByUserID(ID userID)
        this.eidc = E_IDCPFDaoWithout.getRecsByUserID(uid);     //EIDCとセットでUserを取得
        if(eidc==null){
            throw new AppManagerException('ID管理の取得に失敗しました。 userId:' + uid);
        }
        this.usrLogin = E_UserLoginDaoWithout.getRecordByUserId(uid);
        this.config = TMT_Config__c.getInstance();
    }

    // iOSからのアクセス時のUUIDを取得する
    private static final String PREFIX_UUID = 'uid_';
    public String getDeviceIdFromUserAgent(String[] userAgentStrings){
        for(String s:userAgentStrings){
            if(s.startsWith(PREFIX_UUID)){
                return s.substring(PREFIX_UUID.length());
            }
        }
        return String.join(userAgentStrings, ' ');    //uuidが取得できない場合、userAgentを利用する
    }

    // UUIDをEIDCに登録する
    public void putDeviceID(){
        //DeviceID未指定の場合は、HttpHeaderからUserAgent取得して登録する。
        putDeviceID(getDeviceIdFromUserAgent(E_Util.getUserAgentStrings()));
    }
    
    //利用可能なUIDとログイン日時を格納しておく（ログインしなおすと日付が更新される）
    public void putDeviceID(String uuid){
        if(String.isNotBlank(this.eidc.uuid1__c) && this.eidc.uuid1__c.startsWith(uuid)){
            this.eidc.uuid1__c = getFormattedUUIDString(uuid);
        }else if(String.isNotBlank(this.eidc.uuid2__c) && this.eidc.uuid2__c.startsWith(uuid)){
            this.eidc.uuid2__c = getFormattedUUIDString(uuid);
        }else{
            if(String.isBlank(this.eidc.uuid1__c)){
                this.eidc.uuid1__c = getFormattedUUIDString(uuid);
            }else if(String.isBlank(this.eidc.uuid2__c)){
                this.eidc.uuid2__c = getFormattedUUIDString(uuid);
            }else{
                this.eidc.uuid1__c = this.eidc.uuid2__c;
                this.eidc.uuid2__c = getFormattedUUIDString(uuid);
            }
        }   
        //EIDCの更新　トリガ処理はSkipする。
        E_IDCPFTriggerHandler.isSkipTriggerActions = true;
        E_IDCPFDaoWithout.updateRec(this.eidc);      //without利用でのUpdate
        E_IDCPFTriggerHandler.isSkipTriggerActions = false;
    }
    
    // EIDCに登録済のUUIDかどうかの判定
    public Boolean isApprovedDevice(String uuid){
        return  (String.isNotBlank(this.eidc.uuid1__c) && this.eidc.uuid1__c.startsWith(uuid)) 
                || (String.isNotBlank(this.eidc.uuid2__c) && this.eidc.uuid2__c.startsWith(uuid));
    }
    
    // 社員ユーザ判定
    public Boolean isEmployee(){
        return eidc.User__r.UserType.equals(E_Const.USERTYPE_STANDARD);     //社員はOK
    }

    // パスワード変更日付のチェック
    //  登録された日付以降にPWが変更されている場合にTrueを返す。
    public Boolean requiredPassWord(String uuid){
        return this.eidc.user__r.LastPasswordChangeDate ==null
                || this.eidc.user__r.LastPasswordChangeDate > getRegistrationTimeStamp(uuid);
    }

    // Versionチェック
    //  最新Versionがある場合にTrue
    public Boolean requiredUpdateApp(String nowVersion){
        return this.config.lastVersion__c > nowVersion;
    }
    
    //  最新MR Versionがある場合にTrue
    public Boolean requiredUpdateMRApp(String nowVersion){
        return this.config.lastMrVersion__c > nowVersion;
    }

    // アプリケーションステータスの確認
    public Boolean isActive(){
        return !config.isDisabled__c;      //無効を反転させる
    }
    
    // 利用ユーザが有効かどうか
    //      EIDCのPWステータスは判定に含めない　User側のステータスと同期がとられているはずなので
    public Boolean isActiveUser(){
        return !usrLogin.isFrozen && !usrLogin.isPasswordLocked
                && eidc!=null && eidc.user__r != null && this.eidc.user__r.isActive;        //ロック中の場合はFalseを返す
    }

    //　ログ保存
    private static String USER_AGENT_LOG_FORMAT = ' {0}:{1}';
    
    //　ログ保存 たもつくんVersion2.5以前
    public void saveRequestLog(TMT_RestResource.appStatusRequest apiRequest, TMT_RestResource.appStatusResponse apiResponse){
        try{
            E_Log__c log = E_LogUtil.createLog(APPNAME);
            log.actionType__c = 'ページ';
            //たもつ君ページを設定
            log.AccessPage__c = E_Util.null2Blank(apiRequest.actionType);
            log.Name = log.AccessPage__c;
            // アクセス日時
            log.AccessDatetime__c = Datetime.now();
            // User－Agent
            String usrAgent = String.format(USER_AGENT_LOG_FORMAT, new List<String>{'uuid', apiRequest.uuid});
            usrAgent += String.format(USER_AGENT_LOG_FORMAT, new List<String>{'version', apiRequest.version});
            for(String s: apiRequest.deviceInfo.keyset()){
                usrAgent += String.format(USER_AGENT_LOG_FORMAT, new List<String>{s, apiRequest.deviceInfo.get(s)});
            }
            log.User_Agent__c = usrAgent;
            // 詳細
            if(apiResponse==null){
                //成功時
                log.Detail__c = E_Util.null2Blank(apiRequest.simulation);
            }else{
                //エラーあり
                log.IsError__c = true;
                log.Detail__c = JSON.serialize(apiResponse);
            }
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.allowFieldTruncation = true;
            log.setOptions(dmo);
            
            insert log;  //DMLOption allowTruncate
        }catch(Exception e){
            //ログ出力エラーはデバッグ出力して、処理を継続させる。
            System.debug('ログ出力エラー:' + e.getMessage() + '/' + e.getStackTraceString());
        }
    }
    
    // たもつくんVersion2.6以降
    public void saveRequestLogV2(
            TMT_RestResource.appStatusRequest apiRequest,
            TMT_RestResource.appStatusResponse apiResponse,
            Boolean  logFlag,
            Boolean  showFlag
        ){
        try{
            E_Log__c log = E_LogUtil.createLog(APPNAME);
            log.actionType__c = 'たもつくん';
            
            //たもつ君ページを設定
            log.AccessPage__c = E_Util.null2Blank(apiRequest.actionType);
            log.Name = log.AccessPage__c;
            // アクセス日時
            log.AccessDatetime__c = Datetime.now();
            // User－Agent
            String usrAgent = String.format(USER_AGENT_LOG_FORMAT, new List<String>{'uuid', apiRequest.uuid});
            usrAgent += String.format(USER_AGENT_LOG_FORMAT, new List<String>{'version', apiRequest.version});
            for(String s: apiRequest.deviceInfo.keyset()){
                usrAgent += String.format(USER_AGENT_LOG_FORMAT, new List<String>{s, apiRequest.deviceInfo.get(s)});
            }
            log.User_Agent__c = usrAgent;
            // 詳細
            if(apiResponse==null){
 
                //成功時
                log.Detail__c = E_Util.null2Blank(apiRequest.simulation);
                
                if (showFlag){
                    // 一覧表示名を表示する
                    log.SummaryName__c = E_Util.null2Blank(apiRequest.simulation);
                }
            }else{
                //エラーあり
                log.IsError__c = true;
                log.Detail__c = JSON.serialize(apiResponse);
            }
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.allowFieldTruncation = true;
            log.setOptions(dmo);
            
            if (logFlag == true){
                insert log;  //DMLOption allowTruncate
            }
        }catch(Exception e){
            //ログ出力エラーはデバッグ出力して、処理を継続させる。
            System.debug('ログ出力エラー:' + e.getMessage() + '/' + e.getStackTraceString());
        }
    }
    
    // EIDCに格納するUUID文字列を設定する  GMTによるTimeStampも付加
    private static String getFormattedUUIDString(String uuid){
        return getFormattedUUIDString(uuid, System.Now());
    }
    @TestVisible
    private static String getFormattedUUIDString(String uuid, Datetime dt){
        return uuid.left(230) + '|' + dt.formatGmt('yyyy-MM-dd HH:mm:ss');//System.Now().format('yyyy/MM/dd HH:mm:ss','Asia/Tokyo');      //255文字に収まる用　uuid部分は230文字で切り捨て
    }
    
    // EIDCに格納されたUUID文字列から登録された日時を取り出す。
    private Datetime getRegistrationTimeStamp(String uuid){
        String registration = null;
        if(String.isNotBlank(this.eidc.uuid1__c) && this.eidc.uuid1__c.startsWith(uuid)){
            registration = this.eidc.uuid1__c;
        }else if(String.isNotBlank(this.eidc.uuid2__c) && this.eidc.uuid2__c.startsWith(uuid)){
            registration = this.eidc.uuid2__c;
        }
        
        if(String.isNotBlank(registration)){
            String ts = registration.substring(uuid.length()+1);        // '|'分 +1 する。
            return Datetime.valueOfGMT(ts);  //GMTで設定されている文字列をDatatimeに戻す。                     
        }
        return null;
    }
}