/**
@version 1.0
@author PwC
リード所有者更新時割り当て通知コンポーネントコントローラ
*/
public without sharing class LMT_LeadAssignmentComponentCtrl {
    public ID salesRepID {get; set;}
    public List<Lead> InactiveLeads = new List<Lead>();

    public List<Lead> getInactiveLeads() {
        InactiveLeads = [SELECT
    						Company,
    						LatestCampMemUrl__c
    						FROM Lead
    						WHERE  ownerId =: salesRepID
        and LastModifiedDate  >= :Datetime.now().addSeconds(-10)
        ];
        return InactiveLeads;
    }
}