/**
 * 募集人一覧ダウンロードクラス
 */
public  with sharing class I_ExportAuthorityAgentController extends  E_CSVExportController {
  //必要な情報が格納されているElogのId
  public String  elogId{get; private set;}
  private String accId{get; set;}


 /*
 ** コンストラクタ
 */
  public I_ExportAuthorityAgentController(){
    super();
    //URLパラメータ取得
    elogId = ApexPages.currentPage().getParameters().get('id');
    //共同GW経由の時は代理店IDのみでcsvを作成する
    accId = ApexPages.currentPage().getParameters().get('accid');
  }

  public  override String mainSobjectAPIName(){
    return  null;
  }

  /*
  ** ファイル名
  */
   public override String getFileName(){
      String sdt = System.Now().format('yyyyMMddHHmmss',UserInfo.getTimeZone().getID());
      return 'IRIS権限管理_募集人一覧_' + sdt +'.csv';
   }

  /**
   * ヘッダ― Changed by Michael Dorrian As Of 2018/02/27 Atria手数料試算 to 設計書作成手数料試算
   */
  public override String getCSVHeader(){
    List<String> header = new List<String>();
    // [IRIS-734] start
    /*
    header.add('募集人名');
    header.add('募集人コード');
    header.add('ID状況');
    header.add('権限種別');
    header.add('ID');
    header.add('メールアドレス');
    header.add('事務所名');
    header.add('設計書作成手数料試算');
    */
    header.add('募集人名');
    header.add('募集人コード');
    header.add('ID状況');
    header.add('ID');
    header.add('メールアドレス');
    header.add('メール配信');
    header.add('権限種別');
    header.add('手数料参照');
    header.add('設計書手数料試算');
    header.add('最終ログイン日時');
    header.add('所属事務所名');
    // [IRIS-734] end

    return String.join(header, CSV_SEPARATOR()) + CSV_BREAKELINE();

  }

  /**
  * データ行作成
  */
  public  List<I_AuthorityAgentRowManager.AgentListRow> getLines(){
    try{
      I_AuthorityAgentRowManager manager = new I_AuthorityAgentRowManager();
      String searchText = '';
      Boolean isAllSearch = true;
      String branchCode = '';
      List<String> Ids = new List<String>();

      if( accId != null ){
        //accId有＝共同GW経由でcsvを作成する場合は、代理店IDのみでリストを取得する
        List<Account> offices = E_AccountDaoWithout.getRecsByAgency_Atria( accId , '', '', true);
        for( Account ac : offices ){
          Ids.add( String.valueOf (ac.Id) );
        }

        I_AuthorityAgentRowManager.agentList_sortType = I_AuthorityAgentController.SORT_TYPE_AGENT;
        I_AuthorityAgentRowManager.agentList_sortIsAsc = true;

      } else {
        //データ取得
        E_log__c log = [Select Id, Detail__c from E_log__c where Id = :elogId ];
        Map<String, Object> logMap = (Map<String, Object>)JSON.deserializeUntyped( log.detail__c );
        searchText = (String)logMap.get('SearchText');//検索ワード
        isAllSearch = (Boolean)logMap.get('isAllSearch');//全件検索フラグ
        branchCode = (String)logMap.get('branchCode');//支社コード
        accId = (String)logMap.get('accId');//代理店格Id

        List<Object> officeListIds = (List<Object>) logMap.get( 'SelectOffice' );
        for (Object officeId : officeListIds ){
          Ids.add( String.valueOf (officeId) );
        }
        I_AuthorityAgentRowManager.agentList_sortType = (String)logMap.get('SortType');//ソート項目
        I_AuthorityAgentRowManager.agentList_sortIsAsc  = (Boolean)logMap.get('SortIsAsc');//ソート方向
      }
      
      return manager.createAgentRows( isAllSearch, accId, branchCode ,Ids, searchText, true );

    } catch(Exception e){
      return null;
    }

  }

}