public with sharing class E_DefectMasterDao {
	/**
	 * @return List<E_CHTPF__c>
	 */
	public static List<E_DefectMaster__c> getRecs(){
		return [Select Code__c,Value__c,SortKey__c From E_DefectMaster__c];
	}
}