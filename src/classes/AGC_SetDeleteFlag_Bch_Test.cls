@isTest(SeeAllData=false)
private class AGC_SetDeleteFlag_Bch_Test {

	private static testMethod void test(){
		
		AGC_TestCommon common = new AGC_TestCommon();
		common.getCustomSetting();
		
		AGC_ShishaMr_Mst__c tanto = common.createTantoMst();
		AGC_Shisha_Mst__c sisha = common.createShishaMst();
		insert tanto;
		insert sisha;
		
		User u = common.createApiUser();
		List<AGC_DairitenKihonJoho__c> insertList = new List<AGC_DairitenKihonJoho__c>();
		Integer dairitenSize = 200;
		
		System.runAs(u){
			for(Integer i=0; i<dairitenSize; i++){
				AGC_DairitenKihonJoho__c kihon = common.createKihonInfo(sisha.Id, tanto.Id);
				kihon.BranchId__c = i+'';
				insertList.add(kihon);
			}
			insert insertList;
		}
		
		List<AGC_DairitenKihonJoho__c> updateList = [Select id, DataInputDate__c from AGC_DairitenKihonJoho__c];
		
		System.assertEquals(Date.today(), updateList[0].DataInputDate__c);
		
		for(AGC_DairitenKihonJoho__c k : updateList){
			k.DataInputDate__c = Date.today().addDays(-3);
		}
		update updateList;
		
		System.assertEquals(Date.today().addDays(-3), [Select id, DataInputDate__c from AGC_DairitenKihonJoho__c limit 1].DataInputDate__c);
		
		
		Test.startTest();
		
		System.assertEquals(0, [Select id from AGC_DairitenKihonJoho__c where DelRecord__c = true].size());
		
		System.runAs(u){
			Database.executeBatch(new AGC_SetDeleteFlag_Bch());
		}
		
		Test.stopTest();
		
		System.assertEquals(dairitenSize, [Select id from AGC_DairitenKihonJoho__c where DelRecord__c = true].size());
		
		
		
	}
	
	private static testMethod void testSched(){
		Test.startTest();
		String jobId = System.schedule('testSched', '0 0 0 3 9 ? 2022', new AGC_SetDeleteFlag_Sched());
		Test.stopTest();
	}
}