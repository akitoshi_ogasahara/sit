@isTest
private class TestE_InveCompareExtender {
	
	//グラフコンポーネントの引数
	//buy
	static testMethod void getExecuteKindBuyTest001(){
		
		E_InveCompareExtender extender = createExtender();
		
		Test.startTest();
		String result = extender.getExecuteKindBuy();
		Test.stopTest();
		
		System.assertEquals(E_Const.GRAPH_INVESTMENT_BUY, result);
	}
	
	//グラフコンポーネントの引数
	//now
	static testMethod void getExecuteKindNowTest001(){
		
		E_InveCompareExtender extender = createExtender();
		
		Test.startTest();
		String result = extender.getExecuteKindNow();
		Test.stopTest();
		
		System.assertEquals(E_Const.GRAPH_INVESTMENT_NOW, result);
	}
	
	//コンストラクタ
	static testMethod void constructorTest001(){
		
		E_ITHPF__c record = new E_ITHPF__c();
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveCompareController extension = new E_InveCompareController(controller);
		
		Test.startTest();
		E_InveCompareExtender extender = new E_InveCompareExtender(extension);
		Test.stopTest();
		
	}
	
	//init
	static testMethod void initTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveCompareExtender extender = createExtender();
			
			Test.startTest();
			extender.init();
			Test.stopTest();
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveCompareExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			system.assertNotEquals(null, result);
		}
	}
	
	//ページアクション
	//doAuth失敗
	static testMethod void pageActionTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveCompareExtender extender = createExtender();
			extender.init();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			system.assertNotEquals(null, result);
		}
	}
	
	//テストユーザ作成
	//プロファイルを投信ヘッダの権限があるものに設定
	static User createUser(String flag03){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG03__c = flag03;
		insert idcpf;
		return u;
	}
	
	//Extender作成
	static E_InveCompareExtender createExtender(){
		//レコード作成
		E_ITHPF__c record = new E_ITHPF__c();
		insert record;
		
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveCompareController extension = new E_InveCompareController(controller);
		E_InveCompareExtender extender = new E_InveCompareExtender(extension);
		return extender;
	}
}