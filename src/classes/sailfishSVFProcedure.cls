public class sailfishSVFProcedure{
	public String execute(String targetId){
		String location;
        String myURL;
        String myCRT;
       //ara make SVF procedure URL confiurable by custom meta data type - start
        SVFProcedure__mdt[] myMdt =[SELECT SVFProcedureURL__c FROM SVFProcedure__mdt WHERE masterlabel ='SailfishURL'];
        if(!myMdt.isEmpty()){
            myURL = myMdt[0].SVFProcedureURL__c;
          }
          SVFProcedure__mdt[] myMdt2 =[SELECT SVFProcedureURL__c FROM SVFProcedure__mdt WHERE masterlabel ='CRT'];
        if(!myMdt.isEmpty()){
            myCRT = myMdt2[0].SVFProcedureURL__c;
          }
        
        System.debug('myURL form custom meta data'+ myURL);
        //ara make SVF procedure URL confiurable by custom meta data type - end
 		HttpRequest req = new HttpRequest();
		Http http = new Http();
		HttpResponse res = new HttpResponse();
		// プロシージャーの実行リクエスト
		req.setMethod('POST');
		req.setClientCertificateName(myCRT);
		// Salesforceユーザーを指定します。ここで指定したユーザー権限でSalesforceからデータを取得します
		req.setHeader('X-SVFCloud-Subject', UserInfo.getUserName());
		// タイムゾーンを指定します
		req.setHeader('X-SVFCloud-TimeZone', UserInfo.getTimeZone().toString());
		req.setEndpoint(myURL);
		// オブジェクトIdを指定します
		String body =('id='+targetId);
		req.setBody(body);
		req.setHeader('Content-type', 'application/x-www-form-urlencoded');
		req.setHeader('Content-Length',String.ValueOf(body.length()));
		req.setCompressed(false);
		req.setTimeout(12000);
		try {
			res = http.send(req);
			System.debug(res.getStatusCode());
			if(!String.isEmpty(res.getStatus()) && res.getStatusCode()==307){
				// 転送先へ転送します
				String endpoint = res.getHeader('Location');
				req.setEndpoint(endpoint);
				res = http.send(req);
                System.debug(res.getStatusCode());
			}
			// Location ヘッダーに成果物のパスが返ってきます
			location = res.getHeader('Location');
			return location;
		}catch(System.CalloutException e) {
			System.debug('Callout error: '+ e);
			System.debug(res.toString());
			return null;
		}
	}
}