@isTest
private class TestI_ChangePWController {

	@isTest //getIsIrisPageメソッドのテスト
	static void getIsIrisPageTest() {
		PageReference pageRef = Page.IRIS_ChangePW;
		pageRef.getParameters().put('after', '1');
		Test.setCurrentPage(pageRef);

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		Boolean access = cpc.getIsIrisPage();
		System.assertEquals(access,false);
		Test.stopTest();
	}

	@isTest //goHomeメソッドのテスト
	static void goHomeTest() {
		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		PageReference pageref = cpc.goHome();
		System.assertEquals(pageref.getUrl(),Page.E_Home.getUrl());
		Test.stopTest();
	}

	@isTest //pageActionのテスト
	static void pageActionTest1() {
		createIDManage();

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		PageReference ref = cpc.pageAction();
		//System.assertEquals(ref.getUrl(),Page.IRIS_ChangePW.getUrl() + '?after=0');
		System.assertEquals(ref,null);
		Test.stopTest();
	}

	@isTest //pageActionのテスト（IRISユーザでない場合）
	static void pageActionTest2() {
		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		PageReference ref = cpc.pageAction();
		System.assertEquals(ref,null);
		Test.stopTest();
	}


	@isTest //doSaveのテスト--validate新規パスワード入力エラー
	static void pagePasswordEmptyTest() {
		createMessageData('SPW|101','新規パスワードを入力して下さい。');
		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		PageReference pageref = cpc.doSave();
		System.assertEquals(cpc.pageMessages.getErrorMessages()[0].summary,'新規パスワードを入力して下さい。');
		Test.stopTest();
	}

	@isTest //doSaveのテスト--validate新規確認用パスワード入力エラー
	static void pagePasswordConfirmEmptyTest() {
		createMessageData('SPW|102','新規パスワード（確認）を入力して下さい。');

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Test1130';
		PageReference pageref = cpc.doSave();
		System.assertEquals(cpc.pageMessages.getErrorMessages()[0].summary,'新規パスワード（確認）を入力して下さい。');
		Test.stopTest();
	}

	@isTest //doSaveのテスト--validateパスワード入力チェックエラー
	static void pagePassworErrorTest() {
		createMessageData('SPW|010','入力規則に従ってパスワードを変更して下さい。');

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = '@@@@@@@@';
		cpc.newConfirmPassword = '@@@@@@@@';
		PageReference pageref = cpc.doSave();
		System.assertEquals(cpc.pageMessages.getErrorMessages()[0].summary,'入力規則に従ってパスワードを変更して下さい。');
		Test.stopTest();
	}

	@isTest //doSaveのテスト--validateパスワード非整合エラー
	static void pagePasswordNotEqualTest() {
		createMessageData('SPW|109','新パスワードと新パスワード(確認）が相違しています。');

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Test1130';
		cpc.newConfirmPassword = 'Test1131';
		PageReference pageref = cpc.doSave();
		System.assertEquals(cpc.pageMessages.getErrorMessages()[0].summary,'新パスワードと新パスワード(確認）が相違しています。');
		Test.stopTest();
	}

	@isTest //doSaveのテスト--正規系(仮基本パスフラグtrue)
	static void doSaveCompBasicTest() {
		User us = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		us.E_UseTempBasicPW__c = true;
		update us;

		Test.startTest();
		PageReference ref;
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Psaa1130';
		cpc.newConfirmPassword = 'Psaa1130';
		System.runAs(us){
			ref = cpc.doSave();
		}
		System.assertEquals(ref.getUrl(),Page.IRIS_ChangePW.getUrl() + '?after=1');
		Test.stopTest();
	}

	@isTest //doSaveのテスト--正規系(既存基本パスフラグtrue)
	static void doSaveCompExistTest() {
		User us = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		us.E_UseExistingBasicPW__c = true;
		update us;

		Test.startTest();
		PageReference ref;
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Psaa1130';
		cpc.newConfirmPassword = 'Psaa1130';
		System.runAs(us){
			ref = cpc.doSave();
		}
		System.assertEquals(ref.getUrl(),Page.IRIS_ChangePW.getUrl() + '?after=1');
		Test.stopTest();
	}

	@isTest //doSaveのテスト--パスワードセットエラー
	static void setPasswordErrorTest() {
		User us = createUser();
		PageReference pageref;

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Test1130';
		cpc.newConfirmPassword = 'Test1130';
		System.runAs(us){
			pageref = cpc.doSave();
		}
		System.assertEquals(pageref,null);
		Test.stopTest();
	}

	@isTest //doSaveのテスト--apexpage
	static void apexPageTest1() {
		PageReference pageRef = Page.IRIS_ChangePW;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'テスト');
		ApexPages.addMessage(msg);
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Psaa1130';
		cpc.newConfirmPassword = 'Psaa1130';
		PageReference ref = cpc.doSave();
		System.assertEquals(ref,null);
		Test.stopTest();
	}

	@isTest //doSaveのテスト--apexpage(パスワード)
	static void apexPageTest2() {
		PageReference pageRef = Page.IRIS_ChangePW;
		Test.setCurrentPage(pageRef);

		Test.startTest();
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'invalid repeated password');
		ApexPages.addMessage(msg);
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Psaa1130';
		cpc.newConfirmPassword = 'Psaa1130';
		PageReference ref = cpc.doSave();
		System.assertEquals(ref,null);
		Test.stopTest();
	}


	@isTest //キャンセル
	static void returnTest() {
		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		PageReference pageref = cpc.doReturn();
		System.assertEquals(pageref.getUrl(),Page.IRIS_UserConfig.getUrl());
		Test.stopTest();
	}

	@isTest //パスワードポリシーテスト
	static void passwordPolicyTest(){
		createMessageData('SPW|010','半角でアルファベットの大文字、小文字、数字の3種類すべてを組み合わせて設定してください。');

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'test1130';
		cpc.newConfirmPassword = 'test1130';
		PageReference pageref = cpc.doSave();
		System.assertEquals(cpc.pageMessages.getErrorMessages()[0].summary,'半角でアルファベットの大文字、小文字、数字の3種類すべてを組み合わせて設定してください。');
		Test.stopTest();
	}

	@isTest //パスワード文字数テスト
	static void passwordLengthTest(){
		createMessageData('SPW|110','8文字以上のパスワードを設定してください。');

		Test.startTest();
		I_ChangePWController cpc = new I_ChangePWController();
		cpc.newPassword = 'Test113';
		cpc.newConfirmPassword = 'Test113';
		PageReference pageref = cpc.doSave();
		System.assertEquals(cpc.pageMessages.getErrorMessages()[1].summary,'8文字以上のパスワードを設定してください。');
		Test.stopTest();
	}

	//テストデータ作成
	//メッセージマスタ
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//ユーザ
	static User createUser(){
		User us = new User();
		Contact con = createContactData();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.ContactId = con.Id;
		us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
		us.IsActive = true;
		insert us;
		return us;
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
			Id accid = createAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
			return con;
	}

	//ID管理
	static void createIDManage(){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.appMode__c = I_Const.APP_MODE_IRIS;
		ic.User__c = UserInfo.getUserId();
		insert ic;
	}
}