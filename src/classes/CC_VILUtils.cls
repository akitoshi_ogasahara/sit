/**
 * クラス名 : CC_VILUtils
 * 概要 : Utility clss for Interaction Launcher
 *
 */

public with sharing class CC_VILUtils{

	/**
	 * 概要 : 入力された生年月日のフォーマット処理
	 */
	public static String birthdateProcess(String birthdate){

		//年月日の取得
		birthdate = birthdate.toUpperCase();
		String strYear = '';
		String strMonth = '';
		String strDay = '';
		if(birthdate.contains('/')){
			if(birthdate.countMatches('/') == 2){
				strYear = birthdate.substringBefore('/');
				strMonth = birthdate.substringBetween('/', '/');
				strDay = birthdate.substringAfterLast('/');
			}else{
				strMonth = birthdate.substringBefore('/');
				strDay = birthdate.substringAfterLast('/');
			}
		}else if(!birthdate.contains('/')){
			if(!birthdate.isNumeric() && birthdate.length() == 8){
				strYear = birthdate.left(4);
				strMonth = (birthdate.mid(4, 2));
				strDay = birthdate.right(2);
			}
		}

		//和暦の年の処理
		if(!strYear.equals('') && !strYear.isNumeric()){
			//明治
			if(strYear.startsWith('M')){
				strYear = strYear.remove('M');
				if(strYear.isNumeric()){
					Integer intYear = Integer.valueOf(strYear);
					if(intYear >= 1 && intYear <= 45){
						intYear = intYear + 1867;
						strYear = String.valueOf(intYear);
					}
				}
			}
			//大正
			else if(strYear.startsWith('T')){
				strYear = strYear.remove('T');
				if(strYear.isNumeric()){
					Integer intYear = Integer.valueOf(strYear);
					if(intYear >= 1 && intYear <= 15){
						intYear = intYear + 1911;
						strYear = String.valueOf(intYear);
					}
				}
			}
			//昭和
			else if(strYear.startsWith('S')){
				strYear = strYear.remove('S');
				if(strYear.isNumeric()){
					Integer intYear = Integer.valueOf(strYear);
					if(intYear >= 1 && intYear <= 64){
						intYear = intYear + 1925;
						strYear = String.valueOf(intYear);
					}
				}
			}
			//平成
			else if(strYear.startsWith('H')){
				strYear = strYear.remove('H');
				if(strYear.isNumeric()){
					Integer intYear = Integer.valueOf(strYear);
					if(intYear >= 1 && intYear <= 31){
						intYear = intYear + 1988;
						strYear = String.valueOf(intYear);
					}
				}
			}
		}

		//フォーマット処理
		if(!strDay.equals('') && !strMonth.equals('')){
			birthdate = strYear;
			if(strMonth.length() == 1){
				birthdate = birthdate + '0' + strMonth;
			}else{
				birthdate = birthdate + strMonth;
			}
			if(strDay.length() == 1){
				birthdate = birthdate + '0' + strDay;
			}else{
				birthdate = birthdate + strDay;
			}
		}

		return birthdate;
	}

	/**
	 * 概要 : 文字列を清音に変換
	 */
	public static String getSeionKana(String strTarget){
		String strResult = '';

		strResult = strTarget.deleteWhitespace();
		strResult = getSeionKanaForZenkaku(strResult);
		strResult = getSeionKanaForHankaku(strResult);

		return strResult;
	}

	private static String getSeionKanaForZenkaku(String strTarget){
		String strResult = '';
		String strConvertSrc = 'ァィゥヴェォガギグゲゴザジズゼゾダヂッヅデドバビブベボパピプペポャュョヮヷヺ';
		String strConvertDst = 'アイウウエオカキクケコサシスセソタチツツテトハヒフヘホハヒフヘホヤユヨワワヲ';

		strTarget = strTarget.replaceAll('[ー]','');
		strResult = conversionProcess(strTarget, strConvertSrc, strConvertDst);

		return strResult;
	}

	private static String getSeionKanaForHankaku(String strTarget){
		String strResult = '';
		String strConvertSrc = 'ｧｨｩｪｫｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁｯﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛｬｭｮﾜｦﾝ';
		String strConvertDst = 'アイウエオアイウエオカキクケコサシスセソタチツツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロヤユヨワヲン';

		strTarget = strTarget.replaceAll('[ﾞ|ﾟ|ｰ]','');
		strResult = conversionProcess(strTarget, strConvertSrc, strConvertDst);

		return strResult;
	}

	private static String conversionProcess(String strTarget, String strConvertSrc, String strConvertDst){
		String strResult = '';

		for(Integer i=0; i<strTarget.length(); i++){
			Integer index = strConvertSrc.indexOf(strTarget.substring(i, i+1));
			if(index >= 0){
				strResult+=strConvertDst.substring(index, index+1);
			}else{
				strResult+=strTarget.substring(i, i+1);
			}
		}

		return strResult;
	}

}