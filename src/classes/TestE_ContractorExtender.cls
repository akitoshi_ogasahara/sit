@isTest
private class TestE_ContractorExtender{

    private static testMethod void testEContractor1(){
        
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;       
        
        //契約者情報登録
        List<Contact> cntList = new List<Contact>();
        Contact cntKeiyaku = new Contact();
        cntKeiyaku.FirstName = 'テスト契約者';
        cntKeiyaku.LastName = 'テスト苗字';
        cntKeiyaku.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
        cntKeiyaku.E_CLTPF_DOB__c = '20141111';
        cntKeiyaku.E_CLTPF_CLTPCODE__c = '1234567';
        cntKeiyaku.E_CLTPF_ZCLKADDR__c = 'テスト住所';
        cntKeiyaku.E_CLTPF_CLTPHONE01__c = '111-1111';
        cntKeiyaku.E_CLTPF_CLTPHONE02__c = '222-2222';
        cntKeiyaku.E_CLTPF_FAXNO__c = '333-3333';
        cntKeiyaku.E_CLTPF_CLNTNUM__c = 'testno1';
        cntList.add(cntKeiyaku);
        //被保険者情報登録
        Contact cntHiho = new Contact();
        cntHiho.FirstName = 'テスト被保険者';
        cntHiho.LastName = 'テスト苗字';
        cntHiho.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
        cntHiho.E_CLTPF_DOB__c = '20141111';
        cntHiho.E_CLTPF_ZKNJSEX__c = '男';
        cntList.add(cntHiho);
        insert cntList;
        //保険契約ヘッダ情報登録
        E_Policy__c epoli = new  E_Policy__c();
        epoli.Contractor__c = cntList[0].id;
        epoli.Insured__c = cntList[1].Id;
        epoli.DSPVA_ANBCCD__c = 99;
        epoli.COMM_CHDRNUM__c = 'テスト証券番号';
        insert epoli;
        //保険顧客情報登録
        E_CRLPF__c eCrf = new E_CRLPF__c();
        List<E_CRLPF__c> eCrfList = new List<E_CRLPF__c>();
        eCrf.ZCLNAME__c = 'テストその他被保険者(妻)';
        eCrf.ZCLKNAME__c = 'テストフリガナ';
        eCrf.ANBCCD__c = 99;
        eCrf.CLRRROLE__c = 'AL';
        eCrf.ZKNJSEX__c = '女';
        eCrf.DOB__c = '20141010';
        eCrf.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf);
        E_CRLPF__c eCrf2 = new E_CRLPF__c();
        eCrf2.ZCLNAME__c = 'テストその他被保険者(子供)';
        eCrf2.ZCLKNAME__c = 'テストフリガナ';
        eCrf2.ANBCCD__c = 99;
        eCrf2.CLRRROLE__c = 'CD';
        eCrf2.ZKNJSEX__c = '女';
        eCrf2.DOB__c = '20141010';
        eCrf2.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf2);
        E_CRLPF__c eCrf3 = new E_CRLPF__c();
        eCrf3.ZCLNAME__c = 'テスト質権者';
        eCrf3.ZCLKNAME__c = 'テストフリガナ';
        eCrf3.CLRRROLE__c = 'NE';
        eCrf3.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf3);
        E_CRLPF__c eCrf4 = new E_CRLPF__c();
        eCrf4.ZCLNAME__c = 'テスト主被保険者';
        eCrf4.ZCLKNAME__c = 'テストフリガナ';
        eCrf4.CLRRROLE__c = 'LA';
        eCrf4.ANBCCD__c = 55;
        eCrf4.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf4);
        insert  eCrfList;
        String actualPostalCode;
        //テストユーザで機能実行開始
        System.runAs(u){        
            Test.startTest();

            Pagereference  testpage = Page.E_Contractor;
            testpage.getParameters().put('id', epoli.Id);

            Test.setCurrentPage(testpage);


            Apexpages.Standardcontroller sdcon = new  Apexpages.Standardcontroller(epoli);
            E_ContractorController cttcon = new E_ContractorController(sdcon);
            E_ContractorExtender  cttex = cttcon.getExtender(); 
            cttex.init();
            cttex.pageAction();
            resultPage = cttex.PageAction();
            actualPostalCode = cttcon.record.Contractor__r.E_CLTPF_CLTPCODE__c;
            //テスト終了
            Test.stopTest();
        }
        //郵便番号が7桁の場合
        //System.assertEquals('123-4567',actualPostalCode);
        system.assertequals(null,resultPage);
    }

    private static testMethod void testEContractor2(){
        
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;       
        //契約者情報登録
        List<Contact> cntList = new List<Contact>();
        Contact cntKeiyaku = new Contact();
        cntKeiyaku.FirstName = 'テスト契約者';
        cntKeiyaku.LastName = 'テスト苗字';
        cntKeiyaku.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
        cntKeiyaku.E_CLTPF_DOB__c = '20141111';
        cntKeiyaku.E_CLTPF_CLTPCODE__c = '1';
        cntKeiyaku.E_CLTPF_ZCLKADDR__c = 'テスト住所';
        cntKeiyaku.E_CLTPF_CLTPHONE01__c = '111-1111';
        cntKeiyaku.E_CLTPF_CLTPHONE02__c = '222-2222';
        cntKeiyaku.E_CLTPF_FAXNO__c = '333-3333';
        cntKeiyaku.E_CLTPF_CLNTNUM__c = 'testno2';
        cntList.add(cntKeiyaku);
        //被保険者情報登録
        Contact cntHiho = new Contact();
        cntHiho.FirstName = 'テスト被保険者';
        cntHiho.LastName = 'テスト苗字';
        cntHiho.E_CLTPF_ZCLKNAME__c = 'テストフリガナ';
        cntHiho.E_CLTPF_DOB__c = '20141111';
        cntHiho.E_CLTPF_ZKNJSEX__c = '男';
        cntList.add(cntHiho);
        insert cntList;
        //保険契約ヘッダ情報登録
        E_Policy__c epoli = new  E_Policy__c();
        epoli.Contractor__c = cntList[0].id;
        epoli.Insured__c = cntList[1].Id;
        epoli.DSPVA_ANBCCD__c = 99;
        epoli.COMM_CHDRNUM__c = 'テスト証券番号';
        insert epoli;
        //保険顧客情報登録
        E_CRLPF__c eCrf = new E_CRLPF__c();
        List<E_CRLPF__c> eCrfList = new List<E_CRLPF__c>();
        eCrf.ZCLNAME__c = 'テストその他被保険者(妻)';
        eCrf.ZCLKNAME__c = 'テストフリガナ';
        eCrf.ANBCCD__c = 99;
        eCrf.CLRRROLE__c = 'AL';
        eCrf.ZKNJSEX__c = '女';
        eCrf.DOB__c = '20141010';
        eCrf.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf);
        E_CRLPF__c eCrf2 = new E_CRLPF__c();
        eCrf2.ZCLNAME__c = 'テストその他被保険者(子供)';
        eCrf2.ZCLKNAME__c = 'テストフリガナ';
        eCrf2.ANBCCD__c = 99;
        eCrf2.CLRRROLE__c = 'CD';
        eCrf2.ZKNJSEX__c = '女';
        eCrf2.DOB__c = '20141010';
        eCrf2.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf2);
        E_CRLPF__c eCrf3 = new E_CRLPF__c();
        eCrf3.ZCLNAME__c = 'テスト質権者';
        eCrf3.ZCLKNAME__c = 'テストフリガナ';
        eCrf3.CLRRROLE__c = 'NE';
        eCrf3.E_Policy__c = epoli.Id;
        eCrfList.add(eCrf3);
        insert  eCrfList;
                //テストユーザで機能実行開始
        System.runAs(u){        
            Test.startTest();
            
            Pagereference  testpage = Page.E_Contractor;
            testpage.getParameters().put('id', epoli.Id);

            Test.setCurrentPage(testpage);

            Apexpages.Standardcontroller sdcon = new  Apexpages.Standardcontroller(epoli);
            E_ContractorController cttcon = new E_ContractorController(sdcon);
            E_ContractorExtender  cttex = cttcon.getExtender(); 
            cttex.init();
            resultpage = cttex.pageAction();
            //テスト終了
            Test.stopTest();
        }
        //郵便番号が6桁の場合
        //System.assertEquals(cttcon.record.Contractor__r.E_CLTPF_CLTPCODE__c,'1');
        system.assertequals(null,resultPage);
    }

}