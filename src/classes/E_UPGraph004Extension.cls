public with sharing class E_UPGraph004Extension {

	// ダウンロード履歴ID
	public String downloadHistoryId{get; set;}
	private E_UPTerm003Controller controller;

	/** コンストラクタ */
	public E_UPGraph004Extension(E_UPTerm003Controller con){
		controller = con;
	}
	
	/** init */
	public Pagereference init004(){
        //controller.pgTitle = E_UPTerm003Controller.UNITPRICE_PAGENAMES.get('004');		//ユニットプライス騰落率
        controller.pgTitle = 'ユニットプライス推移グラフ';
		Pagereference pref = controller.init();
		
		if(controller.dlCondi != null){
			// ダウンロード履歴ID取得
			downloadHistoryId = getDownloadHistoryId();
			return pref;

		//　DLConditionがNULLの場合
		}else{
			return E_Util.toUpErrorPage('ERR|002');
		}
	}
	
	/** ファンド基本情報 */
	public E_UPBPF__c getUpbpf(){
		return controller.eupbpf;
	}
	
	/* ダウンロード履歴登録 */
	private String getDownloadHistoryId(){
		// ダウンロード履歴登録
		E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
		dh.outputType__c = E_Const.DH_OUTPUTTYPE_GRAPH;		// 
		dh.Conditions__c = controller.dlCondi.toJSON();		// 検索条件
		insert dh;
			
		return dh.Id;
	}
}