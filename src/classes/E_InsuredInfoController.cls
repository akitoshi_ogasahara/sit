global with sharing class E_InsuredInfoController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Contact record {get{return (Contact)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_InsuredInfoExtender getExtender() {return (E_InsuredInfoExtender)extender;}
	
	
	public InsuredList InsuredList {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_InsuredInfoController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome',' {!Extender.welcome} ');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component970');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component970',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!extender.birthDate}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1009');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1009',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!InsuredList_item.record.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1010');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1010',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component975');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component975',tmpPropMap);


		SObjectField f;

		f = E_Policy__c.fields.ContractorName__c;
		f = E_Policy__c.fields.COMM_ZCOVRNAM__c;
		f = E_Policy__c.fields.COMM_ZRSTDESC__c;
		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.RecordTypeId;
		f = E_Policy__c.fields.COMM_STATCODE__c;
		f = E_Policy__c.fields.ContractorE_CLTPF_ZCLKNAME__c;
		f = E_Policy__c.fields.COMM_OCCDATE__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Contact.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('Contact');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('E_COMM_ZCLADDR__c');
			mainQuery.addFieldAsOutput('E_CLTPF_ZKNJSEX__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			InsuredList = new InsuredList(new List<E_Policy__c>(), new List<InsuredListItem>(), new List<E_Policy__c>(), null);
			listItemHolders.put('InsuredList', InsuredList);
			query = new SkyEditor2.Query('E_Policy__c');
			query.addFieldAsOutput('COMM_CHDRNUM__c');
			query.addFieldAsOutput('ContractorName__c');
			query.addFieldAsOutput('COMM_ZCOVRNAM__c');
			query.addFieldAsOutput('COMM_ZRSTDESC__c');
			query.addFieldAsOutput('RecordType.Name');
			query.addFieldAsOutput('COMM_OCCDATE__c');
			query.addFieldAsOutput('COMM_CRTABLE2__c');
			query.addFieldAsOutput('COMM_STATCODE__c');
			query.addFieldAsOutput('Insured__c');
			query.addFieldAsOutput('RecordType.DeveloperName');
			query.addFieldAsOutput('COMM_CHDRNUM_LINK__c');
			query.addFieldAsOutput('COMM_CHDRNUM_LINK_I__c');
			query.addFieldAsOutput('RecordTypeId');
			InsuredList.queryRelatedEvent = False;
			query.limitRecords(100);
			queryMap.put('InsuredList', query);
			
			
			SkyEditor2.Query InsuredListQuery = queryMap.get('InsuredList');
			InsuredListQuery.addWhereIfNotFirst('AND');
			InsuredListQuery.addWhere('(  ( RecordType.DeveloperName = \'ECHDPF\' AND COMM_STATCODE__c != \'A\' AND COMM_STATCODE__c != \'J\' )  OR RecordType.DeveloperName = \'ESPHPF\')');
			InsuredListQuery.addSort('ContractorE_CLTPF_ZCLKNAME__c',True,False).addSort('COMM_OCCDATE__c',True,False);
			p_showHeader = false;
			p_sidebar = false;
			sve_ClassName = 'E_InsuredInfoController';
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_InsuredInfoExtender(this);
			init();
			
			InsuredList.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class InsuredListItem extends SkyEditor2.ListItem {
		public E_Policy__c record{get; private set;}
		@TestVisible
		InsuredListItem(InsuredList holder, E_Policy__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class InsuredList extends SkyEditor2.ListItemHolder {
		public List<InsuredListItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			InsuredList(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<InsuredListItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new InsuredListItem(this, (E_Policy__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}