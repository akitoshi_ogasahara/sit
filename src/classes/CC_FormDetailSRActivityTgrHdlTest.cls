/*
 * CC_FormDetailSRActivityTgrHdlTest
 * Test class of CC_FormDetailSRActivityTgrHdl
 * created  : Accenture 2018/5/29
 * modified :
 */

@isTest
private class CC_FormDetailSRActivityTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
			insert SRTypeMaster;

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			insert newCase;

			//Create FormDetail
			CC_FormDetail__c newformDetail = CC_TestDataFactory.getFormDetailSkel(newCase.Id);
			insert newformDetail;

		}
	}

	/**
	* Test Update
	* FollowUpSRActivity will be created
	*/
	static testMethod void formDetailSRActivityTgrHdlTestUpdate01() {
		System.runAs ( testUser ) {

			CC_FormDetail__c newformDetail = [select Id, CC_IsFormOutputFlg__c from CC_FormDetail__c where CC_Input001_1__c = 'testFormDetail'];

			Test.startTest();
			newformDetail.CC_IsFormOutputFlg__c = true;
			Update newformDetail;
			Test.stopTest();

		}
	}

	/**
	* Test Update
	* FollowUpSRActivity will not be created
	*/
	static testMethod void formDetailSRActivityTgrHdlTestUpdate02() {
		System.runAs ( testUser ) {

			//Prepare test data
			CC_FormDetail__c newformDetail = [select Id, CC_Case__c, CC_IsFormOutputFlg__c from CC_FormDetail__c where CC_Input001_1__c = 'testFormDetail'];

			//update Case
			Case newCase = [select Id, CC_SubStatus__c from Case where Id =: newformDetail.CC_Case__c];
			newCase.CC_SubStatus__c = '不備中';
			Update newCase;

			Test.startTest();
			newformDetail.CC_IsFormOutputFlg__c = true;
			Update newformDetail;
			Test.stopTest();

		}
	}

}