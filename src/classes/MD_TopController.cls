public with sharing class MD_TopController extends MD_AbstractController {
	// 表示件数
	private static Integer MD_VIEW_LIMIT = 5;

	// お知らせのページングNo
	private Integer infoPagingNo;
	// お知らせMap
	private Map<Integer, List<E_MessageMaster__c>> infoMap;

	// お知らせリスト
	public List<E_MessageMaster__c> infoList {get; set;}

	// 前後表示制御
	public Boolean isViewPrev {get; set;}
	public Boolean isViewNext {get; set;}
	
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	
	// 社員フラグ
	public Boolean getIsEmployee() {
		return access.isEmployee();
	}

	public Boolean getIsSumiseiIRIS(){
		return access.isSumiseiIRISUser();
	}

	// 掲示ファイルリスト
	public List<E_CMSFileInfo> fileList {
		get {
			List<E_CMSFileInfo> cmsFileList = new List<E_CMSFileInfo>();

			// ファイル情報取得(ファイル提示)
			E_MenuMaster__c menu = E_MessageMasterDao.getMenuMasterRecByKey(MD_Const.MD_MENU_KEY_FILE);
			List<E_MessageMaster__c> cmsMsg = E_MessageMasterDao.getMessagesByMenuId(menu.Id);

			// ファイルリスト作成
			for (E_CMSFile__c cf : E_MessageMasterDao.getCMSFilesWithAttachmentByIds(cmsMsg[0].E_CMSFiles__r)) {
				E_CMSFileInfo finfo = new E_CMSFileInfo(cf);
				cmsFileList.add(finfo);
			}

			return cmsFileList;
		}
		set;
	}

	/* Constructor */
	public MD_TopController() {
		infoPagingNo = 1;
	}

	/* Init */
	public PageReference init() {
		try {
			// ページメッセージの初期化
			pageMessages = getPageMessages();

			// お知らせMap作成
			createInfoMap();

			// 表示するお知らせセット
			setInfoList(infoPagingNo);

			// フラグセット
			isViewPrev = false;
			isViewNext = infoMap.size() > 1 ? true : false;	

			// IRIS Day2 ページアクセスログは E_Logging コンポーネントで取得するように変更
			return null;
			// アクセスログ登録
			//return insertAccessLog();
	
		} catch (Exception e) {
			System.debug('■［Error_init］' + e.getMessage());

			// アクセスログ登録、 エラーページへ
			return insertErrorLog(e.getMessage());
		}
	}

	/* doPrev */
	public void doPrev() {
		try {
			// メッセージ初期化
			pageMessages.clearMessages();

			infoPagingNo--;
			setInfoList(infoPagingNo);

		} catch (Exception e) {
			System.debug('■［Error_init］' + e.getMessage());
			pageMessages.addErrorMessage(e.getMessage());
		}
	}

	/* doNext */
	public void doNext() {
		try {
			// メッセージ初期化
			pageMessages.clearMessages();

			infoPagingNo++;
			setInfoList(infoPagingNo);

		} catch (Exception e) {
			System.debug('■［Error_init］' + e.getMessage());
			pageMessages.addErrorMessage(e.getMessage());
		}
	}

	/* おしらせMap作成 */
	private void createInfoMap() {
		infoMap = new Map<Integer, List<E_MessageMaster__c>>();
		List<E_MessageMaster__c> infoList = new List<E_MessageMaster__c>();

		// メッセージマスタ取得(お知らせ)
		List<E_MessageMaster__c> messageList = E_MEssageMasterDao.getRecsByMenuKey(MD_Const.MD_MENU_KEY_INFO, MD_Const.MD_LIMIT_INFO);

		Integer keyNo = 1;
		Integer cnt = 0;
		for (E_MessageMaster__c msg : messageList) {
			// 5件単位のリスト生成
			if (cnt < MD_VIEW_LIMIT) {
				infoList.add(msg);
				cnt++;
			} else {
				infoMap.put(keyNo, infoList);
				infoList = new List<E_MessageMaster__c>();
				infoList.add(msg);
				keyNo++;
				cnt = 1;
			}
		}
		// 最終行対応
		if (!messageList.isEmpty()) {
			infoMap.put(keyNo, infoList);
		}
	}

	/* お知らせ情報セット */
	private void setInfoList(Integer targetNo) {
		// 表示するお知らせセット
		infoList = infoMap.get(targetNo);

		// 前の5件：対象が1ページ目の場合はｆａｌｓｅ
		isViewPrev = (targetNo == 1) ? false : true;
		// 次の5件：対象が最終ページでない場合はtrue
		isViewNext = targetNo < infoMap.size() ? true : false;
	}
}