@isTest
private class TestCR_OutSrcStatusManager {
	// コンストラクタテスト（承認権限・個人情報取り扱いなし）
	private static testMethod void CROutSrcStatusManagerTest001(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
                
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
			Test.stopTest();
        }
	}

	// コンストラクタテスト（承認権限なし、個人情報取り扱い有）
	private static testMethod void CROutSrcStatusManagerTest002(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcHasCustomerInfo(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
			Test.stopTest();
        }
	}

	// コンストラクタテスト（承認権限あり、個人情報取り扱い有、ステータス：02.確認中）
	private static testMethod void CROutSrcStatusManagerTest003(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusConfirm(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
			Test.stopTest();
        }
	}

	// コンストラクタテスト（承認権限あり、個人情報取り扱い有、ステータス：02.報告済）
	private static testMethod void CROutSrcStatusManagerTest004(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusReport(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
			Test.stopTest();
        }
	}

	// コンストラクタテスト（承認権限あり、個人情報取り扱い有、ステータス：03.承認済）
	private static testMethod void CROutSrcStatusManagerTest005(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusApprove(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
			Test.stopTest();
        }
	}

	// コンストラクタテスト（承認権限あり、個人情報取り扱い有、ステータス：09.委託終了）
	private static testMethod void CROutSrcStatusManagerTest006(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusTerminate(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
			Test.stopTest();
        }
	}

	// validateFiles テスト
	private static testMethod void CROutSrcStatusManagerTest007(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC); 
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, 0);
				system.assertEquals(outsrcStatus.validateFiles(), false);
			Test.stopTest();
        }
	}	

	// validateFiles テスト(ファイルあり)
	private static testMethod void CROutSrcStatusManagerTest008(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.validateFiles(), true);
			Test.stopTest();
        }
	}

	// validateFiles テスト(理由あり)
	private static testMethod void CROutSrcStatusManagerTest009(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcReason(true, agny.Id);
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, 0);
				system.assertEquals(outsrcStatus.validateFiles(), true);
			Test.stopTest();
        }
	}

	// canEdit テスト(ステータス：01.作成中)	
	private static testMethod void CROutSrcStatusManagerTest010(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.canEdit(), true);
			Test.stopTest();
        }
	}

	// canEdit テスト(ステータス：02.確認中、承認権限なし 社員ユーザ　→　編集可能　社員ユーザは確認中編集可能とする)	
	private static testMethod void CROutSrcStatusManagerTest011(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusConfirm(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.canEdit(), true);		//社員は確認中も編集可能
			Test.stopTest();
        }
	}

	// canEdit テスト(ステータス：09.委託終了、承認権限あり)	
	private static testMethod void CROutSrcStatusManagerTest012(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusTerminate(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.canEdit(), true);
			Test.stopTest();
        }
	}	
	
	// canDelete テスト(ステータス：01.作成中)	
	private static testMethod void CROutSrcStatusManagerTest013(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.canDelete(), true);
			Test.stopTest();
        }
	}

	// canDelete テスト(ステータス：02.確認中、承認権限なし)	
	private static testMethod void CROutSrcStatusManagerTest014(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusConfirm(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.canDelete(), true);		//社員は確認中も編集可能なので削除も可能
			Test.stopTest();
        }
	}

	// canDelete テスト(ステータス：09.委託終了、承認権限あり)	
	private static testMethod void CROutSrcStatusManagerTest015(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusTerminate(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.canDelete(), true);
			Test.stopTest();
        }
	}
	
	// hasNextAction テスト(ステータス：01.作成中)	
	private static testMethod void CROutSrcStatusManagerTest016(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.hasNextAction(), true);
			Test.stopTest();
        }
	}

	// hasNextAction テスト(ステータス：09.委託終了)	
	private static testMethod void CROutSrcStatusManagerTest017(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusTerminate(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.hasNextAction(), false);
			Test.stopTest();
        }
	}

	// getNextActionLabel テスト(ステータス：01.作成中)	
	private static testMethod void CROutSrcStatusManagerTest018(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.getNextActionLabel(), '報告');
			Test.stopTest();
        }
	}

	// getNextActionLabel テスト(ステータス：09.委託終了)	
	private static testMethod void CROutSrcStatusManagerTest019(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcHasCustomerInfo(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.getNextActionLabel(), '申請');
			Test.stopTest();
        }
	}
	
	// doNextAction テスト(doApply)	
	private static testMethod void CROutSrcStatusManagerTest020(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcHasCustomerInfo(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				outsrcStatus.doNextAction();
				system.assertEquals(true, E_PageMessagesHolder.getInstance().getHasErrorMessages());		//必須項目チェックに抵触

				//必須項目を入力して、申請実行
				outsrc.address__c = '東京都中央区日本橋';
				outsrc.Business__c = '業務委託内容';
				outsrc.OutsrcFrom__c = System.Today();
				outsrcStatus.doNextAction();
				
				system.assertEquals(false, E_PageMessagesHolder.getInstance().getHasErrorMessages());
				system.assertEquals(outsrc.Status__c, CR_Const.OUTSRC_STATUS_02a);
			Test.stopTest();
        }
	}

	// doNextAction テスト(doReport)	
	private static testMethod void CROutSrcStatusManagerTest021(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				outsrc.Representative__c = '代表者名';
				outsrc.address__c = '東京都中央区日本橋';
				outsrc.Business__c = '業務委託内容';
				outsrc.OutsrcFrom__c = System.Today();
				outsrcStatus.doNextAction();
				
				system.assertEquals(false, E_PageMessagesHolder.getInstance().getHasErrorMessages());
				system.assertEquals(outsrc.Status__c, CR_Const.OUTSRC_STATUS_02b);
			Test.stopTest();
        }
	}

	// doNextAction テスト(doApprove)	
	private static testMethod void CROutSrcStatusManagerTest022(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusConfirm(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				outsrcStatus.doNextAction();
				
				system.assertEquals(outsrc.Status__c, CR_Const.OUTSRC_STATUS_03);
			Test.stopTest();
        }
	}

	// doNextAction テスト(doTerminate)	
	private static testMethod void CROutSrcStatusManagerTest023(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusApprove(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());				
				system.assertEquals(outsrcStatus.doNextAction().getUrl(), '/apex/e_croutsrcterminate?id=' + outsrc.Id);
			Test.stopTest();
        }
	}
	
	// doNextAction テスト(doTerminate)	
	private static testMethod void CROutSrcStatusManagerTest024(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusReport(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());				
				system.assertEquals(outsrcStatus.doNextAction().getUrl(), '/apex/e_croutsrcterminate?id=' + outsrc.Id);
			Test.stopTest();
        }
	}
	
	// hasRejectAction テスト(ステータス：01.作成中)	
	private static testMethod void CROutSrcStatusManagerTest025(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.hasRejectAction(), false);
			Test.stopTest();
        }
	}

	// hasRejectAction テスト(ステータス：02.確認中)	
	private static testMethod void CROutSrcStatusManagerTest026(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusConfirm(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.hasRejectAction(), true);
			Test.stopTest();
        }
	}

	// hasRejectAction テスト(ステータス：09.委託終了)	
	private static testMethod void CROutSrcStatusManagerTest027(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusTerminate(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.hasRejectAction(), false);
			Test.stopTest();
        }
	}

	// getRejectActionLabel テスト(ステータス：02.確認中)	
	private static testMethod void CROutSrcStatusManagerTest028(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusConfirm(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.getRejectActionLabel(), '申請取消');
			Test.stopTest();
        }
	}

	// getRejectActionLabel テスト(ステータス：02.報告済)	
	private static testMethod void CROutSrcStatusManagerTest029(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusReport(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.getRejectActionLabel(), '報告取消');
			Test.stopTest();
        }
	}
	
	// getRejectActionLabel テスト(ステータス：03.承認済)	
	private static testMethod void CROutSrcStatusManagerTest030(){
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createAllPermissions(usr.Id);
        
        system.runAs(usr){		
			Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
			    CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			    CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcStatusApprove(true, agny.Id);
			    Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
			    List<Attachment> fileList = [Select Id From Attachment Where Id =: file.Id];
			    
				CR_OutSrcStatusManager outsrcStatus = New CR_OutSrcStatusManager(outsrc, fileList.size());
				system.assertEquals(outsrcStatus.getRejectActionLabel(), '承認取消');
			Test.stopTest();
        }
	}
}




/*	
	// 差戻しアクション
	public PageReference doRejectAction(){
		E_PageMessagesHolder.getInstance().clearMessages();
		// ステータスを『01.作成中』に更新
		updateStatus(STATUS_01);
		
		return null;
	}
	
	// NextActionが報告の場合にTrue　→　個人情報取扱なしのワーニングを表示
	public Boolean nextActionIsReport(){
		return actions.nextAction == OutSrcAction.REPORT;
	}
*/