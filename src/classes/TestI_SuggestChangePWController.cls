@isTest
private class TestI_SuggestChangePWController {

	//今回は変更しないボタン押下
	@isTest static void doReturnTest() {
		User us = createUser();
		PageReference ref;
		Test.startTest();
		System.runAs(us){
			createIDManage(us.id,'');
			I_SuggestChangePWController scp = new I_SuggestChangePWController();
			ref = scp.doReturn();
		}
		System.assertEquals(String.valueOf(ref),String.valueOf(Page.E_Home));
		Test.stopTest();
	}

	@isTest //変更するボタン押下時 IRIS
	static void changePWTest1(){
		User us = createUser();
		PageReference ref = Page.IRIS_SuggestChangePW;
		PageReference result;
		Test.setCurrentPage(ref);
		ApexPages.currentPage().getHeaders().put('User-Agent','Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko');
		Test.startTest();
		System.runAs(us){
			createIDManage(us.id,I_Const.APP_MODE_IRIS);
			I_SuggestChangePWController scp = new I_SuggestChangePWController();
			result = scp.changePW();
		}
		System.assertEquals(String.valueOf(result),String.valueOf(Page.IRIS_ChangePW));
		Test.stopTest();
	}

	@isTest //変更するボタン押下時 NNLink
	static void changePWTest2(){
		User us = createUser();
		PageReference ref;
		Test.startTest();
		System.runAs(us){
			createIDManage(us.id,I_Const.APP_MODE_NNLINK);
			I_SuggestChangePWController scp = new I_SuggestChangePWController();
			ref = scp.changePW();
		}
		System.assertEquals(String.valueOf(ref),String.valueOf(Page.E_StandardPasswordChange));
		Test.stopTest();
	}

	@isTest //pageActionのテスト
	static void pageActionTest1() {
		User us = createUser();
		PageReference ref;
		Test.startTest();
		System.runAs(us){
			createIDManage(us.id,'');
			I_SuggestChangePWController scp = new I_SuggestChangePWController();
			ref = scp.pageAction();
		}
		System.assertEquals(ref,null);
		Test.stopTest();
	}

	//ユーザ
	static User createUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			//User us = new User();
			Contact con = createContactData();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = con.Id;
			us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
			us.IsActive = true;
			insert us;

			Set<String> permissionNames = new Set<String>();
			permissionNames.add('E_PermissionSet_Base');//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = us.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}
		}
		return us;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
		Id accid = createAccount();
		con.FirstName = '花子';
		con.LastName = 'テストテスト';
		con.AccountId = accid;
		insert con;
		return con;
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	//ID管理
	static void createIDManage(ID usid,String appMode){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.ZSTATUS01__c = '1';
		ic.ZDSPFLAG02__c = '0';
		ic.User__c = usid;
		ic.appMode__c = appMode;
		insert ic;
	}

}