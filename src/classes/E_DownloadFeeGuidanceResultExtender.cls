global with sharing class E_DownloadFeeGuidanceResultExtender extends E_AbstractViewExtender{
	
	public E_ASPPF__c rec {get; private set;}
	
    private static final String PAGE_TITLE = '手数料のご案内';
	// Referer
	public String retUrl {get; set;}
    
    E_DownloadFeeGuidanceResultController extension;
    pagereference pageref;
    /* コンストラクタ */
    public E_DownloadFeeGuidanceResultExtender(E_DownloadFeeGuidanceResultController extension){
        super();
        this.extension = extension;
        string recId = ApexPages.currentPage().getParameters().get('spfid');
        if(E_Util.isValidateId(recId, Schema.E_ASPPF__c.SObjectType)){
	        rec = E_ASPPFDaoWithout.getRecById(recId);
        }
        pageRef = doAuth(E_Const.ID_KIND.DOWNLOAD, null);
        this.pgTitle = PAGE_TITLE;
        
    }
    
    public String getZDATBKTR(){
//        return E_util.getFormatDate(extension.record.ZDATBKTR__c,'yyyy年MM月dd日');
        return E_util.getFormatDate(rec.ZDATBKTR__c,'yyyy/MM/dd');
    }
    
    /* 個別チェック処理 */
    public override boolean isValidate(){
    	//レコード存在確認
    	if(rec == null){
    		errorMSGCode = E_Const.ERROR_MSG_PARAMETER;
    		return false;
    	}
    	
    	//手数料のご案内の参照先Accountと募集人の代理店格Accountが同じかを確認する
    	boolean isAccountEqual = false;
    	if(access.isAgent()){
    		isAccountEqual = access.user.Contact.Account.ParentId.equals(rec.Account__c);
    	}else if(access.isEmployee()){
    		isAccountEqual = true;
    	}
        if(isAccountEqual && access.hasAuthDistributor() && access.canFeeDetail()){
            return true;
        }else{
            errorMSGCode = E_Const.ERROR_MSG_FUNCTION;
	        return false;
        }
    }
    
	public pagereference pageAction() {
		// Referer をセット
		retUrl = ApexPages.currentPage().getHeaders().get('referer');
        return E_Util.toErrorPage(pageRef,null);
	}
	
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		PageReference pageRef;
		if(retUrl.toLowerCase().indexOf(Page.IRIS_LogView.getUrl().toLowerCase()) > -1){
			pageRef = new PageReference(retUrl);
		} else {
			pageRef = Page.e_downloadfeeguidancesearch;
			pageRef.getParameters().put('did', rec.Account__c);
			System.debug(pageRef);
		}
		return pageRef;
	}
}