public with sharing class E_CancellFormDownloadControlDao {
    /**
     * 商品コードと取引先IDをキーに、指定した解約申込書DL設定情報を取得する。
     * 複数件存在する場合は、１件のみ取得する。
     * @param policyId: 取引先ID
     * @param productCode: 商品コード
     * @return E_CancellFormDownloadControl: 解約申込書DL設定
     */
    public static E_CancellFormDownloadControl__c getRecByType (Id accountId,String productCode) {
        E_CancellFormDownloadControl__c rec = null;
        List<E_CancellFormDownloadControl__c> recs = [
            Select 
                Id,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,
                Name,Account__c,ProductCode__c,Product__c 
            From 
                E_CancellFormDownloadControl__c 
            Where 
                ProductCode__c =: productCode
            AND
                (
                    Account__c =: accountId
                OR 
                    Account__c = NULL
                )
                order by Account__c NULLS LAST
        ];
        if (recs.size() > 0) {
            // 複数件取得した場合は１件目を取得（ソート条件でNULLは最初にこない）
            rec = recs.get(0);
        }
        return rec;
    }
}