@isTest
private class TestE_UnitDao {
	
	@isTest static void getUnitRecTest() {
		E_Area__c area = new E_Area__c(
			Name = 'テストエリア',
			AreaCode__c = 'XX'
		);
		insert area;

		E_Unit__c unit = new E_Unit__c(
			Name = 'テスト営業部',
			Area__c = area.Id
		);
		insert unit;

		Test.startTest();
		List<E_Unit__c> rec = E_UnitDao.getUnitRec();
		System.assertEquals(unit.Id, rec.get(0).Id);
		Test.stopTest();
	}
	
	@isTest static void getUnitRecByIdTest() {
		E_Area__c area = new E_Area__c(
			Name = 'テストエリア',
			AreaCode__c = 'XX'
		);
		insert area;

		E_Unit__c unit = new E_Unit__c(
			Name = 'テスト営業部',
			Area__c = area.Id
		);
		insert unit;

		String unitId = unit.Id;

		Test.startTest();
		E_Unit__c rec = E_UnitDao.getUnitRecById(unitId);
		System.assertEquals(unit.Id, rec.Id);
		Test.stopTest();
	}

	@isTest static void getUnitRecByBRCodeTest() {
		E_Area__c area = new E_Area__c(
			Name = 'テストエリア',
			AreaCode__c = 'XX'
		);
		insert area;

		E_Unit__c unit = new E_Unit__c(
			Name = 'テスト営業部',
			Area__c = area.Id,
			BRANCH__c = 'XX'
		);
		insert unit;

		String brCode = unit.BRANCH__c;

		Test.startTest();
		E_Unit__c rec = E_UnitDao.getUnitRecByBRCode(brCode);
		System.assertEquals(unit.Id, rec.Id);
		Test.stopTest();
	}
}