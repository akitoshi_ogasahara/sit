public with sharing class I_SurveyController extends I_AbstractController{
	//アンケートデータ
	public I_Survey__c survey {get;set;}
	//アクセスユーザ
	public User user {get;set;}
	//独自の例外クラス
	public class SurveyException extends Exception{}
	//アンケート完了フラグ
	public boolean isSurveyCompleted {get; private set;}
	//ページ名
	private static final String PAGE_NAME = '代理店アンケート';
	//アンケート完了
	//private static final String SURVEY_COMPLETE = 'SV|001';
	//アンケート対象外
	private static final String SURVEY_EXCLUDE = 'SV|002';
	//メールアドレス未登録
	private static final String NO_EMAIL = 'SV|003';
	//アンケート取得失敗
	private static final String SURVEY_REC_FAILED = 'SV|004';
	//アンケート回答済み
	private static final String SURVEY_FINISHED = 'SV|005';
	//アンケート期間外
	private static final String SURVEY_PERIOD = 'SV|006';


	//添付ファイルURL
	public String getAttUrl(){
		String attURL= '';
		if(survey != null && survey.id != null && !survey.Attachments.isEmpty()){
			attURL = '/servlet/servlet.FileDownload?file=' + survey.Attachments[0].id;
		}
		return attURL;
	}


	public I_SurveyController() {
		isSurveyCompleted = false;
	}
	
	/**
	 * Confirmボタンアクション
	 */
	public PageReference doConfirm(){

		Savepoint sp = Database.setSavepoint();

		try{
			I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
			sTarget.Ownerid = user.id;
			sTarget.Survey__c = survey.id;

// 			//対象が募集人の場合
// 			if(getIsTargetAgentCheck()){
// //				sTarget.Name = user.contact.Name;
// 				sTarget.Contact__c = user.ContactId;
// 			//対象が代理店の場合
// 			}else if(getIsTargetAgencyCheck()){
// 				sTarget.ParentAccount__c = user.Contact.Account.ParentId;
// 			}
			//募集人は常に設定、対象が代理店の時には追加で代理店格を設定
			sTarget.Contact__c = user.contactId;
			if( getIsTargetAgencyCheck() ){
				sTarget.ParentAccount__c = user.Contact.Account.ParentId;
			}

			//参照権を持たない代理店格Idをレコードに含むとエラーになるので、withoutでinsertする
			I_SurveyTargetDaoWithout.insertRec(sTarget);

			I_SurveyAnswer__c answer = new I_SurveyAnswer__c();
			answer.SurveyTarget__c = sTarget.id;
			if(getIsTargetAgentCheck()){
				answer.SurveyTargetName__c = user.Contact.Name;
			}else if(getIsTargetAgencyCheck()){
				answer.SurveyTargetName__c = user.Contact.Account.Parent.Name;
			}
			answer.SurveyQuestion__c = survey.I_SurveyQuestions__r[0].id;
			answer.Answer__c = survey.I_SurveyQuestions__r[0].Selection__c;
			//登録時点のレコードを作成
			answer.SurveyAgentName__c = user.Contact.Name;											//募集人名
			answer.SurveyOfficeCode__c = user.Contact.Account.E_CL2PF_ZAGCYNUM__c;					//代理店事務所コード
			answer.SurveyAccountName__c = user.Contact.Account.Name;								//代理店名
			answer.SurveyParentAccountCode__c = user.Contact.Account.Parent.E_CL1PF_ZHEADAY__c;		//代理店格コード
			answer.SurveyParentAccountName__c = user.Contact.Account.Parent.Name;					//代理店格名
			answer.SurveyEmail__c = user.Email;
			answer.SurveyBranchCode__c = user.Contact.Account.E_CL2PF_BRANCH__c;					//支社コード
			answer.SurveyUnit__c = user.Contact.Account.Owner.Unit__c;								//営業部
			answer.SurveyUnitName__c = user.Contact.Account.Owner.UnitName__c;						//担当拠点
			answer.SurveySectionCode__c = user.Contact.Account.KSECTION__c;							//課コード
			answer.SurveyChannelCode__c = user.Contact.Account.Parent.E_CL1PF_KCHANNEL__c;			//チャネル
			answer.SurveySource__c = user.Contact.Account.Parent.E_CL1PF_ZSOURCE__c;				//ソース
			answer.SurveyMRName__c = user.Contact.Account.Owner.Name;								//MR名

			I_SurveyAnswerDaoWithout.insertRec(answer);
			//insert answer;
			isSurveyCompleted = true;

		}catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			getPageMessages().addErrorMessage(e.getMessage());
			Database.rollback(sp);
			pageAccessLog = E_LogUtil.createLog();
			URL urlObj = URL.getCurrentRequestUrl();
		}
		return null;
	}

	/**
	 * init
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = PAGE_NAME;

		//アクセスの情報では不足するためかつ代理店格情報を取得するためWithoutで取得
		user = E_UserDaoWithout.getUserRecordByUserId(access.user.id);
		//アンケート情報の取得
		getSurveyRecord();
		//アンケート画面表示判定
		doValidate();
		return pr;
	}

	/**
	 * アンケート画面表示判定
	 */
	private void doValidate(){
		//募集人以外の場合はエラー
		doAgentCheck();
		//メールアドレスの登録判定
		doEntryEmailCheck();
		//画面表示・アンケート情報取得に失敗した時点でエラーを出す
		if(getPageMessages().getHasErrorMessages()){
			return;
		}
		//アンケート登録済か判定
		doDupulicateCheck();
		doPeriodCheck();
	}

	/**
	 * アンケートデータの取得
	 */
	private void getSurveyRecord(){
		try{
			String surveyKey = ApexPages.CurrentPage().getParameters().get('skey');
			if(String.isNotBlank(surveyKey)){
				survey = I_SurveyDao.getSurveyBySurveyKey(surveyKey);
			}
			if(survey == null || survey.id == null){
				throw new SurveyException(getMSG().get(SURVEY_REC_FAILED));
			}
		}catch(Exception e){
			getPageMessages().addErrorMessage(getMSG().get(SURVEY_REC_FAILED));
		}
	}

	/**
	 * Mailアドレス登録済か判定
	 */
	private void doEntryEmailCheck(){
		if(getIsEntryEMail() == false){
			getPageMessages().addErrorMessage(getMSG().get(NO_EMAIL));
		}
	}

	/**
	 * アンケート登録済か判定
	 */
	private void doDupulicateCheck(){
		List<I_SurveyTarget__c> sTargets = new List<I_SurveyTarget__c>();
		//対象が募集人の場合
		if(getisTargetAgentCheck()){
			sTargets = I_SurveyTargetDao.getRecByUserIdAndSurveyId(user.Id, survey.Id);
		//対象が代理店の場合
		}else if(getIsTargetAgencyCheck()){
			sTargets = I_SurveyTargetDaoWithout.getRecByAccountIdAndSurveyId(user.Contact.Account.ParentId, survey.Id);
		}

		if(sTargets.size() >= 1){
			throw new SurveyException(getMSG().get(SURVEY_FINISHED));
		}
	}

	/**
	 * アンケート期間エラー表示判定
	 */
	private void doPeriodCheck(){
		if(getIsPeriod() == false){
			getPageMessages().addErrorMessage(getMSG().get(SURVEY_PERIOD));
		}
	}

	/**
	 * 募集人判定
	 */
	private void doAgentCheck(){
		//募集人以外の場合はエラー
		if(access.isAgent() == false){
			throw new SurveyException(getMSG().get(SURVEY_EXCLUDE));
		}
	}

	/**
	 * 選択肢が募集人か判定
	 */
	private boolean getIsTargetAgentCheck(){
		if(survey.SurveyTarget__c.equals(I_Const.SEARCH_TYPE_LABEL_AGENT)){
			return true;
		}
		return false;
	}
	/**
	 * 選択肢が代理店か判定
	 */
	private boolean getIsTargetAgencyCheck(){
		if(survey.SurveyTarget__c.equals(I_Const.SEARCH_TYPE_LABEL_AGENCY)){
			return true;
		}
		return false;
	}

	/**
	 * Mailアドレス登録済か判定
	 */
	public boolean getIsEntryEMail() {
		//社員はメールアドレス登録不要
		if(access.isEmployee()){
			return true;
		}
//		String userName = access.user.Username.substringBefore(System.Label.E_USERNAME_SUFFIX);     //ユーザ名のSUFFIXより前の部分を抽出
//		if(userName.equals(access.user.EMAIL)){
//			return true;
//		}
//		return false;
		return E_Util.isActiveEmail( access.user.EMAIL );
	}

	/**
	 * 募集人判定
	 */
	//public boolean getIsAgent(){
	//	if(access.isAgent()){
	//		return true;
	//	}
	//	return false;
	//}

	/**
	 * 期間判定
	 */
	public boolean getIsPeriod(){
		if(survey.PeriodFrom__c <= Date.today() && Date.today() <= survey.PeriodTo__c){
			return true;
		}
		return false;
	}
}