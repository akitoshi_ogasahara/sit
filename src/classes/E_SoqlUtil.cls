public with sharing class E_SoqlUtil {

	/**
	 *		SOQLのWhere句以降を取得
	 */
	public static String getAfterWhereClause(String soql, String sobjNm){
		List<String> ret = soql.split('(?i)from ' + sobjNm);
		if(ret.size()==2){
			System.debug(ret[1].trim());
			return ret[1].trim();
		}
		//2つ以外に分断された場合は空文字を返す
		return '';
	}
	
	/**
	 *		Limit句以降をトリム（最後のLimit句のみ　副問合せも含まれている場合は、最後のLimit句のみの削除）
	 */
	public static String trimLimitClause(String clause){
		Integer pos = clause.lastIndexOfIgnoreCase('LIMIT');
		if(pos>=0){
			return clause.left(pos-1).trim();
		}
		
		return clause;
	} 
}