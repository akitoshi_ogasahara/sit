@isTest
private with sharing class TestMNT_YakkanViewContoroller{
	private static testMethod void testPageMethods() {		MNT_YakkanViewController extension = new MNT_YakkanViewController(new ApexPages.StandardController(new I_ContentMaster__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testyakkanFileViewList() {
		MNT_YakkanViewController.yakkanFileViewList yakkanFileViewList = new MNT_YakkanViewController.yakkanFileViewList(new List<I_ContentMaster__c>(), new List<MNT_YakkanViewController.yakkanFileViewListItem>(), new List<I_ContentMaster__c>(), null);
		yakkanFileViewList.create(new I_ContentMaster__c());
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}