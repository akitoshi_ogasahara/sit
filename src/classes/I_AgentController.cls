public with sharing class I_AgentController extends I_AbstractController{
	//ページメッセージ
//	public E_PageMessagesHolder pageMessages {get;set;}
	public List<String> pageMessages {get;private set;}
	public Boolean getHasMessages(){
		return pageMessages <> null && !pageMessages.isEmpty();
	}
	// リスト行数
	public Integer rowCount {get; set;}
	//事務所ID
	public String officeid {get; set;}

	//挙績情報（社内挙績情報） 権限セット付与チェック
	public Boolean viewSR{
		get{
			if(access.hasSREmployeePermission()){
				return true;
			}
			return false;
		} set;
	}
	// 基本情報-挙績情報(AG)-挙績情報(MR)切り替え用
	public String srTab {get; set;}
	public void selectTab(){
		srTab = ApexPages.currentPage().getParameters().get('srTab');
	}


	// 募集人一覧(表示用)
	public List<AgentListRow> agentRows {get{
		if(agentRows != null){
			return agentRows;
		}
			return readyAction();
		} set;}

	// 募集人一覧取得件数
	public Integer getAgentCount() {
		return agentRows == null ? 0 : agentRows.size();
	}

	// 代理店名
	public String agency {get; set;}

	// 事務所名
	public String office {get; set;}

	// ContactレコードのList
	transient public List<Contact> contactList {get; set;}

	// ContactIDとUserレコードのMap
	transient public Map<ID, User> userMap {get; set;}

	// ContactIDとUserLoginレコードのMap
	transient public Map<ID, UserLogin> userLoginMap {get; set;}			//JIRA_#51 170208 削除

	// UserIDとE_IDRequest__cレコードのMap
	transient public Map<ID, E_IDRequest__c> idRequesutMap {get; set;}

	//JIRA_#251 170403
	//各募集人の利用履歴一覧のリスト
	transient public List<AggregateResult> logList {get;set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE   = 'st';
	// URLパラメータ名 - フィルタID
	public static final String URL_PARAM_FILTER_ID   = 'fid';
	// ソートキー
	public static final String SORT_TYPE_AGENT       = 'agent';
	public static final String SORT_TYPE_CODE        = 'code';
	public static final String SORT_TYPE_IDSTATUS    = 'idStatus';

	//挙績情報 - ソートキー
	public static final String SORT_TYPE_NBCNPYTD		 = 'nbcnp_ytd';
	public static final String SORT_TYPE_NBCANPSPYTD	 = 'nbcanp_spytd';
	public static final String SORT_TYPE_IANP			 = 'ianp';
	public static final String SORT_TYPE_IQA			 = 'iqa';
	public static final String SORT_TYPE_TOTALYTD 		 = 'totalYTD';
	public static final String SORT_TYPE_FSYTD			 = 'fsYTD';
	public static final String SORT_TYPE_PROYTD			 = 'proYTD';
	public static final String SORT_TYPE_DEFRATE		 = 'defRate';
	public static final String SORT_TYPE_AGENTCOUNT		 = 'agentCount';

	//JIRA_#251 170403
	public static final String SORT_TYPE_USAGEHISTORY   = 'usageHistory';
	public static final String USAGEHISTORY_TRUE     = '有り';
	public static final String USAGEHISTORY_FALSE    = '無し';

	//社員ユーザチェック
	public Boolean isEmployee {get; set;}

	//MAX表示件数
	public Integer getListMaxRows(){
//		return I_Const.LIST_MAX_ROWS;
		return 500;
	}

	//データ基準日
	public String bizDate{
		get{
			if(bizDate != null) return this.bizDate;
			String inquiryD = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
			String year = inquiryD.left(4);
			String month = inquiryD.mid(4, 2);
			String day = inquiryD.mid(6, 2);
			return year+'/'+month+'/'+day;
		} set;
	}
	private Decimal null2Zero(Decimal dec){
		return dec == null ? 0 : dec;
	}

	/**
	 * コンストラクタ
	 */
	public I_AgentController() {
		srTab = 'top';
		pageMessages = new List<String>();

		// デフォルトソート - 募集人名 : 昇順
		sortType  = SORT_TYPE_AGENT;
		sortIsAsc = true;

		//社員ユーザチェック
		isEmployee = access.isEmployee();

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > getListMaxRows()){
				pRows = getListMaxRows();
			}
			rowCount = pRows;
		} catch(Exception e){
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	public List<AgentListRow> readyAction() {
		//String searchId = apexPages.currentPage().getParameters().get('id');
		String searchId = apexPages.currentPage().getParameters().get('agent');//募集人コード
		Boolean isAccountId = false;
/*		if(String.isBlank(searchId)){
			// MR,AH,AYを判定
			if (access.isEmployee() || access.isAuthAH()) {
				// URLパラメータ取得
				searchId = apexPages.currentPage().getParameters().get('accid');
				// MR,AH
				if (String.isBlank(searchId)) {
					return redirectAgencyPage();
				}
			} else if (access.isAuthAY()) {
				// AY
				searchId = access.user.AccountId;
				System.debug('*********He is AY searchId='+searchId);
			}
			isAccountId = true;
		}*/
//		searchId = apexPages.currentPage().getParameters().get('office');
		searchId = officeid;
		isAccountId = true;
		// Contact取得
		system.debug('■0'+searchId);
		contactList = getContactList(searchId, isAccountId);
		system.debug('■1'+contactList);

		// User取得
		List<User> userList = getUserList(contactList);

		// ContactIDとUserのMap
		userMap = new Map<ID, User>();
		// UserIDのSet
		Set<ID> userIdSet = new Set<ID>();

		//JIRA #251 170403
		logList = new List<AggregateResult>();

		for (User us: userList) {
			userMap.put(us.Contact.Id, us);
			userIdSet.add(us.ID);
		}

		logList = E_LogDao.getRecAccessLogList(userIdSet);

		/* JIRA_#51 170207 */

		// UserLogin取得
		userLoginMap = getUserLoginList(userIdSet);
		/*
		// E_IDRequest__c取得
		idRequesutMap = getIDRequestMap(userIdSet);
		// 募集人一覧(表示用)取得
		getAgenctList(contactList, userMap, userLoginMap, idRequesutMap);
		*/
		// E_IDRequest__c取得
		idRequesutMap = getIDRequestMap(userIdSet);
		// 募集人一覧(表示用)取得
		getAgenctList(contactList, userMap, idRequesutMap);
		/* 改修ここまで */
		return agentRows;
	}

	/**
	 * 募集人リスト - Contact
	 */
	private List<Contact> getContactList(String searchId, Boolean isAccountId) {
		List<Contact> cons = new List<Contact>();
		if(isAccountId){
			//cons = E_ContactDaoWithout.getRecsByOfficeId(searchId);
			cons = E_ContactDaoWithout.getRecsByAccountId(searchId);		//JIRA_#51 170207
		} else {
			//Contact con = E_ContactDaoWithout.getRecById(Id.valueOf(searchId));
			cons = E_ContactDaoWithout.getRecsByAgntNum(searchId);		//JIRA_#51 170207 募集人コードをキーに前方一致検索
		}

		if (cons.size() > 0) {
			agency = cons.get(0).E_AccParentName__c;
			office = cons.get(0).E_Account__c;
		}

		return cons;
	}

	/**
	 * ユーザリスト - User
	 */
	private List<User> getUserList(List<Contact> cons) {
		// 募集人IDのSet
		Set<ID> contactIdSet = new Set<ID>();
		for (Contact con: cons) {
			contactIdSet.add(con.Id);
		}

		// Userのリスト
		List<User> users = new List<User>();

		// DAO呼び出し
		//users = E_UserDao.getRecsAgentId(contactIdSet);
		//users = E_UserDao.getRecsAgentIdWithEIDC(contactIdSet);			//JIRA_#51 170207
		users = E_UserDaoWithout.getRecsAgentIdWithEIDC(contactIdSet);			//JIRA_#51 170209

		return users;
	}

	/**
	 * ユーザログインのMap - UserLogin
	 */
	/* JIRA_#51 170207 */

	private Map<ID, UserLogin> getUserLoginList(Set<ID> userIdSet){

		// UserLoginのリスト
		List<UserLogin> logins = new List<UserLogin>();

		// DAO呼び出し
		logins = E_UserLoginDaoWithout.getRecsWithIsLockByUserIds(userIdSet);

		// UserIdとUserLoginのMap
		Map<ID, UserLogin> ulMap = new Map<ID, UserLogin>();
		for(UserLogin login : logins){
			ulMap.put(login.UserId, login);
		}

		return ulMap;
	}

	/* 削除ここまで */

	/**
	 * IDリクエストのMap - E_IDRequest__c
	 */
	private Map<ID, E_IDRequest__c> getIDRequestMap(Set<ID> userIdSet) {

		// IDリクエストのリスト
		List<E_IDRequest__c> idrs = new List<E_IDRequest__c>();

		// DAO呼び出し
		idrs = E_IDRequestDaoWithout.getRecsByAgentUserId(userIdSet);

		// UserIdとIDリクエストのMap
		Map<ID, E_IDRequest__c> idrMap = new Map<ID, E_IDRequest__c>();
		for (E_IDRequest__c idr: idrs) {
			if (!idrMap.containsKey(idr.E_IDCPF__r.User__r.Id)) {
				idrMap.put(idr.E_IDCPF__r.User__r.Id, idr);
			}
		}

		return idrMap;
	}

	/**
	 * 募集人一覧(表示用)作成
	 */
//	private void getAgenctList(List<Contact> cons, Map<ID, User> uMap, Map<ID, UserLogin> ulMap, Map<ID, E_IDRequest__c> idrMap) {
	private void getAgenctList(List<Contact> cons, Map<ID, User> uMap, Map<ID, E_IDRequest__c> idrMap) {		//JIRA_#51 170207

		// 画面に表示するリスト
		agentRows = new List<AgentListRow>();

		/* JIRA_#51 170206 */
		/*
		for (Contact con : cons) {
			AgentListRow row = new AgentListRow();
			row.id = con.Id;
			row.name = con.Name;
			row.code = con.E_CL3PF_AGNTNUM__c;

			row.nameSort = con.E_CL3PF_ZEATKNAM__c;	//スプリントバックログ9(2017/1/18) 募集人カナ氏名

			if (!uMap.containsKey(con.Id)) {
				row.status = I_Const.NNLINKID_STATUS_UNISSUED;			//未発行
			} else if (idrMap.containskey(userMap.get(con.Id).Id) && idrMap.get(userMap.get(con.Id).Id).ZUPDFLAG__c == 'D') {
				row.status = I_Const.NNLINKID_STATUS_LONGTERMUNUSED;	//無効（長期未使用）
			} else if (isPassedDays(uMap.get(con.Id))) {
				row.status = I_Const.NNLINKID_STATUS_PWCHANGE;			//90日未ログイン　要パスワード変更
			} else if (ulMap.get(uMap.get(con.Id).Id).IsPasswordLocked) {
				row.status = I_Const.NNLINKID_STATUS_PWLOCK;			//パスワードロック
			} else if (uMap.get(con.Id).IsActive) {
				row.status = I_Const.NNLINKID_STATUS_ACTIVE;			//有効
			} else {
				row.status = I_Const.NNLINKID_STATUS_INACTIVE;			//無効
			}
			agentRows.add(row);
			if(agentRows.size() >= getListMaxRows()){
				break;
			}
		}
		*/

		//募集人情報マップ生成  キー:'|'なしの募集人コード  値:募集人コードに紐づく募集人レコードリスト
		Map<String, List<AgentListRow>> agentMap = new Map<String, List<AgentListRow>>();
		//マップで使用するkey('|'なしの募集人コード)
		String agentKey='';
		//'|'なしの募集人コードに紐づくレコードリスト
		List<AgentListRow> rowsList;
		//画面に表示する募集人レコード
		AgentListRow row;
		//募集人レコード追加フラグ trueの場合そのレコードを画面表示するフラグ
		Boolean addContactFlg;

		//JIRA_#251 170403
		Map<String,String> logMap = new Map<String,String>();

		for(AggregateResult result : logList){
			String accUserId = String.valueOf(result.get('AccessUser__c'));
			logMap.put(accuserId,'true');
		}

		Set<id> conid = new Set<id>(new Map<Id,Contact>(cons).keyset());

		List<E_AgencySalesResults__c> agSRList = E_AgencySalesResultsDaoWithout.getSRbyConId(conid);
		// key:代理店Id value:代理店挙績情報
		Map<Id, E_AgencySalesResults__c> srMap = new Map<Id, E_AgencySalesResults__c>();
		for(E_AgencySalesResults__c s : agSRList){
			srMap.put(s.Agent__c, s);
		}

		for (Contact con : cons) {
			//'|'より前の募集人コードをキーとする(5桁を想定する)
			agentKey = con.E_CL3PF_AGNTNUM__c.substringBefore('|');
			//募集人レコードからagentListRowの新規作成
			row = new AgentListRow();
			row.id = con.Id;
			row.name = con.Name;
			row.code = agentKey;
			row.nameSort = con.E_CL3PF_ZEATKNAM__c;	//スプリントバックログ9(2017/1/18) 募集人カナ氏名
			row.notPipeFlg = false;

			if(agentKey.equals(con.E_CL3PF_AGNTNUM__c)){	//募集人レコードの募集人コードが'|'なしの場合
				row.notPipeFlg = true;
			}
			if(srMap.containsKey(con.id)){
				E_AgencySalesResults__c sr = srMap.get(con.id);
				row.nbcnp_ytd = null2Zero(sr.NBCANP_YTD__c);
				row.nbcanp_spytd = null2Zero(sr.NBCANP_SpInsTypeYTD__c);
				row.ianp = null2Zero(sr.IANP__c);
				row.iqa = null2Zero(sr.IQA__c);
				row.totalYTD = null2Zero(sr.NBWANP_TotalYTD__c);
				row.fsYTD = null2Zero(sr.NBWANP_FSYTD__c);
				row.proYTD = null2Zero(sr.NBWANP_ProtectionYTD__c);
				row.agentCount = null2Zero(sr.ActivePolicyNumber__c);
			}else{
				row.nbcnp_ytd = 0;
				row.nbcanp_spytd = 0;
				row.ianp = 0;
				row.iqa = 0;
				row.totalYTD = 0;
				row.fsYTD = 0;
				row.proYTD = 0;
				row.agentCount = 0;
			}

			//AgentListRowリストの新規
			rowsList = new List<AgentListRow>();
			addContactFlg = false;

			//無効ユーザが紐づいている or EIDR種別='D'
//			if (uMap.containsKey(con.Id)
//					&& (!uMap.get(con.Id).IsActive
//						|| (idrMap.containskey(uMap.get(con.Id).Id) &&
//							idrMap.get(uMap.get(con.Id).Id).ZUPDFLAG__c.equals(E_Const.ZUPDFLAG_D))
//						)
//			) {
			if (uMap.containsKey(con.Id) //募集人レコードに紐づいている
					&& (!uMap.get(con.Id).IsActive //非アクティブ（無効）
						&& (idrMap.containskey(uMap.get(con.Id).Id) && //EIDRレコードを持っている
							idrMap.get(uMap.get(con.Id).Id).ZUPDFLAG__c.equals(E_Const.ZUPDFLAG_D)) //リクエスト種別がD
						)
			) {

				//JIRA #251
				if(logMap.containsKey(uMap.get(con.Id).Id)){
					row.haveLogs = true;
				}
				if(row.haveLogs){
					row.usagehistory = USAGEHISTORY_TRUE;
				}else{
					row.usagehistory = USAGEHISTORY_FALSE;
				}

				row.status = I_Const.NNLINKID_STATUS_UNUSED;			//長期未使用
				row.userid = uMap.get(con.Id).Id;						//UserのID
			//有効ユーザが紐づいている,ID管理.パスワードステータス='1'
			} else if (uMap.containskey(con.Id) && uMap.get(con.Id).IsActive
						&& !uMap.get(con.Id).E_IDCPFs__r.isEmpty() && uMap.get(con.Id).E_IDCPFs__r[0].ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)) {
						//旧パスワードステータス6も対象とするため、条件を変更
						//&& uMap.get(con.Id).E_IDCPFs__r[0].ZDSPFLAG02__c != null && uMap.get(con.Id).E_IDCPFs__r[0].ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_IRIS)) {
				//JIRA #251
				if(logMap.containsKey(uMap.get(con.Id).Id)){
					row.haveLogs = true;
				}
				if(row.haveLogs){
					row.usagehistory = USAGEHISTORY_TRUE;
				}else{
					row.usagehistory = USAGEHISTORY_FALSE;
				}

				if(isPassedDays(uMap.get(con.Id) )){//90日未ログイン
					row.status = I_Const.NNLINKID_STATUS_PWCHANGE;			//要パスワード変更
					row.userid = uMap.get(con.Id).Id;						//UserのID
				} else if(userLoginMap.containsKey(uMap.get(con.Id).Id)
						&& userLoginMap.get(uMap.get(con.Id).Id).IsPasswordLocked ){ //PWロックされている
					row.status = I_Const.NNLINKID_STATUS_PWLOCK;			//パスワードロック
					row.userid = uMap.get(con.Id).Id;						//UserのID
				} else {
					row.status = I_Const.NNLINKID_STATUS_ACTIVE;			//有効
					row.userid = uMap.get(con.Id).Id;						//UserのID
				}
			// 募集人に紐づくUserがない等
			} else {
				row.status = I_Const.NNLINKID_STATUS_NOPOLICY;			//保有なし
				row.userid = '';
			}
			//上記で設定したagentKeyでとってこられるListがある場合--Listへの更新・追加
			if(agentMap.containsKey(agentKey)){
				rowsList = agentMap.get(agentKey);
				for(AgentListRow grouprow : rowsList){
					//if(row.notPipeFlg){										//募集人レコードの募集人コードが'|'なしの場合
					//	//グループのContactIdに、'|'なしの募集人コードを持つContactIdを設定する →　契約者一覧への遷移用のID
					//	grouprow.id = row.id;

					//} else if(grouprow.notPipeFlg && !row.notPipeFlg) {		//グループに'|'なしの募集人コードを持つレコードがある場合
					//	//募集人レコードのContactIdに、グループのContactIdを設定する
					//	row.id = grouprow.id;
					//}

					//グループ内に有効ユーザレコードがない場合
					if( (!grouprow.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) && //有効
						!grouprow.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) && //要パスワード変更
						!grouprow.status.equals(I_Const.NNLINKID_STATUS_PWLOCK)) && //パスワードロック
						(row.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) || //現在行が有効
						 row.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) || //現在行が要パスワード変更
						 row.status.equals(I_Const.NNLINKID_STATUS_PWLOCK))//現在行がパスワードロック
					  ){
						//グループの募集人名・ステータスを募集人レコードに更新
						grouprow.id = row.id;
						grouprow.name = row.name;
						grouprow.nameSort = row.nameSort;
						grouprow.status = row.status;
						grouprow.userid = row.userid;
						//JIRA #251
						grouprow.haveLogs = row.haveLogs;
						grouprow.usagehistory = row.usagehistory;
					//グループ内に有効ユーザレコードがある場合 かつ 現在行の募集人レコードが有効
					} else if( (grouprow.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) || //有効
								grouprow.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) || //要パスワード変更
								grouprow.status.equals(I_Const.NNLINKID_STATUS_PWLOCK)) && //パスワードロック
							   (row.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) || //現在行が有効
							   	row.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) || //現在行が要パスワード変更
							   	row.status.equals(I_Const.NNLINKID_STATUS_PWLOCK))//現在行がパスワードロック
							){ //現在行がパスワードロック
						//募集人レコードをAgentListRowリストに追加
						addContactFlg = true;
					}
				}
			//ない場合--Listの追加('|'なしの募集人コードが重複している募集人でも、一度はこちらを通る)
			} else {
				//AgentListRowリストの新規追加,リストにAgentListRow追加
				addContactFlg = true;
			}

			if(addContactFlg){
				//リストにAgentListRow追加
				rowsList.add(row);
			}
			//マップのAgentListRowリスト追加/更新
			agentMap.put(agentKey, rowsList);
			//1001件以上をvisualforceに渡すとエラーになるためbreak
			if(agentMap.size() == getListMaxRows()){
				break;
			}
		}


		if(agentMap != null && !agentMap.isEmpty()){
			for(List<AgentListRow> rowList : agentMap.values()){
				//表示用リストにマップのAgentListRowリストを入力
				agentRows.addAll(rowList);
			}
		}
		/* 改修ここまで */

		// 募集人が0の場合、メッセージを表示
		if (agentRows.size() <= 0) {
			pageMessages.add(getMSG().get('AGT|001'));
		}
		/* 挙積対応 取得したContactが1000件ならエラーとする
		// 募集人が1000件の場合、メッセージを表示
		if(agentRows.size() >= getListMaxRows()){
			pageMessages.add(getMSG().get('LIS|004'));
		} */
		// 募集人が1000件の場合、メッセージを表示
		if(cons.size() >= getListMaxRows()){
			pageMessages.add(getMSG().get('LIS|004'));
		}

		//JIRA_#251 170403
		//Map<Id,AgentListRow> agentLogMap = new Map<Id,AgentListRow>();
		//List<String> accessUserId = new List<String>();
		//for(AgentListRow agent :agentRows){
		//	if(agent.userid != ""){
		//		agentLogMap.add(agent.userid,agent);
		//		accessUserId.add(agent.userid);
		//	}
		//}

		//List<E_Log__c> logs = E_LogDao.getRecAccessLogList(accessUserId);

		//for(E_Log__c log :logs){

		//}

		I_AgentController.list_sortType  = SORT_TYPE_AGENT;
		I_AgentController.list_sortIsAsc = true;

		agentRows.sort();
		return;
	}

	/**
	 * N日未ログインチェックのメソッド(90日を想定)
	 * @param userRec ユーザレコード
	 * @return Boolean true:N日以上経っている、false:経っていない
	 */
	private Boolean isPassedDays(User userRec){
		Integer criteriaDays = 1000;
		try{
			criteriaDays = Integer.valueOf(System.Label.E_LASTLOGINDATE_LAPSEDDAYS);
		}catch(Exception e){
			//デフォルト1000日にしておく
		}

		//クライテリアの日数以上の時にTrue
		return E_Util.days_after_lastLogin(userRec) > criteriaDays;
	}

	/**
	 * ソート
	 */
	public void sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if (sortType != sType) {
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		I_AgentController.list_sortType  = sortType;
		I_AgentController.list_sortIsAsc = sortIsAsc;

		agentRows.sort();

		return;
	}

	/**
	 * 代理店一覧へ遷移
	 */
	public PageReference redirectAgencyPage() {
		PageReference pr = Page.IRIS_Agency;
		pr.setRedirect(true);
		return pr;
	}

	/**
	 * 契約者一覧へ遷移
	 */
	public PageReference moveToOwnerList() {
		// ID
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);

		// Name
		String name = E_ContactDaoWithout.getRecById(id).Name;
		// BreadCrumb
		List<String> breadCrumb = new List<String>();
		breadCrumb.add(agency);
		breadCrumb.add(office);
		breadCrumb.add(name);

		// Field
		String field = I_Const.CONDITION_FIELD_AGENT;

		iris.setPolicyCondition(field, name, id, String.join(breadCrumb, '|'));

		PageReference pr = Page.IRIS_Owner;
		pr.getParameters().put(I_Const.URL_PARAM_MENUID, iris.irisMenus.menuItemsByKey.get('ownerList').record.Id);
		pr.setRedirect(true);

		return pr;
	}
	/**
	 * 新契約提案へ遷移
	 */
	public PageReference moveToNewPolicyList() {
		// ID
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);

		// Name
		String name = E_ContactDaoWithout.getRecById(id).Name;
		// BreadCrumb
		List<String> breadCrumb = new List<String>();
		breadCrumb.add(agency);
		breadCrumb.add(office);
		breadCrumb.add(name);

		// Field
		String field = I_Const.CONDITION_FIELD_AGENT;

		iris.setPolicyCondition(field, name, id, String.join(breadCrumb, '|'));

		PageReference pr = Page.IRIS_NewPolicy;
		pr.setRedirect(true);

		return pr;
	}
	/**
	 * 募集人毎利用履歴へ遷移
	 */
	public PageReference moveToLogView() {
		// 募集人ID
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);
		// UserのID
		String userid = ApexPages.currentPage().getParameters().get('userid');

		// Name
		String name = E_ContactDaoWithout.getRecById(id).Name;
		// BreadCrumb
		List<String> breadCrumb = new List<String>();
		breadCrumb.add(agency);
		breadCrumb.add(office);
		breadCrumb.add(name);

		// Field
		String field = I_Const.CONDITION_FIELD_AGENT;

		iris.setPolicyCondition(field, name, id, String.join(breadCrumb, '|'));

		PageReference pr = Page.IRIS_LogView;
		pr.getParameters().put('id',userid);
		pr.setRedirect(true);

		return pr;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > getListMaxRows()){
			rowCount = getListMaxRows();
		}
		return null;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return agentRows == null ? 0 : agentRows.size();
	}


	/**
	 * 内部クラスで表示用リストを作成
	 */
	public class AgentListRow implements Comparable {
		public String id {get; set;}
		public String name {get; set;}
		public String code {get; set;}
		public String status {get; set;}
		public String userid {get; set;}


		//スプリントバックログ9(2017/1/18) カナソート用変数
		public String nameSort{get; set;}

		//JIRA_#51 170207 '|'なしの募集人コードが設定されているレコードフラグ
		public Boolean notPipeFlg {get;set;}

		public Boolean haveLogs {get;set;}
		public String usagehistory {get;set;}

		//挙績情報（AG）
		// 係数ANP(YTD)
		public Decimal nbcnp_ytd {get; set;}
		// 係数ANP（特定）(YTD)
		public Decimal nbcanp_spytd {get; set;}
		// 保有ANP(YTD)
		public Decimal ianp {get; set;}
		// IQA継続率＊ (YTD)
		public Decimal iqa {get; set;}

		//挙績情報（MR）
		// Total WAPE(YTD)
		public Decimal totalYTD {get; set;}
		// FS WAPE(YTD)
		public Decimal fsYTD {get; set;}
		// Protection WAPE(YTD)
		public Decimal proYTD {get; set;}
		// 稼動募集人数
		public Decimal agentCount {get; set;}

		// コンストラクタ
		public AgentListRow() {
			id = '';
			name = '';
			code = '';
			status = '';
			userid = '';

			//スプリントバックログ9(2017/1/18) カナソート用変数
			nameSort = '';

			//JIRA_#51 170207 '|'なしの募集人コードが設定されているレコードフラグ
			notPipeFlg = false;

			haveLogs = false;
			usagehistory = '';
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			AgentListRow compareToRow = (AgentListRow)compareTo;
			Integer result = 0;

			// 募集人名
			if (list_sortType == SORT_TYPE_AGENT) {
				//スプリントバックログ9(2017/1/18) カナソート修正
				result = I_Util.compareToString(nameSort, compareToRow.nameSort);
				//result = I_Util.compareToString(name, compareToRow.name);
			// 募集人コード
			} else if(list_sortType == SORT_TYPE_CODE){
				result = I_Util.compareToString(code, compareToRow.code);
			// NN Link ID
			} else if(list_sortType == SORT_TYPE_IDSTATUS){
				result = I_Util.compareToString(status, compareToRow.status);
			//過去1ヵ月のIRIS使用歴
			}else if(list_sortTYpe == SORT_TYPE_USAGEHISTORY){
				result = I_Util.compareToString(usagehistory, compareToRow.usagehistory);
			// 係数ANP(YTD)
			} else if (list_sortType == SORT_TYPE_NBCNPYTD) {
				result = compareToDecimal(nbcnp_ytd, compareToRow.nbcnp_ytd);
			// 係数ANP（特定）(YTD)
			} else if (list_sortType == SORT_TYPE_NBCANPSPYTD) {
				result = compareToDecimal(nbcanp_spytd, compareToRow.nbcanp_spytd);
			// 保有ANP(YTD)
			} else if (list_sortType == SORT_TYPE_IANP) {
				result = compareToDecimal(ianp, compareToRow.ianp);
			// IQA継続率＊ (YTD)
			} else if (list_sortType == SORT_TYPE_IQA) {
				result = compareToDecimal(iqa, compareToRow.iqa);
			// Total WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_TOTALYTD) {
				result = compareToDecimal(totalYTD, compareToRow.totalYTD);
			// FS WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_FSYTD) {
				result = compareToDecimal(fsYTD, compareToRow.fsYTD);
			// Protection WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_PROYTD) {
				result = compareToDecimal(proYTD, compareToRow.proYTD);
			// 稼動件数(月平均)
			} else if (list_sortType == SORT_TYPE_AGENTCOUNT) {
				result = compareToDecimal(agentCount, compareToRow.agentCount);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

		/**
	 * ソート判定（Decimal）
	 * @paarm  Decimal：比較元
	 * @paarm  Decimal：比較先
	 * @return Decimal:比較結果
	 */
	public static Integer compareToDecimal(Decimal compare, Decimal compareTo){
		Integer result = 0;
		if(compare==null) return 1;
		if(compareTo==null) return -1;
		if(compare == compareTo){
			result = 0;
		} else if(compare > compareTo){
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

}