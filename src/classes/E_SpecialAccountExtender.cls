global with sharing class E_SpecialAccountExtender extends E_AbstractViewExtender {
	E_SpecialAccountController extension;

    /** ページタイトル */
    private static final String PAGE_TITLE = '特別勘定';
	/** 繰入比率 合計 */
	private Decimal sumZINVPERC = 0;
	/** 組立金残高（円） 合計 */
	private Decimal sumZREVAMT = 0;
	/** 残高の割合 合計 */
	private Decimal sumZHLDPERC = 0;
    /**遷移元個別照会画面の判定に使用*/
    public String isType { get; private set; }
    /**遷移元画面の判定に使用*/
    public String isOriginType { get; private set; }

    /* コンストラクタ */
    public E_SpecialAccountExtender(E_SpecialAccountController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        
		this.isType = ApexPages.CurrentPage().getParameters().get('type');
		this.isOriginType = E_Const.FROM_SPECIALACCOUNT;
		

        // 変額保険ファンドの検索条件を追加
        SkyEditor2.Query tableQuery1 = extension.queryMap.get('dataTableSVFPF');
        tableQuery1.addWhereIfNotFirst('AND');
        // 【特別勘定】.[設定日]<=システム日付 のみExtenderで拡張
        tableQuery1.addWhere(' ( E_SVCPF__r.CURRFROM__c <= \'' + DateTime.now().format('yyyyMMdd') + '\')');
        extension.queryMap.put('dataTableSVFPF',tableQuery1);
    }

    /* init */
    global override void init(){
        // アクセスチェック
        pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
    }

    /* PageAction */
    public PageReference pageAction () {
        // 合計値の設定
        calcTotalRow();
        // 権限チェック
        return E_Util.toErrorPage(pageRef, null);
    }

    /*
     * [繰入比率の変更 申込]、[繰入比率の変更 ご利用方法]を表示するかどうかを判定します
     * ※SVEからコール
     */
    public boolean getIsDispFundRatioLink(){
        if(extension.record.COMM_ZINVDCF__c && getIsTrCodeFundRatio()){
            return true;
        }
        return false;
    }

    /*
     * [積立金の変更 申込]、[積立金の変更 ご利用方法]
     * ※SVEからコール
     */
    public boolean getIsDispFundTransferLink(){
        if(extension.record.COMM_ZTFRDCF__c && getIsTrCodeFundTransfer()){
            return true;
        }
        return false;
    }

    /*
     * 繰入比率の合計を返却します
     * ※SVEからコール
     */
    public String getSumZi(){
        return sumZINVPERC.format();
    }

    /*
     * 組立金残高（円）の合計を返却します
     * ※SVEからコール
     */
    public String getSumZr(){
        return sumZREVAMT.format();
    }

    /*
     * 繰入比率の合計を返却します
     * ※SVEからコール
     */
    public Decimal getSumZh(){
        return sumZHLDPERC;
    }

    /**
     *  変額保険ファンドの合計値を算出する
     */
    private void calcTotalRow(){
        List<E_SpecialAccountController.dataTableSVFPFItem> items = extension.dataTableSVFPF.items;
        for(E_SpecialAccountController.dataTableSVFPFItem item :items ){
            E_SVFPF__c obj = (E_SVFPF__c)item.record;
            // 繰入比率
            sumZINVPERC += obj.ZINVPERC__c != null? obj.ZINVPERC__c : 0;
            // 組立金残高（円）
            sumZREVAMT += obj.ZREVAMT__c != null? obj.ZREVAMT__c : 0;
            // 残高の割合
            sumZHLDPERC += obj.ZHLDPERC__c != null? obj.ZHLDPERC__c : 0;
        }
    }
    
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		PageReference pageRef = Page.E_Coli;
		pageRef.getParameters().put('id', extension.record.id);
		return pageRef;
	}
}