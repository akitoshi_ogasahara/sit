@isTest
private class TestE_AgencySalesResultsDaoWithout {
	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static E_AgencySalesResults__c asr;
	private static E_SpRelationCorp__c spRelationCorps;

	@isTest static void getSpRelationCorpTest() {
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String BUSINESS_DATE = '20000101';

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH',
			QualifSim__c = '優績S'
		);
		insert asr;

		// 特定関係法人
		spRelationCorps = new E_SpRelationCorp__c(
			SpRelationCorpName__c='テスト特定関係法人',
			E_AgencySalesResults__c=asr.Id,
			BusinessDate__c = BUSINESS_DATE
		);
		insert spRelationCorps;

		String agency = distribute.Id;
		String bizDate = asr.BusinessDate__c;
		// テスト
		Test.startTest();
		List<E_AgencySalesResults__c> rec = E_AgencySalesResultsDaoWithout.getSpRelationCorp(agency, bizDate);
		System.assertEquals(asr.Id, rec.get(0).Id);
		Test.stopTest();
	}
	
	@isTest static void getAHRecByAccIdTest() {
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String BUSINESS_DATE = '20000101';

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH',
			QualifSim__c = '優績S'
		);
		insert asr;

		String aid = distribute.Id;
		String bizDate = asr.BusinessDate__c;
		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDaoWithout.getAHRecByAccId(aid, bizDate);
		System.assertEquals(asr.Id, rec.Id);
		Test.stopTest();
		
	}
	@isTest static void getSRAgentRecsTest() {
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String BUSINESS_DATE = '20000101';

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH',
			QualifSim__c = '優績S'
		);
		insert asr;
		String soqlWhere = 'QualifSim__c =  \'優績S\' ';

		// テスト
		Test.startTest();
		List<E_AgencySalesResults__c> rec = E_AgencySalesResultsDaoWithout.getSRAgentRecs(soqlWhere);
		System.assertEquals(asr.Id, rec.get(0).Id);
		Test.stopTest();
	}
}