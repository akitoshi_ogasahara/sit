public with sharing class MNT_ExportSalesToolMainteController extends E_CSVExportController{

	/**
	 * Object名のセット
	 */
	public override String mainSObjectAPIName(){
		return Schema.SObjectType.I_ContentMaster__c.Name;
	}
	
	/**
	 *	Constructor
	 */
	public MNT_ExportSalesToolMainteController(){
		super();
		
		// 項目の追加
		addColumn(new E_CSVColumnDescribe.BaseField('Name','資料名'));	// 資料名	Name(String)
		addColumn(new E_CSVColumnDescribe.BaseField('DocumentNo__c'));	// 資料管理番号	DocumentNo__c(String)
		addColumn(new E_CSVColumnDescribe.BaseField('Content__c'));	// 内容	Content__c(LongTextarea)
		addColumn(new E_CSVColumnDescribe.BaseField('ApprovalNo__c'));	// 承認番号	ApprovalNo__c(String)
		addColumn(new E_CSVColumnDescribe.DateField('DisplayFrom__c'));	// 掲示開始	DisplayFrom__c(Date)
		addColumn(new E_CSVColumnDescribe.DateField('ValidTo__c'));	// 有効期限	ValidTo__c(Date)
		addColumn(new E_CSVColumnDescribe.BaseField('Category__c'));	// カテゴリ	Category__c(Picklists)
		addColumn(new E_CSVColumnDescribe.BaseField('Management__c'));	// 管理項目	Management__c(Picklists)
		addColumn(new E_CSVColumnDescribe.BaseField('FileTargetCSV__c', 'ファイル対象'));	// ファイル対象（CSV用）	FileTargetCSV__c(String)
	}
}