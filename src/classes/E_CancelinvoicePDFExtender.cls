global with sharing class E_CancelinvoicePDFExtender extends E_AbstractViewExtender {
    private static final String PAGE_TITLE = '当社宛送付状';

    E_CancelinvoicePDFController extension;
  
    /* コンストラクタ */
    public E_CancelinvoicePDFExtender(E_CancelinvoicePDFController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }

    /** init */
    global override void init(){
        // アクセスチェック
        pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);

    }

    public PageReference pageAction () {
        return E_Util.toErrorPage(pageRef, null);
    }
}