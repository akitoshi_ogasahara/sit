@isTest
private class TestI_InformationNewsController
{

	//CMSコンテンツが「お知らせ」に紐づいていない場合
	static testMethod void getInfoNewsTest1(){

		//IRISメニュー
		I_MenuMaster__c iMenu = new I_MenuMaster__c();
		iMenu.Name = 'テスト用';
		insert iMenu;

		//IRISページ
		I_PageMaster__c iPage = new I_PageMaster__c();
		iPage.Name = 'IRISPAge';
		iPage.Menu__c = iMenu.id;
		insert iPage;

		//IRIS　CMSコンテンツ
		I_ContentMaster__c st = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert st;
		I_ContentMaster__c iCms = new I_ContentMaster__c();
		iCms.Name = 'テスト用';
		iCms.Page__c = iPage.id;
		iCms.DateStr__c='20000101';
		insert iCms;

		Test.startTest();
			I_InformationNewsController iNewscon = new I_InformationNewsController();
			I_ContentMaster__c icon = new I_ContentMaster__c();
			icon = iNewscon.getInfoNews();
			System.assertEquals(icon.Id, st.Id);
			System.assertEquals(icon.ChildContents__r.size(), 0);
		Test.stopTest();
	}


	//CMSコンテンツが「お知らせ」に紐づいている場合
	static testMethod void getInfoNewsTest2(){

		//IRISメニュー
		I_MenuMaster__c iMenu = new I_MenuMaster__c();
		iMenu.Name = I_Const.IRIS_CONTENTS_NAME_INFO;
		insert iMenu;

		//IRISページ
		I_PageMaster__c iPage = new I_PageMaster__c();
		iPage.Name = 'IRISPAge';
		iPage.Menu__c = iMenu.id;
		insert iPage;

		//IRIS　CMSコンテンツ
		I_ContentMaster__c st = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert st;
		I_ContentMaster__c iCms = new I_ContentMaster__c();
		iCms.Name = I_Const.IRIS_CONTENTS_NAME_INFO;
		iCms.Page__c = iPage.id;
		iCms.ParentContent__c = st.Id;
		iCms.DateStr__c='20000101';
		insert iCms;


		Test.startTest();
			I_InformationNewsController iNewscon = new I_InformationNewsController();
			I_ContentMaster__c icon = new I_ContentMaster__c();
			icon = iNewscon.getInfoNews();
			System.assertEquals(icon.Id, st.Id);
			System.assertEquals(icon.ChildContents__r.size(), 1);
			System.assertEquals(icon.ChildContents__r[0].Name,iCms.Name);
			System.assertEquals(icon.ChildContents__r[0].DateStr__c,iCms.DateStr__c);
		Test.stopTest();
	}

}