@isTest
private class TestASS_OfficeSummaryController {

	//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}

	//事務所作成
	private static Account createOffice(User mr) {
		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		return office;
	}

	//FS見込案件作成
	private static void createOpp(Account office, User mr) {
		//商品マスタ作成
		ProductMaster__c fsprod = new ProductMaster__c(Name = 'Sample_FS', LargeClassification__c = 'CategoryⅢ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert fsprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = office.Id, OwnerId = mr.Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = office.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;
	}

	//行動作成
	private static void createEvent(Account office, User mr, String recTypeId) {
		List<Event> eventList = new List<Event>();

		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = recTypeId));
		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = recTypeId));
		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test3', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = recTypeId));
		insert eventList;
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（MRページから）
	 * エリア部長の場合の初期表示
	 */
	private static testMethod void areaManagerTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//事務所作成
		Account office = createOffice(mr);

		//見込案件作成
		createOpp(office,mr);

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		//行動作成
		createEvent(office, mr,eventRecType.Id);

		//営業目標
		SalesTarget__c st = new SalesTarget__c(Account__c = office.Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3);
		insert st;

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			con.getNotes();
			con.getOffice();
			con.getOppList();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.currentEvents.size(),3);
		}
	
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MRページから）
	 * MR場合の画面と更新処理
	 */
	private static testMethod void mrTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);

		//見込案件作成
		createOpp(office,actUser);

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		//行動作成
		createEvent(office, actUser,eventRecType.Id);

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			con.getNotes();
			con.getOffice();
			con.getOppList();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.currentEvents.size(),3);
		}
	
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MRページから）
	 * MR場合の直近と過去の行動の表示件数変更処理
	 */
	private static testMethod void meChangeEventListTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);


		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		//直近の行動を作成
		for(Integer i; i<20; i++){
			Event cuEv = new Event(Subject = 'test'+i, WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id);
			eventList.add(cuEv);
		}
		//過去の行動を作成
		for(Integer i; i<20; i++){
			Event olEv = new Event(Subject = 'test'+i, WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id);
			eventList.add(olEv);
		}
		insert eventList;

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			con.getOffice();
			//直近全件表示
			con.cuAddRows();
			//直近10件表示
			con.cuReturnRows();
			//過去全件表示
			con.olAddRows();
			//過去10件表示
			con.olReturnRows();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.cuRowCount,10);
			System.assertEquals(con.olRowCount,10);
		}
	
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MRページから）
	 * Memo更新、削除、新規作成処理
	 */
	private static testMethod void mrCreateMemoTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);


		//営業日報メモ
		ASS_Memo__c memo = new ASS_Memo__c(Name = 'test', Account__c = office.Id, Contents__c = 'テスト');
		insert memo;

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			con.getNotes();
			con.getOffice();
			//Memo編集
			con.editNote();
			//Memo編集キャンセル
			con.cancel();
			//Memo削除
			con.deleteNote();

			//Memo新規作成
			con.newnote();
			//Memo保存
			con.save();

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.memoList.size(),1);
		}
	
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MRページから）
	 * 戻る処理
	 */
	private static testMethod void mrBackPageTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);


		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			con.back();

//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_MRPage;
			newPage.getParameters().put('mrId',actUser.Id);
			newPage.getParameters().put('unitId',unit.Id);
			newPage.setRedirect(true);

			System.assertEquals(con.back().getUrl(),newPage.getUrl());
		}
	
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MRページから）
	 * 新規行動作成画面遷移処理
	 */
	private static testMethod void mrCreateEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);


		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			con.newEvent();

//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_EventInputPage;
			newPage.getParameters().put('mrId',actUser.Id);
			newPage.getParameters().put('unitId',unit.Id);
			newPage.getParameters().put('retPgName','/apex/ASS_OfficeSummary');
			newPage.getParameters().put('accountId',office.Id);
			newPage.setRedirect(true);

			System.assertEquals(con.newEvent().getUrl(),newPage.getUrl());
		}
	}


	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MRページから）
	 * 行動参照画面遷移処理
	 */
	private static testMethod void mrViewEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		Event ev = new Event(OwnerId = actUser.Id,Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id);

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',actUser.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_OfficeSummaryController con = new ASS_OfficeSummaryController();
			pr.getParameters().put('eventId',ev.Id);
			Test.setCurrentPage(pr);
			con.viewEvent();

//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_EventInputPage;
			newPage.getParameters().put('mrId',actUser.Id);
			newPage.getParameters().put('unitId',unit.Id);
			newPage.getParameters().put('retPgName','/apex/ASS_OfficeSummary');
			newPage.getParameters().put('accountId',office.Id);
			newPage.getParameters().put('eventId',ev.Id);
			newPage.setRedirect(true);

			System.assertEquals(con.viewEvent().getUrl(),newPage.getUrl());
		}
	
	}
}