public with sharing class I_MenuMasterDao {

	/**
	 *		Idから1件メニューレコードを取得
	 */
	public static I_MenuMaster__c getRecordById(Id menuId) {
		for(I_MenuMaster__c menu:[SELECT
										Name,
										MainCategory__c,
										MainCategoryAlias__c,
										MainCategoryStyleClass__c,
										SubCategory__c,
										ParentMenu__c,
										Style__c,
										MenuKey__c,
										LinkURL__c,
										Default__c,
										CanFiltered__c,
										DestURL__c,
										ParentMenu__r.Name
									FROM
										I_MenuMaster__c
									WHERE
										Id = :menuId]){
			return menu;
		}
		return null;
	}


	/**
	 *		以下のソート順でソートされた全メニューレコードの取得
	 			1.カテゴリ　昇順
	 			2.デフォルトフラグ　（チェック有⇒無）
	 			3.並び順　昇順
	 */
	public static List<I_MenuMaster__c> getSortedRecords(){
		return [SELECT Id,Name,
					MainCategory__c,
					MainCategoryAlias__c,
					SubCategory__c,
					ParentMenu__c,
					Style__c,
					MenuKey__c,
					LinkURL__c,
					IRISPageName__c,
					DestUrl__c,
					UrlParameters__c,
					requiredDefaultPageId__c,
					WindowTarget__c,
					Default__c,
					Required_ZIDTYPE__c,
					RequiredPermissionSet__c,
					Required_EIDC_Flag__c,
					ShowOnTop__c,
					CanFiltered__c,
					ParentMenu__r.Name,
					UpsertKey__c,	//20180214 資料発送申込対応
					(select id,name
						from Pages__r
						order by default__c desc		//true → false
								, Name 					//　通常はデフォル＝Trueの1件を取得する
						limit 1)
				FROM
					I_MenuMaster__c
				ORDER BY
					MainCategory__c ASC
					, SubCategory__c ASC
						//, Default__c DESC		//True　⇒　False の順			並び順にデフォルトフラグは含めない→ロジックでデフォルトフラグを参照して設定する
					, DisplayOrder__c ASC
		];
	}

	public static List<I_MenuMaster__c>  getRecordsByLinkMenuKey(String linkMenuKey) {
		if(String.isBlank(linkMenuKey)){
			return null;
		}

		linkMenuKey = linkMenuKey.replaceAll('%', '\\\\%');
		linkMenuKey = linkMenuKey.replaceAll('_', '\\\\_');
		linkMenuKey = '%' + linkMenuKey + '%';

		return [
				SELECT
					Name,
					MainCategory__c,
					MainCategoryAlias__c,
					MainCategoryStyleClass__c,
					SubCategory__c,
					ParentMenu__c,
					Style__c,
					MenuKey__c,
					LinkURL__c,
					Default__c,
					CanFiltered__c,
					ParentMenu__r.Name
				FROM
					I_MenuMaster__c
				WHERE
					MenuKey__c LIKE: linkMenuKey
				ORDER BY
					DisplayOrder__c ASC
		];
	}

	/**
	 * メニュー名からIRISメニューレコードを1件返す.
	 *
	 * @param String メニュー名
	 * @return I_MenuMaster__c IRISメニューレコード
	 */
	public static I_MenuMaster__c getRecByName(String name) {
		I_MenuMaster__c res = null;

		List<I_MenuMaster__c> recs =
			[
				Select
					Id,
					Name,
					(
						Select
							Id,
							Name,
							Default__c
						From
							Pages__r
					)
				From
					I_MenuMaster__c
				Where
					Name =: name
			];

		if(recs.size() > 0) {
			res = recs.get(0);
		}

		return res;
	}

	/**
	 * メニュー名からIRISメニューレコードを1件返す.
	 *
	 * @param String メニュー名
	 * @return I_MenuMaster__c IRISメニューレコード
	 */
	public static I_MenuMaster__c getRecByNameAndSubCategory(String name, String subcategory) {
		I_MenuMaster__c res = null;

		List<I_MenuMaster__c> recs =
			[
				Select
					Id,
					Name,
					(
						Select
							Id,
							Name,
							Default__c
						From
							Pages__r
					)
				From
					I_MenuMaster__c
				Where
					Name =: name
				And
					SubCategory__c = :subcategory
			];

		if(recs.size() > 0) {
			res = recs.get(0);
		}

		return res;
	}
}