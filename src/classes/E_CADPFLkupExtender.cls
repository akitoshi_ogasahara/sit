global with sharing class E_CADPFLkupExtender extends E_AbstractSearchExtender{
    private static final String PAGE_TITLE = '住所検索';
    
    E_CADPFLkupController extension;
    String ZPOSTCD;
    String ZPOSTMARK;
    integer ZPOSTMARKLength = 0;
    
    public String getErrorMessage0(){
        return getMSG().get('CPL|003');
    }
    
    /* コンストラクタ */
    public E_CADPFLkupExtender(E_CADPFLkupController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        
        // パラメータから住所管理IDを取得
        String cadpfId = Apexpages.currentPage().getParameters().get('cadpfid');
        //if(cadpfId != null){
        if(String.isNotBlank(cadpfId)){
            E_CADPF__c cadpf = E_CADPFDao.getRecById(cadpfId);
            extension.ZPOSTCD.SkyEditor2__Text__C = cadpf.ZPOSTCD__c;
            
        }else{
        	extension.execInitialSearch = false;        //初期検索を実施しない
        }
system.debug('************************E_CADPFLkupExtender [check1]' + extension.ZPOSTCD.SkyEditor2__Text__C);
    }
    
    /* init */
    global override void init(){
        
    }
    
    /*郵便番号のエラーチェックを行う*/
    global override void preSearch() {
        
system.debug('************************E_CADPFLkupExtender [check2]' + extension.ZPOSTCD.SkyEditor2__Text__C);
        
        if(extension != null){
            ZPOSTCD = extension.ZPOSTCD.SkyEditor2__Text__C;
            ZPOSTMARK = extension.ZPOSTMARK.SkyEditor2__Text__C;
            
            //ワイルドカードは許容するため、%を文字列から削除する。空白も許容するため、空白チェックを行う。
            if(String.isnotBlank(ZPOSTMARK)){
                ZPOSTMARK = ZPOSTMARK.replaceAll('\\*','%');//ワイルドカードとして*が使えないので、*を%に変換する
                extension.ZPOSTMARK.SkyEditor2__Text__C = ZPOSTMARK;
                ZPOSTMARKLength = ZPOSTMARK.length();
                ZPOSTMARK = ZPOSTMARK.replaceAll('%','');//ワイルドカードは許容するため、%を文字列から削除し、エラー処理に引っかからないようにする
            }

			//必須チェックを実施	エラーの場合に検索を実行しないようExceptionはハンドリングしない
//            try{
                validationCheck();
//            }catch(SkyEditor2.ExtenderException e){
//                e.setMessagesToPage();
//           }
            
        }
    }
    
    /*レコード件数のチェック。0件の場合エラーメッセージを表示*/
    global override void afterSearch() {
        if(extension != null){
            integer listSize = extension.resultTable.items.size();
            String ZPOSTMARK = extension.ZPOSTMARK.SkyEditor2__Text__C;
            integer ZPOSTMARKLength = 0;
            if(String.isnotBlank(ZPOSTMARK)){
                ZPOSTMARK = ZPOSTMARK.replaceAll('%','\\*');//*が%に変換されているため、*に戻す。このせいで、最初から%をワイルドカードとしていた場合でも、*になってしまう。要検討
                extension.ZPOSTMARK.SkyEditor2__Text__C = ZPOSTMARK;
            }
        }
    }
    
    private void validationCheck(){
        //郵便番号3桁のエラーチェック
        if(String.isnotBlank(ZPOSTCD) && !E_Util.isEnNum(ZPOSTCD)){//数字以外が含まれているかチェック      
            extension.resultTable.items.clear();
            throw new SkyEditor2.ExtenderException(getMSG().get('CPL|001'));    	
        }else if(String.isBlank(ZPOSTCD) || ZPOSTCD.length()!= 3){//桁数のチェック  
            extension.resultTable.items.clear();
            throw new SkyEditor2.ExtenderException(getMSG().get('CPL|002'));    	
        }
        
        //郵便番号4桁のエラーチェック
        if(String.isnotBlank(ZPOSTMARK) && !E_Util.isEnNum(ZPOSTMARK)){//数字以外が含まれているかチェック。空白は許容する。
            extension.resultTable.items.clear();
            throw new SkyEditor2.ExtenderException(getMSG().get('CPL|001'));    	
        }else if(ZPOSTMARKLength > 4){//桁数のチェック
            extension.resultTable.items.clear();
            throw new SkyEditor2.ExtenderException(getMSG().get('CPL|002'));    	
        }
    }
    
}