/**
 *
 */
@isTest
private class TestI_AbstractPushOutBatchOperation {

	//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];	//Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Contact atCon;
	private static E_BizDataSyncLog__c log;

	// テスト用の継承クラス
	public class Operation extends I_AbstractPushOutBatchOperation{
		public override List<I_PushOut__c> execute(List<Sobject> records){
			return records;
		}

		public void setQueryCondition(String condition){
			queryCondition = condition;
		}

		public I_PushOut__c createRecordByIdcpf_ext(E_IDCPF__c idcpf, String noticeType, Boolean isEmployee, String formatType){
			return createRecordByIdcpf(idcpf, noticeType, isEmployee, formatType);
		}

		//public I_PushOut__c createRecordByUser_ext(User ou, String noticeType, Boolean isEmployee, String formatType){
		//	return createRecordByUser(ou, noticeType, isEmployee, formatType);
		//}

		public Set<E_IDCPF__c> getAgentIdcpfs_ext(Set<String> conIds){
			return getAgentIdcpfs(conIds);
		}

		public Set<E_IDCPF__c> getGroupIdcpfs_ext(Set<String> grpCode){
			return getGroupIdcpfs(grpCode);
		}

		public Set<E_IDCPF__c> getEmployeeIdcpfs_ext(){
			return getEmployeeIdcpfs();
		}
	}

	/**
	 * Database.QueryLocator getQueryLocator()
	 */
	static testMethod void getQueryLocator_test01() {
		Database.QueryLocator result;

		Test.startTest();
		Operation operation = new Operation();
		operation.pType = I_Const.EBIZDATASYNC_KBN_NCOL;
		result = operation.getQueryLocator();
		Test.stopTest();

		// 比較用
    	String soql = '';
    	String queryCondition = '';
    	String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(I_Const.EBIZDATASYNC_KBN_NCOL);
    	Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(I_Const.EBIZDATASYNC_KBN_NCOL);
    	if(columns.isEmpty() == false){
	    	soql = ' Select Id';
	    	for(String col :columns){
	    		soql += ', ' + col;
	    	}
	    	soql += ' From ' + objApi;
	    	if(String.isNotBlank(queryCondition)){
	    		soql += queryCondition;
	    	}
    	}
		Database.QueryLocator queryLocater = Database.getQueryLocator(soql);

		// assertion
		System.assertEquals(String.valueOf(queryLocater), String.valueOf(result));
    }

	/**
	 * Database.QueryLocator getQueryLocator()
	 */
	static testMethod void getQueryLocator_test02() {
		Database.QueryLocator result;

		// Where
		String addCondition = ' Where Id != null ';

		Test.startTest();
		Operation operation = new Operation();
		operation.pType = I_Const.EBIZDATASYNC_KBN_NCOL;
		operation.setQueryCondition(addCondition);
		result = operation.getQueryLocator();
		Test.stopTest();

		// 比較用
    	String soql = '';
    	String queryCondition = addCondition;
    	String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(I_Const.EBIZDATASYNC_KBN_NCOL);
    	Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(I_Const.EBIZDATASYNC_KBN_NCOL);
    	if(columns.isEmpty() == false){
	    	soql = ' Select Id';
	    	for(String col :columns){
	    		soql += ', ' + col;
	    	}
	    	soql += ' From ' + objApi;
	    	if(String.isNotBlank(queryCondition)){
	    		soql += queryCondition;
	    	}
    	}
		Database.QueryLocator queryLocater = Database.getQueryLocator(soql);

		// assertion
		System.assertEquals(String.valueOf(queryLocater), String.valueOf(result));
    }

	/**
	 * List<I_PushOut__c> execute(List<Sobject> records)
	 */
	static testMethod void execute_test01() {
		// TestData
		List<I_PushOut__c> recs = createPushOuts();

		Test.startTest();
		Operation operation = new Operation();
		operation.pType = I_Const.EBIZDATASYNC_KBN_NCOL;
		List<I_PushOut__c> result = operation.execute(recs);
		operation.finish(result);
		Test.stopTest();

		// assertion
		System.assertEquals(recs.size(), result.size());
	}

	/**
	 * I_PushOut__c createRecordByIdcpf(E_IDCPF__c idcpf, String noticeType, Boolean isEmployee, String formatType)
	 * 代理店ユーザ
	 */
	static testMethod void createRecordByIdcpf_test01() {
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');
		E_IDCPF__c eidcp = [Select Id, ContactCL3PF_AGNTNUM__c, User__r.Id, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];

		Test.startTest();
		Operation operation = new Operation();
		operation.pType = I_Const.EBIZDATASYNC_KBN_NCOL;
		operation.ebizLog = [Select Id, CreatedDate From E_BizDataSyncLog__c Where Id = :log.Id];
		I_PushOut__c result = operation.createRecordByIdcpf_ext(eidcp, I_Const.EBIZDATASYNC_KBN_NCOL, false, 'yyyyMMdd');
		Test.stopTest();

		System.debug(result);
	}

	/**
	 * I_PushOut__c createRecordByIdcpf(E_IDCPF__c idcpf, String noticeType, Boolean isEmployee, String formatType)
	 * 社員ユーザ
	 */
	static testMethod void createRecordByIdcpf_test02() {
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, 'EP');
		E_IDCPF__c eidcp = [Select Id, ContactCL3PF_AGNTNUM__c, User__r.Id, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];

		Test.startTest();
		Operation operation = new Operation();
		operation.pType = I_Const.EBIZDATASYNC_KBN_NCOL;
		operation.ebizLog = [Select Id, CreatedDate From E_BizDataSyncLog__c Where Id = :log.Id];
		I_PushOut__c result = operation.createRecordByIdcpf_ext(eidcp, I_Const.EBIZDATASYNC_KBN_NCOL, true, 'yyyyMMdd');
		Test.stopTest();

		System.debug(result);
	}

	/**
	 * I_PushOut__c createRecordByUser(User user, String noticeType, Boolean isEmployee, String formatType)
	 * 代理店ユーザ
	 */
	static testMethod void createRecordByUser_test01() {
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');

		Test.startTest();
		Operation operation = new Operation();
		operation.pType = I_Const.EBIZDATASYNC_KBN_NCOL;
		operation.ebizLog = [Select Id, CreatedDate From E_BizDataSyncLog__c Where Id = :log.Id];
		//I_PushOut__c result = operation.createRecordByUser_ext(user, I_Const.EBIZDATASYNC_KBN_NCOL, false, 'yyyyMMdd');
		Test.stopTest();

		//System.debug(result);
	}

	/**
	 * Set<E_IDCPF__c> getAgentIdcpfs(Set<String> conIds)
	 * 正常
	 */
	static testMethod void getAgentIdcpfs_test01(){
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');

		// ID Update 個人保険照会フラグ = 1
		E_IDCPF__c idcpf = [Select Id, FLAG01__c, IsNotNotification__c, ZSTATUS01__c, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.IsNotNotification__c = false;
		update idcpf;

		Set<String> ids = new Set<String>{atCon.Id};
		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getAgentIdcpfs_ext(ids);
		Test.stopTest();

		System.assert(result.isEmpty() == false);
	}

	/**
	 * Set<E_IDCPF__c> getAgentIdcpfs(Set<String> conIds)
	 * 個人保険照会フラグ　が　『1』　でない
	 */
	static testMethod void getAgentIdcpfs_test02(){
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');

		// ID Update 個人保険照会フラグ = 0
		E_IDCPF__c idcpf = [Select Id, FLAG01__c, IsNotNotification__c, ZSTATUS01__c, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];
		idcpf.FLAG01__c = '0';
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.IsNotNotification__c = false;
		update idcpf;

		Set<String> ids = new Set<String>{atCon.Id};
		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getAgentIdcpfs_ext(ids);
		Test.stopTest();

		System.assert(result.isEmpty() == true);
	}

	/**
	 * Set<E_IDCPF__c> getAgentIdcpfs(Set<String> conIds)
	 * パスワードステータス　が　『1』　でない
	 */
	static testMethod void getAgentIdcpfs_test03(){
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');

		// ID Update パスワードステータス = 0
		E_IDCPF__c idcpf = [Select Id, FLAG01__c, IsNotNotification__c, ZSTATUS01__c, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = '0';
		idcpf.IsNotNotification__c = false;
		update idcpf;

		Set<String> ids = new Set<String>{atCon.Id};
		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getAgentIdcpfs_ext(ids);
		Test.stopTest();

		System.assert(result.isEmpty() == true);
	}

	/**
	 * Set<E_IDCPF__c> getAgentIdcpfs(Set<String> conIds)
	 * 送信対象外　が　『false』　でない
	 */
	static testMethod void getAgentIdcpfs_test04(){
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');

		// ID Update 通知対象外 = true
		E_IDCPF__c idcpf = [Select Id, FLAG01__c, IsNotNotification__c, ZSTATUS01__c, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.IsNotNotification__c = true;
		update idcpf;

		Set<String> ids = new Set<String>{atCon.Id};
		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getAgentIdcpfs_ext(ids);
		Test.stopTest();

		System.assert(result.isEmpty() == true);
	}

	/**
	 * Set<E_IDCPF__c> getGroupIdcpfs(Set<String> grpCode)
	 */
	static testMethod void getGroupIdcpfs_test01(){
		String GRP_CODE = 'L112345';

		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AH');

		// ID Update
		E_IDCPF__c idcpf = [Select Id, FLAG01__c, IsNotNotification__c, ZSTATUS01__c, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.IsNotNotification__c = false;
		idcpf.ZINQUIRR__c = GRP_CODE;
		update idcpf;

		Set<String> ids = new Set<String>{GRP_CODE};
		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getGroupIdcpfs_ext(ids);
		Test.stopTest();

		System.assert(result.isEmpty() == false);
	}

	/**
	 * Set<E_IDCPF__c> getGroupIdcpfs(Set<String> grpCode)
	 */
	static testMethod void getGroupIdcpfs_test02(){
		String GRP_CODE = 'L112345';

		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AH');

		// ID Update 通知対象外 = true
		E_IDCPF__c idcpf = [Select Id, FLAG01__c, IsNotNotification__c, ZSTATUS01__c, User__r.Email From E_IDCPF__c Where User__r.Id = :user.Id];
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.IsNotNotification__c = true;
		idcpf.ZINQUIRR__c = GRP_CODE;
		update idcpf;

		Set<String> ids = new Set<String>{'L111111'};
		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getGroupIdcpfs_ext(ids);
		Test.stopTest();

		System.assert(result.isEmpty() == true);
	}

	/**
	 * Set<E_IDCPF__c> getEmployeeIdcpfs()
	 */
	static testMethod void getEmployeeIdcpfs_test01(){
		// TestData
		createDataEBizLog(I_Const.EBIZDATASYNC_KBN_NCOL);
		createUser('ＭＲ');
		createDataAccessObj(user.Id, 'EP');

		Test.startTest();
		Operation operation = new Operation();
		Set<E_IDCPF__c> result = operation.getEmployeeIdcpfs_ext();
		Test.stopTest();

		System.assert(result.isEmpty() == false);
	}


	/** Test Data ************************************************************************ */
	private static List<I_PushOut__c> createPushOuts(){
		List<I_PushOut__c> recs = new List<I_PushOut__c>();
		recs.add(new I_PushOut__c());
		recs.add(new I_PushOut__c());
		recs.add(new I_PushOut__c());
		insert recs;
		return recs;
	}

    private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
        Profile p = [Select Id From Profile Where Name = :profileName];

		// Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );

    	// User
    	if(profileName != PF_PARTNER){
    		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
    		user.UserRoleId = portalRole.Id;
    		insert user;

    	// Partner User
    	}else{
			system.runAs(thisuser){
				// Account 代理店格
				ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
				insert ahAcc;

				// Account 事務所
				ayAcc = new Account(
						Name = 'office1'
						,ParentId = ahAcc.Id
						,E_CL2PF_ZAGCYNUM__c = 'ay001'
						,E_COMM_VALIDFLAG__c = '1'
				);
				insert ayAcc;

				// Contact 募集人
				atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}

    		user.ContactId = atCon.Id;
    		insert user;

			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
    	}
    }

    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);

            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,ZDSPFLAG02__c = '0'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }

    private static void createDataEBizLog(String kind){
        system.runAs(thisuser){
            log = new E_BizDataSyncLog__c();
            log.Kind__c = kind;
            insert log;
        }
    }
}