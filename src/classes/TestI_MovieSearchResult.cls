@isTest
private class TestI_MovieSearchResult {

	private static testMethod void should_build_movie_result_when_has_received_media_data() {
		I_MovieSearchResult.NNTubeResult tube = new I_MovieSearchResult.NNTubeResult();
		tube.resultcode = '';
		tube.moviesum = 0;
		tube.meta = new List<I_MovieSearchResult.NNTubeMedia>();
		tube.error = '';

		I_MovieSearchResult.NNTubeMedia media = new I_MovieSearchResult.NNTubeMedia();
		media.api_keywords = '';
		media.ccode = '';
		media.cname = '';
		media.copyright = '';
		media.copyright_url = '';
		media.customer_movie = '';
		media.disclaimer_id = 0;
		media.duration = 0;
		media.expiration_date = '';
		media.insert_date = '';
		media.keywords = '';
		media.level = '';
		media.like_cnt = 0;
		media.level = '';
		media.long_description = '';
		media.mid = 0;
		media.obfuscated_mid = '';
		media.play_cnt = 0;
		media.publication_date = '2000/01/01';
		media.recommend = '';
		media.scode = '';
		media.small_thumbnail_url = '';
		media.small_thumbnail_url_ssl = '';
		media.thumbnail_url = '';
		media.thumbnail_url_ssl = '';
		media.title = '';
		media.update_date = '';

		I_MovieSearchResult result = I_MovieSearchResult.build(media);
		System.assertEquals('', result.getFormattedSubject());
		System.assertEquals('', result.getFormattedDescription());
	}
}