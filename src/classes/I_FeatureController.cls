public with sharing class  I_FeatureController extends I_CMSController{
	// ページ：ライブラリ配下のIRIS CMSコンテンツ群
	public List<I_ContentMaster__c> sections{get;set;}

	//社内限コンポーネント判別済みリスト
	public List<I_CMSSection> featureSection{get;set;}

	// I_TubeConnectHelper
	private I_TubeConnectHelper connector;

	//pgkey が設定されているかのフラグ
	public Boolean pkeyFlag{get;set;}
	//メインビジュアル用Attachment
	public Attachment att{get;set;}
	//特集掲載期間のフラグ
	public Boolean featureDayFlag{get;set;}


	// コンストラクタ
	public I_FeatureController() {
		try{
			featureDayFlag = false;
			this.sections = new List<I_ContentMaster__c>();
			this.featureSection = new List<I_CMSSection>();

			for(I_CMSSection s : CMSSections) {
				this.sections.add(s.record);
				this.featureSection.add(s);
			}
			this.connector = I_TubeConnectHelper.getInstance();

			String pkey = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGEKEY);
			if(String.isNotBlank(pkey)){
				pkeyFlag = true;
			}else{
				pkeyFlag = false;
			}

			if((activePage.FeatureDisplayFrom__c <= E_Util.SYSTEM_TODAY()) && (activePage.FeatureDisplayTo__c >=  E_Util.SYSTEM_TODAY())){
				featureDayFlag = true;
			}else{
				featureDayFlag = false;
			}
		}catch(Exception e){
			getPageMessages().addErrorMessage(e.getMessage(), e.getStackTraceString());
		}

	}
	
	/**
	 * I_TubeConnectHelperインスタンスを返す.
	 *
	 * @return I_TubeConnectHelper
	 */
	public I_TubeConnectHelper getTubeConnectHelper() {
		return this.connector;
	}

	//メインビジュアル用添付ファイル取得
	public Boolean getBannerThumbnail(){
		this.att = new Attachment();
		this.att = E_AttachmentDao.getRecById(activePage.Id);
		if(att != null){
			return true;
		}
		return false;
	}
}