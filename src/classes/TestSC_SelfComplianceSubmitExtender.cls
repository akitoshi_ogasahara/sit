@istest
private class TestSC_SelfComplianceSubmitExtender {
	// コンストラクタ:新規作成
	private static testMethod void SelfComplianceSubmitTest001() {
		// テストデータ作成
		SC_SelfCompliance__c record = new SC_SelfCompliance__c();

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);
		Test.stopTest();

        System.assertEquals(extender.helpMenukey,    SC_Const.MENU_HELP_SELFCOMPLIANCE);
        System.assertEquals(extender.helpPageJumpTo, SC_Const.MENU_HELP_SELFCOMPLIANCE_SUBMIT_NEW);
        System.assertEquals(extender.guideMenukey,   SC_Const.MENU_GUIDE_SELFCOMPLIANCE);

    	return;
	}

	// コンストラクタ:変更
	private static testMethod void SelfComplianceSubmitTest002() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
		TestSC_TestUtil.createAttachment(true, scsc.Id);

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(scsc);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);
		Test.stopTest();

        System.assertEquals(extender.helpMenukey,    SC_Const.MENU_HELP_SELFCOMPLIANCE);
        System.assertEquals(extender.helpPageJumpTo, SC_Const.MENU_HELP_SELFCOMPLIANCE_SUBMIT_UPD);
        System.assertEquals(extender.guideMenukey,   SC_Const.MENU_GUIDE_SELFCOMPLIANCE);

    	return;
	}


	// init:正常パターン
	private static testMethod void SelfComplianceSubmitTest003() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
		TestSC_TestUtil.createAttachment(true, scsc.Id);

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	ApexPages.currentPage().getParameters().put('parentid', sco.Id);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(scsc);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);

			extender.init();
		Test.stopTest();

        System.assertEquals(extender.isSuccessOfInitValidate, true);

    	return;
	}

	// init:parentIdなし
	private static testMethod void SelfComplianceSubmitTest004() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
		TestSC_TestUtil.createAttachment(true, scsc.Id);

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	ApexPages.currentPage().getParameters().put('parentid', null);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(scsc);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);

			extender.init();
		Test.stopTest();

        System.assertEquals(extender.isSuccessOfInitValidate, false);

    	return;
	}

	// doSaveEx:ファイルなし
	private static testMethod void SelfComplianceSubmitTest005() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		SC_SelfCompliance__c scsc = new SC_SelfCompliance__c();
		Attachment att = new Attachment();

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	ApexPages.currentPage().getParameters().put('parentid', sco.Id);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(scsc);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);

			extender.init();
			extender.file = att;
			PageReference pr = extender.doSaveEx();
		Test.stopTest();

        System.assertEquals(pr, null);
        System.assertEquals(extender.pageMessages.getErrorMessages().get(0).summary, SC_Const.ERR_MSG03_REQUIRED_FILE);

    	return;
	}

	// doSaveEx:（変更）正常パターン
	private static testMethod void SelfComplianceSubmitTest006() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		SC_SelfCompliance__c scsc = TestSC_TestUtil.createSCOffice(true, sco.Id);
		Attachment att = new Attachment();
		att.body = Blob.valueOf('test.jpg');

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	ApexPages.currentPage().getParameters().put('parentid', sco.Id);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(scsc);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);
			extender.init();

			extender.file = att;
			PageReference pr = extender.doSaveEx();
		Test.stopTest();

		System.assertEquals(pr.getUrl(), Page.E_SCSelfCompliances.getUrl() + '?id=' + sco.Id);

    	return;
	}

	// doSaveEx:（新規）正常パターン
	private static testMethod void SelfComplianceSubmitTest007() {
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c sco = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		SC_SelfCompliance__c scsc = new SC_SelfCompliance__c();
		Attachment att = new Attachment();
		att.Name = 'file1';
		att.body = Blob.valueOf('test.jpg');

		Test.startTest();
	    	Test.setCurrentPage(Page.E_SCSelfComplianceSubmit);
	    	ApexPages.currentPage().getParameters().put('parentid', sco.Id);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(scsc);
			SC_SelfComplianceSubmitController controller = new SC_SelfComplianceSubmitController(standardcontroller);
			SC_SelfComplianceSubmitExtender extender = new SC_SelfComplianceSubmitExtender(controller);
			extender.init();

			extender.file = att;
			PageReference pr = extender.doSaveEx();
		Test.stopTest();

		System.assertEquals(pr.getUrl(), Page.E_SCSelfCompliances.getUrl() + '?id=' + sco.Id);

    	return;
	}
}