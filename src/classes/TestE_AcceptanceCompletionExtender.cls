@isTest(SeeAllData=false)
public with sharing class TestE_AcceptanceCompletionExtender {
    private static User testuser;
    private static E_IDCPF__c idcpf;
    private static Account account;
    private static Contact contact;
    private static RecordType recordType; 
    private static datetime nowDateTime = system.now();
    private static contact insCon;
    
    static void init(){
        system.debug('this is init');
        insertMessage();
        createDataCommon();
        insertE_bizDataSyncLog();
    }
    
    //レコード1件の正常系 
    private static testMethod void testStandard() {
        init();
        //インスタンス
        User user = new user();
        string type = 'ACC|001';
        PageReference pageRef = Page.E_AcceptanceCompletion;
        system.runAs(testuser){
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('type', type);
            ApexPages.currentPage().getParameters().put('conid', contact.ID);
            Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(user);
            E_AcceptanceCompletionController controller = new E_AcceptanceCompletionController(standardcontroller);
            E_AcceptanceCompletionExtender extender = controller.getExtender();
            Test.startTest();   
            //確認
            System.assertEquals(extender.type,type );
            extender.doContactInfo();
            Test.stopTest();
        }
    }
    
    private static void insertMessage(){
        LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
        String type = 'メッセージ';
        String param = 'ACC|001';
        insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
        insert insMessage;
    }
    /* test data */
    static void createDataCommon(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            
            // User
            profile	profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
            testuser = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
                , CommunityNickName='tuser1'
            );
            insert testuser;
            // Account
            account = new Account(Name = 'testAccount');
            insert account;
            
            // Contact
            contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id,E_dataSyncDate__c = nowDateTime);
            insert contact;
            
            //
            idcpf = new E_IDCPF__c(
                FLAG01__c = '1',
                FLAG02__c = '1',
                //TRCDE01__c = 'TD06',
                //TRCDE02__c = 'TD06',
                User__c = testuser.Id,
                ZSTATUS02__c = '1',
                ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
            );
            insert idcpf;
        }        
    }
    private static void insertE_bizDataSyncLog(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        system.runAs(thisUser){
            E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
            insert log;
        }    
    }
    
    
}