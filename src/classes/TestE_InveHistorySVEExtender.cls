@isTest
private class TestE_InveHistorySVEExtender {
	
	//コンストラクタ
	//URLパラメータあり
	static testMethod void constructorTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			
			E_ITHPF__c record = new E_ITHPF__c();
			insert record;
			ApexPages.CurrentPage().getParameters().put('ithpfId', record.Id);
			ApexPages.StandardController controller = new ApexPages.StandardController(record);
			E_InveHistorySVEController extension = new E_InveHistorySVEController(controller);
			
			Test.startTest();
			E_InveHistorySVEExtender extender = new E_InveHistorySVEExtender(extension);
			Test.stopTest();
			
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			system.assertNotEquals(null, extender.pageRef);
			/*
			System.assert(extension.queryMap.get('dataTable').toSoql().contains('E_ITHPF__c='), extension.queryMap.get('dataTable').toSoql());
			*/
		}
	}
	
	//コンストラクタ
	//URLパラメータなし
	static testMethod void constructorTest002(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			
			E_ITHPF__c record = new E_ITHPF__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(record);
			E_InveHistorySVEController extension = new E_InveHistorySVEController(controller);
			
			Test.startTest();
			E_InveHistorySVEExtender extender = new E_InveHistorySVEExtender(extension);
			Test.stopTest();
			
			System.assert(!extension.queryMap.get('dataTable').toSoql().contains('E_ITHPF__c='));
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveHistorySVEExtender extender = createExtender();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			System.assertNotEquals(null, result);
		}
	}
	
	//ページアクション
	//doAuth失敗
	static testMethod void pageActionTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveHistorySVEExtender extender = createExtender();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			System.assertNotEquals(null, result);
		}
	}
	
	//テストユーザ作成
	//プロファイルを投信ヘッダの権限があるものに設定
	static User createUser(String flag03){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG03__c = flag03;
		insert idcpf;
		return u;
	}
	
	//Extender作成
	static E_InveHistorySVEExtender createExtender(){
		E_ITHPF__c record = new E_ITHPF__c();
		insert record;
		ApexPages.CurrentPage().getParameters().put('ithpfId', record.Id);
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveHistorySVEController extension = new E_InveHistorySVEController(controller);
		E_InveHistorySVEExtender extender = new E_InveHistorySVEExtender(extension);
		return extender;
	}
}