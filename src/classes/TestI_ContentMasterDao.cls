@isTest
/**
 * ContentMasterDaoテストクラス
 * 部分リリースように呼び出しのみ　※ToDo assertを追加
 */
private class TestI_ContentMasterDao {

/*
* 観点:getRecsByPageId　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの親ページのIDを引数へ渡す
* 検証:親ページに紐づくIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecordsByPageId_Test001() {
		//IRISページ
		I_PageMaster__c page = new I_PageMaster__c(Name='テストページ');
		insert page;
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name='テストCMSコンテンツ' ,Page__c = page.Id);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getRecordsByPageId(page.Id);
		Test.stopTest();

		System.assertEquals(ctt.Id ,recs[0].Id);
	}


	@isTest static void getChatterFileIdsByRecId_Test001() {
		Test.startTest();
		I_ContentMasterDao.getChatterFileIdsByRecId(new Set<Id>());
		Test.stopTest();
	}

/*
* 観点:getRecordsByMenuCategory　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツのメインカテゴリを引数へ渡す
* 検証:指定したメインカテゴリのIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecordsByMenuCategory_Test001() {
		//IRISメニュー
		I_MenuMaster__c menu = new I_MenuMaster__c(name='テストメニュー',MainCategory__c = I_Const.MENU_MAIN_CATEGORY_NEW_PROPOSAL);
		insert menu;
		//IRISページ
		I_PageMaster__c page = new I_PageMaster__c(Name = 'テストページ', Menu__c = menu.Id);
		insert page;
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name='テストCMSコンテンツ' ,Page__c = page.Id);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getRecordsByMenuCategory(I_Const.MENU_MAIN_CATEGORY_NEW_PROPOSAL);
		Test.stopTest();

		System.assertEquals(ctt.Id,recs[0].Id);
	}

/*
* 観点:getRecByDocNumber　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの資料管理番号を引数へ渡す
* 検証:指定した資料管理番号のIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecByDocNumber_Test001() {
		String no = '資料管理番号01';
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name='テストCMSコンテンツ' ,DocumentNo__c = no);
		insert ctt;

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecByDocNumber(no);
		Test.stopTest();

		System.assertEquals(ctt.Id,rec.Id);
	}
/*
* 観点:getRecByFormNo　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツのフォームNoを引数へ渡す
* 検証:指定したフォームNoのIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecByFormNo_Test001() {
		String no = 'S-4552-01-19';
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name='テストCMSコンテンツ' ,FormNo__c = no);
		insert ctt;

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecByFormNo(no);
		Test.stopTest();

		System.assertEquals(ctt.Id,rec.Id);
	}
/*
* 観点:getRecByFormNo　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツのフォームNoとNameを引数へ渡す
* 検証:指定したフォームNoのIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecByFormNoAndName_Test001() {
		String contentname = 'テストCMSコンテンツ';
		String no = 'S-4552-01-19';
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name=contentname ,FormNo__c = no);
		insert ctt;

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecByFormNoAndName(no, contentname);
		Test.stopTest();

		System.assertEquals(ctt.Id,rec.Id);
	}

/*
* 観点:getRecByDocumentNoAndName　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの資料管理番号とNameを引数へ渡す
* 検証:指定した資料管理番号のIRIS_CMSコンテンツのリストが返されること
*/

	@isTest static void getRecByDocumentNoAndName_Test001() {

		String contentName = 'テストCMSコンテンツ';
		String no = '123-abc';

		I_ContentMaster__c ctt = new I_ContentMaster__c(Name = contentName, documentNo__c = no);
		insert ctt;

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecByDocumentNoAndName(no, contentName);
		Test.stopTest();

		System.assertEquals(ctt.Id,rec.Id);

	}


/*
* 観点:getSalesToolRecords　メソッド正常系
* 条件:Sales Toolに紐づいた、取得したいIRIS_CMSコンテンツのカテゴリを引数へ渡す
* 検証:Sales Toolに紐づく指定したカテゴリのIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getSalesToolRecords_Test001() {
		String cate = '定期保険;終身ガン保険';
		//IRIS_CMSコンテンツ-Sales Tool
		I_ContentMaster__c st = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_SALESTOOL);
		insert st;
		//IRIS_CMSコンテンツ-Sales Tool
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = 'テスト子コンテンツ'
														,ParentContent__c = st.Id
														,DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-5)
														,ValidTo__c = E_Util.SYSTEM_TODAY().addDays(10)
														,Category__c = cate
														);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getSalesToolRecords(cate.split(';')[0]);
		Test.stopTest();

		System.assertEquals(st.Id ,recs[0].Id);
	}

/*
* 観点:getRecordsInfoContent　メソッド正常系
* 条件:引数　なし
* 検証:空のIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecordsInfoContent_Test001() {
		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getRecordsInfoContent();
		Test.stopTest();

		System.assert(recs.isEmpty());
	}

/*
* 観点:getRecordsInfoContent　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツのIdを引数へ渡す
* 検証:空のIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getRecordsInfoContent_Test002() {
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getRecordsInfoContent(ctt.Id);
		Test.stopTest();

		System.assertEquals(ctt.Id, recs[0].Id);
	}


/*
* 観点:getPickupContentsForLibTop　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの条件と、件数を引数へ渡す
* 検証:指定した条件のIRIS_CMSコンテンツの指定した件数のリストが返されること
*/
	@isTest static void getPickupContentsForLibTop_Test001() {
		//IRIS_CMSコンテンツ
		List<I_ContentMaster__c> ctts = new List<I_ContentMaster__c>();
		for(Integer i = 0; i < 4;i++){
			ctts.add(new I_ContentMaster__c(name = 'テスト子コンテンツ00' + i
											,isHTML__c = false));
		}
		ctts[0].isHTML__c = true;//1つだけtrueにする
		insert ctts;

		String condition = 'isHTML__c = false';
		Decimal retrieveRecordCnt = 2;
		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getPickupContentsForLibTop(condition,retrieveRecordCnt);
		Test.stopTest();

		System.assertEquals(retrieveRecordCnt,recs.size());
		for(I_ContentMaster__c rec : recs){
			System.assertEquals(null,rec.Content__c);
		}
	}

/*
* 観点:getPickupContentsForLibTop　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの条件と、件数を引数へ渡す
* 検証:指定した条件のIRIS_CMSコンテンツの指定した件数のリストが返されること
*/
	@isTest static void getPickupContentsForLibTop_Test002() {
		//IRIS_CMSコンテンツ
		List<I_ContentMaster__c> ctts = new List<I_ContentMaster__c>();
		for(Integer i = 0; i < 4;i++){
			ctts.add(new I_ContentMaster__c(name = 'テスト子コンテンツ00' + i
											,isHTML__c = false));
		}
		ctts[0].isHTML__c = true;//1つだけtrueにする
		insert ctts;

		String condition = 'isHTML__c = false';
		Decimal retrieveRecordCnt = 2;
		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getPickupContentsForLibTop(condition);
		Test.stopTest();

		System.assertEquals(3,recs.size());
		for(I_ContentMaster__c rec : recs){
			System.assertEquals(null,rec.Content__c);
		}
	}


/*
* 観点:getBannerContents　メソッド正常系
* 検証:特集のIRIS_CMSコンテンツのリストが返されること
*/
	@isTest static void getBannerContents_Test001() {
		//IRISメニュー
		I_MenuMaster__c menu = new I_MenuMaster__c(name='テストメニュー'
													,SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
													);
		insert menu;
		//IRISページ
		/*
		Date today = E_Util.SYSTEM_TODAY();
		I_PageMaster__c page = new I_PageMaster__c(Name = 'テストページ'
													,Menu__c = menu.Id
													,FeatureDisplayFrom__c = today.addDays(-5)
													,FeatureDisplayTo__c = today.addDays(+5)
													);
		*/
		
		I_PageMaster__c page = new I_PageMaster__c(Name = 'テストページ'
													,Menu__c = menu.Id
													,FeatureDisplayFrom__c = Date.today()
													,FeatureDisplayTo__c = Date.today()
													);
		
		insert page;
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO
														,IRIS_Page__c = page.Id
														, Page__c = page.Id
														);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.getBannerContents();
		Test.stopTest();

		System.assertEquals(ctt.Id,recs[0].Id);
	}

/*
* 観点:getRecordByUpsertKey　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの外部IDを引数へ渡す
* 検証:取得したいIRIS_CMSコンテンツが返されること
*/
	@isTest static void getRecordByUpsertKey_Test001() {
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert ctt;
		ctt = [SELECT Id, Name, UpsertKey__c FROM I_ContentMaster__c WHERE ID =: ctt.Id];

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecordByUpsertKey(ctt.UpsertKey__c);
		Test.stopTest();

		System.assertEquals(ctt.Id ,rec.Id);
	}

/*
* 観点:getRecordByUpsertKey　メソッド異常系
* 条件:引数へnullを渡す
* 検証:nullが返されること
*/
	@isTest static void getRecordByUpsertKey_Test002() {
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert ctt;
		ctt = [SELECT Id, Name, UpsertKey__c FROM I_ContentMaster__c WHERE ID =: ctt.Id];

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecordByUpsertKey(null);
		Test.stopTest();

		System.assertEquals(null ,rec);
	}

/*
* 観点:getRecordByUpsertKey　メソッド異常系
* 条件:引数へ空文字を渡す
* 検証:nullが返されること
*/
	@isTest static void getRecordByUpsertKey_Test003() {
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert ctt;
		ctt = [SELECT Id, Name, UpsertKey__c FROM I_ContentMaster__c WHERE ID =: ctt.Id];

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getRecordByUpsertKey('');
		Test.stopTest();

		System.assertEquals(null ,rec);
	}

/*
* 観点:getRecordByUpsertKey　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの外部IDを引数へ渡す
* 検証:取得したいIRIS_CMSコンテンツが返されること
*/
	@isTest static void getTextByUpsertKey_Test001() {
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert ctt;
		ctt = [SELECT Id, Name, UpsertKey__c FROM I_ContentMaster__c WHERE ID =: ctt.Id];

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getTextByUpsertKey(ctt.UpsertKey__c);
		Test.stopTest();

		System.assertEquals(ctt.Id ,rec.Id);
	}

/*
* 観点:getCMSMapByUpsertKey　メソッド正常系
* 条件:取得したいIRIS_CMSコンテンツの外部IDを引数へ渡す
* 検証:取得したいIRIS_CMSコンテンツが返されること
*/
	@isTest static void getCMSMapByUpsertKey_Test001() {
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert ctt;
		ctt = [SELECT Id, Name, UpsertKey__c FROM I_ContentMaster__c WHERE ID =: ctt.Id];

		Test.startTest();
		List<String> upsertkeys = new List<String>();
		upsertkeys.add(ctt.UpsertKey__c);
		Map<String, I_ContentMaster__c> rec = I_ContentMasterDao.getCMSMapByUpsertKey(upsertkeys);
		Test.stopTest();

		System.assertEquals(ctt.Id ,rec.get(ctt.UpsertKey__c).Id);
	}

/*
* 観点:getRecordByInfoName　メソッド正常系
* 検証:取得したいIRIS_CMSコンテンツが返されること
*/
	@isTest static void getRecordByInfoName_Test001() {
		I_ContentMaster__c st = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert st;
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = '改訂お知らせ'
														,ParentContent__c = st.Id
														,InfoLabel__c = 'パンフレット等'
														);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> rec = I_ContentMasterDao.getRecordByInfoName(I_Const.DR_INFO_LABEL, 5);
		Test.stopTest();

		System.assertEquals(rec.size(), 1);
	}

/*
* 観点:getOshiraseInfo　メソッド正常系
* 検証:お知らせは以下のIRIS_CMSコンテンツが返されること
*/
	@isTest static void getOshiraseInfo_Test001() {
		I_ContentMaster__c st = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_INFO);
		insert st;
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = '改訂お知らせ'
														,ParentContent__c = st.Id
														,InfoLabel__c = 'パンフレット等'
														);
		insert ctt;

		Test.startTest();
		I_ContentMaster__c rec = I_ContentMasterDao.getOshiraseInfo();
		Test.stopTest();

		System.assertEquals(rec.Id, st.Id);
		System.assertEquals(rec.ChildContents__r[0].Id, ctt.Id);
	}
/*
* 観点:getPamphletRecords メソッド正常系
* 検証:パンフレット等のIRIS_CMSコンテンツが返されること
*/
	@isTest static void getPamphletRecords_Test001(){
		List<I_ContentMaster__c> parents = new List<I_ContentMaster__c>();
		Integer i = 0;
		for(String nm : I_Const.PAMPHLET_CONTENTS){
			I_ContentMaster__c parent = new I_ContentMaster__c();
			parent.Name = nm;
			parent.DisplayOrder__c = String.valueOf(i);
			parent.Style__c = 'パンフレット等';
			parents.add(parent);
			i++;
		}
		insert parents;
		//IRIS_CMSコンテンツ
		List<I_ContentMaster__c> children = new List<I_ContentMaster__c>();
		Integer n = 0;
		for(I_ContentMaster__c parent : parents){
			I_ContentMaster__c child = new I_ContentMaster__c();
			child.name = parent.Name + 'child';
			child.ParentContent__c = parent.Id;
			child.PamphletCategory__c = '定期保険';
			child.DisplayFrom__c = Date.today().addDays(-5);
			child.DisplayOrder__c = String.valueOf(n);
			children.add(child);
			n++;
		}
		insert children;

		Test.startTest();
		List<I_ContentMaster__c> rec = I_ContentMasterDao.getPamphletRecords('定期保険');
		Integer countRec = 0;
		for(I_ContentMaster__c pCtt: rec){
			if(!pCtt.ChildContents__r.isEmpty()){
				countRec += pCtt.ChildContents__r.size();
			}
		}
		Test.stopTest();
		System.assertEquals(i,rec.size());
		System.assertEquals(n,countRec);

	}

	@isTest static void retrieveDocumentalContentsTest(){
		//IRISページ
		I_PageMaster__c page = new I_PageMaster__c(Name='テストページ');
		insert page;
		//IRIS_CMSコンテンツ
		I_ContentMaster__c ctt = new I_ContentMaster__c(name='テストCMSコンテンツ' ,Page__c = page.Id);
		ctt.DocumentCategory__c = 'パンフレット／特に重要なお知らせ';
		ctt.DisplayFrom__c = Date.today();
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs = I_ContentMasterDao.retrieveDocumentalContents(new Set<String>{'パンフレット／特に重要なお知らせ'});
		Test.stopTest();

		System.assertEquals(ctt.Id ,recs[0].Id);
	}

	@isTest static void getSalesToolChildRecordsTest(){
		String cate = '定期保険;終身ガン保険';
		//IRIS_CMSコンテンツ-Sales Tool
		I_ContentMaster__c st = new I_ContentMaster__c(name = I_Const.IRIS_CONTENTS_NAME_SALESTOOL);
		insert st;
		//IRIS_CMSコンテンツ-Sales Tool
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = 'テスト子コンテンツ'
														,ParentContent__c = st.Id
														,DisplayFrom__c = E_Util.SYSTEM_TODAY().addDays(-5)
														,ValidTo__c = E_Util.SYSTEM_TODAY().addDays(10)
														,Category__c = cate
														);
		insert ctt;

		Test.startTest();
		List<I_ContentMaster__c> recs =  I_ContentMasterDao.getSalesToolChildRecords('定期保険', new Set<Id>{st.Id});
		Test.stopTest();

		System.assertEquals(1,recs.size());
		System.assertEquals(ctt.Id ,recs[0].Id);

	}
	
	// 201810 Web約款 -->
	@isTest
	static void getYakkanContentsTest(){
		
		String contentName = '親';
		
		I_ContentMaster__c ctt = new I_ContentMaster__c(name = contentName);
		ctt.DisplayFrom__c = Date.today();
		ctt.IsYakkanParentFlg__c = true;
		ctt.isSitePublishFlg__c = true;
		ctt.GuideAgreeDiv__c = '新契約用';
		ctt.AgencyCode__c = '汎用';
		ctt.InsType__c = 'TP';
		ctt.ContractDateFrom__c = Date.today();
		insert ctt;
		
		Test.startTest();
		
		List<I_ContentMaster__c> result = I_ContentMasterDao.getYakkanContents(' And Name = \'' + contentName + '\'', Date.today());
		
		Test.stopTest();
		
		System.assertEquals(1, result.size());
		System.assertEquals(ctt.Id, result.get(0).Id);
	}
	
	@isTest
	static void getContentAndAttachmentTest(){
		I_ContentMaster__c child = new I_ContentMaster__c(name = '子');
		child.YakkanType__c = '本編';
		child.IsYakkanParentFlg__c = false;
		child.isSitePublishFlg__c = true;
		insert child;
		
		Attachment att = new Attachment();
		att.Name = 'testAttachment.pdf';
		att.Body = Blob.valueOf('testAttachment');
		att.ParentId = child.Id;
		insert att;
		
		
		Set<Id> parentIds = new Set<Id>{child.Id};
		
		Test.startTest();
		
		List<I_ContentMaster__c> result = I_ContentMasterDao.getContentAndAttachment(parentIds);
		
		Test.stopTest();
		
		System.assertEquals(1, result.size());
		System.assertEquals(child.Id, result.get(0).Id);
		
		System.assertEquals(1, result.get(0).Attachments.size());
		System.assertEquals(att.Id, result.get(0).Attachments.get(0).Id);
	}
	// <-- 201810 Web約款
}