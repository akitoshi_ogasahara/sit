@isTest
private class TestE_IDCPFTriggerHandler
{
	/**
	 * E_AbstractControllerの網羅テスト12(異常系)
	 */
	private static testMethod void testIDCPFTriggerHandler001() {
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){

			Test.startTest();

			//ID管理
			E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '0';
			idcpf.FLAG02__c = '0';
			insert idcpf;
			update idcpf;
			delete idcpf;
			undelete idcpf;

			E_IDCPFTriggerHandler handler = new E_IDCPFTriggerHandler();
			E_IDCPFTriggerHandler.onAfterInsert(new List<E_IDCPF__c>{idcpf});
			E_IDCPFTriggerHandler.onBeforeInsert(new List<E_IDCPF__c>{idcpf});
			Map<ID,E_IDCPF__c> idcpMap = new Map<ID,E_IDCPF__c>();
			idcpMap.put(idcpf.Id, idcpf);
			E_IDCPFTriggerHandler.onBeforeUpdate(new List<E_IDCPF__c>{idcpf},idcpMap);
			E_IDCPFTriggerHandler.onAfterUpdate(new List<E_IDCPF__c>{idcpf},idcpMap);
			E_IDCPFTriggerHandler.OnAfterUpdateLog(idcpMap,idcpMap);
			// 例外がスローされないこと
			System.assert(true);
			Test.stopTest();
		}
	}
}