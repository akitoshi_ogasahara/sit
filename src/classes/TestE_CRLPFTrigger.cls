@isTest
private class TestE_CRLPFTrigger {

    //顧客ロールがOWのとき、契約者を更新
    //契約者区分がnullの場合は更新
    static testMethod void updatePolicyContractorTest01(){
        
        Account account01 = TestE_TestUtil.createAccount(true);
        Contact contract01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName01', '111', '1111');
        Contact contract02 = TestE_TestUtil.createContact(true, account01.Id, 'lastName02', '222', '2222');
        E_Policy__c policy01 = createPolicy(contract01.Id, null);
        E_CRLPF__c crlpf01 = createCRLPF(false, policy01.Id, E_Const.CLRRROLE_OW, contract02.Id);
        
        Test.startTest();
        insert crlpf01;
        Test.stopTest();
        
        List<E_Policy__c> resultPolicyList = getPolicyList(new Set<Id>{policy01.Id});
        System.assertEquals(1, resultPolicyList.size());
        E_Policy__c result = resultPolicyList[0];
        System.assertEquals(contract02.Id, result.Contractor__c);
        System.assertEquals(contract01.Id, result.Insured__c);
        System.assertEquals(contract01.Id, result.Annuitant__c);
        System.assertEquals(E_Const.COMM_ZCNTRDSC_P, result.COMM_ZCNTRDSC__c);
    }
    
    //顧客ロールがGRのとき、契約者を更新
    //契約者区分がnullの場合は更新
    static testMethod void updatePolicyContractorTest02(){
        
        Account account01 = TestE_TestUtil.createAccount(true);
        Contact contract01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName01', '111', '1111');
        Contact contract02 = TestE_TestUtil.createContact(true, account01.Id, 'lastName02', '222', '2222');
        E_Policy__c policy01 = createPolicy(contract01.Id, null);
        E_CRLPF__c crlpf01 = createCRLPF(false, policy01.Id, E_Const.CLRRROLE_GR, contract02.Id);
        
        Test.startTest();
        insert crlpf01;
        Test.stopTest();
        
        List<E_Policy__c> resultPolicyList = getPolicyList(new Set<Id>{policy01.Id});
        System.assertEquals(1, resultPolicyList.size());
        E_Policy__c result = resultPolicyList[0];
        System.assertEquals(contract01.Id, result.Contractor__c);
        System.assertEquals(contract01.Id, result.Insured__c);
        System.assertEquals(contract01.Id, result.Annuitant__c);
        System.assertEquals(null, result.COMM_ZCNTRDSC__c);
    }
    
    //顧客ロールがLAのとき、被保険者を更新
    static testMethod void updatePolicyContractorTest03(){
        
        Account account01 = TestE_TestUtil.createAccount(true);
        Contact contract01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName01', '111', '1111');
        Contact contract02 = TestE_TestUtil.createContact(true, account01.Id, 'lastName02', '222', '2222');
        E_Policy__c policy01 = createPolicy(contract01.Id, null);
        E_CRLPF__c crlpf01 = createCRLPF(true, policy01.Id, E_Const.CLRRROLE_LA, contract02.Id);
        
        Test.startTest();
        update crlpf01;
        Test.stopTest();
        
        List<E_Policy__c> resultPolicyList = getPolicyList(new Set<Id>{policy01.Id});
        System.assertEquals(1, resultPolicyList.size());
        E_Policy__c result = resultPolicyList[0];
        System.assertEquals(contract01.Id, result.Contractor__c);
        System.assertEquals(contract02.Id, result.Insured__c);
        System.assertEquals(contract01.Id, result.Annuitant__c);
        System.assertEquals(null, result.COMM_ZCNTRDSC__c);
    }
    
    //顧客ロールがBNかつ受取人種別が09のとき、年金受取人を更新
    static testMethod void updatePolicyContractorTest04(){
        
        Account account01 = TestE_TestUtil.createAccount(true);
        Contact contract01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName01', '111', '1111');
        Contact contract02 = TestE_TestUtil.createContact(true, account01.Id, 'lastName02', '222', '2222');
        E_Policy__c policy01 = createPolicy(contract01.Id, null);
        E_CRLPF__c crlpf01 = createCRLPF(true, policy01.Id, E_Const.CLRRROLE_BN, contract02.Id);
        
        Test.startTest();
        update crlpf01;
        Test.stopTest();
        
        List<E_Policy__c> resultPolicyList = getPolicyList(new Set<Id>{policy01.Id});
        System.assertEquals(1, resultPolicyList.size());
        E_Policy__c result = resultPolicyList[0];
        System.assertEquals(contract01.Id, result.Contractor__c);
        System.assertEquals(contract01.Id, result.Insured__c);
        System.assertEquals(contract02.Id, result.Annuitant__c);
        System.assertEquals(null, result.COMM_ZCNTRDSC__c);
    }
    
    //複数パターンが同時に更新される場合、すべて反映されることを確認
    static testMethod void updatePolicyContractorTest05(){
        
        Account account01 = TestE_TestUtil.createAccount(true);
        Contact contract01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName01', '111', '1111');
        Contact contract02 = TestE_TestUtil.createContact(true, account01.Id, 'lastName02', '222', '2222');
        Contact contract03 = TestE_TestUtil.createContact(true, account01.Id, 'lastName03', '333', '3333');
        Contact contract04 = TestE_TestUtil.createContact(true, account01.Id, 'lastName04', '444', '4444');
        E_Policy__c policy01 = createPolicy(contract01.Id, null);
        E_CRLPF__c crlpf01 = createCRLPF(true, policy01.Id, E_Const.CLRRROLE_OW, contract02.Id);
        E_CRLPF__c crlpf02 = createCRLPF(true, policy01.Id, E_Const.CLRRROLE_LA, contract03.Id);
        E_CRLPF__c crlpf03 = createCRLPF(true, policy01.Id, E_Const.CLRRROLE_BN, contract04.Id);
        
        Test.startTest();
        update new List<E_CRLPF__c>{crlpf01, crlpf02, crlpf03};
        Test.stopTest();
        
        List<E_Policy__c> resultPolicyList = getPolicyList(new Set<Id>{policy01.Id});
        System.assertEquals(1, resultPolicyList.size());
        E_Policy__c result = resultPolicyList[0];
        System.assertEquals(contract02.Id, result.Contractor__c);
        System.assertEquals(contract03.Id, result.Insured__c);
        System.assertEquals(contract04.Id, result.Annuitant__c);
        System.assertEquals(E_Const.COMM_ZCNTRDSC_P, result.COMM_ZCNTRDSC__c);
    }
    
    //保険契約ヘッダの更新結果にエラーがあった場合は、保険顧客レコードにAddError
    static testMethod void updatePolicyContractorTest06(){
        
        Account account01 = TestE_TestUtil.createAccount(true);
        Contact contract01 = TestE_TestUtil.createContact(true, account01.Id, 'lastName01', '111', '1111');
        Contact contract02 = TestE_TestUtil.createContact(true, account01.Id, 'lastName02', '222', '2222');
        E_Policy__c policy01 = createPolicy(contract01.Id, null);
        //更新対象のContactIDにAccountIDを設定し、Updateエラーを起こす
        E_CRLPF__c crlpf01 = createCRLPF(true, policy01.Id, E_Const.CLRRROLE_OW, account01.Id);
        
        Test.startTest();
        E_CRLPFTriggerBizLogic bizLogic = new E_CRLPFTriggerBizLogic();
        bizLogic.updatePolicyContractor(new List<E_CRLPF__c>{crlpf01});
        Test.stopTest();
        
        //保険契約ヘッダが更新されていないことを確認
        List<E_Policy__c> resultPolicyList = getPolicyList(new Set<Id>{policy01.Id});
        System.assertEquals(1, resultPolicyList.size());
        E_Policy__c result = resultPolicyList[0];
        System.assertEquals(contract01.Id, result.Contractor__c);
        System.assertEquals(contract01.Id, result.Insured__c);
        System.assertEquals(contract01.Id, result.Annuitant__c);
        System.assertEquals(null, result.COMM_ZCNTRDSC__c);
    }
    
    //保険契約ヘッダを作成
    static E_Policy__c createPolicy(Id contractId, String zcntrdsc){
        E_Policy__c policy01 = TestE_TestUtil.createPolicy(false, contractId, E_Const.POLICY_RECORDTYPE_COLI, '111');
        policy01.Contractor__c = contractId;
        policy01.Insured__c = contractId;
        policy01.Annuitant__c = contractId;
        policy01.COMM_ZCNTRDSC__c = zcntrdsc;
        insert policy01;
        return policy01;
    }
    
    //保険顧客情報を作成
    static E_CRLPF__c createCRLPF(Boolean isInsert, Id policyId, String clrrrole, Id contractId){
        E_CRLPF__c crlpf01 = new E_CRLPF__c();
        crlpf01.E_Policy__c = policyId;
        if(isInsert) insert crlpf01;
        crlpf01.CLRRROLE__c = clrrrole;
        crlpf01.BNYTYP__c = E_Const.BNYTYP_09;
        crlpf01.Contractor__c = contractId;
        return crlpf01;
    }
    
    //保険契約ヘッダを取得
    static List<E_Policy__c> getPolicyList(Set<Id> policyIds){
        return [
            select Id
                , Contractor__c
                , Insured__c
                , Annuitant__c
                , COMM_ZCNTRDSC__c
            from E_Policy__c
            where Id in :policyIds
        ];
    }

}