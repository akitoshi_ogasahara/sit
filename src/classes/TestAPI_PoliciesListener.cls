@isTest
private class TestAPI_PoliciesListener {

	//API呼び出しテスト 現在の状況コードI、Y
	@isTest static void doGetTest1() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			//月払
			E_Policy__c pol = createPolicy(con,'12345678','12','I');
			createCovpf(pol,'I');
			createCovpf(pol,'Y');
			//年払
			E_Policy__c pol2 = createPolicy(con,'12341234','01','I');
			createCovpf(pol2,'I');
			createCovpf(pol2,'Y');
			//半年払
			E_Policy__c pol3 = createPolicy(con,'87654321','02','I');
			createCovpf(pol3,'I');
			createCovpf(pol3,'Y');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(60000, res.zcsbnamt);
			System.assertEquals(10000, res.premiumM);
			System.assertEquals(10000, res.premiumY);
			System.assertEquals(10000, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 現在の状況コードL、N
	@isTest static void doGetTest2() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			//月払
			E_Policy__c pol = createPolicy(con,'12345678','12','L');
			createCovpf(pol,'L');
			createCovpf(pol,'N');
			//年払
			E_Policy__c pol2 = createPolicy(con,'12341234','01','L');
			createCovpf(pol2,'L');
			createCovpf(pol2,'N');
			//半年払
			E_Policy__c pol3 = createPolicy(con,'87654321','02','L');
			createCovpf(pol3,'L');
			createCovpf(pol3,'N');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(60000, res.zcsbnamt);
			System.assertEquals(0, res.premiumM);
			System.assertEquals(0, res.premiumY);
			System.assertEquals(0, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 現在の状況コードB、M、X
	@isTest static void doGetTest3() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			E_Policy__c pol = createPolicy(con,'12345678','12','B');
			E_Policy__c pol2 = createPolicy(con,'12341234','01','M');
			E_Policy__c pol3 = createPolicy(con,'87654321','02','X');
			createCovpf(pol,'B');
			createCovpf(pol,'M');
			createCovpf(pol,'X');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(0, res.zcsbnamt);
			System.assertEquals(0, res.premiumM);
			System.assertEquals(0, res.premiumY);
			System.assertEquals(0, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 現在の状況コードA、J、K
	@isTest static void doGetTest4() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			E_Policy__c pol = createPolicy(con,'12345678','12','A');
			createCovpf(pol,'A');
			createCovpf(pol,'J');
			createCovpf(pol,'K');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(0, res.zcsbnamt);
			System.assertEquals(0, res.premiumM);
			System.assertEquals(0, res.premiumY);
			System.assertEquals(0, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 保険契約なし
	@isTest static void doGetTest5() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals(404,response.statusCode);
			System.assertEquals('004', res.errors[0].msgId);
			Test.stopTest();
		}
	}

	//API呼び出しテスト パスワードステータス != 1
	@isTest static void doGetTest6() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','0');
			E_Policy__c pol = createPolicy(con,'12345678','12','I');
			createCovpf(pol,'I');
			createCovpf(pol,'L');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals(403,response.statusCode);
			System.assertEquals('001', res.errors[0].msgId);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 個人保険照会フラグ != 1
	@isTest static void doGetTest7() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'0','1');
			E_Policy__c pol = createPolicy(con,'12345678','12','I');
			createCovpf(pol,'I');
			createCovpf(pol,'L');
		}

		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals(403,response.statusCode);
			System.assertEquals('002', res.errors[0].msgId);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 保険契約ヘッダ 月払のみ
	@isTest static void doGetTest8() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			E_Policy__c pol = createPolicy(con,'12345678','12','I');
			createCovpf(pol,'I');
			createCovpf(pol,'Y');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(20000, res.zcsbnamt);
			System.assertEquals(10000, res.premiumM);
			System.assertEquals(0, res.premiumY);
			System.assertEquals(0, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 保険契約ヘッダ 年払のみ
	@isTest static void doGetTest9() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			E_Policy__c pol = createPolicy(con,'12345678','01','I');
			createCovpf(pol,'I');
			createCovpf(pol,'Y');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(20000, res.zcsbnamt);
			System.assertEquals(0, res.premiumM);
			System.assertEquals(10000, res.premiumY);
			System.assertEquals(0, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 保険契約ヘッダ 半年払のみ
	@isTest static void doGetTest10() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			E_Policy__c pol = createPolicy(con,'12345678','02','I');
			createCovpf(pol,'I');
			createCovpf(pol,'Y');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(20000, res.zcsbnamt);
			System.assertEquals(0, res.premiumM);
			System.assertEquals(0, res.premiumY);
			System.assertEquals(10000, res.premiumHY);
			Test.stopTest();
		}
	}

	//API呼び出しテスト 現在の状況コードが複数
	@isTest static void doGetTest11() {
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User runUs = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = createContact(acc);
			runUs = createCustomer(con);
			createIDCP(runUs,'1','1');
			E_Policy__c pol = createPolicy(con,'12345678','12','I');
			createCovpf(pol,'I');
			createCovpf(pol,'B');
			E_Policy__c pol2 = createPolicy(con,'87654321','01','L');
			createCovpf(pol2,'L');
			createCovpf(pol2,'A');
		}


		RestRequest request = new RestRequest();
		RestResponse response = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl() + '/services/apexrest/policies/doGet';
		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = response;

		System.runAs(runUs){
			Test.startTest();
			API_PoliciesListener.doGet();
			API_PoliciesBizLogic.policies res = (API_PoliciesBizLogic.policies)JSON.deserialize(response.responseBody.toString(),API_PoliciesBizLogic.policies.class);
			System.assertEquals('Test1000', res.clientno);
			System.assertEquals('テスト契約者', res.ownerName);
			System.assertEquals(40000, res.zcsbnamt);
			System.assertEquals(10000, res.premiumM);
			System.assertEquals(0, res.premiumY);
			System.assertEquals(0, res.premiumHY);
			Test.stopTest();
		}
	}

	//メニュー種類
	static E_MenuKind__c createMenu(){
		E_MenuKind__c menu = new E_MenuKind__c();
		menu.Name = '契約者';              //メニュー種類名
		menu.ExternalId__c = 'MK_Customer'; //外部ID

		insert menu;
		return menu;
	}

	//取引先（代理店）
	static Account createAccount(){
		E_MenuKind__c menu = createMenu();

		Account acc = new Account();
		acc.Name = 'テスト代理店';     //代理店名
		acc.ZHEADAY__c = 'A1234';   //代理店番号
		acc.E_MenuKind__c = menu.Id;//メニュー種類
		insert acc;
		return acc;
	}

	//取引先責任者（契約者）
	static Contact createContact(Account acc){
		Contact con = new Contact();
		con.LastName = 'テスト契約者';    //取引先責任者名
		con.E_CLTPF_ZCLKNAME__c = con.LastName; //カナ名
		con.AccountId = acc.Id; //取引先
		con.E_CLTPF_CLNTNUM__c = 'Test1000';    //顧客番号
		insert con;
		return con;
	}

	//契約者ユーザー作成
	static User createCustomer(Contact con){
		User us = new User();
		us.Username = 'testUser@terrasky.ingtesting';
		us.Alias = 'テスト契約者';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_CustomerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
		return us;
	}

	//ID管理
	static void createIDCP(User us,String coliFlag,String zstatus){
		E_IDCPF__c idcp = new E_IDCPF__c();
		idcp.OwnerId = us.Id;           //所有者
		idcp.User__c = us.Id;           //ユーザー
		idcp.ZIDTYPE__c = 'OW';         //ID種別
		idcp.FLAG01__c = coliFlag;      //個人保険照会フラグ
		idcp.ZSTATUS01__c = zstatus;    //パスワードステータス

		insert idcp;
	}

	//保険契約ヘッダ
	static E_Policy__c createPolicy(Contact con,String chdrnum,String payMethod,String pstatus){
		E_Policy__c pol = new E_Policy__c();
		pol.Contractor__c = con.Id; //契約者
		pol.COMM_CHDRNUM__c = chdrnum;  //証券番号
		pol.COLI_BILLFREQ__c = payMethod;	//支払方法コード
		pol.COMM_SINSTAMT__c = 10000;	//1回あたりの保険料
		pol.COLI_ZCSHVAL__c = 20000;	//解約返戻金

		pol.COMM_STATCODE__c = pstatus;	//現在の状況コード

		String coliRecType = E_Const.POLICY_RECORDTYPE_COLI;
		RecordType rec = [SELECT id FROM RecordType WHERE DeveloperName =: coliRecType];
		pol.RecordTypeId = rec.Id;  //レコードタイプ

		insert pol;
		return pol;

	}

	//個人保険特約
	static void createCovpf(E_Policy__c pol,String pstatus){
		E_COVPF__c cov = new E_COVPF__c();
		cov.COLI_INSTPREM__c = 10000;   //保険料
		cov.DCOLI_ZCSBNAMT__c = 1000;   //解約時受取金額
		cov.COLI_PSTATCODE__c = pstatus;    //現在の状況コード
		cov.COLI_ZCRIND__c = I_Const.COVPF_COLI_ZCRIND_C;   //主契約フラグ
		cov.E_Policy__c = pol.Id;   //保険契約ヘッダー
		
		String coliRecType = E_Const.COVPF_RECORDTYPE_ECOVPF;
		RecordType rec = [SELECT id FROM RecordType WHERE DeveloperName =: coliRecType];
		cov.RecordTypeId = rec.Id;  //レコードタイプ

		insert cov;
	}
}