public with sharing class E_ReissuePasswordController extends E_AbstractController {

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;private set;}

	//ユーザ名
	public String userId {get;set;}

	//パスワード再発行フラグ
	private Boolean isReissue;

	//コンストラクタ
	public E_ReissuePasswordController(){
		pgTitle = 'パスワード再発行';
		isReissue = false;
		pageMessages = getPageMessages();

	}

	//認証画面_ページアクション
	public PageReference pageAction(){
		isReissue = true;
		pageRef = doAuth(E_Const.ID_KIND.LOGIN, userinfo.getUserId());
		return E_Util.toErrorPage(pageRef, null);
	}

	//認証画面_[次へ]ボタン
	public PageReference doReissuePassword(){

		PageReference ret = null;
		pageMessages.clearMessages();

		try {

			//ユーザIDチェック
			if(String.isBlank(userId)){
				pageMessages.addErrorMessage(getMSG().get('LRE|202'));
				return null;
			}
			
	  //ユーザレコード取得
	  User userRec = E_UserDaoWithout.getUserRecByUserName(userId + System.Label.E_USERNAME_SUFFIX);
	  if(userRec == null){
		pageMessages.addErrorMessage(getMSG().get('LGE|001'));
		return null;
	  }
	  
	  //ユーザロック
	  UserLogin userLoginRecBefore = E_UserLoginDaoWithout.getRecByUserId(userRec.id);
	  if(userLoginRecBefore == null){
		String pathPrefix = Site.getPathPrefix();
		if (!String.isEmpty(pathPrefix)) {
		  if (pathPrefix.indexOf(E_Const.NNLINK_CLI) > 0) {
			pageMessages.addErrorMessage(getMSG().get('LRE|203'));
		  } else {
			pageMessages.addErrorMessage(getMSG().get('LRE|204'));
		  }
		  return null;
		}
	  }
	  
			//パスワードリセット
			boolean isPass = Site.forgotPassword(userId + System.Label.E_USERNAME_SUFFIX);
			
			//画面遷移
			ret = Page.E_ReissuePasswordComp;
			isReissue = true;

		} catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
			ret = null;
		}

		return ret;
	}

	//[戻る]ボタン
	public PageReference doBack(){
		return getBackPage();
	}

	//完了画面_ページアクション
	public PageReference pageAction_Comp(){
		if(!isReissue){
			return getBackPage();
		}
		return null;
	}

	private PageReference getBackPage() {
		if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_CLI)) {
			return Page.E_LoginCustomer;
		} else if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_AGE)) {
			return Page.E_LoginAgent;
		} else {
			return Page.E_Exception;
		}
	}
}