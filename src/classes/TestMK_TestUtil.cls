public with sharing class TestMK_TestUtil {
	/**
	 * MK_CampaignTarget__c作成
	 * @param isInsert インサートフラグ
	 * @return MK_CampaignTarget__c: キャンペーンターゲットレコード
	 */
	public static MK_CampaignTarget__c createMK_CampaignTarget(Boolean isInsert){
		MK_CampaignTarget__c ret = new MK_CampaignTarget__c(
										AgentMailAddress__c = 'a@example.com'
										,ContractantMailAddress__c = 'b@example.com');
		if(isInsert) insert ret;
		return ret;
	}

	/**
	 * MK_CampaignTarget__c作成
	 * @param isInsert インサートフラグ
	 * @param aid: 募集人
	 * @return MK_CampaignTarget__c: キャンペーンターゲットレコード
	 */
	public static MK_CampaignTarget__c createMK_CampaignTarget(Boolean isInsert,Id aid){
		MK_CampaignTarget__c ret = new MK_CampaignTarget__c(Agent__c = aid);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_CampaignTarget__c作成
	 * @param isInsert インサートフラグ
	 * @param CLTPF_CLNTNUM: 顧客番号
	 * @param contractantKana: 契約者名カナ
	 * @return MK_CampaignTarget__c: キャンペーンターゲットレコード
	 */
	public static MK_CampaignTarget__c createMK_CampaignTarget(Boolean isInsert,String CLTPF_CLNTNUM,String contractantKana){
		MK_CampaignTarget__c ret = new MK_CampaignTarget__c(
											CLTPF_CLNTNUM__c = CLTPF_CLNTNUM
											,ContractantKana__c = contractantKana
											,ContractantMailAddress__c = 'a@example.com'
											);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_CampaignTarget__c作成
	 * @param isInsert インサートフラグ
	 * @param CLTPF_CLNTNUM: 顧客番号
	 * @param contractantKana: 契約者名カナ
	 * @return MK_CampaignTarget__c: キャンペーンターゲットレコード
	 */
	public static MK_CampaignTarget__c createMK_CampaignTarget(Boolean isInsert,ID cmid,String CLTPF_CLNTNUM,String contractantKana){
		MK_CampaignTarget__c ret = new MK_CampaignTarget__c(
											Campaign__c = cmid
											,CLTPF_CLNTNUM__c = CLTPF_CLNTNUM
											,ContractantKana__c = contractantKana
											,ContractantMailAddress__c = 'a@example.com'
											,Name = '名前'
											);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * キャンペーンターゲットID無しでinsert不可なため、インサートフラグtrueは使用不可
	 * @param isInsert インサートフラグ
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert){
		MK_leadTMP__c ret = new MK_leadTMP__c();
		if(isInsert) return null;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param ctId: キャンペーンターゲットID
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert,Id ctId){
		MK_leadTMP__c ret = new MK_leadTMP__c(MK_CampaignTarget__c = ctId);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param email: メール
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert,String email){
		MK_leadTMP__c ret = new MK_leadTMP__c(Email__c = email);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param ctId: キャンペーンターゲットID
	 * @param email: メール
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert,Id ctId,String email){
		MK_leadTMP__c ret = new MK_leadTMP__c(
								 MK_CampaignTarget__c = ctId
								,LastName__c = '姓'
								,LastNameKana__c = 'セイ'
								,FirstName__c = '名'
								,FirstNameKana__c = 'メイ'
								,Email__c = email
								,Company__c = 'company'
								,CreateLeadFlag__c = true
								);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param ctId: キャンペーンターゲットID
	 * @param lastName: 姓
	 * @param lastNameKana: 姓カナ
	 * @param firstName: 名
	 * @param firstNameKana: 名カナ
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert,Id ctId,String lastName,String lastNameKana,String firstName,String firstNameKana){
		MK_leadTMP__c ret = new MK_leadTMP__c(
								 MK_CampaignTarget__c = ctId
								,LastName__c = lastName
								,LastNameKana__c = lastNameKana
								,FirstName__c = firstName
								,FirstNameKana__c = firstNameKana
								);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param ctId: キャンペーンターゲットID
	 * @param recNum: レコード数
	 * @return List<MK_leadTMP__c>: リードTMPレコードリスト
	 */
	public static List<MK_leadTMP__c> createMK_leadTMP(Boolean isInsert,Id ctId,Integer recNum){
		List<MK_leadTMP__c> ret = new List<MK_leadTMP__c>();
		for(Integer i = 0;i<recNum;I++){
			ret.add(createMK_leadTMP(false,ctId));
		}
		if(isInsert) insert ret;
		return ret;
	}
	/*
	 * Lead作成(項目とAPI参照名にずれがあるため注意)
	 * @param company 代表者名
	 * @param lastName 商号
	 * @param name_kana 代表者名仮名
	 * @param email メール
	 * @param state 都道府県
	 * @param phone 電話番号
	 */
	public static Lead createLead(Boolean isInsert,String company,String lastName,String name_kana,String email,String state,String phone){
		Lead ret = new Lead(
					 Company = company
					,LastName = lastName
					,name_kana__c = name_kana
					,Email = email
					,State = state
					,Phone = phone
					);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param ctId: キャンペーンターゲットID
	 * @param createLeadFlag Lead作成済みフラグ
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert,ID ctid,Boolean createLeadFlag){
		MK_leadTMP__c ret = new MK_leadTMP__c(
								 MK_CampaignTarget__c = ctId
								,CreateLeadFlag__c = createLeadFlag
								);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * MK_leadTMP__c作成
	 * @param isInsert インサートフラグ
	 * @param ctId: キャンペーンターゲットID
	 * @param lastName 姓
	 * @param company 会社名
	 * @return MK_leadTMP__c: リードTMPレコード
	 */
	public static MK_leadTMP__c createMK_leadTMP(Boolean isInsert,ID ctid,String lastName,String company){
		MK_leadTMP__c ret = new MK_leadTMP__c(
								 MK_CampaignTarget__c = ctId
								,LastName__c = lastName
								,Company__c = company
								);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * Account作成
	 * @param isInsert インサートフラグ
	 * @param Name: 代理店事務所名
	 * @return Account: 代理店事務所レコード
	 */
	public static Account createAccount(Boolean isInsert,String name){
		Account ret = new Account(Name = name);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * Contact作成
	 * @param isInsert インサートフラグ
	 * @return Contact: 募集人レコード
	 */
	public static Contact createContact(Boolean isInsert){
		Contact ret = new Contact();
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * Contact作成
	 * @param isInsert インサートフラグ
	 * @param lastName: 姓
	 * @return Contact: 募集人レコード
	 */
	public static Contact createContact(Boolean isInsert,String lastName){
		Contact ret = new Contact(
						 LastName = lastName
						);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * Contact作成
	 * @param isInsert インサートフラグ
	 * @param lastName: 姓
	 * @param aid: 代理店事務所名
	 * @return Contact: 募集人レコード
	 */
	public static Contact createContact(Boolean isInsert,String lastName,id aid){
		Contact ret = new Contact(
						 LastName = lastName
						,AccountId = aid
						);
		if(isInsert) insert ret;
		return ret;
	}

		/**
	 * MK_CampaignTarget__c作成
	 * @param isInsert インサートフラグ
	 * @param aid: 募集人
	 * @return MK_CampaignTarget__c: キャンペーンターゲットレコード
	 */
	public static MK_CampaignTarget__c createMK_CampaignTarget(Boolean isInsert,id cmid,Id aid){
		MK_CampaignTarget__c ret = new MK_CampaignTarget__c(
										 Campaign__c = cmid
										,Agent__c = aid
										,Name = '契約者名'
										,AgentMailAddress__c = 'a@example.com'
										,ContractantMailAddress__c = 'b@example.com');
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * User作成
	 * @param isInsert: insertするかどうか
	 * @param userName: userName
	 * @param profileID: プロファイルID
	 * @param ConId:
	 * @return 作成したユーザ
	 */
	public static User createUser(Boolean isInsert, String userName, ID profileID,ID ConId) {
		User src = new User(
				Username = userName,
				Lastname = 'LastName',
				Email = 'qawsedrftgyh@example.nn.baton',
				Alias = 'Alias',
				CommunityNickname = 'CommunityNickname',
				TimeZoneSidKey = 'Asia/Tokyo',
				LocaleSidKey = 'ja_JP',
				EmailEncodingKey = 'ISO-2022-JP',
				ProfileId = profileID,
				LanguageLocaleKey = 'ja',
				ContactId = ConId
				);
		if (userName == null) {
			src.Username = 'gyfsdfhsjf@example.nn.baton';
		}
		if (profileID == null) {
			src.ProfileId = UserInfo.getprofileid();
		}
		if (isInsert) {
			insert src;
		}
		return src;
	}
	/**
	 * CampaignManagement__c作成
	 * @param isInsert: insertするかどうか
	 * @param fromDate: 開始日
	 * @param toDate: 終了日
	 * @return キャンペーン管理
	 */
	public static CampaignManagement__c createCampaignManagement(Boolean isInsert,Date fromDate,Date toDate){
		CampaignManagement__c ret = new CampaignManagement__c(
										Incentive__c = 'インセンティブ'
										,title__c = 'タイトル'
										,ContractantIncentive__c = '紹介者限定インセンティブ'
										,Information__c = 'インフォメーション'
										,Overview__c = '概要'
										,Introduction__c = '導入'
										,E_MessageMaster_UpsertKey__c = 'コンテストレギュレーションキー'
										,FromDate__c = fromDate
										,ToDate__c = toDate
										,Name = '名前'
										);
		if(isInsert) insert ret;
		return ret;
	}
	/**
	 * Attachment作成
	 * @param isInsert: insertするかどうか
	 * @param parentid: 親ID
	 * @param name: ファイル名
	 * @param body: 内容
	 * @return 添付ファイル
	 */
	public static Attachment createAttachment(Boolean isInsert,Id parentId,String name,Blob body){
		Attachment src = new Attachment(
										ParentId = parentId,
										Name = name,
										Body = body
										);
		if(IsInsert){
			insert src;
		}
		return src;
	}

}