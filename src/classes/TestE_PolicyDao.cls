@isTest
public with sharing class TestE_PolicyDao {
	
	/**
	 * E_PolicyDao 分岐網羅
	 */
	private static testMethod void testE_PolicyDao() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c P = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();

			set<id> ids = new set<id>{null};
			
			system.assertEquals( E_PolicyDao.getEPoliCustomerResearch('select id from E_Policy__c where name = \'testString\'').size() , 0);
			System.assertEquals( E_PolicyDao.getRecById ('dummyId') , null);
			system.assert( E_PolicyDao.getRecById (p.id) != null);
			system.assertEquals( E_PolicyDao.getRecByIdChildSVFPF('dummyId') , null);
			system.assert( E_PolicyDao.getRecByIdChildSVFPF(p.id) != null);
			system.assert( E_PolicyDao.getRecByIdFrProcessHistoryCid(con.id) != null);
			system.assert( E_PolicyDao.getRecByIdFrProcessHistoryPid(p.id) != null);
			//system.assert( E_PolicyDao.getRecForDLHistoryById(p.id) != null);
			//system.assertEquals( E_PolicyDao.getRecForDLHistoryById('dummyId') , null);
			system.assertEquals( E_PolicyDao.getRecMapByIds(ids).size() , 0);

			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(null, resultPage);
	}

	/**
	 * Idから保険契約ヘッダレコードを取得
	 */
	@isTest static void getRecByIdChildSVFPFFundTransferTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		E_Policy__c result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecByIdChildSVFPFFundTransfer(policy.Id);
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result.Id);
	}

	/**
	 * 手続履歴画面
	 * ContactIdからレコードを取得
	 */
	@isTest static void getRecByIdFrProcessHistoryCidTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		E_Policy__c result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecByIdFrProcessHistoryCid(con.Id);
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result.Id);
	}

	/**
	 * 手続履歴画面
	 * Idからレコードを取得
	 */
	@isTest static void getRecByIdFrProcessHistoryPidTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		E_Policy__c result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecByIdFrProcessHistoryPid(policy.Id);
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result.Id);
	}

	/**
	 * Idセットから Map<Id, E_Policy__c> を取得
	 */
	@isTest static void getRecMapByIdsTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		Set<Id> idSet = new Set<Id>();
		idSet.add(policy.Id);

		//結果
		Map<Id, E_Policy__c> result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecMapByIds(idSet);
			Test.stopTest();
		}

		system.assertEquals(idSet, result.keySet());
	}

	/**
	 * 証券番号からレコードを取得
	 */
	@isTest static void getRecByIdCHDRNUMTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		E_Policy__c result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecByIdCHDRNUM(policy.COMM_CHDRNUM__c);
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result.Id);
	}

	/**
	 * [IRIS]契約者一覧
	 */
	@isTest static void getRecsIRISOwnerListTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy02(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, false);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		List<E_Policy__c> result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecsIRISOwnerList('');
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result[0].Id);
	}

	/**
	 * [IRIS] 失効契約用 →　メソッドがE_COVPFDaoに移動
	 */
	/*@isTest static void getRecsIRISLapsedPolicyTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy02(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, true);
		E_COVPF__c covpf = CreateCOVPF(true, policy.Id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		List<E_Policy__c> result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecsIRISLapsedPolicy('');
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result[0].Id);
	}*/

	/**
	 * [IRIS] 消滅契約用
	 */
	@isTest static void getRecsIRISVanishPolicyTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy02(true, con.Id, E_Const.POLICY_RECORDTYPE_DCOLI, true);
		E_COVPF__c covpf = CreateCOVPF(true, policy.Id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		List<E_Policy__c> result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecsIRISVanishPolicy('');
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result[0].Id);
	}

	/**
	 * [IRIS] 契約応答月用
	 */
	@isTest static void getRecsIRISPolicysTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(false, con.Id);
		String sDate = String.valueOf(Date.Today().addMonths(3));
		sDate = sDate.replace('-', '');
		policy.COMM_OCCDATE__c = sDate;
		RecordType rtype = [Select Id From RecordType Where DeveloperName = :E_Const.POLICY_RECORDTYPE_COLI Limit 1];
		policy.RecordTypeId = rtype.Id;
		insert policy;
		E_COVPF__c covpf = CreateCOVPF(true, policy.Id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		List<E_Policy__c> result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			//3か月分の月を取得
			String termMonth = getResponseMonths(1);
			result = E_PolicyDao.getRecsIRISPolicys(termMonth, '');
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result[0].Id);
	}

	/**
	 * [IRIS] 契約応答月団体用
	 */
	@isTest static void getRecsIRISGrupPolicysTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.Id);
		E_COVPF__c covpf = CreateCOVPF(true, policy.Id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		List<E_Policy__c> result;

		//テストユーザで機能実行開始
		System.runAs(u){
			Test.startTest();
			result = E_PolicyDao.getRecsIRISGrupPolicys('09876543', '');
			Test.stopTest();
		}

		system.assertEquals(policy.Id, result[0].Id);
	}
	/**
	 * [AMS] 契約に紐づく4帳票
	 */
	@isTest static void getPeriodNoticesByPlcyIdTest(){
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.id);
		E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		//結果
		E_Policy__c result;

		Test.startTest();
		result = E_PolicyDao.getPeriodNoticesByPlcyId(policy.Id);
		Test.stopTest();

		system.assertEquals(policy.Id, result.Id);
	}


	/**
	 * 保険契約検索 契約者
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> policy = E_PolicyDao.getRecsIRISPolicySearchBox('テスト',I_Const.SEARCH_TYPE_OWNER);
			test.stopTest();

			system.assertEquals(policy.size(), 1);
		}
	}

	/**
	 * 保険契約検索 被保険者
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest2(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> policy = E_PolicyDao.getRecsIRISPolicySearchBox('テスト',I_Const.SEARCH_TYPE_INSURED);
			test.stopTest();

			system.assertEquals(policy.size(), 1);
		}
	}

	/**
	 * 保険契約検索 証券番号
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest3(){
		E_Policy__c policy = TestUtil_I_SearchBox.createEmptyPolicy(false, null);
		policy.COMM_CHDRNUM__c = '12345678';
		insert policy;

		test.startTest();
		List<E_Policy__c> pol = E_PolicyDao.getRecsIRISPolicySearchBox('12345678',I_Const.SEARCH_TYPE_POLICY_NO);
		test.stopTest();

		system.assertEquals(pol.size(), 1);
	}

	/**
	 * 保険契約検索 契約者_and検索
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest4(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		TestUtil_I_SearchBox.createPolicys();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> pol = E_PolicyDao.getRecsIRISPolicySearchBox('テスト契約者 1',I_Const.SEARCH_TYPE_OWNER,'and');
			test.stopTest();

			system.assertEquals(pol.size(), 1);
		}
	}

	/**
	 * 保険契約検索 契約者_or検索
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest5(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		TestUtil_I_SearchBox.createPolicys();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> pol = E_PolicyDao.getRecsIRISPolicySearchBox('テスト契約者 1',I_Const.SEARCH_TYPE_OWNER,'or');
			test.stopTest();

			system.assertEquals(pol.size(), 4);
		}
	}

	/**
	 * 保険契約検索 被保険者_and検索
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest6(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		TestUtil_I_SearchBox.createPolicys();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> pol = E_PolicyDao.getRecsIRISPolicySearchBox('テスト被保険者 1',I_Const.SEARCH_TYPE_INSURED,'and');
			test.stopTest();

			system.assertEquals(pol.size(), 1);
		}
	}

	/**
	 * 保険契約検索 被保険者_or検索
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest7(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, null, true);
		TestUtil_I_SearchBox.createPolicys();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');

		System.runAs(u){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> pol = E_PolicyDao.getRecsIRISPolicySearchBox('テスト被保険者 1',I_Const.SEARCH_TYPE_INSURED,'or');
			test.stopTest();

			system.assertEquals(pol.size(), 4);
		}
	}

	/**
	 * 保険契約検索 証券番号
	 */
	@isTest static void getRecsIRISPolicySearchBoxTest8(){
		E_Policy__c policy = TestUtil_I_SearchBox.createEmptyPolicy(false, null);
		policy.COMM_CHDRNUM__c = '12345678';
		insert policy;

		test.startTest();
		List<E_Policy__c> pol = E_PolicyDao.getRecsIRISPolicySearchBox('12345678',I_Const.SEARCH_TYPE_POLICY_NO,'and');
		test.stopTest();

		system.assertEquals(pol.size(), 1);
	}

	/**
	 * 検索ボックス（保険契約ヘッダ(消滅)） 契約者
	 */
	@isTest static void getRecsIRISVanishPolicySearchBoxTest1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, E_Const.POLICY_RECORDTYPE_DCOLI, true);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> policy = E_PolicyDao.getRecsIRISVanishPolicySearchBox('テスト',I_Const.SEARCH_TYPE_OWNER,'');
			test.stopTest();

			system.assertEquals(policy.size(), 0);
		}
	}

	/**
	 * 検索ボックス（保険契約ヘッダ(消滅)） 被保険者
	 */
	@isTest static void getRecsIRISVanishPolicySearchBoxTest2(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, E_Const.POLICY_RECORDTYPE_DCOLI, true);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> policy = E_PolicyDao.getRecsIRISVanishPolicySearchBox('テスト',I_Const.SEARCH_TYPE_INSURED,'');
			test.stopTest();

			system.assertEquals(policy.size(), 0);
		}
	}

	/**
	 * 検索ボックス（保険契約ヘッダ(消滅)） 証券番号
	 */
	@isTest static void getRecsIRISVanishPolicySearchBoxTest3(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		util.createStandardPolicy(true, E_Const.POLICY_RECORDTYPE_DCOLI, true);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> policy = E_PolicyDao.getRecsIRISVanishPolicySearchBox('テスト',I_Const.SEARCH_TYPE_POLICY_NO,'');
			test.stopTest();

			system.assertEquals(policy.size(), 0);
		}
	}

	/**
	 * 検索ボックス（保険契約ヘッダ(消滅)）
	 */
	@isTest static void getRecsIRISLapsedPolicySearchBoxTest1(){
		TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();
		Set<Id> policyIds = new set<Id>();
		policyIds.add(util.createStandardPolicy(true, null, true).Id);

		System.runAs(util.aUser){
			TestUtil_I_SearchBox.createBasePermissions(util.aUser.Id);

			test.startTest();
			List<E_Policy__c> policy = E_PolicyDao.getRecsIRISLapsedPolicySearchBox(policyIds);
			test.stopTest();

			system.assertEquals(policy.size(), 1);
		}
	}

	/**
	 * カウント（指定されたContactが契約者、または被保険者にセットされているか）
	 */
	@isTest static void getOwnerOrInsuredCountTest1(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.Id);

		System.runAs(u){

			test.startTest();
			Integer cnt = E_PolicyDao.getOwnerOrInsuredCount(con.Id);
			test.stopTest();

			system.assertEquals(cnt, 1);
		}
	}

	/**
	 * getRecsByContactID
	 */
	@isTest static void getRecsByContactIDTest1(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.Id);

		System.runAs(u){

			test.startTest();
			List<E_Policy__c> policys = E_PolicyDao.getRecsByContactID(con.Id);
			test.stopTest();

			system.assertEquals(policys.size(), 1);
		}
	}
	/**
	 * getRecByIdForIRISColi
	 */
	@isTest static void getRecByIdForIRISColiTest(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount');
		Contact con = CreateContact(true, acc.Id);
		E_Policy__c policy = CreatePolicy(true, con.Id);

		System.runAs(u){

			test.startTest();
			E_Policy__c resulPolicy = E_PolicyDao.getRecByIdForIRISColi(policy.Id);
			test.stopTest();

			system.assertEquals(resulPolicy.id,policy.Id);
		}
	}

	/**
	 * ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	private static User createUser(Boolean isInsert, String LastName, String profileDevName){
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}
	private static Map<String, Id> pMap;
	private static Map<String, Id> getProfileIdMap(){
		if(pMap != null){
			return pMap;
		}
		pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}
	
	/**
	 * 取引先作成
	 * @param isInsert: whether to insert
	 * @param accName: 取引先名
	 * @return Account: 取引先
	 */
	private static Account createAcc(Boolean isInsert, String accName){
		Account src = new Account(
				  name = accName
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}
   
	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: 取引先Id 
	 * @return Contact: 取引先責任者
	 */
	private static Contact CreateContact(Boolean isInsert, Id accId){
		Contact src = new Contact(
				  LastName = 'TestContact'
				 ,AccountId = accId
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}
	
	/**
	 * 契約ヘッダ作成
	 * @param isInsert: whether to insert
	 * @param accId: 取引先Id 
	 * @return E_Policy__c: 契約ヘッダ
	 */
	private static E_Policy__c CreatePolicy(Boolean isInsert, Id conId){
		String sDate = String.valueOf(Date.Today().addMonths(2));
		sDate = sDate.replace('-', '');
		E_Policy__c src = new E_Policy__c(
				 Contractor__c = conId
				 ,COMM_CHDRNUM__c = '01234567|8'
				 ,COLI_GRUPNUM__c = '09876543'
				  
				 ,COMM_OCCDATE__c = sDate
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}

	/**
	 * 契約ヘッダ作成 レコードタイプ・失効契約指定
	 * @param isInsert: whether to insert
	 * @param accId: 取引先Id
	 * @param recordType: レコードタイプ
	 * @param lost: 失効契約フラグ
	 * @return E_Policy__c: 契約ヘッダ
	 */
	private static E_Policy__c CreatePolicy02(Boolean isInsert, Id conId, String recordTypeName, Boolean lost){
		RecordType rtype = [Select Id From RecordType Where DeveloperName = :recordTypeName Limit 1];

		E_Policy__c src = new E_Policy__c();
		src.Contractor__c = conId;
		src.COMM_CHDRNUM__c = '01234567|8';
		src.RecordTypeId = rtype.Id;
		src.COLI_GRUPNUM__c = '09876543';
		String sDate = String.valueOf(Date.Today().addMonths(-6));
		sDate = sDate.replace('-', '');
		src.COMM_STATDATE__c = sDate;
		if(lost){
			src.COMM_STATCODE__c = I_Const.LAPS_POLICY_STATCODE;
			src.COMM_ZRSTDESC__c = I_Const.LAPS_POLICY_ZRSTDESC;
		}

		if (isInsert) {
			insert src;
		}
		return src;
	}

	/**
	 * 契約特約作成 失効契約
	 * @param isInsert: whether to insert
	 * @param accId: 契約ヘッダId
	 * @return E_COVPF__c: 個人保険特約
	 */
	private static E_COVPF__c CreateCOVPF(Boolean isInsert, Id policyId){
		E_COVPF__c covpf = new E_COVPF__c();
		covpf.E_Policy__c = policyId;
		covpf.COLI_ZCRIND__c = I_Const.COVPF_COLI_ZCRIND_C;
		String sDate = String.valueOf(Date.Today().addYears(-3));
		sDate = sDate.replace('-', '');
		covpf.COMM_STATDATE__c = sDate;

		if (isInsert) {
			insert covpf;
		}
		return covpf;
	}

	//今日＋指定月先3か月の月を連結して取得 	今月からの場合は0を指定してください　1-3か月の場合は『1』を指定
	private static String getResponseMonths(Integer nextFrom){
		//nextFrom から3か月分の月を取得
		Date sysDate = E_Util.SYSTEM_TODAY();
		Integer m1 = sysDate.toStartOfMonth().addMonths(nextFrom).month();
		Integer m2 = sysDate.toStartOfMonth().addMonths(nextFrom+1).month();
		Integer m3 = sysDate.toStartOfMonth().addMonths(nextFrom+2).month();
		return String.valueOf(m1) + ',' + String.valueOf(m2) + ',' + String.valueOf(m3);
	}
}