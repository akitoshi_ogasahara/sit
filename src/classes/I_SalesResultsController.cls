public with sharing class I_SalesResultsController extends I_AbstractSalesResultsController{

	//営業部
	private final String HIERARCHY_BR = 'BR';
	//MR
	private final String HIERARCHY_MR = 'MR';

	//支社コード
	public String brunchCode {get; private set;}
	//営業部名
	public String unitName {get; private set;}
	//MR名
	public String mrName {get; private set;}
	//表示タイプ
	public String viewType {get; set;}
	//社内挙積情報
	public E_EmployeeSalesResults__c employeeSalesResults {get; private set;}

	//コンストラクタ
	public I_SalesResultsController() {
		super();
		//デフォルト
		viewType = '0';
		getMrNameById();
	}
	public String getPreviewURL(){
		if(String.isNotBlank(unitid)){
			return 'IRIS_SalesResultsPrint?brno=' + unitid;
		}else if(String.isNotBlank(mrid)){
			return 'IRIS_SalesResultsPrint?mrid=' + mrid;
		}
		return null;
	}
	/**
	 * MRユーザの場合にtureを返す
	 */
	public Boolean getHasSREmployeePermission(){
		return access.hasSREmployeePermission();
	}

	/**
	 * メニューキー
	 */
	protected override String getLinkMenuKey() {
		return 'agencyList';
	}


	//選択されたMRIDと、それに紐づく支社コード(ID管理の照会者コード)を取得
	public String getBrunchCodeBymrId(){
		E_IDCPF__c eidc = E_IDCPFDaoWithout.getEidcCodeByMrId(mrid);
		return (String.isBlank(eidc.ZINQUIRR__c))? '':eidc.ZINQUIRR__c.right(2);

	}

	//URLパラメータからMR名を取得
	public String getMrNameById(){
		User us = new User();

		if(String.isNotBlank(mrid)){
			us = [SELECT Name,Unit__c FROM User WHERE Id =:mrid];

//			isMRHeader = true;
			unitName = us.Unit__c;
			mrName = us.Name;

		}else if(String.isBlank(unitId)){
			us = [SELECT Name,Unit__c FROM User WHERE Id =:UserInfo.getUserId()];
//			isUnitHeader = true;
			unitName = us.Unit__c;
			mrName = us.Name;
		}
		return mrName;
	}

	/**
	 * リダイレクト
	 */
	protected override PageReference doRedirect() {

		//代理店ユーザは判定なしでリダイレクト
		if(!(access.isEmployee())){
			return redirectAgencySalesPage();
		}
		//パラメータなし（ログインユーザ判定）
		if(String.isBlank(unitId) && String.isBlank(mrId)){
			if(access.ZINQUIRR_is_BRAll()){
				return redirectUnitPage();
			}else{
				//unitId = access.getAccessKindFromThree();
				mrid= UserInfo.getUserId();
				//viewType = '2';
			}
		}
		//unitIdあり
		if(String.isNotBlank(unitId)){
			if(unitId.equals('00')){
				unitName= '本社営業部';
			}else{
				E_Unit__c units = E_UnitDao.getUnitRecByBRCode(unitId);
				unitName = units.Name;
			}
		}
		//mrIdあり
		else if(String.isNotBlank(mrId)){
			brunchCode = getBrunchCodeBymrId();
		}
		system.debug('mrid='+mrid);
		//条件+作成日が最新のもの一件
		String soqlWhere = 'Where BusinessDate__c = \'' + getBizDate() + '\' ';
		//営業部
		if(String.isNotBlank(unitId)){
			viewType = '1';
			soqlWhere += 'AND Hierarchy__c = \'' + HIERARCHY_BR + '\' '
					  + 'AND E_Unit__r.BRANCH__c = \''  + unitId + '\' ';
		}else if(String.isNotBlank(mrId)){
			viewType = '2';
			soqlWhere += 'AND Hierarchy__c = \'' + HIERARCHY_MR + '\' '
					  + 'AND MR__c = \''  + mrId + '\' ';
		}else{
			//エラー
			pageMessages.addErrorMessage(getMSG().get('ERR|999'));
		}

		String soql = 'Select id, BusDate__c From E_EmployeeSalesResults__c ';
		soql += soqlWhere;

		List<E_EmployeeSalesResults__c> recs = Database.query(soql);
		employeeSalesResults = (recs.size() == 0) ? null : recs[0];
		if(employeeSalesResults != null){
			isShowSR = true;
		}
		return null;
	}

	/**
	 * 個別でチェック判定
	 */
	protected override boolean isValidate() {
		//社員以外のアクセス不正
		if(!access.isEmployee()){
			return false;
		}
		//所属内の情報以外は不正とする
		//BR**はすべて参照可能
		if(access.ZINQUIRR_is_BRAll()){
			return true;
		}
		//Unitidの場合は、照会者コードで判定
		if(String.isNotBlank(unitId)){
			return access.getAccessKindFromThree() == unitid;
		}
		//mridの場合は、照会者コードで判定
		if(String.isNotBlank(mrId)){
			return access.getAccessKindFromThree() == getAccessKindFromThree(mrid);
		}
		return false;
	}
	private String getAccessKindFromThree(id userid) {
		E_IDCPF__c idcpf = E_IDCPFDaoWithout.getRecsByUserID(userid);
		if (idcpf != null) {
			if (idcpf.ZINQUIRR__c != null && idcpf.ZINQUIRR__c.length() > 2) {
				return idcpf.ZINQUIRR__c.substring(2);
			}
		}
		return null;
	}
	/**
	 * BRNo取得
	 */
	public String getBRNo(){
		User user = [SELECT Unit__c FROM User WHERE Id =: mrid];
		E_Unit__c unitRec = [SELECT BRANCH__c FROM E_Unit__c WHERE Name =:user.Unit__c];
		return unitRec.BRANCH__c;
	}
	
	/**
	 * IRIS_AgencySRInfoディスクレーマ
	 */
	public List<String> getASRDisclaimer (){
		return new List<String>();
	}
}