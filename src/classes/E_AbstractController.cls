public abstract class E_AbstractController {

	public E_AccessController access;
	public E_AccessController getAccess(){
		return access;
	}

	public E_PageMessagesHolder getPageMessages(){
			return E_PageMessagesHolder.getInstance();
	}

	public String errorMsgCode;

	public PageReference pageRef;

	// [IRIS]
	public E_IRISHandler iris {get; private set;}

	public E_AbstractController() {
		access = E_AccessController.getInstance();
		iris = E_IRISHandler.getInstance();
	}

	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}

	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}

	/**
	 * 整合性、アクセス権判定
	 */
	public PageReference doAuth(E_Const.ID_KIND kind, Id id) {

		boolean isAuth = true;
		PageReference pageRef = null;
		try {
			// 20180403 メンテナンス対応 START
			//メンテナンス中でもテストクラスが実行できるよう、テスト時はメンテナンス機能をスキップする
			//メンテナンス中 かつ　メンテナンス中にログイン可能なIPアドレスの範囲外の場合はメンテナンス画面に遷移
			if(System.Label.E_MAINTENANCE == '1' && !E_Util.ismatchIpRange(System.Label.E_MAINTENANCE_IP_ADRS) && !Test.isRunningTest() ){
				return Page.E_InMaintenance;
			}
			// 20180403 メンテナンス対応 END

			// 20171101 銀行代理店開放 START
			if(kind != E_Const.ID_KIND.LOGIN && !access.accessIRIS()){
				//エラーページへ
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_IPADDRESS);
				return pageRef;
			}
			// 20171101 銀行代理店開放 END

			//遷移先URLをセット
			E_CookieHandler.setCookieReferer(access.getReferer(kind));
			//service hour menu
			pageRef = access.isServiceHourMenu(kind);
			if (pageRef != null) {
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_SERVICEHOUR);
				return pageRef;
			}


			//ログイン関連チェック
			pageRef = access.isLoginControl(kind);
			if (pageRef != null) {
				return pageRef;
			}

			// [IRIS][Day1.5]ID管理が存在しない場合はエラーページへ遷移させる
			if(access.idcpf == null && kind != E_Const.ID_KIND.LOGIN){
                pageRef = Page.E_ErrorPage;
                pageRef.getParameters().put('code', E_Const.ERROR_MSG_NO_IDCPF);
                return pageRef;
			}

			//コンプラ画面の場合は、以下の処理を行わずページを表示する
			if(kind == E_Const.ID_KIND.COMPLIANCE){
				return null;
			}

			//機能制限、オブジェクト権限、パラメータチェック
			if (kind == E_Const.ID_KIND.DOWNLOAD) {
				isAuth = doAuthDownload();
			} else if (kind == E_Const.ID_KIND.CUSTOMER_SEARCH) {
				isAuth = doAuthCustomerSearch();
			} else {
				isAuth = true;
			}

			//個別チェック
			if (isAuth) {
				isAuth = isValidate();
			}

			//遷移先の決定
			if (!isAuth) {
				//アクセス権検証NGの場合
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', errorMsgCode);
			}else{
				//リダイレクトする場合にTrue
				pageRef = redirectTo();
			}

			//住友ユーザチェック
			if(access.isSumiseiIRISUser() && (!access.user.IsActive || access.idcpf.ZSTATUS01__c != '1')){
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_ACTIVEUSER);
			}
		} catch (Exception e) {
			getPageMessages().addErrorMessage(e.getMessage());
		}
		return pageRef;
	}

	/**
	 *		ページリダイレクト処理
	 */
	protected virtual PageReference redirectTo(){
		//スプリントバックログ対応 #2 ここから 2017/1/18
		//パスワード変更画面へ促すかチェック
		//条件に当てはまればパスワード変更案内画面へ
		if(access.isSuggestPWChange()){
			return Page.IRIS_SuggestChangePW;
		}
		//スプリントバックログ対応 #2 ここまで

		//メールアドレス登録画面へ促すかチェック
		//条件に当てはまればメールアドレス変更画面へ
		if(access.isEmailSuggestUser()){		//NNLinkモードで条件を満たす場合はどうなるか？UIはNNLinkへ
			PageReference pr = Page.E_EMailAddressChange;
			if(iris.getIsIRIS()){
				pr = Page.IRIS_ChangeUserID;
				pr.getParameters().put('suggestion', '1');
			}
			return pr;
		}

		return null;
	}

	/**
	 * ページタイトル
	 */
	protected String pgTitle;           //サブクラスにて設定してください。
	public String getPageTitle() {
		String sTitle = '';     //'['+ E_Const.SYSTEM_NAME +'] - '; faviconつけるのでシステム名削除
		if(String.isNotBlank(pgTitle)){
			sTitle += pgTitle;
		}else{
			sTitle = '['+ E_Const.SYSTEM_NAME +']';
		}
		return sTitle;
	}

	/**
	 * 連携日時取得（契約関連）
	 */
	public String getDataSyncDate() {
		return E_Util.formatDataSyncDate()  + '　' + '現在の契約内容';
	}

	/**
	 * 現在日時取得（投資信託関連）
	 */
	public String getDataSyncDateInvestment() {
		return E_Util.formatDataSyncDate()  + '　' + '現在';
	}

	/**
	 * 連携日時取得（ダウンロード）
	 */
	public String getDataSyncDateDownload() {
		return 'データ作成日　' + E_Util.formatDataSyncDate();
	}

	/**
	 * welcome取得
	 */
	public String getWelcome() {
		if(access.isSumiseiUser()){
			return null;
		}
		return E_Util.getWelcome();
	}

	/**
	 * 個別でチェック判定
	 */
	protected virtual boolean isValidate() {
		return true;
	}

	/**
	 * ダウンロード関連機能判定
	 */
	protected virtual boolean doAuthDownload () {
		//プロファイル＋ID管理種別＋ID管理各権限フラグ
		if(!access.canDownloads()){
			errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
			return false;
		//オブジェクトアクセス制限
		}else if (!access.isAccessibleDownload()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		return true;
	}

	/**
	 * ダウンロード 手数料のご案内 機能判定
	 */
	public boolean getCanDisplayFeeGuidance () {
		if(access.canDisplayFeeGuidance()) {
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
		return false;
	}

	/**
	 * ダウンロード 手数料明細(生保/変額商品/ボーナス) 機能判定
	 */
	public boolean getCanDisplayFeeData () {
		if(access.canDisplayFeeData()) {
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
		return false;
	}

	/**
	 * ダウンロード 保有契約(個人保険) 機能判定
	 */
	public boolean getCanDisplayHoldPolicyColi () {
		if(access.canDisplayHoldPolicyColi()) {
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
		return false;
	}

	/**
	 * ダウンロード 保有契約(SPVA) 機能判定
	 */
	public boolean getCanDisplayHoldPolicySpva () {
		if(access.canDisplayHoldPolicySpva()) {
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
		return false;
	}

	/**
	 * ダウンロード 手続きの履歴 機能判定
	 */
	public boolean getCanDisplayPolicyHisotry () {
		if(access.canDisplayPolicyHisotry()){
			return true;
		}
		errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
		return false;
	}


	/**
	 * 顧客検索機能判定
	 */
	protected virtual boolean doAuthCustomerSearch () {
		//機能制限確認
		if (!access.canCustomerSearch()) {
			errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
			return false;
		}
		//オブジェクトアクセス制限
		if (!access.isAccessiblePolicy()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		return true;
	}

	/**
	 * 共同GW経由判定
	 */
	public boolean getIsCommonGateway() {
		return access.isCommonGateway();
	}

	/**
	 * 戻るボタン
	 */
	public virtual PageReference doReturn(){
		return new PageReference(E_CookieHandler.getCookieReferer());
	}

	/*
	// ISS経由時のクッキー設定メソッド
	protected void setIssCookie(){
		//抽象クラスのクッキー設定メソッド
		if(E_Util.getIsIssParam()){
			E_CookieHandler.setCookieIssSessionId();
		}
	}
	*/

	/**
	 *	住生ユーザの場合True
	 */
	public Boolean getIsSumiseiUser(){
		return access.isSumiseiUser();
	}

	/**
	 *	IRIS住生表示可能の場合True
	 */
	public Boolean getCanViewSumiseiIRIS(){
		return access.canViewSumiseiIRIS();
	}

	/**
	 * 社員ユーザで住生トップ表示用パラメータの付与が必要な場合True
	 * @return [description]
	 */
	public Boolean getSumiseiIRISParam(){
		return access.canViewSumiseiIRIS() && !access.isSumiseiIRISUser();
	}

	//P16-0003 Atria対応開発
	public Boolean getCanAccessAtria() {
		return access.canAccessAtria();
	}

}