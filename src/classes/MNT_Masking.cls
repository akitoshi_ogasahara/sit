public class MNT_Masking {
	public static final String MASK_CHAR = '■';
	public static final String MASK_CHAR_Phone = '9';
	public static final String MASKED_EMAIL_DOMAIN = '@nnlink.sandbox';

	/**
	 *	maskField　Factory　Method
	 */
	public static maskField createMaskField(String fldNm, MNT_SandBoxRefresh.MASKTYPE mt){
		if(mt == MNT_SandBoxRefresh.MASKTYPE.Email){
			return new maskEmailField(fldNm);
		}else if(mt == MNT_SandBoxRefresh.MASKTYPE.MaskByID){
			return new maskByIDField(fldNm);
		}else if(mt == MNT_SandBoxRefresh.MASKTYPE.Phone){
			return new maskPhoneField(fldNm);
		}else if(mt == MNT_SandBoxRefresh.MASKTYPE.Address){
			return new maskAddressField(fldNm);
		}else{
			return new maskField(fldNm);
		}	
	}

	/**
	 *	項目マスク処理クラス
	 */
	public virtual class maskField{
		public String fieldName{get;private set;}
		protected String maskChar;
		
		public maskField(String fldNm){
			this.fieldName = fldNm;
			this.maskChar = MASK_CHAR;
		}
	
		//マスク処理実装（MASKTYPEによって、継承させる）
		protected virtual String getMaskedString(Object val, SObject rec){
			system.assert(val != null);			//Nullの場合は呼び出さないでください

			List<String> splited = String.valueOf(val).split('[\\s　]',0);
			List<String> masked = new List<String>();
			
			for(String s:splited){
				if(String.isNotBlank(s) && s.length()>1){		//2文字以上空白以外の場合にマスクを実施
					String maskedStr='';
					for(Integer i=0;i<s.length();i++){
						if(math.mod(i,2) == 0){			//奇数番目をマスクする（0から始まるのでIndexは偶数番目の時に）
							maskedStr += maskChar;
						}else{
							maskedStr += s.substring(i,i+1);
						}
					}
					masked.add(maskedStr);
				}else{
					masked.add(s);
				}
			}
			
			return String.join(masked, ' ');	//全角スペースもすべて半角スペースで戻す 
		}
		
		//マスク処理の実施
		public void mask(SObject rec){
			Object val = rec.get(fieldName);
			if(val!=null){
				rec.put(fieldName, getMaskedString(val, rec));
			}
		}
	}
	
	/**
	 *	Address項目マスク処理クラス
	 */
	public class maskAddressField extends maskField{
		//Constructor
		public maskAddressField(String fldNm){
			super(fldNm);
		}
		//マスク処理実装（MASKTYPEによって、継承させる）
		protected override String getMaskedString(Object val, SObject rec){
			return String.valueOf(val).left(8);
		}
	}

	/**
	 *	Email項目マスク処理クラス
	 */
	public class maskEmailField extends maskField{
		//Constructor
		public maskEmailField(String fldNm){
			super(fldNm);
		}
		//マスク処理実装（MASKTYPEによって、継承させる）
		protected override String getMaskedString(Object val, SObject rec){
			return rec.Id + MASKED_EMAIL_DOMAIN;		// @nnlink.sandbox
		}
	}

	/**
	 *	ID値でマスクする項目処理クラス 　※ユニーク制約の項目など
	 */
	public class maskByIDField extends maskField{
		//Constructor
		public maskByIDField(String fldNm){
			super(fldNm);
		}
		//マスク処理実装（MASKTYPEによって、継承させる）
		protected override String getMaskedString(Object val, SObject rec){
			return rec.Id;
		}
	}

	/**
	 *	Phone/Fax項目マスク処理クラス
	 */
	public class maskPhoneField extends maskField{
		//Constructor
		public maskPhoneField(String fldNm){
			super(fldNm);
			this.maskChar = MASK_CHAR_Phone;			//電話番号形式のマスク文字は『9』
		}
	}
}