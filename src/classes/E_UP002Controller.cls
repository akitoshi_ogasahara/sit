public  with sharing class E_UP002Controller extends E_AbstractUPInfoController{
    public LIST<recordData> recordDataList{get;set;}
    public LIST<recordData> headerList{get;set;}
    public dateTime baseDate{get;set;}
    public boolean isToushikaSenyou{get; private set;}
    public E_UPBPF__c upbpf{get;set;}
    private boolean isVariable = false;
    public boolean getisVariable(){
        return isVariable;
    }
    private List<DateTime> baseDateList;//直近五日間の日付を格納する
    public List<String> getBaseDateList(){
        List<String> headerBaseDateStringList = new LIST<String>();
        for(integer i = 0;i<5;i++){
            headerBaseDateStringList.add(baseDateList[i].format('M/d'));
        }
        return headerBaseDateStringList;
    }
    public string getUpperMessage(){
        String message = getDisclaimer().get('up2|001');
        if(isVariable){
            message = getDisclaimer().get('up2|002');
        }
        return message;
    }
    list<E_UPFPF__c> upfpfList;

    private date ebizDate = getDataSyncDate_DateType();
    private String prdCode;
    private String errorCode = 'UP2|006';
    
    public E_UP002Controller(){
        super();
    }
    /**
* #getMenuKey.
*/
    private String getMenuKey() {
        return 'UP' + prdCode;
    }
    
    
    public pagereference childInit(){
    	
    	/*
		//service hour menu
		PageReference pageRef = isServiceHourMenu();
		if (pageRef != null) {
			return pageRef;
		}
		*/
    	
        prdCode = E_EncryptUtil.prdCodeConversion(ApexPages.currentPage().getParameters().get('Zprdcd'), false);
        String fndCode = E_EncryptUtil.prdCodeConversion(ApexPages.CurrentPage().getParameters().get('Zfndcd'), false);
        
        if(!urlParameterCheck(prdCode, fndCode)){
            //URLパラメータの不正があった場合
            return E_Util.toUpErrorPage(errorCode);
        }
        pgTitle = 'ユニットプライス';
        //pagereference resultPref = super.init();
        
        isVariable = isValiableAnnuity(prdCode);
        upbpf = E_UPBPFDao.getRecByZPRDCD(prdCode);
        
        if(upbpf == null){
            //ユニットプライス基本情報がない場合
            return E_Util.toUpErrorPage(errorCode);
        }
        
        isToushikaSenyou = upbpf.ZQLFLG__c;
        baseDate = checkBaseDate(ebizDate,upbpf.ZIDXDATE01__c);
        //選択可能ファンドの取得
        list<E_SFGPF__c> sfgfpList = new list<E_SFGPF__c>();
        if(string.isNotBlank(fndCode)){
            sfgfpList = E_SFGPFDao.getRecByFundMasterBySFGPFCode(upbpf.E_FundMaster__c,upbpf.ZGFUNDCD__c + '|' + fndCode);
        }else{
            sfgfpList = E_SFGPFDao.getRecByFundMasterIdOrderBy(upbpf.E_FundMaster__c);
        }
        
        if(sfgfpList.isEmpty()){
            //選択可能ファンドがない場合
            return E_Util.toUpErrorPage(errorCode);
        }    
        
        upfpfList = getUPFPF(sfgfpList,fndCode);
        
        /*
        if(upfpfList.isEmpty()){
            //ユニットプライス表示用ファンドが存在しない場合
            return E_Util.toUpErrorPage(errorCode);
        }
        */
        
        recordDataList = createRecordDataList(upfpfList,baseDate,isVariable);
        recordDataList = bindColumn(recordDataList);
        return null;
    }
    
    /**
     * ユニットプライス表示用ファンドから、特別勘定IDを基にデータを取得する
     * @param sfgfpList: ユニットプライス基本情報
     * @param fndCode: ファンドコード
     * @return List<E_UPFPF__c>: ユニットプライス表示用ファンド
     */
    private List<E_UPFPF__c> getUPFPF(List<E_SFGPF__c> sfgfpList,String fndCode){
        String baseDateString = baseDate.format('yyyyMMdd');
        List<ID> tokubetuKanjouIDList = new LIST<ID>();
        for(E_SFGPF__c item : sfgfpList){
            //特別勘定IDをセットする
            tokubetuKanjouIDList.add(item.E_SVCPF__c);
        }
        
     	// URLパラメータにファンドコードが設定されているかによってクエリを分ける
     	List<E_UPFPF__c> upList = new List<E_UPFPF__c>();
        if(String.isnotBlank(fndCode)){           
            upList = E_UPFPFDao.getRecByfndCode(tokubetuKanjouIDList,fndCode,baseDateString);
        }else{           
            upList = E_UPFPFDao.getRecBySvcpfId_Time(tokubetuKanjouIDList,baseDateString);            
        }
       
        // 選択可能ファンドの並び順で並び替え
        Map<String, E_UPFPF__c> upfpfMap = new Map<String, E_UPFPF__c>();
        System.debug(upList);
        for(E_UPFPF__c upfpf : upList){
        	upfpfMap.put(upfpf.SVCPF__c, upfpf);
        }
        System.debug(upfpfMap);
        
        List<E_UPFPF__c> retUpfpfList = new List<E_UPFPF__c>();
        for(E_SFGPF__c sfgpf : sfgfpList){
        	E_UPFPF__c retUpfpf = upfpfMap.get(sfgpf.E_SVCPF__c);
            // IM143674 ユニットプライス表示エラー本番障害対応：NULL判定条件を入れる
            if(retUpfpf != null) {
                retUpfpfList.add(retUpfpf);
            }
        }
        
        return retUpfpfList;
/*     	
     	// URLパラメータにファンドコードが設定されているかによってクエリを分ける
        IF(String.isnotBlank(fndCode)){           
            return E_UPFPFDao.getRecByfndCode(tokubetuKanjouIDList,fndCode,baseDateString);
        }else{           
            return E_UPFPFDao.getRecBySvcpfId_Time(tokubetuKanjouIDList,baseDateString);            
        }
*/        
        
        
    }
    
    //カスタムクラス
    //画面表示用のオブジェクト
    public class recordData{
        public String tokubetukanjouMei{get;set;}
        public String toushiShintaku{get;set;}
        public List<String> unitPrice{get;set;}
        //↓レイアウト情報
        public MAP<Integer,Integer> rowSpan {get;set;}
        public MAP<Integer,Integer> colSpan {get;set;}
        public MAP<Integer,String> display{get;set;}
        public boolean isCompositURL{get;private set;}
/*
        public recordData(){//コンストラクタ
        }
*/        
        public recordData(String tokubetuKanjouMei,String toushiShintaku,boolean isCompositURL){
            this.tokubetukanjouMei = tokubetuKanjouMei;
            this.toushiShintaku = toushiShintaku;
            this.unitPrice = new LIST<String>{'-','-','-','-','-'};
                this.rowSpan = new MAP<Integer,Integer>();
            this.colSpan = new MAP<Integer,Integer>();
            this.display =new MAP<Integer,String>();
            for(integer i = 1;i<8;i++){
                rowSpan.put(i,1);
                colSpan.put(i,1);
                display.put(i,'');
            }
            this.isCompositURL = isCompositURL;
        }
    }
    
    /**
     * ユニットプライス表示用ファンドを画面表示用オブジェクトへ変換する
     * @param upfpfList: ユニットプライス表示用ファンド
     * @param baseDate: 基準日
     * @return List<recordData>: 画面表示用オブジェクト
     */
    private LIST<recordData> createRecordDataList(list<E_UPFPF__c> upfpfList,dateTime baseDate){
        LIST<recordData> retRecordDataList = new LIST<recordData>();
        recordData rd;
        LIST<E_UPXPF__c> childList;
        //integer parentListCount = 0;
        //integer j = 0;
        String IVTNAME = ''; 
        for(E_UPFPF__c item:upfpfList){
        	System.debug(item);
            if(item.ZFUNDFLG__c){
                //コンポジットファンドの時
                //IVTNAME = '<a href =' + item.ZHPLNK__c + 'target="_blank">こちらをご覧下さい<a/>';//要修正
                IVTNAME = createCompositURL(item.ZHPLNK__c);
            }else if(item.ZSFUNDCD__c.equals('V002')){
                IVTNAME = '投資信託は利用していません';                
            }else{
                IVTNAME = item.ZIVTNAMEUnion__c;                
            }
            rd = new recordData(item.ZFNDNAMEUnion__c,IVTNAME,item.ZFUNDFLG__c);
            childList = item.E_UPXPFs__r;
            integer childListCount = 0;            
            decimal index;
            for(integer i = 0;i < baseDateList.size() && childListCount < childList.size();i++){
                index = childList[childListCount].ZVINDEX__c;
                if(baseDateList[i] == E_Util.string2DateyyyyMMdd(childList[childListCount].ZFUNDDTE__c)){
                    if(index != null && index != 0 ) rd.unitPrice[i] = E_Util.getRoundingDownString(index, 2);        
                    childListCount++;
                }
            }
            
            retRecordDataList.add(rd);
        }
        return retRecordDataList;
    }
    
    
    /**
     * ユニットプライス表示用ファンドから、特別勘定IDを基にデータを取得する
     * @param sfgfpList: ユニットプライス基本情報
     * @param fndCode: ファンドコード
     * @return List<E_UPFPF__c>: ユニットプライス表示用ファンド
     */    
    private dateTime checkBaseDate(date ebizDate,String IDXDATE01) {
        dateTime dateTimeIDXDATE01 = E_Util.string2DateyyyyMMdd(IDXDATE01);
        dateTime retDateTime;
        if(ebizDate < E_Util.string2DateyyyyMMdd(IDXDATE01)){
            retDateTime = ebizDate;
        }else{
            retDateTime = dateTimeIDXDATE01;
        }
        baseDateList = createBaseDateList(retDateTime,5);
        return retDateTime;
    }
    
    private  List<DateTime> createBaseDateList(datetime basedate,integer count){
        
        List<DateTime> retList = new LIST<DateTime>();
        for(integer i = 0;i < count;i++){           
            retList.add(baseDate.addDays(-1*i));
        }
        return retList;   
    }
    
    private boolean urlParameterCheck(String prdCode,String fndCode){
        if(String.isBlank(prdCode)) return false;
        return isTarget(prdCode)? String.isNotBlank(fndCode): String.isBlank(fndCode);
    }
    
    private string createCompositURL(String val){
        String prefix = system.label.E_UP_COMPOSITFUND_PASS;
        return prefix + val;
    }
    
    public PageReference openUP003(){
        PageReference pr = Page.UP003;
        pr.getParameters().put('id', upbpf.id);
        if(isTarget(prdCode)){
            pr.getParameters().put('spid', upfpfList[0].SVCPF__c);
        }
        return pr;
    }
    //カラム結合処理
    private LIST<recordData> bindColumn(LIST<recordData> val){
        integer rowSpanVal;
        integer j = 0;
        integer parentListCount = 1;
        integer listsize = val.size();
        for(integer i = 1;i < listsize;i++ ){
            recordData rd = val[i];
            if(val[j].tokubetukanjouMei.equals(rd.tokubetuKanjouMei)){
                rd.display.put(1,'none');
                val[i] = rd;
                rowSpanVal = val[j].rowSpan.get(1);
                val[j].rowSpan.put(1,rowSpanVal + 1);
                if(isVariable){
                    for(integer k = 3;k<=rd.display.size();k++){
                        rd.display.put(k,'none');
                        val[j].rowSpan.put(k,rowSpanVal + 1);
                    }
                    
                }
            }else{
                j = parentListCount;
            }
            parentListCount++;
        }
        return val;
    }

    /**
     * ユニットプライス表示用ファンドを画面表示用オブジェクトへ変換する
     * @param upfpfList: ユニットプライス表示用ファンド
     * @param baseDate: 基準日
     * @return List<recordData>: 画面表示用オブジェクト
     */
    private LIST<recordData> createRecordDataList(list<E_UPFPF__c> upfpfList,dateTime baseDate,boolean isVariable){
        if(!isVariable){
            return createRecordDataList(upfpfList,baseDate);
        }
        LIST<recordData> retRecordDataList = new LIST<recordData>();
        recordData rd;
        LIST<E_UPXPF__c> childList;
        String IVTNAME = ''; 
        
        list<E_UPFPF__c> valliableUpfpfList = new list<E_UPFPF__c>();
        Map<String, E_VariableAnnuity__c> customSetting = E_VariableAnnuity__c.getAll();
        String key;
        E_UPFPF__c workItem;
        for(E_UPFPF__c item:upfpfList){
            key = item.ZSFUNDCD__c;
                item.put('ZFUNDFLG__c', false);
            if(customSetting.containsKey(key)){
                item.put('ZIVTNAME01__c', customSetting.get(key).InvName__c);
                valliableUpfpfList.add(item);                                
            }else if(item.ZSFUNDCD__c.equals('V003') || item.ZSFUNDCD__c.equals('V005')){
                item.put('ZIVTNAME01__c', customSetting.get(key + '|001').InvName__c);
                valliableUpfpfList.add(item);
                workItem = item.clone();
                workItem.put('ZIVTNAME01__c', customSetting.get(key + '|002').InvName__c);
                valliableUpfpfList.add(workItem);			
            }
        }
        
        for(E_UPFPF__c item:valliableUpfpfList){
            IVTNAME = item.ZIVTNAME01__c;                
            rd = new recordData(item.ZFNDNAMEUNION__c,IVTNAME,item.ZFUNDFLG__c);
            childList = item.E_UPXPFs__r;
            integer childListCount = 0;            
            decimal index;
            for(integer i = 0;i < baseDateList.size() && childListCount < childList.size();i++){
                index = childList[childListCount].ZVINDEX__c;
                if(baseDateList[i] == E_Util.string2DateyyyyMMdd(childList[childListCount].ZFUNDDTE__c)){
                    if(index != null && index != 0 ) rd.unitPrice[i] = E_Util.getRoundingDownString(index, 2);        
                    childListCount++;
                }
            }
            
            retRecordDataList.add(rd);
        }

        return retRecordDataList;
    }
    

    
}