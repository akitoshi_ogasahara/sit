global with sharing class E_OfficeLkupExtender extends E_AbstractSearchExtender {

    E_OfficeLkupController extension;
    //ページタイトル
    private static final String PAGE_TITLE = '代理店事務所選択';

    /* コンストラクタ */
    public E_OfficeLkupExtender(E_OfficeLkupController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        extension.execInitialSearch = false;        //初期検索を実施しない
        //社員ユーザ拠点権限の場合には、EIDCのZINQUIRRの3桁目から2桁は**以外の場合、支社権限と意味して、ECL２のZBUSBRまたはBRANCHと一致するレコードのみ検索可能とする。
        if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL)) {
            SkyEditor2.Query tableQuery1 = extension.queryMap.get('searchDataTable');
            tableQuery1.addWhereIfNotFirst('AND');
            tableQuery1.addWhere(' ( E_CL2PF_ZBUSBR__c = \''+ access.getAccessKindFromThree() + '\' OR E_CL2PF_BRANCH__c = \''+ access.getAccessKindFromThree() + '\')');
            extension.queryMap.put('searchDataTable',tableQuery1);
        }
    }

    /*半角カナ入力値を全角カナへ変換*/
    global override void preSearch() {
        IF(extension != null){
            String ipValDairitenjimushocd = extension.ipVal_dairitenjimushocd.SkyEditor2__Text__C;
            String ipValDairitenjimushokana = extension.ipVal_dairitenjimushokana.SkyEditor2__Text__C;
    
            if (E_Util.isNotNull(ipValDairitenjimushocd) && (!E_Util.isEnAlphaNum(ipValDairitenjimushocd) || ipValDairitenjimushocd.length() != 5)) {
                throw new SkyEditor2.ExtenderException(getMSG().get('LKM|002'));
            }
            
            //①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ
            IF(ipValDairitenjimushokana != null ){
                extension.ipVal_dairitenjimushokana.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(ipValDairitenjimushokana))));
                ipValDairitenjimushokana = extension.ipVal_dairitenjimushokana.SkyEditor2__Text__c;
            }
            
            //DF-000618対応  start
            if (!access.isAccessKind(E_Const.USER_ACCESS_KIND_BRALL) && (E_Util.isNotNull(ipValDairitenjimushocd) || E_Util.isNotNull(ipValDairitenjimushokana))) {
                String soql = 'Select E_CL2PF_ZBUSBR__c, E_CL2PF_BRANCH__c From Account ';
                List<String> whereList = new List<String>();
                if (E_Util.isNotNull(ipValDairitenjimushocd)) {
                    whereList.add('E_CL2PF_ZAGCYNUM__c =: ipValDairitenjimushocd');
                }
                if (E_Util.isNotNull(ipValDairitenjimushokana)) {
                    whereList.add('E_CL2PF_ZEAYKNAM__c =: ipValDairitenjimushokana');
                }
                whereList.add('E_COMM_VALIDFLAG__c = \'1\'');
                whereList.add('E_IsAgency__c = false');
                if (whereList.size() > 0) {
                    soql += 'Where ';
                    soql += String.join(whereList, ' and ');
                }
                soql += ' limit 100';
                List<Account> recs = Database.query(soql);
                if (recs.size() > 0) {
                    boolean isReference = false;
                    for (Account acc : recs) {
                        System.debug(access.getAccessKindFromThree().equals(acc.E_CL2PF_ZBUSBR__c));
                        System.debug(access.getAccessKindFromThree().equals(acc.E_CL2PF_BRANCH__c));
                        System.debug(access.getAccessKindFromThree());
                        System.debug(acc.E_CL2PF_BRANCH__c);
                        if (access.getAccessKindFromThree().equals(acc.E_CL2PF_ZBUSBR__c) || access.getAccessKindFromThree().equals(acc.E_CL2PF_BRANCH__c)) {
                            isReference = true;
                        }
                    }
                    if (!isReference){
                        throw new SkyEditor2.ExtenderException(getMSG().get('LKM|005'));
                    }
                }
            }
            //DF-000618対応  end
        }
    }
}