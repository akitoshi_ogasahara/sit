@isTest
private class TestI_PIPMeritTableAvailabilityChecker {

	@isTest static void isAvailableContractTest_OK() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		System.AssertEquals(true, checker.isAvailableContract());

	}

	@isTest static void isAvailableContractTest_haraikomimanryo() {
		//保険契約ヘッダを作成
		TestI_PIPTestUtil.PolicyParam pParam= TestI_PIPTestUtil.createDefaultpParam();
		pParam.currentStatus = 'U';
		E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		//保険顧客情報を作成
		TestI_PIPTestUtil.createInsuredInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		System.AssertEquals(true, checker.isAvailableContract());

	}


	@isTest static void isAvailableContractTest_Haraizumi() {
		//保険契約ヘッダを作成
		TestI_PIPTestUtil.PolicyParam pParam= TestI_PIPTestUtil.createDefaultpParam();
		pParam.currentStatus = 'R';
		E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		//保険顧客情報を作成
		TestI_PIPTestUtil.createInsuredInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		System.AssertEquals(false, checker.isAvailableContract());

	}

	@isTest static void isAvailableContractTest_Zenno() {

		//保険契約ヘッダを作成
		TestI_PIPTestUtil.PolicyParam pParam= TestI_PIPTestUtil.createDefaultpParam();
		pParam.zennoFlg = true;
		E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		//保険顧客情報を作成
		TestI_PIPTestUtil.createInsuredInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		System.AssertEquals(false, checker.isAvailableContract());

	}

	@isTest static void isAvailableContractTest_syometsu() {

		//保険契約ヘッダを作成
		TestI_PIPTestUtil.PolicyParam pParam= TestI_PIPTestUtil.createDefaultpParam();
		pParam.currentStatus = 'C';
		E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		//保険顧客情報を作成
		TestI_PIPTestUtil.createInsuredInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		System.AssertEquals(false, checker.isAvailableContract());

	}

	@isTest static void isAvailableContractTest_cannotcalcCV() {
		//保険契約ヘッダを作成
		TestI_PIPTestUtil.PolicyParam pParam= TestI_PIPTestUtil.createDefaultpParam();
		pParam.cannotCalcCV = true;
		E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		//保険顧客情報を作成
		TestI_PIPTestUtil.createInsuredInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		System.AssertEquals(false, checker.isAvailableContract());

	}

	@isTest static void isServiceTime_OK_0600() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		DateTime localTime = Datetime.newInstance(1993,9,6,6,0,0) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);

		I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

		System.AssertEquals(true,checker.isServiceTime());


	}

	@isTest static void isServiceTime_OK_2359() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		DateTime localTime = Datetime.newInstance(1993,9,6,23,59,59) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);

		I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

		System.AssertEquals(true,checker.isServiceTime());


	}

	@isTest static void isServiceTime_NG_0000() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		DateTime localTime = Datetime.newInstance(1993,9,6,0,0,0) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);

		I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

		System.AssertEquals(false,checker.isServiceTime());

	}

	@isTest static void isServiceTime_NG_0559() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//個人保険特約リスト(主契約・特約)を作成
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCoversInfo(policy);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);

		DateTime localTime = Datetime.newInstance(1993,9,6,5,59,59) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);

		I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

		System.AssertEquals(false,checker.isServiceTime());

	}

	/*
		対象商品かをチェックするロジックのテスト
	*/

	//重大疾病の場合
	@isTest static void productCheckTest_TT() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTTの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TT';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}
	//生活保障の場合
	@isTest static void productCheckTest_TS() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTSの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TS';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期（定期）のパターン（始め）

	//逓増定期(定期)GAの場合
	@isTest static void productCheckTest_TP_GA() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GA';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)GBの場合
	@isTest static void productCheckTest_TP_GB() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GB';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)GCの場合
	@isTest static void productCheckTest_TP_GC() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GC';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}
	//逓増定期(定期)GDの場合
	@isTest static void productCheckTest_TP_GD() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GD';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)GEの場合
	@isTest static void productCheckTest_TP_GE() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GE';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)GFの場合
	@isTest static void productCheckTest_TP_GF() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GF';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)GGの場合
	@isTest static void productCheckTest_TP_GG() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GG';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)GHの場合
	@isTest static void productCheckTest_TP_GH() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'GH';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)ULの場合
	@isTest static void productCheckTest_TP_UL() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'UL';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)UMの場合
	@isTest static void productCheckTest_TP_UM() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'UM';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)UNの場合
	@isTest static void productCheckTest_TP_UN() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'UN';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)UOの場合
	@isTest static void productCheckTest_TP_UO() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'UO';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(定期)UPの場合
	@isTest static void productCheckTest_TP_UP() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'UP';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}


	//逓増定期特約が付加されていない定期の場合
	@isTest static void productCheckTest_TP_NONLCV2() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードTPの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TP';
		ttParams[1].crCode = 'AD';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//逓増定期（定期）のパターン（終わり）

	//逓増定期（終身）のパターン（始め）

	//逓増定期(終身)GAの場合
	@isTest static void productCheckTest_WL_GA() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GA';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)GBの場合
	@isTest static void productCheckTest_WL_GB() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GB';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)GCの場合
	@isTest static void productCheckTest_WL_GC() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GC';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}
	//逓増定期(終身)GDの場合
	@isTest static void productCheckTest_WL_GD() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GD';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)GEの場合
	@isTest static void productCheckTest_WL_GE() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GE';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)GFの場合
	@isTest static void productCheckTest_WL_GF() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GF';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)GGの場合
	@isTest static void productCheckTest_WL_GG() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GG';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)GHの場合
	@isTest static void productCheckTest_WL_GH() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'GH';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)ULの場合
	@isTest static void productCheckTest_WL_UL() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'UL';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)UMの場合
	@isTest static void productCheckTest_WL_UM() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'UM';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)UNの場合
	@isTest static void productCheckTest_WL_UN() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'UN';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)UOの場合
	@isTest static void productCheckTest_WL_UO() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'UO';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//逓増定期(終身)UPの場合
	@isTest static void productCheckTest_WL_UP() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'UP';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}


	//逓増定期特約が付加されていない終身の場合
	@isTest static void productCheckTest_WL_NONLCV2() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'WL';
		ttParams[1].crCode = 'AD';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//逓増定期（終身）のパターン（終わり

	//終身ガンのパターン　始め
	//終身ガンOFで新税制後の契約日の場合
	@isTest static void productCheckTest_OF_NEWTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120427';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OF';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//終身ガンOFで新税制前の契約日の場合
	@isTest static void productCheckTest_OF_OLDTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120426';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OF';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//終身ガンOGで新税制後の契約日の場合
	@isTest static void productCheckTest_OG_NEWTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120427';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OG';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//終身ガンOGで新税制前の契約日の場合
	@isTest static void productCheckTest_OG_OLDTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120426';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OG';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//終身ガンOHで新税制後の契約日の場合
	@isTest static void productCheckTest_OH_NEWTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120427';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OH';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//終身ガンOHで新税制前の契約日の場合
	@isTest static void productCheckTest_OH_OLDTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120426';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OH';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//終身ガンOIで新税制後の契約日の場合
	@isTest static void productCheckTest_OI_NEWTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120427';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OI';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(true, checker.isAvailableProduct() );

	}

	//終身ガンOIで新税制前の契約日の場合
	@isTest static void productCheckTest_OI_OLDTAXSYSTEM() {
		//被保険者の作成
        TestI_PIPTestUtil.InsuredParam iparam = TestI_PIPTestUtil.createDefaultiParam();
        Id insuredId = TestI_PIPTestUtil.createInsured(iparam);
        //作成した被保険者の保険契約ヘッダを作成
        TestI_PIPTestUtil.PolicyParam pParam = TestI_PIPTestUtil.createDefaultpParam(insuredId);
        pParam.OCCDate = '20120426';
        //保険契約ヘッダを作成
        E_Policy__c policy = TestI_PIPTestUtil.createPolicy(pParam);


		//CRコードWLの個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> coverParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		coverParams[0].crCode = 'OI';

		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(coverParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}
	//終身ガンのパターン　終わり



	//対象外のCRコードとして'TL(低解約定期)'を設定。対象商品が変更になるタイミングで修正する。
	@isTest static void productCheckTest_NG() {
		//保険契約ヘッダを作成

		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();

		//個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TL';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);


		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//失効中の場合
	@isTest static void productCheckTest_lapse() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();
		policy.comm_statcode__c = 'L';
		update policy;

		//個人保険特約リスト(主契約・特約)を作成
		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TT';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

	//失効中（貸付）の場合
	@isTest static void productCheckTest_lapse_PL() {
		//保険契約ヘッダを作成
		E_Policy__c policy = TestI_PIPTestUtil.createPolicyInfo();
		policy.comm_statcode__c = 'O';
		update policy;

		List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(policy);
		ttParams[0].crCode = 'TT';
		List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);

		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy, covers);
		system.assertEquals(false, checker.isAvailableProduct() );

	}

}