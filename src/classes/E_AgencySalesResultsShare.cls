public with sharing class E_AgencySalesResultsShare extends E_ShareBase {

	private Set<SObject> records;
	// 募集人のUserIdマップ
	private Map<String, Id> agentUserMap;
	//代理店格
	private final String HIERARCHY_AH = 'AH';
	//募集人
	private final String HIERARCHY_AT = 'AT';

    
	/**
	 * Constructor
	 */
	public E_AgencySalesResultsShare(Set<SObject> records){
		this.records = records;
		agentUserMap = new Map<String, Id>();
	}

	/**
	 * execute
	*/
	public void execute(){
		// Share削除
		//Insertのみのため削除は行わない
//		deleteNNLinkShares(records);
		
		// Share登録
		createSharesNNLink(records, E_Const.SHARE_ACCESS_LEVEL_READ);
	}

	/*
	 * @param records: 代理店挙積情報
	 * @param level: アクセスレベル
	 */
	private List<SObject> createSharesNNLink(Set<SObject> records, String level){
		shares = new List<SObject>();
		
		// AccessLevel
		accessLevel = level;
		
		// Prefix
		lastRecPrefix = null;

		// ShareBizLogic
		createSharesBizLogic(records);
		
		// 正常レコードのみInsert、エラーレコードは連携ログの詳細を更新
		if(shares.isEmpty() == false){
			// Insert
			List<Database.SaveResult> insrList = Database.insert(shares,false);
			
			// エラーレコードのエラー内容をセット
			errMsg = '';
			for(Integer i = 0; i< shares.size(); i++){
				if(insrList != null && !insrList.get(i).isSuccess()){
					Database.Error err = insrList.get(i).getErrors().get(0);
					//errList.add('[Id: ' + shares.get(i).Id + ', エラー: ' +err.getmessage() + ']\n');
					errMsg += '[' + shares.get(i).get('parentId') + '/' +err.getmessage() + ']\n';
				}				
			}
			if(String.isNotBlank(errMsg)){
				throw new ShareBaseException(errMsg);
			}
		}
		return shares;
	}

	/**
	 * Shareロジック
	 * 階層がAHの場合: 代理店に紐づく事務所の支社コード、L1代理店格コードに共有を設定
	 * 階層がAYの場合: 事務所の支社コード、L1代理店格コード、L2代理店事務所コードに共有を設定
	 * 階層がATの場合: 事務所の支社コード、L1代理店格コード、L2代理店事務所コード、募集人コードの前5桁が一致する有効な募集人に共有を設定
	 */
	private void createSharesBizLogic(Set<SObject> records){
		//代理店格ID => 支社コードリスト
		Map<id,Set<String>> parentAccBRs = new  Map<id,Set<String>>();
		Set<String> brs = new Set<String>();
		Set<String> agentCodes = new Set<String>();

		// レコードの件数分、処理を行う
		for(SObject parentRec : records){
			//階層がAHの場合のListを作成
			if((String)parentRec.get('Hierarchy__c') == HIERARCHY_AH){
				// 格IDをMapに設定
				parentAccBRs.put((String)parentRec.get('ParentAccount__c'),new Set<String>());
			}else if((String)parentRec.get('Hierarchy__c') == HIERARCHY_AT){
				String agentCode = (String)parentRec.get('AgentCode__c');
				if(!String.isBlank(agentCode)){
					// 募集人コードSetに追加
					agentCodes.add((String)parentRec.get('AgentCode__c'));
				}
			}
		}
		//代理店格Setがある場合
		if(!parentAccBRs.isEmpty()){
			//代理店格ID => 支社コードリストを作成
			for(SObject aResult :[SELECT parentid,E_CL2PF_BRANCH__c FROM Account WHERE ParentId in: parentAccBRs.keySet() AND E_COMM_VALIDFLAG__c = '1']){
				String key = (String)aResult.get('parentid');
				String value = (String)aResult.get('E_CL2PF_BRANCH__c');
				parentAccBRs.get(key).add(value);
				brs.add('BR' + value);
			}
		}
		//募集人コードから有効な募集人を取得
		if(!agentCodes.isEmpty()){
			List<User> users = [Select Id, Contact.AgentNum5__c From User Where Contact.AgentNum5__c In :agentCodes And IsActive = true Order by Contact.E_CL3PF_AGNTNUM__c asc];
			for(User u : users){
				agentUserMap.put(u.Contact.AgentNum5__c, u.Id);
			}
		}
		// 事務所毎の公開グループより、GroupIdを取得
		Map<String, Group> grMap = getGroupIds(records,brs);
		// 事務所毎の公開グループより、GroupIdを取得
//		Map<String, Group> grMap = getGroupIds(records);

		for(SObject parentRec : records){
			//ShareオブジェクトのAPI名取得		prefixが変更された場合のみDescribeを呼び出す。
			if(lastRecPrefix == null || lastRecPrefix != String.valueOf(parentRec.Id).subString(0,3)){
				sObjName = parentRec.Id.getSObjectType().getDescribe().getName();
				shareObjName = sObjName.replace('__c','__share');
				shareObjType = Schema.getGlobalDescribe().get(shareObjName);
				lastRecPrefix = String.valueOf(parentRec.Id).subString(0,3);
			}
			createShares(parentRec, grMap);
			if((String)parentRec.get('Hierarchy__c') == HIERARCHY_AH){
				createSharesAH(parentRec,parentAccBRs.get((String)parentRec.get('ParentAccount__c')), grMap);
			}
		}
	}
	/**
	 * Shareの作成 
	 */
	private void createShares(SObject parentRec, Map<String, Group> grMap){
		String brCode = (String)parentRec.get('brCode__c');
		String parentAccCode = (String)parentRec.get('parentAccountCode__c');
		String accCode = (String)parentRec.get('AccountCode__c');
		String agentCode = (String)parentRec.get('AgentCode__c');

		//L1　公開グループ設定
		if(String.isNotBlank(parentAccCode)){
			Group grL1 = grMap.get('L1'+parentAccCode);
			if(grL1 != null){
				shares.add(createShare(shareObjType, parentRec.Id, grL1.Id));
			}
		}
		//L2　公開グループ設定
		if(String.isNotBlank(accCode)){
			Group grL2 = grMap.get('L2'+accCode);
			if(grL2 != null){
				shares.add(createShare(shareObjType, parentRec.Id, grL2.Id));
			}
		}
		//BR　公開グループ設定
		if(String.isNotBlank(brCode)){
			Group grBR = grMap.get('BR'+brCode);
			if(grBR != null){
				shares.add(createShare(shareObjType, parentRec.Id, grBR.Id));
			}	
		}
		// 募集人ユーザ設定
		if(String.isNotBlank(agentCode)){
			Id agentUserId = agentUserMap.get(agentCode);
			if(agentUserId != null){
				shares.add(createShare(shareObjType, parentRec.Id, agentUserId));
			}
		}
	}
	/**
	 * Shareの作成 
	 * AH専用
	 */
	private void createSharesAH(SObject parentRec,Set<String> accBRs, Map<String, Group> grMap){
		for(String brCode :accBRs){
			//BR　公開グループ設定
			if(String.isNotBlank(brCode)){
				Group grBR = grMap.get('BR'+brCode);
				if(grBR != null){
					shares.add(createShare(shareObjType, parentRec.id, grBR.Id));
				}
			}
		}
	}

	/**
	 * 事務所毎の公開グループより、GroupIdを取得
	 */
	private Map<String, Group> getGroupIds(Set<SObject> records,Set<String> brs){

		// 事務所毎の公開グループを設定（L1、L2、BR）
		Set<String> groupNames = getGroupNames(records);
		//支社ｌコード追加
		groupNames.addAll(brs);
		// Groupの取得
		Map<String, Group> grMap = getGroupMap(groupNames);
		
		return grMap;
	}
	
	/**
	 * 事務所毎の公開グループ取得
	 * @param Map<SObject, Account> 
	 * @return Set<String>
	 */
	private Set<String> getGroupNames(Set<SObject> records){

		Set<String> groupNames = new Set<String>();
		
		// 事務所毎の公開グループを設定（L1、L2、BR）
		for(SObject parentRec:records){
			String parentAccCode = (String)parentRec.get('parentAccountCode__c');
			String accCode = (String)parentRec.get('AccountCode__c');
			String brCode = (String)parentRec.get('brCode__c');
			
			/* 公開グループ設定 */
			// L1（代理店格コード）
			if(String.isNotBlank(parentAccCode)){
				groupNames.add('L1' + parentAccCode);
			}
			// L2（代理店事務所コード）
			if(String.isNotBlank(accCode)){
				groupNames.add('L2' + accCode);
			}
			// BR（支社コード）
			if(String.isNotBlank(brCode)){
				groupNames.add('BR' + brCode);
			}
		}
		
		return groupNames;
	}
}