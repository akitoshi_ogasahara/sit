/**
 * 基底となるExtenderの抽象クラスです
 */
public abstract class E_AbstractViewExtender extends E_AbstractExtender {
	
	/**
	 * 契約関連機能判定
	 */
	protected override virtual boolean doAuthPolicy (String policyId) {
		//機能制限確認
		//手続き履歴
		if (this instanceof E_ProcessHistoryExtender) {
			if (!access.canProcessHistory()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}
		}
		
		//パラ―メータ確認
		if (!E_Util.isValidateId(policyId, Schema.E_Policy__c.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		//クエリ発行
		E_Policy__c policy = E_PolicyDao.getRecById(policyId);
		if (policy == null) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		
		if (policy.RecordType != null) {
			if (policy.RecordType.DeveloperName.equals (E_Const.POLICY_RECORDTYPE_COLI)) {
				if (!access.canColi()) {
					errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
					return false;
				}
			} else if (policy.RecordType.DeveloperName.equals(E_Const.POLICY_RECORDTYPE_SPVA)) {
				if (!access.canSpva()) {
					errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
					return false;
				}
			} else if (policy.RecordType.DeveloperName.equals(E_Const.POLICY_RECORDTYPE_DCOLI)) {
				if(!access.canColi() || !iris.getIsIRIS()){		//COLI参照権あり　かつ　IRISのときはOK　それ以外はNG
					errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
					return false;
				}
			} else if (policy.RecordType.DeveloperName.equals(E_Const.POLICY_RECORDTYPE_DSPVA)) {
				if(!access.canSpva() || !iris.getIsIRIS()){		//SPVA参照権あり　かつ　IRISのときはOK　それ以外はNG
					errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
					return false;
				}
			}
		}
		
		//オブジェクトアクセス制限
		if (!access.isAccessiblePolicy()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		
		//手続き履歴
		//if (this instanceof E_ProcessHistoryExtender) {
		//	if (!access.isAccessiblePolicyHistory()) {
		//		errorMsgCode = E_Const.ERROR_MSG_OBJECT;
		//		return false;				
		//	}
		//}
		
		//ID確認制限
		if (!Id.valueOf(policyId).equals(policy.id)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		
		//ISS
		if(this instanceOf E_ColiExtender
		|| this instanceOf E_SpvaExtender
		|| this instanceOf E_AnnuityExtender) {
			//抽象クラスのクッキー設定メソッド
			//setIssCookie();
			//Referer設定メソッド
			setRefererPolicy();
		} else if (this instanceOf E_PortfolioExtender) {
			//Referer設定メソッド
			setRefererPortfolio();
		} else if (this instanceof E_ProcessHistoryExtender) {
			setRefererHistory(true);
		}

/*スプリントバックログ 19対応　start  2017/1/23 */
//else if(this instanceof E_ContractorExtender){
//	E_CookieHandler.setCookieEndReferer('Contractor',access.getReferer(E_Const.ID_KIND.POLICY));
//}
/*スプリントバックログ 19対応　end */

		return true;
	}
	
	/**
	 * 契約関連機能判定
	 */
	protected override virtual boolean doAuthContact (String contactId) {
		//機能制限確認
		//契約者照会
		if (this instanceof E_CustomerInfoExtender) {
			if (!access.canColi() && !access.canSpva() && !access.canInvestment()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}

			/* スプリントバックログ 19対応　start  2017/1/20 */
			String refererContact = E_CookieHandler.getCookieRefererContact();
			if(String.isBlank(refererContact)){
				String referer =  ApexPages.currentPage().getHeaders().get('Referer');
				System.debug('setRefererContact==== ' + referer);
				if(referer != null){
					E_CookieHandler.setCookieContactReferer(access.getReferer(E_Const.ID_KIND.CONTACT));
				}
			}
			/* スプリントバックログ 19対応　end */

		//被保険者照会
		} else if (this instanceof E_InsuredInfoExtender) {
			if (!access.canColi() && !access.canSpva()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}
			/* スプリントバックログ 19対応　start  2017/1/30 */
			String refererContact = E_CookieHandler.getCookieRefererContact();
			if(String.isBlank(refererContact)){
				String referer =  ApexPages.currentPage().getHeaders().get('Referer');
				System.debug('setRefererContact==== ' + referer);
				if(referer != null){
					E_CookieHandler.setCookieContactReferer(access.getReferer(E_Const.ID_KIND.CONTACT));
				}
			}
			/* スプリントバックログ 19対応　end */

		}
		//手続き履歴
		if (this instanceof E_ProcessHistoryExtender) {
			if (!access.canProcessHistory()) {
				errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
				return false;
			}
		}
		
		//オブジェクトアクセス制限
		if (!access.isAccessiblePolicy()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		//手続き履歴
		//if (this instanceof E_ProcessHistoryExtender) {
		//	if (!access.isAccessiblePolicyHistory()) {
		//		errorMsgCode = E_Const.ERROR_MSG_OBJECT;
		//		return false;				
		//	}
		//}
		
		//パラ―メータ確認
		if (!E_Util.isValidateId(contactId, Schema.Contact.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		
		if (this instanceof E_ProcessHistoryExtender) {
			setRefererHistory(false);
		}
		
		return true;
	}
	
	/**
	 * 投信信託関連機能判定
	 * ※2015年、投資信託サービス終了に伴い投資信託機能をコメントアウト
	 */
	/*
	protected override virtual boolean doAuthInvestment (String investmentId) {
		//機能制限確認
		if (!access.canInvestment()) {
			errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
			return false;
		}
		
		//オブジェクトアクセス制限
		if (!access.isAccessibleInvestment()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		
		//パラ―メータ確認
		if (!E_Util.isValidateId(investmentId, Schema.E_ITHPF__c.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		//クエリ発行
		E_ITHPF__c  ithpf = E_ITHPFDao.getRecById(investmentId);
		if (ithpf == null) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		if (!Id.valueOf(investmentId).equals(ithpf.id)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		
		if (this instanceOf E_InveExtender) {
			//Referer設定メソッド
			setRefererInve();
		}
		
		return true;
	}
	*/

    /**
	 * ダウンロード関連機能判定
	 */
	protected override boolean doAuthDownload () {
		//オブジェクトアクセス制限
		if (!access.isAccessibleDownload()) {
			errorMsgCode = E_Const.ERROR_MSG_OBJECT;
			return false;
		}
		return true;
	}

	/**
	 * Referer設定メソッド
	 * E_CustomerInfo, E_InsuredInfo, E_ProcessHistoryのみ設定
	 */
	private void setRefererPolicy(){
		String customerInfo = Page.E_CustomerInfo.getUrl().replace('/apex/', '').toLowerCase();
		String insuredInfo = Page.E_InsuredInfo.getUrl().replace('/apex/', '').toLowerCase();
		String processHistory = Page.E_ProcessHistory.getUrl().replace('/apex/', '').toLowerCase();
		String coliSearch = Page.E_ColiSearch.getUrl().replace('/apex/', '').toLowerCase();
		String spvaSearch = Page.E_SpvaSearch.getUrl().replace('/apex/', '').toLowerCase();
		String annuitySearch = Page.E_AnnuitySearch.getUrl().replace('/apex/', '').toLowerCase();

		String iris_owner  = Page.IRIS_Owner.getUrl().replace('/apex/', '').toLowerCase();
		String iris_policy = Page.IRIS_Policys.getUrl().replace('/apex/', '').toLowerCase();
		String iris_group  = Page.IRIS_GroupPolicy.getUrl().replace('/apex/', '').toLowerCase();
		String iris_lapse  = Page.IRIS_LapsedPolicy.getUrl().replace('/apex/', '').toLowerCase();
		String iris_vanish = Page.IRIS_VanishPolicy.getUrl().replace('/apex/', '').toLowerCase();
		String iris_ncol = Page.IRIS_NoticeNCOL.getUrl().replace('/apex/', '').toLowerCase();
		String iris_bila = Page.IRIS_NoticeBILA.getUrl().replace('/apex/', '').toLowerCase();
		String iris_bils = Page.IRIS_NoticeBILS.getUrl().replace('/apex/', '').toLowerCase();
		String iris_agpo = Page.IRIS_NoticeAGPO.getUrl().replace('/apex/', '').toLowerCase();
		
		String referer =  ApexPages.currentPage().getHeaders().get('Referer');
		System.debug('setRefererPolicy==== ' + referer);
		if(referer != null){
			String lowerReferer = referer.toLowerCase();

			if (lowerReferer.indexOf(customerInfo) > 0 || lowerReferer.indexOf(insuredInfo) > 0 || lowerReferer.indexOf(processHistory) > 0
				|| lowerReferer.indexOf(coliSearch) > 0 || lowerReferer.indexOf(spvaSearch) > 0 || lowerReferer.indexOf(annuitySearch) > 0
				|| lowerReferer.indexOf(iris_owner) > 0 || lowerReferer.indexOf(iris_policy) > 0 || lowerReferer.indexOf(iris_lapse) > 0
				|| lowerReferer.indexOf(iris_vanish) > 0 || lowerReferer.indexOf(iris_group) > 0 || lowerReferer.indexOf(iris_ncol) > 0
				|| lowerReferer.indexOf(iris_bila) > 0 || lowerReferer.indexOf(iris_bils) > 0 || lowerReferer.indexOf(iris_agpo) > 0
			) {
				E_CookieHandler.setCookieRefererPolicy(access.getReferer(E_Const.ID_KIND.POLICY));
			}
/*
			if (lowerReferer.indexOf(customerInfo) > 0 || lowerReferer.indexOf(insuredInfo) > 0 || lowerReferer.indexOf(processHistory) > 0
				|| lowerReferer.indexOf(coliSearch) > 0 || lowerReferer.indexOf(spvaSearch) > 0 || lowerReferer.indexOf(annuitySearch) > 0) {
				E_CookieHandler.setCookieRefererPolicy(access.getReferer(E_Const.ID_KIND.POLICY));
			}
*/
		}
	}
	
	/**
	 * Referer設定メソッド
	 * E_Spva, E_Annuityのみ設定
	 */
	private void setRefererPortfolio(){
		String spva = Page.E_Spva.getUrl().replace('/apex/', '').toLowerCase();
		String annuity = Page.E_Annuity.getUrl().replace('/apex/', '').toLowerCase();
		
		String referer =  ApexPages.currentPage().getHeaders().get('Referer');
		if(referer != null){
			String lowerReferer = referer.toLowerCase();
			if (lowerReferer.indexOf(spva) > 0 || lowerReferer.indexOf(annuity) > 0 ) {
				E_CookieHandler.setCookieRefererPortfolio(access.getReferer(E_Const.ID_KIND.POLICY));
			}
		}
	}
	
	/**
	 * Referer設定メソッド
	 * メソッドの呼び出しがなくなったため、@TestVisible追記
	 */
	@TestVisible
	private void setRefererInve(){
		String customerInfo = Page.E_CustomerInfo.getUrl().replace('/apex/', '').toLowerCase();
		String inveSearch = Page.E_InveSearch.getUrl().replace('/apex/', '').toLowerCase();
		String processHistory = Page.E_ProcessHistory.getUrl().replace('/apex/', '').toLowerCase();
		
		String referer =  ApexPages.currentPage().getHeaders().get('Referer');
		if(referer != null){
			String lowerReferer = referer.toLowerCase();
			if (lowerReferer.indexOf(customerInfo) > 0 || lowerReferer.indexOf(inveSearch) > 0 || lowerReferer.indexOf(processHistory) > 0) {
				E_CookieHandler.setCookieRefererInve(access.getReferer(E_Const.ID_KIND.INVESTMENT));
			}
		}
	}
	
	/**
	 * Referer設定メソッド
	 * 部分リリースにカバレッジが不足しているため、@TestVisible追記
	 */
	@TestVisible
	private void setRefererHistory(boolean isPolicy){
		String customerInfo = Page.E_CustomerInfo.getUrl().replace('/apex/', '').toLowerCase();

		String referer =  ApexPages.currentPage().getHeaders().get('Referer');
		if(referer != null){
			String lowerReferer = referer.toLowerCase();
			if (lowerReferer.indexOf(customerInfo) > 0) {
				if (isPolicy) {
					E_CookieHandler.setCookieRefererHistory(access.getReferer(E_Const.ID_KIND.POLICY));
				} else {
					system.debug('■setCookieRefererHistory='+access.getReferer(E_Const.ID_KIND.CONTACT));
					E_CookieHandler.setCookieRefererHistory(access.getReferer(E_Const.ID_KIND.CONTACT));
				}
			}
		}
	}
}