global with sharing class E_DownloadAgentLkupController extends SkyEditor2.SkyEditorPageBaseWithSharing{
    
    public E_IDCPF__c record{get;set;}
    public E_DownloadAgentLkupExtender getExtender() {return (E_DownloadAgentLkupExtender)extender;}
    public searchDataTable searchDataTable {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c ipVal_agentcd{get;set;}
    public SkyEditor2__SkyEditorDummy__c ipVal_agentkana{get;set;}
    public E_DownloadAgentLkupController(ApexPages.StandardController controller){
        super(controller);

        SObjectField f;
        f = E_IDCPF__c.fields.ContactCL3PF_AGNTNUM__c;
        f = E_IDCPF__c.fields.ContactCL3PF_ZEATKNAM__c;
        f = E_IDCPF__c.fields.ContactName__c;
        f = E_IDCPF__c.fields.ContactAccParentName__c;
        f = E_IDCPF__c.fields.ContactAccount__c;
        f = E_IDCPF__c.fields.ZIDTYPE__c;
        f = E_IDCPF__c.fields.ContactCL3PF_VALIDFLAG__c;

        try {
            mainRecord = null;
            mainSObjectType = E_IDCPF__c.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            
            ipVal_agentcd = new SkyEditor2__SkyEditorDummy__c();
            ipVal_agentkana = new SkyEditor2__SkyEditorDummy__c();
            
            queryMap.put(
                'searchDataTable',
                new SkyEditor2.Query('E_IDCPF__c')
                    .addFieldAsOutput('ContactCL3PF_AGNTNUM__c')
                    .addFieldAsOutput('ContactName__c')
                    .addFieldAsOutput('ContactAccParentName__c')
                    .addFieldAsOutput('ContactAccount__c')
                    .addField('Name')
                    .limitRecords(100)
                    .addListener(new SkyEditor2.QueryWhereRegister(ipVal_agentcd, 'SkyEditor2__Text__c', 'ContactCL3PF_AGNTNUM__c', new SkyEditor2.TextHolder('eq'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(ipVal_agentkana, 'SkyEditor2__Text__c', 'ContactCL3PF_ZEATKNAM__c', new SkyEditor2.TextHolder('co'), false, true, false))
                     .addWhere(' ( ZIDTYPE__c = \'AT\' AND ContactCL3PF_AGNTNUM__c != \'\' AND ContactCL3PF_VALIDFLAG__c = \'1\')')
.addSort('ContactCL3PF_ZEATKNAM__c',True,True)
            );
            
            searchDataTable = new searchDataTable(new List<E_IDCPF__c>(), new List<searchDataTableItem>(), new List<E_IDCPF__c>(), null);
            listItemHolders.put('searchDataTable', searchDataTable);
            
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_IDCPF__c.SObjectType);
            
            p_showHeader = false;
            p_sidebar = false;
            presetSystemParams();
            extender = new E_DownloadAgentLkupExtender(this);
            initSearch();
            
            extender.init();
            
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }

    public List<SelectOption> getOperatorOptions_E_IDCPF_c_ContactCL3PF_AGNTNUM_c() {
        return getOperatorOptions('E_IDCPF__c', 'ContactCL3PF_AGNTNUM__c');
    }
    public List<SelectOption> getOperatorOptions_E_IDCPF_c_ContactCL3PF_ZEATKNAM_c() {
        return getOperatorOptions('E_IDCPF__c', 'ContactCL3PF_ZEATKNAM__c');
    }
    
    global with sharing class searchDataTableItem extends SkyEditor2.ListItem {
        public E_IDCPF__c record{get; private set;}
        @TestVisible
        searchDataTableItem(searchDataTable holder, E_IDCPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class searchDataTable extends SkyEditor2.ListItemHolder {
        public List<searchDataTableItem> items{get; private set;}
        @TestVisible
        searchDataTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<searchDataTableItem>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new searchDataTableItem(this, (E_IDCPF__c)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
}