@isTest(SeeAllData=false)
private class TestE_ErrorPage {

	//追加メッセージなし通常パターン
	private static testMethod void standardTest(){
		string key = 'test';
		string value = 'val';
		E_MessageMaster__c insMSG = new E_MessageMaster__c(Key__c = key,Value__c = value);
		insert insMSG;
		PageReference pref = Page.E_ErrorPage;
		pref.getParameters().put('code',key);
		test.setCurrentPage(pref);
		E_ErrorPage controller = new E_ErrorPage();
		controller.init();
		system.assertEquals(value, controller.getPageMessages().getErrorMessages()[0].summary);

	}

	//追加メッセージあり通常パターン
	private static testMethod void standardPNMTest(){
		string key = 'test';
		String[] pnm = new String[]{'pnmVal1','pnmVal2','pnmVal3'};
		string value = 'test{0}_test{1}_test{2}';
		E_MessageMaster__c insMSG = new E_MessageMaster__c(Key__c = key,Value__c = value);
		insert insMSG;
		PageReference pref = Page.E_ErrorPage;
		pref.getParameters().put('code',key);
		pref.getParameters().put('p1',pnm[0]);
		pref.getParameters().put('p2',pnm[1]);
		pref.getParameters().put('p3',pnm[2]);
		test.setCurrentPage(pref);
		E_ErrorPage controller = new E_ErrorPage();
		controller.init();
		String assertValue = String.format(value, pnm);
		system.assertEquals(assertValue, controller.getPageMessages().getErrorMessages()[0].summary);
	}

	//追加メッセージなし通常パターン
	private static testMethod void ErrorTest(){
		string key = 'test';
		string value = 'エラーが発生しました。';
		PageReference pref = Page.E_ErrorPage;
		pref.getParameters().put('code',key);
		test.setCurrentPage(pref);
		E_ErrorPage controller = new E_ErrorPage();
		controller.init();
		controller.getIsClosebtn();
		I_MenuMaster__c menu = controller.menu;
		system.assertEquals(value, controller.getPageMessages().getErrorMessages()[0].summary);

	}

	private static testMethod void pipErrorTest(){
		string key = I_Const.ERR_URLPARAM_KEY;
		string value = '1';
		PageReference pref = Page.E_ErrorPage;
		pref.getParameters().put(key,value);
		test.setCurrentPage(pref);
		E_ErrorPage controller = new E_ErrorPage();
		system.assertEquals(true, controller.pipflag);

	}

}