/*
 * CC_PolicyDetailVOIControllerTest
 * Test class of CC_PolicyDetailVOIController
 * created  : Accenture 2018/10/4
 * modified :
 */

@isTest
private class CC_PolicyDetailVOIControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	 * Prepare test data
	 */
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create Account test data(Account0,Account1)
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '代理店' limit 1];
			List<Account> accountList = CC_TestDataFactory.getAccountSkelList(recordTypeOfAccount.Id);
			insert accountList;

			//Create Contact test data(Contact0,Contact1)
			List<Contact> contactList = CC_TestDataFactory.getContactSkelList();
			for(Contact contact : contactList){
				contact.AccountId = accountList.get(0).Id;
			}
			insert contactList;

			//Create EPolicy test data(with Contact0)
			E_Policy__c newEPolicy = CC_TestDataFactory.getEPolicySkel(accountList.get(0).Id, contactList.get(0).Id);
			newEPolicy.Contractor__c = contactList.get(0).Id;
			newEPolicy.COMM_ZCOVRNAM__c = '定期保険';
			insert newEPolicy;

		}
	}

	/**
	* Test Before Insert01
	* CMN_ContactName__c != null
	*/
	static testMethod void invokeMethodTest01() {
		CC_PolicyDetailVOIController ctrl = new CC_PolicyDetailVOIController();
		String methodName = 'getData';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {
			try{

				List<E_Policy__c> policyList = [SELECT Contractor__c FROM E_Policy__c];
				inputMap.put('Id', (Object)policyList[0].Contractor__c);

				Test.startTest();
				ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}
}