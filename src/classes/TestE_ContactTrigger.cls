@isTest
private class TestE_ContactTrigger{

	@testSetup
	static void createBaseContactRecs(){
	}

	//Contactのレコードタイプの設定テスト:BeforeInsert
	static testMethod void updateRecordTypeTestBeforeInsert(){
		Test.startTest();

		//202件のContactを作成：半数は募集人コードへ値を設定
		Integer agentCnt = 0;
		Integer clientCnt = 0;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				testBaseContacts.add(new Contact(LastName = 'TestCon' + i, E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')));
				agentCnt++;
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestCon' + i));
				clientCnt++;
			}
		}
		insert testBaseContacts;
		
		Test.stopTest();

		//募集人レコードタイプのcontactをカウント
		Integer agentRecTypeCnt = 0;
		Integer clientRecTypeCnt = 0;
		List<Contact> testContacts = [select Id, RecordType.DeveloperName, E_CL3PF_AGNTNUM__c from Contact];
		for(Contact con : testContacts){
			if(con.RecordType.DeveloperName == E_Const.CONTACT_RECORDTYPE_AGENT){
				agentRecTypeCnt++;
			} else if(con.RecordType.DeveloperName == E_Const.CONTACT_RECORDTYPE_CLIENT){
				clientRecTypeCnt++;
			}
		}

		System.assertEquals(agentCnt, agentRecTypeCnt);
		System.assertEquals(clientCnt, clientRecTypeCnt);
	}

	//Contactのレコードタイプの設定テスト:BeforeUpdate
	static testMethod void updateRecordTypeTestBeforeUpdate(){

		//202件のContactを作成：半数は募集人コードへ値を設定
		Integer agentCnt = 0;
		Integer clientCnt = 0;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				testBaseContacts.add(new Contact(LastName = 'TestCon' + i, E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')));
				agentCnt++;
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestCon' + i));
				clientCnt++;
			}
		}
		insert testBaseContacts;

		List<Contact> updateContacts = new List<Contact>();
		Test.startTest();
		for(Contact con : [select id,name,RecordTypeId from Contact where id in :(new Map<Id,Contact>(testBaseContacts)).keySet()]){
			con.RecordTypeId = null;
			updateContacts.add(con);
		}
		//レコードタイプをNULLに更新してもbeforeUpdateで再度設定される
		update updateContacts;
		
		Test.stopTest();

		//募集人レコードタイプのcontactをカウント
		Integer agentRecTypeCnt = 0;
		Integer clientRecTypeCnt = 0;
		List<Contact> testContacts = [select Id, RecordType.DeveloperName, E_CL3PF_AGNTNUM__c from Contact];
		for(Contact con : testContacts){
			if(con.RecordType.DeveloperName == E_Const.CONTACT_RECORDTYPE_AGENT){
				agentRecTypeCnt++;
			} else if(con.RecordType.DeveloperName == E_Const.CONTACT_RECORDTYPE_CLIENT){
				clientRecTypeCnt++;
			}
		}

		System.assertEquals(agentCnt, agentRecTypeCnt);
		System.assertEquals(clientCnt, clientRecTypeCnt);
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert1
	//顧客レコードと募集人レコードが同一トランザクション
	static testMethod void updateAgentToClientRelationTestAfterInsert01(){

		Test.startTest();

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				clntnum = E_Util.leftPad(String.valueOf(i), 8, '0');
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = clntnum,
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = clntnum
												)
									);
			}
		}
		insert testBaseContacts;
		
		Test.stopTest();

		//募集人レコードのParentECLT__cに設定された全IDが、顧客レコードの全IDに含まれること
		List<Contact> testContacts = [select Id, ParentECLT__c, E_CL3PF_AGNTNUM__c from Contact];
		Set<Id> clientRecIdSet = new Set<Id>();
		Set<Id> referenceClientRecIdSet = new Set<Id>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				referenceClientRecIdSet.add(con.ParentECLT__c);
			} else if(con.E_CL3PF_AGNTNUM__c == null){
				clientRecIdSet.add(con.Id);
			}
		}
		System.assertEquals(true, clientRecIdSet.containsAll(referenceClientRecIdSet));
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert2
	//顧客レコードと募集人レコードが別トランザクション
	//先に顧客を作成
	static testMethod void updateAgentToClientRelationTestAfterInsert02(){

		Test.startTest();

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 101; i++){
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0')
												)
									);
		}

		insert testBaseContacts;
		testBaseContacts.clear();

		for(Integer i=0; i < 101; i++){
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0'),
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
		}
		insert testBaseContacts;
		
		Test.stopTest();

		//募集人レコードのParentECLT__cに設定された全IDが、顧客レコードの全IDに含まれること
		List<Contact> testContacts = [select Id, ParentECLT__c, E_CL3PF_AGNTNUM__c from Contact];
		Set<Id> clientRecIdSet = new Set<Id>();
		Set<Id> referenceClientRecIdSet = new Set<Id>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				referenceClientRecIdSet.add(con.ParentECLT__c);
			} else if(con.E_CL3PF_AGNTNUM__c == null){
				clientRecIdSet.add(con.Id);
			}
		}
		System.assertEquals(true, clientRecIdSet.containsAll(referenceClientRecIdSet));
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert3
	//顧客レコードと募集人レコードが別トランザクション
	//先に募集人を作成
	static testMethod void updateAgentToClientRelationTestAfterInsert03(){

		Test.startTest();

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 101; i++){
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0'),
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
		}

		insert testBaseContacts;
		testBaseContacts.clear();

		for(Integer i=0; i < 101; i++){
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0')
												)
									);
		}
		insert testBaseContacts;
		
		Test.stopTest();

		//募集人レコードのParentECLT__cに設定された全IDが、顧客レコードの全IDに含まれること
		List<Contact> testContacts = [select Id, ParentECLT__c, E_CL3PF_AGNTNUM__c from Contact];
		Set<Id> clientRecIdSet = new Set<Id>();
		Set<Id> referenceClientRecIdSet = new Set<Id>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				referenceClientRecIdSet.add(con.ParentECLT__c);
			} else if(con.E_CL3PF_AGNTNUM__c == null){
				clientRecIdSet.add(con.Id);
			}
		}
		System.assertEquals(true, clientRecIdSet.containsAll(referenceClientRecIdSet));
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert4
	//処理に必要な項目が未設定
	static testMethod void updateAgentToClientRelationTestAfterInsert04(){

		Test.startTest();

		//202件のContactを作成：顧客番号や募集人顧客コードが未設定
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i));
		}
		insert testBaseContacts;
		
		Test.stopTest();

		//全レコードのParentECLT__cがNullであること
		List<Contact> testContacts = [select Id, ParentECLT__c from Contact];
		Integer cnt = 0;
		for(Contact con : testContacts){
			if(con.ParentECLT__c == null){
				cnt += 1;
			}
		}
		System.assertEquals(cnt, testContacts.size());
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert5
	//顧客番号と募集人顧客コードに同じ値が設定されたcontactレコードが1件
	static testMethod void updateAgentToClientRelationTestAfterInsert05(){

		Test.startTest();

		//1件のContactを作成
		//顧客番号と募集人顧客コードに同じ値が設定
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 1; i++){
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0'),
												 E_CL3PF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0'),
												 E_CLTPF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0'))
									);
		}
		insert testBaseContacts;

		Test.stopTest();

		//contactが既存含めて1件のみ登録されており、顧客番号と募集人顧客コードに同じ値が設定されている場合、自レコードを主ECLTに設定しないこと
		List<Contact> testContacts = [select Id, ParentECLT__c from Contact];
		System.assertEquals(null, testContacts[0].ParentECLT__c);
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert6
	//顧客番号と募集人顧客コードに同じ値が設定されたcontactレコードが5件
	static testMethod void updateAgentToClientRelationTestAfterInsert06(){

		Test.startTest();

		//5件のContactを作成
		//顧客番号と募集人顧客コードに同じ値が設定
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 5; i++){
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0'),
												 E_CL3PF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0'),
												 E_CLTPF_CLNTNUM__c = E_Util.leftPad(String.valueOf(i), 8, '0'))
									);
		}
		insert testBaseContacts;

		Test.stopTest();

		//contactが顧客番号と募集人顧客コードに同じ値が設定されている場合、自レコードを主ECLTに設定しないこと
		List<Contact> testContacts = [select Id, ParentECLT__c from Contact];
		for(Contact assertCon : testContacts){
			System.assertEquals(null, assertCon.ParentECLT__c);
		}
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert7
	//顧客番号と募集人顧客コードがNULLのcontactレコードが5件
	static testMethod void updateAgentToClientRelationTestAfterInsert07(){

		Test.startTest();

		//5件のContactを作成
		//顧客番号と募集人顧客コードがNull、募集人コード非NULL
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 5; i++){
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0'),
												 E_CL3PF_CLNTNUM__c = null,
												 E_CLTPF_CLNTNUM__c = null)
									);
		}
		insert testBaseContacts;

		Test.stopTest();

		//自レコードを主ECLTに設定しないこと
		List<Contact> testContacts = [select Id, ParentECLT__c from Contact];
		for(Contact assertCon : testContacts){
			System.assertEquals(null, assertCon.ParentECLT__c);
		}
	}

	//募集人レコードへの関連顧客の設定テストAfterInsert8
	//関連するレコードが別トランザクション
	static testMethod void updateAgentToClientRelationTestAfterInsert08(){

		Test.startTest();

		//1件単位でContactを作成
		Contact testCon1 = new Contact(LastName = 'TestClient', E_CLTPF_CLNTNUM__c = '12345678', E_CL3PF_CLNTNUM__c = null);
		insert testCon1;
		Contact testCon2 = new Contact(LastName = 'TestAgent1', E_CL3PF_CLNTNUM__c = '12345678', E_CL3PF_AGNTNUM__c = 'A1234');
		insert testCon2;
		Contact testCon3 = new Contact(LastName = 'TestAgent2', E_CL3PF_CLNTNUM__c = '12345678', E_CL3PF_AGNTNUM__c = 'B1234');
		insert testCon3;

		Test.stopTest();

		//募集人レコードの主ECLTに顧客レコードのIｄが設定されていること
		List<Contact> testContacts = [select Id, ParentECLT__c, E_CL3PF_AGNTNUM__c from Contact where E_CL3PF_AGNTNUM__c != null];
		for(Contact assertCon : testContacts){
			System.assertEquals(testCon1.Id, assertCon.ParentECLT__c);
		}
	}

	//募集人レコードへの関連顧客の設定テストAfterUpdate1
	//更新対象あり：主ECLTがNull
	static testMethod void updateAgentToClientRelationTestAfterUpdate01(){

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				clntnum = E_Util.leftPad(String.valueOf(i), 8, '0');
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = clntnum,
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = clntnum
												)
									);
			}
		}
		//insertトリガをスキップさせる
		E_ContactTriggerHandler.isSkipTriggerActions = true;
		insert testBaseContacts;
		E_ContactTriggerHandler.isSkipTriggerActions = false;

		Test.startTest();
		//全Contactを空更新する:insert時にトリガがスキップされているため主ECLTが未設定
		List<Contact> updateContacts = [select id,name,ParentECLT__c from Contact];
		update updateContacts;
		Test.stopTest();

		//募集人レコードのParentECLT__cに設定された全IDが、顧客レコードの全IDに含まれること
		List<Contact> testContacts = [select Id, ParentECLT__c, E_CL3PF_AGNTNUM__c from Contact];
		Set<Id> clientRecIdSet = new Set<Id>();
		Set<Id> referenceClientRecIdSet = new Set<Id>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				referenceClientRecIdSet.add(con.ParentECLT__c);
			} else if(con.E_CL3PF_AGNTNUM__c == null){
				clientRecIdSet.add(con.Id);
			}
		}
		System.assertEquals(true, clientRecIdSet.containsAll(referenceClientRecIdSet));
	}	

	//募集人レコードへの関連顧客の設定テストAfterUpdate2
	//更新対象あり：募集人顧客コード及び顧客番号が同じ値に変更された場合
	static testMethod void updateAgentToClientRelationTestAfterUpdate02(){

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				clntnum = E_Util.leftPad(String.valueOf(i), 8, '0');
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = clntnum,
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = clntnum
												)
									);
			}
		}
		//insertトリガで主ECLTを設定させる
		insert testBaseContacts;

		Test.startTest();
		List<Contact> updateContacts = [select id,name,ParentECLT__c,E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c from Contact];
		//募集人顧客コード又は顧客番号を+1に変更する
		for(Contact con : updateContacts){
			if(con.E_CL3PF_CLNTNUM__c != null){
				con.E_CL3PF_CLNTNUM__c = String.valueOf(Integer.valueOf(con.E_CL3PF_CLNTNUM__c) + 1);
			} else if(con.E_CLTPF_CLNTNUM__c != null){
				con.E_CLTPF_CLNTNUM__c = String.valueOf(Integer.valueOf(con.E_CLTPF_CLNTNUM__c) + 1);
			}
		}
		update updateContacts;
		Test.stopTest();

		//募集人レコードのParentECLT__cに設定された全IDが、顧客レコードの全IDに含まれること
		List<Contact> testContacts = [select Id, ParentECLT__c,E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c,E_CL3PF_AGNTNUM__c from Contact];
		Set<Id> clientRecIdSet = new Set<Id>();
		Set<Id> referenceClientRecIdSet = new Set<Id>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				referenceClientRecIdSet.add(con.ParentECLT__c);
			} else if(con.E_CL3PF_AGNTNUM__c == null){
				clientRecIdSet.add(con.Id);
			}
		}
		System.assertEquals(true, clientRecIdSet.containsAll(referenceClientRecIdSet));
	}

	//募集人レコードへの関連顧客の設定テストAfterUpdate3
	//更新対象あり：募集人顧客コードのみ変更され、関連付く顧客レコードが存在しなくなる場合
	static testMethod void updateAgentToClientRelationTestAfterUpdate03(){

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				clntnum = E_Util.leftPad(String.valueOf(i), 8, '0');
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = clntnum,
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = clntnum
												)
									);
			}
		}
		//insertトリガで主ECLTを設定させる
		insert testBaseContacts;

		Test.startTest();
		List<Contact> updateContacts = [select id,name,ParentECLT__c,E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c from Contact];
		//募集人顧客コードを+1に変更する
		for(Contact con : updateContacts){
			if(con.E_CL3PF_CLNTNUM__c != null){
				con.E_CL3PF_CLNTNUM__c = String.valueOf(Integer.valueOf(con.E_CL3PF_CLNTNUM__c) + 1);
			} else if(con.E_CLTPF_CLNTNUM__c != null){
				//顧客番号は変更しない
			}
		}
		update updateContacts;
		Test.stopTest();

		//募集人レコードのParentECLT__cが全てNULLであること
		List<Contact> testContacts = [select Id, ParentECLT__c,E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c,E_CL3PF_AGNTNUM__c from Contact];
		Integer conParentECLTNum = 0;
		Set<Contact> agentRecs = new Set<Contact>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				agentRecs.add(con);
			}
			if(con.E_CL3PF_AGNTNUM__c != null && con.ParentECLT__c == null){
				conParentECLTNum++;
			}
		}
		System.assertEquals(conParentECLTNum, agentRecs.size());
	}

	//募集人レコードへの関連顧客の設定テストAfterUpdate4
	//更新対象あり：顧客番号のみ変更され、関連付く募集人レコードが存在しなくなる場合
	static testMethod void updateAgentToClientRelationTestAfterUpdate04(){

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				clntnum = E_Util.leftPad(String.valueOf(i), 8, '0');
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = clntnum,
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = clntnum
												)
									);
			}
		}
		//insertトリガで主ECLTを設定させる
		insert testBaseContacts;

		Test.startTest();
		List<Contact> updateContacts = [select id,name,ParentECLT__c,E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c from Contact];
		//顧客番号を+1に変更する
		for(Contact con : updateContacts){
			if(con.E_CL3PF_CLNTNUM__c != null){
				//募集人顧客コードは変更しない
			} else if(con.E_CLTPF_CLNTNUM__c != null){
				con.E_CLTPF_CLNTNUM__c = String.valueOf(Integer.valueOf(con.E_CLTPF_CLNTNUM__c) + 1);
				
			}
		}
		update updateContacts;
		Test.stopTest();

		//募集人レコードのParentECLT__cが全てNULLであること
		List<Contact> testContacts = [select Id, ParentECLT__c,E_CL3PF_CLNTNUM__c,E_CLTPF_CLNTNUM__c,E_CL3PF_AGNTNUM__c from Contact];
		Integer conParentECLTNum = 0;
		Set<Contact> agentRecs = new Set<Contact>();

		for(Contact con : testContacts){
			if(con.E_CL3PF_AGNTNUM__c != null){
				agentRecs.add(con);
			}
			if(con.E_CL3PF_AGNTNUM__c != null && con.ParentECLT__c == null){
				conParentECLTNum++;
			}
		}
		System.assertEquals(conParentECLTNum, agentRecs.size());
	}

	//募集人レコードへの関連顧客の設定テストAfterUpdate10
	//更新対象なし：
	static testMethod void updateAgentToClientRelationTestAfterUpdate10(){

		//202件のContactを作成：半数は募集人として作成し、募集人顧客コードを設定。
		//残りの半数の顧客番号に募集人と同じ値を設定。
		String clntnum;
		List<Contact> testBaseContacts = new List<Contact>();
		for(Integer i=0; i < 202; i++){
			if(math.mod(i, 2) == 0){
				clntnum = E_Util.leftPad(String.valueOf(i), 8, '0');
				testBaseContacts.add(new Contact(LastName = 'TestAgent' + i, 
												 E_CL3PF_CLNTNUM__c = clntnum,
												 E_CL3PF_AGNTNUM__c = E_Util.leftPad(String.valueOf(i), 5, '0')
												 )
									);
			} else {
				testBaseContacts.add(new Contact(LastName = 'TestClient' + i,
												 E_CLTPF_CLNTNUM__c = clntnum
												)
									);
			}
		}
		//insertトリガで主ECLTを設定させる
		insert testBaseContacts;

		Test.startTest();
		//全Contactを空更新する
		List<Contact> updateContacts = [select id,name,ParentECLT__c from Contact];
		update updateContacts;
		Test.stopTest();

		//updateトリガによる更新対象が存在しなかったため、主ECLTが更新されて無いこと
		List<Contact> testContacts = [select id,name,ParentECLT__c from Contact where id in :(new Map<Id,Contact>(updateContacts)).keySet()];
		System.assertEquals(true, testContacts.equals(updateContacts));
	}
}