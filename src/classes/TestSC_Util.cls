@isTest
private class TestSC_Util {

	// 自主点検事務所
	private static SC_Office__c scOffice;
	//
	private static Account accParent;
	//
	private static List<Account> accOfficeList;
	// 
	private static List<Contact> contactList;
	// 
	private static List<User> userList;
	// 
	private static List<SC_AgencyManager__c> agManagerList;
	// 
	private static Set<String> agentType = new Set<String>{'AH-Y', 'AH-N', 'AY-Y', 'AY-N','AT-Y', 'AT-N'};
	


	/**
	 * void createScAgencyManagers(Set<Id> scOfficeIds)
	 */
	@isTest static void createScAgencyManagers_test001() {
		// TestData
		createTestData(true);

		Test.startTest();
		SC_Util util = new SC_Util();
		util.createScAgencyManagers(new Set<Id>{scOffice.Id});
		Test.stopTest();

		Map<String, SC_AgencyManager__c> resultMap = new Map<String, SC_AgencyManager__c>();
		for(SC_AgencyManager__c result : [Select Id, User__r.LastName, Contact__r.LastName From SC_AgencyManager__c Where SC_Office__r.Id = :scOffice.Id]){
			resultMap.put(result.Contact__r.LastName, result);
		}
		System.assertEquals(6, resultMap.size());
		System.assertEquals(true, resultMap.get('T0001TestUserAH-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAH-N') != null);
		System.assertEquals(true, resultMap.get('T0001TestUserAY-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAY-N') != null);
		System.assertEquals(true, resultMap.get('T0001TestUserAT-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAT-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAH-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAH-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAY-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAY-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAT-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAT-N') != null);
	}

	/**
	 * void createScAgencyManagers(Set<Id> scOfficeIds)
	 */
	@isTest static void createScAgencyManagers_test002() {
		// TestData
		createTestData(true);
		createAgManagerList(contactList);

		Test.startTest();
		SC_Util util = new SC_Util();
		util.createScAgencyManagers(new Set<Id>{scOffice.Id});
		Test.stopTest();

		Map<String, SC_AgencyManager__c> resultMap = new Map<String, SC_AgencyManager__c>();
		for(SC_AgencyManager__c result : [Select Id, User__r.LastName, Contact__r.LastName From SC_AgencyManager__c Where SC_Office__r.Id = :scOffice.Id]){
			resultMap.put(result.Contact__r.LastName, result);
		}
		System.assertEquals(6, resultMap.size());
		System.assertEquals(true, resultMap.get('T0001TestUserAH-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAH-N') != null);
		System.assertEquals(true, resultMap.get('T0001TestUserAY-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAY-N') != null);
		System.assertEquals(true, resultMap.get('T0001TestUserAT-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAT-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAH-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAH-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAY-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAY-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAT-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAT-N') != null);
	}
	
	/**
	 * void createScAgencyManagers(Set<Id> scOfficeIds)
	 */
	@isTest static void createScAgencyManagers_test003() {
		// TestData
		createTestData(true);

		E_IDCPF__c targetRec = [Select Id, ZSTATUS01__c From E_IDCPF__c Where User__r.LastName = 'T0001TestUserAH-Y'];
		targetRec.ZSTATUS01__c = '2';
		update targetRec;

		Test.startTest();
		SC_Util util = new SC_Util();
		util.createScAgencyManagers(new Set<Id>{scOffice.Id});
		Test.stopTest();

		Map<String, SC_AgencyManager__c> resultMap = new Map<String, SC_AgencyManager__c>();
		for(SC_AgencyManager__c result : [Select Id, User__r.LastName, Contact__r.LastName From SC_AgencyManager__c Where SC_Office__r.Id = :scOffice.Id]){
			resultMap.put(result.Contact__r.LastName, result);
		}
		System.assertEquals(5, resultMap.size());
		System.assertEquals(false, resultMap.get('T0001TestUserAH-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAH-N') != null);
		System.assertEquals(true, resultMap.get('T0001TestUserAY-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAY-N') != null);
		System.assertEquals(true, resultMap.get('T0001TestUserAT-Y') != null);
		System.assertEquals(false, resultMap.get('T0001TestUserAT-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAH-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAH-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAY-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAY-N') != null);
		System.assertEquals(true, resultMap.get('T0002TestUserAT-Y') != null);
		System.assertEquals(false, resultMap.get('T0002TestUserAT-N') != null);
	}

	/**
	 * void updateAgencyManagerHasUserEmail(List<User> newList, Map<Id, User> oldMap)
	 */
	@isTest static void updateAgencyManagerHasUserEmail_test001() {
		// TestData
		createTestData(true);

		Test.startTest();
		SC_Util util = new SC_Util();
		util.createScAgencyManagers(new Set<Id>{scOffice.Id});

		List<User> newList = new List<User>();
		Map<Id, User> oldMap = new Map<Id, User>();
		Map<Id, Boolean> beforeMap = new Map<Id, Boolean>();

		SC_AgencyManager__c am = [Select Id, User__r.Id, HasUserEMail__c From SC_AgencyManager__c Where SC_Office__r.Id = :scOffice.Id And User__r.Lastname = 'T0001TestUserAH-Y'];
		beforeMap.put(am.User__r.Id, am.HasUserEMail__c);

		User u = [Select Id, LastName, Email From User Where Id = :am.User__r.Id];
		oldMap.put(u.Id, u);

		// 無効メールアドレス
		User un = [Select Id, LastName, Email From User Where Id = :am.User__r.Id];
		un.Email = 'testtest@sfdc.com';
		newList.add(un);

		util.updateAgencyManagerHasUserEmail(newList, oldMap);
		Test.stopTest();
		
		SC_AgencyManager__c result = [Select Id, User__r.Id, HasUserEMail__c From SC_AgencyManager__c Where Id = :am.Id];
		System.assertEquals(false, result.HasUserEMail__c);
	}

	/**
	 * void updateAgencyManagerHasUserEmail(List<User> newList, Map<Id, User> oldMap)
	 */
	@isTest static void updateAgencyManagerHasUserEmail_test002() {
		// TestData
		createTestData(false);

		Test.startTest();
		SC_Util util = new SC_Util();
		util.createScAgencyManagers(new Set<Id>{scOffice.Id});

		List<User> newList = new List<User>();
		Map<Id, User> oldMap = new Map<Id, User>();
		Map<Id, Boolean> beforeMap = new Map<Id, Boolean>();

		SC_AgencyManager__c am = [Select Id, User__r.Id, HasUserEMail__c From SC_AgencyManager__c Where SC_Office__r.Id = :scOffice.Id And User__r.Lastname = 'T0001TestUserAH-Y'];
		beforeMap.put(am.User__r.Id, am.HasUserEMail__c);

		User u = [Select Id, LastName, Email From User Where Id = :am.User__r.Id];
		oldMap.put(u.Id, u);

		// 有効メールアドレス
		User un = [Select Id, LastName, Email From User Where Id = :am.User__r.Id];
		un.Email = 'testtest@tstestnn.com';
		newList.add(un);

		util.updateAgencyManagerHasUserEmail(newList, oldMap);
		Test.stopTest();
		
		SC_AgencyManager__c result = [Select Id, User__r.Id, HasUserEMail__c From SC_AgencyManager__c Where Id = :am.Id];
		System.assertEquals(true, result.HasUserEMail__c);
	}
	
	/**
	 * TestData 自主点検事務所
	 */
	private static void createTestData(Boolean isActiveEmail){
		// 1. 取引先（格）を作成
		accParent = TestSC_TestUtil.createAccount(true, null);

		// 2. 取引先（事務所）を作成
		accOfficeList = new List<Account>();
		Account acc1 = TestSC_TestUtil.createAccount(false, accParent, 'T0001');
		acc1.ParentId = accParent.Id;
		Account acc2 = TestSC_TestUtil.createAccount(false, accParent, 'T0002');
		acc2.ParentId = accParent.Id;
		accOfficeList.add(acc1);
		accOfficeList.add(acc2);
		insert accOfficeList;

		// 3. 取引先責任者を作成
		Id rtIdAgent = E_RecordTypeDao.getRecIdBySobjectTypeAndDevName(E_Const.CONTACT_RECORDTYPE_AGENT, SObjectType.Contact.Name);
		contactList = new List<Contact>();
		for(Account office : accOfficeList){
			for(String at : agentType){
				String lastName = office.E_CL2PF_ZAGCYNUM__c + 'TestUser' + at;
				Contact con = TestSC_TestUtil.createContact(false, office.Id, lastName);
				con.AccountId = office.Id;
				con.RecordTypeId = rtIdAgent;
				con.E_CL3PF_AGNTNUM__c = office.E_CL2PF_ZAGCYNUM__c + at;
				// 業務管理責任者セット
				if(at.contains('-Y')){
					con.E_CL3PF_ZAGMANGR__c = 'Y';
				}
				contactList.add(con);
			}
		}
		insert contactList;

		// 4. 実行ユーザを作成
		userList = new List<User>();
		for(Contact con : contactList){
			if(isActiveEmail){
				userList.add(TestSC_TestUtil.createAgentUser(false, con.LastName, 'E_PartnerCommunity', con.Id));
			}else{
				userList.add(TestSC_TestUtil.createAgentUser(false, con.LastName, 'E_PartnerCommunity', con.Id, 'testtest@sfdc.com'));
			}
		}
		insert userList;
		
		// 5. ID管理を作成
		List<E_IDCPF__c> idcpfList = new List<E_IDCPF__c>();
		for(User u : userList){
			String idType = u.LastName.substring(u.LastName.length() -2, u.LastName.length());
			idcpfList.add(TestSC_TestUtil.createIDCPF(false, u.Id, idType));
		}
		insert idcpfList;

		// 6. 自主点検事務所を更新
		scOffice = TestSC_TestUtil.createSCOfficeByFiscalYear(true, accOfficeList[0].Id, null, '2018');
		scOffice.ZAGCYNUM__c =  accOfficeList[0].E_CL2PF_ZAGCYNUM__c;
		scOffice.Account__c = accOfficeList[0].Id;
		update scOffice;
	}

	/**
	 * TestData 自主点検業務管理責任者
	 */
	private static void createAgManagerList(List<Contact> contacts){
		agManagerList = new List<SC_AgencyManager__c>();

		Map<Id, User> userMap = new Map<Id, User>();
		for(User u : userList){
			userMap.put(u.ContactId, u);
		}

		// 業務管理責任者（SC_AgencyManager__c）
		for(Contact c : contacts){
			SC_AgencyManager__c agManager = new SC_AgencyManager__c();
			agManager.SC_Office__c = scOffice.Id;
			agManager.Contact__c = c.Id;
			agManager.User__c = userMap.get(c.Id).Id;
			agManagerList.add(agManager);
		}
		insert agManagerList;
	}
}