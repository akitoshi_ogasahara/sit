public abstract class I_AbstractController extends E_AbstractController{
	// IRISページ
	public I_PageMaster__c activePage {get; set;}
	// IRISメニュー
	public I_MenuMaster__c menu {get{
										if(menu==null){
											menu = iris.irisMenus.activeMenu;
										}
										return menu;
									} 
									set;}
	
	//Constructor
	public I_AbstractController(){
		try{
			Id mnId = getIrisMenuId();
			if(String.isNotBlank(mnId)){
				iris.setIrisMenuId(mnId);
			}
			//ActiveMenuが取得できていないときはメニューキーから設定
			if(iris.irisMenus.activeMenu==null){
				iris.setLinkMenuKey(getLinkMenuKey());
			}
		}catch(Exception e){
			//ApexPages.addMessages(e);
			getPageMessages().addErrorMessage(e.getMessage(), e.getStackTraceString());
		}
	}
	
	//IRISのメニューIdを設定してください。
	protected virtual Id getIrisMenuId(){
		return null;
	}
	
	//IRISのメニューキーを設定してください。
	protected virtual String getLinkMenuKey(){
		return null;
	}

	//一覧画面に表示するシステム日時取得メソッド　スプリントバックログ No.10 対応　2017/2/3
	public String getSysDateTime(){
		return Datetime.now().format('yyyy/MM/dd HH:mm');
	}

	/*Page Actionはこちらに実装してください。
			AccessLogに別途出力する内容がある場合はこちらで出力してください。()
				pageAccessLog.recId = xxxxx;
				
	*/
	protected virtual PageReference init(){
		PageReference pr = super.doAuth(null, null);
		if(pr!=null){
			return pr;
		}

		//IRIS選択でない場合は、E_Homeへ遷移（GuestユーザもI_Abstractページの表示OKとする）
		if(!iris.getIsIRIS() && !access.isGuest()){
			pr = Page.E_AppSwitch;
			pr.getParameters().put(I_Const.APPSWITCH_URL_PARAM_APP, I_Const.APP_MODE_NNLINK);		//?app=nnlink
			return pr;
//			getPageMessages().addWarningMessage('IRISモードになっていません！今後はE_Homeにリダイレクトされます。→' + String.valueOf(access.isUseIRIS()) + access.idcpf.appMode__c);
		}

		return null;
	}
//-------------------------------------------------------------------
//			ページアクセスログの取得

	protected E_Log__c pageAccessLog;
	
	//ページアクセスログをController内で出力しない場合　overrideしてTrueにしてください。readOnly=trueの場合など
	protected virtual Boolean preventWritePageAccessLog(){
		return false;
	}
	
	public PageReference PageAction(){
		PageReference pr = null;
		E_PageMessagesHolder msgHolder= getPageMessages();
		
		try{
			if(pageAccessLog == null){
				pageAccessLog = E_LogUtil.createLog(access.idcpf.appMode__c);
				pageAccessLog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_PAGE;		//ActionTypeに『ページ』を設定
			}
			//サブクラスでの初期処理
			if(!msgHolder.getHasErrorMessages()){
				pr = init();		//エラーが発生していない状態のみ、init()を実施
				if(pr!=null){
					msgHolder.addInfoMessage('[リダイレクト] ' + pr.getUrl());
				}
			}	
		}catch(Exception e){
			msgHolder.addErrorMessage(e.getMessage(),e.getStackTraceString());
		}

		//アクセスログ登録
		if(E_LogUtil.requiredLogging()){
			try{
				pageAccessLog.isError__c = msgHolder.getHasErrorMessages();
				List<String> logDetails = new List<String>();		
				if(String.isNotBlank(pageAccessLog.detail__c)){		//既に設定してあるものを設定しておく
					logDetails.add(pageAccessLog.detail__c);
				}
				
				String msg = E_LogUtil.getLogDetailString(msgHolder.getMessages());
				if(String.isNotBlank(msg)){
					logDetails.add(msg);
				}
				if(logDetails.size()>0){
					pageAccessLog.detail__c = String.join(logDetails, '\r\n');
				}
				
				//文字数オーバーの場合TruncateしてUpsert PageAccessLog許可の場合
				if(!preventWritePageAccessLog()){
					Database.DMLOptions dml = new Database.DMLOptions();
					dml.allowFieldTruncation = true;
					pageAccessLog.setOptions(dml);
					upsert pageAccessLog;
				}
			}catch(Exception e){
				msgHolder.addErrorMessage('★★★アクセスログの更新に失敗しました。★★★' + e.getMessage());
			}	
		}
		return pr;
	}
	

}