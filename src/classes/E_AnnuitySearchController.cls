global with sharing class E_AnnuitySearchController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_Policy__c record{get;set;}	
			
	public E_AnnuitySearchExtender getExtender() {return (E_AnnuitySearchExtender)extender;}
	
		public dataTable dataTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component5_val {get;set;}	
		public SkyEditor2.TextHolder Component5_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c NameKana_val {get;set;}	
		public SkyEditor2.TextHolder NameKana_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component57_val {get;set;}	
		public SkyEditor2.TextHolder Component57_op{get;set;}	
			
	public String recordTypeRecordsJSON_E_Policy_c {get; private set;}
	public String defaultRecordTypeId_E_Policy_c {get; private set;}
	public String metadataJSON_E_Policy_c {get; private set;}
	public Boolean QueryPagingConfirmationSetting {get;set;}
	{
	setApiVersion(31.0);
	}
		public E_AnnuitySearchController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.InsuredE_CLTPF_ZCLKNAME__c;
		f = E_Policy__c.fields.InsuredName__c;
		f = E_Policy__c.fields.COMM_CHDRNUM_LINK__c;
		f = E_Policy__c.fields.SPVA_ZANNFROM_yyyyMMdd__c;
		f = E_Policy__c.fields.SPVA_ZANNSTATD__c;
		f = E_Policy__c.fields.COMM_ZCOVRNAM__c;
		f = E_Policy__c.fields.SPVA_ZANNDSCA01__c;
		f = E_Policy__c.fields.RecordTypeId;
		f = E_Policy__c.fields.SpClassification__c;
		f = E_Policy__c.fields.SPVA_ZANNSTAT__c;
		f = E_Policy__c.fields.COMM_OCCDATE__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_Policy__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component5_val = new SkyEditor2__SkyEditorDummy__c();	
				Component5_op = new SkyEditor2.TextHolder('eq');	
					
				NameKana_val = new SkyEditor2__SkyEditorDummy__c();	
				NameKana_op = new SkyEditor2.TextHolder('co');	
					
				Component57_val = new SkyEditor2__SkyEditorDummy__c();	
				Component57_op = new SkyEditor2.TextHolder('co');	
					
				queryMap.put(	
					'dataTable',	
					new SkyEditor2.Query('E_Policy__c')
						.addFieldAsOutput('COMM_CHDRNUM_LINK__c')
						.addFieldAsOutput('SPVA_ZANNFROM_yyyyMMdd__c')
						.addFieldAsOutput('SPVA_ZANNSTATD__c')
						.addFieldAsOutput('InsuredName__c')
						.addFieldAsOutput('COMM_ZCOVRNAM__c')
						.addFieldAsOutput('SPVA_ZANNDSCA01__c')
						.addFieldAsOutput('Id')
						.addFieldAsOutput('COMM_CHDRNUM__c')
						.addFieldAsOutput('ContractorName__c')
						.addFieldAsOutput('ContractorCLTPF_CLNTNUM__c')
						.addFieldAsOutput('SPVA_ZANNFROM__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component5_val, 'SkyEditor2__Text__c', 'COMM_CHDRNUM__c', Component5_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(NameKana_val, 'SkyEditor2__Text__c', 'InsuredE_CLTPF_ZCLKNAME__c', NameKana_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component57_val, 'SkyEditor2__Text__c', 'InsuredName__c', Component57_op, true, 0, true ))
						.addWhere(' ( RecordType.DeveloperName = \'ESPHPF\' AND SpClassification__c = \'ANNUITY\' AND SPVA_ZANNSTAT__c != \'C\')')
.addSort('COMM_OCCDATE__c',True,False).addSort('COMM_CHDRNUM__c',True,False)
				);	
					
					dataTable = new dataTable(new List<E_Policy__c>(), new List<dataTableItem>(), new List<E_Policy__c>(), null, queryMap.get('dataTable'));
				 dataTable.setPageSize(100);
				listItemHolders.put('dataTable', dataTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_Policy__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
					Cookie c = ApexPages.currentPage().getCookies().get('QueryPagingConfirm');
					QueryPagingConfirmationSetting = c != null && c.getValue() == '0';
			extender = new E_AnnuitySearchExtender(this);
			presetSystemParams();
			extender.init();
			dataTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_E_Policy_c_COMM_CHDRNUM_c() { 
			return getOperatorOptions('E_Policy__c', 'COMM_CHDRNUM__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_Policy_c_InsuredE_CLTPF_ZCLKNAME_c() { 
			return getOperatorOptions('E_Policy__c', 'InsuredE_CLTPF_ZCLKNAME__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_Policy_c_InsuredName_c() { 
			return getOperatorOptions('E_Policy__c', 'InsuredName__c');	
		}	
			
			
	global with sharing class dataTableItem extends SkyEditor2.ListItem {
		public E_Policy__c record{get; private set;}
		@TestVisible
		dataTableItem(dataTable holder, E_Policy__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTable extends SkyEditor2.QueryPagingList {
		public List<dataTableItem> items{get; private set;}
		@TestVisible
			dataTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector, SkyEditor2.Query query) {
			super(records, items, deleteRecords, recordTypeSelector, query);
			this.items = (List<dataTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableItem(this, (E_Policy__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

	}

			
	}