@isTest
private class TestI_SalesPlanDataHolder {
	private static Account acc;
	private static E_AgencySalesResults__c asr;
	private static E_SalesPlan__c sp;
	private static Attachment att;
	
	// 未入力
	//@isTest static void I_SalesPlanDataHolder_test01() {
	//	E_SalesPlan__c sp = createData();
	//	I_SalesPlanDataHolder data = new I_SalesPlanDataHolder(sp, false, false);
	//}
	// PDF出力
	@isTest static void I_SalesPlanDataHolder_test02() {
		createData();
		I_SalesPlanDataHolder data = new I_SalesPlanDataHolder(sp, false, false);

		System.assertEquals(sp.SalesResult__r.NBCANP_SpInsTypeLYYTD__c, data.spInsType_ly);
		System.assertEquals(sp.SalesResult__r.NBCANP_SpInsTypeYTD__c, data.spInsType_cy);
		System.assertEquals(sp.SalesResult__r.NBCANP_LYYTD__c, data.anp_ly);
		System.assertEquals(sp.SalesResult__r.NBCANP_YTD__c, data.anp_cy);
		System.assertEquals(sp.SalesResult__r.IQA_LY__c, data.iqa_ly);
		System.assertEquals(sp.SalesResult__r.IQA__c, data.iqa_cy);
		System.assertEquals(sp.SalesResult__r.AgencyName__c, data.agencyName);
	}
	// 新規入力
	@isTest static void I_SalesPlanDataHolder_test03() {
		createData();
		I_SalesPlanDataHolder data = new I_SalesPlanDataHolder(sp, true, false);
		data.getDaysByMonth();
	}

	// テストデータ作成
	private static void createData(){

		acc = new Account(Name = 'テスト代理店格'
						, E_CL1PF_ZHEADAY__c = '10001');
		insert acc;

		asr = new E_AgencySalesResults__c(ParentAccount__c = acc.Id
										, BusinessDate__c = '20000101'
										, Hierarchy__c = 'AH'
										, QualifSim__c = '優績S'
										, NBCANP_SpInsTypeLYYTD__c = 12345678
										, NBCANP_SpInsTypeYTD__c = 12345678
										, NBCANP_LYYTD__c = 87654321
										, NBCANP_YTD__c = 87654321
										, IQA_LY__c = 99.9
										, IQA__c = 99.9
										, AgencyName__c = '代理店名'
										);
		insert asr;

		sp = new E_SalesPlan__c(ParentAccount__c = acc.Id
								, SalesResult__c = asr.Id);
		insert sp;
	}
}