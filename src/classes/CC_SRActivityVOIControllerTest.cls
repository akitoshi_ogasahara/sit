/*
 * CC_SRActivityVOIControllerTest
 * Test class of CC_SRActivityVOIController
 * created  : Accenture 2018/7/9
 * modified :
 */

@isTest
private class CC_SRActivityVOIControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Test invokeMethod()
	* methodName is getInitData
	*/
	static testMethod void invokeMethodTest01() {
		CC_SRActivityVOIController srActivityVOIController = new CC_SRActivityVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'getInitData';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			srActivityVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is getPicklist
	*/
	static testMethod void invokeMethodTest02() {
		CC_SRActivityVOIController srActivityVOIController = new CC_SRActivityVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'getPicklist';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			srActivityVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is saveSRActivity
	*/
	static testMethod void invokeMethodTest03() {
		CC_SRActivityVOIController srActivityVOIController = new CC_SRActivityVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveSRActivity';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			srActivityVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* Exception
	*/
	static testMethod void invokeMethodTest04() {
		CC_SRActivityVOIController srActivityVOIController = new CC_SRActivityVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveSRActivity';
			Map<String, Object> inputMap = null;
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			srActivityVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}