@isTest
private with sharing class TestE_SpvaSearchController{
		private static testMethod void testPageMethods() {	
			E_SpvaSearchController page = new E_SpvaSearchController(new ApexPages.StandardController(new E_Policy__c()));	
			page.getOperatorOptions_E_Policy_c_COMM_CHDRNUM_c();	
			page.getOperatorOptions_E_Policy_c_InsuredE_CLTPF_ZCLKNAME_c();	
			page.getOperatorOptions_E_Policy_c_InsuredName_c();	
			System.assert(true);
		}	
			
	private static testMethod void testdataTable() {
		E_SpvaSearchController.dataTable dataTable = new E_SpvaSearchController.dataTable(new List<E_Policy__c>(), new List<E_SpvaSearchController.dataTableItem>(), new List<E_Policy__c>(), null, null);
		dataTable.create(new E_Policy__c());
		dataTable.doDeleteSelectedItems();
		dataTable.setPagesize(10);		dataTable.doFirst();
		dataTable.doPrevious();
		dataTable.doNext();
		dataTable.doLast();
		dataTable.doSort();
		System.assert(true);
	}
	
}