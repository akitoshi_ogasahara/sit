@isTest
public class TestUtil_I_SearchBox {

	// 代理店レコード
	public Account agency {get; set;}
	// 事務所レコード
	public Account office {get; set;}
	// 募集人レコード
	public Contact agent {get; set;}
	// 募集人ユーザ
	public User aUser {get; set;}
	// 募集人ユーザのID管理
	public E_IDCPF__c aIdcpf {get; set;}

	public TestUtil_I_SearchBox(){}

	// メンバ変数初期化
	private void initialize(){
		agency = null;
		office = null;
		agent = null;
		aUser = null;
		aIdcpf = null;
	}

	/**
	 * 保険契約ヘッダ作成
	 * ※参照項目も含めて作成
	 * @param Boolean isInsert: true:insertする
	 * @param String recTypeDevName:レコードタイプのDeveloperName
	 * @param:Boolean createAgent: true:募集人のパートナーユーザを作成する(AH)
	 */
	public E_Policy__c createStandardPolicy(Boolean isInsert, String recTypeDevName, Boolean createAgent){
		initialize();
		// 契約者、被保険者 作成
		Account dummyAgency = createAgency(true, 'ダミー代理店');
		Contact owner = createContactOwner(true, dummyAgency.Id, 'テスト契約者');
		Contact insured = createContactInsured(true, dummyAgency.Id, 'テスト被保険者');

		// 募集人作成（主のみ）
		agency = createAgency(true, 'テスト代理店');
		office = createoffice(true, agency.Id, 'テスト事務所');
		agent = createContactAgent(true, office.Id, 'テスト募集人');

		// ユーザ作成
		if(createAgent){
			aUser = createAgentUser(true, 'testAgent', 'E_PartnerCommunity', agent.Id);
			aIdcpf = createIDCPF(true, aUser.Id, 'AH');
		}

		// 保険契約ヘッダ作成
		E_Policy__c policy = createEmptyPolicy(false, recTypeDevName);

		policy.Contractor__c = owner.Id;
		policy.Insured__c = insured.Id;
		policy.MainAgent__c = agent.Id;
		policy.MainAgentAccount__c = office.Id;

		policy.COMM_CHDRNUM__c = '12345678';

		if(isInsert){
			insert policy;

			// レコードを共有
			if(createAgent){
				createPolicyShare(policy.Id, aUser.Id);
			}
		}

		return policy;
	}

	/**
	 * 保険契約ヘッダ作成
	 * ※ガワのみ
	 * @param Boolean isInsert: true:insertする
	 * @param String recTypeDevName: レコードタイプDeveloperName
	 */
	public static E_Policy__c createEmptyPolicy(Boolean isInsert, String recTypeDevName){
		if(String.isBlank(recTypeDevName)){
			recTypeDevName = E_Const.POLICY_RECORDTYPE_COLI;
		}

		RecordType recTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = :recTypeDevName LIMIT 1];

		E_Policy__c policy = new E_Policy__c();
		policy.RecordTypeId = recTypeId.Id;

		if(isInsert){
			insert policy;
		}

		return policy;
	}

	/**
	 * （ソート用）保険契約ヘッダを複数作成
	 * 
	 */
	public static List<E_Policy__c> createPolicys(){
		// 代理店、事務所を作成
		Account dAgency = createAgency(true, 'ダミー代理店');
		// 契約者、被保険者を作成
		List<Contact> owners = new List<Contact>();
		List<Contact> insureds = new List<Contact>();
		for(Integer i = 0; i < 3; I++){
			// 契約者
			Contact owner = createContactOwner(false, dAgency.Id, 'テスト契約者' + i);

			owner.E_CLTPF_CLNTNUM__c = '12345' + i;
			owner.E_CLTPF_ZCLKNAME__c = 'テストケイヤクシャ' + (3 - i);
			owner.E_COMM_ZCLADDR__c = '日本橋' + i;

			owners.add(owner);

			// 被保険者
			Contact insured = createContactInsured(false, dAgency.Id, 'テスト被保険者' + i);

			insured.E_CLTPF_CLNTNUM__c = '67890' + i;
			insured.E_CLTPF_ZCLKNAME__c = 'テストケイヒホケンシャ' + (3 - i);
			insured.E_COMM_ZCLADDR__c = '日本橋' + i;
			insured.E_CLTPF_DOB__c = '200' + i + '0101';
			if(Math.mod(i, 2) == 0){
				insured.E_CLTPF_ZKNJSEX__c = '女性';
			} else {
				insured.E_CLTPF_ZKNJSEX__c = '男性';
			}

			insureds.add(insured);
		}

		insert owners;
		insert insureds;

		// 保険契約ヘッダを作成
		List<E_Policy__c> policys = new List<E_Policy__c>();
		for(Integer i = 0; i < 3; i++){
			E_Policy__c policy = createEmptyPolicy(false, null);

			policy.Contractor__c = owners.get(i).Id;
			policy.Insured__c = insureds.get(i).Id;

			policys.add(policy);
		}

		insert policys;

		return policys;
	}

	/**
	 * 保険契約ヘッダを共有
	 */
	public static void createPolicyShare(ID policyId, ID userId){
		E_Policy__share share = new E_policy__share();

		share.parentId = policyId;
		share.UserOrGroupId = userId;
		share.AccessLevel = 'Read';

		insert share;

		return;
	}

// 代理店・事務所・募集人
	/**
	 * 代理店作成
	 * @param Boolean isInsert: true:insertする
	 * @param String agencyName:代理店名
	 */
	public static Account createAgency(Boolean isInsert, String agencyName){
		Account acc = new Account();

		acc.Name = agencyName;

		if(isInsert){
			insert acc;
		}
		return acc;
	}

	/**
	 * 事務所作成
	 * @param Boolean isInsert: true:insertする
	 * @param ID parentId:代理店レコード
	 * @param String officeName:事務所名
	 */
	public static Account createOffice(Boolean isInsert, ID parentId, String officeName){
		Account acc = new Account();

		acc.Name = officeName;
		acc.parentId = parentId;

		if(isInsert){
			insert acc;
		}
		return acc;
	}

	/**
	 * 契約者作成
	 * @param Boolean isInsert: true:insertする」
	 * @param ID agencyId:事務所レコード（ダミー）.ID
	 * @param String ownerName:契約者名
	 */
	public static Contact createContactOwner(Boolean isInsert, ID agencyId, String ownerName){
		Contact con = new Contact();

		con.LastName = ownerName;
		con.AccountId = agencyId;
		con.E_CLTPF_CLNTNUM__c = '12345';
		con.E_CLTPF_FLAG01__c = '1';

		if(isInsert){
			insert con;
		}
		return con;
	}

	/**
	 * 被保険者作成
	 * @param Boolean isInsert: true:insertする」
	 * @param ID agencyId:事務所レコード（ダミー）.ID
	 * @param String insuredName:被保険者名
	 */
	public static Contact createContactInsured(Boolean isInsert, ID agencyId, String insuredName){
		Contact con = new Contact();

		con.LastName = insuredName;
		con.AccountId = agencyId;
		con.E_CLTPF_CLNTNUM__c = '67890';
		con.E_CLTPF_FLAG01__c = '1';
		con.E_CLTPF_FLAG02__c = '1';

		if(isInsert){
			insert con;
		}
		return con;
	}

	/**
	 * 募集人作成
	 * @param Boolean isInsert: true:insertする」
	 * @param ID officeId:事務所レコード
	 * @param String agentName:募集人名
	 */
	public static Contact createContactAgent(Boolean isInsert, ID officeId, String agentName){
		Contact con = new Contact();

		con.LastName = agentName;
		con.AccountId = officeId;
		con.E_CL3PF_AGNTNUM__c = 'A1234';

		if(isInsert){
			insert con;
		}
		return con;
	}

// ユーザ
    /**
     * Profileマップ取得
     */
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }

    /**
     * 代理店ユーザ作成
     * @param Boolean isInsert: true:insertする
     * @param String LastName: 姓
     * @param String profileDevName: プロファイルAPI名
     * @param ID ConId： 募集人ID
     */
    public static User createAgentUser(Boolean isInsert, String LastName, String profileDevName, ID ConId){
        String userName = LastName + '@testuser.iris';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , ContactId = ConId
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }

    /**
     * ID管理作成
 	 * @param Boolean isInsert: true:insertする
	 * @param ID userId:ユーザ.Id
	 * @param String permission:ユーザ権限（AH,AY,AT）
    */
    public static E_IDCPF__c createIDCPF(Boolean isInsert, Id userId, String permission){
        E_IDCPF__c src = new E_IDCPF__c(
             User__c = userId
            ,OwnerId = userId
            ,ZIDTYPE__c = permission
            ,ZSTATUS01__c = '1'
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }

    /**
     * ベース権限セット付与
     * @param ID usrId: ユーザID
     */
    public static void createBasePermissions(ID usrId) {
        Id permAgnyId = [Select Id FROM PermissionSet Where Name = :E_Const.PERM_BASE].Id;
        PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
        insert src;
    }
}