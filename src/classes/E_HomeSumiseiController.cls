public with sharing class E_HomeSumiseiController extends E_HomeController{

	public override PageReference init(){
		// 住生ユーザーでない、かつ 社員でない場合、E_Home へ遷移
		if(!access.isSumiseiUser() && !access.isEmployee()){
			return Page.E_Home;
		}

		pageRef = doAuth(E_Const.ID_KIND.INFO, userinfo.getUserId());
		return E_Util.toErrorPage(pageRef, null);
	}

	//住生対応 遷移先URLのフィールド。権限ごとにを振り分ける。
		//2017.02.13 oidとdidは振り分け先にて設定されているためパラメータから削除
	//---新契約---
	//新契約成立状況
	public String destNewContractStatus{get{
											String eidc = access.idcpf.ZIDTYPE__c;
											//社員
											if(E_Const.ZIDTYPE_EP.equals(eidc)){
												return 'E_DownloadPrintSheetsSelect';
											//AH
											} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
												return 'E_DownloadNewPolicy';
											//AY
											} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
												return  'E_DownloadNewPolicy';
											} else{
												  return null;
											}
										}set;}
	//嘱託医検索機能
	public String destDoctorSearch{get{ return 'E_MDTop';
										}set;}

	//---保全---
	//保有契約変更通知
	public String destContractChangeNotification{get{
													String eidc = access.idcpf.ZIDTYPE__c;
													//社員
													if(E_Const.ZIDTYPE_EP.equals(eidc)){
														return 'E_DownloadPrintSheetsSelect';
													//AH
													} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
														return 'E_DownloadNoticeAGPO';	//?did=' + access.User.Contact.account.parentId;
													//AY
													} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
														return 'E_DownloadNoticeAGPO';	//?oid='+ access.User.accountId;
													} else{
														return null;
													}
												}set;}

	//保険料未入金通知
	public String destNotAcceptedNotification{get{
												String eidc = access.idcpf.ZIDTYPE__c;
												//社員
												if(E_Const.ZIDTYPE_EP.equals(eidc)){
													return 'E_DownloadPrintSheetsSelect';
												//AH
												} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
													return 'E_DownloadNoticeNCOL';	//?did=' + access.User.Contact.account.parentId;
												//AY
												} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
													return 'E_DownloadNoticeNCOL';	//?oid='+ access.User.accountId;
												} else{
													return null;
												}
											}set;}

	//保険料請求予告通知
	public String destToBeChargedNotification{get{
												String eidc = access.idcpf.ZIDTYPE__c;
												//社員
												if(E_Const.ZIDTYPE_EP.equals(eidc)){
													return 'E_DownloadPrintSheetsSelect';
												//AH
												} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
													return 'E_DownloadNoticeBILA';	//?did=' + access.User.Contact.account.parentId;
												//AY
												} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
													return 'E_DownloadNoticeBILA';	//?oid='+ access.User.accountId;
												} else{
													return null;
												}
											}set;}

	//保険料請求通知
	public String destChargeNotification{get{
											String eidc = access.idcpf.ZIDTYPE__c;
											//社員
											if(E_Const.ZIDTYPE_EP.equals(eidc)){
												return 'E_DownloadPrintSheetsSelect';
											//AH
											} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
												return 'E_DownloadNoticeBILS';	//?did=' + access.User.Contact.account.parentId;
											//AY
											} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
												return 'E_DownloadNoticeBILS';	//?oid='+ access.User.accountId;
											} else{
												return null;
											}
										}set;}

	//保全帳票
	public String destMaintenanceReport{get{ return 'E_Info?menu=except_spva';
										}set;}

	//---保有契約状況---
	//保有契約一覧
	public String destContractList{get{
										String eidc = access.idcpf.ZIDTYPE__c;
										//社員
										if(E_Const.ZIDTYPE_EP.equals(eidc)){
											return 'E_DownloadPrintSheetsSelect';
										//AH
										} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
											return 'E_DownloadHoldPolicySearch?pt=coli';	//&did=' + access.User.Contact.account.parentId;
										//AY
										} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
											return 'E_DownloadHoldPolicySearch?pt=coli';	//&oid='+ access.User.accountId;
										} else{
											return null;
										}
									}set;}

	//お客様検索・契約内容照会
	public String destCustomerSearch{get{ return 'E_CustomerSearch';
									}set;}

	//手続き履歴
	public String destProcessHistory{get{
										String eidc = access.idcpf.ZIDTYPE__c;
										//社員
										if(E_Const.ZIDTYPE_EP.equals(eidc)){
											return 'E_DownloadPrintSheetsSelect';
										//AH
										} else if(E_Const.ZIDTYPE_AH.equals(eidc)){
											return 'E_DownloadPolicyHistorySearch';	//?did=' + access.User.Contact.account.parentId;
										//AY
										} else if(E_Const.ZIDTYPE_AY.equals(eidc)){
											return 'E_DownloadPolicyHistorySearch';	//?oid='+ access.User.accountId;
										} else{
											return null;
										}
									}set;}
}