/**
@version 1.0
@author PwC
リード所有者更新時割り当て通知
*/

public without sharing class LMT_LeadUpdateTriggerHandler {
    public Map<Id,User> oldOwnerMap;
    public Map<Id,User> newOwnerMap;
    public Map<Id, List<Lead>> emailToUserMap;
    public Set<Id> oldOwnerIdSet;
    public String kakucode = '';
    public Id hQGroupId = null;
    public String hqGroupName = '';
    public Map<String, List<Id>> hqMemberMap;
    public User loginUser;
    public LMT_LeadUpdateTriggerHandler(){
    }
    public LMT_LeadUpdateTriggerHandler(List<Lead> oldList ,List<Lead> newList){
        emailToUserMap = new Map<Id, List<Lead>>();
        oldOwnerIdSet = New Set<Id>();
        hqMemberMap = new Map<String, List<Id>>();
        for(Lead lead:oldList){
            oldOwnerIdSet.add(lead.OwnerId);
        }

        loginUser = [SELECT Id,
                            IsPrmSuperUser,
                            UserRoleId
                        FROM User
                        WHERE Id =:UserInfo.getUserId()];

        oldOwnerMap = new Map<Id,User>([SELECT Id,
                                            UserType,
                                            ContactId,
                                            IsPrmSuperUser,
                                            UserRoleId
                                    FROM User
                                    WHERE Id IN:oldOwnerIdSet]);

        Set<Id> newOwnerIdSet = New Set<Id>();
        for(Lead lead:newList){

            newOwnerIdSet.add(lead.OwnerId);
        }

        newOwnerMap = new Map<Id,User>([SELECT Id,
                                            UserType,
                                            ContactId,
                                            IsPrmSuperUser,
                                            Contact.Account.E_ParentZHEADAY__c,
                                            Contact.Account.OwnerId,
                                            UserRoleId
                                    FROM User
                                    WHERE Id IN:newOwnerIdSet]);

        //募集人が所属する代理店格コードの抽出
        if(newOwnerMap != null){
            for(User newowner :  newOwnerMap.values()){
                kakucode = newowner.Contact.Account.E_ParentZHEADAY__c;
            }
        }

        //代理店本部公開グループの抽出
        if(kakucode != null){
            String soql = 'SELECT Id, DeveloperName FROM Group  WHERE DeveloperName LIKE ' + '\'LMT_'+kakucode + '\'';

            for(Group groupList:Database.query(soql)){
                hQGroupId = groupList.Id;
            }
        }

        //割り当て通知を募集人、募集人アドレスが未登録の場合は本部メンバー、エヌエヌ生命保険メールアドレスへ通知を送信
        List<Id> gmemList;
        for(GroupMember groupMemList :[SELECT Id,
                                                UserOrGroupId,
                                                GroupId,
                                                Group.DeveloperName
                                        FROM GroupMember
                                        WHERE GroupId =:hQGroupId]){

            //割り当てられた募集人が所属する本部メンバー
            if(hqMemberMap.get(groupMemList.Group.DeveloperName) != null){
                if(string.valueOf(groupMemList.UserOrGroupId).substring(0,3).equals('005')){
                    hqMemberMap.get(groupMemList.Group.DeveloperName).add(groupMemList.UserOrGroupId);
                }
            }else{
                if(string.valueOf(groupMemList.UserOrGroupId).substring(0,3).equals('005')){
                    gmemList = new List<Id>();
                    gmemList.add(groupMemList.UserOrGroupId);
                    hqMemberMap.put(groupMemList.Group.DeveloperName, gmemList);
                }
            }
        }
    }

    /**
        ログインユーザの割当先整合性チェック
        内部ユーザは制限なし、代理店ユーザは募集人ユーザは割当制限、その他はキューへの割当を制限する
        @param oldList
        @param newList
        @return なし
    */

    public void checkLeadAssignAuthority(List<Lead> oldList ,List<Lead> newList){
        Boolean stopFlg = false;
        for(Lead oldRecord : oldList){
            if(UserInfo.getUserType().equals('PowerPartner')){
                for(Lead newRecord : newList){
                    if((oldRecord.Id ==newRecord.Id)
                        && (oldRecord.OwnerId != newRecord.OwnerId)){
                        if(string.valueOf(oldRecord.OwnerId).startsWith('005')
                            && oldOwnerMap.get(oldRecord.OwnerId).UserType.equals('PowerPartner')
                            && string.valueOf(newRecord.OwnerId).startsWith('005')
                            && newOwnerMap.get(newRecord.OwnerId).UserType.equals('PowerPartner')){
                            //ログインユーザがスーパーユーザ以外
                            if((loginUser.IsPrmSuperUser == null | loginUser.IsPrmSuperUser == false)){
                            	//本部から募集人直接割当は不可
                            	if(hqMemberMap.get('LMT_' + kakucode)!= null){
                                    for(Id hquser: hqMemberMap.get('LMT_' + kakucode)){
                                        if(hquser == loginUser.Id){
                                            stopFlg = true;
                                            if(loginUser.UserRoleId != newOwnerMap.get(newRecord.OwnerId).UserRoleId
			                            	&& (newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser == false)){
			                            		newRecord.addError(System.label.Msg_Error_LMT_LeadAssignmentAuthority);
			                            	}
                                        }
                                    }
                                }
                            	if(stopFlg == false){
                                    //支店長割当時
                                    if(newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser  != null && newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser){
                                        newRecord.addError(System.label.Msg_Error_LMT_LeadAssignmentAuthority);
                                    //他募集人割当時
                                    }else if(oldOwnerMap.get(oldRecord.OwnerId).UserRoleId == newOwnerMap.get(newRecord.OwnerId).UserRoleId){
                                        newRecord.addError(System.label.Msg_Error_LMT_LeadAssignmentAuthority);
                                    }
                                }
                            }
                        //代理店ユーザが操作時はキューへの移管は不可
                        }else if( string.valueOf(oldRecord.OwnerId).startsWith('005')
                            && oldOwnerMap.get(oldRecord.OwnerId).UserType.equals('PowerPartner')
                            && string.valueOf(newRecord.OwnerId).startsWith('00G')){
                                newRecord.addError(System.label.Msg_Error_LMT_LeadAssignmentAuthority);
                            }
                    }
                }
            }

        }
    }
    /**
        割当状況を更新する
        @param oldList
        @param newList
        @return 更新用レコード
        @Note 業務上、本部から募集人直接割当が存在しないため、本部→募集人割当時のロジックは未記載
    */

    public List<Lead> setAssignmentStatus(List<Lead> oldList ,List<Lead> newList){

        for(Lead oldRecord : oldList){
            for(Lead newRecord : newList){
                if((oldRecord.Id ==newRecord.Id)
                    && (oldRecord.OwnerId != newRecord.OwnerId)){

                    if(string.valueOf(oldRecord.OwnerId).startsWith('005')
                        && oldOwnerMap.get(oldRecord.OwnerId).UserType.equals('PowerPartner')
                        && string.valueOf(newRecord.OwnerId).startsWith('005')
                        && newOwnerMap.get(newRecord.OwnerId).UserType.equals('PowerPartner')){

                        //割当元がスーパーユーザ（支店長）なら「募集人割当済み」
                        if(oldOwnerMap.get(oldRecord.OwnerId).IsPrmSuperUser != null
                        && oldOwnerMap.get(oldRecord.OwnerId).IsPrmSuperUser){
                            if(oldOwnerMap.get(oldRecord.OwnerId).UserRoleId == newOwnerMap.get(newRecord.OwnerId).UserRoleId){
                                newRecord.AssignmentStatus__c = '03';
                            }else{
                                //本部ユーザが割当先なら「本部割当済み」
                                for(Id hquser: hqMemberMap.get('LMT_' + kakucode)){
                                    if(hquser == newRecord.OwnerId){
                                        newRecord.AssignmentStatus__c = '01';
                                        break;
                                    }
                                }
                            }
                        //割当元がスーパーユーザ（支店長）以外
                        }else{
                        	//割当先がスーパーユーザ（支店長）なら「支店・支社割当済み」
                        	if(newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser  != null && newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser){
	                            newRecord.AssignmentStatus__c = '02';
                        	}else if(newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser  == null | newOwnerMap.get(newRecord.OwnerId).IsPrmSuperUser == false
                        	&& oldOwnerMap.get(oldRecord.OwnerId).UserRoleId != newOwnerMap.get(newRecord.OwnerId).UserRoleId){
                        		newRecord.AssignmentStatus__c = '01';
                        	}
                        }
                    }else if( string.valueOf(oldRecord.OwnerId).startsWith('005')
                        && oldOwnerMap.get(oldRecord.OwnerId).UserType.equals('PowerPartner')
                        && string.valueOf(newRecord.OwnerId).startsWith('00G')){
                            newRecord.AssignmentStatus__c = '00';
                        }
                }
            }
        }
        return newList;
    }

    /**
        見込顧客を割り当てられた、拠点長または募集人に割り当て通知を送信する
        @param oldList
        @param newList
        @return なし
    */
    public void sendNotificationMail (List<Lead> oldList ,List<Lead> newList){

        system.debug('=====START=======');
        //代理店メール送信先を用意
        Map<Id,LMT_EmailAddressManagement__c> mailAddressMap = new Map<Id,LMT_EmailAddressManagement__c>();
        for(LMT_EmailAddressManagement__c mailAddressList : [SELECT Id,
                                                                        User1__c,
                                                                        User1__r.Email,
                                                                        User2__c,
                                                                        User2__r.Email,
                                                                        User3__c,
                                                                        User3__r.Email,
                                                                        User4__c,
                                                                        User4__r.Email,
                                                                        User5__c,
                                                                        User5__r.Email,
                                                                        User6__c,
                                                                        User6__r.Email
                                                                FROM LMT_EmailAddressManagement__c]){

            mailAddressMap.put(mailAddressList.User1__c, mailAddressList);
        }

        //送信元アドレスを用意
        OrgWideEmailAddress sender = [SELECT Id,
                                Address
                                FROM  OrgWideEmailAddress
                                WHERE DisplayName = 'エヌエヌ生命保険'];

        //割り当て用メールテンプレートを用意
        EmailTemplate assignEt = new EmailTemplate();
        EmailTemplate alertEt = new EmailTemplate();

        for(EmailTemplate et:[SELECT Id,
                                DeveloperName,
                                Body,
                                Subject
                             FROM EmailTemplate
                             WHERE DeveloperName = 'LMT_Lead_Assignment_Notification'
                             OR DeveloperName = 'LMT_Alert_Lead_Assignment']){
            if(et.DeveloperName.equals('LMT_Lead_Assignment_Notification')){
                assignEt = et;
            }else if(et.DeveloperName.equals('LMT_Alert_Lead_Assignment')){
                alertEt = et;
            }
         }

        //メール送信対代理店ユーザ（支店長または募集人）
        List<Lead> targetLeads;
        for(Lead oldRecord : oldList){
            for(Lead newRecord : newList){
                if((oldRecord.Id ==newRecord.Id)
                    && (oldRecord.OwnerId != newRecord.OwnerId)){
                        if(newRecord.AssignmentStatus__c != null){
                            if(newRecord.AssignmentStatus__c.equals('02') || newRecord.AssignmentStatus__c.equals('03')){
                                if(emailToUserMap.get(newRecord.OwnerId) != null){
                                    emailToUserMap.get(newRecord.OwnerId).add(newRecord);
                                }else{
                                    targetLeads = new List<Lead>();
                                    targetLeads.add(newRecord);
                                    emailToUserMap.put(newRecord.OwnerId, targetLeads);
                                }
                            }
                        }
                    }
            }
        }

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(Id toUser: emailToUserMap.keySet()){

            Messaging.SingleEmailMessage message01 = new Messaging.SingleEmailMessage();
            if(mailAddressMap.get(toUser) != null){
                message01.setBccSender(false);
                message01.setOrgWideEmailAddressId(sender.Id);
                message01.setTemplateId(assignEt.Id);
                message01.setTargetObjectId(toUser);
                message01.setSaveAsActivity(false);
                message01.setWhatId(emailToUserMap.get(toUser)[0].Id);
                messages.add(message01);

                Messaging.SingleEmailMessage message02 = new Messaging.SingleEmailMessage();
                if(mailAddressMap.get(toUser).User2__c != null){

                    message02.setBccSender(false);
                    message02.setOrgWideEmailAddressId(sender.Id);
                    message02.setTemplateId(assignEt.Id);
                    message02.setTargetObjectId(mailAddressMap.get(toUser).User2__c);
                    message02.setSaveAsActivity(false);
                    message02.setWhatId(emailToUserMap.get(toUser)[0].Id);
                    messages.add(message02);

                }
                Messaging.SingleEmailMessage message03 = new Messaging.SingleEmailMessage();
                if(mailAddressMap.get(toUser).User3__c != null){

                    message03.setBccSender(false);
                    message03.setOrgWideEmailAddressId(sender.Id);
                    message03.setTemplateId(assignEt.Id);
                    message03.setTargetObjectId(mailAddressMap.get(toUser).User3__c);
                    message03.setSaveAsActivity(false);
                    message03.setWhatId(emailToUserMap.get(toUser)[0].Id);
                    messages.add(message03);

                }
                Messaging.SingleEmailMessage message04 = new Messaging.SingleEmailMessage();
                if(mailAddressMap.get(toUser).User4__c != null){

                    message04.setBccSender(false);
                    message04.setOrgWideEmailAddressId(sender.Id);
                    message04.setTemplateId(assignEt.Id);
                    message04.setTargetObjectId(mailAddressMap.get(toUser).User4__c);
                    message04.setSaveAsActivity(false);
                    message04.setWhatId(emailToUserMap.get(toUser)[0].Id);
                    messages.add(message04);

                }
                Messaging.SingleEmailMessage message05 = new Messaging.SingleEmailMessage();
                if(mailAddressMap.get(toUser).User5__c != null){

                    message05.setBccSender(false);
                    message05.setOrgWideEmailAddressId(sender.Id);
                    message05.setTemplateId(assignEt.Id);
                    message05.setTargetObjectId(mailAddressMap.get(toUser).User5__c);
                    message05.setSaveAsActivity(false);
                    message05.setWhatId(emailToUserMap.get(toUser)[0].Id);
                    messages.add(message05);

                }
                Messaging.SingleEmailMessage message06 = new Messaging.SingleEmailMessage();
                if(mailAddressMap.get(toUser).User6__c != null){

                    message06.setBccSender(false);
                    message06.setOrgWideEmailAddressId(sender.Id);
                    message06.setTemplateId(assignEt.Id);
                    message06.setTargetObjectId(mailAddressMap.get(toUser).User6__c);
                    message06.setSaveAsActivity(false);
                    message06.setWhatId(emailToUserMap.get(toUser)[0].Id);
                    messages.add(message06);

                }
                Messaging.SingleEmailMessage message07 = new Messaging.SingleEmailMessage();
                //割当先が募集人の場合は担当MR
                if(emailToUserMap.get(toUser)[0].AssignmentStatus__c.equals('03')){

                        message07.setBccSender(false);
                        message07.setOrgWideEmailAddressId(sender.Id);
                        message07.setTemplateId(assignEt.Id);
                        message07.setTargetObjectId(newOwnerMap.get(toUser).Contact.Account.OwnerId);
                        message07.setSaveAsActivity(false);
                        message07.setWhatId(emailToUserMap.get(toUser)[0].Id);
                        messages.add(message07);

                    }
            }else{
                //LMTメール通知管理に未登録の場合はエヌエヌ生命保険アドレス、本部メンバーへ通知
                //エヌエヌ生命保険メールアドレス
                Messaging.SingleEmailMessage message08 = new Messaging.SingleEmailMessage();
                message08.setToAddresses(new String[]{sender.Address});
                message08.setBccSender(false);
                message08.setOrgWideEmailAddressId(sender.Id);
                message08.setTemplateId(alertEt.Id);
                message08.setSubject(replaceBindStrs(alertEt.subject,emailToUserMap.get(toUser)[0]));
                message08.setPlainTextBody(replaceBindStrs(alertEt.Body,emailToUserMap.get(toUser)[0]));
                message08.setSaveAsActivity(false);
                messages.add(message08);

                //代理店本部メンバー
                if(hqMemberMap != null){
                    for(Id hquser: hqMemberMap.get('LMT_' + kakucode)){
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.setTargetObjectId(hquser);
                        message.setBccSender(false);
                        message.setOrgWideEmailAddressId(sender.Id);
                        message.setTemplateId(alertEt.Id);
                        message.setSubject(replaceBindStrs(alertEt.subject,emailToUserMap.get(toUser)[0]));
                        message.setPlainTextBody(replaceBindStrs(alertEt.Body,emailToUserMap.get(toUser)[0]));
                        message.setSaveAsActivity(false);
                        messages.add(message);
                    }
                }

            }
        }
        Messaging.sendEmail(messages);
        system.debug('=====END=======');
    }

    /**
        メールテンプレートの差し込み項目に値を置換してセットする
        @param targetStr
        @param sobj
        @return テンプレートにセットする置換後の文字列
    */
    private String replaceBindStrs(String targetStr,SObject sobj){
        //置換処理
        String regex = '\\{!.+?\\}';
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(targetStr);

        while (m.find()) {
            String tempStr = m.group();
            tempStr = tempStr.replace('}','');
            String fieldNameStr = tempStr.split('\\.')[1];
            //存在しない項目名が指定された場合無視
            String afterStr;
            if(sobj.get(fieldNameStr) == null ) {
                afterStr = '';
            } else {
                afterStr = String.valueOf(sobj.get(fieldNameStr));
            }
            targetStr = targetStr.replace(m.group(),afterStr);
        }
        return targetStr;
    }

}