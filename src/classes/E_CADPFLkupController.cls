global with sharing class E_CADPFLkupController extends SkyEditor2.SkyEditorPageBaseWithSharing{
    
    public E_CADPF__c record{get;set;}
    public E_CADPFLkupExtender getExtender() {return (E_CADPFLkupExtender)extender;}
    public resultTable resultTable {get; private set;}
    public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
    public SkyEditor2__SkyEditorDummy__c ZPOSTCD{get;set;}
    public SkyEditor2__SkyEditorDummy__c ZPOSTMARK{get;set;}
    public E_CADPFLkupController(ApexPages.StandardController controller){
        super(controller);

        SObjectField f;
        f = E_CADPF__c.fields.ZPOSTCD__c;
        f = E_CADPF__c.fields.ZPOSTMARK__c;
        f = E_CADPF__c.fields.Name;
        f = E_CADPF__c.fields.StateCity__c;
        f = E_CADPF__c.fields.Street__c;

        try {
            mainRecord = null;
            mainSObjectType = E_CADPF__c.SObjectType;
            mode = SkyEditor2.LayoutMode.TempProductLookup_01;
            
            ZPOSTCD = new SkyEditor2__SkyEditorDummy__c();
            ZPOSTMARK = new SkyEditor2__SkyEditorDummy__c();
            
            queryMap.put(
                'resultTable',
                new SkyEditor2.Query('E_CADPF__c')
                    .addFieldAsOutput('Name')
                    .addFieldAsOutput('StateCity__c')
                    .addFieldAsOutput('Street__c')
                    .addField('Name')
                    .limitRecords(500)
                    .addListener(new SkyEditor2.QueryWhereRegister(ZPOSTCD, 'SkyEditor2__Text__c', 'ZPOSTCD__c', new SkyEditor2.TextHolder('eq'), false, true, false))
                    .addListener(new SkyEditor2.QueryWhereRegister(ZPOSTMARK, 'SkyEditor2__Text__c', 'ZPOSTMARK__c', new SkyEditor2.TextHolder('co'), false, true, false))
.addSort('Name',True,True)
            );
            
            resultTable = new resultTable(new List<E_CADPF__c>(), new List<resultTableItem>(), new List<E_CADPF__c>(), null);
            listItemHolders.put('resultTable', resultTable);
            
            recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_CADPF__c.SObjectType);
            
            p_showHeader = false;
            p_sidebar = false;
            presetSystemParams();
            extender = new E_CADPFLkupExtender(this);
            initSearch();
            
            extender.init();
            
        } catch (SkyEditor2.Errors.SObjectNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.Errors.FieldNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
        } catch (SkyEditor2.ExtenderException e){
            e.setMessagesToPage();
        } catch (SkyEditor2.Errors.PricebookNotFoundException e) {
            SkyEditor2.Messages.addErrorMessage(e.getMessage());
            hidePageBody = true;
        }
    }

    public List<SelectOption> getOperatorOptions_E_CADPF_c_ZPOSTCD_c() {
        return getOperatorOptions('E_CADPF__c', 'ZPOSTCD__c');
    }
    public List<SelectOption> getOperatorOptions_E_CADPF_c_ZPOSTMARK_c() {
        return getOperatorOptions('E_CADPF__c', 'ZPOSTMARK__c');
    }
    
    global with sharing class resultTableItem extends SkyEditor2.ListItem {
        public E_CADPF__c record{get; private set;}
        @TestVisible
        resultTableItem(resultTable holder, E_CADPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
        public List<resultTableItem> items{get; private set;}
        @TestVisible
        resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<resultTableItem>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new resultTableItem(this, (E_CADPF__c)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    } 
}