@isTest
private class TestCR_AgencySearchBizConfExtender{
    
    static testMethod void test_ExportCSV01() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizDpaPermissions(usr.Id);
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBizConf;
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			CR_AgencySearchBizConfSVEController controller = new CR_AgencySearchBizConfSVEController(stdController);
			CR_AgencySearchBizConfExtender extender = new CR_AgencySearchBizConfExtender(controller);
			PageReference resultPref = extender.ExportCSV();
			
			Test.stopTest();
			
			PageReference targetPref = Page.E_CRExportAgencyBizConf;
			String soql = controller.queryMap.get('resultTable').toSoql();
			String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
			sWhere = E_SoqlUtil.trimLimitClause(sWhere);
			
			String resultParam = resultPref.getParameters().get(E_CSVExportController.SOQL_WHERE);
			
			// 実行結果確認
			System.assertEquals(sWhere, E_EncryptUtil.getDecryptedString(resultParam));
		}
    }
    
/*
	public pageReference changeConfig(){
		Integer row = Integer.valueOf(ApexPages.currentPage().getParameters().get('row'));
		CR_Agency__c  rec = (CR_Agency__c)controller.resultTable.records[row];
		rec.LargeAgency__c = !rec.LargeAgency__c;
		
		update rec;
		//権限セット付与
		grantPermissionSet(rec.AgencyCode__c);
	
		return null;
	}
*/
	
	static testMethod void test_doSearch() {

    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizDpaPermissions(usr.Id);
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBizConf;
			Test.setCurrentPage(pref);
			
			// データ作成
			List<Account> accountList = TestCR_TestUtil.createAccountList(true, 3);
			Set<Id> ids = new Set<Id>();
			for(Account acc : accountList){
				ids.add(acc.Id);
			}
			List<CR_Agency__c> agnyList = TestCR_TestUtil.createCRAgencyList(true, ids, CR_Const.RECTYPE_DEVNAME_BIZ);

			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			CR_AgencySearchBizConfSVEController controller = new CR_AgencySearchBizConfSVEController(stdController);
			CR_AgencySearchBizConfExtender extender = new CR_AgencySearchBizConfExtender(controller);
			
			// 検索実行
			controller.doSearch();
			system.debug(controller.resultTable);

			Test.stopTest();

			// 実行結果確認
			System.assertEquals(3,controller.resultTable.items.size());
		}		
		
	}

	static testMethod void test_changeConfig() {

    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizDpaPermissions(usr.Id);
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBizConf;
			Test.setCurrentPage(pref);
			
			// データ作成
			List<Account> accountList = TestCR_TestUtil.createAccountList(true, 3);
			Set<Id> ids = new Set<Id>();
			for(Account acc : accountList){
				ids.add(acc.Id);
			}
			List<CR_Agency__c> agnyList = TestCR_TestUtil.createCRAgencyList(true, ids, CR_Const.RECTYPE_DEVNAME_BIZ);
			System.assertEquals(3, agnyList.size());

			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			CR_AgencySearchBizConfSVEController controller = new CR_AgencySearchBizConfSVEController(stdController);
			CR_AgencySearchBizConfExtender extender = new CR_AgencySearchBizConfExtender(controller);
			
			// 検索実行
			controller.doSearch();
			system.debug(controller.resultTable);

			//大規模代理店が1件取得不可
			List<CR_Agency__c> largeAgencys = [select id,name from CR_Agency__c where LargeAgency__c = true];
			System.assertEquals(0, largeAgencys.size());

			ApexPages.currentPage().getParameters().put('row','1');
			extender.changeConfig();								//大規模代理店へ変更

			Test.stopTest();

			//大規模代理店が1件取得できる
			largeAgencys = [select id,name from CR_Agency__c where LargeAgency__c = true];
			System.assertEquals(1, largeAgencys.size());
			
		}		
	}
}