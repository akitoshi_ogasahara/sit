@isTest
private class TestI_MenuMasterDao {
	private static I_MenuMaster__c menu;
	
	@isTest static void getRecordById_test001() {
		createData('getRecordById');
		Test.startTest();
		I_MenuMaster__c rec = I_MenuMasterDao.getRecordById(menu.Id);
		System.assertEquals(menu.Id, rec.Id);
		Test.stopTest();
	}
	
	@isTest static void getSortedRecords_test001() {
		createData('getSortedRecords');
		Test.startTest();
		List<I_MenuMaster__c> recList = I_MenuMasterDao.getSortedRecords();
		System.assertEquals(menu.Id, recList[1].Id);
		Test.stopTest();
	}

	@isTest static void getRecordsByLinkMenuKey_test001() {
		createData('getRecordsByLinkMenuKey');
		Test.startTest();
		List<I_MenuMaster__c> recList = I_MenuMasterDao.getRecordsByLinkMenuKey(menu.MenuKey__c);
		System.assertEquals(menu.Id, recList[0].Id);
		Test.stopTest();
	}
	
	@isTest static void getRecByName_test001() {
		createData('getRecByName');
		Test.startTest();
		I_MenuMaster__c rec = I_MenuMasterDao.getRecByName('getRecByName');
		System.assertEquals(menu.Id, rec.Id);
		Test.stopTest();
	}

	@isTest static void getRecByNameAndSubCategory_test001() {
		createData('getRecByName');
		Test.startTest();
		I_MenuMaster__c rec = I_MenuMasterDao.getRecByNameAndSubCategory('getRecByName','');
		System.assertEquals(menu.Id, rec.Id);
		Test.stopTest();
	}

	private static void createData(String name){
		
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.default__c = false;
		insert pm;

		I_MenuMaster__c mn = new I_MenuMaster__c();
		mn.Name = 'parent';
		insert mn;

		menu = new I_MenuMaster__c();
		menu.Name = name;
		menu.MenuKey__c = 'ownerList';
		menu.MainCategory__c = '2.保有契約';
		menu.SubCategory__c = '';
		menu.ParentMenu__c = mn.Id;
		menu.Style__c = '';
		menu.LinkURL__c = '';
		menu.IRISPageName__c = pm.Name;
		menu.UrlParameters__c = '';
		menu.WindowTarget__c = '';
		menu.Default__c = true;
		menu.Required_ZIDTYPE__c = '';
		menu.RequiredPermissionSet__c = '';
		menu.Required_EIDC_Flag__c = '';
		menu.ShowOnTop__c = false;
		menu.CanFiltered__c = false;
		//menu.ParentMenu__r.Name = mn.Name;
		menu.UpsertKey__c = name;
		insert menu;
	}
}