public with sharing class SC_ExportSearchController extends E_CSVExportController{

	public String today {get; private set;}

	public override String mainSObjectAPIName(){
		return Schema.SObjectType.SC_Office__c.Name;
	}

	/**
	 *	Constructor
	 */
	public SC_ExportSearchController(){
		super();

		// 当日日付の取得
		today = System.now().format('データ作成基準日:yyyy年MM月dd日');
		
		//項目の追加
		addColumn(new E_CSVColumnDescribe.BaseField('FiscalYear__c'));	// 年度
		addColumn(new E_CSVColumnDescribe.BaseField('ARACDE__c'));	// エリア
		addColumn(new E_CSVColumnDescribe.DecimalField('No__c'));	// NO
		addColumn(new E_CSVColumnDescribe.BaseField('AGNTBR__c'));	// 拠点コード
		addColumn(new E_CSVColumnDescribe.BaseField('MRUnitName__c'));	// 営業部名
		addColumn(new E_CSVColumnDescribe.BaseField('AccBRANCH__c', '拠点コード（最新）'));	// （事務所）支社コード　※最新　名称[拠点コード（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('OwnerMRUnitName__c', '営業部名（最新）'));	// （所有者）営業部名　※最新　名称[営業部名（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('TaxAccoutantTarget__c'));	// 税理士法人関連臨店対象
		addColumn(new E_CSVColumnDescribe.BaseField('PQ2ToCQ1Concern__c'));	// 懸念事案（前年Q2～当年Q1）
		addColumn(new E_CSVColumnDescribe.BaseField('PreviousYearConcern__c'));	// 過去1年懸念事案
		addColumn(new E_CSVColumnDescribe.BaseField('PoliciesWritten__c'));	// 過去12ヶ月挙績
		addColumn(new E_CSVColumnDescribe.BaseField('SelfComplianceOmit__c'));	// 代理店自主点検省略先
		/* 2018年度対応
		addColumn(new E_CSVColumnDescribe.BaseField('CMDAnnual__c'));	// CMD毎年臨店点検先
		addColumn(new E_CSVColumnDescribe.BaseField('CMDBiannualCurrentYear__c'));	// CMD隔年臨店（当年先）
		addColumn(new E_CSVColumnDescribe.BaseField('CMDBiannualOneYear__c'));	// CMD隔年臨店（当年～次年先）
		addColumn(new E_CSVColumnDescribe.BaseField('MRAnnual__c'));	// MR毎年臨店点検先
		addColumn(new E_CSVColumnDescribe.BaseField('MRBiannualVisitCurrentYear__c'));	// MR隔年臨店（当年先）
		addColumn(new E_CSVColumnDescribe.BaseField('MRBiannualVisitNextYear__c'));	// MR隔年臨店（次年先）
		*/
		addColumn(new E_CSVColumnDescribe.BaseField('OmitPropriety__c'));	// 当年点検省略可否
		addColumn(new E_CSVColumnDescribe.BaseField('SCPlan__c'));	// 点検実施方法（予定）
		addColumn(new E_CSVColumnDescribe.BaseField('SCFixed__c', '点検実施方法（結果）（最新）'));	// 点検実施方法（結果）
		addColumn(new E_CSVColumnDescribe.BaseField('Status__c', '代理店業務点検ステータス（最新）'));	// ステータス　※最新　名称[代理店業務点検ステータス（最新）]
		/* 201705対応 不備ステータス（最新）追加 */
		addColumn(new E_CSVColumnDescribe.BaseField('DispDefect__c', '不備ステータス（最新）'));	// 不備ステータス　※最新　名称[不備ステータス（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('HQSupport__c'));	// 本社対応
		addColumn(new E_CSVColumnDescribe.BaseField('MRName__c'));	// MR氏名
		addColumn(new E_CSVColumnDescribe.BaseField('OwnerMRName__c', 'MR氏名（最新）'));	// （所有者）MR氏名　※最新　名称[MR氏名（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('ZBUSMAIN__c'));	// 母店フラグ
		addColumn(new E_CSVColumnDescribe.BaseField('ZAGCYTYPE__c'));	// 種別
		addColumn(new E_CSVColumnDescribe.BooleanField('IsHeadSection__c', '本社営業部フラグ（最新）'));	// （事務所）本社営業部　※最新　名称[本社営業部フラグ（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('ZAGCYNUM__c'));	// 事務所コード
		addColumn(new E_CSVColumnDescribe.BaseField('ZEAYNAM__c'));	// 事務所名
		addColumn(new E_CSVColumnDescribe.BaseField('AccZEAYNAM__c', '事務所名（最新）'));	// （事務所）事務所名　※最新　名称[事務所名（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('KCHANNEL__c'));	// チャネル
		addColumn(new E_CSVColumnDescribe.BaseField('ZSOURCE__c'));	// ソース
		addColumn(new E_CSVColumnDescribe.BaseField('ZAGSHARE__c'));	// 乗合フラグ
		addColumn(new E_CSVColumnDescribe.BaseField('ZSHRMAIN__c'));	// 代申フラグ
		addColumn(new E_CSVColumnDescribe.BaseField('ZADDREASN__c'));	// 合算事由
		addColumn(new E_CSVColumnDescribe.BaseField('ZADDREL__c'));	// 合算関係
		addColumn(new E_CSVColumnDescribe.BaseField('ZADDAGENCY__c'));	// 母店コード（合算先）
		addColumn(new E_CSVColumnDescribe.BaseField('ZHEADAY__c'));	// 代理店格コード
		addColumn(new E_CSVColumnDescribe.BaseField('AGCLS__c'));	// 代理店資格
		addColumn(new E_CSVColumnDescribe.BaseField('ZhatFlg__c'));	// 募集資格者存在フラグ
		addColumn(new E_CSVColumnDescribe.BaseField('ZNWAY__c'));	// 当年登録フラグ
		addColumn(new E_CSVColumnDescribe.BooleanField('IsActiveOffice__c', '有効/廃業（最新）'));	// （事務所）有効　※最新　名称[有効/廃業（最新）]
		addColumn(new E_CSVColumnDescribe.BaseField('ZAHNAME__c'));	// 代理店格名
		/* 2018年度対応
		addColumn(new E_CSVColumnDescribe.DecimalField('InsuranceShareCnt__c'));	// 生保乗合
		addColumn(new E_CSVColumnDescribe.DecimalField('ZPASTMN__c'));	// 登録後経過月数
		addColumn(new E_CSVColumnDescribe.DecimalField('Z12NBIA__c'));	// COLI過去12ヶ月新契約ANP
		*/
		addColumn(new E_CSVColumnDescribe.BaseField('PostalCode__c'));	// 郵便番号（通信先）
		addColumn(new E_CSVColumnDescribe.BaseField('Address__c'));	// 通信先住所
		/* 2018年度対応
		addColumn(new E_CSVColumnDescribe.DecimalField('ZNBICNTY__c'));	// COLI新契約件数_YTD
		addColumn(new E_CSVColumnDescribe.DecimalField('ZNBIANPY__c'));	// COLI新契約ANP_YTD
		addColumn(new E_CSVColumnDescribe.DecimalField('ZHOICNT__c'));	// COLI保有件数
		addColumn(new E_CSVColumnDescribe.DecimalField('ZHOIANP__c'));	// COLI保有ANP
		addColumn(new E_CSVColumnDescribe.DecimalField('ZLAPSURA__c'));	// COLI失効・解約・取消・無効ANP
		addColumn(new E_CSVColumnDescribe.DecimalField('Z12NIIA__c'));	// COLI過去12ヶ月純増ANP
		addColumn(new E_CSVColumnDescribe.DecimalField('ZIQARAT__c'));	// IQA継続率
		addColumn(new E_CSVColumnDescribe.DecimalField('ZHOSPCNT__c'));	// SPVA保有件数
		addColumn(new E_CSVColumnDescribe.DecimalField('ZHOSRES__c'));	// SPVA保有積立金
		addColumn(new E_CSVColumnDescribe.DecimalField('ZNBHPRMY__c'));	// SPWH新契約実収保険料_YTD
		addColumn(new E_CSVColumnDescribe.DecimalField('ZHOHPCNT__c'));	// SPWH保有件数
		addColumn(new E_CSVColumnDescribe.DecimalField('ZHOHP__c'));	// SPWH保有実収保険料
		addColumn(new E_CSVColumnDescribe.BaseField('ZVALFLG__c'));	// 変額資格フラグ
		addColumn(new E_CSVColumnDescribe.DecimalField('ZVALATC__c'));	// 変額有資格募集人数
		*/
		addColumn(new E_CSVColumnDescribe.BaseField('UpsertKey__c'));	// UpsertKey
		/* 2018年度対応 */
		addColumn(new E_CSVColumnDescribe.BaseField('LimitPlan__c'));	// 締切（予定）
		addColumn(new E_CSVColumnDescribe.BaseField('LimitFixed__c'));	// 締切（結果）
		addColumn(new E_CSVColumnDescribe.BaseField('CMDRemandDate__c'));	// CMD差戻日

	}  
}