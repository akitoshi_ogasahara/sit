public with sharing class I_ExportSpRelationCorpController extends E_CSVExportController{
	private E_AgencySalesResults__c salesResult;


	public override String mainSObjectAPIName(){
		return Schema.SObjectType.E_SpRelationCorp__c.Name;
	}

	private String getBizDate(){
		return E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
	}
	/**
	 *  Constructor
	 */
	public I_ExportSpRelationCorpController(){
		super();

		//パラメータ取得
		String agency = ApexPages.currentPage().getParameters().get('agency');
		// データ取得
		List<E_AgencySalesResults__c> salesResults = E_AgencySalesResultsDaoWithout.getSpRelationCorp(agency, getBizDate());
		if(salesResults.size() == 1){
			salesResult = salesResults[0];
		}
	}
	/**
	 * タイトル
	 */
	public String getCSVTitle(){
		List<String> titles = new List<String>();
		titles.add(salesResult.AgencyName__c + '　特定関係法人');
		titles.add('');	//表示位置の調整
		titles.add('データ基準日　　'+salesResult.BusDate__c);	//営業日

		return String.join(titles, CSV_SEPARATOR()) + CSV_BREAKELINE();
	}
	/**
	 * ヘッダー
	 */
	public override String getCSVHeader(){
		List<String> header = new List<String>();
		header.add('特定関係法人名');
		header.add('関係');
		return String.join(header, CSV_SEPARATOR()) + CSV_BREAKELINE();
	}
	/**
	 * 行
	 */
	public List<String> getLines(){
		try{
			List<String> lines = new List<String>();
			
			// CSVデータ作成
			for(E_SpRelationCorp__c src : salesResult.E_SpRelationCorps__r){
				List<String> ln = new List<String>();
				
				// 特定関係法人名
				ln.add(E_Util.getEscapedCSVString(src.SpRelationCorpName__c));
				// 関係
				ln.add(E_Util.getEscapedCSVString(src.Relation__c));

				lines.add(String.join(ln, CSV_SEPARATOR()) + CSV_BREAKELINE());
			}
			return lines;
		}catch(Exception e){
			return null;
		}
	}
}