@isTest
private class TestI_OwnerController {

	//pageActionのテスト--MR
	@isTest static void pageActionMRTest() {
		User us = createRunAsUser('MR');
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);

		System.runAs(us){
			createIDManage(us);

			Test.startTest();
			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			System.assert(oc.pageMessages.hasMessages());
			Test.stopTest();
		}
	}

	//addRowsのテスト
	@isTest static void addRowsTest() {
		Test.startTest();
		I_OwnerController lpc = new I_OwnerController();
		lpc.rowCount = 981;
		lpc.addRows();
		System.assertEquals(lpc.rowCount,1000);
		Test.stopTest();
	}

	//該当する契約がありませんエラー
	@isTest static void policyRecsEmptyTest() {
		createMessageData('LIS|003','該当する契約者がおりません');
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us =[ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			Test.startTest();

			createIDManage(us);
			I_OwnerController oc = new I_OwnerController();

			oc.pageAction();
			System.assertEquals(oc.pageMessages.getWarningMessages()[0].summary,'該当する契約者がおりません');
			Test.stopTest();
		}
	}

	//sortRowsのテスト--顧客番号
	@isTest static void sortRowsCustomerNoTest() {
		Test.startTest();
		createPolicy();
		createPolicy();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'customerNo');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			conNoList.sort();
			System.assertEquals(oc.owners[0].customerNo,conNoList[0]);
			Test.stopTest();
		}
	}


	//sortRowsのテスト--契約者
	@isTest static void sortRowsCustomorTest() {
		clear();
		Test.startTest();
		createPolicy();
		createPolicy();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'contractant');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			oc.sortRows();
			conNameList.sort();
			conNameList.sort();
			System.assertEquals(oc.owners[0].owner,conNameList[0]);
			Test.stopTest();
		}
	}

	//sortRowsのテスト--住所
	@isTest static void sortRowsAddressTest() {
		clear();
		Test.startTest();
		createPolicy();
		createPolicy();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'address');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			addressList.sort();
			System.assertEquals(oc.owners[0].address,addressList[0]);
			Test.stopTest();
		}
	}

	//sortRowsのテスト--件数
	@isTest static void sortRowsNumTest() {
		clear();
		createPolicy();
		createPolicy4();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'num');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			Test.startTest();
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			System.assertEquals(oc.owners[0].num, 2);
			Test.stopTest();
		}
	}

	//sortRowsのテスト--代理店
	/*@isTest static void sortRowsAgencyTest() {
		Test.startTest();
		clear();
		createPolicy();
		createPolicy();
		PageReference pageRef = Page.IRIS_Owner;
		I_OwnerController oc = new I_OwnerController();

		pageRef.getParameters().put('st', 'customerNo');
		Test.setCurrentPage(pageRef);
		oc.sortRows();

		pageRef.getParameters().put('st', 'agency');
		Test.setCurrentPage(pageRef);
		oc.sortRows();
		System.assertEquals(oc.owners[0].agency,'(複数)');
		Test.stopTest();
	}*/

	//sortRowsのテスト--事務所
	@isTest static void sortRowsOfficeTest() {
		clear();
		createPolicy();
		createPolicy4();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'office');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			Test.startTest();
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			System.assertEquals(oc.owners[0].office,'(複数)');
			Test.stopTest();
		}
	}

	//sortRowsのテスト--募集人
	@isTest static void sortRowsAgentTest() {
		clear();
		createPolicy();
		createPolicy();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'agent');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			Test.startTest();
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			System.assertEquals(oc.owners[0].agent,'(複数)');
			Test.stopTest();
		}
	}

	//sortRowsのテスト--担当MR名
	@isTest static void sortRowsMRNameTest() {
		clear();
		createPolicy();
		createPolicy4();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'mr');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			Test.startTest();
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			oc.sortRows();
			System.assertEquals(oc.owners[0].mr,'(複数)');
			Test.stopTest();
		}
	}

	//getTotalRowsのテスト
	@isTest static void getTotalRowsTest() {
		clear();
		createPolicy();
		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			Test.startTest();
			createIDManage(us);

			I_OwnerController oc = new I_OwnerController();
			oc.pageAction();
			String rows = String.valueOf(oc.getTotalRows());
			System.assertEquals(rows,'1');
			Test.stopTest();
		}
	}
	/*
	 *フィルターモード
	 */
	@isTest static void filterModeTest() {
		
		Datetime d = system.now();
		//代理店格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//募集人コードが5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');
		//保険契約ヘッダ作成
		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = agentA.Id;
		po.MainAgentAccount__c = office.Id;
		po.MainAgent__c = agentA.Id;
		po.SubAgentAccount__c =office.Id;
		po.SubAgent__c = agentA.Id;
		insert po;

		PageReference pageRef = Page.IRIS_Owner;
		pageRef.getParameters().put('st', 'customerNo');
		pageRef.getHeaders().put('User-Agent', 'owner');
		Test.setCurrentPage(pageRef);
		
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		createIDManage(us);
		System.runAs(us){
			//=====================テスト開始=====================
			Test.startTest();
			I_OwnerController oc = new I_OwnerController();
			E_IRISHandler iris = E_IRISHandler.getInstance();
			//募集人Bで参照
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
			oc.pageAction();
			Integer totalRows = oc.getTotalRows();
			System.assertEquals(1,totalRows);
			//=====================テスト終了=====================
			Test.stopTest();
		}
	}


	//データ作成
	static Integer num = 0;
	static List<String> conNameList = new List<String>();
	static List<String> conNoList = new List<String>();
	static List<String> addressList = new List<String>();
	static void clear(){
		num = 0;
		conNameList = new List<String>();
		conNoList   = new List<String>();
		addressList = new List<String>();
	}

	static void createPolicy(){
		Account acc = createAccount();
		Contact con = createContact(acc);

		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;

		insert po;
		createPolicy2(con,acc);
		createPolicy3(con);
	}

	static void createPolicy2(Contact con,Account acc){
		Account acc2 = createAccount();
		Contact con2 = createContact2(acc);

		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con2.Id;

		insert po;
	}

	static void createPolicy3(Contact con){
		User us = createTestUser(con);
		Account acc3 = createAccountOwner(us);
		Contact con3 = createContact3(acc3);

		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.MainAgentAccount__c = acc3.Id;
		po.MainAgent__c = con3.Id;
		po.SubAgentAccount__c =acc3.Id;
		po.SubAgent__c = con3.Id;

		insert po;
	}

	static void createPolicy4(){
		Account accc = createAccount();
		Contact con = createContact(accc);

		User us = createTestUser(con);
		Account acc = createAccountOwner(us);

		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgentAccount__c =accc.Id;
		po.SubAgent__c = con.Id;

		insert po;
		createPolicy5(con,us);
	}

	static void createPolicy5(Contact con, User us){
		Account acc = createAccount();
		Contact con2 = createContact(acc);

		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.MainAgent__c = con2.Id;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con2.Id;

		insert po;
	}

	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}

	static Account createAccount(){
		Account pac = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = pac.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}

	static Account createAccountOwner(User us){
		Account pac = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = pac.Id;
		acp.OwnerId = us.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}

	static Contact createContact(Account acc){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋'+ String.valueOf(++num);
		addressList.add(con.E_COMM_ZCLADDR__c);
		con.E_CLTPF_ZCLKADDR__c = 'トウキョウトニホンバシ' + String.valueOf(num);
		con.E_CLTPF_CLNTNUM__c = 'Test' + String.valueOf(++num);
		conNoList.add(con.E_CLTPF_CLNTNUM__c);
		insert con;
		return con;
	}

	static Contact createContact2(Account acc){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋'+ String.valueOf(++num);
		con.E_CLTPF_ZCLKADDR__c = 'トウキョウトニホンバシ' + String.valueOf(num);
		con.E_CLTPF_CLNTNUM__c = 'Test' + String.valueOf(++num);
		insert con;
		return con;
	}

	static Contact createContact3(Account acc){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋'+ String.valueOf(++num);
		con.E_CLTPF_ZCLKADDR__c = 'トウキョウトニホンバシ' + String.valueOf(num);
		con.E_CLTPF_CLNTNUM__c = 'Test' + String.valueOf(++num);
		insert con;
		return con;
	}

	//ユーザ
	static User createTestUser(Contact con){
		User us = new User();
		us.Username = 'test@test.com@sfdc.com' + String.valueOf(++num);
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト' + String.valueOf(++num);
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
		return us;
	}

	//ユーザ
	static User createRunAsUser(String proName){
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		System.runAs(runAsUs){
//			us.Username = 'test@test.com@sfdc.com' + String.valueOf(++num);
			us.Username = 'testUser@terrasky.ingtesting' + String.valueOf(++num);
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト' + String.valueOf(++num);
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(proName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.IsActive = true;

			insert us;
		}
		return us;
	}

	//メッセージマスタ
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//ID管理作成
	static void createIDManage(User us){
		 E_IDCPF__c ic = new E_IDCPF__c();
		 ic.ZWEBID__c = '1234567890';
		 ic.ZIDOWNER__c = 'AG' + 'test01';
		 ic.User__c = us.Id;
		 ic.AppMode__c = 'IRIS';
		 ic.ZINQUIRR__c = 'BR00';
		 insert ic;
	}


	
}