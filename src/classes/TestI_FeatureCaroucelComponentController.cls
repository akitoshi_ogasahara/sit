@isTest
private class TestI_FeatureCaroucelComponentController {

	@isTest static void compJsonTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c();
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		I_FeatureCaroucelComponentController fcc = new I_FeatureCaroucelComponentController();
		fcc.cmsComps.add(ccc);

		System.assertNotEquals(fcc.getCompIdsAsJson(),null);
		Test.stopTest();
	}

	@isTest static void chatterJsonTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c();
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		I_FeatureCaroucelComponentController fcc = new I_FeatureCaroucelComponentController();
		fcc.cmsComps.add(ccc);

		System.assertNotEquals(fcc.getChatterJson(),null);
		Test.stopTest();
	}

}