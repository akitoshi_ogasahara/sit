@isTest
private class TestE_MessageMasterDao
{
    /**
     * MessageMasterDao 正常系
     */
    private static testMethod void testMessageMasterDao001() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	// テストデータの作成
        	RecordType rtype = [Select Id From RecordType Where DeveloperName = 'MSG_DISCLAIMER' Limit 1];
        	// Menuの登録
            E_MenuMaster__c menuMaster1 = new E_MenuMaster__c(MenuMasterKey__c='key1',SelectedMenuKey__c='key1');
            insert menuMaster1;
            E_MenuMaster__c menuMaster2 = new E_MenuMaster__c(MenuMasterKey__c='key2',SelectedMenuKey__c='key1');
            insert menuMaster2;

            List<E_MessageMaster__c> msgList = new List<E_MessageMaster__c>();
            for(Integer i = 0; i < 8; i++){
                // メッセージ
                E_MessageMaster__c messageMaster = new E_MessageMaster__c();
                messageMaster.Name = 'Err' + String.valueOf(i);
                messageMaster.RecordTypeId = rtype.Id;
                messageMaster.Type__c = 'メッセージ';
                messageMaster.Key__c = 'ACE|00' + String.valueOf(i);
                messageMaster.Value__c = 'ACE|00' + String.valueOf(i);
                messageMaster.Menu__c = menuMaster1.Id;
                if(i == 7){
                	messageMaster.DispFrom__c = Date.today();
                	messageMaster.DispTo__c  = Date.today();
                }
                msgList.add(messageMaster);
            }
            insert msgList;

            ContentVersion content = new ContentVersion(Title='title',PathOnClient='/foo.txt');
            content.VersionData = Blob.valueOf('Testing base 64 encode');
            insert content;

            E_CMSFile__c cmsFile = new E_CMSFile__c(ContentsName__c='ContentsName');
            cmsFile.MessageMaster__c = msgList.get(0).Id;
            insert cmsFile;

            //テスト開始
            Test.startTest();

            System.assert(E_MessageMasterDao.getRecByCode('ACE|001') != null);
            System.assert(E_MessageMasterDao.getMessageMapByType('メッセージ') != null);
            System.assert(E_MessageMasterDao.getMessagesByMenuId('1234567') != null);
            System.assert(E_MessageMasterDao.getMessagesByMenuId(menuMaster1.Id) != null);
            Set<String> setString = new Set<String>{'title'};
            System.assert(E_MessageMasterDao.getContentVersionsByTitles(setString) != null);
            List<E_CMSFile__c> cmsList = new List<E_CMSFile__c>();
            cmsList.add(cmsFile);
            System.assert(E_MessageMasterDao.getCMSFilesWithAttachmentByIds(cmsList) != null);
            System.assert(E_MessageMasterDao.getAttachmentsByMenuId('dummy') != null);
            // レコード取得
            System.assert(E_MessageMasterDao.getMenuMasterRecById(menuMaster1.Id) != null);
            // レコード未取得
            System.assert(E_MessageMasterDao.getMenuMasterRecById(msgList.get(0).Id) == null);
            // レコード取得
            System.assert(E_MessageMasterDao.getMenuMasterRecByKey('key1') != null);
            // レコード未取得
            System.assert(E_MessageMasterDao.getMenuMasterRecByKey('keyDummy') == null);
            System.assert(E_MessageMasterDao.getSubPagesByKey('key1') != null);
            // レコード取得
            System.assert(E_MessageMasterDao.getMenuMasterRecByLikeKey('key') != null);
            // レコード未取得
            System.assert(E_MessageMasterDao.getMenuMasterRecByLikeKey('keyDummy') == null);
           	
            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
        
    }
    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}