/**
 * 10桁ロジックを行うクラス
 * CreatedDate 2016/10/04
 * @author Terrasky
 */
public without sharing class E_createNNLinkID {
//public without sharing class E_createNNLinkID {
    //ID管理オブジェクトの使用者によってアクセスできない可能性。


	public E_createNNLinkID() {
    

	}

    public static String createNNLinkID(){

        String idPf = '50%';
        //'50'で始まるＩＤ番号の最大値を取得
        List<E_IDCPF__c> records = 
        [select ZWEBID__c from E_IDCPF__c where ZWEBID__c like :idPf order by ZWEBID__c DESC limit 1];
        System.debug(records);
        String strNum;
        if(records.isEmpty()){
            System.debug('該当するID番号が存在しません。');
            //該当する番号が存在しない場合、5000000000であるため、左端と右端を除外して+1した値
            strNum = '00000001';
        }else{
            //該当する番号が存在する場合、右端を除外
            strNum = String.valueOf(records[0].ZWEBID__c).substring(0,9);
            //+1
            strNum = String.valueOf(Integer.valueOf(strNum) + 1);
            //左端を除外
            strNum = strNum.substring(1);
        }
        System.debug(strNum);

        //計算結果格納用
        Integer num = 0;
        //倍数格納用
        Integer multiple = 1;
        //該当する桁の値格納用
        String box = ' ';
        //右端の桁から計算実施
        for(Integer sub = 8; sub >= 1 ; sub--){
            box = strNum.substring(sub-1,sub);
            System.debug(sub);
            System.debug(box);
            num += Integer.valueOf(box) * multiple;
            multiple *= 2;
        }
        
        //得られた値を11で割り、さらに11から引く
        Integer checkDigit = num;
        checkDigit = math.mod(checkDigit,11);
        checkDigit = 11 - checkDigit;

        if(checkDigit == 10){
            checkDigit = 9;
        }else if(checkDigit == 11){
            checkDigit = 1;
        } 

        //上記の計算で得られた値を結合
        strNum = strNum + String.valueOf(checkDigit);
        //左端を除外
        strNum = strNum.substring(1);
        //先頭に50を結合
        strNum = E_Const.ZWEBID_TOP_50 + strNum;
        System.debug(strNum);

        return strNum;

    }
}