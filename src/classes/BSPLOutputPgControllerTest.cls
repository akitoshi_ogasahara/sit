/**
* クラス名	:  BSPLOutputPageController
* クラス概要	:  ＢＳ・ＰＬ詳細画面(テスト)
* @created	:  2
* @modified	:   
*/
@isTest
private class BSPLOutputPgControllerTest {

	/**
	* testMethodBSPLOutputPgController()
	* ページ新規化処理（テスト）
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodBSPLOutputPgController() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);

		ApexPages.currentPage().getParameters().put('id', fi.Id);
		
		// ファイナンス情報
		Finance__c finance02 = createFinance(989, customs.Id, 1);


		ApexPages.StandardController sc = new ApexPages.StandardController(fi);
		Test.startTest();
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		Test.stopTest();
		//System.assertEquals(1, ct.finance.Term__c);
	}
	/**
	* testMethodBSPLOutputPgController2()
	* ページ新規化処理（テスト）
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodBSPLOutputPgController2() {
		
		ApexPages.currentPage().getParameters().put('id', 'a0N00000001');

		// ファイナンス情報
		Finance__c finance = new Finance__c();
		
		Finance__c finance02 = new Finance__c();

		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		Test.startTest();
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		Test.stopTest();
		//System.assertEquals(1, ct.finance.Term__c);
	}
	/**
	* testMethodClickLinkKokyakuShousaiJouhou()
	* 「顧客詳細情報へ」リンク処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodClickLinkKokyakuShousaiJouhou() {
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);

		delete customs;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(fi);
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		
		ct.finance = fi;
		
		Test.startTest();
		try{
			// 「顧客詳細情報へ」リンク処理
			ct.clickLinkKokyakuShousaiJouhou();
		}catch(Exception ex){
			//System.assert(true, String.valueOf(Apexpages.getMessages()).contains(label.Msg_Error_H_NotCustomer));	
		}
		Test.stopTest();
	}
	/**
	* testMethodClickLinkKokyakuShousaiJouhou_1()
	* 「顧客詳細情報へ」リンク処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodClickLinkKokyakuShousaiJouhou_1() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;


		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);

		ApexPages.StandardController sc = new ApexPages.StandardController(fi);
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		
		ct.finance = fi;
		
		Test.startTest();
		//  「顧客詳細情報へ」リンク処理
		Pagereference pageTest = ct.clickLinkKokyakuShousaiJouhou();
		Test.stopTest();
		//System.assert(true, pageTest.getUrl().contains(ct.finance.Customer__c));
	}
	/**
	* testMethodActionButton()
	* test ham actionButton()
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodActionButton() {
		
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);

		ApexPages.StandardController sc = new ApexPages.StandardController(fi);
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		
		ct.finance =fi;
		
		Test.startTest();
		Pagereference pageTest = ct.actionButton();
		Test.stopTest();
		//System.assert(true, pageTest.getUrl().contains('BSPLInputPage'));
	}
	/**
	* testMethodActionButton2()
	* test ham actionButton()
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodActionButton2() {
		// ファイナンス情報
		Finance__c finance = new Finance__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		Test.startTest();
		Pagereference pageTest = ct.actionButton();
		Test.stopTest();
		//System.assert(true, pageTest.getUrl().contains('BSPLInputPage'));
	}
	/**
	* testMethodhearringInput()
	* 「ヒアリング情報入力へ」リンク処理(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodhearringInput() {
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(finance);
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		Test.startTest();
		try{
			// 「ヒアリング情報入力へ」リンク処理
			ct.hearringInput();
		}catch(Exception ex){
			//System.assert(true, String.valueOf(Apexpages.getMessages()).contains(label.Msg_Error_H_NotCustomer));	
		}
		Test.stopTest();
	}
	/**
	* testMethodhearringInput2()
	* 「ヒアリング情報入力へ」リンク処理(テスト)
	* @return: なし
	* @created: 
	*/
	static testMethod void testMethodhearringInput2() {
		// 顧客情報を作成
		Customer__c customs = createCustomer('BSPLInput-test');
		insert customs;

		// ファイナンス情報を作成
		Finance__c fi = createFinance(990, customs.Id, 1);

		ApexPages.StandardController sc = new ApexPages.StandardController(fi);
		// コントローラを作成
		BSPLOutputPgController ct = new BSPLOutputPgController(sc);
		
		ct.finance = fi;
		
		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_FIID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);

		Test.startTest();
		try{
			// 「ヒアリング情報入力へ」リンク処理
			ct.hearringInput();
		}catch(Exception ex){
			//System.assert(true, String.valueOf(Apexpages.getMessages()).contains(label.Msg_Error_H_NotCustomer));	
		}
		Test.stopTest();
	}

	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 
	*/
	static Customer__c createCustomer(String name){
		// 顧客
		Customer__c customs = new Customer__c();
		// 顧客名
		customs.Name = name;
		// 代理店
		//  代理店【INGAgency】を作成
		AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
		agcINGA.Name = 'BSPLInput-test-test';
		insert agcINGA;
		customs.AgencyForINGAgency__c = agcINGA.Id;
		// 業界平均マスタ
		customs.Industry__c = 'BSPLInput-test';
		return customs;
	}
	/**
	* createFinance()
	* ファイナンス情報を作成
	* @return: なし
	* @created: 
	*/
	static Finance__c createFinance(integer opTerm, string cuid, Integer intOption){
		// 顧客を作成
		//Customer__c customs = createCustomer('BSPLInput-test');
		//insert customs;
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		
		finance.Term__c = opTerm;
		finance.BS_Cash__c = 1000 * intOption;
		finance.BS_NR__c = 2000 * intOption;
		finance.BS_AR__c = 3000 * intOption;
		finance.BS_PROD__c = 4000 * intOption;
		finance.BS_WIPRM__c = 5000 * intOption;
		finance.BS_SE__c = 6000 * intOption;
		finance.BS_STLR__c = 7000 * intOption;
		finance.BS_PRP__c = 8000 * intOption;
		finance.BS_PRC__c = 9000 * intOption;
		finance.BS_TA__c = 10000 * intOption;
		finance.BS_OCA__c = 11000 * intOption;
		finance.BS_LAC__c = 12000 * intOption;
		//outputBS_CATT = '0' * intOption;
		finance.BS_DP__c = 13000 * intOption;
		finance.BS_Land__c = 14000 * intOption;
		finance.BS_IFA__c = 15000 * intOption;
		finance.BS_IS__c = 16000 * intOption;
		finance.BS_LTLR__c = 17000 * intOption;
		finance.BS_DE__c = 18000 * intOption;
		finance.BS_PR__c = 19000 * intOption;
		finance.BS_OFA__c = 20000 * intOption;
		finance.BS_LAF__c = 21000 * intOption;
		//outputBS_FATT = '0' * intOption;
		finance.BS_DA__c = 22000 * intOption;
		//outputBS_ASTT = '0' * intOption;
										
		finance.BS_F_Cash__c = 1000 * intOption;
		finance.BS_F_NR__c = 2000 * intOption;
		finance.BS_F_AR__c = 3000 * intOption;
		finance.BS_F_PROD__c = 4000 * intOption;
		finance.BS_F_WIPRM__c = 5000 * intOption;
		finance.BS_F_SE__c = 6000 * intOption;
		finance.BS_F_STLR__c = 7000 * intOption;
		finance.BS_F_PRP__c = 8000 * intOption;
		finance.BS_F_PRC__c = 9000 * intOption;
		finance.BS_F_TA__c = 10000 * intOption;
		finance.BS_F_OCA__c = 11000 * intOption;
		finance.BS_F_LAC__c = 12000 * intOption;
		finance.BS_F_CATT__c = 13000 * intOption;
		finance.BS_F_DP__c = 14000 * intOption;
		finance.BS_F_Land__c = 15000 * intOption;
		finance.BS_F_IFA__c = 16000 * intOption;
		finance.BS_F_IS__c = 17000 * intOption;
		finance.BS_F_LTLR__c = 18000 * intOption;
		finance.BS_F_DE__c = 19000 * intOption;
		finance.BS_F_PR__c = 20000 * intOption;
		finance.BS_F_OFA__c = 21000 * intOption;
		finance.BS_F_LAF__c = 22000 * intOption;
		finance.BS_F_FATT__c = 23000 * intOption;
		finance.BS_F_DA__c = 24000 * intOption;
		finance.BS_F_ASTT__c = 25000 * intOption;
		
		finance.BS_NP__c = 1000 * intOption;
		finance.BS_AP__c = 2000 * intOption;
		finance.BS_STD__c = 3000 * intOption;
		finance.BS_OAP__c = 4000 * intOption;
		finance.BS_AE__c = 5000 * intOption;
		finance.BS_ADR__c = 6000 * intOption;
		finance.BS_DR__c = 7000 * intOption;
		finance.BS_ITP__c = 8000 * intOption;
		finance.BS_CTP__c = 9000 * intOption;
		finance.BS_AC__c = 10000 * intOption;
		finance.BS_OCL__c = 11000 * intOption;
		//outputBS_CLTT = '0' * intOption;
		finance.BS_LTD__c = 12000 * intOption;
		finance.BS_Bond__c = 13000 * intOption;
		finance.BS_AF__c = 14000 * intOption;
		finance.BS_OLTL__c = 15000 * intOption;
		//outputBS_LLTT = '0' * intOption;
		//outputBS_LBTT = '0' * intOption;
		finance.BS_CAP__c = 16000 * intOption;
		finance.BS_CS__c = 17000 * intOption;
		finance.BS_RE__c = 18000 * intOption;
		finance.BS_TS__c = 19000 * intOption;
		finance.BS_Other__c = 66000 * intOption;
		//outputBS_NATT = '0' * intOption;
		//outputBS_LNAT = '0' * intOption;
		
		finance.BS_F_NP__c = 3100 * intOption;
		finance.BS_F_AP__c = 3200 * intOption;
		finance.BS_F_STD__c = 3300 * intOption;
		finance.BS_F_OAP__c = 3400 * intOption;
		finance.BS_F_AE__c = 3500 * intOption;
		finance.BS_F_ADR__c = 3600 * intOption;
		finance.BS_F_DR__c = 3700 * intOption;
		finance.BS_F_ITP__c = 3800 * intOption;
		finance.BS_F_CTP__c = 3900 * intOption;
		finance.BS_F_AC__c = 4000 * intOption;
		finance.BS_F_OCL__c = 4100 * intOption;
		finance.BS_F_CLTT__c = 4200 * intOption;
		finance.BS_F_LTD__c = 4300 * intOption;
		finance.BS_F_Bond__c = 4400 * intOption;
		finance.BS_F_AF__c = 4500 * intOption;
		finance.BS_F_OLTL__c = 4600 * intOption;
		finance.BS_F_LLTT__c = 4700 * intOption;
		finance.BS_F_LBTT__c = 4800 * intOption;
		finance.BS_F_CAP__c = 4900 * intOption;
		finance.BS_F_CS__c = 5000 * intOption;
		finance.BS_F_RE__c = 5100 * intOption;
		finance.BS_F_TS__c = 5200 * intOption;
		finance.BS_F_Other__c = 5300 * intOption;
		finance.BS_F_NATT__c = 5400 * intOption;
		finance.BS_F_LNAT__c = 5500 * intOption;

					
		finance.BS_C_Cash_P__c  = '0％';
		finance.BS_C_NR_P__c    = '0％';
		finance.BS_C_AR_P__c    = '0％';
		finance.BS_C_PROD_P__c  = '0％';
		finance.BS_C_WIPRM_P__c = '0％';
		finance.BS_C_SE_P__c    = '0％';
		finance.BS_C_STLR_P__c  = '0％';
		finance.BS_C_PRP_P__c   = '0％';
		finance.BS_C_PRC_P__c   = '0％';
		finance.BS_C_TA_P__c    = '0％';
		finance.BS_C_OCA_P__c   = '0％';
		finance.BS_C_DP_P__c    = '0％';
		finance.BS_C_Land_P__c  = '0％';
		finance.BS_C_IFA_P__c   = '0％';
		finance.BS_C_IS_P__c    = '0％';
		finance.BS_C_LTLR_P__c  = '0％';
		finance.BS_C_DE_P__c    = '0％';
		finance.BS_C_PR_P__c    = '0％';
		finance.BS_C_OFA_P__c   = '0％';
		finance.BS_C_DA_P__c    = '0％';

		finance.PL_SV__c = 10 * intOption;
		finance.PL_COS__c = 20 * intOption;
		//outputPL_GP = '0';
		finance.PL_SGAAE__c = 30 * intOption;
		//outputPL_SI = '0';
		finance.PL_NSI__c = 40 * intOption;
		finance.PL_NSE__c = 50 * intOption;
		//outputPL_OI = '0';
		finance.PL_EI__c = 60 * intOption;
		finance.PL_EL__c = 70 * intOption;
		//outputPL_IBIT = '0';
		finance.PL_Tax__c = 80 * intOption;
		//outputPL_IBIT = '0';
		//outputPL_CI = '0';
		finance.PL_DE__c = 500 * intOption;
		finance.PL_IED__c = 300 * intOption;
		finance.PL_DIS__c = 100 * intOption;
					

		finance.Customer__c = cuid; // 顧客名
		insert finance;
	
		return finance;

	}
}