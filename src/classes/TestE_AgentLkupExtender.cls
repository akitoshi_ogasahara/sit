@isTest(SeeAllData=false)
public with sharing class TestE_AgentLkupExtender {

	private static testMethod void testAgentLkup() {
		//データ作成
		Contact cnt = new Contact();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'test', 'システム管理者');

		System.runAs(u){
			//テスト開始
			Test.startTest();

			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(cnt);
			E_AgentLkupController controller = new E_AgentLkupController(standardcontroller);
			E_AgentLkupExtender extender = controller.getExtender();
			controller.ipVal_agentkana.SkyEditor2__Text__c = 'ﾃｽﾄ';
			extender.presearch();

			//確認
			system.assertEquals(controller.ipVal_agentkana.SkyEditor2__Text__c, 'テスト');

			//テスト終了
			Test.stopTest();
		}
	}

}