@isTest
private class TestI_SurveyTargetDao {

	/*
	 * 重複チェック/アンケート回答済みの場合
	 * 
	 */
	@isTest static void getRecByUserIdAndSurveyIdTest01(){
		//テストデータ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, true);
		//テスト実行
		Test.startTest();
		List<I_SurveyTarget__c> targetList = I_SurveyTargetDao.getRecByUserIdAndSurveyId(us.Id, survey.Id);
		Test.stopTest();
		//テスト検証
		List<I_SurveyTarget__c> targetRecs = [SELECT Id, OwnerId, Survey__r.Id FROM I_SurveyTarget__c WHERE Id =: targetList[0].Id];
		System.assertEquals(survey.Id, targetRecs[0].Survey__r.Id);
		System.assertEquals(us.Id, targetRecs[0].OwnerId);
		System.assertEquals(1, targetRecs.size());
	}

	/*
	 * 重複チェック/アンケート未回答の場合
	 * 
	 */
	@isTest static void getRecByUserIdAndSurveyIdTest02(){
		//テストデータ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, false);
		//テスト実行
		Test.startTest();
		List<I_SurveyTarget__c> targetList = I_SurveyTargetDao.getRecByUserIdAndSurveyId(us.Id, survey.Id);
		Test.stopTest();
		//テスト検証
		System.assertEquals(0, targetList.size());
	}

	/*
	 * アンケート登録
	 * @param userId: userId
	 * @param isAnswered: whether agent has answered
	 * @return I_Survey__c： アンケート
	 */
	private static I_Survey__c createSurveyRec(Id userId, boolean isAnswered){
		//アンケート作成
		I_Survey__c survey = new I_Survey__c();
		survey.Name = 'テストアンケート';
		survey.OverView__c = 'テスト概要';
		survey.UpsertKey__c = 'survey01';
		survey.PeriodFrom__c = Date.today();
		survey.PeriodTo__c = Date.today().addDays(5);
		survey.SurveyTarget__c = '募集人';
		insert survey;
		//アンケートに紐づく対象者作成
		if(isAnswered){
			I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
			sTarget.Ownerid = userId;
			sTarget.Survey__c = survey.id;
			insert sTarget;
		}
		//アンケートに紐づく設問作成
		I_SurveyQuestion__c question = new I_SurveyQuestion__c();
		question.Name = 'テスト設問';
		question.Selection__c = '参加';
		question.Survey__c = survey.Id;
		insert question;

		return survey;
	}

	/*
	 * 代理店ユーザー作成
	 * @return User: ユーザー
	 */
	private static User createUser(){
		User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
		User us = new User();
		System.runAs(thisUser){
			Account parentAcc = TestI_TestUtil.createAccount(true, null);
			Account acc = TestI_TestUtil.createAccount(true, parentAcc);
			Contact con = TestI_TestUtil.createContact(true, acc.Id, 'testCon');
			us = TestI_TestUtil.createAgentUser(true, 'testUser', E_Const.PROFILE_E_PARTNERCOMMUNITY, con.Id);
			TestI_TestUtil.createAccountShare(acc.Id, us.Id);
			//代理店権限を付与
			TestI_TestUtil.createBasePermissions(us.Id);
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, us.Id, 'AY');
		}
		return us;
	}

}