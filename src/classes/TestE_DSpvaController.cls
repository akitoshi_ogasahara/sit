@isTest
private with sharing class TestE_DSpvaController{
	private static testMethod void testPageMethods() {		E_DSpvaController extension = new E_DSpvaController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testCRLPFTable() {
		E_DSpvaController.CRLPFTable CRLPFTable = new E_DSpvaController.CRLPFTable(new List<E_CRLPF__c>(), new List<E_DSpvaController.CRLPFTableItem>(), new List<E_CRLPF__c>(), null);
		CRLPFTable.create(new E_CRLPF__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableZREV() {
		E_DSpvaController.dataTableZREV dataTableZREV = new E_DSpvaController.dataTableZREV(new List<E_TransitionWK__c>(), new List<E_DSpvaController.dataTableZREVItem>(), new List<E_TransitionWK__c>(), null);
		dataTableZREV.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableSUMI() {
		E_DSpvaController.dataTableSUMI dataTableSUMI = new E_DSpvaController.dataTableSUMI(new List<E_TransitionWK__c>(), new List<E_DSpvaController.dataTableSUMIItem>(), new List<E_TransitionWK__c>(), null);
		dataTableSUMI.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableZRCH() {
		E_DSpvaController.dataTableZRCH dataTableZRCH = new E_DSpvaController.dataTableZRCH(new List<E_TransitionWK__c>(), new List<E_DSpvaController.dataTableZRCHItem>(), new List<E_TransitionWK__c>(), null);
		dataTableZRCH.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableSTEP() {
		E_DSpvaController.dataTableSTEP dataTableSTEP = new E_DSpvaController.dataTableSTEP(new List<E_TransitionWK__c>(), new List<E_DSpvaController.dataTableSTEPItem>(), new List<E_TransitionWK__c>(), null);
		dataTableSTEP.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
}