@isTest
private class TestSC_SelfCompliances_IrisController {

	public static String fiscalYear = '2018';

	//共通 PDF 追加 PDF
	@isTest static void initTest01() {

		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);

		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');


			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);
		
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
			Test.stopTest();
			System.assertEquals(3,controller.submitList.size());
		}
		
	}

	//共通 Web 追加 PDF
	@isTest static void initTest02() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'WEB');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');


			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
			Test.stopTest();
			System.assertEquals(3,controller.submitList.size());
		}
	}


	//点検実施の手引き取得
	@isTest static void getManualTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'WEB');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);

			//ページ作成
			I_PageMaster__c page = createManualPage();
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				String manualId = controller.getManualId();
			Test.stopTest();
			System.assertEquals(page.Id,manualId);
		}
	}

	//操作方法PDF取得
	@isTest static void getHelpIdTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'WEB');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);

			//ページ作成
			Attachment att = createHelpCMS();
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				String helpId = controller.getHelpId();
			Test.stopTest();
			System.assertEquals(att.Id,helpId);
		}
	}

	//コメント保存処理 未入力
	@isTest static void doSaveCommentTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.doSaveComment();
			Test.stopTest();

			SC_Office__c afterOffice = SC_OfficeDao.getRecbyId(office.id);

			System.assertEquals(true,String.isBlank(afterOffice.Comment__c));
		}
	}

	//コメント保存処理 コメント有
	@isTest static void doSaveCommentTest02() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.addComment = 'test';
				controller.doSaveComment();
			Test.stopTest();

			SC_Office__c afterOffice = SC_OfficeDao.getRecbyId(office.id);

			System.assertEquals(false,String.isBlank(afterOffice.Comment__c));
		}
	}

	//問い合わせ保存処理 未入力
	@isTest static void doSaveInquiryTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			SC_Office__c office = createOffice(accAgency);
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.doSaveInquiry();
			Test.stopTest();

			SC_Office__c afterOffice = SC_OfficeDao.getRecbyId(office.id);

			System.assertEquals(true,String.isBlank(afterOffice.Inquiry__c));
		}
	}

	//問い合わせ保存処理 コメント有 代理店
	@isTest static void doSaveInquiryTest02() {
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenuAg();
		//// テストデータ作成
		SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
		Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

		// 実行ユーザ作成（代理店）
		User actUser = TestSC_TestUtil.createTestDataSet('AY', office);


		// 業務管理責任者
		SC_AgencyManager__c agManager = new SC_AgencyManager__c();
		agManager.SC_Office__c = office.Id;
		agManager.User__c = actUser.Id;
		insert agManager;

		pageRef.getParameters().put('id',office.id);
		Test.setCurrentPage(pageRef);
		System.runAs(actUser){
			// 自主点検（代理店）権限セットを付与
			TestSC_TestUtil.createAgencyPermissions(actUser.Id);
			TestSC_TestUtil.createBasePermissions(actUser.Id);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.addInquiry = 'test';
				controller.doSaveInquiry();
			Test.stopTest();
		}

		SC_Office__c afterOffice = SC_OfficeDao.getRecbyId(office.id);

		System.assertEquals(false,String.isBlank(afterOffice.Inquiry__c));
	}

	//問い合わせ保存処理 コメント有 MR/拠点長
	@isTest static void doSaveInquiryTest03() {
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		createMenu();
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfCompliances_IrisController.createContact(true, accAgency.Id, actUserLastName);
		// 実行ユーザ作成（社員）
		User actUser = TestSC_TestUtil.createUser(true, 'MR', 'ＭＲ');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		accAgency.Owner = actUser;
		update accAgency;
		SC_Office__c office = new SC_Office__c();
		System.runAs(actUser){
			office = createOffice(accAgency);
		}

		Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'WEB');
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
		updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

		pageRef.getParameters().put('id',office.id);
		Test.setCurrentPage(pageRef);
		System.runAs(actUser){
			//社員権限セット
			TestSC_TestUtil.createEmployeePermissions(actUser.Id);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.addInquiry = 'test';
				controller.doSaveInquiry();
				System.debug(SC_OfficeDao.getRecbyId(office.id));
			Test.stopTest();
		}

		SC_Office__c afterOffice = SC_OfficeDao.getRecbyId(office.id);

		System.assertEquals(false,String.isBlank(afterOffice.Inquiry__c));
	}

	//問い合わせ保存処理 コメント有 CMD
	@isTest static void doSaveInquiryTest04() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.addInquiry = 'test';
				controller.doSaveInquiry();
			Test.stopTest();

			SC_Office__c afterOffice = SC_OfficeDao.getRecbyId(office.id);

			System.assertEquals(false,String.isBlank(afterOffice.Inquiry__c));
		}
	}

	//画面再描画テスト
	@isTest static void rerenderBrowserTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			pageRef.getParameters().put('isOldIE','false');
			Test.setCurrentPage(pageRef);

			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				PageReference rerenderPage = controller.rerenderBrowser();
			Test.stopTest();

			System.assertEquals(null,rerenderPage);
		}
	}

	//ファイル削除
	@isTest static void deleteFileTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);

			//添付ファイルのパラメータ
			scMap = getSelfCompMap(office.id);
			List<Attachment> att = scMap.get(SC_Const.SC_COMP_TYPE_COMMON).Attachments;
			pageRef.getParameters().put('modalAttId',att[0].Id);
			pageRef.getParameters().put('modalAttName',att[0].Name);

			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.createModalData();
				PageReference rerenderPage = controller.deleteFile();
			Test.stopTest();

			scMap = getSelfCompMap(office.id);
			att = scMap.get(SC_Const.SC_COMP_TYPE_COMMON).Attachments;
			System.assertEquals(null,rerenderPage);
			System.assertEquals(0,att.size());
		}
	}

	//提出種別更新
	@isTest static void updateTypeSubmitTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			//更新後の自主点検
			scMap = getSelfCompMap(office.id);
			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.targetId = scMap.get(SC_Const.SC_COMP_TYPE_COMMON).Id;
				controller.typeSubmit = 'WEB';
				controller.updateTypeSubmit();
			Test.stopTest();

			scMap = getSelfCompMap(office.id);
			System.assertEquals('WEB',scMap.get(SC_Const.SC_COMP_TYPE_COMMON).TypeSubmit__c );
		}
	}

	//PDFファイル削除
	@isTest static void deletePDFFileTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);

			//添付ファイルのパラメータ
			scMap = getSelfCompMap(office.id);
			List<Attachment> att = scMap.get(SC_Const.SC_COMP_TYPE_COMMON).Attachments;

			Test.setCurrentPage(pageRef);
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				controller.targetPDFId = att[0].Id;
				controller.deletePDFFile();
			Test.stopTest();

			scMap = getSelfCompMap(office.id);
			att = scMap.get(SC_Const.SC_COMP_TYPE_COMMON).Attachments;
			System.assertEquals(0,att.size());
		}
	}

	//提出ボタン 無効判定 提出ボタン有効
	@isTest static void getIsApplyDisabledTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				Boolean isDisabled = controller.getIsApplyDisabled();
			Test.stopTest();

			System.assertEquals(false,isDisabled);
		}
	}

	//提出ボタン 無効判定 提出ボタン無効
	@isTest static void getIsApplyDisabledTest02() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON));
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				Boolean isDisabled = controller.getIsApplyDisabled();
			Test.stopTest();

			System.assertEquals(true,isDisabled);
		}
	}

	//申請ボタン PDFのみ
	@isTest static void doActionTest01() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				PageReference rerenderPage = controller.doAction();
			Test.stopTest();

			System.assertEquals(null,rerenderPage);
		}
	}

	//申請ボタン Web回答有
	@isTest static void doActionTest02() {
		// 実行ユーザ作成（CMD）
		User actUser = TestSC_TestUtil.createUser(true, 'CMD', 'E_EmployeeStandard');
		TestSC_TestUtil.createIDCPF(true, actUser.Id, null);
		PageReference pageRef = Page.IRIS_SCSelfCompliances;
		//メニュー作成
		createMenu();
		// テストユーザで実行
		System.runAs(actUser){
			TestSC_TestUtil.createCMDPermissions(actUser.Id);
			// テストデータ作成
			SC_Office__c office = TestSC_TestUtil.createSCOffice(true, null, null, SC_Const.AMSOFFICE_STATUS_01, '2018');
			Map<String, SC_SelfCompliance__c> scMap = getSelfCompMap(office.id);
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_COMMON),'WEB');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_NN),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');
			updateSelfComp(scMap.get(SC_Const.SC_COMP_TYPE_TRAIL),'PDF');

			pageRef.getParameters().put('id',office.id);
			Test.setCurrentPage(pageRef);
			Test.setMock(HttpCalloutMock.class, new TestSC_SelfCompliancesHTTPRequestMock(false));
			Test.startTest();
				SC_SelfCompliances_IrisController controller = new SC_SelfCompliances_IrisController();
				PageReference pageActionres = controller.pageAction();
				PageReference rerenderPage = controller.doAction();
			Test.stopTest();

			System.assertEquals(null,rerenderPage);
		}
	}


	public static SC_Office__c createOffice(Account acc){
		SC_Office__c office = new SC_Office__c();
		office.LimeSurveyTokenCommon__c = 'commonToken';
		office.LimeSurveyTokenAdd__c = 'addToken';
		office.FiscalYear__c = fiscalYear;
		office.Status__c = SC_Const.AMSOFFICE_STATUS_01;
		Insert office;
		return office;
	}

	public void createSelfCompliance(){
		SC_SelfCompliance__c selfComp = new SC_SelfCompliance__c();
	}



	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 */
	public static Contact createContact(Boolean isInsert, Id accId, String lastName) {
		Contact src = new Contact(
			  AccountId = accId
			, LastName = lastName
			, E_CL3PF_ZAGMANGR__c = 'Y'
		);
		if(isInsert){
			insert src;
			src = [Select Id, Account.Name, LastName, E_AccParentCord__c From Contact Where Id =: src.Id];
		}
		return src;
	}

	/*
	 * メニュー作成 社員
	 */
	public static void createMenu(){
		I_MenuMaster__c menu = new I_MenuMaster__c();
		menu.Name = '代理店自己点検';
		menu.MenuKey__c = SC_Const.MENU_KEY_SC_SEARCH_EMPLOYEE;
		menu.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT;
		Insert menu;
	}

	/*
	 * メニュー作成 代理店
	 */
	public static void createMenuAg(){
		I_MenuMaster__c menu = new I_MenuMaster__c();
		menu.Name = '代理店自己点検';
		menu.MenuKey__c = SC_Const.MENU_EKY_SC_SEARCH_AGENT;
		menu.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT;
		Insert menu;
	}


	/**
	 * 自主点検レコード取得
	 *
	 */
	public static Map<String, SC_SelfCompliance__c> getSelfCompMap(ID officeid) {
		Map<String, SC_SelfCompliance__c> tmpMap = new Map<String, SC_SelfCompliance__c>();
		for (SC_SelfCompliance__c record : SC_SelfComplianceDao.getRecsByOfficeId(officeid)) {
			tmpMap.put(record.Kind__c, record);
		}
		return tmpMap;
	}

	/*
	 * 自主点検レコード更新
	 * 提出方法（PDF、Web）によって紐づきを分ける
	 */
	public static void updateSelfComp(SC_SelfCompliance__c sc,String typeSubmit){
		sc.TypeSubmit__c = typeSubmit;
		if(typeSubmit == 'PDF'){
			createAttachment(sc.Id);
		}else{
			createWebAnswer(sc);
		}
		update sc;
	}

	/*
	 * 自主点検レコード更新
	 * PDFで添付ファイルなし
	 */
	public static void updateSelfComp(SC_SelfCompliance__c sc){
		sc.TypeSubmit__c = 'PDF';
		update sc;

	}

	//テストデータ作成
	//添付ファイル
	static Attachment createAttachment(ID parentId){
		Attachment att = new Attachment();
		att.Name = 'Test.pdf';
		att.Body = Blob.valueOf('テスト');
		att.ParentId = parentId;
		insert att;
		return att;
	}

	//Web回答作成
	public static void createWebAnswer(SC_SelfCompliance__c sc){
		SC_WebAnswer__c ans = new SC_WebAnswer__c();
		ans.SC_SelfCompliance__c = sc.Id;
		ans.submitDate__c = '2018-04-27 10:00:00';
		insert ans;
	}

	//Web回答変更履歴作成
	public static void createWebAnswerHistories(SC_WebAnswer__c ans) {
		SC_WebAnswerHistory__c webAnsHistory = new SC_WebAnswerHistory__c();
		webAnsHistory.FieldName__c = '1';
		
		webAnsHistory.SC_WebAnswer__c = ans.Id;
	}

	//点検実施の手引きページ作成
	public static I_PageMaster__c createManualPage(){
		I_PageMaster__c page = new I_PageMaster__c();
		page.page_unique_key__c = SC_Const.SC_MANUAL_PREFIX + fiscalYear;
		page.Name = '点検実施の手引';
		insert page;
		return page;
	}

	//操作方法PDF関連オブジェクト作成
	public static Attachment createHelpCMS(){
		I_ContentMaster__c parentCMS = new I_ContentMaster__c();
		I_ContentMaster__c childCMS = new I_ContentMaster__c();
		parentCMS.Name = '代理店業務点検　操作方法の説明';
		insert parentCMS;
		childCMS.Name = fiscalYear + '年度代理店業務点検　操作方法の説明';
		childCMS.ParentContent__c = parentCMS.id;
		childCMS.ClickAction__c = 'DL(Attachment)';
		childCMS.filePath__c = 'Test.pdf';
		insert childCMS;
		Attachment att = createAttachment(childCMS.Id);
		return att;
	}
}