@isTest
private class TestI_DocumentRequest {

	@isTest static void test_stepToComplete01() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			List<I_ContentMaster__c> docs = new List<I_ContentMaster__c>();
			docs.add(createCMS('テスト資料１','パンフレット／特に重要なお知らせ','003'));
			docs.add(createCMS('テスト資料２','パンフレット／特に重要なお知らせ','001'));
			docs.add(createCMS('テスト資料３','パンフレット／特に重要なお知らせ','002'));
			docs.add(createCMS('テスト資料４','ご契約のしおり・約款','004'));
			insert docs;

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();
			controller.toErrPage();
			Boolean hasMsg = controller.hasInternalMessage;
			controller.stepToEnter();

			controller.form.agencyName	= 'テスト代理店';
			controller.form.agentName	= 'テスト募集人';
			controller.form.postalCode	= '111-2222';
			controller.form.addressMain	= 'テスト住所';
			controller.form.phoneNumber	= '090-3333-4444';

			controller.getCategories()[0].items[0].numberOfCopies = 1;
			controller.stepToConfirm();
			I_DocumentRequest.DocumentCategory category = controller.categoriesByName.get('パンフレット／特に重要なお知らせ');

			Test.startTest();
				controller.stepToComplete();
				controller.getCategoryMap();
				List<String> keys = controller.upsertKeys;
			Test.stopTest();

			System.assertEquals(TRUE, category.items[0].isMax);
			System.assertEquals('セクション|テスト資料２|', category.items[0].getUpsertKey());
			System.assertEquals('テスト資料２', category.items[0].getName());
			System.assertEquals('001', category.items[0].getFormNo());
			System.assertEquals(Date.today(), category.items[0].getDisplayFrom());
			System.assertEquals(true, category.hasItem);
			category.items[0].getNewValidTo();

			List<I_OrderInfo__c> result = [SELECT Id, (SELECT Id FROM OrderInfoI_OrderDetailss__r) FROM I_OrderInfo__c];
			System.assertEquals(1, result.size());
			System.assertEquals(1, result[0].OrderInfoI_OrderDetailss__r.size());
			System.assertEquals('COMPLETE', controller.getCurrentStatusName());
			System.assertEquals(true, controller.userIsMR);
		}
	}

	@isTest static void test_stepToComplete02() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			List<I_ContentMaster__c> docs = new List<I_ContentMaster__c>();
			for(Integer i=0; i<10; i++){
				docs.add(createCMS('パンフレット／特に重要なお知らせ'+i,'パンフレット／特に重要なお知らせ','00'+i));
			}
			insert docs;

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();

			controller.form.agencyName	= 'テスト代理店テスト代理店テスト代理店テスト代理店テスト代理店';
			controller.form.agentName	= 'テスト募集人テスト募集人テスト';
			controller.form.postalCode	= '1111-1111';
			controller.form.addressMain	= 'テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所';
			controller.form.phoneNumber	= '11111-11111-11111';
			for(Integer i=0; i<10; i++){
				controller.getCategories()[0].items[i].numberOfCopies = 1;
			}
			controller.stepToConfirm();

			System.assertEquals(ApexPages.getMessages().size(), 0);
			System.assertEquals('CONFIRM', controller.getCurrentStatusName());

			Test.startTest();
				controller.stepToComplete();
			Test.stopTest();

			E_Log__c result = [SELECT Id, Name, ActionType__c, SummaryName__c FROM E_Log__c ORDER BY CreatedDate DESC LIMIT 1];
			System.assertEquals(result.Name, I_Const.DOCUMENT_REQUEST);
			System.assertEquals(result.ActionType__c, I_Const.DOCUMENT_REQUEST);
			System.assertEquals(result.SummaryName__c.right(10), '＜以降の情報は省略＞');
		}
	}

	@isTest static void test_stepToConfirm01() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			insert createCMS('テスト資料１','パンフレット／特に重要なお知らせ','001');

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();
			controller.toErrPage();
			Boolean hasMsg = controller.hasInternalMessage;

			Test.startTest();
				//必須チェック
				controller.form.agencyName	= '';	//必須エラー
				controller.form.agentName	= '';
				controller.form.postalCode	= '';	//必須エラー
				controller.form.addressMain	= '';	//必須エラー
				controller.form.phoneNumber	= '';	//必須エラー
				controller.getCategories()[0].items[0].numberOfCopies = 0;	//必須エラー
				controller.stepToConfirm();
				System.assertEquals(ApexPages.getMessages().size(), 5);

			Test.stopTest();
			System.assertEquals('ENTER', controller.getCurrentStatusName());
		}
	}

	@isTest static void test_stepToConfirm02() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			insert createCMS('テスト資料１','パンフレット／特に重要なお知らせ','001');

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();
			controller.toErrPage();
			Boolean hasMsg = controller.hasInternalMessage;

			Test.startTest();
				//文字数超過
				controller.form.agencyName	= 'テスト代理店テスト代理店テスト代理店テスト代理店テスト代理店テ';	//文字数超過エラー(30)
				controller.form.agentName	= 'テスト募集人テスト募集人テスト募';	//文字数超過エラー(15)
				controller.form.postalCode	= '1111-11111';	//文字数超過エラー(9)
				controller.form.addressMain	= 'テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テ';	//文字数超過エラー(60)
				controller.form.phoneNumber	= '11111-11111-111111';	//文字数超過エラー(17)
				controller.getCategories()[0].items[0].numberOfCopies = 1;
				controller.stepToConfirm();
				System.assertEquals(ApexPages.getMessages().size(), 5);
			Test.stopTest();
			System.assertEquals('ENTER', controller.getCurrentStatusName());
		}
	}

	@isTest static void test_stepToConfirm03() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			insert createCMS('テスト資料１','パンフレット／特に重要なお知らせ','001');

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();
			controller.toErrPage();
			Boolean hasMsg = controller.hasInternalMessage;

			Test.startTest();
				//半角チェック
				controller.form.agencyName	= 'ﾃｽﾄ代理店';	//半角エラー
				controller.form.agentName	= 'ﾃｽﾄ募集人';	//半角エラー
				controller.form.postalCode	= '１１１ー２２２２';
				controller.form.addressMain	= 'ﾃｽﾄ住所';	//半角エラー
				controller.form.phoneNumber	= '０１ー２３４５ー６７８９';
				controller.getCategories()[0].items[0].numberOfCopies = 1;
				controller.stepToConfirm();
				System.assertEquals(ApexPages.getMessages().size(), 3);

			Test.stopTest();
			System.assertEquals('ENTER', controller.getCurrentStatusName());
		}
	}

	@isTest static void test_stepToConfirm04() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			insert createCMS('テスト資料１','パンフレット／特に重要なお知らせ','001');

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();
			controller.toErrPage();
			Boolean hasMsg = controller.hasInternalMessage;

			Test.startTest();
				//数字、ハイフンチェック
				controller.form.agencyName	= 'テスト代理店';
				controller.form.agentName	= 'テスト募集人';
				controller.form.postalCode	= '111-2222a';	//文字エラー
				controller.form.addressMain	= 'テスト住所';
				controller.form.phoneNumber	= '01-2345-6789a';	//文字エラー
				controller.getCategories()[0].items[0].numberOfCopies = 1;
				controller.stepToConfirm();
				System.assertEquals(ApexPages.getMessages().size(), 2);

			Test.stopTest();
			System.assertEquals('ENTER', controller.getCurrentStatusName());
		}
	}

	@isTest static void test_stepToConfirm05() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User adminUser = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(adminUser){
			insert createCMS('テスト資料１','パンフレット／特に重要なお知らせ','001');

			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);

			I_DocumentRequest controller = new I_DocumentRequest();
			controller.toErrPage();
			Boolean hasMsg = controller.hasInternalMessage;

			Test.startTest();
				//数字、ハイフンチェック
				controller.form.agencyName	= 'テスト代理店＆';	//特殊文字エラー
				controller.form.agentName	= 'テスト募集人＆';	//特殊文字エラー
				controller.form.postalCode	= '111-2222';
				controller.form.addressMain	= 'テスト住所＆';	//特殊文字エラー
				controller.form.phoneNumber	= '01-2345-6789';
				controller.getCategories()[0].items[0].numberOfCopies = 1;
				controller.stepToConfirm();
				System.assertEquals(ApexPages.getMessages().size(), 3);

			Test.stopTest();
			System.assertEquals('ENTER', controller.getCurrentStatusName());
		}
	}

	@isTest static void test_toErrPage01() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		Contact con = createContactData('SH', 'BK');
		User partner = createUser(con.Id);
		createIDManage(partner.Id);
		System.runAs(u){
			TestI_TestUtil.createAgencyPermissions(partner.Id);
			TestI_TestUtil.createAccountShare(con.AccountId, partner.Id);
		}
		System.runAs(partner){
			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);
			Test.startTest();
				I_DocumentRequest controller = new I_DocumentRequest();
				PageReference result = controller.toErrPage();
				controller.getInternalMessages();
			Test.stopTest();

			System.assert(String.valueOf(result).contains('e_errorpage'));
		}
	}

	@isTest static void test_refresh01() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		Contact con = createContactData('', '');
		Account agency = [select id from account where E_IsAgency__c = TRUE];
		update new Account(Id=agency.Id, Z1OFFING__c = 'Y');
		User partner = createUser(con.Id);
		createIDManage(partner.Id);
		System.runAs(u){
			TestI_TestUtil.createAgencyPermissions(partner.Id);
			TestI_TestUtil.createAccountShare(con.AccountId, partner.Id);
		}
		System.runAs(partner){
			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);
			Test.startTest();
				I_DocumentRequest controller = new I_DocumentRequest();
				controller.refresh();
			Test.stopTest();

			System.assertEquals(controller.form.agencyName, null);
			System.assertEquals(controller.form.agentName, UserInfo.getName());
		}
	}

	@isTest static void test_refresh02() {
		TestE_TestUtil.createMessageMaster_Messages(true);
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		Contact con = createContactData('', '');
		update new Account(
			Id=con.AccountId,
			E_CL2PF_ZEAYNAM__c = 'テスト株式会社テスト株式会社テスト株式会社テスト株式会社テスト',
			E_COMM_ZCLADDR__c = 'テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テ'
		);
		User partner = createUser(con.Id);
		createIDManage(partner.Id);
		System.runAs(u){
			TestI_TestUtil.createAgencyPermissions(partner.Id);
			TestI_TestUtil.createAccountShare(con.AccountId, partner.Id);
		}
		System.runAs(partner){
			PageReference pageRef = Page.IRIS_DocumentRequest;
			Test.setCurrentPage(pageRef);
			Test.startTest();
				I_DocumentRequest controller = new I_DocumentRequest();
				controller.refresh();
			Test.stopTest();

			System.assertEquals(controller.form.agencyName, 'テスト株式会社テスト株式会社テスト株式会社テスト株式会社テス');
			System.assertEquals(controller.form.addressMain, 'テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所テスト住所');
		}
	}

	//親取引先作成
	static Account createParentAccount(String channel, String source){
		Account acc = new Account(
			 Name = 'テスト株式会社'
			,E_CL1PF_KCHANNEL__c = channel
			,E_CL1PF_ZSOURCE__c = source
			,Optout_SelfRegist__c = false
		);
		insert acc;

		return acc;
	}

	//取引先作成
	static Id createAccount(String channel, String source){
		Account pac = createParentAccount(channel,source);
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.ZAUTOTRF__c = ' ';
		acc.ParentId = pac.Id;
		insert acc;

		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(String channel, String source){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount(channel,source);
			con.E_CL3PF_AGNTNUM__c = 'test01';
			con.E_CLTPF_DOB__c = '20161017';
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.E_CL3PF_VALIDFLAG__c = '1';
			con.AccountId = accid;
			con.E_CL3PF_KMOFCODE__c = 'test01';
			con.E_CLTPF_CLNTNUM__c = 'client01';
			con.E_CL3PF_CLNTNUM__c = 'client01';
			insert con;
		}
		return con;
	}

	//ID管理作成
	static void createIDManage(Id userId){
		E_IDCPF__c idcpf = new E_IDCPF__c(
			FLAG01__c = '1',
			FLAG02__c = '1',
			User__c = userId,
			ZSTATUS01__c = '1',
			ZSTATUS02__c = '1',
			ZINQUIRR__c = 'L3',
			ZIDTYPE__c  = 'AT',
			OwnerId = userId
		);
		insert idcpf;
	}

	//ユーザ作成
	static User createUser(Id conId){
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		User us = new User(
			 Username = 'test@test.com' + System.Label.E_USERNAME_SUFFIX
			,Alias = 'テスト花子'
			,Email = 'test@test.com'
			,EmailEncodingKey = 'UTF-8'
			,LanguageLocaleKey = 'ja'
			,LastName = 'テスト'
			,LocaleSidKey = 'ja_JP'
			,ProfileId = pro.Id
			,TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO
			,ContactId = conId
			,E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null)
			,IsActive = true
		);
		insert us;
		return us;
	}

	static I_ContentMaster__c createCMS(String cmsName, String category, String sortNo){
		I_ContentMaster__c component = new I_ContentMaster__c(
			 name = cmsName
			,ClickAction__c = 'DL(Attachment)'
			,DocumentCategory__c = category
			,DisplayFrom__c = Date.today()
			,DisplayOrder__c = sortNo
			,UpsertKey__c = sortNo
			,FormNo__c = sortNo
		);

		return component;
	}

}