/**
 * E_BizDataSyncLogTriggerHandler
 */
@isTest
private class TestE_BizDataSyncLogTriggerHandler {

	/**
	 * 区分：「4」
	 */
    static testMethod void kind_4_test01() {
        // Data
        E_BizDataSyncLog__c log = createRec(E_Const.EBIZ_ADD_GROUP_AGENCY);
        
        Test.startTest();
        insert log;
        Test.stopTest();
        
        // assertion
        List<E_BizDataSyncLog__c> results = [Select Id From E_BizDataSyncLog__c];
        System.assertEquals(4, results.size());
    }
    
	/**
	 * 区分：「E」
	 */
    static testMethod void kind_E_test01() {
        // Data
        E_BizDataSyncLog__c log = createRec(I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
        
        Test.startTest();
        insert log;
        Test.stopTest();
        
        // assertion
        List<E_BizDataSyncLog__c> results = [Select Id From E_BizDataSyncLog__c Where Kind__c = :I_Const.EBIZDATASYNC_KBN_NEWPOLICY];
        System.assertEquals(1, results.size());
    }
    
    /* TestData */
    private static E_BizDataSyncLog__c createRec(String kind){
    	E_BizDataSyncLog__c rec = new E_BizDataSyncLog__c();
    	rec.Kind__c = kind;
    	return rec;
    }
}