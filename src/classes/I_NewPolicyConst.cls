public with sharing class I_NewPolicyConst {
	//団体フラグ
	public static final String KFRAMEIND_Y = 'Y';

	//ステータス文言
	public static final String STATUS_NAME_I = '成立';
	public static final String STATUS_NAME_Z = '謝絶';
	public static final String STATUS_NAME_Q = '取下げ';
	public static final String STATUS_NAME_QZ = '謝絶/取下げ';
	public static final String STATUS_NAME_P1 = '申込書類到着済';
	public static final String STATUS_NAME_P2 = '不備対応中';
	public static final String STATUS_NAME_P3 = '査定完了入金待ち';
	public static final String STATUS_NAME_P4 = '査定中';
	
	//最終ステータス
	public static final Set<String> finalStatus = new Set<String>{STATUS_NAME_I, STATUS_NAME_Z, STATUS_NAME_Q};

	//不備ステータス値
	public static final String DEFECT_STATUS_OPEN = 'O';
	public static final String DEFECT_STATUS_FINISH = 'F';
	public static final String DEFECT_STATUS_DELETE = 'D';

	//対象不備コードなし時のコード値
	public static final String DEFECT_OTHER = 'OTH';
	
	//データ取得対象不備ステータス
	public static final Set<String> defStatus = new Set<String>{DEFECT_STATUS_OPEN, DEFECT_STATUS_FINISH};

	//未入金不備コード
	public static final String NOT_PAYMENT_CODE = 'W50';

	//ステータスソート用Map
	public static final Map<String,String> statusSortMap = new Map<String,String>{
										STATUS_NAME_P1    => '1' ,
//										''   => '2' ,
//										''   => '3' ,
										STATUS_NAME_P2 => '4' ,
										STATUS_NAME_P3 => '5' ,
										STATUS_NAME_P4   => '6' ,
										STATUS_NAME_I => '61' ,
										STATUS_NAME_Q => '62' ,
										STATUS_NAME_Z => '63' ,
										STATUS_NAME_QZ => '64' 
//										''      => '7'
									};
	//ステータス対応css用Map
	public static final Map<String,String> statusCssMap = new Map<String,String>{
											STATUS_NAME_I    => 'label_status_i' ,
											STATUS_NAME_Z   => 'label_status_z' ,
											STATUS_NAME_Q   => 'label_status_q' ,
											STATUS_NAME_QZ   => 'label_status_qz' ,
											STATUS_NAME_P1 => 'label_status_p1' ,
											STATUS_NAME_P2 => 'label_status_p2' ,
											STATUS_NAME_P3 => 'label_status_p3',
											STATUS_NAME_P4 => 'label_status_p4'
									};
}