/*
 * CC_SRActivityVOIBizLogicTest
 * Test class of CC_SRActivityVOIBizLogic
 * created  : Accenture 2018/7/9
 * modified :
 */

@isTest
private class CC_SRActivityVOIBizLogicTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create SRActivity
			List<CC_SRActivity__c> srActivityList = CC_TestDataFactory.createSRActivityList();
			insert srActivityList;

		}
	}

	/**
	* Test getInitData()
	*/
	static testMethod void getInitDataTest() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_SRActivityVOIBizLogic.getInitData(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test getPicklist()
	*/
	static testMethod void getPicklistTest() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('obj', 'CC_SRActivity__c');
			inputMap.put('item', 'CC_Status__c');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_SRActivityVOIBizLogic.getPicklist(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveSRActivity()
	* Worker is an user
	*/
	static testMethod void saveSRActivityTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//get SRActivity
			CC_SRActivity__c srActivity = [Select Id from CC_SRActivity__c limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', srActivity.Id);
			inputMap.put('[\'CC_ActivityType__c\']', 'testActivityMaster');
			inputMap.put('[\'CC_Status__c\']', '開始前');
			inputMap.put('[\'CC_Worker__c\']', 'testuser001/testuser');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_SRActivityVOIBizLogic.saveSRActivity(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveSRActivity()
	* Worker is a group
	*/
	static testMethod void saveSRActivityTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//get SRActivity
			CC_SRActivity__c srActivity = [Select Id from CC_SRActivity__c limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', srActivity.Id);
			inputMap.put('[\'CC_ActivityType__c\']', 'testActivityMaster');
			inputMap.put('[\'CC_Status__c\']', '開始前');
			inputMap.put('[\'CC_Worker__c\']', 'NNLinkForContents_Employee');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_SRActivityVOIBizLogic.saveSRActivity(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveSRActivity()
	* Worker has error
	*/
	static testMethod void saveSRActivityTest03() {
		System.runAs ( testUser ) {

			//Prepare test data
			//get SRActivity
			CC_SRActivity__c srActivity = [Select Id from CC_SRActivity__c limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', srActivity.Id);
			inputMap.put('[\'CC_ActivityType__c\']', 'testActivityMaster');
			inputMap.put('[\'CC_Status__c\']', '開始前');
			inputMap.put('[\'CC_Worker__c\']', 'errortest');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_SRActivityVOIBizLogic.saveSRActivity(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveSRActivity()
	* system error
	*/
	static testMethod void saveSRActivityTest04() {
		System.runAs ( testUser ) {

			//Prepare test data
			//get SRActivity
			CC_SRActivity__c srActivity = [Select Id from CC_SRActivity__c limit 1];

				//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', srActivity.Id);
			inputMap.put('[\'CC_ActivityType__c\']', ' ');
			inputMap.put('[\'CC_Status__c\']', Null);

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_SRActivityVOIBizLogic.saveSRActivity(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}