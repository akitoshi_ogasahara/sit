@isTest
private class TestI_NewPolicyDTO {
	/*
	**未入金通知
	*/
	@isTest static void I_NewPolicyDTO_test01(){
		Datetime d = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		E_NewPolicy__c plcy1 = TestUtil_AMS.createNewPolicy(true,'P','TEST001',d,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		E_NewPolicy__c plcy2= TestUtil_AMS.createNewPolicy(true,'P','TEST002',d,'','',200002,'契約者名2','ケイヤクシャメイ2','被保険者名2','ヒホケンシャメイ2',distribute.id,office.id,agent.id);
		E_DefectMaster__c def = createDefectMaster();
		List<I_NewPolicyDTO.NewPolicy> npdtos = new List<I_NewPolicyDTO.NewPolicy>();
		I_NewPolicyDTO.NewPolicy newpol1 = new I_NewPolicyDTO.NewPolicy(plcy1);
		newpol1.styleClass = 'testClass';
		newpol1.insured = '被保険者名1';
		newpol1.getJSTDateStr();
		newpol1.mr = 'MR1';
		newpol1.establishedAC = '2017/12';

		I_NewPolicyDTO.NewPolicy newpol2 = new I_NewPolicyDTO.NewPolicy(plcy2);
		npdtos.add(newpol1);
		npdtos.add(newpol2);
		List<I_NewPolicyDTO.Defect> dfdtos = new List<I_NewPolicyDTO.Defect>();
		I_NewPolicyDTO.Defect defect1 = new I_NewPolicyDTO.Defect(def);
		I_NewPolicyDTO.Defect defect2 = new I_NewPolicyDTO.Defect(def);
		dfdtos.add(defect1);
		dfdtos.add(defect2);


		I_NewPolicyDTO.sortIsAsc = true;
		/* 共通 */
		I_NewPolicyDTO.sortType = 'status';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'statusDate';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'owner';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'insured';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'GRUPKEY';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'cntType';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'insttot01';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'office';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'agent';
		npdtos.sort();
		I_NewPolicyDTO.sortType = 'code';
		dfdtos.sort();
		I_NewPolicyDTO.sortType = 'detail';
		dfdtos.sort();
		I_NewPolicyDTO.sortType = 'key';
		dfdtos.sort();
		I_NewPolicyDTO.sortType = 'CHDRNUM';
		npdtos.sort();

		System.assertEquals(npdtos[0].CHDRNUM,'TEST001');

	}


	private static E_DefectMaster__c createDefectMaster(){
		E_DefectMaster__c dm = new E_DefectMaster__c();
		dm.Code__c = 'AAA';
		dm.Value__c = 'テスト';
		dm.SortKey__c = 'sk';

		return dm;
	}
}