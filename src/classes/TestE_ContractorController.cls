@isTest
private with sharing class TestE_ContractorController{
	private static testMethod void testPageMethods() {		E_ContractorController extension = new E_ContractorController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testkoHihokenshaTab() {
		E_ContractorController.koHihokenshaTab koHihokenshaTab = new E_ContractorController.koHihokenshaTab(new List<E_CRLPF__c>(), new List<E_ContractorController.koHihokenshaTabItem>(), new List<E_CRLPF__c>(), null);
		koHihokenshaTab.create(new E_CRLPF__c());
		System.assert(true);
	}
	
	private static testMethod void testshitukenshaTab() {
		E_ContractorController.shitukenshaTab shitukenshaTab = new E_ContractorController.shitukenshaTab(new List<E_CRLPF__c>(), new List<E_ContractorController.shitukenshaTabItem>(), new List<E_CRLPF__c>(), null);
		shitukenshaTab.create(new E_CRLPF__c());
		System.assert(true);
	}
	
}