/*
 * CC_ContactVOIBizLogic
 * Business logic class for Contact
 * created  : Accenture 2018/6/18
 * modified :
 */

public with sharing class CC_ContactVOIBizLogic{

	/**
	 * Save agent
	 */
	public static Boolean saveAgent(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		Contact contactObj = new Contact();
		String strErrorMessage = '';
		contactObj.Id = (String)inputMap.get('targetId');
		System.debug(inputMap);

		if(inputMap.get('[\'EditableAgent\'][\'MobilePhone\']') != null) contactObj.MobilePhone = (String)inputMap.get('[\'EditableAgent\'][\'MobilePhone\']');											//携帯電話番号
		if(inputMap.get('[\'EditableAgent\'][\'OtherPhone\']') != null) contactObj.OtherPhone = (String)inputMap.get('[\'EditableAgent\'][\'OtherPhone\']');											//その他の電話番号
		if(inputMap.get('[\'EditableAgent\'][\'Email\']') != null) contactObj.Email = (String)inputMap.get('[\'EditableAgent\'][\'Email\']');															//メールアドレス
		if(inputMap.get('[\'EditableAgent\'][\'CC_AttachmentPassword__c\']') != null) contactObj.CC_AttachmentPassword__c = (String)inputMap.get('[\'EditableAgent\'][\'CC_AttachmentPassword__c\']');	//添付ファイルパスワード
		if(inputMap.get('[\'EditableAgent\'][\'Description\']') != null) contactObj.Description = (String)inputMap.get('[\'EditableAgent\'][\'Description\']');											//特記事項

		//システムエラーを取得
		if(strErrorMessage == ''){
			Database.SaveResult result = Database.update(contactObj, false);
			System.debug('getErrors' + result.getErrors());
			for(Database.Error err : result.getErrors()){
				String errorContent = err.getMessage();
				if(errorContent.contains('メールアドレス')){
					strErrorMessage += '無効なメールアドレスです。';
				}
				else if(errorContent.contains('添付ファイルパスワード')){
					strErrorMessage += '添付ファイルパスワードの長さが50桁を超過しました。';
				}
				else{
					strErrorMessage += errorContent;
				}

			}
			System.debug('strErrorMessage'+strErrorMessage);
		}

		if(strErrorMessage == ''){
			Map<String, Object> latestObjectValueMap = new Map<String, Object>();
			for(Contact latestObj : [SELECT Id, SystemModstamp, MobilePhone, OtherPhone, Email, CC_AttachmentPassword__c, Description, CC_LastUpdateDateForEditableField__c FROM Contact WHERE Id = :contactObj.Id]){
				Map<String, Object> fieldValues = latestObj.getPopulatedFieldsAsMap();
				for (String fieldName : fieldValues.keySet()) {
					if(fieldName == 'SystemModstamp'){
						latestObjectValueMap.put(fieldName, ((Datetime)fieldValues.get(fieldName)).getTime());
					}else{
						latestObjectValueMap.put(fieldName, fieldValues.get(fieldName));
					}
				}
			}

			//項目をブランクにするとき画面上でブランクを即時に表示ちょっこ
			if(latestObjectValueMap.size() != 0){
				if(latestObjectValueMap.get('MobilePhone') == null){
					latestObjectValueMap.put('MobilePhone', '');
				}
				if(latestObjectValueMap.get('OtherPhone') == null){
					latestObjectValueMap.put('OtherPhone', '');
				}
				if(latestObjectValueMap.get('Email') == null){
					latestObjectValueMap.put('Email', '');
				}
				if(latestObjectValueMap.get('CC_AttachmentPassword__c') == null){
					latestObjectValueMap.put('CC_AttachmentPassword__c', '');
				}
				if(latestObjectValueMap.get('Description') == null){
					latestObjectValueMap.put('Description', '');
				}
				outputMap.put('latestObjectValue', (Object)latestObjectValueMap);
				System.debug('latestObjectValue'+latestObjectValueMap);
			}
		}else{
			outputMap.put('inputErrorMessage', strErrorMessage);
			success = false;
		}

		return success;
	}

	/**
	 * Save client
	 */
	public static Boolean saveClient(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		Contact contactObj = new Contact();
		contactObj.Id = (String)inputMap.get('targetId');
		System.debug(inputMap);

		if(inputMap.get('[\'Contact\'][\'Description\']') != null) contactObj.Description = (String)inputMap.get('[\'Contact\'][\'Description\']');		//特記事項

		Database.SaveResult result = Database.update(contactObj, false);

		Map<String, Object> latestObjectValueMap = new Map<String, Object>();

		for(Contact latestObj : [SELECT Id, SystemModstamp, Description FROM Contact WHERE Id = :contactObj.Id]){
			Map<String, Object> fieldValues = latestObj.getPopulatedFieldsAsMap();
			for (String fieldName : fieldValues.keySet()) {
				if(fieldName == 'SystemModstamp'){
					latestObjectValueMap.put(fieldName, ((Datetime)fieldValues.get(fieldName)).getTime());
				}else{
					latestObjectValueMap.put(fieldName, fieldValues.get(fieldName));
				}
			}
		}

		//項目をブランクにするとき画面上でブランクを即時に表示ちょっこ
		if(latestObjectValueMap.size() != 0){
			if(latestObjectValueMap.get('Description') == null){
				latestObjectValueMap.put('Description', '');
			}
			outputMap.put('latestObjectValue', (Object)latestObjectValueMap);
			System.debug('latestObjectValue'+latestObjectValueMap);
		}

		return success;
	}

}