@isTest
private class TestI_TamotsukunIntroductionController{
	static String uniquekey = 'iris_tamotsukun';
	static List<String> tamotsuCompList = new List<String>{'たもつくんキャプチャ','たもつくんダウンロード'};

	//正常なたもつくん紹介ページ作成
	@isTest static void testCase1() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createTamotsuPage();
		createTamotsuCMS(ipm,true);

		System.runAs(actUser){
			//update us;
			PageReference pageRef = Page.IRIS_TamotsukunIntroduction;
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_TamotsukunIntroductionController cms = new I_TamotsukunIntroductionController();
			PageReference pageRefs = cms.pageAction();

			//各セクションが存在する
			System.assertNotEquals(cms.getCaputureSection(),null);
			System.assertNotEquals(cms.getdownLoadSection(),null);

			Test.stopTest();
		}
	}

	//異常ケース：該当のCMSコンテンツが存在しない
	@isTest static void testCase2() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createTamotsuPage();

		System.runAs(actUser){
			//update us;
			PageReference pageRef = Page.IRIS_TamotsukunIntroduction;
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_TamotsukunIntroductionController cms = new I_TamotsukunIntroductionController();
			PageReference pageRefs = cms.pageAction();

			//各セクションが存在しない
			System.assertEquals(cms.getCaputureSection(),null);
			System.assertEquals(cms.getdownLoadSection(),null);

			Test.stopTest();
		}
	}

	//ID管理
	static void createIDManage(User us){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.User__c = us.Id;
		ic.AppMode__c = 'IRIS';
		ic.ZINQUIRR__c = 'BR00';
		insert ic;
	}

	//irisPage
	static I_PageMaster__c createTamotsuPage(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = uniquekey;
		insert pm;
		return pm;
	}

	//irisCMS
	static List<I_ContentMaster__c> createTamotsuCMS(I_PageMaster__c ipm,Boolean create){
		List<I_ContentMaster__c> cmsList = new List<I_ContentMaster__c>();
			for( String cmpName : tamotsuCompList ){
			I_ContentMaster__c icm = new I_ContentMaster__c();
			icm.Page__c = ipm.Id;
			icm.Name = cmpName;
			system.debug( icm );
			cmsList.add( icm );
		}
			insert cmsList;
			system.debug(cmsList);
			return cmsList;
	}

}