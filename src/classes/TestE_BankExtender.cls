@isTest(SeeAllData=false)
private class TestE_BankExtender{
     private static testMethod void testE_bank1() {
        User u = TestE_TestUtil.createUser(true, 'Ye', 'システム管理者');
        system.debug('$$$userId=' + u.usertype);
        
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        idcpf.FLAG01__c = '1';
        insert idcpf;
        
        Account acc = TestE_TestUtil.createAccount(true);
        system.debug('$$$accId= ' + acc.id);
        Contact Con = TestE_TestUtil.createContact(true, acc.id, 'ye', '56781234','bo123');
        //system.debug('$$$ConId= ' + con.recordtypeId);
        
        E_Policy__c policy = TestE_TestUtil.createPolicy(true, Con.id,'ECHDPF','syokenba');
        system.debug('$$$policyId= ' + policy.id);
        
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            //テスト開始
            Test.startTest();     
            
            PageReference pref = Page.E_Bank;
            system.debug('$$$pref= ' + pref);
            String uId = String.valueOf(policy.id);
            system.debug('$$$uId= ' + uId);
            pref.getParameters().put('id',uId);
            system.debug('$$$pref.getParameters()= ' + pref.getParameters().get('id'));
                 
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_BankController controller = new E_BankController(standardcontroller);
            E_BankExtender extender = controller.getExtender();
            
            system.debug('$$$currentPage1= '+ ApexPages.CurrentPage().getUrl());
            system.debug('$$$controller= '+ controller);
            system.debug('$$$extender= '+ extender);
            
            extender.init();
            extender.PageAction();
            
            PageReference refPage = extender.PageAction();
            system.debug('$$$currentPage2= '+ ApexPages.CurrentPage().getUrl());
            system.debug('$$$resultPage= '+ resultPage);
            system.debug('$$$refPage= '+ refPage);
            
            
            // 確認
            System.assertEquals(policy.id,pref.getParameters().get('id'));
            system.assertEquals(null, refPage);
            
            //テスト終了
            Test.stopTest();
            
        }
        
        
        //※正常処理ならこれを確認↓
/*      system.assertEquals(null, resultPage);
        //※例外処理(エラーページへ飛ぶ場合)ならこれを確認↓
        system.assertEquals('/apex/E_ErrorPage', resultPage.getUrl());
*/      
    }
}