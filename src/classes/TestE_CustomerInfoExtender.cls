@isTest
private class TestE_CustomerInfoExtender {

	//初期表示確認：契約者以外のユーザ(正常:COLI有り)
	static testMethod void checkInitialdisplay_1(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_COLI, 'xxxxxxxx');
		insert policy;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		E_COVPF__c covpf1 = new E_COVPF__c(
			E_Policy__c = policy.Id
			, COLI_ZCRIND__c = 'C' // 契約フラグ
			, COLI_ZPREMADD__c = true // 加算フラグ
			, COLI_INSTPREM__c = 100 //保険料
			, COMM_CRTABLE__c = 'MZ' //
		);
		insert covpf1;
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();
			extender.init();

			resultPage = extender.pageAction();
			system.debug('resultPage' + resultPage);

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
	}

	//初期表示確認：契約者以外のユーザ(正常:SPVA有り)
	static testMethod void checkInitialdisplay_2(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		E_Policy__c p = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxxx');
		p.SPVA_ZANNFROM__c = '20000101';
		p.SPVA_ZVWDCF__c = '2';
		insert p;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
	}

	//初期表示確認：契約者以外のユーザ(正常:ANNUITY有り)
	static testMethod void checkInitialdisplay_3(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		E_Policy__c p = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxxx');
		p.SPVA_ZANNFROM__c = '20000101';
		p.SPVA_ZVWDCF__c = '0';
		p.SPVA_ZANNSTFLG__c = true;
		insert p;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
	}

	//初期表示確認：契約者以外のユーザ(正常:投資信託有り)
	static testMethod void checkInitialdisplay_4(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		TestE_TestUtil.createIthpf(true, con.Id, 'xxxxxxx');
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}
		system.assertEquals(null, resultPage);
	}

	//初期表示確認：契約者以外のユーザ(異常：子レコードなし)
	static testMethod void checkInitialdisplay_err1(){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}
		system.assertEquals('/apex/e_errorpage?code=' + EncodingUtil.urlEncode(E_Const.ERROR_MSG_PARAMETER, 'UTF-8'), resultPage.getUrl());
	}

	//URLパラメータ -- disIDなどに値がある場合
	static testMethod void checkURLParam1(){
		Account parent = TestE_TestUtil.createAccount(true);
		Account acc = TestI_TestUtil.createAccount(true,parent);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		E_Policy__c p = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxxx');
		p.SPVA_ZANNFROM__c = '20000101';
		p.SPVA_ZVWDCF__c = '0';
		p.SPVA_ZANNSTFLG__c = true;
		p.MainAgent__c = con.Id;
		p.MainAgentAccount__c = acc.Id;
		insert p;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			pref.getParameters().put('disId', parent.Id);
			pref.getParameters().put('offId', acc.Id);
			pref.getParameters().put('ageId', con.Id);
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}

		system.assertEquals(null, resultPage);
	}

	//cno、cnameと契約者情報が異なる場合
	static testMethod void checkURLParam2(){
		Account parent = TestE_TestUtil.createAccount(true);
		Account acc = TestI_TestUtil.createAccount(true,parent);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');

		Contact ann = TestE_TestUtil.createContact(true, acc.Id, 'annuitanto', '11111111', '');
		E_Policy__c p = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxxx');
		p.SPVA_ZANNFROM__c = '20000101';
		p.SPVA_ZVWDCF__c = '0';
		p.SPVA_ZANNSTFLG__c = true;
		p.Annuitant__c = ann.Id;
		insert p;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', ann.Id);
			pref.getParameters().put('cno', ann.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'annuitanto');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}

		system.assertEquals(null, resultPage);
	}

	//checkURLParam1,2以外の場合
	static testMethod void checkURLParamError(){
		Account parent = TestE_TestUtil.createAccount(true);
		Account acc = TestI_TestUtil.createAccount(true,parent);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');

		Contact ann = TestE_TestUtil.createContact(true, acc.Id, 'annuitanto', '11111111', '');
		E_Policy__c p = TestE_TestUtil.createPolicy(false, con.Id, E_Const.POLICY_RECORDTYPE_SPVA, 'xxxxxxxx');
		p.SPVA_ZANNFROM__c = '20000101';
		p.SPVA_ZVWDCF__c = '0';
		p.SPVA_ZANNSTFLG__c = true;
		p.Annuitant__c = ann.Id;
		insert p;
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', ann.Id);
			pref.getParameters().put('cno', ann.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.pageAction();

			Test.stopTest();
		}

		system.assertEquals('/apex/e_errorpage?code=' + EncodingUtil.urlEncode(E_Const.ERROR_MSG_PARAMETER, 'UTF-8'), resultPage.getUrl());
	}

	//toContactStandardPage()のテスト
	static testMethod void checkToContactStandardPageTest(){

		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestI_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.toContactStandardPage();

			Test.stopTest();
		}

		String url = con.Id;
		//　/***************のID部分15文字と、18桁のIDの先頭15文字を比べる
		System.assertEquals(resultPage.getUrl().substring(1,16),url.substring(0, 15));
	}

	static testMethod void checkTodoReturnTest(){

		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestI_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		PageReference resultPage;

		System.runAs(u){
			Test.startTest();

			PageReference pref = Page.E_CustomerInfo;
			pref.getParameters().put('cid', con.Id);
			pref.getParameters().put('cno', con.E_CLTPF_CLNTNUM__c);
			pref.getParameters().put('cname', 'testsan');
			Test.setCurrentPage(pref);

			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(con);
			E_CustomerInfoController controller = new E_CustomerInfoController(standardcontroller);
			E_CustomerInfoExtender extender = controller.getExtender();

			resultPage = extender.doReturn();

			Test.stopTest();
		}

		//遷移元画面がないため空
		System.assertEquals(resultPage.getUrl(),'');
	}


}