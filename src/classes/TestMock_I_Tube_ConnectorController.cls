@isTest
private class TestMock_I_Tube_ConnectorController {

	static testMethod void myUnitTest01() {
		Mock_I_Tube_ConnectorController ctrl = new Mock_I_Tube_ConnectorController();
		
		Test.startTest();
		
		ctrl.getMetaUrl();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest02() {
		Mock_I_Tube_ConnectorController ctrl = new Mock_I_Tube_ConnectorController();
		
		Test.startTest();
		
		ctrl.getSsoUrl();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest03() {
		Mock_I_Tube_ConnectorController ctrl = new Mock_I_Tube_ConnectorController();
		
		Test.startTest();
		
		ctrl.getUserId();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest04() {
		Mock_I_Tube_ConnectorController ctrl = new Mock_I_Tube_ConnectorController();
		
		Test.startTest();
		
		ctrl.getSfDomain();
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest05() {
		Mock_I_Tube_ConnectorController ctrl = new Mock_I_Tube_ConnectorController();
		
		Test.startTest();
		
		ctrl.getReqType();
		
		Test.stopTest();
	}
	
}