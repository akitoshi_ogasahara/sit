global class I_UpdateLogBatchSched implements Schedulable {
	global void execute(SchedulableContext sc){
		Database.executeBatch(new I_UpdateLogBatch());
		Database.executeBatch(new I_LogNNTubeBatch());
	}
}