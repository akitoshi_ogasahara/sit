public with sharing class SC_SelfComplianceDao {
	
	/**
	 * 自主点検事務所IDをキーに、自主点検に紐づく添付ファイル情報を取得
	 * @param String 自主点検事務所ID
	 * @return List<SC_SelfCompliance__c>
	 */
	public static List<SC_SelfCompliance__c> getAttachmentsByOfficeId(String officeId){
		return [SELECT 
					Id, Name, Type__c, LastModifiedDate, SC_Office__c, TypeShort__c
					,(SELECT Id, Name, LastmodifiedDate, LastmodifiedById, LastmodifiedBy.FirstName, LastmodifiedBy.LastName FROM Attachments)
				FROM 
					SC_SelfCompliance__c
				WHERE 	
					SC_Office__c = :officeId
				ORDER BY 
					TypeShort__c, 
					LastModifiedDate DESC];
	}


	public static SC_SelfCompliance__c getRecById(String recordId){
		return [SELECT
					Id,
					Name,
					OtherTypeName__c,
					MRUnitName__c,
					ZAGCYNUM__c,
					ZEAYNAM__c,
					SC_Office__c,
					Type__c,
					TypeShort__c,
					FiscalYear__c,
					LimeSurveyToken__c,
					LimeSurveyId__c,
					Kind__c
				FROM
					SC_SelfCompliance__c
				WHERE
					Id = :recordId
				];
	}

	public static List<SC_SelfCompliance__c> getRecsByOfficeId(String officeId){
		return [SELECT
					Id,
					Name,
					OtherTypeName__c,
					MRUnitName__c,
					ZAGCYNUM__c,
					ZEAYNAM__c,
					SC_Office__c,
					Type__c,
					TypeShort__c,
					FiscalYear__c,
					LimeSurveyToken__c,
					LimeSurveyId__c,
					Kind__c,
					TypeSubmit__c,
					holdCount__c,
					// 自主点検Web回答
					(SELECT Id, Name, LastmodifiedDate, LastmodifiedById, LastmodifiedBy.FirstName, LastmodifiedBy.LastName, LastmodifiedBy__c, submitDate__c FROM SC_WebAnswers__r),
					// 添付ファイル
					(SELECT Id, Name, LastmodifiedDate, LastmodifiedById, LastmodifiedBy.FirstName, LastmodifiedBy.LastName FROM Attachments)
				FROM
					SC_SelfCompliance__c
				WHERE
					SC_Office__c = :officeId
				];
	}
}