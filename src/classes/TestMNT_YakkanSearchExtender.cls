@isTest
private class TestMNT_YakkanSearchExtender{
	@isTest static void agencyCodeList_Test001() {
		PageReference pr = Page.WY_WebYakkanSearch;
		Test.setCurrentPage(pr);
		MNT_YakkanSearchController controller = new MNT_YakkanSearchController(new ApexPages.StandardController(new I_ContentMaster__c()));
		MNT_YakkanSearchExtender extender = new MNT_YakkanSearchExtender(controller);
		List<String> agcList = new List<String>();

		Test.startTest();
			agcList = extender.agencyCodeList;
		Test.stopTest();

		System.assertEquals('汎用', agcList[0]);
	}
}