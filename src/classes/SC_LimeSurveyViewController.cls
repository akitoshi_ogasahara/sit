public with sharing class SC_LimeSurveyViewController {

	public String action{get; private set;}
	public String loginHost{get; private set;}
	public Boolean isError{get; private set;}

	public SC_LimeSurveyViewController() {
		String token = ApexPages.CurrentPage().getParameters().get('token');
		String surveyId = ApexPages.CurrentPage().getParameters().get('surveyid');

		isError = false;
		if (String.isEmpty(token)) {
			isError = true;
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'tokenがありません。エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE001】'));
		}
		if (String.isEmpty(surveyId)) {
			isError = true;
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'survey idがありません。エヌエヌ生命コンプライアンス管理部フリーダイヤル（0120-521-474）までお問い合わせください。【SCE002】'));
		}

		// POST action URL
		action = system.Label.SC_LimeSurveyURL + surveyId + '?token=' + token;

		User us = [select id, contactId from User where id = :UserInfo.getUserId()];
		Organization org = [select Id, IsSandbox from Organization limit 1];

		if (us.contactId == null && !org.IsSandbox) {
			loginHost = 'https://nnlife-jp.my.salesforce.com';

		} else if (us.contactId == null && org.IsSandbox) {
			loginHost = URL.getSalesforceBaseUrl().toExternalForm().replace('--c','').replace('visual.force', 'my.salesforce');

		} else {
			loginHost = URL.getSalesforceBaseUrl().toExternalForm() + site.getPathPrefix();
		}

		//if (us.contactId == null) {
		//	Integer len = system.Label.E_DOMAIN_EMPLOYEE.indexOf('.force.com/') + 10;
		//	loginHost = system.Label.E_DOMAIN_EMPLOYEE.left(len);
		//} else {
		//	loginHost = URL.getSalesforceBaseUrl().toExternalForm() + site.getPathPrefix();
		//}
	}
}