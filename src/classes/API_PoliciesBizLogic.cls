public with sharing class API_PoliciesBizLogic {
	//レスポンス用内部クラス
	public class policies extends API_Listener.responseBody{
		public String clientno {get;set;}	//顧客番号
		public String ownerName {get;set;}	//契約者名
		public Decimal zcsbnamt {get;set;}	//解約時受取金額
		public Decimal premiumM {get;set;}	//月払払込保険料
		public Decimal premiumY {get;set;}	//年払払込保険料
		public Decimal premiumHY {get;set;}	//半年払込保険料

		//コンストラクタ
		public policies(){
			this.clientno = '';
			this.ownerName = '';
			this.zcsbnamt = 0;
			this.premiumM = 0;
			this.premiumY = 0;
			this.premiumHY = 0;
		}
	}

	//コンストラクタ
	public void API_PoliciesBizLogic(){

	}

	//契約情報作成メソッド
	public policies getPolicies(Id conId){
		policies response = new policies();
		try{
			//契約者の保険契約と、それに紐づく個人保険特約を取得
			String cRecType = E_Const.POLICY_RECORDTYPE_COLI;
			String covRecType = E_Const.COVPF_RECORDTYPE_ECOVPF;
			Set<Id> polIds = new Set<Id>();
			List<E_Policy__c> polList = [SELECT
											id,
											ContractorCLTPF_CLNTNUM__c,	//契約者顧客番号（数式）
											ContractorName__c,	//契約者氏名（数式）
											COLI_BILLFREQ__c,	//払込方法（回数）コード
											COMM_SINSTAMT__c,	//1回あたりの保険料
											COLI_ZCSHVAL__c,	//解約返戻金
											COLI_ZEAPLTOT__c,	//保険料振替貸付合計
											COLI_ZEPLTOT__c,	//契約者貸付合計
											COLI_ZEADVPRM__c,	//前納未経過保険料
											COLI_ZUNPREM__c,	//未経過保険料
											COLI_ZCVDCF__c,		//解約返戻金計算不能フラグ
											COLI_ZUNPCF__c,		//未経過保険料計算不能フラグ
											COLI_ZUNPDCF__c,	//未経過保険料表示フラグ
											COMM_STATCODE__c	//現在の状況コード
										FROM E_Policy__c
										WHERE Contractor__c =: conId	//契約者
										AND RecordType.DeveloperName =: cRecType	//レコードタイプ
										AND Id IN (SELECT
														E_Policy__c
													FROM
														E_COVPF__c
													WHERE
														COLI_ZCRIND__c = 'C'	//主契約フラグ
													AND RecordType.DeveloperName =: covRecType	//個人保険特約レコードタイプ)
													)
										];

			//契約者の保険契約があるかチェック
			if(polList.size() == 0){
				return response;
			}else{
				//契約者情報
				response.clientno = polList[0].ContractorCLTPF_CLNTNUM__c;
				response.ownerName = polList[0].ContractorName__c;
			}
			//複数証券の解約時受取金額、払込保険料の合計をそれぞれ求める
			for(E_Policy__c pol : polList){
				Decimal surrenderValue = I_PolicyUtil.getSurrenderValueSum(pol);
				//現在の状況コードにより加算判定
				switch on pol.COMM_STATCODE__c{
					//解約時受取金額、払込保険料両方不要
					when 'B','M','X' {
					}
					//解約時受取金額、払込保険料両方
					when 'I','Y' {
						response.zcsbnamt += surrenderValue;
						
						if(pol.COMM_SINSTAMT__c != null){
							//払込方法によって加算方法を変更
							if(pol.COLI_BILLFREQ__c == '12'){	//月払
								response.premiumM += pol.COMM_SINSTAMT__c;
							}else if(pol.COLI_BILLFREQ__c == '02'){	//半年払
								response.premiumHY += pol.COMM_SINSTAMT__c;
							}else if(pol.COLI_BILLFREQ__c == '01'){	//年払
								response.premiumY += pol.COMM_SINSTAMT__c;
							}else{
								throw new API_Listener.RestAPIException('COLI_DATA_CORRUPTION');
							}
						}
					}
					//解約時受取金額のみ
					when 'L','N' {
						response.zcsbnamt += surrenderValue;
					}
					//それ以外の時、何もしない
					when else{
					}
				}
			}

			return response;
		}catch(Exception e){
			return null;
		}
		
	}
}