@isTest(SeeAllData=false)
public with sharing class TestE_DownloadPolicyHistorySearchExt{

    private static User testuser;
    private static User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    private static E_IDCPF__c idcpf;
    private static Account account;
    private static E_ASPPF__c ASPPF;
    private static string  ZDSPFLAG01 = '1';
    private static Account kakuAcc;
	private static List<Account> offAccList;
    private static String year = system.now().format('yyyy/MM/01').split('/')[0];
    private static String month = system.now().format('yyyy/MM/dd').split('/')[1];
    
    static void init(){
        insertMessage();
        createDataCommon();
    }

    private static testMethod void normalTest() {
        init();
        system.runAs(testuser){
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceSearch;
            testpage.getParameters().put('did', kakuAcc.ID);
            Test.setCurrentPage(testpage);
            E_DownloadController controller = new E_DownloadController();            
            E_DownloadPolicyHistorySearchExtension extender = new E_DownloadPolicyHistorySearchExtension(controller);
            pagereference pref = extender.pageAction();
            system.assertequals(null,pref);            
            extender.selectedPeriodFrom = year + '/' + month;
            extender.selectedPeriodTo = year + '/' + month;
            system.debug(year + '/' + month);
            pref = extender.doDownloadCsv();
//            system.assert(pref.getURL().contains('opro'));
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
        }
    }    
    
    //全期間
    private static testMethod void allPeriodTest() {
        init();
        system.runAs(testuser){
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceSearch;
            testpage.getParameters().put('did', kakuAcc.ID);
            Test.setCurrentPage(testpage);
            E_DownloadController controller = new E_DownloadController();            
            E_DownloadPolicyHistorySearchExtension extender = new E_DownloadPolicyHistorySearchExtension(controller);
            pagereference pref = extender.pageAction();
            system.assertequals(null,pref);            
            extender.selectedPeriodFrom = year + '/' + month;
            extender.selectedPeriodTo = year + '/' + month;
            system.debug(year + '/' + month);
            extender.isAllPeriod = true;
            pref = extender.doDownloadCsv();
//            system.assert(pref.getURL().contains('opro'));
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
        }
    }    

    //oidパラメータを使った正常系
    private static testMethod void normalOidTest() {
        init();
        system.runAs(testuser){
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceSearch;
            testpage.getParameters().put('oid', offAccList[0].ID);
            Test.setCurrentPage(testpage);
            E_DownloadController controller = new E_DownloadController();            
            E_DownloadPolicyHistorySearchExtension extender = new E_DownloadPolicyHistorySearchExtension(controller);
            pagereference pref = extender.pageAction();
            system.assertequals(null,pref);            
            extender.selectedPeriodFrom = year + '/' + month;
            extender.selectedPeriodTo = year + '/' + month;
            system.debug(year + '/' + month);
            pref = extender.doDownloadCsv();
//            system.assert(pref.getURL().contains('opro'));
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
        }
    }    
    

    //aidパラメータを使った正常系
    private static testMethod void normalAidTest() {
        init();
        contact inscon = new Contact();
        system.runAs(thisUser){
            inscon = new contact(lastname='test',accountID=kakuAcc.ID, E_CL3PF_AGNTNUM__c = 'agnum');
            insert inscon;
        }
        system.runAs(testuser){
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceSearch;
            testpage.getParameters().put('aid', inscon.ID);
            Test.setCurrentPage(testpage);
            E_DownloadController controller = new E_DownloadController();            
            E_DownloadPolicyHistorySearchExtension extender = new E_DownloadPolicyHistorySearchExtension(controller);
            pagereference pref = extender.pageAction();
            system.assertequals(null,pref);            
            extender.selectedPeriodFrom = year + '/' + month;
            extender.selectedPeriodTo = year + '/' + month;
            system.debug(year + '/' + month);
            pref = extender.doDownloadCsv();
//            system.assert(pref.getURL().contains('opro'));
	    	// doDownloadCsv assertion
	    	E_DownloadHistorry__c dh = [Select Id From E_DownloadHistorry__c Where Id = :controller.dh.Id];
	    	system.assert(dh != null);
        }
    }    
    
	//日付入力値の検証結果エラー
    private static testMethod void errorValidateDateTest() {
        init();
        system.runAs(testuser){
            E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceSearch;
            testpage.getParameters().put('did', kakuAcc.ID);
            Test.setCurrentPage(testpage);
            E_DownloadController controller = new E_DownloadController();            
            E_DownloadPolicyHistorySearchExtension extender = new E_DownloadPolicyHistorySearchExtension(controller);
            extender.selectedPeriodFrom = '1234/56';
            extender.selectedPeriodTo = '1234/56';
            extender.doDownloadCsv();
            system.assert(controller.pageMessages.hasMessages());
        }
    }    
    
    private static void insertMessage(){
        system.runAs(thisUser){
            LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
            String type = 'メッセージ';
            String param = 'ERR|003';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'DFG|003';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            insert insMessage;
        }
    }
    
    /* test data */
    static void createDataCommon(){
        system.runAs(thisUser){
            
            // User
            profile	profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
            testuser = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
                , CommunityNickName='tuser1'
            );
            insert testuser;
            // Account
            account = new Account(Name = 'testAccount',ownerid = testuser.id);
            insert account;

            //
            idcpf = new E_IDCPF__c(
                FLAG01__c = '1',
                FLAG02__c = '1',
                ZDSPFLAG01__c = ZDSPFLAG01,
                FLAG06__c = '1',
                //TRCDE01__c = 'TD06',
                //TRCDE02__c = 'TD06',
                User__c = testuser.Id,
                ZSTATUS02__c = '1',
                ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
            );
            insert idcpf;
            ASPPF = new E_ASPPF__c(Account__c = account.id);
            insert ASPPF;
            
            // Account 代理店格
            kakuAcc = new Account(Name = 'kakuAccount');
            insert kakuAcc;
            
            offAccList = new List<Account>();
            for(Integer i = 1; i < 4; i++){
                Account acc = new Account(
                    Name = 'office' + i
                    ,ParentId = kakuAcc.Id
                );
                offAccList.add(acc);
            }
            insert offAccList;
            
            decimal decimalYear = decimal.valueOf(year);
            decimal decimalmonth = decimal.valueOf(month);
            
            LIST<E_ASPPF__c> kakuOffASPPFList = new LIST<E_ASPPF__c>();
            kakuOffASPPFList.add(new E_ASPPF__c(Account__c = kakuAcc.Id,ACCTYR__c = decimalYear,ACCTMN__c = decimalmonth));
            E_ASPPF__c kakuOffASPPFItem;
            for(Account item:offAccList){
                kakuOffASPPFItem = new E_ASPPF__c(Account__c = item.Id,ACCTYR__c = decimalYear,ACCTMN__c = decimalmonth);
                kakuOffASPPFList.add(kakuOffASPPFItem);
            }
            insert kakuOffASPPFList;
            
            LIST<contact> insConList = new LIST<contact>();
            for(integer i = 0; i < kakuOffASPPFList.size(); i++){
            	E_ASPPF__c item = kakuOffASPPFList[i];
                insConList.add(new contact(lastname='test',accountID=item.account__c, E_CL3PF_AGNTNUM__c = i.format().leftpad(5,'0')));
            }
            insert insConList;
                        
            LIST<E_policy__c> insPolicyList = new LIST<E_Policy__c>();
            for(contact item:insConList){
                insPolicyList.add(new E_Policy__c(MainAgent__c = item.id));
            }
            insert insPolicyList;
            
            LIST<E_PTNPF__c> insPTNPFList = new LIST<E_PTNPF__c>();
            for(E_policy__c item:insPolicyList){
                insPTNPFList.add(new E_PTNPF__c(E_Policy__c = item.id,TRANDATE__c=system.now().format('yyyyMMdd')));
            }
            insert insPTNPFList;
            
        }        
    }
    
}