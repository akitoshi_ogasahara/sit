/**
 * 代理店提出レコード（外部委託、管理規定）作成、Sharingのビジネスロジッククラス
 */
public with sharing class CR_BizLogicBatchOperation extends E_AbstractCommonBatchOperation {
	
	//代理店提出オブジェクト登録用
	private List<CR_Agency__c> insRecs = new List<CR_Agency__c>();
	//代理店提出オブジェクト更新用
	private Map<Id, CR_Agency__c> updRecs = new Map<Id, CR_Agency__c>();
	//代理店提出オブジェクト存在する場合用
	private Map<Id, CR_Agency__c> updShareRecs = new Map<Id, CR_Agency__c>();
	//代理店提出オブジェクトShare登録用
	private List<CR_Agency__Share> insShare = new List<CR_Agency__Share>();
	//代理店提出オブジェクトShare登録用削除用
	private List<CR_Agency__Share> delShare = new List<CR_Agency__Share>();
	//key:AccountId, value:Account
	private Map<Id, Account> accMap = new Map<Id, Account>();
	//key:AccountId, value:List<Account>
	private Map<Id, Account> accOfficeMap = new Map<Id, Account>();
	//key:AccountId, value:List<Account>
	private Map<Id, List<Account>> accOfficeListMap = new Map<Id, List<Account>>();

	//レコードタイプ取得
	private static Map<String,RecordType> agencyRecIdTypes;
	private static Map<String,RecordType> getAgencyRecordTypes(){
		if(agencyRecIdTypes == null){
			agencyRecIdTypes = new Map<String,RecordType>();
			for(RecordType rt:[SELECT Id,Name,DeveloperName FROM RecordType 
										WHERE SObjectType = :CR_Agency__c.sObjectType.getDescribe().getName()]){
				agencyRecIdTypes.put(rt.DeveloperName, rt);
			}
		}
		return agencyRecIdTypes;
	}
/*	
	//MR取得 key:MRコード, value:ユーザID
	private static Map<String, Id> mrUserMap;
	private static Map<String, Id> getMrUserMap() {
		if (mrUserMap == null) {
			mrUserMap = new  Map<String, Id>();
			for (User u : [Select Id, MRCD__c From User where userType = 'Standard' and MRCD__c != null and isActive = true]) {
				System.debug(u);
				mrUserMap.put(u.MRCD__c, u.id);
			}
		}
		return mrUserMap;
	}
*/
	//2016.07.11 AMS対応時にﾘﾌｧｸﾀ対応
	private static Map<String, Id> getMrUserMap() {
		return E_UserDao.getMrUserMap();
	}
	
	/**
	 * 更新対象データを作成します
	 */
	public void createData() {
		List<Account> accounts = [Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c, E_CL1PF_ZBUSAGCY__c
											,(Select Id, Name, MR__c, Account__c, RecordTypeId,KSection__c From CR_Agencys__r) 
											,(select id, ZMRCODE__c, E_CL1PF_ZBUSAGCY__c, E_CL2PF_ZAGCYNUM__c,KSection__c from ChildAccounts)
									From Account
									where id in :sobjects];
		//代理店提出のレコード生成
		for (Account acc : accounts) {
			//外部委託レコード
			setAgencyRecord(acc, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			//管理規定レコード
			setAgencyRecord(acc, CR_Const.RECTYPE_DEVNAME_RULE);
			//事業報告・帳簿書類レコード
			setAgencyRecord(acc, CR_Const.RECTYPE_DEVNAME_Biz);
			accMap.put(acc.id, acc);
		}
		
		//事務所Accountの取得
		for (Account acc : [Select Id, ParentId, E_CL2PF_BRANCH__c		//支社コード 
							From Account 
							where ParentId in: accMap.keySet() 
										And (E_COMM_VALIDFLAG__c <> '2' And E_COMM_VALIDFLAG__c <> null)
							]){
			List<Account> accList = null;
			if (accOfficeListMap.containsKey(acc.ParentId)) {
				accList = accOfficeListMap.get(acc.ParentId);
			} else {
				accList = new List<Account>();
			}
			accList.add(acc);
			accOfficeListMap.put(acc.ParentId, accList);
			accOfficeMap.put(acc.id, acc);
		}
	}
	
	/**
	 * データ登録処理です
	 */
	public void executeData() {
		//代理店提出のレコード登録
		if (insRecs.size() > 0) {
			insert insRecs;
		}
		//代理店提出のレコード更新
		if (updRecs.size() > 0) {
			update updRecs.values();
		}
		//代理店提出Shareレコード生成
		agencySharing();
		
		//代理店提出Shareレコード削除
		if (delShare.size() > 0) {
			delete delShare;
		}
		//代理店提出Shareレコード登録
		if (insShare.size() > 0) {
			insert insShare;
		}
	}
	
	/**
	 * 代理店提出レコード生成と更新
	 * @param Account acc 
	 * @param String rectypeDevname
	 */
	private void setAgencyRecord(Account acc, String rectypeDevname) {
		//母店の担当MRを取得
		Account botenAccount = null;
		for (Account childAcc : acc.ChildAccounts) {
			//代理店格の母店コードが事務所のコードと同じ場合は、母店
			if (acc.E_CL1PF_ZBUSAGCY__c == childAcc.E_CL2PF_ZAGCYNUM__c) {
				botenAccount = childAcc;
				break;
			}
		}
		
		Id mrId = null;
		String botenKSection = null;
		if (botenAccount != null) {
			mrId = getMrUserMap().get(botenAccount.ZMRCODE__c);
			botenKSection = botenAccount.KSection__c;			//課コードの取得
		}
		if (mrId == null) {
			//担当MRが存在しない場合はログ出力
			setLogger(acc.E_CL1PF_ZHEADAY__c + '/'+ acc.E_CL1PF_ZBUSAGCY__c);
		}
		
		//既に、引数レコードタイプの代理店提出レコード作成されているか確認する
		CR_Agency__c agency = null;
		for (CR_Agency__c ag : acc.CR_Agencys__r) {
			if (ag.recordtypeid ==  getAgencyRecordTypes().get(rectypeDevname).id) {
				agency = ag;
				break;
			}
		}
		if (agency != null) {
			//課コードおよび担当MRに変更なし
			if(agency.KSection__c == botenKSection && agency.MR__c == mrId){
				//レコード更新しない
			}else{
				agency.MR__c = mrId;
				//課コードを設定
				agency.KSection__c = botenKSection;
				updRecs.put(agency.id, agency);
			}
			updShareRecs.put(agency.id, agency);
		} else {
			//作成されていない
			agency = new CR_Agency__c(
					Account__c = acc.id,
					MR__c = mrId, 
					DomainURL__c = URL.getSalesforceBaseUrl().toExternalForm(), 
					RecordTypeId = getAgencyRecordTypes().get(rectypeDevname).id,
					KSection__c = botenKSection					//課コードを設定
				);
			insRecs.add(agency);
		}
	}
	
	/**
	 * CR_Agency__Shareに権限を設定
	 */
	private void agencySharing() {
		//既に代理店提出レコードが存在する場合、CR_Agency__Shareレコード削除する。対象はSObjectTypeがGroupのレコード。
		for (CR_Agency__Share share : [SELECT Id, ParentId, UserOrGroupId, AccessLevel 
										FROM CR_Agency__Share 
										where ParentId in :updShareRecs.keySet()
										  AND RowCause = :Schema.CR_Agency__Share.RowCause.Manual]) {
			if (Schema.Group.SObjectType == share.UserOrGroupId.getSobjectType()) {
				delShare.add(share);
	   		}
		}
		//Share登録データ作成
		insShare.addAll(addShareList(insRecs));
		insShare.addAll(addShareList(updShareRecs.values()));
	}
	
	/**
	 * CR_Agency__Shareの権限付与リスト作成
	 * 
	 * @param List<CR_Agency__c> agencyRecs 更新対象の代理店提出リスト
	 * @return List<CR_Agency__Share> 更新対象の代理店提出Shareリスト
	 */
	private List<CR_Agency__Share> addShareList(List<CR_Agency__c> agencyRecs) {
		List<CR_Agency__Share> shareRecs = new List<CR_Agency__Share>();
		
		Id RECTYPE_ID_BIZ = getAgencyRecordTypes().get(CR_Const.RECTYPE_DEVNAME_Biz).id;		//事業報告書/帳簿書類のレコードタイプID
		
		for (CR_Agency__c agency : agencyRecs) {
			Group mrGp = null;
			if (accMap.get(agency.Account__c).E_CL1PF_ZBUSBR__c != null) {
				mrGp = zbusbrCdMap.get('BR' + accMap.get(agency.Account__c).E_CL1PF_ZBUSBR__c);
			} 
			if (mrGp != null) {
				shareRecs.add(new CR_Agency__Share(
					ParentID = agency.id, 
					UserOrGroupId = mrGp.id,
					AccessLevel = 'Edit',
					RowCause = Schema.CR_Agency__Share.RowCause.Manual)
				);
			}
			//代理店格ロール設定
			Group acGp = getUserRoleAccMap().get(agency.Account__c);
			if (acGp != null) {
				shareRecs.add(new CR_Agency__Share(
					ParentID = agency.id, 
					UserOrGroupId = acGp.id,
					AccessLevel = 'Edit',
					RowCause = Schema.CR_Agency__Share.RowCause.Manual)
				);
			}
			//代理店事務所ロール設定
			if (accOfficeListMap.get(agency.Account__c) != null) {
				for (Account acc : accOfficeListMap.get(agency.Account__c)) {
					acGp = getUserRoleAccMap().get(acc.Id);
					if (acGp != null) {
						shareRecs.add(new CR_Agency__Share(
							ParentID = agency.id, 
							UserOrGroupId = acGp.id,
							AccessLevel = 'Edit',
							RowCause = Schema.CR_Agency__Share.RowCause.Manual)
						);
					}
					
					//代理店事務所の担当支社に参照権限を付与 [事業報告書/帳簿書類]の場合のみ
					if(agency.recordTypeId == RECTYPE_ID_BIZ){
						Group branchGp = null;
						if (acc.E_CL2PF_BRANCH__c != null) {
							branchGp = zbusbrCdMap.get('BR' + acc.E_CL2PF_BRANCH__c);
						} 
						if(branchGp!=null){
							shareRecs.add(new CR_Agency__Share(
								ParentID = agency.id, 
								UserOrGroupId = branchGp.id,
								AccessLevel = 'Edit',
								RowCause = Schema.CR_Agency__Share.RowCause.Manual)
							);
						}
					}
				}
			}
		}
		return shareRecs;
	}
	
	//代理店のロールを取得 key:AccountId, value:Groupリスト
	private Map<Id, Group> userRoleAccMap = null;
	private Map<Id, Group> getUserRoleAccMap() {
		if (userRoleAccMap == null) {
			userRoleAccMap = new Map<Id, Group>();
			//key:UserroleId, value:AccountId
			Map<Id, Id> userRoleMap = new Map<Id, Id>();
			//AccountIDからUserRoleを取得
			for (UserRole uRole : [Select Id, Name, PortalAccountId From UserRole where PortalAccountId in: accMap.keySet()]) {
				userRoleMap.put(uRole.Id, uRole.PortalAccountId);
			}
			for (UserRole uRole : [Select Id, Name, PortalAccountId From UserRole where PortalAccountId in: accOfficeMap.keySet()]) {
				userRoleMap.put(uRole.Id, uRole.PortalAccountId);
			}
			for (Group gp : [Select Id, Name, Type, DeveloperName, RelatedId From Group where RelatedId in :userRoleMap.keySet() And Type = 'RoleAndSubordinates']) {
				if (userRoleMap.get(gp.RelatedId) != null) {
					userRoleAccMap.put(userRoleMap.get(gp.RelatedId), gp);
				}
			}
		}
		return userRoleAccMap;
	}
}