public with sharing class SC_OfficeTriggerHandler {
    
    public SC_OfficeTriggerHandler(){
    	
    }
    
    /**
     * before insert
     */
    public void onBeforeInsert(List<SC_Office__c> newList){
		/* ****************************************** 
		 * Domainのセット
		 * ******************************************/
		String domainNNlinkage = E_Util.getDomainNNlinkage();
		String domainNNlinkcgwage = E_Util.getURLForGWTB(domainNNlinkage);
		
		for(SC_Office__c rec : newList){
          	rec.DomainInternet__c = domainNNlinkage;
          	rec.DomainGw__c = domainNNlinkcgwage;
          	rec.DomainEmployee__c = URL.getSalesforceBaseUrl().toExternalForm() + '/';
		}
    }
    
    /**
	 * after insert
	 */
	public void onAfterInsert(List<SC_Office__c> newList){
		/* ****************************************** 
		 * 自主点検の登録
		 * ******************************************/
		// レコードタイプID取得 :カスタム表示ラベルの値から取得
		Id rtIdScComp = E_RecordTypeDao.getRecIdBySobjectTypeAndDevName('FY' + System.Label.SC_FiscalYear, SObjectType.SC_SelfCompliance__c.Name);

		List<SC_SelfCompliance__c> selfComplianceList = new List<SC_SelfCompliance__c>();
		for(SC_Office__c rec : newList){
			for(String t : SC_Const.SC_COMP_TYPE_LIST){
				SC_SelfCompliance__c selfComliance = new SC_SelfCompliance__c();
				selfComliance.RecordTypeId = rtIdScComp;
				selfComliance.Name = t;
				selfComliance.SC_Office__c = rec.Id;
				selfComliance.Kind__c = t;
				selfComplianceList.add(selfComliance);
			}
		}
		insert selfComplianceList;
	}
}