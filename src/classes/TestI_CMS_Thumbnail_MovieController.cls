@isTest
private class TestI_CMS_Thumbnail_MovieController {
	
	@isTest static void constTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.runAs(actUser){
			Test.startTest();
			I_CMS_Thumbnail_MovieController ctm = new I_CMS_Thumbnail_MovieController();
			System.assertEquals(ctm.companyLimitedFlag,true);
			Test.stopTest();
		}
	}
}