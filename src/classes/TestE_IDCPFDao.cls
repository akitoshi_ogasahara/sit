@isTest
public with sharing class TestE_IDCPFDao {
	private static user thisuser = [select id from user where id =:system.UserInfo.getUserId()];
	/**
	 * E_IDCPFDao 分岐網羅
	 */
	private static testMethod void testE_IDCPFDao() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idManager = createIDCPF(true,portalUser.id);


		//結果画面
		PageReference resultPage;

		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();
			Id dummyId = acc.id;
			Set<Id> ids = new set<Id>{idManager.id};

			System.assert(E_IDCPFDao.getRecsById(portalUser.id) != null);
			System.assertEquals(E_IDCPFDao.getRecsById(u.id),null);
			System.assertNotEquals(E_IDCPFDao.getActiveUsersRecsByIDs(ids).size(),0);
			//System.assert(E_IDCPFDao.getRecsByContactId(con.id) != null);

			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(null, resultPage);
	}

	/**
	 * E_IDCPFDao 無効ユーザのパターン
	 */
	private static testMethod void testE_IDCPFDao_InactiveUser() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		portalUser.IsActive = false;
		update portalUser;
		E_IDCPF__c idManager = createIDCPF(true,portalUser.id);

		system.debug('@@@portalOwner= '+ portalOwner.id);
		system.debug('@@@acc= '+ acc.id);
		system.debug('@@@con= '+ con.id);
		system.debug('@@@portalUser= '+ portalUser.id);

		//結果画面
		PageReference resultPage;

		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();
			Id dummyId = acc.id;
			Set<Id> ids = new set<Id>{idManager.id};

			System.assert(E_IDCPFDao.getRecsById(portalUser.id) != null);
			System.assertNotEquals(E_IDCPFDao.getRecsById(portalUser.Id),null);			//無効ユーザも1件UserId指定の場合取得可能
			System.assertEquals(E_IDCPFDao.getActiveUsersRecsByIDs(ids).size(),0);		//無効ユーザなので0件となる

			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(null, resultPage);
	}

	private static testMethod void test_getRecsByUserIds() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idManager = createIDCPF(true,portalUser.id);

		//テストユーザで機能実行開始
		System.runAs(u){
			//テスト開始
			Test.startTest();

			//nullまたはSetが0件の時は、0件のリストが取得される
			List<E_IDCPF__c> results = E_IDCPFDao.getRecsByUserIds(null);
			System.assertEquals(0,results.size());
			results = E_IDCPFDao.getRecsByUserIds(new Set<Id>());
			System.assertEquals(0,results.size());

			//リストが取得される場合
			results = E_IDCPFDao.getRecsByUserIds(new Set<Id>{portalOwner.Id, portalUser.Id});
			System.assertEquals(1,results.size());		//portalUser分のみ取得
			System.assertEquals(idManager.Id, results[0].Id);

			//テスト終了
			Test.stopTest();
		}
	}

	/**
	　* List<E_IDCPF__c> getNotificationRecsByUserContactId(Set<String> conIds)
	　*/
	private static testMethod void getNotificationRecsByUserContactId_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idcpf = createIDCPF(true,portalUser.id);

		// データ更新
		idcpf = [Select Id, IsNotNotification__c, FLAG01__c, ZSTATUS01__c From E_IDCPF__c Where Id = :idcpf.Id];
		idcpf.IsNotNotification__c = false;
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
		update idcpf;

		// テストパラメータ
		Set<String> conIds = new Set<String>();
		conIds.add(con.Id);

		System.runAs(u){
			Test.startTest();

			List<E_IDCPF__c> result = E_IDCPFDao.getNotificationRecsByUserContactId(conIds);
			System.assertEquals(1, result.size());

			Test.stopTest();
		}
	}

	private static testMethod void getNotificationRecsByUserContactIdQuery_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idcpf = createIDCPF(true,portalUser.id);

		// データ更新
		idcpf = [Select Id, IsNotNotification__c, FLAG01__c, ZSTATUS01__c From E_IDCPF__c Where Id = :idcpf.Id];
		idcpf.IsNotNotification__c = false;
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
		update idcpf;

		// テストパラメータ
		Set<Id> conIds = new Set<Id>();
		conIds.add(con.Id);

		System.runAs(u){
			Test.startTest();

			List<E_IDCPF__c> result = E_IDCPFDao.getNotificationRecsByUserContactIdQuery(conIds,true);
			System.assertEquals(1, result.size());

			Test.stopTest();
		}
	}

	/**
	　* List<E_IDCPF__c> getNotificationRecsByZINQUIRR(Set<String> grpCode)
	　*/
	private static testMethod void getNotificationRecsByZINQUIRR_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idcpf = createIDCPF(true,portalUser.id);

		// 照会者コード
		String grpCode = 'L112345';

		// データ更新
		idcpf = [Select Id, IsNotNotification__c, FLAG01__c, ZSTATUS01__c, ZINQUIRR__c From E_IDCPF__c Where Id = :idcpf.Id];
		idcpf.IsNotNotification__c = false;
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
		idcpf.ZINQUIRR__c = grpCode;
		update idcpf;

		// テストパラメータ
		Set<String> grpCodes = new Set<String>();
		grpCodes.add(grpCode);

		System.runAs(u){
			Test.startTest();

			List<E_IDCPF__c> result = E_IDCPFDao.getNotificationRecsByZINQUIRR(grpCodes);
			System.assertEquals(1, result.size());

			Test.stopTest();
		}
	}

	private static testMethod void getNotificationRecsByContactAgentNum5Query_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idcpf = createIDCPF(true,portalUser.id);

		// 募集人コード
		String agentNum = 'Z1234';

		// データ更新
		idcpf = [Select Id, IsNotNotification__c, FLAG01__c, ZSTATUS01__c, ZINQUIRR__c From E_IDCPF__c Where Id = :idcpf.Id];
		idcpf.IsNotNotification__c = false;
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		idcpf.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
		update idcpf;

		con.E_CL3PF_AGNTNUM__c = agentNum;
		update con;

		//テストパラメータ
		Set<String> agentNums = new Set<String>();
		agentNums.add(agentNum);

		System.runAs(u){
			Test.startTest();

			List<E_IDCPF__c> result = E_IDCPFDao.getNotificationRecsByContactAgentNum5Query(agentNums,true);
			System.assertEquals(1, result.size());

			Test.stopTest();
		}
	}

	/**
	　* List<E_IDCPF__c> getRecsByProfileNames(Set<String> profileNames)
	　*/
	private static testMethod void getRecsByProfileNames_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'ＭＲ');
		E_IDCPF__c idcpf = createIDCPF(true,u.id);

		Test.startTest();
		List<E_IDCPF__c> result = E_IDCPFDao.getRecsByProfileNames(I_Const.EMPLOYEE_PROFILE_NAMES);
		Test.stopTest();

		Map<Id, E_IDCPF__c> idcpfMap = new Map<Id, E_IDCPF__c>();
		for(E_IDCPF__c rec : result){
			idcpfMap.put(rec.User__r.Id, rec);
		}

		System.assert(idcpfMap.get(u.Id) != null);

	}

	private static testMethod void getActiveUsersRecsByZHEADAY_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idcpf = createIDCPF(true,portalUser.id);

		// 募集人コード
		String zheaday = 'Z1234';

		// データ更新
		idcpf = [Select Id, IsNotNotification__c, FLAG01__c, ZSTATUS01__c, ZINQUIRR__c From E_IDCPF__c Where Id = :idcpf.Id];
		idcpf.IsNotNotification__c = false;
		idcpf.FLAG01__c = E_Const.IS_INQUIRY_1;
		idcpf.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
		update idcpf;

		acc.E_CL1PF_ZHEADAY__c = zheaday;
		update acc;

		System.runAs(u){
			Test.startTest();

			List<E_IDCPF__c> result = E_IDCPFDao.getActiveUsersRecsByZHEADAY(zheaday);
			System.assertEquals(1, result.size());

			Test.stopTest();
		}
	}


	private static testMethod void getNotificationRecsByUserIds_test01(){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'testUser', 'システム管理者');
		User portalOwner = createportalAccountOwner(true, 'PartnertUser', 'システム管理者');
		Account acc = createAcc(true, 'dummyAccount',portalOwner.id);
		Contact con = CreateContact(true, acc.Id);
		User portalUser = createportalUser(true, 'TestContact', 'Partner Community User',con.id);
		E_IDCPF__c idcpf = createIDCPF(true,portalUser.id);

		Set<Id> userIds = new Set<Id>();
		userIds.add(portalUser.id);

		System.runAs(u){
			Test.startTest();

			List<E_IDCPF__c> result = E_IDCPFDao.getNotificationRecsByUserIds(userIds);
			System.assertEquals(1, result.size());

			Test.stopTest();
		}
	}

	/**
	 * ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	private static User createUser(Boolean isInsert, String LastName, String profileDevName){
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
		);
		system.runAs(thisuser){
			if (isInsert) {
				insert src;
			}
		}
			return src;
	}
	private static Map<String, Id> pMap;
	private static Map<String, Id> getProfileIdMap(){
		if(pMap != null){
			return pMap;
		}
		pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}

	 /**
	 * パートナーユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	private static User createportalAccountOwner(Boolean isInsert, String LastName, String profileDevName){
		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		system.debug('@@@USERTYPE= ' + portalRole.id);
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User portalAccountOwner = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
				, UserRoleId = portalRole.Id
		);
		system.runAs(thisuser){
			if (isInsert) {
				insert portalAccountOwner;
			}
		}
		return portalAccountOwner;
	}

	 /**
	 * パートナー取引先作成
	 * @param isInsert: whether to insert
	 * @param accName: 取引先名
	 * @return Account: 取引先
	 */
	private static Account createAcc(Boolean isInsert, String accName, Id portalAccountOwnerid){
		Account src = new Account(
				  name = accName
				 ,OwnerId = portalAccountOwnerid
		);
		system.runAs(thisuser){
			if (isInsert) {
				insert src;
			}
		}
		return src;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: 取引先Id
	 * @return Contact: 取引先責任者
	 */
	private static Contact CreateContact(Boolean isInsert, Id accId){
		Contact src = new Contact(
				  LastName = 'TestContact'
				 ,AccountId = accId

		);
		system.runAs(thisuser){
			if (isInsert) {
				insert src;
			}
		}
		return src;
	}

	/**
	 * ポータルユーザ作成
	 * @param isInsert: whether to insert
	 * @param conId: 取引先責任者Id
	 * @return User: User
	 */
	private static User createportalUser(Boolean isInsert, String LastName, String profileDevName, Id conId){
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
				, ContactId = conId

		);
		system.runAs(thisuser){
			if (isInsert) {
				insert src;
			}
		}
		return src;
	}

	/**
	 * ID管理作成
	 * @param isInsert: whether to insert
	 * @param UserId: ユーザId
	 * @return E_IDCPF__c: E_IDCPF__c
	 */
	private static E_IDCPF__c createIDCPF(Boolean isInsert, Id userId){
		E_IDCPF__c src = new E_IDCPF__c(
				 User__c = userId

		);
		system.runAs(thisuser){
			if (isInsert) {
				insert src;
			}
		}
		return src;
	}

}