global with sharing class MNT_PamphMaintenanceController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public MNT_PamphMaintenanceExtender getExtender() {return (MNT_PamphMaintenanceExtender)extender;}
	{
	setApiVersion(42.0);
	}
	public MNT_PamphMaintenanceController(ApexPages.StandardController controller) {
		super(controller);
		appComponentProperty = new Map<String, Map<String, Object>>();
		appComponentProperty.put('Component90',new Map<String,Object>{'id'=>'','top'=>'','left'=>'','width'=>'','height'=>'','SIncludeOrder'=>'System.Object','p_rendered'=>'{! Not(extender.hasFileflag)}','p_newFile'=>'{! extender.uploadFile}','Component__Width'=>'229','Component__Height'=>'28','Component__id'=>'Component90','Component__Name'=>'EUploadAttachment','Component__NameSpace'=>'','Component__Top'=>'0','Component__Left'=>'0'});
		registTransitionControl='/apex/MNT_PamphletView?id={ID}';
		editTransitionControl='/apex/MNT_PamphletView?id={ID}';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DOM_Id__c;
		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.Content__c;
		f = I_ContentMaster__c.fields.PamphletCategory__c;
		f = I_ContentMaster__c.fields.ProductCategory__c;
		f = I_ContentMaster__c.fields.Jurisdiction__c;
		f = I_ContentMaster__c.fields.DisplayOrder__c;
		f = I_ContentMaster__c.fields.CreatedDepartment__c;
		f = I_ContentMaster__c.fields.Reason__c;
		f = I_ContentMaster__c.fields.DocumentCategory__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.WindowTarget__c;
		f = I_ContentMaster__c.fields.filePath__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_PamphMaintenanceController';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addField('Name');
			mainQuery.addField('DOM_Id__c');
			mainQuery.addField('ParentContent__c');
			mainQuery.addField('FormNo__c');
			mainQuery.addField('DisplayFrom__c');
			mainQuery.addField('ApprovalNo__c');
			mainQuery.addField('ValidTo__c');
			mainQuery.addField('Content__c');
			mainQuery.addField('PamphletCategory__c');
			mainQuery.addField('ProductCategory__c');
			mainQuery.addField('Jurisdiction__c');
			mainQuery.addField('DisplayOrder__c');
			mainQuery.addField('CreatedDepartment__c');
			mainQuery.addField('Reason__c');
			mainQuery.addField('DocumentCategory__c');
			mainQuery.addField('ClickAction__c');
			mainQuery.addField('WindowTarget__c');
			mainQuery.addField('filePath__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_PamphMaintenanceExtender(this);
			init();
			if (record.Id == null) {
				saveOldValues();
			}

			extender.init();
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}