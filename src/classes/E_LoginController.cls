public with sharing class E_LoginController extends E_InfoController {
	private static final String MENU_KEY_AGENT = 'login_agent';
	private static final String MENU_KEY_CUSTOMER = 'login_customer';


	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}

	//ブラウザ判定Handler
	public E_BrowserJudgeHandler browserHandler{get;set;}

	//ユーザ名
	public String userId {get;set;}
	//パスワード
	public String password {get;set;}
	//メニューキー
	public String menuKey {get;set;}

	//コンストラクタ
	public E_LoginController(){
		pgTitle = 'ログイン';
		pageMessages = getPageMessages();
	}

	//ページアクション
	public PageReference pageAction(){
		//ブラウザ判定クラス
		browserHandler = new E_BrowserJudgeHandler(getLoginPage());

		pageRef = doAuth(E_Const.ID_KIND.LOGIN, userinfo.getUserId());
		return E_Util.toErrorPage(pageRef, null);
	}

	//[ログイン]ボタン
	public PageReference doLogin(){

		PageReference ret = null;
		pageMessages.clearMessages();

		try {
			E_CookieHandler.clearCookieCompliance();
			
			//ログインパスワード
			String loginPassword = password;

			//ユーザレコード取得
			User userRec = E_UserDaoWithout.getUserRecByUserName(userId + System.Label.E_USERNAME_SUFFIX);
			if(userRec == null){
				pageMessages.addErrorMessage(getMSG().get('LGE|001'));
				return null;
			}
			
			//ユーザロックチェック
			if (isLocked(userRec)) {
				return null;
			}
			
			//初回ログインの場合
			if(userRec.E_UseTempBasicPW__c){
				//ログインパスワードにプレフィックスをつけ暗号化
				loginPassword = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, userRec.E_TempBasicPWModifiedDate__c);
			}else if(userRec.E_UseExistingBasicPW__c){
				//既存ユーザの場合
				//ログインパスワードにプレフィックスをつけ暗号化
				loginPassword = E_EncryptUtil.encrypt(password);
			}

			//ログイン  startUrlがある場合そちらのページへ遷移
			String startUrl = ApexPages.currentPage().getParameters().get('startUrl');
			if(String.isBlank(startUrl)){
				//ログイン後遷移先(住生ユーザの場合、コンプラをとばしてE_HomeSumiseiへ) 住生IPアドレスから契約者がアクセスする場合にはコンプラへ遷移
				Pagereference pr = (isSumiseiIpAddress() && getMenuKey()==MENU_KEY_AGENT)?Page.E_HomeSumisei:Page.E_Compliance;
				System.debug('=====' + isSumiseiIpAddress() + getMenuKey() + pr);
				//ログイン
				ret = Site.login(userId + System.Label.E_USERNAME_SUFFIX, loginPassword, pr.getUrl());
			}else{
				ret = Site.login(userId + System.Label.E_USERNAME_SUFFIX, loginPassword, startUrl);
			}

			
			//ユーザロックチェック
			if (isLocked(userRec)) {
				return null;
			}

			//エラーメッセージをNNLinkコンポーネントに移します。
			System.debug('ApexPages=====' + ApexPages.getMessages());
			if(ApexPages.hasMessages()){
				pageMessages.addMessages(ApexPages.getMessages());
				return null;
			}

		} catch(Exception e) {
			System.debug('Exception=====' + e);
			pageMessages.addErrorMessage(e.getMessage());
			ret = null;
		}

		return ret;
	}

	//[パスワードをお忘れの方はこちら]ボタン
	public PageReference doReissue(){
		return Page.E_ReissuePassword;
	}
	
	//メニューキーを返却
	protected override String getMenuKey(){
		if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_CLI)) {
			return MENU_KEY_CUSTOMER;//'login_customer';
		} else if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_AGE)) {
			return MENU_KEY_AGENT;//'login_agent';
		}
		return '';
	}
	
	//ログインページを取得　　ブラウザチェック後の戻り先とする。
	private PageReference getLoginPage(){
		if(getMenuKey()==MENU_KEY_AGENT){
			return Page.E_LoginAgent;
		}else if(getMenuKey()==MENU_KEY_CUSTOMER){
			return Page.E_LoginCustomer;
		}else{
			return null;
		}
	}
	
	private boolean isLocked(User userRec) {
		UserLogin userLoginRecBefore = E_UserLoginDaoWithout.getRecByUserId(userRec.id);
		if(userLoginRecBefore == null){
			String pathPrefix = Site.getPathPrefix();
			if (!String.isEmpty(pathPrefix)) {
				if (pathPrefix.indexOf(E_Const.NNLINK_CLI) > 0) {
					pageMessages.addErrorMessage(getMSG().get('LGE|002'));
				} else if(isSumiseiIpAddress()){										//住生アドレスからの場合メッセージを変更する
					pageMessages.addErrorMessage(getMSG().get('LGE|003_sumisei'));
				} else {
					pageMessages.addErrorMessage(getMSG().get('LGE|003'));
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 *  Client IPアドレスが、住生のIPアドレスホワイトリスト(System.Label.E_SUMISEI_IP_WHITELIST)の
	 *  ものと合致する場合、trueを返す
	 */
	public boolean isSumiseiIpAddress(){
		return E_Util.isIncludedIpRange(System.Label.E_SUMISEI_IP_WHITELIST);
	}
}