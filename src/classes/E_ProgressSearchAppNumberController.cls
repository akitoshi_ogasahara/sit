global with sharing class E_ProgressSearchAppNumberController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_CPCPF__c record{get;set;}	
			
	public E_ProgressSearchAppNumberExtender getExtender() {return (E_ProgressSearchAppNumberExtender)extender;}
	
		public searchResult searchResult {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c iProductCd1_val {get;set;}	
		public SkyEditor2.TextHolder iProductCd1_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c iProductNum1_val {get;set;}	
		public SkyEditor2.TextHolder iProductNum1_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c iProductCd2_val {get;set;}	
		public SkyEditor2.TextHolder iProductCd2_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c iProductNum2_val {get;set;}	
		public SkyEditor2.TextHolder iProductNum2_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c iProductCd3_val {get;set;}	
		public SkyEditor2.TextHolder iProductCd3_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c iProductNum3_val {get;set;}	
		public SkyEditor2.TextHolder iProductNum3_op{get;set;}	
			
	public String recordTypeRecordsJSON_E_CPCPF_c {get; private set;}
	public String defaultRecordTypeId_E_CPCPF_c {get; private set;}
	public String metadataJSON_E_CPCPF_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public E_ProgressSearchAppNumberController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_CPCPF__c.fields.ZWEBCNTT__c;
		f = E_CPCPF__c.fields.PROPNUM__c;
		f = E_CPCPF__c.fields.ContractorName__c;
		f = E_CPCPF__c.fields.AgentName__c;
		f = E_CPCPF__c.fields.CHDRNUMOrGRUPNUM__c;
		f = E_CPCPF__c.fields.ZSTATUS01__c;
		f = E_CPCPF__c.fields.ZSTATUS02__c;
		f = E_CPCPF__c.fields.ZSTATUS03__c;
		f = E_CPCPF__c.fields.ZSTATUS04__c;
		f = E_CPCPF__c.fields.ZSTATUS05__c;
		f = E_CPCPF__c.fields.ZSTATUS06__c;
		f = E_CPCPF__c.fields.ZSTATUS07__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_CPCPF__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				iProductCd1_val = new SkyEditor2__SkyEditorDummy__c();	
				iProductCd1_op = new SkyEditor2.TextHolder('eq');	
					
				iProductNum1_val = new SkyEditor2__SkyEditorDummy__c();	
				iProductNum1_op = new SkyEditor2.TextHolder('co');	
					
				iProductCd2_val = new SkyEditor2__SkyEditorDummy__c();	
				iProductCd2_op = new SkyEditor2.TextHolder('eq');	
					
				iProductNum2_val = new SkyEditor2__SkyEditorDummy__c();	
				iProductNum2_op = new SkyEditor2.TextHolder('co');	
					
				iProductCd3_val = new SkyEditor2__SkyEditorDummy__c();	
				iProductCd3_op = new SkyEditor2.TextHolder('eq');	
					
				iProductNum3_val = new SkyEditor2__SkyEditorDummy__c();	
				iProductNum3_op = new SkyEditor2.TextHolder('co');	
					
				queryMap.put(	
					'searchResult',	
					new SkyEditor2.Query('E_CPCPF__c')
						.addFieldAsOutput('PROPNUM__c')
						.addFieldAsOutput('ContractorName__c')
						.addFieldAsOutput('AgentName__c')
						.addFieldAsOutput('CHDRNUMOrGRUPNUM__c')
						.addFieldAsOutput('ZSTATUS01__c')
						.addFieldAsOutput('ZSTATUS02__c')
						.addFieldAsOutput('ZSTATUS03__c')
						.addFieldAsOutput('ZSTATUS04__c')
						.addFieldAsOutput('ZSTATUS05__c')
						.addFieldAsOutput('ZSTATUS06__c')
						.addFieldAsOutput('ZSTATUS07__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(iProductCd1_val, 'SkyEditor2__Text__c', 'ZWEBCNTT__c', iProductCd1_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(iProductNum1_val, 'SkyEditor2__Text__c', 'PROPNUM__c', iProductNum1_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(iProductCd2_val, 'SkyEditor2__Text__c', 'ZWEBCNTT__c', iProductCd2_op, true, 1, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(iProductNum2_val, 'SkyEditor2__Text__c', 'PROPNUM__c', iProductNum2_op, true, 1, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(iProductCd3_val, 'SkyEditor2__Text__c', 'ZWEBCNTT__c', iProductCd3_op, true, 2, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(iProductNum3_val, 'SkyEditor2__Text__c', 'PROPNUM__c', iProductNum3_op, true, 2, true ))
				);	
					
					searchResult = new searchResult(new List<E_CPCPF__c>(), new List<searchResultItem>(), new List<E_CPCPF__c>(), null);
				 searchResult.setPageItems(new List<searchResultItem>());
				 searchResult.setPageSize(100);
				listItemHolders.put('searchResult', searchResult);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_CPCPF__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_ProgressSearchAppNumberExtender(this);
			presetSystemParams();
			extender.init();
			searchResult.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_E_CPCPF_c_ZWEBCNTT_c() { 
			return getOperatorOptions('E_CPCPF__c', 'ZWEBCNTT__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_CPCPF_c_PROPNUM_c() { 
			return getOperatorOptions('E_CPCPF__c', 'PROPNUM__c');	
		}	
			
			
	global with sharing class searchResultItem extends SkyEditor2.ListItem {
		public E_CPCPF__c record{get; private set;}
		@TestVisible
		searchResultItem(searchResult holder, E_CPCPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class searchResult extends SkyEditor2.PagingList {
		public List<searchResultItem> items{get; private set;}
		@TestVisible
			searchResult(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<searchResultItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new searchResultItem(this, (E_CPCPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<searchResultItem> getViewItems() {            return (List<searchResultItem>) getPageItems();        }
	}

			
	}