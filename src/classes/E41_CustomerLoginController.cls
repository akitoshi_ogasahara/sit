public with sharing class E41_CustomerLoginController {

    /**
     *      Constructor
     */
    public E41_CustomerLoginController(){

    }

    /**
    *       Pageaction
    */
    public  Pagereference toErrPage(){
        System.debug('@@デバッグ@@'+E_Util.getUserAgent());
        //ブラウザチェック
        if(E_Util.isUserAgentMacintosh()){
            return Page.E41_ErrMcCustomer;
        }else if(!E_Util.isUserAgentIE()){
            return Page.E41_ErrIeCustomer;
        }else{
            //正常処理
            return null;
        }
    }
   



}