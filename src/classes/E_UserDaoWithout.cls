public without sharing class E_UserDaoWithout {

	/**
	 * ユーザデータを統合IDから取得する
	 * @param userId: 統合ID
	 * @return List<User>: ユーザ情報
	 */
	public static User getUserRecByFederationIdentifier(String federationIdentifier){
		User rec = null;
		List<User> recs =
			[SELECT
				AccountId,
				ContactId,
				UserType,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.Account.ParentId
			FROM User
			WHERE FederationIdentifier = :federationIdentifier
			And IsActive = true
			limit 1
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 * ユーザデータをユーザ名から取得する
	 * @param userId: ユーザ名
	 * @return User: ユーザ情報
	 */
	public static User getUserRecByUserName(String username){
		User rec = null;
		List<User> recs = [
			SELECT
				AccountId,
				ContactId,
				UserType,
				Username,
				Email,
				LastLoginDate,
				LastModifiedDate,
				LastPasswordChangeDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_MenuKindId__c,
				Contact.E_CADPF__r.Name,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.Account.ParentId,
				Contact.Account.ZAUTOTRF__c,
				Contact.Account.E_DoCheckIP__c,	//20171106追加 銀行代理店解放
				Contact.Account.Parent.ZAUTOTRF__c,
				(SELECT Id,ZSTATUS01__c FROM E_IDCPFs__r)	//追加 ID適正化
			FROM User
			WHERE username = :username
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 * ユーザデータをユーザIDから取得する
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static User getUserRecByUserId(ID userId){
		User record = null;

		List<User> recs =
			[SELECT
				Name,
				AccountId,
				Username,
				ContactId,
				UserType,
				Email,
				LastLoginDate,
				LastModifiedDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.Account.ParentId,
				Contact.E_AccParentName__c,
				Contact.E_Account__c,
				Contact.Account.Owner.Name
			FROM User
			WHERE ID =:userId
			];
		if (recs.size() > 0) {
			record = recs.get(0);
		}

		System.debug(record);

		return record;

	}

	/**
	 * Site iss 用 ログインせずにUser情報をupdate
	 * @param confirmUser ユーザ情報
	 */
	public static void updateRec(User confirmUser){
		update confirmUser;
	}

	/**
	 * ユーザデータを作成する
	 * @param  uc:値がセットされたUser変数
	 * @param  proId:プロファイルのID　withoutでないと設定できないため、引数で受け取り設定する
	 * @return   uc:作られたUser
	 */
	public static User createUser(User userRec,String proId){

		Database.SaveResult result;
		Database.DMLOptions ddo = new Database.DMLOptions();
		//自動送信メールをoff
		ddo.EmailHeader.triggerUserEmail = false;
		userRec.ProfileId = proId;
		result = Database.insert(userRec,ddo);
		//insert userRec;
		//saveresult を判定
		if(!result.isSuccess()){
			System.debug(result);
			return null;
		}
		return userRec;
	}

	/**
	 * ユーザデータを作成する
	 * @param  userRec:値がセットされたUser変数
	 */
	public static Database.Saveresult updateUserRec(User userRec){

		Database.SaveResult result;
		Database.DMLOptions ddo = new Database.DMLOptions();
		//自動送信メールをoff
		ddo.EmailHeader.triggerUserEmail = false;
		result = Database.update(userRec,ddo);
		return result;
	}

	public static Database.Saveresult updateUserRecWithClearFederationIdentifier(User userRec){
		//ユーザ情報をupdate
		userRec.FederationIdentifier = null;
		return E_UserDaoWithout.updateUserRec(userRec);

	}


	/**
	 *ユーザレコードとＩＤ管理レコードをContactIdから取得
	 *@param  contactId:募集人ＩＤ
	 *@return 検索されたUserレコード
	 */
	public static List<User> getUserRecsByContactId(Id contactId){
		List<User> recs =
		[SELECT
				AccountId,
				Username,
				ContactId,
				UserType,
				Email,
				LastLoginDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				isActive,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.Account.ParentId,
				(SELECT Id, ZSTATUS01__c FROM E_IDCPFs__r)	//20171206 項目追加
		FROM User
		WHERE ContactId = :contactId
		];

		return recs;
	}

	/**
	 *ユーザレコードを(username or email) and isactiveで取得する
	 *@param email：入力されたEmail
	 *@return 検索されたUserレコード
	 */
	public static List<User> getUserRecsByEmailOrUsernameAndIsActive(String email,String userId){
		String username = email + System.Label.E_USERNAME_SUFFIX;

		List<User> recs =
		[SELECT
				AccountId,
				Username,
				ContactId,
				UserType,
				Email,
				IsActive,
				LastLoginDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.Account.ParentId
		FROM User
		WHERE (Email = :email OR Username = :username)
		AND   IsActive = true
		AND   Id != :userId
		];

		return recs;

	}

	/**
	 * ユーザデータをユーザ名から取得する(ID管理取得)
	 * @param username: ユーザ名
	 * @return User: ユーザ情報
	 */
	public static User getUserRecByUserNameWithEIDC(String username){
		User record = null;

		List<User> recs =
			[SELECT
				AccountId,
				Username,
				ContactId,
				UserType,
				Email,
				LastLoginDate,
				LastModifiedDate,
				LastPasswordChangeDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_MenuKindId__c,
				Contact.E_CADPF__r.Name,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.Account.ParentId,
				Contact.Account.ZAUTOTRF__c,
				Contact.Account.E_DoCheckIP__c,	//20171106追加 銀行代理店解放
				Contact.Account.Parent.ZAUTOTRF__c,
				//(SELECT Id FROM E_IDCPFs__r)
				(SELECT Id,ZSTATUS01__c FROM E_IDCPFs__r WHERE ZSTATUS01__c <> '2')	//20171206 条件追加
			FROM User
			WHERE Username = :username];
		if (recs.size() > 0) {
			record = recs.get(0);
		}

		return record;
	}

	/**
	 * ユーザデータをContactId,UserName,isActiveから取得
	 * @param us ユーザレコード
	 * @return recs: ユーザ情報
	 */
	public static List<User> getUserRecByUserRec(User us){

		List<User> recs =
			[SELECT
				AccountId,
				Username,
				ContactId,
				UserType,
				Email,
				LastLoginDate,
				LastModifiedDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				IsActive,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.E_CL3PF_AGNTNUM__c,
				Contact.Account.ParentId,
				Contact.Account.ZAUTOTRF__c
			FROM User
			WHERE Username = :us.Username
			AND   ContactId = :us.ContactId
			AND   IsActive = :true ];

		return recs;
	}

	/**
	 * ユーザデータをUserNameから取得する	ContactやAccountの情報を付加して再取得
	 * @param List<User>
	 * @return Map<Id, User>
	 */
	public static Map<Id, User> getUserByUser(List<User> usrs){
		Set<String> userNames = new Set<String>();
		for(User u : usrs){
			userNames.add(u.UserName);
		}

		return new Map<Id, User>([
			SELECT
				AccountId,
				ContactId,
				UserType,
				Email,
				Username,
				ProfileId,
				isActive,
				UserRole.DeveloperName,
				Contact.Account.Parent.E_MenuKind__c,
				Contact.E_AccParentCord__c,
				Contact.Account.Parent.E_CL1PF_KCHANNEL__c,
				Contact.Account.canSelfRegist__c,
				Contact.Id,
				Contact.Account.ParentId,
				Contact.AccountId
			 FROM User
			 WHERE UserName in :userNames
		]);
	}

	/**
	 * 利用募集人一覧用
	 */
	public static List<User> getIRISAgentUserRecord(String brunchCode) {
		return getIRISAgentUserRecord(brunchCode, null);
	}
	public static List<User> getIRISAgentUserRecord(String brunchCode, String appendixCondition) {
		String soqlWhere = '';
		Id userId = UserInfo.getUserId();
		if(brunchCode != '**') {
			soqlWhere += ' AND Contact.Account.OwnerId =: userId ';
		}

		if (String.isNotBlank(appendixCondition)) {
			soqlWhere += appendixCondition;
		}

		DateTime lastMonth = DateTime.now().addMonths(-1);
		if (Test.isRunningTest()) {
			// テストクラスでLastLoginDateを更新することができないため取得だけでも行えるようにする。
			lastMonth = null;
		}

		String soql = 'SELECT Id '
					+ ', Name '
					+ ', LastLoginDate '
					+ ', Contact.Name '
					+ ', Contact.E_CL3PF_ZEATKNAM__c '
					+ ', Contact.Account.Name '
					+ ', Contact.Account.E_CL2PF_ZEAYKNAM__c '
					+ ', (SELECT id '
					+ '   FROM NNLinkLogs__r '
					+ '   WHERE AccessDatetime__c >= :lastMonth '
					+ '   AND isDisplay__c = true '
					+ '   LIMIT 1) '
					+ 'FROM User '
					+ 'WHERE isActive = true '
					+ 'AND UserType = \'PowerPartner\' '
					+ 'AND LastLoginDate >= :lastMonth '
					+ 'AND Contact.E_CL3PF_AGNTNUM__c != null '
					+ 'AND Contact.E_CL3PF_VALIDFLAG__c = \'1\' '
					+ 'AND Contact.E_CL3PF_AGTYPE__c != \'01\' '
					+ 'AND Contact.E_CL3PF_AGTYPE__c != \'07\' '
					+ 'AND Contact.E_CL3PF_AGTYPE__c != \'08\' '
					+ 'AND Contact.E_CL3PF_AGTYPE__c != \'99\' '
					+ soqlWhere
					+ 'LIMIT 5000';

		system.debug(soql);
		return Database.query(soql);
	}

	/**
	 * 募集人IDに紐付くユーザをID管理とともに取得
	 * JIRA_#51 170209
	 * [IRIS-734]
	 */
	public static List<User> getRecsAgentIdWithEIDC(Set<Id> contactIdset) {
		// ToDo リストを持たない
		return [SELECT
						Id,
						Contact.Id,
						Contact.E_CL3PF_AGNTNUM__c,	// 募集人コード
						AgentCode__c,				// 募集人コード（パイプ無し）
						UserName, //P16-0003 Atria対応開発
						Email, //P16-0003 Atria対応開発
						E_ZWEBID__c, 
						IsActive,
						LastLoginDate,
						LastModifiedDate,
						LastPasswordChangeDate,
						//P16-0003 Atria対応開発 ZIDTYPE__c,AtriaCommissionAuthorityEditable__c を追加
						(SELECT 
							Id, ZSTATUS01__c, ZIDTYPE__c, ZDSPFLAG02__c, AtriaCommissionAuthorityEditable__c,
							ZDSPFLAG01__c, IsNotNotification__c
						 FROM E_IDCPFs__r)
					FROM
						User
					WHERE
						Contact.Id in :contactIdset];
		//return userList;
	}

	// /**
	//  * ユーザIDに紐付くContactIdを取得
	//  */
	// public static Map<Id, Id> getRecsContactById(Set<Id> userId){
	// 	Map<Id, Id> userMap = new Map<Id, Id>();
	// 		for(User u : [SELECT Id,Contact.Id FROM User WHERE Id in:userId]){
	// 			userMap.put(u.Id,u.Contact.Id);
	// 		}
	// 		return userMap;
	// }

	// R1.1特別対応
	/**
	 * ユーザIDに紐付くContactId,E_CL3PF_ZAGMANGR__cを取得
	 */
	public static Map<Id, User> getRecsContactById(Set<Id> userId){
		Map<Id, User> userMap = new Map<Id, User>();
			for(User u : [SELECT Id,Contact.Id,Contact.E_CL3PF_ZAGMANGR__c FROM User WHERE Id in:userId]){
				userMap.put(u.Id,u);
			}
			system.debug('userId =' + userId );
			system.debug('userMap.keyset()' + userMap.keyset() );

			return userMap;
	}
	/**
	 * ユーザデータをユーザIdから取得する
	 * @param Set<Id>: ユーザID
	 * @return User: ユーザ情報
	 */
	public static List<User> getUserRecByUserId(Set<Id> userIdSet){
		List<User> uList =
			[SELECT
				Name
				,UserType
				,Contact.E_AccParentName__c
			FROM User
			WHERE Id In:userIdSet
			];
		return uList;
	}



	/**
	 *ユーザレコードとＩＤ管理レコードをContactIdから取得
	 *@param  contactId:募集人ＩＤ
	 *@return 検索されたUserレコード
	 */
	public static List<User> getUserRecsByContactIds(Set<Id> contactIdSet){
		List<User> recs =
		[SELECT
				AccountId,
				Username,
				ContactId,
				UserType,
				Email,
				LastLoginDate,
				E_UseTempBasicPW__c,
				E_UseExistingBasicPW__c,
				E_TempBasicPWModifiedDate__c,
				E_ZWEBID__c,
				isActive,
				Contact.Id,
				Contact.Name,
				Contact.LastName,
				Contact.FirstName,
				Contact.E_CLTPF_CLNTNUM__c,
				Contact.E_CLTPF_ZCLKNAME__c,
				Contact.E_COMM_ZCLADDR__c,
				Contact.E_CLTPF_ZCLKADDR__c,
				Contact.E_CLTPF_CLTPHONE01__c,
				Contact.E_CLTPF_CLTPHONE02__c,
				Contact.E_CLTPF_FAXNO__c,
				Contact.E_CADPF__r.Name,
				Contact.Account.ParentId,
				Contact.SystemOriginFlag__c,
				(SELECT Id, ZSTATUS01__c FROM E_IDCPFs__r)	//20171206 項目追加
		FROM User
		WHERE ContactId In :contactIdSet
		ORDER BY Contact.SystemOriginFlag__c DESC
		];

		return recs;
	}

	/**
	 * ユーザデータをユーザIDから取得する 2018/03/07追加 代理店アンケート用
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static User getUserRecordByUserId(ID userId){
		User user = new User();
		user = [SELECT id,
					   Email,
					   Contact.Name,
					   ContactId,
					   Contact.Account.E_CL2PF_ZAGCYNUM__c,
					   Contact.Account.Parent.Name,
					   Contact.E_CL3PF_ZAGCYNUM__c,
					   Contact.Account.Parent.E_CL1PF_ZHEADAY__c,
					   Contact.Account.ParentId,
					   Contact.Account.Name,
					   Contact.Account.Owner.Name,
					   Contact.Account.E_CL2PF_BRANCH__c,
					   Contact.Account.Owner.UnitName__c,
					   Contact.Account.Owner.Unit__c,
					   Contact.Account.KSECTION__c,
					   Contact.Account.Parent.E_CL1PF_KCHANNEL__c,
					   Contact.Account.Parent.E_CL1PF_ZSOURCE__c,
					   Contact.Account.OwnerId
				FROM User 
				Where id =: userId
			   ];
		return user;

	}

	/**
	 * ユーザデータを権限セット付でユーザIdから取得する
	 * @param userId: ユーザID
	 * @return User: ユーザ情報
	 */
	public static Map<Id, User> getUserWithPermissions(Set<Id> uIds){
		return new Map<Id, User>(
							[SELECT Id
									,Name
									,isActive
									,(Select Id
											, PermissionSetId
											, PermissionSet.Name
											, AssigneeId
									  From PermissionSetAssignments
									  Where PermissionSet.ProfileId = null		//ProfileIdがNullのものが権限セット
										AND PermissionSet.Name in :E_Const.PERMISSIONSET_NAMES)
							 FROM User
							 WHERE Id in :uIds
							]);
	}

	/**
	 * [挙積情報] 営業部支社名と一致するユーザと、それに紐づく挙績情報を取得
	 * @param  brName 営業部支社名
	 * @return List<User>
	 */
	public static List<User> getUserRecsWithEmployeeSales(String sWhere,String busdate){
	 	String soql = 'SELECT'
				+' Id'
				+ ' ,Name'
				+ ' ,MemberNo__c'
				+ ',(SELECT Id'
					+ ' ,Budget_FSYTD__c'
					+ ' ,NBWANP_FSYTD__c'
					+ ' ,AchiveRate_FSYTD__c'
					+ ' ,Budget_ProtectionYTD__c'
					+ ' ,NBWANP_ProtectionYTD__c'
					+ ' ,AchiveRate_ProtectionYTD__c'
					+ ' ,Budget_BACOLIYTD__c'
					+ ' ,NBWANP_BACOLIYTD__c'
					+ ' ,AchiveRate_BACOLIYTD__c'
					+ ' ,Budget_TotalYTD__c'
					+ ' ,NBWANP_TotalYTD__c'
					+ ' ,AchiveRate_TotalYTD__c'
					+ ' ,ActiveAgency__c'
					+ ' ,ActiveAgent__c'
					+ ' ,MR__c'
				+ ' FROM E_EmployeeSalesResults__r'
				+ ' Where BusinessDate__c =: busdate)'
				+ ',(Select id From E_IDCPFs__r limit 1)'
				+ ' FROM User'
				+ ' WHERE IsActive = true'
				+ sWhere;
		return Database.query(soql);
	}

}