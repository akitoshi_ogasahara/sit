global with sharing class E_InveValueController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_ITHPF__c record {get{return (E_ITHPF__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_InveValueExtender getExtender() {return (E_InveValueExtender)extender;}
	
	
	public dataTableITFPF dataTableITFPF {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_InveValueController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1374');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1374',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','inve_search,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','投資運用レポート');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','https://www.nnlife.co.jp/mf/reports');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1412');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1412',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_InveValue');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','89');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1376');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1376',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','投資信託運用レポート');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','https://www.nnlife.co.jp/mf/reports');
        tmpPropMap.put('p_target','_blank');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink ');
		tmpPropMap.put('Component__Width','116');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1413');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1413',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('ithpfId','{!record.Id}');
        tmpPropMap.put('executeKind','{!Extender.executeKind}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','200');
		tmpPropMap.put('Component__id','Component1097');
		tmpPropMap.put('Component__Name','EInvestmentGraph');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1097',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','評価金額と比較する');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_InveCompare?id={!record.id}&menu=0');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn-small btn-arrowLink');
		tmpPropMap.put('Component__Width','109');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1399');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1399',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1398');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1398',tmpPropMap);


		SObjectField f;

		f = E_ITHPF__c.fields.ContractorName__c;
		f = E_ITHPF__c.fields.ZCLNTCDE__c;
		f = E_ITFPF__c.fields.ZASSTNAM__c;
		f = E_ITFPF__c.fields.ZBRNDNMS__c;
		f = E_ITFPF__c.fields.ZBASCPRC__c;
		f = E_ITFPF__c.fields.ZPRINUNT__c;
		f = E_ITFPF__c.fields.ZITUNIT__c;
		f = E_ITFPF__c.fields.ZCURRPRC__c;
		f = E_ITFPF__c.fields.ZBRNDPER03__c;
		f = E_ITFPF__c.fields.ZCHGRATE__c;
		f = E_ITFPF__c.fields.ZASSTCLS__c;
		f = E_ITFPF__c.fields.ZBRNDCDE__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_ITHPF__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_ITHPF__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('ContractorName__c');
			mainQuery.addFieldAsOutput('ZCLNTCDE__c');
			mainQuery.addFieldAsOutput('Contractor__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			dataTableITFPF = new dataTableITFPF(new List<E_ITFPF__c>(), new List<dataTableITFPFItem>(), new List<E_ITFPF__c>(), null);
			listItemHolders.put('dataTableITFPF', dataTableITFPF);
			query = new SkyEditor2.Query('E_ITFPF__c');
			query.addFieldAsOutput('ZASSTNAM__c');
			query.addFieldAsOutput('ZBRNDNMS__c');
			query.addFieldAsOutput('ZBASCPRC__c');
			query.addFieldAsOutput('ZPRINUNT__c');
			query.addFieldAsOutput('ZITUNIT__c');
			query.addFieldAsOutput('ZCURRPRC__c');
			query.addFieldAsOutput('ZBRNDPER03__c');
			query.addFieldAsOutput('ZCHGRATE__c');
			query.addFieldAsOutput('ZINVAMT__c');
			query.addWhere('E_ITHPF__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dataTableITFPF', 'E_ITHPF__c');
			dataTableITFPF.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dataTableITFPF', query);
			
			registRelatedList('E_ITFPFs__r', 'dataTableITFPF');
			
			SkyEditor2.Query dataTableITFPFQuery = queryMap.get('dataTableITFPF');
			dataTableITFPFQuery.addSort('ZASSTCLS__c',True,True).addSort('ZBRNDCDE__c',True,True);
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_InveValueExtender(this);
			init();
			
			dataTableITFPF.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class dataTableITFPFItem extends SkyEditor2.ListItem {
		public E_ITFPF__c record{get; private set;}
		@TestVisible
		dataTableITFPFItem(dataTableITFPF holder, E_ITFPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTableITFPF extends SkyEditor2.ListItemHolder {
		public List<dataTableITFPFItem> items{get; private set;}
		@TestVisible
			dataTableITFPF(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dataTableITFPFItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableITFPFItem(this, (E_ITFPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}