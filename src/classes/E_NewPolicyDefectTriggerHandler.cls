public with sharing class E_NewPolicyDefectTriggerHandler {
	public E_NewPolicyDefectTriggerHandler() {
		
	}
	/**
	 * 新契約不備Insertトリガー処理
	 */
	public void onAfterInsert(List<E_NewPolicyDefect__c > newList) {
		Set<Id> upd = new Set<Id>();
		for (E_NewPolicyDefect__c newRec :newList){
			upd.add(newRec.E_NewPolicy__c);
		}
		E_NewPolicyTriggerHandler handler = new E_NewPolicyTriggerHandler();
		handler.setStstusDate([Select id,Status__c,SystemStatus__c,StatusDate__c,SyncDate__c,KFRAMEIND__c,GroupKey__c From E_NewPolicy__c Where id in :upd]);
		handler.setGroupStatusDate([Select id,Status__c,SystemStatus__c,StatusDate__c,SyncDate__c,KFRAMEIND__c,GroupKey__c From E_NewPolicy__c Where id in :upd]);
	}
	/**
	 * 新契約不備Updateトリガー処理
	 */
	public void onAfterUpdate(Map<Id, E_NewPolicyDefect__c> newMap, Map<Id, E_NewPolicyDefect__c> oldMap) {
		Set<Id> upd = new Set<Id>();
		for (E_NewPolicyDefect__c newRec :newMap.values()){
			upd.add(newRec.E_NewPolicy__c);
		}
		E_NewPolicyTriggerHandler handler = new E_NewPolicyTriggerHandler();
		handler.setStstusDate([Select id,Status__c,SystemStatus__c,StatusDate__c,SyncDate__c,KFRAMEIND__c,GroupKey__c From E_NewPolicy__c Where id in :upd]);
		handler.setGroupStatusDate([Select id,Status__c,SystemStatus__c,StatusDate__c,SyncDate__c,KFRAMEIND__c,GroupKey__c From E_NewPolicy__c Where id in :upd]);
	}
}