@isTest
private class TestE_UserDao {

	/**
	 * ユーザデータをユーザIdから取得
	 */
	@isTest static void getUserRecByUserIdTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDao.getUserRecByUserId(us.Id);
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ユーザデータをユーザIdから取得(権限セット付き)
	 */
	@isTest static void getUserWithPermissionsByUserIdTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDao.getUserWithPermissionsByUserId(us.Id);
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ユーザデータをユーザIdセットから取得
	 */
	@isTest static void getUserByIdsTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		Set<Id> userIdSet = new Set<Id>();
		userIdSet.add(us.Id);

		Test.startTest();
		Map<Id, User> result = E_UserDao.getUserByIds(userIdSet);
		Test.stopTest();

		System.assertEquals(userIdSet, result.keySet());
	}

	/**
	 * ユーザデータをユーザIdセットから取得(権限セット付き)
	 */
	@isTest static void getUserWithPermissionsTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		Set<Id> userIdSet = new Set<Id>();
		userIdSet.add(us.Id);

		Test.startTest();
		Map<Id, User> result = E_UserDao.getUserWithPermissions(userIdSet);
		Test.stopTest();

		System.assertEquals(userIdSet, result.keySet());
	}

	/**
	 * NNLink関連の権限セットのMap
	 */
	@isTest static void getPermissionSetForNNLinkTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		Map<String, PermissionSet> result = E_UserDao.getPermissionSetForNNLink();
		Test.stopTest();

		System.assertEquals(E_Const.PERMISSIONSET_NAMES.size(), result.size());
	}

	/**
	 * MRユーザ取得 Map<String, Id>
	 */
	@isTest static void getMrUserMapTest(){
		//データ作成
		User us = createUser02('E_EmployeeStandard');

		Test.startTest();
		Map<String, Id> result = E_UserDao.getMrUserMap();
		Test.stopTest();

		System.assertEquals(us.Id, result.get('testUs'));
	}

	/**
	 * MRユーザ取得 Map<String, User>
	 */
	@isTest static void getMrUserRecordMapTest(){
		//データ作成
		User us = createUser02('E_EmployeeStandard');

		Test.startTest();
		Map<String, User> result = E_UserDao.getMrUserRecordMap();
		Test.stopTest();

		System.assertEquals(us.Id, result.get('testUs').Id);
	}

	/**
	 * MRコードからUserId取得
	 * 成功
	 */
	@isTest static void getActiveUserIdByMRCDTest01(){
		//データ作成
		User us = createUser02('E_EmployeeStandard');

		Test.startTest();
		Id result = E_UserDao.getActiveUserIdByMRCD('testUs');
		Test.stopTest();

		System.assertEquals(us.Id, result);
	}

	/**
	 * MRコードからUserId取得
	 * 失敗
	 */
	@isTest static void getActiveUserIdByMRCDTest02(){
		//データ作成
		User us = createUser02('E_EmployeeStandard');
		us.MRCD__c = null;
		update us;

		Test.startTest();
		Id result = E_UserDao.getActiveUserIdByMRCD('testUs');
		Test.stopTest();

		System.assertEquals(null, result);
	}

	/**
	 * 拠点長ユーザ取得
	 */
	@isTest static void getMrManagerMapTest(){
		//データ作成
		User us = createUser02('拠点長');

		Test.startTest();
		Map<String, Id> result = E_UserDao.getMrManagerMap();
		Test.stopTest();

		System.assertEquals(null, result.get('testUs'));
	}

	/**
	 * 本社営業部拠点長ユーザ取得
	 */
	@isTest static void getHQManagerIdTest(){
		//データ作成
		User us = createUser02('本社営業部拠点長');

		Test.startTest();
		Id result = E_UserDao.getHQManagerId();
		Test.stopTest();

		System.assertNotEquals(null, result);
	}

	/**
	 * MRコードからUser取得
	 */
	@isTest static void getUserRecByMRCodeTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDao.getUserRecByMRCode('testUs');
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ContactIdからUser.Id取得
	 */
	@isTest static void getRecByContactIdTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		Id result = E_UserDao.getRecByContactId(us.ContactId);
		Test.stopTest();

		System.assertEquals(us.Id, result);
	}

	/**
	 * User情報Update
	 */
	@isTest static void updateUserRecTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		us.LastName = 'テスト02';

		Test.startTest();
		E_UserDao.updateUserRec(us);
		Test.stopTest();

		us = [select Id, LastName from user where Id = :us.Id];
		System.assertEquals('テスト02', us.LastName);
	}

	/**
	 * MRコードリストからUserリスト取得
	 */
	@isTest static void getUserRecsByMRCodeTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		List<String> mrCodeList = new List<String>();
		mrCodeList.add(us.MRCD__c);

		Test.startTest();
		List<User> result = E_UserDao.getUserRecsByMRCode(mrCodeList);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * ユーザ無効化
	 */
	@isTest static void updateUserRecsActiveFalseTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		Set<Id> userIdSet = new Set<Id>();
		userIdSet.add(us.Id);

		Test.startTest();
		UserTriggerHandler.isSkipTriggerActions = TRUE;
		E_UserDao.updateUserRecsActiveFalse(userIdSet);
		Test.stopTest();

		us = [select Id, LastName, isActive from user where Id = :us.Id];
		System.assertEquals(false, us.isActive);
	}

	/**
	 * ContactIdセットからUserリスト取得
	 */
	@isTest static void getRecsAgentIdTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		Set<Id> contactIdSet = new Set<Id>();
		contactIdset.add(us.ContactId);

		Test.startTest();
		List<User> result = E_UserDao.getRecsAgentId(contactIdset);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * Idセットから無効化対象ユーザの項目を取得
	 */
	@isTest static void getFreezeUserListByIdsTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		createIDCPF(us.Id);

		Set<Id> userIdSet = new Set<Id>();
		userIdset.add(us.Id);

		Test.startTest();
		List<User> result = E_UserDao.getFreezeUserListByIds(userIdset);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * PowerPartnerライセンスでMRが担当支社となっている事務所の募集人ユーザを取得
	 */
	@isTest static void getPartnerUserRecByUserTypeTest(){
		//データ作成
		User us = createUser03('E_PartnerCommunity');
		createIDCPF(us.Id);

		Set<Id> userIdSet = new Set<Id>();
		userIdset.add(us.Id);

		Test.startTest();
		List<User> result = E_UserDao.getPartnerUserRecByUserType('Name', false);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * ContactIdセットからID管理とともにUserリスト取得
	 */
	@isTest static void getRecsAgentIdWithEIDCTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		Set<Id> contactIdSet = new Set<Id>();
		contactIdset.add(us.ContactId);

		Test.startTest();
		List<User> result = E_UserDao.getRecsAgentIdWithEIDC(contactIdset);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}
	/**
	 * ユーザデータをユーザIdセットから取得
	 */
	@isTest static void getUserByIdsWithBatchTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		Set<Id> userIdSet = new Set<Id>();
		userIdSet.add(us.Id);

		Test.startTest();
		Map<Id, User> result = E_UserDao.getUserByIdsWithBatch(userIdSet);
		Test.stopTest();

		System.assertEquals(userIdSet, result.keySet());
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	/**
	 * ユーザ作成
	 * Contactあり
	 */
	static User createUser01(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(profileName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.MRCD__c = 'testUs';
			us.ContactId = con.Id;
			insert us;
		}
		return us;
	}

	/**
	 * ユーザ作成
	 * Contactなし
	 */
	static User createUser02(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(profileName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.MRCD__c = 'testUs';
			insert us;
		}
		return us;
	}

	/**
	 * ID管理作成
	 */
	static void createIDCPF(Id userId){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		E_IDCPF__c idcpf = new E_IDCPF__c();
		system.runAs(thisUser){
			idcpf.ZSTATUS01__c = '1';
			idcpf.User__c = userId;
			idcpf.OwnerId = userId;
			insert idcpf;
		}
	}


	//親取引先作成
	static Id createParentAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	//親取引先をもつ取引先作成
	static Id createChildAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.ParentId = createParentAccount();
		insert acc;
		return acc.Id;
	}

	//MRが担当支社となっている事務所の募集人作成
	static Contact createContactDataMR(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createChildAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	/**
	 * ユーザ作成
	 * Contactあり
	 * MRが担当支社
	 */
	static User createUser03(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactDataMR();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(profileName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.MRCD__c = 'testUs';
			us.ContactId = con.Id;
			insert us;
		}
		return us;
	}
}