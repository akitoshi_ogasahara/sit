public class I_ContentSearchResult implements Comparable {

	public static final Integer VIEW_LENGTH = 120;
	public static final Integer VIEW_MARGIN_LENGTH = 3;
	public static final String RESULT_TYPE_PAGE = 'ページ';
	public static final String RESULT_TYPE_NOTIFYCATION = 'お知らせ';
	public static final String RESULT_TYPE_QUESTION = 'よくあるご質問';
	public static final String RESULT_TYPE_FEATURE = '特集';
	public static final String SORT_TYPE_PAGE_NAME = 'pagename';
	public static final String SORT_TYPE_PAGE_TYPE = 'type';
	private static String sortType = SORT_TYPE_PAGE_NAME;
	private static Boolean sortIsAsc = true;

	public SObject record {get; set;}
	public String name {get; set;}
	public String context {get; set;}
	public String url {get; set;}
	public String type {get; set;}
	private I_KeywordMarker marker;

	public void setMarker(I_KeywordMarker marker) {
		this.marker = marker;
	}

	public Boolean hasAccess() {
		return accesses(this.record);
	}

	public static Boolean accesses(SObject rec) {
		Boolean hasAccess;
		if (rec instanceof I_MenuMaster__c) {
			I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(((I_MenuMaster__c)rec).Id);
			if (menuItem != null) {
				hasAccess = menuItem.rendered;
			} else {
				hasAccess = false;
			}
		} else if (rec instanceof I_PageMaster__c) {
			I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(((I_PageMaster__c)rec).Menu__c);
			if (menuItem != null) {
				hasAccess = menuItem.rendered;
			} else {
				hasAccess = false;
			}
		} else if (rec instanceof I_ContentMaster__c) {
			I_ContentMaster__c master = (I_ContentMaster__c)rec;
			E_AccessController accessor = E_AccessController.getInstance();
			//代理店ユーザアクセス時、社員向けページ表示用コンテンツは非表示 20180314 START
			if(accessor.isAgent() && master.Category__c != null && 
				(master.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_MRDOCUMENT) || master.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_RECRUIT) || master.Category__c.contains(I_Const.CMSCONTENTS_CATEGORY_ADVANCEINFO))){
				hasAccess = false;
			//社員向けページ表示用コンテンツは非表示 20180314 EMD
			// パンフレット等　社員用カテゴリの場合
			} else if(accessor.isAgent() && master.PamphletCategory__c != null && master.isEmployeeCategory__c){
				hasAccess = false;
			
			} else if (String.isBlank(master.CanDisplayVia__c)) {
				hasAccess = true;
			} else {
				String rights = master.CanDisplayVia__c;
				if (accessor.isEmployee()) {
					// 社員のみ
					hasAccess = rights.indexOf(I_Const.DISP_RIGHT_EMPLOYEE) >= 0;
				} else if (accessor.isAgent()) {
					// 代理店のみ
					hasAccess = rights.indexOf(I_Const.DISP_RIGHT_AGENCY) >= 0;
				} else if (accessor.isViaInternet()) {
					// インターネット経由
					hasAccess = rights.indexOf(I_Const.ACCESS_VIA_INTERNET) >= 0;
				} else if (accessor.isCommonGateway()) {
					// 共同GW経由
					hasAccess = rights.indexOf(I_Const.ACCESS_VIA_CMN_GW) >= 0;
				} else {
					// いずれにも一致しなかった場合
					hasAccess = true;
				}
			}
		} else {
			hasAccess = false;
		}
		return hasAccess;
	}

	public Boolean isMatched() {
		Boolean result = false;
		if (this.marker != null) {
			result = this.marker.wholeMatches(new List<String>{this.name, this.context});
		}
		return result;
	}

	public String getMarkedName() {
		return this.marker != null ? this.marker.mark(this.name, -1) : this.name;
	}

	public String getMarkedContext() {
		return this.marker != null ? this.marker.mark(this.context, VIEW_LENGTH) : this.context;
	}

	public Integer compareTo(Object compareTo) {
		I_ContentSearchResult row = (I_ContentSearchResult)compareTo;
		Integer result = 0;
		if (I_ContentSearchResult.sortBy(SORT_TYPE_PAGE_NAME)) {
			result = I_Util.compareToString(this.name, row.name);
		} else if (I_ContentSearchResult.sortBy(SORT_TYPE_PAGE_TYPE)) {
			result = I_Util.compareToString(this.type, row.type);
		}
		return I_ContentSearchResult.isAsc() ? result : result * -1;
	}

	public static void setSortOption(String sortType, Boolean sortIsAsc) {
		I_ContentSearchResult.sortType = sortType;
		I_ContentSearchResult.sortIsAsc = (sortIsAsc == true);
	}

	public static Boolean sortBy(String sortType) {
		return I_ContentSearchResult.sortType == sortType;
	}

	public static Boolean isAsc() {
		return I_ContentSearchResult.sortIsAsc;
	}

	public static I_ContentSearchResult build(SObject record) {
		I_ContentSearchResult result = new I_ContentSearchResult();
		result.record = record;
		return result;
	}

	/**
	 * I_PageMaster__cをもとに結果データを構築する
	 * @param I_PageMaster__c record
	 * @return I_ContentSearchResult
	 */
	public static I_ContentSearchResult build(I_PageMaster__c record) {
		I_ContentSearchResult result = new I_ContentSearchResult();
		result.name = (String.isNotBlank(record.name) ? record.name : '').stripHtmlTags();
		result.record = record;
		result.context = null;

		// メニューレコードがページレコードの遷移先を保持しているので
		// 取得できた場合のみ遷移先を設定する。
		I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(record.Menu__c);
		if (menuItem != null) {
			// 特集データだった場合
			if (menuItem.record.MainCategory__c == I_Const.MENU_MAIN_CATEGORY_LIBLARY && menuItem.record.subCategory__c == I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE && !record.Default__c) {
				PageReference ref = new PageReference('IRIS_Feature');
				ref.getParameters().put(I_Const.URL_PARAM_PAGEKEY, record.page_unique_key__c);
				result.url = ref.getUrl();
				result.type = RESULT_TYPE_FEATURE;
			}
			// 特集データ以外だった場合
			else {
				result.url = menuItem.getDestUrl();
				result.type = RESULT_TYPE_PAGE;
			}
		}

		return result;
	}

	/**
	 * I_MenuMaster__cをもとに結果データを構築する
	 * @param I_MenuMaster__c record
	 * @return I_ContentSearchResult
	 */
	public static I_ContentSearchResult build(I_MenuMaster__c record) {
		I_ContentSearchResult result = new I_ContentSearchResult();
		result.name = (String.isNotBlank(record.name) ? record.name : '').stripHtmlTags();
		result.context = null;
		result.type = RESULT_TYPE_PAGE;
		result.record = record;

		// メニューレコード自身のメニューデータが作成されている場合に遷移先を設定する。
		I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(record.id);
		if (menuItem != null) {
			result.url = menuItem.getDestUrl();
		}

		return result;
	}

	/**
	 * I_ContentMaster__cをもとに結果データを構築する
	 * @param I_ContentMaster__c record
	 * @return I_ContentSearchResult
	 */
	public static I_ContentSearchResult build(I_ContentMaster__c record) {
		I_ContentSearchResult result = new I_ContentSearchResult();
		result.name = (String.isNotBlank(record.name) ? record.name : '').stripHtmlTags();
		result.record = record;

		// お知らせデータであった場合
		if (record.ParentContent__c != null && record.ParentContent__r.Page__c != null && record.ParentContent__r.Page__r.Menu__c != null && record.ParentContent__r.Page__r.Menu__r.Name == 'お知らせ') {
			//20180214 資料発送申込対応 START
			E_AccessController accessor = E_AccessController.getInstance();
			if(accessor.isAgent() && accessor.user.Contact.Account.CannotBeOrder__c && I_Const.DR_INFO_LABEL.contains(record.InfoLabel__c)){
				return result;
			}
			//20180214 資料発送申込対応 END

			PageReference ref = new PageReference(record.ParentContent__r.Page__r.Menu__r.DestUrl__c);
			Map<String, String> params = ref.getParameters();
			params.put(I_Const.URL_PARAM_PAGE, record.ParentContent__r.Page__r.Menu__c);
			params.put('infoid', String.valueOf(record.id));
			result.url = ref.getUrl();
			result.type = RESULT_TYPE_NOTIFYCATION;
			// よくあるご質問、お知らせのみcontextを表示
			result.context = (String.isNotBlank(record.Content__c) ? record.Content__c : '').stripHtmlTags();
		}
		// よくあるご質問データであった場合
		else if (record.ParentContent__c != null && record.ParentContent__r.Page__c != null && record.ParentContent__r.Page__r.Menu__c != null && record.ParentContent__r.Page__r.Menu__r.MainCategoryAlias__c == 'よくあるご質問') {
			String domId = null;
			String DOM_ID_PREFIX = 'iris-cms-';
			if (String.isBlank(record.Dom_Id__c)) {
				domId = DOM_ID_PREFIX + String.valueOf(record.id);
			} else {
				domId = DOM_ID_PREFIX + record.Dom_id__c;
			}
			PageReference ref = new PageReference('IRIS');
			ref.setAnchor(domId);
			Map<String, String> params = ref.getParameters();
			params.put('page', record.ParentContent__r.Page__c);
			params.put('irismn', String.valueOf(record.ParentContent__r.Page__r.Menu__c));
			result.url = ref.getUrl();
			result.type = RESULT_TYPE_QUESTION;
			// よくあるご質問、お知らせのみcontextを表示
			result.context = (String.isNotBlank(record.Content__c) ? record.Content__c : '').stripHtmlTags();
		}
		// URLデータであった場合
		else if (record.ClickAction__c == 'URL') {
			result.type = RESULT_TYPE_PAGE;
			result.url = record.LinkURL__c;
			result.context = null;
		}
		return result;
	}

}