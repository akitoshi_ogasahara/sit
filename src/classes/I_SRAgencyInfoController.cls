public with sharing class I_SRAgencyInfoController {

    public Id paramSalesResultsId {get; set;}               // 代理店挙積情報ID
    public String paramViewType {get; set;}                 // 表示タイプ AH（代理店を情報を表示）AY（事務所情報を表示）AT（募集人情報を表示）

    transient public E_AgencySalesResults__c agencySalesResults {get; set;}         // 代理店挙積情報
    transient public List<E_Representative__c> representatives {get; set;}          // 法人代表者/責任者
    public List<String> repGroupList {get; set;}                                    // 法人代表者/責任者 グループ
    public Map<String, Integer> repGroupMap {get; set;}                             // 法人代表者/責任者 グループ
    transient public List<E_VansInsurance__c> vansInsurances {get; set;}            // 乗合保険会社
    public Boolean IsShow {get; set;}                                               // 代表者/責任者 乗合保険会社の表示
    //母店事務所名の桁数が間違っていたための暫定対応
    transient public String motherOfficeName {get;set;}                         //母店事務所名


    public static final String CATEGORY_PATTERN1 = '代表者';
    public static final String CATEGORY_PATTERN2 = '業務管理責任者';
    public static final String CATEGORY_PATTERN3 = '教育管理責任者';
    public static final String CATEGORY_PATTERN4 = '教育責任者';

    //データ基準日
    public String bizDate{
        get{
            if(bizDate != null) return this.bizDate;
            String inquiryD = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
            String year = inquiryD.left(4);
            String month = inquiryD.mid(4, 1)+inquiryD.mid(5, 1);
            String day = inquiryD.mid(6, 1)+inquiryD.mid(7, 1);
            return year+'/'+month+'/'+day;
        } set;
    }

    /**
     * コンストラクタ
     */
    public I_SRAgencyInfoController(){
        this.paramSalesResultsId    = null;                                         // 代理店挙積情報ID
        this.paramViewType          = '';                                           // 表示タイプ

        //母店事務所名の桁数が間違っていたための暫定対応
        this.motherOfficeName = '';

        this.agencySalesResults     = new E_AgencySalesResults__c();                // 代理店挙積情報取得
        this.representatives        = new List<E_Representative__c>();              // 法人代表者/責任者
        this.repGroupList           = new List<String>();                           // 法人代表者/責任者 グループ
        this.repGroupMap            = new Map<String, Integer>();                   // 法人代表者/責任者 グループ
        this.vansInsurances         = new List<E_VansInsurance__c>();               // 乗合保険会社
        this.IsShow                 = false;                                        // 代表者/責任者 乗合保険会社の表示

    }

    /**
     * 設定値の取得
     */
    public void getParameter(){
        repGroupList.add(CATEGORY_PATTERN1);
        repGroupList.add(CATEGORY_PATTERN2);
        repGroupList.add(CATEGORY_PATTERN4);
        repGroupMap.put(CATEGORY_PATTERN1, 0);
        repGroupMap.put(CATEGORY_PATTERN2, 0);
        repGroupMap.put(CATEGORY_PATTERN4, 0);
        this.IsShow                 = false;                                        // 代表者/責任者 乗合保険会社の表示

        // 代理店挙積情報 取得
        this.agencySalesResults     = E_AgencySalesResultsDao.getRecTypeEById(this.paramSalesResultsId);
        if(this.paramViewType == 'AH'){
            if(this.agencySalesResults.XHAH_KCHANNEL__c != 'IB' && this.agencySalesResults.XHAH_KCHANNEL__c != 'CB'){
                this.IsShow                 = true;                                     // 代表者/責任者 乗合保険会社の表示
                // 法人代表者/責任者 取得
                this.representatives        = E_RepresentativeDao.getRecsByAsrId(this.agencySalesResults.Id);
                // 乗合保険会社 取得
                this.vansInsurances         = E_VansInsuranceDao.getRecsByAsrId(this.agencySalesResults.Id);
                // 法人代表者/責任者 グループ設定
                //for(E_Representative__c rep: this.representatives){
                //    if((rep.Category__c == CATEGORY_PATTERN1)
                //    || (rep.Category__c == CATEGORY_PATTERN2)
                //    || (rep.Category__c == CATEGORY_PATTERN3)){
                //        repGroupMap.put(rep.Category__c, repGroupMap.get(rep.Category__c) + 1);
                //    }
                //}

                //修正山田
                for(E_Representative__c rep: this.representatives){
                    if((rep.Category__c == CATEGORY_PATTERN1) || (rep.Category__c == CATEGORY_PATTERN2)){
                    	repGroupMap.put(rep.Category__c, repGroupMap.get(rep.Category__c) + 1);
                    }
                    if((rep.Category__c == CATEGORY_PATTERN3) || (rep.Category__c == CATEGORY_PATTERN4)){
                    	rep.Category__c = CATEGORY_PATTERN4;
                        repGroupMap.put(CATEGORY_PATTERN4, repGroupMap.get(CATEGORY_PATTERN4) + 1);
                    }
                }
            }
        }else{
            List<E_AgencySalesResults__c> agencySalesResultsAH = E_AgencySalesResultsDaoWithout.getRecordsByParentAccountId(
                                                            this.agencySalesResults.ParentAccount__c, 'AH', this.agencySalesResults.BusinessDate__c);
            if(agencySalesResultsAH.size() > 0){
                E_AgencySalesResults__c asrAH = agencySalesResultsAH[0];
                if(asrAH.XHAH_KCHANNEL__c != 'IB' && asrAH.XHAH_KCHANNEL__c != 'CB'){
                    this.IsShow                 = true;                                     // 代表者/責任者 乗合保険会社の表示
                    // 法人代表者/責任者 取得
                    this.representatives        = E_RepresentativeDaoWithout.getRecsByAsrId(asrAH.Id);
                    // 乗合保険会社 取得
                    this.vansInsurances         = E_VansInsuranceDaoWithout.getRecsByAsrId(asrAH.Id);
                    // 法人代表者/責任者 グループ設定
                    //for(E_Representative__c rep: this.representatives){
                    //    if((rep.Category__c == CATEGORY_PATTERN1)
                    //    || (rep.Category__c == CATEGORY_PATTERN2)
                    //    || (rep.Category__c == CATEGORY_PATTERN3)){
                    //        repGroupMap.put(rep.Category__c, repGroupMap.get(rep.Category__c) + 1);
                    //    }
                    //}

                	for(E_Representative__c rep: this.representatives){
                    	if((rep.Category__c == CATEGORY_PATTERN1) || (rep.Category__c == CATEGORY_PATTERN2)){
                    		repGroupMap.put(rep.Category__c, repGroupMap.get(rep.Category__c) + 1);
                    	}
                    	if((rep.Category__c == CATEGORY_PATTERN3) || (rep.Category__c == CATEGORY_PATTERN4)){
                    		rep.Category__c = CATEGORY_PATTERN4;
                        	repGroupMap.put(CATEGORY_PATTERN4, repGroupMap.get(CATEGORY_PATTERN4) + 1);
                   		}
                	}
                }
            }
        }
        //母店事務所名の桁数が間違っていたための暫定対応
        Account mAcc = E_AccountDaoWithout.getRecByAccCode(agencySalesResults.XHAH_ZBUSAGCY__c);
        motherOfficeName = (mAcc == null)? '' : mAcc.Name;
    }
}