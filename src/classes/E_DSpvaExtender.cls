global with sharing class E_DSpvaExtender extends E_AbstractViewExtender {
	E_DSpvaController extension;

	/** ページタイトルSPVA */
	private static final String PAGE_TITLE_SPVA = '契約内容個別照会(一時払変額年金保険)';
	/** ページタイトルSPVWL */
	private static final String PAGE_TITLE_SPVWL = '契約内容個別照会(一時払変額終身保険)';
	/** 保険種類ST */
	private static final String COMM_CRTABLE_ST = 'ST';
	/** 保険種類SR */
	private static final String COMM_CRTABLE_SR = 'SR';

	//ステップアップ金額のディスクレーマー
	public String disclaimerStepUp {get;set;}

	private static final String TYPE_ZREV = '積立金額の推移';
	private static final String TYPE_SUMI = '基本給付金額/基本保険金額の推移';
	private static final String TYPE_ZNOW = '最低死亡保障額の推移';
	private static final String TYPE_ZRCH = 'ラチェット死亡保障額の推移';
	private static final String TYPE_STEP = 'ステップアップ金額の推移';

	/** 【保険契約ヘッダ】.[現在の状況]の値 */
	private static final String ZRSTDESC_SHIFT = '一時払定額年金へ移行';

	//4帳票通知リスト表示用
    public E_DownloadNoticeDTO noticeDTO {get;private set;}

	/** 契約者 */
	private E_CRLPF__c contractor = null;
	/** 被保険者 */
	private E_CRLPF__c mainAgent = null;
	/** 引出者 */
	private E_SWDPF__c pullOuter = null;

	//遷移元情報取得
	public String getFromType(){
		return E_Const.FROM_SPVA;
	}

	//レコードタイプ=「SPVA消滅」判定
	public Boolean getIsRecordTypeDSpva(){
		return this.extension.record.RecordTypeDeveloperName__c == E_Const.POLICY_RECORDTYPE_DSPVA;
	}

	/* コンストラクタ */
	public E_DSpvaExtender(E_DSpvaController extension){
		super();
		this.extension = extension;
	}

	/* init */
	global override void init(){
		// アクセスチェック
		pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
		if (pageRef == null) {
			//Contact情報を追加取得
			E_ContactDaoWithout.fillContactsToPolicy(extension.record, false, false,false,true, true);
			// 積立金遷移情報の初期化
			initDataTableZREV();
			// 基本保険金額の推移／基本給付金額の推移／最低死亡保障額の推移の初期化
			initDataTableSUMI();
			// 最低死亡保障額移情報の初期化
			//initDataTableZNOW();
			// ラチェット保障額移情報の初期化
			initDataTableZRCH();
			// ステップアップ死亡保障額の初期化
			initDataTableStep();

			// 受取人データテーブルの初期化  DF-000418対応 2015/6/8
			initSetCRLPFTable();

			// 4帳票通知リスト
			noticeDTO = new E_DownloadNoticeDTO(extension.record.Id);
		}
		// タイトルの設定
		if(extension.record.COMM_CRTABLE2__c != COMM_CRTABLE_SR){
			this.pgTitle = PAGE_TITLE_SPVA;
		}else{
			this.pgTitle = PAGE_TITLE_SPVWL;
		}
	}

	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}

	/*
	 * 解約手続きリンクを表示するかどうかを判定します
	 * ※SVEからコール
	 */
	public boolean getIsDispCancelProcessLink(){
		if(access.isEmployee() || access.isCustomer()){
			E_Policy__c policy = E_PolicyDaoWithout.getRecForDLHistoryById(extension.record.id);
			if (policy != null) {
				E_CancellFormDownloadControl__c downloadControl = E_CancellFormDownloadControlDao.getRecByType(policy.MainAgent__r.Account.ParentId,policy.COMM_CRTABLE2__c);
				// 【解約申込書DL設定】にデータが存在する場合
				if(downloadControl != null ){
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * 契約者名の取得
	 * ※SVEからコール
	 */
	public String getContractorName(){
		if(initContractor()){
			// 漢字名
			return contractor.ZCLNAME__c;
		}
		return null;
	}

	/*
	 * 契約者性別の取得
	 * ※SVEからコール
	 */
	public String getContractorSex(){
		if(initContractor()){
			// 性別
			return contractor.ZKNJSEX__c;
		}
		return null;
	}

	/*
	 * 契約者年齢(加入時)の取得
	 * ※SVEからコール
	 */
	public String getContractorOld(){
		if(extension.record.COMM_CRTABLE2__c != COMM_CRTABLE_SR){
			return '-';
		}
		if(initContractor()){
			// 加入時年齢
			if(contractor.ANBCCD__c != null){
				return Math.abs(contractor.ANBCCD__c).format();
			}
		}
		return null;
	}

	/*
	 * 被保険者名の取得
	 * ※SVEからコール
	 */
	public String getMainAgentName(){
		if(initMainAgent()){
			// 被保険者名
			return mainAgent.ZCLNAME__c;
		}
		return null;
	}

	/*
	 * 被保険者性別の取得
	 * ※SVEからコール
	 */
	public String getMainAgentSex(){
		if(initMainAgent()){
			// 加入時年齢
			return mainAgent.ZKNJSEX__c;
		}
		return null;
	}

	/*
	 * 被保険者年齢の取得
	 * ※SVEからコール
	 */
	public String getMainAgentOld(){
		if(initMainAgent()){
			// 加入時年齢
			if(mainAgent.ANBCCD__c != null){
				return Math.abs(mainAgent.ANBCCD__c).format();
			}
		}
		return null;
	}

	/*
	 * 引出情報の取得
	 * ※SVEからコールE_SWDPF
	 */
	public boolean getIsESWDPF(){
		if(initPullOuter()){
			// 引出情報
			System.debug('1=' + pullOuter);
			return true;
		}
		return false;
	}

	/*
	 * 引出基準額の取得
	 * ※SVEからコール
	 */
	public String getZWDRBAMT(){
		if(initPullOuter()){
			// 引出基準額
			return String.valueOf(pullOuter.ZWDRBAMT__c);
		}
		return null;
	}

	/*
	 * 引出割合の取得
	 * ※SVEからコール
	 */
	public String getZWTHDRRN(){
		if(initPullOuter()){
			// 引出割合
			return String.valueOf(pullOuter.ZWTHDRRN__c);
		}
		return null;
	}

	/*
	 * 引出回数の取得
	 * ※SVEからコール
	 */
	public String getZWTHDRFN(){
		if(initPullOuter()){
			// 引出回数
			return String.valueOf(pullOuter.ZWTHDRFN__c);
		}
		return null;
	}

	/*
	 * 次回引出日の取得
	 * ※SVEからコール
	 */
	public String getZWDRNEXT(){
		if(initPullOuter()){
			// 次回引出日
			return String.valueOf(pullOuter.ZWDRNEXT__c);
		}
		return '-';
	}

	/*
	 * 引出金額の取得
	 * ※SVEからコール
	 */
	public String getZWDRAMT(){
		if(initPullOuter()){
			// 引出金額
			return String.valueOf(pullOuter.ZWDRAMT__c);
		}
		return null;
	}

	/**
	 * 保険契約が定額年金へ移行済かどうか判定します
	 * ※SVEからコール
	 * @return true:定額年金へ移行済 false:定額年金へ移行済ではない
	 */
	public boolean getIsFixedAmount(){
		// 定額年金特則表示フラグ
		// @See レイアウト調整時、画面項目を強制表示する場合はフラグをfalseに変更する
		if (extension.record.SPVA_ZFIXAF__c){
			// ONの場合
			return true;
		}
		return false;
	}

	/**
	 * 契約者を初期化します。
	 * @return Boolean 契約者の有無
	 */
	private Boolean initContractor(){
		if(contractor == null){
			contractor = E_CRLPFDao.getRecByType(extension.record.Id, E_CRLPFDao.ROLE_OW );
		}
		return contractor == null ? false : true;
	}

	/**
	 * 被保険者を初期化します。
	 * @return Boolean 被保険者の有無
	 */
	private Boolean initMainAgent(){
		if(mainAgent == null){
			mainAgent = E_CRLPFDao.getRecByType(extension.record.Id, E_CRLPFDao.ROLE_LA );
		}
		return mainAgent == null ? false : true;
	}

	/**
	 * 引出者を初期化します。
	 * @return Boolean 引出者の有無
	 */
	private Boolean initPullOuter(){
		if(pullOuter == null){
			pullOuter = E_SWDPFDao.getRecByType(extension.record.Id);
		}
		return pullOuter == null ? false : true;
	}

	/**
	 * COMM_ZRSTDESC__cが”一時払定額年金へ移行”と一致するか判定します。
	 */
	public Boolean getZrstdesc(){
		if(extension.record.COMM_ZRSTDESC__c != null){
			if(extension.record.COMM_ZRSTDESC__c.equals(ZRSTDESC_SHIFT)){
				return true;
			}
		}
		return false;
	}

	/**
	 * 積立金遷移情報を初期化します。
	 */
	private void initDataTableZREV(){
		//積立金推移日付の当月、6ヶ月前、1年前、3年前、5年前、10年前の１日における積立金額
		for(Integer i=2;i<=7;i++){
			extension.dataTableZREV.add(
				new E_TransitionWK__c(
					//種別
					Type__c = TYPE_ZREV
					//日付
					,Column1Date__c = E_Util.toString(extension.record.get('SPVA_ZREVDATE0' + String.valueOf(i) + '__c'))
					//金額
					,Column2Number__c = E_Util.toDecimal(extension.record.get('SPVA_ZREVAMT0' + String.valueOf(i) + '__c'))
					// 並び順
					,SortNumber__c = i
				)
			);
		}
	}



	/**
	 * 受取人テーブルの対応
	 * DF-000418対応 2015/6/8
	 */
	private void initSetCRLPFTable() {
		//　保険種類コード先頭2桁の数式
		String hokenCord = extension.record.COMM_CRTABLE2__c;
		// 指定代理請求特約表示フラグ
		Boolean zdptydspFlg = false;
		zdptydspFlg = extension.record.SPVA_ZDPTYDSP__c;
		//　初期値は08とする。
		String bnytypeNumber = '08';
		// 保険種類コードがSRならば表示する受取人種別は01,
		// SR以外ならば表示する受取人種別は08とする。
//		if(hokenCord!=null || hokenCord!=''){
		if(!String.isBlank(hokenCord)){
			if(hokenCord.equals(COMM_CRTABLE_SR)){
				bnytypeNumber = '01';
			}
		}
		//受取人を取得
		E_CRLPF__c[] crlpftList = E_CRLPFDao.getRecsByType(extension.record.id,'BN');
		//もとのデータテーブルの値をクリアします。
		extension.CRLPFTable.clear();

		//死亡受取人用
		List<E_CRLPF__c> lists = new List<E_CRLPF__c> ();


		//受取人が取得出来るとき
		if(crlpftList !=null){
			for(E_CRLPF__c item : crlpftList){
				// 受取人種別が11、または、保険種類がSRでない時に受取人種別が09ならばデータテーブルに値を追加する。
				if( (!(hokenCord.equals(COMM_CRTABLE_SR)) && item.BNYTYP__c.equals('09')) ||item.BNYTYP__c.equals('11')){
					extension.CRLPFTable.add(
						new E_CRLPF__c(
							// 受取人種別名
							BNYTYP__c = (String)item.BNYTYPName__c
							// 受取人名
							,ZCLNAME__c = (String)item.ZCLNAME__c
							// 受取人割合(ハイフン)
							,CLNTNUM__c = '-'
						)
					);
				}
				// 受取人種別が40 かつ　指定代理請求フラグがTrueの時
				if( item.BNYTYP__c.equals('40') && zdptydspFlg ){
					extension.CRLPFTable.add(
						new E_CRLPF__c(
							// 受取人種別名
							BNYTYP__c = (String)item.BNYTYPName__c
							// 受取人名
							,ZCLNAME__c = (String)item.ZCLNAME__c
							// 受取人割合
							, CLNTNUM__c =  String.valueOf((item.BNYPC__c).format())
						)
					);
				}

				// 受取人種別が01または08のとき
				// ※保険種類コードがSRならば表示する受取人種別は01,
				// 　 SR以外ならば表示する受取人種別は08とする。
				if(item.BNYTYP__c ==bnytypeNumber){
					lists.add(item);
				}
			}
		}
		//DF-000137対応:契約内容個別照会（一時払変額終身保険）」「契約内容個別照会（一時払変額年金保険）」では、
		//年金受取人は1名固定表示で、受取人が居ない場合、氏名：”ブランク”、受取合割合：”-”で編集をする
		boolean isNoBNYTYP09 = true;
		for(E_DSpvaController.CRLPFTableItem item : extension.CRLPFTable.items){
			if(item.record.BNYTYP__c.equals('年金受取人')){
				isNoBNYTYP09 = false;
				break;
			}
		}
		if(isNoBNYTYP09){
			extension.CRLPFTable.add(
				new E_CRLPF__c(
					  BNYTYP__c = '年金受取人'
					, CLNTNUM__c = '-'
				)
			);
		}

		// もし死亡保険金受取人または死亡給付金受取人の値が存在するとき、
		if(lists!=null){
			for(E_CRLPF__c list01_08:lists){
				extension.CRLPFTable.add(
					new E_CRLPF__c(
						BNYTYP__c = (String)list01_08.BNYTYPName__c
						,ZCLNAME__c = (String)list01_08.ZCLNAME__c
						, CLNTNUM__c =  String.valueOf((list01_08.BNYPC__c).format())
					)
				);
			}
		}
	}



	/**
	 * 基本保険金額の推移／基本給付金額の推移／最低死亡保障額の推移を返却します。
	 */
	private void initDataTableSUMI(){
		// 直近の契約応当日・1年前・2年前・3年前・10年前・20年前・30年前の応当日における基本給付金額/基本保険金額
		for(Integer i=2;i<=8;i++){
			String numStr = 'SPVA_SUMINS0';
			// 金額のapi参照名は02のみ変更
			if(i == 2){
				numStr = 'COMM_SUMINS0';
			}
			extension.dataTableSUMI.add(
				new E_TransitionWK__c(
					//種別
					Type__c = TYPE_ZNOW
					//日付
					,Column1Date__c = E_Util.toString(extension.record.get('SPVA_ZSINSDAT0' + String.valueOf(i) + '__c'))
					//金額
					,Column2Number__c = E_Util.toDecimal(extension.record.get(numStr + String.valueOf(i) + '__c'))
					// 並び順
					,SortNumber__c = i
				)
			);
		}
	}

	/**
	 * 最低死亡保障額移情報を返却します。
	 */
	/*
	private void initDataTableZNOW(){

		// 直近の契約応当日・1年前・2年前・3年前・10年前・20年前・30年前の応当日における最低死亡保障額
		for(Integer i=2;i<=8;i++){
			String numStr = 'SPVA_SUMINS0';
			// 金額のapi参照名は02のみ変更
			if(i == 2){
				numStr = 'COMM_SUMINS0';
			}
			extension.dataTableZNOW.add(
				new E_TransitionWK__c(
					//種別
					Type__c = TYPE_ZNOW
					//日付
					,Column1Date__c = E_Util.toString(extension.record.get('SPVA_ZSINSDAT0' + String.valueOf(i) + '__c'))
					//金額
					,Column2Number__c = E_Util.toDecimal(extension.record.get(numStr + String.valueOf(i) + '__c'))
					// 並び順
					,SortNumber__c = i
				)
			);
		}
	}
	*/

	/**
	 * ラチェット保障額移情報を初期化します。
	 */
	private void initDataTableZRCH(){

		// 直近の契約応当日・1年前・2年前・3年前・10年前・20年前・30年前の応当日におけるラチェット死亡保障額
		for(Integer i=1;i<=7;i++){
			extension.dataTableZRCH.add(
				new E_TransitionWK__c(
					//種別
					Type__c = TYPE_ZRCH
					//日付
					,Column1Date__c = E_Util.toString(extension.record.get('SPVA_ZRCHTDTE0' + String.valueOf(i) + '__c'))
					//金額
					,Column2Number__c = E_Util.toDecimal(extension.record.get('SPVA_ZRCHTSUM0' + String.valueOf(i) + '__c'))
					// 並び順
					,SortNumber__c = i
				)
			);
		}
	}

	/**
	 * ステップアップ金額用のディスクレーマーを
	 * 組み立てます。
	 */
	private void initDataTableStep(){
		// ディスクレーマーの組み立て
		disclaimerStepUp = '* ステップアップ金額は、契約日においては一時払保険料の額とします。以後、積立金額';
		if(extension.record.COMM_CRTABLE2__c == COMM_CRTABLE_ST){
			disclaimerStepUp += '（年金支払開始日以後は、積立金額と既払い年金合計額を合算した額）';
		}
		disclaimerStepUp += 'が次に定めるいずれかの金額に到達した場合、その金額（同日に複数の金額に到達した場合はいずれか高い金額）と同額とし、その日から適用します。';
		if(extension.record.COMM_CRTABLE2__c == COMM_CRTABLE_ST){
			disclaimerStepUp += 'ただし、積立金額がなくなった場合、以後ステップアップ金額は増加しません。';
		}
		disclaimerStepUp += '<br>';
		// ステップアップ金額の取得
		E_SUPPF__c eSuppf = E_SUPPFDao.getRecByPolicyId(extension.record.Id);
		if(eSuppf != null){
			//　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
			Boolean stepFlg = true;
			// ステップアップの履歴を最新から最大で6履歴表示
			for(Integer i = 1; i <= 20; i++){
				// 【ステップアップ情報】処理日01~06, ステップアップ金額01~06, ステップアップ金額率01~06
				if(i <= 6){
					extension.dataTableSTEP.add(
						new E_TransitionWK__c(
							//種別
							Type__c = TYPE_STEP
							//日付
							,Column1Date__c = E_Util.toString(eSuppf.get('ZSTUPDAY0' + String.valueOf(i) + '__c'))
							//金額率
							,Column2Number__c = E_Util.toDecimal(eSuppf.get('ZSTUPRAT0' + String.valueOf(i) + '__c'))
							//金額
							,Column3Number__c = E_Util.toDecimal(eSuppf.get('ZSTUPSUM0' + String.valueOf(i) + '__c'))
							// 並び順
							,SortNumber__c = i
						)
					);

					// ここからディスクレーマー関連
					//　【ステップアップ情報】商品ごとステップアップ金額率01~6
					if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c') != null){
						if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c') == 0){
							stepFlg = false;
						}
						if(stepFlg){
							if(i >= 2){
								// ステップアップ金額が２行目以降
								disclaimerStepUp += '　または<br>';
							}
							disclaimerStepUp += '基本給付金額の' + eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')+'%の額';
						}
					}else{
						stepFlg = false;
					}
				}
				//　【ステップアップ情報】商品ごとステップアップ金額率06~9
				if( i>6 && i<=9){
					if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c') != null){
						if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c') == 0){
							stepFlg = false;
						}
						if(stepFlg){
							disclaimerStepUp += '　または<br>';
							disclaimerStepUp += '基本給付金額の'+eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')+'%の額';
						}
					}else{
						stepFlg = false;
					}
				//　【ステップアップ情報】商品ごとステップアップ金額率10~20
				}else if(i>9 && i<= 20){
					if(eSuppf.get('ZATTMRAT' + String.valueOf(i) + '__c') != null){
						if(eSuppf.get('ZATTMRAT' + String.valueOf(i) + '__c') == 0){
							stepFlg = false;
						}
						if(stepFlg){
							disclaimerStepUp += '　または<br>';
							disclaimerStepUp += '基本給付金額の'+eSuppf.get('ZATTMRAT' + String.valueOf(i) + '__c')+'%の額';
						}
					}else{
						stepFlg = false;
					}
				}
			}
		}
		disclaimerStepUp += '<br>';
		disclaimerStepUp += '一部解約された場合には、基本給付金額と同じ割合で減額されたステップアップ金額を、新たなステップアップ金額とします。新たなステップアップ金額は、一部解約日の翌営業日翌日以後適用します。';
	}

	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		//return new PageReference(E_CookieHandler.getCookieRefererPolicy());
		PageReference pr = Page.IRIS_VanishPolicy;
		pr.getParameters().put(I_Const.URL_PARAM_MENUID, iris.irisMenus.menuItemsByKey.get('disappearanceList').record.Id);
		pr.setRedirect(true);
		return pr;
	}

	/**
	 * 連携日時取得（消滅日）
	 */
//	public String getDataSyncDateVanish() {
//		return '消滅日：'+ E_Util.formatDataSyncDate();
//	}

	/**
	 * 連携日時取得（消滅日時点の契約内容）
	 */
	public String getDataSyncDateVanishCustomer() {
		return E_Util.formatDataSyncDate()  + '　' + '時点の契約内容';
	}

	/**
	 * 証券番号取得
	 */
	public String getCommChdrNum() {
		return extension.record.COMM_CHDRNUM__c.substringBeforeLast('|');
	}

	// 4帳票追加
	public PageReference getE_Download(){
		PageReference ret = Page.E_DownloadPrintSheetsSelect;
		List<I_MenuMaster__c> menus = I_MenuMasterDao.getRecordsByLinkMenuKey('download,key-policy');
		if(menus.size() == 1){
			ret.getParameters().put(I_Const.URL_PARAM_MENUID, menus.get(0).id);
		}
		return ret;
	}
}