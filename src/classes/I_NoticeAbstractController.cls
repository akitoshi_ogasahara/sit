public abstract class I_NoticeAbstractController extends I_AbstractController{
	// 代理店
	private Account distributor;
	// 事務所
	private Account office;
	// 募集人
	private Contact agent;
	
	// フィルターモード種別
	protected Boolean isFilterAh;
	protected Boolean isFilterAy;
	protected Boolean isFilterAt;	
	// フィルターモード情報
	private String conditionField;
	private String conditionValue;
	
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}
	
	// 代理店格コード
	public String distributeCode {get; set;}
	// ダウンロード帳票No
	public String dlFormNo {get; set;}
	// ダウンロード履歴
	public E_DownloadHistorry__c dlhistory {get; set;}
	// DTOリスト
	public List<I_NoticeDTO> dtos {get; set;}
	// レコード件数
	public Integer rowCnt {get; set;}
	// リスト行数
	public Integer rowCount {get; set;}
	// 帳票種別　未入金（NCOL）
	public Boolean isNcol {get; set;}
	
	// 表示制御フラグ
	public Boolean isViewGrp {get; set;}
	public Boolean isViewMr {get; set;}
	public Boolean isViewDistribute {get; set;}
	public Boolean isViewOffice {get; set;}
	public Boolean isViewAgent {get; set;}
	
	// 画面表示制御
	public Boolean hasError {get; set;}
	
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	
	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return dtos == null ? 0 : dtos.size();
	}
	
	/**
	 * MAX表示件数
	 */
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}
	
	/**
	 * DLアイコン表示フラグ
	 * 社員ユーザ、対象データが0件の場合に非表示
	 */
	public virtual Boolean getIsViewDl(){
		Boolean isView = true;
		// 対象件数が0件の場合は非表示
		if(dtos.isEmpty()){
			return false;
		}
		
		// 社員ユーザ
		if(access.isEmployee()){
			// フィルターモードの場合は表示
			if(isFilterAh || isFilterAy || isFilterAt){
				isView = true;
			}else{
				isView = false;
			}
		}
		return isView;
	}
	/**
	 * MRダウンロード表示フラグ
	 * 募集人ユーザ、フィルターモードの場合に非表示
	 */
	public Boolean getIsViewMRDl(){
		Boolean isView = true;
		// 対象件数が0件の場合は非表示
		// 社員ユーザ
		if(access.isEmployee()){
			// フィルターモードの場合は表示
			if(isFilterAh || isFilterAy || isFilterAt){
				isView = false;
			}else{
				isView = true;
			}
		}else{
			isView = false;
		}
		return isView;
	}

    /**
     * Constructor 
     */
    public I_NoticeAbstractController(){
    	super();
    	pageMessages = getPageMessages();
    	
    	// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		
		// 初期化
		this.hasError = false;
    	this.isViewGrp = false;
    	this.isFilterAh = false;
    	this.isFilterAy = false;
    	this.isFilterAt = false;
    	this.isNcol = false;

    }
    
	/**
	 * PageAction 
	 */
	protected virtual override PageReference init(){
		PageReference pr = super.init();
		try{
	    	// 代理店ユーザの場合、ログインユーザの代理店コードをセット
   			distributeCode = access.getOfficeCode();

   			//Cookieから代理店格、事務所、募集人のいづれかをセット
   			//setCookieValue();
   			
			// 表示制御フラグ設定
			setViewColumns();
			
			// 表示リスト作成
			createList();
			// 件数チェック
			checkSize();

		}catch(Exception e){
			pageMessages.clearMessages();
			pageMessages.addErrorMessage('［システムエラー］ ' + e.getMessage());
			this.hasError = true;
		}
		
		return pr;
	}

	// 件数チェック
	public void checkSize(){
		// 0件チェック
		if(dtos.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('DNI|002'));
			return;
		}
		
		// 最大件数以上のデータが存在するかチェック
		if(dtos.size() > I_Const.LIST_MAX_ROWS){
			pageMessages.addWarningMessage(getMSG().get('DNI|003'));
			
			// 1001件目削除
			dtos.remove(I_Const.LIST_MAX_ROWS);
			// 件数設定
			rowCnt = I_Const.LIST_MAX_ROWS;
		}
	}

	/**
	 * 基本条件作成
	 * フィルタモードでの検索条件作成
	 */
	public String createNoticeCondition(){

		/* 主従の出し分け対応 */
		String result = getSoqlWhereByUser();
		
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				/* 従募集人もフィルターモード対象とする */
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					result += ' AND SearchParentAccountId__c = \'' + conditionValue + '\'';
					isFilterAh = true;
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					result += ' AND SearchAccountId__c = \'' + conditionValue + '\'';
					isFilterAy = true;
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					//修正　山田
					String agentId = E_Util.getMainAccountByAgentId(conditionValue);
						result += ' AND SearchContactId__c = \'' + agentId + '\'';
					isFilterAt = true;
				//営業部フィルタ
				}else if(conditionField == I_Const.CONDITION_FIELD_UNIT){
					result = ' AND ( SearchBRNO__c  = \'' + conditionValue + '\' '
						   + 'OR SearchMainBRNO__c  = \'' + conditionValue + '\' ) ';
				//MRフィルタ
				}else if(conditionField == I_Const.CONDITION_FIELD_MR){
					result = ' AND SearchOwnerId__c = \'' + conditionValue + '\' ';
				}
			}
		}
		system.debug('■createNoticeCondition='+ result);
		return result;
	}

	/**
	 * アクセスユーザに応じた検索条件を追加
	 * @return String： ユーザ情報に準ずる検索条件
	 */
/*	public String getSoqlWhereByUser(){
		return I_NoticeAbstractController.getSoqlWhereByUser(access);
	}
*/
	/**
	 * アクセスユーザに応じた検索条件を追加
	 * I_NoticeTopControllerから使用するため、staticとする
	 * @param E_AccessController： アクセスコントローラー
	 * @return String： ユーザ情報に準ずる検索条件
	 */
	public static String getSoqlWhereByUser(){
		E_AccessController accessController = E_AccessController.getInstance();
		String result = '';
		if(accessController.isEmployee() && !accessController.ZINQUIRR_is_BRAll()){
			String brNo = accessController.getAccessKindFromThree();
			/* ログインユーザの支社コードが母店コードor支社コードと一致すること */
			result += ' AND (SearchBRNO__c = \'' + brNo + '\''
				+ ' OR SearchMainBRNO__c = \''+ brNo + '\')';
		}else if(accessController.isAgent()){
			//数式
			User agent = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
			//代理店格ユーザは格で絞込を行う
			if(accessController.isAuthAH()){
				result = ' AND SearchParentAccountId__c = \'' + agent.Contact.Account.ParentId + '\'';
			//代理店事務所ユーザは事務所で絞込を行う
			}else if(accessController.isAuthAY()){
				result = ' AND SearchAccountId__c = \'' + agent.AccountId + '\'';
			//募集人ユーザは募集人で絞込を行う
			}else if(accessController.isAuthAT()){
				//メインアカウントの取得
				Contact tmp_con = E_ContactDaoWithout.getRecByAgentNumber(agent.Contact.E_CL3PF_AGNTNUM__c.Left(5));
				//Contact tmp_con = E_ContactDao.getRecByAgentNumber(agent.Contact.E_CL3PF_AGNTNUM__c.Left(5));
				ID agentId = tmp_con.Id;

				result = ' AND SearchContactId__c = \'' + agentId + '\'';
			}
		}
		return result;
	}
	
	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		try{
			rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
			if(rowCount > I_Const.LIST_MAX_ROWS){
				rowCount = I_Const.LIST_MAX_ROWS;
			}
			
		}catch(Exception e){
			pageMessages.clearMessages();
			pageMessages.addErrorMessage('［システムエラー］ ' + e.getMessage());
			this.hasError = true;
		}

		return null;
	}
	
	/**
	 * CSVダウンロード
	 */
	public PageReference doDownloadCsv(){
		try{
			// E_DownloadConditionの生成
			E_DownloadCondition.NoticeDownloadCondi dlCondi= createDLCondition();
			
			// ダウンロード履歴の登録
			dlhistory = new E_DownloadHistorry__c();
			dlhistory.FormNo__c = dlFormNo;
			dlhistory.Conditions__c = dlCondi.toJSON();
			dlhistory.outputType__c = E_Const.DH_OUTPUTTYPE_CSV;
			system.debug('■Insert:dlhistory' + dlhistory);
			insert dlhistory;
			system.debug('■Insert:dlhistory.id' + dlhistory.id);
			
		}catch(Exception e){
			pageMessages.clearMessages();
			pageMessages.addErrorMessage('［システムエラー］ ' + e.getMessage());
			this.hasError = true;
			system.debug('■Err' + e.getMessage());
			
		}
		
		return null;
	}

    /**
     * ソート
     */
    public void sortRows(){
    	try{
    		// URLパラメータよりソート項目を取得
	        String sType = ApexPages.currentPage().getParameters().get('st');
			// ソート方法 - SOQL
			if(sortType != sType){
				sortType  = sType;
				sortIsAsc = true;
			} else {
				sortIsAsc = !sortIsAsc;
			}
			I_NoticeDTO.sortType = sortType;
			I_NoticeDTO.sortIsAsc = sortIsAsc;
			dtos.sort();
			
    	}catch(Exception e){
    		pageMessages.clearMessages();
    		pageMessages.addErrorMessage('［システムエラー］ ' + e.getMessage());
    		this.hasError = true;
    	}
    }
    
	/**
	 * 表示用リスト（I_NoticeDTO）を作成
	 */
	protected virtual void createList(){
		return;
	}
    
	/**
	 * 表示制御フラグ設定
	 */

	private void setViewColumns(){
		isViewMr = false;
		isViewDistribute = false;
		isViewOffice = false;
		isViewAgent = false;
		
		// 代理店ユーザ　AH権限
		if(access.isAgent() && access.isAuthAH()){
			isViewOffice = true;
			isViewAgent = true;
			
		// 代理店ユーザ　AY権限
		}else if(access.isAgent() && access.isAuthAY()){
			isViewAgent = true;
		
		// 代理店ユーザ　AT権限　表示なし
		}else if(access.isAgent() && access.isAuthAT()){
			
		// その他
		}else{
			isViewMr = true;
			isViewDistribute = true;
			isViewOffice = true;
			isViewAgent = true;
		}
	}

	/**
	 * DLCondition作成
	 */
	private E_DownloadCondition.NoticeDownloadCondi createDLCondition(){		
		// E_DownloadConditionの生成
		E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
		
		// 基本情報　：　AH権限
		if(access.isAuthAH() || isFilterAh){
			// 代理店情報取得
			if(isFilterAh){
				id did = conditionValue;
				distributor = [Select Id, Name, E_CL1PF_ZHEADAY__c From Account Where Id = :did];
			} else {
				id oid = access.user.Accountid;
				Account acc = E_AccountDao.getRecById(oid);
				distributor = new Account(Id = acc.ParentId, Name = acc.E_ParentName__c, E_CL1PF_ZHEADAY__c = acc.E_ParentZHEADAY__c);
			}
			
			// Condition
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
			dlCondi.distributorCode = distributor.Id;
			dlCondi.ZHEADAY = distributor.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = distributor.Name;
			
			// 事務所
			Set<String> officeCodes = new Set<String>();
			for(I_NoticeDTO dto : dtos){
				if(dto.distributeId == distributor.Id){
					officeCodes.add(dto.officeId);
				}
			}
			dlCondi.officeCodes = new List<String>(officeCodes);
			
		// 基本情報　：　AY権限
		}else if(access.isAuthAY() || isFilterAy){
			// 事務所情報取得
			Id oid = isFilterAy ? conditionValue : access.user.AccountId;
			office = E_AccountDao.getRecById(oid);
			
			// Condition
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AY;
			dlCondi.distributorCode = office.ParentId;
			dlCondi.ZHEADAY = office.E_ParentZHEADAY__c;
			dlCondi.ZHEADNAME = office.E_ParentName__c;
			
			// 事務所
			dlCondi.officeCodes = new List<String>{oid};
			
		// 基本情報　：　AT権限
		}else if(access.isAuthAT() || isFilterAt){
			// 募集人情報取得
			Id aid = isFilterAt ? conditionValue : access.user.ContactId;
			agent = E_ContactDao.getRecById(aid);

			// Condition
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AT;
			dlCondi.distributorCode = agent.Account.ParentId;
			dlCondi.ZHEADAY = agent.E_CL3PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = agent.E_AccParentName__c;
			
			// 事務所
			dlCondi.officeCodes = new List<String>{agent.AccountId};
						
			// メインアカウント（募集人コードの5桁）を取得
			Contact tmp_con = E_ContactDaoWithout.getRecByAgentNumber(agent.E_CL3PF_AGNTNUM__c.Left(5));
			//Contact tmp_con = E_ContactDao.getRecByAgentNumber(agent.E_CL3PF_AGNTNUM__c.Left(5));
			if(tmp_con == null){
				pageMessages.addErrorMessage(getMSG().get('DNI|004'));
				return null;
			}
			dlcondi.agentCode = tmp_con.Id;
		}

		// 期間　：　1ヶ月
/*		String periodDay = E_DownloadNoticeUtil.getTargetPeriodDay(dlFormNo);
		List<String> periodList = new List<String>(E_DownloadNoticeUtil.getPeriodSet(periodDay));
		dlCondi.fromyyyyMM = periodList.get(2);
		dlCondi.toyyyyMM = periodList.get(0);*/
		Date targetDate = E_DownloadNoticeUtil.getPeriodStartDateByObjApi(E_DownloadNoticeConst.FNO_OBJNM_MAP.get(dlFormNo));
		dlCondi.fromyyyyMM = E_Util.getFormatDateStr(targetDate, 'yyyy/MM');
		dlCondi.toyyyyMM = E_Util.getFormatDateStr(targetDate, 'yyyy/MM');
		
		// 保険契約ヘッダレコードタイプ　：　Coli　『ECHDPF』
		dlCondi.policyTypes = getRecordTypeDevName();
		

		return dlCondi;
	}
	protected virtual List<String> getRecordTypeDevName(){
		return new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};
	}
	//OPRO呼び出しURL生成
	public string getDownloadId(){
		if(dlhistory == null || dlhistory.Id == null){
			return '';
		}
		return 
			dlhistory.Id;
	}
}