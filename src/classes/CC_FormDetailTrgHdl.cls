/**
 * コールセンター帳票情報トリガハンドラ.
 */
public with sharing class CC_FormDetailTrgHdl {
	
	public Boolean isTriggerRun;
	public Boolean isCreateFollowUpSRActivity;
	public List<CC_FormDetail__c> detailsToCreateActivity;
	private CC_FormDetailTriggerBizLogic logic;
	
	public CC_FormDetailTrgHdl() {
		this.isTriggerRun = true;
		this.isCreateFollowUpSRActivity = false;
		this.detailsToCreateActivity = new List<CC_FormDetail__c>();
		this.logic = new CC_FormDetailTriggerBizLogic();
	}

	/**
	 * AfterUpdate処理.
	 *
	 * @param List<CC_FormDetail__c> コールセンター帳票情報リスト
	 */
	public void onAfterUpdate(List<CC_FormDetail__c> newList) {
		
		List<CC_FormDetail__c> relocateResults = this.relocateForm(newList);
		
		if(relocateResults.isEmpty()) {
			return;
		}
		
		// 活動作成が必要なコールセンター帳票情報を取得
		this.detailsToCreateActivity = this.getDetailsToCreateActibity(relocateResults);
		
		if(this.detailsToCreateActivity.isEmpty()) {
			return;
		}
		
		// 活動作成フラグをON
		this.isCreateFollowUpSRActivity = true;
	}
	
	/**
	 * 帳票の再配置を行う.
	 *
	 * @param List<CC_FormDetail__c> コールセンター帳票情報レコードのリスト
	 * @return List<CC_FormDetail__c> 再配置を行ったコールセンター帳票情報レコードのリスト
	 */
	private List<CC_FormDetail__c> relocateForm(List<CC_FormDetail__c> details) {
		
		List<CC_FormDetail__c> procDetail = new List<CC_FormDetail__c>();
		
		for(CC_FormDetail__c detail : details) {
			
			// 帳票出力フラグがOFFであれば処理不要
			if(!detail.CC_IsFormOutputFlg__c) {
				continue;
			}
			
			procDetail.add(detail);
		}
			
		// ビジネスロジック呼び出し
		return this.logic.relocateForm(procDetail);
	}

	/**
	 * 活動作成が必要なコールセンター帳票情報を取得する.
	 *
	 * @param コールセンター帳票情報レコードのリスト
	 */
	private List<CC_FormDetail__c> getDetailsToCreateActibity(List<CC_FormDetail__c> details) {
		return this.logic.getDetailsToCreateActibity(details);
	}

	/**
	 * コールセンター帳票情報を更新する.
	 *
	 * @param List<CC_FormDetail__c> コールセンター帳票情報レコードのリスト
	 */
	public void updateFormInfo(List<CC_FormDetail__c> details, Boolean isNeedDml) {
		this.logic.markIsCreatedActivity(details, isNeedDml);
		
		this.isTriggerRun = false;
	}
}