global with sharing class E_CancelInvoiceExtender extends E_AbstractViewExtender {
    E_CancelInvoiceController extension;
    
    /** ページタイトル */
    private static final String PAGE_TITLE = '解約請求書ダウンロード';
    /** 同意チェック */
    public Boolean isAgree {get; set;}

    /** 解約申込書DL設定 */
    private E_CancellFormDownloadControl__c cancelControl = null;

    /** 解約申込書DL設定に紐づく添付ファイル */
    private Attachment attach = null;

    /** 前画面 */
    private Pagereference prevPage;

    /** 画面制御：ご解約を検討されているお客様へ画面での、確認ボタン押下の有無 */
    private Boolean isConfirm = false;

    /** 上記の内容に同意するのチェック有無 */
    private Boolean isEntry = false;

    /** エラーメッセージコード：同意チェックがされていない（COLI/SPVA） */
    private String errMsgNotCheckAgree = 'CID|009';
    

    /* コンストラクタ */
    public E_CancelInvoiceExtender(E_CancelInvoiceController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }

    /* init */
    global override void init(){
        // アクセスチェック
        pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
    }
    
    /* PageAction */
    public PageReference pageAction () {
    	if (pageRef == null) {
			// 不正遷移チェック
			pageRef = doCheckPageTransition(ApexPages.currentPage().getUrl());
		
		}
		// 権限チェック
		return E_Util.toErrorPage(pageRef, null);
	}

    /*
     * [ご解約を検討されているお客様へ]から、[解約手続きについて]へ遷移
     * ※SVE[ご解約を検討されているお客様へ画面]確認ボタンからコール
     */
    public Pagereference doNextAgree(){
    	pageMessages.clearMessages();
        this.prevPage = Page.E_CancelInvoiceConfirm;
        this.isConfirm = true;
        return Page.E_CancelInvoiceAgree;
    }

    /*
     * [解約手続きについて]から、[解約請求書ダウンロード]へ遷移
     * ※SVE[解約手続きについて画面]上記の内容に同意するボタンからコール
     */
    public Pagereference doNextDownload(){
    	pageMessages.clearMessages();
        // 同意チェックボックス判定
        if(this.isAgree){
        	
        	try{
	        	//解約DL契約レコードの作成
	        	E_CancellFormDownloadHistoryCreator dlHistory = new E_CancellFormDownloadHistoryCreator();
	        	dlHistory.createLog(extension.record.Id											//保険契約ヘッダId
	        						, getFN()													//DLファイル名
	        						, E_AccessController.getInstance().idcpf.ZIDTYPE__c);		//ID種別
        	}catch(Exception e){
        		pageMessages.addErrorMessage('同意処理中にエラーが発生しました。' + e.getMessage());		//万が一契約者に見えてもいいメッセージとする。
        		return null;
        	}
            this.prevPage = Page.E_CancelInvoiceAgree;
            this.isEntry = true;
            return Page.E_CancelInvoiceDownloads;
            
        }else{
            pageMessages.addErrorMessage(getMSG().get(this.errMsgNotCheckAgree));
            return null;
        }
    }

    /*
     * [解約手続きについて]から[ご解約を検討されているお客様へ]、へ遷移
     * ※SVE[解約手続きについて]キャンセルボタンからコール
     */
    public Pagereference doPrevConfirm(){
        pageMessages.clearMessages();
        this.prevPage = null;
        // 同意チェックボックスをOFF
        this.isEntry = false;
        return Page.E_CancelInvoiceConfirm;
    }

    /*
     * [解約請求書ダウンロード]から、[解約手続きについて]へ遷移
     * ※SVE[解約手続きについて]前へ戻るボタンからコール
     */
    public Pagereference doPrevAgree(){
    	pageMessages.clearMessages();
        this.prevPage = Page.E_CancelInvoiceConfirm;
        // 同意チェックボックスをOFF
        this.isEntry = false;
        return Page.E_CancelInvoiceAgree;
    }
    
	/* 
	 * 戻るボタン押下処理
	 */
	public override Pagereference doReturn(){
		pageMessages.clearMessages();
		PageReference rePage = Page.E_Spva;
		rePage.getParameters().put('id', extension.record.id);
		return rePage;
	}

    /*
     * 解約ダウンロードから解約請求書名を返却する。
     * 取得できない場合はNullを返却。
     * ※SVE[解約請求書ダウンロード]からコール
     */
	public String getFN(){
		pageMessages.clearMessages();
		if(initCancellFormDownloadControl()){
			return attach.Name;
		}
		return null;
	}

    /*
     * 解約ダウンロードのIDを返却。
     * ※SVE[解約請求書ダウンロード]からコール
     */
    public ID getDownloaId(){
    	pageMessages.clearMessages();
        if(initCancellFormDownloadControl()){
            return attach.Id;
        }
        return null;
    }

    /*
     * 解約ダウンロードから解約請求書のパスを返却する。
     * 取得できない場合はNullを返却。
     * ※SVE[解約請求書ダウンロード]からコール
     */
    public String getCancelInvoicePdfPath(){
    	pageMessages.clearMessages();
        if(initCancellFormDownloadControl()){
            return attach.Name;
        }
        return null;
    }

    /* 不正遷移チェック */
    private PageReference doCheckPageTransition(String url){
        Pagereference pageRef = null;
        
        // URLを小文字へ変換
        url = url.toLowerCase();
        
        // URLのページが「解約手続きについて」画面かを判定
        if(url.indexOf(Page.E_CancelInvoiceAgree.getUrl()) != -1){
            // [ご解約を検討されているお客様へ]画面の確認ボタンを経由しない場合Falseをセット
            if(!this.isConfirm){
                pageRef = Page.E_ErrorPage;
    			pageRef.getParameters().put('code', this.errMsgNotCheckAgree);
            }
        }
        // URLのページが[解約請求書ダウンロード]画面かを判定
        if(url.indexOf(Page.E_CancelInvoiceDownloads.getUrl()) != -1){
            // 同意チェックがTrueでない場合Falseをセット
            if(!this.isEntry){
                pageRef = Page.E_ErrorPage;
    			pageRef.getParameters().put('code', this.errMsgNotCheckAgree);
            }
        }
        return pageRef;
    }

    /**
     * 解約申込書DL設定を初期化します。
     * @return Boolean 解約ダウンロードの有無
     */
    private Boolean initCancellFormDownloadControl(){
        if(cancelControl == null){
        	E_Policy__c policy = E_PolicyDaoWithout.getRecForDLHistoryById(extension.record.id);
        	if (policy != null) {
				cancelControl = E_CancellFormDownloadControlDao.getRecByType(policy.MainAgent__r.Account.ParentId,policy.COMM_CRTABLE2__c);
        	}
        }

        // 解約申込書DL設定が取得可かつ、添付ファイルが未取得
        if(cancelControl != null && attach == null){
            attach = E_AttachmentDao.getRecById(cancelControl.Id );
        }
        return attach == null ? false : true;
    }
}