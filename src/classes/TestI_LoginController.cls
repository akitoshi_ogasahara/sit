@isTest
private class TestI_LoginController {

	private final static String username = 'shoyusya';
	private final static String tempBasicPWModifiedDate = '20150407';

	//ページアクション
	static testMethod void pageActionTest01(){
		I_LoginController controller = new I_LoginController();

		Test.startTest();
		PageReference result = controller.pageAction();
		Test.stopTest();

		//System.assertEquals(null, result);
		System.assert(result.getUrl().contains('/e_errorpage?code'));           //Menuマスタが取得できないエラーとなる
	}


	//ページアクション
	//ブラウザ非互換
	static testMethod void pageActionTest02(){
		PageReference pageRef = Page.E_LoginAgent;
		//ヘッダーパラメータをセット
		pageRef.getHeaders().put('User-Agent', E_Const.BROWSER_MSIE +' 6.');
		Test.setCurrentPage(pageRef);

		I_LoginController controller = new I_LoginController();

		Test.startTest();
		PageReference result = controller.pageAction();
		Test.stopTest();

		//エラーページに遷移
		System.assertEquals('/apex/e_browsererr', result.getUrl());
	}

/*	//[ログイン]ボタン
	//暗号化なしでログイン成功
	static testMethod void doLoginTest01(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(Page.E_updateEmail.getUrl(), (result.getUrl()).substring(0,19));
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//初回ログインでログイン成功
	static testMethod void doLoginTest02(){

		//ログインユーザ作成
		String password = '1234567801';
		User user01 = createUser(true, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, user01.E_TempBasicPWModifiedDate__c));

			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(Page.E_updateEmail.getUrl(), (result.getUrl()).substring(0,19));
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}

*/	//[ログイン]ボタン
	//既存ユーザでログイン成功
	static testMethod void doLoginTest03(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, true);
		System.setPassword(user01.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encrypt(password));

		System.runAs(user01){
			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = 'Hogehoge1';

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			//System.assertEquals(Page.E_updateEmail.getUrl(), (result.getUrl()).substring(0,19));
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}

	static testMethod void doLoginTest03_tamotsukun(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createSelfIDUser(false, false);
		System.setPassword(user01.Id, password);

		System.runAs(user01){
			PageReference pageRef = Page.E_LoginAgent;
			//ヘッダーパラメータをセット
			pageRef.getHeaders().put('User-Agent', 'SalesforceMobileSDK');
			Test.setCurrentPage(pageRef);

			createEidcRec(user01);
			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = 'Hogehoge1';

			Test.startTest();
//			controller.access.forceIsViaMobileSDK = true;
			System.assertEquals(controller.getViaMobileSDK(), true);

			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			//System.assertEquals(Page.E_updateEmail.getUrl(), (result.getUrl()).substring(0,19));
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//既存ユーザでログイン成功
/*	static testMethod void doLoginTest033(){

		String password = 'Hogehoge1';
		User thisUser = [ select Id,UserName from User where Id = '00510000002gHx5AAE' ];
		User sysUser = [ select Id,UserName from User where Id = :UserInfo.getUserId() ];
		PageReference pageRef = Page.E_LoginAgent;
		Test.setCurrentPage(pageRef);
		System.setPassword(thisUser.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encrypt(password));

		System.runAs(thisUser){
			I_LoginController controller = new I_LoginController();
			controller.userId = thisUser.UserName;
			controller.password = 'Hogehoge1';

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			//System.assertEquals(Page.E_updateEmail.getUrl(), (result.getUrl()).substring(0,19));
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}
*/
	//[ログイン]ボタン
	//存在しないユーザID
/*	static testMethod void doLoginTest04(){

		//メッセージマスタ作成
		E_MessageMaster__c LGE001 = TestE_TestUtil.createMessageMaster(false, 'ログイン画面_ユーザID不正', null, 'MSG_DISCLAIMER', E_Const.E_MEGTYPE_MEG, 'LGE|001');
		LGE001.Value__c = 'ログイン画面_ユーザID不正';
		insert LGE001;

		//実行
		I_LoginController controller = new I_LoginController();
		controller.userId = '';

		Test.startTest();
		PageReference result = controller.doLogin();
		Test.stopTest();

		System.assertEquals(null, result);
		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(LGE001.Value__c, controller.pageMessages.getErrorMessages()[0].summary);
	}
*/

	//[ログイン]ボタン
	//ID正、パスワード誤り(ログイン不可)
	static testMethod void doLoginTest05(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = 'hogehoge2';

//			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			//System.assert(!controller.pageMessages.getHasErrorMessages());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}


	//[ログイン]ボタン
	//ID正、パスワード未入力(ログイン不可)
	static testMethod void doLoginTest06(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = null;

//			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			//System.assert(!controller.pageMessages.getHasErrorMessages());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	//[ログイン]ボタン
	//エラー発生
	static testMethod void doLoginTest07(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(true, false);
		user01.E_TempBasicPWModifiedDate__c = null;
		update user01;

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}


	//[ログイン]ボタン
	//IDが正、メールアドレスが誤、パスワードが正（ログイン可能）
	static testMethod void doLoginTest08(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		String userMail = 'aaaaa@aaaa';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = username;
			controller.password = 'Hogehoge1';
			controller.userMail = 'aaaa@bbbb';

			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null,result);
		}
	}

	//[ログイン]ボタン
	//ID誤、パスワード正(ログイン不可)
	static testMethod void doLoginTest09(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = 'Hogehoge1';
		//String userMail = 'aaaaa@aaaa';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = 'bosyusya';
			controller.password = 'Hogehoge1';

			//ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null,result);
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	//[ログイン]ボタン
	//ID、メールアドレスともに誤、パスワードは正（ログイン不可）
	static testMethod void doLoginTest10(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		String userMail = 'aaaaa@aaaa';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = 'bosyusya';
			controller.password = 'Hogehoge1';
			controller.userMail = 'aaaa@bbbb';

			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//ID未入力、パスワード正（ログイン不可）
	static testMethod void doLoginTest11(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = null;
			controller.password = 'hogehoge1';

//			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
//			System.assert(controller.pageMessages.getHasErrorMessages());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	//[ログイン]ボタン
	//ID、パスワードすべて誤（ログイン不可）
	static testMethod void doLoginTest12(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = 'bosyusya';
			controller.password = 'hogehoge2';

//			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
//			System.assert(controller.pageMessages.getHasErrorMessages());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	//[ログイン]ボタン
	//ID、メールアドレス、パスワードすべて未入力（ログイン不可）
	static testMethod void doLoginTest13(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		String userMail = 'aaaaa@aaaa';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			I_LoginController controller = new I_LoginController();
			controller.userId = null;
			controller.password = null;
			controller.userMail = null;

			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//ID自動発行時メールアドレス
	static testMethod void doLoginTest14(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = '1111111101';
		//String userMail = 'aaaaa@aaaa';
		String idUserMail = 'bbbbb@bbbb';
		User user01 = createSelfIDUser(true, false);

		//実行
		System.runAs(user01){

			String loginpass = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, user01.E_TempBasicPWModifiedDate__c);
			System.setPassword(user01.Id, loginpass);


			PageReference pageRef = Page.E_LoginAgent;
			//パラメータをセット
			pageRef.getParameters().put('maillogin','1');
			Test.setCurrentPage(pageRef);


			I_LoginController controller = new I_LoginController();
			controller.password = '1111111101';
			controller.idUserMail = 'bbbbb@bbbb';

			//ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference pageActionres = controller.pageAction();
//			controller.userIdDisableFlg = true;
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			//System.assert(!controller.pageMessages.getHasErrorMessages());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	static testMethod void doLoginTest14_tamotsukun(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = '1111111101';
		//String userMail = 'aaaaa@aaaa';
		String idUserMail = 'bbbbb@bbbb';
		User user01 = createSelfIDUser(true, false);

		//実行
		System.runAs(user01){

			String loginpass = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, user01.E_TempBasicPWModifiedDate__c);
			System.setPassword(user01.Id, loginpass);


			PageReference pageRef = Page.E_LoginAgent;
			//パラメータをセット
			pageRef.getParameters().put('maillogin','1');
			pageRef.getHeaders().put('User-Agent', 'SalesforceMobileSDK');
			Test.setCurrentPage(pageRef);

			I_LoginController controller = new I_LoginController();
			controller.password = '1111111101';
			controller.idUserMail = 'bbbbb@bbbb';

			//ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));

			Test.startTest();
			PageReference pageActionres = controller.pageAction();
//			controller.userIdDisableFlg = true;
			PageReference result = controller.doLogin();
			Test.stopTest();
//			System.assert(false, controller.pageMessages.getErrorMessages());

			//Site.loginはtestmethod内では何もしない
//			System.assertEquals(null, result);
			//System.assert(!controller.pageMessages.getHasErrorMessages());
//			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}


	//[ログイン]ボタン
	//ユーザーID誤り
	static testMethod void doLoginTest15(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, true);
		System.setPassword(user01.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encrypt(password));

		System.runAs(user01){
			I_LoginController controller = new I_LoginController();
			controller.userId = 'shoyusya01';
			controller.password = 'Hogehoge1';

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			//System.assertEquals(Page.E_updateEmail.getUrl(), (result.getUrl()).substring(0,19));
			//System.assert(controller.pageMessages.getHasErrorMessages());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	//[ログイン]ボタン
	//初回ログイン失敗
	static testMethod void doLoginTest16(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|004','ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');

		//ログインユーザ作成
		String password = '1111111101';
		String idUserMail = 'bbbbb@bbbb';
		User user01 = createSelfIDUser(true, false);

		//実行
		System.runAs(user01){

			String loginpass = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, user01.E_TempBasicPWModifiedDate__c);
			System.setPassword(user01.Id, loginpass);

			PageReference pageRef = Page.E_LoginAgent;
			//パラメータをセット
			pageRef.getParameters().put('maillogin','1');
			Test.setCurrentPage(pageRef);

			I_LoginController controller = new I_LoginController();
			controller.password = '111111110';
			controller.idUserMail = 'bbbbb@bbbb';

			Test.startTest();
			PageReference pageActionres = controller.pageAction();
			PageReference result = controller.doLogin();
			Test.stopTest();

			System.assertEquals(result.getUrl(), Page.E_LoginAgent.getUrl());
			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ログインに失敗しました。ユーザ名とパスワードが正しいかご確認ください。');
		}
	}

	//[ログイン]ボタン
	//IPアドレス アクセス不可
	static testMethod void doLoginTest17(){
		//メッセージ作成
		createIRISCMS();
		createMessageData('LGE|011','ご利用の環境からは、ログインいただくことはできません。');

		//ログインユーザ作成
		User user01 = createUser(true, true);

		User user02;
		System.runAs(user01){
			// 1. 取引先（格）を作成
			Account accParent = TestI_TestUtil.createAccount(false, null);
			accParent.E_CheckIPFlg__c = true;
			accParent.E_WhiteIPAddress__c = '';
			insert accParent;
			// 2. 取引先（事務所）を作成
			Account accAgency = TestI_TestUtil.createAccount(true, accParent);
			// 3. 取引先責任者を作成
			String actUserLastName = 'shoyusyaP';
			Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);

			user02 = TestI_TestUtil.createAgentUser(false, actUserLastName, 'E_PartnerCommunity', con.Id);
			user02.userName = actUserLastName +  System.Label.E_USERNAME_SUFFIX;
			user02.E_UseTempBasicPW__c = false;
			user02.E_UseExistingBasicPW__c = true;
			user02.E_TempBasicPWModifiedDate__c = tempBasicPWModifiedDate;
			insert user02;
			String password = 'Hogehoge1';
			System.setPassword(user02.Id, password);
			TestI_TestUtil.createAgencyPermissions(user02.Id);
		}

		System.runAs(user02){
			I_LoginController controller = new I_LoginController();
			controller.userId = 'shoyusyaP';
			controller.password = 'Hogehoge1';

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			System.assertEquals(controller.pageMessages.getErrorMessages()[0].summary, 'ご利用の環境からは、ログインいただくことはできません。');
		}
	}


	//[パスワードをお忘れの方はこちら]ボタン
	static testMethod void doReissueTest01(){

		I_LoginController controller = new I_LoginController();

		Test.startTest();
		PageReference result = controller.doReissue();
		Test.stopTest();

		System.assertEquals(Page.IRIS_ReissuePassword.getUrl(), result.getUrl());
	}

/*
	static testMethod void createEidcRecTest(){
		I_LoginController controller = new I_LoginController();
		USer ownerUser =
		Test.startTest();
		controller.createEidcRec(ownerUser);
		Test.stopTest();

		//System.assertEquals();
	}
*/


	//Coverage用テストメソッド
	static testMethod void forCoverage(){
		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, true);
		user01 = E_UserDaoWithout.getUserRecByUserName(username +  System.Label.E_USERNAME_SUFFIX);
		//実行
		System.runAs(user01){
			I_LoginController controller = new I_LoginController();
			System.assertEquals(controller.isExistsEIDC(user01), false);
			System.assertEquals(controller.isAgent(user01), false);
//			System.assertEquals(controller.isCreateNNLinkID(user01), true);
			System.assertEquals(controller.getPageParameterStrings(), null);
			System.assertEquals(controller.getViaMobileSDK(), false);

			E_IDCPF__c eidc = new E_IDCPF__c();
			controller.createEidrRec(eidc);
			system.assertEquals(controller.menukey, null);
			system.assertEquals(controller.anchor, null);
		}
	}

/*
	//Coverage用テストメソッド
	static testMethod void forCoverage2(){
		//ログインユーザ作成
		User user01 = createAgentUser();
		user01 = E_UserDaoWithout.getUserRecByUserNameWithEIDC(username +  System.Label.E_USERNAME_SUFFIX);
		//実行
		System.runAs(user01){
			E_IDCPF__c eidc = E_IDCPFDaoWithout.getRecsByUserID(user01.id);
			E_IDRequest__c eidr = createEidrRec(eidc, E_Const.ZUPDFLAG_D);

			I_LoginController controller = new I_LoginController();
			System.assertEquals(controller.isInActiveUser(username), false);
		}
	}
*/

	//ログインユーザ作成
	static User createUser(Boolean useTempBasicPW, Boolean useExistingBasicPW){
		UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
		User ownerUser = TestE_TestUtil.createUser(false, username, E_Const.SYSTEM_ADMINISTRATOR);
		ownerUser.userroleid = uRole.Id;
		ownerUser.username =  username +  System.Label.E_USERNAME_SUFFIX;
		ownerUser.E_UseTempBasicPW__c = useTempBasicPW;
		ownerUser.E_UseExistingBasicPW__c = useExistingBasicPW;
		ownerUser.E_TempBasicPWModifiedDate__c = tempBasicPWModifiedDate;
		insert ownerUser;
		return ownerUser;
	}

	static user createAgentUser(){
		// 代理店実行ユーザ作成（AY）
		// 1. 取引先（格）を作成
		Account accParent = TestI_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = username;
		Contact con = TestI_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, actUser.Id, 'AY');

		return actUser;
	}


	//ID自動発行時ログインユーザ作成
	static User createSelfIDUser(Boolean useTempBasicPW, Boolean useExistingBasicPW){
		UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
		User ownerUser = TestE_TestUtil.createUser(false, username, E_Const.SYSTEM_ADMINISTRATOR);
		ownerUser.userroleid = uRole.Id;
		ownerUser.username =  'bbbbb@bbbb' +  System.Label.E_USERNAME_SUFFIX;
		ownerUser.E_UseTempBasicPW__c = useTempBasicPW;
		ownerUser.E_UseExistingBasicPW__c = useExistingBasicPW;
		ownerUser.E_TempBasicPWModifiedDate__c = tempBasicPWModifiedDate;
		insert ownerUser;

		//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add('E_PermissionSet_Base');//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = ownerUser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
		   }
		return ownerUser;
	}

	//ID管理作成
	static E_IDCPF__c createEidcRec(User userRec){
		E_IDCPF__c eidc = new E_IDCPF__c(
			User__c = userRec.Id									//ユーザ
			,ZEMAILAD__c = userRec.EMAIL							//E-MAILアドレス
			,ZIDOWNER__c = 'AG'+userRec.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
			,ZIDTYPE__c = E_Const.ZIDTYPE_AT						//ID種別
			,ZINQUIRR__c = 'L3'+userRec.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
			,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
			,ZWEBID__c = userRec.E_ZWEBID__c						//ID番号
			,OwnerId = userRec.ID									//所有者ID
		);
		insert eidc;
		return eidc;
	}

	//IDリクエスト作成
	static E_IDRequest__c createEidrRec(E_IDCPF__c eidc){
		return createEidrRec(eidc,E_Const.ZUPDFLAG_I);
	}
	static E_IDRequest__c createEidrRec(E_IDCPF__c eidc, String ZUPDFLAG){
		E_IDRequest__c eidr = new E_IDRequest__c(
			E_IDCPF__c = eidc.ID				//ID管理
			,ZUPDFLAG__c = ZUPDFLAG	//リクエスト種別
			,ZWEBID__c = eidc.ZWEBID__c			//ID番号
			,ZIDTYPE__c = eidc.ZIDTYPE__c		//ID種別
			,ZIDOWNER__c = eidc.ZIDOWNER__c		//所有者コード
			,ZINQUIRR__c = eidc.ZINQUIRR__c		//照会者コード
			,ZEMAILAD__c = eidc.ZEMAILAD__c	//E-MAILアドレス
		);
		insert eidr;
		return eidr;
	}


	//メッセージマスタ作成
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			createMenuData();
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//メニューマスタ作成
	static void createMenuData(){
		E_MenuMaster__c mn = new E_MenuMaster__c();
		mn.MenuMasterKey__c = 'internet_service_policy_agency';
		insert mn;
	}

	//メッセージマスタ作成
	static void createIRISCMS(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			I_PageMaster__c pm = createIRISPageData();
			I_ContentMaster__c cms = new I_ContentMaster__c();
			cms.Page__c = pm.Id;
			cms.Content__c = '内容';
			insert cms;
		}
	}

	//メニューマスタ作成
	static I_PageMaster__c createIRISPageData(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'SelfRegistConfirm';
		insert pm;
		return pm;
	}
}