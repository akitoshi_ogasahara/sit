/**
 * @Created Jia Hu on 2018/4/17
 *
 * @ModifiedBy Jia Hu on 2018/05/01
 * @version 1.0
 * @Changes add apex comment header
 * 
 */
public without sharing class WeeklyActivityReportsController {
    
    @AuraEnabled
    public static String callCalcMethod(String strRecordId) {
        String result;
        String strDate;
        String strU;
        Boolean blnLastM;
        
        WeeklyActivityReports__c weeklyreport = [
            Select Id, Date__c, OwnerId, FAINAL_FLG__c 
            from WeeklyActivityReports__c where Id =: strRecordId];
        
        if(weeklyreport != null) {
            strDate = String.valueOf(weeklyreport.Date__c);
            strU = weeklyreport.OwnerId;
            blnLastM = weeklyreport.FAINAL_FLG__c; 
        }
        
        System.debug('--- In WeeklyActivityReportsController(): WeeklyActivityReports__c  = ' 
                     + weeklyreport);
        
        result = shujiShukei.execute(strRecordId, strDate, strU, blnLastM);
        
        return result;
    }
    //webService static String execute(String id, String strDate, String strU, Boolean blnLastM) {
     
}