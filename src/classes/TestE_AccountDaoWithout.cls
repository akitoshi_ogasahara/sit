@isTest
private class TestE_AccountDaoWithout {
	
	@isTest static void getRecsByParentIdTest() {
		//テストデータ作成
		Account a = new Account(Name='テストgetRecsByParentIdTest', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
		INSERT a;
		String id = a.Id;
		List<Account> returned = E_AccountDaoWithout.getRecsByParentId(id); 
		
		System.assertNotEquals(null, returned);
	}
	
	@isTest static void getRecsByParentIdIsActiveTest() {
		//テストデータ作成
		Account a = new Account(Name='テストgetRecsByParentIdIsActiveTest', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
		INSERT a;
		String id = a.Id;
		List<Account> returned = E_AccountDaoWithout.getRecsByParentIdIsActive(id); 
		
		System.assertNotEquals(null, returned);
	}
	@isTest static void getOfficesByParentId_BRCDTest() {
		Account pa = new Account(Name='テストParent', E_CL1PF_ZHEADAY__c='テスト1T');
		INSERT pa;
		Account a = new Account(Name='テストgetRecsByMRId', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
		INSERT a;
		String parentId = pa.Id;
		String brcd = null;
		Set<String> validFlgs = new Set<String>();
		validFlgs.add('1');
		validFlgs.add('1');
		validFlgs.add('1');
		List<Account> returned = E_AccountDaoWithout.getOfficesByParentId_BRCD(parentId, brcd, validFlgs);

		System.assertNotEquals(null, returned);
	}
	@isTest static void getRecsByAgencyTest() {
		Account pa = new Account(Name='テストParent', E_CL1PF_ZHEADAY__c='テスト1T');
		INSERT pa;
		Account a = new Account(Name='テストgetRecsByMRId', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
		INSERT a;
		String id = pa.Id;
		String brunchCode = a.E_CL2PF_BRANCH__c;
		Boolean isAllOffice = false;
		String sortField = 'Name';
		Boolean sortIsAsc = false;
		List<Account> returned = E_AccountDaoWithout.getRecsByAgency(id, brunchCode, isAllOffice, sortField, sortIsAsc);

		System.assertNotEquals(null, returned);
	}
	@isTest static void getRecByIdTest() {
		Account pa = new Account(Name='テストParent', E_CL1PF_ZHEADAY__c='テスト1T');
		INSERT pa;
		Account a = new Account(Name='テストgetRecsByMRId', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
		INSERT a;
		String accountId = a.Id;
		Account returned = E_AccountDaoWithout.getRecById(accountId);

		System.assertNotEquals(null, returned);
	}
	@isTest static void getRecsIRISAgencySearchBoxAgencyTest1() {
		//テストデータ作成
		Account a = new Account(Name='テストgetRecsIRISAgencySearchBox', E_CL1PF_ZHEADAY__c='テスト1T');
		INSERT a;
		String keyword = 'テスト';
		String soqlWhere = '';
		List<Account> returned = E_AccountDaoWithout.getRecsIRISAgencySearchBoxAgency(soqlWhere, keyword);

		System.assertNotEquals(null, returned);
	}
	/**
	 * 代理店検索 and検索
	 */
	@isTest static void getRecsIRISAgencySearchBoxAgencyTest2() {
		//テストデータ作成
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account parent2 = new Account();
		parent2.Name = 'test parentZ';
		parent2.E_CL1PF_ZHEADAY__c = '54321';
		insert parent2;

		Account acc1 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト1');
		acc1.E_CL2PF_BRANCH__c = '11';
		acc1.E_COMM_VALIDFLAG__c = '1';
		acc1.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc1;

		Account acc2 = TestUtil_I_SearchBox.createOffice(false,parent2.Id,'テスト2');
		acc2.E_CL2PF_BRANCH__c = '11';
		acc2.E_COMM_VALIDFLAG__c = '1';
		acc2.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc2;

		String keyword = 'test Z';
		String soqlWhere = '';
		List<Account> returned = E_AccountDaoWithout.getRecsIRISAgencySearchBoxAgency(soqlWhere, keyword,'and');

		System.assertEquals(1, returned.size());
	}

	/**
	 * 代理店検索 or検索
	 */
	@isTest static void getRecsIRISAgencySearchBoxAgencyTest3() {
		//テストデータ作成
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account parent2 = new Account();
		parent2.Name = 'test parentZ';
		parent2.E_CL1PF_ZHEADAY__c = '54321';
		insert parent2;

		Account acc1 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト1');
		acc1.E_CL2PF_BRANCH__c = '11';
		acc1.E_COMM_VALIDFLAG__c = '1';
		acc1.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc1;

		Account acc2 = TestUtil_I_SearchBox.createOffice(false,parent2.Id,'テスト2');
		acc2.E_CL2PF_BRANCH__c = '11';
		acc2.E_COMM_VALIDFLAG__c = '1';
		acc2.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc2;
		String keyword = 'test Z';
		String soqlWhere = '';
		List<Account> returned = E_AccountDaoWithout.getRecsIRISAgencySearchBoxAgency(soqlWhere, keyword,'or');

		System.assertEquals(2, returned.size());
	}
	@isTest static void getRecsIRISAgencySearchBoxOfficeTest1() {
		//テストデータ作成
		Account a = new Account(Name='テストgetRecsIRISAgencySearchBox', E_CL1PF_ZHEADAY__c='テスト1T');
		INSERT a;
		String keyword = 'テスト';
		String soqlWhere = '';
		List<Account> returned = E_AccountDaoWithout.getRecsIRISAgencySearchBoxOffice(soqlWhere, keyword);

		System.assertNotEquals(null, returned);
	}

	/**
	 * 事務所検索 and検索
	 */
	@isTest static void getRecsIRISAgencySearchBoxOfficeTest2() {
		//テストデータ作成
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account acc1 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト1');
		acc1.E_CL2PF_BRANCH__c = '11';
		acc1.E_COMM_VALIDFLAG__c = '1';
		acc1.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc1;

		Account acc2 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト2');
		acc2.E_CL2PF_BRANCH__c = '11';
		acc2.E_COMM_VALIDFLAG__c = '1';
		acc2.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc2;

		String keyword = 'テスト 1';
		String soqlWhere = '';
		List<Account> returned = E_AccountDaoWithout.getRecsIRISAgencySearchBoxOffice(soqlWhere, keyword,'and');

		System.assertEquals(1, returned.size());
	}

	/**
	 * 事務所検索 or検索
	 */
	@isTest static void getRecsIRISAgencySearchBoxOfficeTest3() {
		//テストデータ作成
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account acc1 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト1');
		acc1.E_CL2PF_BRANCH__c = '11';
		acc1.E_COMM_VALIDFLAG__c = '1';
		acc1.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc1;

		Account acc2 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト2');
		acc2.E_CL2PF_BRANCH__c = '11';
		acc2.E_COMM_VALIDFLAG__c = '1';
		acc2.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc2;

		String keyword = 'テスト 1';
		String soqlWhere = '';
		List<Account> returned = E_AccountDaoWithout.getRecsIRISAgencySearchBoxOffice(soqlWhere, keyword,'or');

		System.assertEquals(2, returned.size());
	}


	@isTest static void getRecByAccCodeTest() {
		Account a = new Account(Name='テストgetRecsByMRId', E_COMM_VALIDFLAG__c='1', E_CL2PF_BRANCH__c='XX');
		INSERT a;
		String accCode = a.E_CL2PF_ZAGCYNUM__c;
		Account returned = E_AccountDaoWithout.getRecByAccCode(accCode);

		System.assertNotEquals(null, returned);
	}
	@isTest static void getOwnerTest() {
		Profile prf = [SELECT Id FROM Profile WHERE Name = 'MR' LIMIT 1];

		String userName = 'test@nnlife.jp';
		User ow = new User(
			Lastname = 'test'
			, Username = userName
			, Email = userName
			, ProfileId = prf.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		INSERT ow;

		Account acc = new Account(Name = 'テストgetOwner', OwnerId = ow.Id);
		INSERT acc;
		String accId = acc.Id;
		Account returned = E_AccountDaoWithout.getOwner(accId);

		System.assertNotEquals(null, returned);
	}

	@isTest static void getRecsByAgency_test001() {
		Date d = Date.today();
		Datetime dt = System.now();

		E_BizDataSyncLog__c bizlog = new E_BizDataSyncLog__c();
		bizlog.Kind__c = 'F';
		bizlog.InquiryDate__c = d;
		bizlog.BatchEndDate__c = dt;
		insert bizlog;

		Account account = new Account();
		account.Name = 'test account';
		account.E_COMM_VALIDFLAG__c = '1';
		account.E_CL2PF_BRANCH__c = '11';
		insert account;

		Test.startTest();
		List<Account> results = E_AccountDaoWithout.getRecsByAgency('11', false, 'Name', false, '');
		Test.stopTest();

		System.assert(!results.isEmpty());
	}

	@isTest static void getParentAcc_test001() {
		String ip = '111.111.111.111';

		Account account = new Account();
		account.Name = 'test account';
		account.E_WhiteIPAddress__c = ip;
		insert account;

		Test.startTest();
		Account result = E_AccountDaoWithout.getParentAcc(account.Id);
		Test.stopTest();

		System.assertEquals(ip, result.E_WhiteIPAddress__c);
	}

	@isTest static void getRecsIRISAuthorityAgencySearch_test001() {
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account account = new Account();
		account.Name = 'test account';
		account.E_CL2PF_BRANCH__c = '11';
		account.E_COMM_VALIDFLAG__c = '1';
		account.ParentId = parent.Id;

		RecordType recType = [Select id from RecordType where DeveloperName = 'Agency'];
		account.RecordTypeId = recType.Id;
		insert account;

		Test.startTest();
		List<Account> results = E_AccountDaoWithout.getRecsIRISAuthorityAgencySearch('', '11', '12345');
		Test.stopTest();

		System.assert(!results.isEmpty());
	}

	@isTest static void getParentRecByAccCode_test001() {
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account account = new Account();
		account.Name = 'test account';
		account.E_CL2PF_BRANCH__c = '11';
		account.E_COMM_VALIDFLAG__c = '1';

		RecordType recType = [Select id from RecordType where DeveloperName = 'Agency'];
		account.RecordTypeId = recType.Id;
		account.ParentId = parent.Id;
		insert account;

		Test.startTest();
		Account result = E_AccountDaoWithout.getParentRecByAccCode('12345');
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void getRecsByAgencyCount_test001() {
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account account = new Account();
		account.Name = 'test account';
		account.E_CL2PF_BRANCH__c = '11';
		account.E_COMM_VALIDFLAG__c = '1';
		account.ParentId = parent.Id;
		insert account;

		Test.startTest();
		Integer result = E_AccountDaoWithout.getRecsByAgencyCount(parent.Id);
		Test.stopTest();

		System.assertEquals(1, result);
	}

	@isTest static void getRecsByAgency_Atria_test001() {
		Account parent = new Account();
		parent.Name = 'test parent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account account = new Account();
		account.Name = 'test account';
		account.E_CL2PF_BRANCH__c = '11';
		account.E_COMM_VALIDFLAG__c = '1';
		account.ParentId = parent.Id;
		insert account;

		Test.startTest();
		List<Account> results = E_AccountDaoWithout.getRecsByAgency_Atria(parent.Id, '11', 'Name', false);
		Test.stopTest();

		System.assert(results.size() > 0);
	}
}