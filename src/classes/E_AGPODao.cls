public with sharing class E_AGPODao {
	public E_AGPODao() {
		
	}

	public static List<E_AGPO__c> getRec( String conditions ){
		// 3ヶ月
		Set<String> periods = E_DownloadNoticeUtil.getPeriodSetAGPO();
		// レコードタイプ

		String soql = ' Select ';
		soql += ' Id ';
		soql += ' ,Name ';
		soql += ' ,ParentAccountId__c ';
	//	soql += ' ,ParentAccountName__c ';
		soql += ' ,AccountId__c ';
		soql += ' ,AccountName__c ';	//代理店事務所
		soql += ' ,ContactName__c ';
		soql += ' ,AccountOwnerName__c ';
		soql += ' ,Contact__r.Id ';
		soql += ' ,E_Policy__c';
		soql += ' ,OWNAME__c ';		//契約者名
	//	soql += ' ,IsZGRUP__c ';
		soql += ' ,CHDRNUM__c ';	// 証券番号
		soql += ' ,CNTTYPE__c ';
		soql += ' ,ZCOVRNAM__c ';
		/* SITP対応 */
		soql += ' ,InsuranceType__c ';
		soql += ' ,INSTAMT12__c ';	//保険料
		soql += ' ,ZSUMINS12N__c ';
		soql += ' ,ZCSV12N__c ';
		soql += ' ,CreatedDate ';	// 通知発信日
		soql += ' ,CONTENT__c ';	// 異動内容
		soql += ' ,RecordTypeNo__c ';
		soql += ' ,LIMITDT__c ';	//失効期限
		//外字対応
		soql +=	' ,ContractorName__c ';	//契約者名
		soql += ' ,E_Policy__r.COLI_ZCSHVAL__c';
		soql += ' ,E_Policy__r.COLI_ZEAPLTOT__c';
		soql += ' ,E_Policy__r.COLI_ZEPLTOT__c';
		soql += ' ,E_Policy__r.COLI_ZEADVPRM__c';
		soql += ' ,E_Policy__r.COLI_ZUNPREM__c';
		soql += ' ,E_Policy__r.COLI_ZCVDCF__c';
		soql += ' ,E_Policy__r.COLI_ZUNPCF__c';
		soql += ' ,E_Policy__r.COLI_ZUNPDCF__c';
		soql += ' ,E_Policy__r.MainAgent__c';
		soql += ' ,E_Policy__r.SubAgent__c';
		soql += ' ,E_Policy__r.recordTypeId';
		soql += ' ,E_Policy__r.recordType.DeveloperName';
		soql += ' ,E_Policy__r.SpClassification__c';
		soql += ' ,E_Policy__r.PolicyElapsedYears__c';
		soql += ' ,E_Policy__r.COMM_OCCDATE_yyyyMMdd__c';
		soql += ' From ';
		soql += ' E_AGPO__c ';
		soql += ' Where ';
		soql += ' YYYYMM__c In :periods ';
		soql += conditions;
		soql += ' Order by RecordTypeNo__c asc';
		soql += ' , YYYYMM__c desc';		//通知月
	//	soql += ' ,CreatedDate desc NULLS LAST';
		soql += ' ,AccountName__c desc NULLS LAST';
		soql += ' ,ContactName__c desc NULLS LAST';
		soql += ' ,COWNNUM__c desc';
		soql += ' ,CHDRNUM__c desc';
		soql += ' Limit 1001';
		return Database.query(soql);
	}
}