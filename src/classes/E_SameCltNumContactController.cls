public with sharing class E_SameCltNumContactController {
    public E_SameCltNumContactController() {

    }

    //Contact関連項目保持内部クラス
    public class contactUserEidcPolicy {
        //rownum
        public Transient Integer rowNum { get; set; }
        //contact
        public Transient String contactId { get; set; }
        public Transient String recordTypeName { get; set; }
        public Transient String contactName { get; set; }
        public Transient String e_CLTPF_CLNTNUM { get; set; }
        public Transient String e_CL3PF_CLNTNUM { get; set; }
        public Transient String e_CL3PF_AGNTNUM { get; set; }
        public Transient Boolean e_IsAgent { get; set; }
        public Transient Datetime createdDate { get; set; }
        //User
        public Transient Boolean isActive { get; set; }
        public Transient String userId { get; set; }
        //Eidc
        public Transient String eidcId { get; set; }
        public Transient String eidcName { get; set; }
        public Transient String e_ZWEBID { get; set; }
        public Transient String zIDTYPE { get; set; }
        //policy
        public Transient Integer policyCountAll { get; set; }
        public Transient Set<Id> policyIdSet { get; set; }
    }

    //Contact関連項目保持内部クラスリスト
    public List<contactUserEidcPolicy> contactList {get; private set;}

    /**
     * init
     * ページにパラメータで渡された顧客番号,募集人顧客コードがいずれもblankでない場合に
     * 顧客番号又は募集人顧客コードに関連するContactレコード一覧を取得する
     * @return PageReference: null
     */
    public PageReference init() {
        //顧客番号,募集人顧客コードを取得する
        String ecltpfCltnum = ApexPages.currentPage().getParameters().get('ecltpfCltnum');
        String ecl3pfCltnum = ApexPages.currentPage().getParameters().get('ecl3pfCltnum');
        //顧客番号,募集人顧客コードのNullでない項目から値取得
        String cltnum = String.isBlank(ecltpfCltnum) ? ecl3pfCltnum : ecltpfCltnum;

        //顧客番号が空文字じゃない場合に一覧取得
        if(!String.isBlank(cltnum)){
            contactList = new List<contactUserEidcPolicy>();
            createContactUserEidcPolicyList(cltnum);
        }

        return null;
    }

    /**
     * createContactUserEidcPolicyList
     * 顧客番号（募集人顧客コード）でContactリスト及び、
     * 関連するUser,EIDC,保険契約ヘッダの情報を取得し、表示用内部クラスへ設定する。
     * @param String cltnum: 顧客番号（募集人顧客コード）
     * @return void: 
     */
    private void createContactUserEidcPolicyList(String cltnum){
        //顧客番号でcontactリスト取得
        List<Contact> contacts = E_ContactDaoWithout.getRecsByCltnum(cltnum);
        //contactリスト関連情報を格納する内部クラス
        contactUserEidcPolicy contactUEP;

        //contactIDのSet作成
        Set<Id> conIdSet = new Set<Id>();
        for(Contact con : contacts){
            conIdSet.add(con.Id);
        }
        //contactIDのSetよりUserのMapを作成
        List<User> contactUserList = E_UserDao.getRecsAgentId(conIdSet);
        Map<Id,User> userMap = new Map<Id,User>();
        for(User conUser : contactUserList){
            userMap.put(conUser.Contact.Id, conUser);
        }
        //contactIDのSetよりEIDCのMapを作成
        Set<Id> userIdSet = (new Map<Id,User>(contactUserList)).keySet();
        List<E_IDCPF__c> contactEIDCList = E_IDCPFDao.getRecsByUserIds(userIdSet);
        Map<Id,E_IDCPF__c> eidcMap = new Map<Id,E_IDCPF__c>();
        for(E_IDCPF__c eidc : contactEIDCList){
            eidcMap.put(eidc.User__c, eidc);
        }

        //取得したcontact,user,eidcの情報を内部クラスへ設定しリストへadd
        if(contacts != null){
            Integer rNum = 1;
            for(Contact con: contacts){
                contactUEP = new contactUserEidcPolicy();

                contactUEP.rowNum           = rNum;
                contactUEP.contactId        = con.Id;
                contactUEP.contactName      = con.Name;
                contactUEP.recordTypeName   = con.RecordType.Name;
                contactUEP.e_CLTPF_CLNTNUM  = con.E_CLTPF_CLNTNUM__c;
                contactUEP.e_CL3PF_CLNTNUM  = con.E_CL3PF_CLNTNUM__c;
                contactUEP.e_CL3PF_AGNTNUM  = con.E_CL3PF_AGNTNUM__c;
                contactUEP.e_IsAgent        = con.E_IsAgent__c;
                contactUEP.createdDate      = con.CreatedDate;
                //Userが紐付く場合のみ
                if(userMap.get(con.Id) != null){
                    contactUEP.isActive     = userMap.get(con.Id).isActive;
                    contactUEP.userId       = userMap.get(con.Id).Id;
                    //ID管理が紐付く場合のみ(ID管理のユーザ項目、所有者はContactに紐付くUserである前提)
                    if(eidcMap.get(userMap.get(con.Id).Id) != null){
                        contactUEP.eidcId   = eidcMap.get(userMap.get(con.Id).Id).Id;
                        contactUEP.eidcName = eidcMap.get(userMap.get(con.Id).Id).Name;
                        contactUEP.e_ZWEBID = eidcMap.get(userMap.get(con.Id).Id).ZWEBID__c;
                        contactUEP.zIDTYPE  = eidcMap.get(userMap.get(con.Id).Id).ZIDTYPE__c;
                    }
                }
                //関連する契約を参照項目リストからIDのセットへ格納
                contactUEP.policyCountAll = 0;
                contactUEP.policyIdSet = new Set<Id>();
                //契約者
                if(con.E_PolicyContractors__r.size() > 0){
                    contactUEP.policyIdSet.addAll((new Map<Id,E_Policy__c>(con.E_PolicyContractors__r)).keySet());
                }
                //被保険者
                if(con.E_PolicyInsureds__r.size() > 0){
                    contactUEP.policyIdSet.addAll((new Map<Id,E_Policy__c>(con.E_PolicyInsureds__r)).keySet());
                }
                //年金受取人
                if(con.E_PolicyAnnuitants__r.size() > 0){
                    contactUEP.policyIdSet.addAll((new Map<Id,E_Policy__c>(con.E_PolicyAnnuitants__r)).keySet());
                }
                //主募集人
                if(con.E_PolicyMainAgents__r.size() > 0){
                    contactUEP.policyIdSet.addAll((new Map<Id,E_Policy__c>(con.E_PolicyMainAgents__r)).keySet());
                }
                //従募集人
                if(con.E_PolicySubAgents__r.size() > 0){
                    contactUEP.policyIdSet.addAll((new Map<Id,E_Policy__c>(con.E_PolicySubAgents__r)).keySet());
                }
                //団体顧客
                if(con.E_PolicyCorporates__r.size() > 0){
                    contactUEP.policyIdSet.addAll((new Map<Id,E_Policy__c>(con.E_PolicyCorporates__r)).keySet());
                }
                //関連付いた契約数
                contactUEP.policyCountAll = contactUEP.policyIdSet.size();

                //内部クラスリストへ追加
                contactList.add(contactUEP);
                rNum++;
            }
        }
    }

    //保険契約ヘッダの詳細リスト
    public Transient String policyDetailTargetContactName { get; set; }
    public Transient String selectedContactRowNum { get; set; }
    public Transient Id policyDetailTargetContactId { get; set; }
    //保険契約ヘッダ保持内部クラス
    public class policyDetailWrapper {
        public Transient String chdrNumLink { get; set; }
        public Transient E_Policy__c policyDetail { get; set; }
    }
    //保険契約ヘッダ保持内部クラスリスト
    public List<policyDetailWrapper> polDetailWrapList {get; private set;}

    /**
     * openPolicyDetail
     * policyDetailTargetContactIdに設定されたContactIｄが
     * 保険証券のいずれかの参照項目に設定された証券のリストを取得
     * @return PageReference: null
     */
    public PageReference openPolicyDetail(){
        polDetailWrapList = new List<policyDetailWrapper>();
        policyDetailWrapper polDetailWrap;

        //ContactIｄが保険証券のいずれかの参照項目に設定された証券のリストを取得
        for(E_Policy__c pol : E_PolicyDao.getRecsByContactID(policyDetailTargetContactId)){
            polDetailWrap = new policyDetailWrapper();
            //取得した証券レコード設定
            polDetailWrap.policyDetail = pol;
            //消滅契約も含めNNLinkVFページ用リンクの設定
            if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI){
                polDetailWrap.chdrNumLink = '/apex/E_Coli?id=' + pol.Id;
            } else if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_DCOLI){
                polDetailWrap.chdrNumLink = '/apex/E_DColi?id=' + pol.Id;
            } else if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA){
                if(pol.SpClassification__c == E_Const.SP_CLASS_SPVWL || 
                   pol.SpClassification__c == E_Const.SP_CLASS_SPVA  ||
                   pol.SpClassification__c == E_Const.SP_CLASS_SPVAANNUITY){
                    polDetailWrap.chdrNumLink = '/apex/E_Spva?id=' + pol.Id;
                } else if(pol.SpClassification__c == E_Const.SP_CLASS_ANNUITY){
                    polDetailWrap.chdrNumLink = '/apex/E_Annuity?id=' + pol.Id;
                }
            } else if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_DSPVA){
                polDetailWrap.chdrNumLink = '/apex/E_DSpva?id=' + pol.Id;
            }
            //Listへadd
            polDetailWrapList.add(polDetailWrap);
        }
        return null;
    }
}