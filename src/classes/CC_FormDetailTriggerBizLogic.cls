/**
 * コールセンター帳票情報トリガビジネスロジック.
 */
public with sharing class CC_FormDetailTriggerBizLogic {

	/**
	 * コールセンター帳票に紐付く帳票（添付ファイル）を再配置する.
	 *
	 * @param List<CC_FormDetail__c> コールセンター帳票情報リスト
	 * @return List<CC_FormDetail__c> 再配置処理を行ったコールセンター情報リスト
	 */
	public List<CC_FormDetail__c> relocateForm(List<CC_FormDetail__c> details) {
		// コールセンター帳票情報に紐付く帳票をコピーしケースに紐付ける
		List<Attachment> delAtts = new List<Attachment>();
		List<ContentVersion> insCvs = new List<ContentVersion>();
		Map<Id, CC_FormDetail__c> detailMap = new Map<Id, CC_FormDetail__c>(details);

		// 20181002
		//List<CC_FormDetail__c> procDetails = new List<CC_FormDetail__c>();
		Map<Id, CC_FormDetail__c> procDetailMap = new Map<Id, CC_FormDetail__c>();

		// Attachment取得
		List<Attachment> atts = CC_FormDetailTriggerBizLogicDao.getAttachmentsByParentIds(detailMap.keySet());

		// ContentVersion作成
		for(Attachment att : atts) {
			CC_FormDetail__c detail = detailMap.get(att.ParentId);

			ContentVersion cv = new ContentVersion();
			cv.VersionData = att.Body;
			cv.FirstPublishLocationId = detail.CC_Case__c;
			cv.PathOnClient = att.Name;
			cv.Title = att.Name;

			insCvs.add(cv);
			delAtts.add(att);

			// 20181002
			//procDetails.add(detail);
			if(procDetailMap.containsKey(detail.Id)) {
				continue;
			}

			procDetailMap.put(detail.Id, detail);

		}

		if(insCvs.isEmpty()) {
			return new List<CC_FormDetail__c>();
		}

		// コールセンター帳票情報に紐付く帳票を削除
		delete delAtts;

		// ContentVersion作成をInsert
		insert insCvs;

		// ContentVersionの共有設定
		this.shareForm(insCvs);

		// 再配置処理を行ったコールセンター情報リストを返す
		// 20181002
		//return procDetails;

		return procDetailMap.values();
	}

	/**
	 * 出力帳票の共有設定を行う.
	 *
	 * @param List<ContentVersion> ContentVersionのリスト
	 */
	private void shareForm(List<ContentVersion> cvs) {

		// ContentDocumentLink作成
		Set<Id> cdIds = new Set<Id>();

		for(ContentVersion cv : CC_FormDetailTriggerBizLogicDao.getContentVersionsByIds(new Map<Id, ContentVersion>(cvs).keySet())) {
			cdIds.add(cv.ContentDocumentId);
		}

		// OperationsのChatterグループ取得
		CollaborationGroup operationGroup = CC_FormDetailTriggerBizLogicDao.getCollaborationGroupByName(Label.CC_OperationGroupName);

		Id operationGroupId = Test.isRunningTest() ? UserInfo.getOrganizationId() : operationGroup.Id;

		if(String.isEmpty(operationGroupId)) {
			return;
		}

		List<ContentDocumentLink> cdls = new List<ContentDocumentLink>();

		for(Id cdId : cdIds) {
			ContentDocumentLink cdl = new ContentDocumentLink();
			cdl.ContentDocumentId = cdId;
			cdl.LinkedEntityId = operationGroupId;
			cdl.ShareType = 'V';

			cdls.add(cdl);
		}

		insert cdls;

		// FeedItem作成
		Map<Id, FeedItem> cvPostMap = new Map<Id, FeedItem>();

		for(ContentVersion cv : cvs) {
			FeedItem post = new FeedITem();
			post.body = cv.Title;
			post.ParentID = cv.FirstPublishLocationId;

			cvPostMap.put(cv.Id, post);
		}

		insert cvPostMap.values();

		// FeedAttachment作成
		List<FeedAttachment> insFeedAtts = new List<FeedAttachment>();

		for(Id cvId : cvPostMap.keySet()) {
			FeedAttachment postAttachment = new FeedAttachment();
			postAttachment.FeedEntityId = cvPostMap.get(cvId).id;
			postAttachment.RecordId = cvId;
			postAttachment.Type = 'Content';

			insFeedAtts.add(postAttachment);
		}

		insert insFeedAtts;
	}

	/**
	 * 活動作成が必要なコールセンター帳票情報を取得する.
	 *
	 * @param コールセンター帳票情報レコードのリスト
	 */
	public List<CC_FormDetail__c> getDetailsToCreateActibity(List<CC_FormDetail__c> details) {
		List<CC_FormDetail__c> result = new List<CC_FormDetail__c>();
		Set<Id> parentDetailIds = new Set<Id>();

		for(CC_FormDetail__c detail : details) {

			// 子で親の活動作成フラグがOFF
			if(detail.CC_ParentSRFormDetail__c != null && !detail.CC_IsParentSRFormDetailCreatedAct__c) {
				parentDetailIds.add(detail.CC_ParentSRFormDetail__c);

				continue;
			}

			// 親で活動作成フラグがOFF、もしくは単一帳票の場合
			if(detail.CC_ParentSRFormDetail__c == null && !detail.CC_IsCreatedSrActivity__c) {
				result.add(detail);

				continue;
			}
		}

		if(parentDetailIds.isEmpty()) {
			return result;
		}

		List<CC_FormDetail__c> parentDetails = CC_FormDetailTriggerBizLogicDao.getFormDetailsByIds(parentDetailIds);
		result.addAll(parentDetails);

		return result;
	}

	/**
	 * コールセンター帳票情報の活動作成済みフラグをONに設定する.
	 *
	 * @param List<CC_FormDetail__c> フラグをONにするコールセンター帳票情報レコードのリスト
	 * @param Boolean DML発行要否
	 */
	public void markIsCreatedActivity(List<CC_FormDetail__c> details, Boolean isNeedDml) {

		if(!isNeedDml) {
			for(CC_FormDetail__c detail : details) {
				detail.CC_IsCreatedSrActivity__c = true;
			}

			return;
		}

		List<CC_FormDetail__c> procDetails = new List<CC_FormDetail__c>();

		for(CC_FormDetail__c detail : details) {
			CC_FormDetail__c d = new CC_FormDetail__c();
			d.Id = detail.Id;
			d.CC_IsCreatedSrActivity__c = true;

			procDetails.add(d);
		}

		update procDetails;
	}
}