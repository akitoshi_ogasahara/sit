@isTest
private class TestDS_SearchController {

	/*
	 * pageAction_画面描画時_住生
	 */
	static testMethod void pageAction_test01(){
		//　テストデータ作成
		User sumiseiUser = TestDS_TestUtil.createSumiseiIRISUser(true);
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');

		System.runAs(sumiseiUser){
			PageReference pageRef = Page.E_DSSearch;
			Test.setCurrentPageReference(pageRef);
			DS_SearchController controller = new DS_SearchController();

			Test.startTest();
			controller.pageAction();
			Test.stopTest();

			System.assertEquals(DS_Const.APPNAME, controller.getPageName());
			System.assertEquals(DS_Const.PAGE_UNIQUE_KEY_SUMISEI, controller.getPageKey());
			System.assertEquals(I_Const.URL_PARAM_PAGEKEY, controller.getPageKeyParam());
			// 分類選択リスト
			System.assertEquals(4, controller.diseaseCategoryOptions.size());
		}
	}

	/*
	 * pageAction_一覧からの遷移時
	 */
	static testMethod void pageAction_test02(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('p', '4');
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.pageAction();
		Test.stopTest();

		System.assertEquals(DS_Const.PAGE_UNIQUE_KEY_NORMAL, controller.getPageKey());
		System.assertEquals(I_Const.URL_PARAM_PAGEKEY, controller.getPageKeyParam());
		System.assertEquals('4', controller.pNumber);
		System.assertEquals(5, controller.rowList.size());
		System.assertEquals('告知のポイント4', controller.diseaseNumber.record.Tips__c);
		System.assertEquals('障がいの部位4', controller.diseaseNumber.record.Symptoms__c);
		System.assertEquals('入院期間4', controller.diseaseNumber.record.Period__c);
		System.assertEquals('障がいの程度4', controller.diseaseNumber.record.Extent__c);
		System.assertEquals('現在の状況4', controller.diseaseNumber.record.Situation__c);
		System.assertEquals('必要な告知4', controller.diseaseNumber.NoticeDisplay);
		System.assertEquals('医的資料4', controller.diseaseNumber.DocumentDisplay);
		System.assertEquals('参考情報4', controller.diseaseNumber.InformationDisplay);
	}

	/*
	 * pageAction_一覧からの遷移時
	 */
	static testMethod void pageAction_test03(){
		// テストデータ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'システム管理者');
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		ApexPages.currentPage().getParameters().put('p', '4');
		ApexPages.currentPage().getParameters().put('isSMTM', '1');
		DS_SearchController controller = new DS_SearchController();

		System.runAs(actUser){
			Test.startTest();
			controller.pageAction();
			Test.stopTest();
		}

		System.assertEquals(DS_Const.PAGE_UNIQUE_KEY_SUMISEI, controller.getPageKey());
		System.assertEquals(I_Const.URL_PARAM_PAGEKEY, controller.getPageKeyParam());
		System.assertEquals('4', controller.pNumber);
		System.assertEquals(3, controller.rowList.size());
		System.assertEquals('告知のポイント4', controller.diseaseNumber.record.Tips__c);
		System.assertEquals('障がいの部位4', controller.diseaseNumber.record.Symptoms__c);
		System.assertEquals('入院期間4', controller.diseaseNumber.record.Period__c);
		System.assertEquals('障がいの程度4', controller.diseaseNumber.record.Extent__c);
		System.assertEquals('現在の状況4', controller.diseaseNumber.record.Situation__c);
		System.assertEquals('必要な告知(住生)4', controller.diseaseNumber.NoticeDisplay);
		System.assertEquals('医的資料(住生)4', controller.diseaseNumber.DocumentDisplay);
		System.assertEquals('参考情報(住生)4', controller.diseaseNumber.InformationDisplay);
	}

	/*
	 * キーワード検索_1ワードのlike検索(アルファベット)
	 */
	static testMethod void searchDiseaseName_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'alphabet');

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = 'B';
		controller.searchDiseaseName();
		Test.stopTest();

		System.assertEquals('B', controller.searchWord);
		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals(5, controller.searchCount);
		System.assertEquals(diseaseList[5].Name, controller.getRecs()[0].Name);
		System.assertEquals(diseaseNumberList[2].Name, controller.getRecs()[0].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[5].Katakana__c, controller.getRecs()[0].Katakana__c);
		System.assertEquals(diseaseList[6].Name, controller.getRecs()[1].Name);
		System.assertEquals(diseaseNumberList[3].Name, controller.getRecs()[1].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[6].Katakana__c, controller.getRecs()[1].Katakana__c);
		System.assertEquals(diseaseList[7].Name, controller.getRecs()[2].Name);
		System.assertEquals(diseaseNumberList[3].Name, controller.getRecs()[2].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[7].Katakana__c, controller.getRecs()[2].Katakana__c);
		System.assertEquals(diseaseList[8].Name, controller.getRecs()[3].Name);
		System.assertEquals(diseaseNumberList[3].Name, controller.getRecs()[3].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[8].Katakana__c, controller.getRecs()[3].Katakana__c);
		System.assertEquals(diseaseList[9].Name, controller.getRecs()[4].Name);
		System.assertEquals(diseaseNumberList[3].Name, controller.getRecs()[4].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[9].Katakana__c, controller.getRecs()[4].Katakana__c);
	}

	/*
	 * キーワード検索_複数キーワードのlike検索（半角ｶﾅと半角空白と全角空白を含めて繋ぎ、検索ワードとする）
	 */
	static testMethod void searchDiseaseName_test02(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(3);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = 'ｱ　ノ 病';
		controller.searchDiseaseName();
		Test.stopTest();

		System.assertEquals('ｱ　ノ 病', controller.searchWord);
		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals(6, controller.searchCount);
		System.assertEquals(diseaseList[0].Name, controller.getRecs()[0].Name);
		System.assertEquals(diseaseNumberList[0].Name, controller.getRecs()[0].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[0].Katakana__c, controller.getRecs()[0].Katakana__c);
		System.assertEquals(diseaseList[1].Name, controller.getRecs()[1].Name);
		System.assertEquals(diseaseNumberList[1].Name, controller.getRecs()[1].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[1].Katakana__c, controller.getRecs()[1].Katakana__c);
		System.assertEquals(diseaseList[2].Name, controller.getRecs()[2].Name);
		System.assertEquals(diseaseNumberList[1].Name, controller.getRecs()[2].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[2].Katakana__c, controller.getRecs()[2].Katakana__c);
		System.assertEquals(diseaseList[3].Name, controller.getRecs()[3].Name);
		System.assertEquals(diseaseNumberList[2].Name, controller.getRecs()[3].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[3].Katakana__c, controller.getRecs()[3].Katakana__c);
		System.assertEquals(diseaseList[4].Name, controller.getRecs()[4].Name);
		System.assertEquals(diseaseNumberList[2].Name, controller.getRecs()[4].diseaseNumber__r.Name);
		System.assertEquals(diseaseList[4].Katakana__c, controller.getRecs()[4].Katakana__c);
	}

	/*
	 * キーワード検索_検索ワードなし
	 */
	static testMethod void searchDiseaseName_test03(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = '';
		controller.searchDiseaseName();
		Test.stopTest();

		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(0, controller.searchCount);
	}

	/*
	 * キーワード検索_検索結果なし（作成するテストデータと一致しないキーワードを検索ワードとする）
	 */
	static testMethod void searchDiseaseName_test04(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = 'Aえの傷病';
		controller.searchDiseaseName();
		Test.stopTest();

		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(0, controller.searchCount);
	}

	/*
	 * キーワード検索_ページネーション_1ページ（キーワード検索結果1件）
	 */
	static testMethod void pagination_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = 'アあの傷病';
		controller.searchDiseaseName();
		Test.stopTest();

		System.assertEquals('アあの傷病', controller.searchWord);
		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals(1, controller.searchCount);
		System.assertEquals(diseaseList[0].Name, controller.getRecs()[0].Name);
	}

	/*
	 * キーワード検索_ページネーション(キーワード検索結果35件以上)
	 */
	static testMethod void pagination_test02(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(8);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = 'の傷病';
		controller.searchDiseaseName();
		controller.clickPageNumber = 6;
		controller.doPageNumber();
		Test.stopTest();

		System.assertEquals('の傷病', controller.searchWord);
		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals(36, controller.searchCount);
		System.assertEquals(diseaseList[25].Name, controller.getRecs()[0].Name);
		System.assertEquals(diseaseList[29].Name, controller.getRecs()[4].Name);
		System.assertEquals(8, controller.getTotalPages());
		System.assertEquals(5, controller.getPageNumberLinks().size());
		System.assertEquals(6, controller.clickPageNumber);
	}

	/*
	 * キーワード検索_結果選択
	 */
	static testMethod void doResultSearchKeyword_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.searchWord = 'あの傷病';
		controller.searchDiseaseName();
		//1or3or5
		controller.pNumber = '3';
		controller.selectedDisease = 'カあの傷病';
		controller.doResultSearchKeyword();
		Test.stopTest();

		System.assertEquals('あの傷病', controller.searchWord);
		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals(3, controller.searchCount);
		System.assertEquals(diseaseList[5].Name, controller.getRecs()[1].Name);
		System.assertEquals(diseaseList[5].Katakana__c, controller.getRecs()[1].Katakana__c);
		System.assertEquals(diseaseNumberList[2].Name, controller.diseaseNumber.record.Name);
		System.assertEquals('アえの傷病 / アおの傷病 / カあの傷病', controller.diseaseNamegroup);
		System.assertEquals('アエノショウビョウ / アオノショウビョウ / カアノショウビョウ', controller.diseaseNameKanaGroup);
		System.assertEquals(5, controller.rowList.size());
		System.assertEquals('告知のポイント3', controller.diseaseNumber.record.Tips__c);
		System.assertEquals('障がいの部位3', controller.diseaseNumber.record.Symptoms__c);
		System.assertEquals('入院期間3', controller.diseaseNumber.record.Period__c);
		System.assertEquals('障がいの程度3', controller.diseaseNumber.record.Extent__c);
		System.assertEquals('現在の状況3', controller.diseaseNumber.record.Situation__c);
		System.assertEquals('必要な告知3', controller.diseaseNumber.NoticeDisplay);
		System.assertEquals('医的資料3', controller.diseaseNumber.DocumentDisplay);
		System.assertEquals('参考情報3', controller.diseaseNumber.InformationDisplay);
	}

	/*
	 * 傷病表示(1つの傷病に複数の傷病が紐づいていること)
	 */
	static testMethod void displayDiseaseNumberData_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.pNumber = '3';
		controller.displayDiseaseNumberData();
		Test.stopTest();

		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals('アえの傷病 / アおの傷病 / カあの傷病', controller.diseaseNamegroup);
		System.assertEquals('アエノショウビョウ / アオノショウビョウ / カアノショウビョウ', controller.diseaseNameKanaGroup);
		System.assertEquals(5, controller.rowList.size());
		System.assertEquals('告知のポイント3', controller.diseaseNumber.record.Tips__c);
		System.assertEquals('障がいの部位3', controller.diseaseNumber.record.Symptoms__c);
		System.assertEquals('入院期間3', controller.diseaseNumber.record.Period__c);
		System.assertEquals('障がいの程度3', controller.diseaseNumber.record.Extent__c);
		System.assertEquals('現在の状況3', controller.diseaseNumber.record.Situation__c);
		System.assertEquals('必要な告知3', controller.diseaseNumber.NoticeDisplay);
		System.assertEquals('医的資料3', controller.diseaseNumber.DocumentDisplay);
		System.assertEquals('参考情報3', controller.diseaseNumber.InformationDisplay);
	}

	/*
	 * 傷病表示(1つの傷病に紐づく傷病が1つであること)
	 */
	static testMethod void displayDiseaseNumberData_test02(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.pNumber = '1';
		controller.displayDiseaseNumberData();
		Test.stopTest();

		System.assertNotEquals(null, controller.pageMessages);
		System.assertEquals('アあの傷病', controller.diseaseNamegroup);
		System.assertEquals('アアノショウビョウ', controller.diseaseNameKanaGroup);
		System.assertEquals(5, controller.rowList.size());
		System.assertEquals('告知のポイント1', controller.diseaseNumber.record.Tips__c);
		System.assertEquals('障がいの部位1', controller.diseaseNumber.record.Symptoms__c);
		System.assertEquals('入院期間1', controller.diseaseNumber.record.Period__c);
		System.assertEquals('障がいの程度1', controller.diseaseNumber.record.Extent__c);
		System.assertEquals('現在の状況1', controller.diseaseNumber.record.Situation__c);
		System.assertEquals('必要な告知1', controller.diseaseNumber.NoticeDisplay);
		System.assertEquals('医的資料1', controller.diseaseNumber.DocumentDisplay);
		System.assertEquals('参考情報1', controller.diseaseNumber.InformationDisplay);
	}

	/*
	 * 傷病表示_住生（注意事項記載画面パラメータ）
	 */
	static testMethod void displayDiseaseNumberData_test03(){
		// テストデータ作成
		User sumiseiUser = TestDS_TestUtil.createSumiseiIRISUser(true);
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		System.runAs(sumiseiUser){
			PageReference pageRef = Page.E_DSSearch;
			Test.setCurrentPageReference(pageRef);
			DS_SearchController controller = new DS_SearchController();

			Test.startTest();
			controller.pNumber = '3';
			controller.displayDiseaseNumberData();
			Test.stopTest();

			System.assertNotEquals(null, controller.pageMessages);
			System.assertEquals('アえの傷病 / アおの傷病 / カあの傷病', controller.diseaseNamegroup);
			System.assertEquals('アエノショウビョウ / アオノショウビョウ / カアノショウビョウ', controller.diseaseNameKanaGroup);
			System.assertEquals(3, controller.rowList.size());
			System.assertEquals('告知のポイント3', controller.diseaseNumber.record.Tips__c);
			System.assertEquals('障がいの部位3', controller.diseaseNumber.record.Symptoms__c);
			System.assertEquals('入院期間3', controller.diseaseNumber.record.Period__c);
			System.assertEquals('障がいの程度3', controller.diseaseNumber.record.Extent__c);
			System.assertEquals('現在の状況3', controller.diseaseNumber.record.Situation__c);
			System.assertEquals('必要な告知(住生)3', controller.diseaseNumber.NoticeDisplay);
			System.assertEquals('医的資料(住生)3', controller.diseaseNumber.DocumentDisplay);
			System.assertEquals('参考情報(住生)3', controller.diseaseNumber.InformationDisplay);
		}
	}

	/*
	 * 分類検索_傷病選択リスト生成
	 */
	static testMethod void diseaseOptions_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(9);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.pageAction();
		controller.selectedCategory = '感染症';
		Test.stopTest();

		// 分類選択リスト
		System.assertEquals(5, controller.diseaseCategoryOptions.size());
		System.assertEquals('感染症', controller.selectedCategory);
		System.assertEquals(16, controller.diseaseOptions.size());
	}

	/*
	 * PDF出力
	 */
	static testMethod void doDownloadPdf_test01(){
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		PageReference pageRef = Page.E_DSSearch;
		Test.setCurrentPageReference(pageRef);
		DS_SearchController controller = new DS_SearchController();

		Test.startTest();
		controller.pNumber = '3';
		controller.doDownloadPdf();
		Test.stopTest();

	}

	/*
	 * PDF出力_住生
	 */
	static testMethod void doDownloadPdf_test02(){
		// テストデータ作成
		User sumiseiUser = TestDS_TestUtil.createSumiseiIRISUser(true);
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');
		List<E_InsType_UnderWriting__c> underWritingList = TestDS_TestUtil.createUnderWritingData(diseaseNumberList);

		System.runAs(sumiseiUser){
			PageReference pageRef = Page.E_DSSearch;
			Test.setCurrentPageReference(pageRef);
			DS_SearchController controller = new DS_SearchController();

			Test.startTest();
			controller.pNumber = '3';
			controller.doDownloadPdf();
			Test.stopTest();

		}
	}
}