@isTest
private class TestE_BizDataSyncBatchOperation_SR {
	
	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static E_AgencySalesResults__c asr;

    /**
     * Database.QueryLocator getQueryLocator()
	 */
	static testMethod void getQueryLocator_test001(){
		/* Test Data */
		createData();
		
		// Test
		Test.startTest();
		
		E_BizDataSyncBatchOperation_SalesResults operation = new E_BizDataSyncBatchOperation_SalesResults();
		Database.QueryLocator dq = operation.getQueryLocator();
		
		Test.stopTest();
		
		// assertion
		System.assert(dq.getQuery().startsWith('SELECT Id'));
	}
	
    /**
     * void execute(List<Sobject> records)
	 */
	static testMethod void execute_test001(){
		/* Test Data */
		createData();
		
		List<Sobject> recs = new List<Sobject>();
		recs.add(asr);
		
		// Test
		Test.startTest();
		
		E_BizDataSyncBatchOperation_SalesResults operation = new E_BizDataSyncBatchOperation_SalesResults();
		operation.execute(recs);
		
		Test.stopTest();
	}
	
	/** *****************************************************************************************
	 * TestData
	 */
	private static void createData(){
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';
		
		//連携ログ作成
		E_BizDataSyncLog__c elog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = E_Util.formattedString2DateyyyyMMdd(BUSINESS_DATE));
		insert elog;
		// 公開グループ
		createGroup('L1' + CODE_DISTRIBUTE, 'L2' + CODE_OFFICE, 'BR' + CODE_BRANCH);
		createGroup('L1' + CODE_DISTRIBUTE_SUB, 'L2' + CODE_OFFICE_SUB, 'BR' + CODE_BRANCH_SUB);

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;
		
		// Account 事務所
		office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
		insert office;
		suboffice = new Account(Name = 'テスト事務所SUB',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
		insert suboffice;
		
		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH'
		);
		insert asr;
	}
	
	/**
	 * 公開グループ準備
	 */
	private static void createGroup(String d, String o, String b){
		// 既存の公開グループ
		Map<String, Group> grpMap = new Map<String, Group>();
		for(Group grp : [Select Id, DeveloperName From Group Where DeveloperName In (:d, :o, :b)]){
			grpMap.put(grp.DeveloperName, grp);
		}
		
		// 既存に存在しない公開グループ作成
		List<Group> grps = new List<Group>();
		if(grpMap.get(d) == null){
			grps.add(new Group(Name=d, DeveloperName=d, Type='Regular'));
		}
		if(grpMap.get(o) == null){
			grps.add(new Group(Name=o, DeveloperName=o, Type='Regular'));
		}
		if(grpMap.get(b) == null){
			grps.add(new Group(Name=b, DeveloperName=b, Type='Regular'));
		}
		if(!grps.isEmpty()){
			insert grps;
		}
	}
}