/*
 * CC_VILClientSearchControllerTest
 * Test class of CC_VILClientSearchController
 * created  : Accenture 2018/7/10
 * modified :
 */

@isTest
private class CC_VILClientSearchControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			vlocity_ins__VlocitySearchWidgetSetup__c newVlocitySearchWidgetSetup = CC_TestDataFactory.getVlocitySearchWidgetSetupSkel('CC_VILClientSearchController');
			insert newVlocitySearchWidgetSetup;

			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert account;

			//Create Contact
			RecordType recordTypeOfContact = [SELECT Id FROM RecordType where SobjectType = 'Contact' and Name = '顧客' limit 1];
			List<Contact> contactList = new List<Contact>();
			contactList.add(new Contact(
							RecordTypeId = recordTypeOfContact.Id,
							FirstName = 'test1',
							LastName = 'Contact1',
							E_CLTPF_CLNTNUM__c = '10001',
							E_CLTPF_ZCLKNAME__c = 'テストコンタクトイチ',
							E_CLTPF_CLTPHONE01__c = '12345678',
							E_CLTPF_CLTTYPE__c = 'C',
							AccountId = account.Id
							));
			contactList.add(new Contact(
							RecordTypeId = recordTypeOfContact.Id,
							FirstName = 'test2',
							LastName = 'Contact2',
							E_CLTPF_CLNTNUM__c = '10002',
							E_CLTPF_ZCLKNAME__c = 'テストコンタクトニ',
							E_CLTPF_CLTPHONE01__c = '12345678',
							E_CLTPF_CLTTYPE__c = 'P',
							AccountId = account.Id
							));

			Insert contactList;

			//Create EPolicy
			RecordType recordTypeOfEpolicy = [SELECT Id FROM RecordType where SobjectType = 'E_Policy__c' and DeveloperName = 'ESPHPF' limit 1];
			List<E_Policy__c> ePolicyList = new List<E_Policy__c>();
			ePolicyList.add(new E_Policy__c(
							RecordTypeId = recordTypeOfEpolicy.Id,
							COMM_CHDRNUM__c = '10001',
							SubAgentAccount__c = account.Id,
							Contractor__c = contactList[0].Id,
							Insured__c = contactList[0].Id
							));
			ePolicyList.add(new E_Policy__c(
							RecordTypeId = recordTypeOfEpolicy.Id,
							COMM_CHDRNUM__c = '10002',
							SubAgentAccount__c = account.Id,
							Contractor__c = contactList[1].Id,
							Insured__c = contactList[1].Id
							));
			Insert ePolicyList;

		}
	}

	/**
	 * Test getSearchRequest
	 */
	static testMethod void getSearchRequestTest() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchRequest';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting lookupRequest
			inputMap.put('lookupRequest', (Object)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0]);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * policyNumber search
	 */
	static testMethod void getSearchResultsTest01() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('policyNumber', '10001');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * policyNumber fuzzy search
	 */
	static testMethod void getSearchResultsTest02() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('policyNumber', '*');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * clientNumber search
	 */
	static testMethod void getSearchResultsTest03() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('clientNumber', '10001');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * clientNumber fuzzy search
	 */
	static testMethod void getSearchResultsTest04() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('clientNumber', '*');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * clientKanaName & phone & birthdate search
	 */
	static testMethod void getSearchResultsTest05() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('clientKanaName', 'テストコンタクト');
			searchValueMap.put('phone', '12345678');
			searchValueMap.put('birthdate', '-');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * clientKanaName & phone & birthdate fuzzy search
	 */
	static testMethod void getSearchResultsTest06() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('clientKanaName', '*');
			searchValueMap.put('phone', '*');
			searchValueMap.put('birthdate', '*');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test launchConsole
	 */
	static testMethod void launchConsoleTest() {
		CC_VILClientSearchController ctrl = new CC_VILClientSearchController();
		String methodName = 'launchConsole';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting Launch data
			inputMap.put('interactionObjName', 'test');
			inputMap.put('recordId', 'test');
			inputMap.put('name', 'test');

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}