/**
 * 共通データアクセスクラス（Selectを除くDML）
 * CreatedDate 2014/12/03
 * @Author Terrasky
 */
public class E_GenericDao {
	public static final String DMLMODE_INS = 'INS';
	public static final String DMLMODE_DEL = 'DEL';

	/**
	 *		UPDATE処理
	 *			一部のみの成功を許可する
	 */
	public static Map<Id, String> updatePartial(List<SObject> records){
		Map<Id, String> errorRecordsMessage = new Map<Id, String>();
		// Update処理
		List<Database.Saveresult> results = Database.Update(records, false);
			
		// Errorレコード判定
		Integer i = 0;
		for (Database.SaveResult sr : results) {
			if(!sr.isSuccess()) {
				Database.Error err = sr.getErrors()[0];
				errorRecordsMessage.put(records[i].Id, err.getMessage());
			}
			i++;
		} 
		return errorRecordsMessage;
	}
 
	/**
	 *		INSERT処理
	 *			エラーレコード結果を受け取る
	 */
	public static Map<SObject, Database.Error[]> insertPartial(List<SObject> records){
		Map<SObject, Database.Error[]> ret = new Map<SObject, Database.Error[]>();
	
		// Insert処理
		List<Database.Saveresult> results = Database.Insert(records, false);
			
		// Errorレコード判定
		Integer i = 0;
		for (Database.SaveResult sr : results) {
			if(!sr.isSuccess()) {
				ret.put(records[i], sr.getErrors());
			}
			i++;
		} 
		return ret;
	}

	/**
	 *		Delete処理
	 *			エラーレコード結果を受け取る
	 */
	public static Map<SObject, Database.Error[]> deletePartial(List<SObject> records){
		Map<SObject, Database.Error[]> ret = new Map<SObject, Database.Error[]>();
	
		// Delete処理
		List<Database.DeleteResult> results = Database.Delete(records, false);
			
		// Errorレコード判定
		Integer i = 0;
		for (Database.DeleteResult dr : results) {
			if(!dr.isSuccess()) {
				ret.put(records[i], dr.getErrors());
			}
			i++;
		} 
		return ret;
	}


	/**
	 *		Upsert処理
	 *			ページからの更新専用（レコード1件のみ　pageMessageでメッセージを受け取ります）
	 */
	//pageMessageHolderのclearMessageは呼出元で呼び出してください
	public static Boolean saveViaPage(SObject rec){
		try{
			
			List<SObject> recs = new List<SObject>{rec};
			Database.SaveResult[] results;
			if(rec.Id == null){
				results = Database.insert(recs,false);
			}else{
				results = Database.update(recs,false);
			}
			
			System.assertEquals(results.size(),1);	//1件のみのDML結果
			
			if(results[0].isSuccess()){
				return true;
			}else{
				for(Database.Error err:results[0].getErrors()){
					E_PageMessagesHolder.getInstance().addErrorMessage(err.getMessage());		//+String.join(err.getFields(),'|')
				}
			}
		}catch(Exception e){
			E_PageMessagesHolder.getInstance().addErrorMessage('保存(saveViaPage)時エラー：'+e.getMessage());
		}
		return false;
	}

}