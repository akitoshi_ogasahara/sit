global with sharing class I_SalesPlanExtender extends E_AbstractExtender{

	//URLParameterで受け取る営業計画IDのキー
	public static final String URL_PARAM_PLANID = 'planId';		//I_SalesPlanPDF?planId=xxxx　で利用
	public static final String URL_PARAM_ISPDF = 'ispdf';		//PDF表示の場合に'1'
	public static final String URL_PARAM_PDFTitle = 'title';		//PDFの名前を設定
	public static final String URL_PARAM_ISCLEAR = 'isClear';		//未入力出力

	public I_SalesPlanController extension {get; set;}
	
	// 営業計画
	private E_SalesPlan__c record;
	
	public I_SalesPlanDataHolder planData{get;set;}


	// 営業計画ID
	public Id salesPlanId{get; set;}
	//　特定保険種類
	public SkyEditor2__SkyEditorDummy__c spInsType_ly {get; set;}
	public SkyEditor2__SkyEditorDummy__c spInsType_h1 {get; set;}
	public SkyEditor2__SkyEditorDummy__c spInsType_h2 {get; set;}
	public SkyEditor2__SkyEditorDummy__c spInsType_cy {get; set;}
	public SkyEditor2__SkyEditorDummy__c spInsType_total {get; set;}
	// 新契約係数ANP
	public SkyEditor2__SkyEditorDummy__c anp_ly {get; set;}
	public SkyEditor2__SkyEditorDummy__c anp_h1 {get; set;}
	public SkyEditor2__SkyEditorDummy__c anp_h2 {get; set;}
	public SkyEditor2__SkyEditorDummy__c anp_cy {get; set;}
	public SkyEditor2__SkyEditorDummy__c anp_total {get; set;}
	// IQA継続率
	public SkyEditor2__SkyEditorDummy__c iqa_h1 {get{System.debug(iqa_h1); return iqa_h1;} set;}
	public SkyEditor2__SkyEditorDummy__c iqa_h2 {get{System.debug(iqa_h2); return iqa_h2;} set;}
	public SkyEditor2__SkyEditorDummy__c iqa_total {get{System.debug(iqa_total); return iqa_total;} set;}
	// 希望サポート内容
	public List<String> supportContentList{get; set;}
	// 計画・商談内容・メモ欄
	public SkyEditor2__SkyEditorDummy__c memo {get; set;}
	// 代理店名
	public SkyEditor2__SkyEditorDummy__c agencyName {get; set;}
	// 募集人名
	public SkyEditor2__SkyEditorDummy__c agentName {get; set;}
	// MR名
	public SkyEditor2__SkyEditorDummy__c mrName {get; set;}

	// 営業計画PDFタイトル
	public SkyEditor2__SkyEditorDummy__c pdfTitle {get; set;}

	public Boolean getIsPDF(){
		return ApexPages.currentPage().getParameters().get(URL_PARAM_ISPDF) == '1';
	}

	public Boolean getIsClear(){
		return ApexPages.currentPage().getParameters().get(URL_PARAM_ISCLEAR) == 'true';
	}

	/*	Constructor	*/
	public I_SalesPlanExtender(I_SalesPlanController ext){
		this.extension = ext;
	}

	/*	init	
	 *		SVE Controllerの初期処理で呼出しされる。
	 */
	public override void init(){
		 this.record = this.extension.record;
System.assert(this.record!=null, '営業計画書レコードが取得できませんでした。');
		
		if(getIsClear() && getIsPDF()){
			//未入力作成
			this.planData = new I_SalesPlanDataHolder(this.record, true, true);
		}else if(getIsPDF()){
			//PDF表示時はDeserialize
			this.planData = I_SalesPlanDataHolder.createByDeserialize(this.record.WK_SalesPlan__c);
		}else{
			//新規作成
			this.planData = new I_SalesPlanDataHolder(this.record, true, false);
		}

			// 代理店挙積情報
			this.spInsType_ly = createSVEDumm(planData.spInsType_ly);
			this.spInsType_h1 = createSVEDumm(planData.spInsType_h1);
			this.spInsType_h2 = createSVEDumm(planData.spInsType_h2);
			this.spInsType_cy = createSVEDumm(planData.spInsType_cy);
			this.spInsType_total = createSVEDumm(planData.spInsType_total);
			this.anp_ly = createSVEDumm(planData.anp_ly);
			this.anp_h1 = createSVEDumm(planData.anp_h1);
			this.anp_h2 = createSVEDumm(planData.anp_h2);
			this.anp_cy = createSVEDumm(planData.anp_cy);
			this.anp_total = createSVEDumm(planData.anp_total);
			//this.iqa_ly = createSVEDumm(planData.iqa_ly);
			this.iqa_h1 = createSVEDumm(planData.iqa_h1);
			this.iqa_h2 = createSVEDumm(planData.iqa_h2);
			//this.iqa_cy = createSVEDumm(planData.iqa_cy);
			this.iqa_total = createSVEDumm(planData.iqa_total);
			this.supportContentList = planData.supportContents;
			this.memo = createSVEDumm2(planData.memo);
			this.agencyName = createSVEDumm(planData.agencyName);
			this.agentName = createSVEDumm(planData.agentName);
			this.mrName = createSVEDumm(planData.mrName);
			this.pdfTitle = createSVEDumm(planData.pdfTitle);
	}

	/**
	 *	createSVEDumm
	 *		SVEDummyオブジェクトに営業計画データを詰める
	 */
	//　数値型 
	private SkyEditor2__SkyEditorDummy__c createSVEDumm(decimal val){
		SkyEditor2__SkyEditorDummy__c dum = new SkyEditor2__SkyEditorDummy__c();
		dum.SkyEditor2__Number__c = val;
		return dum;
	}

	//　文字列型 
	private SkyEditor2__SkyEditorDummy__c createSVEDumm(String val){
		SkyEditor2__SkyEditorDummy__c dum = new SkyEditor2__SkyEditorDummy__c();
		dum.SkyEditor2__text__c = val;
		return dum;
	}

	// ロングテキストエリア型
	private SkyEditor2__SkyEditorDummy__c createSVEDumm2(String val){
		SkyEditor2__SkyEditorDummy__c dum = new SkyEditor2__SkyEditorDummy__c();
		dum.SkyEditor2__LongTextArea__c = val;
		return dum;
	}

	/* IQ継続率　年月 */
	//	年
	public String getYYYY(){
		return String.isBlank(this.record.SalesResult__r.YYYYMM__c)?
				'':this.record.SalesResult__r.YYYYMM__c.subString(0,4);
	}
	//	月
	public String getMM(){
		return String.isBlank(this.record.SalesResult__r.YYYYMM__c)?
				'':this.record.SalesResult__r.YYYYMM__c.subString(5,6);
	}


	/*	doSave
	 *		入力されたデータをJSON形式で営業計画レコードに保存
	 */
	private void doSave(){
		planData.spInsType_ly = spInsType_ly.SkyEditor2__Number__c;
		planData.spInsType_h1 = spInsType_h1.SkyEditor2__Number__c;
		planData.spInsType_h2 = spInsType_h2.SkyEditor2__Number__c;
		planData.spInsType_cy = spInsType_cy.SkyEditor2__Number__c;
		planData.spInsType_total = spInsType_cy.SkyEditor2__Number__c;
		planData.anp_ly = anp_ly.SkyEditor2__Number__c;
		planData.anp_h1 = anp_h1.SkyEditor2__Number__c;
		planData.anp_h2 = anp_h2.SkyEditor2__Number__c;
		planData.anp_cy = anp_cy.SkyEditor2__Number__c;
		planData.anp_total = anp_cy.SkyEditor2__Number__c;
		planData.iqa_h1 = iqa_h1.SkyEditor2__text__c;
		planData.iqa_h2 = iqa_h2.SkyEditor2__text__c;
		planData.iqa_total = iqa_total.SkyEditor2__text__c;
		planData.memo = memo.SkyEditor2__LongTextArea__c;
		planData.agencyName = agencyName.SkyEditor2__text__c;
		planData.agentName = agentName.SkyEditor2__text__c;
		planData.mrName = mrName.SkyEditor2__text__c;
		planData.pdfTitle = pdfTitle.SkyEditor2__text__c;
		record.WK_SalesPlan__c = planData.toJSON();
		upsert record;
	}

	/*	doSaveAndCreatePDF
	 *		PDF作成ボタン押下処理
	 */
	public PageReference doSaveAndCreatePDF(){
		try{
			if(!String.isBlank(pdfTitle.SkyEditor2__text__c)){
				//保存処理の実行
				doSave();
		
				//PDF生成＆Attach用のページへ遷移する。
				PageReference pr = Page.IRIS_SalesPlanPDF;
				pr.getParameters().put(URL_PARAM_PLANID, record.Id);
				pr.getParameters().put(URL_PARAM_PDFTitle, pdfTitle.SkyEditor2__text__c);
				pr.getParameters().put(URL_PARAM_ISCLEAR, 'false');
				return pr;
			}
		}catch(Exception e){
			pageMessages.addErrorMessage('PDF作成処理呼出しに失敗しました。[' + e.getMessage() + ']');
		}
		return null;
	}
	/*	createClearPDF
	 *		未入力ボタン押下処理
	 */
	public PageReference createClearPDF(){
		try{
			//PDF生成＆Attach用のページへ遷移する。
			PageReference pr = Page.IRIS_SalesPlanPDF;
			pr.getParameters().put(URL_PARAM_PLANID, record.Id);
			pr.getParameters().put(URL_PARAM_PDFTitle, pdfTitle.SkyEditor2__text__c);
			pr.getParameters().put(URL_PARAM_ISCLEAR, 'true');
			return pr;
		}catch(Exception e){
			pageMessages.addErrorMessage('PDF作成処理呼出しに失敗しました。[' + e.getMessage() + ']');
		}
		return null;
	}
}