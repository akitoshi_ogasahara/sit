@isTest
private class TestCR_OutSrcExtender {
	
	// コンストラクタテスト
  	private static testMethod void CROutSrcsTest001(){
    	CR_OutSrcsController controller;
        CR_OutSrcsExtender target = new CR_OutSrcsExtender(controller);
        System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_OUTSRC);
        System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_OUTSRC_List);
  	}
	
  	// initテスト (外部委託参照権限なしエラー)
	private static testMethod void CROutSrcEditTest002(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
				
	        	PageReference pref = Page.E_CROutSrcs;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_OutSrcsController controller = new CR_OutSrcsController(standardcontroller);
	        	CR_OutSrcsExtender target = new CR_OutSrcsExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest();
        }
	}

  	// initテスト２ (レコードId、またはレコード自体が存在しない場合)
	private static testMethod void CROutSrcEditTest003(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){
        	Test.startTest();
				CR_Agency__c agny = New CR_Agency__c();
				
	        	PageReference pref = Page.E_CROutSrcs;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_OutSrcsController controller = new CR_OutSrcsController(standardcontroller);
	        	CR_OutSrcsExtender target = new CR_OutSrcsExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest(); 	
        } 
	}

	// initテスト３
	private static testMethod void CROutSrcEditTest004(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	
	        	PageReference pref = Page.E_CROutSrcs;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_OutSrcsController controller = new CR_OutSrcsController(standardcontroller);
	        	CR_OutSrcsExtender target = new CR_OutSrcsExtender(controller);
	        	target.init();

				System.assertEquals(target.isSuccessOfInitValidate, true);
	        Test.stopTest();
		}
	}

	// 新規外部委託
	private static testMethod void CROutSrcsTest005(){
		Account acc = TestCR_TestUtil.createAccount(true);
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
		
        Test.startTest();
        	PageReference pref = Page.E_CROutSrcs;
        	Test.setCurrentPage(pref);
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
			CR_OutSrcsController controller = new CR_OutSrcsController(standardcontroller);
        	CR_OutSrcsExtender target = new CR_OutSrcsExtender(controller);
        	
        	System.assertEquals(target.getUrlForNew(), '/apex/e_croutsrcedit?parentId=' + agny.Id);
        Test.stopTest();  
	}
}