public with sharing class E_IDCPFTriggerHandler {

	public static Boolean isSkipTriggerActions = false;

	private E_IDCPFTriggerBizLogic bizLogic;
	
	//コンストラクタ
	public E_IDCPFTriggerHandler(){
		bizLogic = new E_IDCPFTriggerBizLogic();
	}

    //before update
    public static void onBeforeUpdate(List<E_IDCPF__c> newList,Map<ID,E_IDCPF__c> oldMap){
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        bizLogic.updateEIDCContact(newList,oldMap);
    }

    //before insert
    public static void onBeforeInsert(List<E_IDCPF__c> newList){
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        bizLogic.updateEIDCContact(newList);
    }

    //after insert
    public static void onAfterInsert(List<E_IDCPF__c> newList){
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        bizLogic.updateEidcpf(newList);
    }

    //after update
    public static void onAfterUpdate(List<E_IDCPF__c> newList,Map<ID,E_IDCPF__c> oldMap){
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        bizLogic.updateEidcpf(newList,oldMap);
    }

    //after insert/update/undelete
    @future
    public static void OnAfterModifiedAsync(Set<ID> newIDs){
        //権限セットの付与は非同期で実施
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        List<E_IDCPF__c> idcpfs = E_IDCPFDao.getActiveUsersRecsByIDs(newIDs);
        bizLogic.addTargetUsers(idcpfs);
        bizLogic.PermissionSetAssign();

        //共有グループ設定
        E_IDCPFTriggerEditGroup.MentenanceReport(E_IDCPFDao.getActiveUsersRecsByIDs(newIDs));
    }

    //after delete
    @future
    public static void OnAfterDeleteAsync(Set<ID> oldIDs){
        //権限セットの削除は非同期で実施
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        bizLogic.PermissionSetDelete(E_IDCPFDao.getActiveUsersRecsByIDs(oldIDs, true)); //削除済のEIDCから権限セットを削除する
    }

    //after update
    public static void OnAfterUpdateLog(Map<Id, E_IDCPF__c> oldMap, Map<Id, E_IDCPF__c> newMap){
        //更新項目のログを出力
        E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
        bizLogic.outputItemChangedLog(oldMap, newMap);
    }
}