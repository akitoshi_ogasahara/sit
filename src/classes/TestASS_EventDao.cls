@isTest
private class TestASS_EventDao {

	//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}

	//エリア部長で実行
	private static testMethod void getEventsByIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			Event retEv = ASS_EventDao.getEventsById(eventList[0].Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEv.Id,eventList[0].Id);
		}
	
	}

	private static testMethod void getThisMonthEventsByOffIdSetTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = offList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = offList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = offList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Event> retEvList = ASS_EventDao.getThisMonthEventsByOffIdSet(eventRecType.Id, officeMap.keySet());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEvList.size(),3);
		}
	
	}

	private static testMethod void getCurrentEventsByWhatIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test4', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Event> retEvList = ASS_EventDao.getCurrentEventsByWhatId(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEvList.size(),3);
		}
	
	}

	private static testMethod void getOldEventsByWhatIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test4', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Event> retEvList = ASS_EventDao.getOldEventsByWhatId(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEvList.size(),3);
		}
	
	}


	//MRで実行
	private static testMethod void getEventsByIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test4', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			Event retEv = ASS_EventDao.getEventsById(eventList[0].Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEv.Id,eventList[0].Id);
		}
	
	}

	private static testMethod void getThisMonthEventsByOffIdSetMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> offList = new List<Account>();
		offList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		offList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id));
		insert offList;

		Map<Id,Account> officeMap = new Map<Id,Account>(offList);

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = offList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = offList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = offList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Event> retEvList = ASS_EventDao.getThisMonthEventsByOffIdSet(eventRecType.Id, officeMap.keySet());
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEvList.size(),3);
		}
	
	}

	private static testMethod void getCurrentEventsByWhatIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);

		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));

		eventList.add(new Event(Subject = 'test4', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Event> retEvList = ASS_EventDao.getCurrentEventsByWhatId(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEvList.size(),3);
		}
	
	}

	private static testMethod void getOldEventsByWhatIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		
		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now().addMonths(-1).addDays(-1), endDatetime = System.now().addMonths(-1).addDays(-1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test4', WhatId = office.Id, startDatetime = System.now().addMonths(1), endDatetime = System.now().addMonths(1), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<Event> retEvList = ASS_EventDao.getOldEventsByWhatId(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retEvList.size(),3);
		}
	
	}
}