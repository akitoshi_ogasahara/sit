/**
 * 
 */
@isTest
private class TestI_NoticeAbstractController {
    
    //ProfileName
    private static String PF_SYSTEM = 'システム管理者';
    private static String PF_EMPLOYEE = 'E_EmployeeStandard';
    private static String PF_PARTNER = 'E_PartnerCommunity';
    
    //User情報
    private static User user;
    private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];   //Id取得
    private static User communityUser;
    private static Account ahAcc;
    private static Account ayAcc;
    private static Contact atCon;

    /**
     * Integer getTotalRows()
     */
    static testMethod void getTotalRows_test01() {
        // Data
        createAccount();
        createData(5, null);
//        createUser(PF_EMPLOYEE);
        createUser(PF_SYSTEM);
        createDataAccessObj(user.Id, 'ER');

        System.runAs(user){
            Test.startTest();
            
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            
            Test.stopTest();
            // assertion
            System.assertEquals(5, controller.getTotalRows());
        }
    }
    
    /**
     * Integer getListMaxRows()
     */
    static testMethod void getListMaxRows_test01() {
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(I_Const.LIST_MAX_ROWS, controller.getListMaxRows());
    }
    
    /**
     * Boolean getIsViewDl()
     * 対象件数0件
     */
    static testMethod void getIsViewDl_test01() {
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(false, controller.getIsViewDl());
    }
    
    /**
     * Boolean getIsViewDl()
     * 対象件数0件以上、社員ユーザ、フィルターモード：なし
     */
    static testMethod void getIsViewDl_test02() {
        // Data
        createAccount();
        createData(5, null);
        
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(false, controller.getIsViewDl());
    }
    
    /** [TODO]
     * Boolean getIsViewDl()
     * 対象件数0件以上、社員ユーザ、フィルターモード：あり
     */
    static testMethod void getIsViewDl_test03() {
        // Data
        createAccount();
        createData(5, null);
        
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(false, controller.getIsViewDl());
    }
    
    /**
     * PageReference init()
     * 対象件数0件
     */
    static testMethod void init_test01() {  
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(true, controller.pageMessages.hasMessages());
    }
    
    /**
     * PageReference init()
     * 対象件数1001件
     */
    static testMethod void init_test02() { 
        // Data
        createAccount();
        createData(1001, null);
//        createUser(PF_EMPLOYEE);
        createUser(PF_SYSTEM);
        createDataAccessObj(user.Id, 'ER');
        System.runAs(user){
            Test.startTest();
            
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            
            Test.stopTest();
            // assertion
            System.assertEquals(true, controller.pageMessages.hasMessages());
            System.assertEquals(1000, controller.dtos.size());
            System.assertEquals(1000, controller.rowCnt);
        }
    }
    
    /**
     * void setViewColumns()
     * 社員
     */
    static testMethod void setViewColumns_test01() { 
        // Data
        createAccount();
        createUser(PF_EMPLOYEE);
            
        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            
            // assertion
            System.assertEquals(true, controller.isViewMr);
            System.assertEquals(true, controller.isViewDistribute);
            System.assertEquals(true, controller.isViewOffice);
            System.assertEquals(true, controller.isViewAgent);
        }
        
        Test.stopTest();
    }
    
    /**
     * void setViewColumns()
     * AH
     */
    static testMethod void setViewColumns_test02() { 
        // Data
        createAccount();
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AH');

        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            
            // assertion
            System.assertEquals(false, controller.isViewMr);
            System.assertEquals(false, controller.isViewDistribute);
            System.assertEquals(true, controller.isViewOffice);
            System.assertEquals(true, controller.isViewAgent);
        }

        Test.stopTest();
    }
    
    /**
     * void setViewColumns()
     * AY
     */
    static testMethod void setViewColumns_test03() { 
        // Data
        createAccount();
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AY');
            
        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            
            // assertion
            System.assertEquals(false, controller.isViewMr);
            System.assertEquals(false, controller.isViewDistribute);
            System.assertEquals(false, controller.isViewOffice);
            System.assertEquals(true, controller.isViewAgent);
        }

        Test.stopTest();
    }
    
    /**
     * void setViewColumns()
     * AT
     */
    static testMethod void setViewColumns_test04() { 
        // Data
        createAccount();
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AT');
            
        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            
            // assertion
            System.assertEquals(false, controller.isViewMr);
            System.assertEquals(false, controller.isViewDistribute);
            System.assertEquals(false, controller.isViewOffice);
            System.assertEquals(false, controller.isViewAgent);
        }

        Test.stopTest();
    }
    
    /**
     * PageReference addRows()
     */
    static testMethod void addRows_test01() { 
        // Data
        createAccount();
        createData(20, null);
            
        Integer beforeCnt = 0;
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        beforeCnt = controller.rowCount;
        controller.addRows();
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(beforeCnt + I_Const.LIST_ADD_MORE_ADD_ROWS, controller.rowCount);
    }
    
    /**
     * PageReference addRows()
     * 1000件以上
     */
    static testMethod void addRows_test02() { 
        // Data
        createAccount();
        createData(990, null);
            
        Integer beforeCnt = 0;
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        beforeCnt = controller.rowCount;
        for(Integer i = 0; i < 501; i++){
            controller.addRows();   
        }
        
        Test.stopTest();
        
        // assertion
        System.assertEquals(I_Const.LIST_MAX_ROWS, controller.rowCount);
    }
    
    /**
     * PageReference doDownloadCsv()
     * AH
     */
    static testMethod void doDownloadCsv_test01() { 
        // Data
        createAccount();
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AH','ah001');
        createData(5, user.Id);
            
        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            controller.doDownloadCsv();
            
            // assertion
            List<E_DownloadHistorry__c> rec  = [Select Id From E_DownloadHistorry__c];
            System.assert(rec.size() > 0);
        }

        Test.stopTest();
    }
    
    /**
     * PageReference doDownloadCsv()
     * AY
     */
    static testMethod void doDownloadCsv_test02() { 
        // Data
        createAccount();
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AY','ay001');
        createData(5, user.Id);
            
        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            controller.doDownloadCsv();
            
            // assertion
            List<E_DownloadHistorry__c> rec  = [Select Id From E_DownloadHistorry__c];
            System.assert(rec.size() > 0);
        }

        Test.stopTest();
    }
    
    /**
     * PageReference doDownloadCsv()
     * AT
     */
    static testMethod void doDownloadCsv_test03() { 
        // Data
        createAccount();
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AT','at001');
        createData(5, user.Id);
            
        Test.startTest();
        
        System.runAs(user){
            I_NoticeBILAController controller = new I_NoticeBILAController();
            controller.pageAction();
            controller.doDownloadCsv();
            
            // assertion
            List<E_DownloadHistorry__c> rec  = [Select Id From E_DownloadHistorry__c];
            System.assert(rec.size() > 0);
        }

        Test.stopTest();
    }
    
    /**
     * void sortRows()
     */
    static testMethod void sortRows_test01() { 
        // Data
        createAccount();
        createData(5, null);
            
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        //ページ情報
        PageReference pref = Page.E_DownloadNoticeBILA;
        pref.getParameters().put('st', 'owname');
        Test.setCurrentPage(pref);
        
        controller.sortRows();

        Test.stopTest();
        
        // assertion
        System.assertEquals('owname', controller.sortType);
        System.assertEquals(true, controller.sortIsAsc);
    }
    
    /**
     * void sortRows()
     */
    static testMethod void sortRows_test02() { 
        // Data
        createAccount();
        createData(5, null);
            
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        
        //ページ情報
        PageReference pref = Page.E_DownloadNoticeBILA;
        pref.getParameters().put('st', 'owname');
        Test.setCurrentPage(pref);
        
        controller.sortRows();
        
        pref.getParameters().put('st', 'owname');
        Test.setCurrentPage(pref);
        
        controller.sortRows();

        Test.stopTest();
        
        // assertion
        System.assertEquals('owname', controller.sortType);
        System.assertEquals(false, controller.sortIsAsc);
    }
    
    /**
     * string getDownloadId()
     */
    static testMethod void getDownloadId_test01() { 
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        String result = controller.getDownloadId();

        Test.stopTest();
        
        // assertion
        System.assertEquals('', result);
    }
    
    /**
     * string getDownloadId()
     */
    static testMethod void getDownloadId_test02() { 
        // Data
        createAccount();
        createData(5, null);
            
        Test.startTest();
        
        I_NoticeBILAController controller = new I_NoticeBILAController();
        controller.pageAction();
        controller.doDownloadCsv();
        String result = controller.getDownloadId();

        Test.stopTest();
        
        // assertion
        List<E_DownloadHistorry__c> dh = [Select Id From E_DownloadHistorry__c];
        System.assertEquals(dh[0].Id, result);
    }
    
    /**
     * String createNoticeCondition()
     * 代理店
     */
    static testMethod void createNoticeCondition_test01() {
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, 'ER');
        System.runAs(user){
            E_IRISHandler iris = E_IRISHandler.getInstance();
            iris.setPolicyCondition('agency', '代理店名', '123456789012345678', 'breadCrumb');
            Test.startTest();
            I_NoticeBILAController controller = new I_NoticeBILAController();
            String result = controller.createNoticeCondition();

            Test.stopTest();
            // assertion
            //System.assertEquals(' AND (E_Policy__r.MainAgentAccountParentId__c = \'agency\'' + ' OR E_Policy__r.SubAgentAccountParent__c = \'agency\')', result);
            System.assertEquals(' AND SearchParentAccountId__c = \'123456789012345678\'', result);
        }
    }
    
    /**
     * String createNoticeCondition()
     * 事務所
     */
    static testMethod void createNoticeCondition_test02() { 
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, 'ER');
        System.runAs(user){
            E_IRISHandler iris = E_IRISHandler.getInstance();
            iris.setPolicyCondition('office', '事務所名', '123456789012345678', 'breadCrumb');
            
            Test.startTest();
            
            I_NoticeBILAController controller = new I_NoticeBILAController();
            String result = controller.createNoticeCondition();

            Test.stopTest();
            
            // assertion
            //System.assertEquals(' AND (E_Policy__r.MainAgentAccount__c = \'office\'' + ' OR E_Policy__r.SubAgentAccount__c = \'office\')', result);
            System.assertEquals(' AND SearchAccountId__c = \'123456789012345678\'', result);
        }
    }
    
    /**
     * String createNoticeCondition()
     * 募集人
     */
    static testMethod void createNoticeCondition_test03() { 

        //代理店作成
        //Account acc = TestI_TestUtil.createAccount(true,null);
        ////事務所作成
        //Account office = TestI_TestUtil.createAccount(true,acc);
        ////募集人作成
        //Contact con = TestI_TestUtil.createContact(false,office.Id,'test');
        ////募集人コード
        //con.E_CL3PF_AGNTNUM__c = '12345';
        //insert con;
        createAccount();

        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, 'ER');
        System.runAs(user){
            E_IRISHandler iris = E_IRISHandler.getInstance();
            iris.setPolicyCondition('agent', '募集人名', atCon.Id, 'breadCrumb');
            //iris.setPolicyCondition('agent', '募集人名', '123456789012345678', 'breadCrumb');
            
            Test.startTest();
            
            I_NoticeBILAController controller = new I_NoticeBILAController();
            String result = controller.createNoticeCondition();

            Test.stopTest();
            String expected = ' AND SearchContactId__c = \'' + atCon.Id +'\'';
            // assertion
            //System.assertEquals(' AND (E_Policy__r.MainAgent__c = \'agent\'' + ' OR E_Policy__r.SubAgent__c = \'agent\')', result);
            System.assertEquals(expected, result);
            //System.assertEquals(' AND SearchContactId__c = \'123456789012345678\'', result);
        }
    }
    
    /** data************************************************************** */
    private static void createAccount(){
        system.runAs(thisuser){
            // Account 代理店格
            ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
            insert ahAcc;
    
            // Account 事務所
            ayAcc = new Account(
                    Name = 'office1'
                    ,ParentId = ahAcc.Id
                    ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                    ,E_COMM_VALIDFLAG__c = '1'
            );
            insert ayAcc;
    
            // Contact 募集人
            atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
            atCon.E_CL3PF_AGNTNUM__c = 'at001';
            atCon.email = 'fstest@terrasky.ingtesting';
            insert atCon;
        }
    }
    
    private static void createUser(String profileName){
        String userName = 'test@terrasky.ingtesting';
        Profile p = [Select Id From Profile Where Name = :profileName];
        
        // Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );
        
        // User
        if(profileName != PF_PARTNER){
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            user.UserRoleId = portalRole.Id;
            insert user;
            
        // Partner User
        }else{
            user.ContactId = atCon.Id;
            insert user;
                
            ContactShare cs = new ContactShare(
                            ContactId = atCon.Id,
                            ContactAccessLevel = 'read',
                            UserOrGroupId = user.Id);
            insert cs;
        }
    }
    
    private static void createDataAccessObj(Id userId, String idType){
        //system.runAs(thisuser){
        //    // 権限割り当て
        //    TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
        //    // ID管理
        //    E_IDCPF__c idcpf = new E_IDCPF__c(
        //        User__c = userId
        //        ,ZIDTYPE__c = idType
        //        ,FLAG01__c = '1'
        //        ,FLAG06__c = '1'
        //        ,ZSTATUS01__c = '1'
        //        ,OwnerId = userId
        //    );
        //    insert idcpf;
        //}
        createDataAccessObj(userId, idType, 'BR**');
    }
    
    private static void createDataAccessObj(Id userId, String idType, String zinquirr){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
                ,ZINQUIRR__c = zinquirr
            );
            insert idcpf;
        }
    }
    
    private static void createData(Integer dataCnt, String shareUserId){
        system.runAs(thisuser){
            //保険契約ヘッダ（個人タイプ）
            E_Policy__c policy = new E_Policy__c();
            policy = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');
    
            // Share
            if(String.isNotBlank(shareUserId)){
                E_Policy__Share ps = new E_Policy__Share(
                                ParentId = policy.Id,
                                AccessLevel = 'read',
                                UserOrGroupId = shareUserId);
                insert ps;
            }
    
            //販売取扱者
            E_CHTPF__c chtpf = new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
            insert chtpf;
    
            //保険料請求予告通知
            List<E_BILA__c> bilas = new List<E_BILA__c>();
            //対象月
            date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA);
            for(Integer i = 0; i < dataCnt; i++){
                //一か月間の表示に変更
//                Date d = targetDate.addMonths(-2);
                Date d = targetDate;
                E_BILA__c bila = new E_BILA__c(
                    ParentAccount__c = ahAcc.Id
                    , Account__c = ayAcc.Id
                    , AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c
                    , Contact__c = atCon.Id
                    , E_Policy__c = policy.Id
                    , E_CHTPF__c = chtpf.Id
                    , YYYY__c = d.year()
                    , MM__c = d.month()
                    , OWNAME__c = 'Owner' + i
                    , ZJINTSEQ__c = '1'
                );
                bilas.add(bila);
            }
            insert bilas;
        system.debug('■作成データ'+bilas);
        }
    }
}