@isTest
private class TestASS_UnitSummaryController {
	//テストデータ作成
	private static List<E_Unit__c> createUnit() {
		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		List<E_Unit__c> units = new List<E_Unit__c>();
		units.add(new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト2営業部', BRANCH__c = '01', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト3営業部', BRANCH__c = '02', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト4営業部', BRANCH__c = '03', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト5営業部', BRANCH__c = '04', IsActive__c = '1', Area__c = area.Id));
		units.add(new E_Unit__c(Name = 'テスト6営業部', BRANCH__c = '05', IsActive__c = '1', Area__c = area.Id));
		insert units;

		return units;
	}

	/**
	 * 実行ユーザ: システム管理者
	 * パラメータ: なし
	 * システム管理者の場合の画面と画面遷移
	 */
	private static testMethod void systemAdminTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'システム管理者');

		//営業部作成
		List<E_Unit__c> units = createUnit();

		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_UnitSummaryController con = new ASS_UnitSummaryController();
			pr.getParameters().put('unitId', String.valueOf(units[0].id));
			con.redirectToMRSummary();
			con.getUnitSummary();
			con.moveToMRSummary();
//===============================テスト終了===============================
			Test.stopTest();

			PageReference newPage = Page.ASS_MRSummary;
			newPage.getParameters().put('unitId',units[0].id);
			newPage.setRedirect(true);
			System.assertEquals(con.moveToMRSummary().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: なし
	 * エリア部長の場合の画面と画面遷移
	 */
	private static testMethod void areaManagerTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'amtest', 'エリア部長');

		//営業部作成
		List<E_Unit__c> units = createUnit();

		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_UnitSummaryController con = new ASS_UnitSummaryController();
			pr.getParameters().put('unitId', String.valueOf(units[0].id));
			con.redirectToMRSummary();
			con.getUnitSummary();
			con.moveToMRSummary();
//===============================テスト終了===============================
			Test.stopTest();

			PageReference newPage = Page.ASS_MRSummary;
			newPage.getParameters().put('unitId',units[0].id);
			newPage.setRedirect(true);
			System.assertEquals(con.moveToMRSummary().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: 営業推進部
	 * パラメータ: なし
	 * エリア部長の場合の画面と画面遷移
	 */
	private static testMethod void sipTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'amtest', '営業推進部');

		//営業部作成
		List<E_Unit__c> units = createUnit();

		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_UnitSummaryController con = new ASS_UnitSummaryController();
			pr.getParameters().put('unitId', String.valueOf(units[0].id));
			con.redirectToMRSummary();
			con.getUnitSummary();
			con.moveToMRSummary();
//===============================テスト終了===============================
			Test.stopTest();

			PageReference newPage = Page.ASS_MRSummary;
			newPage.getParameters().put('unitId',units[0].id);
			newPage.setRedirect(true);
			System.assertEquals(con.moveToMRSummary().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ:MR
	 * パラメータ: なし
	 * MRの場合の画面と画面遷移
	 */
	private static testMethod void mrtest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'mrtest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		insert actUser;

		//営業部作成
		List<E_Unit__c> units = createUnit();

		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_UnitSummaryController con = new ASS_UnitSummaryController();
			con.redirectToMRSummary();

//===============================テスト終了===============================
			Test.stopTest();

			PageReference newPage = Page.ASS_MRSummary;
			System.assertEquals(con.redirectToMRSummary().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ:MR
	 * パラメータ: なし
	 * 拠点長の場合の画面と画面遷移
	 */
	private static testMethod void directortest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'mrtest', '拠点長');
		actUser.Unit__c = 'テスト1営業部';
		insert actUser;

		//営業部作成
		List<E_Unit__c> units = createUnit();

		//ページ表示
		PageReference pr = Page.ASS_UnitSummary;
		Test.setCurrentPage(pr);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_UnitSummaryController con = new ASS_UnitSummaryController();
			con.redirectToMRSummary();

//===============================テスト終了===============================
			Test.stopTest();

			PageReference newPage = Page.ASS_MRSummary;
			System.assertEquals(con.redirectToMRSummary().getUrl(),newPage.getUrl());
		}
	}
}