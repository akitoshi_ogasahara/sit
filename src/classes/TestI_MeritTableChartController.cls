@isTest
private class TestI_MeritTableChartController {

    //ProfileName
    private static String PF_SYSTEM = 'システム管理者';
    private static String PF_EMPLOYEE = 'E_EmployeeStandard';
    private static String PF_PARTNER = 'E_PartnerCommunity';

    //User情報
    private static User user;
    private static List<Account> ayAccList;
    //private static Contact atCon;
    private static List<E_Policy__c> policyList;
    private static List<E_COVPF__c> covList;
    private static I_ContentMaster__c content;
    //今日に対する条件に合う契約
    private static String occDate;

    /*
    * 観点:pageAction　メソッド正常系
    * 条件:証券のIdをパラメータに付与してpageActionを実行
    * 検証1:Controllerで証券を取得し、クラス変数に格納されること
    * 検証2:実効税率にデフォルト値が設定されていること
    */
    @isTest static void test_pageAction_001() {
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = TestI_PIPTestUtil.createPolicys();
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);

            PageReference pageRef = Page.IRIS_MeritTableChart;
            Test.setCurrentPageReference(pageRef);
            ApexPages.currentPage().getParameters().put('policyId', pol.Id);
            I_MeritTableChartController controller = new I_MeritTableChartController();

            Test.startTest();

            controller.pageAction();

            Test.stopTest();

            System.assertEquals(pol.Id, controller.policy.Id);
            System.assertEquals(controller.taxRate , I_PIPConst.EFFECTIVE_TAX_RATE );
        }
    }

    /*
    * 観点:pageAction getCovers　updateMeritTableData メソッド正常系
    * 条件:推移表表示可能な証券で実行
    * 検証:エラーフラグがfalseであること。エラーメッセージが表示されないこと
    */
    @isTest static void test_updateMeritTableData_001() {
    	Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        //user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        //TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        //TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){

			E_policy__c pol = TestI_PIPTestUtil.createPolicyInfo();

            //CRコードTTの個人保険特約リスト(主契約・特約)を作成
            List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(pol);
            ttParams[0].crCode = 'TT';
            List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);
            TestI_PIPTestUtil.createInsuredInfo(pol);

	        PageReference pageRef = Page.IRIS_MeritTableChart;
	        Test.setCurrentPageReference(pageRef);

	        ApexPages.currentPage().getParameters().put('policyId', pol.Id);
	        I_MeritTableChartController controller = new I_MeritTableChartController();

            DateTime localTime = Datetime.newInstance(1993,9,6,6,0,0) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
            I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;


	        Test.startTest();
			controller.pageAction();
	        controller.updateMeritTableData();
            system.assertEquals(false,controller.errorFlg);
            system.assert(controller.modalMessage == null);

	        Test.stopTest();

        }

    }

    /*
    * 観点:pageAction getCovers　updateMeritTableData メソッド異常系
    * 条件:対象商品外の証券で実行
    * 検証:エラーフラグがtrueであること。対象の商品でない旨のメッセージが表示されること。
    */
    @isTest static void test_updateMeritTableData_002() {
    	Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
			E_policy__c pol = TestI_PIPTestUtil.createPolicyInfo();

            //CRコードTTの個人保険特約リスト(主契約・特約)を作成
            List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(pol);
            ttParams[0].crCode = 'WL';
            List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);
            TestI_PIPTestUtil.createInsuredInfo(pol);

	        PageReference pageRef = Page.IRIS_MeritTableChart;
	        Test.setCurrentPageReference(pageRef);

	        ApexPages.currentPage().getParameters().put('policyId', pol.Id);
	        I_MeritTableChartController controller = new I_MeritTableChartController();

            DateTime localTime = Datetime.newInstance(1993,9,6,6,0,0) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
            I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

	        Test.startTest();

			controller.pageAction();
	        controller.updateMeritTableData();
            system.assertEquals(true,controller.errorFlg);
            //system.assert(controller.modalMessage == I_PIPConst.ERROR_MODAL_MESSAGE_UNAVEIRABLE_PRODUCT);
            system.assertEquals(controller.getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_UNAVEIRABLE_PRODUCT),controller.modalMessage);

	        Test.stopTest();

        }

    }

    /*
    * 観点:pageAction getCovers　updateMeritTableData メソッド異常系
    * 条件:対象商品の証券で時間外に実行
    * 検証:エラーフラグがtrueであること。サービス時間でない旨のメッセージが表示されること。
    */
    @isTest static void test_updateMeritTableData_003() {
    	Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
			E_policy__c pol = TestI_PIPTestUtil.createPolicyInfo();

            //CRコードTTの個人保険特約リスト(主契約・特約)を作成
            List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(pol);
            ttParams[0].crCode = 'TT';
            List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);
            TestI_PIPTestUtil.createInsuredInfo(pol);

	        PageReference pageRef = Page.IRIS_MeritTableChart;
	        Test.setCurrentPageReference(pageRef);

	        ApexPages.currentPage().getParameters().put('policyId', pol.Id);
	        I_MeritTableChartController controller = new I_MeritTableChartController();

            DateTime localTime = Datetime.newInstance(1993,9,6,5,0,0) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
            I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

	        Test.startTest();

			controller.pageAction();
	        controller.updateMeritTableData();
            system.assertEquals(true,controller.errorFlg);
            system.assertEquals(controller.getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_OUT_OF_SERVICE_TIME), controller.modalMessage);
            //system.assert(controller.modalMessage == I_PIPConst.ERROR_MODAL_MESSAGE_OUT_OF_SERVICE);

	        Test.stopTest();

        }

    }

    /*
    * 観点:pageAction getCovers　updateMeritTableData メソッド異常系
    * 条件:対象商品かつ前納の証券で実行
    * 検証:エラーフラグがtrueであること。対象の契約でない旨のメッセージが表示されること。
    */
    @isTest static void test_updateMeritTableData_004() {
    	Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
			E_policy__c pol = TestI_PIPTestUtil.createPolicyInfo();
            pol.COMM_STATCODE__c = 'R';
            update pol;

            //CRコードTTの個人保険特約リスト(主契約・特約)を作成
            List<TestI_PIPTestUtil.CoverParam> ttParams = TestI_PIPTestUtil.createDefaultcParam(pol);
            ttParams[0].crCode = 'TT';
            List<E_COVPF__c> covers = TestI_PIPTestUtil.createCover(ttParams);
            TestI_PIPTestUtil.createInsuredInfo(pol);

	        PageReference pageRef = Page.IRIS_MeritTableChart;
	        Test.setCurrentPageReference(pageRef);

	        ApexPages.currentPage().getParameters().put('policyId', pol.Id);
	        I_MeritTableChartController controller = new I_MeritTableChartController();

            DateTime localTime = Datetime.newInstance(1993,9,6,5,0,0) + UserInfo.getTimeZone().getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
            I_PIPMeritTableAvailabilityChecker.testDatetime = localTime;

	        Test.startTest();

			controller.pageAction();
	        controller.updateMeritTableData();
            system.assertEquals(true,controller.errorFlg);
            system.assertEquals(controller.getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_UNAVEIRABLE_CONTRACT),controller.modalMessage);
            //system.assert(controller.modalMessage == I_PIPConst.ERROR_MODAL_MESSAGE_UNAVEIRABLE_CONTRACT);

	        Test.stopTest();

        }

    }

    /*
    * 観点:elapsedYears メソッド正常系
    * 条件:契約日が5か月前の証券のIdをパラメータに付与してpageActionを実行
    * 検証:現在年数が1年になること
    */
    @isTest static void test_getElapsedYears_001() {
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        Date tdy = Date.today();
        occDate = String.valueOf(tdy.addMonths(-5)).remove('-');

        System.runAs(user){

            policyList = new List<E_Policy__c>();
            policyList = TestI_PIPTestUtil.createPolicys(occDate);
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);
            covList = TestI_PIPTestUtil.createCOV(pol);

            PageReference pageRef = Page.IRIS_MeritTableChart;
            Test.setCurrentPageReference(pageRef);
            ApexPages.currentPage().getParameters().put('policyId', pol.Id);
            I_MeritTableChartController controller = new I_MeritTableChartController();

            Test.startTest();

            controller.pageAction();

            Test.stopTest();
            System.assertEquals('1',controller.elapsedYears);
        }
    }

    /*
    * 観点:elapsedYears メソッド正常系
    * 条件:契約日が5年前の1日後の証券のIdをパラメータに付与してpageActionを実行
    * 検証:現在年数が5年になること
    */
    @isTest static void test_getElapsedYears_002() {
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        Date tdy = Date.today();
        Date pst = tdy.addYears(-5);
        occDate = String.valueOf(pst.addDays(1)).remove('-');

        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = TestI_PIPTestUtil.createPolicys(occDate);
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);
            covList = TestI_PIPTestUtil.createCOV(pol);

            PageReference pageRef = Page.IRIS_MeritTableChart;
            Test.setCurrentPageReference(pageRef);
            ApexPages.currentPage().getParameters().put('policyId', pol.Id);
            I_MeritTableChartController controller = new I_MeritTableChartController();

            Test.startTest();

            controller.pageAction();

            Test.stopTest();
            System.assertEquals('5',controller.elapsedYears);
        }
    }

    /*
    * 観点:elapsedYears メソッド正常系
    * 条件:契約日が5年前の1ヶ月後の証券のIdをパラメータに付与してpageActionを実行
    * 検証:現在年数が5年になること
    */
    @isTest static void test_getElapsedYears_003() {
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        Date tdy = Date.today();
        Date pst = tdy.addYears(-5);
        occDate = String.valueOf(pst.addMonths(1)).remove('-');

        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = TestI_PIPTestUtil.createPolicys(occDate);
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);
            covList = TestI_PIPTestUtil.createCOV(pol);

            PageReference pageRef = Page.IRIS_MeritTableChart;
            Test.setCurrentPageReference(pageRef);
            ApexPages.currentPage().getParameters().put('policyId', pol.Id);
            I_MeritTableChartController controller = new I_MeritTableChartController();

            Test.startTest();

            controller.pageAction();

            Test.stopTest();
            System.assertEquals('5',controller.elapsedYears);
        }
    }

    /*
    * 観点:getPledges メソッド正常系
    * 条件:質権を設定した証券のIdをパラメータに付与してpageActionを実行
    * 検証:適切な質権者が設定されていること
    */
    @isTest static void test_getPledges_001() {
        //User
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = TestI_PIPTestUtil.createPolicys();
            TestI_PIPTestUtil.createContentMaster();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);
            TestI_PIPTestUtil.createCrlpf(pol, E_Const.CLRRROLE_NE);

            //ページ情報
            PageReference pref = Page.IRIS_MeritTableChart;
            pref.getParameters().put('policyId', pol.Id);
            Test.setCurrentPage(pref);
            //テスト開始
            Test.startTest();

            I_MeritTableChartController controller = new I_MeritTableChartController();
            controller.pageAction();

            List<I_ColiDatas.Pledge> result = controller.getPledges();

            System.assertEquals('照須快',result[0].zclname);

            //テスト終了
            Test.stopTest();
        }
    }

    /*
    * 観点:insertPIPDownloadMeritDetail メソッド正常系
    * 条件:画面表示用DTOリストに3年分値を設定してinsertPIPDownloadMeritDetailを実行
    * 検証:PIPDownloadMeritDetailレコードに、画面表示用DTOリストの値が想定通りに設定されていること
    * 検証:PIPDownloadMeritDetailレコードの3年目に、適切に現在年フラグが設定されていること
    */
    @isTest static void test_insertPIPDownloadMeritDetail_main(){

        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
            Date occDate = Date.today().addYears(-2);
            String s_occDate = String.valueOf(occDate).remove('-');

            List<E_Policy__c> policyList;
            policyList = TestI_PIPTestUtil.createPolicys(s_occDate);
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);
            covList = TestI_PIPTestUtil.createCOV_CR(pol);
            TestE_TestUtil.createEbizLog(true);

            List<I_MeritTableDTO> meritTableDTOList = new List<I_MeritTableDTO>();

            I_MeritTableDTO record1 = new I_MeritTableDTO();

            record1.age = '60';
            record1.simpleHenreiRt = 80.00;
            record1.keikaNensu = '1';
            record1.sonkinP = 1000;
            record1.shisanKeijoGaku = 2000;
            record1.jisshitsuHenreiRt = 90.00;
            record1.pRuikei = 3000;
            record1.jisshitsuFutanGaku = 500;
            record1.S = 10000;
            record1.koukaGakuRuikei = 20000;
            record1.cv = 4000;
            record1.koukaGaku = 5000;

            meritTableDTOList.add(record1);

            I_MeritTableDTO record2 = new I_MeritTableDTO();

            record2.age = '61';
            record2.simpleHenreiRt = 80.01;
            record2.keikaNensu = '2';
            record2.sonkinP = 1001;
            record2.shisanKeijoGaku = 2001;
            record2.jisshitsuHenreiRt = 90.01;
            record2.pRuikei = 3001;
            record2.jisshitsuFutanGaku = 501;
            record2.S = 10001;
            record2.koukaGakuRuikei = 20001;
            record2.cv = 4001;
            record2.koukaGaku = 5001;

            meritTableDTOList.add(record2);

            I_MeritTableDTO record3 = new I_MeritTableDTO();

            record3.age = '62';
            record3.simpleHenreiRt = 80.02;
            record3.keikaNensu = '3';
            record3.sonkinP = 1002;
            record3.shisanKeijoGaku = 2002;
            record3.jisshitsuHenreiRt = 90.02;
            record3.pRuikei = 3002;
            record3.jisshitsuFutanGaku = 502;
            record3.S = 10002;
            record3.koukaGakuRuikei = 20002;
            record3.cv = 4002;
            record3.koukaGaku = 5002;

            meritTableDTOList.add(record3);


            I_MeritTableChartController con = new I_MeritTableChartController();
            con.mainCovpf = covList[0];

            con.meritTableDTOList = meritTableDTOList;
            con.policy = pol ;
            con.setElapsedYears();
            con.taxRate = I_PIPConst.EFFECTIVE_TAX_RATE;

            con.insertPIPDownloadMeritDetail();


            I_PIPDownloadHistory__c pdh = [select S__c, Hokensyurui__c ,Syokenbango__c,Keiyakusha__c,Hihokensha__c,Detakijunbi__c,Jikkozeiritsu__c  from I_PIPDownloadHistory__c limit 1];

            //PDFヘッダの証券番号の値をテスト
            System.assertEquals(pol.COMM_CHDRNUM__c,pdh.Syokenbango__c);

            //PDFヘッダの契約者の値をテスト
            System.assertEquals(pol.ContractorName__c, pdh.Keiyakusha__c);

            //PDFヘッダの被保険者の値をテスト
            System.assertEquals(pol.InsuredName__c, pdh.Hihokensha__c);

            //PDFヘッダの保険種類の値をテスト
            System.assertEquals(covList[0].COMM_ZCOVRNAM__c, pdh.Hokensyurui__c);

            //PDFヘッダの保障額の値をテスト
            System.assertEquals(covList[0].COMM_SUMINS__c, pdh.S__c);

            //PDFヘッダのデータ基準日のテスト
            String kijunbi = String.valueOf(Date.valueOf(system.today()));//yyyy-mm-dd形式
            System.assertEquals( kijunbi.replace( '-','/' ) ,pdh.Detakijunbi__c);

            //PDFヘッダの実効税率のテスト
            System.assertEquals( I_PIPConst.EFFECTIVE_TAX_RATE , pdh.Jikkozeiritsu__c );



            List<I_PIPDownloadMeritDetail__c> dlm = [select isCurrent__c,age__c, simpleHenreiRt__c, keikaNensu__c, sonkinP__c,shisanKeijoGaku__c,
                                                        jisshitsuHenreiRt__c, pRuikei__c, jisshitsuFutanGaku__c, S__c, koukaGakuRuikei__c, cv__c, koukaGaku__c
                                                        from I_PIPDownloadMeritDetail__c order by keikanensu__c asc ];

            System.assertEquals(false,dlm[0].isCurrent__c);
            System.assertEquals(record1.keikaNensu,dlm[0].keikanensu__c);
            System.assertEquals(record1.age,dlm[0].age__c);
            System.assertEquals(record1.simpleHenreiRt,dlm[0].simpleHenreiRt__c);
            System.assertEquals(record1.keikaNensu,dlm[0].keikaNensu__c);
            System.assertEquals(record1.sonkinP,dlm[0].sonkinP__c);
            System.assertEquals(record1.shisanKeijoGaku,dlm[0].shisanKeijoGaku__c);
            System.assertEquals(record1.jisshitsuHenreiRt,dlm[0].jisshitsuHenreiRt__c);
            System.assertEquals(record1.pRuikei,dlm[0].pRuikei__c);
            System.assertEquals(record1.jisshitsuFutanGaku,dlm[0].jisshitsuFutanGaku__c);
            System.assertEquals(record1.S,dlm[0].S__c);
            System.assertEquals(record1.koukaGakuRuikei,dlm[0].koukaGakuRuikei__c);
            System.assertEquals(record1.cv,dlm[0].cv__c);
            System.assertEquals(record1.koukaGaku,dlm[0].koukaGaku__c);

            System.assertEquals(false,dlm[1].isCurrent__c);
            System.assertEquals(record2.age,dlm[1].age__c);
            System.assertEquals(record2.simpleHenreiRt,dlm[1].simpleHenreiRt__c);
            System.assertEquals(record2.keikaNensu,dlm[1].keikaNensu__c);
            System.assertEquals(record2.sonkinP,dlm[1].sonkinP__c);
            System.assertEquals(record2.shisanKeijoGaku,dlm[1].shisanKeijoGaku__c);
            System.assertEquals(record2.jisshitsuHenreiRt,dlm[1].jisshitsuHenreiRt__c);
            System.assertEquals(record2.pRuikei,dlm[1].pRuikei__c);
            System.assertEquals(record2.jisshitsuFutanGaku,dlm[1].jisshitsuFutanGaku__c);
            System.assertEquals(record2.S,dlm[1].S__c);
            System.assertEquals(record2.koukaGakuRuikei,dlm[1].koukaGakuRuikei__c);
            System.assertEquals(record2.cv,dlm[1].cv__c);
            System.assertEquals(record2.koukaGaku,dlm[1].koukaGaku__c);

            System.assertEquals(true,dlm[2].isCurrent__c);
            System.assertEquals(record3.age,dlm[2].age__c);
            System.assertEquals(record3.simpleHenreiRt,dlm[2].simpleHenreiRt__c);
            System.assertEquals(record3.keikaNensu,dlm[2].keikaNensu__c);
            System.assertEquals(record3.sonkinP,dlm[2].sonkinP__c);
            System.assertEquals(record3.shisanKeijoGaku,dlm[2].shisanKeijoGaku__c);
            System.assertEquals(record3.jisshitsuHenreiRt,dlm[2].jisshitsuHenreiRt__c);
            System.assertEquals(record3.pRuikei,dlm[2].pRuikei__c);
            System.assertEquals(record3.jisshitsuFutanGaku,dlm[2].jisshitsuFutanGaku__c);
            System.assertEquals(record3.S,dlm[2].S__c);
            System.assertEquals(record3.koukaGakuRuikei,dlm[2].koukaGakuRuikei__c);
            System.assertEquals(record3.cv,dlm[2].cv__c);
            System.assertEquals(record3.koukaGaku,dlm[2].koukaGaku__c);

        }
    }

    /*
    * 観点:insertPIPDownloadMeritDetail メソッド正常系
    * 条件:画面表示用DTOリストを3年分設定してinsertPIPDownloadMeritDetailを実行
    * 検証:PIPDownloadMeritDetailレコードの3年目に、適切に現在年フラグが」設定されていること
    */
    @isTest static void test_insertPIPDownloadMeritDetail_keikanensuTest(){
        Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
        //ayAccList = TestI_PIPTestUtil.createDataAccount();
        user = TestI_PIPTestUtil.createUser(PF_EMPLOYEE);
        ayAccList = TestI_PIPTestUtil.createDataAccount();
        TestI_PIPTestUtil.createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[0]);
        TestI_PIPTestUtil.updateAccOwner(ayAccList[1]);

        System.runAs(user){
            TestE_TestUtil.createEbizLog(true);
            Date occDate = Date.today().addYears(-2);
            String s_occDate = String.valueOf(occDate).remove('-');

            List<E_Policy__c> policyList;
            policyList = TestI_PIPTestUtil.createPolicys(s_occDate);
            E_policy__c pol = TestI_PIPTestUtil.getPolicy(policyList[0].Id);
            covList = TestI_PIPTestUtil.createCOV_CR(pol);

                        List<I_MeritTableDTO> meritTableDTOList = new List<I_MeritTableDTO>();

            I_MeritTableDTO record1 = new I_MeritTableDTO();
            record1.keikanensu = '1';
            meritTableDTOList.add(record1);


            I_MeritTableDTO record2 = new I_MeritTableDTO();
            record2.keikanensu = '2';
            meritTableDTOList.add(record2);

            I_MeritTableDTO record3 = new I_MeritTableDTO();
            record3.keikanensu = '3';
            meritTableDTOList.add(record3);


            I_MeritTableChartController con = new I_MeritTableChartController();
            con.mainCovpf = covList[0];

            con.meritTableDTOList = meritTableDTOList;
            con.policy = pol ;
            con.setElapsedYears();

            con.insertPIPDownloadMeritDetail();

            I_PIPDownloadMeritDetail__c pdm = [select keikanensu__c from I_PIPDownloadMeritDetail__c where isCurrent__c = true];
            System.assertEquals('3', pdm.keikanensu__c);

        }

    }

    /*
    * 観点:insertErrorLog メソッド正常系
    * 条件:メッセージおよび詳細メッセージがインプットとして渡されること
    * 検証:E_Logにアクセスログが保存されていること
    */
	@isTest static void test_insertErrorLog(){

		String message = 'エラーのメッセージ';
		String detailMessage = '詳細のエラー内容はこちらをご覧ください';
        String expectMessage = message + '\r\n詳細：' + detailMessage;

        E_Policy__c pol =  new E_Policy__c();
        insert pol;

		I_MeritTableChartController con = new I_MeritTableChartController();
        con.policy = pol;
		con.insertErrorLog(message,detailMessage);

		E_Log__c log = [SELECT Id,AccessPage__c,Detail__c,RecordId__c,IsError__c from E_Log__c Limit 1 ];
        System.assertEquals('保険契約概要', log.AccessPage__c);
        System.assertEquals(expectMessage, log.Detail__c);
        System.assertEquals(pol.Id, log.RecordId__c);
        System.assertEquals(true, log.IsError__c);

	}

    /*
    * 観点:retunColi メソッド正常系
    * 条件:policyを作成しておくこと
    * 検証:正常にIRIS_Coliページに遷移していること
    */
    @isTest static void test_retunColi(){
        E_Policy__c pol =  new E_Policy__c();
        insert pol;

		I_MeritTableChartController con = new I_MeritTableChartController();
        con.policy = pol;

        PageReference page = Page.IRIS_Coli;
        page.getParameters().put('id', pol.Id );
        page.setRedirect(true);
        System.assertEquals(page.getUrl(), con.returnColi().getUrl());

    }

}