public with sharing class I_MeritTableChartController extends I_AbstractPIPController{
	private static final String PAGE_TITLE = '保険契約概要';

	public Boolean isDisplayChart {get; set;}

	// 個人契約ヘッダ
	@Testvisible  public E_Policy__c policy {get; private set;}
	// 主契約（個人保険特約）
	public E_COVPF__c mainCovpf {get; set;}
	// 個人保険特約リスト
	public List<E_COVPF__c> covpfs {get; set;}

	public Boolean errorFlg{get;private set;}

	public List<I_MeritTableDTO> meritTableDTOList {get; set;}

	public Id downloadId {get; private set;}

	//実効税率
	public Double taxRate {get; set;}

	public String elapsedYears{get; private set;}

	public String modalMessage{get; private set;}

	//選択リスト値格納場所 養老保険のリリースまでコメントアウト
	//public String accountingTreatment {get;set;}

	//選択リスト(主契約の経理処理)
	/*
	public List<SelectOption> accountingTreatments{
		get{
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('half','1/2損金算入'));
			options.add(new SelectOption('all','全額資産計上'));
			return options;
		}
	}
	*/


	@Testvisible protected override PageReference init(){
		// LogName
		pageAccessLog.Name = '推移表';

		// アクセス判定
		kind = E_Const.ID_KIND.POLICY;

		PageReference pr = super.init();

		//選択リスト値初期化
		//accountingTreatment = 'half';

		try{
			if(pr != null){
				// PIPからの遷移判定のパラメータ付与
				pr.getParameters().put(I_Const.ERR_URLPARAM_KEY, I_Const.ERR_URLPARAM_PIP);
				return pr;

			}else{
				// 保険契約ヘッダ取得
				String policyId =  ApexPages.currentPage().getParameters().get('policyId');
				this.policy = E_PolicyDao.getRecByIdForIRISColi(policyId);

				// 個人保険特約取得
				this.covpfs = E_COVPFDao.getRecsByPolicyIdAndRtdevname(policy.Id, E_Const.COVPF_RECORDTYPE_ECOVPF);

				// 主契約セット
				setMainCovpf(this.covpfs);

				//現在年のセット
				setElapsedYears();

				//実効税率のデフォルト値のセット
				taxRate = I_PIPConst.EFFECTIVE_TAX_RATE;

			}

			this.errorFlg = false;

			return null;
		}catch(exception ex){
			pr = Page.E_ErrorPage;
			pr.getParameters().put(I_Const.ERR_URLPARAM_KEY, I_Const.ERR_URLPARAM_PIP);
			pr.getParameters().put('code', I_PIPConst.MESSAGE_KEY_MERITTABLE_UNIVERSAL_ERROR ); //推移表作成時の汎用エラー
			//pr.getParameters().put('code', I_PIPConst.MESSAGE_KEY_MERITTABLE_ATRIA_ERROR ); //推移表作成時の汎用エラー
			return pr;
		}
	}

	public I_MeritTableChartController(){

		super();
		pageMessages = getPageMessages();
		pgTitle = PAGE_TITLE ;

		this.mainCovpf = new E_COVPF__c();
		this.covpfs = new List<E_COVPF__c>();
		isDisplayChart = false;
	}


	public PageReference changeDisplay(){
		isDisplayChart = !isDisplayChart;
		return null;
	}

	/**
	 * 保障内容
	 */
	public List<I_COLIDatas.Cover> getCovers(){
		return I_ColiUtil.getCovers(this.covpfs, this.mainCovpf);
	}

	/**
	 * 主契約
	 */
	private void setMainCovpf(List<E_COVPF__c> covpfs){
		// 個人保険特約
		for(E_COVPF__c covpf : covpfs){
			// 保障内容　C：主契約、C以外：特約
			if(covpf.COLI_ZCRIND__c.equals(I_Const.COVPF_COLI_ZCRIND_C) ){
				this.mainCovpf = covpf;
				break;
			}
		}
	}

	/**
	 * 質権設定
	 */
	public List<I_COLIDatas.Pledge> getPledges(){
		List<I_COLIDatas.Pledge> pledges = new List<I_COLIDatas.Pledge>();
		for(E_CRLPF__c crlpf : E_CRLPFDao.getRecsByRoleNE(this.policy.Id)){
			pledges.add(I_ColiUtil.getPledge(crlpf));
		}
		return pledges;
	}

	public PageReference updateMeritTableData(){

		//表示対象の商品か確認
		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(this.policy,this.covpfs);
		if ( !checker.isAvailableProduct() ){
			errorFlg = true;
			modalMessage = getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_UNAVEIRABLE_PRODUCT);  //対象表示外の契約の場合のエラー
			return null;
		}
		if ( !checker.isAvailableContract() ){
			errorFlg = true;
			modalMessage = getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_UNAVEIRABLE_CONTRACT); //対象商品でない場合のエラー
			return null;
		}
		if ( !checker.isServiceTime() ){
			errorFlg = true;
			modalMessage = getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_OUT_OF_SERVICE_TIME); //サービス時間外の場合のエラー
			return null;
		}

		I_MeritTableBizLogic bizLogic = new I_MeritTableBizLogic();
		I_TableInputData input = new I_TableInputData();
		input.taxRate = E_Util.getRoundingDownString(taxRate,2);
		input.nnlinkid = access.user.E_ZWEBID__c;
		try{
			meritTableDTOList = bizLogic.createTableData( this.policy, this.covpfs, input);
		} catch( I_PIPException e ){
			errorFlg = true;
			modalMessage = getMSG().get(I_PIPConst.MESSAGE_KEY_MERITTABLE_ATRIA_ERROR); //推移表作成時の汎用エラー
			insertErrorLog( e.getmessage(), e.detailMessage );
		}

		return null;
	}

	@TestVisible private void setElapsedYears() {

		//今日の日付を取得する
		Date tdy = Date.today();
		String s_occDate = policy.COMM_OCCDATE__c;

		//今日の日付から年だけ取得する
		Integer currentYear = tdy.year();

		//契約日の年だけ取得する
		Integer occYear = Integer.valueOf(s_occDate.substring(0, 4));

		//契約応答日の年と今年の差を、○年として計算する（年）
		Integer elapsedYearsInt = currentYear - occYear;
		Integer occMonth = Integer.valueOf(s_occDate.substring(4, 6));
		Integer currentMonth = tdy.month();

		//今日の日付が、今年の契約応答日より前の場合、○年から1を引いた数を返す
		if(currentMonth < occMonth) {
			elapsedYearsInt -= 1;
		}else if(currentMonth == occMonth){

			Integer occDay = Integer.valueOf(s_occDate.substring(6, 8));
			Integer currentDay = tdy.day();

			if(currentDay < occDay){
				elapsedYearsInt -= 1;
			}

		}
		elapsedYearsInt = elapsedYearsInt + 1; //経過年数は0年目は1年目を表示する
		elapsedYears = String.valueOf(elapsedYearsInt);
	}

	public void insertPIPDownloadMeritDetail(){

		//PIPダウンロード履歴オブジェクトに履歴を登録
		I_PIPDownloadHistory__c pdh = new I_PIPDownloadHistory__c();
		pdh.Syokenbango__c = policy.COMM_CHDRNUM__c;
		pdh.Keiyakusha__c = policy.ContractorName__c;
		pdh.Hihokensha__c = policy.InsuredName__c;
		pdh.Hokensyurui__c = mainCovpf.COMM_ZCOVRNAM__c;
		pdh.S__c = mainCovpf.COMM_SUMINS__c;
		pdh.Detakijunbi__c = getDataSyncDateHead();
		pdh.Jikkozeiritsu__c = taxrate;
		pdh.keiyakubi__c = E_Util.formatBirthday(policy.COMM_OCCDATE__c);

		insert pdh;
		downloadId = pdh.Id;

		//PIPダウンロードメリット明細に印刷データを登録
		List<I_PIPDownloadMeritDetail__c> dml = new List<I_PIPDownloadMeritDetail__c>();

		for(I_MeritTableDTO meritTableDTO : meritTableDTOList){
			I_PIPDownloadMeritDetail__c dm = new I_PIPDownloadMeritDetail__c();

			dm.keikaNensu__c = meritTableDTO.keikaNensu;
			dm.age__c = meritTableDTO.age;
			dm.simpleHenreiRt__c = meritTableDTO.simpleHenreiRt;
			dm.keikaNensu__c = meritTableDTO.keikaNensu;
			dm.sonkinP__c = meritTableDTO.sonkinP;
			dm.shisanKeijoGaku__c = meritTableDTO.shisanKeijoGaku;
			dm.jisshitsuHenreiRt__c = meritTableDTO.jisshitsuHenreiRt;
			dm.pRuikei__c = meritTableDTO.pRuikei;
			dm.jisshitsuFutanGaku__c = meritTableDTO.jisshitsuFutanGaku;
			dm.S__c = meritTableDTO.S;
			dm.koukaGakuRuikei__c = meritTableDTO.koukaGakuRuikei;
			dm.cv__c = meritTableDTO.cv;
			dm.koukaGaku__c = meritTableDTO.koukaGaku;

			//経過年数が現在と同じであれば、今ココフラグをTrueにする。
			if(elapsedYears.equals(meritTableDTO.keikaNensu)){
				dm.isCurrent__c = true;
			}else{
				dm.isCurrent__c = false;
			}

			dm.I_PIPDownloadHistory__c = pdh.id;

			dml.add(dm);
		}

		insert dml;

	}

	@TestVisible private void insertErrorLog(String message, String detailMessage){
		String logMessage = message + '\r\n詳細：' + detailMessage;
		logMessage = logMessage.abbreviate(30000);
		E_Log__c errorLog = E_LogUtil.getLogRec( PAGE_TITLE, logMessage, policy.Id, true );
		insert errorLog;
	}

	//戻るボタンを押した際の遷移
	public PageReference returnColi(){
		PageReference pr = Page.IRIS_Coli;
		pr.getParameters().put('id', policy.Id );
		return pr;
	}

	public class I_TableInputData{
		public String taxRate;
		public String nnlinkid;
	}
}