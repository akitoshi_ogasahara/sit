@isTest
private class TestI_PamphletController {

	static String docNum = 'TEST0001';
	static String docId = 'docID';

	//IRISMenu-docNumberから値を取得できる場合
	@isTest static void test_getIrisMenuId01() {
		I_PageMaster__c ipm = createData();
		User executeUser = TestI_TestUtil.createUser(true, 'testUser', 'ＭＲ');
		PageReference pageRef = Page.IRIS_Pamphlet;
		pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
		pageRef.getParameters().put(I_Const.URL_PARAM_DOC_NUM, docNum);
		Test.setCurrentPage(pageRef);

		Test.startTest();
			System.runAs(executeUser){
				I_PamphletController ipc = new I_PamphletController();
				String ass = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGE);
				System.assertEquals(ipm.Id, ass);
			}
			
		Test.stopTest();
	}

	// pid == null
	@isTest static void test_getIrisMenuId02() {
		I_PageMaster__c ipm = createData();
		User executeUser = TestI_TestUtil.createUser(true, 'testUser', 'ＭＲ');
		PageReference pageRef = Page.IRIS_Pamphlet;
		Test.setCurrentPage(pageRef);

		Test.startTest();
			System.runAs(executeUser){
				I_PamphletController ipc = new I_PamphletController();
				Map<String, String> params = ApexPages.currentPage().getParameters();
				Boolean hasPid = params.containsKey(I_Const.URL_PARAM_PAGE);
				System.assert(!hasPid);
			}
		Test.stopTest();
	}

	@isTest static void test_getIrisMenuId03() {
		I_PageMaster__c ipm = createData();
		User executeUser = TestI_TestUtil.createUser(true, 'testUser', 'ＭＲ');
		E_IDCPF__c idcpf = TestI_TestUtil.createIDCPF(true, executeUser.Id, 'EP');
		PageReference pageRef = Page.IRIS_Pamphlet;
		Test.setCurrentPage(pageRef);

		Test.startTest();
			System.runAs(executeUser){
				I_PamphletController ipc = new I_PamphletController();
				Map<String, String> params = ApexPages.currentPage().getParameters();
				Boolean hasPid = params.containsKey(I_Const.URL_PARAM_PAGE);
				System.assert(hasPid);
			}
		Test.stopTest();
	}


	//コンストラクタ用データ（I_CMSController用）作成
	static I_PageMaster__c createData(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		createCMSContents();

		return ipm;
	}

	static I_MenuMaster__c createIRISMenuMaster(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = '定期保険';
		// 資料・動画
		mm.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY;
		// パンフレット等
		mm.SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_PAMPHLET;
		mm.MenuKey__c = 'menukey';
		insert mm;
		return mm;
	}
	
	//irisPage
	static I_PageMaster__c createIRISPage(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.Menu__c = mmId;
		insert pm;
		return pm;
	}


	static void createCMSContents(){

		List<I_ContentMaster__c> parents = new List<I_ContentMaster__c>();
		Integer i = 0;
		for(String nm : I_Const.PAMPHLET_CONTENTS){
			I_ContentMaster__c parent = new I_ContentMaster__c();
			parent.Name = nm;
			parent.DisplayOrder__c = String.valueOf(i);
			parents.add(parent);
			i++;
		}
		insert parents;
		//IRIS_CMSコンテンツ
		List<I_ContentMaster__c> children = new List<I_ContentMaster__c>();
		for(I_ContentMaster__c parent : parents){
			I_ContentMaster__c child = new I_ContentMaster__c();
			child.name = parent.Name;
			child.ParentContent__c = parent.Id;
			child.FormNo__c = docNum;
			child.DOM_Id__c = 'xxx999';
			child.PamphletCategory__c = '定期保険';
			children.add(child);
		}
		insert children;
	}

}