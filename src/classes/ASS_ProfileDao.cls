/**
 * 営業日報データアクセスオブジェクト
 * CreatedDate 2018/05/07
 * @Author Terrasky
 */

 public class ASS_ProfileDao {
 	/**
	 * プロファイル取得
	 * @param proId: プロファイルID
	 * @return Profile: プロファイル
	 */
	public static Profile getProfileById(Id proId) {
		return [SELECT
					 Name
				FROM 
					Profile
				WHERE 
					Id = :proId
				];
	}
 }