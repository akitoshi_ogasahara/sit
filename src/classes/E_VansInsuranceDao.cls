public with sharing class E_VansInsuranceDao {

	/**
	 * 代理店挙積情報IDをキーに、指定した乗合保険会社を取得する。
	 * @param asrId: 代理店挙積情報ID
	 * @return List<E_VansInsurance__c>: 乗合保険会社
	 */
	public static List<E_VansInsurance__c> getRecsByAsrId(Id asrId) {
		return [Select
					Id							// カスタムオブジェクト ID
					,Name						// 乗合保険会社No
					,BusinessDate__c			// 営業日
					,E_AgencySalesResults__c	// 代理店挙積情報
					,YYYYMM__c					// 年月
					,Hierarchy__c				// 階層
					,AGSR_ZLIFAPLB__c			// 代申・非代申コード
					,Proxy__c					// 代申・非代申
					,AGSR_KLIAJCOY__c			// 乗合保険会社コード
					,VansInsCompany__c			// 乗合保険会社
				From
					E_VansInsurance__c
				Where
					E_AgencySalesResults__c	= :asrId	// 代理店挙積情報
				Order By
					Sort__c						// 並び順
		];
	}

}