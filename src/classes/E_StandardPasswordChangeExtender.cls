global with sharing class E_StandardPasswordChangeExtender extends E_AbstractEditExtender{
	//private static final String PAGE_TITLE = '基本サービス用パスワード変更';
	private static final String INTERNET_SERVICE_POLICY_AGENCY = 'internet_service_policy_agency';
	private static final String INTERNET_SERVICE_POLICY_CONTRACTANT = 'internet_service_policy_contractant';
	private static final String INTERNET_SERVICE_POLICY_SUMISEI = 'internet_service_policy_sumisei';

	E_StandardPasswordChangeController extension;
	Pagereference pref = Page.E_Home;
	String contactId;

	//タイトル
	public String title{get;set;}

	//通常モードか？
	public Boolean isNormalMode{get;set;}
	//初回モードか？
	public Boolean isInitialMode{get;set;}
	//有効期限切れモードか？
	public Boolean isExpiredMode{get;set;}
	//代理店ユーザの初回ログインモードか？
	public Boolean isAgentMode{get; set;}
	//住生ユーザーか?
	public Boolean isSumiseiUser{get{return access.isSumiseiUser();} set;}

	//現在の基本サービス用パスワード
	public String nowPassword{get;set;}
	//新規基本サービス用パスワード
	public String newPassword{get;set;}
	//新規基本サービス用パスワード(確認)
	public String newConfirmPassword{get;set;}

	//電子メールアドレス
	public String newMail{get;set;}
	//電子メールアドレス（確認）
	public String newConfirmMail{get;set;}

	//冒頭のディスクレーマーを取得
	/*
	public String getHeadDisclaimer(){
		String ret = null;
		if(isExpiredMode){
			ret = getDISCLAIMER().get('LGN|201');
		} else {
			ret = getDISCLAIMER().get('LGN|202');
		}
		return ret;
	}
	*/

	//E_ChangePassword.componentのパラメータ「ページ種別」を取得
	public String getPageType(){
		String ret = '';
		if(isInitialMode){
			if(isAgentMode){
				//代理店ユーザからの初回ログイン
				ret = 'isAgentLogin';
			}else{
				//初回ログイン
				ret = 'initLogin';
			}
		} else if(isExpiredMode){
			//有効期限切れ
			ret = 'expiredLogin';
		} else {
			ret = '';
		}
		return ret;
	}

	//インターネット利用規約メニューマスターキーを取得
	public String getInternetServicePolicy(){
		String ret = INTERNET_SERVICE_POLICY_CONTRACTANT;
		//住生ユーザの場合、住生用
		if(isSumiseiUser){
			ret = INTERNET_SERVICE_POLICY_SUMISEI;
		//それ以外の代理店ユーザの場合	
		}else if(getIsAgent()){
			ret = INTERNET_SERVICE_POLICY_AGENCY;
		}
		return ret;
	}

	//コンストラクタ
	public E_StandardPasswordChangeExtender(E_StandardPasswordChangeController extension){
		super();
		this.extension = extension;
		//this.pgTitle = PAGE_TITLE;
		User userRec = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
		//if (!userRec.E_UseTempBasicPW__c && !Site.isPasswordExpired()) {
		//	pageRef = doAuth(E_Const.ID_KIND.USER, userinfo.getUserId());
		//}
		if (!userRec.E_UseTempBasicPW__c && !Site.isPasswordExpired() && !access.isSuggestPWChange()) {
			pageRef = doAuth(E_Const.ID_KIND.USER, userinfo.getUserId());
		}
		
		this.contactId = access.user.ContactId;

		isNormalMode = false;
		isInitialMode = false;
		isExpiredMode = false;
		isAgentMode = false;

		if(Site.isPasswordExpired()){
			//ログインユーザのパスワード有効期限が切れている場合、有効期限切れモード
			isExpiredMode = true;
			title = 'パスワード変更';
			if(access.isCustomer()){
				title = '基本サービス用パスワード変更';
			}
		} else if(userRec.E_UseTempBasicPW__c){ 
			//ログインユーザの仮基本PW使用がTRUEの場合、初回モード
			isInitialMode = true;
			//代理店の時
			if(access.isNormalAgent()){
				isAgentMode = true;
			}
			title = 'ログイン　新規の登録';
		} else {
			//上記以外の場合、通常モードで表示
			isNormalMode = true;
			title = 'パスワード変更';
			if(access.isCustomer()){
				title = '基本サービス用パスワード変更';
			}
		}
		//ページタイトル
		/*
		title = 'パスワード変更';
		if(isNormalMode){
			title = PAGE_TITLE;
		}
		*/
		System.debug('isInitialMode======' + isInitialMode);
		pgTitle = title;
	}

	//ページアクション
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}

	//キャンセル
	public override PageReference doReturn(){
		PageReference ret = null;
		if(isNormalMode){
			//通常モードの場合
			if(getIsCustomer()){
				pref = Page.E_ContactInfo;
				pref.getParameters().put('id', contactId);
			}
			ret = pref;
		} else {
			ret = new PageReference('/secur/logout.jsp');
		}
		return ret;
	}

	//実行
	public pagereference doSave(){
		pageMessages.clearMessages();
		try{
			if(isNormalMode){
				//入力チェック（通常モード）
				if(!validatePassword()){
					return null;
				}
			} else {
				//入力チェック（通常モード以外）
				if(!validateLoginPassword()){
					return null;
				}
			}
			
			User userRec = access.user;
			//メールアドレスを更新		住生ユーザ対象外
			if(isInitialMode&&!isAgentMode&&!isSumiseiUser){
				userRec.email = newMail;
				update userRec;
			}

			//初回ログインの場合、ログインパスワードを暗号化
			String nowLoginPassword = nowPassword;

			if(String.isNotBlank(nowPassword)){
				if(userRec.E_UseTempBasicPW__c){
					//仮基本PW使用の場合
					E_IDCPF__c idcpf = E_IDCPFDao.getRecsById(UserInfo.getUserId());
					if(idcpf != null){
						nowLoginPassword = E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(nowPassword, idcpf.ZPASSWD02DATE__c);
					}
				}else if(userRec.E_UseExistingBasicPW__c){
					//既存基本PW使用の場合
					nowLoginPassword = E_EncryptUtil.encrypt(nowPassword);
				}
			}

			//変更
			if(isNormalMode){
				//PageReference pr = site.changePassword(newPassword,newConfirmPassword,nowPassword);
				PageReference pr = site.changePassword(newPassword,newConfirmPassword,nowLoginPassword);
			} else {
				System.setPassword(UserInfo.getUserId(), newPassword);
			}
			if(ApexPages.hasMessages()){
				//ChangeMessasgeの失敗はApexPagesのメッセージに出力されている。
				//パスワード登録に失敗した場合は、処理を中断する。
				//テストメソッドはここを通ることはないので、通過していなくても良しとする
				ApexPages.Message errMsg = ApexPages.getMessages().get(0);
				for(string s:E_Const.SFDC_EXCEPTION_MSG_HACK.keyset()){
					if(errMsg.getDetail().contains(s)){
						addExceptionToPage(E_Const.SFDC_EXCEPTION_MSG_HACK.get(s));
						return null;
					}
				}
				//エラーメッセージをNNLinkコンポーネントに移します。
				addExceptionToPage(ApexPages.getMessages());
System.debug('ApexPages.getMessages()=============' +ApexPages.getMessages());
				return null;
			}

			//暗号化されたパスワードを変更した場合、フラグを降ろす
			if(userRec.E_UseTempBasicPW__c){
				userRec.E_UseTempBasicPW__c = false;
				update userRec;
			}else if(userRec.E_UseExistingBasicPW__c){
				userRec.E_UseExistingBasicPW__c = false;
				update userRec;
			}

			//メールオブジェクト作成
			insMailObj(UserInfo.getUserId());

			string key = 'ACC|';
			string type;
			if(getIsAgent()){
				type = '007';
			}else{
				type = '004';
			}

			return new pagereference('/apex/E_AcceptanceCompletion?type=' + key + type + '&conid=' + this.contactId );
		}catch(DMLException ex) {
			System.debug('ex=============' +ex);
			if (ex.getDmlStatusCode(0).equals(E_Const.INVALID_EMAIL_ADDRESS)) {
				pageMessages.addErrorMessage(getMSG().get('MAC|005'));
				return null;
			}
			pageMessages.addErrorMessage(ex.getMessage());
			return null;
		}catch(Exception e){
			System.debug('e=============' +e);
			string errMsg = e.getMessage();

			for(string s:E_Const.SFDC_EXCEPTION_MSG_HACK.keyset()){
				if(errMsg.contains(s)){
					addExceptionToPage(E_Const.SFDC_EXCEPTION_MSG_HACK.get(s));
					return null;
				}
			}

			addExceptionToPage(e.getMessage());
			return null;
		}
	}

	//入力チェック（通常モード）
	private boolean validatePassword(){
		boolean isOK = false;
		if(String.isblank(nowPassword)){
			addExceptionToPage(getMSG().get('SPW|004'));
		}
		if(String.isblank(newPassword)){
			addExceptionToPage(getMSG().get('SPW|005'));
			//addExceptionToPage('新パスワードがnull');
		}
		if(String.isblank(newConfirmPassword)){
			addExceptionToPage(getMSG().get('SPW|006'));
			//addExceptionToPage('新パスワード(確認用)がnull');
		}




		if(!pageMessages.hasMessages()){
			// パスワードに使用できる文字(半角英小文字及び数字のみ)以外の入力がある
			if(!E_Util.isEnAlphaNum(newPassword)){
				addExceptionToPage(getMSG().get('SPW|009'));
				//入力規則に従ってパスワードを変更して下さい。''
			}

			if(!newPassword.equals(newConfirmPassword)){
				addExceptionToPage(getMSG().get('SPW|008'));
				//addExceptionToPage('確認パスワードが一致しない');
			}
			if(nowPassword.equals(newPassword)){
				addExceptionToPage(getMSG().get('SPW|007'));
				//addExceptionToPage('パスワードが変更されていません');
			}

		}
		System.debug('ApexPages.getMessages()()()()()()=============' +ApexPages.getMessages());
		if(!pageMessages.hasMessages()) isOk = true;
		return isOK;

	}

	//入力チェック（通常モード以外）
	private boolean validateLoginPassword(){
		boolean isOK = false;
		if(String.isblank(newPassword)){
			addExceptionToPage(getMSG().get('SPW|101'));
			//addExceptionToPage('新規パスワードを入力して下さい。');
		}
		if(String.isblank(newConfirmPassword)){
			addExceptionToPage(getMSG().get('SPW|102'));
			//addExceptionToPage('新規パスワード（確認）を入力して下さい。');
		}
		//メールアドレスのチェックは初回モードのみ	住生ユーザはチェックなし
		if(isInitialMode && !isAgentMode && !isSumiseiUser){
			if(String.isblank(newMail)){
				addExceptionToPage(getMSG().get('SPW|103'));
				//addExceptionToPage('電子メールアドレスを入力して下さい。');
			}
			if(String.isblank(newConfirmMail)){
				addExceptionToPage(getMSG().get('SPW|104'));
				//addExceptionToPage('電子メールアドレス（確認）を入力して下さい。');
			}
		}
		if(!pageMessages.hasMessages()){
			// パスワードに使用できる文字(半角英小文字及び数字のみ)以外の入力がある
			if(!E_Util.isEnAlphaNum(newPassword)){
				addExceptionToPage(getMSG().get('SPW|009'));
				//addExceptionToPage('入力規則に従ってパスワードを変更して下さい。');
			}else if(!E_util.passwordPolicyCheck(newPassword)){ 
				//パスワードポリシー(アルファベットの大文字、小文字、数字の３種類すべてを組み合わせて、8文字以上のパスワードが設定されているか)チェック
				if(isAgentMode){
					addExceptionToPage(getMSG().get('SPW|009'));
				}
			}
			
			if(!newPassword.equals(newConfirmPassword)){
				addExceptionToPage(getMSG().get('SPW|105'));
				//addExceptionToPage('新規パスワード（確認）が正しくありません。');
			}
			//メールアドレスのチェックは初回モードのみ
			if(isInitialMode){
				if(!isAgentMode && !isSumiseiUser){
					if(newMail != newConfirmMail){//メールアドレスが違っていないかチェック
						pageMessages.addErrorMessage(getMSG().get('MAC|003'));
					}
				}
			}
			/*
			if(isInitialMode){
				User userRec = E_UserDao.getUserRecByUserId(UserInfo.getUserId());
				if(!userRec.Email.equals(newMail)){
					addExceptionToPage(getMSG().get('SPW|107'));
					//addExceptionToPage('電子メールアドレスが正しくありません。');
				}
				if(!newMail.equals(newConfirmMail)){
					addExceptionToPage(getMSG().get('SPW|106'));
					//addExceptionToPage('電子メールアドレス（確認）が正しくありません。');
				}
			}
			*/
		}
		if(!pageMessages.hasMessages()) isOk = true;
		return isOK;

	}

	//メールオブジェクト作成
	private void insMailObj(ID userID){
		//通常モードの場合のみ行う	住生ユーザも対象外
		if(!isNormalMode || !isSumiseiUser) return;
		String ReceptionDatetime = System.now().format('yyyy年MM月dd日 HH時mm分');
		user userContact = E_UserDao.getUserRecByUserId(userID);
		ID E_IDCPFID = E_IDCPFDao.getRecsById(userID).ID;

		E_ADRPF__c insE_ADRPFItem = new E_ADRPF__c(
			Type__c = E_Const.MAIL_TYPE_1P
			,Kbn__c = E_Const.KBN_1PPW
			,ReceptionDatetime__c = ReceptionDatetime
			,E_IDCPF__c = E_IDCPFID
			,ContactNameKana__c = userContact.contact.E_CLTPF_ZCLKNAME__c
			,ContactName__c = userContact.contact.name
			,EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c
			,CLNTNUM__c = userContact.contact.E_CLTPF_CLNTNUM__c
		);
		insert insE_ADRPFItem;
	}

	//エラーメッセージ追加
	private void addExceptionToPage(String str){
		//ApexPages.addMessages(new skyeditor2.ExtenderException(str));
		pageMessages.addErrorMessage(str);
	}
	private void addExceptionToPage(ApexPages.Message[] pgMsgs){
		pageMessages.addMessages(pgMsgs);
	}

	/*
	private Map<string,string> SFDC_EXCEPTION_MSG_HACK = new Map<String,String>{
		'Your password must have a mix of numbers and uppercase and lowercase letters'  => 'アルファベットの大文字、小文字、数字の3種類すべてを組み合わせて設定してください。'
		,'Your password must be at least 8 characters long' => '8文字以上のパスワードを設定してください。'

	};
	*/

}