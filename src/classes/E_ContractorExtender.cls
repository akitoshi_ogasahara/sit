global with sharing class E_ContractorExtender extends E_AbstractViewExtender {
    private static final String PAGE_TITLE = '契約者等情報';
   E_ContractorController extension;
    
	private static final String LA = E_CRLPFDao.ROLE_LA; //顧客ロールは主たる被保険者

	// 主たる被保険者の「加入時年齢」を取得する
	public String insuredPersonAddAge {get;set;}

    public E_ContractorExtender(E_ContractorController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }

    /* init */
    global override void init(){
        // アクセスチェック
    	pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
    	if (pageRef == null) {
            //Contact情報を追加取得
            E_ContactDaoWithout.fillContactsToPolicy(extension.record, true, true,false,false, false);             
    		initInsuredPerson();
    		
			for(E_ContractorController.koHihokenshaTabItem item : extension.koHihokenshaTab.Items) {
				if (item.record.ANBCCD__c != null) {
					item.record.ANBCCD__c = Math.abs(item.record.ANBCCD__c);
				}
            }
    	}
    }

    public PageReference pageAction () {
    	return E_Util.toErrorPage(pageRef, null);
    }


    /**
     * 保険顧客（ LA:(主たる)被保険者 ）を取得します。
     */
    private void initInsuredPerson(){
    	E_CRLPF__c rec = E_CRLPFDao.getRecByType(extension.record.id,LA);
        if(rec != null && rec.ANBCCD__c != null){
        	insuredPersonAddAge = Math.abs(rec.ANBCCD__c).format();
        }
    }

}