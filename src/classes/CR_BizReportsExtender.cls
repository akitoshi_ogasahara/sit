global with sharing class CR_BizReportsExtender extends CR_AbstractExtender{

	private CR_BizReportsController controller; 

	// 出力基準年月
	public String outputRecordYm {get; set;}

	/**
	 * Constructor
	 */
	public CR_BizReportsExtender(CR_BizReportsController extension){
		super();
		this.controller = extension;
		this.helpMenukey = CR_Const.MENU_HELP_REPORTS;
	}
	
	/**
	 * init
	 */
	public override void init(){
		isSuccessOfInitValidate = false;
		
		// URLパラメータチェック
		if(this.controller.record == null 
				|| String.isBlank(this.controller.record.AgencyCode__c)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);
		// 権限チェック(大規模代理店=True、事業報告書権限セット)
		}else if(!(getHasReportsPermission() == true && this.controller.record.LargeAgency__c)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG04_PH2_REQUIRED_PERMISSION_REPORTS); 
		// エラーなし
		}else{
			isSuccessOfInitValidate = true;
		}
		
		// 単位の初期値設定
		this.controller.record.ReportType__c = getDefaultReportType();
		
		// 出力基準年月の初期値取得
		outputRecordYm = E_Util.getFormatDateStr(Date.today(), CR_Const.OUTPUT_RECORD_YM_FORMAT);
	}
	
	/**
	 * ダウンロード履歴一覧取得
	 */
	public List<E_DownloadHistorry__c> getHistoryList(){
		return E_DownloadHistoryDao.getRecsByAgencyAndFormNo(this.controller.record.Id, CR_Const.TYPE_BIZREPORTS, E_Util.getDayAgo(CR_Const.CR_ATTACHMENTS_EXPIRE_DAYS));
	}

	/**
	 * データ作成依頼
	 */
	public PageReference doRequestCreateData(){	
		system.debug('■[outputRecordYm]' + outputRecordYm);

		// Savepoint
		Savepoint sp = Database.setSavepoint();

		try{
	
			// 検索条件(JSON)作成
			String dlCondition = getDownloadCondition();
			
			// ダウンロード履歴作成
			E_DownloadHistorry__c dh = E_DownloadHistoryDao.createDlHistory(
						this.controller.record.Id, CR_Const.TYPE_BIZREPORTS, outputRecordYm, dlCondition);
			
			// エラーチェック
			if(validate(dlCondition, dh) == false){
				return null;
			}

			// ダウンロード履歴登録
			insert dh;
			
			// ダウンロード履歴のShare登録(L1+代理店コード 公開グループ) 
			List<E_DownloadHistorry__Share> dhShareList = E_ShareUtil.getDownloadHistoryShareL1(dh.Id, new Set<String>{this.controller.record.AgencyCode__c});
			insert dhShareList;
	
			// 代理店提出更新(最新依頼日時)
			CR_Agency__c agency = CR_AgencyDao.getAgencyLastRequestById(this.controller.record.Id);
			agency.LastRequestReport__c = Datetime.now();
			update agency;
	
			PageReference pr = Page.E_CRBizReports;
			pr.getParameters().put('id', controller.record.Id);
			return pr;
				
		}catch(exception e){
			// rollback
			Database.rollback(sp);
			pageMessages.addErrorMessage(e.getMessage());
			return null;
		}
	}
	
	/**
	 * 単位の初期値設定
	 */
	private String getDefaultReportType(){
		String str = '';
		DescribeFieldResult f = SObjectType.CR_Agency__c.fields.ReportType__c;
		List<PicklistEntry> entries = f.getPicklistValues();
		for (PicklistEntry e : entries) {
			if (e.isActive()) {
				str = e.getValue();
				break;
			}
		}
		return str;
	}
	
	/**
	 * エラーチェック
	 */
	private Boolean validate(String dlCondition, E_DownloadHistorry__c dh){   	
		// 出力基準年月チェック
		if(CR_Validate.hasErrOutputRecordYm(outputRecordYm)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG01_PH2_FRAUD_OUTPUTYM);
			return false;
		}
		
		// 出力基準年月期間チェック
		if(CR_Validate.hasErrOutputRecordYmFrom(outputRecordYm, CR_Const.OUTPUT_RECORD_YM_REPORTS_FROM, CR_Const.OUTPUT_RECORD_YM_REPORTS_TO)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG07_PH2_FROM_OUTPUTYM);
			return false;
		}
		/*
		if(CR_Validate.hasErrOutputRecordYmFrom(outputRecordYm, CR_Const.OUTPUT_RECORD_YM_MAP_REPORTS)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG07_PH2_FROM_OUTPUTYM);
			return false;
		}
		*/
		
		/* 2016/05/06 matsuzaki ETL実行予定日が同じデータを取得するように変更 */
		// 数式項目値算出
		dh.recalculateFormulas();

		// ステータスが「処理待ち」、かつETL実行予定日が同じダウンロード履歴取得
		List<E_DownloadHistorry__c> downloadHistroyList = E_DownloadHistoryDaoWithout.getRecsByAgencyAndFormNoAndStatusAndEtlPlanDate(
					this.controller.record.Id,	 // 代理店提出レコードID 
					CR_Const.TYPE_BIZREPORTS, 
					CR_Const.DL_STATUS_WAIT,
					dh.EtlPlanDate__c);
		/*
		// ステータスが「処理待ち」のダウンロード履歴取得
		List<E_DownloadHistorry__c> downloadHistroyList = E_DownloadHistoryDaoWithout.getRecsByAgencyAndFormNoAndStatus(
					this.controller.record.Id,	 // 代理店提出レコードID 
					CR_Const.TYPE_BIZREPORTS, 
					CR_Const.DL_STATUS_WAIT,
					CR_Validate.getRequestFromDatetime());
		*/
		
		// リクエスト同条件チェック
		if(CR_Validate.hasErrSameCondition(outputRecordYm, dlCondition, downloadHistroyList)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG02_PH2_SAME_CONDITION);
			return false;
		}
		
		// リクエスト回数チェック
		if(CR_Validate.hasErrRequestCount(downloadHistroyList)){
			pageMessages.addErrorMessage(CR_Const.ERR_MSG03_PH2_LIMIT_REQUEST);
			return false;
		}
		
		return true;
	}
	
	/**
	 * 検索条件(JSON)作成
	 */
	private String getDownloadCondition(){
		E_DownloadCondition.BizReportsDownloadCondi dlCondi = new E_DownloadCondition.BizReportsDownloadCondi();
		dlCondi.ZHEADAY = controller.record.AgencyCode__c;
		dlCondi.reportType = this.controller.record.ReportType__c.substring(0,1);
		return dlCondi.toJSON();
	}
}