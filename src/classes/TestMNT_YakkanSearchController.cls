@isTest
private with sharing class TestMNT_YakkanSearchController{
		private static testMethod void testPageMethods() {	
			MNT_YakkanSearchController page = new MNT_YakkanSearchController(new ApexPages.StandardController(new I_ContentMaster__c()));	
			page.getOperatorOptions_I_ContentMaster_c_FormNo_c();	
			page.getOperatorOptions_I_ContentMaster_c_AgencyCodeText_c();	
			page.getOperatorOptions_I_ContentMaster_c_FormNoWoHyphen_c();	
			page.getOperatorOptions_I_ContentMaster_c_GuideAgreeDiv_c();	
			page.getOperatorOptions_I_ContentMaster_c_InsType_c();	
			System.assert(true);
		}	
			
	private static testMethod void testResultList() {
		MNT_YakkanSearchController.ResultList ResultList = new MNT_YakkanSearchController.ResultList(new List<I_ContentMaster__c>(), new List<MNT_YakkanSearchController.ResultListItem>(), new List<I_ContentMaster__c>(), null);
		ResultList.create(new I_ContentMaster__c());
		ResultList.doDeleteSelectedItems();
		System.assert(true);
	}
	
	@isTest
	private static void testLightDataTables(){

		System.assert(true);
	}
}