/**
 * I_MovieSearchBoxDetailController
 *
 * I_MovieSearchResultの表示を制御するためのコントローラクラス
 */
public with sharing class I_MovieSearchBoxDetailController {

	/** 初期表示件数 */
	public static Integer DEFAULT_DISPLAY_ROW_COUNT = 5;

	/** 加算件数 */
	public static Integer ADDITIONAL_ROW_COUNT = 5;

	/** 制御する動画のリスト */
	private List<I_MovieSearchResult> rows;

	/** 現在表示可能となっている件数の値 */
	public Integer rowCount {get; set;}

	/**
	 * コンストラクタ
	 */
	public I_MovieSearchBoxDetailController() {
		this.rowCount = DEFAULT_DISPLAY_ROW_COUNT;
	}

	/**
	 * 最大表示数の数値を取得する
	 * @return Integer
	 */
	public Integer getListMaxRows() {
		return I_Const.LIST_MAX_ROWS;
	}

	/**
	 * rowsを取得するためのゲッターメソッド
	 * @return List<I_MovieSearchResult>
	 */
	public List<I_MovieSearchResult> getMovieRows() {
		return this.rows;
	}

	/**
	 * rowsを設定するためのセッターメソッド
	 * すでに保持しているリストと受け取ったリストを比較し、別インスタンスであった場合のみ
	 * 設定が行われる。
	 * 設定を行う際は各クラス変数の初期化を行う
	 * @param List<I_FileSearchResult> rows
	 */
	public void setMovieRows(List<I_MovieSearchResult> rows) {
		// 参照が変わっていた場合初期化を行う
		if (this.rows != rows) {
			this.rowCount = DEFAULT_DISPLAY_ROW_COUNT;
			this.rows = rows;
		}
	}

	/**
	 * 現在の表示可能件数を増加させる。
	 * 一定以上の値になった場合は、最大値を設定する。
	 */
	public PageReference addRows() {
		this.rowCount += ADDITIONAL_ROW_COUNT;
		if(this.rowCount > I_Const.LIST_MAX_ROWS){
			this.rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}
}