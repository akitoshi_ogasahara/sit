public with sharing class E_RepresentativeDao {

	/**
	 * 代理店挙積情報IDをキーに、指定した法人代表者/責任者を取得する。
	 * @param asrId: 代理店挙積情報ID
	 * @return List<E_Representative__c>: 法人代表者/責任者
	 */
	public static List<E_Representative__c> getRecsByAsrId(Id asrId) {
		return [Select
					Id							// カスタムオブジェクト ID
					,Name						// 法人代表者/責任者名
					,Category__c				// カテゴリー
					,CnvsLicense__c				// 募集資格
					,E_AgencySalesResults__c	// 代理店挙積情報
					,Hierarchy__c				// 階層
					,XHAT_KQUALFLG__c			// 募集資格コード
					,XHAT_AGTYPE__c				// 募集人区分
					,XHAT_ZAGMANGR__c			// 業務管理責任者コード
					,XHAT_ZAGEDUCT__c			// 教育責任者コード
					,BusinessDate__c			// 営業日
					,YYYYMM__c					// 年月
					,Agent__c					// 募集人コード
				From
					E_Representative__c
				Where
					E_AgencySalesResults__c	= :asrId	// 代理店挙積情報
				Order By
					Sort__c						// 並び順
		];
	}

}