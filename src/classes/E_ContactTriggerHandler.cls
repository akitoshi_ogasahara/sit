public with sharing class E_ContactTriggerHandler {

	public static Boolean isSkipTriggerActions = System.Label.E_IsSkipContactTrigger == '1';

	private E_ContactTriggerBizLogic bizLogic;
	
	//コンストラクタ
	public E_ContactTriggerHandler(){
		bizLogic = new E_ContactTriggerBizLogic();
	}

	//before insert
	public void onBeforeInsert(List<Contact> newList){
		//レコードタイプ設定
		bizLogic.updateRecordType(newList);
	}

	//after insert
	public void onAfterInsert(List<Contact> newList, Map<Id,Contact> newMap){
		//募集人レコードへ関連顧客レコード設定
		bizLogic.updateAgentToClientRelation(newList, null);
	}

	//before update
	public void onBeforeUpdate(List<Contact> newList, Map<Id,Contact> newMap, Map<Id,Contact> oldMap){
		//レコードタイプ設定
		bizLogic.updateRecordType(newList);
	}

	//after update
	public void onAfterUpdate(List<Contact> newList, Map<Id,Contact> newMap, Map<Id,Contact> oldMap){
		//募集人レコードへ関連顧客レコード設定
		bizLogic.updateAgentToClientRelation(newList, oldMap);

		//P16-0003AtriaR1.1対応
		bizLogic.commissionSetByContact(newList, oldMap);

		// 権限セットの再設定
		//bizLogic.permissionSetAssign(newList, oldMap);
	}
}