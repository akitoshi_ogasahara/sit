/**
 * 
 */
@isTest
private class TestE_SPSPFEmailTriggerHandler {

	static testMethod void myUnitTest01() {
		Test.startTest();
		E_SPSPFEmail__c sps1 = new E_SPSPFEmail__c();
		insert sps1;
		E_SPSPFEmail__c sps2 = new E_SPSPFEmail__c();
		insert sps2;
		E_SPSPFEmail__c sps3 = new E_SPSPFEmail__c();
		insert sps3;
		List<E_SPSPFEmail__c> spsList = new List<E_SPSPFEmail__c>{
			  new E_SPSPFEmail__c(Id = sps3.Id, MailNunmer__c = '')
			, new E_SPSPFEmail__c(Id = sps2.Id, MailNunmer__c = '')
			, new E_SPSPFEmail__c(Id = sps1.Id, MailNunmer__c = '')
		};
		update spsList;
		Test.stopTest();
		
		E_SPSPFEmail__c result = [select MailNunmer_numOnly__c From E_SPSPFEmail__c where Id = :sps3.Id];
		System.assertEquals(3, result.MailNunmer_numOnly__c);
		
        system.debug([select Name, MailNunmer__c From E_SPSPFEmail__c Where Id IN :spsList]);
	}
	
	
	/**
    * ■保守backlog[NN_OPS-4 本番環境ファンドスイッチ障害]対応
    * 事象再現テストケース001
    * UTCで前月31日となる[ファンドスイッチング更新メール]レコードを1件作成し、
    * その後、2件目作成時にエラーとならないことを確認する。
    * UTCの閾値となる0:00:00
    */
	static testMethod void testJstFirstDayUtcLastDay001() {
		
		//UTCで前月31日と判定されるレコードの作成
		E_SPSPFEmail__c sps1 = new E_SPSPFEmail__c();
		insert sps1;
        //テスト実施日の当月1日の0時0分0秒(utc)を作成日に設定
		Date firstDate = Date.today().toStartOfMonth();
		Time time1 = time.newInstance(0,0,0,0);
		Datetime dt1 = DateTime.newInstance(firstDate,time1);
        test.setCreatedDate(sps1.Id, dt1);
		
		//2件目の作成
        E_SPSPFEmail__c sps2 = new E_SPSPFEmail__c();
        insert sps2;

        //2件目の[メール送信NO]の連番部が2であること
		E_SPSPFEmail__c result = [select MailNunmer_numOnly__c From E_SPSPFEmail__c where Id = :sps2.Id];
		System.assertEquals(2, result.MailNunmer_numOnly__c);
		
        system.debug([select Name, MailNunmer__c From E_SPSPFEmail__c Where Id = :sps2.Id]);
	    
	}

    /**
    * ■保守backlog[NN_OPS-4 本番環境ファンドスイッチ障害]対応
    * 事象再現テストケース002
    * UTCで前月31日となる[ファンドスイッチング更新メール]レコードを1件作成し、
    * その後、2件目作成時にエラーとならないことを確認する。
    * JST+9の閾値となる8:59:59
    */
	static testMethod void testJstFirstDayUtcLastDay002() {
		
        //UTCで前月31日と判定されるレコードの作成
        E_SPSPFEmail__c sps1 = new E_SPSPFEmail__c();
        insert sps1;
        //JSTの場合テスト実施日の当月1日の8時59分59秒を作成日に設定
        Date firstDate = Date.today().toStartOfMonth();
        Integer localHourMinusOne = (-(Datetime.now().hourGMT() - Datetime.now().hour()) - 1);
        Time time1 = time.newInstance(localHourMinusOne,59,59,99);
        Datetime dt1 = DateTime.newInstance(firstDate,time1);
        test.setCreatedDate(sps1.Id, dt1);
        
        system.debug([select id,createdDate,MailNunmer__c,MailNunmer_numOnly__c from E_SPSPFEmail__c where Id = :sps1.Id]);
		
		//2件目の作成
        E_SPSPFEmail__c sps2 = new E_SPSPFEmail__c();
        insert sps2;
        
        system.debug([select id,createdDate,MailNunmer__c ,MailNunmer_numOnly__c from E_SPSPFEmail__c where Id = :sps2.Id]);

        //2件目の[メール送信NO]の連番部が2であること
		E_SPSPFEmail__c result = [select MailNunmer_numOnly__c From E_SPSPFEmail__c where Id = :sps2.Id];
		System.assertEquals(2, result.MailNunmer_numOnly__c);
		
        system.debug([select Name, MailNunmer__c From E_SPSPFEmail__c Where Id = :sps2.Id]);
	    
	}

    /**
    * ■保守backlog[NN_OPS-4 本番環境ファンドスイッチ障害]対応
    * 事象対象外ケース
    * JSTでも当月1日、UTCでも当月1日となる[ファンドスイッチング更新メール]レコードを1件作成し、
    * その後、2件目作成時にエラーとならないことを確認する。
    */
	static testMethod void testJstFirstDay001() {
		
        //UTCで当月1日と判定されるレコードの作成
        E_SPSPFEmail__c sps1 = new E_SPSPFEmail__c();
        insert sps1;
        //JSTの場合テスト実施日の当月1日の9時0分0秒を作成日に設定。
        Date firstDate = Date.today().toStartOfMonth();
        Integer localHourMinusOne = (-(Datetime.now().hourGMT() - Datetime.now().hour()) - 1);
        Time time1 = time.newInstance(localHourMinusOne,0,0,0);
        Datetime dt1 = DateTime.newInstance(firstDate,time1);
        test.setCreatedDate(sps1.Id, dt1);
        
        system.debug([select id,createdDate,MailNunmer__c,MailNunmer_numOnly__c from E_SPSPFEmail__c where Id = :sps1.Id]);
		
		//2件目の作成
        E_SPSPFEmail__c sps2 = new E_SPSPFEmail__c();
        insert sps2;
        
        system.debug([select id,createdDate,MailNunmer__c ,MailNunmer_numOnly__c from E_SPSPFEmail__c where Id = :sps2.Id]);

        //2件目の[メール送信NO]の連番部が2であること
		E_SPSPFEmail__c result = [select MailNunmer_numOnly__c From E_SPSPFEmail__c where Id = :sps2.Id];
		System.assertEquals(2, result.MailNunmer_numOnly__c);
		
        system.debug([select Name, MailNunmer__c From E_SPSPFEmail__c Where Id = :sps2.Id]);
	    
	}
    /**
    * ■保守backlog[NN_OPS-4 本番環境ファンドスイッチ障害]対応
    * 事象対象外ケース
    * 複数件同時insert及びupdate時に例外が発生しないことを確認する
    */
	static testMethod void testJstFirstDayUtcLastDayListUpd001() {
		
        //200件の同時insert
        List<E_SPSPFEmail__c> spsList = new List<E_SPSPFEmail__c>();
        for(Integer i=0; i < 200; i++){
	        E_SPSPFEmail__c spsIns = new E_SPSPFEmail__c(ContactName__c = string.valueOf(i));
	        spsList.add(spsIns);
        }
        insert spsList;
        
        //メール送信NOがNullに更新された、クエリの実行対象単位となる年月が100か月分（約8.3年）の一括更新時にエラーとならない事。
        datetime baseDate = DateTime.newInstance(1990, 1, 1, 1, 1, 1);
        datetime testDate;
        E_SPSPFEmail__c spsUpd = null;
        for(Integer i=0; i < 100; i++){
            testDate = baseDate.addMonths(i);
            spsUpd = spsList[i];
            spsUpd.MailNunmer__c = '';
            test.setCreatedDate(spsUpd.Id, testDate);
        }
        
        test.startTest();
        update spsList;
        test.stopTest();
        
        //最後に更新したレコードのメール送信NO（数値のみ）が[1]であること。
		E_SPSPFEmail__c result = [select MailNunmer_numOnly__c From E_SPSPFEmail__c where Id = :spsUpd.Id];
		System.assertEquals(1, result.MailNunmer_numOnly__c);
	    
	}
}