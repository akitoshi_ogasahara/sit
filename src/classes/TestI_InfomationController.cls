@isTest
private class TestI_InfomationController {

	//データなし初期状態
	static testMethod void pageActionTest() {
		// ページ表示
		PageReference pr = Page.IRIS_Information;
		Test.setCurrentPage(pr);

		Test.startTest();

			I_InfomationController infocon = new I_InfomationController();
			infocon.pageAction();
			System.assertEquals(infocon.getlistRows(), 0);
			System.assertEquals(infocon.rowCount, 10);
			System.assertEquals(infocon.listMaxRows, I_Const.LIST_MAX_ROWS);

		Test.stopTest();

	}
	//表示数追加
	static testMethod void addRowTest() {
		// ページ表示
		PageReference pr = Page.IRIS_Information;
		Test.setCurrentPage(pr);

		Test.startTest();

			I_InfomationController infocon = new I_InfomationController();
			infocon.pageAction();
			infocon.addRows();
			System.assertEquals(infocon.rowCount, 10+I_Const.LIST_ADD_MORE_ADD_ROWS);

		Test.stopTest();

	}
	//表示数追加　限界
	static testMethod void addRowTest2() {
		// ページ表示
		PageReference pr = Page.IRIS_Information;
		Test.setCurrentPage(pr);

		Test.startTest();

			I_InfomationController infocon = new I_InfomationController();
			infocon.pageAction();
			infocon.rowCount=I_Const.LIST_MAX_ROWS;
			infocon.addRows();
			System.assertEquals(infocon.rowCount, I_Const.LIST_MAX_ROWS);

		Test.stopTest();

	}
	//IRISメニュー取得
	static testMethod void irisMenuTest() {

		I_MenuMaster__c iMenu = new I_MenuMaster__c(name=I_Const.IRIS_CONTENTS_NAME_INFO);
		insert iMenu;
		// ページ表示
		PageReference pr = Page.IRIS_Information;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.URL_PARAM_INFOID, iMenu.Id);

		Test.startTest();

			I_InfomationController infocon = new I_InfomationController();
			infocon.pageAction();
			System.assertEquals(infocon.getActiveRecordId(), iMenu.Id);

		Test.stopTest();

	}
	//IRISメニュー取得
	static testMethod void irisMenuTest2() {

		I_MenuMaster__c iMenu = new I_MenuMaster__c(name=I_Const.IRIS_CONTENTS_NAME_INFO);
		insert iMenu;
		//Irisページ
		I_PageMaster__c iPage = new I_PageMaster__c(name='IRISPAge',Menu__c=iMenu.id);
		insert iPage;
		//IRIS　CMSコンテンツ
		I_ContentMaster__c iCms = new I_ContentMaster__c(name=I_Const.IRIS_CONTENTS_NAME_INFO,Page__c=iPage.id,DateStr__c='20000101');
		insert iCms;

		// ページ表示
		PageReference pr = Page.IRIS_Information;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.URL_PARAM_INFOID, iMenu.Id);

		Test.startTest();

			I_InfomationController infocon = new I_InfomationController();
			infocon.pageAction();
			infocon.getlistRows();
			System.assertEquals(infocon.getActiveRecordId(), iMenu.Id);

		Test.stopTest();

	}
	//IRISメニュー取得
	static testMethod void irisMenuTest3() {

		I_MenuMaster__c iMenu = new I_MenuMaster__c(name=I_Const.IRIS_CONTENTS_NAME_INFO);
		insert iMenu;
		//Irisページ
		I_PageMaster__c iPage = new I_PageMaster__c(name='IRISPAge',Menu__c=iMenu.id);
		insert iPage;
		//IRIS　CMSコンテンツ
		String tubeId = '999';
		I_ContentMaster__c iCms = new I_ContentMaster__c(name=I_Const.IRIS_CONTENTS_NAME_INFO,Page__c=iPage.id,DateStr__c='20000101');
		insert iCms;
		I_ContentMaster__c iCmsC = new I_ContentMaster__c(name='動画リンク',DateStr__c='20000101',tubeIds__c=tubeId,ParentContent__c=iCms.Id,Content__c='テスト<br><a href="douga1">動画1リンク</a>');
		insert iCmsC;

		// ページ表示
		PageReference pr = Page.IRIS_Information;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.URL_PARAM_INFOID, iCmsC.Id);

		Test.startTest();

			I_InfomationController infocon = new I_InfomationController();
			infocon.pageAction();
			I_ContentMaster__c conM = infocon.CMSSections.get(0).cmsComps.get(0).record;
			I_TubeConnectHelper helper = new I_TubeConnectHelper();
			String tubeURL = helper.getTubeTopUrl() +'?'+ helper.getParams_MovieType() +'&mid='+tubeId;
			System.assert(conM.Content__c.contains(tubeURL));

		Test.stopTest();

	}

}