@isTest private class TestI_VanishPolicyController{

	//addRows
	@isTest static void addRowsTest() {
		Test.startTest();
		I_VanishPolicyController vcont = new I_VanishPolicyController();
		vcont.rowCount = 981;
		vcont.addRows();
		System.assertEquals(vcont.rowCount,1000);
		Test.stopTest();
	}

	//該当する契約がありませんエラー
	@isTest static void policyRecsEmptyTest() {
		createMessageData('LIS|002','該当する契約がありません');
		PageReference pageRef = Page.IRIS_VanishPolicy;
		pageRef.getHeaders().put('User-Agent', 'xn');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			Test.startTest();
			I_VanishPolicyController lpc = new I_VanishPolicyController();
			lpc.pageAction();
			System.debug('テストデバッグ' + lpc.pageMessages);
			System.assertEquals(lpc.pageMessages.getWarningMessages()[0].summary,'該当する契約がありません');
			Test.stopTest();
		}
	}

	//sortRows(消滅日)
	@isTest
		static void sortRowsVanishDayTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'vanishDay');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				System.assertEquals(vcont.policyList[0].vanishDay, getStatDate(true));
				Test.stopTest();
			}
		}

	//sortRows(消滅事由)
	@isTest
		static void sortRowsVanishReasonTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'vanishReason');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);
			
				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				System.assertEquals(vcont.policyList[0].vanishReason,'消滅');
				Test.stopTest();
			}
		}

	//sortRows(契約者)
	@isTest
		static void sortRowsContractorNameTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'contractorName');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				conNameList.sort();
				System.assertEquals(vcont.policyList[0].contractorName,'テスト契約者');
				Test.stopTest();
			}
		}

	//sortRows(保険種類)
	@isTest
		static void sortRowsPolicyTypeTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'policyType');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				conNameList.sort();
				//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類格納フィールドを変更
				System.assertEquals(vcont.policyList[0].policyType,'テスト保険1<br>テスト保険1<br>テスト保険1');
				Test.stopTest();
			}
		}

	//sortRows(証券番号)
	@isTest
		static void sortRowsPolicyNumTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'policyNum');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				conNameList.sort();
				System.assertEquals(vcont.policyList[0].policyNum,'test0001');
				Test.stopTest();
			}
		}

	//sortRows(契約日)
	@isTest
		static void sortRowsContractDayTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'contractDay');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);
			
				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				conNameList.sort();
				System.assertEquals(vcont.policyList[0].contractDay,'2014/01/01');
				Test.stopTest();
			}
		}

	//sortRows(MR名)
	@isTest
		static void sortRowsMrNameTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'mrName');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id,Name,LastName,FirstName from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				String runUsName = runus.LastName;
				if(runus.FirstName != null) runUsName += runus.FirstName;
				System.assertEquals(vcont.policyList[0].mrName,runUsName);
				Test.stopTest();
			}
		}

	//sortRows(代理店)
	/*@isTest
		static void sortRowsAgencyTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'agency');
			Test.setCurrentPage(pageRef);

			I_VanishPolicyController vcont = new I_VanishPolicyController();
			vcont.pageAction();
			vcont.sortRows();
			pacNameList.sort();
			System.assertEquals(vcont.policyList[0].agency,pacNameList[0]);
			Test.stopTest();
		}*/

	//sortRows(事務所)
	@isTest
		static void sortRowsOfficeTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'office');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				accNameList.sort();
				System.assertEquals(vcont.policyList[0].office,accNameList[0]);
				Test.stopTest();
			}
		}

	//sortRows(募集人)
	@isTest
		static void sortRowsAgentTest(){
			Test.startTest();
			clear();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'agent');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				conNameList.sort();
				System.assertEquals(vcont.policyList[0].agent,conNameList[0]);
				Test.stopTest();
			}
		}

	//sortRows(保険金額)
	/*@isTest
		static void sortRowsPolicyAmountTest(){
			Test.startTest();
			clear();
			User us = getUser();
			E_Policy__c po = createPolicy1(true,us);
			createCOV(po);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'policyAmount');
			Test.setCurrentPage(pageRef);

			I_VanishPolicyController vcont = new I_VanishPolicyController();
			vcont.pageAction();
			vcont.sortRows();
			vcont.sortRows();
			System.assertEquals(vcont.policyList[0].policyAmount,'100,000');
			Test.stopTest();
		}*/

	//sortRows(保険料)
	@isTest
		static void sortRowsPolicyFeeTest(){
			Test.startTest();
			clear();
			User us = getUser();
			E_Policy__c po = createPolicy1(true,us);
			createCOV(po);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'policyFee');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				vcont.sortRows();
				vcont.sortRows();
				System.assertEquals(vcont.policyList[0].policyFee,'100,000');
				Test.stopTest();
			}
		}

	//getIsTotalRowsのテスト
	@isTest 
		static void getIsTotalRowsTest(){
			Test.startTest();
			User us = getUser();
			createPolicy1(true,us);
			createPolicy2(false,us);
			createPolicy3(false,us);
			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];

			System.runAs(runus){
				createIDManage(runus);

				I_VanishPolicyController vcont = new I_VanishPolicyController();
				vcont.pageAction();
				String rows = vcont.getIsTotalRows();
				System.assertEquals(rows,'3');
				Test.stopTest();
			}
	}

	/*
	 *フィルターモード
	 */
	@isTest
		static void filterModeTest(){
			
			clear();

			Datetime d = system.now();
			//代理店格作成
			Account distribute = TestI_TestUtil.createAccount(true,null);
			//事務所作成
			Account office = TestI_TestUtil.createAccount(true,distribute);

			//募集人コードが5桁の募集人作成
			Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
			agentA.E_CL3PF_AGNTNUM__c = '12345';
			agentA.E_CL3PF_VALIDFLAG__c = '2';
			insert agentA;
			
			//ユーザを作成し無効化
			User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
			userA.IsActive = false;
			insert userA;

			//有効で5桁+|付きの募集人作成
			Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
			agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
			agentB.E_CL3PF_VALIDFLAG__c = '1';
			insert agentB;
			
			//ユーザを作成し有効化
			User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
			userB.IsActive = true;
			insert userB;

			//ID管理を付与
			E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

			//保険契約ヘッダ作成
			E_Policy__c pol = new E_Policy__c();
			pol.COMM_STATDATE__c = getStatDate(false);
			pol.COMM_ZRSTDESC__c = '消滅';
			pol.COMM_ZCLNAME__c = 'テスト契約者';
			pol.COMM_ZCOVRNAM__c = 'テスト保険1';
			pol.Insured__c = agentA.Id;
			pol.Contractor__c = agentA.Id;
			pol.COMM_CHDRNUM__c = 'test0001';
			pol.COMM_OCCDATE__c = '20140101';
			pol.MainAgentAccount__c = office.Id;
			pol.MainAgent__c = agentA.Id;
			pol.DCOLI_SUMINS__c = 5000;
			pol.COMM_SINSTAMT__c = 5000;
			RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
							  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_DCOLI];
			pol.RecordTypeId = rec.Id;
			insert pol;



			PageReference pageRef = Page.IRIS_VanishPolicy;
			pageRef.getParameters().put('st', 'vanishDay');
			pageRef.getHeaders().put('User-Agent', 'xn');
			Test.setCurrentPage(pageRef);
			User runus = [ select Id from User where Id = :UserInfo.getUserId() ];
			createIDManage(runus);

			System.runAs(runus){
				//=====================テスト開始=====================
				Test.startTest();
				I_VanishPolicyController vcont = new I_VanishPolicyController();
				E_IRISHandler iris = E_IRISHandler.getInstance();
				//募集人Bで参照
				iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
				vcont.pageAction();
				Integer totalRows = vcont.listRows;
				System.assertEquals(1,totalRows);
				//=====================テスト終了=====================
				Test.stopTest();
			}
		}


	//データ作成
	static Integer num = 0;
	static List<String> conNameList = new List<String>();
	static List<String> accNameList = new List<String>();
	static List<String> pacNameList = new List<String>();
	static void clear(){
		num = 0;
		conNameList = new List<String>();
		accNameList = new List<String>();
		pacNameList = new List<String>();
	}

	//ユーザ
	static User getUser(){
		User thisUser = [ select Id,ContactId from User where Id = :UserInfo.getUserId() ];
		return thisUser;
	}


	//保険契約ヘッダ作成
	//一人目
	static E_Policy__c createPolicy1(Boolean choice,User us){		
		Account acc = createAccount();
		Contact con = createContact(acc,choice);
		E_Policy__c pol = new E_Policy__c();
		pol.COMM_STATDATE__c = getStatDate(false);
		pol.COMM_ZRSTDESC__c = '消滅';
		pol.COMM_ZCLNAME__c = 'テスト契約者';
		pol.COMM_ZCOVRNAM__c = 'テスト保険1';
		pol.DCOLI_ZNAME40A01__c = 'テスト保険1';
		pol.DCOLI_ZNAME40A02__c = 'テスト保険1';
		pol.Insured__c = con.Id;
		pol.Contractor__c = con.Id;
		pol.COMM_CHDRNUM__c = 'test0001';
		pol.COMM_OCCDATE__c = '20140101';
		pol.MainAgentAccount__c = acc.Id;
		pol.MainAgent__c = con.Id;
		pol.OwnerId = us.Id;
		pol.DCOLI_SUMINS__c = 5000;
		pol.COMM_SINSTAMT__c = 5000;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
						  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_DCOLI];
		pol.RecordTypeId = rec.Id;
		insert pol;
		return pol;
	}

	//二人目
	static void createPolicy2(Boolean choice,User us){
		Account acc = createAccount();
		Contact con = createContact(acc,choice);
		E_Policy__c pol = new E_Policy__c();
		pol.COMM_STATDATE__c = getStatDate(false);
		pol.COMM_ZRSTDESC__c = '消滅';
		pol.COMM_ZCLNAME__c = 'テスト契約者2';
		pol.COMM_ZCOVRNAM__c = 'テスト保険2';
		pol.DCOLI_ZNAME40A01__c = 'テスト保険2';
		pol.DCOLI_ZNAME40A02__c = 'テスト保険2';
		pol.Insured__c = con.Id;
		pol.Contractor__c = con.Id;
		pol.COMM_CHDRNUM__c = 'test0002';
		pol.COMM_OCCDATE__c = '20150101';
		pol.SubAgentAccount__c =acc.Id;
		pol.SubAgent__c = con.Id;
		pol.OwnerId = us.Id;
		pol.COMM_SUMINS02__c = 100000;
		pol.COMM_ZTOTPREM__c = 100000;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
						  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_DSPVA];
		pol.RecordTypeId = rec.Id;
		insert pol;
	}

	//三人目
	static void createPolicy3(Boolean choice,User us){
		Account acc = createAccount();
		Contact con = createContact(acc,choice);
		E_Policy__c pol = new E_Policy__c();
		pol.COMM_STATDATE__c = getStatDate(false);
		pol.COMM_ZRSTDESC__c = '消滅';
		pol.COMM_ZCLNAME__c = 'テスト契約者3';
		pol.COMM_ZCOVRNAM__c = 'テスト保険3';
		pol.DCOLI_ZNAME40A01__c = 'テスト保険3';
		pol.DCOLI_ZNAME40A02__c = 'テスト保険3';
		pol.Insured__c = con.Id;
		pol.Contractor__c = con.Id;
		pol.COMM_CHDRNUM__c = 'test0003';
		pol.COMM_OCCDATE__c = '20160101';
		//pol.MainAgentAccount__c = acc.Id;
		pol.SubAgentAccount__c =acc.Id;
		//pol.MainAgent__c = con.Id;
		pol.SubAgent__c =con.Id;
		pol.OwnerId = us.Id;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
		 				  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_DCOLI];
		pol.RecordTypeId = rec.Id;
		insert pol;
	}
	
	//
	static String getStatDate(Boolean isFormat){
		String sDate = String.valueOf(Date.Today().addMonths(-3));
		if(isFormat){
			sDate = sDate.replace('-', '/');
		}else{
			sDate = sDate.replace('-', '');
		}
		return sDate;
	}

	//個人保険特約
	static void createCOV(E_Policy__c po){
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_SUMINS__c = 100000;
		cov.DCOLI_SINSTAMT__c = 100000;
		insert cov;
	}

/*
	//募集人（取引先責任者)
	static Contact createContact(Account acc,Boolean choice){
		Contact con = new Contact();
		con.FirstName = '花子';
		con.LastName = 'テストテスト';
		con.AccountId = acc.Id;
		insert con;
		if(choice) createTestUser(con,acc);
		return con = [select Name,Id,FirstName,LastName from Contact where FirstName ='花子' limit 1];
	}
*/
	//募集人（取引先責任者)
	static Contact createContact(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CL3PF_ZEATKNAM__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createTestUser(con,acc);
		return con;
	}

	//親取引先
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		pacNameList.add(acc.Name);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}

/*
	//取引先作成
	static Account createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト会社';
		acc.ZMRCODE__c = 'MR001';
		insert acc;
		return acc;
	}
*/

	//取引先
	static Account createAccount(){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		accNameList.add(acp.Name);
		acp.E_CL2PF_ZEAYKNAM__c = 'トリヒサキ' + String.valueOf(num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}

	//ユーザ
	static void createTestUser(Contact con,Account acc){
		User us = new User();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
	}

	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//ID管理作成
	static void createIDManage(User us){
		 E_IDCPF__c ic = new E_IDCPF__c();
		 ic.ZWEBID__c = '1234567890';
		 ic.ZIDOWNER__c = 'AG' + 'test01';
		 ic.User__c = us.Id;
		 ic.AppMode__c = 'IRIS';
		 ic.ZINQUIRR__c = 'BR00';
		 insert ic;
	}


}