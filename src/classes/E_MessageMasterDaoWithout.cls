/**
 *
 * E_MessageMasterDaoWithout
 * メッセージ関連DAO（メッセージ＆ファイル）のWithout版
 *          
 */
public without sharing class E_MessageMasterDaoWithout {


	
	/**
	 *      CMS用メッセージ取得
	 *          今日の日付値のレコードを取得する。
	 */
	public static List<E_MessageMaster__c> getMessagesByMenuId(String menuId){
		List<E_MessageMaster__c> ret = new List<E_MessageMaster__c>();
		for(E_MessageMaster__c rec :[SELECT Id,Name 
										, isHTML__c 
										, NameToSectionTitle__c 
										, value__c
										, DispFrom__c 
										, DispTo__c
										, HTML_Dom_Id__c
										, ShowAdobeReader__c
										, ShowPDFUsage__c 
										, ShowSubPageLink__c
										, ShowPicsByAttachment__c
										, ShowHTMLByAttachment__c 
										, ShowTopOfThePage__c
										, ShowBorderBottom__c
										, Menu__r.MenuMasterKey__c
										,(select id,name,recordType.DeveloperName,contentsName__c 
											from E_CMSFiles__r 
											order by sortOrder__c nulls last)       //添付ファイル管理用オブジェクト
										From E_MessageMaster__c
										Where Menu__c = :menuId
										Order By Key__c, DispFrom__c nulls first, DispTo__c nulls last]){
			if((rec.DispFrom__c == null || rec.DispFrom__c <= System.Now()) 
				&&
				(rec.DispTo__c ==null || rec.DispTo__c.addDays(1) > System.Now())){
				ret.add(rec);
			}
		}
		return ret;
	}

	/**
	 * メニューマスタレコードをKeyに取得
	 * @param menuKey: メニューKey
	 * @return E_MenuMaster__c
	 */
	public static E_MenuMaster__c getMenuMasterRecByKey(String menukey){
		if(String.isBlank(menuKey)) return null;
		for(E_MenuMaster__c menu: [SELECT id,name,MenuMasterKey__c,SelectedMenuKey__c,IsHideMenu__c, IsShowCloseBtn__c 
											,IsStandardUserOnly__c, IsPartnerUserOnly__c, UseCommonGW__c, UseInternet__c
									 FROM E_MenuMaster__c 
									WHERE MenuMasterKey__c = :menukey
									Order By Id]){
			return menu;
		}
		return null;
	}

}