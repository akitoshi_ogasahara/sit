public with sharing class I_MRController {

	// 支社名
	public String brNo {get; set;}
	// 挙績情報表示フラグ
	public Boolean isDispSR {get; set;}
	// MRリスト
	public List<MRListRow> MRRows {get{
		if(MRRows != null){
			return MRRows;
		}
		return readyAction();
		} set;}

	// リスト行数
	public Integer rowCount {get; set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}


	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE	= 'st';
	//ソートキー
	public static final String SORT_TYPE_MRNAME		= 'mrName';
	public static final String SORT_TYPE_MRCODE		= 'mrCode';
	public static final String SORT_TYPE_FS 	 	= 'fs';
	public static final String SORT_TYPE_FSYTD 		= 'fsYTD';
	public static final String SORT_TYPE_PRO		= 'pro';
	public static final String SORT_TYPE_PROYTD 	= 'proYTD';
	public static final String SORT_TYPE_COLI 		= 'coli';
	public static final String SORT_TYPE_COLIYTD 	= 'coliYTD';
	public static final String SORT_TYPE_TOTAL 		= 'total';
	public static final String SORT_TYPE_TOTALYTD 	= 'totalYTD';
	public static final String SORT_TYPE_AGENCY 	= 'agencyCount';
	public static final String SORT_TYPE_AGENT 		= 'agentCount';
	private final Set<String> HQ_PROFILE_NAMES= new Set<String>{'本社営業部MR','本社営業部拠点長'};

	/**
	 * 営業日の取得
	 */
	private String getBizDate (){
		return E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
	}
	//データ基準日
	public String bizDate{
		get{
			if(bizDate != null) return this.bizDate;
			String inquiryD = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
			String year = inquiryD.left(4);
			String month = inquiryD.mid(4, 2);
			String day = inquiryD.mid(6, 2);
			return year+'/'+month+'/'+day;
		} set;
	}


	//コンストラクタ
	public I_MRController() {

		//初期値 挙績情報表示フラグ:false
		isDispSR = false;

		// デフォルトソート　MR名・昇順
		sortType = SORT_TYPE_MRCODE;
		sortIsAsc = true;

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
		}
	}

	public List<MRListRow> readyAction(){
		getMRRecList();
		// ソート
		I_MRController.list_sortType  = sortType;
		I_MRController.list_sortIsAsc = sortIsAsc;
		MRRows.sort();
		return MRRows;
	}

	//対象データ取得
	public void getMRRecList(){
		//表示用リスト
		MRRows = new List<MRListRow>();
		List<User> users = new List<User>();
		String sWhere = '';
		if(brNo.equals('00')){
			//本社営業部MR、本社営業部拠点長ユーザー取得
			sWhere += ' AND Profile.Name IN (\'本社営業部MR\',\'本社営業部拠点長\')';
			//sWhere += ' AND Profile.Name IN : HQ_PROFILE_NAMES';
		}else{
			//営業部名を取得
			E_Unit__c unit = E_UnitDao.getUnitRecByBRCode(brNo);
			//ユーザ・挙績情報取得用リスト
			sWhere += ' AND Unit__c = \'' + unit.Name + '\'';
		}
		//users = E_UserDao.getUserRecsWithEmployeeSales(sWhere,getBizDate());
		//E_IDCPFを取得するため、without化
		users = E_UserDaoWithout.getUserRecsWithEmployeeSales(sWhere,getBizDate());
		for(User us : users){
			MRListRow eRow = new MRListRow();

			eRow.mrId 		= us.Id;
			eRow.mrName 	= us.Name;
			eRow.mrCode 	= us.MemberNo__c;
			eRow.eidcid 	= (us.E_IDCPFs__r == null || us.E_IDCPFs__r.isEmpty())? null : us.E_IDCPFs__r[0].id;
			//社内挙積情報
			//条件：isDispSR = true
			/* [Ph1対応]*/
			if(isDispSR == true){
System.debug(us.E_EmployeeSalesResults__r);
System.debug(us);
				if(us.E_EmployeeSalesResults__r.size() == 1){
					E_EmployeeSalesResults__c eRec = us.E_EmployeeSalesResults__r[0];

					eRow.fs 				= null2Zero(eRec.NBWANP_FSYTD__c);
					eRow.fsYTD 				= null2Zero(eRec.AchiveRate_FSYTD__c);
					eRow.pro 				= null2Zero(eRec.NBWANP_ProtectionYTD__c);
					eRow.proYTD 			= null2Zero(eRec.AchiveRate_ProtectionYTD__c);
					eRow.coli 				= null2Zero(eRec.NBWANP_BACOLIYTD__c);
					eRow.coliYTD 			= null2Zero(eRec.AchiveRate_BACOLIYTD__c);
					eRow.total 				= null2Zero(eRec.NBWANP_TotalYTD__c);
					eRow.totalYTD 			= null2Zero(eRec.AchiveRate_TotalYTD__c);
					eRow.agencyCount 		= eRec.ActiveAgency__c;
					eRow.agentCount 		= eRec.ActiveAgent__c;
				}
			}
			MRRows.add(eRow);
		}
	}
	private Decimal null2Zero(Decimal dec){
		return dec == null ? 0 : dec;
	}
	

	/**
	 * ソート
	 */
	public PageReference sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}
		//初回作成が非同期のため、間に合わない場合は作成し直す
		if(MRRows == null){
			getMRRecList();
		}
		I_MRController.list_sortType  = sortType;
		I_MRController.list_sortIsAsc = sortIsAsc;
		MRRows.sort();

		return null;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	//総行数取得
	public Integer getTotalRows(){
		Integer totalRow = 0;

		totalRow = MRRows  == null ? 0 : MRRows.size();
		return totalRow;
	}


	/**
	 * 内部クラス
	 */
	public class MRListRow implements Comparable {
		//MRユーザID
		public String mrId {get; set;}
		//ID管理ID
		public String eidcid {get; set;}
		//MR名
		public String mrName {get; set;}
		//社員番号
		public String mrCode {get; set;}
		//FS
		public Decimal fs {get; set;}
		//FS(YTD)
		public Decimal fsYTD {get; set;}
		//Protection
		public Decimal pro {get; set;}
		//Protection(YTD)
		public Decimal proYTD {get; set;}
		//BA－COLI
		public Decimal coli {get; set;}
		//BA－COLI(YTD)
		public Decimal coliYTD {get; set;}
		//Total
		public Decimal total {get; set;}
		//Total(YTD)
		public Decimal totalYTD {get; set;}
		//稼動事務所数
		public Decimal agencyCount {get; set;}
		//稼動募集人数
		public Decimal agentCount {get; set;}


		//コンストラクタ
		public MRListRow(){
			mrId 			= '';
			eidcid 			= '';
			mrName 			= '';
			mrCode 			= '';

			fs 					= 0;
			fsYTD 				= 0;
			pro 				= 0;
			proYTD 				= 0;
			coli 				= 0;
			coliYTD 			= 0;
			total 				= 0;
			totalYTD 			= 0;
			agencyCount 		= 0;
			agentCount 			= 0;
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			MRListRow compareToRow = (MRListRow)compareTo;
			Integer result = 0;

			//MR名
			if(list_sortType == SORT_TYPE_MRNAME){
				result = I_Util.compareToString(mrName, compareToRow.mrName);
			//社員番号
			}else if(list_sortType == SORT_TYPE_MRCODE){
				result = I_Util.compareToString(mrCode, compareToRow.mrCode);
			//FS
			}else if(list_sortType == SORT_TYPE_FS){
				result = I_Util.compareToDecimal(null2Minus(fs), null2Minus(compareToRow.fs));
			//FS(YTD)
			}else if(list_sortType == SORT_TYPE_FSYTD){
				result = I_Util.compareToDecimal(fsYTD, compareToRow.fsYTD);
			//Protection
			}else if(list_sortType == SORT_TYPE_PRO){
				result = I_Util.compareToDecimal(null2Minus(pro), null2Minus(compareToRow.pro));
			//Protection(YTD)
			}else if(list_sortType == SORT_TYPE_PROYTD){
				result = I_Util.compareToDecimal(proYTD, compareToRow.proYTD);
			//BA－COLI
			}else if(list_sortType == SORT_TYPE_COLI){
				result = I_Util.compareToDecimal(null2Minus(coli), null2Minus(compareToRow.coli));
			//BA－COLI(YTD)
			}else if(list_sortType == SORT_TYPE_COLIYTD){
				result = I_Util.compareToDecimal(coliYTD, compareToRow.coliYTD);
			//Total
			}else if(list_sortType == SORT_TYPE_TOTAL){
				result = I_Util.compareToDecimal(null2Minus(total), null2Minus(compareToRow.total));
			//Total(YTD)
			}else if(list_sortType == SORT_TYPE_TOTALYTD){
				result = I_Util.compareToDecimal(totalYTD, compareToRow.totalYTD);
			//稼動事務所数
			}else if(list_sortType == SORT_TYPE_AGENCY){
				result = I_Util.compareToDecimal(null2Minus(agencyCount), null2Minus(compareToRow.agencyCount));
			//稼動募集人数
			}else if(list_sortType == SORT_TYPE_AGENT){
				result = I_Util.compareToDecimal(null2Minus(agentCount), null2Minus(compareToRow.agentCount));
			}
			return list_sortIsAsc ? result : result * (-1);
		}
		private final Decimal MINUS = -1;
		private Decimal null2Minus(Decimal dec){
			return (dec == null)? MINUS : dec;
		}
	}
}