@isTest
private class TestSC_ChangeFixedController {

	// コンストラクタ
	private static testMethod void ChangeFixedTest001() {
		Test.startTest();
		SC_ChangeFixedController controller = new SC_ChangeFixedController();
		controller.canEditFixed = false;
		controller.canViewDefect = false;
		controller.canEditDefect = false;
		Test.stopTest();  

		System.assertEquals(controller.isEditFixed, false);
		System.assertEquals(controller.isEditDefect, false);
	}

	// doChangeDisplay:編集状態切替
	private static testMethod void ChangeFixedTest002(){
		Test.startTest();
		SC_ChangeFixedController controller = new SC_ChangeFixedController();
		controller.doChangeDisplay();
		Test.stopTest();  

		System.assertEquals(controller.isEditFixed, true);
	}

	// doChangeDisplayDefect:編集状態切替
	private static testMethod void ChangeFixedTest003(){
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		SC_ChangeFixedController controller = new SC_ChangeFixedController();
		controller.record = record;

		controller.doChangeDisplayDefect();
		Test.stopTest();  

		System.assertEquals(controller.isEditDefect, true);
	}

	// doSave:保存
	private static testMethod void ChangeFixedTest004(){
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		SC_ChangeFixedController controller = new SC_ChangeFixedController();
		record.Defect__c = SC_Const.DEFECT_STATUS_02;
		controller.record = record;

		controller.doSave();
		controller.getDefectStatus();
		Test.stopTest();  

		System.assertEquals(controller.isEditFixed, false);
		System.assertEquals(controller.isEditDefect, false);
	}
}