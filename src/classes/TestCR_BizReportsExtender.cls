/**
 * 
 */
@isTest
private class TestCR_BizReportsExtender {

	/**
	 * Constructor
	 */
    static testMethod void test_CR_BizReportsExtender01() {
    	CR_BizReportsController controller;
        CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
        
        // assert
        System.assertEquals(extender.helpMenukey, CR_Const.MENU_HELP_REPORTS);
    }
    
	/**
	 * init
	 * 社員ユーザ / 正常
	 */
	static testMethod void test_init01(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	// assert確認用
				DescribeFieldResult f = SObjectType.CR_Agency__c.fields.ReportType__c;
				List<PicklistEntry> entries = f.getPicklistValues();
				String defaultReportType = entries[0].getValue();
				
				Date d = Date.today();
				DateTime dt = DateTime.newInstance(d.year(), d.month(),d.day());

	        	// assert
	        	System.assertEquals(extender.isSuccessOfInitValidate, true);
	        	System.assertEquals(controller.record.ReportType__c, defaultReportType);
	        	System.assertEquals(extender.outputRecordYm, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT));
	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * init
	 * 社員ユーザ / エラー：レコードが存在しない
	 */
	static testMethod void test_init02(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				CR_Agency__c agny = new CR_Agency__c();
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	// assert
	        	System.assertEquals(extender.isSuccessOfInitValidate, false);
	        	
	        Test.stopTest();
        }
	}

	/**
	 * init
	 * 社員ユーザ / エラー：代理店コードがNULL
	 */
	static testMethod void test_init03(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店コードをNULLで更新
				acc.E_CL1PF_ZHEADAY__c = null;
				update acc;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	// assert
	        	System.assertEquals(extender.isSuccessOfInitValidate, false);
	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * init
	 * 社員ユーザ / エラー：権限なし
	 */
	static testMethod void test_init04(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	// assert
	        	System.assertEquals(extender.isSuccessOfInitValidate, false);
	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * List<E_DownloadHistorry__c> getHistoryList()
	 * 社員ユーザ / 正常
	 */
	static testMethod void test_getHistoryList01(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				update agny;
				
				// ダウンロード履歴の登録(事業x1、帳簿x2)
				List<E_DownloadHistorry__c> dhList = TestCR_TestUtil.createDlHistoryList(agny.Id);
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	List<E_DownloadHistorry__c> resultList = extender.getHistoryList();

	        	// assert
	        	System.assertEquals(1, resultList.size());
	        	
	        Test.stopTest();
        }
	}

	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / 正常
	 */
	static testMethod void test_doRequestCreateData01(){
		String reportType = '01:testType';
		
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.ReportType__c = reportType;
				agny.LastRequestReport__c = Datetime.now();
				update agny;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	PageReference resultPref = extender.doRequestCreateData();

	        	// assert
	        	System.assert(controller.record.LastRequestReport__c != agny.LastRequestReport__c);
	        	
	        	List<E_DownloadHistorry__c> resultDhList = [Select Id From E_DownloadHistorry__c Where CR_Agency__c = :agny.Id];
	        	System.assert(resultDhList.size() == 1);
	        	
	        	PageReference assertPref = Page.E_CRBizReports;
	        	assertPref.getParameters().put('id', agny.Id);
	        	System.assertEquals(resultPref.getUrl(), assertPref.getUrl());
	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：出力基準年月
	 */
	static testMethod void test_doRequestCreateData02(){
		String reportType = '01:testType';
		
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.ReportType__c = reportType;
				agny.LastRequestReport__c = Datetime.now();
				update agny;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	PageReference resultPref;
	        	
	        	// 出力基準年月に空白をセット
	        	extender.outputRecordYm = null;
				// 実行
	        	resultPref = extender.doRequestCreateData();
	        	// assert
	        	System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG01_PH2_FRAUD_OUTPUTYM);
	        	extender.pageMessages.clearMessages();
	        	
	        	// 出力基準年月に文字列をセット
	        	extender.outputRecordYm = 'テスト';
				// 実行
	        	resultPref = extender.doRequestCreateData();
	        	// assert
	        	System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG01_PH2_FRAUD_OUTPUTYM);
	        	extender.pageMessages.clearMessages();
	        	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：期間
	 */
	static testMethod void test_doRequestCreateData03(){
		String reportType = '01:testType';
		
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.ReportType__c = reportType;
				agny.LastRequestReport__c = Datetime.now();
				update agny;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	// 出力基準年月に期限開始前をセット
	        	extender.outputRecordYm = TestCR_TestUtil.getOutputYmLast(CR_Const.TYPE_BIZREPORTS);

				// 実行
	        	PageReference resultPref = extender.doRequestCreateData();
	        	
	        	// assert
	        	System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG07_PH2_FROM_OUTPUTYM);
	        	extender.pageMessages.clearMessages();
	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：同条件
	 */
	static testMethod void test_doRequestCreateData04(){
		String reportType = '01:testType';
		
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.ReportType__c = reportType;
				agny.LastRequestReport__c = Datetime.now();
				update agny;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	PageReference resultFirstPref = extender.doRequestCreateData();
	        	
	        	PageReference resultSecondPref = extender.doRequestCreateData();

	        	// assert
	        	System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG02_PH2_SAME_CONDITION);
	        	
	        Test.stopTest();
        }
	}
	
	/**
	 * public PageReference doRequestCreateData()
	 * 社員ユーザ / エラー：同条件
	 */
	static testMethod void test_doRequestCreateData05(){
		String reportType = '01:testType';
		
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createBizReportsPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	// データ作成
				Account acc = TestCR_TestUtil.createAccount(true);
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
				
				// 代理店提出の「大規模代理店」をTrueで更新
				agny.LargeAgency__c = true;
				agny.ReportType__c = reportType;
				agny.LastRequestReport__c = Datetime.now();
				update agny;
				
				// ダウンロード履歴登録
	        	Date d = Date.today();
	        	DateTime dt = DateTime.newInstance(d.year(), d.month(),d.day());
	        	List<E_DownloadHistorry__c> dhList = new List<E_DownloadHistorry__c>();
				dhList.add(TestCR_TestUtil.createDlHistory(agny.Id, CR_Const.TYPE_BIZREPORTS, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT), 'condition1'));
				dhList.add(TestCR_TestUtil.createDlHistory(agny.Id, CR_Const.TYPE_BIZREPORTS, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT), 'condition2'));
				dhList.add(TestCR_TestUtil.createDlHistory(agny.Id, CR_Const.TYPE_BIZREPORTS, dt.format(CR_Const.OUTPUT_RECORD_YM_FORMAT), 'condition3'));
				insert dhList;
				
	        	PageReference pref = Page.E_CRBizReports;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(agny);
				CR_BizReportsController controller = new CR_BizReportsController(standardcontroller);
	        	CR_BizReportsExtender extender = new CR_BizReportsExtender(controller);
	        	extender.init();
	        	
	        	PageReference resultPref = extender.doRequestCreateData();

	        	// assert
	        	System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, CR_Const.ERR_MSG03_PH2_LIMIT_REQUEST);
	        	
	        Test.stopTest();
        }
	}
}