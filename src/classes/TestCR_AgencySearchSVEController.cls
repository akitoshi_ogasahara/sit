@isTest
private with sharing class TestCR_AgencySearchSVEController{
		private static testMethod void testPageMethods() {	
			CR_AgencySearchSVEController page = new CR_AgencySearchSVEController(new ApexPages.StandardController(new CR_Agency__c()));	
			page.getOperatorOptions_CR_Agency_c_Status_c();	
			page.getOperatorOptions_CR_Agency_c_OutOfBusiness_c();	
			page.getOperatorOptions_Account_E_CL1PF_ZHEADAY_c();	
			page.getOperatorOptions_CR_Agency_c_AgencyNameKana_c();	
			page.getOperatorOptions_CR_Agency_c_MRUnit_c();	
			page.getOperatorOptions_CR_Agency_c_MRCode_c();	
			System.assert(true);
		}	
			
	private static testMethod void testresultTable() {
		CR_AgencySearchSVEController.resultTable resultTable = new CR_AgencySearchSVEController.resultTable(new List<CR_Agency__c>(), new List<CR_AgencySearchSVEController.resultTableItem>(), new List<CR_Agency__c>(), null);
		resultTable.create(new CR_Agency__c());
		resultTable.doDeleteSelectedItems();
		System.assert(true);
	}
	
}