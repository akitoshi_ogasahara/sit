/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class pop_selectOwnerControllerTest {

    static Boolean isTest = true;

    /* コンストラクタのテストケース１【タイトル：所有者選択】 */
    static testMethod void pop_selectOwnerControllerTestCase1() {
    	if (isTest) {
	        system.debug('---------------------------------------------- コンストラクタのテストケース１【タイトル：所有者選択】の開始 ----------------------------------------------');
	        PageReference pageRef = Page.pop_selectOwner;
	        pageRef.getParameters().put('type','search');
	        pageRef.getParameters().put('ownerName','MR');

	        Test.setCurrentPage(pageRef);

	        Test.startTest();
	        pop_selectOwnerController ctl = new pop_selectOwnerController();
	        Test.stopTest();

	        //system.assertEquals('所有者選択', ctl.title);
	        //system.assertEquals(true, ctl.isUser);
	        //system.assertEquals('%MR%', ctl.ownerName);
	        system.debug('---------------------------------------------- コンストラクタのテストケース１【タイトル：所有者選択】の終了 ----------------------------------------------');
    	}
    }

    /* コンストラクタのテストケース２【タイトル：更新所有者選択】 */
    static testMethod void pop_selectOwnerControllerTestCase2() {
    	if (isTest) {
	        system.debug('---------------------------------------------- コンストラクタのテストケース２【タイトル：更新所有者選択】の開始 ----------------------------------------------');
	        PageReference pageRef = Page.pop_selectOwner;
	        pageRef.getParameters().put('type','update');
	        pageRef.getParameters().put('ownerName','MR');

	        Test.setCurrentPage(pageRef);

	        Test.startTest();
	        pop_selectOwnerController ctl = new pop_selectOwnerController();
	        Test.stopTest();

	        //system.assertEquals('変更所有者選択', ctl.title);
	        //system.assertEquals(true, ctl.isUser);
	        //system.assertEquals('%MR%', ctl.ownerName);
	        system.debug('---------------------------------------------- コンストラクタのテストケース２【タイトル：更新所有者選択】の終了 ----------------------------------------------');
    	}
    }

    /* コンストラクタのテストケース３【所有者関連情報の0件検索】 */
    static testMethod void pop_selectOwnerControllerTestCase3() {
    	if (isTest) {
	        system.debug('---------------------------------------------- コンストラクタのテストケース３【所有者関連情報の0件検索】の開始 ----------------------------------------------');
	        PageReference pageRef = Page.pop_selectOwner;
	        pageRef.getParameters().put('ownerName','XXXXXXXXXXX');

	        Test.setCurrentPage(pageRef);

	        Test.startTest();
	        pop_selectOwnerController ctl = new pop_selectOwnerController();
	        Test.stopTest();

	        system.assertEquals(false, ctl.isUser);
	        system.debug('---------------------------------------------- コンストラクタのテストケース３【所有者関連情報の0件検索】の終了 ----------------------------------------------');
    	}
    }
}