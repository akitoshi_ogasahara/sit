@isTest
private class TestSC_SendCommentController {
	// コンストラクタ
	private static testMethod void sendCommentTest001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			SC_SendCommentController controller = new SC_SendCommentController();
		Test.stopTest();

		System.assertEquals(controller.addComment, '');
	}

	// doSave:正常
	private static testMethod void sendCommentTest002(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			SC_SendCommentController controller = new SC_SendCommentController();
			controller.record = record;

			controller.addComment = '追加コメント';
			controller.isSendMailCMD     = true;
			controller.isSendMailMR      = true;
			controller.isSendMailManager = true;
			controller.canEdit           = true;

			controller.doSave();			
		Test.stopTest();

		System.assertEquals(controller.addComment, '');
		System.assertEquals(controller.record.sendMailMR__c     , false);
		System.assertEquals(controller.record.sendMailManager__c, false);
		System.assertEquals(controller.record.sendMailCMD__c    , false);
	}

	// doSave:コメント未入力
	private static testMethod void sendCommentTest003(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			SC_SendCommentController controller = new SC_SendCommentController();
			controller.record = record;

			controller.doSave();			
		Test.stopTest();
	}
}