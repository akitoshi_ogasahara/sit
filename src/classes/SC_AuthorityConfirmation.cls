public with sharing class SC_AuthorityConfirmation {
	public class SCAuthorityConfirmationException extends Exception{}

	public final String TYPE_SUBMISSION_REMAND 		= '提出・差戻し';
	public final String TYPE_UPLOAD 				= 'アップロード';
	public final String TYPE_DELETE 				= '削除';
	public final String TYPE_DOWNLOAD				= 'ダウンロード';
	public final String TYPE_COMMENT				= 'コメント';
	public final String TYPE_QUESTIONS_ADDITIONAL 	= '質問追加';
	public final String TYPE_WEB_ANSWER				= 'Webフォーム回答';
	public final String TYPE_WEB_READ				= 'Webフォーム閲覧';
	public final String TYPE_WEB_SUBMISSION 		= 'Webフォーム提出';
	public final String TYPE_CHANGE_PDF				= 'Web→PDF';
	public final String TYPE_CHANGE_WEB				= 'PDF→Web';
	public final String TYPE_DOWNLOAD_PDF			= 'PDFダウンロード';

	private final String STATUS_NOT_SUBMITTED		= '未提出';
	private final String STATUS_MR					= '担当MR確認中';
	private final String STATUS_MGR					= '営業部長確認中';
	private final String STATUS_OFFICE				= '本社確認中';
	private final String STATUS_COMPLETION			= '完了';

	private final String USER_AG 					= '代理店';
	private final String USER_MR 					= 'MR（拠点）';
	private final String USER_OFFICE_SALES 			= '本社営業MR';
	private final String USER_MGR 					= '拠点長';
	private final String USER_OFFICE_SALES_MGR		= '本社営業部拠点長';
	private final String USER_CMD 					= 'CMD';
	private final String USER_DPA 					= 'DPA';
	private final String USER_READ_ONLY 			= '参照のみ（エリア部長）';

	private String userType;
	private SC_Office__c record;

	private final Map<String, Integer> statusIndexMap = new Map<String, Integer>{
																STATUS_NOT_SUBMITTED	=> 0,
																STATUS_MR 				=> 1,
																STATUS_MGR				=> 2,
																STATUS_OFFICE			=> 2,
																STATUS_COMPLETION 		=> 3
															};

	private final Map<String, List<Boolean>> submissionRemandMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{true, true, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, true, false, false},
																USER_MGR 				=> new List<Boolean>{true, true, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, true, false, false},
																USER_CMD 				=> new List<Boolean>{true, true, true, false},
																USER_DPA 				=> new List<Boolean>{true, true, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};

	private final Map<String, List<Boolean>> uploadMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{true, true, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, true, false, false},
																USER_MGR 				=> new List<Boolean>{true, true, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, true, false, false},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{true, true, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};

	private final Map<String, List<Boolean>> deleteMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{true, true, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, true, false, false},
																USER_MGR 				=> new List<Boolean>{true, true, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, true, false, false},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{true, true, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};

	private final Map<String, List<Boolean>> downloadMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, true},
																USER_MR 				=> new List<Boolean>{true, true, true, true},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, true, true, true},
																USER_MGR 				=> new List<Boolean>{true, true, true, true},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, true, true, true},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{true, true, true, true},
																USER_READ_ONLY 			=> new List<Boolean>{true, true, true, true}
															};

	private final Map<String, List<Boolean>> commentMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{false, false, false, false},
																USER_MR 				=> new List<Boolean>{false, true, true, true},
																USER_OFFICE_SALES 		=> new List<Boolean>{false, true, true, true},
																USER_MGR 				=> new List<Boolean>{false, true, true, true},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{false, true, true, true},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{false, true, true, true},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};
	private final Map<String, List<Boolean>> questionsAdditionalMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, true, true, true},
																USER_MR 				=> new List<Boolean>{true, true, true, true},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, true, true, true},
																USER_MGR 				=> new List<Boolean>{true, true, true, true},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, true, true, true},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{true, true, true, true},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};
	private final Map<String, List<Boolean>> webAnswerMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{false, false, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{false, false, false, false},
																USER_MGR 				=> new List<Boolean>{false, false, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{false, false, false, false},
																USER_CMD 				=> new List<Boolean>{false, false, false, false},
																USER_DPA 				=> new List<Boolean>{false, false, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};
	private final Map<String, List<Boolean>> webReadMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, true, true, true},
																USER_MR 				=> new List<Boolean>{true, true, true, true},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, true, true, true},
																USER_MGR 				=> new List<Boolean>{true, true, true, true},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, true, true, true},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{true, true, true, true},
																USER_READ_ONLY 			=> new List<Boolean>{true, true, true, true}
															};
	private final Map<String, List<Boolean>> webSubmissionMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{false, false, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{false, false, false, false},
																USER_MGR 				=> new List<Boolean>{false, false, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{false, false, false, false},
																USER_CMD 				=> new List<Boolean>{false, false, false, false},
																USER_DPA 				=> new List<Boolean>{false, false, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};
	private final Map<String, List<Boolean>> changePdfMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{true, false, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{true, false, false, false},
																USER_MGR 				=> new List<Boolean>{true, false, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{true, false, false, false},
																USER_CMD 				=> new List<Boolean>{true, true, true, true},
																USER_DPA 				=> new List<Boolean>{true, false, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};
	private final Map<String, List<Boolean>> changeWebMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{true, false, false, false},
																USER_MR 				=> new List<Boolean>{false, false, false, false},
																USER_OFFICE_SALES 		=> new List<Boolean>{false, false, false, false},
																USER_MGR 				=> new List<Boolean>{false, false, false, false},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{false, false, false, false},
																USER_CMD 				=> new List<Boolean>{false, false, false, false},
																USER_DPA 				=> new List<Boolean>{false, false, false, false},
																USER_READ_ONLY 			=> new List<Boolean>{false, false, false, false}
															};
	private final Map<String, List<Boolean>> dwonloadPdfMap = new Map<String, List<Boolean>>{
																USER_AG 				=> new List<Boolean>{false, true, true, true},
																USER_MR 				=> new List<Boolean>{false, true, true, true},
																USER_OFFICE_SALES 		=> new List<Boolean>{false, true, true, true},
																USER_MGR 				=> new List<Boolean>{false, true, true, true},
																USER_OFFICE_SALES_MGR 	=> new List<Boolean>{false, true, true, true},
																USER_CMD 				=> new List<Boolean>{false, true, true, true},
																USER_DPA 				=> new List<Boolean>{false, true, true, true},
																USER_READ_ONLY 			=> new List<Boolean>{false, true, true, true}
															};

	/*
	 * コンストラクタ
	 */
	public SC_AuthorityConfirmation(SC_Office__c record) {

		E_AccessController accCon = E_AccessController.getInstance();
		SC_StatusManager_Iris stManager = new SC_StatusManager_Iris(record, accCon);

		this.record = record;

		if (stManager.getIsMR()) userType = USER_MR;
		else if (stManager.getIsAG()) userType = USER_AG;
		else if (stManager.getIsMGR()) userType = USER_MGR;
		else if (stManager.getIsCMD()) userType = USER_CMD;
		else if (stManager.getIsPermissionReadOnly()) userType = USER_READ_ONLY;

		if (String.isEmpty(userType)) {
			throw new SCAuthorityConfirmationException('権限が取得できません。');
		}
	}

	public Boolean getAuthority(String typeName) {
		system.debug('**' + typeName);
		Map<String, List<Boolean>> authorityMap = getAuthorityMap(typeName);
		system.debug('**' + authorityMap.get(userType));
		system.debug('**' + record.Status__c);
		return authorityMap.get(userType)[statusIndexMap.get(record.Status__c)];
	}

	private Map<String, List<Boolean>> getAuthorityMap(String typeName) {
		Map<String, List<Boolean>> tmpMap = new Map<String, List<Boolean>>();

		if (typeName == TYPE_SUBMISSION_REMAND) tmpMap = submissionRemandMap;
		else if (typeName == TYPE_UPLOAD) tmpMap = uploadMap;
		else if (typeName == TYPE_DELETE) tmpMap = deleteMap;
		else if (typeName == TYPE_DOWNLOAD) tmpMap = downloadMap;
		else if (typeName == TYPE_COMMENT) tmpMap = commentMap;
		else if (typeName == TYPE_QUESTIONS_ADDITIONAL) tmpMap = questionsAdditionalMap;
		else if (typeName == TYPE_WEB_ANSWER) tmpMap = webAnswerMap;
		else if (typeName == TYPE_WEB_READ) tmpMap = webReadMap;
		else if (typeName == TYPE_WEB_SUBMISSION) tmpMap = webSubmissionMap;
		else if (typeName == TYPE_CHANGE_PDF) tmpMap = changePdfMap;
		else if (typeName == TYPE_CHANGE_WEB) tmpMap = changeWebMap;
		else if (typeName == TYPE_DOWNLOAD_PDF) tmpMap = dwonloadPdfMap;

		return tmpMap;
	}
}