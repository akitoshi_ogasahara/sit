public with sharing class E_GroupAgencyBizLogicBatchOperation extends E_AbstractCommonBatchOperation {
    
    //GroupMember登録リスト作成
    List<GroupMember> addGmList = new List<GroupMember>();
    //GroupMember削除リスト作成
    List<GroupMember> delGmList = new List<GroupMember>();
    
    /**
     * 更新対象データを作成します
     * @param
     * @return
     */
    public void createData() {
        //L1代理店格とBR母店支社の組み合わせMapを作成
        Map<String, String> afterGmMap = new Map<String, String>();
        System.debug('sObjects===========' + sObjects);
        for (SObject sObjectRec : sObjects) {
            Account acc = (Account)sObjectRec;
            if (acc.E_CL1PF_ZHEADAY__c != null && acc.E_CL1PF_ZBUSBR__c != null) {
                afterGmMap.put('L1' + acc.E_CL1PF_ZHEADAY__c, 'BR' + acc.E_CL1PF_ZBUSBR__c);
            }
        }
        System.debug('afterGmMap===========' + afterGmMap);
        if (afterGmMap.isEmpty()) {
            setLogger('no update.');
            return;
        }
        //公開グループ取得(BRxxxx)
        List<Group> groupIds = [Select Id From Group where type = 'Regular' and developerName like 'BR%'];
        
        //公開グループ取得(L1xxxx)
        Map<Id,Group> zheadayIdMap = new Map<Id,Group>([Select Id, Name, Type, DeveloperName, (Select Id, GroupId, UserOrGroupId From GroupMembers where UserOrGroupId in : groupIds) From Group where type = 'Regular' and developerName in : afterGmMap.keySet()]);
        
        //処理ログ
        String executelog = null;
        //GroupMember削除&登録処理
        if (!afterGmMap.isEmpty()) {
            System.debug('zheadayIdMap===========' + zheadayIdMap);
            for (Group zheadayGp : zheadayIdMap.values()) {
                //代理店格に母店支社存在フラグ
                boolean isBrgroup = false;
                //変更後の母店支社
                Group afterZbusbrGp = null;
                String afterZbusbrCd = afterGmMap.get(zheadayGp.DeveloperName);
                System.debug('afterZbusbrCd================='+afterZbusbrCd);
                if (afterZbusbrCd != null) {
                    afterZbusbrGp = zbusbrCdMap.get(afterZbusbrCd);
                }
                System.debug('zbusbrCdMap===========' + zbusbrCdMap);
                System.debug('zheadayGp.GroupMembers===========' + zheadayGp.GroupMembers);
                for (GroupMember zheadayGm : zheadayGp.GroupMembers) {
                    //変更前の母店支社
                    Group beferZbusbrGp = zbusbrIdMap.get(zheadayGm.userorgroupid);
                    if (beferZbusbrGp != null) {
                        //変更後の母店支社グループが存在しない場合
                        if (afterZbusbrGp == null) {
                            //変更前の母店支社グループを削除
                            delGmList.add(zheadayGm);
                            isBrgroup = true;
                            isBrgroup = true;
                            setLogger('A:'+ zheadayGp.name + '/noGroup');
                            break;
                        //母店支社と比較し異なる場合
                        } else if (!beferZbusbrGp.Id.equals(afterZbusbrGp.Id)) {
                            //変更前の母店支社グループを削除
                            delGmList.add(zheadayGm);
                            addGmList.add(new GroupMember(GroupId = zheadayGp.Id, UserOrGroupId = afterZbusbrGp.Id));
                            isBrgroup = true;
                            //setLogger(zheadayGp.name + '/' + afterZbusbrGp.Name);
                            break;
                        }
                    }
                }
                //代理店格に母店支社が存在しない場合
                if (!isBrgroup) {
                    if (afterZbusbrGp != null) {
                        addGmList.add(new GroupMember(GroupId = zheadayGp.Id, UserOrGroupId = afterZbusbrGp.Id));
                        //setLogger(zheadayGp.name + '/' + afterZbusbrGp.Name);
                    } else {
                        setLogger('B:'+ zheadayGp.name + '/noGroup');
                    }
                }
            }
        }
        System.debug(addGmList);
        System.debug(delGmList);
    }
    
    /**
     * データ登録処理です
     * @param 
     * @return
     */
    public void executeData() {
        //GroupMember削除（母店支社）
        if (!delGmList.isEmpty()) {
            delete delGmList;
        }
        //GroupMember登録（母店支社）
        if (!addGmList.isEmpty()) {
            insert addGmList;
        }
    }
}