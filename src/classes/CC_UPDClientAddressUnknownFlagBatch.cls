global class CC_UPDClientAddressUnknownFlagBatch implements Database.Batchable<sObject>, Database.Stateful{

	/**
	 * BatchApexの開始処理です
	 * @param ctx
	 * @return Database.QueryLocator
	 */
	global Database.Querylocator start(Database.BatchableContext BC){
		Date today = Date.today();

		String query = 'SELECT '
					+ 'Id, E_CLTPF_CLTTYPE__c, AccountId, CMN_isAddressUnknown__c , E_COMM_ZCLADDR__c, CC_isServiceCenterAdd__c '
					+ 'FROM Contact '
					+ ' WHERE '
					+ 'E_COMM_ZCLADDR__c != \'\' '
					+ 'AND CMN_isAddressUnknown__c = true '
					+ 'AND CC_isServiceCenterAdd__c = false '
					+ 'AND E_dataSyncDate__c >=: today';

		return Database.getQuerylocator(query);
	}

	/**
	 * BatchApexの処理実行します
	 * @param ctx
	 * @param scope
	 */
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('CC_UPDClientAddressUnknownFlagBatch >>>> execute >>>> scope: ' + scope);
		Set<ID> accountIdSet = new Set<Id>();
		List<Contact> contactList = (List<Contact>)scope;

		//Contactの住所不明フラグFalseにする
		for(Contact contact : contactList){
			contact.CMN_isAddressUnknown__c = false;
			if(contact.E_CLTPF_CLTTYPE__c.equals('C')){
				accountIdSet.add(contact.AccountId);
			}
		}

		Update scope;

		//法人顧客の場合、Accountの住所不明フラグもFalseにする
		if(accountIdSet.size() > 0){
			List<Account> accountList = [Select Id, CMN_isAddressUnknown__c
										FROM Account
										WHERE Id IN : accountIdSet
										AND CMN_isAddressUnknown__c = true];

			for(Account account : accountList){
				account.CMN_isAddressUnknown__c = false;
			}

			System.debug('CC_UPDClientAddressUnknownFlagBatch >>>> execute >>>> accountList:' + accountList);
			Update accountList;
		}

	}


	/**
	 * BatchApexの後処理
	 * @param ctx
	 * @return
	 */
	global void finish(Database.BatchableContext BC) {}

}