/**
 * 
 */
@isTest
private class TestCR_AgencySearchBizWrapController {

	/**
	 * pageAction
	 * 社員ユーザ / 事業報告書
	 */
    static testMethod void test_pageAction01() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizReportsPermissions(usr.Id);
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZREPORTS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
						
			Test.stopTest();
			
			// 実行結果確認
			System.assertEquals(null, target.pageAction());
			System.assertEquals(true, target.isSuccessOfInitValidate);
			System.assertEquals(true, target.isReports);
			System.assertEquals(false, target.isBooks);
			System.assertEquals('代理店検索(事業報告書)', target.getPageTitle());
			System.assertEquals(CR_Const.MENU_REPORTS, target.menuNo);
		}
    }
    
	/**
	 * pageAction
	 * 代理店ユーザ / 事業報告書
	 */
    static testMethod void test_pageAction02() {
    	Account acc = TestCR_TestUtil.createAccount(true);
    	Contact con = TestCR_TestUtil.createContact(true, acc.Id, 'TestUser002');
    	
    	// 代理店ユーザ作成
		User usr = TestCR_TestUtil.createAgentUser(true, 'TestUser002', 'E_PartnerCommunity', con.Id);
		TestCR_TestUtil.createContactShare(con.id, usr.id);
		
		System.runAs(usr){
			TestCR_TestUtil.createBizReportsPermissions(usr.Id);
			
			Test.startTest();

			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZREPORTS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
			
            PageReference pr = Page.E_CRBizReports;
            pr.getParameters().put('id', agny.Id);
            
            
            // 実行結果確認
			System.assertEquals(new PageReference(pr.getUrl()).getUrl(), target.pageAction().getUrl());
	
			Test.stopTest();
		}
    }
    
	/**
	 * pageAction
	 * 社員ユーザ / 帳簿書類
	 */
    static testMethod void test_pageAction03() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		TestCR_TestUtil.createBizBooksPermissions(usr.Id);
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZBOOKS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
						
			Test.stopTest();
			
			// 実行結果確認
			System.assertEquals(null, target.pageAction());
			System.assertEquals(true, target.isSuccessOfInitValidate);
			System.assertEquals(false, target.isReports);
			System.assertEquals(true, target.isBooks);
			System.assertEquals('代理店検索(帳簿書類)', target.getPageTitle());
			System.assertEquals(CR_Const.MENU_BOOKS, target.menuNo);
		}
    }
    
	/**
	 * pageAction
	 * 代理店ユーザ / 帳簿書類
	 */
    static testMethod void test_pageAction04() {
    	Account acc = TestCR_TestUtil.createAccount(true);
    	Contact con = TestCR_TestUtil.createContact(true, acc.Id, 'TestUser002');
    	
    	// 代理店ユーザ作成
		User usr = TestCR_TestUtil.createAgentUser(true, 'TestUser002', 'E_PartnerCommunity', con.Id);
		TestCR_TestUtil.createContactShare(con.id, usr.id);
		
		System.runAs(usr){
			TestCR_TestUtil.createBizBooksPermissions(usr.Id);
			
			Test.startTest();

			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZBOOKS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
			
            PageReference pr = Page.E_CRBizBooks;
            pr.getParameters().put('id', agny.Id);
            
            
            // 実行結果確認
			System.assertEquals(new PageReference(pr.getUrl()).getUrl(), target.pageAction().getUrl());
	
			Test.stopTest();
		}
    }
    
	/**
	 * pageAction
	 * 社員ユーザ / URLパラメータ不正
	 */
    static testMethod void test_pageAction05() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, null);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
						
			Test.stopTest();
			
			// 実行結果確認
			System.assertEquals(null, target.pageAction());
			System.assertEquals(false, target.isSuccessOfInitValidate);
			System.assertEquals(false, target.isReports);
			System.assertEquals(false, target.isBooks);
			System.assertEquals('エラー', target.getPageTitle());
			System.assert(String.isBlank(target.menuNo));
			System.assertEquals(CR_Const.ERR_MSG03_INVALID_URL, target.pageMessages.getErrorMessages()[0].summary);
		}
    }
    
	/**
	 * pageAction
	 * 社員ユーザ / 事業報告書権限なし
	 */
    static testMethod void test_pageAction06() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		
		System.runAs(usr){
			Test.startTest();
			
			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZREPORTS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
						
			Test.stopTest();
			
			// 実行結果確認
			System.assertEquals(null, target.pageAction());
			System.assertEquals(false, target.isSuccessOfInitValidate);
			System.assertEquals(true, target.isReports);
			System.assertEquals(false, target.isBooks);
			System.assertEquals('代理店検索(事業報告書)', target.getPageTitle());
			System.assertEquals(CR_Const.MENU_REPORTS, target.menuNo);
			System.assertEquals(CR_Const.ERR_MSG04_PH2_REQUIRED_PERMISSION_REPORTS, target.pageMessages.getErrorMessages()[0].summary);
		}
    }
    
	/**
	 * pageAction
	 * 代理店ユーザ / 帳簿書類権限なし
	 */
    static testMethod void test_pageAction07() {
    	Account acc = TestCR_TestUtil.createAccount(true);
    	Contact con = TestCR_TestUtil.createContact(true, acc.Id, 'TestUser002');
    	
    	// 代理店ユーザ作成
		User usr = TestCR_TestUtil.createAgentUser(true, 'TestUser002', 'E_PartnerCommunity', con.Id);
		TestCR_TestUtil.createContactShare(con.id, usr.id);
		
		System.runAs(usr){
			Test.startTest();

			PageReference pref = Page.E_CRAgencySearchBiz;
			pref.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZBOOKS);
			Test.setCurrentPage(pref);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_BIZ);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchBizWrapController target = new CR_AgencySearchBizWrapController();
			
            PageReference pr = Page.E_CRBizBooks;
            pr.getParameters().put('id', agny.Id);
            
            
            // 実行結果確認
			System.assertEquals(null, target.pageAction());
			System.assertEquals(CR_Const.ERR_MSG05_PH2_REQUIRED_PERMISSION_BOOKS, target.pageMessages.getErrorMessages()[0].summary);
	
			Test.stopTest();
		}
    }
}