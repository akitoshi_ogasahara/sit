@isTest
private class TestE_AnnuityExtender {

	/**
	 * 最低限のレコードで実施
	 */
    private static testMethod void testAnnuity1() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c();
        insert policy;

        //テストユーザで機能実行開始
        System.runAs(u){
        	
        	//テスト開始
			Test.startTest();
			PageReference pref = Page.E_Annuity;
        	pref.getParameters().put('Id',policy.Id);
        	

        	Test.setCurrentPage(pref);
        	
        	Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
        	E_AnnuityController controller = new E_AnnuityController(standardcontroller);
        	E_AnnuityExtender extender = controller.getExtender();
        	//extender.init();
        	//extender.PageAction();
        	extender.getIsAgentJudge();
        	extender.getIsEmployeeJudge();
        	extender.getIsFundJudge();
            extender.getFromType();
            
        	
        	//ページアクションの戻り値
        	resultPage = extender.PageAction();

        	//テスト終了
			Test.stopTest();
        }
         //※正常処理
        system.assertEquals(null, resultPage);
    }
    
    
    /**
	 * 最低限のレコードで実施
	 * ユーザのフラグを3つ立てている。
	 */
    private static testMethod void testAnnuity2() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
        	SPVA_ZSTUPDSP__c = true
        	,SPVA_ZDPTYDSP__c = true
        	,COMM_CRTABLE__c = 'ST'
        );
        insert policy;
        
        //保険顧客(受取人) 受取人種別は指定代理請求人
        E_CRLPF__c crlpf1 = new E_CRLPF__c(
        	E_Policy__c = policy.Id
        	,BNYTYP__c = '40'
        	,ZCLNAME__c ='yamada'
        	,CLRRROLE__c = 'BN'
        	,BNYPC__c = 100
        );
        insert crlpf1;
        
        //保険顧客(受取人) 受取人種別は後継年金受取人
        E_CRLPF__c crlpf2 = new E_CRLPF__c(
        	E_Policy__c = policy.Id
        	,BNYTYP__c = '11'
        	,CLRRROLE__c = 'BN'
        	,ZCLNAME__c ='tanaka'
        	,BNYPC__c = 100
        );
        insert crlpf2;
        
        
        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
        	//ひもづく保険契約ヘッダ
        	E_Policy__c = policy.Id
        	// 処理日01~06
        	,ZSTUPDAY01__c = '20001223'
        	,ZSTUPDAY02__c = '20001224'
        	,ZSTUPDAY03__c = '20011224'
        	,ZSTUPDAY04__c = '20021224'
        	,ZSTUPDAY05__c = '20031224'
        	,ZSTUPDAY06__c = '20031224'
        	// ステップアップ金額01~06
        	,ZSTUPSUM01__c = 10000
        	,ZSTUPSUM02__c = 12000
        	,ZSTUPSUM03__c = 13000
        	,ZSTUPSUM04__c = 14000
        	,ZSTUPSUM05__c = 16000
        	,ZSTUPSUM06__c = 17000
        	// ステップアップ金額率01~06
        	,ZSTUPRAT01__c = 100
        	,ZSTUPRAT02__c = 120
        	,ZSTUPRAT03__c = 130
        	,ZSTUPRAT04__c = 140
        	,ZSTUPRAT05__c = 160
        	,ZSTUPRAT06__c = 170
        	//商品毎ステップアップ金額率01~20
        	,ZATTMRAT01__c = 100
        	,ZATTMRAT02__c = 105
        	,ZATTMRAT03__c = 110
        	,ZATTMRAT04__c = 115
        	,ZATTMRAT05__c = 120
        	,ZATTMRAT06__c = 125
        	,ZATTMRAT07__c = 130
        	,ZATTMRAT08__c = 135
        	,ZATTMRAT09__c = 140
        	,ZATTMRAT10__c = 145
        	,ZATTMRAT11__c = 150
        	,ZATTMRAT12__c = 155
        	,ZATTMRAT13__c = 160
        	,ZATTMRAT14__c = 165
        	,ZATTMRAT15__c = 170
        	,ZATTMRAT16__c = 175
        	,ZATTMRAT17__c = 180
        	,ZATTMRAT18__c = 185
        	,ZATTMRAT19__c = 190
        	,ZATTMRAT20__c = 195
        	
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	
        	//テスト開始
			Test.startTest();
			PageReference pref = Page.E_Annuity;
        	pref.getParameters().put('Id',policy.Id);
        	

        	Test.setCurrentPage(pref);
        	Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
        	E_AnnuityController controller = new E_AnnuityController(standardcontroller);
        	E_AnnuityExtender extender = controller.getExtender();
        	//extender.init();
        	//extender.PageAction();
        	extender.getIsAgentJudge();
        	extender.getIsEmployeeJudge();
        	extender.getIsFundJudge();
        	
        	//ページアクションの戻り値
        	resultPage = extender.PageAction();

        	//テスト終了
			Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }
    /**
	 * 最低限のレコードで実施
	 * ユーザのフラグを3つ立てている。
	 */
    private static testMethod void testAnnuity3() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        idcpf.TRCDE01__c = 'TD06';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
        	SPVA_ZSTUPDSP__c = true
        	,SPVA_ZDPTYDSP__c = true
        	,COMM_CRTABLE__c = 'ST'
        );
        insert policy;
        
        //保険顧客(受取人) 受取人種別は指定代理請求人
        E_CRLPF__c crlpf1 = new E_CRLPF__c(
        	E_Policy__c = policy.Id
        	,BNYTYP__c = '40'
        	,ZCLNAME__c ='yamada'
        	,CLRRROLE__c = 'BN'
        	,BNYPC__c = 100
        );
        insert crlpf1;
        
        //保険顧客(受取人) 受取人種別は後継年金受取人
        E_CRLPF__c crlpf2 = new E_CRLPF__c(
        	E_Policy__c = policy.Id
        	,BNYTYP__c = '11'
        	,CLRRROLE__c = 'BN'
        	,ZCLNAME__c ='tanaka'
        	,BNYPC__c = 100
        );
        insert crlpf2;
        
        
        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
        	//ひもづく保険契約ヘッダ
        	E_Policy__c = policy.Id
        	// 処理日01~06
        	,ZSTUPDAY01__c = '20001223'
        	,ZSTUPDAY02__c = '20001224'
        	,ZSTUPDAY03__c = '20011224'
        	,ZSTUPDAY04__c = '20021224'
        	,ZSTUPDAY05__c = '20031224'
        	,ZSTUPDAY06__c = '20031224'
        	// ステップアップ金額01~06
        	,ZSTUPSUM01__c = 10000
        	,ZSTUPSUM02__c = 12000
        	,ZSTUPSUM03__c = 13000
        	,ZSTUPSUM04__c = 0
        	,ZSTUPSUM05__c = 16000
        	,ZSTUPSUM06__c = 17000
        	// ステップアップ金額率01~06
        	,ZSTUPRAT01__c = 100
        	,ZSTUPRAT02__c = 120
        	,ZSTUPRAT03__c = 0
        	,ZSTUPRAT04__c = 140
        	,ZSTUPRAT05__c = 160
        	,ZSTUPRAT06__c = 170
        	//商品毎ステップアップ金額率01~20
        	,ZATTMRAT01__c = 100
        	,ZATTMRAT02__c = 105
        	,ZATTMRAT03__c = 110
        	,ZATTMRAT04__c = 115
        	,ZATTMRAT05__c = 120
        	,ZATTMRAT06__c = 125
        	,ZATTMRAT07__c = 130
        	,ZATTMRAT08__c = 0
        	,ZATTMRAT09__c = 140
        	,ZATTMRAT10__c = 145
        	,ZATTMRAT11__c = 150
        	,ZATTMRAT12__c = 155
        	,ZATTMRAT13__c = 160
        	,ZATTMRAT14__c = 165
        	,ZATTMRAT15__c = 170
        	,ZATTMRAT16__c = 175
        	,ZATTMRAT17__c = 180
        	,ZATTMRAT18__c = 185
        	,ZATTMRAT19__c = 190
        	,ZATTMRAT20__c = 195
        	
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
        	
        	//テスト開始
			Test.startTest();
			PageReference pref = Page.E_Annuity;
        	pref.getParameters().put('Id',policy.Id);
        	

        	Test.setCurrentPage(pref);
        	Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
        	E_AnnuityController controller = new E_AnnuityController(standardcontroller);
        	E_AnnuityExtender extender = controller.getExtender();
        	//extender.init();
        	//extender.PageAction();
        	extender.getIsAgentJudge();
        	extender.getIsEmployeeJudge();
        	extender.getIsFundJudge();
        	
        	//ページアクションの戻り値
        	resultPage = extender.PageAction();

        	//テスト終了
			Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }
   /**
     * 最低限のレコードで実施
     * ユーザのフラグを3つ立てている。
     */
    private static testMethod void testAnnuity4() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
            SPVA_ZSTUPDSP__c = true
            ,SPVA_ZDPTYDSP__c = true
            ,COMM_CRTABLE__c = 'ST'
        );
        insert policy;

        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
            //ひもづく保険契約ヘッダ
            E_Policy__c = policy.Id
            //商品毎ステップアップ金額率01~20
            ,ZATTMRAT01__c = 0
            ,ZATTMRAT02__c = 105
            
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            
            //テスト開始
            Test.startTest();
            PageReference pref = Page.E_Annuity;
            pref.getParameters().put('Id',policy.Id);
            

            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_AnnuityController controller = new E_AnnuityController(standardcontroller);
            E_AnnuityExtender extender = controller.getExtender();
            //extender.init();
            //extender.PageAction();
            extender.getIsAgentJudge();
            extender.getIsEmployeeJudge();
            extender.getIsFundJudge();
            
            //ページアクションの戻り値
            resultPage = extender.PageAction();

            //テスト終了
            Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }

  /**
     * 最低限のレコードで実施
     * ユーザのフラグを3つ立てている。
     */
    private static testMethod void testAnnuity5() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
            SPVA_ZSTUPDSP__c = true
            ,SPVA_ZDPTYDSP__c = true
            ,COMM_CRTABLE__c = 'ST'
        );
        insert policy;

        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
            //ひもづく保険契約ヘッダ
            E_Policy__c = policy.Id
            //商品毎ステップアップ金額率01~20
            ,ZATTMRAT01__c = 100
            ,ZATTMRAT02__c = 0
            ,ZATTMRAT03__c = 110
            ,ZATTMRAT04__c = 115
            ,ZATTMRAT05__c = 120
            ,ZATTMRAT06__c = 125
            ,ZATTMRAT07__c = 130
            ,ZATTMRAT08__c = 200
            ,ZATTMRAT09__c = 140
            ,ZATTMRAT10__c = 145
            ,ZATTMRAT11__c = 150
            ,ZATTMRAT12__c = 155
            ,ZATTMRAT13__c = 160
            ,ZATTMRAT14__c = 165
            ,ZATTMRAT15__c = 170
            ,ZATTMRAT16__c = 175
            ,ZATTMRAT17__c = 180
            ,ZATTMRAT18__c = 185
            ,ZATTMRAT19__c = 190
            ,ZATTMRAT20__c = 195
            
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            
            //テスト開始
            Test.startTest();
            PageReference pref = Page.E_Annuity;
            pref.getParameters().put('Id',policy.Id);
            

            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_AnnuityController controller = new E_AnnuityController(standardcontroller);
            E_AnnuityExtender extender = controller.getExtender();
            //extender.init();
            //extender.PageAction();
            extender.getIsAgentJudge();
            extender.getIsEmployeeJudge();
            extender.getIsFundJudge();
            
            //ページアクションの戻り値
            resultPage = extender.PageAction();

            //テスト終了
            Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }    
  /**
     * 最低限のレコードで実施
     * ユーザのフラグを3つ立てている。
     */
    private static testMethod void testAnnuity6() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
            SPVA_ZSTUPDSP__c = true
            ,SPVA_ZDPTYDSP__c = true
            ,COMM_CRTABLE__c = 'ST'
        );
        insert policy;

        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
            //ひもづく保険契約ヘッダ
            E_Policy__c = policy.Id
            //商品毎ステップアップ金額率01~20
            ,ZATTMRAT01__c = 100
            ,ZATTMRAT02__c = 105
            ,ZATTMRAT03__c = 110
            ,ZATTMRAT04__c = 115
            ,ZATTMRAT05__c = 120
            ,ZATTMRAT06__c = 125
            ,ZATTMRAT07__c = null
            ,ZATTMRAT08__c = 200
            ,ZATTMRAT09__c = 140
            ,ZATTMRAT10__c = 145
            ,ZATTMRAT11__c = 150
            ,ZATTMRAT12__c = 155
            ,ZATTMRAT13__c = 160
            ,ZATTMRAT14__c = 165
            ,ZATTMRAT15__c = 170
            ,ZATTMRAT16__c = 175
            ,ZATTMRAT17__c = 180
            ,ZATTMRAT18__c = 185
            ,ZATTMRAT19__c = 190
            ,ZATTMRAT20__c = 195
            
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            
            //テスト開始
            Test.startTest();
            PageReference pref = Page.E_Annuity;
            pref.getParameters().put('Id',policy.Id);
            

            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_AnnuityController controller = new E_AnnuityController(standardcontroller);
            E_AnnuityExtender extender = controller.getExtender();
            //extender.init();
            //extender.PageAction();
            extender.getIsAgentJudge();
            extender.getIsEmployeeJudge();
            extender.getIsFundJudge();
            
            //ページアクションの戻り値
            resultPage = extender.PageAction();

            //テスト終了
            Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }
    private static testMethod void testAnnuity7() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
            SPVA_ZSTUPDSP__c = true
            ,SPVA_ZDPTYDSP__c = true
            ,COMM_CRTABLE__c = 'ST'
        );
        insert policy;

        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
            //ひもづく保険契約ヘッダ
            E_Policy__c = policy.Id
            //商品毎ステップアップ金額率01~20
            ,ZATTMRAT01__c = 100
            ,ZATTMRAT02__c = 105
            ,ZATTMRAT03__c = 110
            ,ZATTMRAT04__c = 115
            ,ZATTMRAT05__c = 120
            ,ZATTMRAT06__c = 125
            ,ZATTMRAT07__c = 150
            ,ZATTMRAT08__c = 200
            ,ZATTMRAT09__c = 140
            ,ZATTMRAT10__c = 0
            ,ZATTMRAT11__c = 150
            ,ZATTMRAT12__c = 155
            ,ZATTMRAT13__c = 160
            ,ZATTMRAT14__c = 165
            ,ZATTMRAT15__c = 170
            ,ZATTMRAT16__c = 175
            ,ZATTMRAT17__c = 180
            ,ZATTMRAT18__c = 185
            ,ZATTMRAT19__c = 190
            ,ZATTMRAT20__c = 195
            
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            
            //テスト開始
            Test.startTest();
            PageReference pref = Page.E_Annuity;
            pref.getParameters().put('Id',policy.Id);
            

            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_AnnuityController controller = new E_AnnuityController(standardcontroller);
            E_AnnuityExtender extender = controller.getExtender();
            //extender.init();
            //extender.PageAction();
            extender.getIsAgentJudge();
            extender.getIsEmployeeJudge();
            extender.getIsFundJudge();
            
            //ページアクションの戻り値
            resultPage = extender.PageAction();

            //テスト終了
            Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }

   private static testMethod void testAnnuity8() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        E_Policy__c policy = new E_Policy__c(
            SPVA_ZSTUPDSP__c = true
            ,SPVA_ZDPTYDSP__c = true
            ,COMM_CRTABLE__c = 'ST'
        );
        insert policy;

        //ステップアップ情報
        E_SUPPF__c suppf = new E_SUPPF__c(
            //ひもづく保険契約ヘッダ
            E_Policy__c = policy.Id
            //商品毎ステップアップ金額率01~20
            ,ZATTMRAT01__c = 100
            ,ZATTMRAT02__c = 105
            ,ZATTMRAT03__c = 110
            ,ZATTMRAT04__c = 115
            ,ZATTMRAT05__c = 120
            ,ZATTMRAT06__c = 125
            ,ZATTMRAT07__c = 150
            ,ZATTMRAT08__c = 200
            ,ZATTMRAT09__c = 140
            ,ZATTMRAT10__c = 170
            ,ZATTMRAT11__c = 150
            ,ZATTMRAT12__c = 155
            ,ZATTMRAT13__c = 160
            ,ZATTMRAT14__c = 165
            ,ZATTMRAT15__c = 170
            ,ZATTMRAT16__c = 175
            ,ZATTMRAT17__c = null
            ,ZATTMRAT18__c = 185
            ,ZATTMRAT19__c = 190
            ,ZATTMRAT20__c = 195
            
        );
        insert suppf;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            
            //テスト開始
            Test.startTest();
            PageReference pref = Page.E_Annuity;
            pref.getParameters().put('Id',policy.Id);
            

            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_AnnuityController controller = new E_AnnuityController(standardcontroller);
            E_AnnuityExtender extender = controller.getExtender();
            //extender.init();
            //extender.PageAction();
            extender.getIsAgentJudge();
            extender.getIsEmployeeJudge();
            extender.getIsFundJudge();
            
            //ページアクションの戻り値
            resultPage = extender.PageAction();

            //テスト終了
            Test.stopTest();
        }
        System.assertEquals(null,resultPage);
    }
}