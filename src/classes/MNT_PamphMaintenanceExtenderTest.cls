@isTest
private class MNT_PamphMaintenanceExtenderTest{
	
	@isTest
	private static void InitTest01(){
		
		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト保険');
		insert content;

		PageReference pageRef = Page.MNT_PamphMaintenance;
		Test.setCurrentPage(pageRef); 
		ApexPages.currentPage().getParameters().put('id', content.Id);
		ApexPages.currentPage().getParameters().put('clone','1');

		ApexPages.standardController stdController = new ApexPages.StandardController(content);
                MNT_PamphMaintenanceController controller = new MNT_PamphMaintenanceController(stdController);
                MNT_PamphMaintenanceExtender extender = new MNT_PamphMaintenanceExtender(controller);
		
		Test.startTest();
                extender.init();
		Test.stopTest();

		System.assertEquals(null,controller.record.UpsertKey__c);

	}

	@isTest
	private static void doCancelTest01(){

                I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト保険');
                insert content;

		PageReference PageRef = Page.MNT_PamphMaintenance;
		Test.setCurrentPage(PageRef);

                ApexPages.standardController stdController = new ApexPages.StandardController(content);
                MNT_PamphMaintenanceController controller = new MNT_PamphMaintenanceController(stdController);
                MNT_PamphMaintenanceExtender extender = new MNT_PamphMaintenanceExtender(controller);
                extender.init();

                Test.startTest();
                PageReference testRef = extender.doCancel();
                Test.stopTest();

                PageReference expectRef = new PageReference('/apex/MNT_PamphletSearch');

                System.assertEquals(expectRef.getUrl(),testRef.getUrl());
	}

	@isTest
	private static void doCancelTest02(){

		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険');
		insert content;

		PageReference PageRef = Page.MNT_PamphMaintenance;
		Test.setCurrentPage(PageRef);
		ApexPages.currentPage().getParameters().put('id', content.Id);
                ApexPages.standardController stdController = new ApexPages.StandardController(content);
                MNT_PamphMaintenanceController controller = new MNT_PamphMaintenanceController(stdController);
                MNT_PamphMaintenanceExtender extender = new MNT_PamphMaintenanceExtender(controller);
                extender.init();

                Test.startTest();
                PageReference testRef = extender.doCancel();
                Test.stopTest();

                PageReference expectRef = new PageReference('/apex/MNT_PamphletView?id=' + content.Id);

                System.assertEquals(expectRef.getUrl(),testRef.getUrl());
	}

	@isTest
	private static void doSaveTest01(){

		I_ContentMaster__c content = new I_ContentMaster__c();
		
		PageReference PageRef = Page.MNT_PamphMaintenance;
		Test.setCurrentPage(PageRef);
                ApexPages.standardController stdController = new ApexPages.StandardController(content);
                MNT_PamphMaintenanceController controller = new MNT_PamphMaintenanceController(stdController);
                MNT_PamphMaintenanceExtender extender = new MNT_PamphMaintenanceExtender(controller);
                extender.init();
                //テスト用のcsvファイル作成
                String body = 'abc';
                Attachment att = new Attachment(
                	Name = 'TestData.txt',
                	Body = Blob.valueOf(body)
                );
                controller.record.Name = 'テスト保険';

                extender.uploadFile = att;

                Test.startTest();
                PageReference testRef = extender.doSave();
                Test.stopTest();

                System.assertEquals(null,controller.record.Id);
	}
        @isTest
        private static void doSaveTest02(){

                I_ContentMaster__c content = new I_ContentMaster__c();
                
                PageReference PageRef = Page.MNT_PamphMaintenance;
                Test.setCurrentPage(PageRef);
                ApexPages.standardController stdController = new ApexPages.StandardController(content);
                MNT_PamphMaintenanceController controller = new MNT_PamphMaintenanceController(stdController);
                MNT_PamphMaintenanceExtender extender = new MNT_PamphMaintenanceExtender(controller);
                extender.init();
                //テスト用のcsvファイル作成
                String body = 'abc';
                Attachment att = new Attachment(
                        Name = 'TestData.txt',
                        Body = Blob.valueOf(body)
                );
                controller.record.FormNo__c = '918291';

                extender.uploadFile = att;

                Test.startTest();
                PageReference testRef = extender.doSave();
                Test.stopTest();

                System.assertEquals(null,controller.record.Id);
        }

	@isTest
	private static void doSaveTest03(){

		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト保険' , FormNo__c = '654321');
		insert content;

		PageReference PageRef = Page.MNT_PamphMaintenance;
		Test.setCurrentPage(PageRef);
		ApexPages.currentPage().getParameters().put('id', content.Id);
		ApexPages.currentPage().getParameters().put('clone','1');
                ApexPages.standardController stdController = new ApexPages.StandardController(content);
                MNT_PamphMaintenanceController controller = new MNT_PamphMaintenanceController(stdController);
                MNT_PamphMaintenanceExtender extender = new MNT_PamphMaintenanceExtender(controller);
                extender.init();
                //テスト用のcsvファイル作成
                String body = 'abc';
                Attachment att = new Attachment(
                	Name = 'TestData.txt',
                	Body = Blob.valueOf(body)
                );

                controller.record.Name = 'テスト保険コピー';
                controller.record.FormNo__c = '123456';
                extender.uploadFile = att;

                Test.startTest();
                PageReference testRef = extender.doSave();
                Test.stopTest();

                Attachment selectAtt = [SELECT Id,Name,ParentId FROM Attachment];
                I_ContentMaster__c selectContent = [SELECT Id,Name FROM I_ContentMaster__c WHERE Id =: controller.record.Id];

                System.assertEquals(selectContent.Id,selectAtt.parentId);
	}
}