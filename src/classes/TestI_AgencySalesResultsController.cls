@isTest
private class TestI_AgencySalesResultsController {

	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static Contact agent;
	private static E_BizDataSyncLog__c ebizLog;
	private static E_AgencySalesResults__c asr;

	/**
	 * 実行ユーザ: 本社営業部
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test001(){
		// 実行ユーザ作成（本社営業部）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('AH');

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));		//パラメータ'row'に繰り返し行数を付与
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();

			Test.stopTest();
			// Assertion
			PageReference redirectPage = Page.IRIS_Unit;
			System.assertEquals(controller.pageAction().getUrl(), redirectPage.getUrl());
		}
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test002(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('AH');
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88');
		insert unit;

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));		//パラメータ'row'に繰り返し行数を付与
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();

			Test.stopTest();
			// Assertion
			PageReference redirectPage = Page.IRIS_SalesResults;
			System.assertEquals(controller.pageAction().getUrl(), redirectPage.getUrl());
		}
	}
	/**
	 * 実行ユーザ: AH
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test003(){

		// テストデータ作成
		createData('AH');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createAgentUser(true, 'TestUser', 'E_PartnerCommunity', agent.Id);
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'AH',ZINQUIRR__c = 'L110001',AppMode__c = 'IRIS',ZSTATUS01__c = '1');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();
			controller.getPreviewURL();
			Test.stopTest();
			// Assertion
			System.assertEquals(controller.agencyId,String.valueOf(distribute.id));
			System.assertEquals(controller.viewType,'1');
		}
	}

	/**
	 * 実行ユーザ: AY
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test004(){

		// テストデータ作成
		createData('AY');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createAgentUser(true, 'TestUser', 'E_PartnerCommunity', agent.Id);
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'AY',ZINQUIRR__c = 'L210002',AppMode__c = 'IRIS',ZSTATUS01__c = '1');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);
		insert new E_AgencySalesResults__Share(
			ParentId = asr.Id
			, AccessLevel = 'Edit'
			, UserOrGroupId = actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			insert new E_ProductBreakdown__c(E_AgencySalesResults__c = asr.Id);

			Test.startTest();
				I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
				controller.pageAction();
				controller.getPreviewURL();
				controller.getHasProduct();
			Test.stopTest();
			// Assertion
			System.assertEquals(controller.officeId,String.valueOf(office.id));
			System.assertEquals(controller.viewType,'2');
		}
	}

	/**
	 * 実行ユーザ: AT
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test005(){

		// テストデータ作成
		createData('AT');
		agent.E_CL3PF_AGNTNUM__c = 'at001';
		update agent;

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createAgentUser(true, 'TestUser', 'E_PartnerCommunity', agent.Id);
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'AT',ZINQUIRR__c = 'L310003',AppMode__c = 'IRIS',ZSTATUS01__c = '1');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
				I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
				controller.pageAction();
			Test.stopTest();
			// Assertion
			System.assertEquals(controller.agentId,String.valueOf(agent.id));
			System.assertEquals(controller.viewType,'3');
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり
	 */
	private static testMethod void pageAction_test006(){

		// テストデータ作成
		createData('AH');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);
		pr.getParameters().put('agency', String.valueOf(distribute.id));

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();
			controller.getBrNo();
			Test.stopTest();
			// Assertion
			System.assertEquals(controller.agencyId,String.valueOf(distribute.id));
			System.assertEquals(controller.viewType,'1');
			System.assert(controller.getIsShowAchievement());
		}
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり 検索から
	 */
	private static testMethod void pageAction_test007(){

		// テストデータ作成
		createData('AH');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);
		pr.getParameters().put('id', String.valueOf(distribute.id));
		pr.getParameters().put('type', 'agency');

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();

			Test.stopTest();
			// Assertion
			System.assertEquals(controller.agencyId,String.valueOf(distribute.id));
			System.assertEquals(controller.viewType,'1');
			System.assert(controller.getIsShowAchievement());
		}
	}

	/**
	 * 実行ユーザ: AT
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test008(){

		// テストデータ作成
		createData('AT');
		agent.E_CL3PF_AGNTNUM__c = 'at01';
		update agent;

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createAgentUser(true, 'TestUser', 'E_PartnerCommunity', agent.Id);
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'AT',ZINQUIRR__c = 'L310003',AppMode__c = 'IRIS',ZSTATUS01__c = '1');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
				I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
				controller.pageAction();
			Test.stopTest();
			// Assertion
			System.assertEquals(controller.agentId,String.valueOf(agent.id));
		}
	}

	/**
	 * moveToNewPolicyList
	 */
	private static testMethod void moveToNewPolicyList_test001(){

		// テストデータ作成
		createData('AH');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();

			pr.getParameters().put('id', String.valueOf(distribute.id));
			controller.moveToNewPolicyList();
			Test.stopTest();
			// Assertion
			PageReference newPage = Page.IRIS_NewPolicy;
			System.assertEquals(controller.moveToNewPolicyList().getUrl(), newPage.getUrl());
		}
	}

	/**
	 * moveToOwnerList
	 */
	private static testMethod void moveToOwnerList_test001(){

		// テストデータ作成
		createData('AH');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		// IRISメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
			controller.pageAction();

			pr.getParameters().put('id', String.valueOf(distribute.id));
			controller.moveToOwnerList();
			Test.stopTest();
			// Assertion
			PageReference newPage = Page.IRIS_Owner;
			System.assertEquals(controller.moveToOwnerList().getUrl(), newPage.getUrl() + '?irismn=' + mn.Id);
		}
	}
	/**
	 * getterテスト
	 */
	private static testMethod void getter_test001(){

		// テストデータ作成
		createData('AH');

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// レコードを共有
		TestI_TestUtil.createAccountShare(office.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(distribute.Id, actUser.Id);

		List<E_MessageMaster__c> msgs = new List<E_MessageMaster__c>();
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|001',Value__c = 'AMS|SR|001',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|002',Value__c = 'AMS|SR|002',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|003',Value__c = 'AMS|SR|003',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|004',Value__c = 'AMS|SR|004',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|005',Value__c = 'AMS|SR|005',Type__c = 'メッセージ'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|006',Value__c = 'AMS|SR|006',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|007',Value__c = 'AMS|SR|007',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|008',Value__c = 'AMS|SR|008',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|012',Value__c = 'AMS|SR|012',Type__c = 'ディスクレイマー'));
		insert msgs;
		// ページ表示
		PageReference pr = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_AgencySalesResultsController controller = new I_AgencySalesResultsController();
			controller.getDisclaimers();
			controller.getChartDisclaimers();
			controller.getLADisclaimers();
			controller.getMsgSRAgent();
			controller.getPreviewURL();
			controller.getCanFeeDetail();
			controller.getProductDisclaimers();
			controller.getBrNo();
			Test.stopTest();
		}
	}
	private static void createData(String hierarchy){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';		// 営業日

		//連携ログ作成
		Date d = E_Util.string2DateyyyyMMdd(BUSINESS_DATE);
		ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = d,BatchEndDate__c = d);
		insert ebizLog;

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// Account 事務所
		office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
		insert office;
		suboffice = new Account(Name = 'テスト事務所SUB',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
		insert suboffice;

		// Contact 募集人作成
		agent = new Contact(AccountId = office.id, LastName = 'テスト募集人');
		insert agent;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = hierarchy,
			QualifSim__c = '優績S'
		);
		if(hierarchy == 'AY'){
			asr.Account__c = office.id;
		}else if(hierarchy == 'AT'){
			asr.Account__c = office.id;
			asr.Agent__c = agent.id;
		}
		insert asr;
	}
	//irisメニューー
	static I_MenuMaster__c createIRISMenu(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		I_MenuMaster__c mn = new I_MenuMaster__c();
		system.runAs(thisUser){
			mn.MenuKey__c = 'ownerList';
			mn.MainCategory__c = '2.保有契約';
			insert mn;
		}
		return mn;
	}
}