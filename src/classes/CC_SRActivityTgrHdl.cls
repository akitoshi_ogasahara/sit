public with sharing class CC_SRActivityTgrHdl{

	// Trigger Handler実行確認用変数
	private static Set<String> runTgrSet = new Set<String>();

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<CC_SRActivity__c> newList){
		if(!runTgrSet.contains('onBeforeInsert')){
			runTgrSet.add('onBeforeInsert');
			validateBeforeInsert(newList);
		}
	}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<CC_SRActivity__c> newList, Map<Id,CC_SRActivity__c> newMap){}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<CC_SRActivity__c> oldList, List<CC_SRActivity__c> newList, Map<Id,CC_SRActivity__c> oldMap, Map<Id,CC_SRActivity__c> newMap){
		if(!runTgrSet.contains('onBeforeUpdate')){
			runTgrSet.add('onBeforeUpdate');
			validateBeforeUpdate(oldList, newList, oldMap, newMap);
		}
	}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<CC_SRActivity__c> oldList, List<CC_SRActivity__c> newList, Map<Id,CC_SRActivity__c> oldMap, Map<Id,CC_SRActivity__c> newMap){}

	/**
	 * Validate Before Insert
	 */
	private static void validateBeforeInsert(List<CC_SRActivity__c> newList){
		validateBeforeCommonCheck(newList);
	}

	/**
	 * Validate Before Update
	 */
	private static void validateBeforeUpdate(List<CC_SRActivity__c> oldList, List<CC_SRActivity__c> newList, Map<Id,CC_SRActivity__c> oldMap, Map<Id,CC_SRActivity__c> newMap){
		validateBeforeCommonCheck(newList);

		Set<Id> objIdSet = new Set<Id>();
		Set<Id> srNoIdSet = new Set<Id>();

		for(CC_SRActivity__c obj : oldList){
			objIdSet.add(obj.Id);
			srNoIdSet.add(obj.CC_SRNo__c);
		}

		List<AggregateResult> arList = [SELECT CC_SRNo__c, COUNT(Id), SUM(CC_IsUnAssigned__c) FROM CC_SRActivity__c WHERE Id NOT IN :objIdSet AND CC_SRNo__c IN :srNoIdSet AND CC_Status__c NOT IN ('終了','作業不要') Group By CC_SRNo__c];
		Set<Id> invalidSrNoIdSet = new Set<Id>();
		for(AggregateResult ar : arList){
			if(ar.get('expr0') == ar.get('expr1')){
				invalidSrNoIdSet.add((Id)ar.get('CC_SRNo__c'));
			}
		}
		Integer i = 0;
		String sysInvalidCommonMsg = System.Label.CC_SRActivityTgrHdl_ErrMsg_InvalidData;
		for(CC_SRActivity__c obj : newList){
			if(obj.CC_Status__c == '終了' || obj.CC_Status__c == '作業不要'){
				if(obj.CC_Status__c != oldList[i].CC_Status__c){
					if(!obj.CC_Worker__c.contains('/')){
						obj.CC_Status__c.addError(System.Label.CC_SRActivityTgrHdl_ErrMsg_InvalidStatus);
					}else if(invalidSrNoIdSet.contains(oldList[i].CC_SRNo__c)){
						obj.CC_Status__c.addError(System.Label.CC_SRActivityTgrHdl_ErrMsg_InvalidStatus);
					}
				}else{
					if(obj.CC_Worker__c != oldList[i].CC_Worker__c){
						obj.CC_Worker__c.addError(sysInvalidCommonMsg.replace('{0}', '作業者'));
					}

					if(obj.CC_ActivityType__c != oldList[i].CC_ActivityType__c){
						obj.CC_ActivityType__c.addError(sysInvalidCommonMsg.replace('{0}', 'SR活動名'));
					}

					if(obj.CC_SRNo__c != oldList[i].CC_SRNo__c){
						obj.CC_SRNo__c.addError(sysInvalidCommonMsg.replace('{0}', 'SR番号'));
					}
				}
			}

			i++;
		}

	}

	/**
	 * Validate Before Common Check
	 */
	private static void validateBeforeCommonCheck(List<CC_SRActivity__c> newList){
		Set<String> actMstSet = new Set<String>();
		for(CC_ActivityMaster__c obj : [SELECT Name FROM CC_ActivityMaster__c]){
			actMstSet.add(obj.Name);
		}

		for(CC_SRActivity__c obj : newList){
			if(obj.CC_Order__c == null){
				obj.CC_Order__c.addError(System.Label.CC_SRActivityTgrHdl_ErrMsg_NotInputData);
			}

			if(String.isBlank(obj.CC_SRNo__c)){
				obj.CC_SRNo__c.addError(System.Label.CC_SRActivityTgrHdl_ErrMsg_NotInputData);
			}

			if(!actMstSet.contains(obj.CC_ActivityType__c)){
				obj.CC_ActivityType__c.addError(System.Label.CC_SRActivityTgrHdl_ErrMsg_InvalidActivityType);
			}
		}
	}
}