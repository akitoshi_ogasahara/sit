public with sharing class UserTriggerHandler {

	public static Boolean isSkipTriggerActions = System.Label.E_IsSkipUserTrigger == '1';
	public static Boolean isSkipTriggerSCActions = false;

	//before insert
	public void onBeforeInsert(List<User> newList){
		changeProfileId(newList);	//特定代理店ユーザの場合、プロファイルの差し替え
	}

	// after insert
	public void onAfterInsert(List<User> newList){
	}

	//before update
	public void onBeforeUpdate(List<User> newList, Map<Id, User> oldMap){
		changeProfileId(newList);	//特定代理店ユーザの場合、プロファイルの差し替え

		for(User newUser : newList){
			User oldUser = oldMap.get(newUser.Id);
			String dateStr = E_Util.getSysDate(null);	//更新実行日を「yyyyMMdd」形式の文字列で取得

			if(oldUser.isActive && !newUser.isActive){			//Userが無効化更新されたとき
				newUser.UserName = newUser.UserName +'_'+ dateStr;		//UserNameの末尾に「_yyyyMMdd」を付与
				newUser.Email =  dateStr  +'_' + newUser.Email;			//メールの先頭に「yyyyMMdd_」を付与
			}else if(!oldUser.isActive && newUser.isActive){	//Userが有効化更新されたとき
				//ログイン名を取得し、suffixを付与してユーザ名にする
				Integer uNamelen = newUser.UserName.length();	//長さ
				String ymd = newUser.UserName.substring(uNamelen - 9, uNamelen);	//UserNameの末尾 「_yyyyMMdd」想定
				String ymdMail = newUser.Email.substring(0,9);						//メールの先頭 「yyyyMMdd_」想定

				//1桁目= '_' & 後ろがyyyyMMdd
				if( ymd.substring(0,1) == '_' && E_Util.isYyyyMMddNonSrash(ymd.substring(1,ymd.length())) ){
					String loginName = newUser.UserName.substringBeforeLast(System.Label.E_USERNAME_SUFFIX);
					newUser.UserName = loginName + System.Label.E_USERNAME_SUFFIX;	//UserNameの末尾の「_yyyyMMdd」を除去
					//newUser.Email = newUser.Email.substringAfterLast(ymd+'_');
				}

				//先頭がyyyyMMdd & 9桁目= '_'
				if( ymdMail.substring(ymdMail.length()-1 , ymdMail.length()) == '_'
					&& E_Util.isYyyyMMddNonSrash(ymdMail.substring(0,8))){
					newUser.Email = newUser.Email.substringAfterLast(ymdMail);		//メールの先頭の「yyyyMMdd_」を除去
				}
			}
		}
	}

	//after update
	public void onAfterUpdate(List<User> newList, Map<Id, User> oldMap){
		//E_IDCPFレコードを空更新して権限セットを付与
		E_IDCPFUpdatebyUserTrigger eidcUpd = new E_IDCPFUpdatebyUserTrigger();
		eidcUpd.doUpdateEIDCPF(newList, oldMap);

		if(isSkipTriggerSCActions==false){
			/* 自主点検管理責任者の「ユーザメールあり」の更新 */
			SC_Util scUtil = new SC_Util();
			scUtil.updateAgencyManagerHasUserEmail(newList, oldMap);
		}
	}

	/*
	*	特定代理店ユーザの場合、プロファイルの差し替え（E_PartnerCommunityからの切替のみに対応）
	*			一般代理店の場合 	E_PARTNERCOMMUNITY_IA
	*			住生の場合		E_PARTNERCOMMUNITY_Sumisei		常に強制的にセットされます　一方通行　E_PartnerCommunityへの戻しは運用対応となります
	*/
	private List<User> changeProfileId(List<User> newList){
		//プロファイルIDの取得
		Map<String, Profile> partnerProfiles = E_ProfileDaoWithout.getRecsByNames(new Set<String>{
																									E_Const.PROFILE_E_PARTNERCOMMUNITY,
																									E_Const.PROFILE_E_PARTNERCOMMUNITY_IA,
																									E_Const.PROFILE_E_PARTNERCOMMUNITY_Sumisei,
																									E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS,
																									E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2
																								});
		// NewListのContactIdからContact情報取得
		Set<Id> conids = new Set<Id>();
		for(User u : newList){
			conids.add(u.ContactId);
		}
		Map<Id, Contact> usrsWithContact = E_ContactDaoWithout.getRecsMapByIds(conids);

		for(User usr : newList){
			Contact usrCon = usrsWithContact.get(usr.ContactId);

			//代理店ユーザで有効な場合に実施
			if((usr.profileId == null || usr.profileId == partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY).Id)
				&& usr.isActive && usrCon!=null){
				//住生ユーザの場合
				if(usrCon.E_AccParentCord__c == E_Const.ZHEADAY_SUMISEI){
					if(usrCon.E_CL3PF_AGTYPE__c != '02' && usrCon.E_CL3PF_AGTYPE__c != '03'){
						usr.ProfileId = partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_Sumisei).Id;
					}else{
						/*カスタム表示ラベルの値でライセンスを決める
							0:Partner Community Login
							1:Partner Community
						*/
						if(System.Label.E_SUMISEI_LICENSE == '0'){
							usr.ProfileId = partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS).Id;
						}else{
							usr.ProfileId = partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2).Id;
						}
					}

				//一般代理店ユーザの場合
				//}else if(usrCon.contact.Account.Parent.E_MenuKind__c == null
				//			&& usrCon.contact.Account.Parent.E_CL1PF_KCHANNEL__c <> 'BK'){
				// 一般代理店の判断を代理店格のID自動発行対象代理店（canSelfRegist__c）で行う　修正2017-2-14
				//   →　メニュー種類未設定　かつ　BKチャネル以外　かつ　自動発行対象外でない
				}else if(usrCon.Account.canSelfRegist__c){
					usr.ProfileId = partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA).Id;
				}

			//住生から他代理店へ異動となった場合（同一募集人コード）
			}else if(usr.profileId != null 
				&& (usr.profileId == partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS).Id || usr.profileId == partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS2).Id)
				&& usrCon.E_AccParentCord__c != E_Const.ZHEADAY_SUMISEI && usr.isActive && usrCon!=null){
				usr.ProfileId = partnerProfiles.get(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA).Id;
			}
		}
		return newList;
	}
}