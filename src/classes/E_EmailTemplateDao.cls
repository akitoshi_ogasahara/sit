public with sharing class E_EmailTemplateDao {
    /**
     * DeveloperNameよりメールテンプレート取得
     * @param String DeveloperName
     * @return EmailTemplate
     */
    public static EmailTemplate getRecByDeveloper(String devName){
    	return [Select Id, Subject, Body From EmailTemplate Where DeveloperName = :devName];
    }
}