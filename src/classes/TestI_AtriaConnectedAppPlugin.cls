// P16-0003 Atria対応開発
/**
 * I_AtriaConnectedAppPluginのテストクラス.
 */
@isTest
private class TestI_AtriaConnectedAppPlugin{

	static testMethod void myUnitTest01() {
		
		TestI_TestUtil.createIDCPF(true, UserInfo.getUserId(), 'EP');
		
		I_AtriaConnectedAppPlugin cls = new I_AtriaConnectedAppPlugin();
		
		Test.startTest();
		
		cls.customAttributes(UserInfo.getUserId(), null, new Map<String, String>(), Auth.InvocationContext.SAML_ASSERTION);
		
		Test.stopTest();
	}
	
	static testMethod void myUnitTest02() {
		Account accParent = TestI_TestUtil.createAccount(true, null);
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		String actUserLastName = 'fstest';
		Contact con = TestI_TestUtil.createContact(false, accAgency.Id, actUserLastName);
		con.E_CL3PF_AGNTNUM__c = 'AG000|0001';
		insert con;
		
		User u = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		TestI_TestUtil.createIDCPF(true, u.Id, 'AY');
		TestI_TestUtil.createIDCPF(true, UserInfo.getUserId(), 'EP');
		
		createLog('Atriaリダイレクト画面', I_Const.ACTION_TYPE_ATRIA_SSO, u.Id, true);
		
		I_AtriaConnectedAppPlugin cls = new I_AtriaConnectedAppPlugin();

		Test.startTest();
		
		cls.customAttributes(UserInfo.getUserId(), null, new Map<String, String>(), Auth.InvocationContext.SAML_ASSERTION);
		
		Test.stopTest();
	}
	
	
	static testMethod void myUnitTest03() {
		Account accParent = TestI_TestUtil.createAccount(true, null);
		Account accAgency = TestI_TestUtil.createAccount(true, accParent);
		String actUserLastName = 'fstest';
		Contact con = TestI_TestUtil.createContact(false, accAgency.Id, actUserLastName);
		con.E_CL3PF_AGNTNUM__c = 'AG000|0001';
		insert con;
		
		User u = TestI_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		
		createLog('Atriaリダイレクト画面', I_Const.ACTION_TYPE_ATRIA_SSO, u.Id, true);

		I_AtriaConnectedAppPlugin cls = new I_AtriaConnectedAppPlugin();

		Test.startTest();
		
		Map<String, String> mp = cls.customAttributes(UserInfo.getUserId(), null, new Map<String, String>(), Auth.InvocationContext.SAML_ASSERTION);
		
		Test.stopTest();
		
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_NNLINK_ID));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_ID_TYPE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_INQUIRER_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_USER_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_AGENT_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_AGECY_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_AGECY_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_AGECY_1OFFICE_FLG));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_AGECY_CLS_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_POSTAL));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_ADDRESS));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_PHONE1));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_PHONE2));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_OFFICE_COMM_CAT));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_MR_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_MR_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_BRANCH_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_BRANCH_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_AGENCY_BK_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_MOF_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_CHANNEL));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_FI_BRANCH_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_MEM_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.SAML_ATT_TAG_ATR_COMM_FLG));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_NNLINK_ID));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_ID_TYPE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_INQUIRER_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_USER_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_AGENT_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_AGECY_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_AGECY_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_AGECY_1OFFICE_FLG));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_AGECY_CLS_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_POSTAL));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_ADDRESS));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_PHONE1));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_PHONE2));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_OFFICE_COMM_CAT));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_MR_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_MR_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_BRANCH_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_BRANCH_NAME));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_AGENCY_BK_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_MOF_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_CHANNEL));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_FI_BRANCH_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_MEM_CODE));
		System.assertEquals(true, mp.containsKey(I_Const.ALT_SAML_ATT_TAG_ATR_COMM_FLG));
	}

	static E_Log__c createLog(String Name, String actType, Id targetUserId, Boolean isInsert) {
		E_Log__c log = E_LogUtil.getLogRec('hoge', 'hogehoge');
		log.Name = Name;
		log.AppName__c = 'IRIS';
		log.ActionType__c = actType;
		log.TargetUser__c = targetUserId;
		
		if(isInsert) {
			insert log;
		}
		
		return log;
	}
}