public class I_Const {

    // IRIS Phase1 GoLive Date  Day2のローンチ日（ユーザロックなどの起算日とする）
    public static final Date IRIS_GOLIVE_DAY = Date.newInstance(2017,3,6);
    

    // CSM機能で利用するURLの引数
    public static final String URL_PARAM_PAGE = 'page';
    public static final String URL_PARAM_PAGEKEY = 'pgkey';
    public static final String URL_PARAM_MENUID = 'irismn';
    public static final String URL_PARAM_DOC_NUM = 'docnumber';
    public static final String URL_PARAM_CATEGORY = 'category';
    public static final String URL_PARAM_INFOID = 'infoid';         //お知らせのId

    // IRISメニューのクリック時の動作
    public static final String MENU_CLICK_ACTION_CMS = '1.CMS';
    public static final String MENU_CLICK_ACTION_VF = '2.VF';
    public static final String MENU_CLICK_ACTION_LINK = '3.リンク';
    public static final String MENU_CLICK_ACTION_SUB_MENU = '4.サブメニュー';

    // グローバルナビの表示名称
    public static final String MENU_DISPLAY_NAME_NEW_PROPOSAL = '新契約提案';
    public static final String MENU_DISPLAY_NAME_POSSESSION_CONTACT = '保有契約';
    public static final String MENU_DISPLAY_NAME_LIBLARY = '資料・動画'; //TODO
    public static final String MENU_DISPLAY_NAME_AGENCY_MANAGEMENT = '代理店事務';
    public static final String MENU_DISPLAY_NAME_CATEGORY_FAQ = 'よくあるご質問';
    public static final String MENU_DISPLAY_NAME_CATEGORY_LOG = '利用履歴';

    // グローバルナビのアイコン
    public static final String MENU_ICON_NAME_NEW_PROPOSAL = 'newopp';
    public static final String MENU_ICON_NAME_POSSESSION_CONTACT = 'policy';
    public static final String MENU_ICON_NAME_LIBLARY = 'library';
    public static final String MENU_ICON_NAME_AGENCY_MANAGEMENT = 'agency';
    public static final String MENU_ICON_NAME_CATEGORY_FAQ = 'qa';
    public static final String MENU_ICON_NAME_CATEGORY_LOG = 'log';

    // IRISメニューのメインカテゴリ
    public static final String MENU_MAIN_CATEGORY_NEW_PROPOSAL = '1.新規提案';
    public static final String MENU_MAIN_CATEGORY_POSSESSION_CONTACT = '2.保有契約';
    public static final String MENU_MAIN_CATEGORY_LIBLARY = '3.資料・動画';
    public static final String MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT = '4.代理店管理';
    public static final String MENU_MAIN_CATEGORY_FAQ = '5.よくあるご質問';
    public static final String MENU_MAIN_CATEGORY_LOG = '6.利用履歴';

    // グロナビ、サブナビで選択するカテゴリAlias
    public static final String MENU_CATEGORY_ALIAS_NEW_OPP = 'newopp';
    public static final String MENU_CATEGORY_ALIAS_POLICY = 'policy';
    public static final String MENU_CATEGORY_ALIAS_LIBRARY = 'library';
    public static final String MENU_CATEGORY_ALIAS_LIB_TOOL = 'lib_tool';
    public static final String MENU_CATEGORY_ALIAS_LIB_PAMPH = 'lib_pamph';
    public static final String MENU_CATEGORY_ALIAS_LIB_MNT = 'lib_mnt';
    public static final String MENU_CATEGORY_ALIAS_AMS = 'ams';
    public static final String MENU_CATEGORY_ALIAS_FAQ = 'faq';
    public static final String MENU_CATEGORY_ALIAS_LOG = 'log';
    public static final String MENU_CATEGORY_ALIAS_LIB_FEAT = 'lib_feat';
    public static final String MENU_CATEGORY_ALIAS_LIB_MANUAL = 'lib_manual';
    public static final String MENU_CATEGORY_ALIAS_LIB_OTHER = 'lib_other';

    // メインカテゴリ →　メニュー名
    public static final Map<String,String> CATEGORY_ALIAS_NAME = new Map<String,String>{
        MENU_CATEGORY_ALIAS_NEW_OPP => I_Const.MENU_MAIN_CATEGORY_NEW_PROPOSAL,
        MENU_CATEGORY_ALIAS_POLICY => I_Const.MENU_MAIN_CATEGORY_POSSESSION_CONTACT,
        MENU_CATEGORY_ALIAS_LIBRARY => I_Const.MENU_MAIN_CATEGORY_LIBLARY,
        MENU_CATEGORY_ALIAS_AMS => I_Const.MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT,
        MENU_CATEGORY_ALIAS_FAQ => I_Const.MENU_MAIN_CATEGORY_FAQ,
        MENU_CATEGORY_ALIAS_LOG => I_Const.MENU_MAIN_CATEGORY_LOG,
        MENU_CATEGORY_ALIAS_LIB_TOOL => MENU_CATEGORY_ALIAS_LIB_TOOL,
        MENU_CATEGORY_ALIAS_LIB_PAMPH => MENU_CATEGORY_ALIAS_LIB_PAMPH,
        MENU_CATEGORY_ALIAS_LIB_MNT => MENU_CATEGORY_ALIAS_LIB_MNT,
        MENU_CATEGORY_ALIAS_LIB_FEAT => MENU_CATEGORY_ALIAS_LIB_FEAT,
        MENU_CATEGORY_ALIAS_LIB_MANUAL => MENU_CATEGORY_ALIAS_LIB_MANUAL,
        MENU_CATEGORY_ALIAS_LIB_OTHER => MENU_CATEGORY_ALIAS_LIB_OTHER
    };

    // サブカテゴリ
    public static final String MENU_SUB_CATEGORY_LIBLARY_TOP = '1.トップ';
    public static final String MENU_SUB_CATEGORY_LIBLARY_SALESTOOL = '3.販売促進資料';
    public static final String MENU_SUB_CATEGORY_LIBLARY_PAMPHLET = '10.パンフレット等';
    public static final String MENU_SUB_CATEGORY_LIBLARY_INFO = '4.手続書類';
    public static final String MENU_SUB_CATEGORY_LIBLARY_FEATURE = '7.特集';
    public static final String MENU_SUB_CATEGORY_LIBLARY_MANUAL = '8.マニュアル';
    public static final String MENU_SUB_CATEGORY_LIBLARY_OTHER = '9.その他';

    public static Map<String, String> LIBRARY_SUB_CATE_NAME_ALIAS = new Map<String, String>{
        MENU_SUB_CATEGORY_LIBLARY_TOP => MENU_CATEGORY_ALIAS_LIBRARY,           //'1.トップ'=>'library',
        MENU_SUB_CATEGORY_LIBLARY_SALESTOOL => MENU_CATEGORY_ALIAS_LIB_TOOL,    //'3.販売促進資料'=>'lib_tool',
        MENU_SUB_CATEGORY_LIBLARY_PAMPHLET => MENU_CATEGORY_ALIAS_LIB_PAMPH,          //'10.パンフレット等'=>'lib_pamph',
        MENU_SUB_CATEGORY_LIBLARY_INFO => MENU_CATEGORY_ALIAS_LIB_MNT,          //'4.保全手続書類'=>'lib_mnt',
        MENU_SUB_CATEGORY_LIBLARY_FEATURE => MENU_CATEGORY_ALIAS_LIB_FEAT,  //'7.特集'=>'lib_feat'
        MENU_SUB_CATEGORY_LIBLARY_MANUAL => MENU_CATEGORY_ALIAS_LIB_MANUAL, //'8.マニュアル'=>'lib_manual'
        MENU_SUB_CATEGORY_LIBLARY_OTHER => MENU_CATEGORY_ALIAS_LIB_OTHER    //'9.その他'=>'lib_other'
    };  

    // IRISページのコンテンツタイプ
    public static final String PAGE_CONTENT_TYPE_VF = '1.Visualforce';
    public static final String PAGE_CONTENT_TYPE_CMS = '2.CMS';

    // 代理店一覧ページの母店担当表示
    public static final String AGENT_VALUE_ZAGCYNUM = '母店担当';

    // 募集人一覧ページのNN LinkIDのステータス表示
    public static final String NNLINKID_STATUS_UNISSUED = '未発行';
    public static final String NNLINKID_STATUS_ACTIVE = '有効';
    public static final String NNLINKID_STATUS_LONGTERMUNUSED = '無効（長期未使用）';
    public static final String NNLINKID_STATUS_INACTIVE = '無効';
    public static final String NNLINKID_STATUS_PWLOCK = 'パスワードロック';
    //public static final String NNLINKID_STATUS_IDLOCK = 'IDロック';
    public static final String NNLINKID_STATUS_PWCHANGE = '要パスワード変更';
    public static final String NNLINKID_STATUS_NOPOLICY = '保有なし';       // JIRA_#51 170206
    public static final String NNLINKID_STATUS_UNUSED = '長期未使用';        // JIRA_#51 170208
    
    // アクセス経路
    public static final String ACCESS_VIA_INTERNET = 'インターネット経由';
    public static final String ACCESS_VIA_CMN_GW = '共同GW経由';
    public static final String DISP_RIGHT_EMPLOYEE = '社員ユーザのみ';
    public static final String DISP_RIGHT_AGENCY = '代理店ユーザのみ';
    public static final String ATRIA_CAN_ACCESS_USER = 'Atria閲覧可能ユーザのみ'; // P16-0003 Atria対応開発
    public static final String ATRIA_CAN_NOT_ACCESS_USER = 'Atria閲覧不可ユーザのみ'; // P16-0003 Atria対応開発

    //IRIS_CMSコンテンツのクリックアクション
    public static final String CMSCONTENTS_CLICKACTION_DL_ATTACHMENT = 'DL(Attachment)';
    public static final String CMSCONTENTS_CLICKACTION_DL_CHATTERFILE = 'DL(ChatterFile)';
    public static final String CMSCONTENTS_CLICKACTION_DL_CONTENTS = 'DL(Contents)';
    public static final String CMSCONTENTS_CLICKACTION_DL_NNLINK = 'DL(NNLink)';
    public static final String CMSCONTENTS_CLICKACTION_URL = 'URL';

    //IRIS_CMSコンテンツのカテゴリ名
    public static final String CMSCONTENTS_CATEGORY_MRDOCUMENT = 'MR作成資料'; //ライブラリ拡張追加対応
    public static final String CMSCONTENTS_CATEGORY_RECRUIT = '会社案内・リクルート'; //ライブラリ拡張追加対応
	public static final String CMSCONTENTS_CATEGORY_ADVANCEINFO = '事前案内資料'; //販売促進資料追加対応


/*****************************************
　* 共通
　*****************************************/
    // アプリケーション切替ページパラメータ /apex/appSwitch?app=
    public static final String APPSWITCH_URL_PARAM_APP = 'app';

    // Cookie名 - アクティブメニュー
    public static final String COOKIE_ACTIVE_MENU = 'activeMenu';

    // ID管理.利用アプリ：NNLink
    public static final String APP_MODE_NNLINK = 'NNLink';
    // ID管理.利用アプリ：IRIS
    public static final String APP_MODE_IRIS   = 'IRIS';

    // 一覧ページ初期表示件数
    public static final Integer LIST_DEFAULT_ROWS = 20;
    // 「もっと見る」押下時追加件数
    public static final Integer LIST_ADD_MORE_ADD_ROWS = 20;
    //一覧ページ取得件数最大値
    public static final Integer LIST_MAX_ROWS = 1000;
    //一覧ページ取得件数最大値（集計）
    public static final Integer LIST_MAX_ROWS_AGGREGATE = 2000;
    //P16-0003 Atria対応開発
    //ダウンロード件数最大値
    public static final Integer LIST_DL_MAX_ROWS = 3000;
    
    public static Integer NN_TUBE_MOVIE_LENGTH_TITLE = 95;

    public static Integer NN_TUBE_MOVIE_LENGTH_DESCRIPTION = 101;

    // 拠点長分表示
    public static final String POLICY_RENDERING_TYPE_BY_MANAGER_ALL = 'all';
    // 本社営業分表示
    public static final String POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE = 'head_office';
    
    // エラーページ（E_ErrorPage）のフレーム切り替え用のURLパラメータ名
    public static final String ERR_URLPARAM_KEY = 'uif';
    public static final String ERR_URLPARAM_PIP = '1';
    
/*****************************************
　* 保険契約ヘッダ関連
　*****************************************/ 
    // 日付フォーマット
    public static final String DATEFROMAT_YYYYMM = 'yyyy/MM';
    // アスタリスク
    public static final String LABEL_ASTERISK = '*';
    // ハイフン
    public static final String LABEL_HYPHEN = '-';
    // データがない場合の文字
    public static final String LABEL_NONE_VALUE = '-';
    // データ更新中
    public static final String POLICY_STATUS_UPDATING = 'データ更新中';
    
    // 現在の状況コード
    public static final String POLICY_STATCODE_Y = 'Y';
    
    // 払込経路
    public static final String POLICY_ZECHNL_ACCOUNTTRAN = '口座振替';
    // 口座振替区分
    public static final String POLICY_ZBKPODIS_10 = '10';
    public static final String POLICY_ZBKPODIS_20 = '20';
    
    // 最終払込
    public static final String PCESDTE_WHOLE = '終身';
    
    
    // 個人保険特約・主契約フラグC
    public static final String COVPF_COLI_ZCRIND_C = 'C';
    public static final String COVPF_COLI_ZCRIND_C_LABEL = '主契約';
    public static final String COVPF_COLI_ZCRIND_R_LABEL = '特約';
    
    // 受取人種別
    public static Set<String> COLI_BNYTYPS = new Set<String>{
            E_Const.BNYTYP_01,E_Const.BNYTYP_02,E_Const.BNYTYP_03,E_Const.BNYTYP_04,E_Const.BNYTYP_05,E_Const.BNYTYP_06
            ,E_Const.BNYTYP_07,E_Const.BNYTYP_08,E_Const.BNYTYP_09,E_Const.BNYTYP_10,E_Const.BNYTYP_11
            ,E_Const.BNYTYP_40,E_Const.BNYTYP_50,E_Const.BNYTYP_51,E_Const.BNYTYP_53
    };    
    
    // 保険種類コード（数式）
    public static final String CRTABLE2_H1 = 'H1';
    public static final String CRTABLE2_H2 = 'H2';
    public static final String CRTABLE2_H3 = 'H3';
    public static final String CRTABLE2_H4 = 'H4';
    public static final String CRTABLE2_H5 = 'H5';
    public static final String CRTABLE2_H6 = 'H6';
    public static final String CRTABLE2_M1 = 'M1';
    public static final String CRTABLE2_M2 = 'M2';
    public static final String CRTABLE2_M5 = 'M5';
    public static final String CRTABLE2_M6 = 'M6';
    public static final String CRTABLE2_N5 = 'N5';
    public static final String CRTABLE2_N6 = 'N6';

    public static final String CRTABLE2_MW = 'MW';
    public static final String CRTABLE2_MX = 'MX';
    public static final String CRTABLE2_MY = 'MY';
    public static final String CRTABLE2_MZ = 'MZ';

    public static final String CRTABLE2_CC = 'CC';
    public static final String CRTABLE2_CD = 'CD';
    public static final String CRTABLE2_CE = 'CE';
    public static final String CRTABLE2_CG = 'CG';
    public static final String CRTABLE2_CH = 'CH';
    public static final String CRTABLE2_CJ = 'CJ';
    public static final String CRTABLE2_CM = 'CM';
    public static final String CRTABLE2_Y1 = 'Y1';
    public static final String CRTABLE2_Y2 = 'Y2';
    public static final String CRTABLE2_Y3 = 'Y3';
    public static final String CRTABLE2_Y4 = 'Y4';
    public static final String CRTABLE2_Y5 = 'Y5';
    public static final String CRTABLE2_Y6 = 'Y6';
    public static final String CRTABLE2_Z1 = 'Z1';
    public static final String CRTABLE2_Z2 = 'Z2';
    public static final String CRTABLE2_Z3 = 'Z3';
    public static final String CRTABLE2_Z4 = 'Z4';
    public static final String CRTABLE2_Z5 = 'Z5';
    public static final String CRTABLE2_Z6 = 'Z6';

    public static final String CRTABLE2_EN = 'EN';
    public static final String CRTABLE2_EF = 'EF';
    public static final String CRTABLE2_JA = 'JA';

    // 新医療保険とSPWH
    public static Set<String> CRTABLE2_NEW_SPWH = new Set<String>{
        CRTABLE2_H1,
        CRTABLE2_H2,
        CRTABLE2_H3,
        CRTABLE2_H4,
        CRTABLE2_H5,
        CRTABLE2_H6,
        CRTABLE2_M1,
        CRTABLE2_M2,
        CRTABLE2_M5,
        CRTABLE2_M6,
        CRTABLE2_N5,
        CRTABLE2_N6,

        CRTABLE2_MW,
        CRTABLE2_MX,
        CRTABLE2_MY,
        CRTABLE2_MZ
    };
    
    // 家族特約（子型）
    public static Set<String> CRTABLE2_FAML = new Set<String>{
        CRTABLE2_CC,
        CRTABLE2_CD,
        CRTABLE2_CE,
        CRTABLE2_CG,
        CRTABLE2_CH,
        CRTABLE2_CJ,
        CRTABLE2_CM,
        CRTABLE2_Y1,
        CRTABLE2_Y2,
        CRTABLE2_Y3,
        CRTABLE2_Y4,
        CRTABLE2_Y5,
        CRTABLE2_Y6,
        CRTABLE2_Z1,
        CRTABLE2_Z2,
        CRTABLE2_Z3,
        CRTABLE2_Z4,
        CRTABLE2_Z5,
        CRTABLE2_Z6
    };

    // 養老 , 年金付
    public static Set<String> CRTABLE2_OLDR = new Set<String>{
        CRTABLE2_EN,
        CRTABLE2_EF,
        CRTABLE2_JA
    };
    
    /* 契約者区分 */
    // 個人:P, 法人:C
    public static final String COLI_ZCNTRDSC_P = 'P';
    public static final String COLI_ZCNTRDSC_C = 'C';
    // 個人/法人
    public static Set<String> COLI_ZCNTRDSC = new Set<String>{
        COLI_ZCNTRDSC_P,
        COLI_ZCNTRDSC_C
    };
    // ラベル
    public static Map<String, String> COLI_ZCNTRDSC_LABEL = new Map<String, String>{
            COLI_ZCNTRDSC_P => '個人'
            ,COLI_ZCNTRDSC_C => '法人'
    };

/*****************************************
　* 保有契約
　*****************************************/
    // Cookie名 - 保有契約フィルタ
    public static final String COOKIE_POLICY_CONDITION = 'policyCondition';

    // フィルタ条件 - 営業部
    public static final String CONDITION_FIELD_UNIT = 'unit';
    // フィルタ条件 - MR
    public static final String CONDITION_FIELD_MR = 'mr';
    // フィルタ条件 - 代理店
    public static final String CONDITION_FIELD_AGENCY = 'agency';
    // フィルタ条件 - 事務所
    public static final String CONDITION_FIELD_OFFICE = 'office';
    // フィルタ条件 - 募集人
    public static final String CONDITION_FIELD_AGENT  = 'agent';

    // JSONキー - Field
    public static final String JSON_KEY_FIELD = 'field';
    // JSONキー - Name
    public static final String JSON_KEY_NAME  = 'name';
    // JSONキー - SFID
    public static final String JSON_KEY_SFID  = 'id';
    // JSONキー - BreadCrumb(パンくずリスト)
    public static final String JSON_KEY_BREADCRUMB = 'breadCrumb';

    //　複数データサマリ時の表示 
    public static final String OL_MULTI_DATA = '(複数)';
    // URLパラメータ名 - 表示行数
    public static final String LIST_URL_PARAM_DISPLAY_ROWS = 'row';
    // 契約者一覧SOQL LIMIT
    public static final String OL_SOQL_LIMIT = '2000';

/*****************************************
 * 失効契約
 *****************************************/
    //現在の状況コード
    public static final String LAPS_POLICY_STATCODE   = 'L';
    //現在の状況
    public static final String LAPS_POLICY_ZRSTDESC   = '失効中';


/*****************************************
 * 契約応当日
 *****************************************/
    //1～3カ月前
    public static final String LABEL_POLICY_TRHEEMONTH = '契約応当日が1～3ヶ月後';
    //4～6カ月前
    public static final String LABEL_POLICY_SIXMONTH = '契約応当日が4～6ヶ月後';
    //団体ページ被保険者一覧SOQL LIMIT
    public static final Integer LIST_MAX_ROWS_GRUPAGGREGATE = 20000;
    //団体一覧ページ、1ページあたりの表示件数
    public static final Integer LIST_DISPLAY_ROWS = 20;

/*****************************************
 * メニュー名
 *****************************************/
    public static final String IRIS_MENU_NAME_LIB_TOP = '資料・動画';
    public static final String IRIS_MENU_NAME_INFOMATION = 'お知らせ';
    public static final String IRIS_CONTENTS_NAME_SALESTOOL = '販売促進資料';         //CMSコンテンツレコードのセクション特定用レコードName値
    public static final String IRIS_CONTENTS_NAME_PAMPHLET = 'パンフレット等';
    public static final String IRIS_CONTENTS_NAME_INFO = 'お知らせ';            //CMSコンテンツレコードのセクション特定用レコードName値

/*****************************************
 * 検索
 *****************************************/
    // 社員_Contact検索時のLIMIT
    public static final Integer SEARCH_CONTACT_SOQL_LIMIT = 5000;
    // 検索カテゴリ
    public static final String SEARCH_CATEGORY_POLICY   = 'IRIS_PolicySearchBox';
    public static final String SEARCH_CATEGORY_AGENCY   = 'IRIS_AgencySearchBox';
    public static final String SEARCH_CATEGORY_DOCUMENT = 'IRIS_DocumentSearchBox';
    public static final String SEARCH_CATEGORY_CONTENT   = 'IRIS_ContentSearchBox';
    public static final String SEARCH_CATEGORY_NEWOPP   = 'IRIS_NewOppSearchBox';

    // 検索カテゴリ（ラベル）
    public static final String SEARCH_LABEL_CATEGORY_POLICY   = '顧客検索';
    public static final String SEARCH_LABEL_CATEGORY_AGENCY   = '代理店・募集人検索';
    public static final String SEARCH_LABEL_CATEGORY_DOCUMENT = 'サイト内情報検索';
    public static final String SEARCH_LABEL_CATEGORY_CONTENT   = 'コンテンツ検索';
    public static final String SEARCH_LABEL_CATEGORY_NEWOPP   = '新契約検索';

    // 検索項目
    public static final String SEARCH_TYPE_OWNER = 'owner';
    public static final String SEARCH_TYPE_INSURED = 'insured';
    public static final String SEARCH_TYPE_POLICY_NO = 'policyNo';
    public static final String SEARCH_TYPE_CUSTOMER_NO = 'customerNo';

    public static final String SEARCH_TYPE_AGENT = 'agent';
    public static final String SEARCH_TYPE_OFFICE = 'office';
    public static final String SEARCH_TYPE_AGENCY = 'agency';

    public static final String SEARCH_TYPE_DOCUMENT = 'document';
    public static final String SEARCH_TYPE_MOVIE = 'movie';

    public static final String SEARCH_TYPE_LABEL_OWNER = '契約者';
    public static final String SEARCH_TYPE_LABEL_INSURED = '被保険者';
    public static final String SEARCH_TYPE_LABELPOLICY_NO = '証券番号';
    public static final String SEARCH_TYPE_LABEL_CUSTOMER_NO = '顧客番号';

    public static final String SEARCH_TYPE_LABEL_AGENT = '募集人';
    public static final String SEARCH_TYPE_LABEL_OFFICE = '事務所';
    public static final String SEARCH_TYPE_LABEL_AGENCY = '代理店';

    // 顧客番号検索用
    public static final String CONTACT_OWNER_FLG_TRUE = '1';
    public static final String CONTACT_INSURED_FLG_TRUE = '1';
    public static final String CUSTOMER_TYPE_OWNER = '契約者';
    public static final String CUSTOMER_TYPE_INSURED = '被保険者';

    // 最近検索したキーワード表示数
    public static final Integer SEARCH_BOX_LOG_NUM = 20;

    // 各種メッセージのメッセージマスタkey
    public static final String MESSAGE_KEY_RECORD_NOT_FOUND = 'IRIS|SEARCH|001';
    public static final String MESSAGE_KEY_RECORD_OVER_LIMIT = 'IRIS|SEARCH|002';
    public static final String MESSAGE_KEY_KEYWORD_NOTHING = 'IRIS|SEARCH|003';

/*****************************************
 * ログ
 *****************************************/
    public static final String ACTION_TYPE_PAGE_ACCESS = 'ページ';
    public static final String ACTION_TYPE_REPORT_DOWNLOAD = '帳票ダウンロード';
    public static final String ACTION_TYPE_FILE_DOWNLOAD = 'ファイルダウンロード';
    public static final String ACTION_TYPE_SEARCH = '検索';
    public static final String ACTION_TYPE_READ_INFOMATION = 'お知らせ';
    public static final String ACTION_TYPE_DOWNLOAD = 'ダウンロード';
    public static final String ACTION_TYPE_FILTER_SETTING = 'フィルタ';
    public static final String ACTION_TYPE_MOVIE = '動画';
    public static final String ACTION_TYPE_PDF = 'PDF出力';
    public static final String ACTION_TYPE_TMT = 'たもつくん';
    // P16-0003 Atria対応開発
    public static final String ACTION_TYPE_ATRIA_SSO = 'AtriaSSO';
    public static final String ACTION_TYPE_AUTHORITY_CHANGE = '権限変更';

    public static final String ATTACHMENTURL = '/servlet/servlet.FileDownload?file=%record%';
    public static final String CONTENTURL = '/sfc/servlet.shepherd/version/download/%record%?aspdf=false';
    public static final String INFORMATIONURL = '/apex/IRIS_Information?infoid=%record%';

    //アクションタイプに「PDF出力」をセットする対象の帳票種別
    public static final Set<String> TYPEPDF = new Set<String>{
                                                    E_Const.DH_FORMNO_IRIS_COLI,
                                                    E_Const.DH_FORMNO_DISEASE,
                                                    E_Const.DH_FORMNO_SELFCOMPLIANCE
    };

/*****************************************
 * PushOut
 *****************************************/
    // 新契約ステータス：主募集人コード（5桁）
    public static String API_MAINAGENTNUM5 = 'MainAgentNum5__c';
    // 新契約ステータス：従募集人コード（5桁）
    public static String API_SUBAGENTNUM5 = 'SubAgentNum5__c';
 
    // 改行コード（ロングテキストエリア）
    public static String BREAK_LF = '\n'; 
    // 情報区切り用
    public static String BREAK_LINE = '--------------------------------';
    
    // PushOut通知バッチ　処理件数
    public static Integer BATCH_SIZE_PUSHOUT = 200;
 
    // EBiz連携ログ　区分
    public static String EBIZDATASYNC_KBN_PUSHOUT = '8'; 
    public static String EBIZDATASYNC_KBN_SALESRSLT_AGT = 'F'; 
    public static String EBIZDATASYNC_KBN_SALESRSLT_EP = 'F1'; 
    public static String EBIZDATASYNC_KBN_SALESPLN = 'F2'; 
 
    // EBiz連携ログ　通知区分
    public static String EBIZDATASYNC_KBN_AGPO = 'A';           // 保有契約異動通知
    public static String EBIZDATASYNC_KBN_BILS = 'B';           // 保険料請求通知
    public static String EBIZDATASYNC_KBN_BILA = 'C';           // 保険料請求予告通知
    public static String EBIZDATASYNC_KBN_NCOL = 'D';           // 保険料未入金通知
    public static String EBIZDATASYNC_KBN_NEWPOLICY = 'E';      // 新契約
    
    // 通知対象オブジェクトAPI
    public static String OBJ_API_AGPO = 'E_AGPO__c';            // 保有契約異動通知
    public static String OBJ_API_NCOL = 'E_NCOL__c';            // 保険料請求通知
    public static String OBJ_API_BILA = 'E_BILA__c';            // 保険料請求予告通知
    public static String OBJ_API_BILS = 'E_BILS__c';            // 保険料未入金通知
    public static String OBJ_API_NEWPOLICY = 'E_NewPolicy__c';  // 新契約

    // OperationクラスMap
    public static Map<String, String> PUSHOUT_OPERATION_MAP = new Map<String, String>{
                            EBIZDATASYNC_KBN_AGPO => 'I_PushOutBatchOperation_Notice'
                            ,EBIZDATASYNC_KBN_NCOL => 'I_PushOutBatchOperation_Notice'
                            ,EBIZDATASYNC_KBN_BILA => 'I_PushOutBatchOperation_Notice'
                            ,EBIZDATASYNC_KBN_BILS => 'I_PushOutBatchOperation_Notice'
                            ,EBIZDATASYNC_KBN_NEWPOLICY => 'I_PushOutBatchOperation_NewPolicy'  
    };
    
    // オブジェクト
    public static Map<String, String> PUSHOUT_OBJ_API_MAP = new Map<String, String>{
                            EBIZDATASYNC_KBN_AGPO => OBJ_API_AGPO
                            ,EBIZDATASYNC_KBN_NCOL => OBJ_API_NCOL
                            ,EBIZDATASYNC_KBN_BILA => OBJ_API_BILA
                            ,EBIZDATASYNC_KBN_BILS => OBJ_API_BILS  
                            ,EBIZDATASYNC_KBN_NEWPOLICY => OBJ_API_NEWPOLICY    
    };
    
    // 取得カラム
    public static Map<String, Set<String>> QUERY_SELECTS_MAP = new Map<String, Set<String>>{
                            EBIZDATASYNC_KBN_AGPO => new Set<String>{'PolicyContactId__c', 'PolicyAccountCode__c', 'PolicyOfficeCode__c', 'PolicyZ1OFFING__c', 'PolicyZAUTOTRF__c'}
                            ,EBIZDATASYNC_KBN_NCOL => new Set<String>{'PolicyContactId__c', 'PolicyAccountCode__c', 'PolicyOfficeCode__c', 'PolicyZ1OFFING__c', 'PolicyZAUTOTRF__c'}
                            ,EBIZDATASYNC_KBN_BILA => new Set<String>{'PolicyContactId__c', 'PolicyAccountCode__c', 'PolicyOfficeCode__c', 'PolicyZ1OFFING__c', 'PolicyZAUTOTRF__c'}
                            ,EBIZDATASYNC_KBN_BILS => new Set<String>{'PolicyContactId__c', 'PolicyAccountCode__c', 'PolicyOfficeCode__c', 'PolicyZ1OFFING__c', 'PolicyZAUTOTRF__c'}
                            ,EBIZDATASYNC_KBN_NEWPOLICY => new Set<String>{'Agent__r.Id', 'SubAgent__r.Id', 'Account__r.OwnerId', 'SubAccount__r.OwnerId'
                                                                            , 'Agent_Name__c', 'AgentAccountName__c' , 'MainAgentNum5__c', 'SubAgentNum5__c'
                                                                            , 'IsHqMR__c', 'IsHqSubMR__c', 'KFRAMEIND__c', 'COMM_ZCLNAME__c', 'COMM_ZINSNAM__c'
                                                                            , 'ContractorE_CLTPF_ZCLKNAME__c', 'InsuredE_CLTPF_ZCLKNAME__c'
                                                                            , 'CHDRNUM__c', 'GRUPKEY__c', 'InsuranceType__c', 'GroupKey__c'
                                                                            //外字対応
                                                                            , 'ContractorName__c', 'InsuredName__c', 'ContractorNameKana__c', 'InsuredNameKana__c'
                                                                            }
    };
    
    // 社員ユーザ　プロファイル定義
    public static Set<String> EMPLOYEE_PROFILE_NAMES = new Set<String>{'ＭＲ', '拠点長', '本社営業部MR', '本社営業部拠点長'};
    
    // Ebiz連携ログの通知区分と通知オブジェクトAPIのMap
    public static Map<String, String> TYPE_MAP = new Map<String, String>{
                            EBIZDATASYNC_KBN_AGPO => OBJ_API_AGPO
                            ,EBIZDATASYNC_KBN_NCOL => OBJ_API_NCOL
                            ,EBIZDATASYNC_KBN_BILA => OBJ_API_BILA
                            ,EBIZDATASYNC_KBN_BILS => OBJ_API_BILS
                            //,EBIZDATASYNC_KBN_NEWPOLICY => OBJ_API_NEWPOLICY
    };

    //ログバッチ
    // バッチ終了時刻
    public static Integer BATCH_END_HOUR = 17;

    //動画ログ
    public static String TUBE_PARAM_FROMDATETIME = 'fromdatetime';
    public static String TUBE_PARAM_TODATETIME = 'todatetime';
    public static String TUBE_PARAM_DIGEST = 'digest';
    public static String TUBE_PARAM_TERMFLG = 'termflg';
    public static String TUBE_DIGEST_ALGORITHM = 'SHA-256';
    public static String TUBE_RESULTCODE_SUCCESS = '0000';
    public static String TUBE_RESULTCODE_ERR_PARAM = '2601';
    public static String TUBE_RESULTCODE_ERR_JSON = '2602';
    public static String TUBE_RESULTCODE_ERR_DIGEST = '2603';
    public static String TUBE_RESULTCODE_ERR_SYSTEM = '2699';

    // P16-0003 Atria対応開発
/*****************************************
 * Atria
 *****************************************/
    /** SAMLアサーション 属性キー群（本人）**/
    // NNLinkID
    public static final String SAML_ATT_TAG_NNLINK_ID = 'nnlinkid';
    //ID種別
    public static final String SAML_ATT_TAG_ID_TYPE = 'zidtype';
    // 照会者コード
    public static final String SAML_ATT_TAG_INQUIRER_CODE = 'zinquirr';
    // ユーザ名
    public static final String SAML_ATT_TAG_USER_NAME = 'zclname';
    // 募集人コード
    public static final String SAML_ATT_TAG_AGENT_CODE = 'agntnum';
    // 所属代理店コード
    public static final String SAML_ATT_TAG_AGECY_CODE = 'zheaday';
    // 所属代理店名
    public static final String SAML_ATT_TAG_AGECY_NAME = 'zahname';
    // 所属代理店の一事務所登録フラグ
    public static final String SAML_ATT_TAG_AGECY_1OFFICE_FLG = 'z1offing';
    // 所属代理店資格コード
    public static final String SAML_ATT_TAG_AGECY_CLS_CODE = 'agcls';
    // 所属事務所コード
    public static final String SAML_ATT_TAG_OFFICE_CODE = 'zagcynum';
    // 所属事務所名
    public static final String SAML_ATT_TAG_OFFICE_NAME = 'zeaynam';
    // 所属事務所郵便番号
    public static final String SAML_ATT_TAG_OFFICE_POSTAL = 'cltpcode';
    // 所属事務所住所
    public static final String SAML_ATT_TAG_OFFICE_ADDRESS = 'zcladdr';
    // 所属事務所電話番号01
    public static final String SAML_ATT_TAG_OFFICE_PHONE1 = 'cltphone01';
    // 所属事務所電話番号02
    public static final String SAML_ATT_TAG_OFFICE_PHONE2 = 'cltphone02';
    // 所属事務所手数料カテゴリー
    public static final String SAML_ATT_TAG_OFFICE_COMM_CAT = 'rcmtab';
    // 担当MRコード
    public static final String SAML_ATT_TAG_MR_CODE = 'zmrcode';
    // 担当MR名
    public static final String SAML_ATT_TAG_MR_NAME = 'zmrname';
    // 所属部署（拠点、営業部）コード
    public static final String SAML_ATT_TAG_BRANCH_CODE = 'branch';
    // 所属部署（拠点、営業部）名
    public static final String SAML_ATT_TAG_BRANCH_NAME = 'zbranchname';
    // チャネル
    public static final String SAML_ATT_TAG_CHANNEL = 'kchanel';
    // 所属代理店金融機関コード
    public static final String SAML_ATT_TAG_AGENCY_BK_CODE = 'zbkcode';
    // MOFコード
    public static final String SAML_ATT_TAG_MOF_CODE = 'kmofcode';
    // 支店コード（金融機関支店）
    public static final String SAML_ATT_TAG_FI_BRANCH_CODE = 'zaysect';
    // 取扱者コード
    public static final String SAML_ATT_TAG_MEM_CODE = 'zmemnum';
    // 手数料試算権限
    public static final String SAML_ATT_TAG_ATR_COMM_FLG = 'atriaCommissionFlag';

    /** SAMLアサーション 属性キー群（別ユーザ閲覧）**/
    // 閲覧対象NNLinkID
    public static final String ALT_SAML_ATT_TAG_NNLINK_ID = 'alt_nnlinkid';
    // 閲覧対象ID種別
    public static final String ALT_SAML_ATT_TAG_ID_TYPE = 'alt_zidtype';
    // 閲覧対象照会者コード
    public static final String ALT_SAML_ATT_TAG_INQUIRER_CODE = 'alt_zinquirr';
    // 閲覧対象ユーザ名
    public static final String ALT_SAML_ATT_TAG_USER_NAME = 'alt_zclname';
    // 閲覧対象ユーザの募集人コード
    public static final String ALT_SAML_ATT_TAG_AGENT_CODE = 'alt_agntnum';
    // 閲覧対象ユーザの所属代理店コード
    public static final String ALT_SAML_ATT_TAG_AGECY_CODE = 'alt_zheaday';
    // 閲覧対象ユーザの所属代理店名
    public static final String ALT_SAML_ATT_TAG_AGECY_NAME = 'alt_zahname';
    // 閲覧対象ユーザの所属代理店の一事務所登録フラグ
    public static final String ALT_SAML_ATT_TAG_AGECY_1OFFICE_FLG = 'alt_z1offing';
    // 閲覧対象ユーザの所属代理店資格コード
    public static final String ALT_SAML_ATT_TAG_AGECY_CLS_CODE = 'alt_agcls';
    // 閲覧対象ユーザの所属事務所コード
    public static final String ALT_SAML_ATT_TAG_OFFICE_CODE = 'alt_zagcynum';
    // 閲覧対象ユーザの所属事務所名
    public static final String ALT_SAML_ATT_TAG_OFFICE_NAME = 'alt_zeaynam';
    // 閲覧対象ユーザの所属事務所郵便番号
    public static final String ALT_SAML_ATT_TAG_OFFICE_POSTAL = 'alt_cltpcode';
    // 閲覧対象ユーザの所属事務所住所
    public static final String ALT_SAML_ATT_TAG_OFFICE_ADDRESS = 'alt_zcladdr';
    // 閲覧対象ユーザの所属事務所電話番号01
    public static final String ALT_SAML_ATT_TAG_OFFICE_PHONE1 = 'alt_cltphone01';
    // 閲覧対象ユーザの所属事務所電話番号02
    public static final String ALT_SAML_ATT_TAG_OFFICE_PHONE2 = 'alt_cltphone02';
    // 閲覧対象ユーザの所属事務所手数料カテゴリー
    public static final String ALT_SAML_ATT_TAG_OFFICE_COMM_CAT = 'alt_rcmtab';
    // 閲覧対象ユーザの担当MRコード
    public static final String ALT_SAML_ATT_TAG_MR_CODE = 'alt_zmrcode';
    // 閲覧対象ユーザの担当MR名
    public static final String ALT_SAML_ATT_TAG_MR_NAME = 'alt_zmrname';
    // 閲覧対象ユーザの所属部署（拠点、営業部）コード
    public static final String ALT_SAML_ATT_TAG_BRANCH_CODE = 'alt_branch';
    // 閲覧対象ユーザの所属部署（拠点、営業部）名
    public static final String ALT_SAML_ATT_TAG_BRANCH_NAME = 'alt_zbranchname';
    // チャネル
    public static final String ALT_SAML_ATT_TAG_CHANNEL = 'alt_kchanel';
    // 所属代理店金融機関コード
    public static final String ALT_SAML_ATT_TAG_AGENCY_BK_CODE = 'alt_zbkcode';
    // MOFコード
    public static final String ALT_SAML_ATT_TAG_MOF_CODE = 'alt_kmofcode';
    // 支店コード（金融機関支店）
    public static final String ALT_SAML_ATT_TAG_FI_BRANCH_CODE = 'alt_zaysect';
    // 取扱者コード
    public static final String ALT_SAML_ATT_TAG_MEM_CODE = 'alt_zmemnum';
    // 手数料試算権限
    public static final String ALT_SAML_ATT_TAG_ATR_COMM_FLG = 'alt_atriaCommissionFlag';

/*****************************************
 * パンフレット等発送依頼
 *****************************************/
    //コネワン上の業推部署コード
    public static final String CONEONE_OFFICE_CODE = '0000000872';
    //ページ・機能名
    public static final String DOCUMENT_REQUEST = 'パンフレット等発送依頼';
    //取得お知らせラベル
    public static final Set<String> DR_INFO_LABEL = new Set<String>{'パンフレット等'};
    //セクション名
    public static final String IRIS_PARENTCONTENT_NAME_PAMPHLET = 'パンフレット／特に重要なお知らせ';
    public static final String IRIS_PARENTCONTENT_NAME_AGREEMENT = 'ご契約のしおり・約款';
    public static final String IRIS_COMPANY_GUIDANCE = '会社案内';
    //資料カテゴリ (表示順で記載する)
    public static final Set<String> DR_DOCUMENT_CATEGORIES = new Set<String>{I_Const.IRIS_PARENTCONTENT_NAME_PAMPHLET, I_Const.IRIS_PARENTCONTENT_NAME_AGREEMENT, I_Const.IRIS_COMPANY_GUIDANCE};
    //資料カテゴリ 英名
    public static final MAP<String, String> DR_DOCUMENT_CATEGORIE_MAP = new MAP<String, String>{
         I_Const.IRIS_PARENTCONTENT_NAME_PAMPHLET => 'pamphlet'
        ,I_Const.IRIS_PARENTCONTENT_NAME_AGREEMENT => 'Payment'
        ,I_Const.IRIS_COMPANY_GUIDANCE => 'CompanyProfile'
    };
    //パンフレット等発送依頼URL
    public static final String DOCUMENTREQUESTURL = '/apex/IRIS_DocumentRequest?irismn=';
    // 各種メッセージのメッセージマスタkey
    public static final String MESSAGE_KEY_NO_AGENCYNAME = 'DRE|001';
    public static final String MESSAGE_KEY_SIZEOVER_AGENCYNAME = 'DRE|002';
    public static final String MESSAGE_KEY_EM_AGENCYNAME = 'DRE|013';
    public static final String MESSAGE_KEY_SIZEOVER_AGENTNAME = 'DRE|003';
    public static final String MESSAGE_KEY_EM_AGENTNAME = 'DRE|014';
    public static final String MESSAGE_KEY_NO_POSTALCODE = 'DRE|004';
    public static final String MESSAGE_KEY_WRONG_POSTALCODE = 'DRE|018';
    public static final String MESSAGE_KEY_SIZEOVER_POSTALCODE = 'DRE|005';
    public static final String MESSAGE_KEY_NO_ADDRESS = 'DRE|006';
    public static final String MESSAGE_KEY_SIZEOVER_ADDRESS = 'DRE|007';
    public static final String MESSAGE_KEY_EM_ADDRESS = 'DRE|008';
    public static final String MESSAGE_KEY_NO_PHONENO = 'DRE|009';
    public static final String MESSAGE_KEY_SIZEOVER_PHONENO = 'DRE|010';
    public static final String MESSAGE_KEY_WRONG_PHONENO = 'DRE|016';
    public static final String MESSAGE_KEY_NO_REQUEST = 'DRE|011';
    public static final String MESSAGE_KEY_REQUEST_ERROR = 'DRE|012';
    public static final String MESSAGE_KEY_NO_AGENT = 'DRE|015';
    public static final String MESSAGE_KEY_WRONG_SYMBOL = 'DRE|017';

    // 資料・動画 セクション
    public static final String IRIS_PARENTCONTENT_NAME_EVOCATION = 'ニード喚起資料等';
    public static final Set<String> PAMPHLET_CATEGORIES = new Set<String>{ I_Const.IRIS_PARENTCONTENT_NAME_PAMPHLET, I_Const.IRIS_PARENTCONTENT_NAME_AGREEMENT, I_Const.IRIS_PARENTCONTENT_NAME_EVOCATION };
    public static final String IRIS_PARENTCONTENT_NAME_PAMPHLETANCHOR = 'パンフレット等アンカー';
    public static final Set<String> PAMPHLET_CONTENTS = new Set<String>{ I_Const.IRIS_PARENTCONTENT_NAME_PAMPHLETANCHOR, I_Const.IRIS_PARENTCONTENT_NAME_PAMPHLET, I_Const.IRIS_PARENTCONTENT_NAME_AGREEMENT, I_Const.IRIS_PARENTCONTENT_NAME_EVOCATION };
}