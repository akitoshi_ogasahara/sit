global with sharing class SC_Search_agSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public SC_Office__c record{get;set;}	
			
	public SC_Search_agExtender getExtender() {return (SC_Search_agExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component51_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component51_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component51_op{get;set;}	
		public List<SelectOption> valueOptions_SC_Office_c_FiscalYear_c_multi {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component18_val {get;set;}	
		public SkyEditor2.TextHolder Component18_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component20_val {get;set;}	
		public SkyEditor2.TextHolder Component20_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component48_val {get;set;}	
		public SkyEditor2.TextHolder Component48_op{get;set;}	
			
	public String recordTypeRecordsJSON_SC_Office_c {get; private set;}
	public String defaultRecordTypeId_SC_Office_c {get; private set;}
	public String metadataJSON_SC_Office_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public SC_Search_agSVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = SC_Office__c.fields.FiscalYear__c;
		f = SC_Office__c.fields.IsActiveOffice__c;
		f = SC_Office__c.fields.ZEAYNAM__c;
		f = SC_Office__c.fields.ZAGCYNUM__c;
		f = SC_Office__c.fields.ToView__c;
		f = SC_Office__c.fields.Status__c;
		f = SC_Office__c.fields.BusinessStatus__c;
		f = SC_Office__c.fields.OwnerMRUnitName__c;
		f = SC_Office__c.fields.OwnerMRCode__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = SC_Office__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component51_val = new SkyEditor2__SkyEditorDummy__c();	
				Component51_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component51_op = new SkyEditor2.TextHolder('inx');	
				valueOptions_SC_Office_c_FiscalYear_c_multi = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : SC_Office__c.FiscalYear__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_SC_Office_c_FiscalYear_c_multi.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component18_val = new SkyEditor2__SkyEditorDummy__c();	
				Component18_op = new SkyEditor2.TextHolder('co');	
					
				Component20_val = new SkyEditor2__SkyEditorDummy__c();	
				Component20_op = new SkyEditor2.TextHolder('eq');	
					
				Component48_val = new SkyEditor2__SkyEditorDummy__c();	
				Component48_op = new SkyEditor2.TextHolder('eq');	
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('SC_Office__c')
						.addFieldAsOutput('ToView__c')
						.addFieldAsOutput('FiscalYear__c')
						.addFieldAsOutput('ZEAYNAM__c')
						.addFieldAsOutput('ZAGCYNUM__c')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component51_val_dummy, 'SkyEditor2__Text__c','FiscalYear__c', Component51_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component18_val, 'SkyEditor2__Text__c', 'ZEAYNAM__c', Component18_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component20_val, 'SkyEditor2__Text__c', 'ZAGCYNUM__c', Component20_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component48_val, 'SkyEditor2__Checkbox__c', 'IsActiveOffice__c', Component48_op, true, 0, true ))
.addSort('Status__c',True,False).addSort('BusinessStatus__c',True,False).addSort('OwnerMRUnitName__c',True,False).addSort('OwnerMRCode__c',True,False).addSort('ZAGCYNUM__c',True,False)
				);	
					
					resultTable = new resultTable(new List<SC_Office__c>(), new List<resultTableItem>(), new List<SC_Office__c>(), null);
				resultTable.ignoredOnSave = true;
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(SC_Office__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new SC_Search_agExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_SC_Office_c_FiscalYear_c_multi() { 
			return SkyEditor2.WhereOperatorOptions.getOperatorOptionsSelectIn(); 
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_ZEAYNAM_c() { 
			return getOperatorOptions('SC_Office__c', 'ZEAYNAM__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_ZAGCYNUM_c() { 
			return getOperatorOptions('SC_Office__c', 'ZAGCYNUM__c');	
		}	
		public List<SelectOption> getOperatorOptions_SC_Office_c_IsActiveOffice_c() { 
			return getOperatorOptions('SC_Office__c', 'IsActiveOffice__c');	
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public SC_Office__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, SC_Office__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (SC_Office__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}