/**
 * 
 */
@isTest
private class TestE_UPPerChange006Extension {

    // testData
    private static E_UPBPF__c upbpf;
    private static List<E_UPFPF__c> upfpfList;
    
    /**
     * init006 
     * インデックスが存在する
     */
    static testMethod void init01() {
        // create Data
        createData();
        
        // Set test page
        Pagereference pref = Page.up003;
        pref.getParameters().put('id',upbpf.Id);
        Test.setCurrentPage(pref);
        
        Test.starttest();
        
        // StandardController
        E_UPTerm003Controller controller = new E_UPTerm003Controller();
        controller.init();
        controller.freeSelectStartDate = '2015/02/03';
        controller.doTransition006();
        controller.dlCondi.ZSFUNDCD.add(upfpfList[0].Id);
        controller.dlCondi.ZSFUNDCD.add(upfpfList[1].Id);
        
        // Extension
        E_UPPerChange006Extension extension = new E_UPPerChange006Extension(controller);
        Pagereference resultPref = extension.init006();
        
        Test.stoptest();

        system.assert(extension.isSpva);
        system.assertEquals(upbpf.Id, extension.getUpbpf().Id);     // ファンド基本情報
        system.assert(!extension.getIsVariableAnnuity());           // バリアブルアニュイティ判定
        system.assertEquals('2015/02/03', extension.getFromDate()); // 開始日の日付
        system.assertEquals('2015/02/09', extension.getToDate());   // 終了の日付

        Set<ID> ids = new Set<ID>();
        for(E_UPFPF__c u : upfpfList){
            ids.add(u.Id);
        }
        Map<Id, E_UPFPF__c> upfpfMap = 
                    new Map<Id, E_UPFPF__c>([Select Id, ZIVTNAMEUnion__c, ZFNDNAMEUnion__c From E_UPFPF__c Where Id In :ids]);
        // 騰落率：1件目
        E_UPFPF__c upfpf1 = upfpfMap.get(upfpfList[0].Id);
        system.assertEquals(upfpf1.ZIVTNAMEUnion__c, extension.perChangeList[0].fundName);
        system.assertEquals('-', extension.perChangeList[0].fromPer);
        system.assertEquals('109.00', extension.perChangeList[0].toPer);
        
        // 騰落率：2件目
        E_UPFPF__c upfpf2 = upfpfMap.get(upfpfList[1].Id);
        system.assertEquals(upfpf2.ZIVTNAMEUnion__c, extension.perChangeList[1].fundName);
        system.assertEquals('-', extension.perChangeList[1].fromPer);
        system.assertEquals('-', extension.perChangeList[1].toPer);
    }
    
    /**
     * init006 
     * インデックスが存在しない
     */
    static testMethod void init02() {
        // create Data
        createData();
        
        // Set test page
        Pagereference pref = Page.up003;
        pref.getParameters().put('id',upbpf.Id);
        Test.setCurrentPage(pref);
        
        Test.starttest();
        
        // StandardController
        E_UPTerm003Controller controller = new E_UPTerm003Controller();
        controller.init();
        controller.doTransition006();
        controller.dlCondi.ZSFUNDCD.add(upfpfList[1].Id);
        
        // Extension
        E_UPPerChange006Extension extension = new E_UPPerChange006Extension(controller);
        Pagereference resultPref = extension.init006();
        
        Test.stoptest();
        
        String resultUrl = resultPref.getUrl().toLowerCase();
        system.assert(resultUrl.indexOf(Page.up999.getUrl()) > -1);

    }
    
    // testDataCreate
    static void createData(){
        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        
        // ファンド群マスタ(E_FundMaster__c)
        E_FundMaster__c fundMaster = new E_FundMaster__c(
            CURRFROM__c = 0,
            CURRTO__c = 99999999,
            ZGFUNDCD__c = 'G04',
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZGRPFND01__c = 'アイエヌジーマイプラン年金',
            ZGRPFND02__c = '',
            ZGRPFND03__c = ''
        );
        insert fundMaster;
        
        // 特別勘定(E_SVCPF__c)
        E_SVCPF__c svcpf  = new E_SVCPF__c(
            ZFSDFROM__c = '20150101',
            ZFSHFROM__c = '10',
            ZFSMFROM__c = '10',
            ZFSDTO__c = '99999999',
            ZFSHTO__c = '99',
            ZFSMTO__c = '99',
            ZEFUNDCD__c = 'A052',
            ZCATFLAG__c = '20',
            ZFUNDNAM01__c = '世界債券型（TS1）',
            ZFUNDNAM02__c = 'ＶＡ外国債券ファンド',
            ZFUNDNAM03__c = '（適格機関投資家専用）',
            ZFUNDNAM04__c = '',
            CURRFROM__c = '20150101',
            CURRTO__c = '99999999'
        );
        insert svcpf;
        
        // 選択可能ファンド(E_SFGPF__c)
        E_SFGPF__c sfgpf = new E_SFGPF__c(
            FundMaster__c = fundMaster.Id,
            E_SVCPF__c = svcpf.Id,
            E_SFGPFCode__c = 'G04|A052'
        );
        insert sfgpf;
        
        // ユニットプライス基本情報(E_UPBPF__c)
        upbpf = new E_UPBPF__c(
            E_FundMaster__c = fundMaster.Id,
            ZPETNAME__c = 'アイエヌジーマイプラン年金',
            ZGFUNDCD__c = 'G04',
            ZGFUNDNM__c = '（特別勘定群 TS型）',
            ZIDXDATE01__c = '20150116',
            ZIDXDATE02__c = '20150115',
            ZIDXDATE03__c = '20150114',
            ZIDXDATE04__c = '20150113',
            ZIDXDATE05__c = '20150112',
            ZPRDCD__c = 'S0013',
            ZQLFLG__c = true,
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '080000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        );
        insert upbpf;
        
        // ユニットプライス表示用ファンド(E_UPFPF__c)
        upfpfList = new List<E_UPFPF__c>();
        upfpfList.add(new E_UPFPF__c(
            SVCPF__c = svcpf.Id,
            ZFUNDFLG__c = false,
            ZSFUNDCD__c = 'A052',
            ZFNDNAME01__c = '',
            ZFNDNAME02__c = '',
            ZIVTNAME01__c = '世界債券型',
            ZIVTNAME02__c = '（TS1）',
            ZIVTNAME03__c = 'ＶＡ外国債券ファンド',
            ZIVTNAME04__c = '（適格機関投資家専用1）',
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '000000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        ));
        upfpfList.add(new E_UPFPF__c(
            SVCPF__c = svcpf.Id,
            ZFUNDFLG__c = true,
            ZSFUNDCD__c = 'A051',
            ZFNDNAME01__c = '',
            ZFNDNAME02__c = '',
            ZIVTNAME01__c = '日本債券型',
            ZIVTNAME02__c = '（TS2）',
            ZIVTNAME03__c = 'ＶＡ国債券ファンド',
            ZIVTNAME04__c = '（適格機関投資家専用2）',
            ZDSPFLAG__c = '1',
            ZSTDTFRM__c = '00000000',
            ZSTTMFRM__c = '000000',
            ZSTDTTO__c = '99999999',
            ZSTTMTO__c = '999999'
        ));
        insert upfpfList;
        
        // ユニットプライス(E_UPXPF__c)
        List<E_UPXPF__c> upxpfList = new List<E_UPXPF__c>();
        for(Integer i = 0; i < 9; i++){
            //2015/02/03 は作成しない
            if(i != 2){
                E_UPXPF__c upxpf = new E_UPXPF__c(
                    E_UPFPF__c = upfpfList[0].Id,
                    ZSFUNDCD__c = 'A052',
                    ZVINDEX__c = 100.000000000 + (i + 1),
                    ZFUNDDTE__c = '2015020' + String.valueOf(i + 1)
                );
                upxpfList.add(upxpf);
            }
        }
        insert upxpfList;
    }
}