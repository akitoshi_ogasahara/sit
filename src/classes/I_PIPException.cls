public class I_PIPException extends Exception {
	public String detailMessage{get;private set;}
	//コンストラクタ
	public I_PIPException(String inMessage, String inDetailMessage) {
		setMessage(inMessage);
		detailMessage = inDetailMessage;
	}
}