/**
 *    代理店規制強化対応　リソース  superクラス
 */
public abstract class CR_AbstractExtender extends E_AbstractExtender{
    
    public class CR_SVEException extends Exception{}
    
    //初期処理チェック
    @TestVisible
    protected Boolean isSuccessOfInitValidate{get;set;}
    public Boolean getIsSuccessInit(){
        return isSuccessOfInitValidate;
    }

    /**
     *  Constructor
     */
    public CR_AbstractExtender() {
        access = E_AccessController.getInstance();
        isSuccessOfInitValidate = false;
    }

    /**
     *  代理店規定管理　権限判断
     */
    //承認権限 
    public Boolean getHasApprovalPermission(){
        //権限セットの承認権限　CR_PermissionSet_Approvalを持っている場合にTrueとする 
        return access.hasCRApprovalPermission();
    }
    //外部委託参照権限
    public Boolean getHasOutsrcViewPermission(){
        return access.hasCROutsrcViewPermission();
    }
    
    //DPA権限
    public Boolean getHasDpaPermission(){
        return access.hasCRDpaPermission();
    }
    //事業報告書権限
    public Boolean getHasReportsPermission(){
        return access.hasCRReportsPermission();
    }
    //帳簿書類権限
    public Boolean getHasBooksPermission(){
        return access.hasCRBooksPermission();
    }
    
    //AY権限ユーザか？
    public Boolean isAuthAY{
    	get{
    		return access.isAuthAY();
    	}
    	set;
    }
    
    /**
     *      ヘルプページURL
     */
    @TestVisible
    protected String helpMenukey{get;set;}
    @TestVisible
    protected String helpPageJumpTo{get;set;}
    public String getUrlforHelp(){
        String url = 'E_Info?';
        url += 'menu='+ helpMenukey;
        if(String.isNotBlank(helpPageJumpTo)){
            url += '#' + helpPageJumpTo;
        }
        return url;
    }

    /**
     *      代理店格コード
     *          検索ページを除く各ページで設定される。
     */
    @TestVisible
    protected String viewingAgencyCode{get{
                                        if(String.isBlank(viewingAgencyCode)){
                                            return access.getAccountCode();
                                        }
                                        return viewingAgencyCode;
                                }
                                set;}

    /**
     *      ページ遷移
     *          代理店コードから外部委託/規定管理のUrlを作成する
     */
     @TestVisible
    private static Map<String,CR_Agency__c> cr_Agencys;
    //外部委託一覧へのURL
    public String getUrlforOutsrcs(){
        //外部委託の参照権限を持っていない場合Null
        if(getHasOutsrcViewPermission()==false){
            return null;
        }
        
        if(cr_Agencys==null){
            cr_Agencys = CR_AgencyDao.getCR_AgencysByAgencyCD(viewingAgencyCode);
        }
        CR_Agency__c ag = cr_Agencys.get(CR_Const.RECTYPE_DEVNAME_OUTSRC);
        if(ag!=null){
            PageReference pr = Page.E_CROutSrcs;
            pr.getParameters().put('id', ag.Id);
            return pr.getUrl();
        }
        return null;
    }
    //規定管理一覧へのURL
    public String getUrlforRules(){
        if(cr_Agencys==null){
            cr_Agencys = CR_AgencyDao.getCR_AgencysByAgencyCD(viewingAgencyCode);
        }
        CR_Agency__c ag = cr_Agencys.get(CR_Const.RECTYPE_DEVNAME_RULE);
        if(ag!=null){
            PageReference pr = Page.E_CRRules;
            pr.getParameters().put('id', ag.Id);
            return pr.getUrl();
        }
        return null;
    
    }
    //事業報告書へのURL
    public String getUrlforReports(){      
        if(cr_Agencys==null){
            cr_Agencys = CR_AgencyDao.getCR_AgencysByAgencyCD(viewingAgencyCode);
        }
        CR_Agency__c ag = cr_Agencys.get(CR_Const.RECTYPE_DEVNAME_BIZ);
        if(ag!=null){
            PageReference pr = Page.E_CRBizReports;
            pr.getParameters().put('id', ag.Id);
            return pr.getUrl();
        }
        return null;
    }
    //帳簿書類へのURL
    public String getUrlforBooks(){
        if(cr_Agencys==null){
            cr_Agencys = CR_AgencyDao.getCR_AgencysByAgencyCD(viewingAgencyCode);
        }
        CR_Agency__c ag = cr_Agencys.get(CR_Const.RECTYPE_DEVNAME_BIZ);
        if(ag!=null){
            PageReference pr = Page.E_CRBizBooks;
            pr.getParameters().put('id', ag.Id);
            return pr.getUrl();
        }
        return null;
    }
    
    //代理店検索（外部委託）
    public String getUrlforSearchOS(){
        //社員以外または外部委託の参照権限を持っていない場合Null
        if(getHasOutsrcViewPermission()==false||access.isEmployee()==false){
            return null;
        }
        return Page.E_CRAgencySearchOS.getUrl();
    }

    //代理店検索（管理規定）
    public String getUrlforSearch(){
        //社員以外の場合Null
        if(access.isEmployee()==false){
            return null;
        }
        return Page.E_CRAgencySearch.getUrl();
    }
    
    //代理店検索（事業報告書）
    public String getUrlforSearchBizReports(){
        //社員以外の場合Null
        if(access.isEmployee()==false){
            return null;
        }
        PageReference pr = Page.E_CRAgencySearchBiz;
        pr.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZREPORTS);
        return pr.getUrl();
    }
    
    //代理店検索（帳簿書類）
    public String getUrlforSearchBizBooks(){
        //社員以外の場合Null
        if(access.isEmployee()==false){
            return null;
        }
        PageReference pr = Page.E_CRAgencySearchBiz;
        pr.getParameters().put(CR_Const.URL_PARAM_TYPE_BIZ, CR_Const.TYPE_BIZBOOKS);
        return pr.getUrl();
    }

}