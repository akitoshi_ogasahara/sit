/**
 * 
 */
@isTest
public with sharing class TestE_SPSPFEmailTrigger {

	static testMethod void myUnitTest() {
		Test.startTest();
		E_SPSPFEmail__c sps1 = new E_SPSPFEmail__c();
		insert sps1;
		E_SPSPFEmail__c sps2 = new E_SPSPFEmail__c();
		List<E_SPSPFEmail__c> spsList = new List<E_SPSPFEmail__c>{sps1, sps2};
		upsert spsList;
		Test.stopTest();
		
		E_SPSPFEmail__c result = [select MailNunmer_numOnly__c From E_SPSPFEmail__c where Id = :sps2.Id];
		System.assertEquals(2, result.MailNunmer_numOnly__c);
		
	}
}