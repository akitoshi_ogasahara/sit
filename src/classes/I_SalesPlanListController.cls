public with sharing class I_SalesPlanListController {
    // 一覧表示用の内部クラス
    public SalesPlanAttachment spAtt{get; set;}
    // 営業計画ID
    public String salesPlanId{get; set;}
    // 挙績情報ID
    public String sResultsId{get; set;}
    // 代理店格ID
    public String parentAccId{get; set;}
    // 営業計画リスト
    public List<SalesPlanAttachment> salesPlanList{
        get{
            if(this.salesPlanList != null && !this.salesPlanList.isEmpty()){
                return this.salesPlanList;
            }
            return getSalesPlanAttachment();
        } private set;}

    // リスト行数
    public Integer rowCount {get; set;}

    /**
     * コンストラクタ
     */
    public I_SalesPlanListController() {
        this.salesPlanId = '';
        this.salesPlanList = new List<SalesPlanAttachment>();
        this.rowCount = 5;
    }


    // リスト表示制御
    public Boolean getIsDisp(){
        if(this.salesPlanList.size() > 0){
            return true;
        }
        return false;
    }

    // 一覧表示用リスト生成
    private List<SalesPlanAttachment> getSalesPlanAttachment(){
        List<SalesPlanAttachment> spAttList = new List<SalesPlanAttachment>();
        List<Attachment> attList = new List<Attachment>();
        E_SalesPlan__c sPlan = getSPlanRecById(this.parentAccId);
        if(sPlan != null){
            this.salesPlanId = sPlan.Id;
            attList = getAttByParentId(this.salesPlanId);
            Map<Id, User> userMap = new Map<Id, User>();
            for(Attachment att: attList){
                userMap.put(att.CreatedBy.Id, new User());
            }
            userMap = new Map<Id, User>(E_UserDaoWithout.getUserRecByUserId(userMap.keySet()));

            for(Attachment att :attList){
                spAtt = new SalesPlanAttachment(att);
                if(userMap.containsKey(att.CreatedBy.Id)){
                    User usr = userMap.get(att.CreatedBy.Id);
                    if(usr.UserType.equals(E_Const.USERTYPE_STANDARD)){
                        spAtt.createdBelong = 'エヌエヌ生命保険(株)'; 
                    }else{
                        spAtt.createdBelong = usr.Contact.E_AccParentName__c;
                    }
                }
                spAttList.add(spAtt);
            }
        }
        return spAttList;
    }

    // 営業計画書画面へ遷移
    public PageReference inputGoal(){
        if(String.isBlank(salesPlanId)){
            E_SalesPlan__c sPlan = new E_SalesPlan__c(SalesResult__c = this.sResultsId, ParentAccount__c = this.parentAccId);
            // AH権限でinsertをかけるため、withoutで対応する
            E_SalesPlanDaoWithout.doInsert(sPlan);
            this.salesPlanId = sPlan.Id;
        }
        PageReference salesplan = Page.IRIS_SalesPlan;
        salesplan.getParameters().put('id', this.salesPlanId);
        return salesplan;
    }

    // PDF表示
    public PageReference openPDF(){
        //AttachmentID取得
        String attachmentId = System.currentPageReference().getParameters().get('attId');
        PageReference attPDF = new PageReference('/servlet/servlet.FileDownload?file=' + attachmentId);
        return attPDF;
    }

    // PDF削除
    public PageReference deletePDF(){
        //ParentID(営業計画書ID)取得
        String deleteId = System.currentPageReference().getParameters().get('deleteId');
        Attachment att = [SELECT Id
                        FROM Attachment
                        WHERE Id = :deleteId];
        E_AttachmentDaoWithout.doDelete(att);
        this.salesPlanList = new List<SalesPlanAttachment>();
        return null;
    }


    /**
     * 表示行数追加
     */
    public PageReference addRows(){
        rowCount += 5;
        if(rowCount > I_Const.LIST_MAX_ROWS){
            rowCount = I_Const.LIST_MAX_ROWS;
        }
        return null;
    }
    //総行数取得
    public Integer getTotalRows(){
        Integer totalRow = 0;

        totalRow = this.salesPlanList  == null ? 0 : this.salesPlanList.size();
        return totalRow;
    }
    //MAX表示件数
    public Integer getListMaxRows(){
        return I_Const.LIST_MAX_ROWS;
    }

    /**
     * 代理店挙積情報をキーに、指定した営業計画を取得する。
     * 使用箇所
     *  
     * @param : parentAccId: 代理店格ID
     * @return E_SalesPlan__c: 営業計画
     */
     private E_SalesPlan__c getSPlanRecById(Id parentAccId) {
        List<E_SalesPlan__c> sp =
            [SELECT
                Id                                      // カスタムオブジェクト ID
                ,ParentAccount__c
            FROM
                E_SalesPlan__c
            WHERE
                ParentAccount__c = :parentAccId
           ];
           if(sp.size() == 1){
                return sp[0];
           }
        return null;
    }
    /**
     * 親IDをキーに、指定したファイルを取得する。
     * 使用箇所
     *  
     * @param parentId: 親オブジェクト
     * @return Attachment: ファイル
     */
     private List<Attachment> getAttByParentId(Id parentId){
        List<Attachment> attList =
            [Select
                Id                                      // オブジェクト ID
                ,ParentId                               // 親ID
                ,Createddate                            // 作成日
                ,Name                                   // タイトル
                ,CreatedBy.Name                         // 作成者
                ,CreatedBy.Id                           // 作成者ID
            From
                Attachment
            Where
                ParentId = :parentId
            Order By
                Createddate Desc
           ];
        return attList;
     }

    

    // 表示する情報
    public class SalesPlanAttachment{
        // 作成日
        public String createDate {get; set;}
        // タイトル
        public String title {get; set;}
        // 作成者
        public String createdName {get; set;}
        // 作成者所属先
        public String createdBelong {get; set;}
        //　遷移先ID
        public String attachmentId {get; set;}
        //　遷移先ID
        public String attParentId {get; set;}

        /**
         *  ファイル
         * @param Attachment: Attachment
         */
        public SalesPlanAttachment(Attachment att){
            Datetime cDate = (Datetime) att.get('CreatedDate');
            this.createDate = getDateStr(cDate);
            this.title = att.Name;
            this.createdName = att.CreatedBy.Name;
            this.attachmentId = att.Id;
            this.attParentId = att.ParentId;

        }
        /**
         * コンストラクタ
         */
        public SalesPlanAttachment(){
        }

        //　日付表記変換
        private String getDateStr(Datetime cDate){
            return cDate.YEAR() + '年' +cDate.MONTH()+ '月' +cDate.DAY()+ '日';
        }
    }
}