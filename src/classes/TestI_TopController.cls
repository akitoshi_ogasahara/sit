@isTest
private class TestI_TopController {
	//PwC
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];

	//--- initメソッド ---
	static testMethod void initTest(){
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');

		System.runAs(actUser){
			I_TopController instance = new I_TopController();
			PageReference ret = instance.pageAction();
			System.assertNotEquals(null, ret);
		}
	}
	// 社員の場合は強制的に IRIS_Top を表示 // 利用アプリを IRIS に更新する
	static testMethod void initTest_returnNull(){
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			TestI_TestUtil.createEmployeePermissions(actUser.Id);
			 //E_IRISHandler
			E_IRISHandler iris = E_IRISHandler.getInstance();
			iris.setPolicyCondition('agency', '代理店名', 'agency', 'breadCrumb');

			I_TopController instance = new I_TopController();
			PageReference ret = instance.pageAction();
			//System.assertEquals(ret.getUrl(),'/apex/e_appswitch?app=NNLink');
			System.assertEquals(ret,null);       //
		}
	}



	//--- getTopMenusメソッド ---
	static testMethod void getTopMenusTest(){
		I_TopController instance = new I_TopController();
		Map<String, I_MenuItem> ret = instance.getTopMenus();
		System.assertNotEquals(null, ret);
	}


	//--- getRECORD_SIZE_SOQL_FOR_RESTAPI_from1メソッド ---
	/*static testMethod void getRECORD_SIZE_SOQL_FOR_RESTAPI_from1Test(){
		I_TopController instance = new I_TopController();
		String actual = instance.getRECORD_SIZE_SOQL_FOR_RESTAPI_from1();
		String expected = I_PolicysController.getRECORD_SIZE_SOQL_FOR_RESTAPI(1);
		System.assertEquals(expected, actual);
	}


	//--- getRECORD_SIZE_SOQL_FOR_RESTAPI_from4メソッド ---
	static testMethod void getRECORD_SIZE_SOQL_FOR_RESTAPI_from4Test(){
		I_TopController instance = new I_TopController();
		String actual = instance.getRECORD_SIZE_SOQL_FOR_RESTAPI_from4();
		String expected = I_PolicysController.getRECORD_SIZE_SOQL_FOR_RESTAPI(4);
		System.assertEquals(expected, actual);
	}*/
	static testMethod void forCoverage(){
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		System.runAs(actUser){
			TestI_TestUtil.createEmployeePermissions(actUser.Id);
			 //E_IRISHandler
			E_IRISHandler iris = E_IRISHandler.getInstance();
			iris.setPolicyCondition('agency', '代理店名', 'agency', 'breadCrumb');

			I_TopController controller = new I_TopController();
			PageReference ret = controller.pageAction();
			I_MenuProvider.LibSubNavItemsHolder item = controller.libSubNavItems;

			System.assertEquals(true, controller.getIsEmployee());
		}
	}


		//getTubeConnectHelperのテスト
	@isTest static void getBannerContentsTest() {
		//I_ContentMaster__c icm = createData();
		I_MenuMaster__c mm = createIRISMenuMaster2();
		I_PageMaster__c ipm = createIRISPage2(mm.Id);
		I_ContentMaster__c cm = createIRISCMS2(ipm);

		Test.startTest();
			I_TopController ilt = new I_TopController();
			List<I_CMSComponentController> compList = ilt.getBannerContents();
			System.assertEquals(compList.size(),1);
		Test.stopTest();
	}

		static I_MenuMaster__c createIRISMenuMaster2(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = '特集';
		mm.SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE;
		insert mm;
		return mm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage2(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		pm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		pm.Menu__c = mmId;
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS2(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS2(icm,ipm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS2(I_ContentMaster__c paricm,I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.IRIS_Page__c = ipm.Id;
		//icm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		//icm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		//icm.FeatureFlag__c = true;
		insert icm;
		return icm;
	}

	//PwC LMT対応分　ここから
	/**
	@author PwC
	@note LMT
	*/
	static testMethod void getSJandFATest(){
			List<User> testPartnerUser = createSJFAPartnerUser('E_PartnerCommunity_IA');
			I_TopController instance = new I_TopController();
			system.runAs(testPartnerUser[0]){
				Boolean ret =instance.getSJandFA();
				//System.assertEquals(true, ret);
			}
			system.runAs(testPartnerUser[1]){
				Boolean ret =instance.getSJandFA();
				//System.assertEquals(true, ret);
			}

	}

	/**
	@author PwC
	@note LMT　ソニックジャパン、ファイナンシャルアライアンスのテストユーザ作成
	*/
	private static List<User> createSJFAPartnerUser(String profileName){
		String sjUserName = 'testSJ01@sj.co.jp.test';
		String faUserName = 'testFA01@fa.co.jp.test';

		Profile p = [Select Id From Profile Where Name = :profileName];

		List<User> userList = new List<User>();
		User user01 = new User(
			Lastname = 'test01'
			, Username = sjUserName
			, Email = sjUserName
			, ProfileId = p.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);


		User user02 = new User(
			Lastname = 'test02'
			, Username = faUserName
			, Email = faUserName
			, ProfileId = p.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);

		system.runAs(thisuser){
				// Account 代理店格
				List<Account> acpList = new List<Account>();
				Account acp01 = new Account(Name = 'Sonic Japan Parent',E_CL1PF_ZHEADAY__c = System.label.LMT_SonicJapan_AgencyCode);
				Account acp02 = new Account(Name = 'Financial Alliance Parent',E_CL1PF_ZHEADAY__c = System.label.LMT_FA_AgencyCode);
				acpList.add(acp01);
				acpList.add(acp02);
				insert acpList;

				// Account 事務所
				List<Account> acList = new List<Account>();
				Account ac01 = new Account(
						Name = 'Sonic Japan'
						,ParentId = acp01.Id
						,E_CL2PF_ZAGCYNUM__c = 'ac001'
						,E_COMM_VALIDFLAG__c = '1'
				);

				Account ac02 = new Account(
						Name = 'Financial Alliance'
						,ParentId = acp01.Id
						,E_CL2PF_ZAGCYNUM__c = 'ac002'
						,E_COMM_VALIDFLAG__c = '1'
				);

				acList.add(ac01);
				acList.add(ac02);
				insert acList;

				// Contact 募集人
				List<Contact> conList = new List<Contact>();
				Contact cn01 = new Contact(LastName = 'SJ',AccountId = ac01.Id, E_CL3PF_ZHEADAY__c = ac01.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ac01.E_CL2PF_ZAGCYNUM__c);
				cn01.E_CL3PF_AGNTNUM__c = 'ac001';
				cn01.email = 'testSJ01@sj.co.jp.test';
				conList.add(cn01);

				Contact cn02 = new Contact(LastName = 'FA',AccountId = ac02.Id, E_CL3PF_ZHEADAY__c = ac02.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ac02.E_CL2PF_ZAGCYNUM__c);
				cn02.E_CL3PF_AGNTNUM__c = 'ac002';
				cn02.email = 'testFA01@sj.co.jp.test';
				conList.add(cn02);
				insert conList;

				user01.ContactId = cn01.Id;
				userList.add(user01);
				user02.ContactId = cn02.Id;
				userList.add(user02);
				insert userList;

				List<ContactShare> csList = new List<ContactShare>();
				ContactShare cs01 = new ContactShare(
				ContactId = cn01.Id,
				ContactAccessLevel = 'read',
				UserOrGroupId = user01.Id);
				csList.add(cs01);

				ContactShare cs02 = new ContactShare(
				ContactId = cn02.Id,
				ContactAccessLevel = 'read',
				UserOrGroupId = user02.Id);
				csList.add(cs02);
				insert csList;
			}
			return userList;
	}

	/**
	@author PwC
	@note LMT
	*/
	static testMethod void getLeadPageTest(){
		I_TopController instance = new I_TopController();
		PageReference ret = instance.getLeadPage();
		System.assertNotEquals(null, ret);
	}
	//PwC LMT対応分　ここまで


	// IRIS住生ユーザーの場合は IRIS_TopSumisei を表示 // 利用アプリを IRIS に更新する
	static testMethod void initTest_returnSumisei(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User uc = new User();
		Contact con = new Contact();
		System.runAs(thisUser){
			//住生IRISユーザー作成
			Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
			Account childAcc = TestI_TestUtil.createAccount(true,parentAcc);
			con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ',true);
			uc = TestI_TestUtil.createAgentUser(true,'sumiseiIRIS',E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS,con.Id);
			TestI_TestUtil.createIDCPF(true,uc.Id,'AT');
			TestI_TestUtil.createContactShare(con.Id,uc.Id);
		}
		System.runAs(uc){
			TestI_TestUtil.createBasePermissions(uc.Id);
			TestI_TestUtil.createAgencyPermissions(uc.Id);
			Test.startTest();
			I_TopController instance = new I_TopController();
			PageReference ret = instance.pageAction();
			System.assertEquals(ret.getUrl(),Page.IRIS_TopSumisei.getUrl());
			Test.stopTest();
		}
	}
}