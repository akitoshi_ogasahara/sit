/**
* クラス名	:  AgencyForMRTriggerTest
* クラス概要	:  代理店【INGAgency】情報画面　トリガー処理(テスト)
* @created	:  2
* @modified	:   
*/
@isTest
private class AgencyForINGAgencyTriggerTest {

	// 所有者名変更時
  static testMethod void myUnitTest() {
  	
    User user1 = [SELECT Id FROM User where isActive=true  limit 1];
   
		User user2 = new User();
		user2 = user1.clone(false, false, false);
		user2.Email = 'AgencyForMRTriggerTest-2@keizu.com';
    user2.LastName = 'LastName';
    user2.profileid = UserInfo.getProfileId();
    user2.emailencodingkey='ISO-8859-1';
    user2.languagelocalekey='en_US';
    user2.localesidkey='en_GB';
    user2.timezonesidkey='Europe/London';
    user2.Username='AgencyForMRTriggerTest-2@keizu.com';
    user2.Alias='Username';
    user2.CommunityNickname='CommunityNickname';
		insert user2;
		
		System.runAs (user1) {
			
			//  代理店【INGAgency】を作成
			AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
			agcINGA.Name = 'trig-test';
			insert agcINGA;
			
			// 顧客情報を作成
			Customer__c customs = createCustomer(agcINGA.Id);
			insert customs;
			
			agcINGA.OwnerId = user2.ID;
			update agcINGA;
			
		}
		
  }
    
   // 削除時のテスト
   static testMethod void myUnitTest2() {
  	
			
			//担当者別代理店を作成
			AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
			agcINGA.Name = 'trig-test';
			insert agcINGA;
			
			// 顧客情報を作成
			Customer__c customs = createCustomer(agcINGA.Id);
			insert customs;
			
			delete agcINGA;
			
	}
		

	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 
	*/
	static Customer__c createCustomer(String agc_id){
		// 顧客
		Customer__c customs = new Customer__c();
		// 顧客名
		customs.Name = 'triger-test';
		// 代理店
		customs.AgencyForINGAgency__c = agc_id;
		
		return customs;
	}

}