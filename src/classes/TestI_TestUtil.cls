@isTest
public with sharing class TestI_TestUtil {
	/**
	 * Profileマップ取得
	 * @return Map<String, Id>: Map of ProfileName & ProfileID
	 */
	private static Map<String, Id> pMap;
	private static Map<String, Id> getProfileIdMap(){
		if(pMap != null){
			return pMap;
		}
		pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}

	/**
	 * 自主点検事務所検索（代理店）用データセット作成
	 * @param permission: 権限（AY or AH）
	 * @return User: 実行ユーザ
	 */
	public static User createTestDataSet(String permission, SC_Office__c office){
	// 実行ユーザ作成
		// 1. 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_TestUtil.createContact(true, accAgency.Id, actUserLastName);
		// 4. 実行ユーザを作成
		User actUser = TestSC_TestUtil.createAgentUser(true, actUserLastName, 'E_PartnerCommunity', con.Id);
		// 5. ID管理を作成
		E_IDCPF__c idMng = TestSC_TestUtil.createIDCPF(true, actUser.Id, permission);

	// テストデータ作成
		// 6. 自主点検事務所を更新
		office.Account__c = accAgency.Id;
		update office;
		// 7. レコードを共有
		TestSC_TestUtil.createSCOfficeShare(office.Id, actUser.Id);
		TestSC_TestUtil.createAccountShare(accAgency.Id, actUser.Id);
		TestSC_TestUtil.createAccountShare(accParent.Id, actUser.Id);

		return actUser;
	}

	/**
	 * ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	public static User createUser(Boolean isInsert, String LastName, String profileDevName){
		String userName = LastName + '@terrasky.ingtesting';

		if(UserInfo.getLanguage() == 'en_US' && profileDevName == 'システム管理者'){
			profileDevName = 'System Administrator';
		}
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}

	/**
	 * 代理店ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @param ConId： 募集人ID
	 * @return User: ユーザ
	 */
	public static User createAgentUser(Boolean isInsert, String LastName, String profileDevName, ID ConId){
		String userName = LastName + '@terrasky.ingtesting';
		if(UserInfo.getLanguage() == 'en_US' && profileDevName == 'システム管理者'){
			profileDevName = 'System Administrator';
		}

		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
				, ContactId = ConId
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}

	/**
	 * 代理店権限セット付与
	 * @param isInsert: whether to insert
	 * @param usrId: ユーザID
	 */
	public static void createAgencyPermissions(ID usrId) {
		Id permAgnyId = [Select Id FROM PermissionSet Where Name =: SC_Const.PERMISSIONSET_SC_Agent].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
		insert src;
	}

	/**
	 * 自主点検（参照のみ）権限セット付与
	 * @param isInsert: whether to insert
	 * @param usrId: ユーザID
	 */
	public static void createReadOnlyPermissions(ID usrId) {
		Id permAgnyId = [Select Id FROM PermissionSet Where Name =: SC_Const.PERMISSIONSET_SC_Director].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
		insert src;
	}

	/**
	 * 自主点検（社員）権限セット付与
	 * @param isInsert: whether to insert
	 * @param usrId: ユーザID
	 */
	public static void createEmployeePermissions(ID usrId) {
		Id permAgnyId = [Select Id FROM PermissionSet Where Name =: SC_Const.PERMISSIONSET_SC_NN].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
		insert src;
	}

	/**
	 * 自主点検（CMD）権限セット付与
	 * @param isInsert: whether to insert
	 * @param usrId: ユーザID
	 */
	public static void createCMDPermissions(ID usrId) {
		Id permAgnyId = [Select Id FROM PermissionSet Where Name =: SC_Const.PERMISSIONSET_SC_Admin].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
		insert src;
	}

	/**
	 * ベース権限セット付与
	 * @param isInsert: whether to insert
	 * @param usrId: ユーザID
	 */
	public static void createBasePermissions(ID usrId) {
		Id permAgnyId = [Select Id FROM PermissionSet Where Name = 'E_PermissionSet_Base'].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
		insert src;
	}

	/**
	 * IRIS住生権限セット付与
	 * @param isInsert: whether to insert
	 * @param usrId: ユーザID
	 */
	public static void createIRISSumiseiPermissions(ID usrId) {
		Id permAgnyId = [Select Id FROM PermissionSet Where Name = 'I_PermissionSet_Sumisei'].Id;
		PermissionSetAssignment src = new PermissionSetAssignment(AssigneeId = usrId, PermissionSetId = permAgnyId);
		insert src;
	}

	/**
	 * Account作成
	 * @param isInsert: whether to insert
	 * @param parentId: Account.Id 親取引先
	 * @return Account: 取引先レコード
	 */
	public static Account createAccount(Boolean isInsert, Account parent) {
		Account src = new Account(
			  Name = 'TestAccountName'
			, E_CL1PF_ZHEADAY__c  = '12345'
			, E_CL2PF_ZAGCYNUM__c = '67890'
			, E_COMM_VALIDFLAG__c = '1'
			, ZMRCODE__c = 'MR00001'
//			, ParentId = parent.Id
		);

		if(parent != null){
			src.ParentId = parent.Id;
		}

		if(isInsert){
			insert src;
			src = [Select Id, Name, E_CL1PF_ZHEADAY__c, E_COMM_VALIDFLAG__c, ZMRCODE__c, E_IsAgency__c From Account Where Id =: src.Id];
		}
		return src;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 */
	public static Contact createContact(Boolean isInsert, Id accId, String lastName) {
		Contact src = new Contact(
			  AccountId = accId
			, LastName = lastName
		);
		if(isInsert){
			insert src;
			src = [Select Id, Account.Name, LastName, E_AccParentCord__c From Contact Where Id =: src.Id];
		}
		return src;
	}

	/**
	 * ID管理DB作成
	 * @param isInsert: whether to insert,userId
	 * @param userId: User.Id
	 * @param permission: AH or AY
	 * @return :src:ID管理レコード
	 */
	public static E_IDCPF__c createIDCPF(Boolean isInsert, Id userId, String permission){
		E_IDCPF__c src = new E_IDCPF__c(
			 User__c = userId
			,OwnerId = userId
			,ZIDTYPE__c = permission
			,ZSTATUS01__c = '1'
			,ZINQUIRR__c = 'L167890'
			,AppMode__c = 'IRIS'
			,ZDSPFLAG02__c = '0'
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}

	/**
	 * Attachment作成
	 * @param isInsert: whether to insert
	 * @param scId: SC_SelfCompliance__c.Id
	 * @return Attachment: Attachmentレコード(規定ファイル)
	 */
	public static Attachment createAttachment(Boolean isInsert, ID scId) {
		Blob body = Blob.valueOf('test.jpg');
		Attachment src = new Attachment(Name = 'File001', ParentId = scId, Body = body);
		if(isInsert){
			insert src;
		}
		return src;
	}

	/**
	 * Account（share）作成
	 * @param isInsert: whether to insert
	 * @param officeId: Account.Id
	 * @param userId: User.Id
	 * @return なし
	 */
	public static void createAccountShare(ID accId, ID userId) {
		AccountShare share = new AccountShare();
		share.AccountId = accId;
		share.UserOrGroupId = userId;
		share.AccountAccessLevel = 'Edit';
		share.CaseAccessLevel = 'Edit';
		share.ContactAccessLevel = 'Edit';
		share.OpportunityAccessLevel = 'Edit';

		insert share;

		return;
	}

	/**
	 * Contact（share）作成
	 * @param conId: Contact.Id
	 * @param userId: User.Id
	 * @return なし
	 */
	public static void createContactShare(ID conId, ID userId) {
		ContactShare share = new ContactShare();
		share.ContactId = conId;
		share.UserOrGroupId = userId;
		share.ContactAccessLevel = 'Edit';

		insert share;

		return;
	}

/*
* IRIS_CMSコンテンツ-コンポーネントを作成する
* @param isInsert 登録するか否か
* @param Name IRIS_CMSコンテンツ名
* @param ClickAction__c クリックアクション
* @param filePath　DLファイル格納先
* @return cmsCtt I_ContentMaster__c 登録したCMSコンテンツ
*/
	public static I_ContentMaster__c createContentMaster(Boolean isInsert,String name,String clickAction,String filePath){

		I_ContentMaster__c src = new I_ContentMaster__c(
			Name = name
			,ClickAction__c = clickAction
			,FilePath__c = filePath
			);

		if(isInsert)insert src;
		return src;
	}

/*
* ファイル情報作成
* @param isInsert: whether to insert
* @param name: ファイル名
* @param mId: 親メッセージマスタのレコードID
* @param recTypeDevName: レコードタイプ名
* @return E_CMSFile__c: ファイル情報レコード
*/
	public static E_CMSFile__c createCMSFile(Boolean isInsert, String name, Id mId, String recTypeDevName){
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_CMSFile__c').get(recTypeDevName);

		E_CMSFile__c src = new E_CMSFile__c(
				Name = name
				,RecordTypeId = recTypeId
				,MessageMaster__c = mId
				,Note__c = 'test'
		);
		if(isInsert)insert src;
		return src;
	}


	//住生代理店格作成
	public static Account createSumiseiParentAccount(){
		Account parentAcc = new Account();
		parentAcc.Name = 'テスト住生代理店格';
		parentAcc.E_CL1PF_ZHEADAY__c = E_Const.ZHEADAY_SUMISEI;
		parentAcc.E_COMM_VALIDFLAG__c = '1';
		Insert parentAcc;
		return parentAcc;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 * @param sumiseiIRIS sumiseiIRISUser
	 */
	public static Contact createSumiseiContact(Id accId, String lastName,Boolean sumiseiIRIS) {
		Contact con = new Contact();
		con.AccountId = accId;
		con.LastName = lastName;
		con.E_CL3PF_AGTYPE__c = '99';
		if(sumiseiIRIS){
			con.E_CL3PF_AGTYPE__c = '02';
		}
		Insert con;
		return con;
	}


}