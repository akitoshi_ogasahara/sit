global with sharing class CR_AgencySearchBizBooksSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public CR_Agency__c record{get;set;}	
			
	public CR_AgencySearchBizBooksExtender getExtender() {return (CR_AgencySearchBizBooksExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component53_val {get;set;}	
		public SkyEditor2.TextHolder Component53_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component55_val {get;set;}	
		public SkyEditor2.TextHolder Component55_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component57_val {get;set;}	
		public SkyEditor2.TextHolder Component57_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component59_val {get;set;}	
		public SkyEditor2.TextHolder Component59_op{get;set;}	
			
	public String recordTypeRecordsJSON_CR_Agency_c {get; private set;}
	public String defaultRecordTypeId_CR_Agency_c {get; private set;}
	public String metadataJSON_CR_Agency_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public CR_AgencySearchBizBooksSVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = CR_Agency__c.fields.AgencyCode__c;
		f = CR_Agency__c.fields.AgencyName__c;
		f = CR_Agency__c.fields.MRUnit__c;
		f = CR_Agency__c.fields.MRCode__c;
		f = CR_Agency__c.fields.MRName__c;
		f = CR_Agency__c.fields.LastRequestBook__c;
		f = CR_Agency__c.fields.BusinessStatus__c;
		f = CR_Agency__c.fields.RecordTypeId;
		f = CR_Agency__c.fields.LargeAgency__c;
		f = CR_Agency__c.fields.OutOfBusiness__c;
		f = CR_Agency__c.fields.MR__c;
		f = CR_Agency__c.fields.Status__c;
		f = CR_Agency__c.fields.LastModifiedDate;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = CR_Agency__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component53_val = new SkyEditor2__SkyEditorDummy__c();	
				Component53_op = new SkyEditor2.TextHolder('eq');	
					
				Component55_val = new SkyEditor2__SkyEditorDummy__c();	
				Component55_op = new SkyEditor2.TextHolder('sw');	
					
				Component57_val = new SkyEditor2__SkyEditorDummy__c();	
				Component57_op = new SkyEditor2.TextHolder('sw');	
					
				Component59_val = new SkyEditor2__SkyEditorDummy__c();	
				Component59_op = new SkyEditor2.TextHolder('eq');	
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('CR_Agency__c')
						.addFieldAsOutput('MRUnit__c')
						.addFieldAsOutput('MRName__c')
						.addFieldAsOutput('AgencyCode__c')
						.addFieldAsOutput('AgencyName__c')
						.addFieldAsOutput('LastRequestBook__c')
						.addFieldAsOutput('BusinessStatus__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component53_val, 'SkyEditor2__Text__c', 'AgencyCode__c', Component53_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component55_val, 'SkyEditor2__Text__c', 'AgencyName__c', Component55_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component57_val, 'SkyEditor2__Text__c', 'MRUnit__c', Component57_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component59_val, 'SkyEditor2__Text__c', 'MRCode__c', Component59_op, true, 0, true ))
						.addWhere(' ( RecordType.DeveloperName = \'CR_Biz\' AND LargeAgency__c = true)')
.addSort('OutOfBusiness__c',True,True).addSort('MRUnit__c',True,True).addSort('MR__c',True,True).addSort('Status__c',True,True).addSort('LastModifiedDate',False,True)
				);	
					
					resultTable = new resultTable(new List<CR_Agency__c>(), new List<resultTableItem>(), new List<CR_Agency__c>(), null);
				resultTable.ignoredOnSave = true;
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(CR_Agency__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_AgencySearchBizBooksExtender(this);
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyCode_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyCode__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyName_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyName__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_MRUnit_c() { 
			return getOperatorOptions('CR_Agency__c', 'MRUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_MRCode_c() { 
			return getOperatorOptions('CR_Agency__c', 'MRCode__c');	
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public CR_Agency__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, CR_Agency__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (CR_Agency__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}