@isTest
private class MNT_PamphletSearchExtenderTest{
    
    @isTest
    private static void exportCSV(){
    	
		System.assert(true);
		MNT_PamphletSearchController page = new MNT_PamphletSearchController(new ApexPages.StandardController(new I_ContentMaster__c()));
		MNT_PamphletSearchExtender extender = new MNT_PamphletSearchExtender(page);

		Test.startTest();
			PageReference result = extender.doExportCSV();

			PageReference pr = new PageReference('/');
			System.assertNotEquals(result, pr);
		Test.stopTest();
    }
}