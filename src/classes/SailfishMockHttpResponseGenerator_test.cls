@isTest
global class SailfishMockHttpResponseGenerator_test implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('https://a0483wvu02.secure.svfcloud.com/api/v1/procedures/Sailfish_DirectPrint', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-type', 'application/x-www-form-urlencoded');
        res.setBody('{"example":"test"}');
        res.setStatusCode(200);
        return res;
    }    

}