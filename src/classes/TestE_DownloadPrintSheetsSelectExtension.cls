/**
 * 
 */
@isTest
private class TestE_DownloadPrintSheetsSelectExtension {
	
	//ProfileName
	private static String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	private static User user;
	private static User thisUser = [select id from user where id =:system.UserInfo.getUserId()];
	private static User communityUser;
	private static Account kakuAcc;
	private static List<Account> offAccList;
	private static Contact agentCon;

	/**
	 * Getter
	 * プロファイル：社員
	 */
    static testMethod void testGetter01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);

			system.assertEquals(E_Const.CS_DISTORIBUTOR, extension.getCS_DISTORIBUTOR());
			system.assertEquals(E_Const.CS_OFFICE, extension.getCS_OFFICE());
			system.assertEquals(E_Const.CS_AGENT, extension.getCS_AGENT());
			system.assertEquals('ダウンロード帳票選択', extension.getTitle());
	    }

    	Test.stopTest();
    }
    
	
	/**
	 * Constructor
	 * プロファイル：社員
	 */
    static testMethod void testConstructor01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);

			system.assertEquals(E_Const.CS_DISTORIBUTOR, extension.selectTerm);
			system.assertEquals(false, extension.isSearched);
	    }

    	Test.stopTest();
    }
    
	/**
	 * Constructor
	 * プロファイル：コミュニティ
	 * ID管理：AH
	 */
    static testMethod void testConstructor02() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(communityUser){
    		E_CookieHandler.setCookieCompliance();
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			system.assert(controller.distributor != null);
			system.assert(controller.office == null);
			system.assert(controller.agent == null);
			system.assertEquals(true, extension.isSearched);
	    }

    	Test.stopTest();
    }
    
	/**
	 * Constructor
	 * プロファイル：コミュニティ
	 * ID管理：AY
	 */
    static testMethod void testConstructor03() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AY);
    	
    	Test.startTest();
    	
    	system.runAs(communityUser){
	    	E_CookieHandler.setCookieCompliance();
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			system.assert(controller.distributor == null);
			system.assert(controller.office != null);
			system.assert(controller.agent == null);
			system.assertEquals(true, extension.isSearched);
	    }

    	Test.stopTest();
    }
    
	/**
	 * Constructor
	 * プロファイル：コミュニティ
	 * ID管理：AT
	 */
    static testMethod void testConstructor04() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AT);
/*
		system.runAs(user){
	    	insert new ContactShare(
	    		  UserOrGroupId = communityUser.Id
	    		, ContactId = communityUser.ContactId
	    		, ContactAccessLevel = 'Read'
	    	);
    	}
*/
    	Test.startTest();
    	
    	system.runAs(communityUser){
    		E_CookieHandler.setCookieCompliance();
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			system.assert(controller.distributor == null);
			system.assert(controller.office == null);
//			system.assert(controller.agent != null);
			system.assertEquals(true, extension.isSearched);
	    }
		
    	Test.stopTest();
    }
    
	/**
	 * Constructor
	 * プロファイル：コミュニティ
	 * ID管理：AH,AY,AT以外
	 */
    static testMethod void testConstructor05() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, 'TT');
    	
    	Test.startTest();
    	
    	system.runAs(communityUser){
    		E_CookieHandler.setCookieCompliance();
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			system.assertEquals(E_Const.ERROR_MSG_FUNCTION, controller.pageRef.getParameters().get('code'));
	    }

    	Test.stopTest();
    }
    
	/**
	 * PageAction
	 * プロファイル：コミュニティ
	 * ID管理：AH
	 */
    static testMethod void testPageAction01() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(communityUser){
    		E_CookieHandler.setCookieCompliance();
 	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.pageAction();
			system.assertEquals(null, pref);
	    }

    	Test.stopTest();
    }
	/**
	 * Constructor
	 * プロファイル：社員
	 * ID管理：EP
	 * AHフィルター
	 */
	static testMethod void testPageAction_IRIS_AH(){
		createData(true);
		createUser(PF_EMPLOYEE);
		createDataAccessObjIRIS(user.Id,E_Const.ZIDTYPE_EP);

		Test.startTest();
		system.runAs(user){
			E_IRISHandler.setPolicyCondition(I_Const.CONDITION_FIELD_AGENCY,kakuAcc.id);
	    		E_CookieHandler.setCookieCompliance();
 		    	E_DownloadController controller = new E_DownloadController();
		    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.pageAction();
			system.assertEquals(null, pref);
		}
	Test.stopTest();
	}
	/**
	 * Constructor
	 * プロファイル：社員
	 * ID管理：EP
	 * AYフィルター
	 */
	static testMethod void testPageAction_IRIS_AY(){
		createData(true);
		createUser(PF_EMPLOYEE);
		createDataAccessObjIRIS(user.Id,E_Const.ZIDTYPE_EP);

		Test.startTest();
		system.runAs(user){
			E_IRISHandler.setPolicyCondition(I_Const.CONDITION_FIELD_OFFICE,offAccList[0].id);
	    		E_CookieHandler.setCookieCompliance();
 		    	E_DownloadController controller = new E_DownloadController();
		    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.pageAction();
			system.assertEquals(null, pref);
		}
	Test.stopTest();
	}
	/**
	 * Constructor
	 * プロファイル：社員
	 * ID管理：EP
	 * ATフィルター
	 */
	static testMethod void testPageAction_IRIS_AT(){
		createData(true);
		createUser(PF_EMPLOYEE);
		createDataAccessObjIRIS(user.Id,E_Const.ZIDTYPE_EP);

		Test.startTest();
		system.runAs(user){
			E_IRISHandler.setPolicyCondition(I_Const.CONDITION_FIELD_AGENT,agentCon.id);
	    		E_CookieHandler.setCookieCompliance();
 		    	E_DownloadController controller = new E_DownloadController();
		    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.pageAction();
			system.assertEquals(null, pref);
		}
	Test.stopTest();
	}
	/**
	 * Constructor
	 * プロファイル：社員
	 * ID管理：EP
	 * MRフィルター
	 */
	static testMethod void testPageAction_IRIS_MR(){
		createData(true);
		createUser(PF_EMPLOYEE);
		createDataAccessObjIRIS(user.Id,E_Const.ZIDTYPE_EP);

		Test.startTest();
		system.runAs(user){
			E_IRISHandler.setPolicyCondition(I_Const.CONDITION_FIELD_MR,user.id);
	    		E_CookieHandler.setCookieCompliance();
 		    	E_DownloadController controller = new E_DownloadController();
		    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.pageAction();
			system.assertEquals(null, pref);
		}
	Test.stopTest();
	}
	/**
	 * reDraw
	 * プロファイル：コミュニティ
	 * ID管理：AH
	 */
    static testMethod void testReDraw01() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(communityUser){
    		E_CookieHandler.setCookieCompliance();
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.reDraw();
			system.assertEquals(null, extension.iDistoributor.accountid);
			system.assertEquals(null, extension.iOffice.accountid);
			system.assertEquals(null, extension.iAgent.MainAgent__c);
	    }

    	Test.stopTest();
    }
    
	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：代理店検索
	 */
    static testMethod void testDoSearch01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_DISTORIBUTOR;
			extension.iDistoributor.accountid = kakuAcc.Id;
		
			Pagereference pref = extension.doSearch();
			system.assertEquals(true, extension.isSearched);
			system.assertEquals(kakuAcc.Id, controller.distributor.Id);
			system.assertEquals(null, controller.office);
			system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }

	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：代理店事務所検索
	 */
    static testMethod void testDoSearch02() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_OFFICE;
			extension.iOffice.accountid = offAccList[0].Id;
		
			Pagereference pref = extension.doSearch();
			system.assertEquals(true, extension.isSearched);
			system.assertEquals(null, controller.distributor);
			system.assertEquals(offAccList[0].Id, controller.office.Id);
			system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }

	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：募集人検索
	 */
    static testMethod void testDoSearch03() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(true, extension.isSearched);
			//system.assertEquals(null, controller.distributor);
			//system.assertEquals(offAccList[0].Id, controller.office.Id);
			//system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }
    
	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AY
	 * 正常：募集人検索
	 */
    static testMethod void testDoSearch04() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AY);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AY);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(true, extension.isSearched);
			//system.assertEquals(null, controller.distributor);
			//system.assertEquals(offAccList[0].Id, controller.office.Id);
			//system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }
  
	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AT
	 * 正常：募集人検索
	 */
    static testMethod void testDoSearch05() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AT);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AT);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(true, extension.isSearched);
			//system.assertEquals(null, controller.distributor);
			//system.assertEquals(offAccList[0].Id, controller.office.Id);
			//system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }
    
	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH,AY,AT以外
	 * 正常：募集人検索
	 */
    static testMethod void testDoSearch06() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AT);
    	createDataAccessObj(communityUser.Id, 'TT');
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(true, extension.isSearched);
			//system.assertEquals(null, controller.distributor);
			//system.assertEquals(offAccList[0].Id, controller.office.Id);
			//system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }

	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * エラー：代理店検索
	 */
    static testMethod void testErrDoSearch01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_DISTORIBUTOR;
			extension.iDistoributor.accountid = null;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(false, extension.isSearched);
	    }

    	Test.stopTest();
    }

	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * エラー：代理店事務所検索
	 */
    static testMethod void testErrDoSearch02() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_OFFICE;
			extension.iOffice.accountid = null;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(false, extension.isSearched);
	    }

    	Test.stopTest();
    }
       
	/**
	 * doSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * エラー：募集人検索
	 */
    static testMethod void testErrDoSearch03() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = null;
			
			Pagereference pref = extension.doSearch();
			system.assertEquals(false, extension.isSearched);
			//system.assertEquals(null, controller.distributor);
			//system.assertEquals(offAccList[0].Id, controller.office.Id);
			//system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }
   
	/**
	 * doBack
	 * プロファイル：コミュニティ
	 * ID管理：AH
	 */
    static testMethod void testDoBack01() {
    	createData(true);
    	createUser(PF_SYSTEM);
    	createCommunityUser();
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(communityUser){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			Pagereference pref = extension.doBack();
			system.assertEquals(null, pref);
			system.assertEquals(false, extension.isSearched);
			system.assertEquals(null, controller.distributor);
			system.assertEquals(null, controller.office);
			system.assertEquals(null, controller.agent);
	    }

    	Test.stopTest();
    }
    
	/**
	 * doDownloadFeeGuidanceSearch
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：手数料のご案内, 代理店
	 */
    static testMethod void testDoDownloadFeeGuidanceSearch01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_DISTORIBUTOR;
			extension.iDistoributor.accountid = kakuAcc.Id;
			controller.distributor = kakuAcc;
		
			Pagereference pref = extension.doDownloadFeeGuidanceSearch();
			String exp = Page.E_DownloadFeeGuidanceSearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(kakuAcc.Id, pref.getParameters().get(controller.KEY_DID));
	    }

    	Test.stopTest();
    }

	/**
	 * doDownloadFeeDataSearchAgspf
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：手数料明細(生保), 代理店事務所検索
	 */
    static testMethod void testDoDownloadFeeDataSearchAgspf01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_OFFICE;
			extension.iOffice.accountid = offAccList[0].Id;
			controller.office = offAccList[0];
		
			Pagereference pref = extension.doDownloadFeeDataSearchAgspf();
			String exp = Page.E_DownloadFeeDataSearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(offAccList[0].Id, pref.getParameters().get(controller.KEY_OID));
	    }

    	Test.stopTest();
    }

	/**
	 * doDownloadFeeDataSearchSpva
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：募集人検索
	 */
    static testMethod void testDoDownloadFeeDataSearchSpva01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			Contact con = [Select Id, AccountId, Account.ParentId From Contact Where Id = :communityUser.contactId];
			controller.agent = con;
			
			Pagereference pref = extension.doDownloadFeeDataSearchSpva();
			String exp = Page.E_DownloadFeeDataSearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(con.AccountId, pref.getParameters().get(controller.KEY_OID));
	    }

    	Test.stopTest();
    }
    
	/**
	 * doDownloadFeeDataSearchBospf
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：募集人検索
	 */
    static testMethod void testDoDownloadFeeDataSearchBospf01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			Contact con = [Select Id, AccountId, Account.ParentId From Contact Where Id = :communityUser.contactId];
			controller.agent = con;
			
			Pagereference pref = extension.doDownloadFeeDataSearchBospf();
			String exp = Page.E_DownloadFeeDataSearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(con.AccountId, pref.getParameters().get(controller.KEY_OID));
	    }

    	Test.stopTest();
    }
    
	/**
	 * doDownloadHoldPolicySearchColi
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：募集人検索
	 */
    static testMethod void testDoDownloadHoldPolicySearchColi01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			Contact con = [Select Id, AccountId, Account.ParentId From Contact Where Id = :communityUser.contactId];
			controller.agent = con;
			
			Pagereference pref = extension.doDownloadHoldPolicySearchColi();
			String exp = Page.E_DownloadHoldPolicySearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(con.Id, pref.getParameters().get(controller.KEY_AID));
	    }

    	Test.stopTest();
    }
    
	/**
	 * doDownloadHoldPolicySearchSpva
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：募集人検索
	 */
    static testMethod void testDoDownloadHoldPolicySearchSpva01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			Contact con = [Select Id, AccountId, Account.ParentId From Contact Where Id = :communityUser.contactId];
			controller.agent = con;
			
			Pagereference pref = extension.doDownloadHoldPolicySearchSpva();
			String exp = Page.E_DownloadHoldPolicySearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(con.Id, pref.getParameters().get(controller.KEY_AID));
	    }

    	Test.stopTest();
    }
    
	/**
	 * doDownloadPolicyHistorySearch
	 * プロファイル：社員
	 * ID管理：AH
	 * 正常：募集人検索
	 */
    static testMethod void testDoDownloadPolicyHistorySearch01() {
    	createData(true);
    	createUser(PF_EMPLOYEE);
    	createCommunityUser();
    	createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
    	createDataAccessObj(communityUser.Id, E_Const.ZIDTYPE_AH);
    	
    	Test.startTest();
    	   	
    	system.runAs(user){
	    	E_DownloadController controller = new E_DownloadController();
	    	E_DownloadPrintSheetsSelectExtension extension = new E_DownloadPrintSheetsSelectExtension(controller);
			
			extension.selectTerm = E_Const.CS_AGENT;
			extension.iAgent.MainAgent__c = communityUser.contactId;
			Contact con = [Select Id From Contact Where Id = :communityUser.contactId];
			controller.agent = con;
			
			Pagereference pref = extension.doDownloadPolicyHistorySearch();
			String exp = Page.E_DownloadPolicyHistorySearch.getUrl().toLowerCase();
			system.assert(pref.getUrl().contains(exp));
			system.assertEquals(con.Id, pref.getParameters().get(controller.KEY_AID));
	    }

    	Test.stopTest();
    }
    
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */    
    
    /** User作成 */
    private static void createUser(String profileName){
        system.runAs(thisuser){
            // User
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            String userName = 'test@terrasky.ingtesting';
            Profile p = [Select Id From Profile Where Name = :profileName];
            user = new User(
                Lastname = 'test'
                , Username = userName
                , Email = userName
                , ProfileId = p.Id
                , Alias = 'test'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , UserRoleId = portalRole.Id
            );
            insert user;
        }
    }
    
    /** コミュニティユーザ作成 */
    private static void createCommunityUser(){
        system.runAs(thisuser){
            // Account
            Account account = new Account(
                Name = 'testAccount'
                ,parentId = kakuAcc.Id
                ,ownerid = user.id
            );
            insert account;
            
            // Contact
            Contact contact = new Contact(
                LastName = 'fstest'
                ,FirstName = 'firstName'
                ,AccountId = account.Id
                ,email = 'fstest@terrasky.ingtesting'
                ,ownerid = user.Id
            );
            insert contact;
            
            Profile p = [Select Id From Profile Where Name = :PF_PARTNER];
            communityUser = new User(
                Lastname = 'fstest'
                ,Username = 'fstest@terrasky.ingtesting'
                ,Email = 'fstest@terrasky.ingtesting'
                ,ProfileId = p.Id
                ,Alias = 'fstest'
                ,TimeZoneSidKey = UserInfo.getTimeZone().getID()
                ,LocaleSidKey = UserInfo.getLocale()
                ,EmailEncodingKey = 'UTF-8'
                ,LanguageLocaleKey = 'ja'
                ,CommunityNickName='tuser1'
                ,contactID = contact.Id
            );
            insert communityUser;
        }
    }
    
    
    /** データアクセス系作成 */
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG02__c = '1'
                ,FLAG06__c = '1'
                ,ZDSPFLAG01__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }
    
    private static void createDataAccessObjIRIS(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG02__c = '1'
                ,FLAG06__c = '1'
                ,ZDSPFLAG01__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
                ,AppMode__c = I_Const.APP_MODE_IRIS
                
            );
            insert idcpf;
        }
    }

    /** Data作成 */
    private static void createData(Boolean isCreateOffice){
        system.runAs(thisuser){
            // Account 代理店格
            kakuAcc = new Account(Name = 'kakuAccount');
            insert kakuAcc;
            
            if(isCreateOffice){
                offAccList = new List<Account>();
                for(Integer i = 1; i < 4; i++){
                    Account acc = new Account(
                        Name = 'office' + i
                        ,ParentId = kakuAcc.Id
                    );
                    offAccList.add(acc);
                }
                insert offAccList;
            }
            
            if(!offAccList.isEmpty()){
                agentCon = new Contact(
                    LastName = 'test'
                    ,AccountId = offAccList[0].Id
                );
                insert agentCon;
            }
            
            // 代理店手数料生保明細CSVダウンロードデータ用
            E_AGSPF__c agspf = new E_AGSPF__c();
        }
    }
}