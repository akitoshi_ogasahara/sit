@isTest
private class TestSC_SelfComplianceDao {

	@isTest static void getAttachmentsByOfficeId_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(true, accAgency.Id, null);
		// 添付ファイルを作成
		List<Attachment> attList = new List<Attachment>();
		for(SC_SelfCompliance__c sc : [SELECT Id FROM SC_SelfCompliance__c]){
			attList.add(TestSC_TestUtil.createAttachment(false, sc.Id));
		}
		insert attList;

		Test.startTest();
			List<SC_SelfCompliance__c> result = SC_SelfComplianceDao.getAttachmentsByOfficeId(scOffice.Id);
		Test.stopTest();

		System.assertEquals(SC_Const.SC_COMP_TYPE_LIST.size(), result.size());
		System.assertEquals(1, result[0].Attachments.size());
	}

	@isTest static void getRecById_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(true, accAgency.Id, null);

		SC_SelfCompliance__c sc = [SELECT Id FROM SC_SelfCompliance__c WHERE SC_Office__c = :scOffice.Id LIMIT 1];
		System.assertNotEquals(null, sc);

		Test.startTest();
			SC_SelfCompliance__c result = SC_SelfComplianceDao.getRecById(sc.Id);
		Test.stopTest();

		System.assertNotEquals(null, result);
	}

	@isTest static void getRecsByOfficeId_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(true, accAgency.Id, null);
		// 添付ファイルを作成
		List<Attachment> attList = new List<Attachment>();
		List<SC_WebAnswer__c> answers = new List<SC_WebAnswer__c>();
		for(SC_SelfCompliance__c sc : [SELECT Id FROM SC_SelfCompliance__c]){
			attList.add(TestSC_TestUtil.createAttachment(false, sc.Id));
			answers.add(new SC_WebAnswer__c(SC_SelfCompliance__c = sc.Id));
		}
		insert attList;
		insert answers;

		Test.startTest();
			List<SC_SelfCompliance__c> result = SC_SelfComplianceDao.getRecsByOfficeId(scOffice.Id);
		Test.stopTest();

		System.assertEquals(SC_Const.SC_COMP_TYPE_LIST.size(), result.size());
		System.assertEquals(1, result[0].SC_WebAnswers__r.size());
		System.assertEquals(1, result[0].Attachments.size());
	}

}