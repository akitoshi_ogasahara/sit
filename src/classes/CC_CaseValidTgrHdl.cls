public with sharing class CC_CaseValidTgrHdl{

	/**
	 * Trigger Handler実行確認用変数
	 */
	private static Set<String> runTgrSet = new Set<String>();

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){
		if(!runTgrSet.contains('onBeforeInsert')){
			runTgrSet.add('onBeforeInsert');

			//validation check
			setValidation(newList, new Map<Id,Case>(), new Map<Id,Case>());
		}
	}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){
		if(!runTgrSet.contains('onAfterInsert')){
			runTgrSet.add('onAfterInsert');
		}
	}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		if(!runTgrSet.contains('onBeforeUpdate')){
			runTgrSet.add('onBeforeUpdate');

			//validation check
			setValidation(newList, newMap, oldMap);
		}
	}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}

	/**
	 * set validation
	 */
	public static void setValidation(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		//SR画面からInsert/Updateの場合、Validation再実行しない
		List<Case> filteredNewList = new List<Case>();
		for(Case caseObj : newList){
			if(!caseObj.CC_PreventTriggerValid__c){
				filteredNewList.add(caseObj);
			}else{
				//データ初期化
				caseObj.CC_PreventTriggerValid__c = false;
				caseObj.CC_IsSRFormPrint__c = false;
			}
		}

		//set validation
		if(filteredNewList.size() > 0){
			Boolean triggerFlag = true;
			String inputAlertMessage = '';
			Map<String, Object> outputValidMap = CC_CaseValid.errorCheckAndOutput(filteredNewList, newMap, triggerFlag, inputAlertMessage);

			if(outputValidMap != null){
				for(Case caseObj : filteredNewList){
					if(outputValidMap.get((String)caseObj.Id) != null){
						//show error message
						String errorMessage = (String)outputValidMap.get((String)caseObj.Id);
						caseObj.addError(errorMessage, false);
					}
				}
			}
		}
	}

}