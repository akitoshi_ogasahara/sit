@isTest
public class TestE_VansInsuranceDao {

	/**
	 * getRecsByAsrIdのテスト
	 */
	@isTest static void getRecsByAsrIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 乗合保険会社の作成
		List<E_VansInsurance__c> vansInsurance = createVansInsurance(agencySalesResults.Id);
		insert vansInsurance;

		// テスト
		Test.startTest();
		List<E_VansInsurance__c> recs = E_VansInsuranceDao.getRecsByAsrId(agencySalesResults.Id);
		System.assertEquals(agencySalesResults.Id, recs[0].E_AgencySalesResults__c);
		Test.stopTest();

	}


	/**
	 * 乗合保険会社の作成
	 */
	public static List<E_VansInsurance__c> createVansInsurance(Id asrId){

		List<E_VansInsurance__c> vansInsurance = new List<E_VansInsurance__c>();

		E_VansInsurance__c rec = new E_VansInsurance__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		//rec.Hierarchy__c				= '乗合保険会社 01';	// 階層
		rec.AGSR_KLIAJCOY__c			= '01';					// 乗合保険会社コード
		rec.VansInsCompany__c			= 'XXX生命1';			// 乗合保険会社
		rec.Proxy__c					= '非代申';				// 代申・非代申
		vansInsurance.add(rec);

		rec = new E_VansInsurance__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		//rec.Hierarchy__c				= '乗合保険会社 02';	// 階層
		rec.AGSR_KLIAJCOY__c			= '02';					// 乗合保険会社コード
		rec.VansInsCompany__c			= 'XXX生命2';			// 乗合保険会社
		rec.Proxy__c					= '非代申';				// 代申・非代申
		vansInsurance.add(rec);

		return vansInsurance;
	}

}