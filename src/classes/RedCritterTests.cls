@isTest
private class RedCritterTests{

   static testMethod void testAddAddActionVariation1(){
       Double value = 1.0;
       RedCritterAPI.addAction('test','','',value );  
   }
   
   static testMethod void testAddAddActionVariation2(){
       Double value = 1.0;
       RedCritterAPI.addAction('test','','','',value );  
   }
   
   static testMethod void testAwardBadgeVariation1 (){
       RedCritterAPI.awardBadge('test','','');    
   }
   
    static testMethod void testAwardBadgeVariation2 (){
       RedCritterAPI.awardBadge('test','','','');    
   }
   
   static testMethod void testRevokeBadgeVariation1 (){
       RedCritterAPI.awardBadge('test','','');    
   }
   
    static testMethod void testRevokeBadgeVariation2 (){
       RedCritterAPI.awardBadge('test','','','');    
   }
   
   static testMethod void testAwardCertVariation1 (){
       RedCritterAPI.awardCert('test','','','','');    
   }
   
    static testMethod void testAwardCertVariation2 (){
       RedCritterAPI.awardCert('test','','','','','');  
   }
   
   static testMethod void testRevokeCertVariation1 (){
       RedCritterAPI.revokeCert('test','','','');    
   }
   
   static testMethod void testRevokeCertVariation2 (){
       RedCritterAPI.revokeCert('test','','','','');  
   }
   
   static testMethod void testAdjustSkillVariation1 (){
       RedCritterAPI.adjustSkill('test','','',1);  
   }
   
   static testMethod void testAdjustSkillVariation2 (){
       RedCritterAPI.adjustSkill('test','','','',1);  
   }
   
   static testMethod void testAdjustRewardPointsVariation1 (){
       RedCritterAPI.adjustRewardPoints('test','',1,'');  
   }
   
   static testMethod void testAdjustRewardPointsVariation2 (){
       RedCritterAPI.adjustRewardPoints('test','','',1,'');  
   }
   
}