@isTest
private class TestI_FooterController {
    //--- コンストラクタ ---
    static testMethod void ConstructorTest(){
        I_FooterController target = new I_FooterController();
        System.assertNotEquals(null, target);
    }


    //--- getPrivacyPolicyPageKeyメソッド ---
    static testMethod void getPrivacyPolicyPageKeyTest(){
    	I_FooterController target = new I_FooterController();
        String actual = target.getPrivacyPolicyPageKey();
        String expected = 'PrivacyPolicy';
        System.assertEquals(expected, actual);
    }


    //--- getInternetPolicyKeyメソッド ---
    //GWUsePolicy
    static testMethod void getInternetPolicyPageKeyTest1(){
       	I_FooterController target = new I_FooterController();
       	String actual = target.getInternetPolicyPageKey();
       	String expected = 'InternetUsePolicy';
       	System.assertEquals(expected, actual);
    }

    //--- getInternetPolicyKeyメソッド ---
    //住生ユーザ
    static testMethod void getInternetPolicyPageKeyTest2(){
        //実行ユーザー作成
        Account parentAcc = TestI_TestUtil.createSumiseiParentAccount();
        Account childAcc = TestI_TestUtil.createAccount(true, parentAcc);
        Contact con = TestI_TestUtil.createSumiseiContact(childAcc.Id,'住生ユーザ', true);
        User actUser = TestI_TestUtil.createAgentUser(true, 'sumiseiIRIS', E_Const.PROFILE_E_PARTNERCOMMUNITY_SumiseiIRIS, con.Id);
        TestI_TestUtil.createIDCPF(true, actUser.Id, 'AT');
        TestI_TestUtil.createContactShare(con.Id, actUser.Id);

        System.runAs(actUser){
            I_FooterController target = new I_FooterController();
            System.assertEquals('InternetUsePolicySumisei', target.getInternetPolicyPageKey());
       }
    }

    //--- getInternetPolicyKeyメソッド ---
    //GWUsePolicy
    static testMethod void getInternetPolicyPageKeyTest3(){
        I_FooterController target = new I_FooterController();
        ApexPages.currentPage().getParameters().put('isSMTM', '1');
        String actual = target.getInternetPolicyPageKey();
        String expected = 'InternetUsePolicy';
        System.assertEquals('InternetUsePolicySumisei&isSMTM=1', target.getInternetPolicyPageKey());
    }

    //--- getIRISCMSPageNameメソッド ---
    //ログイン後
    static testMethod void getIRISCMSPageNameTest_afterLogin(){
        I_FooterController target = new I_FooterController();
        String actual = target.getIRISCMSPageName();
        String expected = 'IRIS';
        System.assertEquals(expected, actual);
    }


    //--- getIsGuest()メソッド ---
    static testMethod void getIsGuestTest(){
        //実行ユーザー作成
        User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
        TestI_TestUtil.createIDCPF(true, actUser.Id, null);

        System.runAs(actUser){
            I_FooterController target = new I_FooterController();
            System.assert(!target.getIsGuest());
        }
    }
}