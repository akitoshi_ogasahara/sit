@isTest
private class TestI_PolicyUtil {
	@isTest static void getCancellationRefundTest1() {
		E_Policy__c pol = createPolicy1();
		Test.startTest();
			Decimal dec = I_PolicyUtil.getCancellationRefund(pol);
		Test.stopTest();
		System.assertEquals(dec,6000);
	}

	@isTest static void getCancellationRefundTest2() {
		E_Policy__c pol = createPolicy2();
		Test.startTest();
			Decimal dec = I_PolicyUtil.getCancellationRefund(pol);
		Test.stopTest();
		System.assertEquals(dec,-1);
	}

	@isTest static void getCancellationRefundTest3() {
		E_Policy__c pol = createPolicy3();
		Test.startTest();
			Decimal dec = I_PolicyUtil.getCancellationRefund(pol);
		Test.stopTest();
		System.assertEquals(dec,6500);
	}

	@isTest static void getAddressTest1(){
		Test.startTest();
			String address = I_PolicyUtil.getAddress('1111111','テスト');
		Test.stopTest();
		System.assertEquals(address,'111-1111 テスト');
	}

	@isTest static void isVal09Test1(){
		Test.startTest();
			Boolean isval = I_PolicyUtil.isVal09('00000000');
		Test.stopTest();
		System.assertEquals(isval,true);
	}

	@isTest static void isVal09Test2(){
		Test.startTest();
			Boolean isval = I_PolicyUtil.isVal09('11111111');
		Test.stopTest();
		System.assertEquals(isval,false);
	}


	@isTest static void getGenzaiHokenTest1(){
		E_Policy__c pol = createPolicy1();
		createCOV(pol,E_Const.COVPF_CRTABLE2_JA);
		E_COVPF__c covpf = [select id,COMM_SUMINS__c,COLI_ZSUMINF__c,COMM_CRTABLE2__c from E_COVPF__c where E_Policy__c =: pol.Id];
		Test.startTest();
			String result = I_PolicyUtil.getGenzaiHoken(covpf);
		Test.stopTest();
		System.assertEquals(result,'?');
	}


	@isTest static void getGenzaiHokenTest2(){
		E_Policy__c pol = createPolicy1();
		createCOV(pol,'TT');
		E_COVPF__c covpf = [select id,COMM_SUMINS__c,COLI_ZSUMINF__c,COMM_CRTABLE2__c from E_COVPF__c where E_Policy__c =: pol.Id];
		Test.startTest();
			String result = I_PolicyUtil.getGenzaiHoken(covpf);
		Test.stopTest();
		System.assertEquals(result,'100,000');
	}


	@isTest static void addZCOVRNAMTest1(){
		Test.startTest();
			String result = I_PolicyUtil.addZCOVRNAM('テスト',true,true);
		Test.stopTest();
		System.assertEquals(result,'テスト<br>（保険料払込免除特則付加あり）<br>（保険料逓減払込特則付加あり）');
	}

	@isTest static void getInsuredANBCCDTest1() {
		E_Policy__c pol = createPolicy1();
		createCrlpf(pol,'LA');
		Test.startTest();
			Decimal dec = I_PolicyUtil.getInsuredANBCCD(pol.Id);
		Test.stopTest();
		System.assertEquals(dec,20);
	}


	@isTest static void getSurrenderValueSumTest1() {
		E_Policy__c pol = createPolicy3();
		Test.startTest();
			Decimal dec = I_PolicyUtil.getSurrenderValueSum(pol);
		Test.stopTest();
		System.assertEquals(dec,6500);
	}


	@isTest static void getSurrenderValueSumTest2() {
		E_Policy__c pol = createPolicy4(I_Const.POLICY_ZBKPODIS_10);
		Test.startTest();
			Decimal dec = I_PolicyUtil.getSurrenderValueSum(pol);
		Test.stopTest();
		System.assertEquals(dec,6000);
	}


	@isTest static void getCorporationNameTest1() {
		Test.startTest();
			String conName = I_PolicyUtil.getCorporationName('テスト');
		Test.stopTest();
		System.assertEquals(conName,'');
	}


	@isTest static void getCorporationNameTest2() {
		Test.startTest();
			String conName = I_PolicyUtil.getCorporationName('');
		Test.stopTest();
		System.assertEquals(conName,'');
	}


	@isTest static void getCorporationNameTest3() {
		createContact();
		Test.startTest();
			String conName = I_PolicyUtil.getCorporationName('12345678');
		Test.stopTest();
		System.assertEquals(conName,'テスト顧客');
	}

	@isTest static void getIsDisablePolicyTest1() {
		E_Policy__c pol = createPolicy5();
		Test.startTest();
			Boolean result = I_PolicyUtil.getIsDisablePolicy(pol);
		Test.stopTest();
		System.assertEquals(result,true);
	}


	@isTest static void getIsViewBankTest1() {
		E_Policy__c pol = createPolicy4(I_Const.POLICY_ZBKPODIS_10);
		Test.startTest();
			Boolean result = I_PolicyUtil.getIsViewBank(pol);
		Test.stopTest();
		System.assertEquals(result,true);
	}

	@isTest static void getIsViewBankTest2() {
		E_Policy__c pol = createPolicy4(I_Const.POLICY_ZBKPODIS_20);
		Test.startTest();
			Boolean result = I_PolicyUtil.getIsViewBank(pol);
		Test.stopTest();
		System.assertEquals(result,true);
	}

	@isTest static void getIsViewBankTest3() {
		E_Policy__c pol = createPolicy4('30');
		Test.startTest();
			Boolean result = I_PolicyUtil.getIsViewBank(pol);
		Test.stopTest();
		System.assertEquals(result,false);
	}




	static E_Policy__c createPolicy1(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 20000;
		pol.COLI_ZEAPLTOT__c = 5000;
		pol.COLI_ZEPLTOT__c = 10000;
		pol.COLI_ZEADVPRM__c = 1000;
		pol.COLI_ZUNPREM__c = 500;
		insert pol;
		return pol;
	}

	static E_Policy__c createPolicy2(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 20000;
		pol.COLI_ZEAPLTOT__c = 5000;
		pol.COLI_ZEPLTOT__c = 10000;
		pol.COLI_ZEADVPRM__c = 1000;
		pol.COLI_ZUNPREM__c = 500;
		pol.COLI_ZCVDCF__c = true;

		return pol;
	}

	static E_Policy__c createPolicy3(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 20000;
		pol.COLI_ZEAPLTOT__c = 5000;
		pol.COLI_ZEPLTOT__c = 10000;
		pol.COLI_ZEADVPRM__c = 1000;
		pol.COLI_ZUNPREM__c = 500;
		pol.COLI_ZUNPCF__c = false;
		pol.COLI_ZUNPDCF__c = true;


		return pol;
	}

	static E_Policy__c createPolicy4(String zbkpodis){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 20000;
		pol.COLI_ZEAPLTOT__c = 5000;
		pol.COLI_ZEPLTOT__c = 10000;
		pol.COLI_ZEADVPRM__c = 1000;
		pol.COLI_ZUNPREM__c = 500;
		pol.COLI_ZUNPCF__c = false;
		pol.COLI_ZUNPDCF__c = false;
		pol.COMM_ZECHNL__c = I_Const.POLICY_ZECHNL_ACCOUNTTRAN;
		pol.COMM_STATCODE__c = 'A';
		pol.COLI_ZNPTDDCF__c = true;
		pol.COLI_ZBKTRDCF__c = true;
		pol.COLI_ZBKPODIS__c = zbkpodis;

		return pol;
	}

	static E_Policy__c createPolicy5(){
		E_Policy__c pol = new E_Policy__c();
		pol.COLI_ZCSHVAL__c = 20000;
		pol.COLI_ZEAPLTOT__c = 5000;
		pol.COLI_ZEPLTOT__c = 10000;
		pol.COLI_ZEADVPRM__c = 1000;
		pol.COLI_ZUNPREM__c = 500;
		pol.COLI_ZUNPCF__c = false;
		pol.COLI_ZUNPDCF__c = false;
		pol.COMM_STATCODE__c = 'L';
		pol.COMM_ZRSTDESC__c  = '失効中';

		return pol;
	}


	//個人保険特約
	static void createCOV(E_Policy__c po,String crtable){

		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_COVPF__c').get(E_Const.COVPF_RECORDTYPE_ECOVPF);


		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_STATDATE__c = '20161111';
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		cov.COLI_ZCRIND__c = 'C';
		cov.COMM_ZCOVRNAM__c = '4テストタイプ';
		cov.COLI_ZDPTYDSP__c = true;
		cov.COMM_CHDRNUM__c = '99999999';
		cov.DCOLI_ZCLNAME__c = '契約者A';
		cov.DCOLI_ZINSNAM__c = '被保険者A';
		cov.DCOLI_SINSTAMT__c = 123456789;
		cov.DCOLI_OCCDATE__c = '20171027';
		cov.COMM_ZRCESDSC__c = '終身';
		cov.COMM_ZPCESDSC__c = '---------';
		cov.RecordTypeId = recTypeId;
		cov.COMM_CRTABLE__c = crtable;
		cov.COLI_ZSUMINF__c = true;

		insert cov;
	}


	//保険顧客情報
	static void createCrlpf(E_Policy__c policy,String role){
		E_CRLPF__c crl = new E_CRLPF__c();

		crl.E_Policy__c = policy.Id;
		crl.BNYTYP__c = '01';
		crl.ZCLNAME__c = '照須快';
		crl.CLNTNUM__c = '99999999';
		crl.ZKNJSEX__c = '男性';
		crl.BNYPC__c = 100.00;
		crl.DOB__c = '11111111';
		crl.CRLPFCode__c = '99999999|TS|1';
		crl.CLRRROLE__c = role;
		crl.VALIDFLAG__c = '1';
		crl.ANBCCD__c = 20;

		insert crl;
	}

	static void createContact(){
		Contact con = new COntact();

		con.LastName = 'テスト顧客';
		con.E_CLTPF_CLNTNUM__c = '12345678';

		insert con;
	}


}