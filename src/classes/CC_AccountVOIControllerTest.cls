/*
 * CC_AccountVOIControllerTest
 * Test class of CC_AccountVOIController
 * created  : Accenture 2018/7/11
 * modified :
 */

@isTest
private class CC_AccountVOIControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Test saveAccount()
	* with no error
	*/
	static testMethod void saveAccountTest01() {
		CC_AccountVOIController accountVOIController = new CC_AccountVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert account;

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', account.Id);
			inputMap.put('[\'CMN_Phone3__c\']', '07012341234');
			inputMap.put('[\'CMN_Phone3PIC__c\']', 'testuser');
			inputMap.put('[\'CMN_Email2__c\']', 'testclass@test.com');
			inputMap.put('[\'Description\']', 'this is a test');

			String methodName = 'saveAccount';
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			accountVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test saveAccount()
	* Standard error: Email
	*/
	static testMethod void saveAccountTest02() {
		CC_AccountVOIController accountVOIController = new CC_AccountVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert account;

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', account.Id);
			inputMap.put('[\'CMN_Email2__c\']', 'testclass@test.com@@@');

			String methodName = 'saveAccount';
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			accountVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAccount()
	* Custom error: CMN_Phone3__c
	*/
	static testMethod void saveAccountTest03() {
		CC_AccountVOIController accountVOIController = new CC_AccountVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert account;

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', account.Id);
			inputMap.put('[\'CMN_Phone3__c\']', '07012341234a!');

			String methodName = 'saveAccount';
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			accountVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAccount()
	* blank data
	*/
	static testMethod void saveAccountTest04() {
		CC_AccountVOIController accountVOIController = new CC_AccountVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert account;

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', account.Id);
			inputMap.put('[\'Description\']', '');

			String methodName = 'saveAccount';
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			accountVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAccount()
	* Exception
	*/
	static testMethod void saveAccountTest05() {
		CC_AccountVOIController accountVOIController = new CC_AccountVOIController();
		System.runAs ( testUser ) {

			//Create data Map
			String methodName = 'saveAccount';
			Map<String, Object> inputMap = null;
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			accountVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}
}