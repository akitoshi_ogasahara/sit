@isTest
private class TestCR_OutSrcViewExtender {
    
    // コンストラクタテスト
    private static testMethod void CROutSrcViewTest001(){
        CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
        CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);

        
        Test.startTest();
            PageReference pref = Page.E_CROutsrcView;
            Test.setCurrentPage(pref);
            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
            
            System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_OUTSRC);
            System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_OUTSRC_View);
        Test.stopTest(); 
    }

  	// initテスト (外部委託参照権限なしエラー)
	private static testMethod void CROutSrcViewTest002(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	        	CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest();
        }
	}

  	// initテスト２ (レコードId、またはレコード自体が存在しない場合)
	private static testMethod void CROutSrcViewTest003(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){
        	Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	Test.setCurrentPage(pref);
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	        	CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	        	target.controller.record.Id = null;
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest(); 	
        } 
	}

	// initテスト３
	private static testMethod void CROutSrcViewTest004(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutSrcEdit;
	        	ApexPages.currentPage().getParameters().put('parentId', '12345');
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	        	CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, true);
	        Test.stopTest();
		}
	}

    // 削除処理
    private static testMethod void CROutSrcsViewTest005(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcAddConfirm06(true, agny.Id);
	        
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            
	            System.assertEquals(target.doDeleteEx().getUrl(), '/apex/e_croutsrcs?id=' + agny.Id);
	        Test.stopTest();
		}
    }

    // ファイルアップロードボタン押下
    private static testMethod void CROutSrcViewTest006(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){          
	        Test.startTest();
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            
	            System.assertEquals(target.getUrlforUpload(), '/apex/e_croutsrcsubmit?id=' + outsrc.Id);
	        Test.stopTest(); 
		}
    }

    // 外部委託先編集ページ遷移
    private static testMethod void CROutSrcViewTest007(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            
	            System.assertEquals(target.getUrlforEdit(), '/apex/e_croutsrcedit?id=' + outsrc.Id);
	        Test.stopTest();
		} 
    }

    // 編集ボタン表示
    private static testMethod void CROutSrcViewTest008(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	Attachment updatedFile = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            target.init();
	            
	            System.assertEquals(target.getCanEdit(), true);
	        Test.stopTest();
		} 
    }

    // 削除ボタン表示
    private static testMethod void CROutSrcViewTest009(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcAddConfirm06(true, agny.Id);
	        	Attachment updatedFile = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            target.init();
	            
	            System.assertEquals(target.getCanDelete(), true);
	        Test.stopTest();
		} 
    }

    // 承認ボタン表示
    private static testMethod void CROutSrcViewTest010(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcAddConfirm06(true, agny.Id);
	        	Attachment updatedFile = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            target.init();
	            
	            System.assertEquals(target.getHasNextAction(), true);
	        Test.stopTest();
		} 
    }

    // 取消ボタン表示
    private static testMethod void CROutSrcViewTest011(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcAddConfirm06(true, agny.Id);
	        	Attachment updatedFile = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            target.init();
	            
	            System.assertEquals(target.getHasRejectAction(), false);
	        Test.stopTest();
		} 
    }
    
    // ワーニング表示
    private static testMethod void CROutSrcViewTest012(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){        
	        Test.startTest();
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcAddConfirm06(true, agny.Id);
	        	Attachment updatedFile = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	
	            PageReference pref = Page.E_CROutsrcView;
	            Test.setCurrentPage(pref);
	            Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
	            CR_OutSrcViewController controller = new CR_OutSrcViewController(standardcontroller);
	            CR_OutSrcViewExtender target = new CR_OutSrcViewExtender(controller);
	            target.init();
	            
	            System.assertEquals(target.getShowWarningForPersonalInfo(), true);
	        Test.stopTest();
		} 
    }               
}