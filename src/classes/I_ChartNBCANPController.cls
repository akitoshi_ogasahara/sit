public with sharing class I_ChartNBCANPController {

	public Id paramSalesResultsId {get; set;}				// 代理店挙積情報ID
	public String paramViewType {get; set;}					// 表示タイプ （1：月別新契約係数ANP実績セクション 2：月別特定保険種類係数ANP実績セクション）

	transient public String json {get; set;}				// JSONデータ
	public String title {									// タイトル
		get{
			if(title == null){
				title = '';
				if(this.paramViewType == '1'){
					title				= '全商品';
				}else if(this.paramViewType == '2'){
					title				= '特定保険種類';
				}
			}
			return title;
		}
		set;
	}

	/**
	 * コンストラクタ
	 */
	public I_ChartNBCANPController(){
		this.paramSalesResultsId	= null;											// 代理店挙積情報ID
		this.paramViewType			= '';											// 表示タイプ

	    this.json					= '';											// JSONデータ
	}

	/**
	 * JS用値設定の取得(リモートアクション)
	 */
	@readOnly
	@RemoteAction
	public static String getJSParam(Id pSalesResultsId, String pViewType){
		I_ChartNBCANPController cls	= new I_ChartNBCANPController();
		cls.paramSalesResultsId			= pSalesResultsId;	// 代理店挙積情報ID
		cls.paramViewType				= pViewType;		// 表示タイプ
		cls.getJSParameter();								// JS用値設定の取得
		return cls.json;									// JSONデータ
	}

	/**
	 * JS用値設定の取得
	 */
	public void getJSParameter(){
		// JSONデータ
		this.json				= '';
		if(this.paramViewType == '1'){
			this.json				= getJSON1();
		}else if(this.paramViewType == '2'){
			this.json				= getJSON2();
		}
	}

	/**
	 * JSONデータの取得
	 */
	public String getJSON1(){
		List<String> values			= new List<String>();
		List<String> subfields		= new List<String>();
		List<String> lines			= new List<String>();
		List<String> fields			= new List<String>();
		Integer year				= 0;

		// 代理店挙積情報取得
		E_AgencySalesResults__c agencySalesResults = E_AgencySalesResultsDao.getRecTypeC1ById(paramSalesResultsId);

		// 年取得
		try{
			year = Integer.valueOf(agencySalesResults.YYYYMM__c.left(4));
		}catch(Exception e){ return ''; }

		lines			= new List<String>();
		// 新契約係数ANP　当年(月単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year) + '');							// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Jan__c));					// 新契約係数ANP　当年1月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Feb__c));					// 新契約係数ANP　当年2月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Mar__c));					// 新契約係数ANP　当年3月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Apr__c));					// 新契約係数ANP　当年4月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_May__c));					// 新契約係数ANP　当年5月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Jun__c));					// 新契約係数ANP　当年6月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Jul__c));					// 新契約係数ANP　当年7月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Aug__c));					// 新契約係数ANP　当年8月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Sep__c));					// 新契約係数ANP　当年9月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Oct__c));					// 新契約係数ANP　当年10月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Nov__c));					// 新契約係数ANP　当年11月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Dec__c));					// 新契約係数ANP　当年12月
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　前年(月単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 1) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYJan__c));				// 新契約係数ANP　前年1月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYFeb__c));				// 新契約係数ANP　前年2月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYMar__c));				// 新契約係数ANP　前年3月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYApr__c));				// 新契約係数ANP　前年4月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYMay__c));				// 新契約係数ANP　前年5月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYJun__c));				// 新契約係数ANP　前年6月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYJul__c));				// 新契約係数ANP　前年7月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYAug__c));				// 新契約係数ANP　前年8月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYSep__c));				// 新契約係数ANP　前年9月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYOct__c));				// 新契約係数ANP　前年10月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYNov__c));				// 新契約係数ANP　前年11月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LYDec__c));				// 新契約係数ANP　前年12月
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　2年前(月単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 2) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YJan__c));				// 新契約係数ANP　2年前1月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YFeb__c));				// 新契約係数ANP　2年前2月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YMar__c));				// 新契約係数ANP　2年前3月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YApr__c));				// 新契約係数ANP　2年前4月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YJun__c));				// 新契約係数ANP　2年前6月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YMay__c));				// 新契約係数ANP　2年前5月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YJul__c));				// 新契約係数ANP　2年前7月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YAug__c));				// 新契約係数ANP　2年前8月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YSep__c));				// 新契約係数ANP　2年前9月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YOct__c));				// 新契約係数ANP　2年前10月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YNov__c));				// 新契約係数ANP　2年前11月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2YDec__c));				// 新契約係数ANP　2年前12月
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"anp":[' + String.join(lines,',') + ']\n');

		lines			= new List<String>();
		// 新契約係数ANP　当年(4半期単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year) + '');							// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_1Q__c));					// 新契約係数ANP　当年1Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_2Q__c));					// 新契約係数ANP　当年2Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_3Q__c));					// 新契約係数ANP　当年3Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_4Q__c));					// 新契約係数ANP　当年4Q
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　前年(4半期単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 1) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LY1Q__c));				// 新契約係数ANP　前年1Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LY2Q__c));				// 新契約係数ANP　前年2Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LY3Q__c));				// 新契約係数ANP　前年3Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_LY4Q__c));				// 新契約係数ANP　前年4Q
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　2年前(4半期単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 2) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2Y1Q__c));				// 新契約係数ANP　2年前1Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2Y2Q__c));				// 新契約係数ANP　2年前2Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2Y3Q__c));				// 新契約係数ANP　2年前3Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_B2Y4Q__c));				// 新契約係数ANP　2年前4Q
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"anpq":[' + String.join(lines,',') + ']');

		lines			= new List<String>();
		// 新契約係数ANP　進捗率
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_Progress__c));			// 新契約係数ANP　当年対前年進捗率
		values.add(I_Util.toStr(agencySalesResults.NBCANP_ProgressLY__c));			// 新契約係数ANP　前年対前年進捗率
		fields.add('"progress":[' + String.join(values,',') + ']');

		return String.join(fields,',\n');
	}

	/**
	 * JSONデータの取得
	 */
	public String getJSON2(){
		List<String> values			= new List<String>();
		List<String> subfields		= new List<String>();
		List<String> lines			= new List<String>();
		List<String> fields			= new List<String>();
		Integer year				= 0;

		// 代理店挙積情報取得
		E_AgencySalesResults__c agencySalesResults = E_AgencySalesResultsDao.getRecTypeC2ById(paramSalesResultsId);

		// 年取得
		try{
			year = Integer.valueOf(agencySalesResults.YYYYMM__c.left(4));
		}catch(Exception e){ return ''; }

		lines			= new List<String>();
		// 新契約係数ANP　特定保険種類　当年(月単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year) + '');							// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeJan__c));		// 新契約係数ANP　特定保険種類　当年1月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeFeb__c));		// 新契約係数ANP　特定保険種類　当年2月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeMar__c));		// 新契約係数ANP　特定保険種類　当年3月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeApr__c));		// 新契約係数ANP　特定保険種類　当年4月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeMay__c));		// 新契約係数ANP　特定保険種類　当年5月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeJun__c));		// 新契約係数ANP　特定保険種類　当年6月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeJul__c));		// 新契約係数ANP　特定保険種類　当年7月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeAug__c));		// 新契約係数ANP　特定保険種類　当年8月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeSep__c));		// 新契約係数ANP　特定保険種類　当年9月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeOct__c));		// 新契約係数ANP　特定保険種類　当年10月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeNov__c));		// 新契約係数ANP　特定保険種類　当年11月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeDec__c));		// 新契約係数ANP　特定保険種類　当年12月
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　特定保険種類　前年(月単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 1) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYJan__c));		// 新契約係数ANP　特定保険種類　前年1月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYFeb__c));		// 新契約係数ANP　特定保険種類　前年2月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYMar__c));		// 新契約係数ANP　特定保険種類　前年3月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYApr__c));		// 新契約係数ANP　特定保険種類　前年4月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYMay__c));		// 新契約係数ANP　特定保険種類　前年5月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYJun__c));		// 新契約係数ANP　特定保険種類　前年6月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYJul__c));		// 新契約係数ANP　特定保険種類　前年7月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYAug__c));		// 新契約係数ANP　特定保険種類　前年8月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYSep__c));		// 新契約係数ANP　特定保険種類　前年9月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYOct__c));		// 新契約係数ANP　特定保険種類　前年10月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYNov__c));		// 新契約係数ANP　特定保険種類　前年11月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLYDec__c));		// 新契約係数ANP　特定保険種類　前年12月
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　特定保険種類　2年前(月単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 2) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YJan__c));		// 新契約係数ANP　特定保険種類　2年前1月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YFeb__c));		// 新契約係数ANP　特定保険種類　2年前2月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YMar__c));		// 新契約係数ANP　特定保険種類　2年前3月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YApr__c));		// 新契約係数ANP　特定保険種類　2年前4月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YJun__c));		// 新契約係数ANP　特定保険種類　2年前6月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YMay__c));		// 新契約係数ANP　特定保険種類　2年前5月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YJul__c));		// 新契約係数ANP　特定保険種類　2年前7月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YAug__c));		// 新契約係数ANP　特定保険種類　2年前8月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YSep__c));		// 新契約係数ANP　特定保険種類　2年前9月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YOct__c));		// 新契約係数ANP　特定保険種類　2年前10月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YNov__c));		// 新契約係数ANP　特定保険種類　2年前11月
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2YDec__c));		// 新契約係数ANP　特定保険種類　2年前12月
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"anp":[' + String.join(lines,',') + ']\n');

		lines			= new List<String>();
		// 新契約係数ANP　特定保険種類　当年(4半期単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year) + '');							// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsType1Q__c));			// 新契約係数ANP　特定保険種類　当年1Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsType2Q__c));			// 新契約係数ANP　特定保険種類　当年2Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsType3Q__c));			// 新契約係数ANP　特定保険種類　当年3Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsType4Q__c));			// 新契約係数ANP　特定保険種類　当年4Q
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　特定保険種類　前年(4半期単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 1) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLY1Q__c));		// 新契約係数ANP　特定保険種類　前年1Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLY2Q__c));		// 新契約係数ANP　特定保険種類　前年2Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLY3Q__c));		// 新契約係数ANP　特定保険種類　前年3Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLY4Q__c));		// 新契約係数ANP　特定保険種類　前年4Q
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		// 新契約係数ANP　特定保険種類　2年前(4半期単位)
		subfields		= new List<String>();
		subfields.add('"year":' + I_Util.toStr(year - 2) + '');						// 年
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2Y1Q__c));		// 新契約係数ANP　特定保険種類　2年前1Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2Y2Q__c));		// 新契約係数ANP　特定保険種類　2年前2Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2Y3Q__c));		// 新契約係数ANP　特定保険種類　2年前3Q
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeB2Y4Q__c));		// 新契約係数ANP　特定保険種類　2年前4Q
		subfields.add('"val":[' + String.join(values,',') + ']');
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"anpq":[' + String.join(lines,',') + ']');

		lines			= new List<String>();
		// 新契約係数ANP　特定保険種類　進捗率
		values			= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeProgress__c));	// 新契約係数ANP　特定保険種類　当年対前年進捗率
		values.add(I_Util.toStr(agencySalesResults.NBCANP_SpInsTypeLY__c));			// 新契約係数ANP　特定保険種類　前年対前年進捗率
		fields.add('"progress":[' + String.join(values,',') + ']');

		return String.join(fields,',\n');
	}
}