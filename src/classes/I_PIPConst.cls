public with sharing class I_PIPConst {
	// メッセージマスタkey
	//個別契約概要画面（対象外商品）
	public static final String MESSAGE_KEY_COLI_NOTAVAILABLE_PRODUCT = 'I|PIP|001';
	//個別契約概要画面（サービス時間外）
	public static final String MESSAGE_KEY_COLI_OUT_OF_SERVICE_TIME = 'I|PIP|002';
	//推移表（Atria呼び出しエラー）
	public static final String MESSAGE_KEY_MERITTABLE_ATRIA_ERROR = 'I|PIP|003';
	//推移表（サービス時間外）
	public static final String MESSAGE_KEY_MERITTABLE_OUT_OF_SERVICE_TIME = 'I|PIP|004';
	//推移表（対象外商品）
	public static final String MESSAGE_KEY_MERITTABLE_UNAVEIRABLE_PRODUCT = 'I|PIP|005';
	//推移表（推移表表示対象外契約）
	public static final String MESSAGE_KEY_MERITTABLE_UNAVEIRABLE_CONTRACT = 'I|PIP|006';
	//推移表（汎用エラー）
	public static final String MESSAGE_KEY_MERITTABLE_UNIVERSAL_ERROR = 'I|PIP|007';

	//実効税率のデフォルト値
	public static final Decimal EFFECTIVE_TAX_RATE = 33.00;

	//特別条件の基準日
	public static final Integer SPECIAL_CONDITIONS_STANDARDDATE = 20010401;

	// CRコード
	// 重大疾病
	public static final String CR_CODE_TT = 'TT';
	// 終身
	public static final String CR_CODE_WL = 'WL';
	//生活障害
	public static final String CR_CODE_TS = 'TS';
	//定期
	public static final String CR_CODE_TP = 'TP';
	//逓増定期(GA)
	public static final String CR_CODE_GA = 'GA';
	//逓増定期(GB)
	public static final String CR_CODE_GB = 'GB';
	//逓増定期(GC)
	public static final String CR_CODE_GC = 'GC';
	//逓増定期(GD)
	public static final String CR_CODE_GD = 'GD';
	//逓増定期(GE)
	public static final String CR_CODE_GE = 'GE';
	//逓増定期(GF)
	public static final String CR_CODE_GF = 'GF';
	//逓増定期(GG)
	public static final String CR_CODE_GG = 'GG';
	//逓増定期(GH)
	public static final String CR_CODE_GH = 'GH';
	//逓増定期(UL)
	public static final String CR_CODE_UL = 'UL';
	//逓増定期(UM)
	public static final String CR_CODE_UM = 'UM';
	//逓増定期(UN)
	public static final String CR_CODE_UN = 'UN';
	//逓増定期(UO)
	public static final String CR_CODE_UO = 'UO';
	//逓増定期(UP)
	public static final String CR_CODE_UP = 'UP';
	//終身ガン(OF)
	public static final String CR_CODE_OF = 'OF';
	//終身ガン(OG)
	public static final String CR_CODE_OG = 'OG';
	//終身ガン(OH)
	public static final String CR_CODE_OH = 'OH';
	//終身ガン(OI)
	public static final String CR_CODE_OI = 'OI';
	//低解約定期(TL)
	public static final String CR_CODE_TL = 'TL';


	//エラーメッセージ
	public static final String ERROR_MESSAGE_CALLOUT_ATRIA_404 = '404:Atriaメンテナンス中、又はサービス時間外';
	public static final String ERROR_MESSAGE_CALLOUT_ATRIA_OTHER = 'Other:予期せぬエラー';
	public static final String ERROR_MESSAGE_CALLOUT_ATRIA_INVALID_RESPONSE = 'InvalidResponse:Atriaからの想定外のレスポンス';
	public static final String ERROR_MESSAGE_BIZLOGIC_TOO_MANY_NOT_STANDART = 'モータルクラスがS以外の特約が2件以上';
}