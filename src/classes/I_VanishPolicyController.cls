public with sharing class I_VanishPolicyController extends I_AbstractPolicyController{

	//保険契約ヘッダリスト
	public List<PolicyListRow> policyList {get; set;}
	//取得リスト行数
	public Integer listRows {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;


	// コンストラクタ
	public I_VanishPolicyController(){
		super();
		pageMessages = getPageMessages();

		//各フラグ　デフォルトはfalse
		//募集人表示フラグ
		contactFrag		= false;
		//MR名表示フラグ
		manegeMRFlag	= false;
		//事務所名表示フラグ
		officeFlag		= false;

		//表示行数初期化
		listRows = 0;
		//デフォルト行数20行
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		// デフォルトソート - 消滅日: 降順
		sortType = 'vanishDay';
		sortIsAsc = false;
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = this.menu.Name;
		if(pr==null){
			//ログイン判定
			//社員（募集人、事務所、担当MR）
			if(access.isEmployee()){
				contactFrag = true;
				officeFlag = true;
				manegeMRFlag = true;
			//AH（募集人・事務所）
			}else if(access.isAuthAH()){
				contactFrag = true;
				officeFlag = true;
			//AY(募集人)
			}else if(access.isAuthAY()){
				contactFrag = true;
			}

			if (String.isBlank(searchType)) {
				searchType = getSearchTypeOptions().get(0).getValue();
			}

			//抽出処理
			makePolicyList();

			//初回は消滅日でソート
			I_VanishPolicyController.list_sortType  = sortType;
			I_VanishPolicyController.list_sortIsAsc = sortIsAsc;
			policyList.sort();

		}

		return pr;
	}

	public void makePolicyList(){
		//保険契約ヘッダ表示用リスト
		policyList = new List<PolicyListRow>();

		String condition = createPolicyCondition();
		if (hasViewedByManager) {
			condition += createPolicyConditionByManager();
		}
		//保険契約ヘッダ取得用リスト
		List<E_Policy__c> policyRecs = E_PolicyDao.getRecsIRISVanishPolicy(condition);
		//0件の時エラー
		if(policyRecs.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('LIS|002'));
			return;
		}
		//最大件数以上の場合は警告を表示する
		if(policyRecs.size() >= I_Const.LIST_MAX_ROWS){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS).format()});
			pageMessages.addWarningMessage(errmessage);
		}

		// アクティブメニューキー（URLに付与することでアクティブメニューを保持）
		String irisMenu = '&' + getIRISMenuParameter();
		for(E_Policy__c pol : policyRecs){
			PolicyListRow plr = new PolicyListRow();

			//各項目に値をセット
			plr.vanishDay = E_Util.formatBirthday(pol.COMM_STATDATE__c);	//消滅日
			plr.vanishReason = pol.COMM_ZRSTDESC__c;						//消滅事由
			plr.contractorName = pol.COMM_ZCLNAME__c;						//契約者
			//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類設定項目を変更
			plr.policyType = pol.COLI_ZCOVRNAMES__c;						//保険種類
			plr.policyNum = pol.COMM_CHDRNUM__c.substringBeforeLast('|');	//証券番号 パイプ不要
			plr.contractDay = E_Util.formatBirthday(pol.COMM_OCCDATE__c);	//契約日

			plr.devName = pol.RecordType.DeveloperName;						//RecordTypeDeveloperName
			//RecordTypeDeveloperNameが個人保険消滅DB（EHLDPF）の場合はIRIS_DColiへ。SPVA消滅(EHDSPF）はE_Dspvaへ
			//個人保険消滅
			if(plr.devName == E_Const.POLICY_RECORDTYPE_DCOLI){
				plr.nextpageURL = 'IRIS_DColi?id=' + pol.Id + irisMenu;
				plr.nextpageURLTarget = '_blank';
				if(pol.COMM_SINSTAMT__c != null){ 
					plr.policyFee = pol.COMM_SINSTAMT__c.format();							//保険料:1回あたりの保険料
					plr.policyFeeSort = pol.COMM_SINSTAMT__c;								//ソート用
				}
			//SPVA消滅
			}else{
				//plr.insuredName = pol.COMM_ZINSNAM__c;										//被保険者（テキスト）
				plr.nextpageURL = 'E_Dspva?id=' + pol.Id + irisMenu;
				plr.nextpageURLTarget = '_self';
				if(pol.COMM_ZTOTPREM__c != null){ 
					plr.policyFee = pol.COMM_ZTOTPREM__c.format();							//保険料：払込保険料
					plr.policyFeeSort = pol.COMM_ZTOTPREM__c;								//ソート用
				}
			}

			//募集人--スプリントバックログ9(2017/1/18)
			plr.setValueAgent(pol.MainAgentName__c, pol.SubAgentName__c,pol.MainAgentNameKana__c,pol.SubAgentNameKana__c);
			//plr.setValueAgent(pol.MainAgentName__c, pol.SubAgentName__c);
			//事務所--スプリントバックログ9(2017/1/18)
			plr.setValueOffice(pol.MainAgentAccountName__c, pol.SubAgentAccountName__c,pol.MainAgentAccountNameKana__c,pol.SubAgentAccountNameKana__c);
			//plr.setValueOffice(pol.MainAgentAccountName__c, pol.SubAgentAccountName__c);
			//MR担当名
			plr.setValueMRName(pol.MainAgentAccountMRName__c,pol.SubAgentAccountMRName__c);

			policyList.add(plr);

		}
		listRows = policyList.size();//画面表示用リストサイズ

	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		//現在の表示行数+20
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		//現在の表示行数が1000より大きくなった場合、表示行数は1000
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * ソート
	 */
	public PageReference sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}

		I_VanishPolicyController.list_sortType  = sortType;
		I_VanishPolicyController.list_sortIsAsc = sortIsAsc;
		policyList.sort();

		return null;
	}


	// 画面表示行ラッパークラス
	public class PolicyListRow implements Comparable{
		//消滅日
		public String vanishDay {get; set;}
		//消滅事由
		public String vanishReason {get; set;}
		//保険種類
		public String policyType {get; set;}
		//証券番号
		public String policyNum {get; set;}
		//契約日
		public String contractDay{get; set;}
		//募集人
		public String agent {get; set;}
		//事務所
		public String office {get; set;}
		//レコードタイプ
		public String devName {get; set;}
		//MR名
		public String mrName {get; set;}
		//遷移先URL
		public String nextpageURL {get; set;}
		//遷移先URL_target
		public String nextpageURLTarget {get; set;}
		//契約者氏名（数式)（ページではこちらを出す）
		public String contractorName {get; set;}
		//保険料
		public String policyFee {get; set;}

		//ソート用
		public Decimal policyFeeSort;

		//スプリントバックログ9(2017/1/18) カナソート用追記
		//募集人カナ名
		public String agentSort{get; set;}
		//代理店事務所カナ名
		public String officeSort{get; set;}


		// コンストラクタ
		public PolicyListRow(){
			vanishDay		= '';
			vanishReason	= '';
			policyType 		= '';
			policyNum 		= '';
			contractDay 	= '';
			agent 			= '';
			office 			= '';
			mrName 			= '';
			nextpageURL 	= '';
			nextpageURLTarget = '';
			contractorName	= '';
			policyFee		= '0';

			policyFeeSort = 0;

			//スプリントバックログ9(2017/1/18) カナソート用追記
			agentSort		= '';
			officeSort		= '';
		}

		//事務所セット--スプリントバックログ9(2017/1/18)
		//public void setValueOffice(String main, String sub){
		public void setValueOffice(String main, String sub, String mainkana, String subkana){
			// 空でないほうをセット
			if(String.isNotBlank(main)){
				office = main;
				officeSort = mainkana;
			}else{
				office = sub;
				officeSort = subkana;
			}
			return;
		}

		// 募集人セット--スプリントバックログ9(2017/1/18)
		//public void setValueAgent(String main, String sub){
		public void setValueAgent(String main, String sub, String mainkana, String subkana){
			// 空でないほうをセット
			if(String.isNotBlank(main) ){
				agent = main;
				agentSort = mainkana;
			}else{
				agent = sub;
				agentSort = subkana;
			}
			return;
		}

		//担当MR名
		public void setValueMRName(String main, String sub){
			// 空でないほうをセット
			if(String.isNotBlank(main)){
				mrName = main;
			}else{
				mrName = sub;
			}
			return;
		}

		// ソート
		public Integer compareTo(Object compareTo){
			PolicyListRow compareToRow = (PolicyListRow)compareTo;
			Integer result = 0;

			// MR担当名
			if(list_sortType == 'mrName'){
				result = I_Util.compareToString(mrName, compareToRow.mrName);
			//保険料
			}else if(list_sortType == 'policyFee'){
				result = I_Util.compareToDecimal(policyFeeSort, compareToRow.policyFeeSort);
			// 事務所
			} else if(list_sortType == 'office'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(officeSort, compareToRow.officeSort);
				//result = I_Util.compareToString(office, compareToRow.office);
			// 募集人
			} else if(list_sortType == 'agent'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(agentSort, compareToRow.agentSort);
				//result = I_Util.compareToString(agent, compareToRow.agent);
			//消滅日
			} else if(list_sortType == 'vanishDay'){
				result = I_Util.compareToString(vanishDay, compareToRow.vanishDay);
			//消滅事由
			} else if(list_sortType == 'vanishReason'){
				result = I_Util.compareToString(vanishReason, compareToRow.vanishReason);
			//契約者
			} else if(list_sortType == 'contractorName'){
				result = I_Util.compareToString(contractorName, compareToRow.contractorName);
			//保険種類
			} else if(list_sortType == 'policyType'){
				result = I_Util.compareToString(policyType, compareToRow.policyType);
			//証券番号
			} else if(list_sortType == 'policyNum'){
				result = I_Util.compareToString(policyNum, compareToRow.policyNum);
			//契約日
			} else if(list_sortType == 'contractDay'){
				result = I_Util.compareToString(contractDay, compareToRow.contractDay);
			}

			return list_sortIsAsc ? result : result * (-1);
		}

	}

	//総行数
	public String getIsTotalRows(){

		//スプリントバックログ29(2017/1/18) 1000件の時1000+件表示
		String listSize = policyList.size().format();
		if(policyList.size() >= I_Const.LIST_MAX_ROWS){
			listSize += ' +';
		}

		return listSize;

		//return policyList.size().format();
	}

	//メニューキー
	protected override String getLinkMenuKey(){
		return 'disappearanceList';
	}

}