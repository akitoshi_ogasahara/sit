global with sharing class E_InveHistorySVEExtender extends E_AbstractSearchExtender {
	private static final String PAGE_TITLE = '取引履歴';
	
	public E_InveHistorySVEController extension;
	private String ithpfId = null;
	
	/** コンストラクタ */
	public E_InveHistorySVEExtender(E_InveHistorySVEController extension) {
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
		ithpfId = ApexPages.CurrentPage().getParameters().get('ithpfId');
		if(String.isBlank(ithpfId)){
			ithpfId = '';
		}
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, ithpfId);
		if(pageRef == null){
			if(String.isNotBlank(ithpfId)){
				SkyEditor2.Query tableQuery1 = extension.queryMap.get('dataTable');
				tableQuery1.addWhere('E_ITHPF__c',ithpfId,SkyEditor2.WhereOperator.Eq);
				extension.queryMap.put('dataTable',tableQuery1);
			}
		}
	}
	
	/** ページアクション */
	public PageReference pageAction(){
		if(pageRef == null){
			extension.doSearch();
		}
		return E_Util.toErrorPage(pageRef, null);
	}
	
	/** 個別チェック処理 */
	public override Boolean isValidate() {
		if (!E_Util.isValidateId(ithpfId, Schema.E_ITHPF__c.SObjectType)) {
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
			return false;
		}
		return true;
	}
}