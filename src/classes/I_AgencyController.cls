public with sharing class I_AgencyController extends I_AbstractController{

	//ページメッセージ
//	public E_PageMessagesHolder pageMessages {get;set;}
	public List<String> pageMessages {get;private set;}
	public Boolean getHasMessages(){
		return pageMessages <> null && !pageMessages.isEmpty();
	}
	// リスト行数
	public Integer rowCount {get; set;}

// 代理店一覧
	//MRSFID
	public String mrId {get; set;}
	// 代理店リスト
	public List<AgencyListRow> agencyRows {get{
		if(agencyRows != null){
			return agencyRows;
		}
			//表示件数が少ないため、全件を表示
			rowCount = I_Const.LIST_MAX_ROWS;
			// デフォルトソート - 代理人名
			sortType = SORT_TYPE_AGENCY;
			sortIsAsc = true;
			return getAgencyList();
		} set;}
	// 代理店リスト件数
	public Integer getAgencyCount() {
		return agencyRows == null ? 0 : agencyRows.size();
	}

// 事務所一覧
	// 代理店名
	public String parentName {get; set;}
	// 代理店SFID
	public String agencyId {get; set;}

	//事務所リスト
	public List<OfficeListRow> officeRows {get{
		if(officeRows != null){
			return officeRows;
		}
			sortType = SORT_TYPE_OFFICE;
			sortIsAsc = true;

			officeRows = getOfficeList(agencyid);

			if(employeeFlag){
				I_AgencyController.list_sortType  = sortType;
				I_AgencyController.list_sortIsAsc = sortIsAsc;
				officeRows.sort();
			}
			return officeRows;
		} set;}
	// 事務所一覧
	public List<Account> offices {get; set;}
	// 事務所一覧取得件数
	public Integer getOfficeCount() {
		return offices == null ? 0 : offices.size();
	}

// 共通
	// 表示制御
	// 代理店-事務所切り替えタブの表示非表示フラグ
	public Boolean isOfficeList {
		get{
			if(isOfficeList){
				officeTab = true;
				if(officeRows==null) readyAction();
			}
			return isOfficeList;
		} set;}
	//　代理店-事務所タブの切り替え用
	public Boolean officeTab {get; set;}
	public void setofficeList(){
		//代理店事務所情報初期化
		agencyRows = new List<AgencyListRow>();
		offices = new List<Account>();
		officeRows = new List<OfficeListRow>();

		this.officeTab = ApexPages.currentPage().getParameters().get('officeTab')=='true' ? true : false;
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		readyAction();
		srTab = 'top';
	}
	// 基本情報-挙績情報(AG)-挙績情報(MR)切り替え用
	public String srTab {get; set;}
	public void selectTab(){
		srTab = ApexPages.currentPage().getParameters().get('srTab');

		//修正
		if(srTab == 'ag'){
			sortType = SORT_TYPE_NBCNPYTD;
			sortIsAsc = false;
		}else if(srTab == 'mr'){
			sortType = SORT_TYPE_TOTALYTD;
			sortIsAsc = false;
		}else{

		sortType = officeTab ? 'office' : 'agency';
		sortIsAsc = true;
		}
		I_AgencyController.list_sortType  = sortType;
		I_AgencyController.list_sortIsAsc = sortIsAsc;

		if(officeTab){
			officeRows = getOfficeList(agencyid);
			if(employeeFlag){
				officeRows.sort();
			}
		}else{
			agencyRows.sort();
		}

	}
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	//MAX表示件数
	public Integer getListMaxRows(){
		return I_Const.LIST_MAX_ROWS;
	}
	//社員フラグ
	public Boolean employeeFlag{get; set;}	// 本社営業部情報を代理店には非表示20170911

	//データ基準日
	public String bizDate{
		get{
			if(bizDate != null) return this.bizDate;
			String inquiryD = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
			String year = inquiryD.left(4);
			String month = inquiryD.mid(4, 2);
			String day = inquiryD.mid(6, 2);
			return year+'/'+month+'/'+day;
		} set;
	}

	//営業日

	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE		 = 'st';
	// URLパラメータ名 - フィルタID
	public static final String URL_PARAM_FILTER_ID		 = 'fid';
	// 代理店一覧用ソートキー
	public static final String SORT_TYPE_AGENCY			 = 'agency';
	public static final String SORT_TYPE_ZHEADAY		 = 'zheaday';
	public static final String SORT_TYPE_AGENTADDRESS	 = 'agentaddress';
	// 事務所一覧用ソートキー
	public static final String SORT_TYPE_OFFICE			 = 'office';
	public static final String SORT_TYPE_OFFICEADDRESS	 = 'officeaddress';
	public static final String SORT_TYPE_ZAGCYNUM		 = 'zagcynum';
	public static final String SORT_TYPE_PHONE			 = 'phone';
	public static final String SORT_TYPE_AGNTBR			 = 'agntbr';
	public static final String SORT_TYPE_MR				 = 'mr';
	public static final String SORT_TYPE_ISMOFFICE		 = 'isMOffice';	//本社営業部担当追加 20170816
	public static final String SORT_TYPE_ZUBSMAIN		 = 'zubsmain';	//母店担当追加 20170816
	//挙績情報 - ソートキー
	public static final String SORT_TYPE_NBCNPYTD		 = 'nbcnp_ytd';
	public static final String SORT_TYPE_NBCANPSPYTD	 = 'nbcanp_spytd';
	public static final String SORT_TYPE_IANP			 = 'ianp';
	public static final String SORT_TYPE_IQA			 = 'iqa';
	public static final String SORT_TYPE_TOTALYTD 		 = 'totalYTD';
	public static final String SORT_TYPE_FSYTD			 = 'fsYTD';
	public static final String SORT_TYPE_PROYTD			 = 'proYTD';
	public static final String SORT_TYPE_DEFRATE		 = 'defRate';
	public static final String SORT_TYPE_AGENTCOUNT		 = 'agentCount';

	// 代理店一覧用ソート項目
	//public static final String SORT_FIELD_AGENCY		 = 'Parent.Name';
	public static final String SORT_FIELD_AGENCY		 = 'E_CL1PF_ZAHKNAME__c';	//スプリントバックログ9(2017/1/18)。代理店格カナ名
	public static final String SORT_FIELD_ZHEADAY		 = 'Parent.E_CL1PF_ZHEADAY__c';
	public static final String SORT_FIELD_AGENTADDRESS	 = 'Parent.E_COMM_ZCLADDR__c';
	// 事務所一覧用ソート項目
	//public static final String SORT_FIELD_OFFICE		 = 'Name';
	public static final String SORT_FIELD_OFFICE		 = 'E_CL2PF_ZEAYKNAM__c';	//スプリントバックログ9(2017/1/18)。代理店事務所カナ名
	public static final String SORT_FIELD_ZAGCYNUM		 = 'E_CL2PF_ZAGCYNUM__c';
	public static final String SORT_FIELD_OFFICEADDRESS	 = 'E_COMM_ZCLADDR__c';
	public static final String SORT_FIELD_PHONE			 = 'CLTPHONE01__c';
	public static final String SORT_FIELD_AGNTBR		 = 'AGNTBR_NM__c';
	public static final String SORT_FIELD_MR			 = 'Owner.Name';
	public static final String SORT_FIELD_ISMOFFICE		 = 'isMOffice__c';	//本社営業部担当追加 20170816
	public static final String SORT_FIELD_ZUBSMAIN		 = 'zubsMain__c';	//母店担当追加 20170816

	// ソートキー：項目変換Map
	public static Map<String, String> sortMap = new Map<String, String>{
		SORT_TYPE_OFFICE => SORT_FIELD_OFFICE
		,SORT_TYPE_OFFICEADDRESS => SORT_FIELD_OFFICEADDRESS
		,SORT_TYPE_ZAGCYNUM => SORT_FIELD_ZAGCYNUM
		,SORT_TYPE_PHONE => SORT_FIELD_PHONE
		,SORT_TYPE_AGNTBR => SORT_FIELD_AGNTBR
		,SORT_TYPE_MR => SORT_FIELD_MR
		,SORT_TYPE_ISMOFFICE => SORT_FIELD_ISMOFFICE	//本社営業部担当追加 20170816
		,SORT_TYPE_ZUBSMAIN => SORT_FIELD_ZUBSMAIN	//母店担当追加 20170816
	};

	//表示制御
	public Boolean isviewAll {get;private set;}
	//支社コード
	public String brunchCode {get;private set;}

	//挙績情報（社内挙績情報） 権限セット付与チェック
	public Boolean viewSR{
		get{
			if(access.hasSREmployeePermission()){
				return true;
			}
			return false;
		} set;
	}

	// コンストラクタ
	public I_AgencyController() {
//		super();
		pageMessages = new List<String>();
		officeTab = false;

		// デフォルトソート - 昇順
		sortIsAsc = true;

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch(Exception e){
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}

		employeeFlag = access.isEmployee() ? TRUE : FALSE;	// 本社営業部情報を代理店には非表示20170911
		srTab = 'top';
	}

	public void readyAction(){
		if (officeTab) {
			// デフォルトソート - 事務所名
			sortType = SORT_TYPE_OFFICE;
			sortIsAsc = true;
			officeRows = getOfficeList(agencyid);
		} else {
			//表示件数が少ないため、全件を表示
			rowCount = I_Const.LIST_MAX_ROWS;
			// デフォルトソート - 代理人名
			sortType = SORT_TYPE_AGENCY;
			sortIsAsc = true;
			getAgencyList();
		}
	}

	/**
	 * 代理店一覧作成
	 */
	private List<AgencyListRow> getAgencyList() {

		String brunchCode = access.idcpf.ZINQUIRR__c.right(2);
		if(String.isBlank(brunchCode)){
			brunchCode = '**';
		}

		// 社員のときしか呼び出されないのでDaoで良い
//		List<Account> agencies = E_AccountDao.getRecsByMRCode(brunchCode);
		//担当する事務所を表示
		List<Account> agencies = E_AccountDao.getRecsByMRId(mrId,brunchCode);

		//最大件数以上の場合は警告を表示
		if(agencies.size() >= I_Const.LIST_MAX_ROWS){
			pageMessages.add(getMSG().get('LIS|004'));
		}

		Set<Id> parentAccId = new Set<Id>();
		for (Account acc : agencies) {
			parentAccId.add(acc.Parent.Id);
		}

		//挙績情報
		List<E_AgencySalesResults__c> agSRList = E_AgencySalesResultsDaoWithout.getSRbyaccId(parentAccId);

		// key:代理店Id value:代理店挙績情報
		Map<Id, E_AgencySalesResults__c> srMap = new Map<Id, E_AgencySalesResults__c>();
		for(E_AgencySalesResults__c s : agSRList){
			srMap.put(s.ParentAccount__c, s);
		}

		// 重複チェック用のIDセット
		Set<Id> agencyIdSet = new Set<Id>();
		agencyRows = new List<AgencyListRow>();
		for (Account acc : agencies) {
			// 重複チェック
			if (!agencyIdSet.contains(acc.Parent.Id)) {
				AgencyListRow row = new AgencyListRow();
				row.id = acc.Parent.Id;
				row.agency = acc.Parent.Name;
				row.zheaday = acc.Parent.E_CL1PF_ZHEADAY__c;
				row.address = acc.Parent.E_COMM_ZCLADDR__c;

				//スプリントバックログ9(2017/1/18) カナソート用追記
				row.agencySort = acc.Parent.E_CL1PF_ZAHKNAME__c;

				//if (acc.E_CL1PF_ZBUSBR__c == acc.E_CL2PF_BRANCH__c) {
				//	row.zubsmain = I_Const.AGENT_VALUE_ZAGCYNUM;
				//} else {
				//	row.zubsmain = '';
				//}

				row.isMOffice = (Integer)acc.isMOffice__c;	//本社営業部担当追加 20170824

				if(srMap.get(acc.Parent.Id) != null){
					E_AgencySalesResults__c sr = srMap.get(acc.Parent.Id);
					row.nbcnp_ytd = null2Zero(sr.NBCANP_YTD__c);
					row.nbcanp_spytd = null2Zero(sr.NBCANP_SpInsTypeYTD__c);
					row.ianp = null2Zero(sr.IANP__c);
					row.iqa = null2Zero(sr.IQA__c);
					row.totalYTD = null2Zero(sr.NBWANP_TotalYTD__c);
					row.fsYTD = null2Zero(sr.NBWANP_FSYTD__c);
					row.proYTD = null2Zero(sr.NBWANP_ProtectionYTD__c);
					row.defRate = null2Zero(sr.DefactRate__c);
					row.agentCount = null2Zero(sr.ActiveAgent__c);
				}else{
					row.nbcnp_ytd = 0;
					row.nbcanp_spytd = 0;
					row.ianp = 0;
					row.iqa = 0;
					row.totalYTD = 0;
					row.fsYTD = 0;
					row.proYTD = 0;
					row.defRate = 0;
					row.agentCount = 0;
				}

				agencyRows.add(row);
				if(agencyRows.size() >= 1000){
					break;
				}
			}
			agencyIdSet.add(acc.Parent.Id);
		}

		// ソート
		I_AgencyController.list_sortType  = sortType;
		I_AgencyController.list_sortIsAsc = sortIsAsc;
		agencyRows.sort();
		return agencyRows;
	}
	private Decimal null2Zero(Decimal dec){
		return dec == null ? 0 : dec;
	}
	/**
	 * 事務所一覧作成
	 */
	private  List<OfficeListRow> getOfficeList(String Id) {
		String sortField = '';
		if(sortMap.containskey(sortType)){
			sortField = sortMap.get(sortType);
		}

		brunchCode = '';
		if(access.isEmployee()){
			brunchCode = access.idcpf.ZINQUIRR__c.right(2);
		}

		// MRの場合
		Boolean isAllOffice = true;
		isviewAll = true;
		if(access.isEmployee() && brunchCode != '**'){
			// 自拠点が母店を担当しているかチェック
			if(E_AccountDao.getCountIsMotherOffice(Id, brunchCode) <= 0){
//				isAllOffice = false;
				isviewAll = false;
			}
		}

		// DAO呼び出し without にする
		String wheresqol = '';
		if(String.isNotBlank(mrid)){
			wheresqol = ' AND Owner.id = \''+ mrId + '\' ';
		}else{
			wheresqol = ' AND Parent.Id = \'' + id + '\' ';
		}
			offices = E_AccountDaoWithout.getRecsByAgency(brunchCode, isAllOffice, sortField, sortIsAsc, wheresqol);
		if(offices != null && !offices.isEmpty()){
			parentName = offices.get(0).Parent.Name;
		}
		if(offices.size() >= I_Const.LIST_MAX_ROWS){
			pageMessages.add(getMSG().get('LIS|004'));
		}
		officeRows = new List<OfficeListRow>();
		for(Account office :offices){
			officeRows.add(new OfficeListRow(office,brunchCode));
		}

		return officeRows;
	}

	/**
	 * ソート
	 */
	public void sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		// ソート方法 - SOQL
		if (sortType != sType) {
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		I_AgencyController.list_sortType  = sortType;
		I_AgencyController.list_sortIsAsc = sortIsAsc;

		if(officeTab && (srTab == 'ag' || srTab == 'mr')){
			officeRows.sort();
		}else if(officeTab){
			officeRows = getOfficeList(agencyId);
		}else{
			agencyRows.sort();
		}

		return;
	}

	/**
	 * 契約者一覧へ遷移
	 */
	public PageReference moveToOwnerList() {
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);
		String name = E_AccountDaoWithout.getRecById(id).Name;

		List<String> breadCrumb = new List<String>();
		String field = '';
		if (officeTab) {
			field = I_Const.CONDITION_FIELD_OFFICE;
			breadCrumb.add(parentName);
		} else {
			field = I_Const.CONDITION_FIELD_AGENCY;
		}
		breadCrumb.add(name);

		iris.setPolicyCondition(field, name, id, String.join(breadCrumb, '|'));

		PageReference pr = Page.IRIS_Owner;
		pr.getParameters().put(I_Const.URL_PARAM_MENUID, iris.irisMenus.menuItemsByKey.get('ownerList').record.Id);
		pr.setRedirect(true);

		return pr;
	}

	/**
	 * 新契約一覧へ遷移
	 */
	public PageReference moveToNewPolicyList() {
		//ToDo共通化
		String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);
		String name = E_AccountDaoWithout.getRecById(id).Name;

		List<String> breadCrumb = new List<String>();
		String field = '';
		if (officeTab) {
			field = I_Const.CONDITION_FIELD_OFFICE;
			breadCrumb.add(parentName);
		} else {
			field = I_Const.CONDITION_FIELD_AGENCY;
		}
		breadCrumb.add(name);

		iris.setPolicyCondition(field, name, id, String.join(breadCrumb, '|'));

		PageReference pr = Page.IRIS_NewPolicy;
		pr.setRedirect(true);

		return pr;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		Integer totalRow = 0;
		if(officeTab){
			totalRow = offices  == null ? 0 : offices.size();
		} else {
			totalRow = agencyRows == null ? 0 : agencyRows.size();
		}
		return totalRow;
	}

	/**
	 * 内部クラス
	 */
	public class AgencyListRow implements Comparable {
		// 代理店SFID
		public String id {get; set;}
		// 代理店名
		public String agency {get; set;}
		// 代理店格コード
		public String zheaday {get; set;}
		// 住所
		public String address {get; set;}
		//エンコード住所
		public String encordAddress {
			get{
				return EncodingUtil.urlEncode(address , 'UTF-8');
			}
			set;
		}
		// 母店担当
		//public String zubsmain {get; set;}

		//スプリントバックログ9(2017/1/18) カナソート用変数
		public String agencySort{get; set;}

		// 本社営業部担当
		public Integer isMOffice{get; set;}

		//挙績情報（AG）
		// 係数ANP(YTD)
		public Decimal nbcnp_ytd {get; set;}
		// 係数ANP（特定）(YTD)
		public Decimal nbcanp_spytd {get; set;}
		// 保有ANP(YTD)
		public Decimal ianp {get; set;}
		// IQA継続率＊ (YTD)
		public Decimal iqa {get; set;}

		//挙績情報（MR）
		// Total WAPE(YTD)
		public Decimal totalYTD {get; set;}
		// FS WAPE(YTD)
		public Decimal fsYTD {get; set;}
		// Protection WAPE(YTD)
		public Decimal proYTD {get; set;}
		// 単純不備率(YTD)
		public Decimal defRate {get; set;}
		// 稼動募集人数
		public Decimal agentCount {get; set;}

		// コンストラクタ
		public AgencyListRow() {
			id = '';
			agency = '';
			zheaday = '';
			address = '';
			//zubsmain = '';
			isMOffice = 1;

			//スプリントバックログ9(2017/1/18) カナソート用変数
			agencySort = '';
			nbcnp_ytd = null;
			nbcanp_spytd = null;
			ianp = null;
			iqa = null;
			totalYTD = null;
			fsYTD = null;
			proYTD = null;
			defRate = null;
			agentCount = null;
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			AgencyListRow compareToRow = (AgencyListRow)compareTo;
			Integer result = 0;

			// 代理店名
			if (list_sortType == SORT_TYPE_AGENCY) {
				//スプリントバックログ9(2017/1/18) カナソート用
				result = I_Util.compareToString(agencySort, compareToRow.agencySort);
				//result = I_Util.compareToString(agency, compareToRow.agency);
			// 代理店コード
			} else if (list_sortType == SORT_TYPE_ZHEADAY) {
				result = I_Util.compareToString(zheaday, compareToRow.zheaday);
			// 住所
			} else if (list_sortType == SORT_TYPE_AGENTADDRESS) {
				result = I_Util.compareToString(address, compareToRow.address);
			// 母店担当
			//} else if (list_sortType == SORT_TYPE_ZUBSMAIN) {
			//	result = I_Util.compareToString(zubsmain, compareToRow.zubsmain);
			//挙積対応(2017/7/27)担当拠点
			//本社営業部担当 20170824追加
			} else if (list_sortType == SORT_TYPE_ISMOFFICE) {
				result = I_Util.compareToInteger(isMOffice, compareToRow.isMOffice);
			// 係数ANP(YTD)
			} else if (list_sortType == SORT_TYPE_NBCNPYTD) {
				result = compareToDecimal(nbcnp_ytd, compareToRow.nbcnp_ytd);
			// 係数ANP（特定）(YTD)
			} else if (list_sortType == SORT_TYPE_NBCANPSPYTD) {
				result = compareToDecimal(nbcanp_spytd, compareToRow.nbcanp_spytd);
			// 保有ANP(YTD)
			} else if (list_sortType == SORT_TYPE_IANP) {
				result = compareToDecimal(ianp, compareToRow.ianp);
			// IQA継続率＊ (YTD)
			} else if (list_sortType == SORT_TYPE_IQA) {
				result = compareToDecimal(iqa, compareToRow.iqa);
			// Total WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_TOTALYTD) {
				result = compareToDecimal(totalYTD, compareToRow.totalYTD);
			// FS WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_FSYTD) {
				result = compareToDecimal(fsYTD, compareToRow.fsYTD);
			// Protection WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_PROYTD) {
				result = compareToDecimal(proYTD, compareToRow.proYTD);
			// 単純不備率(YTD)
			} else if (list_sortType == SORT_TYPE_DEFRATE) {
				result = compareToDecimal(defRate, compareToRow.defRate);
			// 稼動募集人数(月平均)
			} else if (list_sortType == SORT_TYPE_AGENTCOUNT) {
				result = compareToDecimal(agentCount, compareToRow.agentCount);
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * 内部クラス
	 */
	public class OfficeListRow implements Comparable {
		// 担当レベル
		public String chargeLevel {get; set;}
		//Account
		public Account acc {get;set;}
		//E_AgencySalesResults
		public E_AgencySalesResults__c sr {get;set;}

		//public OfficeListRow(){
		//	chargeLevel = '';
		//	acc = new Account();
		//	sr = new E_AgencySalesResults__c();
		//}
		public OfficeListRow(Account office,String brunchCode){
			if(office.OwnerId == UserInfo.getUserId()){
				chargeLevel = '1';
			}else if(office.E_CL2PF_BRANCH__c == brunchCode){
				chargeLevel = '2';
			}else{
				chargeLevel = '3';
			}
			acc = office;
			sr = office.E_AgencySalesResults__r.size() != 0 ? office.E_AgencySalesResults__r : new E_AgencySalesResults__c();
		}

		public Integer compareTo(Object compareTo) {
			OfficeListRow compareToRow = (OfficeListRow)compareTo;
			Integer result = 0;
			result = I_Util.compareToString(chargeLevel, compareToRow.chargeLevel);
			// 係数ANP(YTD)
			if (list_sortType == SORT_TYPE_NBCNPYTD) {
				result = compareToDecimal(sr.NBCANP_YTD__c, compareToRow.sr.NBCANP_YTD__c);
			// 係数ANP（特定）(YTD)
			} else if (list_sortType == SORT_TYPE_NBCANPSPYTD) {
				result = compareToDecimal(sr.NBCANP_SpInsTypeYTD__c, compareToRow.sr.NBCANP_SpInsTypeYTD__c);
			// 保有ANP（当年）
			} else if (list_sortType == SORT_TYPE_IANP) {
				result = compareToDecimal(sr.IANP__c, compareToRow.sr.IANP__c);
			// Total WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_TOTALYTD) {
				result = compareToDecimal(sr.NBWANP_TotalYTD__c, compareToRow.sr.NBWANP_TotalYTD__c);
			// FS WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_FSYTD) {
				result = compareToDecimal(sr.NBWANP_FSYTD__c, compareToRow.sr.NBWANP_FSYTD__c);
			// Protection WAPE(YTD)
			} else if (list_sortType == SORT_TYPE_PROYTD) {
				result = compareToDecimal(sr.NBWANP_ProtectionYTD__c, compareToRow.sr.NBWANP_ProtectionYTD__c);
			// 稼動募集人数(月平均)
			} else if (list_sortType == SORT_TYPE_AGENTCOUNT) {
				result = compareToDecimal(sr.ActiveAgent__c, compareToRow.sr.ActiveAgent__c);
			//本社営業部担当 20170824追加
			} else if (list_sortType == SORT_TYPE_ISMOFFICE) {
				result = compareToDecimal(acc.isMOffice__c, compareToRow.acc.isMOffice__c);
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * ソート判定（Decimal）
	 * @paarm  Decimal：比較元
	 * @paarm  Decimal：比較先
	 * @return Decimal:比較結果
	 */
	public static Integer compareToDecimal(Decimal compare, Decimal compareTo){
		Integer result = 0;
		if(compare==null) return 1;
		if(compareTo==null) return -1;
		if(compare == compareTo){
			result = 0;
		} else if(compare > compareTo){
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

}