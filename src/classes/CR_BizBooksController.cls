global with sharing class CR_BizBooksController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CR_Agency__c record {get{return (CR_Agency__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public CR_BizBooksExtender getExtender() {return (CR_BizBooksExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public CR_BizBooksController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','business_book');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','代理店検索(帳簿書類)');
        tmpPropMap.put('p_escapeTitle','False');
        tmpPropMap.put('p_rendered','{!Not(isBlank(Extender.UrlforSearchBizBooks)) && Extender.isEmployee}');
        tmpPropMap.put('p_value','{!Extender.urlforSearchBizBooks}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2040');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2040',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','ご利用方法');
        tmpPropMap.put('p_escapeTitle','False');
        tmpPropMap.put('p_rendered','{!Extender.isSuccessInit}');
        tmpPropMap.put('p_value','{!Extender.UrlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2115');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2115',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2076');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2076',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_selectItems','{!Extender.officeOptions}');
        tmpPropMap.put('p_selectedValues','{!Extender.selOfficeList}');
        tmpPropMap.put('p_isSupportBrowser','');
		tmpPropMap.put('Component__Width','400');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','Component2077');
		tmpPropMap.put('Component__Name','E_MultiSelect');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2077',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isMonthPicker','true');
        tmpPropMap.put('p_isMultiple','false');
        tmpPropMap.put('p_layout','');
        tmpPropMap.put('p_startDate','2016');
        tmpPropMap.put('p_endDate','2050');
        tmpPropMap.put('p_fromDate','{!Extender.outputRecordYm}');
        tmpPropMap.put('p_toDate','');
        tmpPropMap.put('p_useOnlyJavaScript','False');
		tmpPropMap.put('Component__Width','102');
		tmpPropMap.put('Component__Height','30');
		tmpPropMap.put('Component__id','Component2031');
		tmpPropMap.put('Component__Name','EYmPicker');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2031',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_sbmtFiles','{!extender.historyList}');
        tmpPropMap.put('p_conditionsKey','');
        tmpPropMap.put('p_conditionsLabel','');
		tmpPropMap.put('Component__Width','400');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','Component2055');
		tmpPropMap.put('Component__Name','CR_DownloadHistoryList');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2055',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = CR_Agency__c.fields.AgencyName__c;
		f = CR_Agency__c.fields.AgencyCode__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CR_Agency__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('CR_Agency__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('AgencyName__c');
			mainQuery.addFieldAsOutput('AgencyCode__c');
			mainQuery.addFieldAsOutput('Account__c');
			mainQuery.addFieldAsOutput('LargeAgency__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new CR_BizBooksExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}