public without sharing class MNT_API_MockController {
	//コミュニティのURLを設定
	//環境に合わせて設定
	public static String COMMUNITY_URI ='https://nnlife-jp.force.com/nnlinkcli';
	
	public static String AUTH_URI = '/services/oauth2/authorize';
	
	//接続アプリケーションのコンシューマ鍵を設定
	//環境に合わせて設定
	public static String CLIENT_ID = '3MVG9I1kFE5Iul2BJMpKRGnVtXkRQwJt43SZjkZan.lbznj.626KbW0o0XAA81GxmgYawkV2NFSVt3AhR8x4f';
	
	//コールバックURLの値
	//環境に合わせて設定
	public static String REDIRECT_URI = 'https://nnlife-jp--c.ap3.visual.force.com/apex/MNT_API_Mock';
	
	//接続アプリケーションのコンシューマの秘密を設定
	//環境に合わせて設定
	public static String CLIENT_SECRET = '7010355054755600470';

	Map<String,String> params = new Map<String,String>{
										'response_type' => 'code'
										,'client_id' => CLIENT_ID
										,'scope' => 'api'
										,'display' => 'popup'};

	public String apiName{get;set;}
	public String accessCode{get; private set;}
	public Boolean accessflg {get;set;}
	public String token{get; private set;}
	public String parsedResponse{get;private set;}

	//コンストラクタ
	public MNT_API_MockController(){
		accessflg = false;
		accessCode = ApexPages.currentPage().getParameters().get('code');
		apiName='policies/doGet';
	}

	//OAuth認証
	public pageReference step1(){
		String url = COMMUNITY_URI + AUTH_URI + '?';
		List<String> paramStrings = new List<String>();
		for(String key:params.keyset()){
			paramStrings.add(key + '=' + params.get(key));  
		}
		paramStrings.add('redirect_uri=' + EncodingUtil.urlEncode(REDIRECT_URI, 'UTF-8'));
		
		url += string.join(paramStrings, '&');
		return new PageReference(url);
	}



	public PageReference init(){
		//accesscodeからTokenの取得
		try{
			if(accessCode != null){
				accessflg = true;
				String endpoint = ApexPages.currentPage().getParameters().get('sfdc_community_url')+'/services/oauth2/token';
				HttpRequest req = new HttpRequest();
				req.setEndpoint(endpoint);
				req.setMethod('POST');
				String params = 'grant_type=authorization_code&code=' + accessCode;
				params += '&client_id=' + CLIENT_ID;
				params += '&client_secret=' + CLIENT_SECRET;
				params += '&redirect_uri=' + REDIRECT_URI;
				
				req.setBody(params);
				
				//テスト実行時はコールアウト実行しない
				if(!System.Test.isRunningTest()){
					Http http = new Http();
					HttpResponse response = http.send(req);
					parsedResponse = response.getBody();
					Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(parsedResponse);

					token = String.valueOf(responseBody.get('access_token'));
				}
			}
		}catch(Exception e){
			
			parsedResponse = e.getMessage();
		}
		return null;
	}

	//APIコールメソッド
	public PageReference callRest(){
		try{
			parsedResponse = null;
			String endpoint = COMMUNITY_URI;
			endpoint += '/services/apexrest/' + apiName;
			
			HttpRequest req = new HttpRequest();
			req.setEndpoint(endpoint);
			req.setMethod('GET');

			req.setHeader('Authorization','Bearer '+token);


			//テスト実行時はコールアウト実行しない
			if(!System.Test.isRunningTest()){
				Http http = new Http();
				HTTPResponse res = http.send(req);
				parsedResponse = res.getBody();
			}
		}catch(Exception e){
			parsedResponse = e.getMessage();
		}
		return null;
	}
}