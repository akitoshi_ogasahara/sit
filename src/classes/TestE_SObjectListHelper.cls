/**
 */
@isTest
private class TestE_SObjectListHelper {

	static testMethod void test_doDelete() {
		Account acc = TestE_TestUtil.createAccount(true);
		
		String accId = acc.Id;
		
		Test.startTest();
		E_SObjectListHelper slh = new E_SObjectListHelper();
		slh.delRecordId = accId;
		slh.delSObjectName = 'Account';
		
		slh.doDelete();
		
		Test.stopTest();
		
		List<Account> accList = [SELECT Id FROM Account WHERE Id =:accId];
		System.assertEquals(0, accList.size());
	}
}