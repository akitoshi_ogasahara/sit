/*
 * CC_ContactVOIBizLogicTest
 * Test class of CC_ContactVOIBizLogic
 * created  : Accenture 2018/7/9
 * modified :
 */

@isTest
private class CC_ContactVOIBizLogicTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {
			//Create agent and client
			List<RecordType> recordTypeListOfContact = [SELECT Id, Name FROM RecordType where SobjectType = 'Contact' and Name in ('募集人', '顧客') ];
			List<Contact> contactList = new List<Contact>();
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account newAccount = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert newAccount;

			for(Integer i = 0; i <= 1; i++){
				contactList.add(new Contact(
								FirstName = 'test' +i,
								LastName = 'Contact' +i
								));
			}

			for(RecordType recordType : recordTypeListOfContact){
				if(recordType.Name.equals('募集人')){
					contactList[0].RecordTypeId = recordType.Id;
					contactList[0].AccountId = newAccount.Id;
				}else if(recordType.Name.equals('顧客')){
					contactList[1].RecordTypeId = recordType.Id;
					contactList[1].AccountId = newAccount.Id;
				}
			}
			Insert contactList;
		}
	}

	/**
	* Test saveAgent()
	* with no error
	*/
	static testMethod void saveAgentTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get angent
			Contact contact = [Select Id from Contact where FirstName = 'test0' and LastName = 'Contact0' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			inputMap.put('[\'EditableAgent\'][\'MobilePhone\']', '07012341234');
			inputMap.put('[\'EditableAgent\'][\'OtherPhone\']', '08012341234');
			inputMap.put('[\'EditableAgent\'][\'Email\']', 'testClass@test.com');
			inputMap.put('[\'EditableAgent\'][\'CC_AttachmentPassword__c\']', '1234567890');
			inputMap.put('[\'EditableAgent\'][\'Description\']', 'This is a test!');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveAgent(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAgent()
	* Standard error: Email
	*/
	static testMethod void saveAgentTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get angent
			Contact contact = [Select Id from Contact where FirstName = 'test0' and LastName = 'Contact0' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			inputMap.put('[\'EditableAgent\'][\'Email\']', 'testClass@test.com@@@');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveAgent(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAgent()
	* Standard error: CC_AttachmentPassword__c
	*/
	static testMethod void saveAgentTest03() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get angent
			Contact contact = [Select Id from Contact where FirstName = 'test0' and LastName = 'Contact0' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			String password = 'パスワードパスワードパスワードパスワードパスワードパスワードパスワードパスワードパスワードパスワードあ';
			inputMap.put('[\'EditableAgent\'][\'CC_AttachmentPassword__c\']', password);

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveAgent(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAgent()
	* Custom error: MobilePhone, OtherPhone
	*/
	static testMethod void saveAgentTest04() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get angent
			Contact contact = [Select Id from Contact where FirstName = 'test0' and LastName = 'Contact0' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			inputMap.put('[\'EditableAgent\'][\'MobilePhone\']', '!!!');
			inputMap.put('[\'EditableAgent\'][\'OtherPhone\']', 'wwww');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveAgent(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveAgent()
	* blank data
	*/
	static testMethod void saveAgentTest05() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get angent
			Contact contact = [Select Id from Contact where FirstName = 'test0' and LastName = 'Contact0' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			inputMap.put('[\'EditableAgent\'][\'MobilePhone\']', '');
			inputMap.put('[\'EditableAgent\'][\'OtherPhone\']', '');
			inputMap.put('[\'EditableAgent\'][\'Email\']', '');
			inputMap.put('[\'EditableAgent\'][\'CC_AttachmentPassword__c\']', '');
			inputMap.put('[\'EditableAgent\'][\'Description\']', '');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveAgent(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveClient()
	* data input
	*/
	static testMethod void saveClientTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get client
			Contact contact = [Select Id from Contact where FirstName = 'test1' and LastName = 'Contact1' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			inputMap.put('[\'EditableAgent\'][\'Description\']', 'This is a test!');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveClient(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test saveClient()
	* blank data
	*/
	static testMethod void saveClientTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get client
			Contact contact = [Select Id from Contact where FirstName = 'test1' and LastName = 'Contact1' limit 1];

			//Create data Map
			Map<String,Object> inputMap = new Map<String,Object>();
			inputMap.put('targetId', contact.Id);
			inputMap.put('[\'EditableAgent\'][\'Description\']', '');

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_ContactVOIBizLogic.saveClient(inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}