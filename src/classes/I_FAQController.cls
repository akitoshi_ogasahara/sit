public with sharing class I_FAQController extends I_AbstractController{		/*TODO I_AbstractControllerにする　CMS機能は不要*/

	// コンストラクタ
	public I_FAQController() {

	}

	//メニューIdを設定
	protected virtual override Id getIrisMenuId(){
		return null;
	}

	// よくあるご質問
	public List<I_CMSSection> getFAQContentsIndexes(){
		List<I_CMSSection> ret = new List<I_CMSSection>();

		// コンポーネント情報を作成
		//for (I_ContentMaster__c cmSec: I_ContentMasterDao.getRecordsByMenuCategory('5.よくあるご質問')) {
		String menuName = access.canViewSumiseiIRIS() ? '5.よくあるご質問(住友生命)' : '5.よくあるご質問';
		for (I_ContentMaster__c cmSec: I_ContentMasterDao.getRecordsByMenuCategory(menuName)) {

			if(cmSec.Name == 'お問い合せ') {
				continue;
			}

			I_CMSSection section = new I_CMSSection(cmSec);

			// コンテンツ情報を作成
			for (I_ContentMaster__c cmCntn: cmSec.ChildContents__r) {
				section.addCMSComp(cmCntn, null, null);
			}
			ret.add(section);
		}
		return ret;
	}

	//　FAQの先頭何件を表示するか？
	public Integer getVisibleRows(){
		return 3;
	}

	public String getSumiseiParam(){
		return access.canViewSumiseiIRIS() && access.isEmployee() ? '&isSMTM=1' : '';
	}


}