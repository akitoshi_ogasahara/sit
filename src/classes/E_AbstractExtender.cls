/**
 * 基底となるExtenderの抽象クラスです
 */
public abstract class E_AbstractExtender extends SkyEditor2.Extender{

	public E_AccessController access;
	public E_PageMessagesHolder pageMessages{get{
													return E_PageMessagesHolder.getInstance();
												}
											private set;}		//addMessages, SkyEditor2.ExtenderExceptionの代わりに利用

	public String errorMsgCode;

	public PageReference pageRef;

	// [IRIS]IRIS UI 切り替えクラス
	public E_IRISHandler iris {get; private set;}

	public E_AbstractExtender() {
		access = E_AccessController.getInstance();
		iris = E_IRISHandler.getInstance();
	}

	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}

	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}

	/**
	 * 整合性、アクセス権判定
	 */
	public PageReference doAuth(E_Const.ID_KIND kind, String id) {
		boolean isAuth = true;
		PageReference pageRef = null;
		try {
			//遷移先URLをセット
			E_CookieHandler.setCookieReferer(access.getReferer(kind));
			//service hour menu
			pageRef = access.isServiceHourMenu(kind);
			if (pageRef != null) {
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_SERVICEHOUR);
				return pageRef;
			}

			// [IRIS][Day1.5]ID管理が存在しない場合はエラーページへ遷移させる
			if(access.idcpf == null){
                pageRef = Page.E_ErrorPage;
                pageRef.getParameters().put('code', E_Const.ERROR_MSG_NO_IDCPF);
                return pageRef;
			}

			//ログイン関連チェック
			pageRef = access.isLoginControl(kind);
			if (pageRef != null) {
				return pageRef;
			}
			//機能制限、オブジェクト権限、パラメータチェック
			if (kind == E_Const.ID_KIND.POLICY) {
				isAuth = doAuthPolicy(id);
			} else if (kind == E_Const.ID_KIND.CONTACT) {
				isAuth = doAuthContact(id);
			} else if (kind == E_Const.ID_KIND.USER) {
				isAuth = doAuthUser (id);
			} else if (kind == E_Const.ID_KIND.INVESTMENT) {
				isAuth = doAuthInvestment(id);
			} else if (kind == E_Const.ID_KIND.DOWNLOAD) {
				isAuth = doAuthDownload();
			} else if (kind == E_Const.ID_KIND.SPECIAL_FEATURES) {
				isAuth = doAuthSpecialFeatures();
			} else {
				isAuth = true;
			}
			//個別チェック
			if (isAuth) {
				isAuth = isValidate();
			}
			if (!isAuth) {
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', errorMsgCode);
			}
		} catch (Exception e) {
System.debug('************************E_AbstractExtender Exception=' + e.getStackTraceString());
			pageMessages.addErrorMessage(e.getMessage());
		}
		return pageRef;
	}

	/**
	 * ページタイトル
	 */
	protected String pgTitle; 			//サブクラスにて設定してください。
	public String getPageTitle() {
		String sTitle = '';		//'['+ E_Const.SYSTEM_NAME +'] - ';	faviconつけるのでシステム名削除
		if(String.isNotBlank(pgTitle)){
			sTitle += pgTitle;
		}else{
			sTitle = '['+ E_Const.SYSTEM_NAME +']';
		}
		return sTitle;
	}

	/**
	 * 連携日時取得（契約関連）
	 */
	public String getDataSyncDate() {
		return E_Util.formatDataSyncDate()  + '　' + '現在の契約内容';
	}

	/**
	 * 連携日時取得（登録関連）
	 */
	public String getDataSyncDateRegist() {
		return E_Util.formatDataSyncDate()  + '　' + '現在のご登録内容';
	}

	/**
	 * 連携日時取得（契約一覧検索関連）
	 */
	public String getDataSyncDateCustomer() {
		return E_Util.formatDataSyncDate() + '　' + '現在の契約/取引内容';
	}

	/**
	 * 連携日時取得（投資信託関連）
	 */
	public String getDataSyncDateInvestment() {
		return E_Util.formatDataSyncDate()  + '　' + '現在';
	}

	/**
	 * welcome取得
	 */
	public String getWelcome() {
		if(access.isSumiseiUser()){
			return null;
		}
		return E_Util.getWelcome();
	}

	/**
	 * ISS判定
	 */
	public Boolean getIsIss() {
		return E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId());
	}

	/**
	 * 実行可能な処理のコード判定（繰入比率変更：TD05）
	 */
	public boolean getIsTrCodeFundRatio() {
		return access.isTransactionCode(E_Const.TRCDE_TD05);
	}

	/**
	 * 実行可能な処理のコード判定（積立金の移転：TD06）
	 */
	public boolean getIsTrCodeFundTransfer() {
		return access.isTransactionCode(E_Const.TRCDE_TD06);
	}

	/**
	 * 実行可能な処理のコード判定（住所変更受付：TDB0）
	 */
	public boolean getIsTrCodeAddress() {
		return access.isTransactionCode(E_Const.TRCDE_TDB0);
	}

	/**
	 * 画面の表示条件用
	 * 社員フラグ
	 */
	public boolean getIsEmployee() {
		return access.isEmployee();
	}

	/**
	 * 画面の表示条件用
	 * 代理店フラグ
	 */
	public boolean getIsAgent() {
		return access.isAgent();
	}

	/**
	 * 画面の表示条件用
	 * 契約者フラグ
	 */
	public boolean getIsCustomer() {
		return access.isCustomer();
		//return true;
	}

	/**
	 * コールセンター判別
	 */
	public boolean getIsCallCenter(){
		return access.isCallCenter();
	}

	/**
	 * 積立金画面の手続きパスワード入力制御
	 */
	public boolean getIsFundTransfer() {
		return getIsCustomer() && getIsTrCodeFundTransfer();
	}

	/**
	 * 繰入比率画面の手続きパスワード入力制御
	 */
	public boolean getIsInwardTransfer() {
		return getIsCustomer() && getIsTrCodeFundRatio();
	}

	/**
	 * 住所情報変更画面の手続きパスワード入力制御
	 */
	public boolean getIsContactInfo() {
		return getIsCustomer() && getIsTrCodeAddress();
	}

	/**
	 * 閉じる＆戻るボタン表示ラベル
	 */
	public String getCloseBackLabel(){
		String label = system.label.E_BTN_BACK;
		if(getIsIss()){
			label = system.label.E_BTN_CLOSE;
		}
		return label;
	}

 	/**
	 * 戻るボタン
	 */
	public virtual PageReference doReturn(){
System.debug('************************E_AbstractExtender doReturn=' + E_CookieHandler.getCookieReferer());
		return new PageReference(E_CookieHandler.getCookieReferer());
	}

	/**
	 * 個別でチェック判定
	 */
	protected virtual boolean isValidate() {
		return true;
	}

	/**
	 * 契約関連機能判定
	 */
	protected virtual boolean doAuthPolicy (String policyId) {
		return false;
	}


	/**
	 * 登録情報関連, 契約関連(100件以上)オブジェクト判定
	 */
	protected virtual boolean doAuthUser (String userId) {
		return false;
	}

	/**
	 * 登録情報関連機能判定
	 */
	protected virtual boolean doAuthContact (String contactId) {
		return false;
	}

	/**
	 * 投信信託関連機能判定
	 */
	protected virtual boolean doAuthInvestment (String investmentId) {
		//※2015年、投資信託サービス終了に伴い投資信託機能をコメントアウト。直接URLの場合のエラーメッセージ
		errorMsgCode = E_Const.ERROR_MSG_NOFUNCTION;
		return false;
	}

	/**
	 * ダウンロード関連機能判定
	 */
	protected virtual boolean doAuthDownload () {
		return false;
	}

	/**
	 * 特定代理店用機能判定
	 */
	protected virtual boolean doAuthSpecialFeatures () {
		return false;
	}

	/*
	// ISS経由時のクッキー設定メソッド
	protected void setIssCookie(){
		//抽象クラスのクッキー設定メソッド
		if(E_Util.getIsIssParam()){
			E_CookieHandler.setCookieIssSessionId();
		}
	}
	*/
}