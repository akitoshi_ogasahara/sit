@isTest
private class TestI_UsersController {
	static testMethod void pageActionTest() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;
		Contact testAgent = new Contact(LastName = 'testAgent', AccountId = testAgency.Id);
		insert testAgent;
		TestCR_TestUtil.createAgentUser(true, 'testUser', 'E_PartnerCommunity', testAgent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_UsersController ucon = new I_UsersController();
				ucon.pageAction();
			Test.stopTest();

			system.assertEquals(ucon.rowCount, I_Const.LIST_DEFAULT_ROWS);
			//system.assertEquals(ucon.sortType, I_UsersController.SORT_TYPE_NAME);
			system.assertEquals(ucon.sortType, I_UsersController.SORT_TYPE_LAST_LONGIN_DATE);
			TimeZone tz = UserInfo.getTimeZone();
			//system.assertEquals(ucon.TZoffset, tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0));
		}
	}

	static testMethod void rowCountTest01() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;
		Contact testAgent = new Contact(LastName = 'testAgent', AccountId = testAgency.Id);
		insert testAgent;

		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '5');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_UsersController ucon = new I_UsersController();
				ucon.pageAction();
			Test.stopTest();

			system.assertEquals(ucon.rowCount, 5);
		}
	}

	static testMethod void rowCountTest02() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// テスト用募集人レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;
		Contact testAgent = new Contact(LastName = 'testAgent', AccountId = testAgency.Id);
		insert testAgent;
		TestCR_TestUtil.createAgentUser(true, 'testUser', 'E_PartnerCommunity', testAgent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '1001');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
			I_UsersController ucon = new I_UsersController();
			ucon.pageAction();
			Test.stopTest();

			system.assertEquals(ucon.rowCount, I_Const.LIST_MAX_ROWS);
		}
	}

	static testMethod void rowCountTest03() {
		// 実行ユーザ作成（システム管理者）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		E_IDCPF__c idcp = TestI_TestUtil.createIDCPF(true, actUser.Id, null);
		//idcp.ZINQUIRR__c = 'BR***';
		//update idcp;

		// テスト用募集人レコード作成
		Account testAgency = new Account(Name = 'testAgency');
		insert testAgency;
		Contact testAgent = new Contact(LastName = 'testAgent', AccountId = testAgency.Id);
		insert testAgent;
		TestCR_TestUtil.createAgentUser(true, 'testUser', 'E_PartnerCommunity', testAgent.Id);

		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '1001');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
			I_UsersController ucon = new I_UsersController();
			ucon.pageAction();
			Test.stopTest();

			system.assertEquals(ucon.rowCount, I_Const.LIST_MAX_ROWS);
		}
	}

	static testMethod void addRowTest01() {
		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);
		TestI_TestUtil.createIDCPF(true, UserInfo.getUserId(), null);

		// テスト用レコード作成
		Account testParentAgency = new Account(Name = 'testParentAgency');
		insert testParentAgency;

		Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
		insert testAgency;

		List<Contact> testAgents = new List<Contact>();
		for (Integer i = 0, j = 30; i < j; i++) {
			testAgents.add(new Contact(
				LastName = 'testAgent' + i,
				AccountId = testAgency.Id,
				E_CL3PF_AGNTNUM__c = String.valueOf(i).leftPad(16).replaceAll(' ', '9'),
				E_CL3PF_VALIDFLAG__c = '1',
				E_CL3PF_AGTYPE__c = '0'
			));
		}
		insert testAgents;
		List<User> testUser = new List<User>();
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
		for (Contact testAgent : testAgents) {
			userName = testAgent.LastName + '@terrasky.ingtesting';
			User src = new User(
					  Lastname = testAgent.LastName
					, Username = userName
					, Email = userName
					, ProfileId = p.Id
					, Alias = testAgent.LastName.left(8)
					, TimeZoneSidKey = UserInfo.getTimeZone().getID()
					, LocaleSidKey = UserInfo.getLocale()
					, EmailEncodingKey = 'UTF-8'
					, LanguageLocaleKey = UserInfo.getLanguage()
					, ContactId = testAgent.Id
			);
			testUser.add(src);
		}
		insert testUser;

		List<E_Log__c> testLog = new List<E_Log__c>();
		for (User usr: testUser) {
			if (testUser.get(0) == usr) continue;
			System.runAs(usr) {
				// ガバナに気を付ける
				E_Log__c log = E_LogUtil.createLog();
				log.isDisplay__c = true;
				testLog.add(log);
			}
		}
		insert testLog;

		Test.startTest();
		I_UsersController ucon = new I_UsersController();
		ucon.pageAction();
		System.debug(ucon.userList.size());
		//ucon.userList = [SELECT Id, Name, LastLoginDate, Contact.Account.Name FROM User WHERE UserType = 'PowerPartner' AND Contact.Account.ParentId != ''];
		ucon.addRows();

		Test.stopTest();
		ucon.getSearchTypeOptions();
		System.assertEquals(ucon.hasViewedByManager, false);
		system.assertEquals(ucon.rowCount, I_Const.LIST_DEFAULT_ROWS + I_Const.LIST_ADD_MORE_ADD_ROWS);
		system.assertEquals(ucon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
		/*system.assertEquals(ucon.getUserCount(), 30);*/
		system.assertEquals(ucon.getTotalRows(), 29);
		/*system.assertEquals(ucon.getTotalRows(), 0);*/
	}

	static testMethod void addRowTest02() {
		//ガバナに抵触する場合には値を下げること
		Integer agentNm = 15;
		
		User managerUser = TestI_TestUtil.createUser(true, 'Manager01', '拠点長');
		TestI_TestUtil.createIDCPF(true, managerUser.Id, null);
		UserRole role = new UserRole(
			DeveloperName = 'test_class_dummy_role',
			Name = '拠点長用のロール'
		);
		System.runAs([select id from user where id =: UserInfo.getUserId()][0]) {
			insert role;
			managerUser.UserRoleId = role.id;
			update managerUser;
		}

		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, '1001');
		TestI_TestUtil.createIDCPF(true, UserInfo.getUserId(), null);

		List<Contact> testAgents = new List<Contact>();

		System.runAs(managerUser) {
			// テスト用レコード作成
			Account testParentAgency = new Account(Name = 'testParentAgency');
			insert testParentAgency;

			Account testAgency = new Account(
				Name = 'testAgency',
				ParentId = testParentAgency.Id,
				KSECTION__c = '00'
			);

			Account headOffice = new Account(
				Name = 'head_office',
				ParentId = testParentAgency.Id,
				KSECTION__c = E_Const.AGENCY_KSECTION_HEAD_OFFICE
			);
			insert new List<Account>{testAgency, headOffice};

	//		for (Integer i = 0, j = 1000; i < j; i++) {
//			for (Integer i = 0, j = 30; i < j; i++) {
			for (Integer i = 0, j = agentNm; i < j; i++) {
				testAgents.add(new Contact(
					LastName = 'testAgent' + i,
					AccountId = i < 15 ? testAgency.Id : headOffice.Id,
					E_CL3PF_AGNTNUM__c = String.valueOf(i).leftPad(16).replaceAll(' ', '9'),
					E_CL3PF_VALIDFLAG__c = '1',
					E_CL3PF_AGTYPE__c = '0'
				));
			}
			insert testAgents;
		}

		String userName;
		List<E_Log__c> testLog = new List<E_Log__c>();
		Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
		for (Contact testAgent : testAgents) {
			userName = testAgent.LastName + '@terrasky.ingtesting';
			User src = new User(
					  Lastname = testAgent.LastName
					, Username = userName
					, Email = userName
					, ProfileId = p.Id
					, Alias = testAgent.LastName.left(8)
					, TimeZoneSidKey = UserInfo.getTimeZone().getID()
					, LocaleSidKey = UserInfo.getLocale()
					, EmailEncodingKey = 'UTF-8'
					, LanguageLocaleKey = UserInfo.getLanguage()
					, ContactId = testAgent.Id
			);

			System.runAs(src) {
				// ガバナに気を付ける
				E_Log__c log = E_LogUtil.createLog();
				log.isDisplay__c = true;
				testLog.add(log);
			}
		}
		insert testLog;
		/*insert testUser;*/

		System.runAs(managerUser) {
			Test.startTest();
			I_UsersController ucon = new I_UsersController();
			ucon.pageAction();
			//ucon.userList = [SELECT Id, Name, LastLoginDate, Contact.Account.Name FROM User WHERE UserType = 'PowerPartner' AND Contact.Account.ParentId != ''];
			ucon.addRows();
			Test.stopTest();

			system.assertEquals(ucon.hasViewedByManager, true);
			system.assertEquals(ucon.rowCount, I_Const.LIST_MAX_ROWS);
			system.assertEquals(ucon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
			//system.assertEquals(ucon.getUserCount(), I_Const.LIST_MAX_ROWS);
			//system.assertEquals(ucon.getTotalRows(), I_Const.LIST_MAX_ROWS);
			system.assertEquals(ucon.searchType, I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_ALL);
			system.assertEquals(ucon.getTotalRows(), 0);

			ucon.searchType = I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE;
			ucon.createUserList();
			system.assertEquals(ucon.searchType, I_Const.POLICY_RENDERING_TYPE_BY_MANAGER_HEAD_OFFICE);
			system.assertEquals(ucon.getTotalRows(), agentNm);
		}
	}


	static testMethod void addRowTest03() {
		// ページ表示
		PageReference pr = Page.IRIS_Users;
		Test.setCurrentPage(pr);


		// テスト用レコード作成
		Account testParentAgency = new Account(Name = 'testParentAgency');
		insert testParentAgency;

		Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
		insert testAgency;

		List<Contact> testAgents = new List<Contact>();
		for (Integer i = 0, j = 30; i < j; i++) {
			testAgents.add(new Contact(LastName = 'testAgent' + i, AccountId = testAgency.Id, E_CL3PF_AGNTNUM__c = String.valueOf(i)));
		}
		insert testAgents;

		List<User> testUser = new List<User>();
		List<E_Log__c> testLog = new List<E_Log__c>();
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
		for (Contact testAgent : testAgents) {
			userName = testAgent.LastName + '@terrasky.ingtesting';
			User src = new User(
					  Lastname = testAgent.LastName
					, Username = userName
					, Email = userName
					, ProfileId = p.Id
					, Alias = testAgent.LastName.left(8)
					, TimeZoneSidKey = UserInfo.getTimeZone().getID()
					, LocaleSidKey = UserInfo.getLocale()
					, EmailEncodingKey = 'UTF-8'
					, LanguageLocaleKey = UserInfo.getLanguage()
					, ContactId = testAgent.Id
			);
			testUser.add(src);
			testLog.add(E_LogUtil.createLog());
		}
		insert testUser;
		insert testLog;

		System.debug('testUser='+testUser[0]);

		E_IDCPF__c idcp = TestI_TestUtil.createIDCPF(true, UserInfo.getUserId(), null);
		idcp.ZINQUIRR__c = 'BR**';
		update idcp;

		Test.startTest();
			I_UsersController ucon = new I_UsersController();
			E_AccessController access = E_AccessController.getInstance();
			System.debug('getUserList='+E_UserDaoWithout.getIRISAgentUserRecord(access.idcpf.ZINQUIRR__c.right(2)));
			ucon.pageAction();
			//ucon.userList = [SELECT Id, Name, LastLoginDate, Contact.Account.Name FROM User WHERE UserType = 'PowerPartner' AND Contact.Account.ParentId != ''];
			ucon.addRows();
		Test.stopTest();

		system.assertEquals(ucon.rowCount, I_Const.LIST_DEFAULT_ROWS + I_Const.LIST_ADD_MORE_ADD_ROWS);
		system.assertEquals(ucon.getListMaxRows(), I_Const.LIST_MAX_ROWS);
		//system.assertEquals(ucon.getUserCount(), 30);
//		system.assertEquals(ucon.getTotalRows(), 30);
		system.assertEquals(ucon.getTotalRows(), 0);
	}

	static testMethod void sortRowsTest01() {
		//ガバナに抵触する場合には値を下げること
		Integer agentNm = 15;
		// ページ表示
		PageReference pr = Page.IRIS_Users;
		pr.getParameters().put(I_UsersController.URL_PARAM_SORT_TYPE, I_UsersController.SORT_TYPE_AGENT_NAME);
		Test.setCurrentPage(pr);
		//pr.getParameters().put(I_UsersController.URL_PARAM_SORT_TYPE, I_UsersController.SORT_TYPE_NAME);
		TestI_TestUtil.createIDCPF(true, UserInfo.getUserId(), null);

		// テスト用レコード作成
		Account testParentAgency = new Account(Name = 'testParentAgency');
		insert testParentAgency;

		Account testAgency = new Account(Name = 'testAgency', ParentId = testParentAgency.Id);
		insert testAgency;

		List<Contact> testAgents = new List<Contact>();
//		for (Integer i = 0, j = 30; i < j; i++) {
		for (Integer i = 0, j = agentNm; i < j; i++) {
			testAgents.add(new Contact(
				LastName = 'testAgent' + i,
				AccountId = testAgency.Id,
				E_CL3PF_AGNTNUM__c = String.valueOf(i).leftPad(16).replaceAll(' ', '9'),
				E_CL3PF_VALIDFLAG__c = '1',
				E_CL3PF_AGTYPE__c = '0'
			));
		}
		insert testAgents;

		List<User> testUser = new List<User>();
		List<E_Log__c> testLog = new List<E_Log__c>();
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
		for (Contact testAgent : testAgents) {
			userName = testAgent.LastName + '@terrasky.ingtesting';
			User src = new User(
					  Lastname = testAgent.LastName
					, Username = userName
					, Email = userName
					, ProfileId = p.Id
					, Alias = testAgent.LastName.left(8)
					, TimeZoneSidKey = UserInfo.getTimeZone().getID()
					, LocaleSidKey = UserInfo.getLocale()
					, EmailEncodingKey = 'UTF-8'
					, LanguageLocaleKey = UserInfo.getLanguage()
					, ContactId = testAgent.Id
			);
			testUser.add(src);
			System.runAs(src) {
				// ガバナに気を付ける
				E_Log__c log = E_LogUtil.createLog();
				log.isDisplay__c = true;
				testLog.add(log);
			}
		}
		/*insert testUser;*/
		testUser.sort();
		insert testLog;

		Test.startTest();
		I_UsersController ucon = new I_UsersController();
		ucon.pageAction();
		//ucon.userList = [SELECT Id, Name, LastLoginDate, Contact.Account.Name FROM User WHERE UserType = 'PowerPartner' AND Contact.Account.ParentId != ''];
		pr = Page.IRIS_Users;
		pr.getParameters().put(I_UsersController.URL_PARAM_SORT_TYPE, I_UsersController.SORT_TYPE_OFFICE_NAME);
		Test.setCurrentPage(pr);
		ucon.sortRows();
		ucon.sortRows();

		pr.getParameters().put(I_UsersController.URL_PARAM_SORT_TYPE, I_UsersController.SORT_TYPE_LAST_LONGIN_DATE);
		Test.setCurrentPage(pr);
		ucon.sortRows();
		ucon.sortRows();

		pr.getParameters().put(I_UsersController.URL_PARAM_SORT_TYPE, I_UsersController.SORT_TYPE_LOGIN_COUNT);
		Test.setCurrentPage(pr);
		ucon.sortRows();
		ucon.sortRows();

		pr = Page.IRIS_Users;
		pr.getParameters().put(I_UsersController.URL_PARAM_SORT_TYPE, I_UsersController.SORT_TYPE_AGENT_NAME);
		Test.setCurrentPage(pr);
		ucon.sortRows();
		ucon.sortRows();
		ucon.userList.get(0).LastLoginDate = Datetime.newInstanceGMT(2000, 1, 1, 0, 0, 0);
		String jst = ucon.userList.get(0).lastLoginDateJST;
		Test.stopTest();
		system.assertEquals(ucon.sortType, I_UsersController.SORT_TYPE_AGENT_NAME);
		system.assertEquals(ucon.sortIsAsc, false);
//		system.assertEquals(ucon.userList.get(29).agentName, testUser.get(29).LastName);
		system.assertEquals(ucon.userList.get(agentNm-1).agentName, testUser.get(agentNm-1).LastName);
	}


	private void insUserLog(List<Contact> testAgents){
		List<User> testUser = new List<User>();
		List<E_Log__c> testLog = new List<E_Log__c>();
		String userName;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'E_PartnerCommunity' LIMIT 1];
		for (Contact testAgent : testAgents) {
			userName = testAgent.LastName + '@terrasky.ingtesting';
			User src = new User(
					  Lastname = testAgent.LastName
					, Username = userName
					, Email = userName
					, ProfileId = p.Id
					, Alias = testAgent.LastName.left(8)
					, TimeZoneSidKey = UserInfo.getTimeZone().getID()
					, LocaleSidKey = UserInfo.getLocale()
					, EmailEncodingKey = 'UTF-8'
					, LanguageLocaleKey = UserInfo.getLanguage()
					, ContactId = testAgent.Id
			);
			testUser.add(src);
			testLog.add(E_LogUtil.createLog());
		}
		insert testUser;
		insert testLog;
	}
}