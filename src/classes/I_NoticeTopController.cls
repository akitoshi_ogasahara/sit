public with sharing class I_NoticeTopController{

	/**
	 * レコードタイプ取得
	 */
	private static Map<String, Id> POLICY_RECORD_TYPES = E_RecordTypeDao.getRecordTypeBySobjectType('E_Policy__c');

	/** TOP画面での応答日に関するレコード件数取得
	 *     件数取得用クエリ
	 */
	private static final Map<String, String> RECORD_SIZE_SOQL_FORMAT = new Map<String, String>{
								'NOTICE' =>
								'SELECT COUNT(Id) FROM {0} '
								 + 'WHERE YYYYMM__c IN ({1}) '+
								 + 'AND E_Policy__r.recordtypeId IN ({2}) '};


	/**
	 * クエリ文字列作成
	 */
	public static Map<String,String> getRECORD_SIZE_SOQL(String sObjectNM, String sWhere){
		// 3ヶ月分年月設定
		Date d = E_DownloadNoticeUtil.getPeriodStartDateByObjApi(sObjectNM);
		/** Start 2017/06/19 3ヶ月分から1ヶ月分に変更 **/
//		String termMonth = '\''+E_Util.getFormatDateStr(d.addMonths(-2), 'yyyy/MM')+'\'' + ',' + '\''+E_Util.getFormatDateStr(d.addMonths(-1), 'yyyy/MM')+'\'' + ',' + '\''+E_Util.getFormatDateStr(d,'yyyy/MM')+'\'';
		String termMonth = '\''+E_Util.getFormatDateStr(d,'yyyy/MM')+'\'';
		/** End 2017/06/19 3ヶ月分から1ヶ月分に変更 **/
		// レコードタイプのId条件の設定
		String termRecordTypeId = '\''+String.valueOf(POLICY_RECORD_TYPES.get(E_Const.POLICY_RECORDTYPE_COLI))+'\'';
		if(sObjectNM == E_DownloadNoticeConst.OBJ_API_E_AGPO){
			termRecordTypeId += ',\''+String.valueOf(POLICY_RECORD_TYPES.get(E_Const.POLICY_RECORDTYPE_SPVA))+'\'';
			termRecordTypeId += ',\''+String.valueOf(POLICY_RECORD_TYPES.get(E_Const.POLICY_RECORDTYPE_DCOLI))+'\'';
			termRecordTypeId += ',\''+String.valueOf(POLICY_RECORD_TYPES.get(E_Const.POLICY_RECORDTYPE_DSPVA))+'\'';
		}
		//クエリ条件設定
		String soql = String.format(RECORD_SIZE_SOQL_FORMAT.get('NOTICE'), new List<String>{sObjectNM, termMonth, termRecordTypeId});

		// 抽出対象外の保険契約ヘッダの証券番号
//		String exculded = System.Label.E_INL_EXC_CHDRNUM.length() >= 10 ? 'E_Policy__r.COMM_CHDRNUM__c  Not In (' + System.Label.E_INL_EXC_CHDRNUM + ')' : '';
		if(System.Label.E_INL_EXC_CHDRNUM.length() >= 10){
			soql += ' AND E_Policy__r.COMM_CHDRNUM__c  Not In (' + System.Label.E_INL_EXC_CHDRNUM + ')';
		}
		soql += sWhere;

		Map<String,String> soqls = new Map<String,String>();
		soqls.put('NOTICE',soql);
		return soqls;
	}

	/*
			検索クエリ取得用クラス

	*/
	public virtual class recordCounterNotice extends E_RecordCountController.recordCountExecuter{
		public virtual String getObjectName(){
			return '';
		}

		//レコード件数問合せクエリ取得用Indicator　複数のクエリを投げる場合にそれぞれのクエリの識別子リストを返す
		public override List<String> getQueryIndicators(){

			return new List<String>(I_NoticeTopController.RECORD_SIZE_SOQL_FORMAT.keySet());
		}

		//レコード件数問合せ実施　レコード件数を返す（但し文字列として） パラメータでgetQueryIndicators()の文字列を受け取る
		public override String getRecordSize(String key){

			try{
				String soql = I_NoticeTopController.getRECORD_SIZE_SOQL(getObjectName(),I_NoticeAbstractController.getSoqlWhereByUser()).get(key);
				List<AggregateResult> agResult = Database.query(soql);
				return String.valueOf(agResult[0].get('expr0'));
			}catch(exception e){
				return e.getMessage();
			}
		}
	}
	public virtual class recordCounterNoticeNCOL extends recordCounterNotice{
		public override String getObjectName(){
			return E_DownloadNoticeConst.OBJ_API_E_NCOL;
		}
	}
	public virtual class recordCounterNoticeBILS extends recordCounterNotice{
		public override String getObjectName(){
			return E_DownloadNoticeConst.OBJ_API_E_BILS;
		}
	}
	public virtual class recordCounterNoticeBILA extends recordCounterNotice{
		public override String getObjectName(){
			return E_DownloadNoticeConst.OBJ_API_E_BILA;
		}
	}
	public virtual class recordCounterNoticeAGPO extends recordCounterNotice{
		public override String getObjectName(){
			return E_DownloadNoticeConst.OBJ_API_E_AGPO;
		}
	}

}