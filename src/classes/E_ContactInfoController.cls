global with sharing class E_ContactInfoController extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public Contact record {get{return (Contact)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_ContactInfoExtender getExtender() {return (E_ContactInfoExtender)extender;}
    
    
    public E_ContactInfoController(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component1362');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1362',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','confirmation_registration');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','119');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component812');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component812',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','ヘルプ画面へ');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_ContactInfo');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
        tmpPropMap.put('Component__Width','85');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component1377');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1377',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','ホームへ');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Home');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
        tmpPropMap.put('Component__Width','80');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component1378');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1378',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_postalCode','{!record.E_CLTPF_CLTPCODE__c}');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component1361');
        tmpPropMap.put('Component__Name','EPostalCodeLabel');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1361',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1366');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        tmpPropMap.put('Component__Top','0');
        tmpPropMap.put('Component__Left','0');
        appComponentProperty.put('Component1366',tmpPropMap);


        SObjectField f;

        f = Contact.fields.E_COMM_ZCLADDR__c;
        f = Contact.fields.E_CLTPF_ZCLKADDR__c;
        f = Contact.fields.E_CLTPF_CLTPHONE01__c;
        f = Contact.fields.E_CLTPF_CLTPHONE02__c;
        f = Contact.fields.E_CLTPF_FAXNO__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = Contact.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('Contact');
            mainQuery.addFieldAsOutput('E_COMM_ZCLADDR__c');
            mainQuery.addFieldAsOutput('E_CLTPF_ZCLKADDR__c');
            mainQuery.addFieldAsOutput('E_CLTPF_CLTPHONE01__c');
            mainQuery.addFieldAsOutput('E_CLTPF_CLTPHONE02__c');
            mainQuery.addFieldAsOutput('E_CLTPF_FAXNO__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('LastName');
            mainQuery.addFieldAsOutput('FirstName');
            mainQuery.addFieldAsOutput('E_CLTPF_CLTPCODE__c');
            mainQuery.addFieldAsOutput('E_dataSyncDate__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = false;
            p_sidebar = false;
            extender = new E_ContactInfoExtender(this);
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    
    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}