/*
 * TestDataFactory
 * Public class for create test data
 * created : Accenture 2018/4/4
 * modified :
 */

@isTest
public class CC_TestDataFactory {

	/**
	 * Create test user
	 */
	public static User createTestUser(){
		String strUserName = 'testuser@test.com.sfdc';
		User testUser = new User();
		List<User> userList = [SELECT Id FROM User where UserName = :strUserName limit 1];

		if(userList.size() != 0){
			testUser = userList[0];
		}else{
			Profile profile = [Select Id from Profile where Name = 'CallCenter' limit 1];
			testUser.ProfileId = profile.Id;
			testUser.UserName = strUserName;
			testUser.FirstName = 'test';
			testUser.LastName = 'user';
			testUser.EMail = 'testuser@test.com';
			testUser.Alias = 'testuser';
			testUser.TimeZoneSidKey = 'Asia/Tokyo';
			testUser.LocaleSidKey = 'ja_JP';
			testUser.EmailEncodingKey = 'ISO-2022-JP';
			testUser.LanguageLocaleKey = 'ja';
			testUser.MemberNo__c = 'testuser001';

			Insert testUser;

			PermissionSet permissionSet = [Select Id from PermissionSet where Label = 'Call Center一般' limit 1];
			PermissionSetAssignment assignment = new PermissionSetAssignment();
			assignment.AssigneeId = testUser.Id;
			assignment.PermissionSetId = permissionSet.Id;
			Insert assignment;
		}

		return testUser;
	}

	/**
	 * Create test user
	 * システム管理者
	 */
	public static User createTestAdminUser(){
		String strUserName = 'testuser@test.com.sfdc';
		User adminUser = new User();
		List<User> userList = [SELECT Id FROM User where UserName = :strUserName limit 1];

		if(userList.size() != 0){
			adminUser = userList[0];
		}else{
			Profile profile = [Select Id from Profile where Name = 'CallCenter' limit 1];
			adminUser.ProfileId = profile.Id;
			adminUser.FirstName = 'test';
			adminUser.LastName = 'admin';
			adminUser.UserName = 'admin@test.com.sfdc';
			adminUser.EMail = 'admin@test.com';
			adminUser.Alias = 'admin';
			adminUser.TimeZoneSidKey = 'Asia/Tokyo';
			adminUser.LocaleSidKey = 'ja_JP';
			adminUser.EmailEncodingKey = 'ISO-2022-JP';
			adminUser.LanguageLocaleKey  = 'ja';
			adminUser.MemberNo__c = 'admin001';
			Insert adminUser;

			PermissionSet permissionSet = [Select Id from PermissionSet where Label = 'Call Center一般' limit 1];
			PermissionSetAssignment assignment = new PermissionSetAssignment();
			assignment.AssigneeId = adminUser.Id;
			assignment.PermissionSetId = permissionSet.Id;
			Insert assignment;

			PermissionSet PermissionSetAdmin = [Select Id from PermissionSet where Label = 'CallCenter管理者権限' limit 1];
			PermissionSetAssignment assignmentAdmin = new PermissionSetAssignment();
			assignmentAdmin.AssigneeId = adminUser.Id;
			assignmentAdmin.PermissionSetId = PermissionSetAdmin.Id;
			Insert assignment;
		}

		return adminUser;
	}

	/**
	 * Get Account
	 */
	public static Account getAccountSkel(Id recordTypeId){
		Account newAccount = new Account();
		newAccount.RecordTypeId = recordTypeId;
		newAccount.Name = 'testAccount';
		return newAccount;
	}

	/**
	 * Get Account List
	 */
	public static List<Account> getAccountSkelList(Id recordTypeId){
		List<Account> accountList = new List<Account>();
		for(Integer i = 0; i <= 1; i++){
			accountList.add(new Account(
							RecordTypeId = recordTypeId,
							Name = 'testAccount' +i
							));
		}
		return accountList;
	}

	/**
	 * Get Case
	 */
	public static Case getCaseSkel(Id SRTypeId){
		Case newCase = new Case();
		newCase.CC_SRTypeId__c = SRTypeId;
		newCase.CMN_Source__c = '電話';
		newCase.CC_SubStatus__c = '未割当';
		newCase.CC_IsCoverLetterOutput__c = true;
		newCase.CC_IsInvoiceOutput__c = false;
		newCase.CMN_FreeSpace1__c = 'testCase';
		RecordType recordTypeOfCase = [SELECT Name FROM RecordType where SobjectType = 'Case' and Name = 'サービスリクエスト' limit 1];
		newCase.RecordTypeId = recordTypeOfCase.Id;
		return newCase;
	}

	/**
	 * Get Case
	 * No Parameter
	 */
	public static Case getCaseSkelWithNoParameter(){
		Case newCase = new Case();
		newCase.CMN_Comment1__c = 'testComment1';
		newCase.CMN_Comment2__c = 'testComment2';
		RecordType recordTypeOfCase = [SELECT Name FROM RecordType where SobjectType = 'Case' and Name = 'サービスリクエスト' limit 1];
		newCase.RecordTypeId = recordTypeOfCase.Id;
		return newCase;
	}

	/**
	 * Get Contact
	 */
	public static Contact getContactSkel(){
		Contact newContact = new Contact();
		newContact.FirstName = 'test';
		newContact.LastName = 'Contact';
		return newContact;
	}

	/**
	 * Get Contact List
	 */
	public static List<Contact> getContactSkelList(){
		List<Contact> contactList = new List<Contact>();
		for(Integer i = 0; i <= 1; i++){
			contactList.add(new Contact(
							FirstName = 'test' +i,
							LastName = 'Contact' +i
							));
		}
		return contactList;
	}

	/**
	 * Get SRTypeMaster
	 */
	public static CC_SRTypeMaster__c getSRTypeMasterSkel(){
		CC_SRTypeMaster__c newSRTypeMaster = new CC_SRTypeMaster__c();
		newSRTypeMaster.Name = '10001';
		newSRTypeMaster.CC_SRTypeName__c = 'testSRTypeMaster';
		newSRTypeMaster.CC_SRTypeSubject__c = 'test';
		newSRTypeMaster.CC_Department__c = 'CS';
		newSRTypeMaster.CC_SRTypeComment1__c = '';
		newSRTypeMaster.CC_SRTypeComment2__c = '';
		return newSRTypeMaster;
	}

	/**
	 * Get SRTypeMaster list
	 */
	public static List<CC_SRTypeMaster__c> getSRTypeMasterSkelList(){
		List<CC_SRTypeMaster__c> SRTypeMasterList = new List<CC_SRTypeMaster__c>();
		for(Integer i = 0; i < 2; i++){
			CC_SRTypeMaster__c newSRTypeMaster = new CC_SRTypeMaster__c();
			newSRTypeMaster.Name = '1000' + i;
			newSRTypeMaster.CC_SRTypeName__c = 'testSRTypeMaster' + i;
			newSRTypeMaster.CC_SRTypeSubject__c = 'test' + i;
			newSRTypeMaster.CC_Department__c = 'CS';
			newSRTypeMaster.CC_SRTypeComment1__c = '';
			newSRTypeMaster.CC_SRTypeComment2__c = '';
			SRTypeMasterList.add(newSRTypeMaster);
		}
		return SRTypeMasterList;
	}

	/**
	 * Get ActivityTemplate
	 */
	public static CC_ActivityTemplate__c getActivityTemplateSkel(){
		CC_ActivityTemplate__c newActivityTemplate = new CC_ActivityTemplate__c();
		newActivityTemplate.name = 'testActivityTemplate';
		newActivityTemplate.CC_UpsertKey__c = '';
		return newActivityTemplate;
	}

	/**
	 * Get SRTypeMasterActivityTemplate
	 */
	public static CC_SRTypeMasterActivityTemplate__c getSRTypeMasterActivityTemplateSkel(Id SRTypeMasterId, Id ActivityTemplateId){
		CC_SRTypeMasterActivityTemplate__c newSRTypeMasterActivityTemplate = new CC_SRTypeMasterActivityTemplate__c();
		newSRTypeMasterActivityTemplate.CC_SRTypeMaster__c = SRTypeMasterId;
		newSRTypeMasterActivityTemplate.CC_ActivityTemplate__c = ActivityTemplateId;
		newSRTypeMasterActivityTemplate.CC_Source__c = '電話;E-mail;FAX;郵便;社内便;拠点受付連絡票;拠点受付（電話）;拠点受付（FAX・E-mail・郵便）';
		return newSRTypeMasterActivityTemplate;
	}

	/**
	 * Get ActivityMaster
	 */
	public static CC_ActivityMaster__c getActivityMasterSkel(){
		CC_ActivityMaster__c newActivityMaster = new CC_ActivityMaster__c();
		newActivityMaster.Name = 'testActivityMaster';
		newActivityMaster.CC_UpsertKey__c = '';
		return newActivityMaster;
	}

	/**
	 * Get ActivityTemplateActivityMaster
	 */
	public static CC_ActivityTemplateActivityMaster__c getActivityTemplateActivityMasterSkel(Id ActivityMasterId, Id ActivityTemplateId){
		CC_ActivityTemplateActivityMaster__c newActivityTemplateActivityMaster = new CC_ActivityTemplateActivityMaster__c();
		newActivityTemplateActivityMaster.CC_ActivityMaster__c = ActivityMasterId;
		newActivityTemplateActivityMaster.CC_ActivityTemplate__c = ActivityTemplateId;
		newActivityTemplateActivityMaster.CC_Order__c = 1;
		newActivityTemplateActivityMaster.CC_AssignedQueue__c = 'UnAssigned';
		return newActivityTemplateActivityMaster;
	}

	/**
	 * Get EPolicy
	 */
	public static E_Policy__c getEPolicySkel(Id accountId, Id contactId){
		E_Policy__c newEPolicy = new E_Policy__c();
		newEPolicy.Contractor__c = contactId;
		newEPolicy.COMM_CHDRNUM__c = '10001';
		newEPolicy.SubAgentAccount__c = accountId;
		return newEPolicy;
	}

	/**
	 * Create SRActivity
	 */
	public static CC_SRActivity__c createSRActivity(Id caseId){
		//create ActivityMaster
		CC_ActivityMaster__c activityMaster = getActivityMasterSkel();
		insert activityMaster;
		//create SRActivity
		Group queue = [Select Id from Group where Type = 'Queue' and name = 'Unassigned' LIMIT 1];
		CC_SRActivity__c newSRActivity = new CC_SRActivity__c();
		newSRActivity.CC_SRNo__c = caseId;
		newSRActivity.CC_Order__c = 1;
		newSRActivity.OwnerId = queue.Id;
		newSRActivity.CC_ActivityType__c = 'testActivityMaster';
		newSRActivity.CC_Status__c = '作業中';
		return newSRActivity;
	}

	/**
	 * Create SRActivityList
	 */
	public static List<CC_SRActivity__c> createSRActivityList(){
		List<CC_SRTypeMaster__c> srTypeMasterList = new List<CC_SRTypeMaster__c>();
		//create SRTypeMaster
		srTypeMasterList = getSRTypeMasterSkelList();
		insert srTypeMasterList;
		//create Case
		List<Case> caseList = new List<Case>();
		for(Integer i = 0; i < srTypeMasterList.size(); i++){
			caseList.add(new Case(
				CC_SRTypeId__c = srTypeMasterList.get(i).Id,
				CMN_Source__c = '電話',
				CC_SubStatus__c = '未割当',
				CC_IsCoverLetterOutput__c = true,
				CC_IsInvoiceOutput__c = false,
				CMN_FreeSpace1__c = 'testCase'
			));
		}
		insert caseList;
		//create ActivityMaster
		CC_ActivityMaster__c activityMaster = getActivityMasterSkel();
		insert activityMaster;
		//create SRActivity
		Group queue = [Select Id from Group where Type = 'Queue' and name = 'Unassigned' LIMIT 1];
		List<CC_SRActivity__c> srActivityList = new List<CC_SRActivity__c>();
		for(Integer i = 0; i < caseList.size(); i++){
			srActivityList.add(new CC_SRActivity__c(
				CC_SRNo__c = caseList.get(i).Id,
				CC_Order__c = 1,
				OwnerId = queue.Id,
				CC_ActivityType__c = 'testActivityMaster',
				CC_Status__c = '作業中'
			));
		}
		return srActivityList;
	}

	/**
	 * Get FormMaster
	 * 帳票区分: 請求書
	 */
	public static CC_FormMaster__c getFormMasterSkel(){
		CC_FormMaster__c newFormMaster = new CC_FormMaster__c();
		newFormMaster.CC_FormNo__c = '100001';
		newFormMaster.CC_FormName__c = 'testFormMaster';
		newFormMaster.CC_CorporatePersonClassification__c = 'P';
		newFormMaster.CC_FormType__c = '請求書';
		return newFormMaster;
	}

	/**
	 * Get FormMaster
	 * 帳票区分: 送付状
	 */
	public static CC_FormMaster__c getCoverLetterSkel(){
		CC_FormMaster__c newFormMaster = new CC_FormMaster__c();
		newFormMaster.CC_FormNo__c = '200001';
		newFormMaster.CC_FormName__c = 'testCoverLetter';
		newFormMaster.CC_CorporatePersonClassification__c = 'P';
		newFormMaster.CC_FormType__c = '送付状';
		return newFormMaster;
	}

	/**
	 * Create SRPolicy
	 */
	public static CC_SRPolicy__c createSRPolicy(){
		//Prepare data Begin.//

		//Create Account
		RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
		Account account = getAccountSkel(recordTypeOfAccount.Id);
		insert account;

		//Create Contact
		RecordType recordTypeOfContact = [SELECT Id FROM RecordType where SobjectType = 'Contact' and Name = '顧客' limit 1];
		Contact newContact = getContactSkel();
		newContact.RecordTypeId = recordTypeOfContact.Id;
		newContact.AccountId = account.Id;
		insert newContact;

		//Create EPolicy
		E_Policy__c ePolicy = getEPolicySkel(account.Id, newContact.Id);
		insert ePolicy;

		//Create FormMaster
		CC_FormMaster__c formMaster = getFormMasterSkel();
		insert formMaster;

		//Create SRTypeMaster
		CC_SRTypeMaster__c srTypeMaster = getSRTypeMasterSkel();
		insert srTypeMaster;

		//Create Case
		Case newcase = getCaseSkel(SRTypeMaster.Id);
		insert newcase;
		//Prepare data End.//

		CC_SRPolicy__c newSRPolicy = new CC_SRPolicy__c();
		newSRPolicy.CC_CaseId__c = newcase.Id;
		newSRPolicy.CC_PolicyID1__c = ePolicy.Id;
		newSRPolicy.CC_PolicyID2__c = ePolicy.Id;
		newSRPolicy.CC_PolicyID3__c = ePolicy.Id;
		newSRPolicy.CC_PolicyID4__c = ePolicy.Id;
		newSRPolicy.CC_PolicyNumber1__c = '10001';
		newSRPolicy.CC_PolicyNumber2__c = '10001';
		newSRPolicy.CC_PolicyNumber3__c = '10001';
		newSRPolicy.CC_PolicyNumber4__c = '10001';
		newSRPolicy.CC_FormMaster1__c = formMaster.Id;
		newSRPolicy.CC_FormMaster2__c = formMaster.Id;
		return newSRPolicy;
	}

	/**
	 * Get CC_SRCommentHistory__c
	 */
	public static List<CC_SRCommentHistory__c> getSRCommentHistorySkelList(Id caseId){
		List<CC_SRCommentHistory__c> SRCommentHistoryList = new List<CC_SRCommentHistory__c>();
		for(Integer i = 0; i <= 5; i++){
			SRCommentHistoryList.add(new CC_SRCommentHistory__c(
							CC_CaseId__c = caseId,
							CC_CommentField__c = 'コメント1',
							CC_Content__c = 'test' +i
							));

			SRCommentHistoryList.add(new CC_SRCommentHistory__c(
							CC_CaseId__c = caseId,
							CC_CommentField__c = 'コメント2',
							CC_Content__c = 'test' +i
							));
		}
		return SRCommentHistoryList;
	}

	/**
	 * Get FormDetail
	 */
	public static CC_FormDetail__c getFormDetailSkel(Id caseId){
		CC_FormDetail__c newFormDetail = new CC_FormDetail__c();
		newFormDetail.CC_Case__c = caseId;
		newFormDetail.CC_IsFormOutputFlg__c = false;
		newFormDetail.CC_Input001_1__c = 'testFormDetail';
		return newFormDetail;
	}

	/**
	 * Get VlocitySearchWidgetSetup
	 */
	public static vlocity_ins__VlocitySearchWidgetSetup__c getVlocitySearchWidgetSetupSkel(String strInvokeClassName){
		vlocity_ins__VlocitySearchWidgetSetup__c newVlocitySearchWidgetSetup = new vlocity_ins__VlocitySearchWidgetSetup__c();
		newVlocitySearchWidgetSetup.Name = 'testName';
		newVlocitySearchWidgetSetup.vlocity_ins__Label__c = 'testLabel';
		newVlocitySearchWidgetSetup.vlocity_ins__isActive__c = true;
		newVlocitySearchWidgetSetup.vlocity_ins__InvokeClassName__c = strInvokeClassName;
		return newVlocitySearchWidgetSetup;
	}

	/**
	 * Create CaseEdit InputMap
	 */
	public static Map<String, Object> createCaseEditInputMap01(Map<String,String> inputJSONMap){
		String inputJSONStr =
			'{'
				+'"ContextId": ' + inputJSONMap.get('ContextId')
				+', "DRId_Case": ' + inputJSONMap.get('DRId_Case')
				+', "targetId": ' + inputJSONMap.get('targetId')
				+', "selectedPolicyIdList": ' + inputJSONMap.get('selectedPolicyIdList')

				+', "SR作成": { '

					+' "Debug": { '
							+' "SYSCorporateName": ' + inputJSONMap.get('SYSCorporateName')
							+', "SYSContactName": '+ inputJSONMap.get('SYSContactName')
							+', "CurrentContactName": '+ inputJSONMap.get('CurrentContactName')
							+', "CurrentCorporateName": '+ inputJSONMap.get('CurrentCorporateName')
					+'}, '

					+' "基本情報セクション1": { '
							+' "SR番号": '+ inputJSONMap.get('SR番号')
							+', "複数代理店フラグ": '+ inputJSONMap.get('複数代理店フラグ')
							+', "ステータス": '+ inputJSONMap.get('ステータス')
							+', "サブステータス": '+ inputJSONMap.get('サブステータス')
							+', "受付担当者": '+ inputJSONMap.get('受付担当者')
							+', "受付日時": ' + inputJSONMap.get('受付日時')
							+', "SRタイプ番号SR用件": ' + inputJSONMap.get('SRタイプ番号SR用件')
							+', "完了日時": ' + inputJSONMap.get('完了日時')
							+', "募集人コード": '+ inputJSONMap.get('募集人コード')
							+', "代理店事務所コード": '+ inputJSONMap.get('代理店事務所コード')
							+', "申出人": ' + inputJSONMap.get('申出人')
							+', "申出人種別": '+ inputJSONMap.get('申出人種別')
							+', "情報源": ' + inputJSONMap.get('情報源')
					+'}, '

					+' "基本情報セクション2": { '
							+' "コメント1": '+ inputJSONMap.get('コメント1')
							+', "コメント2": '+ inputJSONMap.get('コメント2')
							+'}, "送付先（ブロック）": { '
							+' "送付先": '+ inputJSONMap.get('送付先')
							+', "発送日": ' + inputJSONMap.get('発送日')
							+', "発送方法": ' + inputJSONMap.get('発送方法')
							+', "送付先Eメール": '+ inputJSONMap.get('送付先Eメール')
							+', "送付先宛名": '+ inputJSONMap.get('送付先宛名')
							+', "送付先敬称": '+ inputJSONMap.get('送付先敬称')
							+', "送付先郵便番号": '+ inputJSONMap.get('送付先郵便番号')
							+', "送付先住所": '+ inputJSONMap.get('送付先住所')
							+'}, "帳票出力": { '
							+' "送付状": '+ inputJSONMap.get('送付状')
							+', "送付状出力": '+ inputJSONMap.get('送付状出力')
							+', "請求書出力": '+ inputJSONMap.get('請求書出力')
							+', "確認書出力": '+ inputJSONMap.get('確認書出力')
							+', "Inner": { '
							+' "証券番号": '+ inputJSONMap.get('証券番号')
							+', "請求書1": '+ inputJSONMap.get('請求書1')
							+', "請求書2": '+ inputJSONMap.get('請求書2')
							+'}}, "フリーセクション": { '
							+' "フリースペース1": '+ inputJSONMap.get('フリースペース1')
							+', "フリースペース2": '+ inputJSONMap.get('フリースペース2')
					+' }'

				+' }'
			+' }';

		Map<String, Object> inputMap = (Map<String, Object>)JSON.deserializeUntyped(inputJSONStr);

		return inputMap;
	}

	/**
	 * Create CaseEdit InputMap
	 */
	public static Map<String, Object> createCaseEditInputMap02(Map<String,String> inputJSONMap){
		String inputJSONStr =
			'{'
				+'"ContextId": ' + inputJSONMap.get('ContextId')
				+', "DRId_Case": ' + inputJSONMap.get('DRId_Case')
				+', "selectedPolicyIdList": ' + inputJSONMap.get('selectedPolicyIdList')

				+', "SR作成": { '

					+' "Debug": { '
							+' "SYSCorporateName": ' + inputJSONMap.get('SYSCorporateName')
							+', "SYSContactName": '+ inputJSONMap.get('SYSContactName')
					+'}, '

					+' "基本情報セクション1": { '
							+' "SR番号": '+ inputJSONMap.get('SR番号')
							+'}, '

					+' "帳票出力": { '
							+' "送付状": '+ inputJSONMap.get('送付状')
							+', "送付状出力": '+ inputJSONMap.get('送付状出力')
							+', "請求書出力": '+ inputJSONMap.get('請求書出力')
							+', "Inner": ['
										+'{ '
											+' "証券番号": ['+ inputJSONMap.get('01証券番号')+']'
											+', "請求書1": '+ inputJSONMap.get('01請求書1')
											+', "請求書2": '+ inputJSONMap.get('01請求書2')
										+'},'
										+'{ '
											+' "証券番号": '+ inputJSONMap.get('02証券番号')
											+', "請求書1": '+ inputJSONMap.get('02請求書1')
											+', "請求書2": '+ inputJSONMap.get('02請求書2')
										+'}'
							+']'
					+'}'
				+' }'

			+' }';
		Map<String, Object> inputMap = (Map<String, Object>)JSON.deserializeUntyped(inputJSONStr);
		return inputMap;
	}

	/**
	 * Create inputJSONMap
	 */
	public static Map<String, String> createinputJSONMap(){
		Map<string, string> inputJSONMap = new Map<string, string>();
		inputJSONMap.put('SYSCorporateName','"null"');
		inputJSONMap.put('SYSContactName','"null"');
		inputJSONMap.put('SR番号','"null"');
		inputJSONMap.put('複数代理店フラグ','false');
		inputJSONMap.put('ステータス','"オープン"');
		inputJSONMap.put('サブステータス','"未割当"');
		inputJSONMap.put('受付担当者','"null"');
		inputJSONMap.put('受付日時','"null"');
		inputJSONMap.put('SRタイプ番号SR用件','"null"');
		inputJSONMap.put('完了日時','"null"');
		inputJSONMap.put('申出人','"null"');
		inputJSONMap.put('申出人種別','"本人"');
		inputJSONMap.put('情報源','"電話"');
		inputJSONMap.put('コメント1','"コメント1"');
		inputJSONMap.put('コメント2','"コメント2"');
		inputJSONMap.put('送付先','"権利者"');
		inputJSONMap.put('発送日','"当日発送"');
		inputJSONMap.put('発送方法','"普通"');
		inputJSONMap.put('送付先Eメール','"test@email.com"');
		inputJSONMap.put('送付先宛名','"テスト"');
		inputJSONMap.put('送付先敬称','"様"');
		inputJSONMap.put('送付先郵便番号','"12345"');
		inputJSONMap.put('送付先住所','"テスト住所"');
		inputJSONMap.put('送付状','"null"');
		inputJSONMap.put('送付状出力','false');
		inputJSONMap.put('請求書出力','false');
		inputJSONMap.put('確認書出力','true');
		inputJSONMap.put('証券番号','"null"');
		inputJSONMap.put('請求書1','"null"');
		inputJSONMap.put('請求書2','"null"');
		inputJSONMap.put('フリースペース1','"フリースペース1"');
		inputJSONMap.put('フリースペース2','"フリースペース2"');
		inputJSONMap.put('01証券番号','"null"');
		inputJSONMap.put('02証券番号','"null"');
		inputJSONMap.put('01請求書1','"null"');
		inputJSONMap.put('01請求書2','"null"');

		return inputJSONMap;
	}
}