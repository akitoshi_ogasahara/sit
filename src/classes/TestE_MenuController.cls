/**
 * メニューコントローラ用テストクラス.
 */
@isTest(SeeAllData=false)
private class TestE_MenuController {

	static E_MenuKind__c menuKind;
	static E_MenuMaster__c menuMasterChild;
	static E_SelectedMenu__c selectedMenu;

	private static void createTestData(){
		// Menu作成.
		menuKind = new E_MenuKind__c(ExternalId__c = 'MK_Agency', SortNumber__c = 1, Name = '一般代理店（インターネット）');
		insert menuKind;
		E_MenuMaster__c menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'C_Home');
		insert menuMasterParent;

		menuMasterChild = new E_MenuMaster__c(
			  Name = 'ChildHome'
			, MenuMasterKey__c = 'home'
			, ParentName__c = menuMasterParent.id
			, SelectedMenuKey__c = 'home'
			, UseCommonGW__c = true
			, UseInternet__c = true
			, IsStandardUserOnly__c = false
			, IsPartnerUserOnly__c = false
//            , IsRequiredAuthCustomerSearch__c = false
//            , IsRequiredAuthInvestment__c = false
//            , IsRequiredAuthDownload__c = false
//            , IsRequiredAuthCustomerInfo__c = false
			, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
		);
		insert menuMasterChild;
		selectedMenu = new E_SelectedMenu__c(MenuKind__c = menuKind.id, MenuMaster__c = menuMasterChild.id, ExternalId__c = 'MK_Agency|home');
		insert selectedMenu;
	}


	private static testMethod void testAdminUser() {
		createTestData();

		System.runAs(TestE_TestUtil.createUser(true, 'menutest', 'システム管理者')) {
			Test.startTest();
			PageReference pref = Page.E_ErrorPage;
			pref.getHeaders().put('X-Salesforce-SIP', 'hogehoge');
			Test.setCurrentPage(pref);

			E_MenuController menuController = new E_MenuController();
			menuController.isHideMenu = false;
			menuController.selectingMenuKeys = 'home';

			// #getMenuItems1
			menuController.getMenuItems();
			// #getIsStandardUser
			System.assertEquals(true, menuController.getIsStandardUser());
			// #changeMenuKind
			System.assertEquals('/apex/e_home', menuController.changeMenuKind().getUrl());
			// #getRenderedMenu
			System.assertEquals(true, menuController.getRenderedMenu());
			// #getMenuItem2
			menuController.getMenuItems();
			// #getIsPopUp
//			System.assertEquals(false, menuController.getIsPopUp());
			// #getIPadress
//			menuController.getIPadress();

			Test.stopTest();
		}
	}

	private static testMethod void doChangeAccessKind() {
		createTestData();

		System.runAs(TestE_TestUtil.createUser(true, 'menutest', 'システム管理者')) {
			Test.startTest();
			PageReference pref = Page.E_ErrorPage;
			pref.getHeaders().put('X-Salesforce-SIP', 'hogehoge');
			Test.setCurrentPage(pref);

			E_MenuController menuController = new E_MenuController();
			menuController.getMenuItems();
			menuController.selectingMenuKeys = 'home';
			menuController.menuKindSelected = 'MK_Agency';
			menuController.accessKindSelected = '0';

			PageReference result = menuController.changeAccessKind();

			Test.stopTest();
		}
	}

	private static testMethod void testPartnerUser() {
		User runUs = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(runUs){
			createTestData();
			Account acc = TestE_TestUtil.createAccount(true);
			Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
			user u = TestE_TestUtil.createUser(false, 'menutest', 'E_PartnerCommunity');
			TestE_TestUtil.createEbizLog(true);
			u.contactId = con.Id;
			insert u;

			Test.startTest();
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, u.Id);
			eidc.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
			eidc.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_IRIS;
			insert eidc;
			Test.stopTest();

			System.runAs(u) {

				PageReference pref = Page.E_ErrorPage;
				Test.setCurrentPage(pref);

				E_MenuController menuController = new E_MenuController();
				menuController.isHideMenu = false;
				menuController.selectingMenuKeys = 'hogehoge';

				// #getMenuItems1
				menuController.getMenuItems();
				menuController.getIsDisplayTubeBanner();

				// result
				System.assertEquals('/apex/e_errorpage', pref.getUrl());
			}
		}
	}

	private static testMethod void testBankUser() {
		User runUs = [select Id from User where Id = :UserInfo.getUserId()];
		System.runAs(runUs){
			createTestData();
			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = 'MK_BankAgency', SortNumber__c = 1, Name = '銀行代理店');
			insert menuKind;
			Account acc = TestE_TestUtil.createAccount(true);
			Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
			user u = TestE_TestUtil.createUser(false, 'menutest', 'E_PartnerCommunity');
			TestE_TestUtil.createEbizLog(true);
			u.contactId = con.Id;
			insert u;

			Test.startTest();
			E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, u.Id);
			eidc.ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE;
			eidc.ZDSPFLAG02__c = E_Const.ZDSPFLAG02_BANK;
			insert eidc;
			Test.stopTest();

			System.runAs(u) {

				PageReference pref = Page.E_ErrorPage;
				Test.setCurrentPage(pref);

				E_MenuController menuController = new E_MenuController();
				menuController.isHideMenu = false;
				menuController.selectingMenuKeys = 'hogehoge';

				// #getMenuItems1
				menuController.getMenuItems();
				menuController.getIsDisplayTubeBanner();

				// result
				System.assertEquals('/apex/e_errorpage', pref.getUrl());
			}
		}
	}

}