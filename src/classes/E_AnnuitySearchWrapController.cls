public class E_AnnuitySearchWrapController extends E_AbstractSearchExtender{
    /**
     * Constractor
     */
    public E_AnnuitySearchWrapController() {
        pgTitle = '契約照会（契約一覧検索）';
    }
    
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		return new PageReference(E_CookieHandler.getCookieGeneralReferer(E_CookieHandler.REFERER_SEARCH));
	}
}