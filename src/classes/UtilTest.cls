@isTest
private class UtilTest {

    static testMethod void myUnitTest01() {
        system.debug('#### return：' + Util.cnvNullStr(null));
        system.debug('#### return：' + Util.cnvNullStr('aaa'));
    }

    static testMethod void myUnitTest02() {
        system.debug('#### return：' + Util.cnvStrDate('2010/11/01'));
    }

    static testMethod void myUnitTest03() {
        system.debug('#### return：' + Util.getFirstDate('2010/12/23'));
    }

    static testMethod void myUnitTest04() {
        system.debug('#### return：' + Util.getLastDate('2010/12/01'));
    }

    static testMethod void myUnitTest05() {
        system.debug('#### return：' + Util.getPicklist(new WeeklyTotalBudget__c(), 'Type__c'));
    }

}