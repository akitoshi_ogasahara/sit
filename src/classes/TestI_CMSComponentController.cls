@isTest
private class TestI_CMSComponentController {

	static String domID = 'domID';
	static String url = 'URL';
	static String chatter = 'DL(ChatterFile)';

	//getDomIdのテスト--record.isBlank
	@isTest static void getDomIdRecordIsBlankTest() {
		List<Attachment> att = null;
		I_ContentMaster__c icm = new I_ContentMaster__c(Dom_Id__c = domID);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		String dom = ccc.getDomId();
		System.assertEquals(dom,'iris-cms-' + domID);
		Test.stopTest();
	}

	//getDomIdのテスト
	@isTest static void getDomIdRecordTest() {
		List<Attachment> att = null;
		I_ContentMaster__c icm = new I_ContentMaster__c();
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		String dom = ccc.getDomId();
		System.assertEquals(dom,'iris-cms-' + icm.Id);
		Test.stopTest();
	}

	//getThumbnailのテスト--DL指定されていないファイル
	@isTest static void getThumbnailLTest() {
		List<Attachment> att = new List<Attachment>();
		att.add(createAttachment());
		I_ContentMaster__c icm = new I_ContentMaster__c(FilePath__c = 'Sample.txt');
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Attachment attRec = ccc.getThumbnail();
		System.assertEquals(attRec.Id,att[0].Id);
		Test.stopTest();
	}

	//getThumbnailのテスト--DL指定されているファイル
	@isTest static void getThumbnailDLTest() {
		List<Attachment> att = new List<Attachment>();
		att.add(createAttachment());
		I_ContentMaster__c icm = new I_ContentMaster__c(FilePath__c = 'Test.txt');
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Attachment attRec = ccc.getThumbnail();
		System.assertEquals(attRec,att[0]);
		Test.stopTest();
	}

	//getThumbnailのテスト--Attachment0件
	@isTest static void getThumbnailTest() {
		List<Attachment> att = new List<Attachment>();
		I_ContentMaster__c icm = new I_ContentMaster__c(FilePath__c = 'Test.txt');
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Attachment attRec = ccc.getThumbnail();
		System.assertEquals(attRec,null);
		Test.stopTest();
	}

	//getExistsDownloadFileのテスト
	@isTest static void getExistsDownloadFileTest() {
		List<Attachment> att = new List<Attachment>();
		att.add(createAttachment());
		I_ContentMaster__c icm = new I_ContentMaster__c(FilePath__c = 'Test.txt',ClickAction__c = 'DL(Attachment)');
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Boolean exist = ccc.getExistsDownloadFile();
		System.assertEquals(exist,true);
		Test.stopTest();
	}

	//getDLContentVersionのテスト--02版以降実装予定
	@isTest static void getDLContentVersionTest() {
		List<Attachment> att = new List<Attachment>();
		I_ContentMaster__c icm = new I_ContentMaster__c();
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		ContentVersion content = ccc.getDLContentVersion();
		System.assertEquals(content,null);
		Test.stopTest();
	}
/*
	//getNNLinkAttachmentのテスト--02版以降実装予定
	@isTest static void getNNLinkAttachmentTest() {
		List<Attachment> att = new List<Attachment>();
		I_ContentMaster__c icm = new I_ContentMaster__c();
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Attachment nnatt = ccc.getNNLinkAttachment();
		System.assertEquals(nnatt,null);
		Test.stopTest();
	}
*/
	//getHasClickActionのテスト
	@isTest static void getHasClickActionTest() {
		List<Attachment> att = new List<Attachment>();
		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = url);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Boolean click = ccc.getHasClickAction();
		System.assertEquals(click,true);
		Test.stopTest();
	}

	//getIsforUrlのテスト
	@isTest static void getIsforUrlTest() {
		List<Attachment> att = new List<Attachment>();
		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = url);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Boolean urlIs = ccc.getIsforUrl();
		System.assertEquals(urlIs,true);
		Test.stopTest();
	}

	//getAnchorToのテスト
	@isTest static void getAnchorToTest() {
		List<Attachment> att = new List<Attachment>();
		I_PageMaster__c ipm = createIRISPage();
		I_ContentMaster__c icm = new I_ContentMaster__c(LinkURL__c = url,Page__c = ipm.Id,Name=url);
		insert icm;
		PageReference pageRef = Page.IRIS;
		pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
		Test.setCurrentPage(pageRef);

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		I_CMSController cms = new I_CMSController();
		String anchor = ccc.getAnchorTo();
		System.assertEquals(anchor,icm.Id);
		Test.stopTest();
	}

	//getActionIsDLAttachment
	@isTest static void getActionIsDLAttachment(){
		List<Attachment> att = new List<Attachment>();
		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = 'DL(Attachment)');
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Boolean click = ccc.getActionIsDLAttachment();
		System.assertEquals(click,true);
		Test.stopTest();
	}

	//getDLAttachment ClickActionがDL(Attacment)以外の場合
	@isTest static void getDLAttachment(){
		List<Attachment> att = new List<Attachment>();
		att.add(createAttachment());
		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = url);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		ccc.getDLAttachment();
		System.assertEquals(att[0].Name,'Test.txt');
		Test.stopTest();
	}

	//chatterFile
	@isTest static void getHasChatterFileTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		Boolean click = ccc.getHasChatterFile();
		System.assertEquals(click,false);
		Test.stopTest();
	}

	//getChatterFileThumbnail
	@isTest static void getChatterFileThumbnailTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		String cfile = ccc.getChatterFileThumbnail();
		System.assertEquals(cfile,null);
		Test.stopTest();
	}

	//getChatterFileDownloadUrl
	@isTest static void getChatterFileDownloadUrlTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		String cfile = ccc.getChatterFileDownloadUrl();
		System.assertEquals(cfile,null);
		Test.stopTest();
	}


	@isTest static void getFeatureDisplayTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm2 = createData2();

		List<I_ContentMaster__c> cntList = [
				SELECT
					Id,
					Name,
					FeatureFlag__c,
					FeatureDisplayTo__c,
					FeatureDisplayFrom__c
				FROM
					I_ContentMaster__c
				WHERE
					FeatureDisplayTo__c >= Today
				AND FeatureDisplayFrom__c <= today
				AND FeatureFlag__c = true
				ORDER BY
					FeatureDisplayTo__c DESC
				];
		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(cntList[0],att);
		Boolean fd = ccc.getFeatureDisplay();
		System.assertEquals(fd,true);
		Test.stopTest();
	}

	@isTest static void getShowUrlCopyTest(){
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.runAs(actUser){
			Test.startTest();
			I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
			System.assertEquals(ccc.getShowUrlCopy(),true);
			Test.stopTest();
		}
	}

	//getChatterFileDownloadUrl
	/*@isTest static void getChatterFileDownloadUrl_via_InternetTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		String cfile = ccc.getChatterFileDownloadUrl_via_Internet();
		System.assertEquals(cfile,URL.getSalesforceBaseUrl().toExternalForm());
		Test.stopTest();
	}
	*/

	@isTest static void getChatterFileDownloadUrl_via_InternetTest() {
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
		I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
		String cfile = ccc.getChatterFileDownloadUrl_via_Internet();
		System.assertNOTEquals(cfile, '');
		Test.stopTest();
	}

	@isTest static void getCategoryNamesTest(){
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter,Category__c = '定期保険;無解約定期保険;商品Ａ');
		insert icm;

		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.runAs(actUser){
			Test.startTest();
			I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
			//List<String> categoryNull = new List<String>();
			System.assertEquals(ccc.getCategoryNames()[0],'定期保険');
			Test.stopTest();
		}
	}


	@isTest static void getCategoryNamesNullTest(){
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.runAs(actUser){
			Test.startTest();
			I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
			List<String> categoryNull = new List<String>();
			System.assertEquals(ccc.getCategoryNames(),categoryNull);
			Test.stopTest();
		}
	}

	@isTest static void getSalesToolShortcutUrlTest(){
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.runAs(actUser){
			Test.startTest();
			I_CMSComponentController ccc = new I_CMSComponentController(icm,att);
			System.assertNOTEquals(ccc.getSalesToolShortcutUrl(),'');
			Test.stopTest();
		}
	}

	@isTest static void getMailBodyTest(){
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
			I_CMSComponentController ccc = new I_CMSComponentController(icm,att,null);
			System.assert(String.isNotBlank(ccc.getMailBody()));
		Test.stopTest();
	}

	@isTest static void getMailSubjectTest(){
		List<Attachment> att = new List<Attachment>();

		ContentVersion content = new ContentVersion(Title='ContentsName',PathOnClient='/foo.txt');
		content.VersionData = Blob.valueOf('Testing base 64 encode');
		insert content;

		I_ContentMaster__c icm = new I_ContentMaster__c(ClickAction__c = chatter);
		insert icm;

		Test.startTest();
			I_CMSComponentController ccc = new I_CMSComponentController(icm,att,null);
			System.assert(String.isNotBlank(ccc.getMailSubject()));
		Test.stopTest();
	}

	//テストデータ作成
	//添付ファイル
	static Attachment createAttachment(){
		Account acc = TestE_TestUtil.createAccount(true);
		Attachment att = new Attachment();
		att.Name = 'Test.txt';
		att.Body = Blob.valueOf('テスト');
		att.ParentId = acc.Id;
		insert att;
		return att;
	}

	//irisPage
	static I_PageMaster__c createIRISPage(){
		I_PageMaster__c pm = new I_PageMaster__c();
		insert pm;
		return pm;
	}

	//データ作成
	static I_ContentMaster__c createData(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		I_ContentMaster__c icm = createIRISCMS(ipm);
		//ID管理
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		return icm;
	}


	//データ作成
	static I_ContentMaster__c createData2(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		I_PageMaster__c ipm2 = createIRISPage2(mm.Id);
		I_ContentMaster__c icm = createIRISCMS2(ipm,ipm2);
		//ID管理
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		return icm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.Menu__c = mmId;
		pm.Default__c = true;
		pm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		pm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		insert pm;
		return pm;
	}

	//irisPage2
	static I_PageMaster__c createIRISPage2(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey1';
		pm.Menu__c = mmId;
		pm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		pm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS(icm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		insert icm;
		return icm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS2(I_PageMaster__c ipm,I_PageMaster__c ipm2){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS2(icm,ipm2);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS2(I_ContentMaster__c paricm,I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.IRIS_Page__c = ipm.Id;
		insert icm;
		return icm;
	}

	static I_MenuMaster__c createIRISMenuMaster(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = '7.特集';
		//mm.Name = '特集';
		mm.SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE;
		insert mm;
		return mm;
	}

}