global with sharing class MNT_PamphletViewController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public MNT_PamphletViewController(ApexPages.StandardController controller) {
		super(controller);
		deleteTransitionControl='/apex/MNT_PamphletSearch';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DOM_Id__c;
		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.Content__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.PamphletCategory__c;
		f = I_ContentMaster__c.fields.ProductCategory__c;
		f = I_ContentMaster__c.fields.Jurisdiction__c;
		f = I_ContentMaster__c.fields.DisplayOrder__c;
		f = I_ContentMaster__c.fields.CreatedDepartment__c;
		f = I_ContentMaster__c.fields.Reason__c;
		f = I_ContentMaster__c.fields.DocumentCategory__c;
		f = I_ContentMaster__c.fields.DocumentOrderFlg__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.WindowTarget__c;
		f = I_ContentMaster__c.fields.filePath__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_PamphletViewController';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('DOM_Id__c');
			mainQuery.addFieldAsOutput('ParentContent__c');
			mainQuery.addFieldAsOutput('FormNo__c');
			mainQuery.addFieldAsOutput('DisplayFrom__c');
			mainQuery.addFieldAsOutput('ApprovalNo__c');
			mainQuery.addFieldAsOutput('Content__c');
			mainQuery.addFieldAsOutput('ValidTo__c');
			mainQuery.addFieldAsOutput('PamphletCategory__c');
			mainQuery.addFieldAsOutput('ProductCategory__c');
			mainQuery.addFieldAsOutput('Jurisdiction__c');
			mainQuery.addFieldAsOutput('DisplayOrder__c');
			mainQuery.addFieldAsOutput('CreatedDepartment__c');
			mainQuery.addFieldAsOutput('Reason__c');
			mainQuery.addFieldAsOutput('DocumentCategory__c');
			mainQuery.addFieldAsOutput('DocumentOrderFlg__c');
			mainQuery.addFieldAsOutput('ClickAction__c');
			mainQuery.addFieldAsOutput('WindowTarget__c');
			mainQuery.addFieldAsOutput('filePath__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = false;
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}