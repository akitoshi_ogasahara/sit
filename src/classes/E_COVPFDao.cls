public with sharing class E_COVPFDao {

	public static final String TYPE_C = 'C';

    /**
	 * 被保険者IDをキーに、指定した個人保険特約をList形式で取得する
	 * @param policyId: 被保険者ID
	 * @return E_COVPF__c[]: 個人保険特約マップ
	 */
	public static Map<id, E_COVPF__c> getRecById (Set<Id> policyId) {

		Map<String, Id> recordTypeMap = E_RecordTypeDao.getRecordTypeBySobjectType('E_COVPF__c');

		Map<id, E_COVPF__c> recMap = new Map<id, E_COVPF__c>();
		for(E_COVPF__c cov : [select id, COMM_ZCOVRNAM__c,COLI_ZPSTDESC__c, E_Policy__c, COLI_ZCOVRNAMES__c from E_COVPF__c where E_Policy__c in: policyId and COLI_ZCRIND__c =: TYPE_C and RecordTypeId =: recordTypeMap.get('ECOVPF')]){
			recMap.put(cov.E_Policy__c, cov);
		}
		return recMap;
	}
    /**
	 * 保険契約ヘッダIDをキーに、指定した個人保険特約をマップ形式で取得する
	 * @param policyId: 保険契約ヘッダID
	 * @return Map<id, E_COVPF__c>: 個人保険特約マップ
	 */
	public static E_COVPF__c[] getRecByIds (Id policyId) {

		Map<String, Id> recordTypeMap = E_RecordTypeDao.getRecordTypeBySobjectType('E_COVPF__c');

		return [
					select
							Id
						, COLI_ZCRIND__c       //主契約フラグ
						, COMM_ZCOVRNAM__c     //保険種類
						, COMM_ZNAME40A01__c   //保険種類（２） ２行目
						, COMM_ZNAME40A02__c   //保険種類（３）
						, E_Policy__c          //保険契約ヘッダへの参照項目
						, COLI_ZCOVRNAMES__c   //保険種類(結合)
						, COMM_ZCOVRNAMFIX__c  //保険種類コード(数式) AdditionalRequest対応
						, COMM_ZPCESDSC__c     //払込期間(COMM_Z'P'CESDSC__c)
						, COMM_ZRCESDSC__c     //保険期間(COMM_Z'R'CESDSC__c)
						, COLI_ZSTDSUM__c      //加入時保険金額
						, COLI_INSTPREM__c     //保険料
						, COLI_ZPSTDESC__c     //現在の状況
						, COLI_ZSUMINF__c      //現在Ｓ計算不能不能フラグ
						, COMM_SUMINS__c       //主契約現在保険金額
						, COLI_SUMINSF__c      //主契約現在保険金額(数式)
						, COLI_ZDPTYDSP__c     //指定代理請求特約表示フラグ
						, COLI_ZDTHAMTA__c		//現在死亡保険金額
						, ZDCPFLG__c 			//P逓減特則フラグ
						, DPIND__c 				//P免特則フラグ
						, COMM_CRTABLE2__c      //保険種類コード（数式）
					from
						E_COVPF__c
					where
						E_Policy__c =: policyId
					and
						RecordTypeId =: recordTypeMap.get('ECOVPF')
						//COLI画面のデータテーブルは主契約を先に表示したい
						Order by COLI_ZCOVRSEQ__c
				];
	}

		/**
	 * [IRIS] 失効契約用
	 */
	public static List<E_COVPF__c> getRecsIRISLapsedPolicy(String policyCondition){
		String sDate = String.valueOf(Date.Today().addYears(-3));
		sDate = sDate.replace('-', '');
		String recType = E_Const.POLICY_RECORDTYPE_COLI;
		String statCode = I_Const.LAPS_POLICY_STATCODE;
		String zrst = I_Const.LAPS_POLICY_ZRSTDESC;
		String frag = I_Const.COVPF_COLI_ZCRIND_C;

		String soql = 'SELECT ' +
			+ 'E_Policy__r.COMM_STATDATE__c, '									//契約状況発生年月日
			+ 'E_Policy__r.Contractor__c, '										//契約者
			//+ 'E_Policy__r.COMM_ZCOVRNAM__c, '									//保険種類
			//SITP(2017/12/1R) 保健種類(結合)表示対応 start
			+ 'E_Policy__r.COLI_ZCOVRNAMES__c, '								//保険種類(結合)
			//SITP(2017/12/1R) 保健種類(結合)表示対応 end
			+ 'E_Policy__r.Insured__c ,'										//被保険者
			+ 'E_Policy__r.COMM_SINSTAMT__c, '									//保険料
			+ 'E_Policy__r.COMM_CHDRNUM__c, '									//証券番号
			+ 'E_Policy__r.COMM_OCCDATE__c, '									//契約日
			+ 'E_Policy__r.ContractorName__c, '									//契約者氏名
			+ 'E_Policy__r.COLI_ZCSHVAL__c, '									//解約返戻金
			+ 'E_Policy__r.COLI_ZEAPLTOT__c, '									//保険料振替貸付合計
			+ 'E_Policy__r.COLI_ZEPLTOT__c, '									//契約者貸付合計
			+ 'E_Policy__r.COLI_ZEADVPRM__c, '									//前納未経過保険料
			+ 'E_Policy__r.COLI_ZUNPREM__c, '									//未経過保険料
			+ 'E_Policy__r.COLI_ZUNPCF__c, '									//未経過保険料計算不能フラグ
			+ 'E_Policy__r.COLI_ZUNPDCF__c, '									//未経過保険料表示フラグ
			+ 'E_Policy__r.COLI_ZCVDCF__c, '									//解約返戻金計算不能フラグ
			+ 'E_Policy__r.InsuredName__c, ' 									//被保険者氏名
			+ 'E_Policy__r.MainAgentAccountParenName__c, '						//主代理店
			+ 'E_Policy__r.SubAgentAccountParentName__c, '						//従代理店
			+ 'E_Policy__r.MainAgentAccountName__c, '							//主事務所
			+ 'E_Policy__r.SubAgentAccountName__c, '							//従事務所
			+ 'E_Policy__r.MainAgentName__c, '									//主募集人氏名
			+ 'E_Policy__r.SubAgentName__c, '									//従募集人氏名
			+ 'E_Policy__r.MainAgentAccountMRName__c, '							//主担当MR名
			+ 'E_Policy__r.SubAgentAccountMRName__c, '							//従担当MR名
			+ 'E_Policy__r.Id, '												//保険契約ID
			//スプリントバックログ9(2017/1/18)　カナソート用項目ここからE_Policy__rがついているところ まで
			+ 'E_Policy__r.ContractorE_CLTPF_ZCLKNAME__c, '						//契約者カナ名
			+ 'E_Policy__r.MainAgentNameKana__c, '								//主募集人氏名カナ（数式）
			+ 'E_Policy__r.MainAgentAccountNameKana__c, '						//主募集人代理店事務所名カナ（数式）
			+ 'COLI_INSTPREM__c, '												//保険料
			+ 'COMM_SUMINS__c, '												//主契約現在保険金額
			+ 'COMM_STATDATE__c, '												//契約状況発生年月日
			//+ 'COMM_ZCOVRNAM__c, '												//保険種類
			//SITP(2017/12/1R) 保健種類(結合)表示対応 start
			+ 'COLI_ZCOVRNAMES__c, '											//保険種類(結合)
			//SITP(2017/12/1R) 保健種類(結合)表示対応 end
			+ 'COLI_ZCRIND__c '													//主契約フラグ
		+ ' FROM '
			+ 'E_COVPF__c'
		+ ' WHERE '
			+ 'E_Policy__r.COMM_STATCODE__c = :statCode'
		+ ' AND '
			+ 'E_Policy__r.COMM_ZRSTDESC__c = :zrst'
		+ ' AND '
			+ 'E_Policy__r.RecordType.DeveloperName = :recType'
		+ ' AND '
			+ 'COLI_ZCRIND__c = :frag'
		+ ' AND '
			+ 'COMM_STATDATE__c >= :sDate'
		// 自動検索中は条件を追加する
		+ policyCondition
		+ ' LIMIT ' + I_Const.LIST_MAX_ROWS;//1000件

		return Database.query(soql);
	}


	//JIRA_#64
	/**
	 * [IRIS] 失効契約用
	 */
	public static List<AggregateResult> getRecsIRISCOVPFSearchBox(String keyword, String searchType, String policyCondition){
		String sDate = String.valueOf(Date.Today().addYears(-3));
		sDate = sDate.replace('-', '');
		String recType = E_Const.POLICY_RECORDTYPE_COLI;
		String statCode = I_Const.LAPS_POLICY_STATCODE;
		String zrst = I_Const.LAPS_POLICY_ZRSTDESC;
		String frag = I_Const.COVPF_COLI_ZCRIND_C;

		// WHERE句を生成
		String soqlWhere = '';
		List<String> keywords = new List<String>();

		// 契約者名
		if(searchType == I_Const.SEARCH_TYPE_OWNER){
			// キーワード分割
			for(String text : I_Util.createSearchkeywordList(keyword)){
				keywords.add('%' + String.escapeSingleQuotes(text) + '%');
			}
			soqlWhere = 'AND (E_Policy__r.ContractorName__c like :keywords OR E_Policy__r.ContractorE_CLTPF_ZCLKNAME__c like :keywords) ';
		// 被保険者名
		} else if(searchType == I_Const.SEARCH_TYPE_INSURED){
			// キーワード分割
			for(String text : I_Util.createSearchkeywordList(keyword)){
				keywords.add('%' + String.escapeSingleQuotes(text) + '%');
			}
			soqlWhere = 'AND (E_Policy__r.InsuredName__c like :keywords OR E_Policy__r.InsuredE_CLTPF_ZCLKNAME__c like :keywords) ';
		// 証券番号
		} else if(searchType == I_Const.SEARCH_TYPE_POLICY_NO){
			// 左右のスペースを削除
			keyword = E_Util.LTrim(E_Util.RTrim(keyword)) + '%';
			soqlWhere = 'AND E_Policy__r.COMM_CHDRNUM__c LIKE :keyword ';
		}

		String soql = 'SELECT '
			+ 'E_Policy__c '
		+ ' FROM '
			+ 'E_COVPF__c'
		+ ' WHERE '
			+ 'E_Policy__r.COMM_STATCODE__c = :statCode'
		+ ' AND '
			+ 'E_Policy__r.COMM_ZRSTDESC__c = :zrst'
		+ ' AND '
			+ 'E_Policy__r.RecordType.DeveloperName = :recType'
		+ ' AND '
			+ 'COLI_ZCRIND__c = :frag'
		+ ' AND '
			+ 'COMM_STATDATE__c >= :sDate '
		+ soqlWhere
		// 自動検索中は条件を追加する
		+ policyCondition
		+ ' GROUP BY E_Policy__c '
		+ ' LIMIT ' + I_Const.LIST_MAX_ROWS;//1000件

		return Database.query(soql);
	}

    /**
	 * 保険契約ヘッダIDとレコードタイプDEVELOPERNAMEをキーに、データを取得する
	 * @param pId: 保険契約ヘッダID
	 * @param rtDevname: レコードタイプDEVELOPERNAME
	 * @return List<E_COVPF__c>: 個人保険特約リスト
	 */
	public static List<E_COVPF__c> getRecsByPolicyIdAndRtdevname(Id pid, String rtDevname) {

		String soql = 'Select ';
		soql += ' Id ';
		soql += ' ,COMM_CHDRNUM__c ';
		soql += ' ,COLI_ZCRIND__c ';
		soql += ' ,COLI_INSTPREM__c ';
		soql += ' ,DPIND__c ';
		soql += ' ,ZDCPFLG__c ';
		soql += ' ,COMM_ZRCESDSC__c ';
		soql += ' ,COMM_ZPCESDSC__c ';
		soql += ' ,COLI_ZSTDSUM__c ';
		soql += ' ,COLI_ZSUMINF__c ';
		soql += ' ,COMM_STATDATE__c ';
		soql += ' ,COMM_ZCOVRNAMFIX__c ';
		soql += ' ,COMM_SUMINS__c ';
		soql += ' ,COMM_CRTABLE2__c ';
		soql += ' ,COLI_ZDPTYDSP__c ';
		soql += ' ,COMM_ZCOVRNAM__c ';
		soql += ' ,COLI_ZCOVRNAMES__c ';
		soql += ' ,DCOLI_ZCLNAME__c ';
		soql += ' ,DCOLI_ZINSNAM__c ';
		soql += ' ,DCOLI_ZCOVRDSC__c ';
		soql += ' ,DCOLI_SINSTAMT__c ';
		soql += ' ,DCOLI_OCCDATE__c ';
		soql += ' ,COLI_MORTCLS__c ';
		soql += ' ,COLI_LIENCD__c ';
		soql += ' ,COLI_BASICINSURANCE__c ';
		soql += ' ,E_Policy__r.COMM_CCDATE__c ';
		soql += ' From ';
		soql += ' E_COVPF__c ';
		soql += ' Where ';
		soql += ' E_Policy__r.Id = :pid ';
		soql += ' And ';
		soql += ' RecordType.DeveloperName = :rtDevname ';
		soql += ' Order by ';
		// 個人保険特約消滅（EHDCPF）
		if(E_Const.COVPF_RECORDTYPE_EHDCPF.equals(rtDevname)){
			soql += ' DCOLI_ZCOVRDSC__c asc ';
		// 消滅以外
		}else{
			soql += ' COLI_ZCOVRSEQ__c asc ';
		}

		return Database.query(soql);
	}
}