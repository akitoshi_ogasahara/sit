/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DailyActivityReportUpdateTriggerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        System.debug('-------------------------------------------------------DailyActivityReportUpdateTriggerTest：START');
        try{

            User userData = [SELECT Id, Unit__c, UnitName__c FROM User where isActive=true limit 1];
            userData.UnitName__c = 'テスト拠点名';
            userData.Unit__c = 'テスト営業部';
            update userData;

            System.runAs(userData){

                DailyActivityReport__c darData = new DailyActivityReport__c();
                darData.WeekStartDay__c = System.today();
                darData.Area__c = 'テスト';
                darData.SalesUnit__c = 'テスト';
                insert darData;

                DailyActivityReport__c InputDataList = [SELECT Id, Area__c, SalesUnit__c FROM DailyActivityReport__c WHERE ID =: darData.Id];
                // 所有者の拠点名と一致すること
                System.assertEquals(userData.UnitName__c, InputDataList.Area__c);
                // 所有者の営業部と一致すること
                System.assertEquals(userData.Unit__c, InputDataList.SalesUnit__c);

            }

        }catch(QueryException qe){
            System.debug('-------------------------------------------------------QueryException例外内容'+qe);
        }catch(Exception e){
            System.debug('-------------------------------------------------------Exception例外内容'+e);
        }
        System.debug('-------------------------------------------------------DailyActivityReportUpdateTriggerTest：END');
    } // END myUnitTest()

}