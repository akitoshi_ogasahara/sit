public without sharing class E_DownloadHistoryDaoWithout {

	/**
	 * 代理店提出とフォームNoとステータスと検索条件をキーに、ダウンロード履歴情報を取得する
	 * @param String 代理店提出ID
	 * @param String フォームNo
	 * @param String ステータス
	 * @return Integer
	 */
	public static List<E_DownloadHistorry__c> getRecsByAgencyAndFormNoAndStatus(String agencyId, String formNo, String status, Datetime reqFromDatetime){
		return [
					Select 
						Id, Conditions__c, OutputRecordYM__c
					From 
						E_DownloadHistorry__c 
					Where 
						CR_Agency__c = :agencyId
					And
						FormNo__c = :formNo
					And
						Status__c = :status
					And
						RequestDateTime__c >= :reqFromDatetime
		];
	}
	
	/**
	 * 代理店提出とフォームNoとステータスと検索条件をキーに、ダウンロード履歴情報を取得する
	 * @param String 代理店提出ID
	 * @param String フォームNo
	 * @param String ステータス
	 * @return Integer
	 */
	public static List<E_DownloadHistorry__c> getRecsByAgencyAndFormNoAndStatusAndEtlPlanDate(String agencyId, String formNo, String status, Date etlPlanDate){
		return [
					Select 
						Id, Conditions__c, OutputRecordYM__c
					From 
						E_DownloadHistorry__c 
					Where 
						CR_Agency__c = :agencyId
					And
						FormNo__c = :formNo
					And
						Status__c = :status
					And
						EtlPlanDate__c = :etlPlanDate
		];
	}
	
	
	/**
	 * 帳票種別と作成日からの経過日数に、ダウンロード履歴情報と添付ファイルを取得する。 
	 * @param Set<String> fmNos		帳票種別番号
	 * @param Date createdBefore	作成日条件
	 * @return ダウンロード履歴レコードリスト
	 */
	public static List<E_DownloadHistorry__c> getRecsAndFilesByfmNoAndDate(Set<String> fmNos, Date createdBefore){
		return [SELECT Id,Status__c,FormNo__c
					,(select id from Attachments)
				FROM E_DownloadHistorry__c
				WHERE CreatedDate < :createdBefore
				  AND FormNo__c in :fmNos
				  //AND Status__c = :CR_Const.DL_STATUS_SUCCESS		//ステータスは問わず日数経過分を削除します。
				];
	}
}