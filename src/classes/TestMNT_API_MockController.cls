@isTest
private class TestMNT_API_MockController {
   @isTest static void cnstTest1(){
	  	MNT_API_MockController con = new MNT_API_MockController();
	   	System.assertEquals(false, con.accessflg);
	}

   @isTest static void step1Test1(){
	   MNT_API_MockCOntroller con = new MNT_API_MockController();
	   PageReference pref = con.step1();
	   System.assertEquals(true, String.isNotBlank(pref.getUrl()));
	}

   
	@isTest static void initTest1(){
		PageReference pref = Page.MNT_API_Mock;
		pref.getParameters().put('code','hogehoge');
		Test.setCurrentPage(pref);

		MNT_API_MockController con = new MNT_API_MockController();
		PageReference pageRef = con.init();
		System.assertEquals(null, pageRef);
		System.assertEquals(true, con.accessflg);
	}

	@isTest static void initTest2(){
		MNT_API_MockController con = new MNT_API_MockController();
		PageReference pageRef = con.init();
		System.assertEquals(null, pageRef);
		System.assertEquals(false, con.accessflg);
	}


	@isTest static void callRestTest(){
		MNT_API_MockController con = new MNT_API_MockController();
		System.assertEquals(null, con.callRest());
	}


}