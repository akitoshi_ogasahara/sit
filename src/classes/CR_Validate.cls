public with sharing class CR_Validate {
	/* 規制強化Phase2******************************************************* */

	/**
	 * 出力基準年月チェック
	 * 空欄、形式(yyyy/MM) 
	 */
	public static Boolean hasErrOutputRecordYm(String outputYm){
		// 空欄チェック
		if(String.isBlank(outputYm)){
			return true;
		}
		
		// 形式チェック
		try{
			List<String> ym = outputYm.split('/');
			
			// 形式チェック
			Integer ymLen = ym[0].length() + ym[1].length() + 1;
			if(outputYm.length() != ymLen){
				return true;
			}
			
			// 半角数値チェック
			if(!(E_Util.isEnNum(ym[0]) && E_Util.isEnNum(ym[1]))){
				return true;
			}
			
			Integer y = Integer.valueOf(ym[0]);
			Integer m = Integer.valueOf(ym[1]);
			
			Date d = Date.newInstance(y, m, 1);
			
			// 年の比較
			if(d.year() != y){
				return true;
			}
			
			// 月の比較
			if(d.month() != m){
				return true;
			}
			
			return false;

		}catch(Exception e){
			system.debug('■[hasErrFormatDate]' + e.getMessage());
			return true;
		}
	}
	
	/**
	 * 出力基準年月チェック
	 * 空欄、形式(yyyy/MM)、期間
	 */
	public static Boolean hasErrOutputRecordYmFrom(String outputYm, String ymFrom, String ymTo){
		// 期間チェック
		try{
			// 期間定数
			Integer fromY = Integer.valueOf(ymFrom.split('/')[0]);
			Integer toY = Integer.valueOf(ymTo.split('/')[0]);
			Integer fromM = Integer.valueOf(ymFrom.split('/')[1]);
			Integer toM = Integer.valueOf(ymTo.split('/')[1]);
	
			// 出力基準年月
			List<String> ym = outputYm.split('/');		
			Integer y = Integer.valueOf(ym[0]);
			Integer m = Integer.valueOf(ym[1]);
			
			// 期間範囲チェック : 年
			if((fromY <= y && y <= toY) == false){
				return true;
			}
			
			// 期間範囲チェック : 月
			Integer fm = fromY == y ? fromM : 1;
			Integer tm = fromM == y ? toM : 12;
			if((fm <= m && m <= tm) == false){
				return true;
			}
			
			return false;

		}catch(Exception e){
			system.debug('■[hasErrFormatDate]' + e.getMessage());
			return true;
		}		
	}
	/*
	public static Boolean hasErrOutputRecordYmFrom(String outputYm, Map<String, Integer> ymMap){
		// 期間定数
		Integer fromY = ymMap.get(CR_Const.OUTPUT_RECORD_YM_KEY_FROM_YEAR);
		Integer toY = ymMap.get(CR_Const.OUTPUT_RECORD_YM_KEY_TO_YEAR);
		Integer fromM = ymMap.get(CR_Const.OUTPUT_RECORD_YM_KEY_FROM_MONTH);
		Integer toM = ymMap.get(CR_Const.OUTPUT_RECORD_YM_KEY_TO_MONTH);

		// 期間チェック
		try{
			List<String> ym = outputYm.split('/');		
			Integer y = Integer.valueOf(ym[0]);
			Integer m = Integer.valueOf(ym[1]);
			
			// 期間範囲チェック
			if(!((fromY <= y && y <= toY) && (fromM <= m && m <= toM))){
				return true;
			}
			
			return false;

		}catch(Exception e){
			system.debug('■[hasErrFormatDate]' + e.getMessage());
			return true;
		}
	}
	*/
	
	/**
	 * リクエスト同条件チェック
	 */
	public static Boolean hasErrSameCondition(String outputYm, String dlCondition, List<E_DownloadHistorry__c> dlList){

		// 月の0埋め処理
		List<String> ym = outputYm.split('/');
		outputYm = ym[0] + E_Util.leftPad(ym[1], 2, '0');
		for(E_DownloadHistorry__c dl : dlList){
			if(dlCondition == dl.Conditions__c && outputYm == dl.OutputRecordYM__c){
				return true;
			}
		}
		return false;
	}
	
	/** 
	 * リクエスト回数チェック
	 * 同じ出力条件で1日に4回以上リクエストしようとしている場合にエラー
	 */
	public static Boolean hasErrRequestCount(List<E_DownloadHistorry__c> dlList){
		return dlList.size() >= CR_Const.REQUEST_CNT_LIMIT;
	}
	
	/**
	 * リクエストチェック用の開始日付時間を取得
	 */
	public static Datetime getRequestFromDatetime(){
		Datetime fromDt;
		Date d = Date.today();

		// 実行時間が12～24時の間の場合、当日の12時をセット
		Integer ph = Datetime.now().hour();
		if(ph < 24 && ph >= 12 ){
		   fromDt = Datetime.newInstance(d.year(), d.month(), d.day(), 12, 00, 00);
		// 実行時間が00～11時の間の場合、前日の12時をセット
		}else{
			fromDt = Datetime.newInstance(d.year(), d.month(), d.day()-1, 12, 00, 00);
		}
		return fromDt;
	}
}