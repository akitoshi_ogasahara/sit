global with sharing class CR_OutsrcViewController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public CR_Outsrc__c record {get{return (CR_Outsrc__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public CR_OutsrcViewExtender getExtender() {return (CR_OutsrcViewExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public CR_OutsrcViewController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','upload_outsrc');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','編集');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!Extender.canEdit && Extender.isSuccessInit}');
        tmpPropMap.put('p_value','{!Extender.urlforEdit}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2466');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2466',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','外部委託先一覧');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!NOT(ISBLANK(Extender.UrlforOutsrcs))}');
        tmpPropMap.put('p_value','{!Extender.UrlforOutsrcs}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2465');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2465',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.UrlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2464');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2464',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2452');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2452',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','ファイルアップロード');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','{!Extender.canEdit}');
        tmpPropMap.put('p_value','{!Extender.UrlforUpload}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
		tmpPropMap.put('Component__Width','143');
		tmpPropMap.put('Component__Height','19');
		tmpPropMap.put('Component__id','Component2468');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2468',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_files','{!Extender.files}');
        tmpPropMap.put('p_canDelete','{!Extender.canDelete}');
		tmpPropMap.put('Component__Width','400');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','filelist');
		tmpPropMap.put('Component__Name','EAttachmentlist');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('filelist',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = CR_Outsrc__c.fields.Status__c;
		f = CR_Outsrc__c.fields.Name;
		f = CR_Outsrc__c.fields.Representative__c;
		f = CR_Outsrc__c.fields.Address__c;
		f = CR_Outsrc__c.fields.Business__c;
		f = CR_Outsrc__c.fields.OutsrcFrom__c;
		f = CR_Outsrc__c.fields.OutsrcTo__c;
		f = CR_Outsrc__c.fields.Confirm01__c;
		f = CR_Outsrc__c.fields.Confirm02__c;
		f = CR_Outsrc__c.fields.Confirm03__c;
		f = CR_Outsrc__c.fields.ReConsignor__c;
		f = CR_Outsrc__c.fields.Confirm04__c;
		f = CR_Outsrc__c.fields.Confirm05__c;
		f = CR_Outsrc__c.fields.Confirm06__c;
		f = CR_Outsrc__c.fields.SharedInfo__c;
		f = CR_Outsrc__c.fields.Confirm06_1__c;
		f = CR_Outsrc__c.fields.Confirm06_2__c;
		f = CR_Outsrc__c.fields.Confirm06_3__c;
		f = CR_Outsrc__c.fields.Confirm06_4__c;
		f = CR_Outsrc__c.fields.ReasonForNofiles__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = CR_Outsrc__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('CR_Outsrc__c');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('Representative__c');
			mainQuery.addFieldAsOutput('Address__c');
			mainQuery.addFieldAsOutput('Business__c');
			mainQuery.addFieldAsOutput('OutsrcFrom__c');
			mainQuery.addFieldAsOutput('OutsrcTo__c');
			mainQuery.addFieldAsOutput('Confirm01__c');
			mainQuery.addFieldAsOutput('Confirm02__c');
			mainQuery.addFieldAsOutput('Confirm03__c');
			mainQuery.addFieldAsOutput('ReConsignor__c');
			mainQuery.addFieldAsOutput('Confirm04__c');
			mainQuery.addFieldAsOutput('Confirm05__c');
			mainQuery.addFieldAsOutput('Confirm06__c');
			mainQuery.addFieldAsOutput('SharedInfo__c');
			mainQuery.addFieldAsOutput('Confirm06_1__c');
			mainQuery.addFieldAsOutput('Confirm06_2__c');
			mainQuery.addFieldAsOutput('Confirm06_3__c');
			mainQuery.addFieldAsOutput('Confirm06_4__c');
			mainQuery.addFieldAsOutput('ReasonForNofiles__c');
			mainQuery.addFieldAsOutput('hasCustomerInfo__c');
			mainQuery.addFieldAsOutput('CR_Agency__c');
			mainQuery.addFieldAsOutput('CR_Agency__r.AgencyCode__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_OutsrcViewExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}