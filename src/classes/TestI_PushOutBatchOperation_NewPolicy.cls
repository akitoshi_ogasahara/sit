/**
 *
 */
@isTest
private class TestI_PushOutBatchOperation_NewPolicy {

	//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];	//Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Contact atCon;
	private static E_BizDataSyncLog__c log;

	/**
	 * void setQueryCondition()
	 */
	static testMethod void setQueryCondition_test01() {
		Database.QueryLocator result;

		// Data
		Datetime dt = System.now();
		E_BizDataSyncLog__c logAgo = createDataEBizLog(dt.addMinutes(-30));
		E_BizDataSyncLog__c log = createDataEBizLog(dt);
		System.debug('* logAgo * ' + logAgo.DataSyncStartDate__c);
		System.debug('* log * ' + log.DataSyncStartDate__c);

		Test.startTest();
		I_PushOutBatchOperation_NewPolicy operation = new I_PushOutBatchOperation_NewPolicy();
		operation.ebizLog = [Select Id, CreatedDate, DataSyncStartDate__c From E_BizDataSyncLog__c Where Id = :log.Id];
		operation.pType = I_Const.EBIZDATASYNC_KBN_NEWPOLICY;
		operation.setQueryCondition();
		result = operation.getQueryLocator();
		Test.stopTest();

		//
		String soql = '';
		String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		if(columns.isEmpty() == false){
			soql = ' Select Id';
			for(String col :columns){
				soql += ', ' + col;
			}
			soql += ' From ' + objApi;

			// 条件作成
			soql += ' Where ';

			// 団体
			soql += ' ( ';
			soql += ' KFRAMEIND__c = \'Y\' ';
			soql += ' And ';
			soql += ' GroupParentFlag__c = True ';
			soql += ' And ';
			soql += ' GroupStatusDate__c > :dt';
			soql += ' And ';
			soql += ' GroupStatus__c = \'' + I_NewPolicyConst.STATUS_NAME_I + '\'';
			soql += ' ) ';

			// 団体以外
			soql += ' Or ';
			soql += ' ( ';
			soql += ' KFRAMEIND__c <> \'Y\' ';
			soql += ' And ';
			soql += ' StatusDate__c > :dt';
			soql += ' And ';
			soql += ' Status__c = \'' + I_NewPolicyConst.STATUS_NAME_I + '\'';
			soql += ' ) ';
		}
		System.debug('* test* ' + soql);
		// assert
		System.assertEquals(String.valueOf(Database.getQueryLocator(soql)), String.valueOf(result));
	}

	/**
	 * List<I_PushOut__c> execute(List<Sobject> records)
	 */
	static testMethod void execute_test01() {

		// Data
		Datetime dt = System.now();
		E_BizDataSyncLog__c logAgo = createDataEBizLog(dt.addMinutes(-30));
		E_BizDataSyncLog__c log = createDataEBizLog(dt);
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, 'AT');
		createData(dt.addMinutes(-10));
system.debug(logAgo.DataSyncStartDate__c);
for(E_NewPolicy__c p : [Select KFRAMEIND__c, GroupStatusDate__c, StatusDate__c From E_Newpolicy__c]){
	system.debug(p);
}


		String soql = '';
		String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		if(columns.isEmpty() == false){
			soql = ' Select Id';
			for(String col :columns){
				soql += ', ' + col;
			}
			soql += ' From ' + objApi;

			dt = logAgo.DataSyncStartDate__c;

			// 条件作成
			soql += ' Where ';

			// 団体
			soql += ' ( ';
			soql += ' KFRAMEIND__c = \'Y\' ';
			soql += ' And ';
			soql += ' GroupParentFlag__c = True ';
			soql += ' And ';
			soql += ' GroupStatusDate__c > :dt';
			soql += ' And ';
			soql += ' GroupStatus__c = \'' + I_NewPolicyConst.STATUS_NAME_I + '\'';
			soql += ' ) ';

			// 団体以外
			soql += ' Or ';
			soql += ' ( ';
			soql += ' KFRAMEIND__c <> \'Y\' ';
			soql += ' And ';
			soql += ' StatusDate__c > :dt';
			soql += ' And ';
			soql += ' Status__c = \'' + I_NewPolicyConst.STATUS_NAME_I + '\'';
			soql += ' ) ';
		}
		List<SObject> sobjs = Database.query(soql);

		Test.startTest();
		I_PushOutBatchOperation_NewPolicy operation = new I_PushOutBatchOperation_NewPolicy();
		operation.ebizLog = [Select Id, CreatedDate, DataSyncStartDate__c From E_BizDataSyncLog__c Where Id = :log.Id];
		operation.pType = I_Const.EBIZDATASYNC_KBN_NEWPOLICY;
		List<I_PushOut__c> result = operation.execute(sobjs);
		Test.stopTest();

		// assert
		System.assert(result.isEmpty() == false);
	}

	/* Test Data */
	private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
		Profile p = [Select Id From Profile Where Name = :profileName];

		// Base Info
		user = new User(
			Lastname = 'test'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);

		// User
		if(profileName != PF_PARTNER){
			UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
			user.UserRoleId = portalRole.Id;
			insert user;

		// Partner User
		}else{
			system.runAs(thisuser){
				// Account 代理店格
				ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
				insert ahAcc;

				// Account 事務所
				ayAcc = new Account(
						Name = 'office1'
						,ParentId = ahAcc.Id
						,E_CL2PF_ZAGCYNUM__c = 'ay001'
						,E_COMM_VALIDFLAG__c = '1'
				);
				insert ayAcc;

				// Contact 募集人
				atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}

			user.ContactId = atCon.Id;
			insert user;

			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
		}
	}

	private static void createDataAccessObj(Id userId, String idType){
		system.runAs(thisuser){
			// 権限割り当て
			TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);

			// ID管理
			E_IDCPF__c idcpf = new E_IDCPF__c(
				User__c = userId
				,ZIDTYPE__c = idType
				,FLAG01__c = '1'
				,FLAG06__c = '1'
				,ZSTATUS01__c = '1'
				,ZDSPFLAG02__c = '0'
				,OwnerId = userId
			);
			insert idcpf;
		}
	}

	private static void createData(Datetime dt){
		system.runAs(thisuser){
			List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();
			// 団体一括以外
			recs.add(new E_NewPolicy__c(
				KFRAMEIND__c = 'N'
				,CHDRNUM__c = '00000001'
				,STATCODE__c = 'I'
				,Account__c = ayAcc.Id
				,Agent__c = atCon.Id
				,SyncDate__c = dt.addHours(9)
				,COMM_ZCLNAME__c = '契約者'
				,ContractorE_CLTPF_ZCLKNAME__c = 'ｹｲﾔｸｼｬ'
				,COMM_ZINSNAM__c = '被保険者'
				,InsuredE_CLTPF_ZCLKNAME__c = 'ﾋﾎｹﾝｼｬ'
			));

			// 団体一括
			recs.add(new E_NewPolicy__c(
				KFRAMEIND__c = 'Y'
				,GRUPKEY__c = '12345'
				,KPAKETNM__c = 01
				,CHDRNUM__c = '00000002'
				,STATCODE__c = 'I'
				,Account__c = ayAcc.Id
				,Agent__c = atCon.Id
				,SyncDate__c = dt.addHours(9)
				,COMM_ZCLNAME__c = '契約者'
				,ContractorE_CLTPF_ZCLKNAME__c = 'ｹｲﾔｸｼｬ'
				,COMM_ZINSNAM__c = '被保険者'
				,InsuredE_CLTPF_ZCLKNAME__c = 'ﾋﾎｹﾝｼｬ'
			));

			insert recs;
		}
	}

	/*  */
	private static E_BizDataSyncLog__c createDataEBizLog(Datetime dt){
		E_BizDataSyncLog__c log = new E_BizDataSyncLog__c();
		system.runAs(thisuser){
			log.Kind__c = I_Const.EBIZDATASYNC_KBN_PUSHOUT;
			log.NotificationKind__c = I_Const.EBIZDATASYNC_KBN_NEWPOLICY;
			log.DataSyncStartDate__c = dt;
			insert log;
		}
		return log;
	}
}