/**
* クラス名	  :  FinanceCheckPageController
* クラス概要 :  ファイナンス 確認画面(コントローラ)
* @created  :  2014/02/10 Hung Nguyen The
* @modified :  2015/12/03 KSVC Thao Doan Phuong
*/
public with sharing class FinanceCheckPageController {
	// ファイナンス情報
	public Finance__c finance{get; set;}
	// 顧客
	public  Customer__c customer{get; set;}
	public String strOrganization{get; set;}
	public Boolean isError{get; set;}
	/* 2015/12/08 KSVC THAO DOAN PHUONG ADD START */
	// タイムアウト回避の詳細
	public string	strFCTimeOut { get; set; }
	/* 2015/12/08 KSVC THAO DOAN PHUONG ADD END */
	/**
	* getCommonConst
	* クラウドシステム定数クラス初期化処理
	* @param	: なし
	* @return   : なし
	* @created  : 2014/02/10 Hung Nguyen The
	* @modified : 
	*/
	public CommonConst getCommonConst(){
		return new CommonConst();
	}
	/**
	* FinanceCheckPageController
	* ページ初期化処理
	* @param	: なし
	* @return   : なし
	* @created  : 2014/02/10 Hung Nguyen The
	* @modified : 
	*/
	public FinanceCheckPageController(){
		try{
			/* 2015/12/08 KSVC THAO DOAN PHUONG ADD START */
			// タイムアウト回避の詳細
			resetSessionTimeout();
			/* 2015/12/08 KSVC THAO DOAN PHUONG ADD END */
			isError = false;
			finance = new Finance__c();
			strOrganization = Userinfo.getOrganizationId();
			// (Urlパラメータからファイナンス情報obj.カスタムオブジェクトIDを受け取る)
			String strFinanceId = Apexpages.currentPage().getParameters().get(CommonConst.STR_PARAMETER_ID);
			// 2014/03/10 Hung Nguyen The 顧客名を取得する　Start
			String strCustomer = Apexpages.currentPage().getParameters().get(CommonConst.STR_PARAMETER_CUID);
			// 2014/03/10 Hung Nguyen The 顧客名を取得する　End
			// パラメータチェック
			if(String.isBlank(strFinanceId)){
				isError = true;
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_FC_Not));
				return;
			}
			// ファイナンス情報レコードを1件取得する。
			Finance__c financeFromUrl = new Finance__c();
			List<Finance__c> listFinanceFromUrl = new List<Finance__c>([Select Term__c, BS_PLFlg__c, Hearing__c 
																		From Finance__c 
																		Where Id =: strFinanceId AND Customer__c =: strCustomer Limit 1]);
			// 取得出来ない場合、エラーメッセージを表示する。
			if(listFinanceFromUrl.isEmpty()){
				isError = true;
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_FC_Not));
				return;
			}
			financeFromUrl = listFinanceFromUrl.get(0);
			// 取得出来ない場合、エラーメッセージを表示する。
			List<Finance__c> listFinanceTerm = new List<Finance__c>([Select Id 
																	 From Finance__c 
																	 Where Term__c =: financeFromUrl.Term__c - 1
																	 	AND Customer__c =: strCustomer]);
			if(listFinanceTerm.isEmpty()){
				isError = true;
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_FC2_Not));
				return;
			}
			// 2014/03/10 Hung Nguyen The 取得したデータの以下の値をOR条件でチェックする。 Start
			if(!financeFromUrl.BS_PLFlg__c || !financeFromUrl.Hearing__c){
				isError = true;
				// BS/PL情報または、ヒアリング情報がありません。再度、顧客画面から該当決算期のＢS/PL情報または、ヒアリング情報をご確認ください。
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_FC3_Not));
				return;
			}
			// ファイナンス情報を取得
			List<Finance__c> listFinance = new List<Finance__c>([Select Id,
																		Customer__c, // 顧客名
																		P_CR__c,// 表示_自己資本比率
																		P_CAR__c,// 表示_営業キャッシュ・フロー
																		P_CFS__c,// 表示_投資キャッシュ・フロー
																		P_CFI__c,// 表示_財務キャッシュ・フロー
																		P_CFF__c,// 売上高
																		PL_SV__c,// 表示_損益分岐点売上高
																		P_BEP__c,// 表示_キャッシュ・フロー損益分岐点売上高
																		P_CFBEP__c,// 表示_売上ショック度
																		P_SHOCK__c,// 販売費及び一般管理費
																		PL_SGAAE__c,// 表示_売上下落時の粗利益
																		P_GPDecline__c,// 表示_必要保障額①
																		P_NSA1__c,// 短期借入金
																		BS_STD__c,// 長期借入金
																		BS_LTD__c,// 役員借入金
																		H_LO__c,// 表示_必要保障額②
																		P_NSA2__c,// *法人税率定義.必要倍率   
																		TaxDefinition_NRate__c,// 遺族生活資金(月)
																		H_BFLEM__c,// 遺族生活資金(必要期間)  
																		H_BFLET__c,// 表示_必要保障額③
																		P_NSA3__c,// 表示_必要保障額(①～③)合計
																		P_NSATT__c,// 死亡保障額(法人契約)
																		H_DBAC__c,// 表示_不足している保障額(会社編)
																		P_GAM_C__c,// 表示_仮払金＋貸付金
																		P_TATLR__c,// 連帯保証債務額
																		H_JASGD__c,// 死亡保障額(個人契約)
																		H_DBAI__c,// 表示_不足している保障額(個人編)
																		P_GAM_I__c,// 販売費及び一般管理費
																		P_NRM1__c,// 銀行返済額(月)
																		H_BRP__c,// 表示_必要返済原資②
																		P_NRM2__c,// 表示_必要リスク対策資金+必要返済原資　合計③
																		P_NRMTT__c,// 含み益
																		H_BRM__c,// 表示_不足しているリスク対策資金・必要返済原資③-④
																		P_RMF__c,// 表示_(千円)現金・預金
																		P_SENAR__c,// 表示_(千円)商品・製品
																		P_SENPROD__c,// 表示_(千円)仕掛品・原材料
																		P_SENWIPRM__c,// 表示_(千円)有価証券
																		P_SENSE__c,// 表示_(千円)短期貸付金
																		P_SENSTLR__c,// 表示_(千円)前払金
																		P_SENPRP__c,// 表示_(千円)前払費用
																		P_SENPRC__c,// 表示_(千円)仮払金
																		P_SENOCA__c,// 表示_(千円)償却資産
																		P_SENDP__c,// 表示_(千円)土地
																		P_SENIFA__c,// 表示_(千円)投資有価証券
																		P_SENLTLR__c,// 表示_(千円)保証金・敷金   
																		P_SENPR__c,// 表示_(千円)その他の固定資産
																		BS_C_Cash__c,//掛目(現金及び預金)
																		BS_C_NR__c,//掛目(受取手形)
																		BS_C_AR__c,//掛目(売掛金)
																		BS_C_PROD__c,//掛目(商品・製品)
																		BS_C_WIPRM__c,//掛目(仕掛品・原材料)
																		BS_C_STLR__c,//掛目(短期貸付金)
																		BS_C_PRP__c,//掛目(前払金)
																		BS_C_PRC__c,//掛目(前払費用)
																		BS_C_TA__c,//掛目(仮払金)
																		BS_C_OCA__c,//掛目(その他の流動資産)
																		BS_C_DP__c,//掛目(償却資産)
																		BS_C_Land__c,//掛目(土地)
																		BS_C_IFA__c,//掛目(無形固定資産)
																		BS_C_IS__c,//掛目(投資有価証券)
																		BS_C_LTLR__c,//掛目(長期貸付金)
																		BS_C_DE__c,//掛目(保証金・敷金)
																		BS_C_PR__c,//掛目(保険料積立金)
																		BS_C_OFA__c,//掛目(その他の固定資産)
																		P_SENCash__c,//表示_(千円)現金・預金
																		P_SENNR__c,//表示_(千円)受取手形
																		P_SENTA__c,//表示_(千円)仮払金
																		P_SENLand__c,//表示_(千円)土地
																		P_SENIS__c,//表示_(千円)投資有価証券
																		P_SENDE__c,//表示_(千円)保証金・敷金
																		P_SENOFA__c,//表示_(千円)その他の固定資産
																		P_SENASTT__c,//表示_(千円)資産合計
																		BS_C_SE__c, // 掛目(有価証券)
																		P_SENNP__c, // 表示_(千円)支払手形
																		P_SENAP__c, // 表示_(千円)買掛金
																		P_SENSTD__c, // 表示_(千円)短期借入金
																		P_SENOAP__c, // 表示_(千円)未払金
																		P_SENAE__c, // 表示_(千円)未払費用
																		P_SENADR__c, // 表示_(千円)前受金
																		P_SENDR__c, // 表示_(千円)預り金
																		P_SENITP__c, // 表示_(千円)未払法人税等
																		P_SENCTP__c, // 表示_(千円)未払消費税等
																		P_SENOCL__c, // 表示_(千円)その他の流動負債
																		P_SENLTD__c, // 表示_(千円)長期借入金
																		P_SENBond__c, // 表示_(千円)社債
																		P_SENOLTL__c, // 表示_(千円)その他の固定負債
																		P_SENERB__c, // 表示_(千円)従業員退職金
																		P_SENORB__c, // 表示_(千円)役員退職金
																		P_SENLNAT__c, // 表示_(千円)負債合計
																		P_NSF__c, // 表示_必要清算資金
																		PL_CI__c, // 当期純利益
																		PL_DE__c, // 減価償却費
																		P_BRPY__c, // 表示_年間銀行返済額
																		P_SF1__c, // 表示_戦略資金①
																		PL_IBIT__c, // 税引前当期純利益
																		P_SF2__c, // 表示_戦略資金②
																		P_SF3__c, // 表示_戦略資金③(フリーキャッシュ・フロー)
																		BS_F_Cash__c, // 現金及び預金の増加額
																		P_SIM_CI__c, // 表示_(シュミ)当期純利益
																		P_SIM_SF1__c, // 表示_(シュミ)戦略資金①
																		P_SIM_IBIT__c, // 表示_(シュミ)税引前当期純利益
																		P_SIM_SF2__c, // 表示_(シュミ)戦略資金②
																		P_SIM_CFS__c, // 表示_(シュミ)営業キャッシュ・フロー
																		P_SIM_CFI__c, // 表示_(シュミ)投資キャッシュフロー
																		P_SIM_SF3__c, // 表示_(シュミ)戦略資金③(フリーキャッシュ・フロー)
																		P_SIM_Cash__c, // 表示_(シュミ)現金及び預金の増加額
																		P_QD__c, // 表示_当座比率
																		P_ROS__c, // 表示_ROS（売上高当期純利益率）
																		P_ROE__c, // 表示_ROE(自己資本当期純利益率）
																		P_ROA__c, // 表示_ROA（総資産経常利益率）
																		P_RTO__c, // 表示_売上債権回転率
																		P_ITO__c, // 表示_棚卸資産回転率
																		P_APTO__c, // 表示_買入債務回転率
																		P_DR__c, // 表示_債務償還年数
																		P_ICR__c, // 表示_インタレスト・カバレッジ・レシオ
																		IndustryAverage_QD__c, // *業界平均マスタ.当座比率
																		IndustryAverage_CAR__c, //  *業界平均マスタ.自己資本比率
																		IndustryAverage_ROS__c, // *業界平均マスタ.ROS（売上高当期純利益率）
																		IndustryAverage_ROE__c, // *業界平均マスタ.ROE(自己資本当期純利益率）
																		IndustryAverage_ROA__c, // *業界平均マスタ.ROA（総資産経常利益率）
																		IndustryAverage_RTO__c, // *業界平均マスタ.売上債権回転率
																		IndustryAverage_ITO__c, // *業界平均マスタ.棚卸資産回転率
																		IndustryAverage_APTO__c, // *業界平均マスタ.買入債務回転率
																		IndustryAverage_DR__c, // *業界平均マスタ.債務償還年数
																		IndustryAverage_ICR__c, // *業界平均マスタ.インタレスト・カバレッジ・レシオ
																		BS_CATT__c , // 流動資産合計 
																		BS_FATT__c , // 固定資産合計
																		BS_CLTT__c , // 流動負債合計 
																		BS_LLTT__c , // 固定負債合計
																		BS_NATT__c , // 純資産合計
																		P_SVDown__c, // 売上高下落時
																		H_RD__c, // 売上高下落率
																		P_INTOP__c , // 棚卸資産回転期間
																		P_ARTOP__c , // 売上債権回転期間
																		P_PDTOP__c , // 買入債務回転期間
																		P_WCTOP__c, // 運転資金回転期間
																		TaxDefinition_TaxRate__c,
																		TaxDefinition_NRateDisp__c,
																		H_CRAA__c,
																		// 2014/03/17 Quy Nguyen Trung Add
																		H_CRAA_Disp__c, // 社長の報酬減額可能額/年(表示_万円)
																		// 2014/03/17 Quy Nguyen Trung End
																		H_UG__c,
																		// 2014/03/10 Hung Nguyen The　項目を追加する Start
																		BS_PLFlg__c, // BS・PLフラグ
																		Hearing__c // ヒアリング情報フラグ
																		// 2014/03/10 Hung Nguyen The　項目を追加する End
																From Finance__c
																Where Term__c =: financeFromUrl.Term__c
																//Where Term__c =: financeFromUrl.Term__c - 1 マイナス１を削除しました　Yoshikawa
																// 2014/03/10 Hung Nguyen The　検索条件を追加する Start
																AND Customer__c =: strCustomer
																// 2014/03/10 Hung Nguyen The　検索条件を追加する End
																Order By LastModifiedDate DESC Limit 1]);
			// 2014/03/10 Hung Nguyen The 取得したデータの以下の値をOR条件でチェックする。 End
			finance = listFinance.get(0);
		}
		catch(Exception ex){
			isError = true;
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
	}
	/**
	* exportPDF
	* 「PDF出力画面へ」リンク処理
	* @param	: なし
	* @return   : なし
	* @created  : 2014/02/10 Hung Nguyen The
	* @modified : 
	*/
	public Pagereference exportPDF(){
		/*String strUrl = Label.PDFURL + '?' + CommonConst.STR_PARAMETER_FIID + '=' + finance.Id;
		Pagereference pagePdf = new Pagereference(strUrl);
		pagePdf.setRedirect(true);
		return pagePdf;*/
		return null;
	}
	/**
	* goToAccountDetail
	* 「顧客詳細情報へ」リンク処理
	* @param	: なし
	* @return   : なし
	* @created  : 2014/02/10 Hung Nguyen The
	* @modified : 
	*/
	public Pagereference goToAccountDetail(){
		Pagereference pageAccountDetail = new Pagereference('/' + finance.Customer__c);
		pageAccountDetail.setRedirect(true);
		return pageAccountDetail;
	}
	/**
	* resetSessionTimeout
	* カスタム設定を取得する
	* @param 	: なし
	* @return	: なし
	* @created  : 2015/12/03 KSVC Thao Doan Phuong
	* @modified : 2015/12/08 KSVC THAO DOAN PHUONG Deleted
	*/
	/*public void resetSessionTimeout(){
		List<FC_TIMEOUT__c> fcTimeoutList =  [select FC_TIMEOUT__c From FC_TIMEOUT__c];
	}
	/**
	* resetSessionTimeout
	* PERSONAタイムアウト回避
	* @param 	: なし
	* @return	: なし
	* @created  : 2015/12/08 KSVC THAO DOAN PHUONG
	* @modified : 
	*/
	public void resetSessionTimeout(){
		try{
			FC_TIMEOUT__c fcTimeOut = HearingDAO.getFcTimeOut();
			strFCTimeOut = fcTimeOut == null || fcTimeOut.FC_TIMEOUT__c == null ? '0' : String.valueOf(fcTimeOut.FC_TIMEOUT__c);
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
	}
}