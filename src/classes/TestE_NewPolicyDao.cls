@isTest
private class TestE_NewPolicyDao{
	//ProfileName
	private static String PF_SYSTEM = 'システム管理者';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];	//Id取得
	private static Account ahAcc;
	private static List<Account> ayAccList;
	private static Contact atCon;

	static testMethod void getNewPolicy(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			recs = E_NewPolicyDao.getNewPolicy('');
		}

		system.assert(!recs.isEmpty());

		Test.stopTest();
	}


	static testMethod void getgetNewPolicyById01(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			recs = E_NewPolicyDao.getNewPolicyById(newplcy.Id);
		}

		system.assert(!recs.isEmpty());

		Test.stopTest();

	}

	//団体の場合
	static testMethod void getgetNewPolicyById02(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'Y','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			recs = E_NewPolicyDao.getNewPolicyById(newplcy.Id);
		}

		system.assert(!recs.isEmpty());

		Test.stopTest();

	}

	static testMethod void getNewPolicysWithNumberOfLimit(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			E_NewPolicy__c newplcy2 = TestUtil_AMS.createNewPolicy(false,'P','TEST002',d1,'','',200002,'契約者名2','ケイヤクシャメイ2','被保険者名2','ヒホケンシャメイ2',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy2;
			recs = E_NewPolicyDao.getNewPolicysWithNumberOfLimit(2);
		}

		system.assertEquals(2,recs.size());

		Test.stopTest();

	}

	static testMethod void getRecsForCsv01(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			recs = E_NewPolicyDao.getRecsForCsv(ahAcc.id,'','');
		}

		system.assert(!recs.isEmpty());

		Test.stopTest();

	}

	static testMethod void getRecsForCsv02(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			recs = E_NewPolicyDao.getRecsForCsv('',ayAccList[0].id,'');
		}

		system.assert(!recs.isEmpty());

		Test.stopTest();

	}

	static testMethod void getRecsForCsv03(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		List<E_NewPolicy__c> recs = new List<E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			recs = E_NewPolicyDao.getRecsForCsv('','',atCon.id);
		}

		system.assert(!recs.isEmpty());

		Test.stopTest();

	}

	static testMethod void getRecsForLog(){
		Datetime d1 = system.now();
		createDataAccount();
		createUser(PF_SYSTEM);
		createDataAccessObj(user.Id,E_Const.ZIDTYPE_EP);
		Test.startTest();

		Map<Id, E_NewPolicy__c> recMap = new Map<Id, E_NewPolicy__c>();

		system.runAs(user){
			E_NewPolicy__c newplcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy;
			E_NewPolicy__c newplcy2 = TestUtil_AMS.createNewPolicy(false,'P','TEST002',d1,'','',200002,'契約者名2','ケイヤクシャメイ2','被保険者名2','ヒホケンシャメイ2',ahAcc.id,ayAccList[0].id,atCon.id);
		insert newplcy2;
		Set<Id> ids = new Set<Id>();
		ids.add(newplcy.id);
		ids.add(newplcy2.id);
			recMap = E_NewPolicyDao.getRecsForLog(ids);
		}

		system.assertEquals(2,recMap.size());

		Test.stopTest();

	}

	static testMethod void getNewPolicyForCountTest(){
		List<AggregateResult> recs = new List<AggregateResult>();
		Test.startTest();
		recs = E_NewPolicyDao.getNewPolicyForCount('');
		System.assertEquals(0, recs.size());
		Test.stopTest();
	}
	static testMethod void getNewPolicyForGroupCountTest(){
		List<AggregateResult> recs = new List<AggregateResult>();
		Test.startTest();
		recs = E_NewPolicyDao.getNewPolicyForGroupCount('');
		System.assertEquals(0, recs.size());
		Test.stopTest();
	}

/* Test Data **************************** */
	/** User作成 */
    private static void createUser(String profileName){
		String userName = 'test@terrasky.ing';
        Profile p = [Select Id From Profile Where Name = :profileName];

		// Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );

    	// User
    	if(profileName != PF_PARTNER){
    		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
    		user.UserRoleId = portalRole.Id;
    		insert user;

    	// Partner User
    	}else{
    		user.ContactId = atCon.Id;
    		insert user;

			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
    	}


    }

	/** データアクセス系作成 */
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);

            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }

    /** Data作成 */
	private static void createDataAccount(){
		system.runAs(thisuser){
			// Account 代理店格
			ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
			insert ahAcc;

			// Account 事務所
			ayAccList = new List<Account>();
			for(Integer i = 1; i < 4; i++){
				Account acc = new Account(
					Name = 'office' + i
					,ParentId = ahAcc.Id
					,E_CL2PF_ZAGCYNUM__c = 'ay001'
					,E_COMM_VALIDFLAG__c = '1'
				);
				ayAccList.add(acc);
			}
			insert ayAccList;

			// Contact 募集人
			if(!ayAccList.isEmpty()){
				atCon = new Contact(LastName = 'test',AccountId = ayAccList[0].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[0].E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ing';
				insert atCon;
			}
		}
	}
}