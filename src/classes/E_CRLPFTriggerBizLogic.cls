public with sharing class E_CRLPFTriggerBizLogic {

    //保険顧客情報の契約者を保険契約ヘッダに移送
    public void updatePolicyContractor(List<E_CRLPF__c> targetCRLPFList){
        
        //更新対象の保険契約ヘッダを取得
        Set<Id> policyIds = new Set<Id>();
        for(E_CRLPF__c crlpf : targetCRLPFList){
            policyIds.add(crlpf.E_Policy__c);
        }

        //対象の保険契約ヘッダのMapを取得
        Map<Id, E_Policy__c> targetPolicyMap = E_PolicyDao.getRecMapByIds(policyIds);
        
        //保険顧客情報でループして保険契約ヘッダを更新
        Map<Id, E_Policy__c> updateMap = new Map<Id, E_Policy__c>();
        for(E_CRLPF__c crlpf : targetCRLPFList){
            E_Policy__c targetPolicy = updateMap.get(crlpf.E_Policy__c);
            if(targetPolicy == null) targetPolicy = targetPolicyMap.get(crlpf.E_Policy__c);
            if(targetPolicy == null) continue;
            
            //顧客ロールがOWのとき、契約者を更新
            if(crlpf.CLRRROLE__c == E_Const.CLRRROLE_OW && targetPolicy.Contractor__c != crlpf.Contractor__c){
                targetPolicy.Contractor__c = crlpf.Contractor__c;
                //契約者区分がnullの場合は更新
                if(targetPolicy.COMM_ZCNTRDSC__c == null){
	                targetPolicy.COMM_ZCNTRDSC__c = E_Const.COMM_ZCNTRDSC_P;
                }
                updateMap.put(targetPolicy.Id, targetPolicy);
                
            } else if(crlpf.CLRRROLE__c == E_Const.CLRRROLE_LA && targetPolicy.Insured__c != crlpf.Contractor__c){
                //顧客ロールがLAのとき、被保険者を更新
                targetPolicy.Insured__c = crlpf.Contractor__c;
                updateMap.put(targetPolicy.Id, targetPolicy);
                
            } else if(crlpf.CLRRROLE__c == E_Const.CLRRROLE_BN && crlpf.BNYTYP__c == E_Const.BNYTYP_09 && targetPolicy.Annuitant__c != crlpf.Contractor__c){
                //顧客ロールがBNかつ受取人種別が09のとき、年金受取人を更新
                //年金受取人は各保険契約ヘッダ毎に1名であることが前提
                targetPolicy.Annuitant__c = crlpf.Contractor__c;
                updateMap.put(targetPolicy.Id, targetPolicy);
            }
        }
        
        //update
        if(!updateMap.isEmpty()){
            Map<Id, String> updateResults = E_GenericDao.updatePartial(updateMap.values());
            
            //保険契約ヘッダの更新結果にエラーがあった場合は、保険顧客レコードにAddError
            if(!updateResults.isEmpty()){
                for(E_CRLPF__c rec: targetCRLPFList){
                    if(updateResults.containsKey(rec.E_Policy__c)){
                        rec.addError(updateResults.get(rec.E_Policy__c));
                    }
                }
            }
        }
        
    }

}