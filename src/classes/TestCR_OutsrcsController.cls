@isTest
private with sharing class TestCR_OutsrcsController{
	private static testMethod void testPageMethods() {		CR_OutsrcsController extension = new CR_OutsrcsController(new ApexPages.StandardController(new CR_Agency__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testOutSrcList() {
		CR_OutsrcsController.OutSrcList OutSrcList = new CR_OutsrcsController.OutSrcList(new List<CR_Outsrc__c>(), new List<CR_OutsrcsController.OutSrcListItem>(), new List<CR_Outsrc__c>(), null);
		OutSrcList.create(new CR_Outsrc__c());
		System.assert(true);
	}
	
}