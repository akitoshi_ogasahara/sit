global with sharing class E_ADRPFExtender extends E_AbstractEditExtender{
  private static final String PAGE_TITLE = '住所/電話・FAX番号変更';

    // extension
    E_ADRPFConrtoller extension;

    // 仮パスワード
    public Boolean isTempPassWord {get; set;}

    // 入力パスワード
    public String inputPassWord {get; set;}
    public String newPassWord {get; set;}
    public String verPassWord {get; set;}

    // レコード
    private E_ADRPF__c record;

    // 取引先責任者
    private Contact contact;

    // ID管理
    private E_IDCPF__c idcpf;

    // エラー判定
    private Boolean isError = false;

    // 変更画面判定
    private Boolean isChange = false;

    // エラーメッセージ
    private String errMsgEm = getMSG().get('ACE|001');        // 全角チェック：漢字
    private String errMsgNoSendStringKJ = getMSG().get('ACE|001');  // 禁則文字チェック：漢字
    private String errMsgEnkana = getMSG().get('ACE|002');      // 半角カナチェック：カナ
    private String errMsgNoSendStringKN = getMSG().get('ACE|002');  // 禁則文字チェック：カナ
    private String errMsgEnNum = getMSG().get('ACE|003');      // 半角数値チェック
    private String errMsgPhoneFormat = getMSG().get('ACE|004');    // 電話番号形式
    private String errMsgPhoneRequired = getMSG().get('ACE|005');  // 電話必須チェック
    private String errMsgInputPassword = getMSG().get('ACE|006');  // 手続きパスワードチェック
    private String errMsgOverMaxLen = getMSG().get('ACE|007');    // 桁数チェック

    /* コンストラクタ */
    public E_ADRPFExtender(E_ADRPFConrtoller extension){
        super();
        this.extension = extension;
    this.pgTitle = PAGE_TITLE;
    }

    /* init */
    global override void init(){

        String str;
        String[] strArr;

        // レコードセット
        this.record = this.extension.record;

      // パラメータ取得
        String contactId = ApexPages.currentPage().getParameters().get('conid');

    // アクセスチェック
    pageRef = doAuth(E_Const.ID_KIND.CONTACT, contactId);
    if(pageRef == null){
      // ログインユーザに紐付くID管理情報取得
      this.idcpf = E_IDCPFDao.getRecsById(UserInfo.getUserId());

      // ID管理.手続パスワード変更日　で仮パスワードかの判定
      this.isTempPassWord = E_UtilPasswordChange.getIsTempPassWord(this.idcpf);

          // 取引先責任者情報取得
          if(String.isNotBlank(contactId)){
            this.contact = E_ContactDao.getRecById(contactId);

            // 顧客情報変更受付にセット
            this.record.E_CADPF__c = this.contact.E_CADPF__c;        // 住所管理
            this.record.CLTPCODE__c = this.contact.E_CADPF__r.Name;      // 郵便番号
            this.record.ZNKJADDR__c = this.contact.E_CADPF__r.ZADRSKJ__c;  // 住所漢字
            this.record.ZADRSKN__c = this.contact.E_CADPF__r.ZADRSKN__c;    // 住所カナ
            this.record.ZNKJADDR01__c = this.contact.E_CLTPF_ZNKJADDR01__c;  // ご住所の続き
            this.record.ZNKJADDR02__c = this.contact.E_CLTPF_ZNKJADDR02__c;  // マンション名・様方
            this.record.ZNCLTADR01__c = this.contact.E_CLTPF_ZNCLTADR01__c;  // カナ：ご住所の続き
            this.record.ZNCLTADR02__c = this.contact.E_CLTPF_ZNCLTADR02__c;  // カナ：マンション名・様方

            // 自宅電話番号
            str = this.contact.E_CLTPF_CLTPHONE01__c;
            if(String.isNotBlank(str)){
              strArr = splitHyphen(str);
              this.record.CLTPHONE011__c = strArr.size() > 0 ? strArr[0] : '';
              this.record.CLTPHONE012__c = strArr.size() > 1 ? strArr[1] : '';
              this.record.CLTPHONE013__c = strArr.size() > 2 ? strArr[2] : '';
            }

            // 日中連絡先
            str = this.contact.E_CLTPF_CLTPHONE02__c;
            if(String.isNotBlank(str)){
              strArr = splitHyphen(str);
              this.record.CLTPHONE021__c = strArr.size() > 0 ? strArr[0] : '';
              this.record.CLTPHONE022__c = strArr.size() > 1 ? strArr[1] : '';
              this.record.CLTPHONE023__c = strArr.size() > 2 ? strArr[2] : '';
            }

            // FAX番号
            str = this.contact.E_CLTPF_FAXNO__c;
            if(String.isNotBlank(str)){
              strArr = splitHyphen(str);
              this.record.FAXNO1__c = strArr.size() > 0 ? strArr[0] : '';
              this.record.FAXNO2__c = strArr.size() > 1 ? strArr[1] : '';
              this.record.FAXNO3__c = strArr.size() > 2 ? strArr[2] : '';
            }
          }
    }
    }

     /* PageAction */
    public Pagereference pageAction(){
      // 不正遷移チェック
      if(pageRef == null){
        pageRef = doCheckPageTransition(ApexPages.currentPage().getUrl(), Page.E_ADRPFChange, this.isChange);
      }
    return E_Util.toErrorPage(pageRef, null);
    }

  /* 個別チェック処理 */
  public override Boolean isValidate() {
    return getIsTrCodeAddress();
  }

  /* 【共通】『キャンセル or 登録情報画面』ボタン押下処理 */
  public override Pagereference doReturn(){
    pageMessages.clearMessages();
    Pagereference pref = Page.E_ContactInfo;
    pref.getParameters().put('id', this.contact.Id);
    return pref;
  }

    /* 【変更画面】『確認画面へ』ボタン押下処理 */
    public Pagereference doNextConfirm(){
      pageMessages.clearMessages();
      this.isError = false;

      // 半角－全角変換
      changeEmEn();

      // 電話のハイフン結合
      this.record.CLTPHONE01__c = joinHyphen(this.record.CLTPHONE011__c, this.record.CLTPHONE012__c, this.record.CLTPHONE013__c);
    this.record.CLTPHONE02__c = joinHyphen(this.record.CLTPHONE021__c, this.record.CLTPHONE022__c, this.record.CLTPHONE023__c);
      this.record.FAXNO__c = joinHyphen(this.record.FAXNO1__c, this.record.FAXNO2__c, this.record.FAXNO3__c);

      // 入力チェック
      validate();

      // ページ遷移
      if(!this.isError){
        this.isChange = true;
        return Page.E_ADRPFConfirm;
      }else{
        return null;
      }
    }

    /* 【確認画面】『送信』ボタン押下処理 */
    public Pagereference doSend(){
      pageMessages.clearMessages();
    Boolean hasError = false;

    //現在日時の文字列変換
    Datetime dt = datetime.now();

    try{
      // 手続サービス用パスワードが初期パスワード
      if(isTempPassWord){
        // 入力されたパスワードのチェック
        //hasError = E_UtilPasswordChange.validatePassword(this.idcpf, this.inputPassWord, this.newPassWord, this.verPassWord);
        List<String> errMsgList = E_UtilPasswordChange.validatePassword(this.idcpf, this.inputPassWord, this.newPassWord, this.verPassWord);
        if(errMsgList.size() > 0){
          // エラーメッセージセット
          for(String errMsg : errMsgList){
            pageMessages.addErrorMessage(errMsg);
          }
            
          hasError = true;
        }
        
        // 入力チェックでエラーがない場合、ID管理の手続サービス用パスワードを更新
        if(!hasError){
            // 手続サービス用パスワード更新
            E_UtilPasswordChange.updateProceduralPassword(this.idcpf, this.newPassword);
            /*
            this.idcpf.ZPASSWD02__c = E_EncryptUtil.encrypt(this.newPassword);
            this.idcpf.ZSTATUS02__c = E_Const.PW_STATUS_OK;
            this.idcpf.ZPASSWD02DATE__c = String.valueOf(dt.format('yyyyMMdd'));
            update this.idcpf;
            */

            this.isTempPassWord = false;
            
            // 顧客情報変更受付履歴情報登録
             E_ADRPF__c adrpf = new E_ADRPF__c();
          adrpf.ReceptionDatetime__c = String.valueOf(dt.format('yyyy年MM月dd日HH時mm分'));  // 受付日時
          adrpf.E_IDCPF__c = this.idcpf.Id;                  // ID管理
          adrpf.CLNTNUM__c = this.contact.E_CLTPF_CLNTNUM__c;          // 顧客番号
          adrpf.ContactName__c =this.contact.Name;              // お客様氏名
          adrpf.ContactNameKana__c = this.contact.E_CLTPF_ZCLKNAME__c;    // お客様フリガナ
          adrpf.Type__c = E_Const.MAIL_TYPE_2P;                // 手続き種別
          adrpf.EmailCharge__c = E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c;// 担当者メールアドレス
          adrpf.Kbn__c = E_Const.KBN_2PPW;                  // 区分
          adrpf.MailNotSend__c = true;
          insert adrpf;
        }

      // 手続サービス用パスワードが初期パスワード以外
      }else{
        // ID管理の手続きパスワードと入力されたパスワード(暗号化された)を比較
        if(!(this.idcpf.ZSTATUS02__c == E_Const.PW_STATUS_OK
            //&& this.idcpf.ZPASSWD02__c == E_EncryptUtil.encrypt(this.inputPassWord))){
            && this.idcpf.RegularProcedurePW__c == E_EncryptUtil.encrypt(this.inputPassWord))){
          hasError = true;
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, this.errMsgInputPassword));
          pageMessages.addErrorMessage(this.errMsgInputPassword);
        }
      }
      
      // 入力チェックでエラーがない場合、登録処理へ
      if(!hasError){
        // 顧客情報変更受付情報登録
        if(String.isNotBlank(this.record.CLTPCODE__c)){
          this.record.CLTPCODE__c = this.record.CLTPCODE__c.replace('-', '');
        }
        
        this.record.ReceptionDatetime__c = String.valueOf(dt.format('yyyy年MM月dd日 HH時mm分'));  // 受付日時
        this.record.ZEACDATE__c = String.valueOf(dt.format('yyyyMMdd'));  // データ受付日
        this.record.ZEACTIME__c = String.valueOf(dt.format('HHmmss'));    // データ受付時刻
        this.record.E_IDCPF__c = this.idcpf.Id;                // ID管理
        this.record.CLNTNUM__c = this.contact.E_CLTPF_CLNTNUM__c;      // 顧客番号
        this.record.ContactName__c = this.contact.Name;            // お客様氏名
        this.record.ContactNameKana__c = this.contact.E_CLTPF_ZCLKNAME__c;  // お客様フリガナ
        this.record.Type__c = E_Const.MAIL_TYPE_AD;              // 手続き種別
        this.record.EmailCharge__c =E_Email__c.getValues(E_Const.EMAIL_CHARGE_CHANGECONTACT).Email__c;  // 担当者メールアドレス
        this.record.Kbn__c = E_Const.KBN_ADD;                // 区分
        insert this.record;
        //return Page.E_ADRPFComplete;
        Pagereference pref = Page.E_AcceptanceCompletion;
        pref.getParameters().put('type', 'ACC|002');
        pref.getParameters().put('conid', this.contact.Id);
        return pref;

      // エラー
      }else{
        return null;
      }

    }catch(Exception e){
      //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
      pageMessages.addErrorMessage(e.getMessage());
      return null;
    }
    }

    /* 【確認画面】『修正』ボタン押下処理 */
    public Pagereference doRevision(){
      pageMessages.clearMessages();
      this.isChange = false;
      return Page.E_ADRPFChange;
    }

  /* 不正遷移チェック */
  private Pagereference doCheckPageTransition(String url, Pagereference firstPage, Boolean isPrevCheck){
    PageReference pageRef = null;

    // URLを小文字へ変換
    url = url.toLowerCase();

      // URLのページが「住所/電話・FAX番号変更」画面かを判定
      if(url.indexOf(firstPage.getUrl()) == -1){
        // 変更画面チェックがTrueでない場合Falseをセット
        if(!isPrevCheck){
          pageRef = Page.E_ErrorPage;
          pageRef.getParameters().put('code', E_Const.ERROR_MSG_PARAMETER);
        }
      }

      return pageRef;
  }

    /* ハイフン分割 */
    private String[] splitHyphen(String str){
      return str.split('-', 5);
    }

    /* ハイフン結合 */
    private String joinHyphen(String str1, String str2, String str3){
      str1 = E_Util.null2Blank(str1);
      str2 = E_Util.null2Blank(str2);
      str3 = E_Util.null2Blank(str3);

      String joinStr = str1 + '-' + str2 + '-' + str3;
      if(String.isBlank(joinStr.replaceAll('-', ''))){
        joinStr = '';
      }

      return joinStr;
    }

    /* 半角－全角変換 */
    private void changeEmEn(){

      // カナ：ご住所の続き
      this.record.ZNCLTADR01__c = E_Util.emToEn(this.record.ZNCLTADR01__c);
      // カナ：マンション名・様方
      this.record.ZNCLTADR02__c = E_Util.emToEn(this.record.ZNCLTADR02__c);

      // 自宅電話番号
      this.record.CLTPHONE011__c = E_Util.emToEn(this.record.CLTPHONE011__c);
      this.record.CLTPHONE012__c = E_Util.emToEn(this.record.CLTPHONE012__c);
    this.record.CLTPHONE013__c = E_Util.emToEn(this.record.CLTPHONE013__c);

      // 日中連絡先
      this.record.CLTPHONE021__c = E_Util.emToEn(this.record.CLTPHONE021__c);
      this.record.CLTPHONE022__c = E_Util.emToEn(this.record.CLTPHONE022__c);
    this.record.CLTPHONE023__c = E_Util.emToEn(this.record.CLTPHONE023__c);

      // FAX番号
      this.record.FAXNO1__c = E_Util.emToEn(this.record.FAXNO1__c);
      this.record.FAXNO2__c = E_Util.emToEn(this.record.FAXNO2__c);
    this.record.FAXNO3__c = E_Util.emToEn(this.record.FAXNO3__c);

    }

    /* 入力チェック */
    private void validate(){
      // ※長さはSF標準でOK？
      String label;

      // 【ご住所の続き：ZNKJADDR01__c】
      label = E_ADRPF__c.ZNKJADDR01__c.getDescribe().getLabel();
      validateZNKJADDR(this.record.ZNKJADDR01__c, label);

      // 【マンション名・様方：ZNKJADDR02__c】
      label = E_ADRPF__c.ZNKJADDR02__c.getDescribe().getLabel();
      validateZNKJADDR(this.record.ZNKJADDR02__c, label);

      // 【カナ：ご住所の続き：ZNCLTADR01__c】
      label = E_ADRPF__c.ZNCLTADR01__c.getDescribe().getLabel();
      validateZNCLTADR(this.record.ZNCLTADR01__c, label);

      // 【カナ：マンション名・様方：ZNCLTADR02__c】
      label = E_ADRPF__c.ZNCLTADR02__c.getDescribe().getLabel();
      validateZNCLTADR(this.record.ZNCLTADR02__c, label);

    // 必須チェック：自宅電話番号と日中連絡先のどちらも入力されていない場合、エラー
    //if(String.isNotBlank(this.record.CLTPHONE01__c) || String.isNotBlank(this.record.CLTPHONE02__c)){
    if(( String.isNotBlank(this.record.CLTPHONE011__c)|| String.isNotBlank(this.record.CLTPHONE012__c)||String.isNotBlank(this.record.CLTPHONE013__c) )|| 
        (String.isNotBlank(this.record.CLTPHONE021__c)||String.isNotBlank(this.record.CLTPHONE022__c)|| String.isNotBlank(this.record.CLTPHONE023__c))
        ){
  
        // 【自宅電話番号：CLTPHONE01__c】
        label = E_ADRPF__c.CLTPHONE01__c.getDescribe().getLabel();
        validatePhone(this.record.CLTPHONE01__c, this.record.CLTPHONE011__c, this.record.CLTPHONE012__c,this.record.CLTPHONE013__c ,  label);

        // 【日中連絡先：CLTPHONE02__c】
        label = E_ADRPF__c.CLTPHONE02__c.getDescribe().getLabel();
        validatePhone(this.record.CLTPHONE02__c, this.record.CLTPHONE021__c, this.record.CLTPHONE022__c, this.record.CLTPHONE023__c, label);

    }else{

      // 必須エラー
      procError(this.errMsgPhoneRequired);
    }

      // 【FAX番号：FAXNO__c】
      label = E_ADRPF__c.FAXNO__c.getDescribe().getLabel();
      validatePhone(this.record.FAXNO__c,this.record.FAXNO1__c , this.record.FAXNO2__c, this.record.FAXNO3__c, label);

    }

    /* 入力チェック：住所（漢字） */
    private void validateZNKJADDR(String str, String label){
      if(String.isNotBlank(str)){
        String msg;
        
        // 禁則文字チェック
        if(E_Util.isNoSendString(str)){
          msg = label + ':' + this.errMsgNoSendStringKJ;
        // 全角チェック
        }else if(!E_Util.isEm(str)){
          msg = label + ':' + this.errMsgEm;
        // 桁数チェック
        }else if(str.length() > 30){
          msg = label + ':' + this.errMsgOverMaxLen;
        }
        
        // メッセージセット
        if(String.isNotBlank(msg)){
          procError(msg);
        }
      }
    }

    /* 入力チェック：住所（カナ） */
    private void validateZNCLTADR(String str, String label){
      if(String.isNotBlank(str)){
        String msg;
        
        // 禁則文字チェック
        if(E_Util.isNoSendString(str)){
          msg = label + ':' + this.errMsgNoSendStringKN;
        // 半角カナチェック
        }else if(!E_Util.isEn(str)){
          msg = label + ':' + this.errMsgEnkana;
        // 半角小文字チェック
        } else if(!E_Util.isNotEnLowerAlpha(str)){
          msg = label + ':' + this.errMsgEnkana;
        // 桁数チェック
        }else if(str.length() > 30){
          msg = label + ':' + this.errMsgOverMaxLen;
        }
        // メッセージセット
        if(String.isNotBlank(msg)){
          procError(msg);
        }
      }
    }

    /* 入力チェック：電話 */
    private void validatePhone(String str,String str01,String str02,String str03, String label){
  		String msg = null;
		if (String.isEmpty(str01) && String.isEmpty(str02) && String.isEmpty(str03)) {
  			return;
  		}
      //if(label != 'FAX番号' || (label == 'FAX番号' && (String.isNotBlank(str01) || String.isNotBlank(str02) || String.isNotBlank(str03)))){
        //if((label == '日中連絡先' && !this.isError && (String.isNotBlank(str01) || String.isNotBlank(str02) || String.isNotBlank(str03))) || label != '日中連絡先'){
          //ブランクチェック
          if (String.isNotBlank(str01) && !E_Util.isEnNum(str01)) {
      			msg = label + ':' + this.errMsgEnNum;
      		} else if (String.isNotBlank(str02) && !E_Util.isEnNum(str02)) {
      			msg = label + ':' + this.errMsgEnNum;
      		} else if (String.isNotBlank(str03) && !E_Util.isEnNum(str03)) {
      			msg = label + ':' + this.errMsgEnNum;
      		}
          //フォーマットチェック
      		if (msg == null) {
      			if(!E_Util.isPhoneFormat(str)){
      				msg = label + ':' + this.errMsgPhoneFormat;
      			}
      		}
        //} 
     // }
        /*
        // 半角数値チェック
        if(!E_Util.isEnNum(str.replaceAll('-', '')) ){
          msg = label + ':' + this.errMsgEnNum;


        // 電話番号形式チェック
        }else if(!E_Util.isPhoneFormat(str)){
          msg = label + ':' + this.errMsgPhoneFormat;
        // 半角数値チェック
        }else if( !E_Util.isEnNum(str01) || !E_Util.isEnNum(str02)|| !E_Util.isEnNum(str03)){

          msg = label + ':' + this.errMsgEnNum;
        }
        */
        /*
		if(String.isNotBlank(str.replaceAll('-', ''))){
			String msg;
			
			// 半角数値チェック
			if(!E_Util.isEnNum(str.replaceAll('-', ''))){
				msg = label + ':' + this.errMsgEnNum;
			// 電話番号形式チェック
        	}else if(!E_Util.isPhoneFormat(str)){
				msg = label + ':' + this.errMsgPhoneFormat;
			}
			// メッセージセット
			if(String.isNotBlank(msg)){
				procError(msg);
			}
		}
		*/
        // メッセージセット
        if(String.isNotBlank(msg)){
          procError(msg);
        }
    }

    /* エラー処理 */
    private void procError(String msg){
      //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
      pageMessages.addErrorMessage(msg);
      this.isError = true;
    }
}