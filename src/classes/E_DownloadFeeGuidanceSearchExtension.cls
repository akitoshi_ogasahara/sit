public with sharing class E_DownloadFeeGuidanceSearchExtension {
    
    E_DownloadController extention;
    
    //選択可能年月の最大値・最小値
    public string startDate {get;set;}
    public string endDate {get;set;}
    
    //選択された年月
    public String selectedInquiryMonth {get;set;}
    
    //データが取得できる最古の年月
    public String getStartYearMonth(){
        return getYearMonth(startDate);
    }
    //データが取得できる最新の年月
    public String getEndYearMonth(){
        return getYearMonth(endDate);
    }
    private String getYearMonth(String yearMonth){
        String year = yearMonth.split('/').get(0);
        String month = yearMonth.split('/').get(1);
        return year + '年' + month + '月';
    }
    
    public String getTitle(){
        return '手数料のご案内出力年月指定';
    }
    
    // 検索条件表示フラグ
    public Boolean isDispSearchArea {get; private set;}
    
    //コンストラクタ
    public E_DownloadFeeGuidanceSearchExtension(E_DownloadController extention) {
//system.debug('constructior start.');
        this.extention = extention;
        extention.setPgTitle(getTitle());
        //実行者チェック
        extention.isValidate = extention.getActor() && checkCanDisplay();
        extention.pageRef = extention.init();
//system.debug('pageref:'+extention.pageRef);
        
        if(extention.pageRef == null){
            startDate = E_Util.formatDataSyncDate('yyyy/MM/01');
            endDate = E_Util.formatDataSyncDate();
            
            //データが取得できる最古の年から最新の年
            AggregateResult ar = E_ASPPFDaoWithout.getStartEndDate(extention.distributor.Id);
            if(ar.get('mindate') != null){
                startDate = (string)ar.get('mindate') + '/01';
                endDate = (string)ar.get('maxdate') + '/01';
                isDispSearchArea = true;
            //データが取得できない場合
            }else{
                extention.pageMessages.addErrorMessage(extention.getMSG().get(E_Const.ERROR_MSG_NODATA));
                isDispSearchArea = false;
                //DF-001082により自画面にエラーメッセージを表示するよう修正
//              extention.pageRef = Page.E_ErrorPage;
//              extention.pageRef.getParameters().put('code', E_Const.ERROR_MSG_NODATA);
            }
        }
system.debug('constructior end.');
    }
    
    //初期チェック
    private Boolean checkCanDisplay(){
        if(extention.distributor == null || !extention.getCanDisplayFeeGuidance()){
            extention.errorMsgCode = E_Const.ERROR_MSG_FUNCTION;
            return false;
        }
        
        return true;
    }
    
    /**
     *  対象明細の最古と最新日の取得
     */
/*
    private AggregateResult getStartEndDate(Id accId){
        E_SoqlManager manager = new E_SoqlManager();
        String soql = 'SELECT Max(ACCTYEARMONTH__c) maxdate, MIN(ACCTYEARMONTH__c) mindate FROM E_ASPPF__c ';
        manager.addAndWhere('Account__c', accId, manager.eq);
        soql += manager.getWhere();
        return Database.query(soql);
    }
*/
    
    //ページアクション
    public PageReference pageAction(){
        return E_Util.toErrorPage(extention.pageRef, null);
    }
        
    //実行ボタン
    public PageReference doInquiry(){
        
        PageReference ret = null;
        extention.pageMessages.clearMessages();
        
        try {
            
            //入力値検証
            if(!extention.validateDate(selectedInquiryMonth, '照会年月', true, null)){
                return null;
            }
            
            //レコード取得
            String year = selectedInquiryMonth.split('/')[0];
            String month = selectedInquiryMonth.split('/')[1];
            List<E_ASPPF__c> asppfList = E_ASPPFDaoWithout.getAsspf(extention.distributor.Id, Integer.valueOf(year), Integer.valueOf(month));
            
            //取得レコード数検証
            if(asppfList == null || asppfList.isEmpty()){
                extention.pageMessages.addErrorMessage('手数料が発生していないか、もしくはデータ準備中です。');
                return null;
            } else if(asppfList.size() > 1){
                extention.pageMessages.addErrorMessage('該当する情報が複数あります。');
                return null;
            }
            
            //画面遷移
            ret = Page.E_DownloadFeeGuidanceResult;
            ret.getParameters().put('spfid', asppfList[0].Id);
            
        } catch(Exception e) {
            extention.pageMessages.addErrorMessage(e.getMessage());
            ret = null;
        }
        
        return ret;
    }
    
/*
    //SoqlManagerを使用して代理店手数料のご案内用ファイルを取得
    private List<E_ASPPF__c> getAsspf(Id accountId, Integer year, Integer month){
        List<E_ASPPF__c> ret = null;
        
        String soql = 'SELECT Id,';
        soql += ' ACCTYR__c, ACCTMN__c ';
        soql += ' FROM E_ASPPF__c ';
        
        E_SoqlManager manager = new E_SoqlManager();
        manager.addString(manager.pStart);
        //accountId
        manager.addAndWhere('Account__c', accountId, manager.eq);
        //年
        manager.addAndWhere('ACCTYR__c', year, manager.eq);
        //月
        manager.addAndWhere('ACCTMN__c', month, manager.eq);
        manager.addString(manager.pEnd);
        soql += manager.getWhere();
        
System.debug(soql);
        ret = Database.query(soql);
        return ret;
    }
*/
}