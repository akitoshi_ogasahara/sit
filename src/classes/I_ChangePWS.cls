global class I_ChangePWS {
	public I_ChangePWS() {

	}

	/**
	 * memo
	 *
	 * NN_IRIS-739
	 *  SF画面上よりパスワードステータスを「5」に変更するフローを可能とする
	 *
	 *  EIDCデータの更新がされ、EIDRレコードが作成されること
	 *  ただし、未連携のEIDR＜リクエスト種別：緊急停止(暫定)＞がある場合には作成を行わない
	 *  ※未連携の判定
	 *  変更受付日時（dataSyncDate__c）が空であること
	 */
	webservice static String invalidatingUser(String updId) {
		String msg = '';
		Savepoint sp = Database.setSavepoint();

		try{
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;

			E_IDCPF__c idcpf = E_IDCPFDao.getEIDRById(updId);

			idcpf.ZSTATUS01__c = '5';
			update idcpf;

			for(E_IDRequest__c r : idcpf.IR__r){
				if(r.dataSyncDate__c == null && r.ZUPDFLAG__c == E_Const.ZUPDFLAG_ES){
					msg = '未連携のEIDRがあります';
					return msg;
				}
			}

			E_IDRequest__c idr = new E_IDRequest__c(
				 ZEMAILAD__c	= idcpf.ZEMAILAD__c
				,E_IDCPF__c		= idcpf.Id
				,ZIDTYPE__c		= idcpf.ZIDTYPE__c
				,ZWEBID__c		= idcpf.ZWEBID__c
				,ZUPDFLAG__c	= E_Const.ZUPDFLAG_ES	//暫定
				,ZIDOWNER__c	= idcpf.ZIDOWNER__c
				,ZINQUIRR__c	= idcpf.ZINQUIRR__c
			);
			insert idr;

			msg = '更新しました';
			return msg;

		}catch(Exception e){
			Database.rollback(sp);
			msg = '更新に失敗しました';
			return msg;
		}
	}
}