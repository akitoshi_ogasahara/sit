public with sharing class I_FileDownloadController {

	//ダウンロードしたい添付ファイルが紐づいたIRIS_CMSコンテンツの外部IDとクリックアクション(attributeで指定)
	public String cmsUpsertKey{get; set;}
	public String fileId{get; private set;}
	public String clickAction{
		get{
			String clickAction = null;
			I_ContentMaster__c cms = getCMS();
			//指定されたUpsertKeyに紐づくIRIS_CMSコンテンツが見つかること
			if(cms != null){
				clickAction = cms.ClickAction__c;
				//現状はAttachmentとファイル情報のファイルのみがDL対象として実装
				Set<String> clickActions = new Set<String>{
					 I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT
					,I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK
					,I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE
					,I_Const.CMSCONTENTS_CLICKACTION_URL
				};
				System.assert(clickActions.contains(clickAction), clickAction + 'は対応していません。');

				if(clickAction == I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT && !cms.Attachments.isEmpty() ){
				//クリックアクションがDL(Attachment)の場合、DLファイル格納先とファイル名が一致する最新のAttachment
					for(Attachment att : cms.Attachments){
						if(att.Name == cms.FilePath__c){
							this.fileId = att.Id;
							break;
						}
					}
				}else if(clickAction == I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK){
				//クリックアクションがDL(NNLink)の場合、DLファイル格納先の外部IDを持つファイル情報の最新のAttachment
					E_CMSFile__c cmsfile = E_AttachmentDao.getRecByUpsertKey(cms.FilePath__c);
					if(cmsfile != null && !cmsfile.Attachments.isEmpty()){
						this.fileId = cmsfile.Attachments[0].Id;
					}
				}else if(clickAction == I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE){
				//クリックアクションがDL(Chatterfile)だった場合
					// ChatterFileのIdを取得
					if(chatterFileIds.containsKey(cms.Id)){
						Id chatterFileId = chatterFileIds.get(cms.Id);
						this.fileId = String.format(E_CONST.CONTENT_DOWNLOAD_URL, new List<String>{String.valueOf(chatterFileId)});
					}
				}else if(clickAction == I_Const.CMSCONTENTS_CLICKACTION_URL){
					this.fileId = cms.LinkURL__c;
	/*
				}else if(cms.ClickAction__c == I_Const.CMSCONTENTS_CLICKACTION_DL_CONTENTS){
				//クリックアクションがDL(Contents)だった場合
	*/
				}
			}
			return clickAction;
		}set;
	}

	//ページ内で2件以上読み込まれる場合、CMSをまとめて取得する
	public List<String> cmsUpsertKeys{get; set;}
	public static MAP<String, I_ContentMaster__c> cmsMap;
	public static Map<Id,Id> chatterFileIds;
	public I_ContentMaster__c getCMS(){
		//1件の場合
		if(cmsUpsertKeys == null){
			I_ContentMaster__c cms = I_ContentMasterDao.getRecordByUpsertKey(cmsUpsertKey);
			if(cms == null) return null;

			if(cms.ClickAction__c == I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE){
				chatterFileIds = I_ContentMasterDao.getChatterFileIdsByRecId(new Set<Id>{cms.Id});
			}
			return cms;
		}

		//2件以上の場合
		if(cmsMap == null){
			cmsMap = I_ContentMasterDao.getCMSMapByUpsertKey(cmsUpsertKeys);

			Set<Id> cmsIds = new Set<Id>();
			for(I_ContentMaster__c cms : cmsMap.values()){
				if(cms.ClickAction__c == I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE){
					cmsIds.add(cms.Id);
				}
			}
			chatterFileIds = I_ContentMasterDao.getChatterFileIdsByRecId(cmsIds);
		}
		if(cmsMap != null) return cmsMap.get(cmsUpsertKey);

		return null;
	}

}