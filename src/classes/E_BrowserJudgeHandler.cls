public with sharing class E_BrowserJudgeHandler {
	public static final String PARAM_SKIP_CONFIRM = 'confirm';

	private PageReference nextPage;
	@TestVisible
	public Boolean isAlreadyConfirmed{get;set;}
	
	//Constructor
	public E_BrowserJudgeHandler(PageReference pr){
		this.nextPage = pr;

		//すでに24時間以内に了承済の場合True
		String val = Apexpages.currentPage().getParameters().get(PARAM_SKIP_CONFIRM);
		Long dt = 0;
		try{
			if(String.isNotBlank(val)){
				dt = Long.valueOf(val);
			}
		}catch(Exception e){
			//変換エラーの場合は0として扱う	
		}
		
		Long nowDt = System.now().getTime();
		
		//24時間(ミリ秒)以内に確認済の場合True
		this.isAlreadyConfirmed = (nowDt-dt) < 24*60*60*1000;

	}

	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}

	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}

	//サポート対象外ブラウザ
	public Boolean getIsUnsupportedBrowser(){
		return E_Util.isUnsupportedBrowser() && !this.isAlreadyConfirmed;
	}
	
	//互換モード
	public Boolean getIsCompatibilityMode(){
		return E_Util.isCompatibilityMode() && !this.isAlreadyConfirmed;
	}
	
	//ページの表示可能
	public Boolean getShowAlert(){
		return getIsUnsupportedBrowser() || getIsCompatibilityMode();
	}
	
	//了承してログインページへ
	public PageReference doSkipConfirm(){
		this.nextPage.getParameters().put(PARAM_SKIP_CONFIRM, String.valueOf(System.now().getTime()));
		this.nextPage.setRedirect(true);
		return nextPage;
	}
	
	//次ページへ遷移
	public PageReference toNextPage(){
		this.nextPage.setRedirect(true);
		return nextPage;
	}

	public string getDebug(){
		return E_Util.getUserAgent();
	}
}