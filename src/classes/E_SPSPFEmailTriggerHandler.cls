public with sharing class E_SPSPFEmailTriggerHandler {

	private E_SPSPFEmailTrrigerBizLogic bizLogic;
	
	//コンストラクタ
	public E_SPSPFEmailTriggerHandler(){
		bizLogic = new E_SPSPFEmailTrrigerBizLogic();
	}
	
	//after insert
	public void onAfterInsert(List<E_SPSPFEmail__c> newList){
		bizLogic.setMailNunmer(newList);
	}

	//after update
	public void onAfterUpdaet(List<E_SPSPFEmail__c> newList){
		bizLogic.setMailNunmer(newList);
	}


}