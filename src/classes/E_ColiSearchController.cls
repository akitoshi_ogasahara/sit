global with sharing class E_ColiSearchController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_COVPF__c record{get;set;}	
			
	public E_ColiSearchExtender getExtender() {return (E_ColiSearchExtender)extender;}
	
		public dataTable dataTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component34_val {get;set;}	
		public SkyEditor2.TextHolder Component34_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c NameKana_val {get;set;}	
		public SkyEditor2.TextHolder NameKana_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component51_val {get;set;}	
		public SkyEditor2.TextHolder Component51_op{get;set;}	
			
	public String recordTypeRecordsJSON_E_COVPF_c {get; private set;}
	public String defaultRecordTypeId_E_COVPF_c {get; private set;}
	public String metadataJSON_E_COVPF_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public E_ColiSearchController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_COVPF__c.fields.InsuredE_CLTPF_ZCLKNAME__c;
		f = E_COVPF__c.fields.InsuredName__c;
		f = E_COVPF__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.COMM_OCCDATE_yyyyMMdd__c;
		f = E_COVPF__c.fields.COLI_ZPSTDESC__c;
		f = E_COVPF__c.fields.COLI_ZCOVRNAMES__c;
		f = E_COVPF__c.fields.COLI_SUMINSF__c;
		f = E_Policy__c.fields.COMM_SINSTAMT__c;
		f = E_COVPF__c.fields.COLI_PSTATCODE__c;
		f = E_COVPF__c.fields.COLI_ZCRIND__c;
		f = E_COVPF__c.fields.RecordTypeId;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_COVPF__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				NameKana_val = new SkyEditor2__SkyEditorDummy__c();	
				NameKana_op = new SkyEditor2.TextHolder('co');	
					
				Component51_val = new SkyEditor2__SkyEditorDummy__c();	
				Component51_op = new SkyEditor2.TextHolder('co');	
					
				Component34_val = new SkyEditor2__SkyEditorDummy__c();	
				Component34_op = new SkyEditor2.TextHolder('eq');	
					
				queryMap.put(	
					'dataTable',	
					new SkyEditor2.Query('E_COVPF__c')
						.addFieldAsOutput('COMM_CHDRNUM__c')
						.addFieldAsOutput('E_Policy__r.COMM_OCCDATE_yyyyMMdd__c')
						.addFieldAsOutput('COLI_ZPSTDESC__c')
						.addFieldAsOutput('InsuredName__c')
						.addFieldAsOutput('COLI_ZCOVRNAMES__c')
						.addFieldAsOutput('COLI_SUMINSF__c')
						.addFieldAsOutput('E_Policy__r.COMM_SINSTAMT__c')
						.addFieldAsOutput('E_Policy__r.COMM_CHDRNUM_LINK__c')
						.addFieldAsOutput('E_Policy__r.COMM_CHDRNUM_LINK_I__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component34_val, 'SkyEditor2__Text__c', 'E_Policy__r.COMM_CHDRNUM__c',E_Policy__c.fields.COMM_CHDRNUM__c, Component34_op, true, false,true,0,true,'E_Policy__c',E_COVPF__c.fields.E_Policy__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(NameKana_val, 'SkyEditor2__Text__c', 'InsuredE_CLTPF_ZCLKNAME__c', NameKana_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component51_val, 'SkyEditor2__Text__c', 'InsuredName__c', Component51_op, true, 0, true ))
						.addWhere(' ( COLI_PSTATCODE__c != \'A\' AND COLI_PSTATCODE__c != \'J\' AND COLI_ZCRIND__c = \'C\' AND COLI_PSTATCODE__c != \'K\' AND RecordType.DeveloperName = \'ECOVPF\')')
				);	
					
					dataTable = new dataTable(new List<E_COVPF__c>(), new List<dataTableItem>(), new List<E_COVPF__c>(), null);
				 dataTable.setPageItems(new List<dataTableItem>());
				 dataTable.setPageSize(100);
				listItemHolders.put('dataTable', dataTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_COVPF__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_ColiSearchExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			dataTable.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_E_Policy_c_COMM_CHDRNUM_c() { 
			return getOperatorOptions('E_Policy__c', 'COMM_CHDRNUM__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_COVPF_c_InsuredE_CLTPF_ZCLKNAME_c() { 
			return getOperatorOptions('E_COVPF__c', 'InsuredE_CLTPF_ZCLKNAME__c');	
		}	
		public List<SelectOption> getOperatorOptions_E_COVPF_c_InsuredName_c() { 
			return getOperatorOptions('E_COVPF__c', 'InsuredName__c');	
		}	
			
			
	global with sharing class dataTableItem extends SkyEditor2.ListItem {
		public E_COVPF__c record{get; private set;}
		@TestVisible
		dataTableItem(dataTable holder, E_COVPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTable extends SkyEditor2.PagingList {
		public List<dataTableItem> items{get; private set;}
		@TestVisible
			dataTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dataTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableItem(this, (E_COVPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<dataTableItem> getViewItems() {            return (List<dataTableItem>) getPageItems();        }
	}

			
	}