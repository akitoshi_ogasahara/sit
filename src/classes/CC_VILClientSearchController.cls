/**
 * クラス名 : CC_VILClientSearchController
 * 概要 : Used in Interaction Launcher to Search Agent(Contact) and Agency(Account)
 *
 */
global with sharing class CC_VILClientSearchController implements vlocity_ins.VlocityOpenInterface2{

	/**
	 * 概要 : This method is mandatory as we implements vlocity Open interface, this is the method by default called from Interaction launcher events
	 *
	 */
	global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);
		System.debug('inputs: ' + inputs);
		System.debug('outputs: ' + output);
		System.debug('options: ' + options);

		Boolean success = true;
		try{
			// Check the method if name is getSearchRequest then call getSearchRequest methods
			if(methodName.equalsIgnoreCase('getSearchRequest')){
				success = getSearchRequest(inputs, output, options);

			// Check the method if name is getSearchResults then call getSearchResults methods
			}else if(methodName.equalsIgnoreCase('getSearchResults')){
				success = getSearchResults(inputs, output, options);

			// launchConsole
			}else if(methodName.equalsIgnoreCase('launchConsole')){
				success = launchConsole(inputs, output, options);
			}

		}catch(Exception e){
			System.debug('Error invoke method: ' + methodName + ' with error: '+ e);
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}

	private static Boolean launchConsole(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;

		String interactionObj = (String) inputMap.get('interactionObjName');
		String interactionId = (String) inputMap.get('recordId');
		String interactionName = (String) inputMap.get('name');

		if( String.isNotBlank(interactionId)){
			outputMap.put('parent'+interactionObj+'Id', interactionId);
			outputMap.put(interactionObj+'Id', interactionId);
		}
		if( String.isNotBlank(interactionName)) {
			outputMap.put(interactionObj+'Name', interactionName);
		}

		return success;
	}

	/**
	 * 概要 : This method generates the SearchRequest for LaunchInteraction
	 */
	private Boolean getSearchRequest(Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options){

		vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest) inputs.get('lookupRequest');

		List<Map<String, Object>> previousSearchResults = request.previousSearchResults;
		Map<String, Object> additionalData = request.additionalData;

		//検索項目設定
		List<String> searchFieldList = request.searchFieldList;
		searchFieldList.add('policyNumber');	//証券番号
		searchFieldList.add('clientNumber');	//顧客番号
		searchFieldList.add('clientKanaName');	//顧客名(カナ)
		searchFieldList.add('phone');	//電話番号
		searchFieldList.add('birthdate');	//生年月日

		//検索項目表示ラベル設定
		Map<String, Map<String, String>>labelTypeMap = request.searchFieldsLabelTypeMap;
		Map<String, String> newType = new Map<String, String> ();

		newType.put('label','証券番号');
		newType.put('datatype','text');
		labelTypeMap.put('policyNumber',newType);

		newType = new Map<String, String> ();
		newType.put('label','顧客番号');
		newType.put('datatype','text');
		labelTypeMap.put('clientNumber',newType);

		newType = new Map<String, String> ();
		newType.put('label','顧客名(カナ)');
		newType.put('datatype','text');
		labelTypeMap.put('clientKanaName',newType);

		newType = new Map<String, String> ();
		newType.put('label','電話番号');
		newType.put('datatype','text');
		labelTypeMap.put('phone',newType);

		newType = new Map<String, String> ();
		newType.put('label','生年月日');
		newType.put('datatype','text');
		labelTypeMap.put('birthdate',newType);

		System.debug('request: '+request);
		output.put('lookupRequest', request);

		return true;
	}

	/**
	 * 概要 : Method performs search and evaluates search result, as well as verification result set
	 */
	private Boolean getSearchResults(Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options){

		vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest) inputs.get('lookupRequest');
		Map<String, Object> searchValueMap = request.searchValueMap;

		// 検索結果項目設定 Begin. //
		Map<String, Map<String, String>> resultFieldsLabelTypeMap = new Map<String, Map<String, String>>();
		List<String> resultFieldList = new List<String> ();
		List<String> resultFieldList1 = new List<String> ();
		List<String> resultFieldList2 = new List<String> ();

		// 証券番号の場合
		if(searchValueMap.get('policyNumber') != NULL){
			// 権利者
			resultFieldList1.add('権利者タイトル');
			resultFieldList1.add('権利者情報');

			Map<String, String> rf1Type1 = new Map<String, String> ();
			rf1Type1.put('label', '権利者');
			rf1Type1.put('datatype','text');
			resultFieldsLabelTypeMap.put('権利者タイトル', rf1Type1);

			Map<String, String> rf1Type2 = new Map<String, String> ();
			rf1Type2.put('label', '');
			rf1Type2.put('datatype','text');
			resultFieldsLabelTypeMap.put('権利者情報', rf1Type2);

			// 被保険者
			resultFieldList2.add('被保険者タイトル');
			resultFieldList2.add('被保険者情報');

			Map<String, String> rf2Type1 = new Map<String, String> ();
			rf2Type1.put('label', '被保険者');
			rf2Type1.put('datatype','text');
			resultFieldsLabelTypeMap.put('被保険者タイトル', rf2Type1);

			Map<String, String> rf2Type2 = new Map<String, String> ();
			rf2Type2.put('label', '');
			rf2Type2.put('datatype','text');
			resultFieldsLabelTypeMap.put('被保険者情報', rf2Type2);
		}
		// 顧客番号の場合
		else if(searchValueMap.get('clientNumber') != NULL){
			resultFieldList.add('顧客名');
			resultFieldList.add('顧客名(カナ)');
			resultFieldList.add('住所');
			resultFieldList.add('電話番号');

			Map<String, String> rfType = new Map<String, String> ();

			rfType.put('label', '顧客名');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('顧客名', rfType);

			rfType = new Map<String, String> ();
			rfType.put('label', '顧客名(カナ)');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('顧客名(カナ)', rfType);

			rfType = new Map<String, String> ();
			rfType.put('label', '住所');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('住所', rfType);

			rfType = new Map<String, String> ();
			rfType.put('label', '電話番号');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('電話番号', rfType);
		}
		//それ以外の顧客情報の場合
		else if(searchValueMap.get('clientKanaName') != NULL || searchValueMap.get('phone') != NULL || searchValueMap.get('birthdate') != NULL){
			resultFieldList.add('顧客名');
			resultFieldList.add('顧客名(カナ)');
			resultFieldList.add('住所');
			resultFieldList.add('電話番号');
			resultFieldList.add('生年月日');

			Map<String, String> rfType = new Map<String, String> ();

			rfType.put('label', '顧客名');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('顧客名', rfType);

			rfType = new Map<String, String> ();
			rfType = new Map<String, String> ();
			rfType.put('label', '顧客名(カナ)');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('顧客名(カナ)', rfType);

			rfType = new Map<String, String> ();
			rfType.put('label', '住所');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('住所', rfType);

			rfType = new Map<String, String> ();
			rfType.put('label', '電話番号');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('電話番号', rfType);

			rfType = new Map<String, String> ();
			rfType.put('label', '生年月日');
			rfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('生年月日', rfType);
		}
		// 検索結果項目設定 End. //

		// 確認項目設定 Begin. //
		List<String> verificationFieldList = new List<String> ();
		List<String> verificationFieldList1 = new List<String> ();
		List<String> verificationFieldList2 = new List<String> ();

		// 証券番号の場合
		if(searchValueMap.get('policyNumber') != NULL){
			// 権利者
			verificationFieldList1.add('権利者名');
			verificationFieldList1.add('権利者電話番号');

			Map<String, String> vf1Type1 = new Map<String, String> ();
			vf1Type1.put('label', '権利者名');
			vf1Type1.put('datatype','text');
			resultFieldsLabelTypeMap.put('権利者名', vf1Type1);

			Map<String, String> vf1Type2 = new Map<String, String> ();
			vf1Type2.put('label', '権利者電話番号');
			vf1Type2.put('datatype','text');
			resultFieldsLabelTypeMap.put('権利者電話番号', vf1Type2);

			// 被保険者
			verificationFieldList2.add('被保険者名');
			verificationFieldList2.add('被保険者電話番号');

			Map<String, String> vf2Type1 = new Map<String, String> ();
			vf2Type1.put('label', '被保険者名');
			vf2Type1.put('datatype','text');
			resultFieldsLabelTypeMap.put('被保険者名', vf2Type1);

			Map<String, String> vf2Type2 = new Map<String, String> ();
			vf2Type2.put('label', '被保険者電話番号');
			vf2Type2.put('datatype','text');
			resultFieldsLabelTypeMap.put('被保険者電話番号', vf2Type2);
		}
		// 顧客番号の場合
		else if(searchValueMap.get('clientNumber') != NULL){
			verificationFieldList.add('顧客名');
			verificationFieldList.add('電話番号');
			verificationFieldList.add('生年月日');

			Map<String, String> vfType = new Map<String, String> ();
			vfType.put('label', '顧客名');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('顧客名', vfType);

			vfType = new Map<String, String> ();
			vfType.put('label', '電話番号');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('電話番号', vfType);

			vfType = new Map<String, String> ();
			vfType.put('label', '生年月日');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('生年月日', vfType);
		}
		//それ以外の顧客情報の場合
		else if(searchValueMap.get('clientKanaName') != NULL || searchValueMap.get('phone') != NULL || searchValueMap.get('birthdate') != NULL){
			verificationFieldList.add('顧客名');
			verificationFieldList.add('住所');
			verificationFieldList.add('電話番号');
			verificationFieldList.add('生年月日');

			Map<String, String> vfType = new Map<String, String> ();
			vfType.put('label', '顧客名');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('顧客名', vfType);

			vfType = new Map<String, String> ();
			vfType.put('label', '住所');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('住所', vfType);

			vfType = new Map<String, String> ();
			vfType.put('label', '電話番号');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('電話番号', vfType);

			vfType = new Map<String, String> ();
			vfType.put('label', '生年月日');
			vfType.put('datatype','text');
			resultFieldsLabelTypeMap.put('生年月日', vfType);
		}
		// 確認項目設定 End. //

		// 検索結果用変数設定 Begin. //
		Map<String, Object> resultValueMap = new Map<String, Object>();
		List<vlocity_ins.LookupResult> results = new List<vlocity_ins.LookupResult> ();
		vlocity_ins.LookupResult result;
		String recordId;
		Map<String, Object> verificationResult = new Map<String, Object>();

		Map<String, Object> resultInfo = (Map<String, Object>)inputs.get('resultInfo');
		String uniqueRequestName = (String) resultInfo.get('uniqueRequestName');
		String drBundleName = (String) resultInfo.get('drBundleName');
		String interactionObjName = (String) resultInfo.get('interactionObjName');

		// 証券番号の場合
		if(searchValueMap.get('policyNumber') != NULL){
			String policyNumber = (String)searchValueMap.get('policyNumber');

			for(E_Policy__c p : [SELECT AccessibleCustomer__c, AccessibleCustomer__r.Name, AccessibleCustomer__r.E_CLTPF_ZCLKNAME__c, Insured__c, InsuredName__c,
								AccessibleCustomer__r.CMN_SYSAddress__c, AccessibleCustomer__r.CMN_SYSContactPhone__c, AccessibleCustomer__r.E_CLTPF_CLTTYPE__c, AccessibleCustomer__r.AccountId,
								InsuredE_CLTPF_ZCLKNAME__c, Insured__r.CMN_SYSAddress__c, Insured__r.CMN_SYSContactPhone__c ,Insured__r.E_CLTPF_CLTTYPE__c, Insured__r.AccountId,
								AccessibleCustomer__r.E_CLTPF_CLTPHONE01__c, AccessibleCustomer__r.E_CLTPF_CLTPHONE02__c, AccessibleCustomer__r.Account.CMN_Phone3__c,
								Insured__r.E_CLTPF_CLTPHONE01__c, Insured__r.E_CLTPF_CLTPHONE02__c, Insured__r.Account.CMN_Phone3__c
								FROM E_Policy__c WHERE AccessibleCustomer__c != null AND COMM_CHDRNUM__c = :policyNumber]){

				// 検索結果出力：権利者
				String accZCLKNAME = p.AccessibleCustomer__r.E_CLTPF_ZCLKNAME__c == null ? '' : '（' + p.AccessibleCustomer__r.E_CLTPF_ZCLKNAME__c + '）';
				String accAddress = p.AccessibleCustomer__r.CMN_SYSAddress__c == null ? '' : p.AccessibleCustomer__r.CMN_SYSAddress__c;
				resultValueMap.put('権利者名', p.AccessibleCustomer__r.Name);

				String accPhone = '';
				if(p.AccessibleCustomer__r.E_CLTPF_CLTTYPE__c != null && p.AccessibleCustomer__r.E_CLTPF_CLTTYPE__c == 'C'){
					accPhone = p.AccessibleCustomer__r.E_CLTPF_CLTPHONE01__c+'　'+p.AccessibleCustomer__r.E_CLTPF_CLTPHONE02__c+'　'+p.AccessibleCustomer__r.Account.CMN_Phone3__c;
					accPhone = accPhone.replace('null', '');
					accPhone = accPhone.replace('　　', '　');
					resultValueMap.put('権利者電話番号', accPhone);
					recordId = p.AccessibleCustomer__r.AccountId;
				}else{
					accPhone = p.AccessibleCustomer__r.E_CLTPF_CLTPHONE01__c+'　'+p.AccessibleCustomer__r.E_CLTPF_CLTPHONE02__c;
					accPhone = accPhone.remove('null');
					resultValueMap.put('権利者電話番号', accPhone);
					recordId = p.AccessibleCustomer__c;
				}
				resultValueMap.put('権利者情報', p.AccessibleCustomer__r.Name +  accZCLKNAME + '<br/>'
									+ accAddress + '<br/>' + accPhone);

				result = new vlocity_ins.LookupResult(resultFieldsLabelTypeMap, resultFieldList1, resultValueMap,
													recordId, uniqueRequestName, request,
													verificationResult, verificationFieldList1, drBundleName, interactionObjName);

				results.add(result);

				// 検索結果出力：被保険者
				String insZCLKNAME = p.InsuredE_CLTPF_ZCLKNAME__c == null ? '' : '（' + p.InsuredE_CLTPF_ZCLKNAME__c + '）';
				String insAddress = p.Insured__r.CMN_SYSAddress__c == null ? '' : p.Insured__r.CMN_SYSAddress__c;
				resultValueMap = new Map<String, Object>();
				resultValueMap.put('被保険者名', p.InsuredName__c);

				String insPhone = '';
				if(p.Insured__r.E_CLTPF_CLTTYPE__c != null && p.Insured__r.E_CLTPF_CLTTYPE__c == 'C'){
					insPhone = p.Insured__r.E_CLTPF_CLTPHONE01__c+'　'+p.Insured__r.E_CLTPF_CLTPHONE02__c+'　'+p.Insured__r.Account.CMN_Phone3__c;
					insPhone = insPhone.replace('null', '');
					insPhone = insPhone.replace('　　', '　');
					resultValueMap.put('被保険者電話番号', insPhone);
					recordId = p.Insured__r.AccountId;
				}else{
					insPhone = p.Insured__r.E_CLTPF_CLTPHONE01__c+'　'+p.Insured__r.E_CLTPF_CLTPHONE02__c;
					insPhone = insPhone.remove('null');
					resultValueMap.put('被保険者電話番号', insPhone);
					recordId = p.Insured__c;
				}
				resultValueMap.put('被保険者情報', p.InsuredName__c + insZCLKNAME + '<br/>'
									+ insAddress + '<br/>' + insPhone);

				result = new vlocity_ins.LookupResult(resultFieldsLabelTypeMap, resultFieldList2, resultValueMap,
													recordId, uniqueRequestName, request,
													verificationResult, verificationFieldList2, drBundleName, interactionObjName);

				results.add(result);
			}
		}
		// 顧客番号の場合
		else if(searchValueMap.get('clientNumber') != NULL){
			String clientNumber = (String)searchValueMap.get('clientNumber');

			String query = 'SELECT Id,Name,E_CLTPF_CLTTYPE__c,AccountId,E_CLTPF_ZCLKNAME__c,CMN_SYSAddress__c,E_CLTPF_CLTPHONE01__c,E_CLTPF_CLTPHONE02__c,CMN_SYSContactPhone__c,CMN_SYSBirthdate__c,Account.CMN_Phone3__c';
			query += ' FROM Contact';
			query += ' WHERE Recordtype.Name = \'顧客\' ';
			clientNumber = clientNumber.removeStart('*');
			clientNumber = clientNumber.removeEnd('*');
			clientNumber = '%' + clientNumber + '%';
			query += ' AND E_CLTPF_CLNTNUM__c like :clientNumber ORDER BY Name';

			List<Contact> contactList = Database.query(query);

			for(Contact c : contactList){
				// 検索結果出力
				resultValueMap = new Map<String, Object>();
				resultValueMap.put('顧客名', c.Name);
				resultValueMap.put('顧客名(カナ)', c.E_CLTPF_ZCLKNAME__c);
				resultValueMap.put('住所', c.CMN_SYSAddress__c);
				resultValueMap.put('生年月日', c.CMN_SYSBirthdate__c);

				String phoneStr = '';
				if(c.E_CLTPF_CLTTYPE__c != null && c.E_CLTPF_CLTTYPE__c.equals('C')){
					phoneStr = c.E_CLTPF_CLTPHONE01__c+'　'+c.E_CLTPF_CLTPHONE02__c+'　'+c.Account.CMN_Phone3__c;
					phoneStr = phoneStr.replace('null', '');
					phoneStr = phoneStr.replace('　　', '　');
					resultValueMap.put('電話番号', phoneStr);
					recordId = c.AccountId;
				}else{
					phoneStr = c.E_CLTPF_CLTPHONE01__c+'　'+c.E_CLTPF_CLTPHONE02__c;
					phoneStr = phoneStr.remove('null');
					resultValueMap.put('電話番号', phoneStr);
					recordId = c.Id;
				}

				result = new vlocity_ins.LookupResult(resultFieldsLabelTypeMap, resultFieldList, resultValueMap,
													recordId, uniqueRequestName, request,
													verificationResult, verificationFieldList, drBundleName, interactionObjName);

				results.add(result);
			}
		}
		//それ以外の顧客情報の場合
		else if(searchValueMap.get('clientKanaName') != NULL || searchValueMap.get('phone') != NULL || searchValueMap.get('birthdate') != NULL){
			String clientKanaName = (String)searchValueMap.get('clientKanaName');
			String phone = (String)searchValueMap.get('phone');
			String birthdate = (String)searchValueMap.get('birthdate');
			if(birthdate!= null && birthdate != ''){
				birthdate = birthdate.removeStart('*');
				birthdate = birthdate.removeEnd('*');
				if(!(birthdate.isNumeric() && birthdate.length() == 8)) birthdate = CC_VILUtils.birthdateProcess(birthdate);
			}

			String query = 'SELECT Id,Name,E_CLTPF_CLTTYPE__c,AccountId,E_CLTPF_ZCLKNAME__c,CMN_SYSAddress__c,E_CLTPF_CLTPHONE01__c,E_CLTPF_CLTPHONE02__c,CMN_SYSContactPhone__c,CMN_SYSBirthdate__c,CMN_SYSAccountPhone__c,Account.CMN_Phone3__c';
			query += ' FROM Contact';
			query += ' WHERE Recordtype.Name = \'顧客\' ';
			if(clientKanaName != null && clientKanaName != ''){
				clientKanaName = CC_VILUtils.getSeionKana(clientKanaName);
				clientKanaName = clientKanaName.removeStart('*');
				clientKanaName = clientKanaName.removeEnd('*');
				clientKanaName = '%' + clientKanaName + '%';
				query += ' AND CC_ClientNameSeionKana__c like :clientKanaName';
			}
			if(phone != null && phone != ''){
				phone = phone.removeStart('*');
				phone = phone.removeEnd('*');
				phone = phone.replace('-', '');
				phone = '%' + phone + '%';
				query += ' AND ((CMN_SYSAccountPhone__c like :phone AND E_CLTPF_CLTTYPE__c = \'C\') OR (CMN_SYSContactPhone__c like :phone AND E_CLTPF_CLTTYPE__c = \'P\'))';
			}
			if(birthdate != null && birthdate != ''){
				if(birthdate.length() == 4){
					birthdate = '%' + birthdate;
					query += ' AND CMN_Birthdate__c like :birthdate ';
				}else{
					query += ' AND CMN_Birthdate__c = :birthdate ';
				}
			}

			query += ' ORDER BY Name';

			List<Contact> contactList = Database.query(query);

			for(Contact c : contactList){
				// 検索結果出力
				resultValueMap = new Map<String, Object>();
				resultValueMap.put('顧客名', c.Name);
				resultValueMap.put('顧客名(カナ)', c.E_CLTPF_ZCLKNAME__c);
				resultValueMap.put('住所', c.CMN_SYSAddress__c);
				resultValueMap.put('生年月日', c.CMN_SYSBirthdate__c);

				String phoneStr = '';
				if(c.E_CLTPF_CLTTYPE__c != null && c.E_CLTPF_CLTTYPE__c.equals('C')){
					phoneStr = c.E_CLTPF_CLTPHONE01__c+'　'+c.E_CLTPF_CLTPHONE02__c+'　'+c.Account.CMN_Phone3__c;
					phoneStr = phoneStr.replace('null', '');
					phoneStr = phoneStr.replace('　　', '　');
					resultValueMap.put('電話番号', phoneStr);
					recordId = c.AccountId;
				}else{
					phoneStr = c.E_CLTPF_CLTPHONE01__c+'　'+c.E_CLTPF_CLTPHONE02__c;
					phoneStr = phoneStr.remove('null');
					resultValueMap.put('電話番号', phoneStr);
					recordId = c.Id;
				}

				result = new vlocity_ins.LookupResult(resultFieldsLabelTypeMap, resultFieldList, resultValueMap,
													recordId, uniqueRequestName, request,
													verificationResult, verificationFieldList, drBundleName, interactionObjName);

				results.add(result);
			}
		}

		System.debug('results:'+results);
		output.put('lookupResults', results);

		// 検索結果用変数設定 End. //

		return true;
	}

}