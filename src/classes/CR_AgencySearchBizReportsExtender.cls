public with sharing class CR_AgencySearchBizReportsExtender extends SkyEditor2.Extender{
	
	private CR_AgencySearchBizReportsSVEController controller;
	
	/**
	 * Constructor
	 */
	public CR_AgencySearchBizReportsExtender(CR_AgencySearchBizReportsSVEController extension){
		this.controller = extension;

		//事業報告書の検索は母店担当MRのみ可能とする。 帳簿書類のために参照権を付与された事務所すべてを表示しない。
		E_IDCPF__c eidc = E_IDCPFDao.getRecsById(UserInfo.getUserId());
		System.assert(eidc!=null,'EIDCレコードが取得できませんでした。');
		if(String.isNotBlank(eidc.ZINQUIRR__c) && eidc.ZINQUIRR__c!='BR**'){			//BR**以外の場合担当母店のみ
			String BRCD = eidc.ZINQUIRR__c.right(2);
			controller.queryMap.get('resultTable').addWhere(' And Account__r.E_CL1PF_ZBUSBR__c = \'' + BRCD + '\'');
		}
	}
/*
	public override void preSearch(){
		system.assert(false, controller.QueryMap.get('resultTable').toSOQL());
	}
*/	
	/**
	 * CSVExport
	 */
	public PageReference ExportCSV(){
		PageReference pr = Page.E_CRExportAgencyBizReports;
		String soql = controller.queryMap.get('resultTable').toSoql();
		String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
		sWhere = E_SoqlUtil.trimLimitClause(sWhere);
		pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
								E_EncryptUtil.getEncryptedString(sWhere));  
		return pr;
	}
}