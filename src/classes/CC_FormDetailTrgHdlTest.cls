@isTest
private class CC_FormDetailTrgHdlTest {

	static CC_SRTypeMaster__c createSrTypeMaster(Boolean isInsert) {
		CC_SRTypeMaster__c srTypeMst = new CC_SRTypeMaster__c();
		srTypeMst.Name = '10001';
		srTypeMst.CC_SRTypeName__c = 'testSRTypeMaster';
		srTypeMst.CC_SRTypeSubject__c = 'test';
		srTypeMst.CC_Department__c = 'COLI';
		
		if(isInsert) {
			insert srTypeMst;
		}
		
		return srTypeMst;
	}
	
	static Case createServiceRequest(CC_SRTypeMaster__c srTypeMst, Boolean isInsert) {
		Case c = new Case();
		c.CC_SRTypeId__c = srTypeMst.Id;
		c.CMN_Source__c = '電話';
		
		if(isInsert) {
			insert c;
		}
		
		return c;
	}
	
	static CC_ActivityMaster__c createActMst(String nm, Boolean isInsert) {
		CC_ActivityMaster__c actMst = new CC_ActivityMaster__c(Name = nm);
		
		if(isInsert) {
			insert actMst;
		}
		
		return actMst;
	}
	
	static CC_FormDetail__c createFormDetail(Case c, Boolean isInsert) {
		CC_FormDetail__c detail = new CC_FormDetail__c(CC_Case__c = c.Id);

		if(isInsert) {
			insert detail;
		}
		
		return detail;
	}

	/**
	 * Updateのテスト.
	 *
	 * 確認事項：添付ファイルからSalesforce Filesへの移し替えが行われること
	 * 確認事項：書類封緘の活動が作られること
	 */
	static testMethod void myUnitTest01() {
		CC_SRTypeMaster__c srTypeMst = createSrTypeMaster(true);
		
		String actName = '書類封緘';
		createActMst(actName, true);
		
		Case c = createServiceRequest(srTypeMst, true);
		
		// コールセンター帳票情報をInsert
		CC_FormDetail__c detail = createFormDetail(c, true);
		
		// 添付ファイルをInsert
		Attachment att = TestI_TestUtil.createAttachment(false, detail.Id);
		String attName = 'TestAttachment_Name001';
		String attDesc = 'TestAttachment_Description001';
		att.Name = attName;
		att.Description = attDesc;
		insert att;
		
		Id attId = att.Id;
		
		// コールセンター帳票情報更新
		detail.CC_IsFormOutputFlg__c = true;
		
		Test.startTest();
		
		update detail;
		
		Test.stopTest();
		
		List<ContentDocumentLink> cdls = [Select ContentDocumentId, LinkedEntityId From ContentDocumentLink Where LinkedEntityId =: c.Id];
		System.assertEquals(1, cdls.size());
		ContentDocumentLink cdl = cdls.get(0);
		
		List<ContentVersion> cvs = [Select ContentDocumentId From ContentVersion];
		System.assertEquals(1, cvs.size());
		
		ContentVersion cv = cvs.get(0);
		System.assertEquals(cdl.ContentDocumentId, cv.ContentDocumentId);
		
		List<CC_SRActivity__c> acts = [Select Id, Name, CC_ActivityType__c From CC_SRActivity__c Where CC_SRNo__c =: c.Id];
		System.assertEquals(1, acts.size());
		System.assertEquals(actName, acts.get(0).CC_ActivityType__c);
	}

	/**
	 * Updateのテスト.
	 * 子→親の順の更新
	 * 確認事項：添付ファイルからSalesforce Filesへの移し替えが行われること
	 * 確認事項：書類封緘の活動が1件のみ作られること
	 */
	static testMethod void myUnitTest02() {
		CC_SRTypeMaster__c srTypeMst = createSrTypeMaster(true);
		
		String actName = '書類封緘';
		createActMst(actName, true);
		
		Case c = createServiceRequest(srTypeMst, true);
		
		// コールセンター帳票情報をInsert
		CC_FormDetail__c detailParent = createFormDetail(c, true);
		
		CC_FormDetail__c detailChild = createFormDetail(c, false);
		detailChild.CC_ParentSRFormDetail__c = detailParent.Id;
		insert detailChild;
		
		// 添付ファイルをInsert
		Attachment attForParent = TestI_TestUtil.createAttachment(false, detailParent.Id);
		attForParent.Name = 'TestAttachment_Name001';
		attForParent.Description = 'TestAttachment_Description001';
		insert attForParent;
		
		Attachment attForChild = TestI_TestUtil.createAttachment(false, detailChild.Id);
		attForChild.Name = 'TestAttachment_Name002';
		attForChild.Description = 'TestAttachment_Description002';
		insert attForChild;
		
		// コールセンター帳票情報更新
		detailChild.CC_IsFormOutputFlg__c = true;
		detailParent.CC_IsFormOutputFlg__c = true;
	
		update detailChild;
		
		Test.startTest();

		update detailParent;
		
		Test.stopTest();
		
		detailParent = [Select Id, CC_IsCreatedSrActivity__c From CC_FormDetail__c Where Id =: detailParent.Id Limit 1];
		System.assertEquals(true, detailParent.CC_IsCreatedSrActivity__c);
		
		detailChild = [Select Id, CC_IsCreatedSrActivity__c From CC_FormDetail__c Where Id =: detailChild.Id Limit 1];
		System.assertEquals(false, detailChild.CC_IsCreatedSrActivity__c);
		
		List<CC_SRActivity__c> acts = [Select Id, Name, CC_ActivityType__c From CC_SRActivity__c Where CC_SRNo__c =: c.Id];
		System.assertEquals(1, acts.size());
		System.assertEquals(actName, acts.get(0).CC_ActivityType__c);
	}

	/**
	 * Updateのテスト.
	 * 親→子の順の更新
	 *
	 * 確認事項：添付ファイルからSalesforce Filesへの移し替えが行われること
	 * 確認事項：書類封緘の活動が1件のみ作られること
	 */
	static testMethod void myUnitTest03() {
		
		CC_SRTypeMaster__c srTypeMst = createSrTypeMaster(true);
		
		String actName = '書類封緘';
		createActMst(actName, true);
		
		Case c = createServiceRequest(srTypeMst, true);
		
		// コールセンター帳票情報をInsert
		CC_FormDetail__c detailParent = createFormDetail(c, true);
		
		CC_FormDetail__c detailChild = createFormDetail(c, false);
		detailChild.CC_ParentSRFormDetail__c = detailParent.Id;
		insert detailChild;
		
		// 添付ファイルをInsert
		Attachment attForParent = TestI_TestUtil.createAttachment(false, detailParent.Id);
		attForParent.Name = 'TestAttachment_Name001';
		attForParent.Description = 'TestAttachment_Description001';
		insert attForParent;
		
		Attachment attForChild = TestI_TestUtil.createAttachment(false, detailChild.Id);
		attForChild.Name = 'TestAttachment_Name002';
		attForChild.Description = 'TestAttachment_Description002';
		insert attForChild;
		
		// コールセンター帳票情報更新
		detailChild.CC_IsFormOutputFlg__c = true;
		detailParent.CC_IsFormOutputFlg__c = true;
		
		update detailParent;

		Test.startTest();
		
		update detailChild;
		
		Test.stopTest();
		
		
		detailParent = [Select Id, CC_IsCreatedSrActivity__c From CC_FormDetail__c Where Id =: detailParent.Id Limit 1];
		System.assertEquals(true, detailParent.CC_IsCreatedSrActivity__c);
		
		detailChild = [Select Id, CC_IsCreatedSrActivity__c From CC_FormDetail__c Where Id =: detailChild.Id Limit 1];
		System.assertEquals(false, detailChild.CC_IsCreatedSrActivity__c);
		
		List<CC_SRActivity__c> acts = [Select Id, Name, CC_ActivityType__c From CC_SRActivity__c Where CC_SRNo__c =: c.Id];
		System.assertEquals(1, acts.size());
		System.assertEquals(actName, acts.get(0).CC_ActivityType__c);
	}

	/**
	 * Updateのテスト.
	 *
	 * 確認事項：添付ファイルからSalesforce Filesへの移し替えが行われないこと
	 */
	static testMethod void myUnitTest04() {
		CC_SRTypeMaster__c srTypeMst = createSrTypeMaster(true);
		
		String actName = '書類封緘';
		createActMst(actName, true);
		
		Case c = createServiceRequest(srTypeMst, true);
		
		// コールセンター帳票情報をInsert
		CC_FormDetail__c detail = createFormDetail(c, true);		
		
		// 添付ファイルをInsert
		Attachment att = TestI_TestUtil.createAttachment(true, detail.Id);
		Id attId = att.Id;
		
		// コールセンター帳票情報更新
		detail.CC_IsFormOutputFlg__c = false;
		
		Test.startTest();
		
		update detail;
		
		Test.stopTest();
		
		List<Attachment> resAtts = [Select Id, ParentId From Attachment Where ParentId =: detail.Id];
		
		System.assertEquals(1, resAtts.size());
		System.assertEquals(attId, resAtts.get(0).Id);
	}
	
	/**
	 * Updateのテスト.
	 *
	 * 確認事項：処理がスキップされること
	 */
	static testMethod void myUnitTest05() {
		
		CC_SRTypeMaster__c srTypeMst = createSrTypeMaster(true);
		
		String actName = '書類封緘';
		createActMst(actName, true);
		
		Case c = createServiceRequest(srTypeMst, true);
		
		// コールセンター帳票情報をInsert
		CC_FormDetail__c detail = createFormDetail(c, true);
		
		// コールセンター帳票情報更新
		detail.CC_IsFormOutputFlg__c = true;
		
		Test.startTest();
		
		update detail;
		
		Test.stopTest();
		
		List<ContentDocumentLink> cdls = [Select ContentDocumentId, LinkedEntityId From ContentDocumentLink Where LinkedEntityId =: c.Id];
		System.assertEquals(0, cdls.size());
	}

	
	static testMethod void myUnitTest06() {

		CC_SRTypeMaster__c srTypeMst = createSrTypeMaster(true);
		
		String actName = '書類封緘';
		createActMst(actName, true);
		
		Case c = createServiceRequest(srTypeMst, true);

		List<CC_FormDetail__c> details = new List<CC_FormDetail__c>();
		
		for(Integer i = 0; i < 5; i++) {
			details.add(new CC_FormDetail__c(CC_Case__c = c.Id));
		}
		
		CC_FormDetailTriggerBizLogic logic = new CC_FormDetailTriggerBizLogic();

		Test.startTest();
		
		logic.markIsCreatedActivity(details, false);

		Test.stopTest();
		
		for(CC_FormDetail__c detail : details) {
			System.assertEquals(true, detail.CC_IsCreatedSrActivity__c);
		}
	}
}