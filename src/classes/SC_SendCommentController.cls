/**
 * コメント送信コンポーネント コントローラー
 */
public with sharing class SC_SendCommentController {
	// 対象レコード 
	public SC_Office__c record {get; set;}
	// 通知フラグ
	public Boolean isSendMailMR {get; set;}
	public Boolean isSendMailManager {get; set;}
	public Boolean isSendMailCMD {get; set;}
	// 編集可能フラグ
	public Boolean canEdit {get; set;}
	// 追加コメント
	public String addComment {get; set;}

	// コンストラクタ
	public SC_SendCommentController(){
		addComment = '';
	}

	/**
	 * 保存処理
	 */
	public void doSave(){
		// コメント未入力の場合は処理終了
		if(String.isBlank(addComment)){
			return;
		}

		// コメントの再取得
		List<SC_Office__c> officeNow = SC_OfficeDao.getRefreshSCOffice(record.Id);
		if(officeNow == null || officeNow.isEmpty()){
			return;
		}

		record.Comment__c = officeNow.get(0).Comment__c;
		if(String.isBlank(record.Comment__c)){
			record.Comment__c = '';
		}

		// 投稿者情報を作成
		String CommentInfo = '';
		CommentInfo += '-----';
		CommentInfo += System.now().format();
		CommentInfo += '  ';
		CommentInfo += UserInfo.getName();
		CommentInfo += '-----';
		CommentInfo += '\n'; 
		CommentInfo += addComment;
		CommentInfo += '\n\n';

		record.Comment__c = CommentInfo + record.Comment__c;
		// CMD がコメントする場合は必ず CMD へメール送信
		if(isSendMailCMD){
			record.sendMailCMD__c = true;
		}

		update record;

		// 追加コメントを初期化
		addComment = '';
		// 通知フラグを外す
		record.sendMailMR__c      = false;
		record.sendMailManager__c = false;
		record.sendMailCMD__c     = false;

		return;
	}
}