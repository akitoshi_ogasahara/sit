@isTest
private class TestCR_AgencySearchOSWrapController {
	
	private static testMethod void AgencySearchOSTest001(){
        
        CR_AgencySearchOSWrapController target = new CR_AgencySearchOSWrapController();
            System.assertEquals(target.getPageTitle(), '代理店検索（外部委託）');        
    }

    //　社員ユーザの場合
    private static testMethod void AgencySearchOSTest002(){
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		System.runAs(usr){
			Test.startTest();
			PageReference pageRef = Page.E_CRAgencySearchOS;
			Test.setCurrentPage(pageRef);
			
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
			ApexPages.StandardController stdController = new ApexPages.StandardController(agny);
			
			// 拡張クラスを生成
			CR_AgencySearchOSWrapController target = new CR_AgencySearchOSWrapController();
						
			// 実行結果確認
			System.assertEquals(target.pageAction(), null);
						
			Test.stopTest();			
		}
	}

	// 代理店ユーザの場合
    private static testMethod void AgencySearchOSTest003(){
    	Account acc = TestCR_TestUtil.createAccount(true);
    	Contact con = TestCR_TestUtil.createContact(true, acc.Id, 'TestUser002');
    	// 代理店ユーザ作成
		User usr = TestCR_TestUtil.createAgentUser(true, 'TestUser002', 'E_PartnerCommunity', con.Id);
		
		System.runAs(usr){
			// 代理店ユーザ　ID管理作成
			TestCR_TestUtil.createAgencyOutSrcPermissions(usr.Id);
			// 代理店提出オブジェクト生成
			CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
			
			Test.startTest();
			PageReference pageRef = Page.E_CRAgencySearchOS;
			Test.setCurrentPage(pageRef);

			// 拡張クラスを生成
			CR_AgencySearchOSWrapController target = new CR_AgencySearchOSWrapController();
			target.viewingAgencyCode = agny.AgencyCode__c;

			// 実行結果確認
			System.assertEquals(target.pageAction().getUrl(),'/apex/e_croutsrcs?id=' + agny.Id);
						
			Test.stopTest();			
		}
	}		
}