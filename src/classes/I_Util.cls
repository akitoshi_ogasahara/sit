/**
 * IRIS 共通処理クラス
 */
public class I_Util{
	/**
	 * ソート判定（文字列）
	 * @paarm  String：比較元
	 * @paarm  String：比較先
	 * @return Integer:比較結果
	 */
	public static Integer compareToString(String compare, String compareTo){
		Integer result = 0;
		// null は昇順で一番下
		if(String.isBlank(compare)){
			result = 1;
		} else if(String.isBlank(compareTo)){
			result = -1;
		} else {
			result = compare.compareTo(compareTo);
		}
		return result;
	}

	/**
	 * ソート判定（整数）
	 * @paarm  Integer：比較元
	 * @paarm  Integer：比較先
	 * @return Integer:比較結果
	 */
	public static Integer compareToInteger(Integer compare, Integer compareTo){
		Integer result = 0;
		if(compare == compareTo){
			result = 0;
		} else if(compare > compareTo){
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

	/**
	 * ソート判定（Decimal）
	 * @paarm  Decimal：比較元
	 * @paarm  Decimal：比較先
	 * @return Decimal:比較結果
	 */
	public static Integer compareToDecimal(Decimal compare, Decimal compareTo){
		Integer result = 0;
		if(compare == compareTo){
			result = 0;
		} else if(compare > compareTo){
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

	/**
	 * ソート判定（Date）
	 * @paarm  Date：比較元
	 * @paarm  Date：比較先
	 * @return Date:比較結果
	 */
	public static Integer compareToDate(Date compare, Date compareTo) {
		Integer result = 0;
		if (compare == compareTo) {
			result = 0;
		} else if(compare > compareTo) {
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

	/**
	 * ソート判定（DateTime）
	 * @paarm  DateTime：比較元
	 * @paarm  DateTime：比較先
	 * @return DateTime:比較結果
	 */
	public static Integer compareToDateTime(DateTime compare, DateTime compareTo) {
		Integer result = 0;
		if (compare == compareTo) {
			result = 0;
		} else if(compare > compareTo) {
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

	/**
	 * 自動検索 - 代理店SFID取得
	 * @paarm  なし
	 * @return String:代理店事務所SFID(Account)
	 */
	public static String getAutoSearchAgencyId(){
		return E_CookieHandler.getCookieAutoSearchAgency();
	}

	/**
	 * 自動検索 - 事務所SFID取得
	 * @paarm  なし
	 * @return String:代理店事務所SFID(Account)
	 */
	public static String getAutoSearchOfficeId(){
		return E_CookieHandler.getCookieAutoSearchOffice();
	}

	/**
	 * 自動検索 - 募集人SFID取得
	 * @paarm  なし
	 * @return String:募集人SFID(Contact)
	 */
	public static String getAutoSearchAgentId(){
		return E_CookieHandler.getCookieAutoSearchAgent();
	}

	/**
	 * 絞り込み検索判定
	 * @return 絞り込み検索がされている:true されていない:false
	 */
	public static Boolean isRefineSearch(){
		if(String.isBlank(I_Util.getAutoSearchAgencyId()) &&
		   String.isBlank(I_Util.getAutoSearchOfficeId()) &&
		   String.isBlank(I_Util.getAutoSearchAgentId())){
			return false;
		}
		return true;
	}

	/**
	 * キーワード分割
	 * 半角スペース、全角スペースで文字列を分割する
	 * @return List<String>
	 */
	public static List<String> splitStringSpace(String target){
		List<String> result = new List<String>();
		for(String s : target.replaceAll('　', ' ').split(' ')){
			if(String.isNotBlank(s)){
				result.add(s);
			}
		}
		return result;
	}

	/**
	 * 検索キーワードリスト作成
	 * 入力されたキーワードから検索用のキーワードをListにして返却する
	 * @return List<String>
	 */
	public static List<String> createSearchkeywordList(String keyword){
		List<String> result = new List<String>();
		// キーワード分割
		for(String s : keyword.replaceAll('　', ' ').split(' ')){
			if(String.isNotBlank(s)){
				result.add(s);

				// 全角カタカナを含む場合、半角カタカナのキーワードを追加する
				if(E_Util.isContainsEmKatakana(s)){
					system.debug('全角カタカナが含まれているので、半角カタカナを追加：' + E_Util.emKatakanaToEn(s));
					result.add(E_Util.emKatakanaToEn(s));
				}

				// 半角カタカナを含む場合、全角カタカナのキーワードを追加する
				if(E_Util.isContainsEnKatakana(s)){
					system.debug('半角カタカナが含まれているので、全角カタカナを追加：' + E_Util.enKatakanaToEm(s));
					result.add(E_Util.enKatakanaToEm(s));
				}
			}
		}
		return result;

	}

	/**
	 * 各値をString型へ変換
	 * @param target 対象値
	 * @return 変換後文字列
	 */
	public static String toStr(String target){
		return (target == null) ? '' : target;
	}
	public static String toStr(Integer target){
		return (target == null) ? '0' : String.valueOf(target);
	}
	public static String toStr(Long target){
		return (target == null) ? '0' : String.valueOf(target);
	}
	public static String toStr(Double target){
		return (target == null) ? '0' : target.format().replace(',','');
	}
	public static String toStr(Decimal target){
		return (target == null) ? '0' : target.format().replace(',','');
	}
	public static String toStr(Boolean target){
		return (target == null) ? 'false' : 'true';
	}
	public static String toStr(Date target){
		String format = '';
		if(target != null){
			format = target.year() + '/';		// 年
			if(target.month() < 10){ format += '0';}
			format += target.month() + '/';		// 月
			if(target.day() < 10){ format += '0';}
			format += target.day();				// 日
		}
		return format;
	}
	public static String toStr(DateTime target){
		String format = '';
		if(target != Null){
			format = target.format('yyyy/MM/dd HH:mm:ss');
		}
		return format;
	}
	public static String toStr(Id target){
		return (target == null) ? '' : target;
	}

	/**
	 * 各値を小数点桁数で整形したString型へ変換
	 * @param target 対象値
	 * @param decpoint 小数点桁数
	 * @return 変換後文字列
	 */
	public static String toDecStr(Double target, Integer decpoint){
		String temp			= '';
		String dec			= '0'.repeat('', decpoint);
		temp = (target == null) ? '0' : target.format().replace(',', '');
		Integer point = temp.indexOf('.', 1);
		temp = (point == -1) ? temp + '.' + dec : (temp + dec).Left(point + decpoint + 1);
		return temp;
	}
	public static String toDecStr(Decimal target, Integer decpoint){
		String temp			= '';
		String dec			= '0'.repeat('', decpoint);
		temp = (target == null) ? '0' : target.format().replace(',', '');
		Integer point = temp.indexOf('.', 1);
		temp = (point == -1) ? temp + '.' + dec : (temp + dec).Left(point + decpoint + 1);
		return temp;
	}

	/**
	 * 各値を3桁カンマで整形したString型へ変換
	 * @param target 対象値
	 * @param decpoint 小数点桁数
	 * @return 変換後文字列
	 */
	public static String toFormatStr(Integer target){
		return (target == null) ? '0' : target.format();
	}
	public static String toFormatStr(Long target){
		return (target == null) ? '0' : target.format();
	}
	public static String toFormatStr(Double target){
		return (target == null) ? '0' : target.format();
	}
	public static String toFormatStr(Decimal target){
		return (target == null) ? '0' : target.format();
	}

}