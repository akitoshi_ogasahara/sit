public with sharing class WY_SearchResult {
	private static final List<String> YAKKANTYPE_LIST = new List<String>{'本編', '別添1', '別添2'};
	
	public String contractDateFrom {get; set;}
	public String contractDateTo {get; set;}
	public String insType {get; set;}
	public I_ContentMaster__c yakkanInfo {get; set;}
	public List<WY_ChildFile> childfileList {get; set;}

	public WY_SearchResult(I_ContentMaster__c parentContent, List<I_ContentMaster__c> childContents){
		this.contractDateFrom = convertDate(parentContent.ContractDateFrom__c) + '～';
		this.contractDateTo = convertDate(parentContent.ContractDateTo__c);
		this.insType = parentContent.InsType__c.replaceAll(';','<br/>');
		this.yakkanInfo = parentContent;

		Map<String, I_ContentMaster__c> yakkanTypeMap = new Map<String, I_ContentMaster__c>();
		childfileList = new List<WY_ChildFile>();
		if(!childContents.isEmpty()){
			for(I_ContentMaster__c content : childContents){
				yakkanTypeMap.put(content.YakkanType__c, content);
			}
		}
		for(String yType : YAKKANTYPE_LIST){
			if(yakkanTypeMap.containsKey(yType)){
				I_ContentMaster__c cont = yakkanTypeMap.get(yType);
				childfileList.add(new WY_ChildFile(yType, cont, cont.Attachments));
			}else{
				childfileList.add(new WY_ChildFile(yType, new I_ContentMaster__c(), new List<Attachment>()));
			}
		}
	}
	
	private String convertDate(Date conDate){
		if(conDate == null){
			return '';
		}
		String cDate = String.valueOf(conDate);
		List<String> dateStr = cDate.split('-');
		return dateStr[0] + '年' + dateStr[1] + '月' + dateStr[2] + '日';
	}
}