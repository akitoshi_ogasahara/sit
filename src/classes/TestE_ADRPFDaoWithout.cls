/**
 * 顧客情報変更受付履歴レコードの作成用(without版)のテストクラス
 * CreateDate 2016/10/27
 * @author Terrasky
 */
@isTest
private class TestE_ADRPFDaoWithout {
	//顧客情報変更受付履歴レコードを作成するメソッドのテスト
	@isTest(SeeAllData=true) static void insertRecTest() {
		//データ作成
		User us = createUser();
		E_IDCPF__c idcp = createIDManage();

		Test.startTest();
		E_ADRPF__c adrp = E_ADRPFDaoWithout.insertRec(us,idcp.Id);
		System.assertEquals('メールアドレス変更',adrp.Kbn__c);
		Test.stopTest();
	}
	
	//顧客情報変更受付履歴レコードを作成する（@future）のテスト
	@isTest(SeeAllData=true) static void insertRecFutureTest() {
		//データ作成
		User us = createUser();
		E_IDCPF__c idcp = createIDManage();

		Test.startTest();
		E_ADRPFDaoWithout.insertRec(us.Id,idcp.Id);
		Test.stopTest();
		E_ADRPF__c adrp = [select Kbn__c,E_IDCPF__c from E_ADRPF__c where E_IDCPF__c = :idcp.Id];
		System.assertEquals('メールアドレス変更',adrp.Kbn__c);
	}

	//ID管理作成
	static E_IDCPF__c createIDManage(){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		insert ic;
		return ic;
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	//ユーザ作成
	static User createUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = con.Id;
			insert us;
		}
		return us;
	}
	
}