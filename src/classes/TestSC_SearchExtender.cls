@isTest
private class TestSC_SearchExtender {
	// コンストラクタ
	private static testMethod void SearchTest001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			Test.setCurrentPage(Page.E_SCSearch);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
			SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
			SC_SearchExtender extender = new SC_SearchExtender(controller);
		Test.stopTest();
	}

	// init
	private static testMethod void SearchTest002(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			Test.setCurrentPage(Page.E_SCSearch);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
			SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
			SC_SearchExtender extender = new SC_SearchExtender(controller);

			extender.init();
		Test.stopTest();
	}

	// ExportCSV
	private static testMethod void SearchTest003(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			Test.setCurrentPage(Page.E_SCSearch);
	    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
			SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
			SC_SearchExtender extender = new SC_SearchExtender(controller);

			PageReference pr = extender.ExportCSV();
		Test.stopTest();
	}

	/**
	 * String getRemindBeforeAg()
	 */
	private static testMethod void getRemindBeforeAg_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getRemindBeforeAg();
		Test.stopTest();

		System.assertEquals(SC_MailManager.MT_REMIND_BEFORE_AG, result);
	}

	/**
	 * String getRemindAfterAg()
	 */
	private static testMethod void getRemindAfterAg_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getRemindAfterAg();
		Test.stopTest();

		System.assertEquals(SC_MailManager.MT_REMIND_AFTER_AG, result);
	}

	/**
	 * String getRemindDefectAg()
	 */
	private static testMethod void getRemindDefectAg_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getRemindDefectAg();
		Test.stopTest();

		System.assertEquals(SC_MailManager.MT_REMIND_DEFECT_AG, result);
	}

	/**
	 * String getRemindBeforeMr()
	 */
	private static testMethod void getRemindBeforeMr_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getRemindBeforeMr();
		Test.stopTest();

		System.assertEquals(SC_MailManager.MT_REMIND_BEFORE_MR, result);
	}

	/**
	 * String getRemindAfterMr()
	 */
	private static testMethod void getRemindAfterMr_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getRemindAfterMr();
		Test.stopTest();

		System.assertEquals(SC_MailManager.MT_REMIND_AFTER_MR, result);
	}

	/**
	 * String getRemindDefectMr()
	 */
	private static testMethod void getRemindDefectMr_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getRemindDefectMr();
		Test.stopTest();

		System.assertEquals(SC_MailManager.MT_REMIND_DEFECT_MR, result);
	}

	/**
	 * void doRemaind()
	 */
	private static testMethod void doRemaind_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		record.FiscalYear__c = System.Label.SC_FiscalYear;
		update record;

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);
		controller.doSearch();

    	for(SC_SearchSVEController.resultTableItem i : controller.resultTable.items){
    		i.selected = true;
		}
		extender.doRemaind();

		Test.stopTest();

		System.assert(true == String.isNotBlank(extender.selectedIds));
	}

	/**
	 * String getReportId()
	 */
	@isTest(SeeAllData=true)
	private static void getReportId_test001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
		Test.setCurrentPage(Page.E_SCSearch);
    	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(record);
		SC_SearchSVEController controller = new SC_SearchSVEController(standardcontroller);
		SC_SearchExtender extender = new SC_SearchExtender(controller);

		String result = extender.getReportId();
		
		Test.stopTest();

		System.assert(true == (String.isNotBlank(result)));
	}
}