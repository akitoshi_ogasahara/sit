public with sharing class I_PerformanceController {

	public Id paramSalesResultsId {get; set;}			// 社内挙積情報ID

	transient public String json {get; set;}			// JSONデータ

	/**
	 * コンストラクタ
	 */
	public I_PerformanceController(){
		this.paramSalesResultsId		= null;										// 社内挙積情報ID

	    this.json					= '';											// JSONデータ
	}

	/**
	 * JS用値設定の取得(リモートアクション)
	 */
	@readOnly
	@RemoteAction
	public static String getJSParam(Id pPerformanceId){
		I_PerformanceController cls	= new I_PerformanceController();
		cls.paramSalesResultsId			= pPerformanceId;	// 社内挙積情報ID
		cls.getJSParameter();								// JS用値設定の取得
		return cls.json;									// JSONデータ
	}

	/**
	 * JS用値設定の取得
	 */
	public void getJSParameter(){
		// JSONデータ
		this.json				= getJSON();
	}

	/**
	 * JSONデータの取得
	 */
	public String getJSON(){
		List<String> subfields		= new List<String>();
		List<String> lines			= new List<String>();
		List<String> fields			= new List<String>();

		// 社内挙積情報取得
		E_EmployeeSalesResults__c employeeSalesResults = E_EmployeeSalesResultsDao.getRecById(this.paramSalesResultsId);

		fields.add('"yyyymm":"' + I_Util.toStr(employeeSalesResults.YYYYMM__c) + '"');						// 年月

		lines			= new List<String>();
		subfields		= new List<String>();
		subfields.add('"name":"fs"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_FS__c));				// 達成率 FS
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_FS__c));						// 予算 FS
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_FS__c));						// 新契約換算ANP　FS
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"protection"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_Protection__c));		// 達成率 Protection
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_Protection__c));				// 予算 Protection
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_Protection__c));				// 新契約換算ANP　Protection
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"bacoli"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_BACOLI__c));			// 達成率 BA-COLI
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_BACOLI__c));					// 予算 BA-COLI
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_BACOLI__c));					// 新契約換算ANP　BA-COLI
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"total"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_Total__c));			// 達成率 Total
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_Total__c));					// 予算 Total
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_Total__c));					// 新契約換算ANP　Total
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"actual":[' + String.join(lines,',') + ']');

		lines			= new List<String>();
		subfields		= new List<String>();
		subfields.add('"name":"fsytd"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_FSYTD__c));			// 達成率（YTD） FS
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_FSYTD__c));					// 予算（YTD） FS
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_FSYTD__c));					// 新契約換算ANP（YTD）　FS
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"protectionytd"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_ProtectionYTD__c));	// 達成率（YTD） Protection
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_ProtectionYTD__c));			// 予算（YTD） Protection
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_ProtectionYTD__c));			// 新契約換算ANP（YTD）　Protection
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"bacoliytd"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_BACOLIYTD__c));		// 達成率（YTD） BA-COLI
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_BACOLIYTD__c));				// 予算（YTD） BA-COLI
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_BACOLIYTD__c));				// 新契約換算ANP（YTD）　BA-COLI
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"totalytd"');
		subfields.add('"achiverate":' + I_Util.toStr(employeeSalesResults.AchiveRate_TotalYTD__c));			// 達成率（YTD） Total
		subfields.add('"budget":' + I_Util.toStr(employeeSalesResults.Budget_TotalYTD__c));					// 予算（YTD） Total
		subfields.add('"nbwanp":' + I_Util.toStr(employeeSalesResults.NBWANP_TotalYTD__c));					// 新契約換算ANP（YTD）　Total
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"actualytd":[' + String.join(lines,',') + ']');

		lines			= new List<String>();
		subfields		= new List<String>();
		subfields.add('"agency":' + I_Util.toStr(employeeSalesResults.ActiveAgency__c));					// 稼動事務所数
		subfields.add('"agent":' + I_Util.toStr(employeeSalesResults.ActiveAgent__c));						// 稼働募集人数

		fields.add('"active":{' + String.join(subfields,',') + '}');

		lines			= new List<String>();
		subfields		= new List<String>();
		subfields.add('"name":"iqa"');
		subfields.add('"percent":' + I_Util.toStr(employeeSalesResults.IQA__c));							// IQA継続率
		subfields.add('"numerator":' + I_Util.toStr(employeeSalesResults.IQA_Numerator__c));				// IQA継続率 分子
		subfields.add('"denominator":' + I_Util.toStr(employeeSalesResults.IQA_Denominator__c));			// IQA継続率 分母
		subfields.add('"average":' + I_Util.toStr(employeeSalesResults.IQA_NNAverage__c));					// IQA継続率（全社平均）
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"mof13"');
		subfields.add('"percent":' + I_Util.toStr(employeeSalesResults.MOF13__c));							// MOF13継続率
		subfields.add('"numerator":' + I_Util.toStr(employeeSalesResults.MOF13_Numerator__c));				// MOF13継続率 分子
		subfields.add('"denominator":' + I_Util.toStr(employeeSalesResults.MOF13_Denominator__c));			// MOF13継続率 分母
		subfields.add('"average":' + I_Util.toStr(employeeSalesResults.MOF13_NNAverage__c));				// MOF13継続率（全社平均）
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"mof25"');
		subfields.add('"percent":' + I_Util.toStr(employeeSalesResults.MOF25__c));							// MOF25継続率
		subfields.add('"numerator":' + I_Util.toStr(employeeSalesResults.MOF25_Numerator__c));				// MOF25継続率 分子
		subfields.add('"denominator":' + I_Util.toStr(employeeSalesResults.MOF25_Denominator__c));			// MOF25継続率 分母
		subfields.add('"average":' + I_Util.toStr(employeeSalesResults.MOF25_NNAverage__c));				// MOF25継続率（全社平均）
		lines.add('{' + String.join(subfields,',') + '}');

		subfields		= new List<String>();
		subfields.add('"name":"defactrate"');
		subfields.add('"percent":' + I_Util.toStr(employeeSalesResults.DefactRate__c));						// 単純不備率
		subfields.add('"numerator":' + I_Util.toStr(employeeSalesResults.DefactRate_Numerator__c));			// 単純不備率 単純不備数
		subfields.add('"denominator":' + I_Util.toStr(employeeSalesResults.DefactRate_Denominator__c));		// 単純不備率 総数
		subfields.add('"average":' + I_Util.toStr(employeeSalesResults.DefactRate_NNAverage__c));			// 単純不備率（全社平均）
		lines.add('{' + String.join(subfields,',') + '}');

		fields.add('"percent":[' + String.join(lines,',') + ']');

		return String.join(fields,',');
	}
}