global with sharing class CR_AgencySearchBizConfExtender extends SkyEditor2.Extender{
	private CR_AgencySearchBizConfSVEController controller;
	public CR_AgencySearchBizConfExtender(CR_AgencySearchBizConfSVEController extension){
		controller = extension;
	}
/*
	public Boolean getIsGrantMode(){
		//検索条件で大規模代理店=Falseが検索条件になっている場合に権限付与モード
		return !controller.largeAgency_val.SkyEditor2__Checkbox__c;
	}
*/
	public pageReference changeConfig(){
		Integer row = Integer.valueOf(ApexPages.currentPage().getParameters().get('row'));
		system.debug('[row]' + row);
		CR_Agency__c  rec = (CR_Agency__c)controller.resultTable.records[row];
		rec.LargeAgency__c = !rec.LargeAgency__c;
		
		update rec;
		
		//権限セット付与
		grantPermissionSet(rec.AgencyCode__c);
	
		return null;
	}
	
	/**
	 * CSVExport
	 */
	public PageReference ExportCSV(){
		PageReference pr = Page.E_CRExportAgencyBizConf;
		String soql = controller.queryMap.get('resultTable').toSoql();
		String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
		sWhere = E_SoqlUtil.trimLimitClause(sWhere);
		pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
								E_EncryptUtil.getEncryptedString(sWhere));  
		return pr;
	}
	
	@future 
	private static void grantPermissionSet(String agencyCd){
		//権限セットの付与は非同期で実施
		E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
		bizLogic.addTargetUsers(E_IDCPFDaoWithout.getActiveUsersRecsByZHEADAY(agencyCd));
		bizLogic.PermissionSetAssign();
	}
}