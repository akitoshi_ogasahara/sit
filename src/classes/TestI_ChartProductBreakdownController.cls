@isTest
private class TestI_ChartProductBreakdownController {

	/**
	 * TypeAのテスト
	 */
	@isTest static void typeATest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 商品内訳の作成
		List<E_ProductBreakdown__c> productBreakdowns = TestE_ProductBreakdownDao.createProductBreakdowns(agencySalesResults.Id);
		insert productBreakdowns;

		Test.startTest();

		I_ChartProductBreakdownController chartProductBreakdownController = new I_ChartProductBreakdownController();
		chartProductBreakdownController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID

		// JS用値設定の取得
		chartProductBreakdownController.getJSParameter();
		System.assertNotEquals(0, chartProductBreakdownController.json.length());

		// JS用値設定の取得(リモートアクション)
		String json = I_ChartProductBreakdownController.getJSParam(chartProductBreakdownController.paramSalesResultsId);
		System.assertNotEquals(0, json.length());

	}

	/**
	 * TypeBのテスト
	 */
	@isTest static void typeBTest() {

		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		agencySalesResults.YYYYMM__c = 'abcdef';
		insert agencySalesResults;

		// 商品内訳の作成
		List<E_ProductBreakdown__c> productBreakdowns = TestE_ProductBreakdownDao.createProductBreakdowns(agencySalesResults.Id);
		insert productBreakdowns;

		Test.startTest();

		I_ChartProductBreakdownController chartProductBreakdownController = new I_ChartProductBreakdownController();
		chartProductBreakdownController.paramSalesResultsId	= agencySalesResults.Id;	// 代理店挙積情報ID

		// JS用値設定の取得
		chartProductBreakdownController.getJSParameter();
		System.assertEquals(0, chartProductBreakdownController.json.length());

	}

}