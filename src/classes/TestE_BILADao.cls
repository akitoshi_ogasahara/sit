@isTest
private class TestE_BILADao {
	@isTest static void TestE_BILADao() {
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人2');
		E_Policy__c policy = createPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		E_BILA__c bila = new E_BILA__c(
										E_Policy__c = policy.id
										,ParentAccount__c = acc.Parent.Id	
										,Account__c = acc.Id			//事務所Id
										,Contact__c = con.Id			//募集人Id	
										,Owname__c = 'テスト契約者'		//契約者名
										,CHDRNUM__c = '12345678'		//証券番号
										,ZCOVRNAM__c = 'テスト保険種類'	//保険種類
										,INSTAMT12__c = 70000			//保険料
										,ZSUMINS12N__c = 1000			//保険金額
										,YYYY__c = Decimal.valueOf(Datetime.now().addMonths(-1).format('YYYY'))
										,MM__c = Decimal.valueOf(Datetime.now().addMonths(-1).format('MM'))
									);
		insert bila;
		Test.setCreatedDate(bila.Id, Datetime.now());

		I_NoticeBILAController controller = new I_NoticeBILAController();
		List<E_BILA__c> recs = new List<E_BILA__c>();
		recs = E_BILADao.getRecsBySortKey('');
		
		system.assert(!recs.isEmpty());
		
	}



	/*
	**AccountData作成
	*/
	private static Account createDataAccount(Boolean isInsert){
		/*代理店格*/
		Account pacc= new Account(
			Name = 'テスト代理店格'
			, E_CL1PF_ZHEADAY__c = 'AMS01'
			);
		if(isInsert){
			insert pacc;
		}
		
		/*代理店事務所*/
		Account acc = new Account(
			Name = 'テスト代理店事務所'
			, ParentId = pacc.Id
			, E_CL2PF_ZAGCYNUM__c = 'AMS11'
			,Owner = TestE_TestUtil.createUser(true,'testMr','E_EmployeeStandard'));
		if(isInsert){
			insert acc;
		}

		return acc;

	}

	/*
	**Contact作成
	*/
	private static Contact createDataContct(Boolean isInsert,Id accId, String lastName){
		/*募集人*/
		Contact con = new Contact(
			LastName = lastName
			, AccountId = accId);
		
		if(isInsert){
			insert con;
		}
		return con;
	}
	/*
	**保険契約ヘッダ
	*/
	public static E_Policy__c createPolicy(Boolean isInsert, Id conId, Id conId2, String recTypeDevName, String cno, Decimal zc) {
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(recTypeDevName);
		E_Policy__c src = new E_Policy__c(
			RecordTypeId = recTypeId
			, COMM_CHDRNUM__c = cno
			, MainAgent__c = conId
			, SubAgent__c  = conId2
			, COLI_ZCSHVAL__c = zc
		);
		if(isInsert){
			insert src;
		}
		return src;
	}

}