public with sharing class Sailfish {

    @AuraEnabled
    public static Sailfish_Onboarding__c insertOnboardingRecordApex(Sailfish_Onboarding__c onboardingRecord) {
        // Perform isUpdatable() checking first, then
        insert onboardingRecord;
        return onboardingRecord; 
    }    

    @AuraEnabled
    public static Sailfish_Onboarding__c updateOnboardingRecordApex(Sailfish_Onboarding__c onboardingRecord) {
        // Perform isUpdatable() checking first, then
        onboardingRecord.Life_Assured_Gender__c = 'Female';
        update onboardingRecord;
        return onboardingRecord; 
    }    
    
    @AuraEnabled
    public static Sailfish_Results__c saveResultsRecord(Sailfish_Results__c resultsRecord) {
        // Perform isUpdatable() checking first, then
        insert resultsRecord;
        return resultsRecord; 
    }    

    @AuraEnabled 
    public static user fetchUser(){
        // query current user information  
        User oUser = [select id,Name,TimeZoneSidKey,Username,Alias,Country,Email,FirstName,LastName,IsActive,IsPortalEnabled 
                      FROM User Where id =: userInfo.getUserId()];
        return oUser;
    }

    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Name from ' + ObjectName + ' where Name LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    } 

    @AuraEnabled
    public static List < sObject > fetchLookUpValuesAccessId(String searchKeyWord, String ObjectName) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery =  'select id, Form_Id__c from ' + ObjectName + ' where Form_Id__c LIKE: searchKey order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }
    
    // Get one Assesment record from its Id 
   
    @AuraEnabled 
    public static List <Sailfish_Onboarding__c> getOnboardingRecordAllFromName(string Name) {
        return [SELECT Id, Name, BMI__c, Height_cm__c, Weight_kg__c, Life_Assured_Gender__c
                FROM Sailfish_Onboarding__c
                WHERE Name = :Name 
                LIMIT 1 
               ];
    }

    // Get [0] Onboarding Record from Id
   
    @AuraEnabled
    public static List <Sailfish_Onboarding__c> getOnboardingRecordFromId(string Id) {
        return [SELECT Id, Name
                FROM Sailfish_Onboarding__c
                WHERE Id = :Id 
                LIMIT 1 
               ];
    }
    
    // Get [0] Onboarding Record from Name
    
    @AuraEnabled
    public static List <Sailfish_Onboarding__c> getOnboardingRecordFromName(string Name) {
        return [SELECT Id, Name
                FROM Sailfish_Onboarding__c
                WHERE Name = :Name 
                LIMIT 1
               ];
    }
 
    @AuraEnabled
    public static List <Sailfish_Onboarding__c> getOnboardingRecordFromAccessId(string accessId) {
        return [SELECT Id, Name, Form_Id__c
                FROM Sailfish_Onboarding__c
                WHERE Form_Id__c = :accessId 
                LIMIT 1
               ];
    }
    
    // Methods for Results management 
    
    @AuraEnabled
    public static List <Sailfish_Results__c> getResultsByOnboarding() {
        return [SELECT Id, Result_Type__c, Result_Definition__c, Result_Definition_jp__c, Result_Rating__c, Result_Score__c, Result_Freeform__c, Result_Active__c, Sailfish_Onboarding__r.Name
                FROM Sailfish_Results__c
                ORDER BY  Sailfish_Onboarding__r.Name  , Result_Definition__c DESC
               ];
    }

    @AuraEnabled
    public static List <Sailfish_Results__c> getSearchRulesetResults(String recordName) {
        return [SELECT Id, Result_Type__c, Result_Definition__c, Result_Definition_jp__c, Result_Rating__c, Result_Score__c, Result_Freeform__c, Result_Active__c, Sailfish_Onboarding__r.Name
                FROM Sailfish_Results__c
                WHERE Sailfish_Onboarding__r.Name = :recordName And Result_Type__c = 'Ruleset' And Result_Active__c = True
                ORDER BY  Result_Definition__c desc 
               ];
    }    

    @AuraEnabled
    public static List <Sailfish_Results__c> getSearchRuleResults(String recordName) {
        return [SELECT Id, Result_Type__c, Result_Definition__c, Result_Definition_jp__c, Result_Rating__c, Result_Score__c, Result_Freeform__c, Result_Active__c, Sailfish_Onboarding__r.Name
                FROM Sailfish_Results__c
                WHERE Sailfish_Onboarding__r.Name = :recordName And Result_Type__c = 'Rule' And Result_Active__c = True
                ORDER BY  Result_Definition__c  desc  
               ];
    }    

    @AuraEnabled
    public static List <Sailfish_Results__c> getSearchDoctorResults(String recordName) {
        return [SELECT Id, Result_Type__c, Result_Definition__c, Result_Definition_jp__c, Result_Rating__c, Result_Score__c, Result_Freeform__c, Result_Active__c, Sailfish_Onboarding__r.Name
                FROM Sailfish_Results__c
                WHERE Sailfish_Onboarding__r.Name = :recordName And Result_Type__c = 'Doctor' //And Result_Active__c = True
                ORDER BY  Sailfish_Onboarding__r.Name DESC
               ];
    }    

    @AuraEnabled
    public static List<Sailfish_Results__c> getLastResults() {
        
        // Search the most recent Onboarding record  
        AggregateResult[] maxNameAggr = [SELECT MAX(Sailfish_Onboarding__r.Name)max
                                         FROM Sailfish_Results__c 
                                        ] ; 
        String maxName = (string) maxNameAggr[0].get('max') ;         
        
        // Return all results matching this record
        return [SELECT Id, Result_Definition__c, Result_Definition_jp__c, Result_Rating__c, Result_Score__c,Result_Freeform__c,Sailfish_Onboarding__r.Name
                FROM Sailfish_Results__c
                WHERE Sailfish_Onboarding__r.Name = :maxName 
                ORDER BY id ASC 
               ];
    }
}