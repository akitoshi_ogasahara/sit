@isTest
private class TestE_DownloadNoticeConst {
	
	@isTest static void testE_constAll() {
		string ERROR_MSG_NODATA = E_DownloadNoticeConst.ERROR_MSG_NODATA;
		string NOTICE_MSG_DATA0 = E_DownloadNoticeConst.NOTICE_MSG_DATA0;
		string NOTICE_MSG_DATA1000 = E_DownloadNoticeConst.NOTICE_MSG_DATA1000;
		string OBJ_API_E_AGPO = E_DownloadNoticeConst.OBJ_API_E_AGPO;
		string OBJ_API_E_BILA = E_DownloadNoticeConst.OBJ_API_E_BILA;
		string OBJ_API_E_BILS = E_DownloadNoticeConst.OBJ_API_E_BILS;
		string OBJ_API_E_NCOL = E_DownloadNoticeConst.OBJ_API_E_NCOL;
		string E_NCOLNM = E_DownloadNoticeConst.E_NCOLNM;
		string E_AGPONM = E_DownloadNoticeConst.E_AGPONM;
		string E_BILANM = E_DownloadNoticeConst.E_BILANM;
		string E_BILSNM = E_DownloadNoticeConst.E_BILSNM;
		Map<String,Integer> FORMATNO_MAP = E_DownloadNoticeConst.FORMATNO_MAP;
		string DH_FORMNO_NOTICE_AGPO = E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO;
		string BIZSYNC_KIND_NOTICE_BILS = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS;
		string BIZSYNC_KIND_NOTICE_BILA = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA;
		string BIZSYNC_KIND_NOTICE_NCOL = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL;
		string EXCLUDED = E_DownloadNoticeConst.EXCLUDED;
		Map<String,String> KIND_MAP = E_DownloadNoticeConst.KIND_MAP;
		Map<String,String> FNO_OBJNM_MAP = E_DownloadNoticeConst.FNO_OBJNM_MAP;
		string KEVNTDSC01_NONPAYMENT = E_DownloadNoticeConst.KEVNTDSC01_NONPAYMENT;
		string ZJINTSEQ_01 = E_DownloadNoticeConst.ZJINTSEQ_01;
		string KSECTION00 = E_DownloadNoticeConst.KSECTION00;
		string KSECTION88 = E_DownloadNoticeConst.KSECTION88;
	}	
}