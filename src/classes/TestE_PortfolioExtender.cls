@isTest
private class TestE_PortfolioExtender {
	/*
	 * 正常パターンテスト
	 */
	static testMethod void OKpatternTest01() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.TRCDE02__c = 'TD05';
        insert idcpf;
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
       
		// 年金受取人者
		Contact cntAnnuitant 
		= TestE_TestUtil.createContact(true, null, 'テスト年金受取人', '56789', null);
		
		
		// ファンド群マスタ
		E_FundMaster__c fms = new E_FundMaster__c(
			ZGFUNDCD__c = 'por'
			,ZGFUNDNM__c = 'テストファンド群マスタ'
		);
		insert fms;

		// 特別勘定 正常01
		E_SVCPF__c svc1_1 = new E_SVCPF__c(
			ZEFUNDCD__c = 'S1b1'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.999999 //インデックス
			,FLAG01__c = true
			,CURRFROM__c = '20141226'
		);
		insert svc1_1;
		// 特別勘定 正常01
		E_SVCPF__c svc1_2 = new E_SVCPF__c(
			ZEFUNDCD__c = 'S1b2'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.999999 //インデックス
			,FLAG01__c = true
			,CURRFROM__c = '20141226'
		);
		insert svc1_2;

		E_SVCPF__c svc2 = new E_SVCPF__c(
			ZEFUNDCD__c = 'Saa2'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.9232354324
			,FLAG01__c = false
			,CURRFROM__c = '20141226'
		);
		insert svc2;
		E_SVCPF__c svc3 = new E_SVCPF__c(
			ZEFUNDCD__c = 'Saa3'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.88888
			,FLAG01__c = true
			,CURRFROM__c = '99999999'
		);
		insert svc3;
		E_SVCPF__c svc4 = new E_SVCPF__c(
			ZEFUNDCD__c = 'Saa4'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.999999
			,FLAG01__c = true
			,CURRFROM__c = '20000101'
		);
		insert svc4;


		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, null, E_Const.POLICY_RECORDTYPE_SPVA, '12345678');
		policy.E_FundMaster__c = fms.Id;
		policy.COMM_CRTABLE__c = 'AI#2';
		policy.SPVA_ZANNSTFLG__c = true; // 年金支払到来フラグ
		policy.COMM_ZTFRDCF__c = true; //積立金移転可能フラグ				
		policy.SPVA_ZTFRDCFA__c = true; //積立金移転可能フラグ(年金内容個別照会画面用)
		policy.SPVA_ZPRDCD__c = 'S0001'; //商品コード
		insert policy;



		// 変額保険ファンド
		E_SVFPF__c varFund1_1 = new E_SVFPF__c(
			E_SVCPF__c = svc1_1.Id
			,E_Policy__c = policy.Id
		);
		insert varFund1_1;
		E_SVFPF__c varFund1_2 = new E_SVFPF__c(
			E_SVCPF__c = svc1_2.Id
			,E_Policy__c =	policy.Id
		);
		insert varFund1_2;

		// 選択可能ファンド
		E_SFGPF__c selectFund1_1 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc1_1.Id
		);
		insert selectFund1_1;
		
		E_SFGPF__c selectFund1_2 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc1_2.Id
		);
		insert selectFund1_2;

		E_SFGPF__c selectFund2 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc2.Id
		);
		insert selectFund2;
		E_SFGPF__c selectFund3 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc3.Id
		);
		insert selectFund3;
		E_SFGPF__c selectFund4 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc4.Id
		);
		insert selectFund4;


		
		// ポートフォリオ履歴
		E_SVPPF__c portHistory1_1 = new E_SVPPF__c(
			E_Policy__c = policy.Id
			,E_SVCPF__c = svc1_1.Id
			,ZREVAMT01__c = 1000
			,ZREVAMT02__c = 1000
			,ZREVAMT03__c = 1000
			,ZREVAMT04__c = 1000
			,ZREVAMT05__c = 1000
			,ZREVAMT06__c = 1000	
			);
		insert portHistory1_1;

		E_SVPPF__c portHistory1_2 = new E_SVPPF__c(
			E_Policy__c = policy.Id
			,E_SVCPF__c = svc1_2.Id
			,ZREVAMT01__c = 9999
			,ZREVAMT02__c = null
			,ZREVAMT03__c = 0
			,ZREVAMT04__c = 1000
			,ZREVAMT05__c = 1000
			,ZREVAMT06__c = 1000	
			);
		insert portHistory1_2;

		//結果画面
		PageReference resultPage;	
		// テストユーザで機能実行開始
		System.runAs(u){		
			//テスト開始
			Test.startTest();
			PageReference pref = Page.E_Portfolio;
			pref.getParameters().put('type', E_Const.FROM_SPVA);
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_PortfolioController controller = new E_PortfolioController(standardcontroller);
			E_PortfolioExtender extender = controller.getExtender();
			extender.init();
			extender.getIsMizuhoBank();
			//ページアクションの戻り値
			resultPage = extender.PageAction();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		System.assertEquals(null, resultPage);
	}


	/*
	 * 正常パターンテスト
	 */
	static testMethod void OKpatternTest02() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.TRCDE02__c = 'TD05';
        insert idcpf;
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        
		// 契約者
		Contact cntKeiyaku 
		= TestE_TestUtil.createContact(true, null, 'テスト契約者', '12345', null);

		// 年金受取人者
		Contact cntAnnuitant 
		= TestE_TestUtil.createContact(true, null, 'テスト年金受取人', '56789', null);
		
		
		// ファンド群マスタ
		E_FundMaster__c fms = new E_FundMaster__c(
			ZGFUNDCD__c = 'por'
			,ZGFUNDNM__c = 'テストファンド群マスタ'
		);
		insert fms;

		// 特別勘定 正常01
		E_SVCPF__c svc1_1 = new E_SVCPF__c(
			ZEFUNDCD__c = 'S1b1'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.999999 //インデックス
			,FLAG01__c = true
			,CURRFROM__c = '20141226'
		);
		insert svc1_1;
		// 特別勘定 正常01
		E_SVCPF__c svc1_2 = new E_SVCPF__c(
			ZEFUNDCD__c = 'S1b2'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.999999 //インデックス
			,FLAG01__c = true
			,CURRFROM__c = '20141226'
		);
		insert svc1_2;

		E_SVCPF__c svc2 = new E_SVCPF__c(
			ZEFUNDCD__c = 'Saa2'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.9232354324
			,FLAG01__c = false
			,CURRFROM__c = '20141226'
		);
		insert svc2;
		E_SVCPF__c svc3 = new E_SVCPF__c(
			ZEFUNDCD__c = 'Saa3'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.88888
			,FLAG01__c = true
			,CURRFROM__c = '99999999'
		);
		insert svc3;
		E_SVCPF__c svc4 = new E_SVCPF__c(
			ZEFUNDCD__c = 'Saa4'
			,ZFUNDNAM01__c = 'ハイイールド債'
			,ZFUNDNAM02__c = '投機的格付債'
			,ZFUNDNAM03__c = 'ジャンク債'
			,ZFUNDNAM04__c = 'ジャンクボンド'
			,ZEQTYRTO__c = 11
			,ZVINDEX__c = 11.999999
			,FLAG01__c = true
			,CURRFROM__c = '20000101'
		);
		insert svc4;


		//保険契約ヘッダ
		E_Policy__c policy = TestE_TestUtil.createPolicy(false, cntKeiyaku.id, E_Const.POLICY_RECORDTYPE_SPVA, '12345678');
		policy.E_FundMaster__c = fms.Id;
		policy.COMM_CRTABLE__c = 'SU#1';
		policy.SPVA_ZANNSTFLG__c = false; // 年金支払到来フラグ
		policy.COMM_ZTFRDCF__c = true; //積立金移転可能フラグ				
		policy.SPVA_ZTFRDCFA__c = true; //積立金移転可能フラグ(年金内容個別照会画面用)
		policy.SPVA_ZPRDCD__c = 'S0013'; //商品コード
		insert policy;



		// 変額保険ファンド
		E_SVFPF__c varFund1_1 = new E_SVFPF__c(
			E_SVCPF__c = svc1_1.Id
			,E_Policy__c = policy.Id
		);
		insert varFund1_1;
		E_SVFPF__c varFund1_2 = new E_SVFPF__c(
			E_SVCPF__c = svc1_2.Id
			,E_Policy__c =	policy.Id
		);
		insert varFund1_2;

		// 選択可能ファンド
		E_SFGPF__c selectFund1_1 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc1_1.Id
		);
		insert selectFund1_1;
		
		E_SFGPF__c selectFund1_2 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc1_2.Id
		);
		insert selectFund1_2;

		E_SFGPF__c selectFund2 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc2.Id
		);
		insert selectFund2;
		E_SFGPF__c selectFund3 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc3.Id
		);
		insert selectFund3;
		E_SFGPF__c selectFund4 = new E_SFGPF__c(
			FundMaster__c = fms.Id
			,E_SVCPF__c = svc4.Id
		);
		insert selectFund4;


		
		// ポートフォリオ履歴
		E_SVPPF__c portHistory1_1 = new E_SVPPF__c(
			E_Policy__c = policy.Id
			,E_SVCPF__c = svc1_1.Id	
			);
		insert portHistory1_1;

		E_SVPPF__c portHistory1_2 = new E_SVPPF__c(
			E_Policy__c = policy.Id
			,E_SVCPF__c = svc1_2.Id	
			);
		insert portHistory1_2;

		//結果画面
		PageReference resultPage;
		
		// テストユーザで機能実行開始
		System.runAs(u){
			
			//テスト開始
			Test.startTest();
			PageReference pref = Page.E_Portfolio;
			pref.getParameters().put('type', E_Const.FROM_SPVA);
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_PortfolioController controller = new E_PortfolioController(standardcontroller);
			E_PortfolioExtender extender = controller.getExtender();
			extender.init();
			//ページアクションの戻り値
			resultPage = extender.PageAction();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		System.assertEquals(null, resultPage);
		
	}
}