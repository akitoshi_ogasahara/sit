public with sharing class DS_SearchController extends DS_AbstractController {

	// 大分類_傷病リストMap
	private Map<String, List<E_Diseases__c>> diseasesByCategory;
	// 傷病名保持Map
	private Map<String, String> diseaseMap;

	// 入力されるキーワード格納用
	public String searchWord {get; set;}
	// パラメータで受け取る傷病Noを格納する変数
	public String pNumber {get; set;}
	// 選択リストの選択値：傷病No
	public String selectedDisease {get; set;}
	// 傷病No情報表示用オブジェクト
	public E_DiseaseNumber diseaseNumber {get; private set;}
	// キーワード検索結果件数表示用
	public Integer searchCount {get; private set;}
	// 保険種類引受条件テーブル
	public List<String> rowList {get; set;}
	// 傷病名連結用変数
	public String diseaseNamegroup {get; private set;}
	// 傷病名かたかな連結用変数
	public String diseaseNameKanaGroup {get; private set;}
	// 分類検索で選択された分類
	public String selectedCategory {get; set;}
	// キーワード検索結果 クリックされたページ番号格納
	public Integer clickPageNumber {get; set;}
	// キーワード検索結果 スタンダードコントローラー
	public ApexPages.StandardSetController setCon {get; set;}


	// 選択リスト 分類検索_分類
	public List<SelectOption> diseaseCategoryOptions{
		get{
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', '---分類を選択してください---'));
			for (String category: diseasesByCategory.keyset()) {
				options.add(new SelectOption(category, category));
			}
			return options;
		}
		set;
	}

	// 選択リスト 分類検索_傷病
	public List<SelectOption> diseaseOptions {
		get{
			diseaseMap = new Map<String, String>();
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', '---傷病を選択してください---'));
			if (diseasesByCategory.containsKey(selectedCategory)) {
				for (E_Diseases__c record: diseasesByCategory.get(selectedCategory)) {
					//options.add(new SelectOption(record.DiseaseNumber__r.Name, record.Name));
					options.add(new SelectOption(record.Name, record.Name));
					diseaseMap.put(record.Name, record.DiseaseNumber__r.Name);
				}
			}
			return options;
		}
		set;
	}

	// ページ名
	public String getPageName(){
		return DS_Const.APPNAME;
	}

	/**
	 * Constructor
	 */
	public DS_SearchController() {
		searchCount = 0;
		pNumber = '';
		diseasesByCategory = createDiseasesMap();
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init() {
		// チェック処理
		PageReference pr = super.init();

		// 一覧画面から遷移してきた場合パラメータで傷病Noを受け取って引受条件、傷病Noマスタの項目を画面に表示
		pNumber = ApexPages.currentPage().getParameters().get(DS_Const.DISEASE_URL_PARAM_NUMBER);
		if(String.isNotBlank(pNumber)){
			setDisplayNumberData();
		}

		// アクセスログの登録
		logPageAccess();

		return pr;
	}

	// 注意事項ページ遷移用 param_Name
	public String getPageKeyParam(){
		return I_Const.URL_PARAM_PAGEKEY;	//?pgKey=
	}

	// 注意事項ページ遷移用 param_Value
	public String getPageKey(){
		if(getIsSumiseiUser()||access.canViewSumiseiIRIS()){
			return DS_Const.PAGE_UNIQUE_KEY_SUMISEI;	//注意事項（IRISページ）のページユニークキー
		}else{
			return DS_Const.PAGE_UNIQUE_KEY_NORMAL;	//注意事項（IRISページ）のページユニークキー
		}
	}

	/**
	 * キーワード検索
	 */
	public PageReference searchDiseaseName() {
		String soqlWhere = '';
		// キーワード検索時、引受条件や告知情報、エラーメッセージが残っている場合削除
		pageMessages.clearMessages();
		diseaseNamegroup = '';
		diseaseNameKanaGroup = '';
		diseaseNumber = null;	// new E_DiseaseNumber__c();
		selectedCategory = null;
		selectedDisease = '';

		if(String.isNotBlank(searchWord)){
			// キーワードから全角・半角スペースを取り除いてリストに追加
			String trimedSearchWord = searchWord.normalizeSpace();
			List<String> keyList = new List<String>();
			keyList = trimedSearchWord.split(' ');
			Integer listSize = keyList.size();
			// E_SoqlManagerでWhere句作成
			for(Integer i = 0; i < listSize; i++){
				String likeWordSearch = keyList[i];
				// 検索キーワードが半角カナの場合全角に変換する
				if(E_Util.isEnKatakana(likeWordSearch)){
					likeWordSearch = E_Util.enKatakanaToEm(likeWordSearch);
				}
				likeWordSearch = '%' + String.escapeSingleQuotes(likeWordSearch) + '%';
				E_SoqlManager manager = new E_SoqlManager();
				manager.addString(manager.pStart);
				manager.addOrWhere('Name', likeWordSearch, manager.co);
				manager.addOrWhere('Hiragana__c', likeWordSearch, manager.co);
				manager.addOrWhere('Katakana__c', likeWordSearch, manager.co);
				manager.addString(manager.pEnd);
				// 2週目以降はWhereを削除
				if(i > 0){
					soqlWhere += manager.getWhere().remove('Where ');
				}else{
					soqlWhere += manager.getWhere();
				}
				soqlWhere += ' AND ';
			}
			soqlWhere += 'DiseaseNumber__c != null ORDER BY Katakana__c';
			List<E_Diseases__c> searchedList = E_DiseasesDao.getDiseaseRecs(soqlWhere);
			searchCount = searchedList.size();
			if(searchCount > 0){
				// 検索結果ページネーション用にスタンダードコントローラーに検索結果を渡す
				setCon = new ApexPages.StandardSetController(searchedList);
				setCon.setPageSize(DS_Const.PAGE_SIZE);
			}else{
				// 検索結果がない場合、検索ワードのログを登録する
				logDisease('キーワード');
				// 検索結果が0件だった場合はエラーメッセージを出す
				pageMessages.addErrorMessage(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));	//IRIS|SEARCH|001
				return null;
			}

		// キーワードが空の場合はエラーメッセージを出して検索結果をリセット
		}else{
			pageMessages.addErrorMessage(getMSG().get(I_Const.MESSAGE_KEY_KEYWORD_NOTHING));	//'IRIS|SEARCH|003'
			searchCount = 0;
			return null;
		}
		return null;
	}

	/**
	 * キーワード検索結果
	 * 		表示するレコードを取得
	 */
	public List<E_Diseases__c> getRecs(){
		if(setCon != null){
			return (List<E_Diseases__c>)setCon.getRecords();
		}
		return new List<E_Diseases__c>();
	}

	/**
	 * キーワード検索結果
	 * 		指定のページへ移動
	 */
	public void doPageNumber(){
		setCon.setPageNumber(clickPageNumber);
	}

	/**
	 * キーワード検索結果
	 * 		総ページ数取得
	 */
	public Integer getTotalPages(){
		Decimal pages = Decimal.valueOf(setCon.getResultSize()) / Decimal.valueOf(setCon.getPageSize());
		return Integer.valueOf(pages.round(System.RoundingMode.CEILING));
	}

	/**
	 * キーワード検索結果
	 * 		ページ番号のリンクを取得
	 */
	public List<Integer> getPageNumberLinks(){
		Integer startPageNumber = setCon.getPageNumber() - DS_Const.NUMBER_OF_LINKS;
		if(startPageNumber < 1){
			startPageNumber = 1;
		}
		Integer endPageNumber = setCon.getPageNumber() + DS_Const.NUMBER_OF_LINKS;
		if(endPageNumber > getTotalPages()){
			endPageNumber = getTotalPages();
		}
		List<Integer> links = new List<Integer>();
		for(Integer i = startPageNumber; i <= endPageNumber; i++){
			links.add(i);
		}
		return links;
	}

	/**
	 * キーワード検索結果リンククリック
	 */
	public void doResultSearchKeyword(){
		// 傷病Noより、傷病情報を生成
		displayDiseaseNumberData();
		// ログ登録
		logDisease(DS_Const.ROUTE_KEYWORD);
	}

	/**
	 * 傷病結果選択リスト選択
	 */
	public void doResultDisease(){
		// 直前にキーワード検索をしていた場合はキーワード、検索結果削除
		if(String.isNotBlank(searchWord)){
			searchWord = '';
			searchCount = 0;
			setCon = null;
		}
		// 傷病Noに選択された傷病Noをセット
		pNumber = diseaseMap.get(selectedDisease);
		// 傷病Noより、傷病情報を生成
		displayDiseaseNumberData();
		// ログ登録
		logDisease(DS_Const.ROUTE_CATEGORY);
	}

	/**
	 * 表示用の傷病情報作成
	 */
	public Pagereference displayDiseaseNumberData() {
		pageMessages.clearMessages();
		setDisplayNumberData();
		return null;
	}

	/**
	 * 選択された分類に関連する傷病を取得
	 */
	private void setDisplayNumberData() {
		//diseaseNumber = E_DiseaseNumberDao.getDiseaseNumber(pNumber);
		diseaseNumber = new E_DiseaseNumber(E_DiseaseNumberDao.getDiseaseNumber(pNumber));
		if(diseaseNumber.record == null || diseaseNumber.record.Id == null){
			return;
		}
		createNameGroups(diseaseNumber.record);

		// 保険種類がkeyでE_InsType_UnderWriting__cのリストがvalueのマップ作成
		Map<String, List<E_InsType_UnderWriting>> underWritingMap = new Map<String, List<E_InsType_UnderWriting>>();
		for(E_InsType_UnderWriting uw : diseaseNumber.insTypeUWs){
			String key = uw.record.InsuranceType__c;
			if(underWritingMap.containsKey(key)){
				underWritingMap.get(key).add(uw);
			}else{
				underWritingMap.put(key, new List<E_InsType_UnderWriting>());
				underWritingMap.get(key).add(uw);
			}
		}
		// 作成したMapのkey分内部クラスにわたす
		rowList = new List<String>();
		for(Schema.PicklistEntry ple : E_InsType_UnderWriting__c.InsuranceType__c.getDescribe().getPicklistValues()){
			String insuranceType = ple.getLabel();
			// 保険種類別引受条件
			if(underWritingMap.containsKey(insuranceType)){
				String insuranceTypeDisplay = underWritingMap.get(insuranceType)[0].insuranceTypeDisplay;
				if(String.isBlank(insuranceTypeDisplay)){
					continue;
				}
				List<String> blockLines = new List<String>();
				blockLines.add('<div class="row row-eq-height ins-uw-row">');
				blockLines.add('<div class="instype-title col-md-3 col-xs-12">' + insuranceTypeDisplay + '</div>');
				blockLines.add('<div class="col-md-9 col-xs-12" style="padding: 0px!important;">');

				for (E_InsType_UnderWriting ituw : underWritingMap.get(insuranceType)) {
					blockLines.add('<div class="col-md-12 col-xs-12 underwrite-row" style="display: flex;">');
					if (String.isNotBlank(ituw.record.TreatmentStatus__c) && String.isNotBlank(ituw.record.Proviso__c)) {
						// 状況と但し書きに値がある場合
						blockLines.add('<div class="col-md-2 col-xs-2 instype-colum">' + ituw.record.TreatmentStatus__c + '</div>');
						//blockLines.add('<div class="col-md-5 col-xs-5 instype-colum">' + ituw.JudgementDisplay__c + '</div>');
						blockLines.add('<div class="col-md-5 col-xs-5 instype-colum">' + ituw.JudgementDisplay + '</div>');
						blockLines.add('<div class="col-md-5 col-xs-5 instype-colum">' + ituw.record.Proviso__c + '</div>');
					} else if (String.isNotBlank(ituw.record.TreatmentStatus__c)) {
						// 状況の値があり、但し書きの値がない場合
						blockLines.add('<div class="col-md-2 col-xs-2 instype-colum">' + ituw.record.TreatmentStatus__c + '</div>');
						//blockLines.add('<div class="col-md-10 col-xs-10 instype-colum">' + ituw.JudgementDisplay__c + '</div>');
						blockLines.add('<div class="col-md-10 col-xs-10 instype-colum">' + ituw.JudgementDisplay + '</div>');
					} else if(String.isNotBlank(ituw.record.Proviso__c)) {
						// 状況の値がなく、但し書きの値がある場合
						//blockLines.add('<div class="col-md-7 col-xs-7 instype-colum">' + ituw.JudgementDisplay__c + '</div>');
						blockLines.add('<div class="col-md-7 col-xs-7 instype-colum">' + ituw.JudgementDisplay + '</div>');
						blockLines.add('<div class="col-md-5 col-xs-5 instype-colum">' + ituw.record.Proviso__c + '</div>');
					} else {
						// 状況と但し書きの値がない場合
						//blockLines.add('<div class="col-md-12 col-xs-12 instype-colum">' + ituw.JudgementDisplay__c + '</div>');
						blockLines.add('<div class="col-md-12 col-xs-12 instype-colum">' + ituw.JudgementDisplay + '</div>');
					}
					blockLines.add('</div>');
				}

				blockLines.add('</div>');
				blockLines.add('</div>');
				rowList.add(String.join(blockLines, ''));
			}
		}
	}

	/**
	 * Noに紐づく傷病名カナを連結させる
	 */
	private void createNameGroups(E_DiseaseNumber__c numberRec) {
		diseaseNamegroup = '';
		diseaseNameKanaGroup = '';
		List<String> diseaseNameGroupList = new List<String>();
		List<String> diseaseNameKanaGroupList = new List<String>();
		if(!numberRec.Diseases__r.isEmpty()){
			for(E_Diseases__c dis : numberRec.Diseases__r){
				diseaseNameGroupList.add(dis.Name);
				diseaseNameKanaGroupList.add(dis.Katakana__c);
			}
			diseaseNamegroup = String.join(diseaseNameGroupList, DS_Const.GROUP_PUNCTUATION);
			diseaseNameKanaGroup = String.join(diseaseNameKanaGroupList, DS_Const.GROUP_PUNCTUATION);
		}
	}

	/**
	 * 大分類-傷病Map
	 */
	private static Map<String, List<E_Diseases__c>> createDiseasesMap() {
		Map<String, List<E_Diseases__c>> diseasesByCategory = new Map<String, List<E_Diseases__c>>();
		for (E_Diseases__c rec: E_DiseasesDao.getDiseaseRecs('WHERE DiseaseNumber__c != null ORDER BY DiseaseNumber__r.CategoryNumber__c, Katakana__c')) {
			String categoryName = rec.DiseaseNumber__r.Category__c;
			if (!diseasesByCategory.containsKey(categoryName)) {
				diseasesByCategory.put(categoryName, new List<E_Diseases__c>());
			}
			diseasesByCategory.get(categoryName).add(rec);
		}
		return diseasesByCategory;
	}

	/**
	 * アクセスログ_初期遷移
	 */
	private void logPageAccess(){
		// E_Log__c.Name
		String name = DS_Const.PAGE_NAME_DS_SEARCH;		// 『医的な引受の目安』に変更可能か
		String detail = '';
		if(String.isNotBlank(pNumber)){
			logDisease(DS_Const.ROUTE_INDEX);
		}else{
			// ログの登録
			createDSSearchLog(name, detail);
		}
	}

	/**
	 * レポート表示用ログ登録
	 */
	private void logDisease(String route){

		String name = route;
		String detail = '';

		// route == 'キーワード' の場合のみ
		if(String.isNotBlank(searchWord)){
			detail += searchWord;
		}else{
			detail += DS_Const.HYPHEN;
		}

		// 選択した傷病の有/無（分類検索の傷病選択、キーワード検索結果押下）
		if(String.isNotBlank(selectedDisease)){
			detail += DS_Const.COMMA + selectedDisease;
		}else{
			detail += DS_Const.COMMA + DS_Const.HYPHEN;
		}

		if(diseaseNumber != null && diseaseNumber.record != null && diseaseNumber.record.Id != null){
			detail += DS_Const.COMMA + diseaseNumber.record.Category__c + DS_Const.COMMA + diseaseNumber.record.Name + DS_Const.COMMA + diseaseNamegroup;
		}else{
			//　キーワード検索結果なしの場合
			detail += DS_Const.COMMA + DS_Const.HYPHEN + DS_Const.COMMA + DS_Const.HYPHEN + DS_Const.COMMA + DS_Const.HYPHEN;
		}

		// ログの登録
		createDSSearchLog(name, detail);
	}

	/**
	 * アクセスログ
	 */
	private void createDSSearchLog(String name, String detail){
		pageAccessLog = E_LogUtil.createLog(getAppName());
		pageAccessLog.Name = name;
		pageAccessLog.Detail__c = detail;
		pageAccessLog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_PAGE;		//ActionTypeに『ページ』を設定
		pageAccessLog.AccessPage__c = ApexPages.currentPage().getUrl().split('\\?')[0];
		if(String.isNotBlank(pNumber)){
			pageAccessLog.AccessPage__c += '?p=' + pNumber;
		}
		// Abstract
		createDSLog();
	}

	/**
	 * ダウンロード履歴の傷病No設定
	 */
	protected override E_DownloadCondition.DiseaseNumberInfo createDLCondition(){
		// E_DownloadConditionの生成
		E_DownloadCondition.DiseaseNumberInfo dlCondi= new E_DownloadCondition.DiseaseNumberInfo();
		if(getCanViewSumiseiIRIS()){
			dlCondi.csvType = DS_Const.FILETYPE_SUMISEI;
		}else{
			dlCondi.csvType = DS_Const.FILETYPE_NORMAL;
		}
		// 傷病表示時の場合
		dlCondi.diseaseNumber = pNumber;

		return dlCondi;
	}

	public class E_DiseaseNumber{
		public E_DiseaseNumber__c record {get; set;}
		public String documentDisplay {get; set;}
		public String informationDisplay {get; set;}
		public String noticeDisplay {get; set;}
		public List<E_InsType_UnderWriting> insTypeUWs {get; set;}

		public E_DiseaseNumber(E_DiseaseNumber__c record){
			if(record == null){
				this.record = new E_DiseaseNumber__c();
				return;
			}
			E_AccessController access = E_AccessController.getInstance();

			this.record = record;
			if(access.canViewSumiseiIRIS()){
				this.documentDisplay = String.isBlank(record.DocumentSumisei__c) ? null :record.DocumentSumisei__c.replace('E_DSSearch?', 'E_DSSearch?isSMTM=1&');
				this.informationDisplay = String.isBlank(record.InformationSumisei__c) ? null :record.InformationSumisei__c.replace('E_DSSearch?', 'E_DSSearch?isSMTM=1&');
				this.noticeDisplay = String.isBlank(record.NoticeSumisei__c) ? null :record.NoticeSumisei__c.replace('E_DSSearch?', 'E_DSSearch?isSMTM=1&');
			}else{
				this.documentDisplay = record.Document__c;
				this.informationDisplay = record.Information__c;
				this.noticeDisplay = record.Notice__c;
			}

			this.insTypeUWs = new List<E_InsType_UnderWriting>();
			for(E_InsType_UnderWriting__c insType : record.InsTypeUWs__r){
				this.insTypeUWs.add(new E_InsType_UnderWriting(insType));
			}
		}
	}

	public class E_InsType_UnderWriting{
		public E_InsType_UnderWriting__c record {get; set;}
		public String judgementDisplay {get; set;}
		public String insuranceTypeDisplay {get; set;}

		public E_InsType_UnderWriting(E_InsType_UnderWriting__c record){
			E_AccessController access = E_AccessController.getInstance();

			this.record = record;
			if(access.canViewSumiseiIRIS()){
				this.insuranceTypeDisplay = record.InsuranceTypeSumisei__c;
				this.judgementDisplay = record.JudgementSumisei__c;

				if(String.isBlank(this.judgementDisplay)) return;

				if(access.isEmployee()){
					this.judgementDisplay = this.judgementDisplay.replaceAll('#', '&isSMTM=1#');
					if(!String.isBlank(this.record.Proviso__c)){
						this.record.Proviso__c = this.record.Proviso__c.replaceAll('#', '&isSMTM=1#');
					}
				}
			}else{
				this.insuranceTypeDisplay = record.InsuranceType__c;
				this.judgementDisplay = record.Judgement__c;
			}
		}
	}
}