@isTest
private class TestE_AbstractController
{
	/**
	 * E_AbstractControllerの網羅テスト
	 */
	private static testMethod void testAbstractController001() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

        // Ebiz連携ログ(E_BizDataSyncLog__c)
        E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;
		E_DownloadController controller;
		E_SelfRegistrationController selfCon;
		//E_ProgressSearchBankSVEExtender extender;

		System.runAs(u){
			Test.startTest();
			//E_MPQPF__c mpqpf = new E_MPQPF__c();
			//PageReference pref = Page.E_ProgressSearchBankSVE;
			//Test.setCurrentPage(pref);

        	controller = new E_DownloadController();
			selfCon = new E_SelfRegistrationController();
            //extender = new E_ProgressSearchBankSVEExtender(controller);
			controller.getMSG();
			controller.getDISCLAIMER();
			controller.getPageTitle();
			controller.getDataSyncDate();
			controller.getDataSyncDateInvestment();
			controller.getDataSyncDateInvestment();
			controller.getDataSyncDateDownload();
			controller.isValidate();
			controller.getCanDisplayFeeGuidance();
			controller.getCanDisplayFeeData();
			controller.getCanDisplayHoldPolicyColi();
			controller.getCanDisplayHoldPolicySpva();
			controller.getCanDisplayPolicyHisotry();
			controller.getIsCommonGateway();
			controller.getAccess();
			controller.getIsSumiseiUser();
			controller.getCanViewSumiseiIRIS();
			controller.getSumiseiIRISParam();
			controller.getCanAccessAtria();
			//controller.getCanAccessAtria(); //20180420 リリース用コメントアウト

			selfCon.doReturn();
			selfCon.isValidate();

			//doAuthのテスト
			controller.doAuth(null,null);
			controller.doAuth(E_Const.ID_KIND.COMPLIANCE,null);
			controller.doAuth(E_Const.ID_KIND.DOWNLOAD,null);
			controller.doAuth(E_Const.ID_KIND.CUSTOMER_SEARCH,null);

			System.assertNotEquals(controller.getWelcome(),Null);

			Test.stopTest();
		}

	}

	//ID管理無し
	private static testMethod void testAbstractController002() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');

		// Ebiz連携ログ(E_BizDataSyncLog__c)
		E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);

		PageReference resultPage;
		E_DownloadController controller;
		E_SelfRegistrationController selfCon;

		System.runAs(u){
			Test.startTest();
			controller = new E_DownloadController();

			controller.getCanDisplayFeeGuidance();
			controller.getCanDisplayFeeData();
			controller.getCanDisplayHoldPolicyColi();
			controller.getCanDisplayHoldPolicySpva();
			controller.doAuth(E_Const.ID_KIND.CUSTOMER_SEARCH,null);
			controller.doAuth(E_Const.ID_KIND.DOWNLOAD,null);

			System.assertNotEquals(controller.getWelcome(),Null);

			Test.stopTest();
		}
	}

	//bizデータなし
	private static testMethod void testAbstractController003() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		// Ebiz連携ログ(E_BizDataSyncLog__c)
		E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
		log.DataSyncEndDate__c = null;
		update log;

		PageReference resultPage;
		E_DownloadController controller;
		E_SelfRegistrationController selfCon;

		System.runAs(u){
			Test.startTest();
			controller = new E_DownloadController();

			resultPage = controller.doAuth(E_Const.ID_KIND.CUSTOMER_SEARCH,null);
			String encoded = EncodingUtil.urlEncode('|', 'UTF-8');
			String param = E_Const.ERROR_MSG_SERVICEHOUR.replace('|',encoded);
			System.assertEquals(resultPage.getUrl(),Page.E_ErrorPage.getUrl() + '?code=' + param);

			Test.stopTest();
		}
	}

	//代理店ユーザ--コンプライアンス遷移
	private static testMethod void testAbstractController004() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User u;
		System.runAs(thisUser){
			//代理店
			Account accDis = TestE_TestUtil.createAccount(true);
			//募集人
			Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
			//ユーザ
			u = createUser(conCust);
			//ID管理
			E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '1';
			idcpf.FLAG02__c = '1';
			idcpf.FLAG04__c = '1';
			idcpf.AppMode__c = 'IRIS';
			insert idcpf;

			//レコードの共有を作成
			TestI_TestUtil.createAccountShare(accDis.Id, u.Id);

			// Ebiz連携ログ(E_BizDataSyncLog__c)
			E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
	}

		PageReference resultPage;
		E_DownloadController controller;

		System.runAs(u){
			Test.startTest();
			controller = new E_DownloadController();

			resultPage = controller.doAuth(null,null);
			System.assertEquals(resultPage.getUrl(),Page.E_Compliance.getUrl());
			Test.stopTest();
		}
	}

	/*
	* 観点:doAuth 銀行代理店ユーザ エラーページへ
	* 条件:IPアドレスチェックフラグ true の代理店に所属する募集人でアクセスする
	* 検証:ERROR_MSG_IPADDRESSが付与され、エラーページに遷移する
	*/
	private static testMethod void testAbstractController005() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User u;
		System.runAs(thisUser){
			//代理店
			Account parentAcc = TestE_TestUtil.createAccount(false);
			parentAcc.E_CheckIPFlg__c = true; //IPアドレスの確認対象とする
			insert parentAcc;
			//事務所
			Account accDis = new Account(Name = 'testOffice', parentId = parentAcc.Id );
			insert accDis;
			//募集人
			Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
			//ユーザ
			u = createUser(conCust);
			//ID管理
			E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '1';
			idcpf.FLAG02__c = '1';
			idcpf.FLAG04__c = '1';
			idcpf.AppMode__c = 'IRIS';
			insert idcpf;
			//レコードの共有を作成
			TestI_TestUtil.createAccountShare(accDis.Id, u.Id);
	}

		PageReference resultPage;
		E_DownloadController controller;

		System.runAs(u){
			Test.startTest();
			controller = new E_DownloadController();

			resultPage = controller.doAuth(null,null);
			String encoded = EncodingUtil.urlEncode('|', 'UTF-8');
			String param =  E_Const.ERROR_MSG_IPADDRESS.replace('|',encoded);
			System.assertEquals(Page.E_ErrorPage.getUrl() + '?code=' + param , resultPage.getUrl());
			Test.stopTest();
		}
	}

	//住友代理店ユーザ--パスワードステータスチェック
	private static testMethod void testAbstractController006() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User u;
		System.runAs(thisUser){
			//代理店
			Account acc = TestI_TestUtil.createSumiseiParentAccount();
			//募集人
			Contact conCust = TestI_TestUtil.createSumiseiContact(acc.Id, 'testsan', true);
			//ユーザ
			u = createUser(conCust);
			//ID管理
			E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
			idcpf.FLAG01__c = '1';
			idcpf.FLAG02__c = '1';
			idcpf.FLAG04__c = '1';
			idcpf.AppMode__c = 'IRIS';
			idcpf.ZSTATUS01__c = '2';
			insert idcpf;

			//レコードの共有を作成
			TestI_TestUtil.createAccountShare(acc.Id, u.Id);

			// Ebiz連携ログ(E_BizDataSyncLog__c)
			E_bizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
		}

		PageReference resultPage;
		E_DownloadController controller;

		System.runAs(u){
			Test.startTest();
			controller = new E_DownloadController();

			resultPage = controller.doAuth(null,null);
			String encoded = EncodingUtil.urlEncode('|', 'UTF-8');
			String param =  E_Const.ERROR_MSG_ACTIVEUSER.replace('|',encoded);
			System.assertEquals(Page.E_ErrorPage.getUrl() + '?code=' + param , resultPage.getUrl());
			Test.stopTest();
		}
	}

	static User createUser(Contact con){
		Profile profile = [Select Id, Name,usertype From Profile Where Name = 'E_PartnerCommunity'];
		User us = new User(
				Lastname = 'fstest'
				, FirstName = 'firstName'
				, Username = 'fstest@terrasky.ingtesting'+ System.Label.E_USERNAME_SUFFIX
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
				, contactID = con.id
		);
		insert us;
		return us;
	}

}