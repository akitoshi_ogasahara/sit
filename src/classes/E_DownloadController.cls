public with sharing class E_DownloadController extends E_AbstractController {
	
	//代理店格
	public Account distributor {get;set;}
	//代理店事務所
	public Account office {get;set;}
	//募集人
	public Contact agent {get;set;}
	
	//ダウンロード機能共通変数
	public final String KEY_DID = 'did';
	public final String KEY_OID = 'oid';
	public final String KEY_AID = 'aid';
	public final String KEY_PT = 'pt';
	public final String VAL_COLI = 'coli';
	public final String VAL_SPVA  = 'spva';
	public final String VAL_BONUS = 'bospf';
	
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// Referer
	public String retUrl {get; set;}

	public class DLException extends Exception {}
	
	//社員ユーザか？
	public boolean getIsEmployee() {
		return access.isEmployee();
	}
	
	/**
	 * 新契約ダウンロードボタン表示制御フラグ取得
	 */
	public Boolean getIsViewNewPolicyDL() {
		String zheaday = '';

		// 社員ユーザの場合、検索で住生関連のデータが選択されたときのみ表示
		if(access.isEmployee()){
			// 代理店格コードセット
			if(distributor != null){
				zheaday = distributor.E_CL1PF_ZHEADAY__c;
			}else if(office != null){
				zheaday = office.E_ParentZHEADAY__c;
			}else if(agent != null){
				zheaday = agent.E_AccParentCord__c;
			}
			return zheaday == E_Const.ZHEADAY_SUMISEI ? true : false;
			
		// 代理店ユーザの場合、住生ユーザのみ表示
		}else if(access.isAgent()){
			return access.isSumiseiUser();
			
		// それ以外
		}else{
			return false;
		}	
	}
	
	//ページタイトルを設定
	public void setPgTitle(String title){
		this.pgTitle = title;
	}
	
	//コンストラクタ
	public E_DownloadController() {
		// Referer をセット
		retUrl = ApexPages.currentPage().getHeaders().get('referer');
		pageMessages = getPageMessages();
	}
	
	//ページアクション
	public PageReference init(){
		return doAuth(E_Const.ID_KIND.DOWNLOAD, null);
	}
	
	//ダウンロード履歴
	public E_DownloadHistorry__c dh; 
	
	//OPRO呼び出しURL生成
	public string getDownloadId(){
		if(dh == null || dh.Id == null){
			return '';
		}
		return 
			dh.Id;
	}
	
	//サポートブラウザ（IE8は非サポート）判定
	public boolean getIsSupportBrowser() {
		return !E_Util.isUserAgentIE8();
	}
	
	//ダウンロード履歴帳票種別
	public String formNo {get; set;}
	
	//パラメータによる実行者情報取得
	public boolean getActor(){
		if(getIsEmployee()){
			String did = ApexPages.currentPage().getParameters().get(KEY_DID);
			String oid = ApexPages.currentPage().getParameters().get(KEY_OID);
			String aid = ApexPages.currentPage().getParameters().get(KEY_AID);
			//代理店取得
			try{
				if(String.isNotBlank(did)){
					Id distributorId = Id.valueOf(did);
					distributor = E_AccountDao.getRecById(distributorId);
				//代理店事務所取得
				}else if(String.isNotBlank(oid)){
					Id officeId = Id.valueOf(oid);
					office = E_AccountDao.getRecById(officeId);
				//募集人取得
				}else if(String.isNotBlank(aid)){
					Id agentId = Id.valueOf(aid);
					agent = E_ContactDao.getRecById(agentId);
				//パラメータ不正
				}else{
					throw new DLException();
				}
			}catch(exception e){
				errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
				return false;
			}
		}else{
			getActor_isAgent();
		}
		if(pageRef == null){
			//パラメータから取得したレコード存在チェック
			if(distributor != null || office != null || agent != null){
				return true;
			}
			errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
		}
		return false;
	}
	
	public void getActor_isAgent(){
		// IRIS フィルタ適用中の場合
		if(iris.getIsIRIS() && iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				// 代理店フィルタ
				if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					Id distributorId = Id.valueOf(conditionValue);
					distributor = E_AccountDaoWithout.getRecById(distributorId);
				// 事務所フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					Id officeId = Id.valueOf(conditionValue);
					office = E_AccountDaoWithout.getRecById(officeId);
				// 募集人フィルタ
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					Id agentId = Id.valueOf(conditionValue);
					agent = E_ContactDaoWithout.getRecByIdWithout(agentId);
				//パラメータ不正
				}else{
					//上記以外の時
					errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
					pageRef = Page.E_ErrorPage;
					pageRef.getParameters().put('code', E_Const.ERROR_MSG_FUNCTION);
				}
			}
		} else {
			if(access.hasAuthDistributor()){
				//ID種別=代理店格
				Account acc = E_AccountDao.getRecById(access.user.AccountId);
				distributor = new Account(Id = acc.ParentId, Name = acc.E_ParentName__c, E_CL1PF_ZHEADAY__c = acc.E_ParentZHEADAY__c);
			} else if(access.hasAuthOffice()){
				//ID種別=代理店事務所
				office = E_AccountDao.getRecById(access.user.AccountId);
				
			} else if(access.hasAuthAgent()){
				//ID種別=代理店募集人
				agent = E_ContactDao.getRecById(access.user.ContactId);
				
			} else {
				//上記以外の時
				errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
				pageRef = Page.E_ErrorPage;
				pageRef.getParameters().put('code', E_Const.ERROR_MSG_FUNCTION);
			}
		}
	}

	//個別エラーチェック
	public boolean isValidate = true;
	public override boolean isValidate() {
		return this.isValidate;
	}
	
	//代理店事務所選択リスト作成（保有条件画面） ※有効な代理店事務所のみ表示する。
	public List<SelectOption> getActiveOfficeOptions(){
		List<SelectOption> ops = new List<SelectOption>();
		for(Account acc: E_AccountDaoWithout.getRecsByParentIdIsActive(distributor.Id)){
			ops.add(new SelectOption(acc.Id,acc.E_CL2PF_ZAGCYNUM__c + '　' + acc.Name));
		}
		return ops;
	}

	//代理店事務所選択リスト作成(手数料条件画面) ※手数料ＤＬは過去3年遡及することがある為, 有効、無効な代理店事務所を表示する。
	public List<SelectOption> getOfficeOptions(){
		List<SelectOption> ops = new List<SelectOption>();
		for(Account acc: E_AccountDaoWithout.getRecsByParentId(distributor.Id)){
			ops.add(new SelectOption(acc.Id,acc.E_CL2PF_ZAGCYNUM__c + '　' + acc.Name));
		}
		return ops;
	}
	
	//期間項目の検証
	//periodFrom/periodTo:検証対象
	//periodName:期間名（契約日、成立日等）
	//isMonth:検証対象が年月の場合true・年月日の場合falseを指定
	public Boolean validateDates(String periodFrom, String periodTo, String periodName, Boolean isMonth){
		
		Boolean ret = true;
		
		//メッセージ内容の設定
		String periodFromLabel = '開始日';
		String periodToLabel = '終了日';
		if(isMonth){
			periodFromLabel = '開始月';
			periodToLabel = '終了月';
		}
		
		//開始日の検証
		if(!validateDate(periodFrom, periodName, isMonth, 'の'+periodFromLabel)){
			ret = false;
		}
		
		//終了日の検証
		if(!validateDate(periodTo, periodName, isMonth, 'の'+periodToLabel)){
			ret = false;
		}
		
		//順序チェック
		if(ret && periodFrom > periodTo){
			pageMessages.addErrorMessage(periodName+'の'+periodToLabel+'が'+periodFromLabel+'より以前に設定されています。');
			ret = false;
		}
		
		return ret;
	}
	
	//期間項目の検証
	//period:検証対象
	//periodName:期間名（契約日、成立日等）
	//isMonth:検証対象が年月の場合true・年月日の場合falseを指定
	//msgParts:エラーメッセージに表示する期間名（開始月・終了日等）
	public Boolean validateDate(String period, String periodName, Boolean isMonth, String msgPart){
		
		String msgPartLabel = msgPart;
		if(String.isBlank(msgPart)){
			msgPartLabel = '';
		}
		
		//ブランクチェック
		if(String.isBlank(period)){
			pageMessages.addErrorMessage(periodName+msgPartLabel+'を設定して下さい。');
			return false;
		}
		
		//形式チェック
		if(!validateExistsDate(period, periodName, isMonth, msgPartLabel)){
			return false;
		}
		
		return true;
	}
	
	//日付形式チェック
	public Boolean validateExistsDate(String period, String periodName, Boolean isMonth, String msgPartLabel){
		String periodDate = period;
		if(isMonth){
			periodDate = period + '/01';
		}
		if(!E_Util.isExistsYyyyMMdd(periodDate)){
			pageMessages.addErrorMessage(periodName + msgPartLabel + 'が正しくありません。');
			return false;
		}
		return true;
	}

	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		PageReference pr = Page.E_DownloadPrintSheetsSelect;
		//UX2残案件_6 ここから
		if(getIsEmployee()){
			String selectTerm;
			String searchId;
			String did = ApexPages.currentPage().getParameters().get(KEY_DID);
			String oid = ApexPages.currentPage().getParameters().get(KEY_OID);
			String aid = ApexPages.currentPage().getParameters().get(KEY_AID);
			//代理店取得
			if(String.isNotBlank(did)){
				searchId = did;
				selectTerm = E_Const.CS_DISTORIBUTOR;
			//代理店事務所取得
			}else if(String.isNotBlank(oid)){
				searchId = oid;
				selectTerm = E_Const.CS_OFFICE;
			//募集人取得
			}else if(String.isNotBlank(aid)){
				searchId = aid;
				selectTerm = E_Const.CS_AGENT;
			}
			pr.getParameters().put('isSearched','true');
			pr.getParameters().put('selectTerm',selectTerm);
			pr.getParameters().put('searchId',searchId);
		}
		//UX2残案件_6 ここまで
		// [住生]住生ユーザの場合は Referer へ戻る
		if(access.isSumiseiUser() && String.isNotBlank(retUrl)){
			pr = new PageReference(retUrl);
		}
		return pr;
	}
}