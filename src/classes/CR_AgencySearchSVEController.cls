global with sharing class CR_AgencySearchSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public CR_Agency__c record{get;set;}	
			
	public CR_AgencySearchExtender getExtender() {return (CR_AgencySearchExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component7_val {get;set;}	
		public SkyEditor2.TextHolder Component7_op{get;set;}	
		public List<SelectOption> valueOptions_CR_Agency_c_Status_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component9_val {get;set;}	
		public SkyEditor2.TextHolder Component9_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component43_val {get;set;}	
		public SkyEditor2.TextHolder Component43_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component50_val {get;set;}	
		public SkyEditor2.TextHolder Component50_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component15_val {get;set;}	
		public SkyEditor2.TextHolder Component15_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component17_val {get;set;}	
		public SkyEditor2.TextHolder Component17_op{get;set;}	
			
	public String recordTypeRecordsJSON_CR_Agency_c {get; private set;}
	public String defaultRecordTypeId_CR_Agency_c {get; private set;}
	public String metadataJSON_CR_Agency_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public CR_AgencySearchSVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = CR_Agency__c.fields.Status__c;
		f = CR_Agency__c.fields.OutOfBusiness__c;
		f = Account.fields.E_CL1PF_ZHEADAY__c;
		f = CR_Agency__c.fields.AgencyNameKana__c;
		f = CR_Agency__c.fields.MRUnit__c;
		f = CR_Agency__c.fields.MRCode__c;
		f = CR_Agency__c.fields.toView__c;
		f = CR_Agency__c.fields.MRName__c;
		f = CR_Agency__c.fields.AgencyName__c;
		f = CR_Agency__c.fields.ZSHRMAIN__c;
		f = CR_Agency__c.fields.LastModifiedDate;
		f = CR_Agency__c.fields.BusinessStatus__c;
		f = CR_Agency__c.fields.RecordTypeId;
		f = CR_Agency__c.fields.MR__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = CR_Agency__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component7_val = new SkyEditor2__SkyEditorDummy__c();	
				Component7_op = new SkyEditor2.TextHolder('eq');	
				valueOptions_CR_Agency_c_Status_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : CR_Agency__c.Status__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_CR_Agency_c_Status_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component9_val = new SkyEditor2__SkyEditorDummy__c();	
				Component9_op = new SkyEditor2.TextHolder('eq');	
					
				Component50_val = new SkyEditor2__SkyEditorDummy__c();	
				Component50_op = new SkyEditor2.TextHolder('co');	
					
				Component15_val = new SkyEditor2__SkyEditorDummy__c();	
				Component15_op = new SkyEditor2.TextHolder('co');	
					
				Component17_val = new SkyEditor2__SkyEditorDummy__c();	
				Component17_op = new SkyEditor2.TextHolder('eq');	
					
				Component43_val = new SkyEditor2__SkyEditorDummy__c();	
				Component43_op = new SkyEditor2.TextHolder('eq');	
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('CR_Agency__c')
						.addFieldAsOutput('toView__c')
						.addFieldAsOutput('Status__c')
						.addFieldAsOutput('MRUnit__c')
						.addFieldAsOutput('MRName__c')
						.addFieldAsOutput('Account__r.E_CL1PF_ZHEADAY__c')
						.addFieldAsOutput('AgencyName__c')
						.addFieldAsOutput('ZSHRMAIN__c')
						.addFieldAsOutput('LastModifiedDate')
						.addFieldAsOutput('BusinessStatus__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component7_val, 'SkyEditor2__Text__c', 'Status__c', Component7_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component9_val, 'SkyEditor2__Checkbox__c', 'OutOfBusiness__c', Component9_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component43_val, 'SkyEditor2__Text__c', 'Account__r.E_CL1PF_ZHEADAY__c',Account.fields.E_CL1PF_ZHEADAY__c, Component43_op, true, false,true,0,true,'Account__c',CR_Agency__c.fields.Account__c )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component50_val, 'SkyEditor2__Text__c', 'AgencyNameKana__c', Component50_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component15_val, 'SkyEditor2__Text__c', 'MRUnit__c', Component15_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component17_val, 'SkyEditor2__Text__c', 'MRCode__c', Component17_op, true, 0, true ))
						.addWhere(' ( RecordType.DeveloperName = \'CR_Rule\')')
.addSort('OutOfBusiness__c',True,True).addSort('MRUnit__c',True,False).addSort('MR__c',True,False).addSort('Status__c',True,True).addSort('LastModifiedDate',False,True)
				);	
					
					resultTable = new resultTable(new List<CR_Agency__c>(), new List<resultTableItem>(), new List<CR_Agency__c>(), null);
				resultTable.ignoredOnSave = true;
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(CR_Agency__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_AgencySearchExtender(this);
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_CR_Agency_c_Status_c() { 
			return getOperatorOptions('CR_Agency__c', 'Status__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_OutOfBusiness_c() { 
			return getOperatorOptions('CR_Agency__c', 'OutOfBusiness__c');	
		}	
		public List<SelectOption> getOperatorOptions_Account_E_CL1PF_ZHEADAY_c() { 
			return getOperatorOptions('Account', 'E_CL1PF_ZHEADAY__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyNameKana_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyNameKana__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_MRUnit_c() { 
			return getOperatorOptions('CR_Agency__c', 'MRUnit__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_MRCode_c() { 
			return getOperatorOptions('CR_Agency__c', 'MRCode__c');	
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public CR_Agency__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, CR_Agency__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (CR_Agency__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}