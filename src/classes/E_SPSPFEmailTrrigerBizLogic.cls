public with sharing class E_SPSPFEmailTrrigerBizLogic {
	
	public void setMailNunmer(List<E_SPSPFEmail__c> newList){
		
		//ソート用（作成日順）ラッパークラスリスト
		List<E_SPSPFEmailWrapper> spsRapList = new List<E_SPSPFEmailWrapper>();
		Set<String> newCreatedYM =  new Set<String>();
		E_SPSPFEmailWrapper spsWrapper;
		//更新対象取得用セット（年/月）設定
		for(E_SPSPFEmail__c sps: newList){
			if(sps.MailNunmer_numOnly__c > 0){
				continue;
			}
			//作成日の年と月を[YYYY-MM]形式でsetへ格納
			spsWrapper = new E_SPSPFEmailWrapper(sps);
			newCreatedYM.add(spsWrapper.createdYM);
			spsRapList.add(spsWrapper);
		}
		if(spsRapList.isEmpty()) return;
		
		//作成日順にソート
		spsRapList.sort();
		
		//保存時の重複エラー回避の為、10回リトライ
		for(integer i = 0; i < 10; i ++){
system.debug('@@@setMailNunmer:start[' + i + ']');
			Map<String, Decimal> numMap = getNumMap(newCreatedYM);
			List<E_SPSPFEmail__c> updateList = getUpdateList(spsRapList, numMap);
			
			try{
				update updateList;
				//処理成功時にリトライをブレイク
system.debug('@@@setMailNunmer:end[' + i + ']');
				break;
				
			}catch(DmlException e){
				//処理失敗時にリトライ（10回目のエラーはThrow）
				if(i < 9 && e.getMessage().contains('duplicate value found: MailNunmer__c')) continue;
				throw e;
			}
		}
	}
	
	//ソート用（作成日順）ラッパークラス
	public class E_SPSPFEmailWrapper implements Comparable {
		public E_SPSPFEmail__c rec;
		public String createdYM;
		public E_SPSPFEmailWrapper(E_SPSPFEmail__c spsEmail) {
			rec = spsEmail;
			//YYYY-MM形式を保持
			createdYM = string.valueOf(spsEmail.CreatedDate.Year()) + '-' + string.valueOf(spsEmail.CreatedDate.month()).leftPad(2, '0');
		}
		public Integer compareTo(Object compareTo) {
			E_SPSPFEmailWrapper compareToSps = (E_SPSPFEmailWrapper)compareTo;
			if (rec.CreatedDate > compareToSps.rec.CreatedDate) return 1;
			else if (rec.CreatedDate < compareToSps.rec.CreatedDate) return -1;
			return 0;
		}
	}
	
	//年月ごととメール送信NoのMax値のMapに値を設定
	private Map<String, Decimal> getNumMap(Set<String> newCreatedYM){
		Map<String, Decimal> resultMap = new Map<String, Decimal>();
		//処理対象年月分ループし、年月のMax値を持つレコードをクエリする
		for(String ym: newCreatedYM){
			E_SPSPFEmail__c maxRec = E_SPSPFEmailDaoWithOut.getMaxMailNumberByYM(ym);
			Decimal mailMaxNum;
			if(maxRec != null){
			    mailMaxNum = maxRec.MailNunmer_numOnly__c;
			} else {
			    mailMaxNum = 0;
			}
			resultMap.put(ym, mailMaxNum);
		}
		return resultMap;
	}
	
	//更新用リストに値を設定
	private List<E_SPSPFEmail__c> getUpdateList(List<E_SPSPFEmailWrapper> spsRapList, Map<String, Decimal> numMap){
		List<E_SPSPFEmail__c> resultList = new List<E_SPSPFEmail__c>();
		for(E_SPSPFEmailWrapper sps: spsRapList){
			Decimal mailNum = 1;
			if(numMap.containsKey(sps.createdYM)){
				mailNum = numMap.get(sps.createdYM) + 1;
			}
			resultList.add(new E_SPSPFEmail__c(id = sps.rec.Id, MailNunmer__c = sps.createdYM + '-' + string.valueOf(mailNum).leftPad(5, '0')));
			numMap.put(sps.createdYM, mailNum);
		}
		return resultList;
	}
}