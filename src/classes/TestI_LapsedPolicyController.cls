@isTest
private class TestI_LapsedPolicyController {

	//addRowsのテスト
	@isTest static void addRowsTest() {
		Test.startTest();
		I_LapsedPolicyController lpc = new I_LapsedPolicyController();
		lpc.rowCount = 981;
		lpc.addRows();
		System.assertEquals(lpc.rowCount,1000);
		Test.stopTest();
	}

	//該当する契約がありませんエラー
	@isTest static void policyRecsEmptyTest() {
		createMessageData('LIS|002','該当する契約がありません');
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'lapsedDay');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			//TestI_TestUtil.createIDCPF(true, us.Id, null);
			createIDManage(us);

			Test.startTest();
			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			System.debug('テストデバッグ' + lpc.pageMessages);
			System.assertEquals(lpc.pageMessages.getWarningMessages()[0].summary,'該当する契約がありません');
			Test.stopTest();
		}
	}

	//createCovpfConditionのテスト--MR
	@isTest static void createCovpfConditionMR() {
		User us = createUserKyotentyo();
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);

		System.runAs(us){
			createIDManage(us);

			Test.startTest();
			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			System.assert(lpc.pageMessages.hasMessages());
			Test.stopTest();
		}
	}

	//sortRowsのテスト-失効日
	@isTest static void sortRowsLapsedDayTest() {
		Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'lapsedDay');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].lapsedDay,'2016/11/11');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約者
	@isTest static void sortRowsContractTest() {
		Test.startTest();
		clear();
		createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'contractor');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			conNameList.sort();
			System.assertEquals(lpc.policyList[0].contractor,conNameList[0]);
			Test.stopTest();
		}
	}

	//sortRowsのテスト-証券番号
	@isTest static void sortRowsPolicyNumTest() {
		Test.startTest();
		clear();
		createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'policyNum');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].policyNum,'Test0000');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-契約日
	@isTest static void sortRowsContractDayTest() {
		Test.startTest();
		clear();
		createPolicy1(false,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'contractDay');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].contractDay,'2014/11/10');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険種類
	@isTest static void sortRowsPolicyTypeTest() {
		Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'policyType');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].policyType,'4テストタイプ<br>4テストタイプ<br>4テストタイプ');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-保険料
	@isTest static void sortRowsMoneyTest() {
		Test.startTest();
		clear();
		E_Policy__c po = createPolicy1(false,UserInfo.getUserId());
		createCOV(po);
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'policyFee');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].policyFee,Decimal.valueOf(100000).format());
			Test.stopTest();
		}
	}

	//sortRowsのテスト-担当MR名
	@isTest static void sortRowsMRNameTest() {
		Test.startTest();
		clear();
		User us = createUserKyotentyo();
		createPolicy(false,us,us.ContactId);
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'mrName');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User runAsUs = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(runAsUs){
			createIDManage(runAsUs);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].mrName,'(複数)');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-代理店
	/*@isTest static void sortRowsAgencyTest() {
		Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'agency');
		Test.setCurrentPage(pageRef);

		I_LapsedPolicyController lpc = new I_LapsedPolicyController();
		lpc.pageAction();
		lpc.sortRows();
		System.assertEquals(lpc.policyList[0].agency,'(複数)');
		Test.stopTest();
	}*/

	//sortRowsのテスト-事務所
	@isTest static void sortRowsOfficeTest() {
		Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'office');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].office,'(複数)');
			Test.stopTest();
		}
	}

	//sortRowsのテスト-募集人
	@isTest static void sortRowsAgentTest() {
		Test.startTest();
		clear();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'agent');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			lpc.sortRows();
			System.assertEquals(lpc.policyList[0].agent,'(複数)');
			Test.stopTest();
		}
	}

	//getIsTotalRowsのテスト
	@isTest static void getIsTotalRowsTest(){
		Test.startTest();
		createPolicy1(true,UserInfo.getUserId());
		createPolicy2(false,UserInfo.getUserId());
		createPolicy3(false,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];

		System.runAs(us){
			createIDManage(us);

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			lpc.pageAction();
			String rows = lpc.getIsTotalRows();
			System.assertEquals(rows,'3');
			Test.stopTest();
		}
	}
	/*
	 *フィルターモード
	 */

	@isTest static void filterModeTest() {
		
		clear();
		//代理店格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//募集人コードが5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		//保険契約ヘッダ作成
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = agentA.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = agentA.Id;
		po.COMM_SINSTAMT__c = 100;
		po.COMM_CHDRNUM__c = 'Test0002';
		po.COMM_OCCDATE__c = '20151110';
		po.COMM_STATCODE__c = 'L';
		po.COMM_ZRSTDESC__c  = '失効中';
		po.COMM_SINSTAMT__c = 50000;
		po.MainAgentAccount__c = office.Id;
		po.SubAgentAccount__c =office.Id;
		po.MainAgent__c = agentA.Id;
		po.SubAgent__c = agentA.Id;
		insert po;
		//個人保険特約作成
		createCOV(po);

		PageReference pageRef = Page.IRIS_LapsedPolicy;
		pageRef.getParameters().put('st', 'lapsedDay');
		pageRef.getHeaders().put('User-Agent', 'lapse');
		Test.setCurrentPage(pageRef);
		User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		createIDManage(us);

		System.runAs(us){
			//=====================テスト開始=====================
			Test.startTest();

			I_LapsedPolicyController lpc = new I_LapsedPolicyController();
			E_IRISHandler iris = E_IRISHandler.getInstance();
			//募集人Bで参照
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
			lpc.pageAction();
			Integer totalRows = lpc.listRows;
			system.debug('totalRows'+totalRows);
			System.assertEquals(1,totalRows);
			//=====================テスト終了=====================
			Test.stopTest();
		}
	}

	//データ作成
	static Integer num = 0;
	static List<String> conNameList = new List<String>();
	static List<String> usNameList = new List<String>();
	static void clear(){
		num = 0;
		conNameList = new List<String>();
		usNameList = new List<String>();
	}
	//ログイン判定ユーザ
	static User runAsUser(String sfid){
		User thisUser = [ select Id,ContactId from User where Id = :sfid ];
		return thisUser;
	}
	//保険契約ヘッダ
	static void createPolicy(Boolean choice,User us,String conId){
		Account acc = createAccountOwner(us);
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		po.COMM_CHDRNUM__c = 'Test0004';
		po.COMM_OCCDATE__c = '20151110';
		po.COMM_STATCODE__c = 'L';
		po.COMM_ZRSTDESC__c  = '失効中';
		po.COMM_SINSTAMT__c = 100000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = conId;
		po.SubAgent__c = con.Id;
		//po.PolicyInCharge__c = true;
		po.OwnerId = us.Id;
		insert po;
		createCOV(po);
	}
	static E_Policy__c createPolicy1(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = cretateContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.DCOLI_ZNAME40A01__c = '2テストタイプ';
		po.DCOLI_ZNAME40A02__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		po.COMM_CHDRNUM__c = 'Test0002';
		po.COMM_OCCDATE__c = '20151110';
		po.COMM_STATCODE__c = 'L';
		po.COMM_ZRSTDESC__c  = '失効中';
		po.COMM_SINSTAMT__c = 50000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.OwnerId = userId;
		insert po;
		createCOV(po);
		return po;
	}
	static void createPolicy2(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20171110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '1テストタイプ';
		po.DCOLI_ZNAME40A01__c = '1テストタイプ';
		po.DCOLI_ZNAME40A02__c = '1テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10000;
		po.COMM_CHDRNUM__c = 'Test0001';
		po.COMM_OCCDATE__c = '20161110';
		po.COMM_STATCODE__c = 'L';
		po.COMM_ZRSTDESC__c  = '失効中';
		po.COMM_SINSTAMT__c = 100000;
		po.OwnerId = userId;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con2.Id;
		insert po;
		createCOV(po);
	}
	static void createPolicy3(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = cretateContact(acc,choice);
		Contact con2 = cretateContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.DCOLI_ZNAME40A01__c = '3テストタイプ';
		po.DCOLI_ZNAME40A02__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10;
		po.COMM_CHDRNUM__c = 'Test0000';
		po.COMM_OCCDATE__c = '20141110';
		po.COMM_STATCODE__c = 'L';
		po.COMM_ZRSTDESC__c  = '失効中';
		po.COMM_SINSTAMT__c = 10000;
		po.OwnerId = userId;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;
		insert po;
		createCOV(po);
	}
	//個人保険特約
	static void createCOV(E_Policy__c po){
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_STATDATE__c = '20161111';
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		cov.COLI_ZCRIND__c = 'C';
		cov.COMM_ZCOVRNAM__c = '4テストタイプ';
		cov.COMM_ZNAME40A01__c = '4テストタイプ';
		cov.COMM_ZNAME40A02__c = '4テストタイプ';
		insert cov;
	}
	//募集人（取引先責任者)
	static Contact cretateContact(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//募集人（取引先責任者)
	static Contact cretateContact2(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//親取引先
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//親取引先2
	static Account createParentAccount2(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//取引先
	static Account createAccount(){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c,ParentId from Account where Id = :acp.Id];
	}
	//取引先2
	static Account createAccount2(){
		Account acc = createParentAccount2();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先3
	static Account createAccountOwner(User us){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		acp.OwnerId = us.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//ユーザ
	static void createUser(Contact con,Account acc){
		User us = new User();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
	}
	//ユーザ（拠点長)
	static User createUserKyotentyo(){
		User us = new User();
		us.Username = 'kyotentyo@test.com@sfdc.com';
		us.Alias = '拠点長';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('拠点長');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		//us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		//us.ContactId = con.Id;
		insert us;
		return us;
	}

	//ユーザ
	static User createRunAsUser(String proName,String idType,Boolean choice){
		User runAsUs =[ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		System.runAs(runAsUs){
			Account acc = createAccount();
			Contact con = cretateContact(acc,false);
			us.Username = 'test@test.com@sfdc.com' + String.valueOf(++num);
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト' + String.valueOf(++num);
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(proName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.IsActive = true;
			if(choice){
				us.ContactId = con.Id;
			}

			insert us;
			if(choice){
				TestI_TestUtil.createIDCPF(true, us.Id, idType);
			}else{
				createIDManage(us);
			}
		}
		return us;
	}

	//メッセージマスタ
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//irisメニューー
	static void createIRISMenu(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			I_MenuMaster__c mn = new I_MenuMaster__c();
			mn.MenuKey__c = 'invalidationList';
			insert mn;
		}
	}

	//ID管理作成
	static void createIDManage(User us){
		 E_IDCPF__c ic = new E_IDCPF__c();
		 ic.ZWEBID__c = '1234567890';
		 ic.ZIDOWNER__c = 'AG' + 'test01';
		 ic.User__c = us.Id;
		 ic.AppMode__c = 'IRIS';
		 ic.ZINQUIRR__c = 'BR00';
		 insert ic;
	}


}