@isTest
private class TestE_ProgressSearchBankSVEExtender
{
	/**
	 * 入力エラー確認 #1 「取扱店番（支店コード）」がスペースの場合
	 */
	private static testMethod void testProgressSearchBank001() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_ProgressSearchBankSVEController controller;
		E_ProgressSearchBankSVEExtender extender;

		System.runAs(u){
			Test.startTest();
			E_MPQPF__c mpqpf = new E_MPQPF__c();
			PageReference pref = Page.E_ProgressSearchBankSVE;
			Test.setCurrentPage(pref);
        	
        	controller = new E_ProgressSearchBankSVEController(new ApexPages.StandardController(mpqpf));
            extender = controller.getExtender();
			resultPage = extender.pageAction();
			extender.init();
			extender.pageAction();
			try{
				extender.preSearch();		
			}catch(SkyEditor2.ExtenderException e){
				System.assert(true);
			}

			Test.stopTest();
		}
	}
	/**
	 * 入力エラー確認 #2 「取扱店番（支店コード）」が数字４桁以外の場合
	 */
	private static testMethod void testProgressSearchBank002() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_ProgressSearchBankSVEController controller;
		E_ProgressSearchBankSVEExtender extender;

		System.runAs(u){
			Test.startTest();
			E_MPQPF__c mpqpf = new E_MPQPF__c();
			PageReference pref = Page.E_ProgressSearchBankSVE;
			Test.setCurrentPage(pref);
        	
        	controller = new E_ProgressSearchBankSVEController(new ApexPages.StandardController(mpqpf));
			controller.iStoreCd_val.SkyEditor2__Text__C = '12345';
            extender = controller.getExtender();
			resultPage = extender.pageAction();
			extender.init();
			extender.pageAction();
			try{
				extender.preSearch();		
			}catch(SkyEditor2.ExtenderException e){
				System.assert(true);
			}

			Test.stopTest();
		}
	}
	/**
	 * 入力エラー確認 #4「契約ステータス」が一つも選択されていない場合
	 */
	private static testMethod void testProgressSearchBank003() {
		//代理店
		Account accDis = TestE_TestUtil.createAccount(true);
		//募集人
		Contact conCust = TestE_TestUtil.createContact(true, accDis.Id, 'testsan', '99999999', '');
		//ユーザ
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG04__c = '1';
		insert idcpf;

		PageReference resultPage;
		E_ProgressSearchBankSVEController controller;
		E_ProgressSearchBankSVEExtender extender;

		System.runAs(u){
			Test.startTest();
			E_MPQPF__c mpqpf = new E_MPQPF__c();
			PageReference pref = Page.E_ProgressSearchBankSVE;
			Test.setCurrentPage(pref);
        	
        	controller = new E_ProgressSearchBankSVEController(new ApexPages.StandardController(mpqpf));
			controller.iStoreCd_val.SkyEditor2__Text__C = '1234';
            extender = controller.getExtender();
			resultPage = extender.pageAction();
			extender.init();
			extender.pageAction();
			try{
				extender.preSearch();		
			}catch(SkyEditor2.ExtenderException e){
				System.assert(true);
			}

			Test.stopTest();
		}
	}
}