/**
* 
*/
@isTest
private class TestE_MKFriendDetailSVEExtender {
    
    
	/* 
	* global override void init() 
	*/
    static testMethod void testInit() {
        
        PageReference pref = Page.E_MKFriendDetailSVE;
        Test.setCurrentPage(pref);
        
        Lead lead = new Lead(firstname='Test', lastname='Test');
        
        Test.startTest();

        E_MKFriendDetailSVEController controller = new E_MKFriendDetailSVEController(new ApexPages.StandardController(lead));
        E_MKFriendDetailSVEExtender extender = new E_MKFriendDetailSVEExtender(controller);

        Test.stopTest();
    }
}