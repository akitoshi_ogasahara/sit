global with sharing class E_InveHistorySVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public E_ITTPF__c record{get;set;}	
			
	public E_InveHistorySVEExtender getExtender() {return (E_InveHistorySVEExtender)extender;}
	
		public dataTable dataTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component28_from{get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component28_to{get;set;}	
			
	public String recordTypeRecordsJSON_E_ITTPF_c {get; private set;}
	public String defaultRecordTypeId_E_ITTPF_c {get; private set;}
	public String metadataJSON_E_ITTPF_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public E_InveHistorySVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = E_ITTPF__c.fields.promiseDate__c;
		f = E_ITTPF__c.fields.ZBRNDNMS__c;
		f = E_ITTPF__c.fields.ZMFTRAND__c;
		f = E_ITTPF__c.fields.ZQUANT__c;
		f = E_ITTPF__c.fields.ZPRMUNTY__c;
		f = E_ITTPF__c.fields.ZTRDPRCY__c;
		f = E_ITTPF__c.fields.ZBLNCTXY__c;
 f = E_ITTPF__c.fields.promiseDate__c;
		f = E_ITTPF__c.fields.ZBRNDCDE__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = E_ITTPF__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component28_from = new SkyEditor2__SkyEditorDummy__c();	
				Component28_to = new SkyEditor2__SkyEditorDummy__c();	
					
				queryMap.put(	
					'dataTable',	
					new SkyEditor2.Query('E_ITTPF__c')
						.addFieldAsOutput('promiseDate__c')
						.addFieldAsOutput('ZBRNDNMS__c')
						.addFieldAsOutput('ZMFTRAND__c')
						.addFieldAsOutput('ZQUANT__c')
						.addFieldAsOutput('ZPRMUNTY__c')
						.addFieldAsOutput('ZTRDPRCY__c')
						.addFieldAsOutput('ZBLNCTXY__c')
						.limitRecords(100)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component28_from, 'SkyEditor2__Date__c', 'promiseDate__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
						.addListener(new SkyEditor2.QueryWhereRegister(Component28_to, 'SkyEditor2__Date__c', 'promiseDate__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
.addSort('promiseDate__c',False,False).addSort('ZBRNDCDE__c',True,False)
				);	
					
					dataTable = new dataTable(new List<E_ITTPF__c>(), new List<dataTableItem>(), new List<E_ITTPF__c>(), null);
				 dataTable.setPageItems(new List<dataTableItem>());
				 dataTable.setPageSize(50);
				listItemHolders.put('dataTable', dataTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(E_ITTPF__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new E_InveHistorySVEExtender(this);
			presetSystemParams();
			extender.init();
			dataTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
			
			
	global with sharing class dataTableItem extends SkyEditor2.ListItem {
		public E_ITTPF__c record{get; private set;}
		@TestVisible
		dataTableItem(dataTable holder, E_ITTPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTable extends SkyEditor2.PagingList {
		public List<dataTableItem> items{get; private set;}
		@TestVisible
			dataTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dataTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableItem(this, (E_ITTPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
		public void doFirst(){first();}
		public void doPrevious(){previous();}
		public void doNext(){next();}
		public void doLast(){last();}
		public void doSort(){sort();}

        public List<dataTableItem> getViewItems() {            return (List<dataTableItem>) getPageItems();        }
	}

			
	}