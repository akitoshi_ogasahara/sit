@isTest
public with sharing class TestE_SVCPFDao {
	
	/**
     * E_PolicyDao 分岐網羅
     */
    private static testMethod void testE_SVCPFDao() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        E_BizDataSyncLog__c log = TestE_TestUtil.createEbizLog(true);
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            //テスト開始
            Test.startTest();

			set<id> ids = new set<id>{null};
			
			system.assertEquals( E_SVCPFDao.getRecsByPolicyId(ids).size() , 0);
			system.assertEquals( E_SVCPFDao.getRecsByCOLI().size() , 0);
			system.assertEquals( E_SVCPFDao.getRecsByPolicyIdSPVA (ids).size(),0);
			

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}