@isTest(seealldata = false)
public with sharing class TestE_ProgressSearchController {
    
    private static testmethod void  standardTest(){
        PageReference pref = Page.E_ProgressSearch;
        String ipage;
        test.setCurrentPage(pref);
        E_ProgressSearchController controller  = new E_ProgressSearchController();      
        ipage = controller.iPage;
        
        System.assertEquals('E_ProgressSearchAgentSVE', ipage);
        System.assertEquals('新契約申込進捗結果', controller.getpagetitle());
        
    }
    
    private static testmethod void  changeIpageTest(){
        PageReference pref = Page.E_ProgressSearch;
        String ipage;
        test.setCurrentPage(pref);
        E_ProgressSearchController controller  = new E_ProgressSearchController();        
        controller.iPage = 'test';
        ipage = controller.iPage;
        System.assertEquals('test',iPage);
        
    }
    
}