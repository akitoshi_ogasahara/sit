global class CC_InformClosedSRActivityBatch implements Database.Batchable<SObject>, Database.Stateful{

    private String sObjectName = Schema.SObjectType.CC_SRActivity__c.Name;
    //送信先支社一覧を保持
    private Set<String> targetBRSet;
    //送信キー
    String batchKey;

    global CC_InformClosedSRActivityBatch() {
        targetBRSet = new Set<String>();
    }

    /**
     * BatchApexの開始処理です
     * @param ctx
     * @return Database.QueryLocator 
     */
    global Database.QueryLocator start(Database.BatchableContext BC){

        batchKey = E_Util.getSysDate('yyyyMMdd_HH00');

        //SOQL作成
        String query = 'SELECT '
                    + 'id, CC_SRNo__c, CC_SRNo__r.CMN_AgencyName__c, CC_SRNo__r.CMN_AgencyName__r.E_CL2PF_BRANCH__c '
                    + 'FROM '
                    + sObjectName
                    + ' WHERE '
                    + 'CC_IsMRNoticeFlag__c = true AND CC_MRBatchKey__c = null ';
        return Database.getQueryLocator(query);
    }

    /**
     * BatchApexの処理実行します
     * @param ctx
     * @param scope
     */
    global void execute(Database.BatchableContext BC, List<CC_SRActivity__c> scope) {
        Set<ID> caseIds = new Set<ID>();
        for(CC_SRActivity__c srActivity : scope){
            //サマリーレコードと紐づけ
            srActivity.CC_MRBatchKey__c = batchKey;
            caseIds.add(srActivity.CC_SRNo__c);
        }

        update scope;

        for(CC_SRPolicy__c srPolicy : CC_InformClosedSRActivityDao.getRecsByCaseids(caseIds)){
            if(srPolicy.CC_PolicyID1__c != null){
                setBRNo(srPolicy.CC_PolicyID1__r);
            }
            if(srPolicy.CC_PolicyID2__c != null){
                setBRNo(srPolicy.CC_PolicyID2__r);
            }
            if(srPolicy.CC_PolicyID3__c != null){
                setBRNo(srPolicy.CC_PolicyID3__r);
            }
            if(srPolicy.CC_PolicyID4__c != null){
                setBRNo(srPolicy.CC_PolicyID4__r);
            }
        }
    }

    /**
     * 契約情報から送付先支社コードを判定し、Setに追加
     * @param policy 契約情報
     */
    private void setBRNo(E_Policy__c policy){
        if(policy != null){
            //主担当の追加
            if(policy.MainAgent__c != null){
                setBRNo(policy.MainAgent__r.Account);
            }
            //従担当の追加
            if(policy.SubAgent__c != null){
                setBRNo(policy.SubAgent__r.Account);
            }
        }
    }
    /**
     * Accountから支社コードを保持
     * @param acc Accountレコード
     */
    private void setBRNo(Account acc){
        //支社コードが空の際は処理をスキップ
        if(acc == null || String.isBlank(acc.E_CL2PF_BRANCH__c)){
            return;
        }
        //本社営業部担当の際には支社コード03に通知
//        if(acc.E_CL2PF_BRANCH__c == E_Const.AGENCY_KSECTION_HEAD_OFFICE){
        if(acc.KSECTION__c == E_Const.AGENCY_KSECTION_HEAD_OFFICE){
            targetBRSet.add('03');
        }else{
            targetBRSet.add(acc.E_CL2PF_BRANCH__c);
        }
    }

    /**
     * BatchApexの後処理
     * @param ctx
     * @return
     */
    global void finish(Database.BatchableContext BC) {

        E_MessageMaster__c message = E_MessageMasterDao.getRecByCode('CC|MRE|001');
        if(message == null){
            return;
        }
        //件名 
        String subject = '【保全受付連絡】通知メール';
        //本文（フォーマット）
        String textBodyFormat = message.Value__c;
        //本文（パラメータ）
        List<String> textBodyParams = new List<String>();

        //URL作成
        String url = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/CC_SRActivityFromListView?bkey=' + batchKey;
        textBodyParams.add(url+'&type=1');
        textBodyParams.add(url+'&type=2');

        //本文
        String textBody = String.format(textBodyFormat, textBodyParams);
        //送信元アドレス
        OrgWideEmailAddress senderAdress = E_OrgWideEmailAddressDao.getOrgWideEmailAddress(E_Const.EMAIL_NAME_ILJPSCREGCHANGE);
        //メール送信クラス
        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[]{};


        try{
            //送信対象者の判定用
            for(CC_BranchConfig__c bac : CC_InformClosedSRActivityDao.getRecsByBRCodes(targetBRSet)){
                Messaging.reserveSingleEmailCapacity(1);

                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setToAddresses(new String[]{ bac.CC_SharedMailAdd__c });
                email.setSubject(subject);
                email.setPlainTextBody(textBody);
                email.setOrgWideEmailAddressId(senderAdress.Id);
                emails.add(email);
            }
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(emails);
        }catch(Exception e){
            system.debug(e);
        }
    }
}