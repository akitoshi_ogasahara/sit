public with sharing class CR_DownloadHistoryListController {
	
	public List<E_DownloadHistorry__c> historyList {get; set;}
	
	/**
	 * 一覧表示クラス
	 */
	public class HistoryWrap{
		public E_DownloadHistorry__c history {get; set;}
		public Attachment file {get; set;}
		public Boolean isDl {get; set;}
		public Integer no {get; set;}
		public String dlStatus {get; set;}
		public String dlOutputYm {get; set;}
		public Map<String, Object> condi{get;set;}
		
		public HistoryWrap(E_DownloadHistorry__c history, Attachment file, Integer no){
			this.history = history;
			this.file = file;
			this.no = no;
			this.isDl = (history.Status__c == CR_Const.DL_STATUS_SUCCESS) && (file != null) ? true : false;
			this.dlStatus = history.StatusLabel__c;
			String outputYm = this.history.OutputRecordYM__c;
			this.dlOutputYm = String.isNotBlank(outputYm) ? outputYm.substring(0,4) + '/' + outputYm.substring(4,6) : '';
			//Conditionsの取得　　コンポーネント側ではConditionsKeyとConditionsLabelを使って表示させる
			condi = (Map<String, Object>)JSON.deserializeUntyped(history.Conditions__c);
		}
	}
	
	/**
	 * Constructor
	 */
	public CR_DownloadHistoryListController(){
		
	}

	/**
	 * ダウンロード履歴一覧取得
	 */
	public List<historyWrap> getHistoryWrapList(){
		List<HistoryWrap> wrapList = new List<HistoryWrap>();
		
		Integer cnt = 1;
		for(E_DownloadHistorry__c dh : historyList){
			// 添付ファイルあり
			if(dh.attachments.size() > 0){
				for(Attachment att : dh.attachments){
					wrapList.add(new HistoryWrap(dh, att, cnt));
					cnt++;
				}
			
			// 添付ファイルなし
			}else{
				wrapList.add(new HistoryWrap(dh, null, cnt));
				cnt++;
			}
		}
		
		return wrapList;
	}
}