public without sharing class SC_AgencyManagerDaoWithout {
	
	/**
	 * UserIdセット　をキーにレコードリストを取得
	 * @param Set<Id>
	 * @return List<SC_AgencyManager__c>
	 */
	public static List<SC_AgencyManager__c> getRecsByUserIds(Set<Id> userIds){
		return [Select Id, User__r.Id, HasUserEMail__c From SC_AgencyManager__c Where User__r.Id In :userIds];
	}
}