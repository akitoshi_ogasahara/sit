@IsTest
private class shujiShukeiTest {
    public static final String strDate = '2010/12/02';      //週次活動報告の日付
    public static final String strArea = 'エリアA';            //エリア
    public static final String strEigy = '営業部B';            //営業部
    public static final String strHarea = '本社営業部';      //本社エリア
    public static final String strHeigy1 = '支援有';       //本社営業部
    public static final String strHeigy2 = '直扱い';       //本社営業部

    public static String strUsrID {get;set;}                //営業部ユーザID
    public static String strHusrID {get;set;}               //本社営業部ユーザID
    public static String strWARID {get;set;}                //週次活動報告ID
    public static Boolean blnMLF {get;set;}                 //月末フラグ

    public static User usr1 {get;set;}                      //通常ユーザ
    public static User usr2 {get;set;}                      //本社ユーザ
    public static String pid {get;set;}                     //プロファイルID
    public static String aid {get;set;}                     //取引先ID1
    public static String aid2 {get;set;}                    //取引先ID2
    public static String pro_id {get;set;}                  //商品マスタID
    public static List<String> opp_id {get;set;}            //見込案件ID
    public static List<String> mtb_id {get;set;}            //月次予算集計ID


    static testMethod void myUnitTest01() {
system.debug('===========================【見込案件データが存在しない】===========================');
        //データ作成
        setDataUser();          //営業部ユーザ登録
        system.runas(usr1){
            setDataSM_WAR();    //商品マスタ、週次活動報告登録
            blnMLF = FALSE;
            system.debug('【RETURN：Message】' + shujiShukei.execute(strWARID, strDate, strUsrID, blnMLF));
        }
    }

    static testMethod void myUnitTest02() {
system.debug('===========================【月末：FALSE】===========================');
        //データ作成
        opp_id = new List<String>();
        setDataUser();          //営業部ユーザ登録
        system.runas(usr1){
            setDataSM_WAR();    //商品マスタ、週次活動報告登録
            setDataOpp();       //見込案件登録
            setDataMTB('E');        //月次予算集計登録
            setDataWTB();       //既存週次予算集計登録
        }
        setDataHuser();         //本社営業部ユーザ登録
        system.runas(usr2){
            setDataHOpp('E');       //本社営業部の見込案件登録
        }
        system.runas(usr1){
            blnMLF = FALSE;
            print_WTB();        //週次予算集計データ出力
            system.debug('【RETURN：Message】' + shujiShukei.execute(strWARID, strDate, strUsrID, blnMLF));
            print_OPP();        //見込案件データ出力
            print_MTB();        //月次予算集計データ出力
            print_WTB();        //週次予算集計データ出力
        }
    }


    static testMethod void myUnitTest03() {
system.debug('===========================【月末：TRUE】===========================');
        //データ作成
        opp_id = new List<String>();
        setDataUser();          //営業部ユーザ登録
        system.runas(usr1){
            setDataSM_WAR();    //商品マスタ、週次活動報告登録
            setDataOpp();       //見込案件登録
            setDataMTB('E');        //月次予算集計登録
            setDataWTB();       //既存週次予算集計登録
        }
        setDataHuser();         //本社営業部ユーザ登録
        system.runas(usr2){
            setDataHOpp('E');       //本社営業部の見込案件登録
        }
        system.runas(usr1){
            blnMLF = TRUE;
            print_WTB();        //週次予算集計データ出力
            system.debug('【RETURN：Message】' + shujiShukei.execute(strWARID, strDate, strUsrID, blnMLF));
            print_OPP();        //見込案件データ出力
            print_MTB();        //月次予算集計データ出力
            print_WTB();        //週次予算集計データ出力
        }
    }


    static testMethod void myUnitTest04() {
system.debug('===========================【本社営業部　月末：FALSE】===========================');
        //データ作成
        opp_id = new List<String>();
        setDataUser();          //営業部ユーザ登録
        system.runas(usr1){
            setDataSM_WAR();    //商品マスタ、週次活動報告登録
            setDataOpp();       //見込案件登録
            setDataMTB('H');        //月次予算集計登録
        }
        setDataHuser();         //本社営業部ユーザ登録
        system.runas(usr2){
            setDataHOpp('H');       //本社営業部の見込案件登録
            setDataWTB();       //既存週次予算集計登録
            blnMLF = FALSE;
            print_WTB();        //週次予算集計データ出力
            system.debug('【RETURN：Message】' + shujiShukei.execute(strWARID, strDate, strUsrID, blnMLF));
            print_OPP();        //見込案件データ出力
            print_MTB();        //月次予算集計データ出力
            print_WTB();        //週次予算集計データ出力
        }
    }

    static testMethod void myUnitTest05() {
system.debug('===========================【本社営業部　月末：TRUE】===========================');
        //データ作成
        opp_id = new List<String>();
        setDataUser();          //営業部ユーザ登録
        system.runas(usr1){
            setDataSM_WAR();    //商品マスタ、週次活動報告登録
            setDataOpp();       //見込案件登録
            setDataMTB('H');        //月次予算集計登録
        }
        setDataHuser();         //本社営業部ユーザ登録
        system.runas(usr2){
            setDataHOpp('H');       //本社営業部の見込案件登録
            setDataWTB();       //既存週次予算集計登録
            blnMLF = TRUE;
            print_WTB();        //週次予算集計データ出力
            system.debug('【RETURN：Message】' + shujiShukei.execute(strWARID, strDate, strUsrID, blnMLF));
            print_OPP();        //見込案件データ出力
            print_MTB();        //月次予算集計データ出力
            print_WTB();        //週次予算集計データ出力
        }
    }




    /*------------------------------------------------------------------------
    *
    * データ作成
    *
    *------------------------------------------------------------------------
    */
    /** 営業部ユーザ・取引先登録 */
    private static void setDataUser() {
        //プロファイル取得
        pid = [SELECT Id FROM Profile WHERE UserType = 'Standard' LIMIT 1].Id;

        //営業部ユーザ
        usr1 = new User();
        usr1.ProfileId = pid;
        usr1.LastName = 'LastName1';
        usr1.Username = 'keizu1@keizu.com';
        usr1.Email = 'keizu1@keizu.com';
        usr1.UnitName__c = strArea;             //拠点名
        usr1.Unit__c = strEigy;                 //営業部
        usr1.TimeZoneSidKey = 'Asia/Tokyo';
        usr1.LocaleSidKey = 'ja_JP';
        usr1.EmailEncodingKey = 'ISO-2022-JP';
        usr1.LanguageLocaleKey = 'ja';
        usr1.Alias = 'keizu1';
        insert usr1;
        strUsrID = [SELECT id FROM User ORDER BY CreatedDate DESC LIMIT 1].id;

        //取引先登録
        Account acc = new Account();
        acc.Name = '集計テスト';
        acc.UnitName__c = strEigy;              //担当拠点
        insert acc;
        aid = [SELECT Id FROM Account ORDER BY CreatedDate DESC LIMIT 1].Id;

        Account acc2 = new Account();
        acc2.Name = '集計テスト';
        acc2.UnitName__c = '営業部C';              //担当拠点
        insert acc2;
        aid2 = [SELECT Id FROM Account ORDER BY CreatedDate DESC LIMIT 1].Id;
    }

    /** 本社営業部ユーザ登録 */
    private static void setDataHuser() {
        usr2 = new User();
        usr2.ProfileId = pid;
        usr2.LastName = 'LastName2';
        usr2.Username = 'keizu2@keizu.com';
        usr2.Email = 'keizu2@keizu.com';
        usr2.UnitName__c = strArea;             //拠点名
        usr2.Unit__c = strHarea;                //営業部
        usr2.TimeZoneSidKey = 'Asia/Tokyo';
        usr2.LocaleSidKey = 'ja_JP';
        usr2.EmailEncodingKey = 'ISO-2022-JP';
        usr2.LanguageLocaleKey = 'ja';
        usr2.Alias = 'keizu2';
        insert usr2;

        strHusrID = [SELECT id FROM User ORDER BY CreatedDate DESC LIMIT 1].id;
    }

    /** 商品マスタ・週次活動報告登録 */
    private static void setDataSM_WAR() {
        //商品マスタ
        ProductMaster__c pm = new ProductMaster__c();
        pm.Name = 'test';
        pm.LargeClassification__c = 'L';
        pm.MiddleClassification__c = 'M';
        insert pm;
        //pro_id = [SELECT Id FROM ProductMaster__c WHERE IsActive__c = True ORDER BY CreatedDate DESC LIMIT 1].Id;

        //週次活動報告
        WeeklyActivityReports__c war = new WeeklyActivityReports__c();
        war.Date__c = Util.cnvStrDate(strDate);
        insert war;

        strWARID = [SELECT Id FROM WeeklyActivityReports__c ORDER BY CreatedDate DESC LIMIT 1].Id;
    }

    /** 見込案件登録 */
    private static void setDataOpp() {
        List<String> tmpDate = new List<String>();
        tmpDate.add('2010/12/01');
        tmpDate.add('2010/12/05');
        tmpDate.add('2010/12/15');
        tmpDate.add('2010/12/25');
        tmpDate.add('2010/12/25');
        tmpDate.add('2010/12/31');
        tmpDate.add('2010/12/31');
        tmpDate.add('2010/11/30');
        List<String> tmpStage = new List<String>();
        tmpStage.add('S');
        tmpStage.add('A');
        tmpStage.add('B');
        tmpStage.add('S');
        tmpStage.add('S');
        tmpStage.add('D');
        tmpStage.add('C');
        tmpStage.add('S');
        List<String> tmpCate1 = new List<String>();
        tmpCate1.add('Strategic');
        tmpCate1.add('Strategic');
        tmpCate1.add('Strategic');
        tmpCate1.add('General');
        tmpCate1.add('General');
        tmpCate1.add('General');
        tmpCate1.add('General');
        tmpCate1.add('General');
        List<String> tmpCate2 = new List<String>();
        tmpCate2.add('SH');
        tmpCate2.add('SH');
        tmpCate2.add('SH');
        tmpCate2.add('SH');
        tmpCate2.add('野村');
        tmpCate2.add('野村');
        tmpCate2.add('野村');
        tmpCate2.add('野村');
        List<String> tmpShohin = new List<String>();
        tmpShohin.add('Level Term');
        tmpShohin.add('Level Term');
        tmpShohin.add('Level Term');
        tmpShohin.add('LTA');
        tmpShohin.add('Cancer');
        tmpShohin.add('Cancer');
        tmpShohin.add('Cancer');
        tmpShohin.add('Cancer');
        List<String> tmpChan = new List<String>();
        tmpChan.add('CA');
        tmpChan.add('CA');
        tmpChan.add('CA');
        tmpChan.add('CA');
        tmpChan.add('IP');
        tmpChan.add('IP');
        tmpChan.add('IP');
        tmpChan.add('IP');
        List<String> tmpYosan = new List<String>();
        tmpYosan.add('Yosan');
        tmpYosan.add('Yosan');
        tmpYosan.add('Yosan');
        tmpYosan.add('Yosan');
        tmpYosan.add('YosanA');
        tmpYosan.add('YosanA');
        tmpYosan.add('YosanA');
        tmpYosan.add('YosanA');
        List<Opportunity> list_opp = new List<Opportunity>();
        for (Integer j = 0; j<=7; j++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'テスト見込案件' + j;
            if (j==2){
                opp.AccountId = aid2;                               //取引先
            } else {
                opp.AccountId = aid;                                //取引先
            }
            opp.CloseDate = Util.cnvStrDate(tmpDate.get(j));        //日付
            opp.SupportFlg__c = false;                              //支援有無フラグ
            opp.StageName = tmpStage.get(j);                        //フェーズ
            opp.InsuranceType__c = pro_id;                          //商品マスタ
            opp.Category1TXT__c = tmpCate1.get(j);                  //区分１
            opp.Category2TXT__c = tmpCate2.get(j);                  //区分２
            opp.ProductType2TXT__c = tmpShohin.get(j);              //商品種別
            opp.Channel__c = tmpChan.get(j);                        //チャネル
            opp.Budget3TXT__c = tmpYosan.get(j);                    //予算３
            opp.PaymentType__c = '一時払';                         //"一時払", Amount * 2
            opp.Amount = j+1;                                       //GAPE = Amount * 2
            opp.WAPE__c = (j+1)*10;                                 //WAPE
            if (j==4) {
                opp.RecordTypeId = '01210000000Hu5O';               //予算データ
            } else {
                opp.RecordTypeId = '01210000000Hu5J';               //実績データ
            }
            opp.Count__c = j+1;                                     //件数
            opp.needs__c = '事業保障';
            opp.C12MonthReq__c = true;
            opp.IndustryMain__c = 'A 農業';
            opp.IndustrySub__c = '01 農業';
            opp.SubmitOWCount__c = 1;
            opp.AGRunning__c = 1;
            opp.ATRunning__c = 1;

            list_opp.add(opp);
        }
        insert list_opp;

        for (Opportunity tmp :[SELECT Id FROM Opportunity ORDER BY CreatedDate DESC LIMIT 8]) {
            opp_id.add(tmp.Id);
        }
    }

    /** 本社見込案件登録 */
    private static void setDataHOpp(String kubun) {
        List<String> tmpDate = new List<String>();
        tmpDate.add('2010/12/01');
        tmpDate.add('2010/12/05');
        tmpDate.add('2010/12/15');
        tmpDate.add('2010/12/25');
        tmpDate.add('2010/12/25');
        tmpDate.add('2010/12/31');
        tmpDate.add('2010/12/31');
        tmpDate.add('2010/11/30');
        List<String> tmpStage = new List<String>();
        tmpStage.add('S');
        tmpStage.add('S');
        tmpStage.add('B');
        tmpStage.add('D');
        tmpStage.add('C');
        tmpStage.add('B');
        tmpStage.add('A');
        tmpStage.add('S');
        List<String> tmpCate1 = new List<String>();
        tmpCate1.add('Strategic');
        tmpCate1.add('General');
        tmpCate1.add('Strategic');
        tmpCate1.add('General');
        tmpCate1.add('General');
        tmpCate1.add('Strategic');
        tmpCate1.add('General');
        tmpCate1.add('General');
        List<String> tmpCate2 = new List<String>();
        tmpCate2.add('SH');
        tmpCate2.add('SH');
        tmpCate2.add('野村');
        tmpCate2.add('SH');
        tmpCate2.add('SH');
        tmpCate2.add('野村');
        tmpCate2.add('野村');
        tmpCate2.add('野村');
        List<String> tmpShohin = new List<String>();
        tmpShohin.add('Level Term');
        tmpShohin.add('Level Term');
        tmpShohin.add('LTA');
        tmpShohin.add('Cancer');
        tmpShohin.add('Cancer');
        tmpShohin.add('Level Term');
        tmpShohin.add('Cancer');
        tmpShohin.add('Cancer');
        List<String> tmpChan = new List<String>();
        tmpChan.add('CA');
        tmpChan.add('CA');
        tmpChan.add('CA');
        tmpChan.add('CA');
        tmpChan.add('IP');
        tmpChan.add('IP');
        tmpChan.add('IP');
        tmpChan.add('IP');
        List<String> tmpYosan = new List<String>();
        tmpYosan.add('Yosan');
        tmpYosan.add('Yosan');
        tmpYosan.add('Yosan');
        tmpYosan.add('Yosan');
        tmpYosan.add('YosanA');
        tmpYosan.add('YosanA');
        tmpYosan.add('YosanA');
        tmpYosan.add('YosanA');
        List<Boolean> tmpSupport = new List<Boolean>();
        if (kubun == 'H') {
            tmpSupport.add(true);
            tmpSupport.add(true);
            tmpSupport.add(true);
            tmpSupport.add(true);
            tmpSupport.add(false);
            tmpSupport.add(true);
            tmpSupport.add(true);
            tmpSupport.add(true);
        } else {
            tmpSupport.add(false);
            tmpSupport.add(false);
            tmpSupport.add(false);
            tmpSupport.add(false);
            tmpSupport.add(true);
            tmpSupport.add(false);
            tmpSupport.add(false);
            tmpSupport.add(false);
        }
        List<Opportunity> list_opp = new List<Opportunity>();
        for (Integer j = 0; j<=7; j++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'テスト見込案件' + j;
            if (j==2){
                opp.AccountId = aid2;                               //取引先
            } else {
                opp.AccountId = aid;                                //取引先
            }
            opp.CloseDate = Util.cnvStrDate(tmpDate.get(j));        //日付
            opp.SupportFlg__c = tmpSupport.get(j);                  //支援有無フラグ
            opp.StageName = tmpStage.get(j);                        //フェーズ
            opp.InsuranceType__c = pro_id;                          //商品マスタ
            opp.Category1TXT__c = tmpCate1.get(j);                  //区分１
            opp.Category2TXT__c = tmpCate2.get(j);                  //区分２
            opp.ProductType2TXT__c = tmpShohin.get(j);              //商品種別
            opp.Channel__c = tmpChan.get(j);                        //チャネル
            opp.Budget3TXT__c = tmpYosan.get(j);                    //予算３
            opp.PaymentType__c = '一時払';                         //"一時払", Amount * 2
            opp.Amount = j+1;                                       //GAPE = Amount * 2
            opp.WAPE__c = (j+1)*10;                                 //WAPE
            if (j==0) {
                opp.RecordTypeId = '01210000000Hu5O';               //予算データ
            } else {
                opp.RecordTypeId = '01210000000Hu5J';               //実績データ
            }
            opp.Count__c = j+1;                                     //件数
            opp.needs__c = '事業保障';
            opp.C12MonthReq__c = true;
            opp.IndustryMain__c = 'A 農業';
            opp.IndustrySub__c = '01 農業';
            opp.SubmitOWCount__c = 1;
            opp.AGRunning__c = 1;
            opp.ATRunning__c = 1;

            list_opp.add(opp);
        }
        insert list_opp;

        for (Opportunity tmp :[SELECT Id FROM Opportunity ORDER BY CreatedDate DESC LIMIT 8]) {
            opp_id.add(tmp.Id);
        }
    }

    /** 月次予算集計登録 */
    private static void setDataMTB(String kubun) {
        List<String> tmpDate = new List<String>();
        tmpDate.add('2010/12/01');
        tmpDate.add('2010/12/05');
        tmpDate.add('2010/12/15');
        tmpDate.add('2010/12/20');
        tmpDate.add('2010/12/25');
        tmpDate.add('2010/12/31');
        tmpDate.add('2010/12/01');
        tmpDate.add('2010/12/05');
        tmpDate.add('2010/12/15');
        tmpDate.add('2010/12/20');
        tmpDate.add('2010/12/25');
        tmpDate.add('2010/11/30');
        List<String> tmpType = new List<String>();
        tmpType.add('1:保険種類');      //区分１用
        tmpType.add('1:保険種類');      //区分１用
        tmpType.add('1:保険種類');      //区分２用
        tmpType.add('1:保険種類');      //区分２用
        tmpType.add('1:保険種類');      //TOTAL用
        tmpType.add('1:保険種類');      //TOTAL用
        tmpType.add('2:商品種別');      //商品用
        tmpType.add('2:商品種別');      //商品用
        tmpType.add('3:チャネル');      //チャネル用
        tmpType.add('3:チャネル');      //チャネル用
        tmpType.add('4:予算3');           //予算３用
        tmpType.add('4:予算3');           //予算３用
        List<String> tmpGroup = new List<String>();
        tmpGroup.add('Strategic');
        tmpGroup.add('Strategic');
        tmpGroup.add('SH');
        tmpGroup.add('SH');
        tmpGroup.add('TOTAL');
        tmpGroup.add('TOTAL');
        tmpGroup.add('LTA');
        tmpGroup.add('LTA');
        tmpGroup.add('CA');
        tmpGroup.add('CA');
        tmpGroup.add('Yosan');
        tmpGroup.add('Yosan');
        List<MonthlyTotalBudget__c> mtb = new List<MonthlyTotalBudget__c>();
        for (Integer k = 0; k<=11; k++) {
            MonthlyTotalBudget__c m = new MonthlyTotalBudget__c();
            if (k==0) {
                m.Area__c = 'エリアB';                     //エリア
            } else {
                if (kubun == 'H') {
                    m.Area__c = strHarea;
                } else {
                    m.Area__c = strArea;                        //エリア
                }
            }
            if (K==2) {
                m.SalesUnit__c = '営業部C';                    //営業部
            } else {
                if (kubun == 'H') {
                    if (k>5) {
                        m.SalesUnit__c = strHeigy2;
                    } else {
                        m.SalesUnit__c = strHeigy1;
                    }
                } else {
                    m.SalesUnit__c = strEigy;                   //営業部
                }
            }
            m.Month__c = Util.cnvStrDate(tmpDate.get(k));   //日付
            m.Type__c = tmpType.get(k);                     //予算区分
            m.GroupName__c = tmpGroup.get(k);               //分類名
            m.GAPEBudget__c = (k+1)*2;                      //GAPE予算
            m.WAPEBudget__c = (k+1)*2;                      //WAPE予算
            mtb.add(m);
        }
        insert mtb;
        mtb_id = new List<String>();
        for (MonthlyTotalBudget__c tmp :[SELECT Id FROM MonthlyTotalBudget__c ORDER BY CreatedDate DESC LIMIT 12]) {
            mtb_id.add(tmp.Id);
        }
    }

    /** 週次予算集計登録 */
    private static void setDataWTB() {
        WeeklyTotalBudget__c wtb1 = new WeeklyTotalBudget__c();
        wtb1.WeeklyActivityReports__c = strWARID;
        wtb1.Area__c = strArea;
        wtb1.SalesUnit__c = strEigy;
        wtb1.WeekStartDay__c = Util.cnvStrDate('2010/11/01');
        insert wtb1;
/*
        WeeklyTotalBudget__c wtb = new WeeklyTotalBudget__c();
        wtb.WeeklyActivityReports__c = strWARID;
        wtb.Area__c = strArea;
        wtb.SalesUnit__c = strEigy;
        wtb.WeekStartDay__c = Util.cnvStrDate(strDate);
        insert wtb;
*/
    }



    /*------------------------------------------------------------------------
    *
    * データ出力
    *
    *------------------------------------------------------------------------
    */
    /** 見込案件データ出力 */
    private static void print_OPP() {
        for(Opportunity pre_OPP : [SELECT Id, Name, Area__c, SalesUnit__c, StageName, AssistanceUnit__c, SupportFlg__c, RecordType.Name,
                                          Category1TXT__c, Category2TXT__c, ProductType2TXT__c, Channel__c, Budget3TXT__c,
                                          GAPE__c, WAPE__c, Count__c, CloseDate
                                   FROM Opportunity
                                   WHERE Id IN : opp_id ORDER BY Id ASC]) {
            system.debug(pre_OPP.Id + ' ' + pre_OPP.Name + '    '
                        + pre_OPP.Area__c + '   ' + pre_OPP.SalesUnit__c + '    ' + pre_OPP.StageName + '   ' + pre_OPP.AssistanceUnit__c + '   ' + pre_OPP.SupportFlg__c + '   ' + pre_OPP.RecordType.Name + ' '
                        + pre_OPP.Category1TXT__c + '   ' + pre_OPP.Category2TXT__c + ' ' + pre_OPP.ProductType2TXT__c + '  ' + pre_OPP.Channel__c + '  ' + pre_OPP.Budget3TXT__c + '   '
                        + pre_OPP.GAPE__c + '   ' + pre_OPP.WAPE__c + ' ' + pre_OPP.Count__c + '    ' + pre_OPP.CloseDate
                        );
        }
    }

    /** 月次予算集計データ出力 */
    private static void print_MTB() {
        for(MonthlyTotalBudget__c pre_MTB : [SELECT Id, Area__c, SalesUnit__c, Month__c,
                                                    Type__c, GroupName__c, GAPEBudget__c, WAPEBudget__c
                                            FROM MonthlyTotalBudget__c
                                            WHERE Id IN : mtb_id ORDER BY Id ASC]) {
            system.debug(pre_MTB.Id + ' ' + pre_MTB.Area__c + ' ' + pre_MTB.SalesUnit__c + '    ' + pre_MTB.Month__c + '    '
                        + pre_MTB.Type__c + '   ' + pre_MTB.GroupName__c + '    ' + pre_MTB.GAPEBudget__c + '   ' + pre_MTB.WAPEBudget__c
            );
        }
    }

    /** 週次予算集計データ出力 */
    private static void print_WTB() {
        for(WeeklyTotalBudget__c pre_WTB : [SELECT Id, Area__c, SalesUnit__c,
                                                   Type__c, GroupName__c,
                                                   GAPE_Max__c, GAPE_Min__c, GAPEBudget__c,
                                                   WAPE_Max__c, WAPE_Min__c, WAPEBudget__c,
                                                   Count__c, FAINAL_FLG__c,
                                                   WeekStartDay__c, WeeklyActivityReports__c
                                            FROM WeeklyTotalBudget__c ORDER BY CreatedDate, SalesUnit__c, Type__c]) {
            system.debug(pre_WTB.Id + ' ' + pre_WTB.Area__c + ' ' + pre_WTB.SalesUnit__c + '    ' + pre_WTB.Type__c + ' ' + pre_WTB.GroupName__c + '    '
                        + pre_WTB.GAPE_Max__c + '   ' + pre_WTB.GAPE_Min__c + ' ' + pre_WTB.GAPEBudget__c + '   '
                        + pre_WTB.WAPE_Max__c + '   ' + pre_WTB.WAPE_Min__c + ' ' + pre_WTB.WAPEBudget__c + '   '
                        + pre_WTB.Count__c + '  ' + pre_WTB.FAINAL_FLG__c + '   ' + pre_WTB.WeekStartDay__c + ' ' + pre_WTB.WeeklyActivityReports__c);
        }
    }

}