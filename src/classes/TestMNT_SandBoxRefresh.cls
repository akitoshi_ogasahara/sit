@isTest
private class TestMNT_SandBoxRefresh {
    
    private static testMethod void testMaskField(){
        User usr = [SELECT Id,LastName,CommunityNickName,Email From User Limit 1];
        
        //名前系項目
        MNT_Masking.maskField mskFld = MNT_Masking.createMaskField('LastName', MNT_SandBoxRefresh.MASKTYPE.Name);
        usr.LastName = 'TerraSky エヌエヌ生命';
        mskFld.mask(usr);
        System.assertEquals(usr.LastName, '■e■r■S■y ■ヌ■ヌ■命');

        //ID値で上書き
        mskFld = MNT_Masking.createMaskField('CommunityNickName', MNT_SandBoxRefresh.MASKTYPE.maskById);
        mskFld.mask(usr);
        System.assertEquals(usr.CommunityNickName, usr.Id);

        //Email
        mskFld = MNT_Masking.createMaskField('Email', MNT_SandBoxRefresh.MASKTYPE.Email);
        mskFld.mask(usr);
        System.assertEquals(usr.Email, usr.Id + '@nnlink.sandbox');

        //電話番号
        usr.phone = '09012345678';
        mskFld = MNT_Masking.createMaskField('Phone', MNT_SandBoxRefresh.MASKTYPE.Phone);
        mskFld.mask(usr);
        System.assertEquals(usr.Phone, '99919395979');

        //住所
        usr.City = '東京都千代田区紀尾井町4-1　ニューオータニガーデンコート26階';
        mskFld = MNT_Masking.createMaskField('City', MNT_SandBoxRefresh.MASKTYPE.Address);
        mskFld.mask(usr);
        System.assertEquals(usr.City, '東京都千代田区紀');
    }

    private static testMethod void testMNT_RefreshJobHandler(){
        MNT_RefreshJobHandler handler = new MNT_RefreshJobHandler('Account');
        String soql = handler.getQueryLocator().getQuery();
        System.assert(soql.startsWith('SELECT Id'));
        System.assert(handler.getNextSObjectName() == 'Contact');

        List<Account> accs = Database.query(soql + ' Limit 1');
        handler.maskRecords(accs);
    }

    private static testMethod void testMNT_RefreshJobBatch(){
        MNT_RefreshJobBatch batch = new MNT_RefreshJobBatch('Account');
        //batch.executeNextJob = true;      連続して実行する場合にTrue
        Database.executeBatch(batch, 2000);
    }

/*    
    private static testMethod void testMNT_RefreshJobHandler(){
        MNT_RefreshJobHandler handler = new MNT_RefreshJobHandler('User');
        String soql = handler.getQueryLocator().getQuery();
        System.assert(soql.startsWith('SELECT Id'));
        System.assert(handler.getNextSObjectName() == 'Account');
    
        User usr = Database.query(soql + ' Limit 1');
        handler.maskRecords(new List<User>{usr});
    }

    private static testMethod void testMNT_RefreshJobBatch(){
        MNT_RefreshJobBatch batch = new MNT_RefreshJobBatch('User');
        //batch.executeNextJob = true;      連続して実行する場合にTrue
        Database.executeBatch(batch, 2000);
    }
*/    
}