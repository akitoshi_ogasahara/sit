/**
 * 
 */
@isTest
private class TestE_BizDataSyncLogDao {

	static testMethod void getRecByLast1_test01() {
		Test.startTest();
		E_BizDataSyncLog__c rec = E_BizDataSyncLogDao.getRecByLast1();
		Test.stopTest();
	}
	
	static testMethod void getNotBlankEndDateByLast1_test01() {
		Test.startTest();
		E_BizDataSyncLog__c rec = E_BizDataSyncLogDao.getNotBlankEndDateByLast1();
		Test.stopTest();
	}
	
	static testMethod void getNotBlankEndDateByLast1_forUp_test01() {
		Test.startTest();
		E_BizDataSyncLog__c rec = E_BizDataSyncLogDao.getNotBlankEndDateByLast1_forUp();
		Test.stopTest();
	}
	
	static testMethod void getRecById_test01() {
		E_BizDataSyncLog__c bds = createData();
		
		Test.startTest();
		E_BizDataSyncLog__c rec = E_BizDataSyncLogDao.getRecById(bds.Id);
		Test.stopTest();
	}
	
	static testMethod void getRecsByKindAndCreatedDate_test01() {
		Test.startTest();
		List<E_BizDataSyncLog__c> recs = E_BizDataSyncLogDao.getRecsByKindAndCreatedDate('0', Date.today());
		Test.stopTest();
	}

	static testMethod void getRecAgoByNotificationKindForPush_test01() {
		E_BizDataSyncLog__c bds1 = createData2();
		E_BizDataSyncLog__c bds2 = createData2();

		Test.startTest();
		E_BizDataSyncLog__c recs = E_BizDataSyncLogDao.getRecAgoByNotificationKindForPush(bds2.Id, I_Const.EBIZDATASYNC_KBN_NEWPOLICY);
		Test.stopTest();
	}
	static testMethod void getRecKindSRNotBlankBatchEndDateByLast1_test01(){
		E_BizDataSyncLog__c bds = createData3();

		Test.startTest();
		E_BizDataSyncLog__c rec = E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1();
		Test.stopTest();
	}

	/** Data **********************************************************/
	private static E_BizDataSyncLog__c createData(){
		E_BizDataSyncLog__c rec = new E_BizDataSyncLog__c();
		rec.Kind__c = '0';
		insert rec;
		return rec;
	}

	private static E_BizDataSyncLog__c createData2(){
		E_BizDataSyncLog__c rec = new E_BizDataSyncLog__c();
		rec.Kind__c = I_Const.EBIZDATASYNC_KBN_PUSHOUT;
		rec.NotificationKind__c = I_Const.EBIZDATASYNC_KBN_NEWPOLICY;
		insert rec;
		return rec;
	}
	private static E_BizDataSyncLog__c createData3(){
		Date d = date.newInstance(1996, 6, 23);
		DateTime dt = datetime.newInstance(1996, 6, 23);
		E_BizDataSyncLog__c rec = new E_BizDataSyncLog__c();
		rec.Kind__c = 'F';
		rec.InquiryDate__c = d;
		rec.BatchEndDate__c = dt;
		insert rec;
		return rec;
	}
}