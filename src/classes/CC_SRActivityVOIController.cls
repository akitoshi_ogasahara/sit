/*
 * CC_CaseSRActivityController
 * Controller class for CC_SRActivity
 * created  : Accenture 2018/6/5
 * modified :
 */

global with sharing class CC_SRActivityVOIController implements Vlocity_ins.VlocityOpenInterface2{
	global Object invokeMethod(String methodName,Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);

		Boolean success = true;
		try{
			if(methodName == 'getInitData'){
				success = CC_SRActivityVOIBizLogic.getInitData(inputs, output, options);
			}else if(methodName == 'getPicklist'){
				success = CC_SRActivityVOIBizLogic.getPicklist(inputs, output, options);
			}else if(methodName == 'saveSRActivity'){
				success = CC_SRActivityVOIBizLogic.saveSRActivity(inputs, output, options);
			}
		}catch(Exception e){
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}
}