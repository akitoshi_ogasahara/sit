public with sharing class I_PushOutBatchOperation {
	
	protected I_AbstractPushOutBatchOperation operation;
	
	private E_BizDataSyncLog__c ebizLog;
	private String pType;
	private String siteDomain;
	
	/**
	 * Constructor
	 */
	public I_PushOutBatchOperation(E_BizDataSyncLog__c ebizLog){
		this.ebizLog = ebizLog;
		this.pType = ebizLog.NotificationKind__c;
		
		// Siteのドメイン取得
		DomainSite d = [Select Domain.Domain, PathPrefix From DomainSite d Limit 1];
    	this.siteDomain = 'https://' + d.Domain.Domain + '/';
	}
    
    /**
     * getQueryLocator
     */
    public Database.QueryLocator getQueryLocator(){
    	// PushOut通知毎のOperationのインスタンス生成
    	String operationClass = I_Const.PUSHOUT_OPERATION_MAP.get(pType);
    	Type t = Type.forName(operationClass);
    	operation = (I_AbstractPushOutBatchOperation)t.NewInstance();
    	operation.pType = pType;
    	operation.siteDomain = this.siteDomain;
    	operation.ebizLog = ebizLog;
	
    	// クエリ条件セット
    	operation.setQueryCondition();
    	system.debug('*［operation.getQueryLocator()］ ' + operation.getQueryLocator());
    	
    	return operation.getQueryLocator();
    }
    
    /**
     * execute
     */
    public List<I_PushOut__c> execute(List<SObject> records){
    	List<I_PushOut__c> pushRecs = new List<I_PushOut__c>();

    	// PushOut通知レコード登録
    	pushRecs = operation.execute(records);

		return pushRecs;
    }
    
    /**
     * finish
     */
    public void finish(List<I_PushOut__c> records){
    	operation.finish(records);
    }
    
    /**
     * PushOut通知　メール送信バッチ呼び出し
     */
    public void callSendBatch(E_BizDataSyncLog__c ebizlog){
    	operation.callSendBatch(ebizlog);
    }
}