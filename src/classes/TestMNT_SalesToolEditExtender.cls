@isTest
private class TestMNT_SalesToolEditExtender{
    
    @isTest
    private static void doInitCreateCopyTest(){
		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険', UpsertKey__c = 'test001');
		insert content;

		PageReference pageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(pageRef); 
		ApexPages.currentPage().getParameters().put('id', content.Id);
		ApexPages.currentPage().getParameters().put('clone','1');

		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
		MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
		
		Test.startTest();
		extender.init();
		Test.stopTest();

		System.assertEquals(null, controller.record.UpsertKey__c);

	}

	@isTest
    private static void doInitCreateNewTest(){
    	I_ContentMaster__c parentContent = new I_ContentMaster__c(Name = '販売促進資料');
    	insert parentContent;

		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険');

		PageReference pageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(pageRef); 

		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
		MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
		
		Test.startTest();
		extender.init();
		Test.stopTest();

		System.assertNotEquals(null,controller.record.ParentContent__c);

	}

	@isTest
	private static void doCancelTest(){

		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険', UpsertKey__c = 'test003');
		insert content;

		PageReference PageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(PageRef);
		ApexPages.currentPage().getParameters().put('id', content.Id);
        ApexPages.standardController stdController = new ApexPages.StandardController(content);
        MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
        MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
        extender.init();

        Test.startTest();
        PageReference testRef = extender.doCancel();
        Test.stopTest();

        PageReference expectRef = new PageReference('/apex/MNT_SalesToolView?id=' + content.Id);

        System.assertEquals(expectRef.getUrl(), testRef.getUrl());
	}

	@isTest
	private static void doNewSaveTest(){

		I_ContentMaster__c content = new I_ContentMaster__c();

		PageReference PageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(PageRef);
		ApexPages.standardController stdController = new ApexPages.standardController(content);
		MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
		MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
		extender.init();

		controller.record.Name = 'テスト長期障害保険';
		controller.record.DocumentNo__c = '123456';

		Test.startTest();
		extender.doSave();
		Test.stopTest();

		I_ContentMaster__c selectContent = [SELECT Id FROM I_ContentMaster__c LIMIT 1];

		System.assertEquals(controller.record.Id,selectContent.Id);

	}

	@isTest
	private static void doSaveNameBlankErrorTest(){

		I_ContentMaster__c content = new I_ContentMaster__c();

		PageReference PageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(PageRef);
		ApexPages.standardController stdController = new ApexPages.standardController(content);
		MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
		MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
		extender.init();

		controller.record.DocumentNo__c = '123456';

		Test.startTest();
		extender.doSave();
		Test.stopTest();

		List<Apexpages.Message> messages = ApexPages.getMessages();
		for (Apexpages.Message error : messages)  {
    		System.assertEquals('資料名を入力してください。',error.getSummary());
  		}
	}

	@isTest
	private static void doSaveDocumentNoBlankErrorTest(){

  		I_ContentMaster__c content = new I_ContentMaster__c();

		PageReference PageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(PageRef);
		ApexPages.standardController stdController = new ApexPages.standardController(content);
		MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
		MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
		extender.init();

		controller.record.Name = 'テスト長期障害保険';

		Test.startTest();
		extender.doSave();
		Test.stopTest();

		List<Apexpages.Message> messages = ApexPages.getMessages();
		for (Apexpages.Message error : messages)  {
    		System.assertEquals('資料管理番号を入力してください。',error.getSummary());
  		}

	}

	@isTest
	private static void doSaveAlreadyUpsertKeyErrorTest(){

		I_ContentMaster__c parentContent = new I_ContentMaster__c(Name = '販売促進資料');
		insert parentContent;

  		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険', DocumentNo__c = '123456', ParentContent__c = parentContent.Id);
  		insert content;

		PageReference PageRef = Page.MNT_SalesToolEdit;
		Test.setCurrentPage(PageRef);
		ApexPages.currentPage().getParameters().put('id', content.Id);
		ApexPages.currentPage().getParameters().put('clone','1');
        ApexPages.standardController stdController = new ApexPages.StandardController(content);
        MNT_SalesToolEditController controller = new MNT_SalesToolEditController(stdController);
        MNT_SalesToolEditExtender extender = new MNT_SalesToolEditExtender(controller);
        extender.init();

		Test.startTest();
		extender.doSave();
		Test.stopTest();

		List<Apexpages.Message> messages = ApexPages.getMessages();
		for (Apexpages.Message error : messages)  {
    		System.assertEquals('資料名と資料管理番号が同一の資料がございます。',error.getSummary());
  		}

	}

}