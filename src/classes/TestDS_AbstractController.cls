@isTest
private class TestDS_AbstractController {

/*
* 観点: dlFormNo_getter 正常系 
* 検証: dlFormNo取得時、値が返されること
*/
	@isTest static void dlFormNo_getter_test01() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		String createdDlFormNo = ctrl.dlFormNo;
		Test.stopTest();

		System.assertNotEquals(createdDlFormNo, null);
	}


/*
* 観点: getAppName 正常系 
* 検証: getAppName呼出時、値が返されること
*/
	@isTest static void getAppName_test01() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		String returnedAppName = ctrl.getAppName();
		Test.stopTest();

		System.assertNotEquals(returnedAppName, null);
	}


/*
* 観点: PageAction/init 正常系 
* 検証: ログが生成され、initで生成されたPageReferenceが返される
*/
	@isTest static void pageAction_test01() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		PageReference pr = ctrl.PageAction();
		Test.stopTest();

		List<E_Log__c> logList = [SELECT ID FROM E_Log__c];
		System.assertEquals(1, logList.size());
		System.assertNotEquals(null, pr);
	}

/*
* 観点: PageAction/init 正常系
* 条件: IRIS権限のないユーザで実行 
*/
	@isTest static void pageAction_test02() {
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestE_TestUtil.createUser(false,'testName', 'E_PartnerCommunity');
		u.contactId = con.Id;
		insert u;

		PageReference pr;
		System.runAs(u){
			Test.startTest();
			DS_IndexController ctrl = new DS_IndexController();
			pr = ctrl.PageAction();
			Test.stopTest();
		}

		List<E_Log__c> logList = [SELECT ID FROM E_Log__c];
		System.assertEquals(1, logList.size());
		System.assertNotEquals(null, pr);
	}

/*
* 観点: PageAction/init 異常系
* 条件: IRIS権限のあるユーザで実行 
*/
	@isTest static void pageAction_test03() {
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestE_TestUtil.createUser(false,'testName', 'E_PartnerCommunity');
		u.contactId = con.Id;
		insert u;
		TestI_TestUtil.createIDCPF(true, u.Id, 'AH');

		PageReference pr;
		System.runAs(u){
			Test.startTest();
			DS_IndexController ctrl = new DS_IndexController();
			pr = ctrl.PageAction();
			Test.stopTest();
		}

		System.assertEquals(null, pr);
	}


/*
* 観点: createDSLog 正常系 
* 条件: PageAction実行後に実行 
* 検証: PageActionで作成したログのdetail__cが更新されること
*/
	@isTest static void createDSLog_test01() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		ctrl.PageAction();
		List<E_Log__c> beforeLog = [SELECT ID, detail__c FROM E_Log__c];
		ctrl.createDSLog();
		Test.stopTest();

		List<E_Log__c> afterlog = [SELECT ID, detail__c FROM E_Log__c];
		System.assertEquals(beforeLog.size() , afterlog.size());
		System.assertEquals(1 , afterlog.size());
		System.assertEquals(beforeLog[0].Id , afterlog[0].Id);
		System.assertNotEquals(beforeLog[0].detail__c , afterlog[0].detail__c);
	}

/*
* 観点: createDSLog 異常系 
* 条件: PageAction未実行で実行(Exception発生) 
* 検証: pageMessageにメッセージが追加されたこと 
*/
	@isTest static void createDSLog_test02() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		ctrl.createDSLog();
		Test.stopTest();

		System.assert(ctrl.pageMessages.hasMessages());
	}


/*
* 観点: doDownloadPdf 正常系 
* 条件: doDownloadPdf実行後にgetDownloadId 
* 検証: ダウンロード履歴が作成されること
*/
	@isTest static void doDownloadPdf_test01() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		ctrl.doDownloadPdf();
		Test.stopTest();

		List<E_DownloadHistorry__c> dlHstr = [SELECT Id FROM E_DownloadHistorry__c];

		System.assertEquals(1, dlHstr.size());
	}


/*
* 観点: getDownloadId 正常系 
* 条件: doDownloadPdf実行後にgetDownloadIdを実行
* 観点: doDownloadPdfで作成されたIdが返されること
*/
	@isTest static void getDownloadId_test01() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		ctrl.doDownloadPdf();
		String createdDLId =  ctrl.getDownloadId();
		Test.stopTest();

		List<E_DownloadHistorry__c> dlHstr = [SELECT Id FROM E_DownloadHistorry__c];

		System.assertEquals(1, dlHstr.size());
		System.assertEquals(dlHstr[0].Id ,createdDLId);
	}

/*
* 観点: getDownloadId 異常系 
* 条件: doDownloadPdf未実行で実行 
* 検証: 空文字が返されること
*/
	@isTest static void getDownloadId_test02() {
		Test.startTest();
		DS_IndexController ctrl = new DS_IndexController();
		String createdDLId =  ctrl.getDownloadId();
		Test.stopTest();

		System.assert(String.isBlank(createdDLId));
	}

}