@isTest
private class TestI_SearchBoxController {
	
	@isTest static void accessUserATTest() {
		User actUser = createUser('AT');

		System.runAs(actUser){
			Test.startTest();
				I_SearchBoxController isb = new I_SearchBoxController();
				//ATのとき、代理店/募集人検索ができないため選択リストは1
				//コンテンツ検索追加のため3に変更
				System.assertEquals(3, isb.searchCategoryItems.size());
			Test.stopTest();
		}
	}
	
	@isTest static void accessUserAHTest() {
		User actUser = createUser('AH');

		System.runAs(actUser){
			Test.startTest();
				I_SearchBoxController isb = new I_SearchBoxController();
				//AT以外の時は、顧客検索と代理店/募集人検索で選択リストが2
				//コンテンツ検索追加のため4に変更
				System.assertEquals(4, isb.searchCategoryItems.size());
			Test.stopTest();
		}
	}

	@isTest static void defaultSearchBoxTest() {
		User actUser = createUser('AH');

		System.runAs(actUser){
			Test.startTest();
				I_SearchBoxController isb = new I_SearchBoxController();
				System.assertEquals(isb.defaultSearchBox,I_Const.SEARCH_CATEGORY_POLICY);
			Test.stopTest();
		}
	}

	@isTest static void getDISCLAIMERTest() {
		Test.startTest();
			I_SearchBoxController isb = new I_SearchBoxController();
			isb.getDISCLAIMER();
		Test.stopTest();
	}

	static User createUser(String idType){

		//親代理店
		Account parent = TestI_TestUtil.createAccount(true,null);
		//事務所
		Account acc = TestI_TestUtil.createAccount(true,parent);
		//募集人
		Contact con = TestI_TestUtil.createContact(true, acc.Id, 'テスト募集人');
		//ユーザ
		User us = TestI_TestUtil.createAgentUser(true, 'fstest',E_Const.PROFILE_E_PARTNERCOMMUNITY_IA,con.Id);
		//ID管理
		E_IDCPFTriggerHandler.isSkipTriggerActions = true;
		E_IDCPF__c idcp = TestI_TestUtil.createIDCPF(true, us.Id, idType);

		return us;
	}
}