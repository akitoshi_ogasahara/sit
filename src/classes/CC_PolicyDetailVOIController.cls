/*
 * CC_PolicyDetailVOIController
 * Controller CC_PolicyDetailLayout of Vlocity Layout
 * created  : Accenture 2018/10/2
 * modified :
 */

global with sharing class CC_PolicyDetailVOIController implements Vlocity_ins.VlocityOpenInterface2{
	global Object invokeMethod(String methodName, Map<String,Object> inputs, Map<String,Object> output, Map<String,Object> options){
		String strOwnInfo = 'className: ' + String.valueOf(this).split(':')[0] + ', methodName: ' + methodName;
		System.debug('[Execute invokeMethod begin] ' + strOwnInfo);

		Boolean success = true;
		try{
			if(methodName == 'getData'){
				success = getData(inputs, output, options);
			}

			if(Test.isRunningTest()) throw new DMLException();

		}catch(Exception e){
			System.debug('Error invoke method: ' + methodName + ' with error: '+ e);
			output.put('error', e);
			success = false;
		}

		System.debug('[Execute invokeMethod end] ' + strOwnInfo);
		return success;
	}

	/**
	 * Get data
	 */
	public static Boolean getData(Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> optionMap){
		Boolean success = true;
		List<Object> dataSetList = new List<Object>();
		List<E_Policy__c> policyList = getPolicyById((Id)inputMap.get('Id'));

		for(E_Policy__c obj : policyList){
			Map<String, Object> policyMap = new Map<String, Object>(obj.getPopulatedFieldsAsMap());

			// 不要データ削除
			policyMap.remove('RecordTypeId');
			policyMap.remove('Contractor__c');
			policyMap.remove('Contractor__r');
			policyMap.remove('E_COVPFs__r');
			policyMap.remove('AccessibleCustomer__c');

			// ロール:SYSROLE
			String strValue = '';
			if(obj.Contractor__c != null){
				if(obj.Contractor__c == obj.Id || obj.Contractor__r.AccountId == obj.Id || obj.Contractor__c == obj.AccessibleCustomer__c){
					strValue = 'OW';
				}else{
					strValue = 'BN';
				}
				policyMap.put('SYSROLE', (Object)strValue);
			}

			// 保険種類:SYSZCOVRNAM
			strValue = '';
			if(obj.COMM_ZCOVRNAM__c != null){
				strValue = obj.COMM_ZCOVRNAM__c;
			}
			if(obj.E_COVPFs__r.size() != 0 && obj.E_COVPFs__r[0].COMM_ZCOVRNAM__c != null){
				if(strValue != '') strValue += '<br/>';
				strValue += obj.E_COVPFs__r[0].COMM_ZCOVRNAM__c;
			}
			if(strValue != ''){
				policyMap.put('SYSZCOVRNAM', (Object)strValue);
			}

			dataSetList.add(policyMap);
		}

		outputMap.put('dataSet', dataSetList);

		return success;
	}

	/**
	 * Object: E_Policy__c
	 * Parameter: inputId (Account id or Contact id)
	 * return: E_Policy__c
	 */
	public static List<E_Policy__c> getPolicyById(Id inputId){
		List<E_Policy__c> policyList = [
				SELECT Id,
				  AccessibleCustomer__c,
				  CC_AgentAccountName__c,
				  CC_Is16ManagedName__c,
				  CMN_ContractDateJP__c,
				  CMN_Is16Managed__c,
				  CMN_NextPaymentDateJP__c,
				  CMN_PaymentMethod__c,
				  COLI_GRUPNUM__c,
				  COMM_CHDRNUM__c,
				  COMM_SINSTAMT__c,
				  COMM_STATCODE__c,
				  COMM_ZCOVRNAM__c,
				  Contractor__r.AccountId,
				  Contractor__r.E_CLTPF_CLTTYPE__c,
				  InsuredName__c,
				  SubAgentAccountName__c,
				  (SELECT COMM_ZCOVRNAM__c FROM E_COVPFs__r WHERE COLI_ZCRIND__c = 'C')
				FROM E_Policy__c
				WHERE AccessibleCustomer__c = :inputId
				  OR Contractor__c = :inputId
				  OR (AccessibleCustomer__r.AccountId = :inputId AND AccessibleCustomer__r.E_CLTPF_CLTTYPE__c = 'C')
				  OR (Contractor__r.AccountId = :inputId AND Contractor__r.E_CLTPF_CLTTYPE__c = 'C')
				LIMIT 10000];

		return policyList;
	}
}