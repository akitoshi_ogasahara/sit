/**
 * E_createNNLinkIDのテストクラス
 * CreatedDate 2016/10/04
 * @author Terrasky
 */
@isTest
private class E_createNNLinkIDTest {

    //テストデータ作成メソッド
    static void createData(String idNum){
         E_createNNLinkID ecnid = new E_createNNLinkID();
         
         E_IDCPF__c data = new E_IDCPF__c(ZWEBID__c = idNum);
         insert data;
    }
	
	@isTest //テストデータがない場合のテスト
    static void testE_createNNLinkID0() {

        //テストデータ作成
        createData('1000000000');
        //insert ecnid;

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000019');

        Test.stopTest();

	}
	
	@isTest //テストデータがある場合のテスト。チェックディジットが1のとき
    static void testE_createNNLinkID1() {
		
        //テストデータ作成
        createData('5000010010');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000010021');

        Test.stopTest(); 
	}

    @isTest //テストデータがある場合のテスト。チェックディジットが2のとき
    static void testE_createNNLinkID2() {
        
        //テストデータ作成
        createData('5000000080');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000092');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが3のとき
    static void testE_createNNLinkID3() {
        
        //テストデータ作成
        createData('5000000070');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000083');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが4のとき
    static void testE_createNNLinkID4() {
        
        //テストデータ作成
        createData('5000000060');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000074');

        Test.stopTest(); 
    }
	
    @isTest //テストデータがある場合のテスト。チェックディジットが5のとき
    static void testE_createNNLinkID5() {
        
        //テストデータ作成
        createData('5000000050');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000065');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが6のとき
    static void testE_createNNLinkID6() {
        
        //テストデータ作成
        createData('5000000040');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000056');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが7のとき
    static void testE_createNNLinkID7() {
        
        //テストデータ作成
        createData('5000000030');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000047');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが8のとき
    static void testE_createNNLinkID8() {
        
        //テストデータ作成
        createData('5000000020');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000038');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが9のとき
    static void testE_createNNLinkID9() {
        
        //テストデータ作成
        createData('5000000010');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000000029');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが9(10)のとき
    static void testE_createNNLinkID10() {
        
        //テストデータ作成
        createData('5000010110');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000010129');

        Test.stopTest(); 
    }

    @isTest //テストデータがある場合のテスト。チェックディジットが1(11)のとき
    static void testE_createNNLinkID11() {
        
        //テストデータ作成
        createData('5000010100');

        Test.startTest();

        String idNumber = E_createNNLinkID.createNNLinkID();
        System.assertEquals(idNumber,'5000010111');

        Test.stopTest(); 
    }
}