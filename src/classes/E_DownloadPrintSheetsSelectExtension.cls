public with sharing class E_DownloadPrintSheetsSelectExtension {
	
	E_DownloadController extention;

	// [IRIS]IRIS判定用
	public E_IRISHandler iris;

	//ラジオボタン選択値
	public String selectTerm {get;set;}
	
	//検索後の場合True
	public Boolean isSearched {get;set;}
	
	//代理店ルックアップ
	public Contact iDistoributor {get; set;}
	//代理店事務所ルックアップ
	public Contact iOffice {get; set;}
	//募集人ルックアップ
	public E_Policy__c iAgent {get; set;}
	
	//ラジオボタンの値
	public String getCS_DISTORIBUTOR() {
		return E_Const.CS_DISTORIBUTOR;
	}
	public String getCS_OFFICE() {
		return E_Const.CS_OFFICE;
	}
	public String getCS_AGENT() {
		return E_Const.CS_AGENT;
	}
	
	public String getTitle(){
		return 'ダウンロード帳票選択';
	}
	//コンストラクタ
	public E_DownloadPrintSheetsSelectExtension(E_DownloadController extention) {
		this.extention = extention;
		extention.setPgTitle(getTitle());
		extention.pageRef = extention.init();

		//UX2残案件_6 ここから
		isSearched = false;
		String searchId;
		String strSearched = '';

		strSearched = ApexPages.currentPage().getParameters().get('isSearched');
		if(String.isNotBlank(strSearched)){
			isSearched = Boolean.valueOf(strSearched);
			selectTerm = ApexPages.currentPage().getParameters().get('selectTerm');
			searchId = ApexPages.currentPage().getParameters().get('searchId');
		}


//system.debug('@@@' + extention.pageRef);
		if(isSearched&&extention.getIsEmployee()){
			if(extention.pageRef == null){
				iris = E_IRISHandler.getInstance();
				iDistoributor = new Contact();
				iOffice = new Contact();
				iAgent = new E_Policy__c();
				String conditionValue = searchId;
				if(selectTerm == E_Const.CS_DISTORIBUTOR){
					iDistoributor.AccountId = conditionValue;
					//代理店を取得
					extention.distributor = E_AccountDaoWithout.getRecById(iDistoributor.accountid);
					extention.office = null;
					extention.agent = null;
					
				} else if(selectTerm == E_Const.CS_OFFICE){
					iOffice.AccountId = conditionValue;
					//代理店事務所を取得
					extention.distributor = null;
					extention.office = E_AccountDaoWithout.getRecById(iOffice.accountid);
					extention.agent = null;
					
				} else if(selectTerm == E_Const.CS_AGENT){
					iAgent.MainAgent__c = conditionValue;
					//募集人を取得
					extention.distributor = null;
					extention.office = null;
					extention.agent = E_ContactDaoWithout.getRecByIdWithout(iAgent.MainAgent__c);
				}
			}
		}else{
		//UX2残案件_6 ここまで
			if(extention.pageRef == null){
				iris = E_IRISHandler.getInstance();
				selectTerm = E_Const.CS_DISTORIBUTOR;
				isSearched = false;
				iDistoributor = new Contact();
				iOffice = new Contact();
				iAgent = new E_Policy__c();
				
				if(extention.getIsEmployee()){
					//社員ユーザのときは検索画面を表示
					isSearched = false;
				} else {
					//社員ユーザでないときは検索画面を非表示
					isSearched = true;
					extention.getActor_isAgent();
				}
			}
		}
	}
	
	//ページアクション
	public PageReference pageAction(){
		PageReference pr = E_Util.toErrorPage(extention.pageRef, null);
		if(pr != null){
			return pr;
		}

		//if(!isSearched){
			// IRIS 検索条件セット
			if(iris.getIsIRIS() && iris.isSetPolicyCondition()){
				// フィルタ項目と値を取得
				String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
				String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

				if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
					// 代理店フィルタ
					if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
						selectTerm = E_Const.CS_DISTORIBUTOR;
						iDistoributor.AccountId = conditionValue;
					// 事務所フィルタ
					} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
						selectTerm = E_Const.CS_OFFICE;
						iOffice.AccountId = conditionValue;
					// 募集人フィルタ
					} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
						selectTerm = E_Const.CS_AGENT;
						iAgent.MainAgent__c = conditionValue;
					// 営業部フィルタ・MRフィルタ
					} else if(conditionField == I_Const.CONDITION_FIELD_UNIT || conditionField == I_Const.CONDITION_FIELD_MR){
						//営業部とMRの場合はdoSearchを行わない
						return pr;
					}
					pr = doSearch();
				}
			}
		//}else{
		//	if(iris.getIsIRIS() && iris.isSetPolicyCondition()){
		//		// フィルタ項目と値を取得
		//		String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
		//		String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);
		//		if(selectTerm == E_Const.CS_DISTORIBUTOR){
		//			iDistoributor.AccountId = conditionValue;
		//			//代理店を取得
		//			extention.distributor = E_AccountDaoWithout.getRecById(iDistoributor.accountid);
		//			extention.office = null;
		//			extention.agent = null;
					
		//		} else if(selectTerm == E_Const.CS_OFFICE){
		//			iOffice.AccountId = conditionValue;
		//			//代理店事務所を取得
		//			extention.distributor = null;
		//			extention.office = E_AccountDaoWithout.getRecById(iOffice.accountid);
		//			extention.agent = null;
					
		//		} else if(selectTerm == E_Const.CS_AGENT){
		//			iAgent.MainAgent__c = conditionValue;
		//			//募集人を取得
		//			extention.distributor = null;
		//			extention.office = null;
		//			extention.agent = E_ContactDaoWithout.getRecByIdWithout(iAgent.MainAgent__c);
		//		}
		//	}
		//}

		return pr;
	}
	
	//ラジオボタン押下処理
	public Pagereference reDraw(){
		iDistoributor.accountid = null;
		iOffice.accountid = null;
		iAgent.MainAgent__c = null;
		return null;
	}
	
	//検索ボタン
	public PageReference doSearch(){
		extention.pageMessages.clearMessages();
		isSearched = checkSearch();
		return null;
	}
	
	//ダウンロード可能な帳票を判定
	private Boolean checkSearch(){
		Boolean ret = false;
/*
		//募集人選択時、募集人の権限によりselectTermを再設定する
		if(selectTerm == E_Const.CS_AGENT){
			//入力チェック
			if(iAgent.MainAgent__c == null){
				extention.pageMessages.addErrorMessage('募集人を選択してください。');
				return ret;
			}
			selectTerm = resetSelectTerm(iAgent.MainAgent__c);
			if(String.isBlank(selectTerm)){
				selectTerm = E_Const.CS_AGENT;
				return ret;
			}
		}
*/
		
		if(selectTerm == E_Const.CS_DISTORIBUTOR){
			//代理店の場合
			//入力チェック
			if(iDistoributor.accountid == null){
				extention.pageMessages.addErrorMessage('代理店を選択してください。');
				return ret;
			}
			//代理店を取得
			extention.distributor = E_AccountDaoWithout.getRecById(iDistoributor.accountid);
			extention.office = null;
			extention.agent = null;
			
		} else if(selectTerm == E_Const.CS_OFFICE){
			//代理店事務所の場合
			//入力チェック
			if(iOffice.accountid == null){
				extention.pageMessages.addErrorMessage('代理店事務所を選択してください。');
				return ret;
			}
			//代理店事務所を取得
			extention.distributor = null;
			extention.office = E_AccountDaoWithout.getRecById(iOffice.accountid);
			extention.agent = null;
		} else if(selectTerm == E_Const.CS_AGENT){
			//募集人の場合
			//入力チェック
			if(iAgent.MainAgent__c == null){
				extention.pageMessages.addErrorMessage('募集人を選択してください。');
				return ret;
			}
			//募集人を取得
			extention.distributor = null;
			extention.office = null;
			extention.agent = E_ContactDaoWithout.getRecByIdWithout(iAgent.MainAgent__c);
		}
		return true;
	}
	
/*
	//募集人の権限によりselectTermを再設定する
	private String resetSelectTerm(Id ContactId){
		E_IDCPF__c idcpf = E_IDCPFDaoWithout.getRecsByContactId(ContactId);
		if(idcpf != null && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AH){
			iDistoributor = new Contact(accountid = idcpf.User__r.Contact.Account.ParentId);
			return E_Const.CS_DISTORIBUTOR;
			
		}else if(idcpf != null && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AY){
			iOffice = new Contact(accountid = idcpf.User__r.AccountId);
			return E_Const.CS_OFFICE;
			
		}else if(idcpf != null && idcpf.ZIDTYPE__c == E_Const.ZIDTYPE_AT){
			return E_Const.CS_AGENT;
			
		}
		extention.pageMessages.addErrorMessage('選択された募集人には帳票を選択する権限がありません。');
		return null;
	}
*/
	
	//戻るボタン
	public PageReference doBack(){
		extention.pageMessages.clearMessages();
		//社員ユーザの場合、検索画面に戻る
		isSearched = false;
		extention.distributor = null;
		extention.office = null;
		extention.agent = null;
		return null;
	}

	//手数料のご案内
	public PageReference doDownloadFeeGuidanceSearch(){
		PageReference ret = Page.E_DownloadFeeGuidanceSearch;
		setUrlParam(ret, extention.KEY_DID);

		return ret;
	}
	
	//手数料明細(生保)
	public PageReference doDownloadFeeDataSearchAgspf(){
		PageReference ret = Page.E_DownloadFeeDataSearch;
		ret.getParameters().put(extention.KEY_PT, extention.VAL_COLI);
		setUrlParam(ret, extention.KEY_OID);
		return ret;
	}
	
	//手数料明細(変額商品)
	public PageReference doDownloadFeeDataSearchSpva(){
		PageReference ret = Page.E_DownloadFeeDataSearch;
		ret.getParameters().put(extention.KEY_PT, extention.VAL_SPVA);
		setUrlParam(ret, extention.KEY_OID);
		return ret;
	}
	
	//手数料明細(ボーナス)
	public PageReference doDownloadFeeDataSearchBospf(){
		PageReference ret = Page.E_DownloadFeeDataSearch;
		ret.getParameters().put(extention.KEY_PT, extention.VAL_BONUS);
		setUrlParam(ret, extention.KEY_OID);
		return ret;
	}
	
	//保有契約（個人保険）
	public PageReference doDownloadHoldPolicySearchColi(){
		PageReference ret = Page.E_DownloadHoldPolicySearch;
		ret.getParameters().put(extention.KEY_PT, extention.VAL_COLI);
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	
	//保有契約（SPVA）
	public PageReference doDownloadHoldPolicySearchSpva(){
		PageReference ret = Page.E_DownloadHoldPolicySearch;
		ret.getParameters().put(extention.KEY_PT,extention.VAL_SPVA);
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	
	//手続の履歴
	public PageReference doDownloadPolicyHistorySearch(){
		PageReference ret = Page.E_DownloadPolicyHistorySearch;
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	
	//保有契約異動通知(AGPO)
	public PageReference doDownloadNoticeAGPO(){
		PageReference ret = Page.E_DownloadNoticeAGPO;
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	//保険料請求予告通知(BILA)
	public PageReference doDownloadNoticeBILA(){
		PageReference ret = Page.E_DownloadNoticeBILA;
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	
	//保険料請求通知(BILS)
	public PageReference doDownloadNoticeBILS(){
		PageReference ret = Page.E_DownloadNoticeBILS;
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	//保険料未入金通知(NCOL)
	public PageReference doDownloadNoticeNCOL(){
		PageReference ret = Page.E_DownloadNoticeNCOL;
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	
	//新契約ステータス
	public PageReference doDownloadNewPolicy(){
		PageReference ret = Page.E_DownloadNewPolicy;
		setUrlParam(ret, extention.KEY_AID);
		return ret;
	}
	
	//URLパラメータに代理店・代理店事務所・募集人のIDを設定
	private void setUrlParam(PageReference pageRef, String param){
		//社員ユーザ または IRISでフィルタ適用中 の場合、パラメータ付与
//		if(extention.getIsEmployee()){
		if(extention.getIsEmployee() || (iris.getIsIRIS() && iris.isSetPolicyCondition())){
			Map<String, String> urlParam = pageRef.getParameters();
			if(extention.distributor <> null && extention.distributor.Id <> null){
//system.debug('@@@:distributor');
				urlParam.put(extention.KEY_DID, extention.distributor.Id);
			}else if(extention.office <> null && extention.office.Id <> null){
//system.debug('@@@:office');
				urlParam.put(extention.KEY_OID, extention.office.Id);
			}else{
//system.debug('@@@:agent');
				if(param == extention.KEY_DID){
					urlParam.put(extention.KEY_DID, extention.agent.Account.ParentId);
				}else if(param == extention.KEY_OID){
					urlParam.put(extention.KEY_OID, extention.agent.AccountId);
				}else{
					urlParam.put(extention.KEY_AID, extention.agent.Id);
				}
			}
		}
	}
}