public with sharing class ASS_EventInputPageController {

	//行動
	public Event event {get; private set;}
	//行動のID
	public String eventId {get; private set;}
	//編集モード
	public Boolean editMode {get; private set;}
	//事務所リスト
	public List<Account> accounts {get; private set;}
	//事務所の選択リスト
	public List<SelectOption> selOptsAccount {get; private set;}

	//遷移元のVFページ名
	public String retPgName;
	//MRId
	public String mrId;
	//営業部ID
	public String unitId;
	//代理店事務所ID
	public String accountId;
	//日付
	public String day;


	// constructor. do initializations
	public ASS_EventInputPageController () {
		retPgName = ApexPages.currentPage().getParameters().get('retPgName');
		mrId = ApexPages.currentPage().getParameters().get('mrId');
		unitId = ApexPages.currentPage().getParameters().get('unitId');
		eventId = ApexPages.currentPage().getParameters().get('eventId');
		accountId = ApexPages.currentPage().getParameters().get('accountId');
		day = ApexPages.currentPage().getParameters().get('date');
		this.init();
	}

	private void init() {
		editMode = String.isEmpty(eventId);

		accounts = ASS_AccountDao.getOfiicesByMRIdANDLoginUsId(mrId,UserInfo.getUserId());

		selOptsAccount = new List<SelectOption>();
		for (Account acc : accounts) {
			selOptsAccount.add(new SelectOption(acc.Id, acc.Name));
		}

		if (String.isEmpty(eventId)) {
			RecordType recordType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
			event = new Event(RecordTypeId = recordType.Id, OwnerId = UserInfo.getUserId());
			//代理店事務所リストから遷移した場合には、事務所名をデフォルトで入れる
			if(accountId != null) {
				event.WhatId = accountId;
			}
			//カレンダーから遷移した場合には、その日付をデフォルトで開始、終了に入れる
			if(day != null) {
				event.StartDateTime = Datetime.valueOf(day +' '+ String.valueOf(Datetime.now().hour() +':00:00'));
				event.EndDateTime = Datetime.valueOf(day +' '+ String.valueOf(Datetime.now().hour() +':00:00')).addHours(1);
			//日付がない場合には、本日の日付を入れる
			} else {
				event.StartDateTime = Datetime.valueOf(String.valueOf(Date.today()) +' '+ String.valueOf(Datetime.now().hour() +':00:00'));
				event.EndDateTime = Datetime.valueOf(String.valueOf(Date.today()) +' '+ String.valueOf(Datetime.now().hour() +':00:00')).addHours(1);
			}
		}
		else {
			event = ASS_EventDao.getEventsById(eventId);
		}
	}

	//保存
	public PageReference save() {
		if(event.StartDateTime > event.EndDateTime) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'開始日および開始時刻は、終了日および終了時刻と同じかそれよりも前に設定してください。'));
			return null;
		}
		try {
			upsert event;
		} catch (DMLException e) {

			for (Integer i = 0; i < e.getNumDml(); i++) {
				ApexPages.addMessages(e);
			}
			return null;
		}
		
		PageReference pr = new PageReference(retPgName);
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		if(String.isNotEmpty(accountId)){
			pr.getParameters().put('accountId',accountId);
		}
		pr.setRedirect(true);
		return pr;  
	}

	//編集
	public PageReference edit() {
		editMode = true;
		return ApexPages.currentPage();
	}

	//参照キャンセル
	public PageReference cancel() {
		PageReference pr = new PageReference(retPgName);
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		if(String.isNotEmpty(accountId)){
			pr.getParameters().put('accountId',accountId);
		}
		pr.setRedirect(true);
		return pr;
	}

	//編集キャンセル
	public PageReference cancelEdit() {
		editMode = false;
		return ApexPages.currentPage();
	}

	//削除
	public PageReference del() {
		ASS_EventDaoWithout.deleteEvent(event);
		
		PageReference pr = new PageReference(retPgName);
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		if(String.isNotEmpty(accountId)){
			pr.getParameters().put('accountId',accountId);
		}
		pr.setRedirect(true);
		return pr;
	}
}