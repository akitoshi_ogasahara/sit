public with sharing class CR_AgencyDao {
	
	public static Map<String, CR_Agency__c> getCR_AgencysByAgencyCD(String agcd){
		Map<String, CR_Agency__c> agencys = new Map<String, CR_Agency__c>();
		if(String.isNotBlank(agcd)){
			for(CR_Agency__c ag:[SELECT Id,Name,RecordType.DeveloperName
								  FROM CR_Agency__c
								  WHERE AgencyCode__c =: agcd]){
				agencys.put(ag.RecordType.DeveloperName, ag);
			}
		}
		return agencys;
	}
	
	/**
	 *		SystemModStamp確認のためのレコード取得
	 */
	public static CR_Agency__c getAgencyModStampById(Id recId){
		for(CR_Agency__c ag:[SELECT Id,ApprovalDate__c,SystemModStamp 
								FROM CR_Agency__c
								WHERE Id = :recId]){
			return ag;
		}
		return null;
	}	
	
	public static CR_Agency__c getRefreshedCommentById(Id recId){
		for(CR_Agency__c ag:[SELECT Id,Comment__c,SystemModStamp 
								FROM CR_Agency__c
								WHERE Id = :recId]){
			return ag;
		}
		return null;
	}
	
	/*
	 *		大規模代理店レコード取得
	 */
	public static List<CR_Agency__c> getLargeAgencysByAccountIds(Set<Id> accIds){
		return [SELECT Id,Name,LargeAgency__c,Account__c
				FROM CR_Agency__c
				WHERE LargeAgency__c = true
				  AND Account__c in :accIds
				  AND RecordType.DeveloperName = :CR_Const.RECTYPE_DEVNAME_BIZ];
	}

	/**
	 * IDをキーに最新作成依頼日確認のためのレコード取得
	 */
	public static CR_Agency__c getAgencyLastRequestById(Id recId){
		for(CR_Agency__c ag : [Select Id, LastRequestReport__c, LastRequestBook__c 
								From CR_Agency__c 
								Where Id = :recId]){
			return ag;
		}
		return null;
	}
}