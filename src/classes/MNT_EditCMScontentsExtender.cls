global with sharing class MNT_EditCMScontentsExtender extends SkyEditor2.Extender{
    MNT_EditCMScontents extension;
    public MNT_EditCMScontentsExtender(MNT_EditCMScontents extension){
        this.extension = extension;
    }

    public string parentCompName {get;set;}
    
    global override void init(){
        ApexPages.PageReference page = ApexPages.currentPage();
        //パラメータで渡された親コンポーネントのNameを取得
        string parentContentId = page.getParameters().get('ParentContent__c');
        // Requestパラメータ取得(コピーの場合)
        String clone = page.getParameters().get('clone');
        I_ContentMaster__c record = extension.record;
        //新規作成の場合
        if(clone == null){
            //パラメータで渡された親コンポーネントがNULLでない場合parentCompNameを設定
            if( parentContentId != null ){
                parentCompName = [select Id,name from I_ContentMaster__c where Id = :parentContentId].Name;
            }
        } else if(clone != null && clone.equals('1')){
            //コピーの場合
            //コピー時初期化処理
            record.UpsertKey__c = null;
        }

    }    
}