@isTest
private class TestE_InveHistoryController {
	
	//コンストラクタ
	//URLパラメータあり
	static testMethod void constructorTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			//レコード作成
			E_ITHPF__c ithpf = new E_ITHPF__c();
			insert ithpf;
			ApexPages.CurrentPage().getParameters().put('ithpfId', ithpf.Id);
			
			Test.startTest();
			E_InveHistoryController result = new E_InveHistoryController();
			Test.stopTest();
			
			System.assertEquals(ithpf.Id, result.record.Id);
		}
	}
	
	//コンストラクタ
	//URLパラメータなし
	static testMethod void constructorTest002(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			Test.startTest();
			E_InveHistoryController result = new E_InveHistoryController();
			Test.stopTest();
			
			System.assertEquals(null, result.record);
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveHistoryController controller = createController();
			
			Test.startTest();
			PageReference result = controller.pageAction();
			Test.stopTest();
			
			System.assertEquals(null, result);
		}
	}
	
	//テストユーザ作成
	static User createUser(String flag03){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG03__c = flag03;
		insert idcpf;
		return u;
	}
	
	//コントローラ作成
	static E_InveHistoryController createController(){
		E_ITHPF__c ithpf = new E_ITHPF__c();
		insert ithpf;
		ApexPages.CurrentPage().getParameters().put('ithpfId', ithpf.Id);
		E_InveHistoryController controller = new E_InveHistoryController();
		return controller;
	}
	
}