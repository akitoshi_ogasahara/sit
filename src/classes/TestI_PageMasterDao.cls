@isTest
private class TestI_PageMasterDao {
    //--- getRecordByIdメソッド ---
    static testMethod void getRecordByIdTest(){
        //テストデータ作成
        I_PageMaster__c ipm = new I_PageMaster__c(Name='よくあるご質問');
		INSERT ipm;
        
        String pid = ipm.Id;
        I_PageMaster__c returned = I_PageMasterDao.getRecordById(pid); //テスト対象メソッド
        System.assertNotEquals(null, returned);
    }
    // nullが返る場合
    static testMethod void getRecordByIdTest_returnNull(){
         //テストデータ作成
        I_PageMaster__c ipm = new I_PageMaster__c(Name='よくあるご質問');
		INSERT ipm;
        
        String pid = 'XXXXXX'; //nullを返すために存在しない id をセット
        I_PageMaster__c returned = I_PageMasterDao.getRecordById(pid); //テスト対象メソッド
        System.assertEquals(null, returned);
    }

    
    //--- getRecordByKeyメソッド ---
    static testMethod void getRecordByKeyTest(){
        //テストデータ作成
        I_PageMaster__c ipm = new I_PageMaster__c(Name='インターネットサービス利用規定');
        INSERT ipm;
        
        String pkey = ipm.page_unique_key__c;
        I_PageMaster__c returned = I_PageMasterDao.getRecordByKey(pkey); //テスト対象メソッド
		System.assertNotEquals(null, returned);       
    }
    // nullが返る場合
    static testMethod void getRecordByKeyTest_returnNull(){
		//テストデータ作成
        I_PageMaster__c ipm = new I_PageMaster__c(Name='インターネットサービス利用規定');
        INSERT ipm;
        
        String pkey = 'XXXXXXX';	//nullを返すために存在しない page_unique_key__cをセット
        I_PageMaster__c returned = I_PageMasterDao.getRecordByKey(pkey); //テスト対象メソッド
		System.assertEquals(null, returned);  
    }

    
    //--- getRecordsByDefaultメソッド ---    
    static testMethod void getRecordsByDefaultTest(){
        //テストデータ作成
        I_PageMaster__c ipm = new I_PageMaster__c(Name='代理店・募集人登録事務', Default__c = true);
        INSERT ipm;
        
		List<I_PageMaster__c> list_PageMaster = I_PageMasterDao.getRecordsByDefault(); //テスト対象メソッド
        System.assert(list_PageMaster.size() == 1);
    }

    
    //--- getRecordsByCategoryメソッド ---
    static testMethod void getRecordsByCategoryTest(){
        //テストデータ作成
        I_PageMaster__c ipm = new I_PageMaster__c(Name='ライブラリ');
        INSERT ipm;
        
        String mainCategory = '3.ライブラリ';
        String subCategory = '1.トップ';
        List<I_PageMaster__c> list_PageMaster = I_PageMasterDao.getRecordsByCategory(mainCategory, subCategory); //テスト対象メソッド
        System.assertNotEquals(null, list_PageMaster);
    }
}