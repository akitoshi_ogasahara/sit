/**
 * ユニットプライス月次レポート コントローラクラス.
 */
public with sharing class E_UPMR002Controller extends E_AbstractUPInfoController {
	
	/**
	 * コンストラクタ.
	 */
	public E_UPMR002Controller() {
		super();
	}

	/**
	 * #getMenuKey.
	 */
	private String getMenuKey() {
//		return ApexPages.currentPage().getParameters().get('Zprdcd');
		String Zprdcd = E_EncryptUtil.prdCodeConversion(ApexPages.currentPage().getParameters().get('Zprdcd'), false);
		
		//E_UPBPF__c eupbpf = E_UPBPFDao.getRecByZPRDCD(Zprdcd);
		if(Zprdcd !=null){
			return Zprdcd;
		}
		return null;
	}
}