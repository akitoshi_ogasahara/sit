@isTest
private class TestI_UpdateLogBatch {
	public static Date today = Date.newInstance(Date.Today().year(), Date.Today().month()+1,1);
	public static Datetime nowDate;		//バッチ起動日時
	public static E_Log__c log = new E_Log__c();

	//アクションタイプ 'ページ'、契約照会
	@isTest static void pageTest01() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog01();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);

		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);

		I_UpdateLogBatchSched scheduler = new I_UpdateLogBatchSched();
		String jobId = System.schedule('I_UpdateLogBatchTest', '0 0 * * * ?',new I_UpdateLogBatchSched());
	}

	//アクションタイプ 'ページ'、消滅契約
	@isTest static void pageTest02() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog02();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}


	//アクションタイプ 'ページ'、手続き履歴照会ページ、嘱託医詳細ページ
	@isTest static void pageTest03() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog03();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}

	//アクションタイプ '帳票ダウンロード'
	@isTest static void pageTest04() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createDownloadHistory('1');
		//createPageNNlog04();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}

	//アクションタイプ 'ファイルダウンロード',Attachment のとき
	@isTest static void pageTest05() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog05();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}



	//アクションタイプ 'ファイルダウンロード'、Content のとき
	@isTest static void pageTest06() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog06();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}

	//アクションタイプ 'お知らせ'
	@isTest static void pageTest07() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog07();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}

	//アクションタイプ ページ 新契約
	@isTest static void pageTest08() {
		nowDate = Datetime.now();

		String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
						'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
						'FROM E_Log__c ' +
						'WHERE batchDate__c != null ';


		Test.startTest();
		createPageNNlog08();
		I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
		Database.executeBatch(logBatch,200);


		Test.stopTest();
		Integer j =  Database.query(query).size();		//バッチ起動後
		System.assertEquals(j, 1);
	}

	//アクションタイプ ページ 代理店挙績情報 代理店
	@isTest static void pageTest09() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog09();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 2);

		}
	}
	//アクションタイプ ページ 代理店挙績情報 事務所
	@isTest static void pageTest10() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog10();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 2);

		}
	}
	//アクションタイプ ページ 代理店挙績情報 募集人
	@isTest static void pageTest11() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog11();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 2);

		}
	}


	//アクションタイプ ページ 代理店挙績情報 アクセスユーザ:AH
	@isTest static void pageTest12() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog12();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 1);
		}
	}
	//アクションタイプ ページ 代理店挙績情報 アクセスユーザ:AY
	@isTest static void pageTest13() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog13();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 1);
		}
	}
	//アクションタイプ ページ 代理店挙績情報 アクセスユーザ:AT
	@isTest static void pageTest14() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog14();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 1);
		}
	}
	//アクションタイプ ページ 代理店挙績情報 type=office
	@isTest static void pageTest15() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog15();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 2);

		}
	}

	@isTest static void pageTest16() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs(thisUser){
			Test.startTest();
				List<E_DiseaseNumber__c> disease = new List<E_DiseaseNumber__c>();
				disease.add(createDiseaseNumber('1'));
				disease.add(createDiseaseNumber('2'));
				insert disease;

				createDisease(disease[0]);
				createDisease(disease[1]);

				createPageNNlog16();

				I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
				Database.executeBatch(logBatch,200);
			Test.stopTest();

			List<E_Log__c> result = [SELECT Name, SummaryName__c, DisplayURL__c, isDisplay__c, ProcAccessPage__c FROM E_Log__c where isDisplay__c = true ORDER BY createddate];
			for(Integer i=0; i<result.size(); i++){
				System.assertEquals(result[i].SummaryName__c, '医的な引受の目安' + '_テスト傷病');
				System.assertEquals(result[i].DisplayURL__c, result[i].ProcAccessPage__c);
				System.assertEquals(result[i].isDisplay__c, TRUE);
			}
		}
	}

	//アクションタイプ ページ　特集
	@isTest static void pageTest17() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog17();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 2);

		}
	}

	//アクションタイプ パンフレット等発送依頼
	@isTest static void pageTest18() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createPageNNlog18();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 1);

		}
	}

	//PIP PDF出力
	@isTest static void pdfTest01() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs(thisUser){
			Test.startTest();


				E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());

				//リクエスト情報のJSON作成
				String jsonInput = '{';
				jsonInput += '"policyTypes":["ECHDPF"],';
				jsonInput += '"policyId":"' + policy.Id + '"';
				jsonInput += '}';
				createDownloadHistoryPDF('E',jsonInput);


				I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
				Database.executeBatch(logBatch,200);
			Test.stopTest();

			List<E_Log__c> result = [SELECT Name, SummaryName__c, DisplayURL__c, isDisplay__c, ProcAccessPage__c FROM E_Log__c where isDisplay__c = true ORDER BY createddate];
			for(Integer i=0; i<result.size(); i++){
				System.assertEquals(result[i].SummaryName__c, '保険契約概要' + '_Test0002' + '_テストテスト3');
				System.assertEquals(result[i].isDisplay__c, TRUE);
			}
		}
	}

	//PIP PDF出力
	@isTest static void pdfTest02() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs(thisUser){
			Test.startTest();


				E_Policy__c policy = createPolicyDColi(true,UserInfo.getUserId());

				//リクエスト情報のJSON作成
				String jsonInput = '{';
				jsonInput += '"policyTypes":["EHLDPF"],';
				jsonInput += '"policyId":"' + policy.Id + '"';
				jsonInput += '}';
				createDownloadHistoryPDF('E',jsonInput);


				I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
				Database.executeBatch(logBatch,200);
			Test.stopTest();

			List<E_Log__c> result = [SELECT Name, SummaryName__c, DisplayURL__c, isDisplay__c, ProcAccessPage__c FROM E_Log__c where isDisplay__c = true ORDER BY createddate];
			for(Integer i=0; i<result.size(); i++){
				System.assertEquals(result[i].SummaryName__c, '保険契約概要' + '_Test0002' + '_テストテスト3');
				System.assertEquals(result[i].isDisplay__c, TRUE);
			}
		}
	}

	//傷病PDF出力
	@isTest static void pdfTest03() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs(thisUser){
			Test.startTest();
				List<E_DiseaseNumber__c> disease = new List<E_DiseaseNumber__c>();
				disease.add(createDiseaseNumber('1'));
				disease.add(createDiseaseNumber('2'));
				insert disease;

				createDisease(disease[0]);
				createDisease(disease[1]);

				//リクエスト情報のJSON作成
				String jsonInput = '{';
				jsonInput += '"diseaseNumber":"1",';
				jsonInput += '"csvType":"0"';
				jsonInput += '}';
				createDownloadHistoryPDF('F',jsonInput);

				I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
				Database.executeBatch(logBatch,200);
			Test.stopTest();

			List<E_Log__c> result = [SELECT Name, SummaryName__c, DisplayURL__c, isDisplay__c, ProcAccessPage__c FROM E_Log__c where isDisplay__c = true ORDER BY createddate];
			for(Integer i=0; i<result.size(); i++){
				System.assertEquals(result[i].SummaryName__c, '医的な引受の目安' + '_テスト傷病');
				System.assertEquals(result[i].isDisplay__c, TRUE);
			}
		}
	}

	//解約請求書DL PDF出力
	@isTest static void pdfTest04() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs(thisUser){
			Test.startTest();
				String logName = '保険契約概要_00000000_解約請求書（個人）';

				// ログレコード生成
				E_Log__c log = E_LogUtil.createLog();
				log.Name = logName;
				log.SummaryName__c = log.Name;
				log.ActionType__c = I_Const.ACTION_TYPE_PDF;
				log.isDisplay__c = false;
				log.AccessPage__c = '';
				insert log;

				I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
				Database.executeBatch(logBatch,200);
			Test.stopTest();

			// assertion
			List<E_Log__c> results = [SELECT Id, Name, SummaryName__c, DisplayURL__c, isDisplay__c, ProcAccessPage__c FROM E_Log__c where isDisplay__c = true ORDER BY createddate];
			for(E_Log__c result : results){
				System.assertEquals(log.Id, result.Id);
				System.assertEquals(true, result.isDisplay__c);
				System.assertEquals(logName, result.Name);
			}
		}
	}

    	//アクションタイプ 'たもつくん'
	@isTest static void tmtTest12() {
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Test.startTest();

			createTmtlog01();

			I_UpdateLogBatch logBatch = new I_UpdateLogBatch();
			Database.executeBatch(logBatch,200);

			Test.stopTest();

			String query = 'SELECT isDisplay__c,batchDate__c,SummaryName__c,DisplayURL__c,Name,ActionType__c,AccessUser__c,RecordId__c,' +
							'AccessPage__c,DownloadHistory__r.FormNoLabel__c ' +
							'FROM E_Log__c ' +
							'WHERE batchDate__c != null ';
			Integer j =  Database.query(query).size();		//バッチ起動後
			System.assertEquals(j, 1);
		}
	}

    //たもつくんログ
	static void createTmtlog01(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'たもつくん';
			log.ActionType__c = 'たもつくん';
			log.SummaryName__c  = 'ｔｓｔ＿15歳 男性 無解約災害・重度疾病定期保険 標準体 34年満了 年払 1,000 万円';
			System.debug('SummaryName = '+ log.SummaryName__c);

		}
		insert log;
	}

	//契約照会
	static void createPageNNlog01(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = 'ページ';
			//
			log.AccessPage__c = '/apex/E_Coli?id='+policy.id;
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	//消滅契約
	static void createPageNNlog02(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = 'ページ';
			//
			log.AccessPage__c = '/apex/E_DColi?id='+policy.id;
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	//手続き履歴照会ページ、嘱託医詳細ページ
	static void createPageNNlog03(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = 'ページ';
			//
			log.AccessPage__c = '/apex/e_processhistory?id='+policy.id;
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	//帳票ダウンロード
	static void createPageNNlog04(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		E_DownloadHistorry__c dh = createDownloadHistory('1');
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = I_Const.ACTION_TYPE_REPORT_DOWNLOAD;
			log.DownloadHistory__c = dh.id;
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	//ファイルダウンロード
	static void createPageNNlog05(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = I_Const.ACTION_TYPE_FILE_DOWNLOAD;
			log.RecordId__c = Attachment.SObjectType.getDescribe().getKeyPrefix();
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}



	//ファイルダウンロード
	static void createPageNNlog06(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = I_Const.ACTION_TYPE_FILE_DOWNLOAD;
			log.RecordId__c = ContentVersion.SObjectType.getDescribe().getKeyPrefix();
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	//お知らせ
	static void createPageNNlog07(){
		E_Policy__c policy = createPolicyColi(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = I_Const.ACTION_TYPE_READ_INFOMATION;
			log.RecordId__c = ContentVersion.SObjectType.getDescribe().getKeyPrefix();
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	//新契約ページ
	static void createPageNNlog08(){
		E_NewPolicy__c policy = createNewPolicy(true,UserInfo.getUserId());
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			log.Name = 'IRIS';
			log.ActionType__c = 'ページ';
			//
			log.AccessPage__c = '/apex/IRIS_NewPolicyDetail?id='+policy.id;
			System.debug('AccessPage = '+ log.AccessPage__c);

		}
		insert log;
	}

	// 代理店挙績情報 代理店
	static void createPageNNlog09(){
		User us = createUser01('E_PartnerCommunity');
		us = [SELECT Id
				, ContactId
				, Contact.AccountId
				, Contact.Account.ParentId
				FROM User
				WHERE Id = :us.Id];

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?agency=' + us.Contact.Account.ParentId + '&irismn=';
		log.AccessUser__c	= us.Id;
		insert log;

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?agency=00000000&irismn=';
		log.AccessUser__c	= us.Id;
		insert log;

	}
	// 代理店挙績情報 事務所
	static void createPageNNlog10(){
		User us = createUser01('E_PartnerCommunity');
		us = [SELECT Id
				, ContactId
				, Contact.AccountId
				, Contact.Account.ParentId
				FROM User
				WHERE Id = :us.Id];

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?office=' + us.Contact.AccountId + '&irismn=';
		log.AccessUser__c	= us.Id;
		insert log;

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?office=00000000&irismn=';
		log.AccessUser__c	= us.Id;
		insert log;

	}
	// 代理店挙績情報 募集人
	static void createPageNNlog11(){
		User us = createUser01('E_PartnerCommunity');
		us = [SELECT Id
				, ContactId
				, Contact.AccountId
				, Contact.Account.ParentId
				FROM User
				WHERE Id = :us.Id];

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?agent=' + us.ContactId + '&irismn=';
		log.AccessUser__c	= us.Id;
		insert log;

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?agent=00000000&irismn=';
		log.AccessUser__c	= us.Id;
		insert log;

	}
	// 代理店挙績情報 アクセスユーザ:AH
	static void createPageNNlog12(){
		User us = createUser01('E_PartnerCommunity');

		E_IDCPF__c idcpf = new E_IDCPF__c();
		idcpf.ZSTATUS01__c	= '1';
		idcpf.ZIDTYPE__c	= 'AH';				//ID種別
		idcpf.User__c		= us.Id;
		idcpf.OwnerId		= us.Id;
		insert idcpf;

		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?irismn=a1Wp00000000000000';
		log.AccessUser__c	= us.Id;
		insert log;

	}
	// 代理店挙績情報 アクセスユーザ:AY
	static void createPageNNlog13(){
		User us = createUser01('E_PartnerCommunity');

		E_IDCPF__c idcpf = new E_IDCPF__c();
		idcpf.ZSTATUS01__c	= '1';
		idcpf.ZIDTYPE__c	= 'AY';				//ID種別
		idcpf.User__c		= us.Id;
		idcpf.OwnerId		= us.Id;
		insert idcpf;

		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?irismn=a1Wp00000000000000';
		log.AccessUser__c	= us.Id;
		insert log;

	}
	// 代理店挙績情報 アクセスユーザ:AT
	static void createPageNNlog14(){
		User us = createUser01('E_PartnerCommunity');

		E_IDCPF__c idcpf = new E_IDCPF__c();
		idcpf.ZSTATUS01__c	= '1';
		idcpf.ZIDTYPE__c	= 'AT';				//ID種別
		idcpf.User__c		= us.Id;
		idcpf.OwnerId		= us.Id;
		insert idcpf;

		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?irismn=a1Wp00000000000000';
		log.AccessUser__c	= us.Id;
		insert log;

	}
	// 代理店挙績情報 事務所
	static void createPageNNlog15(){
		User us = createUser01('E_PartnerCommunity');
		us = [SELECT Id
				, ContactId
				, Contact.AccountId
				, Contact.Account.ParentId
				FROM User
				WHERE Id = :us.Id];

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?id=' + us.Contact.AccountId + '&type=office';
		log.AccessUser__c	= us.Id;
		insert log;

		log = new E_Log__c();
		log.Name			= 'IRIS';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/IRIS_AgencySalesResults?id=00000000&type=office';
		log.AccessUser__c	= us.Id;
		insert log;

	}

	//疾患検索
	static void createPageNNlog16(){
		User us = createUser01('E_PartnerCommunity');

		List<E_Log__c> logList = new List<E_Log__c>();
		log  =  new E_Log__c();
		log.Name			= '傷病名検索';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/E_DSSearch?p=1';
		log.AccessUser__c	= us.Id;
		logList.add(log);

		log  =  new E_Log__c();
		log.Name			= '傷病名検索';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/E_DSSearch?p=2';
		log.AccessUser__c	= us.Id;
		logList.add(log);

		insert logList;
	}

	//特集
	static void createPageNNlog17(){
		User us = createUser01('E_PartnerCommunity');

		List<E_Log__c> logList = new List<E_Log__c>();
		log  =  new E_Log__c();
		log.Name			= '事業承継への備え 提案のポイント';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/iris_feature?pgkey=Feature99';
		log.AccessUser__c	= us.Id;
		logList.add(log);

		log  =  new E_Log__c();
		log.Name			= '事業承継への備え 提案のポイント';
		log.ActionType__c	= 'ページ';
		log.AccessPage__c	= '/apex/iris_feature?pgkey=Feature100';
		log.AccessUser__c	= us.Id;
		logList.add(log);

		insert logList;
	}

	//パンフレット等発送依頼
	static void createPageNNlog18(){
		User us = createUser01('E_PartnerCommunity');

		List<E_Log__c> logList = new List<E_Log__c>();
		log  =  new E_Log__c();
		log.Name			= I_Const.DOCUMENT_REQUEST;
		log.ActionType__c	= I_Const.DOCUMENT_REQUEST;
		logList.add(log);

		insert logList;
	}

	//データ作成
	static Integer num = 0;
	static List<String> conNameList = new List<String>();
	static List<String> usNameList = new List<String>();
	static void clear(){
		num = 0;
		conNameList = new List<String>();
		usNameList = new List<String>();
	}

	//保険契約ヘッダ
	static E_Policy__c createPolicy(Boolean choice,User us,String conId){
		Account acc = createAccountOwner(us);
		Account acc2 = createAccount();
		Contact con = createContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0004';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = 15000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = conId;
		po.SubAgent__c = con.Id;
		//po.PolicyInCharge__c = true;
		po.OwnerId = us.Id;
		insert po;
		return po;
	}

	static E_Policy__c createPolicyColi(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = createContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0002';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.SPVA_ZNOWSUM__c = 30000;
		po.COMM_ZTOTPREM__c = 350000;
		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		po.COLI_ZCSHVAL__c = 10000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.OwnerId = userId;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType
		 				  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_COLI];
		po.RecordTypeId = rec.Id;
		insert po;
		return po;
	}

	static E_Policy__c createPolicyDColi(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = createContact(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20161110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 100;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0002';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COMM_ZCLNAME__c = con.LastName;
		po.SPVA_ZNOWSUM__c = 30000;
		po.COMM_ZTOTPREM__c = 350000;
		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		po.COLI_ZCSHVAL__c = 10000;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.OwnerId = userId;
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType
		 				  where SobjectType = 'E_Policy__c' AND DeveloperName =: E_Const.POLICY_RECORDTYPE_DCOLI];
		po.RecordTypeId = rec.Id;
		insert po;
		return po;
	}

	static void createPolicy2(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = createContact(acc,choice);
		Contact con2 = createContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20171110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '2テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10000;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0001';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZGRUPDCF__c = true;
		po.COLI_GRUPNUM__c = '12345678';
		po.COLI_ZCSHVAL__c = 1000;
		po.OwnerId = userId;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con2.Id;
		insert po;
	}
	//個人
	static void createPolicy3(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = createContact(acc,choice);
		Contact con2 = createContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10;
		//po.COMM_SINSTAMT__c = null;
		po.COMM_CHDRNUM__c = 'Test0003';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = 20000;
		po.OwnerId = userId;
		po.SubAgentAccount__c =acc.Id;
		po.SubAgent__c = con.Id;
		insert po;
	}

	static void createPolicy4(Boolean choice,String userId){
		Account acc = createAccount();
		Account acc2 = createAccount();
		Contact con = createContact(acc,choice);
		Contact con2 = createContact2(acc,choice);
		E_Policy__c po = new E_Policy__c();
		po.COMM_STATDATE__c = '20151110';
		po.Contractor__c = con.Id;
		po.COMM_ZCOVRNAM__c = '3テストタイプ';
		po.Insured__c = con.Id;
		po.COMM_SINSTAMT__c = 10;
		po.COMM_CHDRNUM__c = 'Test0005';
		if(Integer.valueOf(today.month())<10){
			po.COMM_OCCDATE__c = String.valueOf(today.year()) + '0' + String.valueOf(today.month()) + '01';
		}else{
			po.COMM_OCCDATE__c = String.valueOf(today.year()) +  String.valueOf(today.month()) + '01';
		}
		po.COLI_ZCSHVAL__c = 20000;
		po.OwnerId = userId;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc2.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con2.Id;
		insert po;
	}

	static E_NewPolicy__c createNewPolicy(Boolean choice,String userId){
		Account acc = createAccount();
		Contact con = createContact(acc,choice);
		E_NewPolicy__c po = new E_NewPolicy__c();
		po.COMM_ZCLNAME__c = con.Name;
		po.InsuranceType__c = '3テストタイプ';
		po.CHDRNUM__c = 'Test0005';
		po.SyncDate__c = today;
		insert po;
		return po;
	}

	//個人保険特約
	static void createCOV(E_Policy__c po){
		E_COVPF__c cov = new E_COVPF__c();
		cov.E_Policy__c = po.Id;
		cov.COMM_SUMINS__c = 100000;
		cov.COLI_INSTPREM__c = 100000;
		insert cov;
	}
	//募集人（取引先責任者)
	static Contact createContact(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//募集人（取引先責任者)
	static Contact createContact2(Account acc,Boolean choice){
		Contact con = new Contact();
		con.LastName = 'テストテスト' + String.valueOf(++num);
		conNameList.add(con.LastName);
		con.E_CLTPF_ZCLKNAME__c = con.LastName;
		con.AccountId = acc.Id;
		insert con;
		if(choice) createUser(con,acc);
		return con;
	}
	//親取引先
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//親取引先2
	static Account createParentAccount2(){
		Account acc = new Account();
		acc.Name = '親取引先' + String.valueOf(++num);
		acc.ZMRCODE__c = 'mr0000';
		insert acc;
		return acc;
	}
	//取引先
	static Account createAccount(){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先2
	static Account createAccount2(){
		Account acc = createParentAccount2();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//取引先3
	static Account createAccountOwner(User us){
		Account acc = createParentAccount();
		Account acp = new Account();
		acp.Name = '取引先' + String.valueOf(++num);
		acp.ZMRCODE__c = 'mr0000';
		acp.ParentId = acc.Id;
		acp.OwnerId = us.Id;
		insert acp;
		return acp = [select Name,ZMRCODE__c,E_ParentName__c from Account where Id = :acp.Id];
	}
	//ユーザ
	static void createUser(Contact con,Account acc){
		User us = new User();
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'ユーザ名';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.MRCD__c = acc.ZMRCODE__c;
		us.IsActive = true;
		us.ContactId = con.Id;
		insert us;
	}

	// ユーザ作成
	static User createUser01(String profileName){
		User us = new User();
		Account acc = createAccount();
		Contact con = createContact(acc, false);
		us.Username = 'test@test.com@sfdc.com';
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName(profileName);
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.MRCD__c = 'testUs';
		us.ContactId = con.Id;
		insert us;
		return us;
	}

	//テストデータ作成
	//ダウンロード履歴
	static E_DownloadHistorry__c createDownloadHistory(String formNo){
		E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
		dh.FormNo__c = formNo;
		insert dh;

		return dh;
	}

	//傷病Noを引数で受け取りName項目に入れる
	private static E_DiseaseNumber__c createDiseaseNumber(String diseaseNo){
		E_DiseaseNumber__c diseaseNumber = new E_DiseaseNumber__c(
			Name = diseaseNo,
			Tips__c = '告知のポイント' + diseaseNo,
			Information__c = '参考情報' + diseaseNo,
			Extent__c = '障がいの程度' + diseaseNo,
			Symptoms__c = '障がいの部位' + diseaseNo,
			Category__c = '大分類' + diseaseNo,
			Period__c = '入院期間' + diseaseNo,
			Notice__c = '必要な告知' + diseaseNo
		);
		return diseaseNumber;
	}

	private static E_Diseases__c createDisease(E_DiseaseNumber__c disNo){
		E_Diseases__c disease = new E_Diseases__c(
			Name = 'テスト傷病',
			DiseaseNumber__c = disNo.Id,
			Hiragana__c = 'てすとしょうびょう',
			Katakana__c = 'テストショウビョウ'
		);
		insert disease;
		return disease;
	}

	//ダウンロード履歴
	static E_DownloadHistorry__c createDownloadHistoryPDF(String formNo,String json){
		E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
		dh.FormNo__c = formNo;
		dh.Conditions__c = json;
		dh.outputType__c = 'PDF';
		insert dh;

		return dh;
	}

}