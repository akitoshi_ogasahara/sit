public with sharing class SC_Util {

	/** *********************************************************************************************************
	 * 業務管理責任者の登録/削除
	 * 		SC_BizDataSyncBatchOperation より呼び出される
	 * 		EBiz連携ログの区分 == 『7』
	 * *********************************************************************************************************/
	/**
	 * 業務管理責任者の登録/削除
	 */
	public void createScAgencyManagers(Set<Id> scOfficeIds){
		List<SC_AgencyManager__c> delMangerList = new List<SC_AgencyManager__c>();

		// 自主点検事務所の再取得
		List<SC_Office__c> scOfficeList = SC_OfficeDao.getRecsByIds(scOfficeIds);

		// 代理店格コードをキーに事務所、事務所に紐づく管理責任者となっている募集人を取得
		Set<Id> parentIds = new Set<Id>();
		for(SC_Office__c scOffice : scOfficeList){
			parentIds.add(scOffice.AccParentId__c);
			// Delete用のSC_AgencyManager__cのIdセット
			for(SC_AgencyManager__c agManager : scOffice.SC_AgencyManagers__r){
				delMangerList.add(agManager);
			}
		}
		parentIds.remove(null);
		// レコードタイプID取得（募集人）
		Id rtIdAgent = E_RecordTypeDao.getRecIdBySobjectTypeAndDevName(E_Const.CONTACT_RECORDTYPE_AGENT, SObjectType.Contact.Name);

		// 代理店格をキーとした、Contact（業務管理責任者）を取得
		List<Account> accList = [Select 
									Id, ParentId, E_ParentZHEADAY__c, 
									(Select Id From Contacts Where E_CL3PF_ZAGMANGR__c = 'Y' And RecordTypeId = :rtIdAgent) 
								From 
									Account 
								Where 
									ParentId In :parentIds And E_IsAgency__c = false];

		// 格コードをキーとしたContactListのMap生成 
		Set<Id> conIds = new Set<Id>();
		Map<String, List<Contact>> conMap = new Map<String, List<Contact>>();
		for(Account acc : accList){
			List<Contact> conList = new List<Contact>();
			for(Contact childCon : acc.Contacts){
				conList.add(childCon);
				conIds.add(childCon.Id);
			}
			
			if(!conMap.containsKey(acc.E_ParentZHEADAY__c)){
				conMap.put(acc.E_ParentZHEADAY__c, conList);
			}else{
				conMap.get(acc.E_ParentZHEADAY__c).addAll(conList);
			}
		}
		conIds.remove(null);

		// Insert
		insRecs(conMap, conIds, scOfficeList);

		// Delete
		delete delMangerList;
	}

	/**
	 * Insert
	 */
	private void insRecs(Map<String, List<Contact>> consMap, Set<Id> conIds, List<SC_Office__c> scOfficeList){
		List<SC_AgencyManager__c> insAgManagerList = getInsScAgencyManagerList(consMap, conIds, scOfficeList);
		insert insAgManagerList;
	}

	/**
	 * 自主点検管理責任者レコード作成
	 */
	private List<SC_AgencyManager__c> getInsScAgencyManagerList(Map<String, List<Contact>> consMap, Set<Id> conIds, List<SC_Office__c> scOfficeList){
		List<SC_AgencyManager__c> agManagerList = new List<SC_AgencyManager__c>();
		// ID管理の取得
		Map<Id, E_IDCPF__c> idcpfMap = new Map<Id, E_IDCPF__c>();
		for(E_IDCPF__c idcpf : E_IDCPFDao.getRecsByContactIds(conIds)){
			idcpfMap.put(idcpf.User__r.Id, idcpf);
		}

		// Userの取得
		Map<Id, User> userMap = new Map<Id, User>();
		for(User u : E_UserDao.getRecsAgentId(conIds)){
			userMap.put(u.ContactId, u);
		}

		// 自主点検管理責任者の作成
		for(SC_Office__c scOffice : scOfficeList){
			for(Contact con : consMap.get(scOffice.AccZHEADAY__c)){
				User targetUser = userMap.get(con.Id);



				SC_AgencyManager__c agManager = new SC_AgencyManager__c();
				agManager.SC_Office__c = scOffice.Id;
				agManager.Contact__c = con.Id;

				// User情報
				if(targetUser != null){
					// ID管理
					E_IDCPF__c userIdcpf = idcpfMap.get(targetUser.Id);
					if(userIdcpf != null && userIdcpf.ZSTATUS01__c != E_Const.ZSTATUS01_ENABLE){
						continue;
					}

					if(targetUser.IsActive){
						agManager.User__c = targetUser.Id;
						agManager.HasUserEmail__c = E_Util.isActiveEmail(targetUser.Email);
					}
				}

				agManagerList.add(agManager);	
			}
		}
		return agManagerList;
	}

	/** *********************************************************************************************************
	 * 業務管理責任者のユーザメールありの更新
	 * 		メールに『@sfdc.com』を含むかどうかで有効なメールアドレスかを判定
	 * 			含む    :無効メールアドレス
	 * 			含まない :有効メールアドレス
	 * *********************************************************************************************************/
	/**
	 * 自主点検管理責任者.ユーザメールありの更新 
	 */
	public void updateAgencyManagerHasUserEmail(List<User> newList, Map<Id, User> oldMap){
		/* 自主点検管理責任者の「ユーザメールあり」の更新 */
		Map<Id, String> emailChangeMap = new Map<Id, String>();

		for(User newUser : newList){
			User oldUser = oldMap.get(newUser.Id);

			// メールアドレス変更あり
			if(oldUser.Email != newUser.Email){
				emailChangeMap.put(newUser.Id, newUser.Email);
			}
		}

		// Futureにて更新
		if(!emailChangeMap.isEmpty()){
			updateFuture(emailChangeMap);
		}
	}

	@future
	private static void updateFuture(Map<Id, String> emailChangeMap){
		// 募集人がメールアドレス変更を行うとサイトゲストユーザでの更新となるため、Withoutでの実装

		// 自主点検管理責任者.ユーザメールありの更新
		List<SC_AgencyManager__c> updateScAgManagerList = new List<SC_AgencyManager__c>();
		for(SC_AgencyManager__c scAgManager : SC_AgencyManagerDaoWithout.getRecsByUserIds(emailChangeMap.keySet())){
			scAgManager.HasUserEMail__c = E_Util.isActiveEmail(emailChangeMap.get(scAgManager.User__r.Id));
			updateScAgManagerList.add(scAgManager);
		}
		//update updateScAgManagerList;
		E_GenericDaoWithOutSharing.updateRecord(updateScAgManagerList);
	}
}