/**
 *		E_CRLPFTriggerから呼び出される可能性があることを注意すること
 */
public with sharing class E_PolicyTriggerHandler {


	private E_PolicyTriggerBizLogic bizLogic;
	
	//コンストラクタ
	public E_PolicyTriggerHandler(){
		bizLogic = new E_PolicyTriggerBizLogic();
	}
	
	//before insert
	public void onBeforeInsert(List<E_Policy__c> newList){
		bizLogic.setAccesibleCustomer(newList);
	}

	//before update
	public void onBeforeUpdate(List<E_Policy__c> newList, Map<Id, E_Policy__c> oldMap){
		bizLogic.setAccesibleCustomer(newList);
	}

}