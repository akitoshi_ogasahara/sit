global with sharing class CR_OutsrcViewExtender extends CR_AbstractExtender{

	@TestVisible
    private CR_OutsrcViewController controller; 
    public CR_OutSrcStatusManager manager{get;set;}
    public List<Attachment> files{get;set;}

    /** 
     * Constructor 
     */
    public CR_OutsrcViewExtender (CR_OutsrcViewController extension){
        super();
        this.controller = extension;
        this.files = E_AttachmentDao.getRecsById(controller.record.Id);
        this.helpMenukey = CR_Const.MENU_HELP_OUTSRC;
        this.helpPageJumpTo = CR_Const.MENU_HELP_OUTSRC_View;
    }
    
    /** 
     * init
     */
    global override void init() {
        isSuccessOfInitValidate = false;
        if(getHasOutsrcViewPermission()==false){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG05_REQUIRED_PERMISSION_OUTSRC);        //'外部委託参照権限がありません'
        }else if(this.controller.record == null 
                || String.isBlank(this.controller.record.Id)){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            manager = new CR_OutSrcStatusManager(controller.record, this.files.size());
            this.viewingAgencyCode = this.controller.record.CR_Agency__r.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }
    }
    
    //外部委託　編集
    public String getUrlforEdit(){
        PageReference pr = Page.E_CROutSrcEdit;
        pr.getParameters().put('id', controller.record.Id);
        return pr.getUrl();
    }

    //ファイルアップロード
    public String getUrlforUpload(){
        PageReference pr = Page.E_CROutSrcSubmit;
        pr.getParameters().put('id', controller.record.Id);
        return pr.getUrl();
    }

    /**     
     * 削除処理 
     */
    public PageReference doDeleteEx(){
        String agencyId = controller.record.CR_Agency__c;
        controller.deleteRecord();
        if(skyEditor2.Messages.hasErrorMessage()){
            pageMessages.addErrorMessage(skyEditor2.Messages.getErrorMessages());
            return null;
        }
        
        PageReference pr = Page.E_CROutsrcs;
        pr.getParameters().put('id', agencyId);
        return pr;
    } 
    
    /** 
     * ボタン表示制御
     */
    public Boolean getCanEdit(){
        return manager.canEdit();
    }
    
    public Boolean getCanDelete(){
        return manager.canDelete();
    }
    
    public Boolean getHasNextAction(){
        return manager.hasNextAction();
    }
    
    public Boolean getHasRejectAction(){
        return manager.hasRejectAction();
    }
    
    /*
     *	個人情報の取扱に関するワーニング表示
     *			報告ボタンが表示され　かつ　個人情報の取扱で「いいえ」が選択されているとき
     */
    public Boolean getShowWarningForPersonalInfo(){
    	return manager.nextActionIsReport()
    			&& controller.record.Confirm06__c == 'いいえ';
    }
}