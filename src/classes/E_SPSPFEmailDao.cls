/**
 * E_SPSPFEmai__cデータアクセスオブジェクト
 * CreatedDate 2015/01/07
 * @Author Terrasky
 */
public with sharing class E_SPSPFEmailDao {
    
    public static E_SPSPFEmail__c getRecById (String fsmailId) {
        E_SPSPFEmail__c rec = null;
        List<E_SPSPFEmail__c> recs = [
                        Select 
                            Name, MailNunmer__c, SendMail__c
                        From 
                            E_SPSPFEmail__c 
                        Where 
                            Id = :fsmailId
        ];
        
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
    }
    
}