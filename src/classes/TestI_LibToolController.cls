@isTest
private class TestI_LibToolController {

	static String docNum = 'TEST0001';
	static String docId = 'docID';
	//static String category = 'category';
	static String category = '定期保険';

	//NN社員判定
	@isTest static void NNEmployeeJudge() {
		//代理店ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		TestE_TestUtil.createIDCPF(true,actUser.Id);

		System.runAs(actUser){
			Test.startTest();
				I_LibToolController ilc = new I_LibToolController();
				Boolean ass = ilc.getIsEmployee();
				System.assertEquals(ass,true);
			Test.stopTest();
		}
	}

	//IRISMenu-docNumberから値を取得できる場合
	@isTest static void getIRISMenu1() {
		I_PageMaster__c ipm = createData();
		PageReference pageRef = Page.IRIS_Lib_Tools;
		pageRef.getParameters().put(I_Const.URL_PARAM_DOC_NUM,docNum);
		Test.setCurrentPage(pageRef);

		Test.startTest();
			I_LibToolController ilc = new I_LibToolController();
			String ass = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGE);
			System.assertEquals(ass,ipm.Id);
		Test.stopTest();
	}

	//IRISMenu-page,docNumber共に値が取得できない場合
	@isTest static void getIRISMenu2() {
		I_PageMaster__c ipm = createData();
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		Test.startTest();
			I_LibToolController ilc = new I_LibToolController();
			String ass = ApexPages.currentPage().getParameters().get(I_Const.URL_PARAM_PAGE);
			System.assertEquals(ass,ipm.Id);
		Test.stopTest();
	}

	@isTest static void getNotExistsToolsClassName1() {
		I_PageMaster__c ipm = createData();
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());
		PageReference pageRef = Page.IRIS_Lib_Tools;
		pageRef.getParameters().put(I_Const.URL_PARAM_PAGE, ipm.Id);
		Test.setCurrentPage(pageRef);

		List<I_ContentMaster__c> conM = [SELECT Page__c, (SELECT Id, ParentContent__c FROM ChildContents__r) FROM I_ContentMaster__c WHERE Page__c = :ipm.Id];
		System.debug(conM[0].ChildContents__r);

		Test.startTest();
			I_LibToolController ilc = new I_LibToolController();
			String ass = ilc.getNotExistsToolsClassName();
			System.assertEquals(ass,null);
		Test.stopTest();
	}

	@isTest static void getNotExistsToolsClassName2() {
		I_PageMaster__c ipm = createData();
		//子供のI_ContentMaster__cを更新するために検索
		I_ContentMaster__c parent = [select Id from I_ContentMaster__c where Page__c = :ipm.Id];
		I_ContentMaster__c child = [select Id,ForCustomers__c,CompanyLimited__c from I_ContentMaster__c where ParentContent__c = :parent.Id];
		child.ForCustomers__c = false;
		child.CompanyLimited__c = false;
		update child;
		//ID管理
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		Test.startTest();
			I_LibToolController ilc = new I_LibToolController();
			String ass = ilc.getNotExistsToolsClassName();
			System.assertEquals(ass,'forCustomer forNN');
		Test.stopTest();
	}

	@isTest static void getHasChatterFile() {
		createData();
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		Test.startTest();
			I_LibToolController ilc = new I_LibToolController();
			Boolean ass = ilc.getHasChatterFile();
			System.assertEquals(ass,false);
		Test.stopTest();
	}


	//コンストラクタ用データ（I_CMSController用）作成
	static I_PageMaster__c createData(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		I_ContentMaster__c  icm = createIRISCMS(ipm);

		PageReference pageRef = Page.IRIS;
		pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
		return ipm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.Menu__c = mmId;
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		icm.DocumentNo__c = docNum;
		icm.Category__c = category;
		//icm.CategoryJoin__c = category;
		icm.DOM_Id__c = docID;
		icm.Name = I_Const.IRIS_CONTENTS_NAME_SALESTOOL;
		insert icm;
		createChildIRISCMS(icm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.DisplayFrom__c = E_Util.SYSTEM_TODAY().addMonths(-1);
		icm.DisplayTo__c = E_Util.SYSTEM_TODAY().addMonths(1);
		icm.Category__c = category;
		//icm.CategoryJoin__c = category;
		icm.ForCustomers__c = true;
		icm.ForAgency__c = true;
		icm.CompanyLimited__c = true;
		insert icm;
		return icm;
	}

	static I_MenuMaster__c createIRISMenuMaster(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = category;
		mm.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY;
		mm.SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_SALESTOOL;
		mm.MenuKey__c = 'menukey';
		insert mm;
		return mm;
	}

}