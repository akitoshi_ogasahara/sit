/**
 * IDリクエストデータアクセスオブジェクト
 * CreatedDate 2016/10/31
 * @Author Terrasky
 */
public class E_IDRequestDao {

	/**
	 * IDリクエストデータリストをID管理レコードIDから取得する
	 * @param  idcpfId :ID管理のレコードID
	 * @return IDリクエスト情報
	 */
	public static List<E_IDRequest__c> getRecsByIDCPFId(ID idcpfId){
		List<E_IDRequest__c> recs = [
				SELECT
					  Id                    //カスタムオブジェクト ID
					, Name                  //IDリクエストNo
					, ZEMAILAD__c           //メールアドレス
					, E_IDCPF__c            //ID管理
					, ZIDTYPE__c            //ID種別
					, ZWEBID__c             //ID番号
					, ZUPDFLAG__c           //リクエスト種別
					, ZUPDNO__c             //更新No
					, ZIDOWNER__c           //所有者コード
					, ZINQUIRR__c           //照会者コード
					, dataSyncDate__c       //変更受付日時
					, E_IDCPF__r.User__c
					, CreatedDate
				FROM E_IDRequest__c
				WHERE E_IDCPF__r.Id = :idcpfId
		];
		return recs;
	}
}