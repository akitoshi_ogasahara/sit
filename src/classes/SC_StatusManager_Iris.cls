/**
 * 2018年度以降のステータス管理クラス
 */
public with sharing class SC_StatusManager_Iris {
	private static String PROCESSED_MSG = '処理が完了しました。';
	private static String PROCESSED_MSG_APPLY_AG = 'エヌエヌ生命へ提出しました。';
	private static String PROCESSED_MSG_APPLY_MR = '本社へ提出処理が完了しました。';

	private E_AccessController access;
	private SC_Office__c record;
	private List<SC_AgencyManager__c> agManagerList;
	private Map<Id, SC_AgencyManager__c> agManagerUserMap;
	private AvailableActions actions;
	private Id profileId;
	private String beforeActionLabel;
	private String beforeActionLabelCmd;
	
	// 処理完了メッセージ
	public String msgActionComplete {get; set;}
	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}

	/** Status定義 **/
	public static String STATUS_01 = SC_Const.AMSOFFICE_STATUS_01;
	public static String STATUS_02 = SC_Const.AMSOFFICE_STATUS_02;
	public static String STATUS_03 = SC_Const.AMSOFFICE_STATUS_03;
	public static String STATUS_04 = SC_Const.AMSOFFICE_STATUS_04;
	public static String STATUS_05 = SC_Const.AMSOFFICE_STATUS_05;

	/* Defect定義 */
	public static String DEFECT_01 = SC_Const.DEFECT_STATUS_01;
	public static String DEFECT_02 = SC_Const.DEFECT_STATUS_02;
	public static String DEFECT_03 = SC_Const.DEFECT_STATUS_03;
	
	/** Action定義 **/
	public Enum ScAction {NONE, APPLY, APPLY_MR, REPORT_MR, APPROVE, REJECT_APPROVE_AG, REJECT_REPORT_MR, REJECT_APPROVE_MR, REJECT_COMPLETE}
	public static Map<ScAction, String> scActionLabel = new Map<ScAction, String>{
															ScAction.NONE => ''
															,ScAction.APPLY => 'エヌエヌ生命に提出'									// 提出　：　AG　→　CMD
															,ScAction.APPLY_MR => 'エヌエヌ生命に提出'									// 提出　：　AG　→　MR
															,ScAction.REPORT_MR => '本社へ提出'										// 提出　：　AG　→　MR
															,ScAction.APPROVE => '完了'											// 提出　：　MR　→　CMD
															,ScAction.REJECT_APPROVE_AG => '代理店へ差し戻し'							// 差戻　：　CMD　→　AG
															,ScAction.REJECT_REPORT_MR => '代理店へ差し戻し'							// 差戻　：　MR　→　AG
															,ScAction.REJECT_APPROVE_MR => 'MRへ差し戻し'							// 差戻　：　CMD　→　MR
															,ScAction.REJECT_COMPLETE => '完了解除'									// 差戻　：　完了　→　CMD
														};
	/**
	 * アクション種別保持クラス
	 */
	public class AvailableActions{
		// 次アクション
		public ScAction nextAction {get; set;}
		// 差し戻しアクション
		public ScAction rejectAction {get; set;}
		// 差し戻しアクション_CMD追加
		public ScAction rejectActionCmd {get; set;}
		
		/** Constructor */
		public AvailableActions(ScAction nextAct, ScAction rejectAct, ScAction rejectActCmd){
			nextAction = nextAct;
			rejectAction = rejectAct;
			rejectActionCmd = rejectActCmd;
		}
		
		/** 次アクションのラベル取得 */
		public String getNextActionLabel(){
			return scActionLabel.get(nextAction);
		}
		
		/** 差し戻しアクションのラベル取得 */
		public String getRejectActionLabel(){
			return scActionLabel.get(rejectAction);
		}
		public String getRejectActionLabelCmd(){
			return scActionLabel.get(rejectActionCmd);
		}
	}
	
	/**
	 * Constructor
	 */
	public SC_StatusManager_Iris(SC_Office__c rec, E_AccessController accCon){
		record = rec;
		access = accCon;
		profileId = access.idcpf.user__r.profileId;
		actions = getAvailableActions();
		agManagerList = [Select Id, User__c From SC_AgencyManager__c Where SC_Office__r.Id = :rec.Id];
		
		agManagerUserMap = new Map<Id, SC_AgencyManager__c>();
		for(SC_AgencyManager__c agManager : agManagerList){
			agManagerUserMap.put(agManager.User__c, agManager);
		}

		pageMessages = E_PageMessagesHolder.getInstance();
	}
	
	/**
	 * アクション種別保持クラスの取得
	 */
	private AvailableActions getAvailableActions(){
		// 自主点検（参照のみ）権限セット
		if(getIsPermissionReadOnly()){
			return new AvailableActions(ScAction.NONE, ScAction.NONE, ScAction.NONE);
		}

		// 未提出
		if((record.Status__c == STATUS_01 || String.isBlank(record.Status__c))
				&& (getIsAG() || getIsMR() || getIsMGR() || getIsCMD())){
			// 差し戻し　MR
			if(record.RemandSource__c == 'MR'){
				 return new AvailableActions(ScAction.APPLY_MR, ScAction.NONE, ScAction.NONE);
			// 差し戻し　なし、CMD
			}else{
				 return new AvailableActions(ScAction.APPLY, ScAction.NONE, ScAction.NONE);
			}
		}
		// 担当MR確認中
		if(record.Status__c == STATUS_02
				&& (getIsMR() || getIsMGR() || getIsCMD())){
			return new AvailableActions(ScAction.REPORT_MR, ScAction.REJECT_REPORT_MR, ScAction.NONE);
		}
		// 本社確認中
		if(record.Status__c == STATUS_04
				&& getIsCMD()){
			return new AvailableActions(ScAction.APPROVE, ScAction.REJECT_APPROVE_AG, ScAction.REJECT_APPROVE_MR);
		}
		// 完了
		if(record.Status__c == STATUS_05
				&& getIsCMD()){
			return new AvailableActions(ScAction.NONE, ScAction.REJECT_COMPLETE, ScAction.NONE);
		}
		// それ以外
		return new AvailableActions(ScAction.NONE, ScAction.NONE, ScAction.NONE);
	}
	
	/***************************************************
	 * NextAction
	 ***************************************************/     
	/** NextAction存在判定 */
	public Boolean getHasNextAction(){
		return  actions.nextAction != ScAction.NONE;
	}
	
	/** ラベル名取得 */
	public String getNextActionLabel(){
		return actions.getNextActionLabel();
	}
	
	/** NextAction処理 */
	public PageReference doNextAction(){
		// 処理完了メッセージの初期化
		pageMessages.clearMessages();
		
		try{			
			// 処理実行時のラベル
			beforeActionLabel = getNextActionLabel();
			
			// [AG　→　CMD] エヌエヌ生命に提出
			if(actions.nextAction == ScAction.APPLY){
				return doApply(String.isNotBlank(record.RemandSource__c));
			}
			// [AG　→　MR] エヌエヌ生命に提出
			if(actions.nextAction == ScAction.APPLY_MR){
				return doApplyMR();
			}
			// [MR　→　CMD] 本社へ提出
			if(actions.nextAction == ScAction.REPORT_MR){
				return doReportMR();
			}
			// [CMD] 完了
			if(actions.nextAction == ScAction.APPROVE){
				return doApprove();
			}
		}catch(Exception e){
			setProcessedMessage(e.getMessage());
		}
		
		return null;
	}
	
	/** [AG　→　CMD] 提出アクション */
	private PageReference doApply(Boolean isRemand){
		// ステータス更新 [未提出　→　本社確認中]
		updateStatus(STATUS_04, record.Defect__c, null, record.CMDRemandDate__c);

		// テンプレート設定
		String tempNN = '';
		String tempAG = '';
		// 差し戻し
		if(isRemand){
			tempNN = SC_MailManager.MT_STATUS_RE_AGCMD_NN;
			tempAG = SC_MailManager.MT_STATUS_RE_AGCMD_AG;
		// 通常
		}else{
			tempNN = SC_MailManager.MT_STATUS_AGCMD_NN;
			tempAG = SC_MailManager.MT_STATUS_AGCMD_AG;
		}

		try{
			// メール送信　NN
			SC_MailManager.sendMail(tempNN, record, new List<SC_AgencyManager__c>());
			
			// メール送信　代理店
			SC_MailManager.sendMail(tempAG, record, agManagerList);
			
			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG_APPLY_AG);
			setProcessedMessage(PROCESSED_MSG_APPLY_AG);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}

		return null;
	}
	
	/** [AG　→　MR] 提出アクション */ 
	private PageReference doApplyMR(){
		// ステータス更新 [未提出　→　担当MR確認中]
		updateStatus(STATUS_02, record.Defect__c, null, record.CMDRemandDate__c);

		try{
			// メール送信　NN
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_RE_AGMR_NN, record, new List<SC_AgencyManager__c>());
			
			// メール送信　代理店
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_RE_AGMR_AG, record, agManagerList);
			
			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG_APPLY_AG);
			setProcessedMessage(PROCESSED_MSG_APPLY_AG);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}
		
		return null;
	}
	
	/** [MR　→　CMD] 本社へ提出アクション */
	private PageReference doReportMR(){
		// ステータス更新
		updateStatus(STATUS_04, record.Defect__c, null, record.CMDRemandDate__c);
		
		try{
			// メール送信　NN 
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_RE_MRCMD_MR, record, new List<SC_AgencyManager__c>());
			
			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG_APPLY_MR);
			setProcessedMessage(PROCESSED_MSG_APPLY_MR);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}

		return null;
	}
	
	/** [CMD] 完了アクション */
	private PageReference doApprove(){
		// ステータス更新
		updateStatus(STATUS_05, (record.Defect__c == DEFECT_02)? DEFECT_03 : '', record.RemandSource__c, record.CMDRemandDate__c);

		try{
			// メール送信　代理店
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_CMDAG, record, agManagerList);

			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG);
			setProcessedMessage(PROCESSED_MSG);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}
		
		return null;
	}
	
	/***************************************************
	 * RejectAction
	 ***************************************************/
	/** RejectAction存在判定 */
	public Boolean getHasRejectAction(){
		return actions.rejectAction != ScAction.NONE;
	}
	public Boolean getHasRejectActionCmd(){
		return actions.rejectActionCmd != ScAction.NONE;
	}
	
	/** ラベル名取得 */
	public String getRejectActionLabel(){
		return actions.getRejectActionLabel();
	}
	public String getRejectActionLabelCmd(){
		return actions.getRejectActionLabelCmd();
	}
	
	/** RejectAction処理 */
	public PageReference doRejectAction(){
		// 処理完了メッセージの初期化
		pageMessages.clearMessages();

		try{		
			// 処理実行時のラベル
			beforeActionLabel = getRejectActionLabel();
		
			// 差し戻し[CMD → AG]
			if(actions.rejectAction == ScAction.REJECT_APPROVE_AG){
				return doRejectApproveAG();
			}
			// 差し戻し[MR → AG]
			if(actions.rejectAction == ScAction.REJECT_REPORT_MR){
				return doRejectReportMR();
			}
			// 完了取消[CMD]
			if(actions.rejectAction == ScAction.REJECT_COMPLETE){
				return doRejectComplete();
			}
		}catch(Exception e){
			setProcessedMessage(e.getMessage());
		}

		return null;
	}
	
	/** RejectAction処理 */
	public PageReference doRejectActionCmd(){
		// 処理完了メッセージの初期化
		pageMessages.clearMessages();

		try{
			// 処理実行時のラベル
			beforeActionLabelCmd = getRejectActionLabelCmd();
		
			// 差し戻し[CMD → MR]
			if(actions.rejectActionCmd == ScAction.REJECT_APPROVE_MR){
				return doRejectApproveMR();
			}
		}catch(Exception e){
			setProcessedMessage(e.getMessage());
		}
		return null;
	}
	
	/** [CMD → AG] 差し戻しアクション */
	private PageReference doRejectApproveAG(){
		// ステータス更新
		updateStatus(STATUS_01, DEFECT_02, 'CMD', System.Now());

		try{
			// メール送信
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_RE_CMDAG, record, agManagerList);
			
			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG);
			setProcessedMessage(PROCESSED_MSG);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}

		return null;
	}
	
	/** [MR → AG] 差し戻しアクション */
	private PageReference doRejectReportMR(){
		// ステータス更新
		updateStatus(STATUS_01, DEFECT_02, 'MR', record.CMDRemandDate__c);

		try{
			// メール送信　代理店
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_RE_MRAG, record, agManagerList);
			
			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG);
			setProcessedMessage(PROCESSED_MSG);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}
		
		return null;
	}
	
	/** [CMD → MR] 差し戻しアクション */
	private PageReference doRejectApproveMR(){
		// ステータス更新
		updateStatus(STATUS_02, DEFECT_02, 'CMD', System.Now());
		
		try{
			// メール送信　NN
			SC_MailManager.sendMail(SC_MailManager.MT_STATUS_RE_CMDMR, record, new List<SC_AgencyManager__c>());
			
			// 処理完了メッセージ
			//pageMessages.addErrorMessage(PROCESSED_MSG);
			setProcessedMessage(PROCESSED_MSG);
		}catch(Exception e){
			setProcessedMessage('[メール送信処理でエラーが発生しました] ' + e.getMessage());
		}
		return null;
	}
	
	/** [CMD] 差し戻しアクション */
	private PageReference doRejectComplete(){
		// ステータス更新
		updateStatus(STATUS_04, record.Defect__c, record.RemandSource__c, record.CMDRemandDate__c);
		
		// 処理完了メッセージ
		//pageMessages.addErrorMessage(PROCESSED_MSG);
		setProcessedMessage(PROCESSED_MSG);
		
		return null;
	}   

	/***************************************************
	 * 処理完了メッセージ取得
	 ***************************************************/
	/**
	 *　処理完了メッセージ取得
	 */
	private String getMsgActionComplete(String label, String msgKey){
		if(String.isBlank(label)){
			return E_Message.getMsgMap().get(msgKey);
		}else{
			return label + E_Message.getMsgMap().get(msgKey);
		}
	}

	private void setProcessedMessage(String msg){
		// 既にメッセージが登録されていない場合に、メッセージセット
		if(!pageMessages.hasMessages()){
			pageMessages.addErrorMessage(msg);
		}
	}

	/***************************************************
	 * ステータス更新
	 ***************************************************/
	/**
	 *　ステータス更新
	 */
	private void updateStatus(String status, String defect, String remandSource, Datetime cMDRemandDate){
		String str = '';
		String beforeStatus = record.Status__c;
		String beforeDefect = record.Defect__c;
		String beforeHistory = record.StatusHistory__c;
		String beforeRemandSource = record.RemandSource__c;
		Datetime beforeCMDRemandDate = record.CMDRemandDate__c;
		
		// 現在日付
		Datetime sysNow = System.Now();
		
		// ステータス
		record.Status__c = status;
		
		// ステータス履歴
		record.StatusHistory__c = createStatusHistory(status, beforeStatus, beforeHistory, sysNow);
		
		if(beforeDefect != defect){
			// 不備ステータス
			record.Defect__c = defect;
			
			defect = String.isBlank(defect) ? DEFECT_01 : defect;
			beforeDefect = String.isBlank(beforeDefect) ? DEFECT_01 : beforeDefect;
			
			//不備ステータスが変わっていないとき、ステータス履歴を更新しない
			if(defect != beforeDefect){
				// ステータス履歴
				record.StatusHistory__c = createStatusHistory(defect, beforeDefect, record.StatusHistory__c, sysNow);
			}
		}
		
		if(beforeRemandSource != remandSource){
			// 差戻元
			record.RemandSource__c = remandSource;
		}
		
		if(beforeCMDRemandDate != cMDRemandDate){	
			// CMD差戻日
			record.CMDRemandDate__c = cMDRemandDate;
		}

		// Update
		Boolean saveResult = E_GenericDao.saveViaPage(record);

		// アクション再取得
		if(saveResult){
			actions = getAvailableActions();
		}else{
			record.Status__c = beforeStatus;
			record.Defect__c = beforeDefect;
			record.StatusHistory__c = beforeHistory;
			record.RemandSource__c = beforeRemandSource;
			record.CMDRemandDate__c = beforeCMDRemandDate;
			system.debug('**getHasErrorMessages ** ' + pageMessages.getHasErrorMessages());
			pageMessages.addErrorMessage('[レコード更新処理でエラーが発生しました] ' + E_PageMessagesHolder.getInstance().getErrorMessages()[0].summary);
		}
	}

	/**
	 * ステータス更新履歴
	 */
	private String createStatusHistory(String status, String beforeStatus, String beforeHistory, Datetime sysNow){
		String str = '';
		str += sysNow.format();
		str += '   ';
		str += UserInfo.getName();
		str += '   [';
		str += beforeStatus;
		str += ' => ';
		str += status;
		str += ']';
		str += '\n'; 
		str += '-------------------------------------------------------------------------------------';
		str += '\n';

		return str + E_Util.null2Blank(beforeHistory);
	}

	/***************************************************
	 * ユーザの処理権限判定
	 ***************************************************/
	/**
	 * AG 業務管理責任者
	 * true: 自主点検（代理店） 権限セット かつ 自主点検管理責任者に存在
	 */
	public Boolean getIsAGMGR(){
		if(access.hasSCAGPermission()
			&& agManagerUserMap.get(UserInfo.getUserId()) != null ){
			return true;
		}
		return false;
	} 
	 
	/**
	 * AG
	 * true: 自主点検（代理店） 権限セット
	 */
	public Boolean getIsAG(){
		if(access.hasSCAGPermission()){
			return true;
		}
		return false;
	}
	
	/**
	 * MR
	 * true: 自主点検（社員） 権限セット　かつ　（MRプロファイル　または　本社営業部MRプロファイル）
	 */
	public Boolean getIsMR(){
		if(access.hasSCNNPermission() 
				&& (E_ProfileDao.isMR(profileId) || E_ProfileDao.isHQMR(profileId))){
			return true;    
		}
		return false;
	}

	/**
	 * MGR
	 * true: 自主点検（社員） 権限セット　かつ　（拠点長プロファイル　または　本社営業部拠点長プロファイル）
	 */
	public Boolean getIsMGR(){
		if(access.hasSCNNPermission() 
				&& (E_ProfileDao.isMRManager(profileId) || E_ProfileDao.isHQMRManager(profileId))){
			return true;    
		}
		return false;
	}
	
	/**
	 * CMD
	 * true: 自主点検（コンプラ） 権限セット
	 */
	public Boolean getIsCMD(){
		return access.hasSCAdminPermission();
	}
	
	/**
	 * 自主点検（参照のみ）権限セット
	 */
	public Boolean getIsPermissionReadOnly(){
		return access.hasSCDirectorPermission();
	}
}