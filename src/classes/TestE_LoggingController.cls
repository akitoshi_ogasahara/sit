@isTest
private class TestE_LoggingController {
	
	static testMethod void cleateLogTest() {
		Test.startTest();
			E_LoggingController logCon = new E_LoggingController();
			String jsonLog = logCon.getPageAccessLogJSON();
			String logId = E_LoggingController.saveLog(jsonLog);
			logCon.getRequiredLogging();
			System.assertNotEquals(null, logId);

		Test.stopTest();
	}
	
	static testMethod void cleateLogTest2() {
		Test.startTest();
			E_LoggingController logCon = new E_LoggingController();
			E_Log__c log = new E_Log__c(name='log',AccessUser__c = UserInfo.getUserId());
			String jsonLog = JSON.serialize(log);
			
			String logId = E_LoggingController.saveLog(jsonLog);
			logCon.getRequiredLogging();
			System.assertNotEquals(null, logId);
		Test.stopTest();
	}
	//ExceptionCase
	static testMethod void cleateLogTest3() {
		Test.startTest();
			E_LoggingController logCon = new E_LoggingController();
			String errMsg = E_LoggingController.saveLog('aaabbb');
			logCon.getRequiredLogging();
			System.assertNotEquals(null, errMsg);
		Test.stopTest();
	}
	
}