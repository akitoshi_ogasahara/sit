global with sharing class E_MKFriendDetailSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public Lead record {get{return (Lead)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_MKFriendDetailSVEExtender getExtender() {return (E_MKFriendDetailSVEExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public E_MKFriendDetailSVEController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1348');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1348',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1347');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1347',tmpPropMap);


		SObjectField f;

		f = Lead.fields.LastName;
		f = Lead.fields.Company;
		f = Lead.fields.name_kana__c;
		f = Lead.fields.ZipCode__c;
		f = Lead.fields.Address__c;
		f = Lead.fields.Phone;
		f = Lead.fields.Email;
		f = MK_CampaignTarget__c.fields.Name;
		f = MK_CampaignTarget__c.fields.ContractantKana__c;
		f = MK_CampaignTarget__c.fields.CLTPF_CLNTNUM__c;
		f = MK_CampaignTarget__c.fields.ContractantMailAddress__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = Lead.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('Lead');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('LastName');
			mainQuery.addFieldAsOutput('Company');
			mainQuery.addFieldAsOutput('name_kana__c');
			mainQuery.addFieldAsOutput('ZipCode__c');
			mainQuery.addFieldAsOutput('Address__c');
			mainQuery.addFieldAsOutput('Phone');
			mainQuery.addFieldAsOutput('Email');
			mainQuery.addFieldAsOutput('MK_CampaignTarget__r.Name');
			mainQuery.addFieldAsOutput('MK_CampaignTarget__r.ContractantKana__c');
			mainQuery.addFieldAsOutput('MK_CampaignTarget__r.CLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('MK_CampaignTarget__r.ContractantMailAddress__c');
			mainQuery.addFieldAsOutput('RecordType.DeveloperName');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_MKFriendDetailSVEExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}