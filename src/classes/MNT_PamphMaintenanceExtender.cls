global with sharing class MNT_PamphMaintenanceExtender extends SkyEditor2.Extender{
	private MNT_PamphMaintenanceController extension;
	private I_ContentMaster__c record;
	public Attachment uploadFile {get;set;}
	public boolean hasFileFlag {get;set;}
	private String beforeParam;
	private boolean isClone;

    public MNT_PamphMaintenanceExtender(MNT_PamphMaintenanceController extension){
    	this.extension = extension;
    	uploadFile = new Attachment();
    	isClone = false;
    }

    global override void init(){
    	this.record = extension.record;
    	record.ClickAction__c = I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT;
    	record.WindowTarget__c = '_blank';

    	Map<String, String> params = ApexPages.CurrentPage().getParameters();

    	// 編集・コピー時は、Idが渡る
    	if(params.containsKey('id')){
    		String pageId = params.get('id');
    		if(!String.isBlank(pageId)){
    			//　遷移前のレコードIdを保持
    			beforeParam = pageId;
	    		//　コピー判定
		    	if(params.containsKey('clone') && params.get('clone').equals('1')){
		    		isClone = true;
			    	record.UpsertKey__c = null;
			   		record.filePath__c = null;
		    	}else{
				    Attachment alreadyFile = E_AttachmentDao.getRecById(record.Id);
			    	if(alreadyFile != null){
			    		hasFileFlag = true;
			    	}
		    	}
    		}
    	}
    	

    }

    public PageReference doCancel(){

		if(!String.isBlank(beforeParam)){
			return new PageReference('/apex/MNT_PamphletView?id=' + beforeParam);
		}else{
			return new PageReference('/apex/MNT_PamphletSearch');
		}
		
	}

	public PageReference doSave(){

		if(String.isBlank(record.Name)){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料名を入力してください。'));
			return null;
		}else if(String.isBlank(record.FormNo__c)){
			ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, 'フォームNoを入力してください。'));
			return null;
		}else{
			// UpsertKey生成時のエラー回避のため入力されているNameとフォームNoが一致するレコードの有無チェック
			I_ContentMaster__c exitContent = I_ContentMasterDao.getRecByFormNoAndName(record.FormNo__c, record.Name);
			if(exitContent == null || (exitContent.Id == beforeParam && !isClone)){
				MNT_Util.saveParentWithAttachment(record,uploadFile);
				return new PageReference('/apex/MNT_PamphletView?id=' + record.Id);
			}else{
				ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.INFO, '資料名とフォームNoが同一の資料がございます。'));
				return null;
			}

		}

	}

}