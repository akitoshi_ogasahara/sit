global with sharing class E_InveCompareExtender extends E_AbstractViewExtender {
	private static final String PAGE_TITLE = 'アセットアロケーション';
	
	E_InveCompareController extension;
	
	//グラフコンポーネントの引数
	public String getExecuteKindBuy(){
		return E_Const.GRAPH_INVESTMENT_BUY;
	}
	public String getExecuteKindNow(){
		return E_Const.GRAPH_INVESTMENT_NOW;
	}
	
	/** コンストラクタ */
	public E_InveCompareExtender(E_InveCompareController extension){
		super();
		this.extension = extension;
		this.pgTitle = PAGE_TITLE;
	}

	/** init */
	global override void init(){
		pageRef = doAuth(E_Const.ID_KIND.INVESTMENT, extension.record.id);
	}

	/** ページアクション */
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}
}