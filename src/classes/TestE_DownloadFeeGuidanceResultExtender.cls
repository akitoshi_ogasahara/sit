@isTest(SeeAllData=false)
public with sharing class TestE_DownloadFeeGuidanceResultExtender {

    private static User testuser;
    private static User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    private static E_IDCPF__c idcpf;
    private static Account account;
    private static E_ASPPF__c ASPPF;
    private static string  ZDSPFLAG01 = '1';
    static void init(){
        insertMessage();
        createDataCommon();
    }

    private static testMethod void normalTest() {
        init();
        system.runAs(testuser){
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceResult;
            testpage.getParameters().put('spfid', ASPPF.ID);
            Test.setCurrentPage(testpage);
            Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(new E_ASPPF__c());
            E_DownloadFeeGuidanceResultController controller = new E_DownloadFeeGuidanceResultController(standardcontroller);
            E_DownloadFeeGuidanceResultExtender extender = controller.getExtender();
            extender.getZDATBKTR();
            
            system.assertEquals(null,extender.pageAction());
        }
    }    
    
    //レコードなし
    private static testMethod void errorNoRecordTest() {
        init();
        system.runAs(testuser){
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceResult;
            testpage.getParameters().put('spfid', null);
            Test.setCurrentPage(testpage);
            //ASPPF = new E_ASPPF__c();
            Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(new E_ASPPF__c());
            E_DownloadFeeGuidanceResultController controller = new E_DownloadFeeGuidanceResultController(standardcontroller);
            E_DownloadFeeGuidanceResultExtender extender = controller.getExtender();
            //extender.isValidate();
            Pagereference  resultPref = extender.pageAction();
            system.assert(resultPref.getURL().contains('ERR'));
            system.assert(extender.errorMSGCode.equals(E_Const.ERROR_MSG_PARAMETER));
        }
    }    
    
    //閲覧権限なし
    private static testMethod void errorCantReadTest() {
        ZDSPFLAG01 = '0';
        init();
        system.runAs(testuser){
            //インスタンス
            Pagereference  testpage = Page.E_DownloadFeeGuidanceResult;
            testpage.getParameters().put('spfid', ASPPF.ID);
            Test.setCurrentPage(testpage);
            Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(new E_ASPPF__c());
            E_DownloadFeeGuidanceResultController controller = new E_DownloadFeeGuidanceResultController(standardcontroller);
            E_DownloadFeeGuidanceResultExtender extender = controller.getExtender();
            Pagereference  resultPref = extender.pageAction();
            system.assert(resultPref.getURL().contains('ERR'));
            system.assert(extender.errorMSGCode.equals('ERR|003'));
        }
    }    
    
    private static void insertMessage(){
        system.runAs(thisUser){
            LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
            String type = 'メッセージ';
            String param = 'ERR|003';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            param = 'DFG|003';
            insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
            insert insMessage;
        }
    }
    
    /* test data */
    static void createDataCommon(){
        system.runAs(thisUser){
            
            // User
            profile	profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
            testuser = new User(
                Lastname = 'fstest'
                , Username = 'fstest@terrasky.ingtesting'
                , Email = 'fstest@terrasky.ingtesting'
                , ProfileId = profile.Id
                , Alias = 'fstest'
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = 'ja'
                , CommunityNickName='tuser1'
            );
            insert testuser;
            // Account
            account = new Account(Name = 'testAccount',ownerid = testuser.id);
            insert account;

            //
            idcpf = new E_IDCPF__c(
                FLAG01__c = '1',
                FLAG02__c = '1',
                ZDSPFLAG01__c = ZDSPFLAG01,
                FLAG06__c = '1',
                //TRCDE01__c = 'TD06',
                //TRCDE02__c = 'TD06',
                User__c = testuser.Id,
                ZSTATUS02__c = '1',
                ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
            );
            insert idcpf;
            ASPPF = new E_ASPPF__c(Account__c = account.id);
            insert ASPPF;
            /*
            E_BizDataSyncLog__c ebiz = new E_BizDataSyncLog__c(                  
                Kind__c = '1'
                , InquiryDate__c = system.today()
                , DataSyncStartDate__c = system.now()
                , DataSyncEndDate__c = system.now()
            );
            insert ebiz;
*/
        }        
    }
    
}