@isTest
private with sharing class TestE_ProgressSearchAgentController{
		private static testMethod void testPageMethods() {	
			E_ProgressSearchAgentController page = new E_ProgressSearchAgentController(new ApexPages.StandardController(new E_CPCPF__c()));	
			page.getOperatorOptions_E_CPCPF_c_AgentName_c();	
			System.assert(true);
		}	
			
	private static testMethod void testprogressTable() {
		E_ProgressSearchAgentController.progressTable progressTable = new E_ProgressSearchAgentController.progressTable(new List<E_CPCPF__c>(), new List<E_ProgressSearchAgentController.progressTableItem>(), new List<E_CPCPF__c>(), null);
		progressTable.setPageItems(new List<E_ProgressSearchAgentController.progressTableItem>());
		progressTable.create(new E_CPCPF__c());
		progressTable.doDeleteSelectedItems();
		progressTable.setPagesize(10);		progressTable.doFirst();
		progressTable.doPrevious();
		progressTable.doNext();
		progressTable.doLast();
		progressTable.doSort();
		System.assert(true);
	}
	
}