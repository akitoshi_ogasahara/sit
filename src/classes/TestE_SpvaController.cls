@isTest
private with sharing class TestE_SpvaController{
	private static testMethod void testPageMethods() {		E_SpvaController extension = new E_SpvaController(new ApexPages.StandardController(new E_Policy__c()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testCRLPFTable() {
		E_SpvaController.CRLPFTable CRLPFTable = new E_SpvaController.CRLPFTable(new List<E_CRLPF__c>(), new List<E_SpvaController.CRLPFTableItem>(), new List<E_CRLPF__c>(), null);
		CRLPFTable.create(new E_CRLPF__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableZREV() {
		E_SpvaController.dataTableZREV dataTableZREV = new E_SpvaController.dataTableZREV(new List<E_TransitionWK__c>(), new List<E_SpvaController.dataTableZREVItem>(), new List<E_TransitionWK__c>(), null);
		dataTableZREV.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableSUMI() {
		E_SpvaController.dataTableSUMI dataTableSUMI = new E_SpvaController.dataTableSUMI(new List<E_TransitionWK__c>(), new List<E_SpvaController.dataTableSUMIItem>(), new List<E_TransitionWK__c>(), null);
		dataTableSUMI.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableZRCH() {
		E_SpvaController.dataTableZRCH dataTableZRCH = new E_SpvaController.dataTableZRCH(new List<E_TransitionWK__c>(), new List<E_SpvaController.dataTableZRCHItem>(), new List<E_TransitionWK__c>(), null);
		dataTableZRCH.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
	private static testMethod void testdataTableSTEP() {
		E_SpvaController.dataTableSTEP dataTableSTEP = new E_SpvaController.dataTableSTEP(new List<E_TransitionWK__c>(), new List<E_SpvaController.dataTableSTEPItem>(), new List<E_TransitionWK__c>(), null);
		dataTableSTEP.create(new E_TransitionWK__c());
		System.assert(true);
	}
	
}