public with sharing class SC_Search_agExtender extends SkyEditor2.Extender{
    
    private SC_Search_agSVEController controller;

    /**
     * Constructor
     */
    public SC_Search_agExtender(SC_Search_agSVEController extension){
        this.controller = extension;
    }
    
    /**
     * init override
     */
    public override void init(){
    	// ログインユーザの代理店格コード取得
    	E_AccessController access = E_AccessController.getInstance();
    	String agencyCode = access.getAccountCode();
    	
    	// 検索条件設定　ユーザの所属する代理店格コード
        SkyEditor2.Query tableQuery1 = this.controller.queryMap.get('resultTable');
		tableQuery1.addWhereIfNotFirst('AND');
		tableQuery1.addWhere('AccZHEADAY__c = \'' + agencyCode + '\'');
        this.controller.queryMap.put('resultTable',tableQuery1);

		// 年度の初期値セット
		this.controller.Component51_val.SkyEditor2__Text__c = system.Label.SC_FiscalYear;
		this.controller.Component51_val_dummy.SkyEditor2__Text__c = '["' + system.Label.SC_FiscalYear + '"]';
	
    	// 初期検索実行
    	this.controller.doSearch();
    }
}