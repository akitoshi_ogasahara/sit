@isTest
private class TestE_NNLinkRegHandler{
    
    //何もしないのでnullが返ってくることを確認
    @isTest static void testCreateUser(){     
        Test.startTest();
        	E_NNLinkRegHandler handler = new E_NNLinkRegHandler();
        	User result = handler.createUser( null,null,null,null,null,null);
        Test.stopTest();
        
        system.assertEquals(result, null);
    }

	//JPユーザの場合 有効になっていることの確認    
    @isTest static void testUpdateUser_JP(){
        Account acc = TestE_TestUtil.createAccount(false);
        acc.E_CL1PF_ZHEADAY__c = '1N20M';//JPユーザ
        insert acc;
        
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestE_TestUtil.createUser(false,'testName', 'E_PartnerCommunity');
        u.contactId = con.Id;
        insert u;
        
        E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, u.Id);
        eidc.ZSTATUS01__c = '1'; //パスワードステータス=1
        insert eidc;

        User sysAdmin = TestE_TestUtil.createUser( true, 'testSys', 'システム管理者' );
        system.runAs(sysAdmin){
            u.IsActive = false;
            update u;
            u = E_UserDao.getUserRecByUserId(u.Id);
            Test.startTest();
                E_NNLinkRegHandler handler = new E_NNLinkRegHandler();
                handler.handleUser(false, u, null, null, true);
            Test.stopTest();            
        }
        system.debug(u);
		User result = [select Id, IsActive from User where Id =:u.Id];
        
        //ユーザが有効になっていることの確認
        system.assertEquals( true , result.IsActive );        
        
    }

	//JPユーザかつパスワードステータス0の場合 有効になっていないことの確認    
    @isTest static void testUpdateUser_JP_PW0(){
        Account acc = TestE_TestUtil.createAccount(false);
        acc.E_CL1PF_ZHEADAY__c = '1N20M';//JPユーザ
        insert acc;
        
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestE_TestUtil.createUser(false,'testName', 'E_PartnerCommunity');
        u.contactId = con.Id;
        insert u;
        
        E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, u.Id);
        eidc.ZSTATUS01__c = '0'; //パスワードステータス=0
        insert eidc;

        User sysAdmin = TestE_TestUtil.createUser( true, 'testSys', 'システム管理者' );
        system.runAs(sysAdmin){
            u.IsActive = false;
            update u;           
        
            Test.startTest();
                E_NNLinkRegHandler handler = new E_NNLinkRegHandler();
                handler.updateUser( u.Id ,null,null,null,null,null,null);
            Test.stopTest();
		}
        
		User result = [select Id, IsActive from User where Id =:u.Id];
        //ユーザが無効のままになっていることの確認
        system.assertEquals( false , result.IsActive );        
        
    }
    
    //JPユーザ以外の場合
    @isTest static void testUpdateUser_NotJP(){
        Account acc = TestE_TestUtil.createAccount(false);
        acc.E_CL1PF_ZHEADAY__c = '12345';//JPユーザ以外の代理店
        insert acc;
        
		Contact con = TestE_TestUtil.createContact(true, acc.Id, 'testsan', '99999999', '');
		User u = TestE_TestUtil.createUser(false,'testName', 'E_PartnerCommunity');
        u.contactId = con.Id;
        insert u;
        
        E_IDCPF__c eidc = TestE_TestUtil.createIDCPF(false, u.Id);
        eidc.ZSTATUS01__c = '1'; //パスワードステータス=1
        insert eidc;

        User sysAdmin = TestE_TestUtil.createUser( true, 'testSys', 'システム管理者' );
        system.runAs(sysAdmin){
            u.IsActive = false;
            update u;           
        
            Test.startTest();
                E_NNLinkRegHandler handler = new E_NNLinkRegHandler();
                handler.updateUser( u.Id ,null,null,null,null,null,null);
            Test.stopTest();
        }
        
		User result = [select Id, IsActive from User where Id =:u.Id];
        //ユーザが無効のままになっていることの確認
        system.assertEquals( false , result.IsActive );        
        
    }
}