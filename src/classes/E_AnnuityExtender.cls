global with sharing class E_AnnuityExtender extends E_AbstractViewExtender {

  private static final String PAGE_TITLE = '年金内容個別照会';

  E_AnnuityController extension;

	//指定代理人の表示非表示フラグ
	public boolean isDesignatedProxyApplicantJudge {get;set;}
	//指定代理請求人の氏名
	public String designatedProxyApplicantName {get;set;}
	//指定代理請求人の割合(%)
	public Decimal designatedProxyApplicantRate {get;set;}
	//後継年金受取人の氏名
	public String nextRecipientName {get;set;}

	//ステップアップ金額の文言
	public String disclaimerStepUp {get;set;}
	//ステップアップ金額のディスクレーマー　（保険種類コードST）
	public String disclaimerStepUpST {get;set;}

    //4帳票通知リスト表示用
    public E_DownloadNoticeDTO noticeDTO {get;private set;}

	private static final String CRTABLE_STEP = 'ST';
    private static final String TYPE_STEP = 'ステップアップ金額の推移';
	private static final String BN = E_CRLPFDao.ROLE_BN; //顧客ロールは受取人
    private static final String BNYTYP40 = '40'; //受取人種別は指定代理請求人
    private static final String BNYTYP11 = '11'; //受取人種別は後継年金受取人

    //遷移元情報取得
    public String getFromType(){
    	return E_Const.FROM_ANNUITY;
    }

    /* コンストラクタ */
    public E_AnnuityExtender(E_AnnuityController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
    }

    /** init */
    global override void init(){
        // アクセスチェック
        pageRef = doAuth(E_Const.ID_KIND.POLICY, extension.record.id);
        if (pageRef == null) {
            // 契約者、被保険者、年金受取人情報を追加取得
            E_ContactDaoWithout.fillContactsToPolicy(extension.record, false, true,true,false, false);          
            // ステップアップ死亡保障額の初期化
            initDataTableStep();
            //保険顧客（受取人）
            initPolicyCustomer();
            // ステップアップ文言
            initDataTableStepST();

            // 4帳票通知リスト
            noticeDTO = new E_DownloadNoticeDTO(extension.record.Id);
        }
    }

    public PageReference pageAction () {
        return E_Util.toErrorPage(pageRef, null);
    }

    /**
     * セクションの表示非表示の制御に使用
     * ログインユーザが代理店/募集人である場合trueを返す
     * ※SVEからコール
     */
    public boolean getIsAgentJudge(){
        if(access.isAgent()){
            return true;
        }
        return false;
    }

    /**
     * セクションの表示非表示の制御に使用
     * ログインユーザが社員のとき、trueを返す
     * ※SVEからコール
     */
    public boolean getIsEmployeeJudge(){
        if(access.isEmployee()){
            return true;
        }
        return false;
    }

     /**
     * 積立金移転の[申込]および[ご利用方法]ボタンの表示制御に使用
     * 条件はIssでない かつ 移転可能フラグ(Annuity)true かつ トランザクションにTD06存在の時。
     * 下記メソッドはログインユーザのトランザクションの中にTD06が存在する場合、trueを返す
     * ※SVEからコール    要修正？
     */
    public boolean getIsFundJudge(){
        if(access.isTransactionCode('TD06')){
            return true;
        }
        return false;
    }

     /**
     * 次回年金支払日に使用
     * 年金支払開始日が到来し、年金支払開始前（ZANNSTAT≠A）の場合は、’-’（ハイフン）を右詰めで表示する。
     * ※以下はAppComponentで制御
     * 年金ステータス（ZANNSTAT=A（年金支払中)）の場合は、 SPVAヘッダDB(ESPH)「次回年金支払日(NEXTDATE)」=99999999の時、’-’（ハイフン）を右詰めで表示する。 
     * SPVAヘッダDB(ESPH)「次回年金支払日(NEXTDATE)」≠99999999の時 、SPVAヘッダDBの次回年金支払日（NEXTDATE)を表示する
     */
    public String getNEXTDATE(){
        if (extension.record.SPVA_ZANNSTAT__c != null) {
            if (extension.record.SPVA_ZANNSTAT__c.equals(E_Const.SPVA_ZANNSTAT_A)) {
                return extension.record.SPVA_NEXTDATE__c;
            } else {
                return '-';
            }
        }
        return extension.record.SPVA_NEXTDATE__c;
    }
    
     /**
     * 年金分割回数(年間)に使用
     * 年金支払開始前（ZANNSTAT≠A）の場合は、’-’（ハイフン）を右詰めで表示する。
     */
    public String getZANNFREQ(){
        if (extension.record.SPVA_ZANNSTAT__c != null) {
            if (extension.record.SPVA_ZANNSTAT__c.equals(E_Const.SPVA_ZANNSTAT_A)) {
				return extension.record.SPVA_ZANNFREQ__c;
            } else {
				return '-';
            }
        }
        return extension.record.SPVA_ZANNFREQ__c;
    }
	
	
	/**
	 * disclaimerStepUpを使用し、
	 * ディスクレーマーを組み立てます。
	 */

	private void initDataTableStepST(){
		disclaimerStepUpST ='';
		if(extension.record.COMM_CRTABLE2__c == CRTABLE_STEP){
			disclaimerStepUpST = '* ステップアップ金額は、契約日においては一時払保険料の額とします。以後、積立金額が次に定めるいずれかの金額に到達した場合、その金額（同日に複数の金額に到達した場合はいずれか高い金額）と同額とし、その日から適用します。<br>';
			disclaimerStepUpST += disclaimerStepUp ;
			disclaimerStepUpST +='<br>一部解約された場合には、基本給付金額と同じ割合で減額されたステップアップ金額を、新たなステップアップ金額とします。新たなステップアップ金額は、一部解約日の翌営業日翌日以降適用します。 ';
		}
	}


    /**
     * ステップアップ金額を初期化します。
     * そしてステップアップのディスクレーマー取得。
     */
    private void initDataTableStep(){
		  disclaimerStepUp = '';
      // ステップアップ金額の取得
      E_SUPPF__c eSuppf = E_SUPPFDao.getRecByPolicyId(extension.record.Id);
      if(eSuppf != null){
			  Boolean stepFlg = true;
			     //　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ

			     // ステップアップの履歴を最新から最大で6履歴表示
			     for(Integer i=1;i<=20;i++){
				      // 【ステップアップ情報】処理日01~06, ステップアップ金額01~06, ステップアップ金額率01~06
              if(i<=6){
                extension.dataTableSTEP.add(
                    new E_TransitionWK__c(
                      //種別
                      	Type__c = TYPE_STEP
                     	//日付
                  	   ,Column1Date__c = E_Util.toString(eSuppf.get('ZSTUPDAY0' + String.valueOf(i) + '__c'))
                  	  //金額率
                   	   ,Column2Number__c = E_Util.toDecimal(eSuppf.get('ZSTUPRAT0' + String.valueOf(i) + '__c'))
                 	    //金額
                  	   ,Column3Number__c = E_Util.toDecimal(eSuppf.get('ZSTUPSUM0' + String.valueOf(i) + '__c'))
                     	// 並び順
                   	   ,SortNumber__c = i
                  	)
              	);

            // ここからディスクレーマー関連
            //　【ステップアップ情報】商品ごとステップアップ金額率01~6
                if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')!=null){
                  if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')==0){
                	   stepFlg=false;
                	   //　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
                	   }
                	if(stepFlg){
                    if(i == 2){
                        // ステップアップ金額が２行目以降
                        disclaimerStepUp += 'または<br>';
                    }else if(i >=3){
                        // ステップアップ金額が３行目以降
                        disclaimerStepUp += '<br>'; 
                    }
						      disclaimerStepUp += '基本給付金額の'+eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')+'%の額　';
                }
				      }else{
					     stepFlg=false;
					     //　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
				      }
			     }
            //　【ステップアップ情報】商品ごとステップアップ金額率06~9
			     if(i>6 &&i<=9){
              if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')!=null){
            	   if(eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')==0){
                	   stepFlg=false;
                		  //　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
                	}
                  if(stepFlg){
                      disclaimerStepUp += '<br>';
						          disclaimerStepUp += '基本給付金額の'+eSuppf.get('ZATTMRAT0' + String.valueOf(i) + '__c')+'%の額　';
                	}
				        }else{
					       stepFlg=false;
					       //　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
				        }
                 //　【ステップアップ情報】商品ごとステップアップ金額率10~20
			         }else if(i>9 && i<=20){
				    if(eSuppf.get('ZATTMRAT' + String.valueOf(i) + '__c')!=null){
					     if(eSuppf.get('ZATTMRAT' + String.valueOf(i) + '__c')==0){
                	stepFlg=false;
                		//　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
                	}
					   if(stepFlg){
						    disclaimerStepUp += '<br>';
						    disclaimerStepUp += '基本給付金額の'+eSuppf.get('ZATTMRAT' + String.valueOf(i) + '__c')+'%の額　';
					   }
					}else{
						stepFlg=false;
						//　【ステップアップ情報】商品ごとステップアップ金額率01~20が0またはnullになったらディスクレーマーの処理を終わらせるためのフラグ
					}
				}
			}
		}
	}

    /**
     * 保険顧客（受取人）を取得します。
     */
    private void initPolicyCustomer(){
    	List<E_CRLPF__c> recipients = E_CRLPFDao.getRecsByType(extension.record.id,BN);
        if(recipients != null){
        	for(E_CRLPF__c rec : recipients){
        		if(rec.BNYTYP__c!=null && rec.BNYTYP__c!=''){
        			if(rec.BNYTYP__c.equals(BNYTYP40)){
        				designatedProxyApplicantName = rec.ZCLNAME__c;
        				designatedProxyApplicantRate = rec.BNYPC__c;
        				isDesignatedProxyApplicantJudge = true;
        			} else if (rec.BNYTYP__c.equals(BNYTYP11)) {
        				nextRecipientName = rec.ZCLNAME__c;
        			}
        		}
        	}
        }
    }
    
	/**
	 * 戻るボタン
	 */
	public override PageReference doReturn(){
		return new PageReference(E_CookieHandler.getCookieRefererPolicy());
	}

    // 4帳票追加
    public PageReference getE_Download(){
        PageReference ret = Page.E_DownloadPrintSheetsSelect;
        List<I_MenuMaster__c> menus = I_MenuMasterDao.getRecordsByLinkMenuKey('download,key-policy');
        if(menus.size() == 1){
            ret.getParameters().put(I_Const.URL_PARAM_MENUID, menus.get(0).id);
        }
        return ret;
    }
}