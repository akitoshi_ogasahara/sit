public with sharing class I_UserConfigController extends I_AbstractController {

	/**
	 * コンストラクタ
	 */
	public I_UserConfigController() {
		iris.irisMenus.clearAcitveMenu();
	}

	/*protected override String getLinkMenuKey() {
		return 'userConfig';
	}*/

	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}

		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}

		return null;
	}

	//Subカテゴリを持っている場合にサブナビを表示
	public Boolean getShowSubNav(){
		return String.isNotBlank(this.menu.subCategory__c);
	}

	// IRIS_Handler
	public E_IRISHandler getIrisHandler() {
		return E_IRISHandler.getInstance();
	}
}