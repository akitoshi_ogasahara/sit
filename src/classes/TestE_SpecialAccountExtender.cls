@isTest
private class TestE_SpecialAccountExtender
{
    /**
     * 繰入比率の変更が非表示の場合
     */
    private static testMethod void testSpecialAccount1() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '1';
        idcpf.FLAG02__c = '1';
        idcpf.FLAG03__c = '1';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            E_Policy__c policy = new E_Policy__c();
            PageReference pref = Page.E_Spva;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            E_SpecialAccountController controller = new E_SpecialAccountController(standardcontroller);
            E_SpecialAccountExtender extender = controller.getExtender();
            extender.init();
            resultPage = extender.PageAction();
            extender.getIsDispFundRatioLink();
            extender.getIsDispFundTransferLink();
            extender.getSumZi();
            extender.getSumZr();
            extender.getSumZh();

            //テスト終了
            Test.stopTest();
        }
        system.assertNotEquals(null, resultPage);

    }

    /**
     * 繰入比率の変更が表示の場合
     */
    private static testMethod void testSpecialAccount2() {

        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
        //ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
        E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        /*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
         *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
         *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
         *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
         */
        idcpf.FLAG01__c = '0';
        idcpf.FLAG02__c = '0';
        idcpf.FLAG03__c = '0';
        idcpf.TRCDE01__c = 'TD05';
        idcpf.TRCDE02__c = 'TD06';
        insert idcpf;
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            E_Policy__c policy = new E_Policy__c();
            policy.COMM_ZINVDCF__c = true;
            policy.COMM_ZTFRDCF__c = true;
            insert policy;
            // 特別勘定の登録
            E_SVCPF__c eSvcpf = new E_SVCPF__c();
            eSvcpf.ZFUNDNAM01__c = 'ファンド名';
            eSvcpf.CURRFROM__c = '19990101';
            eSvcpf.FLAG01__c = true;
            eSvcpf.ZEFUNDCD__c = 'V001';
            eSvcpf.ZCATFLAG__c = '10';

            insert eSvcpf;
            // 変額ファンドの登録
            E_SVFPF__c eSvfpf = new E_SVFPF__c();
            eSvfpf.E_SVCPF__c = eSvcpf.Id;
            eSvfpf.E_Policy__c = policy.Id;
            eSvfpf.ZINVPERC__c = 10;
            eSvfpf.ZREVAMT__c = 20;
            eSvfpf.ZHLDPERC__c = 30;
            insert eSvfpf;

            PageReference pref = Page.E_Spva;

            //テスト開始
            Test.startTest();
            Test.setCurrentPage(pref);
            Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
            ApexPages.CurrentPage().getParameters().put('type','1');
            E_SpecialAccountController controller = new E_SpecialAccountController(standardcontroller);
            E_SpecialAccountExtender extender = controller.getExtender();
            extender.init();
            resultPage = extender.PageAction();
            extender.getIsDispFundRatioLink();
            extender.getIsDispFundTransferLink();
            extender.getSumZi();
            extender.getSumZr();
            extender.getSumZh();

            //テスト終了
            Test.stopTest();
        }
        system.assertNotEquals(null, resultPage);

    }
}