public with sharing class CC_CaseSRActivityTgrHdl{

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){
		List<Case> caseList = new List<Case>();
		Set<Id> SRTypeIdList = new Set<Id>();

		//Filter Case and get SRTypeId list
		for(Case caseObj : newList){
			if(caseObj.CC_SRTypeId__c != null && String.isNotBlank(caseObj.CMN_Source__c)
			   && !(caseObj.CC_isNonDeliveryManage__c && caseObj.Status.equals('クローズ')) ){
				caseList.add(caseObj);
				SRTypeIdList.add(caseObj.CC_SRTypeId__c);
			}
		}

		//create SRActivity
		Map<Id,Case> oldMap = new Map<Id,Case>();
		if(caseList != null && caseList.size() > 0){
			createSRActivity(caseList, oldMap, SRTypeIdList);
		}
	}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		List<Case> caseList = new List<Case>();
		Set<Id> SRTypeIdList = new Set<Id>();

		//Filter Case and get SRTypeId list
		for(Case caseObj : newList){
			if(caseObj.CC_SRTypeId__c != null && String.isNotBlank(caseObj.CMN_Source__c)
				&& (caseObj.CC_SRTypeId__c != oldMap.get(caseObj.Id).CC_SRTypeId__c
						|| caseObj.CMN_Source__c != oldMap.get(caseObj.Id).CMN_Source__c)
				&& !(caseObj.CC_isNonDeliveryManage__c && caseObj.Status.equals('クローズ')) ){
				caseList.add(caseObj);
				SRTypeIdList.add(caseObj.CC_SRTypeId__c);
				SRTypeIdList.add(oldMap.get(caseObj.Id).CC_SRTypeId__c);
			}
		}

		//create SRActivity
		if(caseList != null && caseList.size() > 0){
			createSRActivity(caseList, oldMap, SRTypeIdList);
		}
	}

	/**
	 * Create SRActivity
	 */
	public static void createSRActivity(List<Case> caseList, Map<Id,Case> oldMap, Set<Id> SRTypeIdList){
		List<CC_SRActivity__c> createSRActivityList = new List<CC_SRActivity__c>();
		Map<Id, Id> caseAndActivityTemplateMap = new Map<Id, Id>();
		Set<Id> ActivityTemplateSet = new Set<Id>();
		List<Id> caseIdListFiltered = new List<Id>();

		//Get SRTypeMasterActivityTemplate list
		List<CC_SRTypeMasterActivityTemplate__c> SRTypeMasterActivityTemplateList = getSRTypeMasActiTemListBySRTypeIdList(SRTypeIdList);

		if(SRTypeMasterActivityTemplateList != null && SRTypeMasterActivityTemplateList.size() > 0){
			//Filter case which need to create SRActivity
			for(Case caseObj : caseList){
				//Case Insert
				if(oldMap.size() == 0){
					for(CC_SRTypeMasterActivityTemplate__c SRTypeMasterActivityTemplateObj : SRTypeMasterActivityTemplateList){
						if(SRTypeMasterActivityTemplateObj.CC_SRTypeMaster__c.equals(caseObj.CC_SRTypeId__c)){
							List<String> SourceList = SRTypeMasterActivityTemplateObj.CC_Source__c.split(';');
							if(SourceList.contains(caseObj.CMN_Source__c)){
								caseAndActivityTemplateMap.put(caseObj.Id, SRTypeMasterActivityTemplateObj.CC_ActivityTemplate__c);
								ActivityTemplateSet.add(SRTypeMasterActivityTemplateObj.CC_ActivityTemplate__c);
								caseIdListFiltered.add(caseObj.Id);
								break;
							}
						}
					}

				//Case Update
				}else if(oldMap.size() > 0){
					Case oldCaseObj = oldMap.get(caseObj.Id);
					Id oldCaseActivityTemplate;
					Id newCaseActivityTemplate;

					for(CC_SRTypeMasterActivityTemplate__c SRTypeMasterActivityTemplateObj : SRTypeMasterActivityTemplateList){
						List<String> SourceList = SRTypeMasterActivityTemplateObj.CC_Source__c.split(';');

						//ActivityTemplate of old Case
						if(SRTypeMasterActivityTemplateObj.CC_SRTypeMaster__c.equals(oldCaseObj.CC_SRTypeId__c)
							&& SourceList.contains(oldCaseObj.CMN_Source__c)){
							oldCaseActivityTemplate = SRTypeMasterActivityTemplateObj.CC_ActivityTemplate__c;
						}
						//ActivityTemplate of new Case
						if(SRTypeMasterActivityTemplateObj.CC_SRTypeMaster__c.equals(caseObj.CC_SRTypeId__c)
							&& SourceList.contains(caseObj.CMN_Source__c)){
							newCaseActivityTemplate = SRTypeMasterActivityTemplateObj.CC_ActivityTemplate__c;
						}
					}

					if(oldCaseActivityTemplate != newCaseActivityTemplate && newCaseActivityTemplate != null){
						caseAndActivityTemplateMap.put(caseObj.Id, newCaseActivityTemplate);
						ActivityTemplateSet.add(newCaseActivityTemplate);
						caseIdListFiltered.add(caseObj.Id);
					}
				}
			}

			if(ActivityTemplateSet.size() > 0){
				//Get ActivityTemplateActivityMaster list
				List<CC_ActivityTemplateActivityMaster__c> ActivityTemplateActivityMasterList = getActiTempActiMasterListByActiTempIdList(ActivityTemplateSet);

				if(ActivityTemplateActivityMasterList != null && ActivityTemplateActivityMasterList.size() > 0){
					//Get Queue list
					List<Group> queueList = getQueueList();

					//Create SRActivity by Case
					for(Id caseId : caseIdListFiltered){
						//Create SRActivity by ActivityTemplateActivityMaster
						for(CC_ActivityTemplateActivityMaster__c ActivityTemplateActivityMasterObj : ActivityTemplateActivityMasterList){
							//New SRActivity
							CC_SRActivity__c newSRActivity = new CC_SRActivity__c(
								CC_SRNo__c = caseId,
								CC_Status__c = '開始前' );

							//Set Order, SRTypeName, Owner
							if(caseAndActivityTemplateMap.get(caseId) == ActivityTemplateActivityMasterObj.CC_ActivityTemplate__c){
								newSRActivity.CC_Order__c = ActivityTemplateActivityMasterObj.CC_Order__c;					  //Order
								newSRActivity.CC_ActivityType__c = ActivityTemplateActivityMasterObj.CC_ActivityMaster__r.Name; //SRTypeName

								//Set Owner
								for(Group groupObj : queueList){
									if(String.isNotBlank(ActivityTemplateActivityMasterObj.CC_AssignedQueue__c)
										&& groupObj.name.equals(ActivityTemplateActivityMasterObj.CC_AssignedQueue__c)){
										newSRActivity.OwnerId = groupObj.Id;
										break;
									}else if(String.isBlank(ActivityTemplateActivityMasterObj.CC_AssignedQueue__c)
													&& groupObj.name.equals('UnAssigned')){
										newSRActivity.OwnerId = groupObj.Id;
										break;
									}
								}
							}

							//Add the new SRActivity to list
							createSRActivityList.add(newSRActivity);
						}
					}
				}
			}

			//Insert the new SRActivity list
			if(createSRActivityList != null && createSRActivityList.size() > 0){
				insert createSRActivityList;
			}
		}
	}

	/**
	 * Object: CC_SRTypeMasterActivityTemplate__c
	 * Parameter: List of CC_SRTypeMaster__c
	 * return: List<CC_SRTypeMasterActivityTemplate__c>
	 */
	public static List<CC_SRTypeMasterActivityTemplate__c> getSRTypeMasActiTemListBySRTypeIdList(Set<Id> SRTypeIdList){
		List<CC_SRTypeMasterActivityTemplate__c> srTypeMasterActivityTemplateList = [
							SELECT CC_SRTypeMaster__c, CC_ActivityTemplate__c, CC_Source__c
							  FROM CC_SRTypeMasterActivityTemplate__c
							 WHERE CC_ActivityTemplate__c != null
							   AND CC_Source__c != ''
							   AND CC_SRTypeMaster__c IN: SRTypeIdList ];

		return srTypeMasterActivityTemplateList;
	}

	/**
	 * Object: CC_ActivityTemplateActivityMaster__c
	 * Parameter: List of CC_ActivityTemplate__c
	 * return: List<CC_ActivityTemplateActivityMaster__c>
	 */
	public static List<CC_ActivityTemplateActivityMaster__c> getActiTempActiMasterListByActiTempIdList(Set<Id> ActivityTemplateSet){
		List<CC_ActivityTemplateActivityMaster__c> activityTemplateActivityMasterList = [
							SELECT CC_ActivityTemplate__c, CC_ActivityMaster__c, CC_ActivityMaster__r.Name, CC_Order__c, CC_AssignedQueue__c
							  FROM CC_ActivityTemplateActivityMaster__c
							 WHERE CC_ActivityMaster__c != null
							   AND CC_ActivityTemplate__c IN: ActivityTemplateSet ];

		return activityTemplateActivityMasterList;
	}

	/**
	 * Object: Group
	 * Parameter: None
	 * return: List<Group>
	 */
	public static List<Group> getQueueList(){
		List<Group> queueList = [SELECT Id, Name FROM Group WHERE Type = 'Queue'];

		return queueList;
	}

}