@isTest
private class TestI_CMSController {

	static String uniquekey = 'uniquekey';
	static String domID = 'domID';
	static String filePath = 'メッセージ|TEST';

	@isTest static void pageActionTest1() {
		//User actUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.debug('UserInfo='+actUser);
		System.debug('カスタム表示ラベル='+System.Label.isIRISEnableUser);
		//us.Username = 'testUser@terrasky.ingtesting';
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		createIRISCMS(ipm,true);
		System.runAs(actUser){
			//update us;

			PageReference pageRef = Page.IRIS;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	@isTest static void pageActionTest2() {
		I_PageMaster__c ipm = createIRISPage();
		createPickupIRISCMS(ipm);
		createIRISCMS();
		PageReference pageRef = Page.IRIS;
		pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
		Test.setCurrentPage(pageRef);
		String errMsgNo = E_Const.ERROR_MSG_NO_IDCPF.replace('|',EncodingUtil.urlEncode('|','UTF-8'));

		Test.startTest();
		I_CMSController cms = new I_CMSController();
		PageReference pageRefRes = cms.pageAction();
		cms.getShowSubNav();
		//System.assertEquals(pageRefRes.getUrl(),Page.E_AppSwitch.getUrl() + '?' +I_Const.APPSWITCH_URL_PARAM_APP + '=' +I_Const.APP_MODE_NNLINK);
		System.assertEquals(pageRefRes.getUrl(),Page.E_ErrorPage.getUrl() + '?code=' + errMsgNo);
		Test.stopTest();
	}

	//NNLinkファイル取得 ファイル情報補足あり
	@isTest static void pageActionTest3() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		//メッセージマスタのレコードタイプ「CMS」を取得
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
		 				  where SobjectType = 'E_MessageMaster__c' AND DeveloperName = 'CMS'];
		createCMSFile(rec,true);
		createIRISCMS2(ipm,false);
		System.runAs(actUser){
			//update us;

			PageReference pageRef = Page.IRIS;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	//NNLinkファイル取得 ファイル情報補足なし
	@isTest static void pageActionTest4() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		//メッセージマスタのレコードタイプ「CMS」を取得
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
		 				  where SobjectType = 'E_MessageMaster__c' AND DeveloperName = 'CMS'];
		createCMSFile(rec,false);
		createIRISCMS2(ipm,false);
		System.runAs(actUser){
			//update us;

			PageReference pageRef = Page.IRIS;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	@isTest static void pageActionFeatureTest() {
		I_MenuMaster__c mn = createFeatureMenu();
		I_PageMaster__c ipm = createFeaturePage(mn);
		createFeatureCMS(ipm,true);
		PageReference pageRef = Page.IRIS;
		pageRef.getParameters().put(I_Const.URL_PARAM_PAGE,ipm.Id);
		Test.setCurrentPage(pageRef);
		String errMsgNo = E_Const.ERROR_MSG_NO_IDCPF.replace('|',EncodingUtil.urlEncode('|','UTF-8'));

		Test.startTest();
		I_CMSController cms = new I_CMSController();
		PageReference pageRefRes = cms.pageAction();
		System.assertEquals(pageRefRes.getUrl(),Page.E_ErrorPage.getUrl() + '?code=' + errMsgNo);
		Test.stopTest();
	}

	@isTest static void getIrisMenuId1() {
		//User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.debug('UserInfo='+actUser);
		System.debug('カスタム表示ラベル='+System.Label.isIRISEnableUser);
		//us.Username = 'testUser@terrasky.ingtesting';
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		createIRISCMS(ipm,true);

		System.runAs(actUser){
			//update us;
			PageReference pageRef = Page.IRIS;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGEKEY,uniquekey);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	@isTest static void getIrisMenuId2() {
		//User us = [ select Id from User where Id = :UserInfo.getUserId() ];
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		System.debug('UserInfo='+actUser);
		System.debug('カスタム表示ラベル='+System.Label.isIRISEnableUser);
		//us.Username = 'testUser@terrasky.ingtesting';
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		createIRISCMS(ipm,true);

		System.runAs(actUser){
			//update us;
			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	@isTest static void doGWLogin1(){
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		PageReference pr = Page.IRIS;
		pr.getParameters().put(I_Const.URL_PARAM_PAGEKEY,'InternetUsePolicy');
		pr.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
		Test.setCurrentPage(pr);
		createIRISCMS(ipm,false);

		System.runAs(actUser){
			//update us;
			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	@isTest static void doGWLogin2(){
		num = 0;
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		PageReference pr = Page.IRIS;
		pr.getParameters().put(I_Const.URL_PARAM_PAGEKEY,uniquekey);
		pr.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			I_PageMaster__c ipm = createIRISPage();
			createIRISCMS(ipm,false);
			createCMSFile();

			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}

	@isTest static void doGWLogin3(){
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		PageReference pr = Page.IRIS;
		pr.getParameters().put(I_Const.URL_PARAM_PAGEKEY,uniquekey);
		//pr.getHeaders().put('X-Salesforce-SIP', System.Label.E_CMN_GW_IP_ADRS);
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			I_PageMaster__c ipm = createIRISPage();
			createCMSFile();
			E_CMSFile__c cm = [SELECT UpsertKey__c FROM E_CMSFile__c];

			createIRISCMS(ipm,false);
			createIRISCMS(ipm,false);
			I_ContentMaster__c icm = createIRISCMS(ipm,false);
			icm.filePath__c = cm.UpsertKey__c;
			update icm;
			System.debug('テストデバッグテスト' + cm.UpsertKey__c + '=' + icm.filePath__c);

			Test.startTest();
			I_CMSController cms = new I_CMSController();
			PageReference pageRefRes = cms.pageAction();
			System.assertEquals(pageRefRes,null);
			Test.stopTest();
		}
	}


	//ID管理
	static void createIDManage(User us){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.User__c = us.Id;
		ic.AppMode__c = 'IRIS';
		ic.ZINQUIRR__c = 'BR00';
		insert ic;
	}

	//irisPage
	static I_PageMaster__c createIRISPage(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = uniquekey;
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(I_PageMaster__c ipm,Boolean create){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		icm.SelectFrom__c = 'CMSコンテンツ';
		icm.ClickAction__c = 'DL(NNLink)';
		icm.filePath__c = uniquekey;
		insert icm;
		if(create){
			createChildIRISCMS(icm);
		}else{
			createChildIRISCMS2(icm);
		}
		return icm;
	}

	//irisCMSSection 子CMSはNNLinkファイル情報取得
	static I_ContentMaster__c createIRISCMS2(I_PageMaster__c ipm,Boolean create){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		icm.Style__c = 'ダウンロードリンク';
		insert icm;
		if(create){
			createChildIRISCMS(icm);
		}else{
			createChildIRISCMS3(icm);
		}
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		insert icm;
		return icm;
	}

	static Integer num = 0;
	static I_ContentMaster__c createChildIRISCMS2(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		if(num == 0) icm.CanDisplayVia__c = I_Const.ACCESS_VIA_INTERNET;
		if(num == 1) icm.CanDisplayVia__c = I_Const.ACCESS_VIA_CMN_GW;
		icm.ClickAction__c = 'DL(NNLink)';
		insert icm;
		System.debug('テストデバッグCHILD' + icm);
		num++;
		return icm;
	}

	//NNLinkファイル情報取得CMS
	static I_ContentMaster__c createChildIRISCMS3(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.ClickAction__c = 'DL(NNLink)';
		icm.filePath__c = filePath;
		insert icm;
		System.debug('テストデバッグCHILD' + icm);
		return icm;
	}

	static E_CMSFile__c createCMSFile(){
		E_MessageMaster__c ms = new E_MessageMaster__c(Key__c = 'TEST',UpsertKey__c=uniquekey);
		insert ms;

		E_CMSFile__c ecf = new E_CMSFile__c();
		ecf.UpsertKey__c = uniquekey;
		ecf.MessageMaster__c = ms.Id;
		insert ecf;
		System.debug('テストデバッグCMS' + ecf);
		return ecf;
	}

	//レコードタイプ設定CMSFile
	static E_CMSFile__c createCMSFile(RecordType rec,Boolean noteflag){
		E_MenuMaster__c menu = new E_MenuMaster__c();
		menu.Name = 'テストメニュー';
		menu.MenuMasterKey__c = uniquekey;
		insert menu;
		E_MessageMaster__c ms = new E_MessageMaster__c();
		ms.Key__c = '1';
		ms.Type__c = 'CMS';
		ms.UpsertKey__c = uniquekey;
		ms.RecordTypeId = rec.Id;
		ms.Menu__c = menu.Id;
		insert ms;

		E_CMSFile__c ecf = new E_CMSFile__c();
		ecf.MessageMaster__c = ms.Id;
		ecf.sortOrder__c = 1;
		ecf.UpdateDate__c = String.valueOf(E_Util.SYSTEM_TODAY());
		if(noteflag){
			ecf.Note__c = 'テストCMSファイル';
		}
		insert ecf;
		System.debug('テストデバッグCMS' + ecf);
		E_CMSFile__c cmsfile = [select id,UpsertKey__c from E_CMSFile__c where id = :ecf.id];
		filePath = cmsfile.UpsertKey__c;
		return ecf;
	}

	static I_MenuMaster__c createFeatureMenu(){
		I_MenuMaster__c mn = new I_MenuMaster__c();
		mn.Name = '特集';
		mn.MainCategory__c = I_Const.MENU_MAIN_CATEGORY_LIBLARY;
		mn.SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE;

		insert mn;
		return mn;
	}

	static I_PageMaster__c createFeaturePage(I_MenuMaster__c mn){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = uniquekey;
		pm.Menu__c = mn.Id;
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createFeatureCMS(I_PageMaster__c ipm,Boolean create){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;

		createChildFeatureCMS(icm);

		return icm;
	}

	static I_ContentMaster__c createChildFeatureCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.FeatureContents__c = I_Const.IRIS_CONTENTS_NAME_SALESTOOL;
		insert icm;
		return icm;
	}

	//irisCMS
	static I_ContentMaster__c createPickupIRISCMS(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		icm.SelectFrom__c = 'CMSコンテンツ';
		icm.SelectLimit__c = 20;
		icm.SelectCondition__c = 'ParentContent__r.Name = \'' + I_Const.IRIS_CONTENTS_NAME_SALESTOOL + '\'';
		icm.filePath__c = uniquekey;
		insert icm;
		return icm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Style__c = 'セールスツール';
		icm.Name = I_Const.IRIS_CONTENTS_NAME_SALESTOOL;
		insert icm;

		createSalestoolChildIRISCMS(icm);

		return icm;
	}

	static I_ContentMaster__c createSalestoolChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.Category__c = I_Const.CMSCONTENTS_CATEGORY_RECRUIT + '（＊）';
		insert icm;
		return icm;
	}



}