public with sharing class I_ContentSearchBoxController extends I_AbstractSearchBoxController {

    public static final String SEARCH_LABEL_CATEGORY_CONTENT = 'コンテンツ検索';
    public static final String SEARCH_TYPE_PAGE_AND_FILE = 'page-and-file';
    public static final String SEARCH_TYPE_MOVIE = 'movie';
    public static final String SEARCH_TYPE_LABEL_PAGE_AND_FILE = 'ファイル・ページ';
    public static final String SEARCH_TYPE_LABEL_MOVIE = '動画';

    // 表示制御 - ページ・ファイルリストを表示
    public Boolean showPageAndFileList {get {return this.isSearchedByPageAndFile();} private set;}
    public Boolean showMovieList {get {return this.isSearchedByMovie();} private set;}

    // 動画取得リクエストを送ってから受信するまでTRUE
    public Boolean isRequestingToNNtube {get; set;}

    // json文字列受け取り用変数
    public String nntubeJsonResult{get; set;}

    public List<I_ContentSearchResult> contentRows {get; set;}
    public List<I_FileSearchResult> fileRows {get; set;}
    public List<I_MovieSearchResult> movieRows {get; set;}

    /**
     * @contructor
     */
    public I_ContentSearchBoxController() {
        super();

        searchCategory = SEARCH_LABEL_CATEGORY_CONTENT;

        searchTypeItems = new List<SelectOption>{
            new SelectOption(SEARCH_TYPE_PAGE_AND_FILE, SEARCH_TYPE_LABEL_PAGE_AND_FILE),
            new SelectOption(SEARCH_TYPE_MOVIE, SEARCH_TYPE_LABEL_MOVIE)
        };

        this.flush();
    }

    /**
     * 検索カテゴリラベルを返却する。
     */
    public String getSearchCategoryLabel() {
        return SEARCH_LABEL_CATEGORY_CONTENT;
    }

    /**
     * これまで保持していたものを初期化する。
     */
    public void flush() {
        this.contentRows = new List<I_ContentSearchResult>();
        this.fileRows = new List<I_FileSearchResult>();
        this.movieRows = new List<I_MovieSearchResult>();
        keyword = '';
        searchType = SEARCH_TYPE_PAGE_AND_FILE;
        searchTypeName = SEARCH_TYPE_LABEL_PAGE_AND_FILE;
        isSearch = false;
        isRequestingToNNtube = false;
        searchMessages.clear();
        getSeachBoxLogRecord(searchTypeName);
    }

    /**
     * apex:actionfunctionのパラメータから検索を行う
     */
    public void doSearchLog() {
        searchType = ApexPages.currentPage().getParameters().get('searchType');
        keyword = ApexPages.currentPage().getParameters().get('keyword');

        doSearch();
    }

    /**
     * NNTube用のヘルパーインスタンスを返却する。
     */
    public I_TubeConnectHelper getTubeHelper() {
        return I_TubeConnectHelper.getInstance();
    }

    /**
     * 検索を行う
     * NNTubeへ検索を行う際にも実行され、有効であれば画面側で検索用スクリプトの実行を行う。
     */
    public void doSearch() {
        searchMessages.clear();
        System.debug(fileRows);

        this.contentRows = new List<I_ContentSearchResult>();
        this.fileRows = new List<I_FileSearchResult>();
        this.movieRows = new List<I_MovieSearchResult>();
        exeSearchType = searchType;
        rowCount = I_Const.LIST_DEFAULT_ROWS;
        isSearch = false;
        isRequestingToNNtube = false;

        // 入力チェック
        if (!isInputValidate()) {
            return;
        }
        // soslの実行にはは2文字以上の指定が必要
        if (keyword.trim().length() < 2) {
            searchMessages.add('検索キーワードを2文字以上入力してください。');
            return;
        }



        getSeachBoxLogRecord(searchTypeName);
        if (this.isSearchedByPageAndFile()) {
            this.searchForPageAndFile();
        } else {
            this.searchForMovie();
        }
        // 戻るボタンの遷移先をセット（IRIS_Top）
        setRefererContactSearch();


    }

    /**
     * 検索を実施したのがファイル・ページであった場合TRUE
     */
    public Boolean isSearchedByPageAndFile() {
        return exeSearchType == SEARCH_TYPE_PAGE_AND_FILE;
    }

    /**
     * 検索を実施したのが動画であった場合TRUE
     */
    public Boolean isSearchedByMovie() {
        return exeSearchType == SEARCH_TYPE_MOVIE;
    }

    /**
     * 検索ログを取得し内部で保持する
     */
    public void getSeachBoxLog() {
        isRequestingToNNTube = false;
        getSeachBoxLogRecord(searchTypeName);
        return;
    }

    /**
     * ファイル・ページ検索を実施する
     */
    public void searchForPageAndFile() {
        List<String> keywords = keyword.replaceAll('\\(|\\)|（|）', '　').replaceAll('^[\\s　]+|[\\s　]+$', '').split('[\\s　]+');/* スペース文字 + かなスペース */
        List<List<SObject>> multipleSobjectResults = searchNoticeAndQA(keywords);
        multipleSobjectResults.addAll(searchPageContent(keywords));
        this.contentRows.addAll(extractEnabledContentResults(multipleSobjectResults, keywords));

        List<List<SObject>> fileSearchList = searchFileContent(keywords);
    System.debug(fileSearchList);
        this.fileRows.addAll(extractEnabledFileResults(fileSearchList, keywords));

        // 集計結果がゼロ件の場合はメッセージを表示
        if (this.contentRows.isEmpty() && this.fileRows.isEmpty()) {
            searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
            return;
        }
        isSearch = true;
    }

    /**
     * NNTubeの検索結果をApexコード内で利用できるようパースし、画面出力用として保持する。
     */
    public void renderNNTubeMedia() {
        I_MovieSearchResult.NNTubeResult result;
        try {
            if (!this.isSearchedByMovie()) {
                throw new ClassException('NNTubeとの接続に失敗しました。');
            }
            result = (I_MovieSearchResult.NNTubeResult)JSON.deserialize(nntubeJsonResult, I_MovieSearchResult.NNTubeResult.class);
            if (result.resultcode != '0000') {
                throw new ClassException('NNTubeとの接続に失敗しました。');
            }
            if (result.moviesum == null || result.moviesum == 0 || result.meta == null) {
                throw new ClassException(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
            }
            for (I_MovieSearchResult.NNTubeMedia media: result.meta) {
                this.movieRows.add(I_MovieSearchResult.build(media));
            }
        } catch (ClassException e) {
            searchMessages.add(e.getMessage());
            this.movieRows.clear();
            result = null;
        } catch (Exception e) {

            searchMessages.add('予期しないエラーが発生しました。');
            searchMessages.add(e.getMessage());
            this.movieRows.clear();
            result = null;
        }
        isRequestingToNNtube = false;
        nntubeJsonResult = null;
    }

    /**
     * NNTubeへ検索を行うためのパラメータ設定を行う
     */
    public void searchForMovie() {
        isSearch = true;
        isRequestingToNNtube = true;
    }


    /**
     * 各オブジェクトの検索結果からファイル表示用として有効なデータを取り出す
     * @param List<List<SObject>> searchList
     * @param List<String> keywords
     */
    public static List<I_FileSearchResult> extractEnabledFileResults(List<List<SObject>> searchList, List<String> keywords) {
        List<String> fuzzyPatternLiterals = new List<String>();
        I_KeywordMarker marker = new I_KeywordMarker();
        for (String keyword: keywords) {
            marker.addKeyword('(?i)' + I_KeywordMarker.createFuzzyPattern(keyword));
        }
        Map<Id, I_ContentMaster__c> recordsById = new Map<Id, I_ContentMaster__c>();
        Set<String> nnlinkKeys = new Set<String>();
        Map<Id, String> docsById = new Map<Id, String>();
        for (List<SObject> searchResults: searchList) {
            for (Sobject searchResult: searchResults) {
                if (searchResult instanceof I_ContentMaster__c) {
                    I_ContentMaster__c record = (I_ContentMaster__c)searchResult;
                    recordsById.put(record.id, record);
                    if (I_FileSearchResult.isNNLink(record)) {
                        nnlinkKeys.add(record.filePath__c);
                    }
                }
            }
        }
        Set<Id> fileRetrievalIds = new Set<Id>();
        fileRetrievalIds.addAll(recordsById.keyset());
        // Dl attachment
        Map<Id, List<Attachment>> attachmentMapsById = recordsById.size() > 0 ? E_AttachmentDao.getRecsByParentIds(fileRetrievalIds) : new Map<Id, List<Attachment>>();
        // Dl chatter
        Map<Id, Id> chatterFileIdsById = recordsById.size() > 0 ? I_ContentMasterDao.getChatterFileIdsByRecId(fileRetrievalIds) : new Map<Id, Id>();
        // Dl NNlink
        Map<String, E_CMSFile__c> nnLinkFilesByFilePath = nnlinkKeys.size() > 0 ? E_MessageMasterDao.getCMSFilesWithAttachmentByKeys(nnlinkKeys) : new Map<String, E_CMSFile__c>();

        List<I_FileSearchResult> results = new List<I_FileSearchResult>();
        for (Id contentId: recordsById.keyset()) {
            I_ContentMaster__c record = recordsById.get(contentId);
            I_FileSearchResult result;
            if (I_FileSearchResult.isNNLink(record) && nnLinkFilesByFilePath.containsKey(record.filePath__c)) {
                E_CMSFile__c cmsFile = nnLinkFilesByFilePath.get(record.filePath__c);
                result = I_FileSearchResult.build(record, new I_CMSComponentController(record, cmsFile.Attachments, chatterFileIdsById.get(record.Id)));
                result.setMarker(marker);

            //20180309 URL(Webコンテンツ)追加 START
            } else if(I_FileSearchResult.isWebContents(record)){
                result = I_FileSearchResult.build(record, new I_CMSComponentController(record, null, null));
                result.fileUrl = record.LinkURL__c;
                result.setMarker(marker);
            //20180309 URL(Webコンテンツ)追加 END
            } else {
                result = I_FileSearchResult.build(record, new I_CMSComponentController(record, attachmentMapsById.get(record.Id), chatterFileIdsById.get(record.Id)));
                result.setMarker(marker);
            }
            if (String.isNotBlank(result.fileUrl) && String.isNotBlank(result.pageUrl) && result.hasAccess() && result.isMatched()) {
                results.add(result);
            }
        }
        return results;
    }

    /**
     * 各オブジェクトの検索結果からページ表示用として有効なデータを取り出す
     * @param List<List<SObject>> searchList
     * @param List<String> keywords
     */
    public static List<I_ContentSearchResult> extractEnabledContentResults(List<List<SObject>> searchList, List<String> keywords) {
        I_KeywordMarker marker = new I_KeywordMarker();
        for (String keyword: keywords) {
            marker.addKeyword('(?i)' + I_KeywordMarker.createFuzzyPattern(keyword));
        }
        Map<Id, I_ContentSearchResult> resultsById = new Map<Id, I_ContentSearchResult>();
        for (List<SObject> searchResults : searchList) {
            for (Sobject searchResult: searchResults) {
                if (searchResult instanceof I_PageMaster__c) {
                    I_PageMaster__c record = (I_PageMaster__c)searchResult;
                    I_ContentSearchResult result = I_ContentSearchResult.build(record);
                    result.setMarker(marker);
                    if (String.isNotBlank(result.url) && result.hasAccess() && result.isMatched()) {
                        resultsById.put(record.Id, result);
                    }
                } else if (searchResult instanceof I_MenuMaster__c) {
                    I_MenuMaster__c record = (I_MenuMaster__c)searchResult;
                    // ページデータを優先する。
                    I_MenuItem menuItem = I_MenuProvider.getInstance().menusById.get(record.id);
                    if (menuItem != null && menuItem.record.Pages__r != null && menuItem.record.Pages__r.size() > 0) {
                        I_PageMaster__c pageRec = menuItem.record.Pages__r.get(0);
                        if (!resultsById.containsKey(pageRec.Id)) {
                            I_ContentSearchResult result = I_ContentSearchResult.build(record);
                            result.setMarker(marker);
                            if (String.isNotBlank(result.url) && result.hasAccess() && result.isMatched()) {
                                resultsById.put(pageRec.Id, result);
                            }
                        }
                    } else {
                        I_ContentSearchResult result = I_ContentSearchResult.build(record);
                        result.setMarker(marker);
                        if (String.isNotBlank(result.url) && result.hasAccess() && result.isMatched()) {
                            resultsById.put(record.Id, result);
                        }
                    }
                } else if (searchResult instanceof I_ContentMaster__c) {
                    I_ContentMaster__c record = (I_ContentMaster__c)searchResult;
                    I_ContentSearchResult result = I_ContentSearchResult.build(record);
                    result.setMarker(marker);
                    if (String.isNotBlank(result.url) && result.hasAccess() && result.isMatched()) {
                        resultsById.put(record.Id, result);
                    }
                }
            }
        }
        return resultsById.values();
    }

    /**
     * ファイルデータ取得のためSOSLを実施する
     * @param List<String> keywords
     */
    public static List<List<SObject>> searchFileContent(List<String> keywords) {
        Date sysday = E_Util.SYSTEM_TODAY();
        List<String> normalizeKeyword = new List<String>();
        for(String k : keywords){
            normalizeKeyword.add(k.replaceAll('(\\-)', '\\\\$1'));
        }
        List<String> clickActions = new List<String>{'DL(ChatterFile)', 'DL(Attachment)', 'DL(Contents)', 'DL(NNLink)', 'URL(Webコンテンツ)', 'URL'};
        return [
            FIND: String.join(normalizeKeyword, ' AND ')
            IN ALL FIELDS
            RETURNING
                I_ContentMaster__c (
                        Id,
                        Name,
                        Content__c,
                        ClickAction__c,
                        filePath__c,
                        DocumentAndBranchNo__c,
                        DocumentNo__c,
                        Dom_Id__c,
                        CanDisplayVia__c,
                        FeatureFlag__c,
                        FeatureSearch__c,
                        FeatureContents__c,
                        LinkURL__c,
                        Category__c,
                        DocumentCategory__c,
                        FormNo__c,
                        PamphletCategory__c,
                        isEmployeeCategory__c,
                        Page__c,
                        Page__r.Name,
                        Page__r.Menu__c,
                        Page__r.page_unique_key__c,
                        ParentContent__c,
                        ParentContent__r.Name,
                        ParentContent__r.Page__r.Name,
                        ParentContent__r.Page__r.Menu__c,
                        ParentContent__r.Page__r.page_unique_key__c
                    WHERE
                        ClickAction__c IN: clickActions
                    AND
                        FeatureContents__c = null
                    AND
                    (
                        (
                            ParentContent__r.Page__r.Menu__r.MainCategory__c =: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                            AND
                            ParentContent__r.Page__r.Menu__r.subCategory__c =: I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
                            AND
                            ParentContent__r.Page__r.FeatureDisplayFrom__c <=: sysday
                            AND
                            ParentContent__r.Page__r.FeatureDisplayTo__c >=: sysday
                        )
                        OR
                        (
                            (
                                (
                                    ParentContent__r.Page__r.Menu__r.MainCategory__c =: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                                    AND
                                    ParentContent__r.Page__r.Menu__r.subCategory__c !=: I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
                                )
                                OR
                                Page__r.Menu__r.MainCategory__c !=: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                                OR
                                ParentContent__r.Page__r.Menu__r.MainCategory__c !=: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                            )
                            AND
                            (
                                (
                                    DisplayFrom__c = null
                                    AND
                                    ValidTo__c = null
                                )
                                OR
                                (
                                    DisplayFrom__c <=: sysday
                                    AND
                                    ValidTo__c >=: sysday
                                )
                                OR
                                (
                                    DisplayFrom__c = null
                                    AND
                                    ValidTo__c >=: sysday
                                )
                                OR
                                (
                                    DisplayFrom__c <=: sysday
                                    AND
                                    ValidTo__c = null
                                )
                            )
                        )
                        OR
                        (
                            DocumentCategory__c IN: I_Const.DR_DOCUMENT_CATEGORIES
                            AND
                            DisplayFrom__c <=: Date.today()
                            AND
                            (
                                ValidTo__c >=: Date.today()
                                OR
                                ValidTo__c = NULL
                            )
                        )
                        OR
                        (
                            PamphletCategory__c != null
                            AND
                            DisplayFrom__c <=: Date.today()
                        )

                    )
                    LIMIT 100
                )
        ];
    }

    /**
     * QA・お知らせ以外のデータ取得のためSOSLを実施する
     * @param List<List<SObject>> keywords
     */
    public static List<List<SObject>> searchPageContent(List<String> keywords) {
        Date sysday = E_Util.SYSTEM_TODAY();
        List<String> clickActions = new List<String>{'URL'};

        return [
            FIND: String.join(keywords, ' AND ')
            IN NAME FIELDS
            RETURNING
                I_MenuMaster__c (
                        Id,
                        Name
                    WHERE
                        ExcludeFromSearch__c = false
                    AND
                    (
                        MainCategory__c != '3.資料・動画'
                        OR
                        (
                            MainCategory__c = '3.資料・動画'
                            AND
                            (
                                ParentMenu__c != null
                                OR
                                (
                                    ParentMenu__c = null
                                    AND
                                    LinkURL__c != null
                                )
                            )
                        )
                    )
                    LIMIT 100
                ),
                I_PageMaster__c (
                        Name,
                        Description__c,
                        page_unique_key__c,
                        Default__c,
                        Menu__c
                    WHERE
                        ExcludeFromSearch__c = false
                    AND
                    (
                        (
                            Menu__r.MainCategory__c !=: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                        )
                        OR
                        (
                            Menu__r.MainCategory__c =: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                            AND
                            Menu__r.subCategory__c !=: I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
                        )
                        OR
                        (
                            Menu__r.MainCategory__c =: I_Const.MENU_MAIN_CATEGORY_LIBLARY
                            AND
                            Menu__r.subCategory__c =: I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE
                            AND
                            FeatureDisplayFrom__c <=: sysday
                            AND
                            FeatureDisplayTo__c >=: sysday
                        )
                    )
                    LIMIT 100
                ),
                I_ContentMaster__c (
                        Name,
                        Id,
                        isHTML__c,
                        Content__c,
                        ClickAction__c,
                        LinkURL__c,
                        Dom_Id__c,
                        Category__c,
                        CanDisplayVia__c,
                        PamphletCategory__c,
                        isEmployeeCategory__c,
                        ParentContent__c,
                        ParentContent__r.Page__c,
                        ParentContent__r.Page__r.Menu__c,
                        ParentContent__r.Page__r.Menu__r.MainCategoryAlias__c,
                        ParentContent__r.Page__r.Menu__r.Name,
                        ParentContent__r.Page__r.Menu__r.requiredDefaultPageId__c,
                        ParentContent__r.Page__r.Menu__r.DestUrl__c,
                        ParentContent__r.Page__r.Menu__r.urlParameters__c
                    WHERE
                        ExcludeFromSearch__c = false
                    AND
                        ClickAction__c IN: clickActions
                    AND
                    (
                        ParentContent__r.Name !=: I_Const.IRIS_CONTENTS_NAME_SALESTOOL
                        OR
                        (
                            ParentContent__r.Name =: I_Const.IRIS_CONTENTS_NAME_SALESTOOL
                            AND
                            DisplayFrom__c <=: sysday
                            AND
                            ValidTo__c >=: sysday
                        )

                    )
                    AND
                        ParentContent__r.Name !=: I_Const.IRIS_CONTENTS_NAME_INFO
                    AND
                        ParentContent__r.Page__r.Menu__r.MainCategoryAlias__c != 'よくあるご質問'
                    LIMIT 100
                )
        ];
    }

    /**
     * QA・お知らせデータ取得のためSOSLを実施する
     * @param List<List<SObject>> keywords
     */
    public static List<List<SObject>> searchNoticeAndQA(List<String> keywords) {
        Date sysday = E_Util.SYSTEM_TODAY();
        String conday = String.valueOf(sysday).replaceAll('-', '/');
        return [
            FIND: String.join(keywords, ' AND ')
            IN ALL FIELDS
            RETURNING
                I_ContentMaster__c (
                        Name,
                        Id,
                        isHTML__c,
                        Content__c,
                        ClickAction__c,
                        LinkURL__c,
                        Dom_Id__c,
                        CanDisplayVia__c,
                        InfoLabel__c,   //20180214 資料発送申込対応
                        Category__c,
                        PamphletCategory__c,
                        isEmployeeCategory__c,
                        ParentContent__c,
                        ParentContent__r.Page__c,
                        ParentContent__r.Page__r.Menu__c,
                        ParentContent__r.Page__r.Menu__r.MainCategoryAlias__c,
                        ParentContent__r.Page__r.Menu__r.Name,
                        ParentContent__r.Page__r.Menu__r.requiredDefaultPageId__c,
                        ParentContent__r.Page__r.Menu__r.DestUrl__c,
                        ParentContent__r.Page__r.Menu__r.urlParameters__c
                    WHERE
                        ExcludeFromSearch__c = false
                    AND
                    (
                        (
                            ParentContent__r.Name =: I_Const.IRIS_CONTENTS_NAME_INFO
                            AND
                            DateStr__c <=: conday
                        )
                        OR
                        ParentContent__r.Page__r.Menu__r.MainCategoryAlias__c = 'よくあるご質問'
                    )
                    LIMIT 100
                )
        ];
    }

    private class ClassException extends Exception {}
}