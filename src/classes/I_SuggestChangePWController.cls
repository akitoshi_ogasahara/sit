//スプリントバックログ対応　#2 ここから 2017/1/18
public with sharing class I_SuggestChangePWController extends E_AbstractController{
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get; set;}

	//タイトル
	public String title{get;set;}

	public I_SuggestChangePWController() {
		pageMessages = getPageMessages();
		title = 'パスワード変更のご案内';
	}

	//ページアクション
	public PageReference pageAction () {
		return E_Util.toErrorPage(pageRef, null);
	}

	//次回へボタン押下
	public override PageReference doReturn(){
		//IDリクエストレコード作成
		E_IDRequest__c eidr = new E_IDRequest__c();
		E_IDCPF__c eidcRec = access.idcpf;	//ID管理検索
		eidr.E_IDCPF__c = eidcRec.Id;		//ID管理
		eidr.ZUPDFLAG__c = E_Const.ZUPDFLAG_PW;	//リクエスト種別="PW_C"
		eidr.note__c = 'パスワード変更キャンセル';		//備考="パスワード変更キャンセル"

		//IDリクエストを登録
		insert eidr;
		return Page.E_Home;
	}

	//変更ボタン押下
	public PageReference changePW(){
		if(iris.getIsIRIS()){
			return Page.IRIS_ChangePW;
		}
		return Page.E_StandardPasswordChange;
	}
}
//スプリントバックログ対応 #2 ここまで