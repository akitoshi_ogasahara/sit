/**
 * 営業日報データアクセスオブジェクト
 * CreatedDate 2018/04/27
 * @Author Terrasky
 */
public class ASS_EventDao {
	/**
	 * 行動レコード取得
	 * @param eventId: 行動のID 
	 * @return Event: 行動レコード
	 */
	public static Event getEventsById(Id eventId) {
		return [SELECT 
					Id
					, OwnerId
					, EventType__c
					, WhatId
					, Subject
					, StartDateTime
					, EndDateTime
					, Action__c
					, Reason__c
					, ToDo__c
					, DesignDocReq__c
					, IsOpportunity__c
					, Description
				FROM 
					Event
				WHERE 
					Id = :eventId
				];
	}

	/**
	 * 行動リスト取得
	 * @param offIdSet: 事務所IDセット 
	 * @return List<Event>: 行動リスト
	 */
	public static List<Event> getThisMonthEventsByOffIdSet(Id recTypeId ,Set<Id> offIdSet) {
		return [SELECT 
					IsOpportunity__c                    //案件
					, DesignDocReq__c                   //設計書依頼
					, WhatId                            //関連先
					, StartDateTime                     //開始
				FROM 
					Event
				WHERE 
					StartDateTime = THIS_MONTH          //「開始」が当月
				AND 
					RecordTypeId = :recTypeId    		//レコードタイプが「営業日報」
				AND 
					WhatId IN : offIdSet            	//関連先が、事務所リストに含まれる
				Order By 
					WhatId
				];
	}

	/**
	 * 行動リスト取得
	 * @param whatId: 関連先ID 
	 * @return List<Event>: 行動リスト
	 */
	public static List<Event> getCurrentEventsByWhatId(String whatId) {
		return  [
				SELECT 
					Id
					, StartDateTime
					, Owner.Name
					, Action__c
					, DesignDocReq__c
					, IsOpportunity__c
					, OwnerId 
					, LastModifiedBy.LastName
					, LastModifiedBy.FirstName
					, CalendarLink__c
				FROM 
					Event
				WHERE 
					WhatId = :whatId 
				AND 
				   StartDateTime >= THIS_MONTH 
				Order by 
					StartDateTime Desc
				LIMIT 
					1000
				];
	}

	/**
	 * 行動リスト取得
	 * @param whatId: 関連先ID 
	 * @return List<Event>: 行動リスト
	 */
	public static List<Event> getOldEventsByWhatId(String whatId) {
		return [
				SELECT 
					StartDateTime
					, Owner.Name
					, Action__c
					, DesignDocReq__c
					, Subject
					, IsOpportunity__c
					, OwnerId 
					, LastModifiedBy.LastName
					, LastModifiedBy.FirstName
					, CalendarLink__c
				FROM 
					Event
				WHERE 
					WhatId = :whatId 
				//昨日から1年前の行動を取得
				AND 
					StartDateTime = LAST_N_DAYS:366
				AND 
					StartDateTime != TODAY
				Order by 
					StartDateTime Desc
				LIMIT 
					1000
				];
	}
}