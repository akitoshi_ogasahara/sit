@isTest
private class TestCR_RuleSubmitExtender{	
	// コンストラクタテスト１
	private static testMethod void CRRuleSubmitTest001(){
		CR_Rule__c rule = TestCR_TestUtil.createCRRule();
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	
        	System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_Rule);
        	System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_RULE_Submit_New);
        Test.stopTest(); 
	}
	
	// コンストラクタテスト２
	private static testMethod void CRRuleSubmitTest002(){
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule = TestCR_TestUtil.createCRRule(true, agny.Id);
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	
        	System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_Rule);
        	System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_RULE_Submit_Upd);
        Test.stopTest(); 
	}	

	// コンストラクタテスト３
	private static testMethod void CRRuleSubmitTest003(){
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule = TestCR_TestUtil.createCRRule(true, agny.Id);
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
			pref.getParameters().put('parentId', '12345');
        	Test.setCurrentPage(pref);

        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	
        	System.assertEquals(target.agencyId, '12345');
        Test.stopTest(); 
	}
	
	// initテスト
	private static testMethod void CRRuleSubmitTest004(){
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule = TestCR_TestUtil.createCRRule(true, agny.Id);
        
        Test.startTest();
        	PageReference pref = Page.E_CRRuleSubmit;
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	target.init();
        	
        	System.assertEquals(target.isSuccessOfInitValidate, false);
        Test.stopTest();  
	}
	
	// initテスト2
	private static testMethod void CRRuleSubmitTest005(){
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule = TestCR_TestUtil.createCRRule(true, agny.Id);
		
        Test.startTest();
        	PageReference pref = Page.E_CRRuleSubmit;
        	pref.getParameters().put('parentId', '12345');
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	target.init();
        	
        	System.assertEquals(target.isSuccessOfInitValidate, true);
        Test.stopTest();  
	}

	// 規定一覧ページ遷移
	private static testMethod void CRRuleSubmitTest006(){
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule = TestCR_TestUtil.createCRRule(true, agny.Id);
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
			pref.getParameters().put('parentId', '12345');
        	Test.setCurrentPage(pref);

        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	PageReference result = target.backToRulesPage();
        	
        	System.assertEquals(result.getUrl(), '/apex/e_crrules?id=12345');
        Test.stopTest(); 
	}

	// 保存ボタン押下(ファイルがないためエラー)
	private static testMethod void CRRuleSubmitTest007(){
		CR_Rule__c rule = TestCR_TestUtil.createCRRule();
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
			pref.getParameters().put('parentId', '12345');
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	PageReference result = target.doSaveEx();
        	
        	System.assertEquals(result, null);
        Test.stopTest(); 
	}
	
	// 保存ボタン押下
	private static testMethod void CRRuleSubmitTest008(){
		CR_Rule__c rule = TestCR_TestUtil.createCRRule();
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule2 = TestCR_TestUtil.createCRRule(true, agny.Id);
		Attachment files = TestCR_TestUtil.createAttachment(true, rule2.Id);
		Blob body = Blob.valueOf('test.jpg');
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
			pref.getParameters().put('parentId', agny.Id);
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	target.controller.record.Type__c = '顧客サポート等管理規程';
        	Attachment src = new Attachment(Name = 'File001', Body = body);
        	target.file = src;
        	PageReference result = target.doSaveEx();
        	
        	System.assertEquals(result.getUrl(), '/apex/e_crrules?id=' + agny.Id);
        Test.stopTest(); 
	}	

	// 保存ボタン押下（エラー発生）
	private static testMethod void CRRuleSubmitTest009(){
		CR_Rule__c rule = new CR_Rule__c();
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule2 = TestCR_TestUtil.createCRRule(true, agny.Id);
		Attachment file = TestCR_TestUtil.createAttachment(true, rule2.Id);
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
			pref.getParameters().put('parentId', '12345');
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	target.file = file;
        	PageReference result = target.doSaveEx();
        	
        	System.assertEquals(result, null);
        Test.stopTest(); 
	}	
	
	// 続けてアップロードボタン押下
	private static testMethod void CRRuleSubmitTest0010(){
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule = TestCR_TestUtil.createCRRule(true, agny.Id);
		Attachment file = TestCR_TestUtil.createAttachment(true, rule.Id);
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
        	Test.setCurrentPage(pref);
        	
        	ApexPages.currentPage().getParameters().put('parentId', agny.Id);
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	PageReference result = target.doSaveAndNew();
        	
        	System.assertEquals(result.getUrl(), '/apex/e_crrulesubmit?parentId=' + agny.Id);
        Test.stopTest(); 
	}
	
	// 続けてアップロードボタン押下（エラー発生）
	private static testMethod void CRRuleSubmitTest011(){
		CR_Rule__c rule = new CR_Rule__c();
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
		CR_Rule__c rule2 = TestCR_TestUtil.createCRRule(true, agny.Id);
		Attachment file = TestCR_TestUtil.createAttachment(true, rule2.Id);
		
		Test.startTest();
			PageReference pref = Page.E_CRRuleSubmit;
			pref.getParameters().put('parentId', '12345');
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(rule);
			CR_RuleSubmitController controller = new CR_RuleSubmitController(standardcontroller);
        	CR_RuleSubmitExtender target = new CR_RuleSubmitExtender(controller);
        	target.file = file;
        	PageReference result = target.doSaveAndNew();
        	
        	System.assertEquals(result, null);
        Test.stopTest(); 
	}	
}