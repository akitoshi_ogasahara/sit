@isTest(seealldata = false)
public with sharing class TestE_ProgressSearchBankController {

    
    private static testmethod void  standardTest(){
        PageReference pref = Page.E_ProgressSearchBank;
        test.setCurrentPage(pref);
        E_ProgressSearchBankController controller  = new E_ProgressSearchBankController();      
        System.assertEquals('新契約申込進捗結果', controller.getpagetitle());
        
    }
    
    
}