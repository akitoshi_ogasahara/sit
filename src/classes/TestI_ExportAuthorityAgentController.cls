@isTest
private class TestI_ExportAuthorityAgentController {	
	/*
	**テストデータ作成
	*/
	//ID管理作成（本社）
	public static E_IDCPF__c createHonIDCPF(Id userId){
		E_IDCPF__c idHonsha = new E_IDCPF__c(
			//ユーザId
			User__c = userId
			//所有者Id
			,OwnerId = userId
			//ID種別
			,ZIDTYPE__c = 'EP'
			//パスワードステータス
			,ZSTATUS01__c = '1'
			//照会者コード
			,ZINQUIRR__c = 'BR**'
			//利用アプリ
			,AppMode__c = 'IRIS'
			);
		insert idHonsha;
		return idHonsha;
	}
	
	//取引先（格）作成
	public static Account createKakuAccount() {
		Account accParent = new Account(
			Name = 'TestKAKU'
			//代理店格コード
			, E_CL1PF_ZHEADAY__c = '12345'
			//漢字住所
			, E_COMM_ZCLADDR__c = '東京都中央区日本橋1-1'
			//代理店格カナ名
			, E_CL1PF_ZAHKNAME__c = 'テストカク'
			//有効フラグ
			, E_COMM_VALIDFLAG__c = '1'
			//支社コード
			, E_CL2PF_BRANCH__c = '01'
			);
		insert accParent;
		return accParent;
	}

	//権限セットを作成（Atria権限管理（参照のみ））
	public static void createAtriaPermissions(Id usrId) {
		Id permAgnyId = [
							Select
								Id
							FROM
								PermissionSet
							Where
								Name = 'ATR_AuthMngRead'
							].Id;
							
		PermissionSetAssignment src = new PermissionSetAssignment(
											AssigneeId = usrId
											, PermissionSetId = permAgnyId
											);
		insert src;
	}
	
	/*
	**本社勤務MRの場合
	**募集人200件作成
	*/
	
    static testMethod void honshaMRTest() {
        // 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(false,'fstest', 'ＭＲ');
		actUser.IsActive = true;
		insert actUser;
		
		//ID管理作成
		E_IDCPF__c idHonsha = createHonIDCPF(actUser.Id);

		//取引先（格）を作成
		Account accParent = createKakuAccount();

		//取引先（事務所）を作成
		List<Account> testAccList = new List<Account>();
		for(Integer i=0; i<200; i++){
			testAccList.add(new Account(
			Name = 'TJimusho'+ String.valueOf(i)
			//代理店事務所コード
			, E_CL2PF_ZAGCYNUM__c = '67890'
			//有効フラグ
			, E_COMM_VALIDFLAG__c = '1'
			//支社コード
			, E_CL2PF_BRANCH__c = '01'
			//親取引先
			, ParentId = accParent.Id
			));
		}
		
		insert testAccList;
		
		//募集人作成
		List<Contact> testConList = new List<Contact>();
		for(Account acc : testAccList){
			Contact con = new Contact();
			con.AccountId = acc.Id;
			con.LastName = 'Test'+acc.Name;
			con.E_CL3PF_AGNTNUM__c = 'T'+acc.Name;
			con.E_CL3PF_VALIDFLAG__c = '1';
			con.Email = 'test@test.test';
			con.E_CL3PF_AGTYPE__c = 'TS';			
			testConList.add(con);
		}
		insert testConList;
		
		// ページ表示
		PageReference pr = Page.IRIS_AuthorityAgent;
		//パラメータrowに繰り返し行数を付与
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			// 権限セット(Atria権限管理（参照のみ）)を付与
			createAtriaPermissions(actUser.Id);
//==================================テスト開始==================================
			Test.startTest();
				I_AuthorityAgentController acon = new I_AuthorityAgentController();
				Apexpages.currentPage().getParameters().put('accid', accParent.Id);
				acon.pageAction();
				acon.sortRows();
				
				//Logレコード作成
				E_Log__c elog = E_LogUtil.createLog();
			 	//必要な情報をjsonにする
			 	JSONGenerator gen = JSON.createGenerator(true);
			 	gen.writeStartobject();
			 	//検索ワード
			 	gen.writeStringField('SearchText','');
			 	//全件検索のフラグ
			 	gen.writeBooleanField('isAllSearch' , true);
			 	//代理店格Id
			 	gen.writeStringField('accId', accParent.Id);
			 	//代理店格Id
			 	gen.writeStringField('branchCode','');
			 	//選択された事務所の一覧
			 	gen.writeFieldName('SelectOffice');
			 	gen.writeStartArray();
//			 	for( OfficeListRow row :officeRows ){
//			 		if( row.isSelect ){
//			 			gen.writeString( row.Id );
//			 		}
//			 	}
			 	gen.writeEndArray();
			 	//ソート項目
			 	gen.writeStringField('SortType' , 'agentName' );
			 	//ソート方向
			 	gen.writeBooleanField( 'SortIsAsc' , true );
		
			 	gen.writeEndObject();
		
			 	elog.detail__c = gen.getAsString();
			 	//遷移先のページを設定
			 	elog.AccessPage__c =  String.valueOf ( Page.IRIS_ExportAuthorityAgent );
			 	elog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_DOWNLOAD;
			 	insert elog;
			 	
			 	PageReference csvPr = Page.IRIS_ExportAuthorityAgent;
			 	csvPr.getParameters().put('id' , elog.Id);
			 	Test.setCurrentPage(csvPr);
			 	
			 	I_ExportAuthorityAgentController eacon = new I_ExportAuthorityAgentController();
			 	eacon.mainSobjectAPIName();
			 	eacon.getFileName();
			 	eacon.getCSVHeader();
			 	eacon.getLines();
//==================================テスト終了==================================
			Test.stopTest();
//==================================テスト判定==================================
			System.assertEquals(eacon.getLines().size() == 200, true);
		}        
    }
    
}