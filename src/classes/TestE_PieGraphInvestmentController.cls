@isTest
private class TestE_PieGraphInvestmentController {
	
	//グラフ作成
	//買付時の資産配分比率
	static testMethod void createGraphTest001() {
		
		//テストデータ作成
		List<E_ITFPF__c> testDataList = createTestData();
		
		//コントローラ設定
		E_PieGraphInvestmentController controller = new E_PieGraphInvestmentController();
		controller.ithpfId = testDataList[0].E_ITHPF__c;
		controller.executeKind = E_Const.GRAPH_INVESTMENT_BUY;
		
		Test.startTest();
		List<E_AbstractPieGraph.GraphData> resultGraphData = controller.getDataList();
		List<E_AbstractPieGraph.GraphDataTable> resultGraphDataTable = controller.getDataTableList();
		Test.stopTest();
		
		System.assertEquals('買付時の資産配分比率', controller.ratioLabel);
		System.assertEquals(3, resultGraphData.size());
		System.assertEquals('アセットクラス01', resultGraphData[0].name);
		System.assertEquals(1.1, resultGraphData[0].data);
		System.assertEquals('アセットクラス02', resultGraphData[1].name);
		System.assertEquals(5.5, resultGraphData[1].data);
		System.assertEquals('アセットクラス03', resultGraphData[2].name);
		System.assertEquals(0.0, resultGraphData[2].data);
		
		System.assertEquals(4, resultGraphDataTable.size());
		System.assertEquals('#FF0000', resultGraphDataTable[0].color);
		System.assertEquals('アセットクラス01', resultGraphDataTable[0].name);
		System.assertEquals('0', resultGraphDataTable[0].amount);
		System.assertEquals(1.1, resultGraphDataTable[0].ratio);
		System.assertEquals('#00FF00', resultGraphDataTable[1].color);
		System.assertEquals('アセットクラス02', resultGraphDataTable[1].name);
		System.assertEquals('0', resultGraphDataTable[1].amount);
		System.assertEquals(5.5, resultGraphDataTable[1].ratio);
		System.assertEquals('#0000FF', resultGraphDataTable[2].color);
		System.assertEquals('アセットクラス03', resultGraphDataTable[2].name);
		System.assertEquals('0', resultGraphDataTable[2].amount);
		System.assertEquals(0.0, resultGraphDataTable[2].ratio);
		System.assertEquals('#FFFFFF', resultGraphDataTable[3].color);
		System.assertEquals('合計', resultGraphDataTable[3].name);
		System.assertEquals('0', resultGraphDataTable[3].amount);
		System.assertEquals(6.6, resultGraphDataTable[3].ratio);
	}
	
	//グラフ作成
	//現在の資産配分比率
	static testMethod void createGraphTest002() {
		
		//テストデータ作成
		List<E_ITFPF__c> testDataList = createTestData();
		
		//コントローラ設定
		E_PieGraphInvestmentController controller = new E_PieGraphInvestmentController();
		controller.ithpfId = testDataList[0].E_ITHPF__c;
		controller.executeKind = E_Const.GRAPH_INVESTMENT_NOW;
		
		Test.startTest();
		List<E_AbstractPieGraph.GraphDataTable> resultGraphDataTable = controller.getDataTableList();
		List<E_AbstractPieGraph.GraphData> resultGraphData = controller.getDataList();
		Test.stopTest();
		
		System.assertEquals('現在の資産配分比率', controller.ratioLabel);
		System.assertEquals(3, resultGraphData.size());
		System.assertEquals('アセットクラス01', resultGraphData[0].name);
		System.assertEquals(11.1, resultGraphData[0].data);
		System.assertEquals('アセットクラス02', resultGraphData[1].name);
		System.assertEquals(55.5, resultGraphData[1].data);
		System.assertEquals('アセットクラス03', resultGraphData[2].name);
		System.assertEquals(0.0, resultGraphData[2].data);
		
		System.assertEquals(4, resultGraphDataTable.size());
		System.assertEquals('#FF0000', resultGraphDataTable[0].color);
		System.assertEquals('アセットクラス01', resultGraphDataTable[0].name);
		System.assertEquals('0', resultGraphDataTable[0].amount);
		System.assertEquals(11.1, resultGraphDataTable[0].ratio);
		System.assertEquals('#00FF00', resultGraphDataTable[1].color);
		System.assertEquals('アセットクラス02', resultGraphDataTable[1].name);
		System.assertEquals('0', resultGraphDataTable[1].amount);
		System.assertEquals(55.5, resultGraphDataTable[1].ratio);
		System.assertEquals('#0000FF', resultGraphDataTable[2].color);
		System.assertEquals('アセットクラス03', resultGraphDataTable[2].name);
		System.assertEquals('0', resultGraphDataTable[2].amount);
		System.assertEquals(0.0, resultGraphDataTable[2].ratio);
		System.assertEquals('#FFFFFF', resultGraphDataTable[3].color);
		System.assertEquals('合計', resultGraphDataTable[3].name);
		System.assertEquals('0', resultGraphDataTable[3].amount);
		System.assertEquals(66.6, resultGraphDataTable[3].ratio);
	}
	
	//グラフ色取得
	static testMethod void getColorSetTest001() {
		
		//テストデータ作成
		List<E_ITFPF__c> testDataList = createTestData();
		
		//コントローラ設定
		E_PieGraphInvestmentController controller = new E_PieGraphInvestmentController();
		controller.ithpfId = testDataList[0].E_ITHPF__c;
		controller.executeKind = E_Const.GRAPH_INVESTMENT_NOW;
		
		Test.startTest();
		String result = controller.getColorSet();
		Test.stopTest();
		
		System.assertEquals('#FF0000,#00FF00,#0000FF', result);
	}
	
	//テストデータ作成
	static List<E_ITFPF__c> createTestData(){
		
		E_ITHPF__c ithpf = new E_ITHPF__c();
		insert ithpf;
		
		List<E_ITAPF__c> itapfList = createITAPFList();
		
		List<E_ITFPF__c> itfpfList = new List<E_ITFPF__c>();
		itfpfList.add(createITFPF(ithpf, itapfList[0], 1.1, 11.1));
		itfpfList.add(createITFPF(ithpf, itapfList[1], 2.2, 22.2));
		itfpfList.add(createITFPF(ithpf, itapfList[1], 3.3, 33.3));
		itfpfList.add(createITFPF(ithpf, itapfList[3], 4.4, 44.4));
		
		insert itfpfList;
		return itfpfList;
	}
	
	//投信ファンド作成
	static E_ITFPF__c createITFPF(E_ITHPF__c ithpf, E_ITAPF__c itapf, Decimal zbrndper02, Decimal zbrndper04){
		E_ITFPF__c record = new E_ITFPF__c();
		record.E_ITHPF__c = ithpf.Id;
		record.E_ITAPF__c = itapf.Id;
		record.ZBRNDPER02__c = zbrndper02;
		record.ZBRNDPER04__c = zbrndper04;
		return record;
	}
	
	//アセットクラス作成
	static List<E_ITAPF__c> createITAPFList(){
		List<E_ITAPF__c> recordList = new List<E_ITAPF__c>();
		
		recordList.add(createITAPF('11', 'アセットクラス01', '1', '#FF0000'));
		recordList.add(createITAPF('22', 'アセットクラス02', '1', '#00FF00'));
		recordList.add(createITAPF('33', 'アセットクラス03', '1', '#0000FF'));
		recordList.add(createITAPF('44', 'アセットクラス04', '0', '#000000'));
		
		insert recordList;
		return recordList;
	}
	static E_ITAPF__c createITAPF(String zasstcls, String zasstnam, String flag, String zcolorcd){
		E_ITAPF__c record = new E_ITAPF__c();
		record.ZASSTCLS__c = zasstcls;
		record.ZASSTNAM__c = zasstnam;
		record.FLAG__c = flag;
		record.ZCOLORCD__c = zcolorcd;
		return record;
	}
	
}