@isTest
private class TestE_DiseasesDao{

	/**
	 * getDiseaseRecsメソッドテスト
	 * 引数が空文字の場合
	 */
	static testMethod void getDiseaseRecs_test01(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');

		Test.startTest();
		List<E_Diseases__c> result = E_DiseasesDao.getDiseaseRecs('');
		Test.stopTest();

		System.assertEquals(diseaseList.size(), result.size());
	}

	/**
	 * getDiseaseRecsメソッドテスト
	 * 引数にwhere句がある場合
	 */
	static testMethod void getDiseaseRecs_test02(){
		// テストデータ作成
		List<E_DiseaseNumber__c> diseaseNumberList = TestDS_TestUtil.createDiseaseNumberData(5);
		List<E_Diseases__c> diseaseList = TestDS_TestUtil.createDiseaseData(diseaseNumberList, 'normal');

		Test.startTest();
		List<E_Diseases__c> result = E_DiseasesDao.getDiseaseRecs('Where Name =\'アあの傷病\'');
		Test.stopTest();

		System.assertEquals(1, result.size());
		System.assertEquals(diseaseList[0].Name, result[0].Name);
		System.assertEquals(diseaseList[0].Hiragana__c, result[0].Hiragana__c);
		System.assertEquals(diseaseList[0].Katakana__c, result[0].Katakana__c);
	}
}