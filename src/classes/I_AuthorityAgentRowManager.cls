/*
 * 権限管理用募集人一覧：行作成用クラス
*/

public with sharing class I_AuthorityAgentRowManager{
  public E_AccessController access;

	// 募集人一覧
	transient private List<AgentListRow> agentRows;

	//csvフラグ
	private Boolean IsCsvFlag;

	//表示行作成用変数
	// ContactIDとUserレコードのMap
	transient private Map<ID, User> userMap;
	// ContactIDとUserLoginレコードのMap
	transient private Map<ID, UserLogin> userLoginMap;
	// UserIDとE_IDRequest__cレコードのMap
	transient private Map<ID, E_IDRequest__c> idRequesutMap;
	//コンタクトのリスト
	transient private List<Contact> contactList;

	transient private Set<ID> userIdSet;

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// 募集人一覧：ソート実行用：項目
	public static String agentList_sortType;
	// 募集人一覧：ソート実行用：昇順 OR 降順
	public static Boolean agentList_sortIsAsc;

	// ソートキー
	//募集人一覧・変更確定モーダル用ソートキー
	public static final String SORT_TYPE_AGENT	= 'agentName';
	public static final String SORT_TYPE_AGENTCODE	= 'agentCode';
	public static final String SORT_TYPE_IDSTATUS		= 'idStatus';
	public static final String SORT_TYPE_AGTYPE		= 'agtype';
	public static final String SORT_TYPE_USERNAME		= 'userName';
	public static final String SORT_TYPE_EMAIL		= 'email';
	public static final String SORT_TYPE_OFFICE		= 'office';
	public static final String SORT_TYPE_COMMISSION	= 'commission';
	// [IRIS-734] start
	public static final String SORT_TYPE_ZDSPFLAG01 = 'zdspFlag01';					// ID管理.手数料明細書照会フラグ
	public static final String SORT_TYPE_ISNOTNOTIFICATION = 'isNotNotification';	// ID管理.通知対象外
	public static final String SORT_TYPE_USERLASTLGIN = 'userLastLogin';			// User.最終ログイン日時
	// [IRIS-734] end

	//csv出力用
	public static final String CSV_SEPARATOR = ',';
	public static final String CSV_BREAKELINE = '\n';

	//ログイン日数用
	public static Integer criteriaDays = 1000;

	//管理責任者用
	private static final String OPERATIONS_MANAGER = '業管';


	/**
	 * 募集人一覧(表示用)作成管理クラス
	 */
	public List<AgentListRow> createAgentRows( Boolean isAllsearch ,String accId , String branchCode, List<Id> officeIdList, String searchText, Boolean isCsvFlag ){

		contactList  = new List<Contact>();
		this.isCsvFlag = isCsvFlag;

		//全件表示中/絞り込み中でクエリを変える
		if( isAllSearch ){
			contactList = E_ContactDaoWithout.getRecsByAgencyId( accId , branchCode, searchText, isCsvFlag );
		} else {
			contactList = E_ContactDaoWithout.getRecsByOfficeIds(officeIdList, searchText, isCsvFlag );

		}
		//criteriaDays取得
		try{
			criteriaDays = Integer.valueOf(System.Label.E_LASTLOGINDATE_LAPSEDDAYS);
		}catch(Exception e){
			//デフォルト1000日にしておく
		}

		// ContactIDとUserのMap
		userMap = new Map<ID, User>();

		userIdSet = new Set<ID>();

		// ToDo Setを作るならMap作るので良い？
		for (User us: getUserList()) {
			userMap.put(us.Contact.Id, us);
			userIdSet.add(us.Id);
		}

		// UserLogin取得
		userLoginMap = new Map<ID, UserLogin>();
		getUserLoginList();

		// E_IDRequest__c取得
		idRequesutMap = new Map<ID, E_IDRequest__c>();
		getIDRequestMap();

		// 募集人一覧(表示用)取得
		getAgentList();


		return AgentRows;
	}

	/**
	 * 募集人一覧(表示用)作成
	 */
	private void getAgentList() {

		// 画面に表示するリスト
		agentRows = new List<AgentListRow>();

		//募集人情報マップ生成	キー:'|'なしの募集人コード	値:募集人コードに紐づく募集人レコードリスト
		Map<String, List<AgentListRow>> agentMap = new Map<String, List<AgentListRow>>();
		//マップで使用するkey('|'なしの募集人コード)
		String agentKey='';
		//'|'なしの募集人コードに紐づくレコードリスト
		List<AgentListRow> rowsList;


		//システム管理者フラグ
		access =  E_AccessController.getInstance();
		Boolean isAdmin = access.isSystemAdministrator();
		//Atria試算権限編集可能フラグ
		Boolean isAuthUpd = access.hasATRAuthMngUpd();
		//募集人レコード追加フラグ trueの場合そのレコードを画面表示するフラグ
		Boolean addContactFlg;

		Map<String,String> AGTYPE_MAP = new Map<String,String>();
		{
			AGTYPE_MAP.put('AH','代理店権限');
			AGTYPE_MAP.put('AY','事務所権限');
			AGTYPE_MAP.put('AT','募集人権限');
		}

		for (Contact con : contactList) {
			//'|'より前の募集人コードをキーとする(5桁を想定する)
			agentKey = con.E_CL3PF_AGNTNUM__c.substringBefore('|');
			//募集人レコードからagentListRowの新規作成
			//画面に表示する募集人レコード
			AgentListRow row = new AgentListRow();
			row.id = con.Id;
			row.name = con.Name;
			row.code = agentKey;
			row.nameSort = con.E_CL3PF_ZEATKNAM__c;	//募集人カナ氏名
			row.office = con.E_Account__c; //事務所名

			//募集人レコードの募集人コードが'|'なしの場合
			row.notPipeFlg = agentKey.equals(con.E_CL3PF_AGNTNUM__c);

			User agentUser = userMap.get(con.Id);

			//NNLinkユーザIDがnullでない場合
			if(userMap.containsKey(con.Id) ){
				if(agentUser.E_ZWEBID__c != null){
					//ユーザ名がNNLinkユーザIDで始まる場合
					if(agentUser.userName.startsWith(agentUser.E_ZWEBID__c)){
						row.userName = agentUser.E_ZWEBID__c;
					} else {
						// [IRIS-734 > IRIS-741] start
						// row.userName = agentUser.userName;
						row.userName = replaceSuffix(agentUser.userName, agentUser.Email);
						// [IRIS-734 > IRIS-741] end
					}
					//メールアドレスがNNLinkユーザIDで始まる場合は、メールアドレスはブランクにする
					if(agentUser.Email.startsWith(agentUser.E_ZWEBID__c)){
						row.email = '－';
					} else {
						row.email = agentUser.Email;
					}
				//NNLinkユーザIDがnullの場合
				} else {
					// [IRIS-734 > IRIS-741] start
					//row.userName = agentUser.userName;
					row.userName = replaceSuffix(agentUser.userName, agentUser.Email);
					// [IRIS-734 > IRIS-741] end
					row.email = agentUser.Email;
				}
			}

			addContactFlg = false;

			/** 長期未使用の場合 */
			if (userMap.containsKey(con.Id) //募集人レコードに紐づいている
				//ここでuserIDを定義してagentUser
					&& (!agentUser.IsActive //非アクティブ（無効）
						&& (idRequesutMap.containskey(agentUser.Id) && //EIDRレコードを持っている
							idRequesutMap.get(agentUser.Id).ZUPDFLAG__c.equals(E_Const.ZUPDFLAG_D)) //リクエスト種別がD
						)
			) {

				row.status = I_Const.NNLINKID_STATUS_UNUSED;		//ID状況：長期未使用

				row.E_IDCPFId = agentUser.E_IDCPFs__r[0].Id;		//ID管理のID
				//ID種別
				if(con.E_CL3PF_ZAGMANGR__c == 'Y'){
					row.agtype = OPERATIONS_MANAGER +'('+ AGTYPE_MAP.get(agentUser.E_IDCPFs__r[0].ZIDTYPE__c) + ')';
				}else{
					row.agtype = AGTYPE_MAP.get(agentUser.E_IDCPFs__r[0].ZIDTYPE__c);
				}

				//row.agtype = agentUser.E_IDCPFs__r[0].ZIDTYPE__c;	//ID種別
				row.commissionTxt = (agentUser.E_IDCPFs__r[0].AtriaCommissionAuthorityEditable__c) ? '有':'無';
				if ( !isCsvFlag  && ( ( isAuthUpd && con.E_CL3PF_ZAGMANGR__c != 'Y' ) || isAdmin ) ){
					//CSV出力でない かつ（(更新権限あり かつ 表示対象の募集人が業務管理責任者でない)  または システム管理者 )の場合、変更ボタンを表示
					row.commissionSort = (agentUser.E_IDCPFs__r[0].AtriaCommissionAuthorityEditable__c) ? '1':'3';

				} else {
					//その他の場合は文言を設定し更新不可にする
					row.commissionSort = (agentUser.E_IDCPFs__r[0].AtriaCommissionAuthorityEditable__c) ? '2':'4';
				}

				// [IRIS-734 > IRIS-794] start
				row.email = '－';
				row.zdspFlag01 = getZdspflag01(agentUser.E_IDCPFs__r[0]);				// ID管理.手数料明細書照会フラグ
				row.isNotNotification = getIsNotNotification(agentUser.E_IDCPFs__r[0]);	// ID管理.通知対象外
				row.userLastLogin = getLastLoginDate(agentUser.LastLoginDate);			// User.最終ログイン日時
				// [IRIS-734 > IRIS-794] end

			/** 有効ユーザが紐づいている,ID管理.パスワードステータス='1'の場合	 */
			} else if (userMap.containskey(con.Id) && agentUser.IsActive
						&& !agentUser.E_IDCPFs__r.isEmpty()
                        && agentUser.E_IDCPFs__r[0].ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)) {

				if(isPassedDays(agentUser )){//90日未ログイン
					row.status = I_Const.NNLINKID_STATUS_PWCHANGE;				//ID状況：要パスワード変更
				} else if(userLoginMap.containsKey(agentUser.Id)
						&& userLoginMap.get(agentUser.Id).IsPasswordLocked ){ 	//PWロックされている
					row.status = I_Const.NNLINKID_STATUS_PWLOCK;				//ID状況：パスワードロック
				} else {
					row.status = I_Const.NNLINKID_STATUS_ACTIVE;				//ID状況：有効

				}
				//ユーザ名とメールアドレスは常に設定
				row.E_IDCPFId = agentUser.E_IDCPFs__r[0].Id;			//ID管理のID

				//ID種別
				if(con.E_CL3PF_ZAGMANGR__c == 'Y'){
					row.agtype = OPERATIONS_MANAGER +'('+ AGTYPE_MAP.get(agentUser.E_IDCPFs__r[0].ZIDTYPE__c) + ')';
				}else{
					row.agtype = AGTYPE_MAP.get(agentUser.E_IDCPFs__r[0].ZIDTYPE__c);
				}

				row.commissionTxt = (agentUser.E_IDCPFs__r[0].AtriaCommissionAuthorityEditable__c) ? '有':'無';
				if ( !isCsvFlag  && ( ( isAuthUpd && con.E_CL3PF_ZAGMANGR__c != 'Y' ) || isAdmin ) ){
					//CSV出力でない かつ（(更新権限あり かつ 表示対象の募集人が業務管理責任者でない)  または システム管理者 )の場合、変更ボタンを表示
					row.commissionSort = (agentUser.E_IDCPFs__r[0].AtriaCommissionAuthorityEditable__c) ? '1':'3';

				} else {
					//その他の場合は文言を設定し更新不可にする
					row.commissionSort = (agentUser.E_IDCPFs__r[0].AtriaCommissionAuthorityEditable__c) ? '2':'4';
				}

				// [IRIS-734] start
				row.zdspFlag01 = getZdspflag01(agentUser.E_IDCPFs__r[0]);				// ID管理.手数料明細書照会フラグ
				row.isNotNotification = getIsNotNotification(agentUser.E_IDCPFs__r[0]);	// ID管理.通知対象外
				row.userLastLogin = getLastLoginDate(agentUser.LastLoginDate);			// User.最終ログイン日時
				// [IRIS-734] end

			/** 上記に当てはまらない場合*/
			} else {
				row.status = I_Const.NNLINKID_STATUS_NOPOLICY;			//保有なし
				row.userName = '－';
				row.email = '－';
				row.agtype = '－';
				row.commissionTxt = '－'; // 保有していない場合には変更ボタンを表示させない制御をかける
				row.commissionSort = '5';
				// [IRIS-734] start
				row.zdspFlag01 = '－';			// ID管理.手数料明細書照会フラグ
				row.isNotNotification = '－';	// ID管理.通知対象外
				row.userLastLogin = '－';		// User.最終ログイン日時
				// [IRIS-734] end
			}

			//AgentListRowリストの新規
			rowsList = new List<AgentListRow>();
			//上記で設定したagentKeyでとってこられるListがある場合--Listへの更新・追加
			if(agentMap.containsKey(agentKey)){
				//ここでrowsListを定義すればいいのでは？
				rowsList = agentMap.get(agentKey);

				for(AgentListRow grouprow : rowsList){
					if(row.notPipeFlg){										//募集人レコードの募集人コードが'|'なしの場合
						//グループのContactIdに、'|'なしの募集人コードを持つContactIdを設定する →　契約者一覧への遷移用のID
						grouprow.id = row.id;

					} else if(grouprow.notPipeFlg && !row.notPipeFlg) {		//グループに'|'なしの募集人コードを持つレコードがある場合
						//募集人レコードのContactIdに、グループのContactIdを設定する
						row.id = grouprow.id;
					}

					//グループ内に有効ユーザレコードがない場合
					if( (!grouprow.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) && //有効
						!grouprow.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) && //要パスワード変更
						!grouprow.status.equals(I_Const.NNLINKID_STATUS_PWLOCK)) && //パスワードロック
						(row.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) || //現在行が有効
						 row.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) || //現在行が要パスワード変更
						 row.status.equals(I_Const.NNLINKID_STATUS_PWLOCK))//現在行がパスワードロック
						){
						//グループの募集人名・ステータスを募集人レコードに更新
						grouprow.name = row.name;
						grouprow.nameSort = row.nameSort;
						grouprow.status = row.status;
						grouprow.email = row.email;
						grouprow.userName = row.userName;
						grouprow.agtype = row.agtype;
						grouprow.commissionTxt = row.commissionTxt;
						grouprow.commissionSort = row.commissionSort;
						// [IRIS-734] start
						grouprow.zdspFlag01 = row.zdspFlag01;				// ID管理.手数料明細書照会フラグ
						grouprow.isNotNotification = row.isNotNotification;	// ID管理.通知対象外
						grouprow.userLastLogin = row.userLastLogin;			// User.最終ログイン日時
						// [IRIS-734] end
						grouprow.E_IDCPFId = row.E_IDCPFId;


					//グループ内に有効ユーザレコードがある場合 かつ 現在行の募集人レコードが有効
					} else if( (grouprow.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) || //有効
								grouprow.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) || //要パスワード変更
								grouprow.status.equals(I_Const.NNLINKID_STATUS_PWLOCK)) && //パスワードロック
								 (row.status.equals(I_Const.NNLINKID_STATUS_ACTIVE) || //現在行が有効
									 row.status.equals(I_Const.NNLINKID_STATUS_PWCHANGE) || //現在行が要パスワード変更
									 row.status.equals(I_Const.NNLINKID_STATUS_PWLOCK))//現在行がパスワードロック
							){
							//募集人レコードをAgentListRowリストに追加
							addContactFlg = true;
					}
				}
			//ない場合--Listの追加('|'なしの募集人コードが重複している募集人でも、一度はこちらを通る)
			} else {
				//AgentListRowリストの新規追加,リストにAgentListRow追加
				addContactFlg = true;
			}


			if(addContactFlg){
				//リストにAgentListRow追加
				rowsList.add(row);
				//csv出力用処理
				if(isCsvFlag){
					//getCSVTextを呼ぶ
					row.getCSVText();
				}
			}
			//マップのAgentListRowリスト追加/更新
			agentMap.put(agentKey, rowsList);

			//1001件以上をvisualforceに渡すとエラーになるためbreak
			if(agentMap.size() == I_Const.LIST_MAX_ROWS && !isCsvFlag ){
				break;
			} else if(agentMap.size() == I_Const.LIST_DL_MAX_ROWS ) {
				break; //csv時の上限は3000件とする
			}
		}

		if(agentMap != null && !agentMap.isEmpty()){
			for(List<AgentListRow> rowList : agentMap.values()){
				//表示用リストにマップのAgentListRowリストを入力
				agentRows.addAll(rowList);
			}
		}
		agentRows.sort();

	}

	/**
	 * [IRIS-734 > IRIS-741]
	 * IDからユーザSuffixを削除
	 */
	private String replaceSuffix(String userName, String email){
		if(String.isNotBlank(userName)
				&& userName.contains(email)){
			userName = userName.replace(System.Label.E_USERNAME_SUFFIX, '');
		}
		return userName;
	}

	/**
	 * [IRIS-734]
	 * 表示用手数料権限の取得（ID管理.手数料明細書照会フラグ）
	 */
	private String getZdspflag01(E_IDCPF__c idcpf){
		return idcpf.ZDSPFLAG01__c == '1' ? '有':'無';
	}

	/**
	 * [IRIS-734]
	 * 表示用メール配信の取得（ID管理.通知対象外）
	 */
	private String getIsNotNotification(E_IDCPF__c idcpf){
		return idcpf.IsNotNotification__c ? '停止':'受信';
	}
	
	/**
	 * [IRIS-734]
	 * 表示用最終ログイン日時の取得（ユーザ.最終ログイン日時）
	 */
	private String getLastLoginDate(Datetime dt){
		return dt == null ? '－' : dt.format('yyyy/MM/dd HH:mm', 'JST');
	}

	/**
	 * N日未ログインチェックのメソッド(90日を想定)
	 * @param userRec ユーザレコード
	 * @return Boolean true:N日以上経っている、false:経っていない
	 */
	private Boolean isPassedDays(User userRec){
		//Integer criteriaDays = 1000;
		//try{
		//	criteriaDays = Integer.valueOf(System.Label.E_LASTLOGINDATE_LAPSEDDAYS);
		//}catch(Exception e){
		//	//デフォルト1000日にしておく
		//}

		//クライテリアの日数以上の時にTrue
		return E_Util.days_after_lastLogin(userRec) > criteriaDays;
	}

	/**
	 * ソート
	 */
	 //募集人一覧用ソート
	public static List<AgentListRow> sortRows( List<AgentListRow> agentRows) {

		agentRows.sort();

		return agentRows;
	}

	/**
	 * 内部クラスで表示用募集人リストを作成
	 */
	public class AgentListRow implements Comparable {
		public String id {get; set;}
		public String name {get; private set;}
		public String code {get; private set;}
		public String status {get; private set;}
		public String userName {get; private set;}
		public String email {get; private set;}
		public String office {get; private set;}
		public String commissionTxt{get; set;}
		public String E_IDCPFId {get; private set;}		//紐づいているId管理のIdE_IDCPFId
		public String agtype {get; private set;}
		public String nameSort{get; private set;}		//カナソート用変数
		public String commissionSort {get; set;} //ソート用文言 1:有(変更ボタン) 2:有(テキスト) 3:無(変更ボタン) 4:無(テキスト) 5:「-」
		// [IRIS-734] start
		public String zdspFlag01 {get; set;}		// ID管理.手数料明細書照会フラグ
		public String isNotNotification {get; set;}	// ID管理.通知対象外
		public String userLastLogin {get; set;}		// User.最終ログイン日時
		// [IRIS-734] end
		// ToDo クラス内の値と固定値のみで作れるから変数にしない
		//csv用区切り文字

		public String getCSVText(){
			List<String> ln = new List<String>();
				// [IRIS-734] start
				/*
				ln.add(E_Util.getEscapedCSVString( this.name )); //募集人名
				ln.add(E_Util.getEscapedCSVString( this.code )); //募集人コード
				ln.add(E_Util.getEscapedCSVString( this.status )); //ID状況
				ln.add(E_Util.getEscapedCSVString( this.agtype )); //権限種別
				ln.add(E_Util.getEscapedCSVString( this.userName )); //ユーザID
				ln.add(E_Util.getEscapedCSVString( this.email )); //メールアドレス
				ln.add(E_Util.getEscapedCSVString( this.office )); //事務所名
				ln.add(E_Util.getEscapedCSVString( this.commissionTxt ));//Atria手数料試算
				*/
				ln.add(E_Util.getEscapedCSVString( this.name )); 				//募集人名
				ln.add(E_Util.getEscapedCSVString( this.code )); 				//募集人コード
				ln.add(E_Util.getEscapedCSVString( this.status )); 				//ID状況
				ln.add(E_Util.getEscapedCSVString( this.userName )); 			//ユーザID
				ln.add(E_Util.getEscapedCSVString( this.email )); 				//メールアドレス
				ln.add(E_Util.getEscapedCSVString( this.isNotNotification )); 	//メール配信
				ln.add(E_Util.getEscapedCSVString( this.agtype )); 				//権限種別
				ln.add(E_Util.getEscapedCSVString( this.zdspFlag01 ));			//手数料参照
				ln.add(E_Util.getEscapedCSVString( this.commissionTxt ));		//Atria手数料試算
				ln.add(E_Util.getEscapedCSVString( this.userLastLogin ));		//最終ログイン日時
				ln.add(E_Util.getEscapedCSVString( this.office )); 				//事務所名
				// [IRIS-734] end

				return String.join(ln, CSV_SEPARATOR)+ CSV_BREAKELINE;
		}

		// '|'なしの募集人コードが設定されているレコードフラグ
		public Boolean notPipeFlg {get; private set;}


		// コンストラクタ
		public AgentListRow() {
			id = '';
			name = '';
			code = '';
			status = '';
			userName = '';
			email = '';
			office = '';
			agtype = '';
			commissionTxt = '';
			E_IDCPFId = '';
			nameSort = '';// カナソート用変数
			commissionSort = '';
			// [IRIS-734] start
			zdspFlag01 = '';
			isNotNotification = '';
			userLastLogin = '';
			// [IRIS-734] end

			// '|'なしの募集人コードが設定されているレコードフラグ
			notPipeFlg = false;

		}

		// ソート
		public Integer compareTo(Object compareTo) {
			AgentListRow compareToRow = (AgentListRow)compareTo;
			Integer result = 0;
			// 募集人名
			if (agentList_sortType == SORT_TYPE_AGENT) {
				// カナソート
				result = I_Util.compareToString(nameSort, compareToRow.nameSort);
			// 募集人コード
			} else if(agentList_sortType == SORT_TYPE_AGENTCODE){
				result = I_Util.compareToString(code, compareToRow.code);
			// ID状況
			} else if(agentList_sortType == SORT_TYPE_IDSTATUS){
				result = I_Util.compareToString(status, compareToRow.status);
			//権限種別
			} else if(agentList_sortType == SORT_TYPE_AGTYPE){
				result = I_Util.compareToString(agtype, compareToRow.agtype);
			//ID
			} else if(agentList_sortType == SORT_TYPE_USERNAME){
				result = I_Util.compareToString(userName, compareToRow.userName);
			//メールアドレス
			} else if(agentList_sortType == SORT_TYPE_EMAIL){
				result = I_Util.compareToString(email, compareToRow.email);
			//事務所名
			} else if(agentList_sortType == SORT_TYPE_OFFICE){
				result = I_Util.compareToString(office, compareToRow.office);
			//Atria手数料
			} else if(agentList_sortType == SORT_TYPE_COMMISSION){
				result = I_Util.compareToString(commissionSort, compareToRow.commissionSort);
			// [IRIS-734] start
			// ID管理.手数料明細書照会フラグ
			} else if(agentList_sortType == SORT_TYPE_ZDSPFLAG01){
				result = I_Util.compareToString(zdspFlag01, compareToRow.zdspFlag01);
			// 	ID管理.通知対象外
			} else if(agentList_sortType == SORT_TYPE_ISNOTNOTIFICATION){
				result = I_Util.compareToString(isNotNotification, compareToRow.isNotNotification);
			// User.最終ログイン日時
			} else if(agentList_sortType == SORT_TYPE_USERLASTLGIN){
				result = I_Util.compareToString(userLastLogin, compareToRow.userLastLogin);
			}
			// [IRIS-734] end
			return agentList_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * ユーザリスト - User
	 */
	private List<User> getUserList() {
		// 募集人IDのSet
		Set<ID> contactIdSet = new Set<ID>();
		for (Contact con: contactList) {
			contactIdSet.add(con.Id);
		}
		return E_UserDaoWithout.getRecsAgentIdWithEIDC( ContactIdSet );

	}

	/**
	 * ユーザログインのMap - UserLogin
	 */

	private void getUserLoginList(){
		for(UserLogin login : E_UserLoginDaoWithout.getRecsWithIsLockByUserIds(userIdSet)){
		//for(UserLogin login : E_UserLoginDaoWithout.getRecsWithIsLockByUserIds(userMap.keySet())){
			userLoginMap.put(login.UserId, login);
		}

	}


	/**
	 * IDリクエストのMap - E_IDRequest__c
	 */
	private void getIDRequestMap() {
		for (E_IDRequest__c idr: E_IDRequestDaoWithout.getRecsByAgentUserId(userIdSet)) {
		//for (E_IDRequest__c idr: E_IDRequestDaoWithout.getRecsByAgentUserId(userMap.keySet())) {
			if (!idRequesutMap.containsKey(idr.E_IDCPF__r.User__r.Id)) {
				idRequesutMap.put(idr.E_IDCPF__r.User__r.Id, idr);
			}
		}
	}

}