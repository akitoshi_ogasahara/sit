public with sharing class E_IssReferenceController {

	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}
	
	public PageReference init() {
		// 証券番号
		String ISS_SHOUKENNO = ApexPages.currentPage().getParameters().get('RENKEI_SHOUBAN');
		
		//抽象クラスのクッキー設定メソッド
		if(E_Util.getIsIssParam()){
			E_CookieHandler.setCookieIssSessionId();
		}
		
		if (String.isEmpty(ISS_SHOUKENNO)) {
			PageReference pageRef = Page.E_ErrorPage;
			pageRef.getParameters().put('code', 'ISS|001');
			pageRef.getParameters().put('iss', '1');
			return pageRef;
		}
		
		// 保険契約ヘッダを取得
		E_Policy__c policy = E_PolicyDao.getRecByIdCHDRNUM(ISS_SHOUKENNO);
		
		if (policy != null) {
			//SPVAヘッダDB「年金支払開始日到来フラグ(ZANNSTFLG)」＝”１”（年金支払開始日以降）の時、
			//SPVAヘッダDB「抽出対象判定フラグ(契約者) (ZEXTFLG01)」＝”0”かつ「抽出対象判定フラグ(被保険者) (ZEXTFLG02)」＝”0”（年金受取人が契約者とも被保険者とも違う）の時
			if (policy.RecordType.DeveloperName.equals(E_Const.POLICY_RECORDTYPE_SPVA)) {
				if (policy.SPVA_ZANNSTFLG__c && (!policy.SPVA_ZEXTFLG01__c && !policy.SPVA_ZEXTFLG02__c)) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, getMSG().get('ISS|005'),''));
					return null;
				}
			}
			
			String SPkind = policy.SpClassification__c;
			Set<String> spKinds = new Set<String>{E_Const.SP_CLASS_SPVA, E_Const.SP_CLASS_SPVWL, E_Const.SP_CLASS_SPVAANNUITY};
			
			String url = null;
			if(SPkind == null || SPkind == ''){
			    url = Page.E_Coli.getUrl();
			} else if (spKinds.contains(SPkind)) {
			    url = Page.E_Spva.getUrl();
			} else if (E_Const.SP_CLASS_ANNUITY.equals(SPkind)) {
			    url = Page.E_Annuity.getUrl();
			}
			
			// パラメータセットして遷移
			PageReference pageRef = new PageReference(url);
			pageRef.getParameters().put('id', policy.id);
			pageRef.getParameters().put('iss', '1');
			pageRef.setRedirect(true);
			return pageRef;
		} else {
			PageReference pageRef = Page.E_ErrorPage;
			pageRef.getParameters().put('code', 'ISS|003');
			pageRef.getParameters().put('iss', '1');
			return pageRef;
		}
		return null;
	}
}