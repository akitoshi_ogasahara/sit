@isTest
private class TestDS_Const {

	static testMethod void test_DS_ConstAll(){
		// 利用アプリケーション
		String test001 = DS_Const.APPNAME;

		//ダウンロード履歴.帳票種別
		String test002 = DS_Const.DH_FORMNO_DISEASE;

		//ファイル種別
		String test003 = DS_Const.FILETYPE_NORMAL;
		String test004 = DS_Const.FILETYPE_SUMISEI;

		//傷病Noパラメータ
		String test005 = DS_Const.DISEASE_URL_PARAM_NUMBER;

		//繋ぎ文字
		String test006 = DS_Const.GROUP_PUNCTUATION;

		String test007 = DS_Const.HYPHEN;
		String test008 = DS_Const.COMMA;

		String test009 = DS_Const.PAGE_NAME_DS_INDEX;
		Map<String, List<String>> test010 = DS_Const.INITIAL_MAP;

		//INITIAL_MAPのKey(昇順)
		List<String> test011 = DS_Const.INDEX_LIST;

		//Indexの初期値
		String test012 = DS_Const.INDEX_A;

		String test013 = DS_Const.PAGE_NAME_DS_SEARCH;

		String test014 = DS_Const.ROUTE_KEYWORD;
		String test015 = DS_Const.ROUTE_CATEGORY;
		String test016 = DS_Const.ROUTE_INDEX;

		// 表示するリンクの数(現在ページを中心に前後に表示する数)
		Integer test017 = DS_Const.NUMBER_OF_LINKS;
		
		//注意事項のIRISページのページユニークキー
		String test018 = DS_Const.PAGE_UNIQUE_KEY_NORMAL;
		String test019 = DS_Const.PAGE_UNIQUE_KEY_SUMISEI;
	}

}