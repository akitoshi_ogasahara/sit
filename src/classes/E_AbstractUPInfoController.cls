/**
 * ユニットプライス情報(月次レポート、ユニットプライス、ご利用方法用) 抽象コントローラクラス.
 */
public abstract class E_AbstractUPInfoController extends E_AbstractUnitPriceController {
	
	// CMS
	public List<E_MessageMaster__c> cmsContents {get; set;}
	// メニューマスタ
	public E_MenuMaster__c menuMaster {get; protected set;}
    //画像用
    public E_MessageMaster__c getCmsContent(){
        return cmsContents[0];
    }

	/**
	 * コンストラクタ.
	 */
	public E_AbstractUPInfoController() {
		super();
		this.menuMaster = null;
	}
	
	/**
	 * #init
	 * Actionメソッド.
	 */
	public PageReference init() {
		
		/*
		//service hour menu
		PageReference pageRef = isServiceHourMenu();
		if (pageRef != null) {
			return pageRef;
		}
		*/
		
		// パラメータ取得
		String mId = ApexPages.currentPage().getParameters().get('menuid');
		String mKey = getMenuKey();
		system.debug('mKey' + mKey);
		// メニューId → メニューKeyの優先順でメニューレコード取得
		if (String.isNotBlank(mId)) {
			this.menuMaster = E_MessageMasterDao.getMenuMasterRecById(mId);
		} else {
			this.menuMaster = E_MessageMasterDao.getMenuMasterRecByLikeKey(mKey);
		}
		
		if (this.menuMaster == null) {
			//  エラー
            return E_Util.toUpErrorPage('UP2|006');
		}
		
		// ページタイトル設定
		this.pgTitle = this.menuMaster.Name;
		
		// CMS取得
		this.cmsContents = E_MessageMasterDao.getMessagesByMenuId(this.menuMaster.Id);

		return null;
	}
	
	/**
	 * #getMenuKey
	 * パラメータ取得メソッド.
	 */
	private abstract String getMenuKey();
}