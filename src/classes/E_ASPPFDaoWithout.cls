public without sharing class E_ASPPFDaoWithout {
	
	//SoqlManagerを使用して代理店手数料のご案内用ファイルを取得
    public static List<E_ASPPF__c> getAsspf(Id accountId, Integer year, Integer month){
        List<E_ASPPF__c> ret = null;
        
        String soql = 'SELECT Id,';
        soql += ' ACCTYR__c, ACCTMN__c ';
        soql += ' FROM E_ASPPF__c ';
        
        E_SoqlManager manager = new E_SoqlManager();
        manager.addString(manager.pStart);
        //accountId
        manager.addAndWhere('Account__c', accountId, manager.eq);
        //年
        manager.addAndWhere('ACCTYR__c', year, manager.eq);
        //月
        manager.addAndWhere('ACCTMN__c', month, manager.eq);
        manager.addString(manager.pEnd);
        soql += manager.getWhere();
        
System.debug(soql);
        ret = Database.query(soql);
        return ret;
    }

	/**
	 *  対象明細の最古と最新日の取得
	 */
	public static AggregateResult getStartEndDate(Id accId){
		E_SoqlManager manager = new E_SoqlManager();
		String soql = 'SELECT Max(ACCTYEARMONTH__c) maxdate, MIN(ACCTYEARMONTH__c) mindate FROM E_ASPPF__c ';
		manager.addAndWhere('Account__c', accId, manager.eq);
		soql += manager.getWhere();
		return Database.query(soql);
	}
	
	/**
	 *  手数料のご案内レコードの取得
	 */
	public static E_ASPPF__c getRecById(Id aspfId){
		
		E_ASPPF__c rec = null;
		List<E_ASPPF__c> recs = [
			 Select 
				  Id
				, Name
				, Account__c
				, Account__r.Name
//				, AccParentName__c
				, ACCTMN__c
				, ACCTYEARMONTH__c
				, ACCTYR__c
				, AGCLS__c
				, AGCLSD__c
				, AGTYPE__c
				, ASPPFCode__c
				, dataSyncDate__c
				, DATIME__c
				, JOBNM__c
				, KCOMMAG__c
				, USRPRF__c
				, ZAGFLAG__c
				, ZCMHIRNM__c
				, ZCOMHIER__c
				, ZDATBKTR__c
				, ZHEADAY__c
				, ZTOTLAMT01__c
				, ZTOTLAMT02__c
				, ZTOTLAMT03__c
				, ZTOTLAMT04__c
				, ZTOTLAMT05__c
				, ZTOTLAMT06__c
				, ZTOTLAMT07__c
				, ZTOTLAMT08__c
				, ZTOTLAMT09__c
				, ZTOTLAMT10__c
				, ZTOTLAMT11__c
				, ZTOTLAMT12__c
				, ZTOTLAMT13__c
				, ZTOTLAMT14__c
				, ZTOTLAMT15__c
				, ZTOTLAMT16__c
				, ZTOTLAMT17__c
				, ZTOTLAMT18__c
				, ZTOTLAMT19__c
				, ZTOTLAMT20__c
				, ZTOTLAMT21__c
				, ZTOTLAMT22__c
				, ZTOTLAMT23__c
				, ZTOTLAMT24__c
				, ZTOTLAMT25__c
				, ZTOTLAMT26__c
				, ZTOTLAMT27__c
				, ZTOTLAMT28__c
				, ZTYPDESC__c
			 From E_ASPPF__c
			 Where 
				Id = :aspfId
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
		
	}
	
}