@isTest
private class TestI_AgencyInformationCollector {

	@isTest
	static void shouldSearchAgencyIfKeywordWasEmpty() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = '';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(collector.getTotalRows(), 0);
			System.assertEquals(collector.rowCount, 20);
			collector.addRows();
			System.assertEquals(collector.rowCount, 40);
			System.assertEquals(collector.dto, null);
			System.assertNotEquals(collector.getDISCLAIMER(), null);
		}
	}

	@isTest
	static void shouldSearchAgencyIfKeywordHasBeenAssigned01() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'stAg';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].agencyName, 'firstAgency');
		}
	}

	@isTest
	static void shouldSearchAgencyIfKeywordHasBeenAssigned02() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'リテンC';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].agencyName, 'secondAgency');
		}
	}

	@isTest
	static void shouldSearchAgencyIfKeywordHasBeenAssigned03() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'AC003';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].agencyName, 'thirdAgency');
		}
	}

	@isTest
	static void shouldSearchAgencyIfKeywordHasBeenAssigned04() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'noresult';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 0);
		}
	}

	@isTest
	static void shouldSearchOfficeIfKeywordWasEmpty() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = '';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 0);
		}
	}

	@isTest
	static void shouldSearchOfficeIfKeywordHasBeenAssigned01() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = 'stof';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].officeName, 'firstOffice');
		}
	}

	@isTest
	static void shouldSearchOfficeIfKeywordHasBeenAssigned02() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = 'ショC';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].officeName, 'secondOffice');
		}
	}

	@isTest
	static void shouldSearchOfficeIfKeywordHasBeenAssigned03() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = 'OF003';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].officeName, 'thirdOffice');
		}
	}

	@isTest
	static void shouldSearchOfficeIfKeywordHasBeenAssigned04() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = 'noresult';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 0);
			System.assertEquals(collector.getHasMessages(), true);
		}
	}

	@isTest
	static void shouldSearchAgentIfKeywordWasEmpty() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			collector.cursor.keyword = '';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 0);
		}
	}

	@isTest
	static void shouldSearchAgentIfKeywordHasBeenAssigned01() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			collector.cursor.keyword = 'stag';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].agentName, 'firstAgent');
		}
	}

	@isTest
	static void shouldSearchAgentIfKeywordHasBeenAssigned02() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			collector.cursor.keyword = 'シュウニンC';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].agentName, 'secondAgent');
		}
	}

	@isTest
	static void shouldSearchAgentIfKeywordHasBeenAssigned03() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			collector.cursor.keyword = 'AT003';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 1);
			System.assertEquals(records[0].agentName, 'thirdAgent');
		}
	}

	@isTest
	static void shouldSearchAgentIfKeywordHasBeenAssigned04() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			collector.cursor.keyword = 'noresult';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 0);
		}
	}

	@isTest
	static void shouldSearchAgentIfKeywordHasBeenAssigned05() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = 'office';
			collector.search();
			System.assertNotEquals(collector.results.size(), 0);

			collector.cursor.recordId = collector.results[0].officeId;
			collector.cursor.displayName = collector.results[0].officeName;
			collector.expand();
			collector.cursor.keyword = 'noresult';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 0);
		}
	}

	@isTest
	static void shouldReturnOptions() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			List<SelectOption> options = collector.getTypeSelectOptions();
			System.assertEquals(options.size(), 3);
			System.assertEquals(options.get(0).getValue(), I_AgencyInformationCollector.Type.AGENCY.name());
			System.assertEquals(options.get(1).getValue(), I_AgencyInformationCollector.Type.OFFICE.name());
			System.assertEquals(options.get(2).getValue(), I_AgencyInformationCollector.Type.AGENT.name());
		}
	}

	@isTest
	static void shouldReturnNextHierarchyName() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			System.assertEquals(collector.getUnderLayer(), I_AgencyInformationCollector.Type.OFFICE.name());
			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			System.assertEquals(collector.getUnderLayer(), I_AgencyInformationCollector.Type.AGENT.name());
			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			System.assertEquals(collector.getUnderLayer(), I_AgencyInformationCollector.Type.AGENT.name());
		}
	}

	@isTest
	static void shouldStacksHierarchy() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'stag';
			collector.search();
			System.assertEquals(collector.cursor.type, 'AGENCY');
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].agencyName, 'firstAgency');
			collector.cursor.recordId = collector.results[0].agencyId;
			collector.cursor.displayName = collector.results[0].agencyName;
			collector.expand();

			System.assertEquals(collector.histories.size(), 1);
			collector.cursor.keyword = 'stof';
			collector.search();
			System.assertEquals(collector.cursor.type, 'OFFICE');
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].officeName, 'firstOffice');
			collector.cursor.recordId = collector.results[0].officeId;
			collector.cursor.displayName = collector.results[0].officeName;
			collector.expand();

			System.assertEquals(collector.histories.size(), 2);
			System.assertEquals(collector.cursor.type, 'AGENT');
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].agentName, 'firstAgent');
			System.assertEquals(collector.cursor.keyword, (String) null);

			{
				collector.collapse();
				System.assertEquals(collector.histories.size(), 1);
				System.assertEquals(collector.cursor.keyword, 'stof');
				System.assertEquals(collector.cursor.type, 'OFFICE');
				System.assertEquals(collector.results.size(), 1);
				System.assertEquals(collector.results[0].officeName, 'firstOffice');
			}
			{
				collector.numberOfHistoryIndex = 0;
				collector.moveToIndex();
				System.assertEquals(collector.histories.size(), 0);
				System.assertEquals(collector.cursor.keyword, 'stag');
				System.assertEquals(collector.cursor.type, 'AGENCY');
				System.assertEquals(collector.results.size(), 1);
				System.assertEquals(collector.results[0].agencyName, 'firstAgency');
			}
		}
	}

	@isTest
	static void shouldResetHistories() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'stag';
			collector.search();
			System.assertEquals(collector.cursor.type, 'AGENCY');
			System.assertEquals(collector.results.size(), 1);
			System.assertEquals(collector.results[0].agencyName, 'firstAgency');
			collector.cursor.recordId = collector.results[0].agencyId;
			collector.cursor.displayName = collector.results[0].agencyName;
			collector.expand();
			collector.cursor.keyword = 'stag';
			System.assertEquals(collector.histories.size(), 1);
			collector.resetType();
			System.assertEquals(collector.cursor.keyword, 'stag');
			System.assertEquals(collector.histories.size(), 0);
		}
	}

	@isTest
	static void testSortRows() {
		User mr = [SELECT id FROM User WHERE lastName = 'mr.user'];
		System.runAs(mr) {
			I_AgencyInformationCollector collector = new I_AgencyInformationCollector();
			collector.cursor.keyword = 'Agency';
			collector.search();
			List<I_AgencyInformationCollector.SearchResultInfo> records = collector.results;
			System.assertEquals(records.size(), 3);
			System.assertEquals(records[0].agencyName, 'thirdAgency');
			ApexPages.currentPage().getParameters().put('st', 'agencyName');
			collector.sortRows();
			System.assertEquals(records[0].agencyName, 'secondAgency');
			ApexPages.currentPage().getParameters().put('st', 'agencyCode');
			collector.sortRows();
			System.assertEquals(records[0].agencyName, 'firstAgency');

			collector.cursor.type = I_AgencyInformationCollector.Type.OFFICE.name();
			collector.cursor.keyword = 'office';
			collector.search();
			records = collector.results;
			System.assertEquals(records.size(), 3);
			System.assertEquals(records[0].officeName, 'thirdOffice');
			ApexPages.currentPage().getParameters().put('st', 'officeName');
			collector.sortRows();
			System.assertEquals(records[0].officeName, 'secondOffice');
			ApexPages.currentPage().getParameters().put('st', 'officeCode');
			collector.sortRows();
			System.assertEquals(records[0].officeName, 'firstOffice');
			ApexPages.currentPage().getParameters().put('st','address');
			collector.sortRows();
			System.assertEquals(records[0].officeName, 'secondOffice');

			collector.cursor.type = I_AgencyInformationCollector.Type.AGENT.name();
			collector.cursor.keyword = 'Agent';
			collector.search();
			records = collector.results;
			System.assertEquals(records.size(), 4);
			System.assertEquals(records[0].agentName, 'thirdAgent');
			ApexPages.currentPage().getParameters().put('st', 'agentName');
			collector.sortRows();
			System.assertEquals(records[0].agentName, 'fourthAgent');
			ApexPages.currentPage().getParameters().put('st', 'agentCode');
			collector.sortRows();
			System.assertEquals(records[0].agentName, 'firstAgent');
		}

	}

	@testSetup
	static void setup() {
		User mrUser = TestI_TestUtil.createUser(true, 'mr.user', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, mrUser.Id, null);

		Account firstAgency = new Account(Name = 'firstAgency', E_CL1PF_ZAHKNAME__c = 'ダイリテンB', E_CL1PF_ZHEADAY__c = 'AC001');
		Account secondAgency = new Account(Name = 'secondAgency', E_CL1PF_ZAHKNAME__c = 'ダイリテンC', E_CL1PF_ZHEADAY__c = 'AC002');
		Account thirdAgency = new Account(Name = 'thirdAgency', E_CL1PF_ZAHKNAME__c = 'ダイリテンA', E_CL1PF_ZHEADAY__c = 'AC003');
		insert new List<Account>{
			firstAgency,
			secondAgency,
			thirdAgency
		};

		Account firstOffice = new Account(name = 'firstOffice', E_CL2PF_ZEAYNAM__c = 'firstOffice', parentId = firstAgency.id, E_CL2PF_ZEAYKNAM__c = 'ジムショB', E_COMM_VALIDFLAG__c = '1', E_CL2PF_BRANCH__c = '90', E_CL2PF_ZAGCYNUM__c = 'OF001', E_COMM_ZCLADDR__c = 'テスト住所2');
		Account secondOffice = new Account(name = 'secondOffice', E_CL2PF_ZEAYNAM__c = 'secondOffice', parentId = secondAgency.id, E_CL2PF_ZEAYKNAM__c = 'ジムショC', E_COMM_VALIDFLAG__c = '1', E_CL2PF_BRANCH__c = '90', E_CL2PF_ZAGCYNUM__c = 'OF002', E_COMM_ZCLADDR__c = 'テスト住所1');
		Account thirdOffice = new Account(name = 'thirdOffice', E_CL2PF_ZEAYNAM__c = 'thirdOffice', parentId = thirdAgency.id, E_CL2PF_ZEAYKNAM__c = 'ジムショA', E_COMM_VALIDFLAG__c = '1', E_CL2PF_BRANCH__c = '90', E_CL2PF_ZAGCYNUM__c = 'OF003', E_COMM_ZCLADDR__c = 'テスト住所3');
		insert new List<Account>{
			firstOffice,
			secondOffice,
			thirdOffice
		};

		Contact firstAgent = new Contact(LastName = 'firstAgent', AccountId = firstOffice.Id, E_CL3PF_ZEATKNAM__c = 'ボシュウニンB', E_CL3PF_AGNTNUM__c = 'AT001', E_CL3PF_VALIDFLAG__c = '1');
		Contact secondAgent = new Contact(LastName = 'secondAgent', AccountId = secondOffice.Id, E_CL3PF_ZEATKNAM__c = 'ボシュウニンC', E_CL3PF_AGNTNUM__c = 'AT002', E_CL3PF_VALIDFLAG__c = '1');
		Contact thirdAgent = new Contact(LastName = 'thirdAgent', AccountId = thirdOffice.Id, E_CL3PF_ZEATKNAM__c = 'ボシュウニンA', E_CL3PF_AGNTNUM__c = 'AT003', E_CL3PF_VALIDFLAG__c = '1');
		Contact fourthAgent = new Contact(LastName = 'fourthAgent', AccountId = thirdOffice.Id, E_CL3PF_ZEATKNAM__c = 'ボシュウニンD', E_CL3PF_AGNTNUM__c = 'AT004', E_CL3PF_VALIDFLAG__c = '1');
		Contact fifthAgent = new Contact(LastName = 'fifthAgent', AccountId = thirdOffice.Id, E_CL3PF_ZEATKNAM__c = 'ボシュウニンE', E_CL3PF_AGNTNUM__c = 'AT004|a', E_CL3PF_VALIDFLAG__c = '1');
		insert new List<Contact>{
			firstAgent,
			secondAgent,
			thirdAgent,
			fourthAgent,
			fifthAgent
		};

		User firstUser = TestI_TestUtil.createAgentUser(true, 'first.user', 'E_PartnerCommunity', firstAgent.id);
		TestI_TestUtil.createIDCPF(true, firstUser.Id, null);

		User secondUser = TestI_TestUtil.createAgentUser(true, 'second.user', 'E_PartnerCommunity', secondAgent.id);
		TestI_TestUtil.createIDCPF(true, secondUser.Id, null);

		User thirdUser = TestI_TestUtil.createAgentUser(true, 'third.user', 'E_PartnerCommunity', thirdAgent.id);
		TestI_TestUtil.createIDCPF(true, thirdUser.Id, null);

	}
}