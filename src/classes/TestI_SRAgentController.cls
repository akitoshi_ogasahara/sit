@isTest
public with sharing class TestI_SRAgentController {
	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static Contact agent;
	private static E_AgencySalesResults__c asr;
	private static E_SpRelationCorp__c spRelationCorps;
	private static E_BizDataSyncLog__c bds;

	public static testMethod void getAgentRecListTest01(){
		createData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		I_SRAgentController controller = new I_SRAgentController();
		controller.agencyId = distribute.Id;

		Test.startTest();
		controller.getAgentRecList();
		System.assertEquals(controller.AgentRows.size(), 21);
		Test.stopTest();
	}
	public static testMethod void getAgentRecListTest02(){
		createData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		I_SRAgentController controller = new I_SRAgentController();
		controller.agencyId = '';
		controller.officeId = office.Id;

		Test.startTest();
		controller.getAgentRecList();
		System.assertEquals(controller.AgentRows.size(), 21);
		Test.stopTest();
	}
	public static testMethod void getAgentRecListTest03(){
		createBizData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		I_SRAgentController controller = new I_SRAgentController();
		controller.agencyId = '';
		controller.officeId = '';

		Test.startTest();
		controller.getAgentRecList();
		System.assertEquals(controller.AgentRows.size(), 0);
		Test.stopTest();
	}
	public static testMethod void getRowsTest01(){
		createData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		I_SRAgentController controller = new I_SRAgentController();

		Test.startTest();
		controller.getListMaxRows();
		controller.addRows();
		controller.getTotalRows();
		Test.stopTest();

	}
	public static testMethod void sortTest01(){
		createData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		I_SRAgentController controller = new I_SRAgentController();

		Test.startTest();
		controller.readyAction();
		pageRef.getParameters().put('URL_PARAM_SORT_TYPE', 'passCode');
		controller.sortRows();
		Test.stopTest();
	}
	public static testMethod void sortTest02(){
		createData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		I_SRAgentController controller = new I_SRAgentController();
		controller.agencyId = distribute.Id;
		controller.officeId = '';
		controller.getAgentRecList();

		pageRef.getParameters().put('stype', 'agentCode');
		controller.sortIsAsc = true;
		controller.sortRows();


		pageRef.getParameters().put('stype', 'kmofCode');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'agentName');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'agentType');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'officeName');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'cnvsLicense');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'startDate');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'cnvsLicenseVar');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'capacity');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'passDate');
		controller.sortIsAsc = true;
		controller.sortRows();

		pageRef.getParameters().put('stype', 'passCode');
		controller.sortIsAsc = true;
		controller.sortRows();
	}
	public static testMethod void rowCountTest01(){
		createBizData();

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('row', '1001');
		Test.startTest();
		I_SRAgentController controller = new I_SRAgentController();
		controller.addRows();
		controller.getListMaxRows();
		System.assertEquals(controller.rowCount, 500);
		Test.stopTest();

	}
	//public static testMethod void exportTest01(){
	//	createData();

	//	PageReference pageRef = Page.IRIS_AgencySalesResults;
	//	Test.setCurrentPage(pageRef);
	//	I_SRAgentController controller = new I_SRAgentController();
	//	controller.agencyId = '';
	//	controller.officeId = office.Id;

	//	Test.startTest();
	//	controller.ExportCSV();
	//	Test.stopTest();
	//}

	public static testMethod void coverageTest01(){
		List<E_MessageMaster__c> msgs = new List<E_MessageMaster__c>();
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|007',Value__c = 'AMS|SR|007',Type__c = 'ディスクレイマー'));
		msgs.add(new E_MessageMaster__c(Key__c = 'AMS|SR|008',Value__c = 'AMS|SR|008',Type__c = 'ディスクレイマー'));
		insert msgs;

		PageReference pageRef = Page.IRIS_AgencySalesResults;
		Test.setCurrentPage(pageRef);

		I_SRAgentController controller = new I_SRAgentController();
		controller.paramMessages = new List<String>{'AMS|SR|007','AMS|SR|008'};
		Map<String,String> msgMap = controller.msg;
		msgMap = controller.msg;
		controller.getIsCommonGateway();

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = System.today());
		System.assertEquals(System.now().format('yyyy/MM/dd'), controller.bizDate);

	}

	private static E_BizDataSyncLog__c createBizData(){
		Date d = date.newInstance(2000, 1, 1);
		DateTime dt = datetime.newInstance(2000, 1, 1);
		bds = new E_BizDataSyncLog__c(
			Kind__c = 'F',
			InquiryDate__c = d,
			BatchEndDate__c = dt
		);
		insert bds;
		return bds;
	}
	private static void createData(){
		// テストデータ作成
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String BUSINESS_DATE = '20000101';

		// Account 格
		distribute = new Account(Name = 'テスト代理店格', E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;

		// Account 事務所
		office = new Account(
			Name = 'テスト事務所01',
			ParentId = distribute.Id,
			E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,
			E_CL2PF_BRANCH__c = CODE_BRANCH,
			E_COMM_VALIDFLAG__c = '1'
		);
		insert office;

		// E-biz
		createBizData();

		// Contact　募集人
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		List<Contact> agentList = new List<Contact>();
		system.runAs(thisUser){
			for(integer i=0; i<21; i++){
				agent = new Contact(
					FirstName = 'テスト'+ String.valueOf(i),
					LastName = '募集人'+ String.valueOf(i),
					E_CL3PF_AGNTNUM__c = 'XX00'+ String.valueOf(i),
					AccountId = office.Id,
					E_CL3PF_ZHEADAY__c = CODE_DISTRIBUTE
				);
				agentList.add(agent);
			}
			insert agentList;
		}

		// 代理店挙績情報
		List<E_AgencySalesResults__c> asrList = new List<E_AgencySalesResults__c>();
		for(Contact agt: agentList){
			asr = new E_AgencySalesResults__c(
				ParentAccount__c = distribute.Id,
				Account__c = office.Id,
				Agent__c = agt.Id,
				BusinessDate__c = BUSINESS_DATE,
				Hierarchy__c = 'AT',
				XHAT_KMOFCODE__c = '1001',				// 募集人登録番号
				AgentType__c = 'XX'	,					// 登録区分
				BelongOfficeName__c = 'テスト事務所',		// 所属事務所名
				CnvsLicense_IRIS__c = '有',				// 当社での募集資格
				CnvsStartDate__c = '20170101',			// 当社での募集開始日
				CnvsLicenseVariableIns__c = 'XX',		// 当社での変額資格
				AGED_KPCDT__c = '201701',				// 専門資格
				AGED_KPCLC__c = '1001'					// 専門資格
			);
			asrList.add(asr);
		}
		insert asrList;
	}
}