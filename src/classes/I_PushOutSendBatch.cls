public with sharing class I_PushOutSendBatch implements Database.Batchable<sObject>, Database.Stateful {
    
	// EBiz連携ログ
	private E_BizDataSyncLog__c ebizLog;
	// EBiz連携ログ 作成日
	private Datetime logCreatedDate;
	// EBiz連携ログ 通知区分
	private String notificationKbn;
	
	private List<String> errorMessages;
    
    /**
     * Constructor
     */
    public I_PushOutSendBatch(E_BizDataSyncLog__c ebizLog){
    	this.ebizLog = ebizLog;
    	this.logCreatedDate = ebizLog.CreatedDate;
    	this.notificationKbn = ebizLog.NotificationKind__c;
    	errorMessages = new List<String>();
    }
    
    /**
     * start
     */
    public Database.QueryLocator start(Database.BatchableContext BC) {
    	String query = 'Select Id, IsSendTarget__c From I_PushOut__c Where Type__c = :notificationKbn And IsSended__c = false And IsSendTarget__c = false Order by isEmployee__c';
    	return Database.getQueryLocator (query);
    }
    
    /**
     * execute
     */
    public void execute(Database.BatchableContext BC, List<SObject> records) {
    	System.debug('*** I_PushOutSendBatch execute ***');
    	try{
	    	for(SObject record : records){
	    		record.put('IsSendTarget__c', true);
	    	}
	    	update records;
	    	
    	}catch(Exception e){
			errorMessages.add(e.getMessage());
		}
    }
    
    /**
     * finish
     */
    public void finish(Database.BatchableContext BC) {
		if(errorMessages.size()>0){
			ebizlog.log__c = E_Util.null2Blank(ebizlog.log__c) + ' +++ sendBatch +++ ';
			ebizlog.log__c += String.join(errorMessages, ',');
		}
		ebizlog.datasyncenddate__c = Datetime.now();
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.allowFieldTruncation = true;
		ebizlog.setOptions(dml);
		update ebizlog;
    }
}