public with sharing class E_AGPOTriggerHandler {
	public void onBeforeInsert (List<E_AGPO__c> newList){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList,new Map<Id,E_AGPO__c>());
	}
	public void onBeforeUpdate (List<E_AGPO__c> newList,Map<Id,E_AGPO__c> oldMap){
		E_DownloadNoticeUtil.setRelateToE_CHTPF (newList,oldMap);
	}
}