@isTest
private class MNT_DocumentMaintenanceExtenderTest{
	/**
	 * 初期化処理
	 */
	@isTest static void initTest01(){
		//テストデータ作成
		I_ContentMaster__c cms = new I_ContentMaster__c(Name = 'test1', UpsertKey__c = 'testUpsert');
		insert cms;

		Pagereference pref = Page.MNT_DocumentMaintenance;
		//コピー時パラメータあり
		pref.getParameters().put('id', cms.Id);
		pref.getParameters().put('clone', '1');
		Test.setCurrentPage(pref);

		//テスト実行
		Test.startTest();
		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(cms);
		MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();
		extender.init();

		Test.stopTest();
		//テスト検証
		System.assertEquals(null, controller.record.UpsertKey__c);
		System.assertEquals(cms.Id, ApexPages.currentPage().getParameters().get('id'));
		System.assertEquals('1', ApexPages.currentPage().getParameters().get('clone'));
	}

	/**
	 * 編集時_添付ファイルあり
	 */
	@isTest static void initTest02(){
		//テストデータ作成
		I_ContentMaster__c cms = new I_ContentMaster__c(Name = 'test1', UpsertKey__c = 'testUpsert');
		insert cms;
		Attachment att = new Attachment(ParentId = cms.Id , Name = 'testFile01', body = Blob.valueOf('testFile01'));
		insert att;

		Pagereference pref = Page.MNT_DocumentMaintenance;
		pref.getParameters().put('id', cms.Id);
		Test.setCurrentPage(pref);
		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(cms);
		MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();

		//テスト実行
		Test.startTest();
		extender.init();

		Test.stopTest();
		//テスト検証
		System.assertEquals(false, extender.isUpload);
	}

	/**
	 * 編集時_添付ファイルあり
	 */
	@isTest static void initTest03(){
		//テストデータ作成
		I_ContentMaster__c cms = new I_ContentMaster__c(Name = 'test1', UpsertKey__c = 'testUpsert');
		insert cms;

		Pagereference pref = Page.MNT_DocumentMaintenance;
		pref.getParameters().put('id', cms.Id);
		Test.setCurrentPage(pref);
		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(cms);
		MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();

		//テスト実行
		Test.startTest();
		extender.init();

		Test.stopTest();
		//テスト検証
		System.assertEquals(true, extender.isUpload);
	}

	/**
	 * キャンセルボタン押下時
	 */
	@isTest static void doCancelTest01(){
		//テストデータ作成
		I_ContentMaster__c cms = new I_ContentMaster__c(Name = 'test1', UpsertKey__c = 'testUpsert');
		insert cms;

		Pagereference pref = Page.MNT_DocumentMaintenance;
		pref.getParameters().put('id', cms.Id);
		pref.getParameters().put('clone', '1');
		Test.setCurrentPage(pref);

		//テスト実行
		Test.startTest();
		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(cms);
		MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();

		Pagereference pageRef = extender.doCancel();

		Test.stopTest();
		//テスト検証
		String pageName = pageRef.getUrl();
		System.assert(pageName.contains('mnt_documentview'));
		System.assertEquals(cms.Id, pageRef.getParameters().get('id'));
		System.assertEquals(null, pageRef.getParameters().get('clone'));

	}

	/**
	 * 新規作成からキャンセルボタン押下時
	 */
	@isTest static void doCancelTest02(){
		//テストデータ作成
		I_ContentMaster__c cms = new I_ContentMaster__c(Name = 'test1', UpsertKey__c = 'testUpsert');
		insert cms;

		Pagereference pref = Page.MNT_DocumentMaintenance;
		pref.getParameters().put('rid', cms.Id);
		Test.setCurrentPage(pref);

		//テスト実行
		Test.startTest();
		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(cms);
		MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();

		Pagereference pageRef = extender.doCancel();

		Test.stopTest();
		//テスト検証
		String pageName = pageRef.getUrl();
		System.assert(pageName.contains('mnt_documentview'));
		System.assertEquals(cms.Id, pageRef.getParameters().get('id'));
	}

	@isTest
	private static void doSaveTest01(){
		String formNo = '012345-S';
		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険', FormNo__c = formNo, DOM_Id__c = formNo);
		insert content;
		
		PageReference pref = Page.MNT_DocumentMaintenance;
		Test.setCurrentPage(pref);
		pref.getParameters().put('id', content.Id);
		pref.getParameters().put('clone', '1');
        ApexPages.standardController standardcontroller = new ApexPages.StandardController(content);
       	MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();

        extender.init();
        controller.record.FormNo__c = formNo;
        controller.record.DOM_Id__c = formNo;

        Test.startTest();
        PageReference testRef = extender.doSave();
        Test.stopTest();
	}

	@isTest
	private static void doSaveTest02(){
		String formNo = '012345-S';
		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険', FormNo__c = formNo);
		insert content;
		
		PageReference PageRef = Page.MNT_DocumentMaintenance;
		Test.setCurrentPage(PageRef);
		ApexPages.currentPage().getParameters().put('id', content.Id);
        ApexPages.standardController standardcontroller = new ApexPages.StandardController(content);
       	MNT_DocumentMaintenance controller = new MNT_DocumentMaintenance(standardcontroller);
		MNT_DocumentMaintenanceExtender extender = controller.getExtender();
        extender.init();
        controller.record.FormNo__c = formNo + 'doSaveTest02';
        //テスト用のcsvファイル作成
        Attachment att = new Attachment(
        	Name = 'TestData.txt',
        	Body = Blob.valueOf('testFile02')
        );

        extender.uploadFile = att;

        Test.startTest();
        PageReference testRef = extender.doSave();
        Test.stopTest();

        Attachment selectAtt = [SELECT Id,Name,ParentId FROM Attachment LIMIT 1];
        I_ContentMaster__c selectContent = [SELECT Id FROM I_ContentMaster__c LIMIT 1];

        System.assertEquals(selectContent.Id, selectAtt.parentId);
	}

}