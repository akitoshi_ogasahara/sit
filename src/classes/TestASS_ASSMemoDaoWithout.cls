@isTest
private class TestASS_ASSMemoDaoWithout {

	//エリア部長で実行
	private static testMethod void getMemosByOfficeIdTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//営業日報メモ作成
		//営業
		ASS_Memo__c memo = new ASS_Memo__c(Name = 'test', Account__c = office.Id, Contents__c = 'テスト');
		insert memo;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<ASS_Memo__c> retMemos = ASS_ASSMemoDaoWithout.getMemosByOfficeId(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retMemos.size(),1);
			System.assertEquals(retMemos[0].Id,memo.Id);
		}
	
	}

	private static testMethod void deleteMemoTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//営業日報メモ作成
		List<ASS_Memo__c> memoList = new List<ASS_Memo__c>();
		memoList.add(new ASS_Memo__c(Name = 'test', Account__c = office.Id, Contents__c = 'テスト'));
		insert memoList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_ASSMemoDaoWithout.deleteMemo(memoList);
//===============================テスト終了===============================
			Test.stopTest();
			List<ASS_Memo__c> retMemoList = [SELECT Id FROM ASS_Memo__c WHERE Account__c = :office.Id];
			System.assertEquals(retMemoList.isEmpty(),true);
		}
	
	}

	//MRで実行
	private static testMethod void getMemosByOfficeIdMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//営業日報メモ作成
		//営業
		ASS_Memo__c memo = new ASS_Memo__c(Name = 'test', Account__c = office.Id, Contents__c = 'テスト');
		insert memo;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			List<ASS_Memo__c> retMemos = ASS_ASSMemoDaoWithout.getMemosByOfficeId(office.Id);
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(retMemos.size(),1);
			System.assertEquals(retMemos[0].Id,memo.Id);
		}
	
	}

	private static testMethod void deleteMemoMRTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//営業日報メモ作成
		List<ASS_Memo__c> memoList = new List<ASS_Memo__c>();
		memoList.add(new ASS_Memo__c(Name = 'test', Account__c = office.Id, Contents__c = 'テスト'));
		insert memoList;


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_ASSMemoDaoWithout.deleteMemo(memoList);
//===============================テスト終了===============================
			Test.stopTest();
			List<ASS_Memo__c> retMemoList = [SELECT Id FROM ASS_Memo__c WHERE Account__c = :office.Id];
			System.assertEquals(retMemoList.isEmpty(),true);
		}
	}
}