/**
 * メニュー種類データアクセスオブジェクト
 * CreatedDate 2014/12/02
 * @Author Terrasky
 */
public class E_MenuKindDao {
    
    /**
     * メニュー種類データを取引先Idから取得する
     * @param MenukndId: 取引先Id
     * @return E_MenuKind__c: メニュー種類
     */
/* 数式で対応したため不要
    public static E_MenuKind__c getMenuKndByAccountId(Id AccountId){
        return [
            SELECT
                  Id
                , Name
                , ExternalId__c
            FROM E_MenuKind__c
            WHERE Id IN (
                        SELECT
                            E_MenuKind__c
                        FROM Account
                        WHERE Id = :AccountId
                    )
        ];
    }
*/
    
    /**
     * メニュー種類データリストを取引先Idから取得する
     * @return List<E_MenuKind__c>: メニュー種類リスト
     */
    public static List<E_MenuKind__c> getMenuKnds(){
        return [
            SELECT
                  Id
                , Name
                , ExternalId__c
            FROM E_MenuKind__c
            WHERE ExternalId__c != :E_Const.MENUKIND_EXID_CUSTOMER
            ORDER BY SortNumber__c
        ];
    }
    
    /**
     * メニュー種類データリストを取引先Idから取得する
     * @return E_MenuKind__c: メニュー種類レコード
     */
    public static E_MenuKind__c getRecById(String menuKindId){
    	Id mkId = menuKindId;
        return [
            SELECT
                  Id
                , Name
                , ExternalId__c
            FROM 
        		E_MenuKind__c
            WHERE 
        		Id = :mkId
        ];
    }

    /**
     * メニュー種類データリストを外部IDから取得する
     * @return E_MenuKind__c: メニュー種類レコード
     */
    public static E_MenuKind__c getRecByExternalId(String ExternalId){
        return [
            SELECT
                  Id
                , Name
                , ExternalId__c
            FROM 
        		E_MenuKind__c
            WHERE 
        		ExternalId__c = :ExternalId
        ];
    }
}