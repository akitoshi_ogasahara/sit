public with sharing class I_LicenseAssessmentController {

	public Id paramSalesResultsId {get; set;}				// 代理店挙積情報ID
	public String paramQualif {								// 現在資格
		get;
		set{
			// 半角変換
			String widthAlphabet = 'ａＡｂＢｃＣｄＤｅＥｆＦｇＧｈＨｉＩｊＪｋＫｌＬｍＭｎＮｏＯｐＰｑＱｒＲｓＳｔＴｕＵｖＶｗＷｘＸｙＹｘＺ';
			String alphabet = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ';
			String temp = value;
			for(Integer i = 0; i < widthAlphabet.length(); i += 2){
				temp = temp.replaceAll('[' + widthAlphabet.substring(i,i+2) + ']', alphabet.substring(i,i+1));
			}
			paramQualif = temp;
		}
	}
	transient public String[] paramDisclaimer {get; set;}	// 表示ディスクレーマ
	public Map<String, String> disclaimers{					// 表示ディスクレーマ(表示用)
		get{
			if(disclaimers == null){
				// パラメータよりMapを作成
				while(this.paramDisclaimer.size() < 1) {
					this.paramDisclaimer.add('');
				}
				disclaimers = new Map<String, String>();
				disclaimers.put('AMS|SR|004', this.paramDisclaimer[0]);
			}
			return disclaimers;
		}
		set;
	}

	public String filteringType {get; set;}		// 選択タブ値
	transient public String json {get; set;}	// JSONデータ

	public Boolean isShow {						// 表示
		get{
			if(isShow == null){
				isShow = false;
				if(ACHIEVEMENT_MAP.containsKey(this.paramQualif)){
					isShow = true;
				}
			}
			return isShow;
		}
		set;
	}
	//public Boolean isShow {						// 表示
	//	get{
	//		if(isShow == null){
	//			isShow = false;
	//			List<SelectOption> filters	= getFilteringOptions();
	//			for(SelectOption filter : filters){
	//				if(this.paramQualif == filter.getLabel()){
	//					isShow = true;
	//					break;
	//				}
	//			}
	//		}
	//		return isShow;
	//	}
	//	set;
	//}

	public String targetId {					// 選択テーブルID
		get{
			targetId = '';
			if(ACHIEVEMENT_MAP.containsKey(this.paramQualif)){
				targetId = ACHIEVEMENT_MAP.get(this.paramQualif);
			}
			return targetId;
		}
		set;
	}
	//public String targetId {					// 選択テーブルID
	//	get{
	//		targetId = '';
	//		List<SelectOption> filters	= getFilteringOptions();
	//		for(SelectOption filter : filters){
	//			if(this.paramQualif == filter.getLabel()){
	//				targetId = filter.getValue();
	//				break;
	//			}
	//		}
	//		return targetId;
	//	}
	//	set;
	//}

	/* 優績 */
	public static String ACHIEVEMENT_TYPE_ACHS	= 'license-assessment-achs';
	public static String ACHIEVEMENT_TYPE_ACHA	= 'license-assessment-acha';
	public static String ACHIEVEMENT_TYPE_ACHB	= 'license-assessment-achb';
	public static String ACHIEVEMENT_TYPE_ADV	= 'license-assessment-adv';
	public static String ACHIEVEMENT_TYPE_NORM	= 'license-assessment-norm';

	public static String ACHIEVEMENT_LABEL_ACHSS= '優績SS';						//優績SS
	public static String ACHIEVEMENT_LABEL_ACHS	= '優績S';
	public static String ACHIEVEMENT_LABEL_ACHA	= '優績A';
	public static String ACHIEVEMENT_LABEL_ACHB	= '優績B';
	public static String ACHIEVEMENT_LABEL_ADV	= '上級';
	public static String ACHIEVEMENT_LABEL_NORM	= '普通';
	public static String ACHIEVEMENT_LABEL_PROT	= 'プロテクション優績特則';			//プロテクション優績特則

	public static Map<String,String> ACHIEVEMENT_MAP = new Map<String,String>{};
	{
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_ACHSS,ACHIEVEMENT_TYPE_ACHS);
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_ACHS,ACHIEVEMENT_TYPE_ACHS);
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_ACHA,ACHIEVEMENT_TYPE_ACHA);
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_ACHB,ACHIEVEMENT_TYPE_ACHB);
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_ADV ,ACHIEVEMENT_TYPE_ADV );
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_NORM,ACHIEVEMENT_TYPE_NORM);
		ACHIEVEMENT_MAP.put(ACHIEVEMENT_LABEL_PROT,ACHIEVEMENT_TYPE_ACHS);
	}



	/**
	 * コンストラクタ
	 */
	public I_LicenseAssessmentController(){
		this.paramSalesResultsId	= null;										// 代理店挙積情報ID
		this.paramQualif			= '';										// 現在資格
		this.paramDisclaimer		= new List<String>();						// 表示ディスクレーマ

	    this.filteringType			= '';										// 選択タブ値
	    this.json					= '';										// JSONデータ
	}

	/**
	 * 選択タブ値の候補リスト
	 */
	public List<SelectOption> getFilteringOptions() {
		return new List<SelectOption>{
			new SelectOption(ACHIEVEMENT_TYPE_ACHS,	ACHIEVEMENT_LABEL_ACHS),
			new SelectOption(ACHIEVEMENT_TYPE_ACHA,	ACHIEVEMENT_LABEL_ACHA),
			new SelectOption(ACHIEVEMENT_TYPE_ACHB,	ACHIEVEMENT_LABEL_ACHB),
			new SelectOption(ACHIEVEMENT_TYPE_ADV,	ACHIEVEMENT_LABEL_ADV),
			new SelectOption(ACHIEVEMENT_TYPE_NORM,	ACHIEVEMENT_LABEL_NORM)
		};
	}

	/**
	 * JS用値設定の取得(リモートアクション)
	 */
	@readOnly
	@RemoteAction
	public static String getJSParam(Id pSalesResultsId, String pQualif){
		I_LicenseAssessmentController cls	= new I_LicenseAssessmentController();
		cls.paramSalesResultsId			= pSalesResultsId;	// 代理店挙積情報ID
		cls.paramQualif					= pQualif;			// 現在資格
		cls.getJSParameter();								// JS用値設定の取得
		return cls.json;									// JSONデータ
	}

	/**
	 * JS用値設定の取得
	 */
	public void getJSParameter(){
		// 代理店挙積情報取得
		//E_AgencySalesResults__c agencySalesResults = E_AgencySalesResultsDao.getRecTypeBById(paramSalesResultsId);

		// 選択タブ値
		this.filteringType			= '';
		List<SelectOption> filters	= getFilteringOptions();
		for(SelectOption filter : filters){
			if(this.paramQualif == filter.getLabel()){
				this.filteringType = filter.getValue();
				break;
			}
		}

		// JSONデータ
		this.json				= getJSON();

	}

	/**
	 * JSONデータの取得
	 */
	public String getJSON(){
		List<String> fields			= new List<String>();
		List<String> lines			= new List<String>();
		List<SelectOption> filters	= getFilteringOptions();

		// 資格査定詳細情報リスト取得
		List<E_LicenseAssessment__c> licenseAssessmentList = E_LicenseAssessmentDao.getRecsByASRId(paramSalesResultsId);

		for(E_LicenseAssessment__c licenseAssessment : licenseAssessmentList){
			// 各項目の変換
			fields				= new List<String>();
			String temp			= '';
			List<String> temps	= new List<String>();
			boolean isNext		= false;

			// ターゲットId
			for(SelectOption filter : filters){
				if(licenseAssessment.Qualification__c == filter.getLabel()){
					temp = filter.getValue();
					break;
				}
			}
			fields.add('"TID":"' + temp + '"');										// ターゲットId

			fields.add('"No":"' + I_Util.toStr(licenseAssessment.SEQ__c) + '"');	// SEQ
			// 新契約係数ANP
			temps = getNumberText(0, licenseAssessment.Standard_NBCANP__c, licenseAssessment.State_NBCANP__c);
			fields.add('"BANP":"' + temps[0] + '"');													// 基準　新契約係数ANP
			fields.add('"SANP":"' + temps[1] + '"');													// 状況　新契約係数ANP
			if(temps[0] != '') isNext = true;

			// 新契約係数ANP 特定保険種類
			temps = getNumberText(0, licenseAssessment.Standard_NBCANP_SpInsType__c, licenseAssessment.State_NBCANP_SpInsType__c);
			fields.add('"BANPS":"' + ((isNext == true && temps[0] != '')?'かつ':'') + temps[0] + '"');	// 基準　新契約係数ANP 特定保険種類
			fields.add('"SANPS":"' + temps[1] + '"');													// 状況　新契約係数ANP 特定保険種類
			if(temps[0] != '') isNext = true;

			// 保有ANP
			temps = getNumberText(0, licenseAssessment.Standard_IANP__c, licenseAssessment.State_IANP__c);
			fields.add('"BIANP":"' + ((isNext == true && temps[0] != '')?'かつ':'') + temps[0] + '"');	// 基準　保有ANP
			fields.add('"SIANP":"' + temps[1] + '"');													// 状況　保有ANP
			if(temps[0] != '') isNext = true;

			// 稼動月数
			temps = getNumberText(2, licenseAssessment.Standard_ActiveMonth__c, licenseAssessment.State_ActiveMonth__c);
			fields.add('"BAM":"' + ((isNext == true && temps[0] != '')?'かつ':'') +  temps[0] + '"');	// 基準　稼動月数
			fields.add('"SAM":"' + temps[1] + '"');														// 状況　稼動月数
			if(temps[0] != '') isNext = true;

			// IQA継続率
			temps = getNumberText(3, licenseAssessment.Standard_IQA__c, licenseAssessment.State_IQA__c);
			fields.add('"BIQA":"' + ((isNext == true && temps[0] != '')?'かつ':'') +  temps[0] + '"');	// 基準　IQA継続率
			fields.add('"SIQA":"' + temps[1] + '"');													// 状況　IQA継続率

			// 1行データへ編集
			lines.add('{' + String.join(fields,',') + '}');
		}
		return String.join(lines,',\n');
	}

	/**
	 * 基準/状況 文字列へ変換
	 */
	public List<String> getNumberText(Integer type, Decimal standard, Decimal state){
		List<String> temps	= new List<String>();
		String temp			= '';
		Decimal num			= 0;
		Integer point		= 0;
		standard	= (standard == null) ? 0 : standard;
		state		= (state == null) ? 0 : state;

		if(standard == 0){
			temps.add('');	// 基準
			temps.add('');	// 状況
		}else{
			num = state;
			if(type==0){
				// 円表記
				// 基準変換
				temp = standard.format().replace(',', '');
				if(temp.lastIndexOf('000000000000',temp.length()) != -1){	// 兆
					temp = temp.Left(temp.length() - 12);
					temp = Long.valueOf(temp).format() + '兆';
				}else if(temp.lastIndexOf('00000000',temp.length()) != -1){	// 億
					temp = temp.Left(temp.length() - 8);
					temp = Long.valueOf(temp).format() + '億';
				}else if(temp.lastIndexOf('0000',temp.length()) != -1){		// 万
					temp = temp.Left(temp.length() - 4);
					temp = Long.valueOf(temp).format() + '万';
				}
				temps.add(temp + '円以上');

				// 状況変換
				//temp = (num >= 0) ? '達成' + state.format() + '円' : 'あと' + Math.abs(num).format() + '円';
				temp = (num >= 0) ? '達成' : 'あと' + Math.abs(num).format() + '円';
				temps.add(temp);

			}else if(type==2){
				// 月表記
				// 基準変換
				temps.add(String.valueOf(standard) + 'ヵ月以上');

				// 状況変換
				//temp = (num >= 0) ? '達成' + state.format() + 'ヵ月' : 'あと' + Math.abs(num).format() + 'ヵ月';
				temp = (num >= 0) ? '達成' : 'あと' + Math.abs(num).format() + 'ヵ月';
				temps.add(temp);

			}else if(type==3){
				// %表記
				// 基準変換
				temp	= round(standard, 1).replace('+ ', '');
				temps.add(temp + '%以上');

				// 状況変換
				temp	= (num >= 0) ? '達成' : round(num, 1) + '%';
				temps.add(temp);
			}
		}
		return temps;
	}
	/**
	 * 小数点変換
	 */
	public String round(Decimal num, Integer dec){
		String temp		= (num == null) ? '0' : Math.abs(num).setScale(dec, RoundingMode.HALF_UP).format().replace(',', '');
		Integer point	= temp.indexOf('.', 1);
		if(dec <= 0){
			temp	= (point == -1) ? temp: temp.Left(point - 1);
		}else{
			temp	= (point == -1) ? temp + '.' + '0'.repeat(dec) : (temp + '0'.repeat(dec)).Left(point + 1 + dec);
		}
		if(num > 0) temp = '+ ' + temp;
		if(num < 0) temp = '- ' + temp;
		return temp;
	}

}