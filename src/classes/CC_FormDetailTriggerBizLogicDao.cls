public with sharing class CC_FormDetailTriggerBizLogicDao {

	/**
	 * ParentIdからAttachmentを取得する.
	 *
	 * @param Set<Id> ParentIdの集合
	 */
	public static List<Attachment> getAttachmentsByParentIds(Set<Id> parentIds) {
		
		return
			[
				Select
					Id,
					Name,
					ParentId,
					Body,
					Description
				From
					Attachment
				Where
					ParentId IN: parentIds
			];
		
	}

	/**
	 * ID群からContentVersionを取得する.
	 *
	 * @param Set<Id> Id群
	 * @return List<ContentVersion> ContentVersionのリスト
	 */
	public static List<ContentVersion> getContentVersionsByIds(Set<Id> cvIds) {
		return
			[
				Select
					Id,
					ContentDocumentId,
					VersionData,
					FirstPublishLocationId,
					PathOnClient,
					Title
				From
					ContentVersion
				Where
					Id IN: cvIds
			];
	}

	/**
	 * グループ名からCollaborationGroupを取得する.
	 *
	 * @param String グループ名
	 * @return CollaborationGroup CollaborationGroupレコード
	 */
	public static CollaborationGroup getCollaborationGroupByName(String gName) {
		
		CollaborationGroup result = new CollaborationGroup();
		
		List<CollaborationGroup> lst =
			[
				Select
					Id,
					Name,
					CollaborationType
				From
					CollaborationGroup
				Where
					Name =: gName
			];

		if(!lst.isEmpty()) {
			result = lst.get(0);
		}
		
		return result;
	}
	
	/**
	 * IDからコールセンター帳票情報を取得する.
	 *
	 * @param Set<Id> Idの集合
	 * @return コールセンター帳票情報レコードのリスト
	 */
	public static List<CC_FormDetail__c> getFormDetailsByIds(Set<Id> ids) {
		
		return
			[
				Select
					Id,
					Name,
					CC_IsCreatedSrActivity__c,
					CC_Case__c
				From
					CC_FormDetail__c
				Where
					Id IN: ids
			];
		
	}
}