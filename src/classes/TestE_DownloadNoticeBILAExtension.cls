@isTest
private class TestE_DownloadNoticeBILAExtension {
	//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';

	//User情報
	private static User user;
	private static User thisUser =[SELECT id FROM user WHERE id = :system.userInfo.getUserId()]; //Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Account atAcc;
	private static List<Account> ayAccList;
	private static Contact atCon;
	private static E_Policy__c policy;
	private static E_CHTPF__c chtpf;
	private static List<E_BILA__c> bilas;

	/**
	 * String getTitle()
	 */
	 static testMethod void getTitle_test01(){
	 	//User
	 	createDataAccount();
	 	createUser(PF_EMPLOYEE);
	 	createDataAccessObj(User.Id,E_Const.ZIDTYPE_EP);
	 	createData(user.Id, true,0);

	 	//ページ情報
	 	PageReference pref = page.E_DownloadNoticeBILA;
	 	pref.getParameters().put('did',ahAcc.Id);
	 	Test.setCurrentPage(pref);

	 	Test.startTest();

	 	System.runAs(user){
	 		E_DownloadController controller = new E_DownloadController();
	 		E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);

	 		//assertion
	 		System.assertEquals(E_DownloadNoticeConst.E_BILANM + 'ダウンロード', extension.getTitle());	
	 	}
	 	Test.stopTest();
	 }

	 /**
	 * PageReference pageAction()
	 * 社員 / エラー　：　対象件数0件

	 static testMethod void pageAction_test02(){
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
		createData(user.Id, true, 0);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		pref.getParameters().put('did', ahAcc.Id);
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();

			// assertion
			System.assertEquals(true, controller.pageMessages.hasMessages());
		}

		Test.stopTest();
	}
	 */
	/**
	 * PageReference doDownloadCsv()
	 * 社員 / 正常
	 */
	static testMethod void doDownloadCsv_test01(){
		//User
		createDataAccount();
		createUser(PF_EMPLOYEE);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
		createData(user.Id, true, 0);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		pref.getParameters().put('did', ahAcc.Id);
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			ApexPages.currentPage().getParameters().put('csvType', '0');
			
			// 事務所選択
			extension.selOfficeList = new List<String>();
			extension.selOfficeList.add(controller.getActiveOfficeOptions()[0].getValue());
			//期間
			extension.periodValue = extension.periodOptions[0].getValue();
			extension.doDownloadCsv();

			//assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlcondi = new E_DownloadCondition.NoticeDownloadCondi();
			dlcondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
			dlcondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlcondi.officeCodes = extension.selOfficeList;
			dlCondi.csvType = '0';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AH） / 正常
	 */
	static testMethod void doDownloadCsv_test02(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			ApexPages.currentPage().getParameters().put('csvType', '1');
			
			// 事務所選択
			extension.selOfficeList = new List<String>();
			extension.selOfficeList.add(controller.getActiveOfficeOptions()[0].getValue());
			// 期間
			extension.periodValue = extension.periodOptions[0].getValue();
			extension.doDownloadCsv();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = extension.selOfficeList;
			dlCondi.csvType = '1';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AY） / 正常
	 */
	static testMethod void doDownloadCsv_test03(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AY);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			ApexPages.currentPage().getParameters().put('csvType', '0');
			
			// 期間
			extension.periodValue = extension.periodOptions[0].getValue();
			extension.doDownloadCsv();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AY;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = new List<String>{atCon.AccountId};
			dlCondi.csvType = '0';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadCsv()
	 * 代理店ユーザ（AT / 正常
	 */
	static testMethod void doDownloadCsv_test04(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AT);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			ApexPages.currentPage().getParameters().put('csvType', '0');
			
			// 期間
			extension.periodValue = extension.periodOptions[0].getValue();
			extension.doDownloadCsv();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AT;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = new List<String>{atCon.AccountId};
			dlcondi.agentCode = atCon.Id;
			dlCondi.csvType = '0';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_CSV, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadPdf()
	 * 代理店ユーザ（AH） / 正常
	 */
	static testMethod void doDownloadPdf_test01(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AH);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			
			// 事務所選択
			extension.selOfficeList = new List<String>();
			extension.selOfficeList.add(controller.getActiveOfficeOptions()[0].getValue());
			// 期間
			extension.periodValue = extension.periodOptions[1].getValue();
			extension.doDownloadPdf();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = extension.selOfficeList;
			dlCondi.csvType = '';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_PDF, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadPdf()
	 * 代理店ユーザ（AY） / 正常
	 */
	static testMethod void doDownloadPdf_test02(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AY);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			
			// 期間
			extension.periodValue = extension.periodOptions[1].getValue();
			extension.doDownloadPdf();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AY;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = new List<String>{atCon.AccountId};
			dlCondi.csvType = '';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_PDF, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadPdf()
	 * 代理店ユーザ（AT / 正常
	 */
	static testMethod void doDownloadPfd_test03(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AT);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			
			// 期間
			extension.periodValue = extension.periodOptions[1].getValue();
			extension.doDownloadPDF();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AT;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = new List<String>{atCon.AccountId};
			dlcondi.agentCode = atCon.Id;
			dlCondi.csvType = '';
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(false, results.isEmpty());
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA, results[0].FormNo__c);
			System.assertEquals(E_Const.DH_OUTPUTTYPE_PDF, results[0].outputType__c);
			System.assertEquals(dlCondi.toJSON(), results[0].COnditions__c);
			
		}

		Test.stopTest();
	}

	/**
	 * PageReference doDownloadPdf()
	 * 代理店ユーザ（AT / 期間エラー
	 */
	static testMethod void doDownloadPfd_test04(){
		//User
		createDataAccount();
		createUser(PF_PARTNER);
		createDataAccessObj(user.Id, E_Const.ZIDTYPE_AT);
		createData(user.Id, true, 3);

		//ページ情報
		PageReference pref = Page.E_DownloadNoticeBILA;
		Test.setCurrentPage(pref);

		Test.startTest();

		System.runAs(user){
			E_DownloadController controller = new E_DownloadController();
			E_DownloadNoticeBILAExtension extension = new E_DownloadNoticeBILAExtension(controller);
			extension.pageAction();
			
			// 期間
			extension.periodValue = extension.periodOptions[0].getValue();
			extension.doDownloadPDF();
			
			// assert用 Condition
			E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
			dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AT;
			dlCondi.distributorCode = ahAcc.Id;
			dlCondi.ZHEADAY = ahAcc.E_CL1PF_ZHEADAY__c;
			dlCondi.ZHEADNAME = ahAcc.Name;
			dlCondi.officeCodes = new List<String>{atCon.AccountId};
			dlcondi.agentCode = atCon.Id;
			List<String> periodList = extension.periodValue.split(',');
			//単月指定
			if(periodList.size() == 1){
				dlCondi.fromyyyyMM = extension.periodValue;
				dlCondi.toyyyyMM = extension.periodValue;
			}
			//複数月指定
			else{
				dlCondi.fromyyyyMM = periodList.get(2);
				dlCondi.toyyyyMM = periodList.get(0);
			}
			dlCondi.policyTypes = new List<String>{(String)E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI)};

			// assertion
			List<E_DownloadHistorry__c> results = [Select Id, FormNo__c, Conditions__c, outputType__c From E_DownloadHistorry__c];
			System.assertEquals(true, results.isEmpty());
			
		}

		Test.stopTest();
	}
/* Test Data **************************** */   	
	/** User作成 */
	private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
		profile p = [SELECT Id From Profile WHERE Name = :profileName];

		//Base Info
		user = new user(
			Lastname = 'test'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'test'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);

		//User
		if(profileName != PF_PARTNER){
			UserRole portaleRole = [SELECT Id FROM UserRole WHERE portalType = 'None' Limit 1];	
			user.UserRoleId = portaleRole.Id;
			insert user;

		//PartnerUser
		}else{
			user.contactId = atcon.Id;
			insert user;

			ContactShare cs = new ContactShare(
				contactId = atCon.Id
				,ContactAccessLevel = 'read'
				,UserOrGroupId = user.Id);
			insert cs;
		}
	}

	/** データアクセス系作成 */
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }
    
    /** Data作成 */
	private static void createDataAccount(){
		system.runAs(thisuser){
			// Account 代理店格
			ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
			insert ahAcc;

			// Account 事務所
			ayAccList = new List<Account>();
			for(Integer i = 1; i < 4; i++){
				Account acc = new Account(
					Name = 'office' + i
					,ParentId = ahAcc.Id
					,E_CL2PF_ZAGCYNUM__c = 'ay001'
					,E_COMM_VALIDFLAG__c = '1'
				);
				ayAccList.add(acc);
			}
			insert ayAccList;

			// Contact 募集人
			if(!ayAccList.isEmpty()){
				atCon = new Contact(LastName = 'test',AccountId = ayAccList[0].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[0].E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}
		}
	}

	/*Data作成*/
	private static void createData(Id shareUserId, Boolean isCreateOffice, Integer roopCnt){
		system.runAs(thisUser){
			//保険契約ヘッダ（個人タイプ）
			policy = new E_Policy__c();
			policy = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');

			E_Policy__Share ps = new E_Policy__Share(
				parentId = policy.Id,
				AccessLevel = 'read',
				UserOrGroupId = shareUserId);
			insert ps;

			//販売取扱者
			chtpf = new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
			insert chtpf;

			//保険料請求予告通知
			bilas = new List<E_BILA__c>();
			//対象月
			date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA);
			for(Integer i = 0; i < roopCnt; i++){
				Date d = targetDate.addMonths(i*-1);
				E_BILA__c bila = new E_BILA__c(
					ParentAccount__c = ahAcc.Id
					, Account__c = ayAccList[0].Id
					, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c
					, Contact__c = atCon.Id
					, E_Policy__c = policy.Id
					, E_CHTPF__c = chtpf.Id
					, YYYY__c = d.year()
					, MM__c = d.month()
				);
				bilas.add(bila);
			}
			insert bilas;
		}	
	}
}