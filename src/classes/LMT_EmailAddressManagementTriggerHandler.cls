/**
@version 1.0
@author PwC
LMTメール通知管理のUser1重複チェック
*/
public with sharing class LMT_EmailAddressManagementTriggerHandler {

    /**
        LMT_メール通知管理に新規レコードが作成された場合のUser1存在チェック
        @param newList
        @return なし
    */
    public void checkUser1existenceForInsert (List<LMT_EmailAddressManagement__c> newList){
        //新規User1指定ユーザ
        Set<Id> user1IdSet = New Set<Id>();
        for(LMT_EmailAddressManagement__c emanage:newList){
          user1IdSet.add(emanage.User1__c);
        }
        //既存User1指定ユーザの存在チェック
        Set<Id> existUser1Set = New Set<Id>();
        for(LMT_EmailAddressManagement__c user1List: [Select id,User1__c from LMT_EmailAddressManagement__c where User1__c in:user1IdSet]){
              existUser1Set.add(user1List.User1__c);
        }
        for(LMT_EmailAddressManagement__c emanage:newList){
            if(existUser1Set.size() > 0){
                emanage.User1__c.adderror(Label.Msg_Error_LMT_DuplicateUser1);
            }
        }
    }

    /**
        LMT_メール通知管理の既存レコードが更新された場合のUser1重複チェック
        @param oldList
        @param newList
        @return なし
    */
    public void checkUser1existenceForUpdate(List<LMT_EmailAddressManagement__c> oldList,List<LMT_EmailAddressManagement__c> newList){
        //新規User1指定ユーザ
        Set<Id> user1IdSet = New Set<Id>();
        for(LMT_EmailAddressManagement__c emanage:newList){
          user1IdSet.add(emanage.User1__c);
        }

        //既存User1指定ユーザの存在チェック
        Set<Id> existUser1Set = New Set<Id>();
        for(LMT_EmailAddressManagement__c user1List: [Select id,User1__c from LMT_EmailAddressManagement__c where User1__c in:user1IdSet]){
              existUser1Set.add(user1List.User1__c);
        }

        for(LMT_EmailAddressManagement__c emanage:newList){
            for(LMT_EmailAddressManagement__c emanageOld:oldList){
                if((emanageOld.User1__c != emanage.User1__c) && (existUser1Set.size() > 0)){
                    emanage.User1__c.adderror(Label.Msg_Error_LMT_DuplicateUser1);
                }
            }
        }
    }
}