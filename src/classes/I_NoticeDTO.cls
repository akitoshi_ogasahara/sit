public with sharing class I_NoticeDTO implements Comparable {

	// ソート実行用：項目
	public static String sortType;
	// ソート実行用：昇順 OR 降順
	public static Boolean sortIsAsc;

	// 遷移先URL
	public String destURL {get; set;}
	// 遷移先URL_Target
	public String destURLTarget {get; set;}
	// 帳票種別
	public String formType {get; set;}
	// 代理店
	public Id distributeId {get; set;}
	// 事務所
	public Id officeId {get; set;}

	/* 一覧項目 */
	// 帳票発信日
	public String transmissionDate {get; set;}
	// 契約者名
	public String owname {get; set;}
	// 団体
	public Boolean isZGRUP {get; set;}
	// 団体番号
	public String grpno {get; set;}
	// 証券番号
	public String no {get; set;}
	// 保険種類
	public String cntType {get; set;}
	// 保険金額
	public Decimal zsumins12 {get; set;}
	// 保険料
	public Decimal instamt12 {get; set;}
	// 保険料猶予期間満了日
	public String limitDt {get; set;}
	// 解約返戻金(未入金)
	public Decimal zcsvn {get; set;}
	// 解約返戻金(予告・請求)
	public Decimal zcsv12n {get; set;}
	// 代理店名
	public String distributeName {get; set;}
	// 代理店事務所名
	public String officeName {get; set;}
	// 募集人名
	public String agentName {get; set;}
	// MR名
	public String mrName {get; set;}
	// 異動内容
	public String content {get; set;}
	// 処理日
	//public String processDate {get; set;}
	// 被保険者名
	//public String insuredName {get; set;}
	//契約応答日
	public String responseDate {get;set;}
	//契約経過年数
	public String elapsedYears {get;set;}

	//再徴/併徴
	public String reason {get;set;}
	//請求区分
	public String kbilldesc {get;set;}

	/**
	 * Constructor
	 * [E_BILA__c] 保険料請求予告通知
	 */
	public I_NoticeDTO(E_BILA__c rec){
		// 共通項目値セット
		setCommonInfo(I_NoticeConst.NOTICE_TYPE_BILA, rec, rec.E_Policy__r);

		// 解約返戻金
		this.zcsv12n = I_PolicyUtil.getCancellationRefund(rec.E_Policy__r);

	}

	/**
	 * Constructor
	 * [E_BILS__c] 保険料請求通知
	 */
	public I_NoticeDTO(E_BILS__c rec){
		// 共通項目値セット
		setCommonInfo(I_NoticeConst.NOTICE_TYPE_BILS, rec, rec.E_Policy__r);

		// 解約返戻金
		this.zcsv12n = I_PolicyUtil.getCancellationRefund(rec.E_Policy__r);

		//再徴/併徴

		if(String.isNotBlank(rec.REASON03__c)){
			this.reason = rec.REASON03__c;
		}else if(String.isNotBlank(rec.REASON04__c)){
			this.reason = rec.REASON04__c;
		}else{
			this.reason = '';
		}
	}

	/**
	 * Constructor
	 * [E_NCOL__c] 保険料未入金通知
	 */
	public I_NoticeDTO(E_NCOL__c rec){
		// 共通項目値セット
		setCommonInfo(I_NoticeConst.NOTICE_TYPE_NCOL, rec, rec.E_Policy__r);

		// 団体
		this.isZGRUP = rec.IsZGRUP__c;
		// 団体番号
		this.grpno = rec.ZGRUPNUM__c;
		// 解約返戻金
		this.zcsvn = I_PolicyUtil.getCancellationRefund(rec.E_Policy__r);
		//請求区分
		this.kbilldesc = rec.KBILLDESC__c;
	}

	/**
	 * Constructor
	 * [E_AGPO__c] 異動通知
	 */
	public I_NoticeDTO(E_AGPO__c rec){
		if(rec.E_Policy__r.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI){
			this.destURL = 'IRIS_Coli?id=' + (String)rec.get('E_Policy__c');
			this.destURLTarget = '_blank';
		}else if(rec.E_Policy__r.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA && rec.E_Policy__r.SpClassification__c == E_Const.SP_CLASS_ANNUITY){
			this.destURL = 'E_Annuity?id=' + (String)rec.get('E_Policy__c');
			this.destURLTarget = '_self';
		}else if(rec.E_Policy__r.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_SPVA){
			this.destURL = 'E_Spva?id=' + (String)rec.get('E_Policy__c');
			this.destURLTarget = '_self';
		}else if(rec.E_Policy__r.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_DCOLI){
			this.destURL = 'IRIS_DColi?id=' + (String)rec.get('E_Policy__c');
			this.destURLTarget = '_blank';
		}else if(rec.E_Policy__r.recordType.DeveloperName == E_Const.POLICY_RECORDTYPE_DSPVA){
			this.destURL = 'E_DSpva?id=' + (String)rec.get('E_Policy__c');
			this.destURLTarget = '_self';
		}
		// 帳票種別
		this.formType = I_NoticeConst.NOTICE_TYPE_AGPO;
		// 帳票発信日
		Datetime dt = (Datetime)rec.get('CreatedDate');
		this.transmissionDate = dt.format('yyyy/MM/dd');
		// 異動内容
		this.content = rec.CONTENT__c;
		// 証券番号
		this.no = rec.CHDRNUM__c;
		// 契約者名
		// this.owname = rec.OWNAME__c;
		//外字対応
		this.owname = rec.ContractorName__c;
		/* SITP対応 */
//		this.cntType = rec.ZCOVRNAM__c;
		this.cntType = rec.InsuranceType__c;

		// 保険料
		this.instamt12 = rec.INSTAMT12__c;
		// 保険料猶予期間満了日
		String ldt = rec.LIMITDT__c;
		if(ldt != null && ldt.length() == 8){
			this.limitDt = ldt.substring(0,4) + '/' ;
			this.limitDt += ldt.substring(4,6) + '/' ;
			this.limitDt += ldt.substring(6,8);
		}
		// 代理店事務所
		this.officeName = rec.AccountName__c;
		// 募集人
		this.agentName = rec.ContactName__c;
		// 担当MR
		this.mrName = rec.AccountOwnerName__c;
		// 代理店格ID
		this.distributeId = (String)rec.get('ParentAccountId__c');
		// 事務所ID
		this.officeId = (String)rec.get('AccountId__c');

		//契約日
		this.responseDate = rec.E_Policy__r.COMM_OCCDATE_yyyyMMdd__c;
		//契約経過年数
		this.elapsedYears = rec.E_Policy__r.PolicyElapsedYears__c;



	}

	/**
	 * ソート
	 */
	public Integer compareTo(Object compareTo){
		I_NoticeDTO compareToDto = (I_NoticeDTO)compareTo;
		Integer result = 0;
		// 帳票発信日
		if(sortType == 'transdate'){
			result = I_Util.compareToString(transmissionDate,compareToDto.transmissionDate);
		// 契約者名
		}else if(sortType == 'owname'){
			result = I_Util.compareToString(owname,compareToDto.owname);
		// 団体番号
		}else if(sortType == 'grpno'){
			result = I_Util.compareToString(grpno,compareToDto.grpno);
		// 証券番号
		}else if(sortType == 'no'){
			result = I_Util.compareToString(no,compareToDto.no);
		// 保険種類
		}else if(sortType == 'cnttype'){
			result = I_Util.compareToString(cnttype,compareToDto.cnttype);
		// 保険料
		}else if(sortType == 'instamt12'){
			result = I_Util.compareToDecimal(instamt12,compareToDto.instamt12);
		// 保険料猶予期間満了日
		}else if(sortType == 'limitDt'){
			result = I_Util.compareToString(limitDt, compareToDto.limitDt);
		// 解約返戻金(未入金)
		}else if(sortType == 'zcsvn'){
			result = I_Util.compareToDecimal(zcsvn,compareToDto.zcsvn);
		 // 解約返戻金(予告・請求)
		}else if(sortType == 'zcsv12n'){
			result = I_Util.compareToDecimal(zcsv12n,compareToDto.zcsv12n);
		// 代理店事務所
		}else if(sortType == 'office'){
			result = I_Util.compareToString(officeName,compareToDto.officeName);
		// 募集人
		}else if(sortType == 'agent'){
			result = I_Util.compareToString(agentName,compareToDto.agentName);
		// MR
		}else if(sortType == 'mr'){
			result = I_Util.compareToString(mrName,compareToDto.mrName);
		// 異動内容
		}else if(sortType == 'content'){
			result = I_Util.compareToString(content, compareToDto.content);
		// 契約応答日
		}else if(sortType == 'response'){
			result = I_Util.compareToString(responseDate,compareToDto.responseDate);
		}else if(sortType == 'elapsed'){
			String elapsedyear = elapsedYears.substringBefore('年');
			String elapsedmonth = elapsedYears.substringBefore('ヶ月');
			elapsedmonth = elapsedmonth.substringAfter('年');
			String cmpElapsedyear = compareToDto.elapsedYears.substringBefore('年');
			String cmpElapsedmonth = compareToDto.elapsedYears.substringBefore('ヶ月');
			cmpElapsedmonth = cmpElapsedmonth.substringAfter('年');
			if(elapsedyear.length() == 1){
				elapsedyear = '0' + elapsedyear;
			}
			if(elapsedmonth.length() == 1){
				elapsedmonth = '0' + elapsedmonth;
			}
			if(cmpElapsedyear.length() == 1){
				cmpElapsedyear = '0' + cmpElapsedyear;
			}
			if(cmpElapsedmonth.length() == 1){
				cmpElapsedmonth = '0' + cmpElapsedmonth;
			}

			result = I_Util.compareToDecimal(Decimal.valueOf(elapsedyear+elapsedmonth), Decimal.valueOf(cmpElapsedyear+cmpElapsedmonth));
/*		// 処理日
		}else if(sortType == 'processDate'){
			result = I_Util.compareToString(processDate, compareToDto.processDate);
		// 被保険者
		}else if(sortType == 'insuredName'){
			result = I_Util.compareToString(insuredName, compareToDto.insuredName);*/
		}else if(sortType == 'reason'){
			result = I_Util.compareToString(reason,compareToDto.reason);
		}else if(sortType == 'kbilldesc'){
			result = I_Util.compareToString(kbilldesc,compareToDto.kbilldesc);
		}
		return sortIsAsc ? result : result * (-1);
	}

	/**
	 * 共通項目値セット
	 * @param String 帳票種別
	 * @param SObject 帳票オブジェクト
	 * @return
	 */
	private void setCommonInfo(String ftype, SObject rec, E_Policy__c policy){
		// 遷移先URL
		this.destURL = 'IRIS_Coli?id=' + (String)rec.get('E_Policy__c');
		this.destURLTarget = '_blank';
		// 帳票種別
		this.formType = ftype;
		// 帳票発信日
		Datetime dt = (Datetime)rec.get('CreatedDate');
		this.transmissionDate = dt.format('yyyy/MM/dd');
		// 契約者名
		//this.owname = (String)rec.get('OWNAME__c');
		//外字対応
		this.owname = (String)rec.get('ContractorName__c');
		// 証券番号
		this.no = (String)rec.get('CHDRNUM__c');
		// 保険種類
		/* SITP対応 */
//		this.cntType = (String)rec.get('ZCOVRNAM__c');
		this.cntType = (String)rec.get('InsuranceType__c');
		// 保険料
		this.instamt12 = (Decimal)rec.get('INSTAMT12__c');
		// 保険金額
		this.zsumins12 = (Decimal)rec.get('ZSUMINS12N__c');
		// 代理店格ID
		this.distributeId = (String)rec.get('ParentAccountId__c');
		// 事務所ID
		this.officeId = (String)rec.get('AccountId__c');
		// 代理店格名
		this.distributeName = (String)rec.get('ParentAccountName__c');
		// 担当MR名
		this.mrName = (String)rec.get('AccountOwnerName__c');
		/* 主従の出し分け対応 */
		// 代理店事務所名
		this.officeName = (String)rec.get('AccountName__c');
		// 募集人名
		this.agentName = (String)rec.get('ContactName__c');
		//応答日
		this.responseDate = (String)policy.get('COMM_OCCDATE_yyyyMMdd__c');
		//契約経過年数
		this.elapsedYears = (String)policy.get('PolicyElapsedYears__c');
/*		// 共同募集
		if(hasSubAgent(policy)){
			// 事務所名
			this.officeName = I_NoticeConst.VIEW_MAIN_SUB_AGENT;
			// 募集人名
			this.agentName = I_NoticeConst.VIEW_MAIN_SUB_AGENT;
		}else{
			// 事務所名
			this.officeName = (String)rec.get('AccountName__c');
			// 募集人名
			this.agentName = (String)rec.get('ContactName__c');
		}*/
	}

	/**
	 * 共同募集人判定
	 */
	/*private Boolean hasSubAgent(E_Policy__c policy){
		// 共同募集（主募集人と従募集人が存在する場合に共同募集とする）
		if(policy.MainAgent__c != null && policy.SubAgent__c != null){
			return true;
		// それ以外
		}else{
			return false;
		}
	}*/
}