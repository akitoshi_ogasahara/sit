global with sharing class E_ProcessHistoryController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_ProcessHistoryExtender getExtender() {return (E_ProcessHistoryExtender)extender;}
	
	
	public insuredTab insuredTab {get; private set;}
	public inveHistoryTab inveHistoryTab {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_ProcessHistoryController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','{!NOT(Extender.isCustomerWay)}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1240');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1240',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,processhistory_contract');
        tmpPropMap.put('isHideMenu','{!NOT(Extender.isCustomerWay)}');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','123');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_ProcessHistory');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1246');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1246',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!insuredTab_item.record.TRANDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1041');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1041',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!inveHistoryTab_item.record.ZPRMSDTE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1183');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1183',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS&&Extender.isCustomerWay}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1239');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1239',tmpPropMap);


		SObjectField f;

		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_PTNPF__c.fields.znameformula__c;
		f = E_Policy__c.fields.InsuredName__c;
		f = E_PTNPF__c.fields.ZTRANDSC__c;
		f = E_ITHPF__c.fields.ZCLNTCDE_LINK__c;
		f = E_ITTPF__c.fields.ZBRNDNMS__c;
		f = E_ITTPF__c.fields.ZMFTRAND__c;
		f = E_ITTPF__c.fields.ZQUANT__c;
		f = E_ITTPF__c.fields.ZBLNCTXY__c;
		f = E_PTNPF__c.fields.TRANDATE__c;
		f = E_PTNPF__c.fields.CHDRNUM__c;
		f = E_PTNPF__c.fields.TRANNO__c;
		f = E_ITTPF__c.fields.ZPRMSDTE__c;
		f = E_ITTPF__c.fields.ZCLNTCDE__c;
		f = E_ITTPF__c.fields.ZBRNDCDE__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			insuredTab = new insuredTab(new List<E_PTNPF__c>(), new List<insuredTabItem>(), new List<E_PTNPF__c>(), null);
			listItemHolders.put('insuredTab', insuredTab);
			query = new SkyEditor2.Query('E_PTNPF__c');
			query.addFieldAsOutput('E_Policy__r.COMM_CHDRNUM__c');
			query.addFieldAsOutput('znameformula__c');
			query.addFieldAsOutput('E_Policy__r.InsuredName__c');
			query.addFieldAsOutput('ZTRANDSC__c');
			query.addFieldAsOutput('E_Policy__c');
			query.addFieldAsOutput('E_Policy__r.Contractor__c');
			query.addFieldAsOutput('TRANDATE__c');
			query.addFieldAsOutput('TRANNO__c');
			query.addFieldAsOutput('E_Policy__r.COMM_CHDRNUM_LINK__c');
			insuredTab.queryRelatedEvent = False;
			query.limitRecords(100);
			queryMap.put('insuredTab', query);
			
			inveHistoryTab = new inveHistoryTab(new List<E_ITTPF__c>(), new List<inveHistoryTabItem>(), new List<E_ITTPF__c>(), null);
			listItemHolders.put('inveHistoryTab', inveHistoryTab);
			query = new SkyEditor2.Query('E_ITTPF__c');
			query.addFieldAsOutput('E_ITHPF__r.ZCLNTCDE_LINK__c');
			query.addFieldAsOutput('ZBRNDNMS__c');
			query.addFieldAsOutput('ZMFTRAND__c');
			query.addFieldAsOutput('ZQUANT__c');
			query.addFieldAsOutput('ZBLNCTXY__c');
			query.addFieldAsOutput('promiseDate__c');
			query.addFieldAsOutput('ZPRMSDTE__c');
			query.addFieldAsOutput('ZBRNDCDE__c');
			query.addFieldAsOutput('ZCLNTCDE__c');
			inveHistoryTab.queryRelatedEvent = False;
			query.limitRecords(100);
			queryMap.put('inveHistoryTab', query);
			
			
			SkyEditor2.Query insuredTabQuery = queryMap.get('insuredTab');
			insuredTabQuery.addSort('TRANDATE__c',False,True).addSort('CHDRNUM__c',True,True).addSort('TRANNO__c',False,True);
			SkyEditor2.Query inveHistoryTabQuery = queryMap.get('inveHistoryTab');
			inveHistoryTabQuery.addSort('ZPRMSDTE__c',False,True).addSort('ZCLNTCDE__c',False,True).addSort('ZBRNDCDE__c',True,True);
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_ProcessHistoryExtender(this);
			init();
			
			insuredTab.extender = this.extender;
			inveHistoryTab.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class insuredTabItem extends SkyEditor2.ListItem {
		public E_PTNPF__c record{get; private set;}
		@TestVisible
		insuredTabItem(insuredTab holder, E_PTNPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class insuredTab extends SkyEditor2.ListItemHolder {
		public List<insuredTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			insuredTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<insuredTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new insuredTabItem(this, (E_PTNPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class inveHistoryTabItem extends SkyEditor2.ListItem {
		public E_ITTPF__c record{get; private set;}
		@TestVisible
		inveHistoryTabItem(inveHistoryTab holder, E_ITTPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class inveHistoryTab extends SkyEditor2.ListItemHolder {
		public List<inveHistoryTabItem> items{get; private set;}
		global override Boolean isRelationalTable() {
		return false;
		}
		@TestVisible
			inveHistoryTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<inveHistoryTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new inveHistoryTabItem(this, (E_ITTPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}