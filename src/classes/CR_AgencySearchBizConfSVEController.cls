global with sharing class CR_AgencySearchBizConfSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public CR_Agency__c record{get;set;}	
			
	public CR_AgencySearchBizConfExtender getExtender() {return (CR_AgencySearchBizConfExtender)extender;}
	
		public resultTable resultTable {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component10_val {get;set;}	
		public SkyEditor2.TextHolder Component10_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c largeAgencyCondi_val {get;set;}	
		public SkyEditor2.TextHolder largeAgencyCondi_op{get;set;}	
			
	public String recordTypeRecordsJSON_CR_Agency_c {get; private set;}
	public String defaultRecordTypeId_CR_Agency_c {get; private set;}
	public String metadataJSON_CR_Agency_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public CR_AgencySearchBizConfSVEController(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = CR_Agency__c.fields.AgencyCode__c;
		f = CR_Agency__c.fields.AgencyNameKana__c;
		f = CR_Agency__c.fields.LargeAgency__c;
		f = CR_Agency__c.fields.AgencyName__c;
		f = CR_Agency__c.fields.MRName__c;
		f = CR_Agency__c.fields.LastRequestReport__c;
		f = CR_Agency__c.fields.LastRequestBook__c;
		f = CR_Agency__c.fields.BusinessStatus__c;
		f = CR_Agency__c.fields.RecordTypeId;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = CR_Agency__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder('eq');	
					
				Component10_val = new SkyEditor2__SkyEditorDummy__c();	
				Component10_op = new SkyEditor2.TextHolder('co');	
					
				largeAgencyCondi_val = new SkyEditor2__SkyEditorDummy__c();	
				largeAgencyCondi_op = new SkyEditor2.TextHolder('eq');	
					
				queryMap.put(	
					'resultTable',	
					new SkyEditor2.Query('CR_Agency__c')
						.addFieldAsOutput('AgencyCode__c')
						.addFieldAsOutput('AgencyName__c')
						.addFieldAsOutput('AgencyNameKana__c')
						.addFieldAsOutput('MRName__c')
						.addFieldAsOutput('LastRequestReport__c')
						.addFieldAsOutput('LastRequestBook__c')
						.addFieldAsOutput('BusinessStatus__c')
						.addFieldAsOutput('LargeAgency__c')
						.addFieldAsOutput('RecordTypeId')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'AgencyCode__c', Component8_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component10_val, 'SkyEditor2__Text__c', 'AgencyNameKana__c', Component10_op, true, 0, true ))
						.addListener(new SkyEditor2.QueryWhereRegister(largeAgencyCondi_val, 'SkyEditor2__Checkbox__c', 'LargeAgency__c', largeAgencyCondi_op, true, 0, false ))
						.addWhere(' ( RecordType.DeveloperName = \'CR_Biz\')')
.addSort('AgencyCode__c',True,False)
				);	
					
					resultTable = new resultTable(new List<CR_Agency__c>(), new List<resultTableItem>(), new List<CR_Agency__c>(), null);
				resultTable.ignoredOnSave = true;
				listItemHolders.put('resultTable', resultTable);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(CR_Agency__c.SObjectType, true);
					
					
			p_showHeader = false;
			p_sidebar = false;
			extender = new CR_AgencySearchBizConfExtender(this);
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyCode_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyCode__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_AgencyNameKana_c() { 
			return getOperatorOptions('CR_Agency__c', 'AgencyNameKana__c');	
		}	
		public List<SelectOption> getOperatorOptions_CR_Agency_c_LargeAgency_c() { 
			return getOperatorOptions('CR_Agency__c', 'LargeAgency__c');	
		}	
			
			
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public CR_Agency__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, CR_Agency__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (CR_Agency__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}