@isTest
private class TestSC_BizDataSyncBatchOperation {
	// getQueryLocator
	private static testMethod void BizDataSyncBatchOperationTest001(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		Test.startTest();
			SC_BizDataSyncBatchOperation op = new SC_BizDataSyncBatchOperation();
			Database.QueryLocator ql = op.getQueryLocator();
		Test.stopTest();

		List<SC_Office__c> resultList = Database.query(ql.getQuery());
		System.assertEquals(resultList.size(), 1);
	}

	// execute
	private static testMethod void BizDataSyncBatchOperationTest002(){
		// テストデータ作成
		Account acc = TestSC_TestUtil.createAccount(true, null);
		SC_Office__c  record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);

		List<SObject> recList = new List<SObject>();
		recList.add(record);

		Test.startTest();
		SC_BizDataSyncBatchOperation op = new SC_BizDataSyncBatchOperation();
		op.execute(recList);
		List<String> resultErrs = op.getErrMsgs();
		Test.stopTest();

		System.assert(resultErrs.isEmpty());
	}

	// execute
	private static testMethod void BizDataSyncBatchOperationTest003(){
		// テストデータ作成
		User u = TestSC_TestUtil.createUser(true, 'MR', 'ＭＲ');
		u.MRCD__c = 'MR00001';
		update u;

        Account pa = new Account(
              Name = 'TestParentAccountName'
            , E_CL1PF_ZHEADAY__c  = '12345'
            , E_CL1PF_CLNTNUM__c = '99999'
        );
        insert pa;

		Account acc = TestSC_TestUtil.createAccount(true, pa);
		acc.E_CL2PF_BRANCH__c = 'A5';
		acc.ParentId = pa.Id;
		acc.OwnerId = u.Id;
		update acc;

		SC_Office__c  record = TestSC_TestUtil.createSCOffice(true, acc.Id, null);
		record.Account__c = acc.Id;
		update record;

		record = [Select Id, Name, OwnerId, isHeadSection__c, Account__r.E_ParentZHEADAY__c, Account__r.E_CL2PF_ZAGCYNUM__c, Account__r.E_CL2PF_BRANCH__c
							,Account__r.ZMRCODE__c, Account__r.Parent.E_CL1PF_CLNTNUM__c, DomainInternet__c, DomainGw__c, IsIris__c, FiscalYear__c
					From SC_Office__c
					Where Id = :record.Id];

		List<SObject> recList = new List<SObject>();
		recList.add(record);

		List<String> resultErrs = new List<String>();
		Test.startTest();
		SC_BizDataSyncBatchOperation op = new SC_BizDataSyncBatchOperation();
		try{
			op.execute(recList);
		}catch(Exception e){
			resultErrs = op.getErrMsgs();
		}
		Test.stopTest();
		
		System.assert(!resultErrs.isEmpty());
	}
}