@isTest
private class TestI_UnitController {
	
	private static testMethod void pageAction_test001(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;

		System.runAs(actUser){
			Test.startTest();
			I_UnitController controller = new I_UnitController();
			controller.pageAction();

			Test.stopTest();

			System.assert(!controller.UnitRows.isEmpty());
			System.assertEquals(controller.getListMaxRows(),I_Const.LIST_MAX_ROWS);
		}
	}

	private static testMethod void sortRows_test001(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;
		E_Unit__c unit2 = new E_Unit__c(Area__c = area.id,BRANCH__c = '89',Name = 'テスト営業部2');
		insert unit2;

		PageReference pr = Page.IRIS_Unit;
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			Test.startTest();
			I_UnitController controller = new I_UnitController();
			controller.pageAction();

			pr.getParameters().put('st', I_UnitController.URL_PARAM_FILTER_ID);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_UnitController.SORT_TYPE_UNITNAME);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_UnitController.SORT_TYPE_UNITAREA);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_UnitController.SORT_TYPE_UNITADDRESS);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_UnitController.SORT_TYPE_PNUM);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_UnitController.SORT_TYPE_DORDER);
			controller.sortIsAsc = true;
			controller.sortRows();

			Test.stopTest();

		}
	}
	/**
	 * moveToNewPolicyList
	 */
	private static testMethod void moveToNewPolicyList_test001(){

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;
		
		// ページ表示
		PageReference pr = Page.IRIS_Unit;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_UnitController controller = new I_UnitController();
			controller.pageAction();

			pr.getParameters().put('id', String.valueOf(unit.id));
			controller.moveToNewPolicyList();
			Test.stopTest();
			// Assertion
			PageReference newPage = Page.IRIS_NewPolicy;
			System.assertEquals(controller.moveToNewPolicyList().getUrl(), newPage.getUrl());
		}
	}
	/**
	 * moveToOwnerList
	 */
	private static testMethod void moveToOwnerList_test001(){

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;

		// IRISメニュー作成
		I_MenuMaster__c mn = createIRISMenu();

		// ページ表示
		PageReference pr = Page.IRIS_Unit;
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_UnitController controller = new I_UnitController();
			controller.pageAction();

			pr.getParameters().put('id', String.valueOf(unit.id));
			controller.moveToOwnerList();
			Test.stopTest();
			// Assertion
			PageReference newPage = Page.IRIS_Owner;
			System.assertEquals(controller.moveToOwnerList().getUrl(), newPage.getUrl() + '?irismn=' + mn.Id);
		}
	}
	//irisメニューー
	static I_MenuMaster__c createIRISMenu(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		I_MenuMaster__c mn = new I_MenuMaster__c();
		system.runAs(thisUser){
			mn.MenuKey__c = 'ownerList';
			mn.MainCategory__c = '2.保有契約';
			insert mn;
		}
		return mn;
	}
}