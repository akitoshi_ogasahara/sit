@isTest
private class TestE_SelfRegistrationController {

	//メッセージマスタ作成
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			createMenuData();
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//メニューマスタ作成
	static void createMenuData(){
		E_MenuMaster__c mn = new E_MenuMaster__c();
		mn.MenuMasterKey__c = 'internet_service_policy_agency';
		insert mn;
	}

	//メッセージマスタ作成
	static void createIRISCMS(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			I_PageMaster__c pm = createIRISPageData();
			I_ContentMaster__c cms = new I_ContentMaster__c();
			cms.Page__c = pm.Id;
			cms.Content__c = '内容';
			insert cms;
		}
	}

	//メニューマスタ作成
	static I_PageMaster__c createIRISPageData(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'SelfRegistConfirm';
		insert pm;
		return pm;
	}

	//募集人作成
	static Contact createContactData(String flag){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.E_CL3PF_AGNTNUM__c = 'test01';
			con.E_CLTPF_DOB__c = '20161017';
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.E_CL3PF_VALIDFLAG__c = flag;
			con.AccountId = accid;
			con.E_CL3PF_KMOFCODE__c = 'test01';
			con.E_CLTPF_CLNTNUM__c = 'client01';
			con.E_CL3PF_CLNTNUM__c = 'client01';
			insert con;
		}

		return con = [select Name,Id,FirstName,LastName from Contact where E_CL3PF_AGNTNUM__c = 'test01'];
	}

	//募集人(顧客でない)作成
	static void createNotClientContactData(String flag){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.E_CL3PF_AGNTNUM__c = 'test02';
			con.E_CLTPF_DOB__c = '20161017';
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.E_CL3PF_VALIDFLAG__c = flag;
			con.AccountId = accid;
			con.E_CL3PF_KMOFCODE__c = 'test02';
			con.E_CL3PF_CLNTNUM__c = 'client01';
			insert con;
		}
	}

	//ID管理作成
	static E_IDCPF__c createIDManage(String sta){
		 E_IDCPF__c ic = new E_IDCPF__c();
		 ic.ZWEBID__c = '1234567890';
		 ic.ZSTATUS01__c = sta;
		 ic.ZIDOWNER__c = 'AG' + 'test01';
		 insert ic;

		 return ic;
	}

	//ユーザ作成
	static User createUser(Id id){
		User us = new User();
		us.Username = 'test@test.com' + System.Label.E_USERNAME_SUFFIX;
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.ContactId = Id;
		us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
		us.IsActive = true;
		insert us;
		return us;
	}

	//無効化ユーザ作成
	static User createNotActiveUser(Id id){
		User us = new User();
		us.Username = 'test@test.com' + System.Label.E_USERNAME_SUFFIX;
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.ContactId = Id;
		us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
		us.IsActive = false;
		insert us;
		return us;
	}

	//親取引先作成
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.E_CL1PF_KCHANNEL__c = 'LDA';
		acc.Optout_SelfRegist__c = false;
		insert acc;

		return acc;
	}

	//取引先作成
	static Id createAccount(){
		Account pac = createParentAccount();
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.ZAUTOTRF__c = ' ';
		acc.ParentId = pac.Id;
		insert acc;

		return acc.Id;
	}

	//System.runAs用ユーザ
	static User runAsUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		return thisUser;
	}

	
	/**
	 * 発行画面入力チェック
	 */
	//募集人コードMOFコード共に入力されていないとき
	@isTest static void inputCheck_ContactCode() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|001','募集人コードまたはMOFコードは必須です。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();

		cic.year = '2016';
		cic.month = '10'; 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'募集人コードまたはMOFコードは必須です。');
		Test.stopTest();
	}
	
	//生年月日が入力されていないとき
	@isTest static void inputCheck_Birth1() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|002','生年月日は必須です。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();

		cic.agOrMofcode = 'test01';
		cic.eMail = 'test@test.com';
		cic.year = '';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'生年月日は必須です。');
		Test.stopTest();
	}

	//生年月日の入力形式が間違っているとき
	@isTest static void inputCheck_Birth2() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|003','生年月日はyyyyMMdd 形式で入力して下さい。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test01';
		cic.year = '20160';
		cic.month = '10'; 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'生年月日はyyyyMMdd 形式で入力して下さい。');
		Test.stopTest();
	}

	//メールアドレスが入力されていないとき
	@isTest static void inputCheck_Mail() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('MAC|001','新規電子メールアドレスを入力して下さい。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test01';
		cic.year = '2016';
		cic.month = '10'; 
		cic.day = '17';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'新規電子メールアドレスを入力して下さい。');
		Test.stopTest();
	}

	//メールアドレスの入力形式が間違っているとき
	@isTest static void inputCheck_Mail4() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('MAC|005','電子メールアドレスが正しくありません。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test01';
		cic.year = '2016';
		cic.month = '10'; 
		cic.day = '17';
		cic.eMail = 'test@test';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'電子メールアドレスが正しくありません。');
		Test.stopTest();
	}

	/**
	 * 発行画面レコードチェック
	 */
	//入力された募集人が確認できないとき
	@isTest static void recordCheck_Contact() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|004','募集人情報が確認できませんでした。');

		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test00';
		cic.year = '2016';
		cic.month = '10'; 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.getOptions();
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'募集人情報が確認できませんでした。');
		Test.stopTest();
	}

	//入力された募集人が顧客マスタを持っていないとき
	@isTest static void recordCheck_Client() {
		//テストデータ作成
		createIRISCMS();
		createNotClientContactData('1');
		createMessageData('CID|004','募集人情報が確認できませんでした。');

		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test02';
		cic.year = '2016';
		cic.month = '10'; 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.getOptions();
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'募集人情報が確認できませんでした。');
		Test.stopTest();
	}

	//ID管理が既に登録済みである場合
	@isTest static void recordCheck_ID() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//テストデータ作成
			createIRISCMS();
			createMessageData('CID|006','すでに登録済みです。');
			Contact con = createContactData('1');
			User uc = createUser(con.Id);
			E_IDCPF__c idcp = createIDManage('1');
			idcp.User__c = uc.Id;
			update idcp;
			//テストスタート
			Test.startTest();
			E_SelfRegistrationController cic = new E_SelfRegistrationController();
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10'; 
			cic.day = '17';
			cic.eMail = 'test@test.com';
			cic.getOptions();
			cic.selection = 'mof';
			cic.doCreateId();
			System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'すでに登録済みです。');
		}
		Test.stopTest();
	}

	//ユーザは存在するが、ＩＤ管理は存在しないとき
	@isTest static void haveUserNotHaveIDRequest() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('MAB|002','メール文言');
		Contact con = createContactData('1');
		createUser(con.id);
		User thisUser = runAsUser();

		System.runAs(thisUser){
			Test.startTest();
			E_SelfRegistrationController cic = new E_SelfRegistrationController();
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10'; 
			cic.day = '17';
			cic.eMail = 'test2@test.com';
			cic.getOptions();
			PageReference pageref = cic.doCreateId();
			System.assertEquals(pageref,null);
			System.assertEquals(cic.selfRegFlag,true);
			Test.stopTest();
		}
	}

	//入力されたメールアドレスがＩＤとして使用できない場合
	@isTest static void mailCheck() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//テストデータ作成
			createIRISCMS();
			createMessageData('CID|008','ご指定のメールアドレスはIDとして利用できません。');
			Contact con = createContactData('1');
			Id accid = createAccount();
			Contact con2 = new Contact(
				E_CL3PF_AGNTNUM__c = 'test02',
				E_CLTPF_DOB__c = '20161017',
				FirstName = '花子',
				LastName = 'テストテスト',
				AccountId = accid,
				E_CL3PF_KMOFCODE__c = 'test02'
			);
			insert con2;
			createUser(con2.id);
			//テストスタート
			Test.startTest();
			E_SelfRegistrationController cic = new E_SelfRegistrationController();
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10'; 
			cic.day = '17';
			cic.eMail = 'test@test.com';
			cic.getOptions();
			cic.doCreateId();
			System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'ご指定のメールアドレスはIDとして利用できません。');
			Test.stopTest();
		}
	}

	//発行画面でエラーがなく、募集人にuserが作成されていないとき
	@isTest static void issueComp() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('TTT|001','テスト');
		Contact con = createContactData('1');

		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test01';
		cic.year = '2016';
		cic.month = '10'; 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.getOptions();
		PageReference pageref = cic.doCreateId();
		System.assertEquals(cic.selfRegFlag,false);
		Test.stopTest();
	}

	//ID管理を持つ無効化されたユーザが存在するとき
	@isTest static void haveNotActiveUserHaveIDCPF(){
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|006','すでに登録済みです。');
		Contact con = createContactData('1');
		User testUser = createNotActiveUser(con.id);
		E_IDCPF__c idcp = createIDManage('1');
		idcp.User__c = testUser.Id;
		update idcp;
		User thisUser = runAsUser();

		System.runAs(thisUser){
			Test.startTest();
			E_SelfRegistrationController cic = new E_SelfRegistrationController();
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10';
			cic.day = '17';
			cic.eMail = 'test2@test.com';
			cic.getOptions();
			PageReference pageref = cic.doCreateId();
			System.assertEquals(pageref,null);
			System.assertEquals(cic.selfRegFlag,true);
			System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary, 'すでに登録済みです。');
			Test.stopTest();
		}
	}

	//ID管理を持たない無効化されたユーザが存在するとき
	@isTest static void haveNotActiveUserNotHaveIDCPF(){
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|006','すでに登録済みです。');
		Contact con = createContactData('1');
		User testUser = createNotActiveUser(con.id);
		User thisUser = runAsUser();

		System.runAs(thisUser){
			Test.startTest();
			E_SelfRegistrationController cic = new E_SelfRegistrationController();
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10';
			cic.day = '17';
			cic.eMail = 'test2@test.com';
			cic.getOptions();
			PageReference pageref = cic.doCreateId();
			System.assertEquals(pageref,null);
			System.assertEquals(cic.selfRegFlag,true);
			System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary, 'すでに登録済みです。');
			Test.stopTest();
		}
	}

	//キャンセルボタンが押されたとき
	@isTest static void issueCancel() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('TTT|001','テスト');

		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		PageReference pageref = cic.doCansel();
		System.assertEquals(pageref.getUrl(),page.E_LoginAgent.getUrl());
		Test.stopTest();
	}

	/**
	 * 確認画面ボタン
	 */
	//情報を修正したい方はこちらリンクを押された場合
	@isTest static void confirmIssueLink() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('TTT|001','テスト');

		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		PageReference pageref = cic.doBackFromConfirm();
		System.assertEquals(cic.selfRegFlag,true);
		Test.stopTest();
	}

	//同意するチェックがついていない場合
	@isTest static void confirmCheck() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|011','利用規定、個人情報の取扱いに同意した後、ボタンを押してください。');
		Contact con = createContactData('1');
		PageReference pageref;

		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10'; 
			cic.day = '17';
			cic.eMail = 'test@test.com';
			cic.getOptions();
			cic.doCreateId();
			pageref = cic.doConfirm();
		}
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'利用規定、個人情報の取扱いに同意した後、ボタンを押してください。');
		Test.stopTest();
	}

	/**
	 * 確認画面ユーザ作成
	 */
	//ユーザが作成できた場合
	@isTest static void userCreateSuccess() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('MAB|002','メール文言');
		Contact con = createContactData('1');
		PageReference pageref;
		E_SelfRegistrationController cic = new E_SelfRegistrationController();

		Test.startTest();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10'; 
			cic.day = '17';
			cic.eMail = 'test@test.com';
			cic.infoCheck = true;
			cic.ruleCheck = true;
			cic.perCheck = true;
			cic.getOptions();
			cic.doCreateId();
			pageref = cic.doConfirm();
		}
		System.assertEquals(pageref,null);
		System.assertEquals(cic.compFlag,true);
		Test.stopTest();
	}

	//ユーザが作成されていた場合エラー
	@isTest static void userCreateFail() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|010','ユーザ作成に失敗しました。');
		Contact con = createContactData('1');
		PageReference pageref;

		Test.startTest();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_SelfRegistrationController cic = new E_SelfRegistrationController();
			cic.agOrMofcode = 'test01';
			cic.year = '2016';
			cic.month = '10'; 
			cic.day = '17';
			cic.eMail = 'test@test.com';
			cic.getOptions();
			cic.infoCheck = true;
			cic.ruleCheck = true;
			cic.perCheck = true;
			cic.doCreateId();
			createUser(con.Id);
			pageref = cic.doConfirm();
			System.assertEquals(pageref,null);
		}
		Test.stopTest();
	}


	//確認用メールアドレスが入力されていないとき
	/*@isTest static void inputCheck_Mail2() {
		//テストデータ作成
		createMessageData('MAC|002','新規電子メールアドレス（確認）を入力して下さい。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test01';
		cic.year = '2016';
		cic.month = '10' 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'新規電子メールアドレス（確認）を入力して下さい。');
		Test.stopTest();
	}*/

	//メールアドレスと確認用メールアドレスが異なっているとき
	/*@isTest static void inputCheck_Mail3() {
		//テストデータ作成
		createMessageData('MAC|003','新規電子メールアドレスと新規電子メールアドレス（確認）が相違しております。');
		
		Test.startTest();
		E_SelfRegistrationController cic = new E_SelfRegistrationController();
		cic.agOrMofcode = 'test01';
		cic.year = '2016';
		cic.month = '10' 
		cic.day = '17';
		cic.eMail = 'test@test.com';
		cic.eMail2 = 'aaaaa@test.com';
		cic.doCreateId();
		System.assertEquals(cic.pageMessages.getErrorMessages()[0].summary,'新規電子メールアドレスと新規電子メールアドレス（確認）が相違しております。');
		Test.stopTest();
	}*/

}