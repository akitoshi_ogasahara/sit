/*
 * CMN_VlocityTriggerOnBatchTest
 * Test class of CMN_VlocityTriggerOnBatch, CMN_VlocityTriggerOnBatchSched
 * created  : Accenture 2018/8/9
 * modified :
 */

@isTest
private class CMN_VlocityTriggerOnBatchTest{

	private static User testSysAdminUser = new User(Id = UserInfo.getUserId());

	/**
	 * Prepare test data
	 */
	@testSetup static void prepareTestData(){
		System.runAs ( testSysAdminUser ) {
			vlocity_ins__TriggerSetup__c ts = new vlocity_ins__TriggerSetup__c();
			ts.Name = 'AllTriggers';
			ts.vlocity_ins__IsTriggerOn__c = false;
			Insert ts;

			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '代理店' LIMIT 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			account.E_UpsertKey__c = '99999999999';
			Insert account;

			List<CronTrigger> ct = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'CMN_VlocityTriggerOnBatch'];
			if(ct.size() != 0){
				System.abortJob(ct[0].Id);
			}
		}
	}

	/**
	 * Batch Execute
	 */
	static testMethod void vlocityTriggerOnBatchTestExecute01() {
		System.runAs ( testSysAdminUser ) {
			Test.startTest();
			CMN_VlocityTriggerOnBatchSched.getCronExpression('0 0,30 0-23 * * ? *');
			CMN_VlocityTriggerOnBatchSched.setBatchSchedule();
			Test.stopTest();
		}
	}

}