global class NetworkAccess implements Process.Plugin
{    
    global Process.PluginResult invoke(Process.PluginRequest request)
    {   
        Map<String, Object> result = new Map<String, Object>();  
        boolean isTrustedIP = false;
        
        String sourceIp = (String)request.inputParameters.get('IP');
        String ChatterFreeUserProfileId=[SELECT Id FROM Profile where Name='MR'].Id;
        //IP not provided so get from session 
        if(sourceIP == null || sourceIP == '')
        {
            try 
            {
                Map<String,String> sessionAttributes = Auth.SessionManagement.getCurrentSession();
                sourceIP = sessionAttributes.get('SourceIp');
            }
            catch(Exception e)
            {
                result.put('IsTrusted', false);
        
                return new Process.PluginResult(result);
            }
        }
        
        system.debug('SourceIP: ' + sourceIp);
        
        if(sourceIp != null)
            isTrustedIP = Auth.SessionManagement.isIpAllowedForProfile(ChatterFreeUserProfileId,sourceIp);//.inOrgNetworkRange(sourceIP);
         
        result.put('IsTrusted', isTrustedIP);
        
        return new Process.PluginResult(result);
    }

    global Process.PluginDescribeResult describe()
    {
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.description='This plug-in verifies if the input IP address is in your organization trusted IP range';
        result.tag='Identity';
        
        result.inputParameters = new List<Process.PluginDescribeResult.InputParameter> {
            new Process.PluginDescribeResult.InputParameter('IP', Process.PluginDescribeResult.ParameterType.STRING, true)
        };
        
        result.outputParameters = new List<Process.PluginDescribeResult.OutputParameter> {
            new Process.PluginDescribeResult.OutputParameter('IsTrusted',
                Process.PluginDescribeResult.ParameterType.Boolean)
        };
           
        return result;
    }
}