@isTest
private class TestSC_SelfComplianceSubmit_Iris {
	
	@isTest static void testCommonPageAction01() {
		PageReference pageRef = Page.IRIS_SCSelfComplianceSubmit;
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfComplianceSubmit_Iris.createContact(true, accAgency.Id, actUserLastName);
		SC_Office__c office = createOffice(accAgency);
		SC_SelfCompliance__c scComp = createSelfComp(office);
		createPage('2018');
		pageRef.getParameters().put('id',scComp.id);
		pageRef.getParameters().put('type',SC_Const.SC_COMP_TYPE_COMMON);
		pageRef.getParameters().put('isSingle','true');
		pageRef.getParameters().put('isOldIE','false');
		pageRef.getParameters().put('year','2018');

		Test.setCurrentPage(pageRef);
		SC_SelfComplianceSubmit_IrisController controller = new SC_SelfComplianceSubmit_IrisController();

		Test.startTest();
			PageReference pageActionres = controller.pageAction();
			controller.reload();
			controller.getPdfUrl();
			System.assertEquals(controller.pageTitle,SC_Const.SC_COMMON_TITLE);
		Test.stopTest();
	}

	@isTest static void testAdditionalPageAction01() {
		PageReference pageRef = Page.IRIS_SCSelfComplianceSubmit;
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfComplianceSubmit_Iris.createContact(true, accAgency.Id, actUserLastName);
		SC_Office__c office = createOffice(accAgency);
		SC_SelfCompliance__c scComp = createSelfComp(office);
		pageRef.getParameters().put('id',scComp.id);
		pageRef.getParameters().put('type',SC_Const.SC_COMP_TYPE_NN);
		pageRef.getParameters().put('isSingle','true');
		pageRef.getParameters().put('isOldIE','false');
		pageRef.getParameters().put('year','');

		Test.setCurrentPage(pageRef);
		SC_SelfComplianceSubmit_IrisController controller = new SC_SelfComplianceSubmit_IrisController();

		Test.startTest();
			PageReference pageActionres = controller.pageAction();
			System.assertEquals(controller.pageTitle,SC_Const.SC_ADD_TITLE);
		Test.stopTest();
	}

	@isTest static void testTrailPageAction01() {
		PageReference pageRef = Page.IRIS_SCSelfComplianceSubmit;
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 2. 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 3. 取引先責任者を作成
		String actUserLastName = 'TestUser001';
		Contact con = TestSC_SelfComplianceSubmit_Iris.createContact(true, accAgency.Id, actUserLastName);
		SC_Office__c office = createOffice(accAgency);
		SC_SelfCompliance__c scComp = createSelfComp(office);
		pageRef.getParameters().put('id',scComp.id);
		pageRef.getParameters().put('type',SC_Const.SC_COMP_TYPE_TRAIL);
		pageRef.getParameters().put('isSingle','false');
		pageRef.getParameters().put('isOldIE','false');
		pageRef.getParameters().put('year','');

		Test.setCurrentPage(pageRef);
		SC_SelfComplianceSubmit_IrisController controller = new SC_SelfComplianceSubmit_IrisController();

		Test.startTest();
			PageReference pageActionres = controller.pageAction();
			System.assertEquals(controller.pageTitle,SC_Const.SC_TRAIL_TITLE );
		Test.stopTest();
	}


	public static SC_Office__c createOffice(Account acc){
		SC_Office__c office = new SC_Office__c();
		office.Account__c = acc.id;
		Insert office;
		return office;
	}

	public static SC_SelfCompliance__c createSelfComp(SC_Office__c office){
		SC_SelfCompliance__c comp = new SC_SelfCompliance__c();
		comp.SC_Office__c = office.id;
		Insert comp;
		return comp;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 */
	public static Contact createContact(Boolean isInsert, Id accId, String lastName) {
		Contact src = new Contact(
			  AccountId = accId
			, LastName = lastName
			, E_CL3PF_ZAGMANGR__c = 'Y'
		);
		if(isInsert){
			insert src;
			src = [Select Id, Account.Name, LastName, E_AccParentCord__c From Contact Where Id =: src.Id];
		}
		return src;
	}

	public static void createPage(String year){
		I_PageMaster__c page = new I_PageMaster__c();
		page.Name = 'test';
		page.page_unique_key__c = SC_Const.SC_MANUAL_PREFIX + year;
		Insert page;
	}
}