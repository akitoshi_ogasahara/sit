public with sharing class MNT_Util {

	public static void saveParentWithAttachment(sObject record, Attachment uploadFile){

		Savepoint sp = Database.setSavepoint();
		Boolean saveResult = false;
		
		try{
			saveResult = E_GenericDao.saveViaPage(record);

			if(saveResult && uploadFile!=null && uploadFile.Body!=null && String.isNotBlank(uploadFile.Name)){				
	            uploadFile.parentId = record.Id;
	            uploadFile.ContentType = 'application/pdf';//強制的にファイルダウンロードを実施させる
	            insert uploadFile;
			}

		}catch(Exception e){
			Database.Rollback(sp);
       		ApexPages.addMessage(new ApexPages.Message( ApexPages.severity.ERROR, '保存中にエラーが発生しました。' + e));
		}
	}
}