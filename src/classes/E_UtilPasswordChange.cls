/**
* パスワード登録時のUtilクラス
*/
public with sharing class E_UtilPasswordChange {
    
    /**
    * 仮パスワード判定
    * @param E_IDCPF__c ID管理
    * @return Boolean　仮パスワード判定結果
    */
    public static Boolean getIsTempPassWord(E_IDCPF__c idcpf){
        /*
        Boolean isTemp = false;
        if(String.isBlank(idcpf.ZPASSWD02DATE__c)){
            isTemp = true;
        }
        return isTemp;
        */
        return idcpf.UseTempProcedurePW__c;
    }
    
    /**
    * 入力チェック
    * @param E_IDCPF__c ID管理
    * @param String 現在のパスワード
    * @param String 新しいパスワード
    * @param String 新しいパスワード（確認）
    * @return List<String> メッセージリスト
    */
    public static List<String> validatePassword(E_IDCPF__c idcpf, String oldPw, String newPw, String verPw){
        
        // エラーメッセージ
        String errMsgRequiredOld = E_Message.getMsgMap().get('PPC|001');    // 現在の手続サービス用パスワードが未入力
        String errMsgNotSameOld = E_Message.getMsgMap().get('PPC|002');     // 現在の手続サービス用パスワードが正しくない
        String errMsgRequiredNew = E_Message.getMsgMap().get('PPC|003');    // 新しい手続サービス用パスワードが未入力
        String errMsgValidateNew = E_Message.getMsgMap().get('PPC|004');    // 新しい手続サービス用パスワードの入力規則
        String errMsgRequiredVer = E_Message.getMsgMap().get('PPC|005');    // 新規パスワード（確認）が未入力
        String errMsgNotSameVer = E_Message.getMsgMap().get('PPC|006');     // 新規パスワード（確認）が正しくない
        
        List<String> errMsgList = new List<String>();
        
        // 現在の手続サービス用パスワードが未入力の時
        if(String.isBlank(oldPw)){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, E_Const.LABEL_2P_OLD + ': ' + errMsgRequiredOld));
            errMsgList.add(E_Const.LABEL_2P_OLD + ': ' + errMsgRequiredOld);
            
            // 入力されている場合のチェック処理
        }else{
            // 現在の手続サービス用パスワードが正しくない、もしくは手続サービス用パスワードが無効な時(ID管理のパスワード２と違う)
            if(!(idcpf.ZSTATUS02__c == E_Const.PW_STATUS_OK
                 //&& idcpf.ZPASSWD02__c == E_EncryptUtil.encrypt(oldPw))){
                 && validateProceduralPassword(idcpf, oldPw))){
                     //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, E_Const.LABEL_2P_OLD + ': ' + errMsgNotSameOld));
                     errMsgList.add(E_Const.LABEL_2P_OLD + ': ' + errMsgNotSameOld);
                 }
        }
        
        // 新規手続サービス用パスワードが未入力の時
        if(String.isBlank(newPw)){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, E_Const.LABEL_2P_NEW + ': ' + errMsgRequiredNew));
            errMsgList.add(E_Const.LABEL_2P_NEW + ': ' + errMsgRequiredNew);
            
            // 入力されている場合のチェック処理
        }else{
            //
            if(validateNewPassword(newPw)){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, E_Const.LABEL_2P_NEW + ': ' + errMsgValidateNew));
                errMsgList.add(E_Const.LABEL_2P_NEW + ': ' + errMsgValidateNew);
            }
        }
        
        // 新規手続サービス用パスワード（確認）が未入力の時
        if(String.isBlank(verPw)){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, E_Const.LABEL_2P_VER + ': ' + errMsgRequiredVer));
            errMsgList.add(E_Const.LABEL_2P_VER + ': ' + errMsgRequiredVer);
            
            // 入力されている場合のチェック処理
        }else{
            // 新規手続サービス用パスワードと新規手続サービス用パスワード(確認）が異なる時
            if(!newPw.equals(verPw)){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, E_Const.LABEL_2P_VER + ': ' + errMsgNotSameVer));
                errMsgList.add(E_Const.LABEL_2P_VER + ': ' + errMsgNotSameVer);
            }
        }
        
        return errMsgList;
    }
    
    /* 新しいパスワードの入力チェック */
    private static Boolean validateNewPassword(String newPw){
        
        // パスワードに使用できる文字(半角英小文字及び数字のみ)以外の入力がある
        if(!E_Util.isEnLowerAlphaNum(newPw)){
            return true;
        }
        
        // 入力全てが英文字、もしくは全てが数字である
        if(E_Util.isEnNum(newPw) || E_Util.isEnAlpha(newPw)){
            return true;
        }
        
        // ８桁でない
        if(newPw.length() != 8){
            return true;
        }
        
        return false;
    }

    /* 現在のパスワードの入力チェック */
    private static Boolean validateProceduralPassword(E_IDCPF__c idcpf, String password){
        Boolean ret = false;
        if(getIsTempPassWord(idcpf)){
            //system.assert(idcpf.ZPASSWD02__c == E_EncryptUtil.encryptInitPass(password, idcpf.ZPASSWD02DATE__c), idcpf.ZPASSWD02__c+'>'+E_EncryptUtil.encryptInitPass(password, idcpf.ZPASSWD02DATE__c));         
            System.debug('仮手続パスワード='+idcpf.ZPASSWD02__c);
            System.debug('入力='+E_EncryptUtil.encryptInitPass(password, idcpf.ZPASSWD02DATE__c));
            ret = (idcpf.ZPASSWD02__c == E_EncryptUtil.encryptInitPass(password, idcpf.ZPASSWD02DATE__c));
        }else{
            ret = (idcpf.RegularProcedurePW__c == E_EncryptUtil.encrypt(password));
        }
        return ret;
    }

    /**
    * 手続パスワード変更
    * @param E_IDCPF__c ID管理
    * @param String 新しいパスワード（暗号化前）
    */
    public static void updateProceduralPassword(E_IDCPF__c idcpf, String password){
        //本手続PW
        idcpf.RegularProcedurePW__c = E_EncryptUtil.encrypt(password);
        if(getIsTempPassWord(idcpf)){
            //仮手続PW使用
            idcpf.UseTempProcedurePW__c = false;
            //仮手続PW変更日 ※2016/7仕様変更のためコメントアウト
            //idcpf.ZPASSWD02DATE__c = E_Util.getSysDate(null);
            //手続パスワードステータス
            idcpf.ZSTATUS02__c = E_Const.PW_STATUS_OK;
        } else {
            //本手続PW変更日
            idcpf.RegularProcedurePWChangeDate__c = E_Util.getSysDate(null);
        }
        update idcpf;
    }

}