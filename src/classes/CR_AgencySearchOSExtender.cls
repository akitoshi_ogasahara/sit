global with sharing class CR_AgencySearchOSExtender extends CR_AbstractExtender{
    
    private CR_AgencySearchOSSVEController controller;
    
    public CR_AgencySearchOSExtender(CR_AgencySearchOSSVEController extension){
        this.controller = extension;
    }

    public PageReference ExportCSV(){
        PageReference pr = Page.E_CRExportAgencyOS;
        String soql = controller.queryMap.get('resultTable').toSoql();
        String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
        sWhere = E_SoqlUtil.trimLimitClause(sWhere);
        pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
                                E_EncryptUtil.getEncryptedString(sWhere));  
        return pr;
    }
}