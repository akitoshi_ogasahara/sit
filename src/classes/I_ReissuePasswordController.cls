public with sharing class I_ReissuePasswordController  extends E_AbstractController{

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;private set;}

	//ユーザ名
	public String userId {get;set;}

	//パスワード再発行フラグ
	private Boolean isReissue;

	//90日未ログインのための変更か
	public Boolean isPassedDays {get;private set;}

	//ロックされている代理店ユーザ
	private Boolean isNnLinkAgeUserLocked;

	//コンストラクタ
	public I_ReissuePasswordController(){
		pgTitle = 'パスワード再発行';
		isReissue = false;
		isNnLinkAgeUserLocked = false;
		pageMessages = getPageMessages();
		//パラメータ取得
		String urlParam = ApexPages.currentPage().getParameters().get('isPassedDays');
		//パラメータが有効(='1')の場合、90日未ログインのための遷移→メッセージを表示
		if(urlParam =='1'){
			isPassedDays = true;
		}else{
			isPassedDays = false;
		}
	}

	//認証画面_ページアクション
	public PageReference pageAction(){
		isReissue = true;
		pageRef = doAuth(E_Const.ID_KIND.LOGIN, userinfo.getUserId());
		return E_Util.toErrorPage(pageRef, null);
	}

	//認証画面_[次へ]ボタン
	public PageReference doReissuePassword(){

		PageReference ret = null;
		pageMessages.clearMessages();

		try {

			//ユーザIDチェック
			if(String.isBlank(userId)){
				pageMessages.addErrorMessage(getMSG().get('LRE|202'));
				return null;
			}
			
			//ユーザレコード取得
			User userRec = E_UserDaoWithout.getUserRecByUserNameWithEIDC(userId + System.Label.E_USERNAME_SUFFIX);

			if(userRec == null){
				pageMessages.addErrorMessage(getMSG().get('LGE|001'));
				return null;
			}

			//ID管理があり、仮パスワードフラグがTRUEであるユーザーケース（紙発行ユーザ）を考慮し、ＩＤ管理の有無で判断する。
			if(userRec.E_IDCPFs__r.size() == 0){
				//仮PWを再発行する
				E_SelfRegistrationController.tempPassSet(userRec,userRec.Contact.Name,getMSG().get('MAB|002'),userId);
				//画面遷移
				ret = Page.IRIS_ReissuePasswordComp;
				isReissue = true;

				return ret;
			}


			//ユーザロック
			UserLogin userLoginRecBefore = E_UserLoginDaoWithout.getRecByUserId(userRec.id);
			if(userLoginRecBefore == null){
				String pathPrefix = Site.getPathPrefix();
				if (!String.isEmpty(pathPrefix)) {
					if (pathPrefix.indexOf(E_Const.NNLINK_CLI) > 0) {
						//契約者
						pageMessages.addErrorMessage(getMSG().get('LRE|203'));
						return null;
					//} else {
					}else if (pathPrefix.indexOf(E_Const.NNLINK_AGE) > 0 ) {
						//代理店の場合はロック中でもリセットする 20161020
						//pageMessages.addErrorMessage(getMSG().get('LRE|204'));
						userLoginRecBefore = E_UserLoginDaoWithout.getRecWithIsLockByUserId(userRec.id);
						
						userLoginRecBefore.IsPasswordLocked=false;
						E_UserLoginDaoWithout.updateRec(userLoginRecBefore);
						//ロックされている代理店ユーザフラグをON →　完了画面表示時にforgotPassword　実施
						isNnLinkAgeUserLocked = true;

						isReissue = true;
						return Page.IRIS_ReissuePasswordComp;

					}else{
						pageMessages.addErrorMessage(getMSG().get('LRE|204'));
						return null;
					}
				}
			}
			
			//パスワードリセット
			boolean isPass = Site.forgotPassword(userId + System.Label.E_USERNAME_SUFFIX);
			
			//メール送信後も現在のパスワードでログインできてしまうのでログインできないように＆パスワード変更画面遷移時、戻るボタンを表示させない
			//userRec.E_UseTempBasicPW__c = true;
			//E_UserDaoWithout.updateRec(userRec);
			
			//画面遷移
			ret = Page.IRIS_ReissuePasswordComp;
			isReissue = true;

		} catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
			ret = null;
		}

		return ret;
	}

	//[戻る]ボタン
	public PageReference doBack(){
		return getBackPage();
	}

	//完了画面_ページアクション
	public PageReference pageAction_Comp(){
		if(!isReissue){
			return getBackPage();
		}
		if(isNnLinkAgeUserLocked){
			Site.forgotPassword(userId + System.Label.E_USERNAME_SUFFIX);
		}
		return null;
	}

	private PageReference getBackPage() {
		if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_CLI)) {
			return Page.E_LoginCustomer;
		} else if (Site.getName() != null && Site.getName().equals(E_Const.NNLINK_AGE)) {
			return Page.E_LoginAgent;
		} else {
			return Page.E_Exception;
		}
	}

}