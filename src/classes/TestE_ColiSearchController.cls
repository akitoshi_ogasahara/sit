@isTest
private with sharing class TestE_ColiSearchController{
		private static testMethod void testPageMethods() {	
			E_ColiSearchController page = new E_ColiSearchController(new ApexPages.StandardController(new E_COVPF__c()));	
			page.getOperatorOptions_E_Policy_c_COMM_CHDRNUM_c();	
			page.getOperatorOptions_E_COVPF_c_InsuredE_CLTPF_ZCLKNAME_c();	
			page.getOperatorOptions_E_COVPF_c_InsuredName_c();	
			System.assert(true);
		}	
			
	private static testMethod void testdataTable() {
		E_ColiSearchController.dataTable dataTable = new E_ColiSearchController.dataTable(new List<E_COVPF__c>(), new List<E_ColiSearchController.dataTableItem>(), new List<E_COVPF__c>(), null);
		dataTable.setPageItems(new List<E_ColiSearchController.dataTableItem>());
		dataTable.create(new E_COVPF__c());
		dataTable.doDeleteSelectedItems();
		dataTable.setPagesize(10);		dataTable.doFirst();
		dataTable.doPrevious();
		dataTable.doNext();
		dataTable.doLast();
		dataTable.doSort();
		System.assert(true);
	}
	
}