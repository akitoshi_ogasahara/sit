@isTest
private class TestE_InveSearchExtender{
	
	//コンストラクタ
	static testMethod void constructorTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			
			E_ITHPF__c record = new E_ITHPF__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(record);
			E_InveSearchController extension = new E_InveSearchController(controller);
			
			Test.startTest();
			E_InveSearchExtender extender = new E_InveSearchExtender(extension);
			Test.stopTest();
		}
	}
	
	//ページアクション
	//doAuth成功
	static testMethod void pageActionTest001(){
		
		//テストユーザ作成
		User u = createUser('1');
		
		System.runAs(u){
			E_InveSearchExtender extender = createExtender();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			//※2015年、投資信託サービス終了に伴い投資信託機能削除
			system.assertNotEquals(null, result);
		}
	}
	
	//ページアクション
	//doAuth失敗
	static testMethod void pageActionTest002(){
		
		//テストユーザ作成
		User u = createUser('0');
		
		System.runAs(u){
			E_InveSearchExtender extender = createExtender();
			
			Test.startTest();
			PageReference result = extender.pageAction();
			Test.stopTest();
			
			system.assertNotEquals(null, result);
		}
	}
	
	//半角カナ⇒全角カナ変換
	static testMethod void preSearchTest001(){
		
		E_ITHPF__c record = new E_ITHPF__c();
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveSearchController extension = new E_InveSearchController(controller);
		E_InveSearchExtender extender = new E_InveSearchExtender(extension);
		
		extension.NameKana_val.SkyEditor2__Text__c = '半角ｶﾅ';
		
		Test.startTest();
		extender.preSearch();
		Test.stopTest();
		
		system.assertEquals('半角カナ', extension.NameKana_val.SkyEditor2__Text__c);
	}
	
	//テストユーザ作成
	static User createUser(String flag03){
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		idcpf.FLAG03__c = flag03;
		insert idcpf;
		return u;
	}
	
	//Extender作成
	static E_InveSearchExtender createExtender(){
		E_ITHPF__c record = new E_ITHPF__c();
		ApexPages.StandardController controller = new ApexPages.StandardController(record);
		E_InveSearchController extension = new E_InveSearchController(controller);
		E_InveSearchExtender extender = new E_InveSearchExtender(extension);
		extender.init();
		return extender;
	}

}