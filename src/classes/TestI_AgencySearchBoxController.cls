@isTest
private class TestI_AgencySearchBoxController {
	private static TestUtil_I_SearchBox util = new TestUtil_I_SearchBox();

	@isTest static void getSearchCategoryLabelTest() {
		Test.startTest();
		I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
		System.assertEquals(agencySearch.getSearchCategoryLabel(),I_Const.SEARCH_LABEL_CATEGORY_AGENCY);
		Test.stopTest();
	}

	@isTest static void getSelectOptionsTest() {
		Test.startTest();
		I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
		List<SelectOption> options = agencySearch.getSearchOptions();
		System.assertEquals(options.size(),2);
		Test.stopTest();
	}

	//募集人検索　and検索
	@isTest static void doSearchTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Contact con = TestUtil_I_SearchBox.createContactAgent(false,util.office.Id,'テスト募集人2');
		con.E_CL3PF_AGNTNUM__c = 'A4321';
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}



	//募集人検索 エラー
	@isTest static void doSearchTest2() {
		util.createStandardPolicy(true, null, false);
		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.doSearch();
			System.assert(!agencySearch.isSearch);
			Test.stopTest();
		}
	}

	//募集人検索 社員
	@isTest static void doSearchTest3() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_EmployeeStandard',null);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');
		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		System.runAs(actUser){
			Test.startTest();
			//TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//募集人検索 長期未使用
	@isTest static void doSearchTest4() {
		User actUser1 = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		E_IDCPF__c idcp1 = TestUtil_I_SearchBox.createIDCPF(true, actUser1.Id, '');
		idcp1.ZINQUIRR__c = 'L3T6610';
		update idcp1;

		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User actUser2 = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_EmployeeStandard',null);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, actUser2.Id, '');
		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;
		E_IDRequest__c testIDR = new E_IDRequest__c(E_IDCPF__c = idcp.Id
													, ZIDOWNER__c = 'AG'
													, ZIDTYPE__c = 'AT'
													, ZINQUIRR__c = 'L3'
													, ZWEBID__c = '50'
													, ZUPDFLAG__c = 'D');
		insert testIDR;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		System.runAs(actUser1){
			actUser2.isActive = false;
			update actUser2;
			Test.startTest();
			//TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//募集人検索　or検索
	@isTest static void doSearchTest5() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Contact con = TestUtil_I_SearchBox.createContactAgent(false,util.office.Id,'テスト募集人2');
		con.E_CL3PF_AGNTNUM__c = 'A4321';
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.selection = 'or';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 2);
			Test.stopTest();
		}
	}

	//募集人検索 AY and検索
	@isTest static void doSearchAYTest1() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Contact con = TestUtil_I_SearchBox.createContactAgent(false,util.office.Id,'テスト募集人2');
		con.E_CL3PF_AGNTNUM__c = 'A4321';
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AY');

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//募集人検索 AY or検索
	@isTest static void doSearchAYTest2() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Contact con = TestUtil_I_SearchBox.createContactAgent(false,util.office.Id,'テスト募集人2');
		con.E_CL3PF_AGNTNUM__c = 'A4321';
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AY');

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.selection = 'or';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 2);
			Test.stopTest();
		}
	}

	//募集人検索 AH and検索
	@isTest static void doSearchAHTest1() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Contact con = TestUtil_I_SearchBox.createContactAgent(false,util.office.Id,'テスト募集人2');
		con.E_CL3PF_AGNTNUM__c = 'A4321';
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//募集人検索 AH or検索
	@isTest static void doSearchAHTest2() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Contact con = TestUtil_I_SearchBox.createContactAgent(false,util.office.Id,'テスト募集人2');
		con.E_CL3PF_AGNTNUM__c = 'A4321';
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.selection = 'or';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 2);
			Test.stopTest();
		}
	}

	//事務所検索 and検索
	@isTest static void doSearchOfficeTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Account office2 = TestUtil_I_SearchBox.createOffice(false,util.agency.Id,'テスト事務所2');
		office2.E_COMM_VALIDFLAG__c = '1';
		office2.E_CL2PF_BRANCH__c = '10';
		insert office2;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');

		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(office2.Id,aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.searchType = I_Const.SEARCH_TYPE_OFFICE;
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//事務所検索 エラー
	@isTest static void doSearchOfficeTest2() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');

		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;


		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_OFFICE;
			agencySearch.doSearch();
			System.assert(!agencySearch.isSearch);
			Test.stopTest();
		}
	}
	//事務所検索 社員
	@isTest static void doSearchOfficeTest3() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_EmployeeStandard',null);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, '');
		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;
		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_OFFICE;
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//事務所検索 or検索
	@isTest static void doSearchOfficeTest4() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		Account office2 = TestUtil_I_SearchBox.createOffice(false,util.agency.Id,'テスト事務所2');
		office2.E_COMM_VALIDFLAG__c = '1';
		office2.E_CL2PF_BRANCH__c = '10';
		insert office2;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');

		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(office2.Id,aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 2';
			agencySearch.searchType = I_Const.SEARCH_TYPE_OFFICE;
			agencySearch.selection = 'or';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 2);
			Test.stopTest();
		}
	}


	//代理店検索 and検索
	@isTest static void doSearchAgencyTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');
		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		//テストデータ作成
		Account parent = new Account();
		parent.Name = 'テストparent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account acc1 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト1');
		acc1.E_CL2PF_BRANCH__c = '10';
		acc1.E_COMM_VALIDFLAG__c = '1';
		acc1.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc1;


		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(parent.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(acc1.Id, aUser.Id);


		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'parent';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENCY;
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 1);
			Test.stopTest();
		}
	}

	//代理店検索 エラー
	@isTest static void doSearchAgencyTest2() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');

		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENCY;
			agencySearch.doSearch();
			System.assert(!agencySearch.isSearch);
			Test.stopTest();
		}
	}

	//代理店検索 or検索
	@isTest static void doSearchAgencyTest3() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');
		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		//テストデータ作成
		Account parent = new Account();
		parent.Name = 'テストparent';
		parent.E_CL1PF_ZHEADAY__c = '12345';
		insert parent;

		Account acc1 = TestUtil_I_SearchBox.createOffice(false,parent.Id,'テスト1');
		acc1.E_CL2PF_BRANCH__c = '10';
		acc1.E_COMM_VALIDFLAG__c = '1';
		acc1.E_CL1PF_ZHEADAY__c = 'テスト1T';
		insert acc1;


		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(parent.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(acc1.Id, aUser.Id);


		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト 代理店';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENCY;
			agencySearch.selection = 'or';
			agencySearch.doSearch();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 2);
			Test.stopTest();
		}
	}



	//「最近検索したキーワード」から検索
	@isTest static void doSearchLogTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;
		User aUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		E_IDCPF__c idcp = TestUtil_I_SearchBox.createIDCPF(true, aUser.Id, 'AH');
		idcp.ZINQUIRR__c = 'L3T6610';
		update idcp;

		util.office.E_COMM_VALIDFLAG__c = '1';
		util.office.E_CL2PF_BRANCH__c = '10';
		update util.office;

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, aUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, aUser.Id);

		User actUser = [select Name,Id,FirstName,LastName,Contact.AccountId,Contact.Account.ParentId from User where LastName = 'testAgent'];
		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENCY;
			agencySearch.doSearchLog();
			System.assert(!agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows.size(), 0);
			Test.stopTest();
		}
	}
	//募集人名ソート
	@isTest static void doSortTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;

		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		List<Contact> contacts = createContacts(util.agency);
		System.debug(contacts);

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'agentName');
		Test.setCurrentPage(pr);

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, actUser.Id);

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENT;
			agencySearch.doSearch();
			agencySearch.sortRows();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows[0].agentName,'テスト募集人');
			Test.stopTest();
		}
	}

	//募集人コードソート
	@isTest static void doagentCodeSortTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;

		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		List<Contact> contacts = createContacts(util.agency);
		System.debug(contacts);

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'agentCode');
		Test.setCurrentPage(pr);

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, actUser.Id);

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENT;
			agencySearch.doSearch();
			agencySearch.sortRows();
			System.debug(agencySearch.agencyRows);
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows[0].agentName,'テスト募集人');
			Test.stopTest();
		}
	}

	//事務所名ソート
	@isTest static void doofficeNameSortTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;

		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		List<Contact> contacts = createContacts(util.agency);
		System.debug(contacts);

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'officeName');
		Test.setCurrentPage(pr);

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, actUser.Id);

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENT;
			agencySearch.doSearch();
			agencySearch.sortRows();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows[0].agentName,'テスト募集人');
			Test.stopTest();
		}
	}

	//事務所コードソート
	@isTest static void doofficeCodeSortTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;

		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		List<Contact> contacts = createContacts(util.agency);
		System.debug(contacts);

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'officeCode');
		Test.setCurrentPage(pr);

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, actUser.Id);

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENT;
			agencySearch.doSearch();
			agencySearch.sortRows();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows[0].agentName,'テスト募集人');
			Test.stopTest();
		}
	}

	//代理店コードソート
	@isTest static void doagencyCodeSortTest() {
		util.createStandardPolicy(true, null, false);
		util.agent.E_CL3PF_VALIDFLAG__c = '1';
		update util.agent;

		User actUser = TestUtil_I_SearchBox.createAgentUser(true,'testAgent','E_PartnerCommunity',util.agent.id);
		TestUtil_I_SearchBox.createIDCPF(true, actUser.Id, '');

		List<Contact> contacts = createContacts(util.agency);
		System.debug(contacts);

		PageReference pr = Page.IRIS_Top;
		pr.getParameters().put('st', 'agencyCode');
		Test.setCurrentPage(pr);

		// 7. レコードを共有
		TestI_TestUtil.createAccountShare(util.agency.Id, actUser.Id);
		TestI_TestUtil.createAccountShare(util.office.Id, actUser.Id);

		System.runAs(actUser){
			Test.startTest();
			TestUtil_I_SearchBox.createBasePermissions(actUser.id);
			I_AgencySearchBoxController agencySearch = new I_AgencySearchBoxController();
			agencySearch.keyword = 'テスト';
			agencySearch.searchType = I_Const.SEARCH_TYPE_AGENT;
			agencySearch.doSearch();
			agencySearch.sortRows();
			System.assert(agencySearch.isSearch);
			System.assertEquals(agencySearch.agencyRows[0].agentName,'テスト募集人');
			Test.stopTest();
		}
	}

	/**
	 * （ソート用）募集人を複数作成
	 * 
	 */
	public static List<Contact> createContacts(Account agency){
		// 代理店、事務所を作成
		Account dAgency = agency;
		Account office = TestUtil_I_SearchBox.createoffice(true, dAgency.Id, 'テスト事務所');
		// 契約者、被保険者を作成
		List<Contact> owners = new List<Contact>();
		for(Integer i = 0; i < 3; I++){
			// 契約者
			Contact owner = TestUtil_I_SearchBox.createContactOwner(false, office.Id, 'テスト募集人' + i);

			owner.E_CLTPF_CLNTNUM__c = '12345' + i;
			owner.E_CLTPF_ZCLKNAME__c = 'テストボシュウニン' + (3 - i);
			owner.E_CL3PF_ZEATKNAM__c = 'テストボシュウニン' + (3 - i);
			owner.E_CL3PF_AGNTNUM__c = 'test' + i;
			owner.E_COMM_ZCLADDR__c = '日本橋' + i;
			owner.E_CL3PF_VALIDFLAG__c = '1';

			owners.add(owner);
		}

		insert owners;
		return owners;
	}

	// 検索 - 最近検索したキーワード
	private static testmethod void testgetSeachBoxLog(){
		createLogs();
		test.startTest();
			I_AgencySearchBoxController controller = new I_AgencySearchBoxController();
			controller.searchTypeName = I_Const.SEARCH_TYPE_LABEL_AGENT;
			controller.getSeachBoxLog();
		test.stopTest();

		//system.assertEquals(controller.policyRows.size(), 1);
	}

	static void createLogs(){
		E_log__c log = new E_log__c();
		log.Name = I_Const.SEARCH_LABEL_CATEGORY_AGENCY + '>' + I_Const.SEARCH_TYPE_LABEL_AGENT + '>佐藤';
		log.Detail__c = '{"searchType": "policyNo", "searchTypeName": "募集人", "keyword": "佐藤"}';
		log.AccessUser__c = userInfo.getUserId();
		log.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_SEARCH;
		log.AccessDateTime__c = DateTime.now();
		insert log;
	}


}