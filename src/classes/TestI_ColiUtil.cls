@isTest
private class TestI_ColiUtil {

    //ProfileName
    private final String PF_SYSTEM = 'システム管理者';
    private static String PF_EMPLOYEE = 'E_EmployeeStandard';
    private static String PF_PARTNER = 'E_PartnerCommunity';

    //User情報
    private static User user;
    private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];   //Id取得
    private static User communityUser;
    private static Account ahAcc;
    private static Account ayAcc;
    private static Account atAcc;
    private static List<Account> ayAccList;
    private static Contact atCon;
    private static List<E_Policy__c> policyList;
    private static List<E_COVPF__c> covList;

    @isTest static void getChargeTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        updateAccOwner(ayAccList[0]);
        updateAccOwner(ayAccList[1]);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Charge colidata = I_ColiUtil.getCharge(pol);


            System.assertEquals(true,colidata.hasMainMr);
            System.assertEquals('test',colidata.mainAgentName);
            System.assertEquals('office1',colidata.mainAccountName);
            System.assertEquals(user.Lastname,colidata.mainMr);
            System.assertEquals('東京',colidata.mainBr);
            System.assertEquals('33%',colidata.mainSplitc);
            System.assertEquals(true,colidata.hasSubMr);
            System.assertEquals('test',colidata.subAgentName);
            System.assertEquals('office2',colidata.subAccountName);
            System.assertEquals(user.Lastname,colidata.subMr);
            System.assertEquals('東京',colidata.subBr);
            System.assertEquals('66%',colidata.subSplitc);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getChargeTest02() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        updateAccOwner(ayAccList[0]);
        updateAccOwner(ayAccList[1]);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys2();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|2');

            I_ColiDatas.Charge colidata = I_ColiUtil.getCharge(pol);


            System.assertEquals(true,colidata.hasMainMr);
            System.assertEquals('test',colidata.mainAgentName);
            System.assertEquals('office1',colidata.mainAccountName);
            System.assertEquals(user.Lastname,colidata.mainMr);
            System.assertEquals('東京',colidata.mainBr);
            System.assertEquals('-',colidata.mainSplitc);
            System.assertEquals(true,colidata.hasSubMr);
            System.assertEquals('test',colidata.subAgentName);
            System.assertEquals('office2',colidata.subAccountName);
            System.assertEquals(user.Lastname,colidata.subMr);
            System.assertEquals('東京',colidata.subBr);
            System.assertEquals('-',colidata.subSplitc);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getCoversTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            covList = new List<E_COVPF__c>();
            for(Integer i = 0; i < 3; i++){
                E_COVPF__c cov =  createCOV(policyList[i]);
                covList.add(cov);
            }
            insert covList;

            List<Id> covIds = new List<Id>();
            for( E_COVPF__c rec : covList ){
                covIds.add(rec.Id);
            }
            covList = getCovers(covIds);            


            //テスト開始
            Test.startTest();

            List<I_ColiDatas.Cover> colidatas = I_ColiUtil.getCovers(covList,covList[0]);

            System.assertEquals('終身',colidatas[0].hokenKikan);
            System.assertEquals('---------',colidatas[0].haraikomiKikan);
            System.assertEquals(true,colidatas[0].isMain);
            System.assertEquals('主契約',colidatas[0].hosyouNaiyou);
            //System.assertEquals('終身',colildatas[0].hokenKikan);
            //System.assertEquals('終身',colildatas[0].hokenKikan);
            //System.assertEquals('終身',colildatas[0].hokenKikan);
            //System.assertEquals('終身',colildatas[0].hokenKikan);
            //System.assertEquals('終身',colildatas[0].hokenKikan);

            //テスト終了
            Test.stopTest();
        }
    }


    //@isTest static void getCoversTest02() {
    //    //User
    //    createDataAccount();
    //    createUser(PF_EMPLOYEE);
    //    createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);

    //    System.runAs(user){
    //        //保険契約ヘッダを作成
    //        E_Policy__c policy = TestI_MeritTableBizLogic.createPolicyInfo();

    //        //個人保険特約リスト(主契約・特約)を作成
    //        List<E_COVPF__c> covers = TestI_MeritTableBizLogic.createCoversInfo(policy);        

    //        //テスト開始
    //        Test.startTest();

    //        List<I_ColiDatas.Cover> colidatas = I_ColiUtil.getCovers(covers,covers[0]);
    //        system.debug( '行サイズ' + colidatas.size() );

    //        //テスト終了
    //        Test.stopTest();
    //    }
    //}

    //@isTest static void getCoversTest02() {
    //    //User
    //    createDataAccount();
    //    createUser(PF_EMPLOYEE);
    //    createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);

    //    System.runAs(user){
    //        policyList = new List<E_Policy__c>();
    //        policyList = createPolicys();
    //        covList = new List<E_COVPF__c>();
    //        for(Integer i = 0; i < 2; i++){
    //            createCOV(policyList[0]);
    //        }

    //        //モータルクラス・削減コードの設定
    //        covList[0].COLI_MORTCLS__c = 'A';
    //        covList[0].COLI_LIENCD__c = '1';
    //        covList[1].COLI_MORTCLS__c = 'Y';
    //        covList[1].COLI_LIENCD__c = '2';
    //        update covList;

    //        covList = E_COVPFDao.getRecsByPolicyIdAndRtdevname( policyList[0].Id , null );

    //        //テスト開始
    //        Test.startTest();

    //        List<I_ColiDatas.Cover> colidatas = I_ColiUtil.getCovers(covList,covList[0]);

    //        system.assertEquals( 8 , colidatas.size() );
    //        system.assertEquals( covList[0].COLI_SPECIALCONDITIONS__c  , colidatas[2].hokenSyurui );
    //        system.assertEquals( covList[0].COLI_LIENCONDITIONS__c , colidatas[3].hokenSyurui );
    //        system.assertEquals( covList[1].COLI_SPECIALCONDITIONS__c  , colidatas[4].hokenSyurui );
    //        system.assertEquals( covList[1].COLI_LIENCONDITIONS__c  , colidatas[5].hokenSyurui );

    //        //テスト終了
    //        Test.stopTest();
    //    }
    //}

	   // @isTest static void getCoversTest03() {
    //    //User
    //    createDataAccount();
    //    createUser(PF_EMPLOYEE);
    //    createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);

    //    System.runAs(user){
    //        policyList = new List<E_Policy__c>();
    //        policyList = createPolicys();
    //        covList = new List<E_COVPF__c>();
    //        for(Integer i = 0; i < 2; i++){
    //            createCOV(policyList[0]);
    //        }

    //        //モータルクラス・削減コードの設定
    //        covList[0].COLI_MORTCLS__c = 'A';
    //        covList[0].COLI_LIENCD__c = '1';
    //        covList[1].COLI_MORTCLS__c = 'A';
    //        covList[1].COLI_LIENCD__c = '1';
    //        update covList;

    //        covList = E_COVPFDao.getRecsByPolicyIdAndRtdevname( policyList[0].Id , null );

    //        //テスト開始
    //        Test.startTest();

    //        List<I_ColiDatas.Cover> colidatas = I_ColiUtil.getCovers(covList,covList[0]);

    //        system.assertEquals( 6 , colidatas.size() );
    //        system.assertEquals( covList[0].COLI_SPECIALCONDITIONS__c  , colidatas[2].hokenSyurui );
    //        system.assertEquals( covList[0].COLI_LIENCONDITIONS__c , colidatas[3].hokenSyurui );

    //        //テスト終了
    //        Test.stopTest();
    //    }
    //}

    @isTest static void getContractorTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Contractor colidata = I_ColiUtil.getContractor(pol);

            System.assertEquals('test',colidata.name);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getInsuredTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Insured colidata = I_ColiUtil.getInsured(pol);

            System.assertEquals('test',colidata.name);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getBeneficiaryTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            createCrlpf(policyList[0]);
            E_CRLPF__c crl = [select id,BNYTYPName__c,ZCLNAME__c,CLNTNUM__c,ZKNJSEX__c,BNYPC__c,DOB__c,Age__c from E_CRLPF__c where CRLPFCode__c = '99999999|TS|1'];
            //テスト開始
            Test.startTest();

            I_ColiDatas.Beneficiary colidata = I_ColiUtil.getBeneficiary(crl,true);

            System.assertEquals('受取人',colidata.bnytp);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getBeneficiaryTest02() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            createCrlpf(policyList[0]);
            E_CRLPF__c crl = [select id,BNYTYPName__c,ZCLNAME__c,CLNTNUM__c,ZKNJSEX__c,BNYPC__c,DOB__c,Age__c,BNYTYP__c from E_CRLPF__c where CRLPFCode__c = '99999999|TS|1'];
            //テスト開始
            Test.startTest();

            I_ColiDatas.Beneficiary colidata = I_ColiUtil.getBeneficiary(crl,false);

            System.assertEquals('01',colidata.bnytp);

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void getBankTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Bank colidata = I_ColiUtil.getBank(pol,true);

            System.assertEquals('有',colidata.zadvdesc);

            //テスト終了
            Test.stopTest();
        }
    }



    @isTest static void getBankTest02() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys2();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|2');

            I_ColiDatas.Bank colidata = I_ColiUtil.getBank(pol,true);

            System.assertEquals('-',colidata.zadvdesc);

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void getSurrenderTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();


            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Surrender colidata = I_ColiUtil.getSurrender(pol);

            System.assertEquals(10000,colidata.zcshval);

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void getSurrenderTest02() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys2();
            //テスト開始
            Test.startTest();


            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|2');

            I_ColiDatas.Surrender colidata = I_ColiUtil.getSurrender(pol);

            System.assertEquals(10000,colidata.zcshval);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getAdvanceTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Advance colidata = I_ColiUtil.getAdvance(pol);


            System.assertEquals('20171027',colidata.rcptdt);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getPledgeTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            createCrlpf(policyList[0]);
            E_CRLPF__c crl = [select id,BNYTYPName__c,ZCLNAME__c,CLNTNUM__c,ZKNJSEX__c,BNYPC__c,DOB__c,Age__c from E_CRLPF__c where CRLPFCode__c = '99999999|TS|1'];
            //テスト開始
            Test.startTest();

            I_ColiDatas.Pledge colidata = I_ColiUtil.getPledge(crl);


            System.assertEquals('照須快',colidata.zclname);

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void getLoneTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Lone colidata = I_ColiUtil.getLone(pol);

            System.assertEquals(10000,colidata.zepltot);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getAutoLoneTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.AutoLone colidata = I_ColiUtil.getAutoLone(pol);

            System.assertEquals(10000,colidata.zeapltot);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getNotificationTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');



            Date d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
            E_NCOL__c ncol = new E_NCOL__c(
                                YYYY__c = d.year()
                                , MM__c = d.month()
                                , E_Policy__c = pol.id
                                );
            insert ncol;

            //テスト開始
            Test.startTest();

            I_ColiDatas.Notification colidata = I_ColiUtil.getNotification(pol.Id);


            System.assertEquals('-',colidata.dto.noticeList[0].ZDUEDATE);

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void getProcessHistoryTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            createPtnpf(policyList[0]);
            E_PTNPF__c ptn = [select id,TRANDATE__c,ZTRANDSC__c from E_PTNPF__c where ZTRANDSC__c = 'テストクラス作成'];
            //テスト開始
            Test.startTest();

            I_ColiDatas.ProcessHistory colidata = I_ColiUtil.getProcessHistory(ptn);

            System.assertEquals('20171027',colidata.trandate);

            //テスト終了
            Test.stopTest();
        }
    }



    @isTest static void getChargeEHDCPFTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        updateAccOwner(ayAccList[0]);
        updateAccOwner(ayAccList[1]);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            I_ColiDatas.Charge colidata = I_ColiUtil.getChargeEHDCPF(pol);

            System.assertEquals('test',colidata.mainAgentName);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getChargeEHDCPFTest02() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);
        updateAccOwner(ayAccList[0]);
        updateAccOwner(ayAccList[1]);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys2();
            //テスト開始
            Test.startTest();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|2');

            I_ColiDatas.Charge colidata = I_ColiUtil.getChargeEHDCPF(pol);

            System.assertEquals('test',colidata.subAgentName);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getCoversEHDCPFTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            covList = new List<E_COVPF__c>();
            for(Integer i = 0; i < 3; i++){
                E_COVPF__c cov = createCOV(policyList[i]);
                covList.add(cov);
            }
            insert covList;
            List<Id> covIds = new List<Id>();
            for( E_COVPF__c rec : covList ){
                covIds.add(rec.Id);
            }
            covList = getCovers(covIds);



            //テスト開始
            Test.startTest();

            List<I_ColiDatas.Cover> colidata = I_ColiUtil.getCoversEHDCPF(covList);

            System.assertEquals('99999999',colidata[0].chdrnum);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getContractorEHDCPFTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            I_ColiDatas.Contractor colidata = I_ColiUtil.getContractorEHDCPF(policyList[0]);

            System.assertEquals('12345678',colidata.clntnum);

            //テスト終了
            Test.stopTest();
        }
    }

    @isTest static void getInsuredEHDCPFTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            //テスト開始
            Test.startTest();

            I_ColiDatas.Insured colidata = I_ColiUtil.getInsuredEHDCPF(policyList[0]);

            System.assertEquals('被保険者A',colidata.name);

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void createDlJsonTest01() {
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);


        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();

            //数式項目を参照するため、soqlで取得
            E_policy__c pol = getPolicy('999999990|1');

            Date d = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
            E_NCOL__c ncol = new E_NCOL__c(
                                YYYY__c = d.year()
                                , MM__c = d.month()
                                , E_Policy__c = pol.id
                                );
            insert ncol;

            //テスト開始
            Test.startTest();

            I_ColiDatas.Notification notification = I_ColiUtil.getNotification(pol.Id);

            String colidata = I_ColiUtil.createDlJson(pol.Id,'募集人A','募集人A', '', '1111/11/11',true,E_Const.POLICY_RECORDTYPE_COLI,notification.dto.noticeList);

            System.assertEquals(true,String.isNotBlank(colidata));

            //テスト終了
            Test.stopTest();
        }
    }


    @isTest static void isViewMainTest01() {
        //テスト開始
        Test.startTest();

        Boolean colidata = I_ColiUtil.isViewMain('123','123','321');

        System.assertEquals(true,colidata);

        //テスト終了
        Test.stopTest();
    }

    @isTest static void isViewMainTest02() {
        //テスト開始
        Test.startTest();

        Boolean colidata = I_ColiUtil.isViewMain('123','321','123');

        System.assertEquals(false,colidata);

        //テスト終了
        Test.stopTest();
    }

    @isTest static void getSpecialConditionRecordTest(){
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);

        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            E_Policy__c pol = policyList[0];
            E_COVPF__c cov = createCOV(pol);
            insert cov;
            List<Id> covIds = new List<Id>();
            covIds.add(cov.Id);
            covList = getCovers(covIds);

            //健康体の場合
            I_ColiDatas.Condition con = new I_ColiDatas.Condition();
            con.MortalityClassKenkotaiYuryotai = 'Y';
            con.TeikicoverFlg = true;
            system.assertEquals( '健康体料率適用特約(特約用)' , I_ColiUtil.getSpecialConditionRecord( con, covList[0] ) );

            //優良体の場合
            I_ColiDatas.Condition con2 = new I_ColiDatas.Condition();
            con2.MortalityClassKenkotaiYuryotai = 'Z';
            system.assertEquals( '優良体料率適用特約' , I_ColiUtil.getSpecialConditionRecord( con2, covList[0] ) );

            //割増の場合(20010401以降)
            I_ColiDatas.Condition con3 = new I_ColiDatas.Condition();
            con3.MortalityClassWarimashi = '1';
            system.assertEquals( '新特別条件特約（割増保険料A\')' , I_ColiUtil.getSpecialConditionRecord( con3, covList[0] ) );

            //割増の場合(20010331以前)
            pol.COMM_CCDATE__c = '20010331';
            update pol;
            covList = getCovers(covIds);
            system.assertEquals( '特別条件特約（割増保険料A\')' , I_ColiUtil.getSpecialConditionRecord( con3, covList[0] ) );

            //値が入らないパターン
            I_ColiDatas.Condition con4 = new I_ColiDatas.Condition();
            system.assertEquals( '' , I_ColiUtil.getSpecialConditionRecord( con4, covList[0] ) );

        }

    }

    @isTest static void getLienRecordTest(){
        //User
        createDataAccount();
        createUser(PF_EMPLOYEE);
        createDataAccessObj(user.Id, E_Const.ZIDTYPE_EP);

        System.runAs(user){
            policyList = new List<E_Policy__c>();
            policyList = createPolicys();
            E_Policy__c pol = policyList[0];
            E_COVPF__c cov = createCOV(pol);
            insert cov;
            List<Id> covIds = new List<Id>();
            covIds.add(cov.Id);
            covList = getCovers(covIds);

            //削減コードあり(20010401以降)
            I_ColiDatas.Condition con = new I_ColiDatas.Condition();
            con.LienCondition = '01';
            system.assertEquals( '新特別条件特約（保険金削減1年）' , I_ColiUtil.getLienRecord( con, covList[0] ) );

            //削減コードあり(20010331以前)
            pol.COMM_CCDATE__c = '20010331';
            update pol;
            covList = getCovers(covIds);
            system.assertEquals( '特別条件特約（保険金削減1年）' , I_ColiUtil.getLienRecord( con, covList[0] ) );

            //削減コードなし
            I_ColiDatas.Condition con2 = new I_ColiDatas.Condition();
            system.assertEquals( '' , I_ColiUtil.getSpecialConditionRecord( con2, covList[0] ) );

        }

    }

    /* Test Data **************************** */
    /** User作成 */
    private static void createUser(String profileName){
        String userName = 'test@terrasky.ingtesting';
        Profile p = [Select Id From Profile Where Name = :profileName];

        // Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );

        // User
        if(profileName != PF_PARTNER){
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            user.UserRoleId = portalRole.Id;
            insert user;

        // Partner User
        }else{
            user.ContactId = atCon.Id;
            insert user;

            ContactShare cs = new ContactShare(
                            ContactId = atCon.Id,
                            ContactAccessLevel = 'read',
                            UserOrGroupId = user.Id);
            insert cs;
        }
    }

    /** データアクセス系作成 */
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);

            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
                ,ZINQUIRR__c = 'BR**'
            );
            insert idcpf;
        }
    }

    /** Data作成 */
    private static void createDataAccount(){
        system.runAs(thisuser){
            // Account 代理店格
            ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
            insert ahAcc;

            // Account 事務所
            ayAccList = new List<Account>();
            for(Integer i = 1; i < 4; i++){
                Account acc = new Account(
                    Name = 'office' + i
                    ,ParentId = ahAcc.Id
                    ,E_CL2PF_ZAGCYNUM__c = 'ay001'
                    ,E_COMM_VALIDFLAG__c = '1'
                    ,AGNTBR_NM__c = '東京'
                );
                ayAccList.add(acc);
            }
            insert ayAccList;

            // Contact 募集人
            if(!ayAccList.isEmpty()){
                atCon = new Contact(LastName = 'test',AccountId = ayAccList[0].Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAccList[0].E_CL2PF_ZAGCYNUM__c);
                atCon.E_CL3PF_AGNTNUM__c = 'at001';
                atCon.email = 'fstest@terrasky.ingtesting';
                insert atCon;
            }
        }
    }

    private static void updateAccOwner(Account acc){
        system.runAs(thisuser){
            acc.OwnerId = user.Id;
            update acc;
        }
    }

    /**
     * 保険契約ヘッダを複数作成
     *
     */
    public static List<E_Policy__c> createPolicys(){

        Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(E_Const.POLICY_RECORDTYPE_COLI);

        // 保険契約ヘッダを作成
        List<E_Policy__c> policys = new List<E_Policy__c>();
        for(Integer i = 0; i < 3; i++){
            E_Policy__c policy = new E_Policy__c();

            policy.Contractor__c = atCon.Id;
            policy.MainAgent__c = atCon.Id;
            policy.Insured__c = atCon.Id;
            policy.COMM_CHDRNUM__c = '99999999'+i+'|1';
            policy.MainAgentAccount__c = ayAccList[0].Id;
            policy.COMM_SPLITC01__c = 33;
            policy.SubAgent__c = atCon.Id;
            policy.SubAgentAccount__c = ayAccList[1].Id;
            policy.COMM_SPLITC02__c = 66;
            policy.COMM_ZEBKNM__c = 'テスト金融機関';
            policy.COMM_ZEBKACOW__c = '口座名義人';
            policy.COMM_BANKACCKEY__c = '1234567890';
            policy.COLI_ZADVPDCF__c = true;
            policy.COMM_STATCODE__c = 'Y';
            policy.COMM_ZEFREQ__c = '年 払';
            policy.COMM_ZECHNL__c = '団体扱';
            policy.COLI_ZNPTDDCF__c = false;
            policy.COLI_ZBKPODIS__c = '20';
            policy.COLI_ZCSHVAL__c = 10000;
            policy.COLI_ZLOANTOT__c = 1000;
            policy.COLI_ZEADVPRM__c = 100;
            policy.COLI_ZUNPREM__c = 10;
            policy.COLI_ZCVDCF__c = true;
            policy.COLI_ZUNPCF__c = true;
            policy.COLI_ZUNPDCF__c = true;
            policy.COLI_RCPTDT__c = '20171027';
            policy.COLI_AVPRMRCV__c = 10000;
            policy.COLI_REMNBANN__c = 1;
            policy.COLI_ZEPLTOT__c = 10000;
            policy.COLI_ZEPLPRN__c = 1000;
            policy.COLI_ZEPLINT__c = 100;
            policy.COLI_ZPLCAPT__c = '1ヶ月';
            policy.COLI_ZPLRATE__c = 123;
            policy.COMM_ZPLDCF__c = true;
            policy.COLI_ZEAPLTOT__c = 10000;
            policy.COLI_ZEAPLPRN__c = 10000;
            policy.COLI_ZEAPLINT__c = 1000;
            policy.COLI_ZAPLCAPT__c = '1ヶ月';
            policy.COLI_ZAPLRATE__c = 123;
            policy.COLI_ZAPLDCF__c = true;
            policy.COMM_CLNTNUM__c = '12345678';
            policy.COMM_ZCLNAME__c = '契約者A';
            policy.COMM_ZINSNAM__c = '被保険者A';
            policy.COMM_ZKNJSEX__c = '男性';
            policy.COMM_DOB__c = '20171027';
            policy.RecordTypeId = recTypeId;
            policy.COMM_SPLITC__c = 55;
            policy.COMM_CCDATE__c = '20171027';



            policys.add(policy);
        }

        insert policys;

        return policys;
    }

    /**
     * 保険契約ヘッダを複数作成(募集割合null,前納表示フラグfalse)
     *
     */
    public static List<E_Policy__c> createPolicys2(){


        // 保険契約ヘッダを作成
        List<E_Policy__c> policys = new List<E_Policy__c>();
        for(Integer i = 0; i < 3; i++){
            E_Policy__c policy = new E_Policy__c();

            policy.Contractor__c = atCon.Id;
            policy.MainAgent__c = atCon.Id;
            policy.Insured__c = atCon.Id;
            policy.COMM_CHDRNUM__c = '99999999'+i+'|2';
            policy.MainAgentAccount__c = ayAccList[0].Id;
            policy.SubAgent__c = atCon.Id;
            policy.SubAgentAccount__c = ayAccList[1].Id;
            policy.COMM_ZEBKNM__c = 'テスト金融機関';
            policy.COMM_ZEBKACOW__c = '口座名義人';
            policy.COMM_BANKACCKEY__c = '1234567890';
            policy.COLI_ZADVPDCF__c = false;
            policy.COMM_STATCODE__c = 'R';
            policy.COMM_ZEFREQ__c = '月払';
            policy.COMM_ZECHNL__c = '団体扱';
            policy.COLI_ZNPTDDCF__c = true;
            policy.COLI_ZBKPODIS__c = '10';
            policy.COLI_ZCSHVAL__c = 10000;
            policy.COLI_ZLOANTOT__c = 1000;
            policy.COLI_ZEADVPRM__c = 100;
            policy.COLI_ZUNPREM__c = 10;
            policy.COLI_ZCVDCF__c = true;
            policy.COLI_ZUNPCF__c = true;
            policy.COLI_ZUNPDCF__c = true;
            policy.COLI_ZEPLTOT__c = 10000;
            policy.COLI_ZEPLPRN__c = 1000;
            policy.COLI_ZEPLINT__c = 100;

            policys.add(policy);
        }

        insert policys;

        return policys;
    }


    //個人保険特約
    private static E_COVPF__c createCOV(E_Policy__c po){
        E_COVPF__c cov = new E_COVPF__c();
        cov.E_Policy__c = po.Id;
        cov.COMM_STATDATE__c = '20161111';
        cov.COMM_SUMINS__c = 100000;
        cov.COLI_INSTPREM__c = 100000;
        cov.COLI_ZCRIND__c = 'C';
        cov.COMM_ZCOVRNAM__c = '4テストタイプ';
        cov.COLI_ZDPTYDSP__c = true;
        cov.COMM_CHDRNUM__c = '99999999';
        cov.DCOLI_ZCLNAME__c = '契約者A';
        cov.DCOLI_ZINSNAM__c = '被保険者A';
        cov.COMM_SUMINS__c = 987654321;
        cov.DCOLI_SINSTAMT__c = 123456789;
        cov.DCOLI_OCCDATE__c = '20171027';
        cov.COMM_ZRCESDSC__c = '終身';
        cov.COMM_ZPCESDSC__c = '---------';

        return cov;
    }


    //保険顧客情報
    private static void createCrlpf(E_Policy__c policy){
        E_CRLPF__c crl = new E_CRLPF__c();

        crl.E_Policy__c = policy.Id;
        crl.BNYTYP__c = '01';
        crl.ZCLNAME__c = '照須快';
        crl.CLNTNUM__c = '99999999';
        crl.ZKNJSEX__c = '男性';
        crl.BNYPC__c = 100.00;
        crl.DOB__c = '11111111';
        crl.CRLPFCode__c = '99999999|TS|1';


        insert crl;
    }

    //保険取引履歴
    private static void createPtnpf(E_Policy__c policy){
        E_PTNPF__c ptn = new E_PTNPF__c();

        ptn.E_Policy__c = policy.Id;
        ptn.TRANDATE__c = '20171027';
        ptn.ZTRANDSC__c = 'テストクラス作成';

        insert ptn;
    }

    private static E_Policy__c getPolicy(String chdrnum){
        E_policy__c pol = [select id,MainAgentName__c,MainAgentAccountName__c,MainAgentAccountMRName__c,MainAgent__r.Account.AGNTBR_NM__c,COMM_SPLITC01__c,
                                SubAgentName__c,SubAgentAccountName__c,SubAgentAccountMRName__c,SubAgent__r.Account.AGNTBR_NM__c,COMM_SPLITC02__c,
                                Contractor__c,ContractorName__c,ContractorE_CLTPF_ZCLKNAME__c,ContractorCLTPF_CLNTNUM__c,ContractorE_CLTPF_CLTPCODE__c,
                                ContractorAddress__c,ContractorE_CLTPF_CLTPHONE01__c,ContractorE_CLTPF_DOB__c,ContractorAge__c,
                                InsuredName__c,InsuredE_CLTPF_ZCLKNAME__c,InsuredCLTPF_CLNTNUM__c,InsuredCLTPF_ZKNJSEX__c,InsuredE_CLTPF_CLTPCODE__c,
                                InsuredE_CLTPF_CLTPHONE01__c,InsuredCLTPF_DOB__c,InsuredAge__c,InsuredE_COMM_ZCLADDR__c,
                                COMM_ZEBKNM__c,COMM_ZEBKACOW__c,COMM_BANKACCKEY__c,COLI_ZADVPDCF__c,COMM_STATCODE__c,COMM_ZEFREQ__c,COMM_ZECHNL__c,
                                COLI_ZNPTDDCF__c,COMM_PTDATE__c,COLI_ZPCDTDCF__c,COLI_PCESDTE__c,COLI_ZBKPODIS__c,COMM_ZEBKBRNM__c,COMM_ZEBKACTP__c,
                                COLI_ZCSHVAL__c,COLI_ZEAPLTOT__c,COLI_ZEPLTOT__c,COLI_ZEADVPRM__c,COLI_ZUNPREM__c,COLI_ZCVDCF__c,COLI_ZUNPCF__c,
                                COLI_ZUNPDCF__c,COLI_ZLOANTOT__c,COLI_RCPTDT__c,COLI_AVPRMRCV__c,COLI_REMNBANN__c,COLI_ZEPLPRN__c,COLI_ZEPLINT__c,
                                COLI_ZPLCAPT__c,COMM_ZPLDCF__c,COLI_ZPLRATE__c,COLI_ZEAPLPRN__c,COLI_ZEAPLINT__c,COLI_ZAPLCAPT__c,COLI_ZAPLRATE__c,
                                COLI_ZAPLDCF__c,COMM_CLNTNUM__c,COMM_ZCLNAME__c,COMM_ZINSNAM__c,COMM_ZKNJSEX__c,COMM_DOB__c,COMM_CHDRNUM__c,COMM_SPLITC__c
                                from E_Policy__c where COMM_CHDRNUM__c =: chdrnum];

        return pol;
    }

    private static List<E_COVPF__c> getCovers(List<Id> covIds){
        return [ select Id,E_Policy__c,COMM_STATDATE__c,COMM_SUMINS__c,COLI_INSTPREM__c,COLI_ZCRIND__c,COMM_ZCOVRNAM__c,COLI_ZDPTYDSP__c,COMM_CHDRNUM__c,
                        DCOLI_ZCLNAME__c,DCOLI_ZINSNAM__c,DCOLI_SINSTAMT__c,DCOLI_OCCDATE__c,COMM_ZRCESDSC__c,COMM_ZPCESDSC__c,DCOLI_ZCOVRDSC__c,
                        COLI_MORTCLS__c,COLI_ZCOVRNAMES__c,COMM_CRTABLE2__c,DPIND__c,COLI_LIENCD__c,ZDCPFLG__c,COLI_ZSTDSUM__c,COLI_ZSUMINF__c,E_Policy__r.COMM_CCDATE__c
                        from E_COVPF__c where Id = : covIds];
    }

}