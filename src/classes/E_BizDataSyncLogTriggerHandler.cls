public with sharing class E_BizDataSyncLogTriggerHandler {
	
	//before insert
	public void onBeforeInsert(List<E_BizDataSyncLog__c> newList, Map<Id, E_BizDataSyncLog__c> newMap) {
		if (isExecute(newList, E_Const.EBIZ_ADD_GROUP_AGENCY)) {
			upStartDate(newList, E_Const.EBIZ_ADD_GROUP_AGENCY);
		} else if (isExecute(newList, CR_Const.EBIZ_RULE_AND_OUTSRC_AGENCY)) {
			upStartDate(newList, CR_Const.EBIZ_RULE_AND_OUTSRC_AGENCY);
		} else if (isExecute(newList, CR_Const.EBIZ_LOGKBN_ATTACHMENTS_DELETE)) {
			upStartDate(newList, CR_Const.EBIZ_LOGKBN_ATTACHMENTS_DELETE);
		}else{
			//	2016.07.15
			//	AMSプロジェクト以降BeforeInsert処理は不要　E_BizDataSyncBatch内で開始時間を設定
		}
	}
	
	//after insert
	public void onAfterInsert(List<E_BizDataSyncLog__c> newList, Map<Id, E_BizDataSyncLog__c> newMap) {
		if (isExecute(newList, E_Const.EBIZ_ADD_GROUP_AGENCY)) {
			//支社グループ割り当てBatch
			Type t = Type.forName('E_GroupAgencyBizLogicBatch');
			E_AbstractCommonBatch batch = (E_AbstractCommonBatch)t.newInstance();
			batch.setup('E_GroupAgencyBizLogicBatch');
			batch.setId(newList.get(0).Id);
			Database.executeBatch(batch, 200);
			
			//EBizログは1件ずつ更新する必要があるためDMLを分けて更新
			insert new E_BizDataSyncLog__c(kind__c = CR_Const.EBIZ_RULE_AND_OUTSRC_AGENCY);
			insert new E_BizDataSyncLog__c(kind__c = CR_Const.EBIZ_LOGKBN_ATTACHMENTS_DELETE);
			insert new E_BizDataSyncLog__c(kind__c = SC_Const.EBIZ_LOGKBN_AMS_SC_ShareSet);
		} else if (isExecute(newList, CR_Const.EBIZ_RULE_AND_OUTSRC_AGENCY)) {
			//代理店提出作成Batch
			Type t = Type.forName('CR_BizLogicBatch');
			E_AbstractCommonBatch batch = (E_AbstractCommonBatch)t.newInstance();
			batch.setup('CR_BizLogicBatch');
			batch.setId(newList.get(0).Id);
			Database.executeBatch(batch, 200);
		} else if (isExecute(newList, CR_Const.EBIZ_LOGKBN_ATTACHMENTS_DELETE)) {
			//添付ファイル削除とログステータス変更（非同期）
			CR_AttachmentsDelete.asyncDelete(newList[0].Id);
		} else if(isExecute(newList, I_Const.EBIZDATASYNC_KBN_PUSHOUT)) {
			// PushOut通知作成Batch
			I_PushOutBatch poBatch = new I_PushOutBatch(newList.get(0).Id);
			Database.executeBatch(poBatch, I_Const.BATCH_SIZE_PUSHOUT);
		} else {
			for(E_BizDataSyncLog__c rec:newList){
				//バッチOperationクラス名が設定されている場合に発火
				if(String.isNotBlank(rec.operationClassName__c)){
					E_BizDataSyncBatch batch = new E_BizDataSyncBatch(rec.Id);
					Database.executeBatch(batch, 200);		//バッチサイズもレコードから取得する変更も可能
				}
				
				if(System.Label.E_PROCESS_PUSHOUT == '1'){
					// PushOut通知対象の場合、PushOutのEBiz連携ログを登録する
					if(isExecutePushOut(rec)){
						insert new E_BizDataSyncLog__c(kind__c = I_Const.EBIZDATASYNC_KBN_PUSHOUT, NotificationKind__c = rec.Kind__c);
					}
				}
			}
			if(isExecute(newList, I_Const.EBIZDATASYNC_KBN_SALESRSLT_AGT)) {
				// 挙積情報関連Batch
				//　社内挙積情報Share作成をchain実行
				//EBizログは1件ずつ更新する必要があるためDMLを分けて更新
				insert new E_BizDataSyncLog__c(kind__c = I_Const.EBIZDATASYNC_KBN_SALESRSLT_EP, InquiryDate__c = newList.get(0).InquiryDate__c);
//				insert new E_BizDataSyncLog__c(kind__c = I_Const.EBIZDATASYNC_KBN_SALESPLN, InquiryDate__c = newList.get(0).InquiryDate__c);
			}
		}
	}
	
	/**
	 * 処理判断
	 */
	private boolean isExecute(List<E_BizDataSyncLog__c> newList, String kind) {
		boolean isExecute = false;
		if (newList.get(0).kind__c.equals(kind)) {
			system.assert(newList.size()==1, '連携ログは1件ずつ更新してください。');
			isExecute = true;
		}
		return isExecute;
	}
	
	/**
	 * PushOut通知の処理判断
	 */
	private Boolean isExecutePushOut(E_BizDataSyncLog__c rec){
		// 新契約の場合はバッチ処理のFinishから呼び出す
		if(I_Const.PUSHOUT_OPERATION_MAP.get(rec.Kind__c) != null
				&& rec.Kind__c != I_Const.EBIZDATASYNC_KBN_NEWPOLICY){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 処理開始日時更新
	 */
	private void upStartDate (List<E_BizDataSyncLog__c> newList, String kind) {
		for (E_BizDataSyncLog__c ebizlog : newList) {
			if (ebizlog.kind__c.equals(kind)) {
				ebizlog.datasyncstartdate__c = Datetime.now();
			}
		}
	}
}