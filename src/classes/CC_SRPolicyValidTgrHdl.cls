public with sharing class CC_SRPolicyValidTgrHdl{

	/**
	 * Trigger Handler実行確認用変数
	 */
	private static Set<String> runTgrSet = new Set<String>();

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<CC_SRPolicy__c > newList){
		if(!runTgrSet.contains('onBeforeInsert')){
			runTgrSet.add('onBeforeInsert');

			//validation check
			Map<Id,CC_SRPolicy__c> newMap = new Map<Id,CC_SRPolicy__c>();
			Map<Id,CC_SRPolicy__c> oldMap = new Map<Id,CC_SRPolicy__c>();
			setValidation(newList, newMap, oldMap);
		}
	}

	/**
	 * Bfter Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<CC_SRPolicy__c> newList, Map<Id, CC_SRPolicy__c> newMap){}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<CC_SRPolicy__c> newList, List<CC_SRPolicy__c> oldList, Map<Id, CC_SRPolicy__c> newMap, Map<Id, CC_SRPolicy__c> oldMap){
		if(!runTgrSet.contains('onBeforeUpdate')){
			runTgrSet.add('onBeforeUpdate');

			//validation check
			setValidation(newList, newMap, oldMap);
		}
	}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<CC_SRPolicy__c> newList, List<CC_SRPolicy__c> oldList, Map<Id, CC_SRPolicy__c> newMap, Map<Id, CC_SRPolicy__c> oldMap){}


	/**
	 * set validation
	 */
	public static void setValidation(List<CC_SRPolicy__c> newList, Map<Id, CC_SRPolicy__c> newMap, Map<Id, CC_SRPolicy__c> oldMap){
		//SR画面からInsert/Updateの場合、Validation再実行しない
		List<CC_SRPolicy__c> filteredNewList = new List<CC_SRPolicy__c>();
		for(CC_SRPolicy__c srPolicyObj : newList){
			if(!srPolicyObj.CC_PreventTriggerValid__c){
				filteredNewList.add(srPolicyObj);
			}else{
				//データ初期化
				srPolicyObj.CC_PreventTriggerValid__c = false;
			}
		}

		//set validation
		if(filteredNewList.size() > 0){
			CC_SRPolicyValid.setValidation(filteredNewList, newMap, oldMap);
		}
	}

}