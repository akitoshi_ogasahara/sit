global with sharing class CR_OutsrcTerminateExtender extends CR_AbstractExtender{

    private CR_OutsrcTerminateController controller; 

    /**
     * Constructor 
     */
    public CR_OutsrcTerminateExtender (CR_OutsrcTerminateController extension){
        super();
        this.controller = extension;
        this.helpMenukey = CR_Const.MENU_HELP_OUTSRC;
        this.helpPageJumpTo = CR_Const.MENU_HELP_OUTSRC_Terminate;
    }
    
    /** 
     * init
     */
    global override void init() {
        isSuccessOfInitValidate = false;
        if(getHasOutsrcViewPermission()==false){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG05_REQUIRED_PERMISSION_OUTSRC);        //'外部委託参照権限がありません'
        }else if(this.controller.record == null 
                || String.isBlank(this.controller.record.Id)){
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            this.viewingAgencyCode = this.controller.record.CR_Agency__r.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }
    }

    //外部委託　参照
    public String getUrlforView(){
        PageReference pr = Page.E_CROutSrcView;
        pr.getParameters().put('id', controller.record.Id);
        return pr.getUrl();
    }
    

    /**
     * 保存処理
     */
    public PageReference doSaveEx(){
        pageMessages.clearMessages();
        String beforeStatus = controller.record.Status__c; 
        controller.record.Status__c = CR_Const.OUTSRC_STATUS_09;
        Boolean saveResult = E_GenericDao.saveViaPage(controller.record);

        //保存成功時は参照ページへ
        if(saveResult){
            PageReference pr = Page.E_CROutsrcView;
            pr.getParameters().put('id', controller.record.Id);
            return pr;
        }else{
            controller.record.Status__c = beforeStatus;
        }

        return null;
    }
}