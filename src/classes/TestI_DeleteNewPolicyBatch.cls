@isTest
private with sharing class TestI_DeleteNewPolicyBatch {

	private static Account distribute;
	private static Account office;
	private static Contact agent;

	static testMethod void shouldShceduleBatch() {
		Test.startTest();
		new I_DeleteNewPolicyBatchSched().execute(null);
		Test.stopTest();
	}

	static testMethod void shouldDeleteNewPolicy() {
		Date today = Date.valueOf('2000-01-01');
		I_DeleteNewPolicyBatch batch = new I_DeleteNewPolicyBatch(today);
		System.assertEquals(19991031, batch.getReferenceNumber());
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();
		assertToDeletedRecords([SELECT CHDRNUM__C FROM E_NewPolicy__c]);
	}

	static testMethod void shouldDeleteNewPolicyWithDuplicateGroupRecords() {
		Date today = Date.valueOf('2000-01-01');
		I_DeleteNewPolicyBatch batch = new I_DeleteNewPolicyBatch(today);

		// Database.Statefulが実装されている必要があるためキャストして確認を行う。
		Database.Stateful stateFullBach = (Database.Stateful)batch;
		// バッチサイズ分割のため手動でバッチ処理を実行する。
		Database.QueryLocator locator = batch.start(null);

		Database.QueryLocatorIterator iterator = locator.iterator();
		Map<String, SObject> recordsByKey = new Map<String, SObject>();
		List<SObject> parsonalRecords = new List<SObject>();
		List<SObject> groupableRecords = new List<SObject>();
		// 団体と個人に分ける
		while (iterator.hasNext()) {
			SObject record = iterator.next();
			if (((E_NewPolicy__c)record).KFRAMEIND__c == 'Y') {
				groupableRecords.add(record);
			} else {
				parsonalRecords.add(record);
			}
		}
		batch.execute(null, parsonalRecords);
		batch.execute(null, groupableRecords);
		// 団体レコードを二回実施し、除外処理が正常に行われているか確認する。
		batch.execute(null, groupableRecords);
		
		Exception error;
		try {
			batch.finish(null);
		} catch(Exception e) {
			error = e;
		}
		// 実施済みグループキーの除外処理が行われていればエラーにならないはず
		System.assertEquals(true, error == null);
	}

	static testMethod void shouldThrownExceptionFromFinishProcess() {
		Date today = Date.valueOf('2000-01-01');
		I_DeleteNewPolicyBatch batch = new I_DeleteNewPolicyBatch(today);
		// エラーを発生させるため手動でバッチ処理を実行する。
		Database.QueryLocator locator = batch.start(null);

		// バッチ対象データを抽出
		Database.QueryLocatorIterator iterator = locator.iterator();
		List<SObject> scope = new List<SObject>();
		while (iterator.hasNext()) {
			scope.add(iterator.next());
		}
		// batch内でExceptionを発生させるためにレコードを削除する。
		delete scope;
		batch.execute(null, scope);
		Exception error;
		try {
			batch.finish(null);
		} catch (Exception e) {
			error = e;
		}
		// エラーが発生していることを確認する。
		System.assertEquals(false, error == null);
	}

	static void assertToDeletedRecords(List<E_NewPolicy__c> records) {
		Map<String, E_NewPolicy__c> policiesByKey = new Map<String, E_NewPolicy__c>();
		//distribute = TestI_TestUtil.createAccount(true,null);
		//office = TestI_TestUtil.createAccount(true,distribute);
		//agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		
		for (E_NewPolicy__c policy: records) {
			policiesByKey.put(policy.CHDRNUM__C, policy);
		}
		// [削除されているかどうか]を[取得できなかったどうか]で判断する
		// 個人/削除可
		
		System.assertEquals(true, policiesByKey.get('9000002') == null);
		System.assertEquals(true, policiesByKey.get('9000003') == null);
		System.assertEquals(true, policiesByKey.get('9000004') == null);

		// 個人/削除不可
		System.assertEquals(false, policiesByKey.get('9000001') == null); //1018修正
		System.assertEquals(false, policiesByKey.get('9000005') == null);
		System.assertEquals(false, policiesByKey.get('9000006') == null);
		System.assertEquals(false, policiesByKey.get('9000007') == null);
		System.assertEquals(false, policiesByKey.get('9000008') == null);
		System.assertEquals(false, policiesByKey.get('9000009') == null);

		// グループ/削除不可(住生対応の修正により)
		System.assertEquals(false, policiesByKey.get('9000010') == null);
		System.assertEquals(false, policiesByKey.get('9000011') == null);
		System.assertEquals(false, policiesByKey.get('9000012') == null);
		System.assertEquals(false, policiesByKey.get('9000013') == null);

		// グループ/申し込み中/削除不可
		System.assertEquals(false, policiesByKey.get('9000014') == null);
		System.assertEquals(false, policiesByKey.get('9000015') == null);
		System.assertEquals(false, policiesByKey.get('9000016') == null);
		System.assertEquals(false, policiesByKey.get('9000017') == null);

		// グループ/成立処理日/削除不可
		System.assertEquals(false, policiesByKey.get('9000018') == null);
		System.assertEquals(false, policiesByKey.get('9000019') == null);
		System.assertEquals(false, policiesByKey.get('9000020') == null);
		System.assertEquals(false, policiesByKey.get('9000021') == null);

		// グループ/謝絶/削除不可
		System.assertEquals(false, policiesByKey.get('9000022') == null);
		System.assertEquals(false, policiesByKey.get('9000023') == null);
		System.assertEquals(false, policiesByKey.get('9000024') == null);
		System.assertEquals(false, policiesByKey.get('9000025') == null);

		// グループ/取り下げ/削除不可
		System.assertEquals(false, policiesByKey.get('9000026') == null);
		System.assertEquals(false, policiesByKey.get('9000027') == null);
		System.assertEquals(false, policiesByKey.get('9000028') == null);
		System.assertEquals(false, policiesByKey.get('9000029') == null);

		// グループ/対象外ステータス/削除不可
		System.assertEquals(false, policiesByKey.get('9000030') == null);
		System.assertEquals(false, policiesByKey.get('9000031') == null);
		System.assertEquals(false, policiesByKey.get('9000032') == null);
		System.assertEquals(false, policiesByKey.get('9000033') == null);
		System.assertEquals(false, policiesByKey.get('9000034') == null);
		// 募集人が不要証券番号管理
		System.assertEquals(true, policiesByKey.get('9000035') == null);
		System.assertEquals(true, policiesByKey.get('9000036') == null);
		System.assertEquals(false, policiesByKey.get('9000037') == null);
		System.assertEquals(false, policiesByKey.get('9000038') == null);
	}

	@testSetup
	static void setup() {

		distribute = TestI_TestUtil.createAccount(true,null);
		office = TestI_TestUtil.createAccount(true,distribute);
		agent = createContact(true,office.id,'募集人1');
		// 削除可能なすべてのパターン(groupkey なし)
		List<E_NewPolicy__c> policies = new List<E_NewPolicy__c>();
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000001',
			ENTRYDATE__C = 19991001,
			RejectDate__c = 20001001,
			PTRN_TRDT__C = 20001001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000002',
			ENTRYDATE__C = 20001001,
			PTRN_TRDT__C = 19991001,
			RejectDate__c = 20001001,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000003',
			ENTRYDATE__C = 20001001,
			PTRN_TRDT__C = 20001001,
			RejectDate__c = 19991001,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000004',
			ENTRYDATE__C = 20001001,
			PTRN_TRDT__C = 20001001,
			RejectDate__c = 19991001,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));

		// 削除されないすべてのパターン(groupkey なし)
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000005',
			ENTRYDATE__C = 20001001,
			RejectDate__c = 19991001,
			PTRN_TRDT__C = 19991001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000006',
			ENTRYDATE__C = 19991001,
			PTRN_TRDT__C = 20001001,
			RejectDate__c = 19991001,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000007',
			ENTRYDATE__C = 19991001,
			PTRN_TRDT__C = 19991001,
			RejectDate__c = 20001001,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000008',
			ENTRYDATE__C = 19991001,
			PTRN_TRDT__C = 19991001,
			RejectDate__c = 20001001,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000009',
			ENTRYDATE__C = 19991001,
			PTRN_TRDT__C = 19991001,
			RejectDate__c = 20001001,
			STATCODE__C = null,
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null
		));

		// グルーピングしすべて削除可なパターン
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000010',
			ENTRYDATE__C = 19991001,
			RejectDate__c = 20001001,
			PTRN_TRDT__C = 20001001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199910
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000011',
			ENTRYDATE__C = 20001001,
			PTRN_TRDT__C = 19991001,
			RejectDate__c = 20001001,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199910
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000012',
			ENTRYDATE__C = 20001001,
			PTRN_TRDT__C = 20001001,
			RejectDate__c = 19991001,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199910
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000013',
			ENTRYDATE__C = 20001001,
			PTRN_TRDT__C = 20001001,
			RejectDate__c = 19991001,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199910
		));

		// グルーピングし申し込み中が削除不可なパターン
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000014',
			ENTRYDATE__C = 20000901,
			RejectDate__c = 19990901,
			PTRN_TRDT__C = 19990901,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199909
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000015',
			ENTRYDATE__C = 20000901,
			PTRN_TRDT__C = 19990901,
			RejectDate__c = 20000901,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199909
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000016',
			ENTRYDATE__C = 20000901,
			PTRN_TRDT__C = 20000901,
			RejectDate__c = 19990901,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199909
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000017',
			ENTRYDATE__C = 20000901,
			PTRN_TRDT__C = 20000901,
			RejectDate__c = 19990901,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199909
		));

		// グルーピングし成立処理日が削除不可なパターン
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000018',
			ENTRYDATE__C = 19990801,
			RejectDate__c = 20000801,
			PTRN_TRDT__C = 20000801,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199908
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000019',
			ENTRYDATE__C = 19990801,
			PTRN_TRDT__C = 20000801,
			RejectDate__c = 19990801,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199908
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000020',
			ENTRYDATE__C = 20000801,
			PTRN_TRDT__C = 20000801,
			RejectDate__c = 19990801,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199908
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000021',
			ENTRYDATE__C = 20000801,
			PTRN_TRDT__C = 20000801,
			RejectDate__c = 19990801,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199908
		));

		// グルーピングし謝絶が削除不可なパターン
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000022',
			ENTRYDATE__C = 19990701,
			RejectDate__c = null,
			PTRN_TRDT__C = null,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199907
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000023',
			ENTRYDATE__C = 20000701,
			PTRN_TRDT__C = 19990701,
			RejectDate__c = 20000701,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199907
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000024',
			ENTRYDATE__C = 19990701,
			PTRN_TRDT__C = 19990701,
			RejectDate__c = 20000701,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199907
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000025',
			ENTRYDATE__C = 20000701,
			PTRN_TRDT__C = 20000701,
			RejectDate__c = 19990701,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199907
		));

		// グルーピングし取り下げが削除不可なパターン
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000026',
			ENTRYDATE__C = 19990601,
			RejectDate__c = 20000601,
			PTRN_TRDT__C = 20000601,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199906
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000027',
			ENTRYDATE__C = 20000601,
			PTRN_TRDT__C = 19990601,
			RejectDate__c = 20000601,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199906
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000028',
			ENTRYDATE__C = 20000601,
			PTRN_TRDT__C = 20000601,
			RejectDate__c = 19990601,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199906
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000029',
			ENTRYDATE__C = 19990601,
			PTRN_TRDT__C = 19990601,
			RejectDate__c = 20000601,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199906
		));


		// グルーピングし対象外ステータスをもつレコードが存在するパターン
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000030',
			ENTRYDATE__C = 19990501,
			RejectDate__c = 20000501,
			PTRN_TRDT__C = 20000501,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199905
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000031',
			ENTRYDATE__C = 20000501,
			PTRN_TRDT__C = 19990501,
			RejectDate__c = 20000501,
			STATCODE__C = 'I',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199905
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000032',
			ENTRYDATE__C = 20000501,
			PTRN_TRDT__C = 20000501,
			RejectDate__c = 19990501,
			STATCODE__C = 'Q',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199905
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000033',
			ENTRYDATE__C = 20000501,
			PTRN_TRDT__C = 20000501,
			RejectDate__c = 19990501,
			STATCODE__C = 'Z',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199905
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000034',
			ENTRYDATE__C = 19990501,
			PTRN_TRDT__C = 19990501,
			RejectDate__c = 19990501,
			STATCODE__C = null,
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = 'Y',
			KPAKETNM__c = 199905
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000035',
			ENTRYDATE__C = 19990501,
			RejectDate__c = 19991001,
			PTRN_TRDT__C = 19991001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null,
			Agent__c = agent.Id
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000036',
			ENTRYDATE__C = 19990501,
			RejectDate__c = 19991001,
			PTRN_TRDT__C = 19991001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null,
			SubAgent__c = agent.Id
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000037',
			ENTRYDATE__C = 20000501,
			RejectDate__c = 19991001,
			PTRN_TRDT__C = 19991001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null,
			Agent__c = agent.Id
		));
		policies.add(new E_NewPolicy__c(
			CHDRNUM__C = '9000038',
			ENTRYDATE__C = 20000501,
			RejectDate__c = 19991001,
			PTRN_TRDT__C = 19991001,
			STATCODE__C = 'P',
			SYNCDATE__C = Datetime.valueOfGmt('2017-03-15 11:05:00.000'),
			KFRAMEIND__c = null,
			KPAKETNM__c = null,
			SubAgent__c = agent.Id
		));
		insert policies;
	}

	/**
	 * Contact作成
	 * @param isInsert: whether to insert
	 * @param accId: AccountId
	 * @param lastName: LastName
	 */
	public static Contact createContact(Boolean isInsert, Id accId, String lastName) {
		Contact src = new Contact(
			  AccountId = accId
			, LastName = lastName
			, E_CL3PF_AGNTNUM__c = '61167'
		);
		if(isInsert){
			insert src;
			src = [Select Id, Account.Name, LastName, E_AccParentCord__c From Contact Where Id =: src.Id];
		}
		return src;
	}
}