global with sharing class I_SalesPlanController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_SalesPlan__c record {get{return (E_SalesPlan__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public I_SalesPlanExtender getExtender() {return (I_SalesPlanExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public I_SalesPlanController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.pdfTitle}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:300px;border: solid 2px #e64415; font-size: 15px;height:20px;');
        tmpPropMap.put('outputStyle','width:300px;color: #ee7f00; font-size: 15px;');
		tmpPropMap.put('Component__Width','300');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','pdfTitle');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','13');
		tmpPropMap.put('Component__Left','71');
		appComponentProperty.put('pdfTitle',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.spInsType_h1}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:72px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:72px;ime-mode:disabled;text-align: right;font-size:9px;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','spInsType_h1_number');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','137');
		tmpPropMap.put('Component__Left','346');
		appComponentProperty.put('spInsType_h1_number',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.spInsType_h2}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:75px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:75px;ime-mode:disabled;text-align: right;font-size:9px;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','spInsType_h2_number');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','137');
		tmpPropMap.put('Component__Left','440');
		appComponentProperty.put('spInsType_h2_number',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.spInsType_ly}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','false');
        tmpPropMap.put('inputStyle','');
        tmpPropMap.put('outputStyle','font-size:9px; text-align:right;');
		tmpPropMap.put('Component__Width','75');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component260');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','138');
		tmpPropMap.put('Component__Left','254');
		appComponentProperty.put('Component260',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.spInsType_total}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','false');
        tmpPropMap.put('inputStyle','');
        tmpPropMap.put('outputStyle','font-size:9px; text-align:right;');
		tmpPropMap.put('Component__Width','75');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','spInsType_total');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','138');
		tmpPropMap.put('Component__Left','536');
		appComponentProperty.put('spInsType_total',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.spInsType_cy}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','false');
        tmpPropMap.put('inputStyle','');
        tmpPropMap.put('outputStyle','font-size:9px; text-align:right;');
		tmpPropMap.put('Component__Width','75');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component263');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','138');
		tmpPropMap.put('Component__Left','628');
		appComponentProperty.put('Component263',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.anp_h1}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:72px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:72px;ime-mode:disabled;text-align: right;font-size:9px;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','anp_h1_number');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','178');
		tmpPropMap.put('Component__Left','346');
		appComponentProperty.put('anp_h1_number',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.anp_h2}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:75px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:75px;ime-mode:disabled;text-align: right;font-size:9px;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','anp_h2_number');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','178');
		tmpPropMap.put('Component__Left','440');
		appComponentProperty.put('anp_h2_number',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.anp_ly}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','false');
        tmpPropMap.put('inputStyle','');
        tmpPropMap.put('outputStyle','font-size:9px; text-align:right;');
		tmpPropMap.put('Component__Width','75');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component261');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','179');
		tmpPropMap.put('Component__Left','254');
		appComponentProperty.put('Component261',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.anp_total}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','false');
        tmpPropMap.put('inputStyle','');
        tmpPropMap.put('outputStyle','font-size:9px; text-align:right;');
		tmpPropMap.put('Component__Width','75');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','anp_total');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','179');
		tmpPropMap.put('Component__Left','536');
		appComponentProperty.put('anp_total',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.anp_cy}');
        tmpPropMap.put('fieldName','SkyEditor2__Number__c');
        tmpPropMap.put('forInput','false');
        tmpPropMap.put('inputStyle','');
        tmpPropMap.put('outputStyle','font-size:9px; text-align:right;');
		tmpPropMap.put('Component__Width','75');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component264');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','179');
		tmpPropMap.put('Component__Left','628');
		appComponentProperty.put('Component264',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.iqa_h1}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:30px;height:12px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:30px;height:12px;ime-mode:disabled;text-align: right;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','iqa_h1_percent');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','209');
		tmpPropMap.put('Component__Left','386');
		appComponentProperty.put('iqa_h1_percent',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.iqa_h2}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:30px;height:12px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:30px;height:12px;ime-mode:disabled;text-align: right;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','iqa_h2_percent');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','209');
		tmpPropMap.put('Component__Left','484');
		appComponentProperty.put('iqa_h2_percent',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.iqa_total}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:30px;height:12px;ime-mode:disabled;font-size: 11px;text-align: right;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:30px;height:12px;ime-mode:disabled;text-align: right;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','iqa_total');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','209');
		tmpPropMap.put('Component__Left','576');
		appComponentProperty.put('iqa_total',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('labelWidth','78');
        tmpPropMap.put('colWidth','56');
        tmpPropMap.put('data','{!Extender.planData}');
        tmpPropMap.put('lineHight','17');
        tmpPropMap.put('lineHight2','30');
        tmpPropMap.put('startMonth','1');
        tmpPropMap.put('isPDF','{!Extender.isPdf}');
		tmpPropMap.put('Component__Width','465');
		tmpPropMap.put('Component__Height','195');
		tmpPropMap.put('Component__id','Component244');
		tmpPropMap.put('Component__Name','IRIS_SalesPlanning');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','307');
		tmpPropMap.put('Component__Left','272');
		appComponentProperty.put('Component244',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('labelWidth','78');
        tmpPropMap.put('colWidth','56');
        tmpPropMap.put('data','{!Extender.planData}');
        tmpPropMap.put('lineHight','17');
        tmpPropMap.put('lineHight2','30');
        tmpPropMap.put('startMonth','7');
        tmpPropMap.put('isPDF','{!Extender.isPdf}');
		tmpPropMap.put('Component__Width','465');
		tmpPropMap.put('Component__Height','195');
		tmpPropMap.put('Component__id','Component246');
		tmpPropMap.put('Component__Name','IRIS_SalesPlanning');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','539');
		tmpPropMap.put('Component__Left','273');
		appComponentProperty.put('Component246',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.memo}');
        tmpPropMap.put('fieldName','SkyEditor2__LongTextArea__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:312px;height:205px;resize: none;overflow:hidden;overflow-x:hidden;overflow-y:hidden;border: solid 2px #e64415;line-height:16px;font-size:11px;padding-left: 6px;');
        tmpPropMap.put('outputStyle','width:324px;height:205px;resize: none;overflow:hidden;overflow-x:hidden;overflow-y:hidden;white-space:normal;padding-left:4px;padding-top:3px;');
		tmpPropMap.put('Component__Width','318');
		tmpPropMap.put('Component__Height','205');
		tmpPropMap.put('Component__id','memo');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','764');
		tmpPropMap.put('Component__Left','396');
		appComponentProperty.put('memo',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('values','{!Extender.supportContentList}');
        tmpPropMap.put('rows','9');
        tmpPropMap.put('labelStyle','width:156px;height:23.5px;white-space:nowrap;font-size: 10px;');
		tmpPropMap.put('Component__Width','15');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component239');
		tmpPropMap.put('Component__Name','E_VerticalLabels');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','765');
		tmpPropMap.put('Component__Left','79');
		appComponentProperty.put('Component239',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.agencyName}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width: 263px; height: 27px; font-size: 10px; border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width: 263px; height: 27px; font-size: 10px; margin-top: 7px; padding-left: 3px;');
		tmpPropMap.put('Component__Width','263');
		tmpPropMap.put('Component__Height','27');
		tmpPropMap.put('Component__id','agencyName');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','1010');
		tmpPropMap.put('Component__Left','80');
		appComponentProperty.put('agencyName',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.agentName}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:207px;height:27px;font-size: 10px;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:207px;height:27px;font-size: 10px;margin-top:7px; padding-left: 3px;');
		tmpPropMap.put('Component__Width','207');
		tmpPropMap.put('Component__Height','27');
		tmpPropMap.put('Component__id','agentName');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','1010');
		tmpPropMap.put('Component__Left','347');
		appComponentProperty.put('agentName',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('record','{!Extender.mrName}');
        tmpPropMap.put('fieldName','SkyEditor2__Text__c');
        tmpPropMap.put('forInput','{!Not(Extender.isPdf)}');
        tmpPropMap.put('inputStyle','width:160px;height:27px;font-size: 10px;border: solid 2px #e64415;');
        tmpPropMap.put('outputStyle','width:160px;height:27px;font-size: 10px;margin-top:7px; padding-left: 3px;');
		tmpPropMap.put('Component__Width','160');
		tmpPropMap.put('Component__Height','27');
		tmpPropMap.put('Component__id','mrName');
		tmpPropMap.put('Component__Name','E_DynamicField');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','1010');
		tmpPropMap.put('Component__Left','558');
		appComponentProperty.put('mrName',tmpPropMap);


		SObjectField f;

		f = E_AgencySalesResults__c.fields.IQA_LY__c;
		f = E_AgencySalesResults__c.fields.IQA__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_SalesPlan__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_SalesPlan__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('SalesResult__r.IQA_LY__c');
			mainQuery.addFieldAsOutput('SalesResult__r.IQA__c');
			mainQuery.addFieldAsOutput('WK_SalesPlan__c');
			mainQuery.addFieldAsOutput('ParentAccount__c');
			mainQuery.addFieldAsOutput('SalesResult__c');
			mainQuery.addFieldAsOutput('SalesResult__r.NBCANP_SpInsTypeLYYTD__c');
			mainQuery.addFieldAsOutput('SalesResult__r.NBCANP_SpInsTypeYTD__c');
			mainQuery.addFieldAsOutput('SalesResult__r.NBCANP_LYYTD__c');
			mainQuery.addFieldAsOutput('SalesResult__r.NBCANP_YTD__c');
			mainQuery.addFieldAsOutput('SalesResult__r.AgencyName__c');
			mainQuery.addFieldAsOutput('SalesResult__r.YYYYMM__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutFree; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			sve_ClassName = 'I_SalesPlanController';
			extender = new I_SalesPlanExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}