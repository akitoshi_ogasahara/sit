@isTest
private class TestI_MovieSearchBoxDetailController {

	private static testMethod void test() {
		Test.startTest();
		I_MovieSearchBoxDetailController ctrl = new I_MovieSearchBoxDetailController();
		System.assertEquals(I_Const.LIST_MAX_ROWS, ctrl.getListMaxRows());
		System.assertEquals(null, ctrl.getMovieRows());
		List<I_MovieSearchResult> rows = new List<I_MovieSearchResult>();
		ctrl.setMovieRows(rows);
		System.assertEquals(rows, ctrl.getMovieRows());

		Integer rowCount = I_MovieSearchBoxDetailController.DEFAULT_DISPLAY_ROW_COUNT;
		System.assertEquals(rowCount, ctrl.rowCount);
		ctrl.addRows();
		rowCount = rowCount + I_MovieSearchBoxDetailController.ADDITIONAL_ROW_COUNT;
		System.assertEquals(rowCount, ctrl.rowCount);

		Integer loopCount = (I_Const.LIST_MAX_ROWS - rowCount / I_MovieSearchBoxDetailController.ADDITIONAL_ROW_COUNT) + 1;
		for (Integer i = 0; i < loopCount; i++) {
			ctrl.addRows();
		}
		System.assertEquals(I_Const.LIST_MAX_ROWS, ctrl.rowCount);
		
		Test.stopTest();

	}

}