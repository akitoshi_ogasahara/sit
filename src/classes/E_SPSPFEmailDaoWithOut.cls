/**
 * E_SPSPFEmai__cデータアクセスオブジェクト(wihtout sharing)
 * CreatedDate 2015/06/05
 * @Author Terrasky
 */
public without sharing class E_SPSPFEmailDaoWithOut {
    
    /**
    * 指定された年月に前方一致するメール送信NO（数値のみ）の降順の1件目を取得
    */
    public static E_SPSPFEmail__c getMaxMailNumberByYM(String newCreatedYM){
        E_SPSPFEmail__c rec = null;
        string likeStr = newCreatedYM + '%';
        List<E_SPSPFEmail__c> recs = [
            SELECT
                MailNunmer_numOnly__c
            FROM
                E_SPSPFEmail__c
            WHERE 
                MailNunmer__c like :likeStr
            ORDER BY
                MailNunmer_numOnly__c DESC
            LIMIT 1
        ];
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
    }

}