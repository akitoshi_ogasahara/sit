public with sharing class I_SRAgentController {

	// 代理店格SFID
	public String agencyId {get; set;}
	// 代理店事務所SFID
	public String officeId {get; set;}
	//営業日
	public String businessDate {get;private set;}
	//支社コード
	public String brNo {get; set;}
	//メッセージ
	public String[] paramMessages {get; set;}
	public Map<String, String> msg {
		get{
			if(msg == null){
				// パラメータよりMapを作成
				while(this.paramMessages.size() < 1) {
					this.paramMessages.add('');
				}
				msg = new Map<String, String>();
				msg.put('AMS|SR|007', this.paramMessages[0]);
				msg.put('AMS|SR|008', this.paramMessages[1]);
			}
			return msg;
		}
		set;
	}
	// 募集人リスト
	public List<AgentListRow> AgentRows {get{
		if(this.AgentRows != null){
			return this.AgentRows;
		}
		getAgentRecList();
		// ソート
		//I_SRAgentController.list_sortType  = sortType;
		//I_SRAgentController.list_sortIsAsc = sortIsAsc;
		//AgentRows.sort();
		return AgentRows;
	} private set;}

	// リスト行数
	public Integer rowCount {get; set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;
	// ダウンロード
	public String sWhere{get; set;}

	//MAX表示件数
	public Integer getListMaxRows(){
//		return I_Const.LIST_MAX_ROWS;
		return 500;
	}
	//営業日
	public String getBizDate(){
		return E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
	}
	public Boolean getIsCommonGateway(){
		return E_AccessController.getInstance().isCommonGateway();
	}

	// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE	= 'stype';

	//ソートキー
	public static final String SORT_TYPE_AGENTCODE			= 'agentCode';
	public static final String SORT_TYPE_KMOFCODE			= 'kmofCode';
	public static final String SORT_TYPE_AGENTNAME 	 		= 'agentName';
	public static final String SORT_TYPE_AGENTTYPE 			= 'agentType';
	public static final String SORT_TYPE_OFFICENAME			= 'officeName';
	public static final String SORT_TYPE_CNVSLICENSE 		= 'cnvsLicense';
	public static final String SORT_TYPE_STARTDATE 			= 'startDate';
	public static final String SORT_TYPE_CNVSLICENSEVAR 	= 'cnvsLicenseVar';
	public static final String SORT_TYPE_CAPACITY 			= 'capacity';
	public static final String SORT_TYPE_PASSDATE 			= 'passDate';
	public static final String SORT_TYPE_PASSCODE 			= 'passCode';

	public String messages{get;private set;}

	//データ基準日
	public String bizDate{
		get{
			if(bizDate != null) return this.bizDate;
			String inquiryD = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
			String year = inquiryD.left(4);
			String month = inquiryD.mid(4, 1)+inquiryD.mid(5, 1);
			String day = inquiryD.mid(6, 1)+inquiryD.mid(7, 1);
			return year+'/'+month+'/'+day;
		} set;
	}

	//コンストラクタ
	public I_SRAgentController() {
		// デフォルトソート 募集人番号・昇順
		sortType = SORT_TYPE_AGENTCODE;
		sortIsAsc = true;
		messages = '';

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try{
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > getListMaxRows()){
				pRows = getListMaxRows();
			}
			rowCount = pRows;
		} catch(Exception e){
		}
	}

	public void readyAction(){
		getAgentRecList();
		// ソート
		I_SRAgentController.list_sortType  = sortType;
		I_SRAgentController.list_sortIsAsc = sortIsAsc;
		AgentRows.sort();
	}



	//募集人データ取得
	public void getAgentRecList(){
		//表示用リスト
		AgentRows = new List<AgentListRow>();

		sWhere = 'Hierarchy__c = \'AT\' AND BusinessDate__c = \'' + getBizDate() + '\' ';
		sWhere += 'AND AgentCode__c != null ';
		//代理店格
		if(!String.isBlank(agencyid)){
			sWhere += 'AND ParentAccount__c = \'' + agencyId + '\'';
		//事務所
		}else if(!String.isBlank(officeid)){
			sWhere += 'AND Account__c = \'' + officeId + '\'';
		//その他はリストを作成しない
		}else{
			return;
		}
		//代理店格IDを取得
		List<E_AgencySalesResults__c> sResults = E_AgencySalesResultsDaoWithout.getSRAgentRecs(sWhere);

		for(E_AgencySalesResults__c eRec : sResults){
			AgentListRow ageRow = new AgentListRow();

			ageRow.agentCode 		= eRec.AgentCode__c;
			ageRow.kmofCode 		= eRec.XHAT_KMOFCODE__c;
			ageRow.agentName 		= eRec.AgentNm__c;
			ageRow.agentType 		= eRec.AgentType__c;
			ageRow.officeName 		= eRec.BelongOfficeName__c;
			ageRow.cnvsLicense 		= eRec.CnvsLicense_IRIS__c;
			ageRow.startDate 		= eRec.ComStartDate__c;
			ageRow.cnvsLicenseVar 	= eRec.CnvsLicenseVariableIns__c;
			ageRow.capacity 		= eRec.AGED_CAP__c;
			ageRow.agentid		= eRec.AgentId__c;
			ageRow.brNo			= eRec.BrCode__c;

			//最大件数の際は終了
			if(AgentRows.size() == getListMaxRows()){
				messages = String.format(msg.get('AMS|SR|007'),new List<String>{String.valueOf(getListMaxRows())});
				break;
			}
			AgentRows.add(ageRow);
		}
		//営業日
		if(sResults <> null && !sResults.isEmpty()){
			businessDate = sResults[0].BusDate__c;
		}
	}

	/**
	 * ソート
	 */
	public PageReference sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;
		}
		
		I_SRAgentController.list_sortType  = sortType;
		I_SRAgentController.list_sortIsAsc = sortIsAsc;
		AgentRows.sort();

		return null;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > getListMaxRows()){
			rowCount = getListMaxRows();
		}
		return null;
	}

	//総行数取得
	public Integer getTotalRows(){
		Integer totalRow = 0;

		totalRow = AgentRows  == null ? 0 : AgentRows.size();
		return totalRow;
	}

	//CSVエクスポート
	//public PageReference ExportCSV(){

	//	PageReference pr = Page.IRIS_ExportSRAgent;

	//	pr.getParameters().put('soqlWhere',sWhere);
	//	return pr;
	//}

	/**
	 * 内部クラス
	 */
	public class AgentListRow implements Comparable {
		//募集人番号
		public String agentCode {get; set;}
		//募集人登録番号
		public String kmofCode {get; set;}
		//募集人名
		public String agentName {get; set;}
		//登録区分
		public String agentType {get; set;}
		//所属事務所名
		public String officeName {get; set;}
		//当社商品の募集資格
		public String cnvsLicense {get; set;}
		//募集開始日
		public String startDate {get; set;}
		//当社での変額資格
		public String cnvsLicenseVar {get; set;}
		//専門資格(合格年月/合格番号)
		public String capacity {get; set;}
		//ID
		public String agentid {get; set;}
		//支社コード
		public String brNo {get; set;}

		//コンストラクタ
		public AgentListRow(){
			agentCode 		= '';
			kmofCode 		= '';
			agentName 		= '';
			agentType 		= '';
			officeName 		= '';
			cnvsLicense 	= '';
			startDate 		= '';
			cnvsLicenseVar 	= '';
			capacity 		= '';
			agentid	= '';
			brNo = '';
		}

		public Integer compareTo(Object compareTo) {
			AgentListRow compareToRow = (AgentListRow)compareTo;
			Integer result = 0;

			//募集人番号
			if(list_sortType == SORT_TYPE_AGENTCODE){
				result = I_Util.compareToString(agentCode, compareToRow.agentCode);
			//募集人登録番号
			}else if(list_sortType == SORT_TYPE_KMOFCODE){
				result = I_Util.compareToString(kmofCode, compareToRow.kmofCode);
			//募集人名
			}else if(list_sortType == SORT_TYPE_AGENTNAME){
				result = I_Util.compareToString(agentName, compareToRow.agentName);
			//登録区分
			}else if(list_sortType == SORT_TYPE_AGENTTYPE){
				result = I_Util.compareToString(agentType, compareToRow.agentType);
			//所属事務所名
			}else if(list_sortType == SORT_TYPE_OFFICENAME){
				result = I_Util.compareToString(officeName, compareToRow.officeName);
			//当社商品の募集資格
			}else if(list_sortType == SORT_TYPE_CNVSLICENSE){
				result = I_Util.compareToString(cnvsLicense, compareToRow.cnvsLicense);
			//募集開始日
			}else if(list_sortType == SORT_TYPE_STARTDATE){
				result = I_Util.compareToString(startDate, compareToRow.startDate);
			//当社での変額資格
			}else if(list_sortType == SORT_TYPE_CNVSLICENSEVAR){
				result = I_Util.compareToString(cnvsLicenseVar, compareToRow.cnvsLicenseVar);
			//専門資格(合格年月/合格番号)
			}else if(list_sortType == SORT_TYPE_CAPACITY){
				result = I_Util.compareToString(capacity, compareToRow.capacity);
			}
			return list_sortIsAsc ? result : result * (-1);
		}
	}
}