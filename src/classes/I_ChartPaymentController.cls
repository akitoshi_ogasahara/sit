public with sharing class I_ChartPaymentController {

	public Id paramSalesResultsId {get; set;}				// 代理店挙積情報ID
	transient public String[] paramDisclaimer {get; set;}	// 表示ディスクレーマ
	public Map<String, String> disclaimers{					// 表示ディスクレーマ(表示用)
		get{
			if(disclaimers == null){
				// パラメータよりMapを作成
				while(this.paramDisclaimer.size() < 2) {
					this.paramDisclaimer.add('');
				}
				disclaimers = new Map<String, String>();
				disclaimers.put('AMS|SR|003', this.paramDisclaimer[0]);
				disclaimers.put('AMS|SR|013', this.paramDisclaimer[1]);
			}
			return disclaimers;
		}
		set;
	}
	//kawano 本番対応
	public String dispYear {
		get{
			return Label.E_SR_YYYY;
		}
		private set;
	}

	transient public String json {get; set;}				// JSONデータ

	/**
	 * コンストラクタ
	 */
	public I_ChartPaymentController(){
		this.paramSalesResultsId	= null;											// 代理店挙積情報ID
		this.paramDisclaimer		= new List<String>();							// 表示ディスクレーマ

	    this.json					= '';											// JSONデータ
	}

	/**
	 * JS用値設定の取得(リモートアクション)
	 */
	@readOnly
	@RemoteAction
	public static String getJSParam(Id pSalesResultsId){
		I_ChartPaymentController cls	= new I_ChartPaymentController();
		cls.paramSalesResultsId			= pSalesResultsId;	// 代理店挙積情報ID
		cls.getJSParameter();								// JS用値設定の取得
		return cls.json;									// JSONデータ
	}

	/**
	 * JS用値設定の取得
	 */
	public void getJSParameter(){
		// JSONデータ
		this.json				= getJSON();
	}

	/**
	 * JSONデータの取得
	 */
	public String getJSON(){
		List<String> values			= new List<String>();
		List<String> subfields		= new List<String>();
		List<String> lines			= new List<String>();
		List<String> fields			= new List<String>();
		Integer year				= 0;

		// 代理店挙積情報
		E_AgencySalesResults__c agencySalesResults = E_AgencySalesResultsDao.getRecTypeDById(this.paramSalesResultsId);

		// 年取得
		try{
			//year = Integer.valueOf(agencySalesResults.YYYYMM__c.left(4));
			//kawano 本番対応
			year = Integer.valueOf(dispYear);
		}catch(Exception e){ return ''; }

		fields		= new List<String>();
		fields.add('"year":' + I_Util.toStr(year) + '');
		values		= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.Payment_Jan__c));	// 振込手数料額　当年1月
		values.add(I_Util.toStr(agencySalesResults.Payment_Feb__c));	// 振込手数料額　当年2月
		values.add(I_Util.toStr(agencySalesResults.Payment_Mar__c));	// 振込手数料額　当年3月
		values.add(I_Util.toStr(agencySalesResults.Payment_Apr__c));	// 振込手数料額　当年4月
		values.add(I_Util.toStr(agencySalesResults.Payment_May__c));	// 振込手数料額　当年5月
		values.add(I_Util.toStr(agencySalesResults.Payment_Jun__c));	// 振込手数料額　当年6月
		values.add(I_Util.toStr(agencySalesResults.Payment_Jul__c));	// 振込手数料額　当年7月
		values.add(I_Util.toStr(agencySalesResults.Payment_Aug__c));	// 振込手数料額　当年8月
		values.add(I_Util.toStr(agencySalesResults.Payment_Sep__c));	// 振込手数料額　当年9月
		values.add(I_Util.toStr(agencySalesResults.Payment_Oct__c));	// 振込手数料額　当年10月
		values.add(I_Util.toStr(agencySalesResults.Payment_Nov__c));	// 振込手数料額　当年11月
		values.add(I_Util.toStr(agencySalesResults.Payment_Dec__c));	// 振込手数料額　当年12月
		fields.add('"val":[' + String.join(values,',') + ']');
		lines.add('"transfer":{' + String.join(fields,',') + '}');

		fields		= new List<String>();
		fields.add('"year":' + I_Util.toStr(year) + '');
		values		= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.FY_Jan__c));			// 初年度手数料額　当年1月
		values.add(I_Util.toStr(agencySalesResults.FY_Feb__c));			// 初年度手数料額　当年2月
		values.add(I_Util.toStr(agencySalesResults.FY_Mar__c));			// 初年度手数料額　当年3月
		values.add(I_Util.toStr(agencySalesResults.FY_Apr__c));			// 初年度手数料額　当年4月
		values.add(I_Util.toStr(agencySalesResults.FY_May__c));			// 初年度手数料額　当年5月
		values.add(I_Util.toStr(agencySalesResults.FY_Jun__c));			// 初年度手数料額　当年6月
		values.add(I_Util.toStr(agencySalesResults.FY_Jul__c));			// 初年度手数料額　当年7月
		values.add(I_Util.toStr(agencySalesResults.FY_Aug__c));			// 初年度手数料額　当年8月
		values.add(I_Util.toStr(agencySalesResults.FY_Sep__c));			// 初年度手数料額　当年9月
		values.add(I_Util.toStr(agencySalesResults.FY_Oct__c));			// 初年度手数料額　当年10月
		values.add(I_Util.toStr(agencySalesResults.FY_Nov__c));			// 初年度手数料額　当年11月
		values.add(I_Util.toStr(agencySalesResults.FY_Dec__c));			// 初年度手数料額　当年12月
		fields.add('"val":[' + String.join(values,',') + ']');
		lines.add('"fy":{' + String.join(fields,',') + '}');

		fields		= new List<String>();
		fields.add('"year":' + I_Util.toStr(year) + '');
		values		= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.ASY_Jan__c));		// 次年度以降手数料額　当年1月
		values.add(I_Util.toStr(agencySalesResults.ASY_Feb__c));		// 次年度以降手数料額　当年2月
		values.add(I_Util.toStr(agencySalesResults.ASY_Mar__c));		// 次年度以降手数料額　当年3月
		values.add(I_Util.toStr(agencySalesResults.ASY_Apr__c));		// 次年度以降手数料額　当年4月
		values.add(I_Util.toStr(agencySalesResults.ASY_May__c));		// 次年度以降手数料額　当年5月
		values.add(I_Util.toStr(agencySalesResults.ASY_Jun__c));		// 次年度以降手数料額　当年6月
		values.add(I_Util.toStr(agencySalesResults.ASY_Jul__c));		// 次年度以降手数料額　当年7月
		values.add(I_Util.toStr(agencySalesResults.ASY_Aug__c));		// 次年度以降手数料額　当年8月
		values.add(I_Util.toStr(agencySalesResults.ASY_Sep__c));		// 次年度以降手数料額　当年9月
		values.add(I_Util.toStr(agencySalesResults.ASY_Oct__c));		// 次年度以降手数料額　当年10月
		values.add(I_Util.toStr(agencySalesResults.ASY_Nov__c));		// 次年度以降手数料額　当年11月
		values.add(I_Util.toStr(agencySalesResults.ASY_Dec__c));		// 次年度以降手数料額　当年12月
		fields.add('"val":[' + String.join(values,',') + ']');
		lines.add('"asy":{' + String.join(fields,',') + '}');

		fields		= new List<String>();
		fields.add('"year":' + I_Util.toStr(year) + '');
		values		= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.OT_Jan__c));			// その他手数料額　当年1月
		values.add(I_Util.toStr(agencySalesResults.OT_Feb__c));			// その他手数料額　当年2月
		values.add(I_Util.toStr(agencySalesResults.OT_Mar__c));			// その他手数料額　当年3月
		values.add(I_Util.toStr(agencySalesResults.OT_Apr__c));			// その他手数料額　当年4月
		values.add(I_Util.toStr(agencySalesResults.OT_May__c));			// その他手数料額　当年5月
		values.add(I_Util.toStr(agencySalesResults.OT_Jun__c));			// その他手数料額　当年6月
		values.add(I_Util.toStr(agencySalesResults.OT_Jul__c));			// その他手数料額　当年7月
		values.add(I_Util.toStr(agencySalesResults.OT_Aug__c));			// その他手数料額　当年8月
		values.add(I_Util.toStr(agencySalesResults.OT_Sep__c));			// その他手数料額　当年9月
		values.add(I_Util.toStr(agencySalesResults.OT_Oct__c));			// その他手数料額　当年10月
		values.add(I_Util.toStr(agencySalesResults.OT_Nov__c));			// その他手数料額　当年11月
		values.add(I_Util.toStr(agencySalesResults.OT_Dec__c));			// その他手数料額　当年12月
		fields.add('"val":[' + String.join(values,',') + ']');
		lines.add('"other":{' + String.join(fields,',') + '}');

		fields		= new List<String>();
		fields.add('"year":' + I_Util.toStr(year) + '');
		values		= new List<String>();
		values.add(I_Util.toStr(agencySalesResults.CB_Jan__c));			// 戻入金額　当年1月
		values.add(I_Util.toStr(agencySalesResults.CB_Feb__c));			// 戻入金額　当年2月
		values.add(I_Util.toStr(agencySalesResults.CB_Mar__c));			// 戻入金額　当年3月
		values.add(I_Util.toStr(agencySalesResults.CB_Apr__c));			// 戻入金額　当年4月
		values.add(I_Util.toStr(agencySalesResults.CB_May__c));			// 戻入金額　当年5月
		values.add(I_Util.toStr(agencySalesResults.CB_Jun__c));			// 戻入金額　当年6月
		values.add(I_Util.toStr(agencySalesResults.CB_Jul__c));			// 戻入金額　当年7月
		values.add(I_Util.toStr(agencySalesResults.CB_Aug__c));			// 戻入金額　当年8月
		values.add(I_Util.toStr(agencySalesResults.CB_Sep__c));			// 戻入金額　当年9月
		values.add(I_Util.toStr(agencySalesResults.CB_Oct__c));			// 戻入金額　当年10月
		values.add(I_Util.toStr(agencySalesResults.CB_Nov__c));			// 戻入金額　当年11月
		values.add(I_Util.toStr(agencySalesResults.CB_Dec__c));			// 戻入金額　当年12月
		fields.add('"val":[' + String.join(values,',') + ']');
		lines.add('"reversion":{' + String.join(fields,',') + '}');

		lines.add('"clawback":' + I_Util.toStr(agencySalesResults.ClawBack__c) + '');				// 戻入未収金残高
		lines.add('"modifieddate":"' + I_Util.toStr(agencySalesResults.YYYYMM__c) + '"');	// 最終更新日

		return String.join(lines,',\n');
	}


}