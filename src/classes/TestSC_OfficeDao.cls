@isTest
private class TestSC_OfficeDao {

	@isTest static void getPrevSCOffice_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		List<SC_Office__c> scOffices = new List<SC_Office__c>();
		SC_Office__c scOffice1 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice1.FiscalYear__c = '2016';
		scOffices.add(scoffice1);
		SC_Office__c scOffice2 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice2.FiscalYear__c = '2017';
		scOffices.add(scoffice2);
		SC_Office__c scOffice3 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice3.FiscalYear__c = '2018';
		scOffices.add(scoffice3);
		insert scoffices;

		Test.startTest();
			List<SC_Office__c> result = SC_OfficeDao.getPrevSCOffice(2018, '67890');
		Test.stopTest();

		System.assertEquals(1, result.size());
		System.assertEquals(scOffice2.Id, result[0].Id);
	}

	@isTest static void getNextSCOffice_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		List<SC_Office__c> scOffices = new List<SC_Office__c>();
		SC_Office__c scOffice1 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice1.FiscalYear__c = '2016';
		scOffices.add(scoffice1);
		SC_Office__c scOffice2 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice2.FiscalYear__c = '2017';
		scOffices.add(scoffice2);
		SC_Office__c scOffice3 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice3.FiscalYear__c = '2018';
		scOffices.add(scoffice3);
		insert scoffices;

		Test.startTest();
			List<SC_Office__c> result = SC_OfficeDao.getNextSCOffice(2016, '67890');
		Test.stopTest();

		System.assertEquals(1, result.size());
		System.assertEquals(scOffice2.Id, result[0].Id);
	}

	@isTest static void getRefreshSCOffice_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		insert scOffice;

		Test.startTest();
			List<SC_Office__c> result = SC_OfficeDao.getRefreshSCOffice(scOffice.Id);
		Test.stopTest();

		System.assertEquals(1, result.size());
	}

	@isTest static void getAttachmentsByOfficeId_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(true, accAgency.Id, null);
		// 添付ファイルを作成
		List<Attachment> attList = new List<Attachment>();
		for(SC_SelfCompliance__c sc : [SELECT Id FROM SC_SelfCompliance__c]){
			attList.add(TestSC_TestUtil.createAttachment(false, sc.Id));
		}
		insert attList;

		Test.startTest();
			List<SC_SelfCompliance__c> result = SC_OfficeDao.getAttachmentsByOfficeId(scOffice.Id);
		Test.stopTest();

		System.assertEquals(SC_Const.SC_COMP_TYPE_LIST.size(), result.size());
		System.assertEquals(1, result[0].Attachments.size());
	}

	@isTest static void getRecById_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		SC_Office__c scOffice = TestSC_TestUtil.createSCOffice(true, accAgency.Id, null);
		// 自主点検管理責任者を作成
		insert new SC_AgencyManager__c(
			 SC_Office__c = scOffice.Id
			,Contact__c = contact.Id
			,User__c = UserInfo.getUserId()
		);

		Test.startTest();
			SC_Office__c result = SC_OfficeDao.getRecById(scOffice.Id);
		Test.stopTest();

		System.assertEquals(scOffice.Id, result.Id);
		System.assertEquals(1, result.SC_AgencyManagers__r.size());
	}

	@isTest static void getRecsForRemindMail_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(true, accParent);
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		List<SC_Office__c> scOffices = new List<SC_Office__c>();
		SC_Office__c scOffice1 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffices.add(scOffice1);
		SC_Office__c scOffice2 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffices.add(scOffice2);
		insert scoffices;
		// 自主点検管理責任者を作成
		List<SC_AgencyManager__c> scManagers = new List<SC_AgencyManager__c>();
		scManagers.add(new SC_AgencyManager__c(
			 SC_Office__c = scOffice1.Id
			,Contact__c = contact.Id
			,User__c = UserInfo.getUserId()
			,HasUserEmail__c = true));
		scManagers.add(new SC_AgencyManager__c(
			 SC_Office__c = scoffice2.Id
			,Contact__c = contact.Id
			,User__c = UserInfo.getUserId()
			,HasUserEmail__c = true));
		insert scManagers;

		Test.startTest();
			List<SC_Office__c> result = SC_OfficeDao.getRecsForRemindMail(new Set<Id>{scOffice1.Id, scOffice2.Id});
		Test.stopTest();

		System.assertEquals(2, result.size());
		System.assertEquals(1, result[0].SC_AgencyManagers__r.size());
	}

	@isTest static void getRecsByParentZHEADAYs_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(false, null);
		accAgency.ParentId = accParent.Id;
		insert accAgency;
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		List<SC_Office__c> scOffices = new List<SC_Office__c>();
		SC_Office__c scOffice1 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice1.FiscalYear__c = '2018';
		scOffices.add(scOffice1);
		SC_Office__c scOffice2 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffices.add(scOffice2);
		SC_Office__c scOffice3 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice3.FiscalYear__c = '2017';
		scOffices.add(scOffice3);
		insert scoffices;

		Test.startTest();
			List<SC_Office__c> result = SC_OfficeDao.getRecsByParentZHEADAYs(new Set<String>{accParent.E_CL1PF_ZHEADAY__c});
		Test.stopTest();
		System.assertEquals(1, result.size());
	}

	@isTest static void getRecsByIds_test001() {
		// 取引先（格）を作成
		Account accParent = TestSC_TestUtil.createAccount(true, null);
		// 取引先（事務所）を作成
		Account accAgency = TestSC_TestUtil.createAccount(false, null);
		accAgency.ParentId = accParent.Id;
		insert accAgency;
		// 取引先責任者を作成
		Contact contact = TestSC_TestUtil.createContact(true, accAgency.Id, 'TestUser001');
		// 自主点検事務所を作成
		List<SC_Office__c> scOffices = new List<SC_Office__c>();
		SC_Office__c scOffice1 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice1.FiscalYear__c = '2018';
		scOffices.add(scOffice1);
		SC_Office__c scOffice2 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffices.add(scOffice2);
		SC_Office__c scOffice3 = TestSC_TestUtil.createSCOffice(false, accAgency.Id, null);
		scOffice3.FiscalYear__c = '2017';
		scOffices.add(scOffice3);
		insert scoffices;

		Test.startTest();
			List<SC_Office__c> result = SC_OfficeDao.getRecsByIds(new Set<Id>{scOffice1.Id, scOffice2.Id, scOffice3.Id});
		Test.stopTest();
		System.assertEquals(1, result.size());
	}

}