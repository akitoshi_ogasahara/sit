@isTest
private with sharing class TestE_HomeSumiseiController {
	private static string key = 'HomeSumisei';
	private static User testuser;
	private static E_IDCPF__c idcpf;
	private static Account account;
	private static Contact contact;
	private static E_MenuKind__c menuKind;
	private static E_MenuMaster__c menuMasterParent;
	private static E_MenuMaster__c menuMasterChild;
	private static E_SelectedMenu__c selectedMenu;
        
    //--- initメソッド ---
    static testMethod void initTest_returnPageE_Home(){
        createUserDataAY();
        System.runAs(testUser){
           test.startTest();
           E_HomeSumiseiController instance = new E_HomeSumiseiController();
		   String actual = instance.init().getUrl();
           String expected = Page.E_Home.getUrl();
           System.assertEquals(expected, actual);	    
           test.stopTest();
        }
    }
  //  static testMethod void initTest_toErrorPage(){
  //	  createUserDataAY();
  //      System.runAs(testuser){
  //      	test.startTest();
  //      	test.setCurrentPage(Page.E_HomeSumisei);
  //      	E_HomeSumiseiController instance = new E_HomeSumiseiController();
  //      	String actual = instance.init().getUrl();
  //      	String expected = null;
  //     	System.assertEquals(expected, actual);	    
  //      	test.stopTest();    
  //      }
  //  }
    
    //---新契約---
    //新契約成立状況
    //社員
    static testMethod void destNewContractStatusEPTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destNewContractStatus;        
            String expected = 'E_DownloadPrintSheetsSelect';
            System.assertEquals(expected, actual);
            test.stopTest();
        }
    }
    //AH
    static testMethod void destNewContractStatusAHTest(){
        createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destNewContractStatus;        
            String expected = 'E_DownloadNewPolicy'; //?did=' + instance.access.User.Contact.account.parentId;
            System.assertEquals(expected, actual);
            test.stopTest();
        }
    }
    //AY
    static testMethod void destNewContractStatusAYTest(){
		createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destNewContractStatus;        
            String expected = 'E_DownloadNewPolicy'; //?oid=' + instance.access.User.accountId;
            System.assertEquals(expected, actual);
            test.stopTest();
        }
    }
    //else return null;
    static testMethod void destNewContractStatusElseTest(){
        createUserDataElse();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destNewContractStatus;        
            String expected = null;
            System.assertEquals(expected, actual);
            test.stopTest();
        }
    }
    
    //嘱託医検索機能
    static testMethod void destDoctorSearchTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destDoctorSearch;        
            String expected = 'E_MDTop';
            System.assertEquals(expected, actual);
            test.stopTest();
        }
    }    

    //---保全---
    //保有契約変更通知
    //社員
    static testMethod void destContractChangeNotificationEPTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destContractChangeNotification;
        	String expected = 'E_DownloadPrintSheetsSelect';
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AH
    static testMethod void destContractChangeNotificationAHTest(){
        createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destContractChangeNotification;
        	String expected = 'E_DownloadNoticeAGPO'; //?did=null';
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AY
    static testMethod void destContractChangeNotificationAYTest(){
        createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destContractChangeNotification;
        	String expected = 'E_DownloadNoticeAGPO'; //?oid=' + instance.access.User.accountId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
        
    }
    //else return null;
    static testMethod void destContractChangeNotificationElseTest(){
        createUserDataElse();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destContractChangeNotification;
        	String expected = null;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    
    
    //保険料未入金通知
    //社員
    static testMethod void destNotAcceptedNotificationTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destNotAcceptedNotification;
        	String expected = 'E_DownloadPrintSheetsSelect';
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AH
    static testMethod void destNotAcceptedNotificationAHTest(){
        createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destNotAcceptedNotification;
        	String expected = 'E_DownloadNoticeNCOL'; //?did=' + instance.access.User.Contact.account.parentId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AY
    static testMethod void destNotAcceptedNotificationAYTest(){
        createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destNotAcceptedNotification;
        	String expected = 'E_DownloadNoticeNCOL'; //?oid=' + instance.access.User.accountId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
	}
    //else return null;
    static testMethod void destNotAcceptedNotificationElseTest(){
        createUserDataElse();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destNotAcceptedNotification;
        	String expected = null;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    
    //保険料請求予告通知
    //社員
    static testMethod void destToBeChargedNotificationTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destToBeChargedNotification;
        	String expected = 'E_DownloadPrintSheetsSelect';
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AH
    static testMethod void destToBeChargedNotificationAHTest(){
        createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destToBeChargedNotification;
        	String expected = 'E_DownloadNoticeBILA'; //?did=' + testUser.accountId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AY
    static testMethod void destToBeChargedNotificationAYTest(){
        createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destToBeChargedNotification;
        	String expected = 'E_DownloadNoticeBILA'; //?oid=' + instance.access.User.accountId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //else return null;
    static testMethod void destToBeChargedNotificationElseTest(){
		createUserDataElse();
		System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destToBeChargedNotification;
        	String expected = null;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }        
    }
    
    
    //保険料請求通知
    //社員
    static testMethod void destChargeNotificationEPTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destChargeNotification;
        	String expected = 'E_DownloadPrintSheetsSelect';
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AH
    static testMethod void destChargeNotificationAHTest(){
        createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destChargeNotification;
        	String expected ='E_DownloadNoticeBILS'; //?did=' + instance.access.User.Contact.account.parentId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //AY
    static testMethod void destChargeNotificationAYTest(){
        createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destChargeNotification;
        	String expected = 'E_DownloadNoticeBILS'; //?oid=' + instance.access.User.accountId;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    //else return null;
    static testMethod void destChargeNotificationElseTest(){
        createUserDataElse();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destChargeNotification;
        	String expected = null;
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    
    //保全帳票
    //社員
    static testMethod void destMaintenanceReportTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
        	String actual = instance.destMaintenanceReport;
        	String expected = 'E_Info?menu=except_spva';
        	test.stopTest();
            System.assertEquals(expected, actual);
        }
    }
    
    //---保有契約状況---
    //保有契約一覧
    //社員
    static testMethod void destContractListEPTest(){
       createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destContractList;
            String expected = 'E_DownloadPrintSheetsSelect';
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    //AH
    static testMethod void destContractListAHTest(){
		createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destContractList;
            String expected = 'E_DownloadHoldPolicySearch?pt=coli'; //&did=' + testUser.accountId;
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    //AY
    static testMethod void destContractListAYTest(){
        createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destContractList;
            String expected = 'E_DownloadHoldPolicySearch?pt=coli'; //&oid=' + instance.access.User.accountId;
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    //else return null;
    static testMethod void destContractListElseTest(){
        createUserDataElse();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destContractList;
            String expected = null;
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }    
    
    //お客様検索・契約内容照会
    static testMethod void destCustomerSearchTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destCustomerSearch;
            String expected = 'E_CustomerSearch';
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    
    //手続き履歴
    //社員
    static testMethod void destProcessHistoryEPTest(){
        createUserDataEP();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destProcessHistory;
            String expected = 'E_DownloadPrintSheetsSelect';
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    //AH
    static testMethod void destProcessHistoryAHTest(){
        createUserDataAH();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destProcessHistory;
            String expected = 'E_DownloadPolicyHistorySearch'; //?did=null';
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    //AY
    static testMethod void destProcessHistoryAYTest(){
        createUserDataAY();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destProcessHistory;
            String expected = 'E_DownloadPolicyHistorySearch'; //?oid=' + instance.access.User.accountId;
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
    //else return null;
    static testMethod void destProcessHistoryElseTest(){
        createUserDataElse();
        System.runAs(testUser){
            E_HomeSumiseiController instance = new E_HomeSumiseiController();
            test.startTest();
            String actual = instance.destProcessHistory;
            String expected = null;
            test.stopTest();
         	System.assertEquals(expected, actual);
        }
    }
 

    
	//=== 住生ユーザ 基本データ ===
	//--- EP権限ユーザー作成 ---
	static void createUserDataEP(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Account = new Account(Name = 'testSumiseiAccount');
			insert account;
			Contact = new Contact(LastName = 'SumiseilastName', FirstName = 'SumiseifirstName', AccountId = account.Id, E_CL3PF_AGNTNUM__c = 'Z1111', E_CL3PF_VALIDFLAG__c = '1');
			insert contact;
			// User
			Profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.PROFILE_E_PARTNERCOMMUNITY Limit 1];
            testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				, ContactId = contact.Id
			);
			insert testuser;
			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add(E_Const.PERM_BASE);//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_EP										//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,OwnerId = testuser.Id									//所有者ID
			);
			insert idcpf;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '住友生命（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'homeMK_Sumitomo');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;
		}
	}
    //--- AH権限ユーザー作成 ---
    static void createUserDataAH(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Account = new Account(Name = 'testSumiseiAccount');
			insert account;
			Contact = new Contact(LastName = 'SumiseilastName', FirstName = 'SumiseifirstName', AccountId = account.Id, E_CL3PF_AGNTNUM__c = 'Z1111', E_CL3PF_VALIDFLAG__c = '1');
			insert contact;
			// User
			Profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.PROFILE_E_PARTNERCOMMUNITY Limit 1];  
            testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				, ContactId = contact.Id
			);
			insert testuser;
			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add(E_Const.PERM_BASE);//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AH									//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,OwnerId = testuser.Id									//所有者ID
			);
			insert idcpf;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '住友生命（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'homeMK_Sumitomo');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;
		}
	}
    //--- AY権限ユーザー作成 ---
    static void createUserDataAY(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Account = new Account(Name = 'testSumiseiAccount');
			insert account;
			Contact = new Contact(LastName = 'SumiseilastName', FirstName = 'SumiseifirstName', AccountId = account.Id, E_CL3PF_AGNTNUM__c = 'Z1111', E_CL3PF_VALIDFLAG__c = '1');
			insert contact;
			// User
			Profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.PROFILE_E_PARTNERCOMMUNITY Limit 1];
            testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				, ContactId = contact.Id
			);
			insert testuser;
			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add(E_Const.PERM_BASE);//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AY								//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,OwnerId = testuser.Id									//所有者ID
			);
			insert idcpf;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '住友生命（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'homeMK_Sumitomo');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;
		}
	}
    //--- その他else句に入る権限（AT）のユーザー作成 ---
    static void createUserDataElse(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Account = new Account(Name = 'testSumiseiAccount');
			insert account;
			Contact = new Contact(LastName = 'SumiseilastName', FirstName = 'SumiseifirstName', AccountId = account.Id, E_CL3PF_AGNTNUM__c = 'Z1111', E_CL3PF_VALIDFLAG__c = '1');
			insert contact;
			// User
			Profile profile = [Select Id, Name,usertype From Profile Where Name = :E_Const.PROFILE_E_PARTNERCOMMUNITY Limit 1];   
            testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = E_Const.USER_EMAILENCODINGKEY_UTF8
				, LanguageLocaleKey = E_Const.USER_LANGUAGELOCLEKEY_JA
				, CommunityNickName='tuser1'
				, E_ZWEBID__c = '5000000000'
				, ContactId = contact.Id
			);
			insert testuser;
			//権限セットの付与
			Set<String> permissionNames = new Set<String>();
			permissionNames.add(E_Const.PERM_BASE);//基本セット　全ユーザ対象

			//権限セット取得
			List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
			//User と PermissionSet の中間OBJリスト
			List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
			for(PermissionSet pSet : permissionSetList){
				PermissionSetAssignment assignment = new PermissionSetAssignment();
				assignment.PermissionSetId = pSet.Id;
				assignment.AssigneeId = testuser.Id;
				assignmentList.add(assignment);
			}
			if(!assignmentList.isEmpty()){
				insert assignmentList;
			}

			idcpf = new E_IDCPF__c(
				User__c = testuser.Id									//ユーザ
				,ZEMAILAD__c = testuser.EMAIL							//E-MAILアドレス
				,ZIDOWNER__c = E_Const.ZIDOWNER_AG+testuser.Contact.E_CL3PF_AGNTNUM__c	//所有者コード
				,ZIDTYPE__c = E_Const.ZIDTYPE_AT										//ID種別
				,ZINQUIRR__c = E_Const.ZINQUIRR_L3+testuser.Contact.E_CL3PF_AGNTNUM__c	//照会者コード
				,ZSTATUS01__c = E_Const.ZSTATUS01_ENABLE				//パスワードステータス
				,ZWEBID__c = testuser.E_ZWEBID__c						//ID番号
				,OwnerId = testuser.Id									//所有者ID
			);
			insert idcpf;

			// Menu作成.
			menuKind = new E_MenuKind__c(ExternalId__c = E_Const.MK_EXID_AGENCY, SortNumber__c = 1, Name = '住友生命（インターネット）');
			insert menuKind;
			menuMasterParent = new E_MenuMaster__c(Name = 'ParentHome', MenuMasterKey__c = 'homeMK_Sumitomo');
			insert menuMasterParent;

			menuMasterChild = new E_MenuMaster__c(
				  Name = 'ChildHome'
				, MenuMasterKey__c = 'home'+ menuKind.ExternalId__c
				, ParentName__c = menuMasterParent.id
				, SelectedMenuKey__c = 'home'
				, UseCommonGW__c = true
				, UseInternet__c = true
				, IsStandardUserOnly__c = false
				, IsPartnerUserOnly__c = false
				, AddParameters__c = 'Pram01.xx; Pram02.xx; Pram03.xx'
			);
			insert menuMasterChild;

			selectedMenu = new E_SelectedMenu__c(
				  MenuKind__c = menuKind.id
				, MenuMaster__c = menuMasterChild.id
				, ExternalId__c = menuKind.ExternalId__c +'|'+ menuMasterChild.SelectedMenuKey__c
			);
			insert selectedMenu;
		}
	}
}