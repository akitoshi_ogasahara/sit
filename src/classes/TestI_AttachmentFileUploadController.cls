@isTest
private class TestI_AttachmentFileUploadController {

	// 旧IE ファイルアップロード 成功
    static testMethod void test_fileUploadOldIE_Success(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	con.fileName = 'fileName.txt';
    	con.contentType = 'text/plain';
    	con.fileBody = Blob.valueOf('body');
    	con.fileSize = 1;

    	con.fileUploadOldIE();

    	con.reload();

    	system.assertEquals(1, con.getAttachments().size());

    }

	// 旧IE ファイルアップロード 失敗
    static testMethod void test_fileUploadOldIE_Error(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	con.fileUploadOldIE();

    	system.assertEquals(0, con.getAttachments().size());

    }

	// ファイル削除 成功
    static testMethod void test_deleteFile_Success(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	Id modalAttId = createAttachment(con.record, con.pageType);

    	con.modalAttId = String.valueOf(modalAttId);
    	con.deleteFile();

    	system.assertEquals(0, con.getAttachments().size());

    }

	// ファイル削除 失敗
    static testMethod void test_deleteFile_Error(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	Id modalAttId = createAttachment(con.record, con.pageType);

    	con.deleteFile();

    	system.assertEquals(1, con.getAttachments().size());

    }

	// ファイル削除 成功
    static testMethod void test_fileDeleteById_Success(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	Id modalAttId = createAttachment(con.record, con.pageType);

    	I_AttachmentFileUploadController.FileUploadResponse res = I_AttachmentFileUploadController.fileDeleteById(String.valueOf(modalAttId));

    	system.assertEquals(0, con.getAttachments().size());

    }

	// ファイル削除 失敗
    static testMethod void test_fileDeleteById_Error(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	Id modalAttId = createAttachment(con.record, con.pageType);
    	String fileId = '';

    	I_AttachmentFileUploadController.FileUploadResponse res = I_AttachmentFileUploadController.fileDeleteById(fileId);

    	system.assertEquals(1, con.getAttachments().size());

    }

	// ファイル削除 成功
    static testMethod void test_fileDelete_Success(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	Id modalAttId = createAttachment(con.record, con.pageType);
    	createAttachment(con.record, con.pageType);

    	I_AttachmentFileUploadController.FileUploadRequest req = new I_AttachmentFileUploadController.FileUploadRequest();
    	req.parentId = String.valueOf(con.record.id);
		req.fileId = String.valueOf(modalAttId);
		req.isSingle = con.isSingle;
		req.pageType = con.pageType;

    	I_AttachmentFileUploadController.FileUploadResponse res = I_AttachmentFileUploadController.fileDelete(req);

    	system.assertEquals(1, con.getAttachments().size());

    }

	// ファイル削除 失敗
    static testMethod void test_fileDelete_Error(){
    	I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
    	con.record = createAccount();
    	con.pageType = 'test';
    	con.isSingle = true;

    	Id modalAttId = createAttachment(con.record, con.pageType);
    	createAttachment(con.record, con.pageType);

    	I_AttachmentFileUploadController.FileUploadRequest req = new I_AttachmentFileUploadController.FileUploadRequest();
    	req.parentId = String.valueOf(con.record.id);
		req.fileId = '';
		req.isSingle = con.isSingle;
		req.pageType = con.pageType;

    	I_AttachmentFileUploadController.FileUploadResponse res = I_AttachmentFileUploadController.fileDelete(req);

    	system.assertEquals(2, con.getAttachments().size());

    }

    // インターネット判定
    static testMethod void test_getUrl_via_Internet(){
        I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
        System.assertEquals(E_Util.getDomainNNlinkage(), con.getUrl_via_Internet());
    }

    // GW判定
    static testMethod void test_getUrl_via_GW(){
        I_AttachmentFileUploadController con = new I_AttachmentFileUploadController();
        System.assertEquals(E_Util.getURLForGWTB(E_Util.getDomainNNlinkage()), con.getUrl_via_GW());
    }
//==============================================
//          テスト環境構築
//==============================================
	private static Account createAccount() {
		Account acc = new Account();
		acc.name = 'TestAccountAttachmentFileUploadController';

		insert acc;

		return acc;
	}

	private static Id createAttachment(sObject record, String pageType) {
		Attachment att = new Attachment();
		att.Name = 'testFile.txt';
		att.Body = Blob.valueOf('test text');
		att.ContentType = 'text/plain';
		att.ParentId = record.Id;
		att.Description = pageType;
		insert att;

		return att.id;
	}
}