/*
 * CC_CaseEditBizLogicTest
 * Test class of CC_CaseEditBizLogic
 * created  : Accenture 2018/4/19
 * modified :
 */

@isTest
private class CC_CaseEditBizLogicTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {
			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = '法人顧客' LIMIT 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			List<Account> accountList = new List<Account>();
			account.Z1OFFING__c = 'Y';
			account.E_CL2PF_ZAGCYNUM__c = '10001';
			accountList.add(account);
			Account childAccount = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			childAccount.Name = 'testChildAccount';
			childAccount.Z1OFFING__c = 'Y';
			childAccount.E_CL2PF_ZAGCYNUM__c = '10002';
			accountList.add(childAccount);
			Insert accountList;

			childAccount.ParentId = account.Id;
			Update childAccount;

			//Create Contact list
			List<RecordType> recordTypeListOfContact = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Contact' AND Name IN ('顧客','募集人')];
			List<Contact> contactList = CC_TestDataFactory.getContactSkelList();
			for(Integer i = 0; i < recordTypeListOfContact.size(); i++){
				if(recordTypeListOfContact[i].Name.equals('顧客')){
					contactList[0].RecordTypeId = recordTypeListOfContact[i].Id;
					contactList[0].AccountId = account.Id;
				}else if(recordTypeListOfContact[i].Name.equals('募集人')){
					contactList[1].RecordTypeId = recordTypeListOfContact[i].Id;
					contactList[1].AccountId = childAccount.Id;
					contactList[1].E_CL3PF_AGNTNUM__c = '12345';
				}
			}
			Insert contactList;

			//Create SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
			Insert SRTypeMaster;

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(srTypeMaster.Id);
			newCase.CMN_ReceptionDateTime__c = system.now();
			newCase.CMN_Comment1__c = 'Comment1';
			newCase.CMN_Comment1__c = 'Comment2';
			newCase.CMN_ShippingChoices__c = '権利者';
			newCase.CMN_ShippingTargetDate__c = '当日発送';
			newCase.CMN_ShippingMethod__c = '普通';
			newCase.CMN_SendingEmail__c = 'test@test.com';
			newCase.CC_ContactNameForPrint__c = 'テスト';
			newCase.CMN_ShippingNameTitle__c = '様';
			newCase.CC_ZipcodeForPrint__c = '123456';
			newCase.CC_AddressForPrint__c = 'テストアドレス';
			newCase.CMN_FreeSpace1__c = 'FreeSpace1';
			newCase.CMN_FreeSpace2__c = 'FreeSpace2';
			Insert newCase;

		}
	}

	/**
	* Test initCase()
	* Initial Case
	* 権利者(法人)からSR作成
	*/
	static testMethod void initCaseTest01() {
		System.runAs ( testUser ) {
			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('targetId', '"'+ (String)account.Id +'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.initCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test initCase()
	* Initial Case
	* 権利者(個人)からSR作成
	*/
	static testMethod void initCaseTest02() {
		System.runAs ( testUser ) {
			//Prepare test data

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('targetId', '"'+ (String)contact.Id +'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.initCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test initCase()
	* Initial Case
	* 募集人からSR作成
	*/
	static testMethod void initCaseTest03() {
		System.runAs ( testUser ) {
			//Prepare test data
			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test1' AND LastName = 'Contact1' LIMIT 1];

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('targetId', '"'+ (String)contact.Id +'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.initCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test initCase()
	* Initial Case
	* 既存SR(法人顧客の場合)
	*/
	static testMethod void initCaseTest04() {
		System.runAs ( testUser ) {
			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Update Case
			Case newCase = [SELECT Id, CMN_CorporateName__c FROM Case WHERE CMN_FreeSpace1__c = 'FreeSpace1' AND CMN_FreeSpace2__c = 'FreeSpace2' LIMIT 1];
			newCase.CMN_CorporateName__c = account.Id;
			Update newCase;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId', '"'+ (String)newCase.Id +'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.initCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test initCase()
	* Initial Case
	* 既存SR(個人顧客の場合)
	*/
	static testMethod void initCaseTest05() {
		System.runAs ( testUser ) {
			//Prepare test data

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Update Case
			Case newCase = [SELECT Id, CMN_CorporateName__c FROM Case WHERE CMN_FreeSpace1__c = 'FreeSpace1' AND CMN_FreeSpace2__c = 'FreeSpace2' LIMIT 1];
			newCase.CMN_ContactName__c = contact.Id;
			Update newCase;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId', '"'+ (String)newCase.Id +'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.initCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test initCase()
	* Initial Case
	* 既存SR(募集人の場合)
	*/
	static testMethod void initCaseTest06() {
		System.runAs ( testUser ) {
			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testChildAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test1' AND LastName = 'Contact1' LIMIT 1];

			//Update Case
			Case newCase = [SELECT Id, CMN_CorporateName__c FROM Case WHERE CMN_FreeSpace1__c = 'FreeSpace1' AND CMN_FreeSpace2__c = 'FreeSpace2' LIMIT 1];
			newCase.CMN_AgencyName__c = account.Id;
			newCase.CMN_AgentName__c = contact.Id;
			newCase.RecruiterCD__c = '12345';
			newCase.Status = 'クローズ';
			Update newCase;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId', '"'+ (String)newCase.Id +'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.initCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}


	/**
	* Test saveCase()
	* Insert Case
	* Clicked '保存' button
	*/
	static testMethod void saveCaseTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert  ePolicy;

			//Create FormMaster(送付状)
			CC_FormMaster__c coverLetter = CC_TestDataFactory.getCoverLetterSkel();
			Insert coverLetter;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			Insert formMaster;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('SYSCorporateName', '"'+ (String)account.Id +'"');
			inputJSONMap.put('SYSContactName', '"'+ (String)contact.Id +'"');
			inputJSONMap.put('受付日時', '"'+'2018/12/01 12:00'+'"');
			inputJSONMap.put('SRタイプ番号SR用件', '"'+'10001/testSRTypeMaster/test'+'"');
			inputJSONMap.put('募集人コード', '"'+'12345'+'"');
			inputJSONMap.put('代理店事務所コード', '"'+'10002/代理店事務所コード'+'"');
			inputJSONMap.put('申出人', '"'+'test Contact'+'"');
			inputJSONMap.put('送付状', '"'+'testCoverLetter'+'"');
			inputJSONMap.put('送付状出力','true');
			inputJSONMap.put('証券番号','"'+'10001'+'"');
			inputJSONMap.put('請求書1','"'+'testFormMaster'+'"');
			inputJSONMap.put('請求書2','"'+'testFormMaster'+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}


	/**
	* Test saveCase()
	* Update Case
	* Clicked '帳票出力' button
	*/
	static testMethod void saveCaseTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create EPolicy
			RecordType recordTypeOfEPolicy = [SELECT Id FROM RecordType WHERE SobjectType = 'E_Policy__c' AND Name = '個人保険ヘッダ' LIMIT 1];
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			ePolicy.RecordTypeId = recordTypeOfEPolicy.Id;
			Insert ePolicy;

			//Create FormMaster(送付状)
			CC_FormMaster__c coverLetter = CC_TestDataFactory.getCoverLetterSkel();
			Insert coverLetter;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			Insert formMaster;

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SYSCorporateName', '"'+ (String)account.Id +'"');
			inputJSONMap.put('SYSContactName', '"'+ (String)contact.Id +'"');
			inputJSONMap.put('SRタイプ番号SR用件', '"'+'10001/testSRTypeMaster/test'+'"');
			inputJSONMap.put('申出人', '"'+'test Contact'+'"');
			inputJSONMap.put('送付状', '"'+'testCoverLetter'+'"');
			inputJSONMap.put('送付状出力','true');
			inputJSONMap.put('証券番号','"'+'10001'+'"');
			inputJSONMap.put('請求書1','"'+'testFormMaster'+'"');
			inputJSONMap.put('請求書2','"'+'testFormMaster'+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName', 'SRFormPrint');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}


	/**
	* Test renewalSRPolicy()
	* Case is exsiting
	* 'Inner' is not list
	* '証券番号' is not list
	* '請求書1' is not null, AND not list
	* '請求書2' is not null, AND not list
	*/
	static testMethod void renewalSRPolicyTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//create SRPolicy
			CC_SRPolicy__c sRPolicy = CC_TestDataFactory.createSRPolicy();
			Insert sRPolicy;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId','"'+(String)sRPolicy.CC_CaseId__c+'"');
			inputJSONMap.put('証券番号','"'+'10001'+'"');
			inputJSONMap.put('請求書1','"'+'testFormMaster'+'"');
			inputJSONMap.put('請求書2','"'+'testFormMaster'+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			optionMap = CC_CaseEditBizLogic.innerJSONProcess(inputMap);
			CC_CaseEditBizLogic.renewalSRPolicy(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test renewalSRPolicy()
	* Case is new
	* 'Inner' is list
	* '証券番号' is list
	* '請求書1' is list/null
	* '請求書2' is list/null
	*/
	static testMethod void renewalSRPolicyTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//create SRPolicy
			CC_SRPolicy__c sRPolicy = CC_TestDataFactory.createSRPolicy();
			Insert sRPolicy;
			String formMasterId = (String)sRPolicy.CC_FormMaster1__c;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('DRId_Case','"'+(String)sRPolicy.CC_CaseId__c+'"');
			string policyNumberTemp = '"10001", "10001","10001","10001","10002"';
			inputJSONMap.put('01証券番号', policyNumberTemp);
			inputJSONMap.put('01請求書1','"'+'testFormMaster'+'"');
			inputJSONMap.put('01請求書2','"'+'testFormMaster'+'"');
			inputJSONMap.put('02証券番号','"'+'10001'+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap02(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			optionMap = CC_CaseEditBizLogic.innerJSONProcess(inputMap);
			CC_CaseEditBizLogic.renewalSRPolicy(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}


	/**
	* Test extractSRPolicy()
	* PolicyID2/PolicyID3/PolicyID4 are existing
	*/
	static testMethod void extractSRPolicyTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//create SRPolicy
			CC_SRPolicy__c sRPolicy = CC_TestDataFactory.createSRPolicy();
			Insert sRPolicy;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId','"'+(String)sRPolicy.CC_CaseId__c+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.extractSRPolicy(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test extractSRPolicy()
	* Only PolicyID1 is existing
	*/
	static testMethod void extractSRPolicyTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//create SRPolicy
			CC_SRPolicy__c sRPolicy = CC_TestDataFactory.createSRPolicy();
			sRPolicy.CC_PolicyID2__c = null;
			sRPolicy.CC_PolicyID3__c = null;
			sRPolicy.CC_PolicyID4__c = null;
			sRPolicy.CC_PolicyNumber2__c = '';
			sRPolicy.CC_PolicyNumber3__c = '';
			sRPolicy.CC_PolicyNumber4__c = '';
			Insert sRPolicy;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId','"'+(String)sRPolicy.CC_CaseId__c+'"');
			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.extractSRPolicy(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test extractSRPolicy()
	* Opened FROM PolicyDetail screen
	*/
	static testMethod void extractSRPolicyTest03() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//create E_Policy__c
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;
			String selectedPolicyIdList = (String)ePolicy.Id;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('selectedPolicyIdList', '"'+selectedPolicyIdList+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.extractSRPolicy(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}


	/**
	* Test createFollowUpSRActivity()
	* Case is exsiting
	*/
	static testMethod void createFollowUpSRActivityTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create ACtivityMaseter
			CC_ActivityMaster__c ActivityMaster = CC_TestDataFactory.getActivityMasterSkel();
			ActivityMaster.Name = '書類封緘';
			Insert ActivityMaster;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('サブステータス', '"未割当"');
			inputJSONMap.put('送付状出力', 'true');
			inputJSONMap.put('ContextId','"'+(String)newCase.Id+'"');
			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);

			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.createFollowUpSRActivity(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test createFollowUpSRActivity()
	* Case is new
	*/
	static testMethod void createFollowUpSRActivityTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create inputMap
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('サブステータス', '"不備中"');
			inputJSONMap.put('送付状出力', 'true');
			inputJSONMap.put('DRId_Case','"'+(String)newCase.Id+'"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			CC_CaseEditBizLogic.createFollowUpSRActivity(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test setValidation01()
	* 保存ボタン押下時
	*/
	static testMethod void setValidationTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			Insert formMaster;

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create SRActivity
			CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
			Insert newSRActivity;

			//Prepare JSON data
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			inputJSONMap.put('ContextId',  '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SR番号', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('募集人コード','"12345"');
			inputJSONMap.put('申出人','null');
			inputJSONMap.put('申出人種別','null');
			inputJSONMap.put('情報源', 'null');
			inputJSONMap.put('ステータス','"クローズ"');
			//Prepare inner data
			inputJSONMap.put('01証券番号','"10000000"');
			inputJSONMap.put('02証券番号','"20000000"');
			inputJSONMap.put('01請求書1','"testFormMaster1"');
			inputJSONMap.put('01請求書2Name','"testFormMaster2"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName', 'saveCase');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test setValidation02()
	* 帳票印刷ボタン押下時
	* '送付状出力' == true
	* '送付状' != null
	* '証券番号' != null (不正)
	* '請求書1' != null  (不正)
	* '請求書2' != null  (不正)
	* '請求書出力' == true || '確認書出力' == true
	*/
	static testMethod void setValidationTest02() {
		System.runAs ( testUser ) {

			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			Insert formMaster;

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create SRActivity
			CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
			Insert newSRActivity;

			//Prepare JSON data
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			//Prepare test data
			inputJSONMap.put('ContextId',  '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SR番号', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('募集人コード','"12345"');
			inputJSONMap.put('代理店事務所コード','"12345/代理店事務所コード"');
			inputJSONMap.put('確認書出力','true');
			inputJSONMap.put('請求書出力','true');
			inputJSONMap.put('送付状出力','true');
			inputJSONMap.put('送付状','"送付状"');
			inputJSONMap.put('送付先','null');
			inputJSONMap.put('送付先宛名','null');
			inputJSONMap.put('送付先郵便番号','null');
			inputJSONMap.put('送付先住所','null');
			inputJSONMap.put('送付先敬称','""');
			inputJSONMap.put('証券番号','"10000000"');
			inputJSONMap.put('請求書1','"testFormMaster1"');
			inputJSONMap.put('請求書2','"testFormMaster2"');
			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName','SRFormPrint');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test setValidation03()
	* 帳票印刷ボタン押下時
	* '送付状出力' == true
	* '請求書出力' == true
	* '送付状' == null
	* '証券番号' == null
	* '請求書1 == null
	* '請求書2 == null
	*/
	static testMethod void setValidationTest03() {
		System.runAs ( testUser ) {

			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			Insert formMaster;

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create SRActivity
			CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
			Insert newSRActivity;

			//Prepare JSON data
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			//Prepare test data
			inputJSONMap.put('ContextId',  '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SR番号', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('募集人コード','"12345"');
			inputJSONMap.put('代理店事務所コード','"10001/代理店事務所コード"');
			inputJSONMap.put('送付状出力','true');
			inputJSONMap.put('請求書出力','true');
			inputJSONMap.put('送付状','null');
			inputJSONMap.put('請求書1','null');
			inputJSONMap.put('請求書2','null');
			inputJSONMap.put('証券番号','null');
			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName','SRFormPrint');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test setValidation04()
	* 帳票印刷ボタン押下時
	* '証券番号' != null　（正）
	* '請求書1 != null （正）
	* '請求書2 != null （正）
	* '請求書出力' == true || '確認書出力' == true
	*/
	static testMethod void setValidationTest04() {
		System.runAs ( testUser ) {

			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id, Name FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;
			RecordType recordTypeOfEPolicy = [SELECT Id FROM RecordType WHERE SobjectType = 'E_Policy__c' AND Name = '個人保険ヘッダ' LIMIT 1];
			ePolicy.RecordTypeId = recordTypeOfEPolicy.Id;
			update ePolicy;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			formMaster.CC_SRTypeNo__c = SRTypeMaster.Name;
			Insert formMaster;

			//Create SRActivity
			CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
			Insert newSRActivity;

			//Prepare JSON data
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			//Prepare test data
			inputJSONMap.put('ContextId',  '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SR番号', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SRタイプ番号SR用件', '"'+'10001/testSRTypeMaster/test'+'"');
			inputJSONMap.put('募集人コード','"11111"');
			inputJSONMap.put('CurrentContactName', '"'+ (String)contact.Id +'"');
			inputJSONMap.put('請求書出力','true');
			inputJSONMap.put('確認書出力','true');
			inputJSONMap.put('証券番号', '"10001"');
			inputJSONMap.put('請求書1','"testFormMaster"');
			inputJSONMap.put('請求書2','"testFormMaster"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName','SRFormPrint');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test setValidation05()
	* 帳票印刷ボタン押下時
	* '証券番号' == null
	* '請求書出力' == false && '確認書出力' == false
	* '送付先' == '代理店事務所（主）' || '代理店事務所（従）'
	*/
	static testMethod void setValidationTest05() {
		System.runAs ( testUser ) {

			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id, Name FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;
			RecordType recordTypeOfEPolicy = [SELECT Id FROM RecordType WHERE SobjectType = 'E_Policy__c' AND Name = '個人保険消滅' LIMIT 1];
			ePolicy.RecordTypeId = recordTypeOfEPolicy.Id;
			update ePolicy;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			formMaster.CC_SRTypeNo__c = SRTypeMaster.Name;
			Insert formMaster;

			//Create SRActivity
			CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
			Insert newSRActivity;

			//Prepare JSON data
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			//Prepare test data
			inputJSONMap.put('ContextId',  '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SR番号', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SRタイプ番号SR用件', '"'+'10001/testSRTypeMaster/test'+'"');
			inputJSONMap.put('CurrentContactName', '"'+ (String)contact.Id +'"');
			inputJSONMap.put('代理店事務所コード','"10001/代理店事務所コード"');
			inputJSONMap.put('請求書出力','false');
			inputJSONMap.put('確認書出力','false');
			inputJSONMap.put('証券番号', 'null');
			inputJSONMap.put('送付先','"代理店事務所（主）"');
			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName','SRFormPrint');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

	/**
	* Test setValidation04()
	* 帳票印刷ボタン押下時
	* '証券番号' != null　（正）
	* 個人保険ヘッダ以外
	* '請求書出力' == true || '確認書出力' == true
	*/
	static testMethod void setValidationTest06() {
		System.runAs ( testUser ) {

			//Get Account
			Account account = [SELECT Id FROM Account WHERE Name = 'testAccount'];

			//Get Contact
			Contact contact = [SELECT Id FROM Contact WHERE FirstName = 'test0' AND LastName = 'Contact0' LIMIT 1];

			//Get SRTypeMaster
			CC_SRTypeMaster__c SRTypeMaster = [SELECT Id, Name FROM CC_SRTypeMaster__c WHERE CC_SRTypeName__c = 'testSRTypeMaster' LIMIT 1];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(SRTypeMaster.Id);
			Insert newCase;

			//Create EPolicy
			E_Policy__c ePolicy = CC_TestDataFactory.getEPolicySkel(account.Id, contact.Id);
			Insert ePolicy;
			RecordType recordTypeOfEPolicy = [SELECT Id FROM RecordType WHERE SobjectType = 'E_Policy__c' AND Name = '個人保険消滅' LIMIT 1];
			ePolicy.RecordTypeId = recordTypeOfEPolicy.Id;
			update ePolicy;

			//Create FormMaster(請求書)
			CC_FormMaster__c formMaster = CC_TestDataFactory.getFormMasterSkel();
			formMaster.CC_SRTypeNo__c = SRTypeMaster.Name;
			Insert formMaster;

			//Create SRActivity
			CC_SRActivity__c newSRActivity = CC_TestDataFactory.createSRActivity(newCase.Id);
			Insert newSRActivity;

			//Prepare JSON data
			Map<String, String> inputJSONMap = CC_TestDataFactory.createinputJSONMap();
			//Prepare test data
			inputJSONMap.put('ContextId',  '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SR番号', '"'+ (String)newCase.Id +'"');
			inputJSONMap.put('SRタイプ番号SR用件', '"'+'10001/testSRTypeMaster/test'+'"');
			inputJSONMap.put('CurrentContactName', '"'+ (String)contact.Id +'"');
			inputJSONMap.put('請求書出力','true');
			inputJSONMap.put('確認書出力','true');
			inputJSONMap.put('証券番号', '"10001"');
			inputJSONMap.put('請求書1','"testFormMaster"');
			inputJSONMap.put('請求書2','"testFormMaster"');

			Map<String, Object> inputMap = CC_TestDataFactory.createCaseEditInputMap01(inputJSONMap);
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();
			optionMap.put('methodName','SRFormPrint');

			Test.startTest();
			CC_CaseEditBizLogic.saveCase(inputMap, outputMap, optionMap);
			Test.stopTest();
		}
	}

}