@isTest
private class TestE_UPParameters {
	
	private static string password;
	
	/**
	 * 暗号化のテスト
	 */
    private static testMethod void testPrdCodeConversion_Encrypt(){
        password = E_UPParameters.prdCodeEncrypt('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]:#=._');
        system.assertEquals('z1SNx3TGcVdDFMr7fvbE08CI6gyhLn9mja4X:qQwRBsU2H5uK!iZ=Wet_loJPp@kOYA[]' , password);
    }
    
    /**
     * 複合化のテスト
     */
    private static testMethod void testPrdCodeConversion_Decrypt(){
        password = E_UPParameters.prdCodeDecrypt('z1SNx3TGcVdDFMr7fvbE08CI6gyhLn9mja4X:qQwRBsU2H5uK!iZ=Wet_loJPp@kOYA[]');
        system.assertEquals('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]:#=._' , password);
    }
}