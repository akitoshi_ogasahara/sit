/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTMT_RestResource {

    //無効ユーザテスト  →　無効ユーザにするとTestMethodでの検証がエラーとなるためSkipする。
    
    //緊急停止テスト
    static testMethod void test_AppDisabled(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(isDisabled__c = true);     //緊急停止
        insert config;
        
        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
           buildRestRequest('/TMTApp/v2/appStatus','POST');
            
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);
            
            System.assertEquals(res.StatusCode, '9');
        }
    }
    
    //許可されていないDevice
    static testMethod void test_NotApproved(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c();
        insert config;
        
        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
           buildRestRequest('/services/apexrest/TMTApp/v2/appStatus','POST');
            
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);
            
            System.assertEquals(res.StatusCode, '2');
        }
    }
    
    //PW有効期限切れ
    static testMethod void test_ChangePW(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c();
        insert config;
        
        eidc.uuid1__c = 'DEVICE_UUID_01|2017-10-1 09:12:34';
        update eidc;
        
        //PWを設定する
        System.setPassword(partnerUsr.Id, 'we_Love_Tamotsu2017');

        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            req.uuid = 'DEVICE_UUID_01';
            buildRestRequest('/TMTApp/v2/appStatus','POST');

            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);
            
            System.assertEquals(res.StatusCode, '1', res.StatusCode + '>' + res.message);
        }
    }

    //PW有効期限切れ
    static testMethod void test_VersionCheck(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(lastVersion__c = '1.６');
        insert config;
        
        eidc.uuid1__c = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_01', System.Now().addHours(+1));
        update eidc;
        
        //PWを設定する
        System.setPassword(partnerUsr.Id, 'we_Love_Tamotsu2017');

        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            req.uuid = 'DEVICE_UUID_01';
            req.version = '1.5';
            buildRestRequest('/TMTApp/v2/appStatus','POST');

            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);

            System.assertEquals(res.StatusCode, '3', res.StatusCode + '>' + res.message);
        }
    }

    //成功
    static testMethod void test_Success(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(lastVersion__c = '2.5');
        config.JSON_updateDate__c = System.Now();
        
        insert config;
        
        eidc.uuid1__c = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_01', System.Now().addHours(+1));
        update eidc;
        
        //PWを設定する
        System.setPassword(partnerUsr.Id, 'we_Love_Tamotsu2017');

        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            req.uuid = 'DEVICE_UUID_01';
            req.version = '2.5';
            
 	        buildRestRequest('/TMTApp/appStatus','POST');
           
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);

            System.assertEquals(res.StatusCode, '0', res.StatusCode + '>' + res.message);
        }
    }
    
      //成功
    static testMethod void test_SuccessV2(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(lastVersion__c = '2.5');
        config.JSON_updateDate__c = System.Now();
        
        insert config;
        
        eidc.uuid1__c = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_01', System.Now().addHours(+1));
        update eidc;
        
        //PWを設定する
        System.setPassword(partnerUsr.Id, 'we_Love_Tamotsu2017');

        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            req.uuid = 'DEVICE_UUID_01';
            req.version = '2.5';
 	        buildRestRequest('/TMTApp/v2/appStatus','POST');
           
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);

            System.assertEquals(res.StatusCode, '0', res.StatusCode + '>' + res.message);
        }
    }
        static testMethod void test_SuccessV2_LOG(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(lastVersion__c = '2.5');
        config.JSON_updateDate__c = System.Now();
        
        insert config;
        
        eidc.uuid1__c = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_01', System.Now().addHours(+1));
        update eidc;
        
        //PWを設定する
        System.setPassword(partnerUsr.Id, 'we_Love_Tamotsu2017');

        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            req.uuid = 'DEVICE_UUID_01';
            req.version = '2.5';
            req.logFlg = false;
        	req.dispFlg = true;
            
 	        buildRestRequest('/TMTApp/v2/appStatus','POST');
           
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);

            System.assertEquals(res.StatusCode, '0', res.StatusCode + '>' + res.message);
        }
    }
            static testMethod void test_SuccessV2_LOG2(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(lastVersion__c = '2.5');
        config.JSON_updateDate__c = System.Now();
        
        insert config;
        
        eidc.uuid1__c = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_01', System.Now().addHours(+1));
        update eidc;
        
        //PWを設定する
        System.setPassword(partnerUsr.Id, 'we_Love_Tamotsu2017');

        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            req.uuid = 'DEVICE_UUID_01';
            req.version = '2.5';
            req.logFlg = false;
        	req.dispFlg = false;
            
 	        buildRestRequest('/TMTApp/v2/appStatus','POST');
           
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);

            System.assertEquals(res.StatusCode, '0', res.StatusCode + '>' + res.message);
        }
    }
    
    static testMethod void test_AppManager(){
        createPartnerUser();
        createEIDC();
        TMT_Config__c config = new TMT_Config__c(lastVersion__c = '1.6');
        insert config;

        TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id);
        
        //getDeviceIdFromUserAgent
        List<String> userAgentStrings = new List<String>{'uid_DEVICE_1234','aaa','bbb'};
        System.assertEquals('DEVICE_1234', appMgr.getDeviceIdFromUserAgent(userAgentStrings));

        //putDeviceID
        String uid1 = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_01', System.Now().addHours(+1));
        String uid2 = TMT_AppManager.getFormattedUUIDString('DEVICE_UUID_02', System.Now().addHours(+2));
        appMgr.eidc.uuid1__c = uid1;
        appMgr.eidc.uuid2__c = uid2;

        //-- 変更なし
        appMgr.putDeviceID('DEVICE_UUID_01');
        System.assertNotEquals(appMgr.eidc.uuid1__c, uid1);     //putDeviceIDのタイミングで常に日時エリアは更新される
        System.assertEquals(appMgr.eidc.uuid2__c, uid2);
        
        //-- New Device
        appMgr.putDeviceID('DEVICE_UUID_03');
        System.assertEquals(appMgr.eidc.uuid1__c, uid2);
        System.assert(appMgr.eidc.uuid2__c.startsWith('DEVICE_UUID_03'));
    }
    
    // ID管理レコードなし
    static testMethod void test_NonIDCPF(){
        createPartnerUser();
        TMT_Config__c config = new TMT_Config__c();
        insert config;
        
        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 
            TMT_RestResource.appStatusRequest req = buildAPIRequest();
            
            TMT_RestResource.appStatusResponse res = TMT_RestResource.appStatus(req);
            
            System.assertEquals(res.StatusCode, 'E');
        }
    }
    
    static testMethod void test_GetJson(){
        createPartnerUser();
        TMT_Config__c config = new TMT_Config__c();
        insert config;
        
        System.runAs(partnerUsr){
            //TMT_AppManager appMgr = new TMT_AppManager(partnerUsr.Id); 

            buildRestRequest('/services/apexrest/TMTApp/json','GET');
                
            TMT_RestResource.doGet();
            
            System.assertNotEquals(RestContext.response.responseBody.toString(), '','response is blank');
        }
    }
    
    static testMethod void testJsonManager(){
        String result = TMT_JsonManager.getJson('TMT_GonstantData' );
            System.assertEquals(result, '','response is Not blank');
    }
//==============================================
//          テスト環境構築
//==============================================

    //PartnerCommunity Userの作成
    private static User partnerUsr;
    private static User createPartnerUser(){
        if(partnerUsr==null){
            // Account
            Account account = new Account(Name = 'testAccount');
            insert account;
            
            // Contact
            Contact contact = new Contact(LastName = 'テスト', FirstName = '太郎', AccountId = account.Id,email = 'tmt_testuser@terrasky.ingtesting');
            insert contact;
        
            // User
            partnerUsr = TestE_TestUtil.createUser(false, 'Tamotsukun', 'E_PartnerCommunity_IA');
            partnerUsr.contactId = contact.Id;
            insert partnerUsr;
        }
        return partnerUsr;
    }
    
    //設定系レコードの作成
    private static E_IDCPF__c eidc;
    private static E_IDCPF__c createEIDC(){
        if(eidc==null){
            eidc = TestE_TestUtil.createIDCPF(true, partnerUsr.Id);
        }
        return eidc;
    }


    private static TMT_RestResource.appStatusRequest buildAPIRequest(){
        TMT_RestResource.appStatusRequest req = new TMT_RestResource.appStatusRequest();
        req.uuid = 'DADE6E37-8E59-4D49-BBF8-BA6FB04A345F';
        req.version = '1.2';
        req.actionType = 'メリット表';
        req.deviceInfo = new Map<String,String>{
                                     'os'=>'iOS10.0.9'
                                    ,'model'=>'iPad'
                                };
        req.simulation = '<xml><header>PX Server Request</header>';
        req.simulation += '<body>定期保険</body></xml>';
        req.logFlg = true;
        req.dispFlg = true;
        
       
        
        return req;
    }
    
    private static void buildRestRequest(String uri, string method){
        RestRequest restReq = new RestRequest();
        RestResponse restRes = new RestResponse();
        restReq.requestURI = uri;
        restReq.httpMethod = method;

        RestContext.request = restReq;
        RestContext.response = restRes;
    }
}