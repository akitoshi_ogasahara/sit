@isTest
private class TestI_CalcServiceAdapter{

    /*
    * 観点:calcMeritTable正常系
    * 条件:Atriaに渡す用のI_CalcServiceInputを渡す
    * 検証:Merit表のEntityのListが返却されること
    */
    @isTest static void testCalcMeritTable(){
        I_CalcServiceInput input = TestI_PIPTestUtil.createAtriaInData();
        I_CalcServiceAdapter adp = new I_CalcServiceAdapter();
        Test.setMock( HttpCalloutMock.class, new I_AtriaServiceMock() );
        List<I_MeritTableEntity> entList = adp.calcMeritTable(input);
        system.assert(entList.size() != 0);
    }

    /*
    * 観点:calcMeritTable異常系
    * 条件:I_AtriaServiceからI_PIPExceptionがスローされた場合
    * 検証:
    */
    @isTest static void testCalcMeritTable_ERROR(){
        I_CalcServiceInput input = TestI_PIPTestUtil.createAtriaInData();
        I_CalcServiceAdapter adp = new I_CalcServiceAdapter();

        Map<String,String> resHeaderMap = new Map<String,String>();
        resHeaderMap.put('Content-Type','application/json');

        I_AtriaServiceMock mock = new I_AtriaServiceMock(500,'Error',resHeaderMap);

        Test.setMock( HttpCalloutMock.class, mock);
        try{
            List<I_MeritTableEntity> entList = adp.calcMeritTable(input);
            System.assert(false);
        } catch(I_PIPException e){
            System.assertEquals(I_PIPConst.ERROR_MESSAGE_CALLOUT_ATRIA_OTHER,e.getMessage());
            System.assertEquals('Error',e.detailMessage);
        }
    }

}