global with sharing class E_InwardTransferController extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    public Map<String,Map<String,Object>> appComponentProperty {get; set;}
    public E_InwardTransferExtender getExtender() {return (E_InwardTransferExtender)extender;}
    
    
    public E_SVFPF_c_E_Policy_c E_SVFPF_c_E_Policy_c {get; private set;}
    public E_InwardTransferController(ApexPages.StandardController controller) {
        super(controller);

        appComponentProperty = new Map<String, Map<String, Object>>();
        Map<String, Object> tmpPropMap = null;

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2879');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2879',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','141');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component185');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component185',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_InwardTransferAgree');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','96');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2884');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2884',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2856');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2856',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('ischk','{!Extender.isAgree}');
        tmpPropMap.put('bflbl','');
        tmpPropMap.put('afLbl','{!Extender.disclaimer[\'ITA|002\']}');
        tmpPropMap.put('Component__Width','15');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component1985');
        tmpPropMap.put('Component__Name','ECheckBox');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component1985',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2883');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2883',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2885');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2885',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component610');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component610',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_InwardTransferEntry');
        tmpPropMap.put('p_target','btn btn-gray');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','103');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2890');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2890',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2867');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2867',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','2');
        tmpPropMap.put('executeScreen','entry');
        tmpPropMap.put('updateData','{!Extender.jsonUpdateData}');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component2672');
        tmpPropMap.put('Component__Name','EPieGraphPolicy');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2672',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_val','{!Extender.totalZINVPERCNow}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2844');
        tmpPropMap.put('Component__Name','ENumberLabel');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2844',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2889');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2889',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2891');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2891',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component2074');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2074',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_InwardTransferConfirm');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','89');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2896');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2896',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2878');
        tmpPropMap.put('Component__Name','EPageMessage');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2878',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','2');
        tmpPropMap.put('executeScreen','confirm');
        tmpPropMap.put('updateData','{!Extender.jsonUpdateData}');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component2673');
        tmpPropMap.put('Component__Name','EPieGraphPolicy');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2673',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('clrs','{!inwardTab_item.record.CHDRNUM__c}');
        tmpPropMap.put('Component__Width','40');
        tmpPropMap.put('Component__Height','20');
        tmpPropMap.put('Component__id','Component2710');
        tmpPropMap.put('Component__Name','EListColors');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2710',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_val','{!Extender.totalZINVPERCNow}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.0');
        tmpPropMap.put('p_floorvalue','10');
        tmpPropMap.put('p_conversion','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2845');
        tmpPropMap.put('Component__Name','ENumberLabel');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2845',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('secret','{!Extender.inputPassWord}');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','14');
        tmpPropMap.put('Component__id','Component2350');
        tmpPropMap.put('Component__Name','EInputSecret');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2350',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('oldpw','{!Extender.inputPassWord}');
        tmpPropMap.put('newpw','{!Extender.newPassWord}');
        tmpPropMap.put('vrfnewpw','{!Extender.verPassWord}');
        tmpPropMap.put('newml','');
        tmpPropMap.put('vrfnewml','');
        tmpPropMap.put('chkval','');
        tmpPropMap.put('pwtype','2');
        tmpPropMap.put('ptype','');
        tmpPropMap.put('Component__Width','400');
        tmpPropMap.put('Component__Height','116');
        tmpPropMap.put('Component__id','Component2702');
        tmpPropMap.put('Component__Name','EChangePW');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2702',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2895');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2895',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','90');
        tmpPropMap.put('Component__id','Component2901');
        tmpPropMap.put('Component__Name','ELogoHeader');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2901',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('Component__Width','140');
        tmpPropMap.put('Component__Height','600');
        tmpPropMap.put('Component__id','Component2361');
        tmpPropMap.put('Component__Name','EMenu');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2361',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_InwardTransferComplete');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
        tmpPropMap.put('Component__Width','95');
        tmpPropMap.put('Component__Height','18');
        tmpPropMap.put('Component__id','Component2902');
        tmpPropMap.put('Component__Name','EOutputLink');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2902',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('policyId','{!record.Id}');
        tmpPropMap.put('executeKind','2');
        tmpPropMap.put('executeScreen','complete');
        tmpPropMap.put('updateData','{!Extender.jsonUpdateData}');
        tmpPropMap.put('recTypeName','{!record.recordType.DeveloperName}');
        tmpPropMap.put('Component__Width','200');
        tmpPropMap.put('Component__Height','200');
        tmpPropMap.put('Component__id','Component2674');
        tmpPropMap.put('Component__Name','EPieGraphPolicy');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2674',tmpPropMap);

        tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('rendered','');
        tmpPropMap.put('Component__Width','100');
        tmpPropMap.put('Component__Height','15');
        tmpPropMap.put('Component__id','Component2900');
        tmpPropMap.put('Component__Name','ECopyRight');
        tmpPropMap.put('Component__NameSpace','');
        appComponentProperty.put('Component2900',tmpPropMap);

        lastPageNumber = 4;
        pagesMap.put(1,'E_InwardTransferAgree');
        pagesMap.put(2,'E_InwardTransferEntry');
        pagesMap.put(3,'E_InwardTransferConfirm');
        pagesMap.put(4,'E_InwardTransferComplete');

        SObjectField f;

        f = E_Policy__c.fields.COMM_CHDRNUM__c;
        f = E_SVFPF__c.fields.SVFPFCode__c;
        f = E_SVFPF__c.fields.ZINVPERC__c;
        f = E_SVFPF__c.fields.InputZINVPERC__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = E_Policy__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            recordTypeIdMap = SkyEditor2.Util.SetRecordTypesMap('E_Policy__c');
            mainQuery = new SkyEditor2.Query('E_Policy__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
            mainQuery.addFieldAsOutput('SystemModstamp');
            mainQuery.addFieldAsOutput('Id');
            mainQuery.addFieldAsOutput('RecordTypeId');
            mainQuery.addFieldAsOutput('RecordType.DeveloperName');
            mainQuery.addFieldAsOutput('COMM_ZINVDCF__c');
            mainQuery.addFieldAsOutput('Contractor__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_ZCLKNAME__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_COMM_ZCLADDR__c');
            mainQuery.addFieldAsOutput('Contractor__r.MobilePhone');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLNTNUM__c');
            mainQuery.addFieldAsOutput('Contractor__r.Name');
            mainQuery.addFieldAsOutput('Contractor__r.HomePhone');
            mainQuery.addFieldAsOutput('Contractor__r.LastName');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_ZKNJSEX__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_DOB__c');
            mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLTPHONE01__c');
            mainQuery.addFieldAsOutput('Contractor__r.FirstName');
            mainQuery.addFieldAsOutput('ContractorName__c');
            mainQuery.addFieldAsOutput('LastModifiedById');
            mainQuery.addFieldAsOutput('LastModifiedDate');
            mainQuery.addFieldAsOutput('CreatedById');
            mainQuery.addFieldAsOutput('CreatedBy.Name');
            mainQuery.addFieldAsOutput('CreatedBy.LastName');
            mainQuery.addFieldAsOutput('CreatedBy.FirstName');
            mainQuery.addFieldAsOutput('CreatedDate');
            mainQuery.addFieldAsOutput('OwnerId');
            mainQuery.addFieldAsOutput('Annuitant__r.Name');
            mainQuery.addFieldAsOutput('Annuitant__r.HomePhone');
            mainQuery.addFieldAsOutput('Annuitant__r.Birthdate');
            mainQuery.addFieldAsOutput('Annuitant__r.Phone');
            mainQuery.addFieldAsOutput('Annuitant__r.E_CL3PF_CLNTNUM__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
            mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
            mainQuery.addFieldAsOutput('dataSyncDate__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            E_SVFPF_c_E_Policy_c = new E_SVFPF_c_E_Policy_c(new List<E_SVFPF__c>(), new List<E_SVFPF_c_E_Policy_cItem>(), new List<E_SVFPF__c>(), null);
            listItemHolders.put('E_SVFPF_c_E_Policy_c', E_SVFPF_c_E_Policy_c);
            query = new SkyEditor2.Query('E_SVFPF__c');
            query.addField('InputZINVPERC__c');
            query.addFieldAsOutput('SVFPFCode__c');
            query.addFieldAsOutput('ZINVPERC__c');
            query.addFieldAsOutput('InputZINVPERC__c');
            query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
            relationFields.put('E_SVFPF_c_E_Policy_c', 'E_Policy__c');
            query.limitRecords(500);
            queryMap.put('E_SVFPF_c_E_Policy_c', query);
            
            registRelatedList('E_SVFPFs__r', 'E_SVFPF_c_E_Policy_c');
            
            
            addInheritParameter('RecordTypeId', 'RecordType');
            extender = new E_InwardTransferExtender(this);
            p_showHeader = false;
            p_sidebar = false;
            init();
            
            E_SVFPF_c_E_Policy_c.extender = this.extender;
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            extender.init();

        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    
    @TestVisible
    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    @TestVisible
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }

    global with sharing  class E_SVFPF_c_E_Policy_cItem extends SkyEditor2.ListItem {
        public E_SVFPF__c record{get; private set;}
        @TestVisible
        E_SVFPF_c_E_Policy_cItem(E_SVFPF_c_E_Policy_c holder, E_SVFPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global class E_SVFPF_c_E_Policy_c extends SkyEditor2.ListItemHolder {
        public List<E_SVFPF_c_E_Policy_cItem> items{get; private set;}
        @TestVisible
        E_SVFPF_c_E_Policy_c(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<E_SVFPF_c_E_Policy_cItem>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new E_SVFPF_c_E_Policy_cItem(this, (E_SVFPF__c)data, recordTypeSelector);
        }
        global override void importByJSON() {
            SkyEditor2.JSONObject.value[] data = SkyEditor2.JSONObject.JSONArray(
                new SkyEditor2.JSONObject.JSONTokener(hiddenValue)
            );
            super.importByJSON(data);
            for (Integer n = items.size() - data.size(); n < items.size(); n++) {
                E_SVFPF_c_E_Policy_cItem i = items[n];
            }
        }
    }
    
    public E_SVFPF__c E_SVFPF_c_E_Policy_c_Conversion { get { return new E_SVFPF__c();}}
    
    public String E_SVFPF_c_E_Policy_c_selectval { get; set; }
    
    public void callRemove_E_SVFPF_c_E_Policy_c() {
        for(Integer i = E_SVFPF_c_E_Policy_c.items.size() - 1; i >= 0; i--){
            E_SVFPF_c_E_Policy_cItem item = E_SVFPF_c_E_Policy_c.items[i];
            if(item.selected){
                item.remove();
            }
        }
    }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}