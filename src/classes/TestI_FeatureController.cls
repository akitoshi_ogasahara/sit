@isTest
private class TestI_FeatureController {
	static String uniquekey = 'uniquekey';
	
	@isTest static void constracterTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		createIRISCMS(ipm,true);
		System.runAs(actUser){
			PageReference pageRef = Page.IRIS_Feature;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGEKEY,uniquekey);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_FeatureController cms = new I_FeatureController();
			System.assertEquals(cms.featureDayFlag,false);
			Test.stopTest();
		}
	}

	@isTest static void constracterTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage2();
		createIRISCMS(ipm,true);
		System.runAs(actUser){
			PageReference pageRef = Page.IRIS_Feature;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGEKEY,uniquekey);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_FeatureController cms = new I_FeatureController();
			System.assertEquals(cms.featureDayFlag,true);
			Test.stopTest();
		}
	}
	
	@isTest static void bannerThumbnailTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();
		createIRISCMS(ipm,true);
		System.runAs(actUser){
			PageReference pageRef = Page.IRIS_Feature;
			pageRef.getParameters().put(I_Const.URL_PARAM_PAGEKEY,uniquekey);
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_FeatureController cms = new I_FeatureController();

			System.assertEquals(cms.getBannerThumbnail(),false);
			Test.stopTest();
		}
	}


	@isTest static void noPageKeyTest() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
		createIDManage(actUser);
		I_PageMaster__c ipm = createIRISPage();

		createIRISCMS2(ipm);
		System.runAs(actUser){
			PageReference pageRef = Page.IRIS_Feature;
			Test.setCurrentPage(pageRef);

			Test.startTest();
			I_FeatureController cms = new I_FeatureController();
			cms.getTubeConnectHelper();

			System.assertEquals(cms.featureDayFlag,false);
			Test.stopTest();
		}
	}

	//irisPage
	static I_PageMaster__c createIRISPage(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = uniquekey;
		insert pm;
		return pm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage2(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = uniquekey;
		pm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		pm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(I_PageMaster__c ipm,Boolean create){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		if(create){
			createChildIRISCMS(icm);
		}else{
			createChildIRISCMS2(icm);
		}
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		insert icm;
		return icm;
	}

	static Integer num = 0;
	static I_ContentMaster__c createChildIRISCMS2(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		if(num == 0) icm.CanDisplayVia__c = I_Const.ACCESS_VIA_INTERNET;
		if(num == 1) icm.CanDisplayVia__c = I_Const.ACCESS_VIA_CMN_GW;
		icm.ClickAction__c = 'DL(NNLink)';
		insert icm;
		System.debug('テストデバッグCHILD' + icm);
		num++;
		return icm;
	}

	static E_CMSFile__c createCMSFile(){
		E_MessageMaster__c ms = new E_MessageMaster__c(Key__c = 'TEST',UpsertKey__c=uniquekey);
		insert ms;

		E_CMSFile__c ecf = new E_CMSFile__c();
		ecf.UpsertKey__c = uniquekey;
		ecf.MessageMaster__c = ms.Id;
		insert ecf;
		System.debug('テストデバッグCMS' + ecf);
		return ecf;
	}

		//ID管理
	static void createIDManage(User us){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.User__c = us.Id;
		ic.AppMode__c = 'IRIS';
		ic.ZINQUIRR__c = 'BR00';
		insert ic;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS2(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		icm.Style__c = 'テキスト表示';
		insert icm;
		createChildIRISCMS3(icm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS3(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.CompanyLimited__c = true;
		insert icm;
		return icm;
	}


}