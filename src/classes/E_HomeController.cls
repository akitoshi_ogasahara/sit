public virtual with sharing class E_HomeController extends E_InfoController{

	public static final String MENUMASTERKEY_HOME = 'home';

	/**
	 *		Constructor
	 */
	public E_HomeController(){
		super();
	}
	public virtual override PageReference init(){

		PageReference superPref = super.init();
		//認証エラーの場合はエラー画面へ　またはメアド変更ページへ遷移
		if(superPref!=null){
			return superPref;
		}

		// IRISfor住生ユーザの場合、IRIS_TopSumisei へ遷移
		if(access.isSumiseiIRISUser()){
			access.switchEIDC_appMode(I_Const.APP_MODE_IRIS);
			return Page.IRIS_TopSumisei;
		}

		// 住生ユーザの場合、E_HomeSumisei へ遷移
		if(access.isSumiseiUser()){
			return Page.E_HomeSumisei;
		}

		// 社員かつメニュー種類が住生の場合　E_HomeSumiseiに遷移
		if(access.isEmployee()){
			String menuKind = E_CookieHandler.getCookieMenuKind();
			if (menuKind != null && menuKind.equals(E_Const.MK_EXID_SUMITOMO)) {
				return Page.E_HomeSumisei;
			}
		}

		// 一般代理店の場合
		if(access.isNormalAgent()){
			//　EIDCでIRIS利用の場合　IRISTopに遷移する。		access.getCanUseIRISは判定に含めない
			if(access.idcpf.appMode__c == I_Const.APP_MODE_IRIS && iris.getCanUseIRIS()){
				return Page.IRIS_Top;
			}

			//　EIDCのAppModeが未設定かつIRIS利用対象の場合　IRISTopに遷移する。
			if(String.isBlank(access.idcpf.appMode__c) && iris.getIsIRIS()){
				access.switchEIDC_appMode(I_Const.APP_MODE_IRIS);
				return Page.IRIS_Top;
			}
		}

		// 社員の場合
		if(access.isEmployee()){
			// 利用アプリを NNLink に更新する
			access.switchEIDC_appMode(I_Const.APP_MODE_NNLINK);
			return null;
		}

		return null;
	}

	protected override String getMenuKey(){
		/*
		return MENUMASTERKEY_HOME;
		*/
		String menuKind = E_CookieHandler.getCookieMenuKind();
		if (menuKind == null || String.isEmpty(menuKind)) {
			if (access.isEmployee()) {
				List<E_MenuKind__c> mklist = E_MenuKindDao.getMenuKnds();
				if (mklist.size() > 0) {
					menuKind = mklist[0].ExternalId__c;
				}
			} else {
				if (access.user.Contact.E_MenuKindId__c != null) {
					E_MenuKind__c mk = E_MenuKindDao.getRecById(access.user.Contact.E_MenuKindId__c);
					if (mk != null) {
						menuKind = mk.ExternalId__c;
					}
				} else {
					E_MenuKind__c mk = null;
					E_IDCPF__c idcpf = E_AccessController.getInstance().idcpf;
					if(idcpf != null){
						//TODO ID適正化
						//ZSTATUS01=1 の場合、'MK_Agency'
						//if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE)){
						if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE) && idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_IRIS)){
							mk = E_MenuKindDao.getRecByExternalId(E_Const.MK_EXID_AGENCY);
						//ZSTATUS01=6 の場合、 ‘MK_BankAgency’
						//}else if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_CGW)){
						}else if(idcpf.ZSTATUS01__c.equals(E_Const.ZSTATUS01_ENABLE) && idcpf.ZDSPFLAG02__c.equals(E_Const.ZDSPFLAG02_BANK)){
							mk = E_MenuKindDao.getRecByExternalId(E_Const.MK_EXID_BANK_AGENCY);
						}
					}
					if (mk != null) {
						menuKind = mk.ExternalId__c;
					}
				}
			}
		}
		return MENUMASTERKEY_HOME + menuKind;
	}

	public Boolean getIsEmployee(){
		return access.isEmployee();
	}

}