public with sharing class I_SearchBoxController {
// 入力
	// 表示タブ
	public List<SelectOption> searchCategoryItems {get; set;}
	// searchCategoryItemsのデフォルト選択値
	public String defaultSearchBox {get {return I_Const.SEARCH_CATEGORY_POLICY;} private set;}

	public I_SearchBoxController(){
		E_AccessController access = E_AccessController.getInstance();
		searchCategoryItems = new List<SelectOption>();

		// ATは「代理店/募集人」検索を使用不可
		if(access.isAuthAT()){
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_POLICY,   '顧客検索'));
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_CONTENT,   'コンテンツ検索'));
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_NEWOPP,   '新契約検索'));
			//searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_DOCUMENT, 'サイト内情報検索'));
		} else {
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_POLICY,   '顧客検索'));
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_AGENCY,   '代理店・募集人検索'));
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_CONTENT,   'コンテンツ検索'));
			searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_NEWOPP,   '新契約検索'));
			//searchCategoryItems.add(new SelectOption(I_Const.SEARCH_CATEGORY_DOCUMENT, 'サイト内情報検索'));
		}
	}

	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}
}