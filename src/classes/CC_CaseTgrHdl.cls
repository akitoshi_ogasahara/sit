public with sharing class CC_CaseTgrHdl{

	/**
	 * Trigger Handler実行確認用変数
	 */
	private static Set<String> runTgrSet = new Set<String>();

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){
		if(!runTgrSet.contains('onBeforeInsert')){
			runTgrSet.add('onBeforeInsert');

			//validation check
			Map<Id,Case> newMap = new Map<Id,Case>();
			Map<Id,Case> oldMap = new Map<Id,Case>();
			setValidation(newList, newMap, oldMap);
		}
	}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		if(!runTgrSet.contains('onBeforeUpdate')){
			runTgrSet.add('onBeforeUpdate');

			//validation check
			setValidation(newList, newMap, oldMap);
		}
	}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}

	/**
	 * set validation
	 */
	public static void setValidation(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		//get master data
		List<CC_SRActivity__c> srActivityList = new List<CC_SRActivity__c>();
		if(newMap != null){
			Set<Id> caseIdSet = newMap.keySet();
			srActivityList = CC_CaseEditDao.getSRActivityListByCaseIdList(caseIdSet);
		}

		for(Case caseObj : newList){
			String errorMessage = ' ';

			if((caseObj.CMN_Source__c == null || caseObj.CMN_Source__c.equals('')) && caseObj.CMN_ReceptionDateTime__c != null){
				errorMessage += System.Label.CC_CaseTgrHdl_ErrMsg_NotInputSource + '<br>';
			}

			if(caseObj.CC_SRTypeId__c == null && caseObj.CMN_ReceptionDateTime__c != null){
				errorMessage += System.Label.CC_CaseTgrHdl_ErrMsg_NotInputSRTypeId + '<br>';
			}

			//when update/delete
			if(newMap != null){
				if(caseObj.Status.equals('クローズ') && srActivityList.size() > 0){
					for(CC_SRActivity__c srActivityObj : srActivityList){
						if(srActivityObj.CC_SRNo__c == caseObj.Id &&
						   !srActivityObj.CC_Status__c.equals('終了') && !srActivityObj.CC_Status__c.equals('作業不要')){

							errorMessage += System.Label.CC_CaseTgrHdl_ErrMsg_InvalidStatus + '<br>';
							Break;
						}
					}
				}
			}

			//show error message
			if(!errorMessage.equals(' ')){
				errorMessage = errorMessage.removeEnd('<br>');
				caseObj.addError(errorMessage, false);
			}

		}
	}
}