/*
 * CC_SRActivityTgrHdl
 * Test class of CC_SRActivityTgrHdl
 * created  : Accenture 2018/7/4
 * modified :
 */

@isTest
private class CC_SRActivityTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create SRActivity test data
			List<CC_SRActivity__c> srActivityList1 = CC_TestDataFactory.createSRActivityList();
			insert srActivityList1;
			//Before Update01 original data
			//create SRTypeMaster
			CC_SRTypeMaster__c newSRTypeMaster = new CC_SRTypeMaster__c();
			newSRTypeMaster.Name = '20000';
			newSRTypeMaster.CC_SRTypeName__c = 'testSRTypeMaster';
			newSRTypeMaster.CC_SRTypeSubject__c = 'test';
			newSRTypeMaster.CC_Department__c = 'CS';
			newSRTypeMaster.CC_SRTypeComment1__c = '';
			newSRTypeMaster.CC_SRTypeComment2__c = '';
			insert newSRTypeMaster;
			//create Case
			Case newCase = new Case();
			newCase.CC_SRTypeId__c = newSRTypeMaster.Id;
			newCase.CMN_Source__c = '電話';
			newCase.CC_SubStatus__c = '未割当';
			newCase.CC_IsCoverLetterOutput__c = true;
			newCase.CC_IsInvoiceOutput__c = false;
			newCase.CMN_FreeSpace1__c = 'testCase';
			RecordType recordTypeOfCase = [SELECT Name FROM RecordType where SobjectType = 'Case' and Name = 'サービスリクエスト' limit 1];
			newCase.RecordTypeId = recordTypeOfCase.Id;
			insert newCase;
			//create SRActivity
			Group queue = [Select Id from Group where Type = 'Queue' and name = 'Unassigned' LIMIT 1];
			List<CC_SRActivity__c> srActivityList2 = new List<CC_SRActivity__c>();
			for(Integer i = 0; i < 2; i++){
				srActivityList2.add(new CC_SRActivity__c(
					CC_SRNo__c = newCase.Id,
					CC_Order__c = 1,
					OwnerId = queue.Id,
					CC_ActivityType__c = 'testActivityMaster0',
					CC_Status__c = '開始前'
				));
			}
			insert srActivityList2;

		}
	}

	/**
	* Test Before Insert
	*/
	static testMethod void srActivityTgrHdlTestBeforeInsert() {
		System.runAs ( testUser ) {
			try{

				CC_SRActivity__c srActivity = new CC_SRActivity__c();
				srActivity.CC_Order__c = null;
				srActivity.CC_SRNo__c = null;
				srActivity.CC_ActivityType__c = 'newTestActivityMaster';

				Test.startTest();
				insert srActivity;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update01
	*
	*/
	static testMethod void srActivityTgrHdlTestBeforeUpdate01() {
		System.runAs ( testUser ) {
			try{

				//Update SRActivity test data
				CC_SRTypeMaster__c newSRtypeMaster = [Select Id from CC_SRTypeMaster__c where Name = '20000'];
				Case newCase = [Select Id from Case where CC_SRTypeId__c = :newSRtypeMaster.Id];
				List<CC_SRActivity__c> newSRActivityList = [Select Id, CC_Status__c  from CC_SRActivity__c where CC_SRNo__c = : newCase.Id];
				CC_SRActivity__c newSRActivity = newSRActivityList.get(0);
				newSRActivity.CC_Status__c = '終了';
				newSRActivity.OwnerId = testUser.Id;

				Test.startTest();
				Update newSRActivity;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update02
	*
	*/
	static testMethod void srActivityTgrHdlTestBeforeUpdate02() {
		System.runAs ( testUser ) {
			try{

				//Update SRActivity test data
				CC_SRTypeMaster__c newSRtypeMaster = [Select Id from CC_SRTypeMaster__c where Name = '10000'];
				Case newCase = [Select Id from Case where CC_SRTypeId__c = :newSRtypeMaster.Id];
				CC_SRActivity__c newSRActivity = [Select Id, CC_Status__c  from CC_SRActivity__c where CC_SRNo__c = : newCase.Id];
				newSRActivity.CC_Status__c = '終了';

				Test.startTest();
				Update newSRActivity;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

	/**
	* Test Before Update03
	*
	*/
	static testMethod void srActivityTgrHdlTestBeforeUpdate03() {
		System.runAs ( testUser ) {
			try{

				CC_SRTypeMaster__c newSRtypeMaster1 = [Select Id from CC_SRTypeMaster__c where Name = '10000'];
				Case newCase1 = [Select Id from Case where CC_SRTypeId__c = :newSRtypeMaster1.Id];
				CC_SRActivity__c newSRActivity = [Select Id, CC_Status__c  from CC_SRActivity__c where CC_SRNo__c = : newCase1.Id];
				//Update SRActivity test data
				newSRActivity.CC_Status__c = '作業不要';
				newSRActivity.OwnerId = testUser.Id;
				newSRActivity.CC_ActivityType__c = 'testSRTypeMaster1';

				CC_SRTypeMaster__c newSRtypeMaster2 = [Select Id from CC_SRTypeMaster__c where Name = '10001'];
				Case newCase2 = [Select Id from Case where CC_SRTypeId__c = :newSRtypeMaster2.Id];
				newSRActivity.CC_SRNo__c = newCase2.Id;

				Test.startTest();
				Update newSRActivity;
				Test.stopTest();

			}catch(Exception e){
				System.debug('Exception caught: ' + e.getMessage());
			}
		}
	}

}