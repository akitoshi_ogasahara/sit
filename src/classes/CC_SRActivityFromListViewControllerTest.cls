@isTest
private with sharing class CC_SRActivityFromListViewControllerTest {

    private static testMethod void init_test01(){
        String batchkey = '20000101_0900';
        User mr = createUser('ＭＲ');
        createIDCPF(mr.Id,'1A');
        CC_SRTypeMaster__c srType = createSRType();
        Case sr = createSR(srType.Id,'電話');
        Account office = createAccount('1A','00');
        Contact agent = createContact(office.Id);
        E_Policy__c policy = createPolicy(agent.Id);
        createSRPolicy(sr.Id, policy.Id);
        createSRActivityMaster();
        CC_SRActivity__c srActivity = createSRActivity(sr.Id,batchkey);

        //Share登録
        createAccountShare(office.Id, mr.Id);
        createPolicyShare(policy.Id, mr.Id);

        //ページ設定
        PageReference pr = Page.CC_SRActivityFromListView;
        Test.setCurrentPage(pr);
        pr.getParameters().put('bkey', batchkey);
        pr.getParameters().put('type', '1');

        System.runAs(mr){
            Test.startTest();
            CC_SRActivityFromListViewController controller = new CC_SRActivityFromListViewController();
            controller.init();

            System.assert(!controller.mrSecList.isEmpty());

            controller.doRefresh();

            controller.isChangeMoreDisp = true;
            controller.doRefresh();

            System.assert(controller.isMoreDisp);

            controller.mrSecList[0].srList[0].record.CC_MRCheckBox__c = true;
            controller.doSave();

            Test.stopTest();

            Case result = [SELECT CC_MRCheckBox__c FROM Case WHERE id =: sr.id];
            System.assert(result.CC_MRCheckBox__c);
        }

    }

    private static testMethod void init_test02(){
        String batchkey = '20000101_0900';
        User mr = createUser('ＭＲ');
        createIDCPF(mr.Id,'**');
        CC_SRTypeMaster__c srType = createSRType();
        Case sr = createSR(srType.Id,'保全ダイヤル');
        Account office = createAccount('1A','88');
        Contact agent = createContact(office.Id);
        E_Policy__c policy = createPolicy(agent.Id);
        createSRPolicy(sr.Id, policy.Id);
        createSRActivityMaster();
        CC_SRActivity__c srActivity = createSRActivity(sr.Id,batchkey);

        //Share登録
        createAccountShare(office.Id, mr.Id);
        createPolicyShare(policy.Id, mr.Id);

        //ページ設定
        PageReference pr = Page.CC_SRActivityFromListView;
        Test.setCurrentPage(pr);
        pr.getParameters().put('bkey', batchkey);
        pr.getParameters().put('type', '2');

        System.runAs(mr){
            Test.startTest();
            CC_SRActivityFromListViewController controller = new CC_SRActivityFromListViewController();
            controller.init();
            Test.stopTest();
            
            System.assert(!controller.mrSecList.isEmpty());
        }

    }


    /**
     * SRType作成
     */
    private static CC_SRTypeMaster__c createSRType(){
        CC_SRTypeMaster__c srType = new CC_SRTypeMaster__c();
        srType.CC_Department__c = 'COLI';
        srType.CC_IsCommissionSR__c = true;
        insert srType;
        return srType;
    }
    /**
     * SR作成
     */
    private static Case createSR(ID srTypeId,String source){
        Case sr = new Case();
        sr.CC_SRTypeId__c = srTypeId;
        sr.CMN_Source__c = source;
        insert sr;
        return sr;
    }
    /**
     * 保険契約ヘッダ作成
     */
    private static E_Policy__c createPolicy(ID conId){
        E_Policy__c policy = new E_Policy__c();
        policy.MainAgent__c = conId;
        policy.SubAgent__c = conId;

        insert policy;
        return policy;
    }
    /**
     * PolicyのShare登録
     */
    public static void createPolicyShare(ID policyId, ID userId) {
        E_Policy__Share share = new E_Policy__Share();
        share.ParentId = policyId;
        share.UserOrGroupId = userId;
        share.AccessLevel = 'Edit';

        insert share;

        return;
    }
    /**
     * SR契約連結作成
     */
    private static CC_SRPolicy__c createSRPolicy(ID srId, ID poicyId){
        CC_SRPolicy__c srPolicy = new CC_SRPolicy__c();
        srPolicy.CC_CaseId__c = srId;
        srPolicy.CC_PolicyID1__c = poicyId;
        srPolicy.CC_PolicyID2__c = poicyId;
        srPolicy.CC_PolicyID3__c = poicyId;
        srPolicy.CC_PolicyID4__c = poicyId;
        insert srPolicy;
        return srPolicy;
     }
     /**
     * 活動マスタ作成
     */
    private static CC_ActivityMaster__c createSRActivityMaster(){
        CC_ActivityMaster__c srActivityMaster = new CC_ActivityMaster__c();
        srActivityMaster.Name = '受付内容確認';
        insert srActivityMaster;
        return srActivityMaster;
     }
     /**
     * 活動作成
     */
    private static CC_SRActivity__c createSRActivity(ID srId, String batchkey){
        CC_SRActivity__c srActivity = new CC_SRActivity__c();
        srActivity.CC_SRNo__c = srId;
        srActivity.CC_ActivityType__c = '受付内容確認';
        srActivity.CC_Status__c = '終了';
        srActivity.CC_Order__c = 0;
        srActivity.CC_MRBatchKey__c = batchkey;
        insert srActivity;
        return srActivity;
     }
     /**
     * Account作成
     */
    public static Account createAccount(String brNo, String ksection) {
        Account parentAcc = new Account();
        parentAcc.Name = 'TestParentAccountName';
        insert parentAcc;

        Account acc = new Account();
        acc.Name = 'TestAccountName';
        acc.ParentId = parentAcc.Id;
        acc.E_CL2PF_BRANCH__c = brNo;
        acc.KSECTION__c = ksection;
        insert acc;

        return acc;
    }
    /**
     * AccountのShare登録
     */
    public static void createAccountShare(ID accId, ID userId) {
        AccountShare share = new AccountShare();
        share.AccountId = accId;
        share.UserOrGroupId = userId;
        share.AccountAccessLevel = 'Edit';
        share.CaseAccessLevel = 'Edit';
        share.ContactAccessLevel = 'Edit';
        share.OpportunityAccessLevel = 'Edit';

        insert share;

        return;
    }

    /**
     * Contact作成
     */
    public static Contact createContact(Id accId) {
        Contact con = new Contact();
        con.AccountId = accId;
        con.LastName = 'TEST募集人';

        insert con;
        return con;
    }
    /**
     * ID管理作成
     */
    public static E_IDCPF__c createIDCPF(Id userId, String brNo){
        E_IDCPF__c src = new E_IDCPF__c(
             User__c = userId
            ,OwnerId = userId
            ,ZINQUIRR__c = 'BR' + brNo
        );
        insert src;
        return src;
    }
    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    public static User createUser(String profileDevName){
        String Lastname = 'testMR';
        String userName = Lastname + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = Lastname
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        insert src;
        return src;
    }
    /**
     * Profileマップ取得
     * @return Map<String, Id>: Map of ProfileName & ProfileID
     */
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}