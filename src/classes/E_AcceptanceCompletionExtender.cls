global with sharing class E_AcceptanceCompletionExtender extends E_AbstractViewExtender{
    private static final String PAGE_TITLE = '受付完了';

    E_AcceptanceCompletionController extension;
    private String AcceptanceMessage;
    private String contactId;
    //private String type;
    public String type {get; set;}
    public Boolean isVisitedCompliance {get;set;}
    /* コンストラクタ */
    public E_AcceptanceCompletionExtender(E_AcceptanceCompletionController extension){
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        this.contactId = ApexPages.currentPage().getParameters().get('conid');
        this.isVisitedCompliance = false;
        if (getIsEmployee()||access.isSumiseiUser()) {
        	this.isVisitedCompliance = true; 
        } else {
	        if(String.isNotBlank(E_CookieHandler.getCookieCompliance())){
	        	this.isVisitedCompliance = true;
	        }
        }
        this.type = '';
        if (ApexPages.currentPage().getParameters().get('type') != null) {
        	this.type = getMSG().get(ApexPages.currentPage().getParameters().get('type'));
        }
    }
    
    /* 登録内容確認へボタン押下処理 */
    public Pagereference doContactInfo(){
        Pagereference pref = Page.E_ContactInfo;
        pref.getParameters().put('id', this.contactId);
        return pref;
    }
}