@isTest
private class TestE_ContactDaoWithout {

	static String contactLasName = '募集人';
	static String customerNumber = '20161220';
	static String agentNum = 'AGN2016';
	static String mofCode = 'MOF2016';
	static String dayOfBirth = '20001220';

	//getRecordsByIdsのテスト
	@isTest static void getRecordsByIdsTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);
		Set<Id> setId = new Set<Id>();
		setId.add(con.Id);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecordsByIds(setId);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//fillContactsToPolicysのテスト
	@isTest static void fillContactsToPolicysTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);
		E_Policy__c pol = createPolicy(acc,con);
		List<E_Policy__c> pols = new List<E_Policy__c>();
		pols.add(pol);

		Test.startTest();
		List<E_Policy__c> polRecs = E_ContactDaoWithout.fillContactsToPolicys(pols, true, true, true, true, true);
		System.assertEquals(polRecs[0].Contractor__c,con.Id);
		Test.stopTest();

	}

	//fillContactsToPolicyのテスト
	@isTest static void fillContactsToPolicyTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);
		E_Policy__c pol = createPolicy(acc,con);
		List<E_Policy__c> pols = new List<E_Policy__c>();
		pols.add(pol);

		Test.startTest();
		E_Policy__c polRec = E_ContactDaoWithout.fillContactsToPolicy(pol, true, true, true, true, true);
		System.assertEquals(polRec.Contractor__c,con.Id);
		Test.stopTest();
	}

	//getRecByCustomerNumberのテスト
	@isTest static void getRecByCustomerNumberTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRec = E_ContactDaoWithout.getRecByCustomerNumber(customerNumber);
		System.assertEquals(conRec.E_CLTPF_CLNTNUM__c,customerNumber);
		Test.stopTest();
	}

	//getRecByIdのテスト
	@isTest static void getRecByIdTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRec = E_ContactDaoWithout.getRecById(con.Id);
		System.assertEquals(conRec.LastName,contactLasName);
		Test.stopTest();
	}

	//getRecByAgntNumのテスト
	@isTest static void getRecByAgntNumTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecByAgntNum(agentNum);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//getRecByMOFのテスト
	@isTest static void getRecByMOFTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecByMOF(mofCode);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//getRecByCLNTNUMのテスト
	@isTest static void getRecByCLNTNUMTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecByCLNTNUM(customerNumber,dayOfBirth);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//getRecsByCdANDDayofBirthのテスト--agentNumが空
	@isTest static void getRecsByCdANDDayofBirthAgentNumEmptyTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecsByCdANDDayofBirth('', mofCode, dayOfBirth);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//getRecsByCdANDDayofBirthのテスト--mofCodeが空
	@isTest static void getRecsByCdANDDayofBirthMofCodeEmptyTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecsByCdANDDayofBirth(agentNum, '', dayOfBirth);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//getRecsByCdANDDayofBirthのテスト
	@isTest static void getRecByAGorMOFTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecByAGorMOF(agentNum, dayOfBirth);
		System.assertEquals(conRecs[0].LastName,contactLasName);
		Test.stopTest();
	}

	//getRecsByOfficeIdのテスト
	@isTest static void getRecsByOfficeIdTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecsByOfficeId(acc.Id);
		System.assertEquals(conRecs[0].Name,contactLasName);
		Test.stopTest();
	}

	//getRecByIdWithOutのテスト
	@isTest static void getRecByIdWithOutTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRec = E_ContactDaoWithout.getRecByIdWithOut(con.Id);
		System.assertEquals(conRec.Name,contactLasName);
		Test.stopTest();
	}

	//getRecByIdWithOutのテスト
	@isTest static void getRecCustomerNoTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRec = E_ContactDaoWithout.getRecCustomerNo(customerNumber);
		System.assertEquals(conRec.Name,contactLasName);
		Test.stopTest();
	}

	//getRecsIRISAgencySearchBoxAgentのテスト
	@isTest static void getRecsIRISAgencySearchBoxAgentTest1() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecsIRISAgencySearchBoxAgent(' ', contactLasName);
		System.assertEquals(conRecs[0].Name, contactLasName);
		Test.stopTest();
	}

	//getRecsIRISAgencySearchBoxAgentのテスト
	@isTest static void getRecsIRISAgencySearchBoxAgentTest2() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Contact con2 = createContact(acc,'テスト募集人');

		Test.startTest();
		List<Contact> conRec = E_ContactDaoWithout.getRecsIRISAgencySearchBoxAgent('','テスト 募集人','and');
		System.assertEquals(1, conRec.size());
		Test.stopTest();
	}

	//getRecsIRISAgencySearchBoxAgentのテスト
	@isTest static void getRecsIRISAgencySearchBoxAgentTest3() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Contact con2 = createContact(acc,'テスト募集人');

		Test.startTest();
		List<Contact> conRec = E_ContactDaoWithout.getRecsIRISAgencySearchBoxAgent('','テスト 募集人','or');
		System.assertEquals(2, conRec.size());
		Test.stopTest();
	}

	//getRecsByAccountIdのテスト
	@isTest static void getRecsByAccountIdTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecsByAccountId(acc.Id);
		System.assertEquals(conRecs[0].Name, contactLasName);
		Test.stopTest();
	}

	//getRecsByAgntNumのテスト
	@isTest static void getRecsByAgntNumTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecsByAgntNum(agentNum);
		System.assertEquals(conRecs[0].Name, contactLasName);
		Test.stopTest();
	}

	//getRecByAgentNum5のテスト
	@isTest static void getRecByAgentNum5Test() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> conRecs = E_ContactDaoWithout.getRecByAgentNum5(agentNum.left(5));
		System.assertEquals(conRecs.size(), 1);
		System.assertEquals(conRecs[0].Id, con.Id);
		Test.stopTest();
	}

	//getRecByAgentNumberのテスト
	@isTest static void getRecByAgentNumberTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Contact conRecs = E_ContactDaoWithout.getRecByAgentNumber(agentNum);
		System.assertEquals(conRecs.Name, contactLasName);
		Test.stopTest();
	}

	//getRecCountByAgencyIdのテスト
	@isTest static void getRecCountByAgencyIdTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Integer result = E_ContactDaoWithout.getRecCountByAgencyId('', contactLasName);
		System.assertEquals(result, 1);
		Test.stopTest();
	}

	//getRecCountByOfficeIdsのテスト
	@isTest static void getRecCountByOfficeIdsTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Integer result = E_ContactDaoWithout.getRecCountByOfficeIds(new List<String>{acc.Id}, contactLasName);
		System.assertEquals(result, 1);
		Test.stopTest();
	}

	//getRecsMapByIdsのテスト
	@isTest static void getRecsMapByIdsTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Map<Id,Contact> result = E_ContactDaoWithout.getRecsMapByIds(new Set<Id>{con.Id});
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}

	//getRecsContactByCodeのテスト
	@isTest static void getRecsContactByCodeTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		Map<String,Contact> result = E_ContactDaoWithout.getRecsContactByCode(new Set<String>{agentNum});
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}

	//getRecsByCltnumのテスト
	@isTest static void getRecsByCltnumTest() {
		Account acc = createAccount();
		Contact con = createContact(acc);

		Test.startTest();
		List<Contact> result = E_ContactDaoWithout.getRecsByCltnum(customerNumber);
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}

	//getRecsByAgencyIdのテスト
	@isTest static void getRecsByAgencyIdTest01() {
		Account acc = createAccount();
		acc.E_CL2PF_BRANCH__c = 'ZZ';
		update acc;
		Contact con = createContact(acc);
		con.E_CL3PF_ZEATKNAM__c = 'ボシュウニン';
		update con;

		Test.startTest();
		List<Contact> result = E_ContactDaoWithout.getRecsByAgencyId('', 'ZZ', 'ボシュウニン', true);
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}

	//getRecsByAgencyIdのテスト
	@isTest static void getRecsByAgencyIdTest02() {
		Account acc = createAccount();
		acc.E_CL2PF_BRANCH__c = 'ZZ';
		update acc;
		Contact con = createContact(acc);
		con.E_CL3PF_ZEATKNAM__c = 'ボシュウニン';
		update con;

		Test.startTest();
		List<Contact> result = E_ContactDaoWithout.getRecsByAgencyId('', 'ZZ', 'ボシュウニン', false);
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}

	//getRecsByOfficeIdsのテスト
	@isTest static void getRecsByOfficeIdsTest01() {
		Account acc = createAccount();
		Contact con = createContact(acc);
		con.E_CL3PF_ZEATKNAM__c = 'ボシュウニン';
		update con;

		Test.startTest();
		List<Contact> result = E_ContactDaoWithout.getRecsByOfficeIds(new List<String>{acc.Id}, 'ボシュウニン', true);
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}

	//getRecsByOfficeIdsのテスト
	@isTest static void getRecsByOfficeIdsTest02() {
		Account acc = createAccount();
		Contact con = createContact(acc);
		con.E_CL3PF_ZEATKNAM__c = 'ボシュウニン';
		update con;

		Test.startTest();
		List<Contact> result = E_ContactDaoWithout.getRecsByOfficeIds(new List<String>{acc.Id}, 'ボシュウニン', false);
		System.assertEquals(result.size(), 1);
		Test.stopTest();
	}



	//テストデータ作成
	//募集人
	static Contact createContact(Account acc){
		Contact con = new Contact();
		con.LastName = contactLasName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_CLNTNUM__c = customerNumber;
		con.E_CL3PF_AGNTNUM__c = agentNum;
		con.E_CL3PF_KMOFCODE__c = mofCode;
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		return con;
	}

	static Contact createContact(Account acc,String conName){
		Contact con = new Contact();
		con.LastName = conName;
		con.AccountId = acc.Id;
		con.E_COMM_ZCLADDR__c = '東京都日本橋';
		con.E_CLTPF_CLNTNUM__c = '20161221';
		con.E_CL3PF_CLNTNUM__c = '20161221';
		con.E_CL3PF_AGNTNUM__c = 'AGN2017';
		con.E_CL3PF_KMOFCODE__c = 'MOF2017';
		con.E_CLTPF_DOB__c = dayOfBirth;
		con.E_CL3PF_VALIDFLAG__c = '1';
		insert con;
		return con;
	}

	//取引先
	static Account createAccount(){
		Account acp = new Account();
		acp.Name = '取引先';
		insert acp;
		return acp;
	}

	//保険契約ヘッダ
	static E_Policy__c createPolicy(Account acc,Contact con){
		E_Policy__c po = new E_Policy__c();
		po.Contractor__c = con.Id;
		po.Insured__c = con.Id;
		po.MainAgentAccount__c = acc.Id;
		po.SubAgentAccount__c =acc.Id;
		po.MainAgent__c = con.Id;
		po.SubAgent__c = con.Id;
		po.Annuitant__c = con.Id;
		insert po;
		return po;
	}
}