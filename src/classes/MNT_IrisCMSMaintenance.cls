global with sharing class MNT_IrisCMSMaintenance extends SkyEditor2.SkyEditorPageBaseWithSharing {	
			
		public I_ContentMaster__c record{get;set;}	
			
	
		public Component3 Component3 {get; private set;}	
			
		public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}	
			
		public I_ContentMaster__c Component6_val {get;set;}	
		public SkyEditor2.TextHolder Component6_op{get;set;}	
			
		public I_ContentMaster__c Component70_val {get;set;}	
		public SkyEditor2.TextHolder Component70_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}	
		public SkyEditor2.TextHolder Component8_op{get;set;}	
			
		public SkyEditor2__SkyEditorDummy__c Component66_val {get;set;}	
		public SkyEditor2.TextHolder Component66_op{get;set;}	
		public List<SelectOption> valueOptions_I_ContentMaster_c_SelectFrom_c {get;set;}
			
		public SkyEditor2__SkyEditorDummy__c Component92_val {get;set;}	
		public SkyEditor2__SkyEditorDummy__c Component92_val_dummy {get;set;}	
		public SkyEditor2.TextHolder Component92_op{get;set;}	
		public List<SelectOption> valueOptions_I_ContentMaster_c_Category_c {get;set;}
			
	public String recordTypeRecordsJSON_I_ContentMaster_c {get; private set;}
	public String defaultRecordTypeId_I_ContentMaster_c {get; private set;}
	public String metadataJSON_I_ContentMaster_c {get; private set;}
	{
	setApiVersion(31.0);
	}
		public MNT_IrisCMSMaintenance(ApexPages.StandardController controller) {	
			super(controller);	

		SObjectField f;

		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.Page__c;
		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.SelectFrom__c;
		f = I_ContentMaster__c.fields.DocumentNo__c;
		f = I_ContentMaster__c.fields.DispAs__c;
		f = I_ContentMaster__c.fields.DisplayOrder__c;
		f = I_ContentMaster__c.fields.isHTML__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.LastModifiedDate;
		f = I_ContentMaster__c.fields.LastModifiedById;
		f = I_ContentMaster__c.fields.Category__c;
		f = I_ContentMaster__c.fields.DisplayOrderKbn__c;
		f = I_ContentMaster__c.fields.DisplayOrderNo__c;

		List<RecordTypeInfo> recordTypes;
			try {	
					
				mainRecord = null;	
				mainSObjectType = I_ContentMaster__c.SObjectType;	
					
					
				mode = SkyEditor2.LayoutMode.TempSearch_01; 
					
				I_ContentMaster__c lookupObjComponent26 = new I_ContentMaster__c();	
				Component6_val = lookupObjComponent26;	
				Component6_op = new SkyEditor2.TextHolder();	
					
				Component70_val = lookupObjComponent26;	
				Component70_op = new SkyEditor2.TextHolder();	
					
				Component8_val = new SkyEditor2__SkyEditorDummy__c();	
				Component8_op = new SkyEditor2.TextHolder();	
					
				Component66_val = new SkyEditor2__SkyEditorDummy__c();	
				Component66_op = new SkyEditor2.TextHolder();	
				valueOptions_I_ContentMaster_c_SelectFrom_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : I_ContentMaster__c.SelectFrom__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_I_ContentMaster_c_SelectFrom_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				Component92_val = new SkyEditor2__SkyEditorDummy__c();	
				Component92_val_dummy = new SkyEditor2__SkyEditorDummy__c();	
				Component92_op = new SkyEditor2.TextHolder();	
				valueOptions_I_ContentMaster_c_Category_c = new List<SelectOption>{
					new SelectOption('', Label.none)
				};
				for (PicklistEntry e : I_ContentMaster__c.Category__c.getDescribe().getPicklistValues()) {
					if (e.isActive()) {
						valueOptions_I_ContentMaster_c_Category_c.add(new SelectOption(e.getValue(), e.getLabel()));
					}
				}
					
				queryMap.put(	
					'Component3',	
					new SkyEditor2.Query('I_ContentMaster__c')
						.addFieldAsOutput('Name')
						.addFieldAsOutput('DocumentNo__c')
						.addFieldAsOutput('ParentContent__c')
						.addFieldAsOutput('DispAs__c')
						.addFieldAsOutput('DisplayOrder__c')
						.addFieldAsOutput('isHTML__c')
						.addFieldAsOutput('DisplayFrom__c')
						.addFieldAsOutput('ValidTo__c')
						.addFieldAsOutput('LastModifiedDate')
						.addFieldAsOutput('LastModifiedById')
						.limitRecords(500)	
						.addListener(new SkyEditor2.QueryWhereRegister(Component6_val, 'ParentContent__c', 'ParentContent__c', Component6_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component70_val, 'Page__c', 'Page__c', Component70_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component8_val, 'SkyEditor2__Text__c', 'Name', Component8_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component66_val, 'SkyEditor2__Text__c', 'SelectFrom__c', Component66_op, true, 0, false ))
						.addListener(new SkyEditor2.QueryWhereRegister(Component92_val_dummy, 'SkyEditor2__Text__c','Category__c', Component92_op, true, 0, false ))
.addSort('ParentContent__c',True,False).addSort('DisplayOrderKbn__c',True,False).addSort('DisplayOrderNo__c',True,False)
				);	
					
					Component3 = new Component3(new List<I_ContentMaster__c>(), new List<Component3Item>(), new List<I_ContentMaster__c>(), null);
				Component3.ignoredOnSave = true;
				listItemHolders.put('Component3', Component3);	
					
					
				recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_ContentMaster__c.SObjectType, true);
					
					
			p_showHeader = true;
			p_sidebar = false;
			execInitialSearch = false;
			presetSystemParams();
			Component3.extender = this.extender;
			initSearch();
			} catch (SkyEditor2.Errors.SObjectNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.Errors.FieldNotFoundException e) {	
				SkyEditor2.Messages.addErrorMessage(e.getMessage());
			} catch (SkyEditor2.ExtenderException e) {				 e.setMessagesToPage();
			} catch (Exception e) {	
				System.Debug(LoggingLevel.Error, e);	
				SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);	
			}	
		}	
			
		public List<SelectOption> getOperatorOptions_I_ContentMaster_c_ParentContent_c() { 
			return getOperatorOptions('I_ContentMaster__c', 'ParentContent__c');	
		}	
		public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Page_c() { 
			return getOperatorOptions('I_ContentMaster__c', 'Page__c');	
		}	
		public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Name() { 
			return getOperatorOptions('I_ContentMaster__c', 'Name');	
		}	
		public List<SelectOption> getOperatorOptions_I_ContentMaster_c_SelectFrom_c() { 
			return getOperatorOptions('I_ContentMaster__c', 'SelectFrom__c');	
		}	
		public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Category_c() { 
			return getOperatorOptions('I_ContentMaster__c', 'Category__c');	
		}	
			
			
	global with sharing class Component3Item extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		Component3Item(Component3 holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class Component3 extends SkyEditor2.ListItemHolder {
		public List<Component3Item> items{get; private set;}
		@TestVisible
			Component3(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<Component3Item>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new Component3Item(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

			
	}