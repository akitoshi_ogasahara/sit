public abstract class E_AbstractUnitPriceRedirectHandler extends E_AbstractUnitPriceController {
	
    public String topMessage{get;set;}
    public String linkMessage1{get;set;}
    public String linkMessage2{get;set;}
    public String Zprdcd1{get;set;}
    public String Zprdcd2{get;set;}
    public String Zfndcd1{get;set;}
    public String Zfndcd2{get;set;}
    private String prdCode;
    private String encryptPrdCode;
    private String direct = E_const.UP_GFNDCD_DIRECT;
    private String abstractPresentUnder70 = E_const.UP_GFNDCD_PRESENT_UNDER70;
    private String abstractPresentOver71 = E_const.UP_GFNDCD_PRESENT_OVER71;
    private String fndCode35 = E_EncryptUtil.prdCodeConversion(E_const.UP_FNDCD_DIRECT35,true);
    private String fndCode50 = E_EncryptUtil.prdCodeConversion(E_const.UP_FNDCD_DIRECT50,true);
    private String errorCode = 'up2|006';
	private MAP<String,LIST<String>> LinkMap =  E_const.UP_URLPARAM_PRESENT;
    protected Pagereference redirectPref;
        
    /* 初期処理 */
    public Pagereference init(){
    	
    	/*
    	//service hour menu
		PageReference pageRef = isServiceHourMenu();
		if (pageRef != null) {
			return pageRef;
		}
		*/
    	
		// パラメータ（商品コード）の復号化
        prdCode = ApexPages.CurrentPage().getParameters().get('Zprdcd');
        if(String.isBlank(prdCode)){
            return E_Util.toUpErrorPage(errorCode);
        }
        encryptPrdCode = E_EncryptUtil.prdCodeConversion(prdCode,false);
        
        E_UPBPF__c upbpf = E_UPBPFDao.getRecByZPRDCD(encryptPrdCode);
        
        if(upbpf == null){
            //ユニットプライス基本情報がない場合
            return E_Util.toUpErrorPage(errorCode);
        }

        return setParameters(upbpf);
    }
    
    /* 振り分け処理 */
    private Pagereference setParameters(E_UPBPF__c upbpf){
        String gFundCD = upbpf.ZGFUNDCD__c;
		linkmap = E_const.UP_URLPARAM_PRESENT;		

    	// ターゲット型
        if(direct.equals(gFundCD)){
            topMessage = getDISCLAIMER().get('up1|001');
            linkMessage1 = getDISCLAIMER().get('up1|002');
            linkMessage2 = getDISCLAIMER().get('up1|003');
            Zprdcd1 = prdCode;
            Zfndcd1 = fndCode35;
            Zprdcd2 = prdCode;
            Zfndcd2 = fndCode50;
        	return null;
            
        // たしかなおくりもの
        }else if(abstractPresentUnder70.equals(gFundCD) || abstractPresentOver71.equals(gFundCD)){
            topMessage = getDISCLAIMER().get('up1|004');
            linkMessage1 = getDISCLAIMER().get('up1|005');
            linkMessage2 = getDISCLAIMER().get('up1|006');
            Zprdcd1 = E_EncryptUtil.prdCodeConversion(LinkMap.get(encryptPrdCode)[0],true);
            Zprdcd2 = E_EncryptUtil.prdCodeConversion(LinkMap.get(encryptPrdCode)[1],true);
            return null;
           
        // 上記以外の商品
        }else{
            //Pagereference pref = Page.up002;
            Pagereference pref = redirectPref;
            pref.getParameters().put('Zprdcd',prdcode);
			return pref;
        }
    }
}