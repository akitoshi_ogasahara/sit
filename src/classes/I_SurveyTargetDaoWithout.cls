public without sharing class I_SurveyTargetDaoWithout {

	/**
	 * 代理店IdとアンケートIdから対象者取得
	 */
	public static List<I_SurveyTarget__c> getRecByAccountIdAndSurveyId(Id ParentAccountId, Id surveyId){
		List<I_SurveyTarget__c> targetList = new List<I_SurveyTarget__c>();
		targetList = [SELECT id FROM I_SurveyTarget__c WHERE ParentAccount__c =: ParentAccountId AND Survey__c =: surveyId];
		return targetList;
	}
	/**
	 * 募集人が、参照権を持たない代理店格への参照を含むレコードをinsertする際に使用
	 */
	public static void insertRec( I_SurveyTarget__c rec ){
		if( rec != null ) insert rec;
	}
}