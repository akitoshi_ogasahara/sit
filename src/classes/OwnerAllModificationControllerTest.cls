/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OwnerAllModificationControllerTest {

    static Boolean isTest = true;

    /* コンストラクタのテストケース */
    static testMethod void OwnerAllModificationControllerTestCase() {
    	if (isTest) {
	        system.debug('---------------------------------------------- コンストラクタのテストケースの開始 ----------------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        system.assertEquals(false, ctl.isOpp);
	        system.assertNotEquals(null, ctl.bean);
	        system.assertNotEquals(null, ctl.bean.sch_StageName);
	        system.debug('---------------------------------------------- コンストラクタのテストケースの終了 ----------------------------------------------');
    	}
    }

    /* 見込案件検索メソッドのテストケース１【正常：検索件数が存在する場合】 */
    static testMethod void searchOppInfoTestCase1() {
    	if (isTest) {
	    	system.debug('------------------------------------ 見込案件検索メソッドのテストケース１【正常：検索件数が存在する場合】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        setBean(ctl, 1);
	        ctl.searchOppInfo();
	        system.debug('------------------------------------ 見込案件検索メソッドのテストケース１【正常：検索件数が存在する場合】の終了 ------------------------------------');
    	}
    }

    /* 見込案件検索メソッドのテストケース２【正常：検索件数が0件の場合】 */
    static testMethod void searchOppInfoTestCase2() {
    	if (isTest) {
	    	system.debug('------------------------------------ 見込案件検索メソッドのテストケース２【正常：検索件数が0件の場合】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        setBean(ctl, 2);
	        ctl.searchOppInfo();
	        system.assertEquals(true, ctl.opps.isEmpty());
	        system.debug('------------------------------------ 見込案件検索メソッドのテストケース２【正常：検索件数が0件の場合】の終了 ------------------------------------');
    	}
    }

    /* 見込案件検索メソッドのテストケース３【異常】 */
    static testMethod void searchOppInfoTestCase3() {
    	if (isTest) {
	    	system.debug('------------------------------------ 見込案件検索メソッドのテストケース３【異常】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        setBean(ctl, 3);
	        ctl.searchOppInfo();
	        system.debug('------------------------------------ 見込案件検索メソッドのテストケース３【異常】の終了 ------------------------------------');
    	}
    }

    /* 見込案件検索メソッドのテストケース４【正常：検索件数が200件を超える場合】 */
    static testMethod void searchOppInfoTestCase4() {
    	if (isTest) {
	    	system.debug('------------------------------------ 見込案件検索メソッドのテストケース４【正常：検索件数が200件を超える場合】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        setBean(ctl, 5);
	        ctl.searchOppInfo();
	        system.debug('------------------------------------ 見込案件検索メソッドのテストケース４【正常：検索件数が200件を超える場合】の終了 ------------------------------------');
    	}
    }

    /* 所有者変更処理メソッドのテストケース１【異常：Hidden値が存在しない場合】 */
    static testMethod void modifyOwnerNameTestCase1() {
    	if (isTest) {
	    	system.debug('------------------------------------ 所有者変更処理メソッドのテストケース１【異常：Hidden値が存在しない場合】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        ctl.h_Owner_Id = null;
	        ctl.modifyOwnerName();
	        system.debug('------------------------------------ 所有者変更処理メソッドのテストケース１【異常：Hidden値が存在しない場合】の終了 ------------------------------------');
    	}
    }

    /* 所有者変更処理メソッドのテストケース２【異常】 */
    static testMethod void modifyOwnerNameTestCase2() {
    	if (isTest) {
	    	system.debug('------------------------------------ 所有者変更処理メソッドのテストケース２【異常】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        ctl.h_Owner_Id = '00510000000VDoXAAW00510000000VDoXAAW';
	        setBean(ctl, 1);
		    ctl.searchOppInfo();
	        ctl.modifyOwnerName();
	        system.debug('------------------------------------ 所有者変更処理メソッドのテストケース２【異常】の終了 ------------------------------------');
    	}
    }

    /* 所有者変更処理メソッドのテストケース３【正常：所有者一括変更処理】 */
    static testMethod void modifyOwnerNameTestCase3() {
    	if (isTest) {
	    	system.debug('------------------------------------ 所有者変更処理メソッドのテストケース３【正常：所有者一括変更処理】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        ctl.h_Owner_Id = '00510000000VDoXAAW';
	        setBean(ctl, 1);
		    ctl.searchOppInfo();
		    ctl.isAllSelected = true;
		    ctl.AllSelected();
	        ctl.modifyOwnerName();
	        system.debug('------------------------------------ 所有者変更処理メソッドのテストケース３【正常：所有者一括変更処理】の終了 ------------------------------------');
    	}
    }

    /* ランク情報取得メソッドのテストケース１ */
    static testMethod void getStageNameTestCase1() {
    	if (isTest) {
	    	system.debug('------------------------------------ ランク情報取得メソッドのテストケース１の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        List<SelectOption> options = ctl.getStageName();
	        List<String> str = new List<String>();
	        str.add('S');
	        str.add('A');
	        str.add('B');
	        str.add('C');
	        str.add('D');
	        str.add('来月');
	        str.add('破談');
	        system.assertEquals(str.size(), options.size());
	        for (Integer i = 0; i < options.size(); i++) {
	        	system.assertEquals(str.get(i), options.get(i).getLabel());
	        }
	        system.debug('------------------------------------ ランク情報取得メソッドのテストケース１の終了 ------------------------------------');
    	}
    }

    /* その他メソッドのテストケース１【正常】 */
    static testMethod void sonotaTestCase1() {
    	if (isTest) {
	    	system.debug('------------------------------------ その他メソッドのテストケース１【正常】の開始 ------------------------------------');
	        PageReference pageRef = Page.OwnerAllModification;
	        Test.setCurrentPage(pageRef);
	        OwnerAllModificationController ctl = new OwnerAllModificationController();
	        setBean(ctl, 4);
	        ctl.searchOppInfo();
	        ctl.isAllSelected = false;
		    ctl.AllSelected();

			setBean(ctl, 1);
			ctl.searchOppInfo();
		    ctl.isAllSelected = true;
		    ctl.AllSelected();
		    ctl.getSelectedOpps();
	        system.debug('------------------------------------ その他メソッドのテストケース１【正常】の終了 ------------------------------------');
    	}
    }

    /* 画面Bean情報の設定 */
    static private void setBean(OwnerAllModificationController ctl, Integer pattern) {
    	if (pattern == 1) {
    		ctl.bean.sch_AgencyName = '営業';
    	    ctl.bean.sch_OwnerId = '営業';
    	    ctl.bean.sch_From_CloseDate = '2008/01/01';
    	    ctl.bean.sch_To_CloseDate = '2010/12/30';
    	    ctl.bean.sch_StageName.add('S:予算');
    	    ctl.bean.sch_StageName.add('S');
    	    ctl.bean.sch_StageName.add('D');
    	} else if (pattern == 2) {
    		ctl.bean.sch_AgencyName = 'aaa';
    	    ctl.bean.sch_OwnerId = 'aaa';
    	} else if (pattern == 3) {
    		ctl.bean.sch_AgencyName = '営業';
    	    ctl.bean.sch_OwnerId = '営業';
    	    ctl.bean.sch_From_CloseDate = '11111';
    	} else if (pattern == 4) {
    		ctl.bean.sch_AgencyName = '営業';
    	    ctl.bean.sch_OwnerId = '営業 太郎';
    	    ctl.bean.sch_From_CloseDate = '2010/01/01';
    	    ctl.bean.sch_To_CloseDate = '2010/12/30';
    	    ctl.bean.sch_StageName.add('S:予算');
    	    ctl.bean.sch_StageName.add('S');
    	    ctl.bean.sch_StageName.add('D');
    	} else if (pattern == 5) {
    		ctl.bean.sch_AgencyName = '営業';
    	    ctl.bean.sch_OwnerId = '営業';
    	}
    }
}