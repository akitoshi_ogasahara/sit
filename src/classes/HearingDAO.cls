/**
*  クラス名	:	HearingDAO
*  クラス概要	:	クラウドシステム定数クラス
*  @created	:	2014/01/21 Hung Nguyen The
*  @modified:
*/
public class HearingDAO {
	/**
	* getFinanceById
	* ファイナンス情報を取得
	* @param 	: String strId　－＞ファイナンス情報。Id
	* @return	: ファイナンス情報
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	public static Finance__c getFinanceById(String strId){
		List<Finance__c> listFinance = new List<Finance__c>();
		listFinance = [Select  Id, 
								Name,				// Name
								Term__c,			// 決算期(前期)
								H_RD__c,			// 予想売上下落率
								H_JASGD__c,			// 連帯保証債務額
								H_LO__c,			// 役員借入金
								H_DBAC__c,			// 死亡保障額(法人契約)
								H_UG__c,			// 	含み益
								H_DBAI__c,			// 死亡保障額(個人契約)
								H_BRM__c,			// 銀行返済額(月)
								H_BRP__c,			// 銀行返済額　準備額
								H_BFLEM__c,			// 遺族生活資金(月)
								H_BFLET__c,			// 遺族生活資金(必要期間)
								H_RE__c,			// 現在の社長の報酬/年
								H_CRAA__c,			// 社長の報酬減額可能額/年
								H_ERB__c,			// 従業員退職金(予定額)
								H_ORB__c,			// 役員退職金予定額
								Customer_Name__c,	// 顧客.顧客名
								Customer__c			// 顧客名
						From Finance__c
						Where Id =: strId LIMIT 1];
		if(listFinance.isEmpty()){
			return null;
		}
		return listFinance.get(0);
	}
	/**
	* getCustomerById
	* 顧客を取得
	* @param 	: String strId　－＞顧客。Id
	* @return	: 顧客
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	public static Customer__c getCustomerById(String strId){
		List<Customer__c> listCustomer = new List<Customer__c>();
		listCustomer = [Select Id, Name, Industry__c From Customer__c Where Id =: strId LIMIT 1];
		if(listCustomer.isEmpty()){
			return null;
		}
		return listCustomer.get(0);
	}
	/**
	* getIndustryAverage()
	* 業界平均マスタを取得
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified : 
	*/
	public static IndustryAverage__c getIndustryAverage(String strIndustry){
		List<IndustryAverage__c> listIndustryAverage = new List<IndustryAverage__c>();
		listIndustryAverage = [SELECT Id,
								  ROA__c,
								  ROE__c,
								  ROS__c,
								  ICR__c,
								  DR__c,
								  CAR__c,
								  LR__c,
								  ITO__c,
								  QD__c,
								  APTO__c,
								  RTO__c
							FROM IndustryAverage__c
							WHERE Name__c =: strIndustry];
		if(listIndustryAverage.isEmpty()){
			return null;
		}
		return listIndustryAverage.get(0);
	}
	
	/**
	* getFcTimeOut
	* PERSONAタイムアウト回避を取得
	* @param 	:  なし
	* @return	: PERSONAタイムアウト回避
	* @created  : 2015/08/12 Phuong Dinh Vu
	* @modified : 
	*/
	public static FC_TIMEOUT__c getFcTimeOut(){
		List<FC_TIMEOUT__c> fcTimeoutList = new List<FC_TIMEOUT__c>();
		fcTimeoutList = [select Id, Name, FC_TIMEOUT__c From FC_TIMEOUT__c LIMIT 1];
		if(fcTimeoutList.isEmpty()){
			return null;
		}
		return fcTimeoutList.get(0);
	}
}