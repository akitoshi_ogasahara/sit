@isTest
private class TestE_CSVColumnDescribe {

    /**
     *  BaseFieldクラスのテストメソッド
     *      String項目に関して検証を実施
     */
    static testMethod void test_BaseField() {
        CR_Agency__c rec = TESTCR_TestUtil.createCRAgency(false);

        Test.startTest();
        new E_CSVColumnDescribe.BaseField('','');
        E_CSVColumnDescribe.BaseField bf = new E_CSVColumnDescribe.BaseField('Status__c');

        // 項目ラベル名がダブルクォートでEscapeされて取得される
        System.assertEquals('"'+ CR_Agency__c.Status__c.getDescribe().getLabel()+'"', bf.getFieldLabel('CR_Agency__c'));

        // 項目値の取得
        rec.Status__c = 'ステータス001';
        System.assertEquals(rec.Status__c, bf.getStringValue(rec));
        System.assertEquals('', bf.getStringValue(null));

        // 項目値（エスケープ）の取得
        System.assertEquals('"'+rec.Status__c+'"', bf.getEscapedString(rec));
        System.assertEquals('""', bf.getEscapedString(null));

        Test.stopTest();
    }

    /**
     *  DecimalFieldクラスのテストメソッド
     *      数値項目に関して検証を実施
     */
    static testMethod void test_DecimalField() {
        CR_Agency__c rec = TESTCR_TestUtil.createCRAgency(true);
        rec = [select id,name,OutsrcCnt__c from CR_Agency__c Where Id =:rec.Id];

        Test.startTest();
        E_CSVColumnDescribe.DecimalField bf = new E_CSVColumnDescribe.DecimalField('OutsrcCnt__c');

        // 項目ラベル名がダブルクォートでEscapeされて取得される
        System.assertEquals('"'+CR_Agency__c.OutsrcCnt__c.getDescribe().getLabel()+'"', bf.getFieldLabel('CR_Agency__c'));

        // 項目値の取得
        System.assertEquals(rec.OutsrcCnt__c, Decimal.valueOf(bf.getStringValue(rec)));
        System.assertEquals('', bf.getStringValue(null));

        // 項目値（エスケープ）の取得
        System.assertEquals('"'+String.valueOf(rec.OutsrcCnt__c)+'"', bf.getEscapedString(rec));
        System.assertEquals('""', bf.getEscapedString(null));

        Test.stopTest();
    }

    /**
     *  BooleanFieldクラスのテストメソッド
     *      チェックボックス項目に関して検証を実施
     */
    static testMethod void test_BooleanField() {
        CR_Agency__c rec = TESTCR_TestUtil.createCRAgency(false);

        Test.startTest();
        new E_CSVColumnDescribe.BooleanField('','');
        E_CSVColumnDescribe.BooleanField bf = new E_CSVColumnDescribe.BooleanField('Mail2DPA__c');

        // 項目ラベル名がダブルクォートでEscapeされて取得される
        System.assertEquals('"'+CR_Agency__c.Mail2DPA__c.getDescribe().getLabel()+'"', bf.getFieldLabel('CR_Agency__c'));

        // 項目値の取得(Trueの場合)
        rec.Mail2DPA__c = true;
        System.assertEquals(rec.Mail2DPA__c, bf.getStringValue(rec) == '1');
        // 項目値（エスケープ）の取得
        System.assertEquals('"1"', bf.getEscapedString(rec));

        // 項目値の取得(Falseの場合)
        rec.Mail2DPA__c = false;
        System.assertEquals('0', bf.getStringValue(rec));
        System.assertEquals('"0"', bf.getEscapedString(rec));

        //　レコードがNullの場合の検証
        System.assertEquals('', bf.getStringValue(null));
        System.assertEquals('""', bf.getEscapedString(null));

        Test.stopTest();
    }

    /**
     *  DatetimeFieldクラスのテストメソッド
     *      日付項目に関して検証を実施
     */
    static testMethod void test_DateTimeField() {
        CR_Agency__c rec = TESTCR_TestUtil.createCRAgency(true);

        Test.startTest();
        E_CSVColumnDescribe.DatetimeField bf = new E_CSVColumnDescribe.DatetimeField('ApprovalDate__c','日付項目');

        // 項目ラベル名がダブルクォートでEscapeされて取得される
        System.assertEquals('"日付項目"', bf.getFieldLabel('CR_Agency__c'));

        // 項目値（エスケープ）の取得
        rec.ApprovalDate__c = Datetime.newInstance(2014,11,31,15,05,0);         //11/31　⇒ 12/01
        System.assertEquals('"'+'2014/12/01 15:05'+'"', bf.getEscapedString(rec));
        System.assertEquals('""', bf.getEscapedString(null));

        Test.stopTest();
    }

    /**
     *  DateFieldクラスのテストメソッド
     *      日付項目に関して検証を実施
     */
    static testMethod void test_DateField() {
        Agency__c agency = new Agency__c(Name = 'keizuOffice');
        insert agency;

        Test.startTest();
        E_CSVColumnDescribe.DateField bf = new E_CSVColumnDescribe.DateField('DELETEDATE__c','日付項目');

        // 項目ラベル名がダブルクォートでEscapeされて取得される
        System.assertEquals('"日付項目"', bf.getFieldLabel('Agency__c'));

        // 項目値（エスケープ）の取得
        agency.DELETEDATE__c = Date.newInstance(2014,11,31);
        System.assertEquals('"'+'2014/12/01'+'"', bf.getEscapedString(agency));
        System.assertEquals('""', bf.getEscapedString(null));

        Test.stopTest();
    }

    /**
     *  ParentStringFieldクラスのテストメソッド
     *      参照関係のテキスト項目に関して検証を実施
     */
    static testMethod void test_ParentStringField() {
        CR_Agency__c rec = TESTCR_TestUtil.createCRAgency(false);
        User MR= TESTCR_TestUtil.createUser(true,'acrc_test_user','E_EmployeeStandard');
        MR = [SELECT Id,Name FROM User WHERE Id = :mr.Id];
        rec.MR__r = MR;

        Test.startTest();
        E_CSVColumnDescribe.ParentStringField pf = new E_CSVColumnDescribe.ParentStringField('MR__r.Name', 'User');

        // 項目ラベル名がダブルクォートでEscapeされて取得される
        System.assertEquals('"' + CR_Agency__c.MR__c.getDescribe().getLabel() + ': '
                                + User.Name.getDescribe().getLabel() + '"'
                            , pf.getFieldLabel('CR_Agency__c'));

        // 項目値の取得
        System.assertEquals(rec.MR__r.Name, pf.getStringValue(rec));
        System.assertEquals('', pf.getStringValue(null));


        // 項目値（エスケープ）の取得
        System.assertEquals('"'+rec.MR__r.Name+'"', pf.getEscapedString(rec));
        System.assertEquals('""', pf.getEscapedString(null));

        //参照関係　未設定
        rec.MR__r.clear();
        System.assertEquals('', pf.getStringValue(rec));

        // 項目ラベル名が上書き
        E_CSVColumnDescribe.ParentStringField pf2 = new E_CSVColumnDescribe.ParentStringField('MR__r.Name', 'User', 'MR名上書き');
        System.assertEquals('"MR名上書き"', pf2.getFieldLabel('CR_Agency__c'));

    }

}