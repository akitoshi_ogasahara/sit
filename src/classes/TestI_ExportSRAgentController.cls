@isTest
private class TestI_ExportSRAgentController {
	
	private static Account distribute;
	private static Account office;
	private static Account suboffice;
	private static Contact agent;
	private static E_BizDataSyncLog__c ebizLog;
	private static E_AgencySalesResults__c asr;
	private static E_AgencySalesResults__c asrAT;

	/**
	 * String mainSObjectAPIName()
	 */
	static testMethod void mainSObjectAPIName_test01(){
		// テストデータ作成
		createData();

		PageReference pr = Page.IRIS_ExportSRAgent;
		Test.setCurrentPage(pr);
		String sWhere = 'Hierarchy__c = \'AT\' AND BusinessDate__c = \'20000101\' ';
		sWhere += 'AND AgentCode__c != null ';
		//代理店格
		sWhere += 'AND ParentAccount__c = \'' + distribute.id + '\'';
		pr.getParameters().put('soqlWhere', sWhere);

		Test.StartTest();
		
		I_ExportSRAgentController controller = new I_ExportSRAgentController();
		
		Test.stopTest();
		
		// assertion
		System.assertEquals('E_AgencySalesResults__c', controller.mainSObjectAPIName());
	}

	/**
	 * getCSVHeader
	 */
	private static testMethod void getCSVHeader_test01(){

		// テストデータ作成
		createData();

		PageReference pr = Page.IRIS_ExportSRAgent;
		Test.setCurrentPage(pr);
		String sWhere = 'Hierarchy__c = \'AT\' AND BusinessDate__c = \'20000101\' ';
		sWhere += 'AND AgentCode__c != null ';
		//代理店格
		sWhere += 'AND ParentAccount__c = \'' + distribute.id + '\'';
		pr.getParameters().put('soqlWhere', sWhere);

		Test.StartTest();
		
		I_ExportSRAgentController controller = new I_ExportSRAgentController();
		
		Test.stopTest();
		system.assert(String.isNotBlank(controller.getCSVTitle()));
		system.assert(String.isNotBlank(controller.getCSVHeader()));
		system.assert(!controller.getLines().isEmpty());
	}
	
	private static void createData(){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';		// 営業日

		//連携ログ作成
		Date d = E_Util.string2DateyyyyMMdd(BUSINESS_DATE);
		ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = d,BatchEndDate__c = d);
		insert ebizLog;

		// Account 格
		distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
		insert distribute;
		
		// Account 事務所
		office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
		insert office;
		suboffice = new Account(Name = 'テスト事務所SUB',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
		insert suboffice;
		
		// Contact 募集人作成
		agent = new Contact(AccountId = office.id, LastName = 'テスト募集人',E_CL3PF_AGNTNUM__c = '11111');
		insert agent;

		// 代理店挙績情報
		asr = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AH'
		);
		insert asr;
		// 代理店挙績情報
		asrAT = new E_AgencySalesResults__c(
			ParentAccount__c = distribute.Id,
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = 'AT',
			Account__c = office.id,
			Agent__c = agent.id
		);
		insert asrAT;
	}
}