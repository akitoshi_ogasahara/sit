@isTest
private class MNT_ExportSalesToolMainteControllerTest {

    // コンストラクタ
    private static testMethod void ExportSalesToolMainteTest001(){
        String mainSObjectAPIName;

        Test.startTest();
            Test.setCurrentPage(Page.MNT_ExportSalesToolMainte);
            MNT_ExportSalesToolMainteController controller = new MNT_ExportSalesToolMainteController();

            mainSObjectAPIName = controller.mainSObjectAPIName();
        Test.stopTest();

        System.assertEquals(mainSObjectAPIName, Schema.SObjectType.I_ContentMaster__c.Name);
    }

}