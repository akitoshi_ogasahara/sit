/**
 * 点検方法結果登録コンポーネント コントローラー
 */
public with sharing class SC_ChangeFixedController {
	// 対象レコード
	public SC_Office__c record {get; set;}
	/* 点検実施方法（結果） */
	//選択リスト値
	public List<SelectOption> fixedList {get; set;}
	//編集権限
	public Boolean canEditFixed {get; set;}
	// 編集モードフラグ
	public Boolean isEditFixed {get; set;}

	/* 不備ステータス */
	//参照権限
	public Boolean canViewDefect {get; set;}
	//編集権限
	public Boolean canEditDefect {get; set;}
	// 編集モードフラグ
	public Boolean isEditDefect {get; set;}
	// 変更前ステータス
	public String beforeDefect;

	// コンストラクタ
	public SC_ChangeFixedController(){
		//初期化
		isEditFixed = false;
		isEditDefect = false;
		beforeDefect = '';

		// 選択リスト値生成
		fixedList = new List<SelectOption>();
		for(PickListEntry pe : Schema.SObjectType.SC_Office__c.fields.SCFixed__c.getPicklistValues()){
			SelectOption option = new SelectOption(pe.getValue(), pe.getLabel());

			fixedList.add(option);
		}
	}

	/*
	 * 編集状態を切り替え
	 */
	public void doChangeDisplay(){
		isEditFixed = !isEditFixed;
		return;
	}

	/*
	 * 編集状態を切り替え（不備）
	 */
	public void doChangeDisplayDefect(){
		isEditDefect = !isEditDefect;
		beforeDefect = record.Defect__c;
		return;
	}

	/**
	 * 保存ボタン押下
	 */
	public void doSave(){
		//不備が変更された際は履歴を更新
		if(!E_Util.null2Blank(beforeDefect).equals(E_Util.null2Blank(record.Defect__c))){
			createHistoryByDefect(beforeDefect);
		}
		update record;
		isEditFixed = false;
		isEditDefect = false;

		return;
	}


	/**
	 * ステータスがNullの際の表示文言
	 */
	private String getDefectStatus01(){
		return SC_Const.DEFECT_STATUS_01;
	}
	/**
	 * 表示用ステータス
	 */
	public String getDefectStatus(){
		return getDispDefect(record.Defect__c);
	}
	/**
	 * 表示用ステータス作成
	 */
	private String getDispDefect(String defect){
		return String.isBlank(defect)? getDefectStatus01() : defect ;
	}
	/***************************************************
	 * 不備ステータス更新
	 ***************************************************/
	/**
	 *　不備ステータス更新時の履歴作成
	 */
	private void createHistoryByDefect(String beforeDefect){
		String str = '';
		beforeDefect = getDispDefect(beforeDefect) ;
		String beforeHistory = record.StatusHistory__c;
		String afterDefect = getDispDefect(record.Defect__c);
			
		// 不備ステータス履歴
		str += '----------------------------------------------------------------------------------------';
		str += '\n';
		str += '- ';
		str += System.now().format();
		str += '   ';
		str += UserInfo.getName();
		str += '   [';
		str += beforeDefect;
		str += ' => ';
		str += afterDefect;
		str += ']';
		str += '\n'; 
		if(String.isBlank(beforeHistory)){
			str += '----------------------------------------------------------------------------------------';
			str += '\n';
		}
		str += E_Util.null2Blank(beforeHistory);
		record.StatusHistory__c = str;
	}
}