@isTest
private class TestI_SalesResultsController {

	private static E_Unit__c unit;
	private static User mr;
	private static E_BizDataSyncLog__c ebizLog;
	private static E_EmployeeSalesResults__c asr;

	/**
	 * 実行ユーザ: 本社営業部
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test001(){
		// 実行ユーザ作成（本社営業部）
		User actUser = TestI_TestUtil.createUser(true, 'fstestsrc', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// ページ表示
		PageReference pr = Page.IRIS_SalesResults;
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));		//パラメータ'row'に繰り返し行数を付与
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_SalesResultsController controller = new I_SalesResultsController();
			Test.stopTest();
			// Assertion
			PageReference redirectPage = Page.IRIS_Unit;
			System.assertEquals(controller.pageAction().getUrl(), redirectPage.getUrl());
		}
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: なし
	 */
	private static testMethod void pageAction_test002(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(false, 'fstestsrc', 'ＭＲ');
		actUser.Unit__c = 'テスト営業部';
		insert actUser;
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('MR');

		// ページ表示
		PageReference pr = Page.IRIS_SalesResults;
		pr.getParameters().put(I_Const.LIST_URL_PARAM_DISPLAY_ROWS, String.valueOf(I_Const.LIST_MAX_ROWS));		//パラメータ'row'に繰り返し行数を付与
		Test.setCurrentPage(pr);

		System.runAs(actUser){

			Test.startTest();
			I_SalesResultsController controller = new I_SalesResultsController();
			controller.pageAction();

			Test.stopTest();
			// Assertion
			System.assertEquals(controller.mrid,String.valueOf(actUser.id));
			System.assertEquals(controller.viewType,'2');
			System.assertEquals(controller.getBrunchCodeBymrId(),'88');
			System.assertEquals(controller.getBRNo(),'88');
			System.assert(controller.getASRDisclaimer().isEmpty());
		}
	}
	/**
	 * 実行ユーザ: 本社営業部
	 * パラメータ: あり
	 */
	private static testMethod void pageAction_test003(){

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstestsrc', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BP**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('BR');

		// ページ表示
		PageReference pr = Page.IRIS_SalesResults;
		Test.setCurrentPage(pr);
		pr.getParameters().put('brno', '88');

		System.runAs(actUser){

			Test.startTest();
			I_SalesResultsController controller = new I_SalesResultsController();
			controller.pageAction();

			Test.stopTest();
			// Assertion
			System.assertEquals(controller.unitid,'88');
			System.assertEquals('IRIS_SalesResultsPrint?brno=88', controller.getPreviewURL());

			pr.getParameters().put('brno', '00');
			controller.pageAction();
		}
	}
	/**
	 * 実行ユーザ: 本社営業部
	 * パラメータ: あり
	 */
	private static testMethod void pageAction_test004(){

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstestsrc', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('MR');

		// ページ表示
		PageReference pr = Page.IRIS_SalesResults;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrid', mr.Id);

		System.runAs(actUser){

			Test.startTest();
			I_SalesResultsController controller = new I_SalesResultsController();
			controller.pageAction();

			Test.stopTest();
			// Assertion
			System.assertEquals('IRIS_SalesResultsPrint?mrid='+mr.Id, controller.getPreviewURL());
		}
	}
	/**
	 * 実行ユーザ: 本社営業部
	 * パラメータ: あり
	 */
	private static testMethod void pageAction_test006(){

		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'fstestsrc', 'システム管理者');
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		createData('BR');

		// ページ表示
		PageReference pr = Page.IRIS_SalesResults;
		Test.setCurrentPage(pr);
		pr.getParameters().put('brno', '88');

		System.runAs(actUser){

			Test.startTest();
			I_SalesResultsController controller = new I_SalesResultsController();
			controller.pageAction();

			Test.stopTest();
			// Assertion
			System.assertEquals(controller.unitid,'88');
			System.assertEquals('IRIS_SalesResultsPrint?brno=88', controller.getPreviewURL());

			pr.getParameters().put('brno', '00');
			controller.pageAction();
		}
	}

	private static void createData(String hierarchy){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';		// 代理店格コード
		String CODE_OFFICE = '10002';			// 事務所コード
		String CODE_BRANCH = '88';				// 支社コード
		String CODE_DISTRIBUTE_SUB = '20001';	// 代理店格コード
		String CODE_OFFICE_SUB = '20002';		// 事務所コード
		String CODE_BRANCH_SUB = '99';			// 支社コード
		String BUSINESS_DATE = '20000101';		// 営業日

		//営業部情報作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = CODE_BRANCH,Name = 'テスト営業部');
		insert unit;

		// MR
		mr = TestI_TestUtil.createUser(false, 'fstest', 'ＭＲ');
		mr.Unit__c = 'テスト営業部';
		insert mr;

		//連携ログ作成
		Date d = E_Util.string2DateyyyyMMdd(BUSINESS_DATE);
		ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = d,BatchEndDate__c = d);
		insert ebizLog;

		// 社内挙績情報
		asr = new E_EmployeeSalesResults__c(
			BusinessDate__c = BUSINESS_DATE,
			Hierarchy__c = hierarchy,
			E_Unit__c = unit.id
		);
		if(hierarchy == 'MR'){
			asr.mr__c = mr.id;
		}
		insert asr;
	}
}