public with sharing class I_NewPolicyDetailController extends I_AbstractController {

	//新契約
	public I_NewPolicyDTO.NewPolicy newPolicy {get; private set;}
	// ソートキー
	public String sortType {get;private set;}
	// ソート順
	public Boolean sortIsAsc {get;private set;}
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	//不備対応中表示フラグ
	public Boolean isDispDefectArea {get;private set;}
	//不備対応中
	public Boolean isDefect {get;private set;}
	//査定完了入金待ち
	public Boolean isWait {get;private set;}
	//査定完了入金待ち表示フラグ
	public Boolean isDispWaitArea {get;private set;}

	public Boolean isDispEnd {
		get{
			return (newPolicy.status == I_NewPolicyConst.STATUS_NAME_QZ
				|| newPolicy.status == I_NewPolicyConst.STATUS_NAME_I
				|| newPolicy.status == I_NewPolicyConst.STATUS_NAME_Q
				|| newPolicy.status == I_NewPolicyConst.STATUS_NAME_Z);
		}private set;
	}

	//Contractor
	public I_NewPolicyDetailController() {
		//初期化
		pageMessages = getPageMessages();
		newPolicy = new I_NewPolicyDTO.NewPolicy();
		sortIsAsc = true;
		isDispDefectArea = false;
		isDefect = false;
		isWait = false;
		isDispWaitArea = false;
		sortType = 'key';
	}

	//初期処理 「ページアクション内でコール」
	public override PageReference init() {
		PageReference pr = super.init();
		pageAccessLog.Name = '④手続き状況を確認する_詳細画面';
		try{
			String sid = ApexPages.currentPage().getParameters().get('id');
			if(String.isEmpty(sid)){
				pageMessages.addErrorMessage(getMSG().get(E_Const.ERROR_MSG_NOTALLOW));
				return null;
			}
			createNewPolicy((Id)sid);
		}catch (Exception e){
			pageMessages.addErrorMessage(getMSG().get(E_Const.ERROR_MSG_NOTALLOW));
			system.debug('[Error]' + e.getMessage());
			return null;
		}
		//初期ソート処理
		I_NewPolicyDTO.sortType = sortType;
		I_NewPolicyDTO.sortIsAsc = sortIsAsc;
		newPolicy.defects.sort();
		return pr;
	}

	/**
	 * メニューキー設定(仮)
	 */
	protected override String getLinkMenuKey(){
		return 'newpolicy';
	}

	/**
	 * データ作成
	 */
	private void createNewPolicy (ID pid){
		List<E_NewPolicy__c> recs = E_NewPolicyDao.getNewPolicyById(pid);
		//契約情報設定
		newPolicy = new I_NewPolicyDTO.NewPolicy(recs.get(0));

		//団体用、ステータス設定Set
		Set<String> statusSet = new Set<String>();
		List<E_NewPolicyDefect__c> defs = new List<E_NewPolicyDefect__c>();
		//ステータス判定
		for(E_NewPolicy__c rec: recs) {
			statusSet.add(rec.status__c);
			if(rec.status__c == I_NewPolicyConst.STATUS_NAME_P2){
				//不備情報を格納
				defs.addAll(rec.E_NewPolicyDefects__r);
			} else if (rec.Status__c == I_NewPolicyConst.STATUS_NAME_I && String.isBlank(newPolicy.establishedAC)) {
				// 成立の場合{成立Ａ／Ｃ（年}/{成立Ａ／Ｃ（月）}を設定する
				newPolicy.establishedAC = generateEstablishedAC(rec);
			}
		}
		//不備設定用
		Set<String> keys = new Set<String>();
		//不備設定
		for(E_NewPolicyDefect__c defect :defs){
			if(defect.FUPSTAT__c == I_NewPolicyConst.DEFECT_STATUS_DELETE){
				//削除済みは判定に利用しない
				continue;
			}
			//ステータスが'O'のものを不備に追加
			if(defect.FUPSTAT__c == I_NewPolicyConst.DEFECT_STATUS_OPEN){
				E_DefectMaster__c tempDef = getDefectMap().get(defect.FUPCDE__c);
				if(tempDef == null){
					//未登録の不備コードには「詳細はお問合せください。」を表示
					tempDef = getDefectMap().get(I_NewPolicyConst.DEFECT_OTHER);
				}
				if(keys.contains(tempDef.SortKey__c)){
					//既に追加済みのものは終了
					continue;
				}
				//一件でも不備がOPENなら表示する
				isDispDefectArea = true;
				newPolicy.defects.add(new I_NewPolicyDTO.Defect(tempDef));
				keys.add(tempDef.SortKey__c);
			}
		}
		newPolicy.status = I_NewPolicyController.getStatus(statusSet);
		newPolicy.styleClass = I_NewPolicyConst.statusCssMap.get(newPolicy.status);
		if(recs.get(0).KFRAMEIND__c == 'Y'){
			newPolicy.insured = String.valueOf(recs.size()) + '名';
		}else{
			//newPolicy.insured = recs.get(0).COMM_ZINSNAM__c;
			//外字対応
			newPolicy.insured = recs.get(0).InsuredName__c;
		}
	}

	/**
	 * 不備置換用Map
	 */
	private static Map<String,E_DefectMaster__c> defectMap;
	private static Map<String,E_DefectMaster__c> getDefectMap () {
		if (defectMap == null || defectMap.isEmpty()){
			defectMap = new Map<String,E_DefectMaster__c>();
			for (E_DefectMaster__c rec :E_DefectMasterDao.getRecs()) {
				defectMap.put(rec.code__c,rec);
			}
		}
		return defectMap;
	}

	/**
	 * 成立アイコン
	 */
	public Boolean getIsStatusI(){
		return newPolicy.status == I_NewPolicyConst.STATUS_NAME_I;
	}

	/**
	 * 謝絶アイコン
	 */
	public Boolean getIsStatusZ(){
		return newPolicy.status == I_NewPolicyConst.STATUS_NAME_Z;
	}

	/**
	 * 取下アイコン
	 */
	public Boolean getIsStatusQ(){
		return newPolicy.status == I_NewPolicyConst.STATUS_NAME_Q;
	}

	/**
	 * 謝絶/取下げアイコン
	 */
	public Boolean getIsStatusQZ(){
		return newPolicy.status == I_NewPolicyConst.STATUS_NAME_QZ;
	}

	/**
	 * 受け取ったレコードのステータスが成立であった場合に成立年月を生成する。
	 * @param E_NewPolicy__c: rec
	 * @return String 成立Ａ／Ｃ（年）/成立Ａ／Ｃ（月）
	 */
	private static String generateEstablishedAC(E_NewPolicy__c rec) {
		String result;
		if (rec != null && rec.Status__c == I_NewPolicyConst.STATUS_NAME_I && rec.BATCACTYR__c != null && rec.BATCACTMN__c != null) {
			result = String.format('{0}/{1}', new String[]{
				rec.BATCACTYR__c != null ? String.valueOf(rec.BATCACTYR__c).leftPad(4).replaceAll(' ', '0') : '',
				rec.BATCACTMN__c != null ? String.valueOf(rec.BATCACTMN__c).leftPad(2).replaceAll(' ', '0') : ''
			});
		} else {
			result = '';
		}
		return result;
	}

}