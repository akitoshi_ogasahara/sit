public with sharing class I_MeritChartController {
    public List<I_MeritTableDTO> meritChartDtoList{get; set;}
    public String currentYearStr {get;set;}
    public I_MeritChartController(){
        this.meritChartDtoList = new List<I_MeritTableDTO>();
    }
    public String getMeritChartJson(){
            if( meritChartDTOList == null || meritChartDTOList.isEmpty() ){
                return null;
            }
            List<String> jsonList = new List<String>();
            List<String> keikanensuList = new List<String>();
            List<String> ageList = new List<String>();
            List<String> sList = new List<String>();
            List<String> pRuikeiList = new List<String>();
            List<String> cvList = new List<String>();
            List<String> simpleHenreiRtList = new List<String>();
            List<String> jisshitsuFutanGakuList = new List<String>();
            List<String> jisshitsuHenreiRtList = new List<String>();

            for( I_MeritTableDTO rec : meritChartDtoList ){
                keikanensuList.add(I_Util.toStr(rec.keikaNensu)); //経過年数
                ageList.add(I_Util.toStr(rec.age)); //年齢
                sList.add((rec.s != null) ? I_Util.toStr(String.valueOf(rec.s * 10000)) : '""'); //保険料 単位が万円のため円に修正
                //sList.add(I_Util.toStr(String.valueOf(rec.s * 10000))); //保険料 単位が万円のため円に修正
                pRuikeiList.add(I_Util.toStr(String.valueOf(rec.pRuikei))); //保険料累計
                cvList.add(I_Util.toStr(String.valueOf(rec.cv))); //解約返戻金
                simpleHenreiRtList.add(I_Util.toStr(String.valueOf(rec.simpleHenreiRt))); //単純返戻率
                jisshitsuFutanGakuList.add(I_Util.toStr(String.valueOf(rec.jisshitsuFutanGaku))); //実質負担額
                jisshitsuHenreiRtList.add(I_Util.toStr(String.valueOf(rec.jisshitsuHenreiRt))); //実質返戻率
            }

            jsonList.add( '{"keikanensuList":["' + String.join(keikanensuList,'","') + '"]' );
            jsonList.add( '"ageList":["' + String.join(ageList,'","') + '"]' );
            jsonList.add( '"s":[' + String.join(sList,',') + ']');
            jsonList.add('"pRuikei":[' + String.join(pRuikeiList,',') + ']');
            jsonList.add('"simpleHenreiRt":[' + String.join(simpleHenreiRtList,',') + ']');
            jsonList.add('"cv":[' + String.join(cvList,',') + ']');
            jsonList.add('"jisshitsuFutanGaku":[' + String.join(jisshitsuFutanGakuList,',') + ']');
            jsonList.add('"jisshitsuHenreiRt":[' + String.join(jisshitsuHenreiRtList,',') + ']}');
            //system.debug('Json = ' + String.join(jsonList,',\n'));
            return String.join(jsonList,',');
    }
}