global with sharing class E_ColiPDFController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_ColiPDFExtender getExtender() {return (E_ColiPDFExtender)extender;}
	
	
	public CRLPFPDFtable CRLPFPDFtable {get; private set;}
	public dataTableCOVPF dataTableCOVPF {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_ColiPDFController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!CRLPFPDFtable_item.record.BNYPC__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','true');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','{! IF(CRLPFPDFtable_item.record.BNYTYP__c==\'09\',\'-\',\'\')}');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','1');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2931');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2931',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COMM_CCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1694');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1694',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COMM_OCCDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1695');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1695',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_doBreak','true');
		tmpPropMap.put('Component__Width','60');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component2827');
		tmpPropMap.put('Component__Name','EPDFPageBreaker');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2827',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!extender.DeathBenefit}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!extender.CrtableCordFlg}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2479');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2479',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZAPLRATE__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COLI_ZAPLDCF__c,record.COLI_ZEAPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.000');
        tmpPropMap.put('p_floorvalue','1000');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2934');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2934',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZEAPLPRN__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COLI_ZAPLDCF__c,record.COLI_ZEAPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2430');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2430',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZEAPLINT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COLI_ZAPLDCF__c,record.COLI_ZEAPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2435');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2435',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZEAPLTOT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COLI_ZAPLDCF__c,record.COLI_ZEAPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2436');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2436',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZPLRATE__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COMM_ZPLDCF__c,record.COLI_ZEPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','0.000');
        tmpPropMap.put('p_floorvalue','1000');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2937');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2937',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZEPLPRN__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COMM_ZPLDCF__c,record.COLI_ZEPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','17');
		tmpPropMap.put('Component__id','Component2453');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2453',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZEPLINT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COMM_ZPLDCF__c,record.COLI_ZEPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2454');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2454',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZEPLTOT__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!IF(AND(record.COMM_ZPLDCF__c,record.COLI_ZEPLTOT__c!=0),true,false)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2455');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2455',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COMM_PTDATE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','{!NOT(OR(record.COMM_STATCODE__c==\'Y\',record.COLI_ZNPTDDCF__c==False))}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2088');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2088',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COLI_PCESDTE__c}');
        tmpPropMap.put('dateFmt','yyyy/MM');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','{!OR(AND(record.COMM_STATCODE__c!=\'Y\' ,NOT(record.COLI_ZNPTDDCF__c)),AND(record.COMM_STATCODE__c!=\'Y\' ,(record.COLI_ZNPTDDCF__c),record.COLI_ZPCDTDCF__c))}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2089');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2089',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.COLI_RCPTDT__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','{!record.COLI_ZADVPDCF__c}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','20');
		tmpPropMap.put('Component__id','Component1760');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1760',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_AVPRMRCV__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!record.COLI_ZADVPDCF__c}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2715');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2715',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_REMNBANN__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','{!record.COLI_ZADVPDCF__c}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2716');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2716',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COMM_ZTOTPREM__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','93');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2745');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2745',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!extender.SurrenderValueSum}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','{!IF(or(record.COLI_ZCVDCF__c,record.COLI_ZUNPCF__c ),\'?\',\'\')}');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2764');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2764',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZCSHVAL__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','{!IF(record.COLI_ZCVDCF__c,\'?\',\'\')}');
        tmpPropMap.put('p_isRendered','true');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2763');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2763',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_val','{!record.COLI_ZUNPREM__c}');
        tmpPropMap.put('p_NulltoHyphen','false');
        tmpPropMap.put('p_ZerotoHyphen','false');
        tmpPropMap.put('p_ZerotoConversion','');
        tmpPropMap.put('p_altChar','{!IF(record.COMM_ZEFREQ__c==\'月払\',\'0\',IF(record.COLI_ZUNPCF__c,\'?\',\'\'))}');
        tmpPropMap.put('p_isRendered','{!IF(record.COMM_ZEFREQ__c==\'月払\',true,record.COLI_ZUNPDCF__c)}');
        tmpPropMap.put('p_prefix','');
        tmpPropMap.put('p_suffix','');
        tmpPropMap.put('p_numformat','');
        tmpPropMap.put('p_floorvalue','0');
        tmpPropMap.put('p_conversion','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2742');
		tmpPropMap.put('Component__Name','ENumberLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2742',tmpPropMap);


		SObjectField f;

		f = Contact.fields.E_CLTPF_CLNTNUM__c;
		f = E_Policy__c.fields.ContractorName__c;
		f = E_Policy__c.fields.COMM_CHDRNUM__c;
		f = E_Policy__c.fields.InsuredName__c;
		f = E_Policy__c.fields.COMM_ZRSTDESC__c;
		f = E_CRLPF__c.fields.BNYTYPName__c;
		f = E_CRLPF__c.fields.ZCLNAME__c;
		f = E_COVPF__c.fields.coli_text255_1__c;
		f = E_COVPF__c.fields.coli_text255_2__c;
		f = E_COVPF__c.fields.COMM_ZRCESDSC__c;
		f = E_COVPF__c.fields.COMM_ZPCESDSC__c;
		f = E_COVPF__c.fields.coli_text255_3__c;
		f = E_COVPF__c.fields.coli_text255_4__c;
		f = E_COVPF__c.fields.coli_text255_5__c;
		f = E_Policy__c.fields.COLI_ZLOANTOT__c;
		f = E_Policy__c.fields.COLI_ZEADVPRM__c;
		f = E_CRLPF__c.fields.BNYTYP__c;
		f = E_CRLPF__c.fields.CLRRROLE__c;
		f = E_CRLPF__c.fields.VALIDFLAG__c;
		f = E_CRLPF__c.fields.BNYPC__c;
		f = E_CRLPF__c.fields.CLNTNUM__c;
		f = E_COVPF__c.fields.COLI_ZCRIND__c;
		f = E_COVPF__c.fields.RecordTypeId;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('ContractorName__c');
			mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
			mainQuery.addFieldAsOutput('InsuredName__c');
			mainQuery.addFieldAsOutput('COMM_ZRSTDESC__c');
			mainQuery.addFieldAsOutput('COLI_ZLOANTOT__c');
			mainQuery.addFieldAsOutput('COLI_ZEADVPRM__c');
			mainQuery.addFieldAsOutput('DCOLI_ZCSBNAMT__c');
			mainQuery.addFieldAsOutput('COLI_ZCSHVAL__c');
			mainQuery.addFieldAsOutput('SPVA_ZREFAMT__c');
			mainQuery.addFieldAsOutput('COLI_ZCVDCF__c');
			mainQuery.addFieldAsOutput('COLI_ZPLCAPT__c');
			mainQuery.addFieldAsOutput('COLI_ZEPLPRN__c');
			mainQuery.addFieldAsOutput('COLI_ZEPLTOT__c');
			mainQuery.addFieldAsOutput('COMM_ZPLDCF__c');
			mainQuery.addFieldAsOutput('COLI_ZEPLINT__c');
			mainQuery.addFieldAsOutput('COLI_ZPLRATE__c');
			mainQuery.addFieldAsOutput('SPVA_ZPLINTRAT__c');
			mainQuery.addFieldAsOutput('SPVA_ZINTLABEL__c');
			mainQuery.addFieldAsOutput('COMM_OCCDATE__c');
			mainQuery.addFieldAsOutput('COLI_PCESDTE__c');
			mainQuery.addFieldAsOutput('COLI_ZPCDTDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZDPTYDSC__c');
			mainQuery.addFieldAsOutput('SPVA_ZDPTYDSP__c');
			mainQuery.addFieldAsOutput('SPVA_ZDPTEIST__c');
			mainQuery.addFieldAsOutput('COMM_PTDATE__c');
			mainQuery.addFieldAsOutput('COLI_ZNPTDDCF__c');
			mainQuery.addFieldAsOutput('SPVA_ZFATRATE__c');
			mainQuery.addFieldAsOutput('MainAgent__c');
			mainQuery.addFieldAsOutput('MainAgent__r.Name');
			mainQuery.addFieldAsOutput('MainAgent__r.AccountId');
			mainQuery.addFieldAsOutput('MainAgent__r.Account.E_CL1PF_ZAHKNAME__c');
			mainQuery.addFieldAsOutput('MainAgent__r.Account.E_CL2PF_ZEAYNAM__c');
			mainQuery.addFieldAsOutput('MainAgent__r.Account.Name');
			mainQuery.addFieldAsOutput('MainAgent__r.Account.AgencyName__c');
			mainQuery.addFieldAsOutput('MainAgent__r.Account.Agency__c');
			mainQuery.addFieldAsOutput('SubAgent__c');
			mainQuery.addFieldAsOutput('SubAgent__r.Name');
			mainQuery.addFieldAsOutput('SubAgent__r.LastName');
			mainQuery.addFieldAsOutput('SubAgent__r.AccountId');
			mainQuery.addFieldAsOutput('SubAgent__r.Account.Name');
			mainQuery.addFieldAsOutput('SubAgent__r.Account.AgencyName__c');
			mainQuery.addFieldAsOutput('SubAgent__r.Account.Agency__c');
			mainQuery.addFieldAsOutput('SubAgent__r.FirstName');
			mainQuery.addFieldAsOutput('COLI_ZAPLDCF__c');
			mainQuery.addFieldAsOutput('COMM_CCDATE__c');
			mainQuery.addFieldAsOutput('DCOLI_ZADVDESC__c');
			mainQuery.addFieldAsOutput('COLI_ZADVPDCF__c');
			mainQuery.addFieldAsOutput('COLI_REMNBANN__c');
			mainQuery.addFieldAsOutput('COLI_AVPRMRCV__c');
			mainQuery.addFieldAsOutput('COLI_RCPTDT__c');
			mainQuery.addFieldAsOutput('COMM_ZAGCYNUM01__c');
			mainQuery.addFieldAsOutput('COMM_ZAGCYNUM02__c');
			mainQuery.addFieldAsOutput('COMM_ZAGCYDCF__c');
			mainQuery.addFieldAsOutput('COLI_ZGCLTNM__c');
			mainQuery.addFieldAsOutput('COLI_GRUPNUM__c');
			mainQuery.addFieldAsOutput('COLI_ZGRUPDCF__c');
			mainQuery.addFieldAsOutput('COMM_ZECHNL__c');
			mainQuery.addFieldAsOutput('COMM_ZTOTPREM__c');
			mainQuery.addFieldAsOutput('COMM_ZEFREQ__c');
			mainQuery.addFieldAsOutput('COLI_ZVARFLAG__c');
			mainQuery.addFieldAsOutput('COMM_ZCOVRNAM__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE__c');
			mainQuery.addFieldAsOutput('COMM_CRTABLE2__c');
			mainQuery.addFieldAsOutput('COLI_ZAPLCAPT__c');
			mainQuery.addFieldAsOutput('COLI_ZEAPLPRN__c');
			mainQuery.addFieldAsOutput('COLI_ZEAPLTOT__c');
			mainQuery.addFieldAsOutput('COLI_ZEAPLINT__c');
			mainQuery.addFieldAsOutput('COLI_ZAPLRATE__c');
			mainQuery.addFieldAsOutput('COMM_SPLITC01__c');
			mainQuery.addFieldAsOutput('COMM_SPLITC__c');
			mainQuery.addFieldAsOutput('COLI_ZAGNTNUM01__c');
			mainQuery.addFieldAsOutput('SPVA_AGNTNUM01__c');
			mainQuery.addFieldAsOutput('COLI_ZAGNTNUM02__c');
			mainQuery.addFieldAsOutput('SPVA_AGNTNUM02__c');
			mainQuery.addFieldAsOutput('COLI_ZUNPREM__c');
			mainQuery.addFieldAsOutput('COLI_ZUNPCF__c');
			mainQuery.addFieldAsOutput('COLI_ZUNPDCF__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			CRLPFPDFtable = new CRLPFPDFtable(new List<E_CRLPF__c>(), new List<CRLPFPDFtableItem>(), new List<E_CRLPF__c>(), null);
			listItemHolders.put('CRLPFPDFtable', CRLPFPDFtable);
			query = new SkyEditor2.Query('E_CRLPF__c');
			query.addFieldAsOutput('BNYTYPName__c');
			query.addFieldAsOutput('ZCLNAME__c');
			query.addFieldAsOutput('BNYPC__c');
			query.addFieldAsOutput('BNYTYP__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('CRLPFPDFtable', 'E_Policy__c');
			CRLPFPDFtable.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('CRLPFPDFtable', query);
			
			dataTableCOVPF = new dataTableCOVPF(new List<E_COVPF__c>(), new List<dataTableCOVPFItem>(), new List<E_COVPF__c>(), null);
			listItemHolders.put('dataTableCOVPF', dataTableCOVPF);
			query = new SkyEditor2.Query('E_COVPF__c');
			query.addFieldAsOutput('coli_text255_1__c');
			query.addFieldAsOutput('coli_text255_2__c');
			query.addFieldAsOutput('COMM_ZRCESDSC__c');
			query.addFieldAsOutput('COMM_ZPCESDSC__c');
			query.addFieldAsOutput('coli_text255_3__c');
			query.addFieldAsOutput('coli_text255_4__c');
			query.addFieldAsOutput('coli_text255_5__c');
			query.addFieldAsOutput('COMM_CRTABLE__c');
			query.addFieldAsOutput('COMM_ZCOVRNAM__c');
			query.addFieldAsOutput('COMM_ZNAME40A01__c');
			query.addFieldAsOutput('COMM_ZNAME20A__c');
			query.addFieldAsOutput('COMM_ZNAME40A02__c');
			query.addFieldAsOutput('COMM_SUMINS__c');
			query.addFieldAsOutput('COLI_ZPREMADD__c');
			query.addFieldAsOutput('COLI_ZDTHAMTA__c');
			query.addFieldAsOutput('COLI_ZDPTYDSP__c');
			query.addFieldAsOutput('COLI_ZCRIND__c');
			query.addFieldAsOutput('DCOLI_SINSTAMT__c');
			query.addFieldAsOutput('COMM_CRTABLE2__c');
			query.addFieldAsOutput('COMM_ZCOVRNAMFIX__c');
			query.addFieldAsOutput('COLI_ZCOVRNAMES__c');
			query.addFieldAsOutput('COLI_ZSTDSUM__c');
			query.addFieldAsOutput('DCOLI_ZRSTDESC__c');
			query.addFieldAsOutput('COLI_SUMINSF__c');
			query.addFieldAsOutput('COLI_INSTPREM__c');
			query.addFieldAsOutput('RecordTypeId');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('dataTableCOVPF', 'E_Policy__c');
			dataTableCOVPF.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('dataTableCOVPF', query);
			
			registRelatedList('E_CRLPFs__r', 'CRLPFPDFtable');
			registRelatedList('E_COVPFs__r', 'dataTableCOVPF');
			
			SkyEditor2.Query CRLPFPDFtableQuery = queryMap.get('CRLPFPDFtable');
			CRLPFPDFtableQuery.addWhereIfNotFirst('AND');
			CRLPFPDFtableQuery.addWhere('(  ( BNYTYP__c = \'01\' OR BNYTYP__c = \'02\' OR BNYTYP__c = \'03\' OR BNYTYP__c = \'04\' OR BNYTYP__c = \'05\' OR BNYTYP__c = \'06\' OR BNYTYP__c = \'07\' OR BNYTYP__c = \'08\' OR BNYTYP__c = \'09\' OR BNYTYP__c = \'10\' OR BNYTYP__c = \'11\' OR BNYTYP__c = \'40\' OR BNYTYP__c = \'50\' OR BNYTYP__c = \'51\' OR BNYTYP__c = \'52\' )  AND CLRRROLE__c = \'BN\' AND VALIDFLAG__c != \'3\')');
			CRLPFPDFtableQuery.addSort('BNYTYP__c',True,True).addSort('BNYPC__c',False,True).addSort('CLNTNUM__c',True,True);
			SkyEditor2.Query dataTableCOVPFQuery = queryMap.get('dataTableCOVPF');
			dataTableCOVPFQuery.addWhereIfNotFirst('AND');
			dataTableCOVPFQuery.addWhere('(  ( COLI_ZCRIND__c = \'C\' OR COLI_ZCRIND__c = \'R\' )  AND RecordType.DeveloperName = \'ECOVPF\')');
			dataTableCOVPFQuery.addSort('COLI_ZCRIND__c',True,True);
			p_showHeader = false;
			p_sidebar = false;
			p_isPdf = true;
			p_pdfPageSize = 'A4';
			p_pdfOrientation = 'portrait';
			p_pdfMargin = '0.5cm';
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_ColiPDFExtender(this);
			init();
			
			CRLPFPDFtable.extender = this.extender;
			dataTableCOVPF.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class CRLPFPDFtableItem extends SkyEditor2.ListItem {
		public E_CRLPF__c record{get; private set;}
		@TestVisible
		CRLPFPDFtableItem(CRLPFPDFtable holder, E_CRLPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class CRLPFPDFtable extends SkyEditor2.ListItemHolder {
		public List<CRLPFPDFtableItem> items{get; private set;}
		@TestVisible
			CRLPFPDFtable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<CRLPFPDFtableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new CRLPFPDFtableItem(this, (E_CRLPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class dataTableCOVPFItem extends SkyEditor2.ListItem {
		public E_COVPF__c record{get; private set;}
		@TestVisible
		dataTableCOVPFItem(dataTableCOVPF holder, E_COVPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null  && record.RecordTypeId == null){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class dataTableCOVPF extends SkyEditor2.ListItemHolder {
		public List<dataTableCOVPFItem> items{get; private set;}
		@TestVisible
			dataTableCOVPF(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<dataTableCOVPFItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new dataTableCOVPFItem(this, (E_COVPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}