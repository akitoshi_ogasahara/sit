@isTest
private class TestE_SameCltNumContactController {

	@testSetup
	static void createBaseTestData(){
		//Acount
		Account acc1 = TestE_TestUtil.createAccount(true);
		Account acc2 = TestE_TestUtil.createAccount(false);
		acc2.name = 'ダミー代理店';
		insert acc2;

		//Contact(client)
		String clientLastName = 'clent001';
		Contact clientRec = TestE_TestUtil.createContact(true, acc2.Id, clientLastName, '12345678', null);
		//Contact(agent)
		List<Contact> agentContactRecs = new List<Contact>();
		for(Integer i=1; i < 6; i++){
			agentContactRecs.add(
				TestE_TestUtil.createContact(false, acc1.Id, 'agent' + String.valueOf(i), null, E_Util.leftPad(String.valueOf(i), 5, '0') )
			);
		}
		//募集人顧客コードにclientRecのE_CLTPF_CLNTNUM__cと同じ値を設定
		for(Contact con : agentContactRecs){
			con.E_CL3PF_CLNTNUM__c = clientRec.E_CLTPF_CLNTNUM__c;
		}
		insert agentContactRecs;

		//User(client)
		User cleintUser = TestI_TestUtil.createAgentUser(true,clientLastName, 'E_CustomerCommunity',clientRec.Id);
		//User(agent)
		List<User> agentUserRecs = new List<User>();
		String agentUserName;
		for(Contact con : agentContactRecs){
			agentUserName = 'agentUser' + con.Id;
			agentUserRecs.add(TestI_TestUtil.createAgentUser(true,agentUserName, 'E_PartnerCommunity_IA',con.Id));
		}

		//EIDC
		List<E_IDCPF__c> eidcRecs = new List<E_IDCPF__c>();
		for(User user : agentUserRecs){
			eidcRecs.add(TestE_TestUtil.createIDCPF(false, user.Id));
		}
		//ZWEBID,ID種別を設定
		List<String> zidTypeList = new List<String>{'AH','AY','AT','OW'};
		for(E_IDCPF__c eidc : eidcRecs){
			eidc.ZIDTYPE__c = zidTypeList[Integer.valueOf(Math.random()*3)];
			eidc.ZWEBID__c = String.valueOf(eidc.OwnerId).right(10);
		}
		insert eidcRecs;

		//E_Policy__c(contractor)
		List<E_Policy__c> epolicyRecs = new List<E_Policy__c>();
		epolicyRecs.add(TestE_TestUtil.createPolicy(false, null, E_Const.POLICY_RECORDTYPE_COLI, E_Util.leftPad('1', 8, '0')));
		epolicyRecs.add(TestE_TestUtil.createPolicy(false, null, E_Const.POLICY_RECORDTYPE_DCOLI, E_Util.leftPad('2', 8, '0')));
		epolicyRecs.add(TestE_TestUtil.createPolicy(false, null, E_Const.POLICY_RECORDTYPE_SPVA, E_Util.leftPad('3', 8, '0')));
		epolicyRecs.add(TestE_TestUtil.createPolicy(false, null, E_Const.POLICY_RECORDTYPE_DSPVA, E_Util.leftPad('4', 8, '0')));
		epolicyRecs.add(TestE_TestUtil.createPolicy(false, null, E_Const.POLICY_RECORDTYPE_SPVA, E_Util.leftPad('5', 8, '0')));

		//各Contact参照項目へContactを設定
		for (Integer i = 0; i < epolicyRecs.size(); i++){
			if(i == 0){
				epolicyRecs.get(i).Contractor__c = clientRec.Id;
				epolicyRecs.get(i).Insured__c = clientRec.Id;
				epolicyRecs.get(i).Annuitant__c = clientRec.Id;
			} else if(i == 1){
				epolicyRecs.get(i).Corporate__c = clientRec.Id;
			} else if(i == 2){
				epolicyRecs.get(i).Contractor__c = clientRec.Id;
				epolicyRecs.get(i).MainAgent__c = agentContactRecs.get(i).Id;
				epolicyRecs.get(i).SPVA_ZANNSTFLG__c = true;
				epolicyRecs.get(i).SPVA_ZVWDCF__c = '0';
			} else if(i == 3){
				epolicyRecs.get(i).Contractor__c = clientRec.Id;
				epolicyRecs.get(i).SubAgent__c = agentContactRecs.get(i).Id;
				epolicyRecs.get(i).SPVA_ZANNSTFLG__c = true;
				epolicyRecs.get(i).SPVA_ZVWDCF__c = '1';
			} else if(i >= 4){
				epolicyRecs.get(i).Contractor__c = clientRec.Id;
				epolicyRecs.get(i).SubAgent__c = agentContactRecs.get(i).Id;
				epolicyRecs.get(i).SPVA_ZANNSTFLG__c = true;
				epolicyRecs.get(i).SPVA_ZVWDCF__c = '2';
			}
		}
		insert epolicyRecs;
	}

	//initTestParamNull
	//パラメーターがNULL
	static testMethod void initTestParamNull() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		
		Test.startTest();
		controller.init();
		Test.stopTest();

		System.assertEquals(null, controller.contactList);
	}
	
	//initTestParamBlank
	//パラメーターがBlank
	static testMethod void initTestParamBlank() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '');
		
		Test.startTest();
		controller.init();
		Test.stopTest();

		System.assertEquals(null, controller.contactList);
	}

	//initTestParam1valid
	//パラメーター1が有効値
	static testMethod void initTestParam1valid() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '12345678');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '');
		
		Test.startTest();
		controller.init();
		Test.stopTest();

		//テストデータの全コンタクトが取得される想定
		Integer conNum = [select count() from contact];
		System.assertEquals(conNum, controller.contactList.size());
	}

	//initTestParam2valid
	//パラメーター2が有効値
	static testMethod void initTestParam2valid() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '12345678');
		
		Test.startTest();
		controller.init();
		Test.stopTest();

		//テストデータの全コンタクトが取得される想定
		Integer conNum = [select count() from contact];
		System.assertEquals(conNum, controller.contactList.size());
	}

	//initTestParamAllvalid
	//パラメーター全てが有効値
	static testMethod void initTestParamAllvalid() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '12345678');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '12345678');
		
		Test.startTest();
		controller.init();
		Test.stopTest();

		//テストデータの全コンタクトが取得される想定
		Integer conNum = [select count() from contact];
		System.assertEquals(conNum, controller.contactList.size());
	}

	//openPolicyDetailTest
	//パラメーター全てが有効値
	//顧客レコードに紐付くopenPolicyDetail取得
	static testMethod void openPolicyDetailTest() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '12345678');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '12345678');

		Contact clientCon = [select id,name from Contact where E_CLTPF_CLNTNUM__c != null limit 1];
		Test.startTest();
		controller.init();
		controller.policyDetailTargetContactId = clientCon.Id;
		controller.policyDetailTargetContactName = clientCon.Name;
		controller.selectedContactRowNum = '1';
		controller.openPolicyDetail();
		Test.stopTest();

		//テストデータの顧客レコードが関連付くPolicy数は5
		System.assertEquals(5, controller.polDetailWrapList.size());

	}

	//mrUserNotPolicyShareTest
	//パラメーター全てが有効値
	//顧客レコードに紐付くopenPolicyDetail取得,実行MRユーザにPolicyが非共有
	static testMethod void mrUserNotPolicyShareTest() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '12345678');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '12345678');

		//User(MR)
		User mrUser = TestI_TestUtil.createUser(true,'testMR', 'ＭＲ');

		Contact clientCon = [select id,name from Contact where E_CLTPF_CLNTNUM__c != null limit 1];
		Test.startTest();
		system.runAs(mrUser){
			controller.init();
			controller.policyDetailTargetContactId = clientCon.Id;
			controller.policyDetailTargetContactName = clientCon.Name;
			controller.selectedContactRowNum = '1';
			controller.openPolicyDetail();
		}
		Test.stopTest();

		//テストデータの顧客レコードが関連付くPolicy数は2だが、MRユーザに共有されている数は0
		System.assertEquals(0, controller.polDetailWrapList.size());
	}

	//mrUserPolicyShareTest
	//パラメーター全てが有効値
	//顧客レコードに紐付くopenPolicyDetail取得,実行MRユーザにPolicyが1件共有
	static testMethod void mrUserPolicyShareTest() {
		E_SameCltNumContactController controller = new E_SameCltNumContactController();

		PageReference pageRef = Page.E_SameCltNumContact;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('ecltpfCltnum', '12345678');
		ApexPages.currentPage().getParameters().put('ecl3pfCltnum', '12345678');

		//User(MR)
		User mrUser = TestI_TestUtil.createUser(true,'testMR', 'ＭＲ');

		Contact clientCon = [select id,name from Contact where E_CLTPF_CLNTNUM__c != null limit 1];

		//policy share
		List<E_Policy__share> sharePolicies = new List<E_Policy__share>();
		List<E_Policy__c> policies = [select id,name from E_Policy__c where Contractor__c = :clientCon.Id];
		for(E_Policy__c spolicy : policies){
			E_Policy__share polShare = new E_Policy__share();
			polShare.ParentId = spolicy.Id;
			polShare.UserOrGroupId = mrUser.Id;
			polShare.AccessLevel = 'Read';
			polShare.RowCause = Schema.E_Policy__share.RowCause.Manual;
			sharePolicies.add(polShare);
		}
		insert sharePolicies;

		Test.startTest();
		system.runAs(mrUser){
			controller.init();
			controller.policyDetailTargetContactId = clientCon.Id;
			controller.policyDetailTargetContactName = clientCon.Name;
			controller.selectedContactRowNum = '1';
			controller.openPolicyDetail();
		}
		Test.stopTest();

		//テストデータの顧客レコードが関連付くPolicy数は5だが、MRユーザに共有されている数は4
		System.assertEquals(4, controller.polDetailWrapList.size());
	}
}