public with sharing class ASS_OfficeSummaryController {
	
	//直近の行動のリスト
	public List<Event> currentEvents {get{
			if(currentEvents != null){
				return currentEvents;
			} 
				return ASS_EventDao.getCurrentEventsByWhatId(accountId);
		}
		set;
	}
	//過去の行動のリスト
	public List<Event> oldEvents {get{
			if(oldEvents != null){
				return oldEvents;
			} 
				return ASS_EventDao.getOldEventsByWhatId(accountId);
		} 
		set;
	}

	//メモリスト
	public List<ASS_Memo__c> memoList {get{
		if(memoList != null){
			return memoList;
		}
			return getNotes();
		} set;
	}

	//ログインユーザID
	public String mrId {get; private set;}
	//事務所ID
	public String accountId {get; private set;}
	//営業部ID
	public String unitId;

	//編集ボタン表示フラグ
	public Boolean showEditButton {get; private set;}
	//編集フラグ
	public Boolean editFlag{get; private set;}
	//月
	public String month {get; private set;}

	//変更前のメモのリストとIDのマップ
	public Map<Id,String> memoMap;

	//デフォルトのリストの行数
	public Integer listDefCount {get; private set;}

	//直近リスト行数
	public Integer cuRowCount{get; private set;}
	//全件表示ボタン表示フラグ
	public Boolean cuIsShowALLBtn {get; private set;}
	//元に戻るボタン表示フラグ
	public Boolean cuIsShowRetBtn {get; private set;}
	//直近の行動のリストのサイズ
	public Integer cuEventSize {get; private set;}

	//過去リスト行数
	public Integer olRowCount{get; private set;}
	//全件表示ボタン表示フラグ
	public Boolean olIsShowALLBtn {get; private set;}
	//元に戻るボタン表示フラグ
	public Boolean olIsShowRetBtn {get; private set;}
	//過去の行動のリストのサイズ
	public Integer olEventSize {get; private set;}

	//営業日報メモタイトル
	public final String MEMOTITLE = '代理店フォローにあたっての特記事項（キーマンや登録形態等）';

	
	//コンストラクタ
	public ASS_OfficeSummaryController() {
		mrId 				= ApexPages.currentPage().getParameters().get('mrId');
		accountId 			= ApexPages.currentPage().getParameters().get('accountId');
		unitId 				= ApexPages.currentPage().getParameters().get('unitId');
		month 				= String.valueOf(System.today().Month());
		editFlag 			= false;
		listDefCount 		= 10;

		cuRowCount 			= listDefCount;
		cuIsShowALLBtn 		= true;
		cuIsShowRetBtn	 	= false;
		cuEventSize 		= currentEvents.size();

		olRowCount 			= listDefCount;
		olIsShowALLBtn 		= true;
		olIsShowRetBtn 		= false;
		olEventSize 		= oldEvents.size();
		getNotes();
	}

	//事務所取得
	public Account getOffice(){
		return ASS_AccountDao.getOfficeById(accountId);
	}

	//見込案件リスト取得
	public List<Opportunity> getOppList(){
		String[] level = new String[]{'S','A','B','C','D','来月','破談'};
		return ASS_OppDaoWithout.getOppsByOfficeId(accountId, level);
	}

	//メモ取得
	public List<ASS_Memo__c> getNotes(){
		
		List<ASS_Memo__c> originmemoList = new List<ASS_Memo__c>();
		memoList = new List<ASS_Memo__c>();
		memoMap = new Map<Id,String>();

		memoList = originmemoList = ASS_ASSMemoDaoWithout.getMemosByOfficeId(accountId);

		if(memoList.isEmpty()){
			showEditButton = false;
		}else {
			showEditButton = true;
			for(ASS_Memo__c ass : originmemoList){
				memoMap.put(ass.Id,ass.Contents__c);
			}
		}
		return memoList;
	}

	public void editNote(){
		editFlag = true;
	}

	public void newnote(){
		ASS_Memo__c newASSMemo = new ASS_Memo__c(Name = MEMOTITLE, Account__c= accountId);
		memoList.add(newASSMemo);
		editFlag = true;
	}

	public PageReference deleteNote(){
		ASS_ASSMemoDaoWithout.deleteMemo(memoList);
		//Delete memoList;
		getNotes();
		editFlag = false;

		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('accountId',accountId);
		pr.getParameters().put('mrId', mrId);
		pr.getParameters().put('unitId', unitId);
		pr.setRedirect(true);
		return pr;
	}
	public PageReference cancel() {
		editFlag = false;

		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('accountId',accountId);
		pr.getParameters().put('mrId', mrId);
		pr.getParameters().put('unitId', unitId);
		pr.setRedirect(true);
		return pr;
	}

	//更新されたものと新規で作成されたレコードのみupsert対象にする
	public PageReference save(){
		//Upsert用のリストを作成
		List<ASS_Memo__c> upsertList = new List<ASS_Memo__c>();
		for(ASS_Memo__c ass :memoList){
			//既存レコードの場合
			if(memoMap.containsKey(ass.Id)){
				if(ass.Contents__c != memoMap.get(ass.Id)){
					upsertList.add(ass);
				}
			//新規レコードの場合
			}else{
				upsertList.add(ass);
			}
			
		}
	upsert(upsertList);
	editFlag = false;

	PageReference pr = Page.ASS_OfficeSummary;
	pr.getParameters().put('accountId',accountId);
	pr.getParameters().put('mrId', mrId);
	pr.getParameters().put('unitId', unitId);
	pr.setRedirect(true);
	return pr;
	}

	//直近リスト全件表示
	public void cuAddRows(){
		cuIsShowALLBtn = false;
		cuIsShowRetBtn = true;
		cuRowCount = currentEvents.size();
	}
	//直近リストデフォルトにもどす
	public void cuReturnRows(){
		cuIsShowALLBtn = true;
		cuIsShowRetBtn = false;
		cuRowCount = listDefCount;
	}

	//過去リスト全件表示
	public void olAddRows(){
		olIsShowALLBtn = false;
		olIsShowRetBtn = true;
		olRowCount = oldEvents.size();
	}
	//過去リストデフォルトにもどす
	public void olReturnRows(){
		olIsShowALLBtn = true;
		olIsShowRetBtn = false;
		olRowCount = listDefCount;
	}


	//行動新規作成
	public PageReference newEvent() {
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('retPgName','/apex/ASS_OfficeSummary');
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		pr.getParameters().put('accountId',accountId);
		pr.setRedirect(true);
		return pr;
	}

	//行動参照
	public PageReference viewEvent() {
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('retPgName','/apex/ASS_OfficeSummary');
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		pr.getParameters().put('accountId',accountId);
		pr.getParameters().put('eventId',ApexPages.currentPage().getParameters().get('eventId'));
		pr.setRedirect(true);
		return pr;
	}

	//戻る
	public PageReference back(){
		PageReference pr = Page.ASS_MRPage;
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		pr.setRedirect(true);
		return pr;
	}

}