/**
 * 値変更_締切日コンポーネント
 */
public with sharing class SC_ChangeValue_LimitController {
	// 対象レコード
	public SC_Office__c record {get; set;}
	
	// 編集権限
	public Boolean canEdit {get; set;}
	// 編集モードフラグ
	public Boolean isEdit {get; set;}
	// 変更前値
	public String beforeValue {get; set;}

	/**
	 * Constractor
	 */
	public SC_ChangeValue_LimitController(){
		// 初期化
		isEdit = false;
		beforeValue = '';
	}

	/*
	 * 編集状態を切り替え
	 */
	public PageReference doChangeDisplay(){
		isEdit = !isEdit;
		beforeValue = record.LimitFixed__c;
		if(!isEdit){
			PageReference pr = Page.IRIS_SCSelfCompliances;
			pr.getParameters().put('id',record.Id);
			return pr.setRedirect(true);
		}
		return null;
	}

	/**
	 * 保存ボタン押下
	 */
	public PageReference doSave(){
		// 締切日（初回）がNullの場合、変更前の締切日（結果）をセット
		if(String.isBlank(record.LimitDefault__c)){
			record.LimitDefault__c = beforeValue;
		}
		update record;
		
		// フラグの初期化
		isEdit = false;
		PageReference pr = Page.IRIS_SCSelfCompliances;
		pr.getParameters().put('id',record.Id);
		return pr.setRedirect(true);
	}
}