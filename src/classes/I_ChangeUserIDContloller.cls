//public with sharing class I_ChangeUserIDContloller extends E_AbstractController{
public with sharing class I_ChangeUserIDContloller extends E_InfoController{
	private static final String MENU_KEY_AGENT = 'login_agent';
	private static final String MENU_KEY_CUSTOMER = 'login_customer';

	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get; set;}

	//ブラウザ判定Handler
	public E_BrowserJudgeHandler browserHandler {get; set;}

	//メールアドレス
	public String mailAddress {get; set;}
	//確認メールアドレス
	public String confirmEmailAddress {get; set;}

	//ログインユーザID
	public String loginUserId {get; set;}
	//suffix付きログインユーザID(User.UserName)
	private String sufUserId;

	//アクセスログ
	private E_Log__c pageAccessLog;

	//E_Home.pageから受け取るURLパラメータ
	public Boolean suggestion {get; set;}
	//ID変更受付完了フラグ
	public Boolean idChangeFlg {get; set;}

	//入力欄ラベル
	public String inputLabel {get; set;}

	//コンストラクタ
	public I_ChangeUserIDContloller() {
		super();
		pageMessages = getPageMessages();
		pgTitle = 'ID変更';
		loginUserId = UserInfo.getUserName().substringBeforeLast(System.Label.E_USERNAME_SUFFIX);
		idChangeFlg = false;

		//パラメータ"suggestion"を受け取る
		String suggestionStr = ApexPages.CurrentPage().getParameters().get('suggestion');
		suggestion = false;
		if(!String.isBlank(suggestionStr)){
			if(suggestionStr == '1'){
				suggestion = true;
			}
		}

		inputLabel = 'メールアドレス';
	}

	//更新ボタン押下
	public PageReference updateEmail() {
		PageReference pr = null;
		pageMessages.clearMessages();

		Savepoint sp = Database.setSavepoint();
		try {
			//入力チェック
			if(validateIsInputError()){
				return pr;
			}

			//新規メールアドレス重複チェック
			if(isOverlapUserName()){
				//エラーメッセージの表示
				pageMessages.addErrorMessage(getMSG().get('CID|008'));
				return pr;
			}

			//ユーザ情報の更新
			//IDリクエストにユーザ情報を入力
			sufUserId = UserInfo.getUserName();     //UserName取得
			User userRec = E_UserDaoWithout.getUserRecByUserName(sufUserId);
			E_IDCPF__c eidc = E_IDCPFDaoWithout.getRecsByContactId(userRec.Contact.Id);
			E_IDRequest__c idReqest = E_IDRequestDaoWithout.insEIdrRecWithMail(eidc, mailAddress);

			//メールアドレス確認メールの送信
			sendConfirmEmail(idReqest);

//          }

		} catch(Exception e) {
			Database.rollback(sp);
			pageMessages.addErrorMessage(e.getMessage());
			//アクセスログ
			pageAccessLog = E_LogUtil.createLog();
			URL urlObj = URL.getCurrentRequestUrl();
			pageAccessLog.AccessPage__c = urlObj.getPath();
			pageAccessLog.Detail__c = E_LogUtil.getLogDetailString(ApexPages.Severity.ERROR, e.getMessage(), e.getStackTraceString());
			insert pageAccessLog;
			return pr;
		}

		idChangeFlg = true;
		return pr;
	}

	/**
	 * 入力チェック
	 * @return Boolean true:エラー　false：エラーなし
	 */
	private Boolean validateIsInputError(){
		Boolean isError = false;
		//メールアドレスが空白かチェック
		if(String.isBlank(mailAddress)){
			pageMessages.addErrorMessage(getMSG().get('MAC|001'));
			isError = true;
		}
		//メールアドレス(確認)が空白かチェック
		if(String.isBlank(confirmEmailAddress)){
			pageMessages.addErrorMessage(getMSG().get('CUI|001'));
			isError = true;
		}
		//メールアドレスと確認メールアドレスが同一かチェック
		if(String.isNotBlank(mailAddress) && String.isNotBlank(confirmEmailAddress)) {
			if(mailAddress != confirmEmailAddress){
				pageMessages.addErrorMessage(getMSG().get('MAC|003'));
				isError = true;
			}
		}

		/* スプリントバックログ16_ユーザーID変更 170119
		//現在のメールアドレスと入力された値が同じときエラー
		if(mailAddress == loginUserId || confirmEmailAddress == loginUserId){
			//メールアドレスが変更されていません。
			pageMessages.addErrorMessage(getMSG().get('MAC|006'));
			isError = true;
		}

		//メールアドレスの形式チェック
		if(!E_Util.isMailAddress(mailAddress.toUpperCase()) || !E_Util.isMailAddress(confirmEmailAddress.toUpperCase())) {
			pageMessages.addErrorMessage(getMSG().get('MAC|005'));
			isError = true;
		}
		*/
		//エラーが発生しているとき・現在のメールアドレスと入力された値が同じとき・メールアドレスの形式ではないとき
		//エラーメッセージ「電子メールアドレスが正しくありません。」は入力エラー発生時に必ず表示
		if(isError
			|| (mailAddress == loginUserId || confirmEmailAddress == loginUserId)
			|| (!E_Util.isMailAddress(mailAddress.toUpperCase()) || !E_Util.isMailAddress(confirmEmailAddress.toUpperCase()))){
			//電子メールアドレスが正しくありません。
			pageMessages.addErrorMessage(getMSG().get('MAC|005'));
		}
		/* スプリントバックログ16 */
		return PageMessages.getHasErrorMessages();
	}


	/**
	 * 新規メールアドレス重複チェック
	 * @return Boolean true:重複  false:重複していない
	 */
	private Boolean isOverlapUserName(){
		//メールアドレスの末尾にsuffixを付けてUsername完全一致検索
		User userRec = E_UserDaoWithout.getUserRecByUserName(mailAddress + System.Label.E_USERNAME_SUFFIX);

		//一致したユーザがあった場合、重複エラー
		if(userRec != null){
			return true;
		}
		return false;
	}

	//メールアドレス確認メールの送信
	private void sendConfirmEmail(E_IDRequest__c idReqest){
		//IDリクエストオブジェクトのレコードId
		String recId = 'recId=' + idReqest.Id;

		//メールアドレス変更サイトのURL
		//現在のサイトのドメインを取得
		String baseUrl = Site.getDomain();
		if(String.isEmpty(baseUrl)){
			baseUrl = '';
		}
		System.debug(baseUrl);
		////現在のユーザのサイト名を取得
		String siteName = Site.getName();
		if(String.isEmpty(siteName)){
			siteName = '';
		}


		////////インターネットログインユーザのメールアドレス変更
		//パラメータを暗号化
		String cryptoParam = E_EncryptUtil.getEncryptedString(recId);

		//Site'nn' と ユーザのSiteレコードを取得
		List<Site> userSite = [SELECT GuestUserId, Name, Subdomain, UrlPathPrefix,Description,ID FROM Site WHERE Name = :siteName];
		List<Site> nnSite = [SELECT GuestUserId, Name, Subdomain, UrlPathPrefix FROM Site WHERE Name = :E_Const.NNLINK_NN];

		//>>>>>>>>>userSite がnull のとき空にする(テストクラス用)>>>>>>>>>>>>
		String userSubDom = '';
		if(!userSite.isEmpty()){
			userSubDom = userSite[0].Subdomain;
		}

		//それぞれのSubDomainを格納
		//String userSubDom = userSite[0].Subdomain;
		String nnSubDom = nnSite[0].Subdomain;
		
		//カスタム表示ラベルから、secureサブドメイン値を取得
		String secureDomein = System.Label.I_SecureSubDomain;
		if(secureDomein != '.secure'){
			secureDomein ='';
		}
		//SubDomainを置き換える
//String siteFullUrl = baseUrl.replace(userSubDom, nnSubDom);
		String siteFullUrl = baseUrl.replace(userSubDom, nnSubDom + secureDomein);

		//Domain以外の部分を結合
		String siteNN = 'https://' + siteFullUrl + '/'+ E_Const.NNLINK_NN;
		//遷移先ページと暗号化したパラメータを結合
		String confPage = siteNN +'/IRIS_ChangeUserIDCmpl?Param='+ cryptoParam;


		///////////GWログインユーザのメールアドレス変更
		//パラメータを暗号化
		String cryptoParamGW = E_EncryptUtil.getEncryptedString(recId);
		String confPageGW = E_Util.getURLForGWTB(siteNN) + '/IRIS_ChangeUserIDCmpl?Param='+ cryptoParamGW;

		//メール本文作成
		User userRec = E_UserDaoWithout.getUserRecByUserName(sufUserId);
		String contactName = userRec.Contact.Name;


		String body = '';

		if(access.isCommonGateway()){
			body = String.format(getMSG().get('MAB|001'),new String[]{contactName, confPage, confPageGW});
		}else{
			body = String.format(getMSG().get('MAB|001'),new String[]{contactName, confPage, confPageGW});
		}

		//IRIS_ChangeUserIDCmplにパラメータを付与してメール送信
		//メール送信先、送信元、件名、本文を設定して送信
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[] { mailAddress });      //送信先アドレス
		//送信元アドレス設定
		OrgWideEmailAddress senderAdress = E_OrgWideEmailAddressDao.getOrgWideEmailAddress('IL-JP-sc_reg-change@nnlife.co.jp');
		mail.setOrgWideEmailAddressId(senderAdress.Id);
		mail.setSubject(System.Label.E_MailTitle_ChangeUserID);     //件名
		mail.setHtmlBody(body);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });       //送信

		//メール送信メッセージ表示
		pageMessages.addErrorMessage(getMSG().get('UDE|001'));
	}

	//次回ボタン押下
	//IDリクエストレコードを作成し、E_Homeへ遷移する
	public PageReference updateNextTime() {
		//IDリクエストレコード作成
		E_IDRequest__c eidr = new E_IDRequest__c();
		E_IDCPF__c eidcRec = E_IDCPFDao.getRecsById(UserInfo.getUserId());  //ID管理検索
		eidr.E_IDCPF__c = eidcRec.Id;                   //ID管理
		eidr.ZUPDFLAG__c = E_Const.ZUPDFLAG_E;          //リクエスト種別="E"
		eidr.ZEMAILAD__c = null;                        //メールアドレス=空欄
		eidr.note__c = '変更キャンセル';                   //備考="変更キャンセル"

		//IDリクエストを登録
		insert eidr;
		return Page.E_Home;
	}

	//戻るボタン押下
	public override PageReference doReturn(){
		return Page.IRIS_UserConfig;
	}
}