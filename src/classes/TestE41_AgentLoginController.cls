@isTest
public with sharing class TestE41_AgentLoginController {

	//ブラウザIE時
	public static testMethod void test1() {

		Pagereference testpg = Page.E41_AgentLogin;
		Test.setCurrentPage(testpg);
		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)');
		E41_AgentLoginController alc = new E41_AgentLoginController();
		alc.toErrPage();
		system.debug('@@@ErrPage1= '+ alc.toErrPage());
		system.assertequals(null,alc.toErrPage());
		
	}

	//ブラウザ非IEかつ非サファリ時
	public static testMethod void test2() {

		Pagereference testpg = Page.E41_AgentLogin;
		Test.setCurrentPage(testpg);
		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0');
		E41_AgentLoginController alc = new E41_AgentLoginController();
		alc.toErrPage();
		system.debug('@@@ErrPage2= '+ alc.toErrPage().getUrl());
		system.assertequals('/apex/e41_errieagent',alc.toErrPage().getUrl());
	}


	//サファリ時
	public static testMethod void test3() {

		Pagereference testpg = Page.E41_AgentLogin;
		Test.setCurrentPage(testpg);
//		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25');
		ApexPages.currentPage().getHeaders().put('USER-AGENT','Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_4_11; ja-jp) AppleWebKit/525.13 (KHTML, like Gecko) Version/3.1 Safari/525.13');
		
		E41_AgentLoginController alc = new E41_AgentLoginController();
		alc.toErrPage();
		
		system.debug('@@@ErrPage3= '+ alc.toErrPage().getUrl());
		system.assertequals('/apex/e41_errmcagent',alc.toErrPage().getUrl());
	}

}