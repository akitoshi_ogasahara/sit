/**
 * ユニットプライスご利用方法 コントローラクラス.
 */
public with sharing class E_UPHelp007Controller extends E_AbstractUPInfoController {
	
	private static final String MENU_MASTER_KEY_UP_HELP = 'Help_E_UnitPrice';
	
	/**
	 * コンストラクタ.
	 */
	public E_UpHelp007Controller() {
		super();
	}
	
	/**
	 * #getMenuKey.
	 */
	private String getMenuKey() {
		return MENU_MASTER_KEY_UP_HELP;
	}
}