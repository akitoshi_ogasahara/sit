global with sharing class SC_SelfCompliancesController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public SC_Office__c record {get{return (SC_Office__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public SC_SelfCompliancesExtender getExtender() {return (SC_SelfCompliancesExtender)extender;}
	
	
	{
	setApiVersion(31.0);
	}
	public SC_SelfCompliancesController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1490');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1490',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','{!Extender.menuKey}');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','点検表はこちら');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.UrlforGuide}');
        tmpPropMap.put('p_target','_nnlinkguide');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2190');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2190',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','操作説明はこちら');
        tmpPropMap.put('p_escapeTitle','True');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','{!Extender.UrlforHelp}');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-gray');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2154');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2154',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_Messages','{!Extender.pageMessages}');
        tmpPropMap.put('p_Severity','ERROR');
        tmpPropMap.put('p_Instruction','');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component2152');
		tmpPropMap.put('Component__Name','EPageMessage');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2152',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_office','{!record}');
        tmpPropMap.put('p_isEditableFixed','{!Extender.manager.canChangeFixed}');
        tmpPropMap.put('p_isViewableDefect','{!Extender.manager.canViewDefectStatus}');
        tmpPropMap.put('p_isEditableDefect','{!Extender.manager.canChangeDefectStatus}');
		tmpPropMap.put('Component__Width','359');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','Component2276');
		tmpPropMap.put('Component__Name','SC_ChangeFixed');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2276',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_sbmtFiles','{!Extender.selfCompliances}');
        tmpPropMap.put('p_canUpload','{!Extender.manager.canUpload}');
        tmpPropMap.put('p_canDelete','{!Extender.manager.canDelete}');
        tmpPropMap.put('p_canDownload','{!Extender.manager.canDownload}');
		tmpPropMap.put('Component__Width','400');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','Component2253');
		tmpPropMap.put('Component__Name','SC_SelfCompliancesList');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2253',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','次回点検内容を見る');
        tmpPropMap.put('p_escapeTitle','False');
        tmpPropMap.put('p_rendered','{!Extender.hasRetSCOffice}');
        tmpPropMap.put('p_value','{!Extender.retSCURL}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component2316');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2316',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','前回点検内容を見る');
        tmpPropMap.put('p_escapeTitle','False');
        tmpPropMap.put('p_rendered','{!Extender.hasPrevSCOffice}');
        tmpPropMap.put('p_value','{!Extender.prevSCURL}');
        tmpPropMap.put('p_target','');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','17');
		tmpPropMap.put('Component__id','Component2153');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2153',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_office','{!record}');
        tmpPropMap.put('p_isMR','true');
        tmpPropMap.put('p_isManager','true');
        tmpPropMap.put('p_isCMD','true');
        tmpPropMap.put('p_isEditable','{!Extender.manager.canComment}');
        tmpPropMap.put('p_disclaimer','{!Extender.Disclaimer[\'AMS|SCSC|002\']}');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','60');
		tmpPropMap.put('Component__id','Component2339');
		tmpPropMap.put('Component__Name','SC_SendComment');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component2339',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1489');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1489',tmpPropMap);


		SObjectField f;

		f = SC_Office__c.fields.FiscalYear__c;
		f = SC_Office__c.fields.Status__c;
		f = SC_Office__c.fields.ZAGCYNUM__c;
		f = SC_Office__c.fields.ZEAYNAM__c;
		f = SC_Office__c.fields.OwnerMRUnitName__c;
		f = SC_Office__c.fields.OwnerMRCode__c;
		f = SC_Office__c.fields.OwnerMRName__c;
		f = SC_Office__c.fields.StatusHistory__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = SC_Office__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('SC_Office__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('FiscalYear__c');
			mainQuery.addFieldAsOutput('Status__c');
			mainQuery.addFieldAsOutput('ZAGCYNUM__c');
			mainQuery.addFieldAsOutput('ZEAYNAM__c');
			mainQuery.addFieldAsOutput('OwnerMRUnitName__c');
			mainQuery.addFieldAsOutput('OwnerMRCode__c');
			mainQuery.addFieldAsOutput('OwnerMRName__c');
			mainQuery.addFieldAsOutput('StatusHistory__c');
			mainQuery.addFieldAsOutput('AccZAGCYNUM__c');
			mainQuery.addFieldAsOutput('IsHeadSection__c');
			mainQuery.addFieldAsOutput('SendMailMR__c');
			mainQuery.addFieldAsOutput('Comment__c');
			mainQuery.addFieldAsOutput('SendMailCMD__c');
			mainQuery.addFieldAsOutput('MailSendFlg__c');
			mainQuery.addFieldAsOutput('SendMailManager__c');
			mainQuery.addFieldAsOutput('SCFixed__c');
			mainQuery.addFieldAsOutput('FiscalYearNum__c');
			mainQuery.addFieldAsOutput('Defect__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			
			p_showHeader = false;
			p_sidebar = false;
			extender = new SC_SelfCompliancesExtender(this);
			init();
			
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}