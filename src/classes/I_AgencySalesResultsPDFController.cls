public with sharing class I_AgencySalesResultsPDFController{

	//表示タイプ
	public String viewType {get; private set;}
	//資格情報
	public String qualificationType {get; private set;}
	//メモ欄テキスト
	public String memo {get; private set;}
	//挙積情報
	public E_AgencySalesResults__c sResults {get; private set;}
	//管理者ログインフラグ
	public Boolean isEmployee {get; private set;}
	//挙積情報ID
	public String sResultsId {get; private set;}

	private String elogId;

	public Map<String, String> disclaimerMap {get; private set;}

	public static final String TYPE_DISCLAIMER = 'ディスクレイマー';

	//コンストラクタ
	public I_AgencySalesResultsPDFController() {
		viewType = '0';
		memo = '';
		isEmployee = false;
		qualificationType = '';

		elogId = ApexPages.currentPage().getParameters().get('id');
		getPDFParam();
	}
	//メモ欄用
	public List<String> getMemoList(){
		List<String> memoList = new List<String>();
		List<String> convMemoList = new List<String>();
		memoList = E_Util.splitByLengthCR_Pdf(memo,60);
		if(memoList.size() > 3){
			for(Integer i=0;i<=3;i++){
				convMemoList.add(memoList[i]);
			}
			return convMemoList;
		}
		return memoList;
		//return E_Util.splitByLengthCR_Pdf(memo,60);
	}

	public void getPDFParam(){

		E_log__c log = E_LogDao.getRecByLogId(elogId);
		Map<String, Object> logMap = (Map<String, Object>)JSON.deserializeUntyped( log.Detail__c );
		sResultsId = (String)logMap.get('sResultsId');				  	//代理店挙積情報ID
		qualificationType = (String)logMap.get('qualificationType'); 	//資格情報
		memo = (String)logMap.get('memo');							  	//メモ
		viewType = (String)logMap.get('viewType');					  	//ビュータイプ
		isEmployee = (Boolean)logMap.get('isEmployee');				 	//管理者ログインフラグ

		disclaimerMap = E_MessageMasterDao.getMessageMapByType(TYPE_DISCLAIMER);
	}


//====================================================I_AgencySRInfoController Start====================================================

	private Map<String,Integer> RANK_MAP = new Map<String,Integer>();
	{
		RANK_MAP.put('優績SS',1); 				//優績SS
		RANK_MAP.put('プロテクション優績特則',2); 					//プロテクション優績特則
		RANK_MAP.put('優績S',3);
		RANK_MAP.put('優績A',4);
		RANK_MAP.put('優績B',5);
		RANK_MAP.put('上級',6);
		RANK_MAP.put('普通',7);
	}
	//private Map<String,Integer> RANK_MAP = new Map<String,Integer>();
	//{
	//	RANK_MAP.put('優績SS',0); 				//優績SS
	//	RANK_MAP.put('優績S',1);
	//	RANK_MAP.put('優績A',2);
	//	RANK_MAP.put('優績B',3);
	//	RANK_MAP.put('上級',4);
	//	RANK_MAP.put('普通',5);
	//	RANK_MAP.put('プロテクション優績特則',0); 					//プロテクション優績特則
	//}
	public Integer year {get; private set;}
	public String rankDetail {get; private set;}
	public String dispJudgment {get; private set;}
	public boolean isShowLicese{get; private set;} 	// 資格査定詳細情報表示フラグ


	public E_AgencySalesResults__c getSalesResult(){
		sResults = E_AgencySalesResultsDao.getRecTypeAById(sResultsId);

		//年取得
		year = Integer.valueOf(sResults.YYYYMM__c.left(4));	 

		//ランク判定処理
		Integer bef = RANK_MAP.get(E_Util.emAlphabetToEn(sResults.XHAH_AGCLS__c));
		Integer aft = RANK_MAP.get(E_Util.emAlphabetToEn(sResults.QualifSim_Formula__c));
		if(bef != null && aft != null){
			if(bef > aft){
				rankDetail = 'rankup';
			}
			if(bef < aft){
				rankDetail = 'rankdown';
			}
		}
		if(aft == null){
			dispJudgment = 'Hide';
		}
		//取得した現在資格が該当する場合
		isShowLicese = RANK_MAP.containsKey(E_Util.emAlphabetToEn(sResults.QualifSim_Formula__c));
		return sResults;
	}
//====================================================I_AgencySRInfoController End====================================================

//====================================================I_LicenseAssessmentController Start====================================================
	/* 
	 * 内部クラスで表示用の資格査定情報リストを作成
	 */

	public class LicenseListRow {
		public String seq{get; set;}			// SEQ
		public String banp{get; set;}			// 基準　新契約係数ANP
		public String sanp{get; set;}			// 状況　新契約係数ANP
		public String banps{get; set;}			// 基準　新契約係数ANP 特定保険種類
		public String sanps{get; set;}			// 状況　新契約係数ANP 特定保険種類
		public String bianp{get; set;}			// 基準　保有ANP
		public String sianp{get; set;}			// 状況　保有ANP
		public String bam{get; set;}			// 基準　稼動月数
		public String sam{get; set;}			// 状況　稼動月数
		public String biqa{get; set;}		   	// 基準　IQA継続率
		public String siqa{get; set;}		   	// 状況　IQA継続率

		public boolean isSanpFlag{get; set;}	// 新契約係数ANP未達フラグ
		public boolean isSanpsFlag{get; set;}	// 新契約係数ANP 特定保険種類未達フラグ
		public boolean isSianpFlag{get; set;}	// 保有ANP未達フラグ
		public boolean isSamFlag{get; set;}		// 稼動月数未達フラグ
		public boolean isSiqaFlag{get; set;}	// IQA継続率未達フラグ

		//コンストラクタ
		public LicenseListRow() {
			seq = '';
			banp = '';
			sanp = '';
			banps = '';
			sanps = '';
			bianp = '';
			sianp = '';
			bam = '';
			sam = '';
			biqa = '';
			siqa = '';
			isSanpFlag = false;
			isSanpsFlag = false;
			isSianpFlag = false;
			isSamFlag = false;
			isSiqaFlag = false;
		}
	}

	public List<LicenseListRow> getLicenseAssessment(){

		//資格査定詳細情報リスト取得
		List<E_LicenseAssessment__c> licenseAssessmentList = E_LicenseAssessmentDao.getRecsByQualification(sResultsId,qualificationType);
		
		List<LicenseListRow> rowList = new List<LicenseListRow>();

		for(E_LicenseAssessment__c licenseAssessment :licenseAssessmentList){
			List<String> temps = new List<String>();
			LicenseListRow row = new LicenseListRow();
			boolean isNext = false;

			row.seq = I_Util.toStr(licenseAssessment.SEQ__c);

			// 新契約係数ANP
			temps = getNumberText(0, licenseAssessment.Standard_NBCANP__c, licenseAssessment.State_NBCANP__c);
			row.banp = temps[0];
			row.sanp = temps[1];
			if(temps[1].contains('あと')) row.isSanpFlag = true;
			if(temps[0] != '') isNext = true;

			// 新契約係数ANP 特定保険種類
			temps = getNumberText(0, licenseAssessment.Standard_NBCANP_SpInsType__c, licenseAssessment.State_NBCANP_SpInsType__c);
			row.banps = ((isNext == true && temps[0] != '')?'かつ':'') + temps[0];
			row.sanps = temps[1];
			if(temps[1].contains('あと')) row.isSanpsFlag = true;
			if(temps[0] != '') isNext = true;

			// 保有ANP
			temps = getNumberText(0, licenseAssessment.Standard_IANP__c, licenseAssessment.State_IANP__c);
			row.bianp = ((isNext == true && temps[0] != '')?'かつ':'') + temps[0];
			row.sianp = temps[1];
			if(temps[1].contains('あと')) row.isSianpFlag = true;
			if(temps[0] != '') isNext = true;

			// 稼動月数
			temps = getNumberText(2, licenseAssessment.Standard_ActiveMonth__c, licenseAssessment.State_ActiveMonth__c);
			row.bam = ((isNext == true && temps[0] != '')?'かつ':'') +  temps[0];
			row.sam = temps[1];
			if(temps[1].contains('あと')) row.isSamFlag = true;
			if(temps[0] != '') isNext = true;

			// IQA継続率
			temps = getNumberText(3, licenseAssessment.Standard_IQA__c, licenseAssessment.State_IQA__c);
			row.biqa = ((isNext == true && temps[0] != '')?'かつ':'') +  temps[0];
			row.siqa = temps[1];
			if(temps[1].contains('あと')) row.isSiqaFlag = true;

			rowList.add(row);
		}
		return rowList;
	}

	/**
	 * 基準/状況 文字列へ変換
	 */
	public List<String> getNumberText(Integer type, Decimal standard, Decimal state){
		List<String> temps  = new List<String>();
		String temp		 = '';
		Decimal num		 = 0;
		Integer point	   = 0;
		standard	= (standard == null) ? 0 : standard;
		state	   = (state == null) ? 0 : state;

		if(standard == 0){
			temps.add('');  // 基準
			temps.add('');  // 状況
		}else{
			num = state;
			if(type==0){
				// 円表記
				// 基準変換
				temp = standard.format().replace(',', '');
				if(temp.lastIndexOf('000000000000',temp.length()) != -1){   // 兆
					temp = temp.Left(temp.length() - 12);
					temp = Long.valueOf(temp).format() + '兆';
				}else if(temp.lastIndexOf('00000000',temp.length()) != -1){ // 億
					temp = temp.Left(temp.length() - 8);
					temp = Long.valueOf(temp).format() + '億';
				}else if(temp.lastIndexOf('0000',temp.length()) != -1){	 // 万
					temp = temp.Left(temp.length() - 4);
					temp = Long.valueOf(temp).format() + '万';
				}
				temps.add(temp + '円以上');

				// 状況変換
				temp = (num >= 0) ? '達成' : 'あと' + Math.abs(num).format() + '円';
				temps.add(temp);

			}else if(type==2){
				// 月表記
				// 基準変換
				temps.add(String.valueOf(standard) + 'ヵ月以上');

				// 状況変換
				temp = (num >= 0) ? '達成' : 'あと' + Math.abs(num).format() + 'ヵ月';
				temps.add(temp);

			}else if(type==3){
				// %表記
				// 基準変換
				temp	= round(standard, 1).replace('+ ', '');
				temps.add(temp + '%以上');

				// 状況変換
				temp	= (num >= 0) ? '達成' : round(num, 1) + '%';
				temps.add(temp);
			}
		}
		return temps;
	}
	/**
	 * 小数点変換
	 */
	public String round(Decimal num, Integer dec){
		String temp	 = (num == null) ? '0' : Math.abs(num).setScale(dec, RoundingMode.HALF_UP).format().replace(',', '');
		Integer point   = temp.indexOf('.', 1);
		if(dec <= 0){
			temp	= (point == -1) ? temp: temp.Left(point - 1);
		}else{
			temp	= (point == -1) ? temp + '.' + '0'.repeat(dec) : (temp + '0'.repeat(dec)).Left(point + 1 + dec);
		}
		if(num > 0) temp = '+ ' + temp;
		if(num < 0) temp = '- ' + temp;
		return temp;
	}
//====================================================I_LicenseAssessmentController End====================================================

}