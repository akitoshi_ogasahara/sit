global class E_LoggingController {

	/**
	 *		saveLog
	 *			ログレコード保存処理。
	 *			NNLink系ページにおいて、Formタグ不要で保存するため、RemoteActionで保存する
	 */
	@RemoteAction
	public static String saveLog(String logJSON) {
		E_Log__c log;
		try{
			log = (E_Log__c)JSON.deserialize(logJSON, SObject.class);
			if(log.AccessDatetime__c==null){
				log.AccessDatetime__c = Datetime.now();
			}
			log.AppName__c = E_AccessController.getInstance().idcpf.AppMode__c;
			insert log;
		}catch(Exception e){
			return e.getMessage() + '@@@' + e.getStackTraceString();
		}

		return String.ValueOf(log.id);
	}
	
	/*	ページ表示時のアクセスログ　JSON	*/
	public String getPageAccessLogJSON(){
		return JSON.serialize(E_LogUtil.createLog());
	}
	
	/*	ログ取得対象かどうか	*/
	public Boolean getRequiredLogging(){
		return E_LogUtil.requiredLogging();
	}
}