global with sharing class MNT_DocumentSearchExtender extends SkyEditor2.Extender{
	MNT_DocumentSearch extension;
	SkyEditor2.Query query;

	public MNT_DocumentSearchExtender(MNT_DocumentSearch extension){
		this.extension = extension;
		this.query = extension.queryMap.get('ResultList');
	}

	public override void init(){
		extension.doSearch();
	}

	public override void preSearch() {
		query.initClause();

		String whereStr = 'DocumentCategory__c IN (\'パンフレット／特に重要なお知らせ\',\'ご契約のしおり・約款\',\'会社案内\')';

		if(!String.isBlank(whereStr)){
			query.addWhere(whereStr);
		}

		query.addSort('DocumentCategory__c', true, false);
		query.addSort('DisplayOrder__c', true, false);
	}
}