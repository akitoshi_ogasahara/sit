@isTest
private with sharing class TestE_InsuredInfoController{
	private static testMethod void testPageMethods() {		E_InsuredInfoController extension = new E_InsuredInfoController(new ApexPages.StandardController(new Contact()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testInsuredList() {
		E_InsuredInfoController.InsuredList InsuredList = new E_InsuredInfoController.InsuredList(new List<E_Policy__c>(), new List<E_InsuredInfoController.InsuredListItem>(), new List<E_Policy__c>(), null);
		InsuredList.create(new E_Policy__c());
		System.assert(true);
	}
	
}