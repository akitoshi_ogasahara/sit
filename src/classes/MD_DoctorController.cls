public with sharing class MD_DoctorController extends MD_AbstractController {
	// 検索結果最大件数
	private static Integer MD_LIMIT_SEARCH_RESULT = 500;

	// ページメッセージ
	public E_PageMessagesHolder pageMessages {get;set;}
	// 検索済フラグ
	public Boolean isSearched {get; set;}
	// 一覧クラス
	public List<MD_DoctorDTO> doctorDtoList {get; set;}
	// 検索文字列
	public String searchText {get; set;}
	// 検索種別
	public String searchType {get; set;}

	// 検索種別ラジオ
	public List<SelectOption> getSearchTypeItems() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(MD_Const.DOCTOR_COL_API_SEARCHHOSPITALNAME, MD_Const.MD_LABEL_CONDITION_HOSPTALNAME));
		options.add(new SelectOption(MD_Const.DOCTOR_COL_API_SEARCHNAME, MD_Const.MD_LABEL_CONDITION_NAME));
		return options;
	}

	/* Constructor */
	public MD_DoctorController() {
		// 初期値設定
		isSearched = false;
		searchType = MD_Const.DOCTOR_COL_API_SEARCHHOSPITALNAME;
		doctorDtoList = new List<MD_DoctorDTO>();
		
		// ページメッセージの初期化
		pageMessages = getPageMessages();
	}

	/**
	 * init
	 */
	public PageReference init() {
		// IRIS Day2 ページアクセスログは E_Logging コンポーネントで取得するように変更
		return null;
		/*
		try {
			// アクセスログ登録
			return insertAccessLog();

		} catch (Exception e) {
			// アクセスログ登録、 エラーページへ
			return insertErrorLog(e.getMessage());
		}
		*/
	}

	/* Search */
	public void doSearch() {
		try {
			// ページメッセージの初期化
			pageMessages.clearMessages();
			
			// 検索条件がブランクの場合エラー
			if(String.isBlank(searchText)){
				pageMessages.addErrorMessage(E_Message.getMsgMap().get('MDDS|001'));
				return;
			}
			
			// 表示用のリスト作成
			doctorDtoList = new List<MD_DoctorDTO>();
			Integer cnt = 1;
			for (MD_Doctor__c doctor : MD_DoctorDao.getRecsByWhere(getQueryCondition(), getQuerySort(), MD_LIMIT_SEARCH_RESULT)) {
				MD_DoctorDTO dto = new MD_DoctorDTO(cnt, doctor);
				doctorDtoList.add(dto);
				cnt++;
			}

			isSearched = true;

		} catch (Exception e) {
			pageMessages.addErrorMessage(e.getMessage());
		}
	}

	/* SOQLのWhere句作成 */
	private String getQueryCondition() {
		String strWhere = '';
		
		// 検索文字列の半角変換
		String searchTextEn = E_Util.emToEn(searchText);
		
		// 病院名（空白のデータは検索対象外）
		strWhere += MD_Const.DOCTOR_COL_API_HOSPITALNAME + ' <> null ';
		
		if(String.isNotBlank(searchTextEn)){
			strWhere += ' And ';
			strWhere += searchType + ' Like \'%' + searchTextEn + '%\'';
		}
		
		return strWhere;
	}

	/*  */
	/* SOQLのWhere句作成 */
	private String getQuerySort() {
		return MD_Const.DOCTOR_COL_API_STATECODE + ' ASC ';
	}
}