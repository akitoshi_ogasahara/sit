public with sharing class CC_SRFormPrintController {
// publicスコープ変数
	public List<BulkPrint> bulkTab {get;set;} // 一括タブ用
	public List<UnitPrint> unitTab {get;set;} // 個別タブ用
	public List<Case> allCases {get;set;} // ケース一覧(10件)
	public String printStr {get;set;} // 印刷用ID文字列
	public Boolean isConfirmationFormOutput {get;set;} // 確認書出力フラグ
	public String selectedRadio {get;set;}
	public String validateResult {get;set;}

	public String maxDisp {
		get {
			return MAX_AMOUNT_DISP;
		}
	}
	public string policyClassUnit {get; private set;}
	public string policyClassBulk {get; private set;}
	public string policyClassCancel {get; private set;}
	public string policyClassReduction {get; private set;}
	public string policyClassContractorLoan {get; private set;}

	public class NoSrIdException extends Exception {}
	public class NoLetterException extends Exception {}
	public class IsSPVAException extends Exception {}

// privateスコープ変数
	private Id srId;
	private Map<Id, Map<String, String>> covpfNames; // 保険種別名一覧保持
	private List<CC_SRPolicy__c> srPolicies; // SR契約連結
	private Map<Id, Set<CC_FormMaster__c>> formIdsMaps; // 帳票IDの一覧(これを基に帳票を発行する)
	private Integer pages; // ページ
	private Case cs; // SR(ケース)
	private Map<String, Map<String, String>> srFormTypes; //
	private Integer policiesCount;

	// 定数
	private static final string PARAM_SRID = 'srid';
	private static final string PARAM_TABNAME = 'tabValue';
	private static final string PARAM_OPTIONNAME = 'optionValue';

	// 画面表示種別
	private static final string POLICY_CLASS_UNIT = '個別';
	private static final string POLICY_CLASS_BULK = '一括';
	private static final string POLICY_CLASS_CANCEL = '解約';
	private static final string POLICY_CLASS_REDUCTION = '減額';
	private static final string POLICY_CLASS_CONTRACTOR_LOAN = '契約者貸付';

	private static final string MAX_AMOUNT_DISP = '最高限度額';

	// 対象の帳票No
	private static final string SR_TYPE_CANCEL = '0310005';
	private static final string SR_TYPE_REDUCTION = '0310002,0310003';
	private static final string SR_TYPE_CONTRACTOR_LOAN = '0310004';

	// 対象の確認書帳票
	private static final string FORM_NO_CONFIRM_CANCEL = 'COLI-POS 122-03';
	private static final string FORM_NO_CONFIRM_CANCEL_BULK = 'COLI-POS 121';
	private static final string FORM_NO_CONFIRM_REDUCTION = 'COLI-POS 322-03';
	private static final string FORM_NO_CONFIRM_REDUCTION_BULK = 'COLI-POS 321';
	private static final string FORM_NO_CONFIRM_CONTRACTOR_LOAN = 'COLI-POS 222-03';
	private static final string FORM_NO_CONFIRM_CONTRACTOR_LOAN_BULK = 'COLI-POS 222-03';

	//画面オプション値
	private static final string RADIO_CANCEL = 'radio-11';
	private static final string RADIO_REDUCTION = 'radio-12';
	private static final string RADIO_CONTRACTOR_LOAN = 'radio-13';

	//
	private static final String FORM_NAME_SENDLETTER = '送付状';
	private static final String FORM_NAME_INVOICE = '請求書';
	private static final String FORM_NAME_CONFIRM = '確認書';
	private static final Integer PAGE_MAXROW_BULK = 10;
	private static final Integer PAGE_MAX_CONFIRMBULK= 5;
	private static final String VALIDATE_STR_SPVA = 'SPVAヘッダ';
	private static final String STR_SPVA = 'CONTAIN_SPVA';
	private static final String STR_NO_SRID = 'NO_SRID';
	private static final String STR_NO_LETTER = 'NO_LETTER';
	private final static String KEY_SRTYPE = 'SRTYPE';
	private final static String KEY_RADIO = 'RADIO';
	private final static String KEY_CONFIRMFORM = 'CONFIRMFORMNO';
	private final static String KEY_CONFIRMFORMBULK = 'CONFIRMFORMNOBULK';

	/**
	 * 個別タブ用インナークラス
	 */
	public class UnitPrint {
		public Integer row {get;set;}
		public String securityNum {get;set;} // 保険証券番号
		public String personamName {get;set;} // 被保険者名
		public String ridersValue {get;set;} // 選択された特約
		public Map<String, String> covpfsmap {get;set;} // 特約Map
		public String discountAfter {get;set;} // 減額された金額
		public String amount {get;set;} // 請求金額
		public Boolean maxAmountDisp {get;set;}

		// 特約
		public void setRiders(String s) {
			this.ridersValue = s;
		}
		public List<SelectOption> getRiders() {
			List<SelectOption> riders = new List<SelectOption>();
			Boolean firstFlg = true;
			riders.add(new SelectOption(' ', ''));
			for (String sKey : covpfsmap.keySet()) {
				riders.add(new SelectOption(sKey, sKey));
				if (firstFlg) {
					this.setRiders(sKey);
					firstFlg = false;
				}
			}

			return riders;
		}
		// インナークラスコンストラクタ
		public UnitPrint(Integer row,
							String secuParam,
							String personParam,
							Map<String, String> covpfnamesmap,
							Decimal rowCnt
						) {
			this.row = row; // 行
			this.securityNum = secuParam; // 証券番号
			this.personamName = personParam; // 契約者名
			this.covpfsmap = covpfnamesmap; // 特約
		}
	}

	/**
	 * 一括タブ用インナークラス
	 */
	public class BulkPrint {
		public Integer row {get;set;}
		// 証券番号 From-To
		public String startNum {get;set;}
		public String endNum {get;set;}
		public Boolean maxAmountDisp {get;set;}

		// コンストラクタ
		public BulkPrint(Integer row, String startParam, String endParam) {
			this.row = row;
			this.startNum = startParam;
			this.endNum = endParam;
		}
	}

	/**
	 * [画面.ラジオボタン用] 活動種別
	 */
	public List<SelectOption> getActClasses() {
		List<SelectOption> classes = new List<SelectOption>();

		classes = getClassesList();

		return classes;
	}

	/**
	 * コンストラクタ
	 * 画面からのパラメータ :srid(Case.Id)
	 * Case.IDに紐づくSR契約連結から、保険契約ヘッダ１～４を取得
	 * 保険契約ヘッダの従オブジェクト個人保険特約の保険種類(結合)を取得
	 *
	 */
	public CC_SRFormPrintController(ApexPages.StandardController ctrl) {
		this.srid = ApexPages.currentPage().getParameters().get(PARAM_SRID);
		this.formIdsMaps = new Map<Id, Set<CC_FormMaster__c>>();
		this.isConfirmationFormOutput = false;
		this.pages = 0;

		try {
			this.policyClassUnit = POLICY_CLASS_UNIT;
			this.policyClassBulk = POLICY_CLASS_BULK;
			this.policyClassCancel = POLICY_CLASS_CANCEL;
			this.policyClassReduction = POLICY_CLASS_REDUCTION;
			this.policyClassContractorLoan = POLICY_CLASS_CONTRACTOR_LOAN;

			// 一括印刷用リスト生成
			this.bulkTab = this.getBulkPrintList();

			if (this.srid == null) {

				throw new NoSrIdException(STR_NO_SRID);
			}

			this.cs = CC_SRFormPrintDao.getCaseById(this.srid);
			this.isConfirmationFormOutput = this.cs.CC_IsConfirmationFormOutput__c;
			if (!validateForSave(this.cs)) {
				throw new NoLetterException(STR_NO_LETTER);
			}

			// 保険契約ヘッダ情報取得
			this.srPolicies = CC_SRFormPrintDao.getCCSRPolicyByCaseId(srid);
			if (this.srPolicies.size() > 0) {

				if (!this.isExistSpvaPolicy(this.srPolicies)) {

					throw new IsSPVAException(STR_SPVA);
				}

				// 帳票マスタ参照IdのMapを取得
				this.formIdsMaps = this.getFormMastersRecs(this.srPolicies);

				// 保険契約ヘッダIDのリストを取得
				List<Id> policyIds = this.getPolicyIdList(this.srPolicies);

				// 個人保険特約情報取得
				List<E_COVPF__c> covpfs = this.getECovpfList(policyIds);

				// 個別タブデータ生成
				if (this.isConfirmationFormOutput) {
					this.unitTab = this.getUnitPrintList(this.srPolicies);
				}

			}

			// 確認書出力フラグ設定

			this.srFormTypes = this.getSrFormType();

			this.selectedRadio = this.getSelectedConfirmRadio(this.cs.CC_SRTypeId__r.Name, this.srFormTypes);

		} catch (NoSrIdException e) {

			this.validateResult = e.getMessage();

		} catch (NoLetterException e) {

			this.validateResult = e.getMessage();

		} catch (IsSPVAException e) {

			this.validateResult = e.getMessage();

		} catch(Exception e) {

			this.validateResult = 'ERROR';
		}

	}

	/**
	 * ページ追加アクション
	 *
	 */
	public PageReference addBulkPage() {
		this.bulkTab.addAll(this.getBulkPrintList());

		return null;
	}

	/**
	 * ページ削除アクション
	 *
	 */
	public PageReference delBulkPage() {
		this.bulkTab = this.getDelBulkPrintList();

		return null;
	}

	/**
	 * 帳票レコード保存アクション
	 *
	 */
	public PageReference save() {
		PageReference ret = null;
		Id insId; // 追加レコードId
		String buttonName; // ボタン名
		Map<Id, String> printIdsButtons = new Map<Id, String>(); // 追加レコードIDとSVFボタン名
		List<String> formTypeConditionList = new List<String>();

		try {
			// フォームマスタ情報取得
			List<CC_FormMaster__c> forms = CC_SRFormPrintDao.getFormMasters();
			// マップに変換
			Map<Id, CC_FormMaster__c> formMaps = new Map<Id, CC_FormMaster__c>();
			for (CC_FormMaster__c form : forms) {
				formMaps.put(form.Id, form);
			}

			// 追加レコードリスト変数初期化
			List<CC_FormDetailPage__c> insRecPages = new List<CC_FormDetailPage__c>();
			List<CC_FormDetail__c> insRecs = new List<CC_FormDetail__c>();
			// 追加レコードIDとSVFボタン名
			List<String> buttonNames = new List<String>();

			// 確認書発行
			String tab = ApexPages.currentPage().getParameters().get(PARAM_TABNAME);
			String option = ApexPages.currentPage().getParameters().get(PARAM_OPTIONNAME);
			// SR契約連結1件目取得
			CC_SRPolicy__c pol = this.srPolicies[0];

			// SRからの帳票(送付状)
			if (this.cs.CC_IsCoverLetterOutput__c) {
				// 帳票データ生成処理
				buttonName = getSVFButtonName(formMaps, this.cs.CC_CoverLetter__c);
				insRecs.add(insCCFormDetail(pol.Id, this.cs.CC_CoverLetter__c, this.srId));
				buttonNames.add(buttonName+':'+formMaps.get(this.cs.CC_CoverLetter__c).CC_FormName__c);
				formTypeConditionList.add(FORM_NAME_SENDLETTER);	// 送付状
			}

			// SR契約連結からの帳票
			if (this.formIdsMaps.size() > 0 && this.cs.CC_IsInvoiceOutput__c == true) {
				for (Id srPolicyId : this.formIdsMaps.keySet()) {
					// 帳票発行処理
					for (CC_FormMaster__c rec : this.formIdsMaps.get(srPolicyId)) {
						// 帳票データ生成処理
						buttonName = getSVFButtonName(formMaps, rec.Id);
						insRecs.add(insCCFormDetail(srPolicyId, rec.Id, this.srId));
						buttonNames.add(buttonName+':'+formMaps.get(rec.Id).CC_FormName__c);
					}
				}
				formTypeConditionList.add(FORM_NAME_INVOICE);	// 請求書
			}

			// 確認書出力フラグ判断
			if (this.isConfirmationFormOutput) {
				// 確認書帳票Id取得
				String formMasterId = this.getConfirmFormMasterId(forms, tab,option);
				// SVFボタン名取得
				buttonName = getSVFButtonName(formMaps, formMasterId);
				insRecs.add(insCCFormDetailConfirm(tab, option, pol.Id, formMasterId, this.srId));
				buttonNames.add(buttonName+':'+formMaps.get(formMasterId).CC_FormName__c);
				formTypeConditionList.add(FORM_NAME_CONFIRM);	// 確認書
			}

			// SRに紐づく出力帳票種別を抽出
			List<CC_FormDetail__c> delRecs = CC_SRFormPrintDao.getFormDetailBySRFormtypeForDel(this.srId, formTypeConditionList);

			// セーブポイント保持・エラーハンドリング
			Savepoint sp = database.setSavepoint();
			try {
				// 出力帳票過去分削除
				delete delRecs;

				// コールセンター帳票情報レコード一括追加
				insert insRecs;

				// 確認書出力フラグ判断
				if (this.isConfirmationFormOutput) {
					CC_FormDetail__c confirmReport = insRecs[insRecs.size()-1];
					Id confirmFormParentId = confirmReport.Id;
					insRecPages = insCCFormDetailConfirmPage(tab, option, confirmFormParentId);

					insert insRecPages;
					confirmReport.CC_NumberOfPolicies__c = this.policiesCount;
					update confirmReport;
				}

				// 親子階層紐づけ作成
				Boolean firstRec = true;
				Id firstRecId = null;
				for (CC_FormDetail__c rec : insRecs) {
					if (firstRec) {
						firstRecId = rec.Id;
						firstRec = false;
						continue;
					}
					rec.CC_ParentSRFormDetail__c = firstRecId;
				}
				update insRecs;


			} catch (DmlException e) {
				// Rollback
				database.rollback(sp);
				throw e;
			} catch (Exception e) {
				// Rollback
				database.rollback(sp);
				throw e;
			}

			// 追加帳票レコードIdとボタンと紐づけ
			for (Integer i=0; i< insRecs.size(); i++) {
				printIdsButtons.put(insRecs[i].Id, buttonNames[i]);
			}

			// 画面へ返すパラメータ生成
			List<String> retStrList = new List<String>();
			for (Id recId : printIdsButtons.keySet()) {
				retStrList.add((string)recId+':'+printIdsButtons.get(recId));
			}
			this.printStr = String.join(retStrList, ',');

		} catch (Exception e) {
			// system.debug(e.getMessage());
		}

		return ret;
	}

//-- privateメソッド ----------------------------------------------------------
	/**
	 * 保存時チェック
	 */
	private Boolean validateForSave(Case cs) {
		Boolean ret = true;

		if (cs.CC_IsCoverLetterOutput__c && cs.CC_CoverLetter__c == null) {

			ret = false;
		}

		return ret;
	}

	/**
	 * SPVAレコードタイプの契約が存在していないかチェック
	 */
	private Boolean isExistSpvaPolicy(List<CC_SRPolicy__c> srPolicies) {
		Boolean ret = true;

		for (CC_SRPolicy__c srp : srPolicies) {
			if (srp.CC_PolicyID1__c != null) {
				if (srp.CC_PolicyID1__r.RecordType.Name == VALIDATE_STR_SPVA) {
					ret = false;
					break;
				}
			}
			if (srp.CC_PolicyID2__c != null) {
				if (srp.CC_PolicyID2__r.RecordType.Name == VALIDATE_STR_SPVA) {
					ret = false;
					break;
				}
			}
			if (srp.CC_PolicyID3__c != null) {
				if (srp.CC_PolicyID3__r.RecordType.Name == VALIDATE_STR_SPVA) {
					ret = false;
					break;
				}
			}
			if (srp.CC_PolicyID4__c != null) {
				if (srp.CC_PolicyID4__r.RecordType.Name == VALIDATE_STR_SPVA) {
					ret = false;
					break;
				}
			}
		}

		return ret;
	}

	/**
	 *
	 */
	private Map<String, String> getMapSr(String srType, String radio, String formNoConfirm, String formNoBulkConrim) {
		Map<String, String> ret = new Map<String, String>();

		ret.put(KEY_SRTYPE, srType);
		ret.put(KEY_RADIO, radio);
		ret.put(KEY_CONFIRMFORM, formNoConfirm);
		ret.put(KEY_CONFIRMFORMBULK, formNoBulkConrim);

		return ret;
	}

	/**
	 *
	 */
	private Map<String, Map<String, String>> getSrFormType() {
		Map<String, Map<String, String>> checkSrTypes = new Map<String, Map<String, String>>();

		checkSrTypes.put(
						POLICY_CLASS_CANCEL,
						this.getMapSr(
									SR_TYPE_CANCEL,
									RADIO_CANCEL,
									FORM_NO_CONFIRM_CANCEL,
									FORM_NO_CONFIRM_CANCEL_BULK
									)
						);
		checkSrTypes.put(
						POLICY_CLASS_REDUCTION,
						this.getMapSr(
									SR_TYPE_REDUCTION,
									RADIO_REDUCTION,
									FORM_NO_CONFIRM_REDUCTION,
									FORM_NO_CONFIRM_REDUCTION_BULK
									)
						);
		checkSrTypes.put(
						POLICY_CLASS_CONTRACTOR_LOAN,
						this.getMapSr(
									SR_TYPE_CONTRACTOR_LOAN,
									RADIO_CONTRACTOR_LOAN,
									FORM_NO_CONFIRM_CONTRACTOR_LOAN,
									FORM_NO_CONFIRM_CONTRACTOR_LOAN_BULK
									)
						);

		return checkSrTypes;
	}

	/**
	 *
	 */
	private String getSelectedConfirmRadio(String srType, Map<String, Map<String, String>> checkSrTypes) {
		String ret = '';

		for (String key : checkSrTypes.keySet()) {
			for (String checkType : checkSrTypes.get(key).get(KEY_SRTYPE).split(',')) {
				if (checkType == srType) {
					ret = checkSrTypes.get(key).get(KEY_RADIO);
				}
			}
		}

		return ret;
	}

	/**
	 *
	 */
	private String getSVFButtonName(Map<Id, CC_FormMaster__c> formMaps, Id formId) {
		// 帳票マスタのSVFボタン名を返す
		return formMaps.get(formId).CC_SvfFormButtonName__c;
	}

	/**
	 * 確認書フォームId取得
	 *
	 */
	private Id getConfirmFormMasterId(List<CC_FormMaster__c> forms, String tab, String option) {
		Id ret;
		String formNo;
		Map<String, String> selMap;

		selMap = this.srFormTypes.get(option); // 請求書毎のマップ
		// 一括の場合は一括用確認書帳票番号、個別の場合は個別用確認書帳票番号
		if (tab == POLICY_CLASS_BULK) {
			formNo = selMap.get(KEY_CONFIRMFORMBULK);
		} else {
			formNo = selMap.get(KEY_CONFIRMFORM);
		}

		// 帳票番号からIdをひく
		for (CC_FormMaster__c rec : forms) {
			if (rec.CC_FormNo__c == formNo) {
				ret = rec.Id;
				break;
			}
		}

		return ret;
	}

	/**
	 * 一括用画面ページ追加
	 *
	 */
	private List<BulkPrint> getBulkPrintList() {
		List<BulkPrint> ret = new List<BulkPrint>();

		if (this.pages >= PAGE_MAX_CONFIRMBULK) {
			return ret;
		}

		for (Integer i = 0;i< PAGE_MAXROW_BULK ;i++) {
			ret.add(new BulkPrint((this.pages*PAGE_MAXROW_BULK)+i+1, '', ''));
		}

		this.pages++;
		return ret;
	}
	/**
	 * 一括用画面ページ削除
	 *
	 */
	private List<BulkPrint> getDelBulkPrintList() {
		List<BulkPrint> ret = this.bulkTab;

		if (this.pages == 1) {

			return ret;
		}

		for (Integer i = this.pages*PAGE_MAXROW_BULK; i > (this.pages*PAGE_MAXROW_BULK)-PAGE_MAXROW_BULK; i--) {
			ret.remove(i-1);
		}
		this.pages--;

		return ret;
	}

	/**
	 * 個別用画面情報生成
	 *
	 */
	private List<UnitPrint> getUnitPrintList(List<CC_SRPolicy__c> recs) {
		List<UnitPrint> ret = new List<UnitPrint>();
		Map<String, String> covpf;
		Set<Id> policyId = new Set<Id>();

		for (CC_SRPolicy__c rec : recs) {

			if (rec.CC_PolicyID1__c != null && !policyId.contains(rec.CC_PolicyID1__c)) {
				covpf = this.getCovpfMap(rec.CC_PolicyID1__c, this.covpfNames); // 特約取得
				ret.add(
					new UnitPrint(getRowNum(ret), rec.CC_PolicyID1__r.COMM_CHDRNUM__c, rec.CC_PolicyID1__r.InsuredName__c, covpf, rec.CC_SYSNumberOfPolicies__c)
					);
				policyId.add(rec.CC_PolicyID1__c);
			}
			if (rec.CC_PolicyID2__c != null && !policyId.contains(rec.CC_PolicyID2__c)) {
				covpf = this.getCovpfMap(rec.CC_PolicyID2__c, this.covpfNames); // 特約取得
				ret.add(
					new UnitPrint(getRowNum(ret), rec.CC_PolicyID2__r.COMM_CHDRNUM__c, rec.CC_PolicyID2__r.InsuredName__c, covpf, rec.CC_SYSNumberOfPolicies__c)
					);
				policyId.add(rec.CC_PolicyID2__c);
			}
			if (rec.CC_PolicyID3__c != null && !policyId.contains(rec.CC_PolicyID3__c)) {
				covpf = this.getCovpfMap(rec.CC_PolicyID3__c, this.covpfNames); // 特約取得
				ret.add(
					new UnitPrint(getRowNum(ret), rec.CC_PolicyID3__r.COMM_CHDRNUM__c, rec.CC_PolicyID3__r.InsuredName__c, covpf, rec.CC_SYSNumberOfPolicies__c)
					);
				policyId.add(rec.CC_PolicyID3__c);
			}
			if (rec.CC_PolicyID4__c != null && !policyId.contains(rec.CC_PolicyID4__c)) {
				covpf = this.getCovpfMap(rec.CC_PolicyID4__c, this.covpfNames); // 特約取得
				ret.add(
					new UnitPrint(getRowNum(ret), rec.CC_PolicyID4__r.COMM_CHDRNUM__c, rec.CC_PolicyID4__r.InsuredName__c, covpf, rec.CC_SYSNumberOfPolicies__c)
					);
				policyId.add(rec.CC_PolicyID4__c);
			}

		}
		return ret;
	}

	/**
	 * 行番号取得
	 */
	private Integer getRowNum(List<Object> objList) {
		return objList.size() + 1;
	}

	/**
	 * 特約のMap用
	 */
	private Map<String, String> getCovpfMap(Id policyId, Map<Id, Map<String, String>> covpfMaps) {
		Map<String, String> ret = new Map<String, String>();

		if (covpfMaps.containsKey(policyId)) {
			ret = covpfMaps.get(policyId);
		}

		return ret;
	}

	/**
	 * 個人保険特約情報取得・加工
	 *
	 */
	private List<E_COVPF__c> getECovpfList(List<Id> policyIds) {
		List<E_COVPF__c> covpfs = CC_SRFormPrintDao.getECovpfByPolicyIds(policyIds);
		this.covpfNames = new Map<Id, Map<String, String>>();
		Map<String, String> tmpnames = new Map<String, String>();
		if (covpfs.size() > 0) {
			Id tmpid = covpfs[0].E_Policy__c;
			for (E_COVPF__c covpf : covpfs) {
				if (tmpid <> covpf.E_Policy__c) {
					this.covpfNames.put(tmpid, tmpnames);
					tmpnames = new Map<String, String>();
					tmpid = covpf.E_Policy__c;
				}
				tmpnames.put(covpf.COLI_ZCOVRNAMES__c, covpf.Id);
			}
			this.covpfNames.put(tmpid, tmpnames);
		}

		return covpfs;
	}

	/**
	 * 保険契約Id抽出
	 *
	 */
	private List<Id> getPolicyIdList(List<CC_SRPolicy__c> recs) {
		List<Id> policyIds = new List<Id>();

		for (CC_SRPolicy__c rec : recs) {
			if (rec.CC_PolicyID1__c != null) {
				policyIds.add(rec.CC_PolicyID1__c);
			}
			if (rec.CC_PolicyID2__c != null) {
				policyIds.add(rec.CC_PolicyID2__c);
			}
			if (rec.CC_PolicyID3__c != null) {
				policyIds.add(rec.CC_PolicyID3__c);
			}
			if (rec.CC_PolicyID4__c != null) {
				policyIds.add(rec.CC_PolicyID4__c);
			}
		}

		return policyIds;
	}

	/**
	 * 保険契約Id抽出
	 *
	 */
	private Map<Id, Set<CC_FormMaster__c>> getFormMastersRecs(List<CC_SRPolicy__c> recs) {
		Map<Id, Set<CC_FormMaster__c>> formMastersRecs = new Map<Id, Set<CC_FormMaster__c>>();

		for (CC_SRPolicy__c rec : recs) {
			// 帳票１
			if (rec.CC_FormMaster1__c != null) {
				if (!formMastersRecs.containsKey(rec.Id)) {
					formMastersRecs.put(rec.Id, new Set<CC_FormMaster__c>());
				}
				CC_FormMaster__c insrec = new CC_FormMaster__c(id=rec.CC_FormMaster1__c
													, CC_FormType__c=rec.CC_FormMaster1__r.CC_FormType__c
													, CC_FormNo__c=rec.CC_FormMaster1__r.CC_FormNo__c
													, CC_FormName__c=rec.CC_FormMaster1__r.CC_FormName__c
													, CC_SRTypeNo__c=rec.CC_FormMaster1__r.CC_SRTypeNo__c
													, CC_CorporatePersonClassification__c=rec.CC_FormMaster1__r.CC_CorporatePersonClassification__c
													);
				formMastersRecs.get(rec.Id).add(insrec);
			}
			// 帳票２
			if (rec.CC_FormMaster2__c != null) {
				if (!formMastersRecs.containsKey(rec.Id)) {
					formMastersRecs.put(rec.Id, new Set<CC_FormMaster__c>());
				}
				CC_FormMaster__c insrec = new CC_FormMaster__c(id=rec.CC_FormMaster2__c
												, CC_FormType__c=rec.CC_FormMaster2__r.CC_FormType__c
												, CC_FormNo__c=rec.CC_FormMaster2__r.CC_FormNo__c
												, CC_FormName__c=rec.CC_FormMaster2__r.CC_FormName__c
												, CC_SRTypeNo__c=rec.CC_FormMaster2__r.CC_SRTypeNo__c
												, CC_CorporatePersonClassification__c=rec.CC_FormMaster2__r.CC_CorporatePersonClassification__c
												);
				formMastersRecs.get(rec.Id).add(insrec);
			}
		}

		formMastersRecs = formMastersRecs == null ? new Map<Id, Set<CC_FormMaster__c>>() : formMastersRecs;

		return formMastersRecs;
	}

	/**
	 * ラジオボタン値生成処理
	 * return List<SelectOption>
	 */
	private List<SelectOption> getClassesList() {
		List<SelectOption> ret = new List<SelectOption>();

		// オプション値設定
		ret.add(new SelectOption(POLICY_CLASS_CANCEL,POLICY_CLASS_CANCEL));
		ret.add(new SelectOption(POLICY_CLASS_REDUCTION,POLICY_CLASS_REDUCTION));
		ret.add(new SelectOption(POLICY_CLASS_CONTRACTOR_LOAN,POLICY_CLASS_CONTRACTOR_LOAN));

		return ret;
	}

	/**
	 * 帳票のレコード生成
	 *
	 */
	private CC_FormDetail__c insCCFormDetail(Id srPolicyId, Id ccFormMasterId, Id caseId) {
		// レコード追加処理
		CC_FormDetail__c ins = new CC_FormDetail__c();

		ins.CC_SRPolicyId__c = srPolicyId; // SR契約連結Id
		ins.CC_FormMasterId__c = ccFormMasterId; // コールセンター帳票マスタId
		ins.CC_Case__c = caseId; // サービスリクエスト
		ins.CC_IsFormOutputFlg__c = false; // 帳票出力フラグ

		return ins; // 生成IDを返す
	}

	/**
	 * 確認書レコード生成
	 * params String tab, String option, Id srPolicyId, Id ccFormMasterId, Id caseId
	 * return Id
	 */
	private CC_FormDetail__c insCCFormDetailConfirm(String tab, String option, Id srPolicyId, Id ccFormMasterId, Id caseId ) {
		CC_FormDetail__c ins = new CC_FormDetail__c();

		ins.CC_SRPolicyId__c = srPolicyId; // SR契約連結Id
		ins.CC_FormMasterId__c = ccFormMasterId; // コールセンター帳票マスタId
		ins.CC_Case__c = caseId; // サービスリクエスト
		ins.CC_IsFormOutputFlg__c = false; // 帳票出力フラグ

		return ins; // 生成IDを返す
	}

	/**
	 * 確認書レコード生成
	 * params String tab, String option, Id srPolicyId, Id ccFormMasterId, Id caseId
	 * return Id
	 */
	private List<CC_FormDetailPage__c> insCCFormDetailConfirmPage(String tab, String option, Id formDetailId ) {
		List<CC_FormDetailPage__c> ins = new List<CC_FormDetailPage__c>();
		// 確認書別マッピング
		if (tab == POLICY_CLASS_BULK) {
			makeParametersBulkPage(ins, formDetailId); // 一括
		} else {
			if (option == POLICY_CLASS_CANCEL) {
				this.policiesCount = makeParametersUnitCancellationPage(ins, formDetailId); // 解約
			} else if (option == POLICY_CLASS_REDUCTION) {
				this.policiesCount = makeParametersUnitReductionPage(ins, formDetailId); // 減額
			} else if (option == POLICY_CLASS_CONTRACTOR_LOAN) {
				this.policiesCount = makeParametersUnitContractorLoanPage(ins, formDetailId); // 契約者貸付
			}
		}

		return ins; // 生成IDを返す
	}

	/**
	 * 3桁左ゼロ埋め
	 *
	 */
	private String paddingLeftZeroThreeDigit(Integer n) {
		return this.paddingLeftZero(n, 3);
	}

	/**
	 * パディング 左ゼロ埋め
	 * @param n 数値
	 * @param len 桁数（数値の桁を含める）
	 * @return
	 */
	public String paddingLeftZero(Integer n, Integer len) {
		Integer nlen = (Math.floor(Math.log10(Double.valueOf(n))).intValue() + 1);
		if(nlen >= len) {
			return String.valueOf(n);
		}
		String s = '';
		for(Integer i=0, l=len-nlen; i<l; i++) {
			s += '0';
		}
		return s + String.valueOf(n);
	}

	/**
	 * 確認書発行 データマッピング(一括)
	 */
	private void makeParametersBulkPage(List<CC_FormDetailPage__c> objs, Id formDetailId) {
		String numStr;
		Integer i = 1;
		Integer iMax = 10;
		Integer iPage = 1;
		CC_FormDetailPage__c obj = new CC_FormDetailPage__c();
		obj.CC_FormDetailId__c = formDetailId;
		obj.CC_PageNo__c = iPage;
		for (BulkPrint bulkData : this.bulkTab) {
			if (i > iMax) {
				objs.add(obj);
				obj = new CC_FormDetailPage__c();
				obj.CC_FormDetailId__c = formDetailId;
				obj.CC_PageNo__c = ++iPage;
				i = 1;
			}
			numStr = this.paddingLeftZeroThreeDigit(i);
			obj.put('CC_Input'+numStr+'_1__c', bulkData.startNum); // 帳票入力項目00n_1
			obj.put('CC_Input'+numStr+'_2__c', bulkData.endNum); // 帳票入力項目00n_1
			i++;
		}
		obj.CC_PageNo__c = iPage;

		objs.add(obj);

	}

	/**
	 * 確認書発行 データマッピング(個別 解約)
	 */
	private Integer makeParametersUnitCancellationPage(List<CC_FormDetailPage__c> objs, Id formDetailId) {
		String numStr;
		Integer i = 1;
		Integer ret = 0;
		Integer iMax = 40;
		Integer iPage = 1;
		CC_FormDetailPage__c obj = new CC_FormDetailPage__c();
		obj.CC_FormDetailId__c = formDetailId;
		obj.CC_PageNo__c = iPage;
		for (UnitPrint unitData : this.unitTab) {
			if (i > iMax) {
				objs.add(obj);
				obj = new CC_FormDetailPage__c();
				obj.CC_FormDetailId__c = formDetailId;
				obj.CC_PageNo__c = ++iPage;
				i = 1;
			}
			numStr = this.paddingLeftZeroThreeDigit(i);
			obj.put('CC_Input'+numStr+'_1__c', unitData.securityNum); // 帳票入力項目00n_1
			obj.put('CC_Input'+numStr+'_2__c', unitData.personamName); // 帳票入力項目00n_1
			i++;
			ret++;
		}
		obj.CC_PageNo__c = iPage;

		objs.add(obj);

		return ret;
	}

	/**
	 * 確認書発行 データマッピング(個別 減額・特約解除)
	 * 1.保険証券番号
	 * 1.被保険者名
	 * 1.減額する主契約・特約名
	 * 1.減額後の保険金額（円）
	 *
	 */
	private Integer makeParametersUnitReductionPage(List<CC_FormDetailPage__c> objs, Id formDetailId) {
		String numStr;
		Integer i = 1;
		Integer ret = 0;
		Integer iMax = 20;
		Integer iPage = 1;
		CC_FormDetailPage__c obj = new CC_FormDetailPage__c();
		obj.CC_FormDetailId__c = formDetailId;
		obj.CC_PageNo__c = iPage;
		for (UnitPrint unitData : this.unitTab) {
			if (i > iMax) {
				objs.add(obj);
				obj = new CC_FormDetailPage__c();
				obj.CC_FormDetailId__c = formDetailId;
				obj.CC_PageNo__c = ++iPage;
				i = 1;
			}
			numStr = this.paddingLeftZeroThreeDigit(i);
			obj.put('CC_Input'+numStr+'_1__c', unitData.securityNum); // 帳票入力項目00n_1
			obj.put('CC_Input'+numStr+'_2__c', unitData.personamName); // 帳票入力項目00n_2
			obj.put('CC_Input'+numStr+'_3__c', unitData.ridersValue); // 帳票入力項目00n_3
			obj.put('CC_Input'+numStr+'_4__c', unitData.discountAfter); // 帳票入力項目00n_4
			i++;
			ret++;
		}
		obj.CC_PageNo__c = iPage;

		objs.add(obj);

		return ret;
	}

	/**
	 * 確認書発行 データマッピング(個別 契約者貸付)
	 */
	private Integer makeParametersUnitContractorLoanPage(List<CC_FormDetailPage__c> objs, Id formDetailId) {
		String numStr;
		Integer i = 1;
		Integer ret = 0;
		Integer iMax = 40;
		Integer iPage = 1;
		CC_FormDetailPage__c obj = new CC_FormDetailPage__c();
		obj.CC_FormDetailId__c = formDetailId;
		obj.CC_PageNo__c = iPage;
		for (UnitPrint unitData : this.unitTab) {
			if (i > iMax) {
				objs.add(obj);
				obj = new CC_FormDetailPage__c();
				obj.CC_FormDetailId__c = formDetailId;
				obj.CC_PageNo__c = ++iPage;
				i = 1;
			}
			numStr = this.paddingLeftZeroThreeDigit(i);
			obj.put('CC_Input'+numStr+'_1__c', unitData.securityNum); // 帳票入力項目00n_1
			obj.put('CC_Input'+numStr+'_2__c', unitData.amount); // 帳票入力項目00n_2
			i++;
			ret++;
		}
		obj.CC_PageNo__c = iPage;

		objs.add(obj);

		return ret;
	}

}