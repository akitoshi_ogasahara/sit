/**
 * ユニットプライスDAO
 */
public with sharing class E_UPXPFDao {	
	/**
	 * 最古と最新の基準日返却用内部クラス
	 */
	public class min_max_Date{
		public String minDate{get;set;}
		public String maxDate{get;set;}
	}
	/**
     * 指定した特別勘定IDをユニットプライスの中から最新の基準日最古と最新の基準日を取得する
     * @param svcpfIdSet: 特別勘定ID SET
     * @return E_UPXPFDao.min_max_Date: 最古と最新の基準日返却用内部クラス
     */
	public static E_UPXPFDao.min_max_Date getMinMaxDateBySvcpfId (Set<Id> svcpfIdSet){
        E_UPXPFDao.min_max_Date ret = new E_UPXPFDao.min_max_Date();
        for(AggregateResult ar:[Select 
						            MAX(ZFUNDDTE__c) maxDate
						            ,MIN(ZFUNDDTE__c) minDate
					        	From E_UPXPF__c
					            Where E_UPFPF__r.SVCPF__c in : svcpfIdSet]){
			ret.maxDate = E_Util.toString(ar.get('maxDate'));
			ret.minDate = E_Util.toString(ar.get('minDate'));
		}
		return ret;
	}
}