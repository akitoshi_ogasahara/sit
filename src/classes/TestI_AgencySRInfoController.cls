@isTest
private class TestI_AgencySRInfoController {
	/**
	 *2017/11追加分
	 *資格がランクアップした場合のテスト
	 */
	@isTest static void rankupTest(){
		//代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 資格査定詳細情報の作成
		E_LicenseAssessment__c licenseAssessment = TestE_LicenseAssessmentDao.createLicenseAssessment(agencySalesResults.Id);
		insert licenseAssessment;

		Test.startTest();

		I_AgencySRInfoController agencySRInfoController = new I_AgencySRInfoController();
		// 代理店挙積情報ID
		agencySRInfoController.paramSalesResultsId	= agencySalesResults.Id;

		// 表示ディスクレーマ(表示用)
		Map<String, String> disclaimers = agencySRInfoController.disclaimers;
		System.assertEquals('', disclaimers.get('AMS|SR|002'));

		agencySRInfoController.getSalesResult();
		System.assertEquals(agencySRInfoController.year == 2017,true);
		System.assertEquals(agencySRInfoController.rankDetail == 'rankup',true);

		Test.stopTest();
	}

	/**
	 *2017/11追加分
	 *資格がランクダウンした場合のテスト
	 */
	@isTest static void rankdownTest(){
		//代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		agencySalesResults.XHAH_AGCLS__c = '優績S';
		agencySalesResults.QualifSim__c = '優績A';
		insert agencySalesResults;

		// 資格査定詳細情報の作成
		E_LicenseAssessment__c licenseAssessment = TestE_LicenseAssessmentDao.createLicenseAssessment(agencySalesResults.Id);
		insert licenseAssessment;

		Test.startTest();

		I_AgencySRInfoController agencySRInfoController = new I_AgencySRInfoController();
		// 代理店挙積情報ID
		agencySRInfoController.paramSalesResultsId	= agencySalesResults.Id;

		agencySRInfoController.getSalesResult();
		System.assertEquals(agencySRInfoController.rankDetail == 'rankdown',true);

		Test.stopTest();
	}
	/**
	 *2017/11追加分
	 *次回予測資格がNull場合のテスト
	 */
	@isTest static void QualifSimnNullTest(){
		//代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		agencySalesResults.XHAH_AGCLS__c = '優績S';
		agencySalesResults.QualifSim__c = null;
		insert agencySalesResults;

		// 資格査定詳細情報の作成
		E_LicenseAssessment__c licenseAssessment = TestE_LicenseAssessmentDao.createLicenseAssessment(agencySalesResults.Id);
		insert licenseAssessment;

		Test.startTest();

		I_AgencySRInfoController agencySRInfoController = new I_AgencySRInfoController();
		// 代理店挙積情報ID
		agencySRInfoController.paramSalesResultsId	= agencySalesResults.Id;

		agencySRInfoController.getSalesResult();
		System.assertEquals(agencySRInfoController.dispJudgment == 'Hide',true);

		Test.stopTest();
	}

}