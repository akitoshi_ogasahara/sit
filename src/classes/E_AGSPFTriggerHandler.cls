public with sharing class E_AGSPFTriggerHandler {
	public E_AGSPFTriggerHandler() {
		
	}
	public void onBeforeInsert(List<E_AGSPF__c> newList){
		setE_CHTPFRelation(newList);
	}
	public void onBeforeUpdate(List<E_AGSPF__c> newList ,Map<ID,E_AGSPF__c> oldMap){
		setE_CHTPFRelation(newList,oldMap);
	}
	/**
	 * 販売取扱者リレーション設定
	 */
	private void setE_CHTPFRelation(List<E_AGSPF__c> newList){
		setE_CHTPFRelation(newList,new Map<Id,E_AGSPF__c>());
	}
	private void setE_CHTPFRelation(List<E_AGSPF__c> newList ,Map<ID,E_AGSPF__c> oldMap){
		//証券番号+募集人SEQ => 販売取扱者ID
		Map<String,ID> E_CHTPFMap = new Map<String,ID> ();
		//In句作成
		for(E_AGSPF__c newRec : newList){
			if(!isTarget(newRec ,oldMap.get(newRec.id))) continue;
			E_CHTPFMap.put(newRec.CHDRNUM__c + '|' + getSEQStr(newRec.ZJINTSEQ2__c),null);
		}
		//更新対象無しの際は処理終了
		if(E_CHTPFMap.isEmpty()) return;
		//Map作成
		for(E_CHTPF__c rec : E_CHTPFDao.getRecsByCHDRAndAGNT(E_CHTPFMap.keySet())){
			E_CHTPFMap.put(rec.E_CHTPFKey__c, rec.id);
		}
		//参照関係作成
		for(E_AGSPF__c newRec : newList){
			if(!isTarget(newRec ,oldMap.get(newRec.id))) continue;
			newRec.E_CHTPF__c = E_CHTPFMap.get(newRec.CHDRNUM__c + '|' + getSEQStr(newRec.ZJINTSEQ2__c));
		}
	}
	/**
	 * 更新対象確認
	 * 更新対象の場合はfalseを返す
	 */
	private Boolean isTarget (E_AGSPF__c newRec ,E_AGSPF__c oldRec){
		if(oldRec <> null){
			//リレーションが既にある かつ データ種類の変更がない かつ 証券番号の変更がない かつ 募集人コードの変更がない場合は処理対象外
			//初期空更新を想定し、リレーションがない場合は処理対象とする
			if(oldRec.E_CHTPF__c <> null
					&& oldRec.KDATATYP__c == newRec.KDATATYP__c
					&& oldRec.CHDRNUM__c == newRec.CHDRNUM__c 
					&& oldRec.ZJINTSEQ2__c == newRec.ZJINTSEQ2__c){
				return false;
			}
			//リレーションの再設定を行うため、一度null更新を行う
			newRec.E_CHTPF__c = null;
		}
		if(newRec.KDATATYP__c <> '2' 
				|| String.isBlank(newRec.CHDRNUM__c) 
				|| newRec.ZJINTSEQ2__c == null){
			return false;
		} 
		return true;
	}
	/**
	 * SEQコードの置換
	 */
	private String getSEQStr (Decimal seq) {
		if(seq == null) return '';
		return String.valueOf(seq).substringBefore('.');
	}
}