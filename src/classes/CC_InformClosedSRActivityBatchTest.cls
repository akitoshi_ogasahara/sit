@isTest
private with sharing class CC_InformClosedSRActivityBatchTest {
    private static testMethod void batch_test01(){
        createMessageMaster('CC|MRE|001');
        CC_SRTypeMaster__c srType = createSRType();
        Case sr = createSR(srType.Id);
        Account office = createAccount('1A');
        Contact agent = createContact(office.Id);
        E_Policy__c policy = createPolicy(agent.Id);
        createSRPolicy(sr.Id, policy.Id);
        createSRActivityMaster();
        CC_SRActivity__c srActivity = createSRActivity(sr.Id);
        createBranchConfig('1A');

        Test.startTest();
        CC_InformClosedSRActivityBatch batch = new CC_InformClosedSRActivityBatch();
        Database.executeBatch(batch,200);
        Test.stopTest();

        CC_SRActivity__c result = [SELECT CC_MRBatchKey__c FROM CC_SRActivity__c WHERE Id =: srActivity.Id];
        system.assertEquals(result.CC_MRBatchKey__c ,E_Util.getSysDate('yyyyMMdd_HH00'));
    }
//----------- 以下TestData作成用 -----------
    /**
     * メッセージマスター作成
     */
    private static E_MessageMaster__c createMessageMaster(String key) {
        E_MessageMaster__c msg = new E_MessageMaster__c();
        msg.Key__c = key;
        msg.Value__c = 'TEST内容';
        insert msg;
        return msg;
    }
    /**
     * SRType作成
     */
    private static CC_SRTypeMaster__c createSRType(){
        CC_SRTypeMaster__c srType = new CC_SRTypeMaster__c();
        srType.CC_Department__c = 'COLI';
        srType.CC_IsCommissionSR__c = true;
        insert srType;
        return srType;
    }
    /**
     * SR作成
     */
    private static Case createSR(ID srTypeId){
        Case sr = new Case();
        sr.CC_SRTypeId__c = srTypeId;
        sr.CMN_Source__c = '電話';
        insert sr;
        return sr;
    }
    /**
     * 保険契約ヘッダ作成
     */
    private static E_Policy__c createPolicy(ID conId){
        E_Policy__c policy = new E_Policy__c();
        policy.MainAgent__c = conId;
        policy.SubAgent__c = conId;

        insert policy;
        return policy;
     }
    /**
     * SR契約連結作成
     */
    private static CC_SRPolicy__c createSRPolicy(ID srId, ID poicyId){
        CC_SRPolicy__c srPolicy = new CC_SRPolicy__c();
        srPolicy.CC_CaseId__c = srId;
        srPolicy.CC_PolicyID1__c = poicyId;
        srPolicy.CC_PolicyID2__c = poicyId;
        srPolicy.CC_PolicyID3__c = poicyId;
        srPolicy.CC_PolicyID4__c = poicyId;
        insert srPolicy;
        return srPolicy;
     }
     /**
     * 活動マスタ作成
     */
    private static CC_ActivityMaster__c createSRActivityMaster(){
        CC_ActivityMaster__c srActivityMaster = new CC_ActivityMaster__c();
        srActivityMaster.Name = '受付内容確認';
        insert srActivityMaster;
        return srActivityMaster;
     }
     /**
     * 活動作成
     */
    private static CC_SRActivity__c createSRActivity(ID srId){
        CC_SRActivity__c srActivity = new CC_SRActivity__c();
        srActivity.CC_SRNo__c = srId;
        srActivity.CC_ActivityType__c = '受付内容確認';
        srActivity.CC_Status__c = '終了';
        srActivity.CC_Order__c = 0;
        insert srActivity;
        return srActivity;
     }
     /**
     * Account作成
     */
    public static Account createAccount(String brNo) {
        Account parentAcc = new Account();
        parentAcc.Name = 'TestParentAccountName';
        insert parentAcc;

        Account acc = new Account();
        acc.Name = 'TestAccountName';
        acc.ParentId = parentAcc.Id;
        acc.E_CL2PF_BRANCH__c = brNo;
        insert acc;

        return acc;
    }

    /**
     * Contact作成
     */
    public static Contact createContact(Id accId) {
        Contact con = new Contact();
        con.AccountId = accId;
        con.LastName = 'TEST募集人';

        insert con;
        return con;
    }
    /**
     * 支社情報作成
     */
    public static CC_BranchConfig__c createBranchConfig(String brNo) {
        CC_BranchConfig__c branchConfig = new CC_BranchConfig__c();
        branchConfig.Name = 'TEST支社';
        branchConfig.CC_BranchCode__c = brNo;
        branchConfig.CC_SharedMailAdd__c = 'nncctestmail@example.com';

        insert branchConfig;
        return branchConfig;
    }
}