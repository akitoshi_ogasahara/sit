public with sharing class I_NewPolicyDTO {
	
	// ソート実行用：項目
	public static String sortType;
	// ソート実行用：昇順 OR 降順
	public static Boolean sortIsAsc;

	//DTOクラス
	public class NewPolicy implements Comparable {
		//状況
		public String status {get;set;}
		//状況スタイルクラス
		public String styleClass {get;set;}
		//契約者名
		public String owner {get;set;}
		//契約者名カナ
		public String ownerKana {get;set;}
		//被保険者名
		public String insured {get;set;}
		//被保険者名カナ
		public String insuredKana {get;set;}
		//保険種類
		public String cntType {get;set;}
		//ステータス発信日
		public Datetime statusDate {get;set;}
		//ステータス発信日(JST)
		public String getJSTDateStr() {
			return (this.statusDate == null)? '':this.statusDate.format('MM/dd', 'JST');
		}
		//団体フラグ
		public Boolean isZGRUP {get; set;}
		//団体番号
		public String GRUPKEY {get; set;}
		//証券番号
		public String CHDRNUM {get; set;}
		//保険料
		public Decimal insttot01 {get;set;}
		//遷移先URL
		public String destURL {get;set;}
		//担当MR(主)
		public String mr {get;set;}
		//事務所名(主)
		public String office {get;set;}
		//募集人(主)
		public String agent {get;set;}
		//不備一覧
		public List<Defect> defects {get;set;}
		// {成立Ａ／Ｃ（年}/{成立Ａ／Ｃ（月）}
		public String establishedAC {get; set;}

		//Contractor
		public NewPolicy () {
		}
		public NewPolicy (E_NewPolicy__c rec) {
			setNewPolicy(rec);
		}
		private void setNewPolicy(E_NewPolicy__c rec) {
			//状況
			this.status = rec.Status__c;
			//発信日
			this.statusDate = rec.statusDate__c;
			//契約者
			//this.owner = rec.COMM_ZCLNAME__c;
			//this.ownerKana = rec.ContractorE_CLTPF_ZCLKNAME__c;
			//外字対応
			this.owner = rec.ContractorName__c;
			//this.ownerKana = rec.ContractorNameKana__c;
			this.ownerKana = rec.ContractorE_CLTPF_ZCLKNAME__c;
			//個/団 アイコン
			this.isZGRUP = rec.KFRAMEIND__c == 'Y';
			//団体番号
			this.GRUPKEY = rec.GRUPKEY__c;
			//証券番号
			this.CHDRNUM = rec.CHDRNUM__c;
			//保険種類
			this.cntType = rec.InsuranceType__c;
			//代理店事務所
			this.office = rec.AgentAccountName__c;
			//募集人
			this.agent = rec.Agent_Name__c;
			//行クリック用
			this.destURL = 'IRIS_NewPolicyDetail?id=' + EncodingUtil.urlEncode(rec.id, 'UTF-8');
			//不備リスト
			this.defects = new List<Defect>();
		}

		// ソート
		public Integer compareTo(Object compareTo){
			NewPolicy compareToNewPolicy = (NewPolicy)compareTo;
			Integer result = 0;
			
			//状況
			if(sortType == 'status'){
				result = compareToStatus(status,compareToNewPolicy.status);
			//発信日
			}else if(sortType == 'statusDate'){
				result = I_Util.compareToDatetime(statusDate,compareToNewPolicy.statusDate);
			//契約者
			}else if(sortType == 'owner'){
				result = I_Util.compareToString(ownerKana,compareToNewPolicy.ownerKana);
			//被保険者
			}else if(sortType == 'insured'){
				result = I_Util.compareToString(insuredKana,compareToNewPolicy.insuredKana);
			//団体番号
			}else if(sortType == 'GRUPKEY'){
				result = I_Util.compareToString(GRUPKEY,compareToNewPolicy.GRUPKEY);
			//証券番号
			}else if(sortType == 'CHDRNUM'){
				result = I_Util.compareToString(CHDRNUM,compareToNewPolicy.CHDRNUM);
			//保険種類
			}else if(sortType == 'cntType'){
				result = I_Util.compareToString(cntType,compareToNewPolicy.cntType);
			//保険料
			}else if(sortType == 'insttot01'){
				result = I_Util.compareToDecimal(insttot01,compareToNewPolicy.insttot01);
			//代理店事務所
			}else if(sortType == 'office'){
				result = I_Util.compareToString(office,compareToNewPolicy.office);
			//募集人
			}else if(sortType == 'agent'){
				result = I_Util.compareToString(agent,compareToNewPolicy.agent);
			}
			return sortIsAsc ? result : result * (-1);
		}
		/**
		 * ソート判定（StatusCode）
		 * @paarm  String：比較元
		 * @paarm  String：比較先
		 * @return Integer:比較結果
		 */
		private Integer compareToStatus(String compare, String compareTo){
			return I_Util.compareToString(I_NewPolicyConst.statusSortMap.get(compare),I_NewPolicyConst.statusSortMap.get(compareTo));
		}
	}


	//DTOクラス
	public class Defect implements Comparable {
		//不備コード
		public String code {get;set;}
		//不備内容
		public String detail {get;set;}
		//key
		public String key {get;set;}

		// ソート
		public Integer compareTo(Object compareTo){
			Defect compareToDefect = (Defect)compareTo;
			Integer result = 0;
			if(sortType == 'code'){
				result = I_Util.compareToString(code,compareToDefect.code);
			}else if(sortType == 'detail'){
				result = I_Util.compareToString(detail,compareToDefect.detail);
			}else if(sortType == 'key'){
				result = I_Util.compareToString(key,compareToDefect.key);
			}
			return sortIsAsc ? result : result * (-1);
		}
		//Constractor
		public Defect (E_DefectMaster__c rec) {
			this.code = rec.Code__c;
			this.detail = rec.Value__c;
			this.key = rec.SortKey__c;
		}
	}
}