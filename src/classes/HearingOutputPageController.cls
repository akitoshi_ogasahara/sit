/**
* クラス名	:  HearingOutputPageController
* クラス概要	:  ヒアリング情報詳細画面(コントローラ)
* @created	:  2014/01/21 Khanh Vo Quoc
* @modified	:  2015/12/08 KSVC PHUONG DINH VU
*/
public with sharing class HearingOutputPageController {
	// ファイナンス情報
	public Finance__c finance{get; set;}
	// ファイナンス情報.決算期(前期)
	public String strTerm{get; set;}
	// 顧客
	private Customer__c customer{get; set;}
	
	/* 2015/12/08 PHUONG DINH VU ADD START */
	// タイムアウト回避の詳細
	public string	strFCTimeOut { get; set; }
	/* 2015/12/08 PHUONG DINH VU ADD END */
	
	/**
	* HearingOutputPageController
	* ページ初期化処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified :   
	*/
	public HearingOutputPageController(ApexPages.StandardController controller){
		/* 2015/12/08 PHUONG DINH VU ADD START */
		// タイムアウト回避の詳細
		resetSessionTimeout();
		/* 2015/12/08 PHUONG DINH VU ADD END */
		// ファイナンス情報を取得
		finance = (Finance__c)controller.getRecord();
		// 画面にファイナンス情報.決算期(前期)値を表示
		strTerm = String.format(Label.H_Term_Out, new List<String>{String.valueOf(finance.Term__c)});
	}
	/**
	* clickLinkKokyakuShousaiJouhou
	* 「顧客詳細情報へ」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified :   
	*/
	public Pagereference clickLinkKokyakuShousaiJouhou(){
		try{
			// 顧客を取得 
			customer = HearingDAO.getCustomerById(finance.Customer__c);
			// 顧客レコードが取得出来ない場合
			if(customer == null || String.isBlank(String.valueOf(customer.Id))){
				// エラーを表示
				Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,
																Label.Msg_Error_H_NotCustomer));
				return null;
			}
			Pagereference pageKokyaku = new Pagereference('/' + customer.Id);
			pageKokyaku.setRedirect(true);
			return pageKokyaku;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
		return null;
		
	}
	/**
	* actionButton
	* 「顧客詳細情報へ」リンク処理
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/21 Khanh Vo Quoc
	* @modified :   
	*/
	/*public Pagereference actionButton(){
		try{
			// BS・PL入力画面へ遷移
			Pagereference hearingInputPage= Page.HearingInputPage;
			// 現在のページURLを取得
			String strUrl = Apexpages.currentPage().getUrl();
			// 現在のページURLをセット
			hearingInputPage.getParameters().put(CommonConst.STR_PARAMETER_RETURL, strUrl);
			// URLパラメータにファイナンス情報obj.レコードIDをセット
			hearingInputPage.getParameters().put('id', finance.Id);
			// URLパラメータにファイナンス情報obj.顧客obj.カスタムオブジェクトIDをセット
			hearingInputPage.getParameters().put(CommonConst.STR_PARAMETER_CUID, finance.Customer__c);
			hearingInputPage.setRedirect(true);
			return hearingInputPage;
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
		return null;
	}*/
	
	/**
	* resetSessionTimeout
	* PERSONAタイムアウト回避
	* @param 	: なし
	* @return	: なし
	* @created  : 2015/12/03 KSVC PHUONG DINH VU
	* @modified : 2015/12/08 KSVC PHUONG DINH VU
	*/
	public void resetSessionTimeout(){
		try{
			// List<FC_TIMEOUT__c> fcTimeoutList =  [select FC_TIMEOUT__c From FC_TIMEOUT__c];
			FC_TIMEOUT__c fcTimeOut = HearingDAO.getFcTimeOut();
			strFCTimeOut = fcTimeOut == null || fcTimeOut.FC_TIMEOUT__c == null ? '0' : String.valueOf(fcTimeOut.FC_TIMEOUT__c);
		}catch(exception ex){
			// システムエラー
			Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.Msg_Error_System));
		}
	}
}