@isTest
private class TestE_InwardTransferExtender{
	/**
	 * 繰入比率変更の同意から最低限のレコードで実施
	 */
	private static testMethod void testInwardTransfer1() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.FLAG05__c = '1';
		idcpf.FLAG06__c = '1';
		idcpf.TRCDE01__c = 'TD04';
		idcpf.TRCDE02__c = 'TD05';
		idcpf.TRCDE03__c = 'TD06';
		idcpf.TRCDE04__c = 'TD07';
		idcpf.TRCDE05__c = 'TDB0';
		insert idcpf;
		
		//結果画面
		PageReference pref;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        E_Policy__c policy = new E_Policy__c();
	       	policy.COMM_ZINVDCF__c = true;
            policy.COMM_ZTFRDCF__c = true;
	        pref = Page.E_InwardTransferAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_InwardTransferController controller = new E_InwardTransferController(standardcontroller);
	        E_InwardTransferExtender extender = controller.getExtender();
	        extender.init();
	        pref = extender.PageAction();
	        extender.isValidate();
	        extender.doReturn();
	        extender.doReturn();
	        extender.doNextEntry();
	        //extender.isAgree = true;
	        //extender.doNextEntry();
	        extender.doNextConfirm();
	        //extender.doCancel();
	        extender.doConfirmSave();

	        //テスト終了
			Test.stopTest();
		}
/*
		String resultUrl = pref.getUrl().toLowerCase();
		// URLを小文字へ変換
		system.assert(resultUrl.indexOf(Page.E_ErrorPage.getUrl()) != -1);
*/
    }

	/**
	 * 繰入比率変更の同意から最低限のレコードで実施
	 */
	private static testMethod void testInwardTransfer2() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.FLAG05__c = '1';
		idcpf.FLAG06__c = '1';
		idcpf.TRCDE01__c = 'TD04';
		idcpf.TRCDE02__c = 'TD05';
		idcpf.TRCDE03__c = 'TD06';
		idcpf.TRCDE04__c = 'TD07';
		idcpf.TRCDE05__c = 'TDB0';
		insert idcpf;
		
		//結果画面
		PageReference pref;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        E_Policy__c policy = new E_Policy__c();
	       	policy.COMM_ZINVDCF__c = true;
            policy.COMM_ZTFRDCF__c = true;
	        pref = Page.E_InwardTransferAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_InwardTransferController controller = new E_InwardTransferController(standardcontroller);
	        E_InwardTransferExtender extender = controller.getExtender();
	        extender.init();
	        pref = extender.PageAction();
	        extender.isAgree = true;
	        extender.doNextEntry();
	        //extender.doCancel();

	        //テスト終了
			Test.stopTest();
		}
/*
		String resultUrl = pref.getUrl().toLowerCase();
		// URLを小文字へ変換
		system.assert(resultUrl.indexOf(Page.E_ErrorPage.getUrl()) != -1);
*/
    }


	/**
	 * 新しい繰入比率が不正
	 */
	private static testMethod void testInwardTransfer3() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.FLAG05__c = '1';
		idcpf.FLAG06__c = '1';
		idcpf.TRCDE01__c = 'TD04';
		idcpf.TRCDE02__c = 'TD05';
		idcpf.TRCDE03__c = 'TD06';
		idcpf.TRCDE04__c = 'TD07';
		idcpf.TRCDE05__c = 'TDB0';
    	idcpf.ZSTATUS02__c = '1';
    	idcpf.ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207'; // terrasky
		insert idcpf;

		// 保険契約ヘッダの登録
        E_Policy__c policy = new E_Policy__c();
       	policy.COMM_ZINVDCF__c = true;
        policy.COMM_ZTFRDCF__c = true;
        insert policy;

		// 特別勘定の登録
        List<E_SVCPF__c> esvcpfList = new List<E_SVCPF__c>();
        for(Integer i = 0; i < 4; i++){
	        esvcpfList.add(new E_SVCPF__c(
	            FLAG01__c = true, 						// FUND表示フラグ
	            ZEFUNDCD__c = 'V00' + String.valueOf(i),// ファンドコード
	            ZCATFLAG__c = E_Const.FUND_KIND_COLI,   // ファンド区分
	            ZFUNDNAM01__c = 'ファンド名101',     		// ファンド名０１
	            ZFUNDNAM02__c = 'ファンド名102',     		// ファンド名０２
	            ZFUNDNAM03__c = 'ファンド名103',     		// ファンド名０３
	            ZFUNDNAM04__c = 'ファンド名104',     		// ファンド名０４
	            ZEQTYRTO__c = 80,                 		// 株式割合
	            CURRFROM__c = '20141201'            	// 設定日
	        ));
        }
        insert esvcpfList;

        // 変額保険ファンドの登録
        List<E_SVFPF__c> esvfpfList = new List<E_SVFPF__c>();
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[0].Id,      // 特別勘定
            ZREVAMT__c = 11100,                 // 積立金
            SVFPFCode__c = 'hfund0001',         // 変額保険ファンドコード
            ZHLDPERC__c = 11.1,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[1].Id,      // 特別勘定
            ZREVAMT__c = 22200,                 // 積立金
            SVFPFCode__c = 'hfund0002',         // 変額保険ファンドコード
            ZHLDPERC__c = 22.2,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[2].Id,      // 特別勘定
            ZREVAMT__c = 33300,                 // 積立金
            SVFPFCode__c = 'hfund0003',         // 変額保険ファンドコード
            ZHLDPERC__c = 33.3,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[3].Id,      // 特別勘定
            ZREVAMT__c = 44400,                 // 積立金
            SVFPFCode__c = 'hfund0004',         // 変額保険ファンドコード
            ZHLDPERC__c = 60.6,                 // 保有割合
            ZINVPERC__c = 40
        ));
        insert esvfpfList;  
		
		//結果画面
		PageReference pref;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        pref = Page.E_InwardTransferAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_InwardTransferController controller = new E_InwardTransferController(standardcontroller);
	        for(E_SVFPF__c svfpf : esvfpfList){
	        	controller.E_SVFPF_c_E_Policy_c.add(svfpf);
	        }
	        controller.E_SVFPF_c_E_Policy_c.items[0].record.InputZINVPERC__c = '10';
	        controller.E_SVFPF_c_E_Policy_c.items[1].record.InputZINVPERC__c = '29';
	        controller.E_SVFPF_c_E_Policy_c.items[2].record.InputZINVPERC__c = null;
	        //controller.E_SVFPF_c_E_Policy_c.items[3].record.InputZINVPERC__c = 'A';
	        E_InwardTransferExtender extender = controller.getExtender();
	        extender.inputPassWord = 'terrasky';

	        extender.init();
			extender.PageAction();
	        extender.isValidate();
	        extender.doReturn();
	        extender.doNextEntry();
	        extender.isAgree = true;
	        extender.doNextEntry();
	        extender.doNextConfirm();
			//extender.doCancel();
	        //extender.doCancel();
			pref = extender.doConfirmSave();

	        //テスト終了
			Test.stopTest();
		}
		system.assertNotEquals(null, pref);
    }

	/**
	 * 正常系のレコードで実施(初期パスワードの場合)
	 */
	private static testMethod void testInwardTransfer4() {
		createMsgs();
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.FLAG05__c = '1';
		idcpf.FLAG06__c = '1';
		idcpf.TRCDE01__c = 'TD04';
		idcpf.TRCDE02__c = 'TD05';
		idcpf.TRCDE03__c = 'TD06';
		idcpf.TRCDE04__c = 'TD07';
		idcpf.TRCDE05__c = 'TDB0';
    	idcpf.ZSTATUS02__c = '1';
    	//idcpf.ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207'; // terrasky
    	idcpf.ZPASSWD02__c = '1771369368';//'Ebiz_8520412101'; // 
		idcpf.UseTempProcedurePW__c = true;
        idcpf.ZPASSWD02DATE__c ='20150101';
		insert idcpf;

		// 保険契約ヘッダの登録
        E_Policy__c policy = new E_Policy__c();
       	policy.COMM_ZINVDCF__c = true;
        policy.COMM_ZTFRDCF__c = true;
        insert policy;

		// 特別勘定の登録
        List<E_SVCPF__c> esvcpfList = new List<E_SVCPF__c>();
        for(Integer i = 0; i < 4; i++){
	        esvcpfList.add(new E_SVCPF__c(
	            FLAG01__c = true, 						// FUND表示フラグ
	            ZEFUNDCD__c = 'V00' + String.valueOf(i),// ファンドコード
	            ZCATFLAG__c = E_Const.FUND_KIND_COLI,   // ファンド区分
	            ZFUNDNAM01__c = 'ファンド名101',     		// ファンド名０１
	            ZFUNDNAM02__c = 'ファンド名102',     		// ファンド名０２
	            ZFUNDNAM03__c = 'ファンド名103',     		// ファンド名０３
	            ZFUNDNAM04__c = 'ファンド名104',     		// ファンド名０４
	            ZEQTYRTO__c = 80,                 		// 株式割合
	            CURRFROM__c = '20141201'            	// 設定日
	        ));
        }
        insert esvcpfList;

        // 変額保険ファンドの登録
        List<E_SVFPF__c> esvfpfList = new List<E_SVFPF__c>();
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[0].Id,      // 特別勘定
            ZREVAMT__c = 11100,                 // 積立金
            SVFPFCode__c = 'hfund0001',         // 変額保険ファンドコード
            ZHLDPERC__c = 11.1,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[1].Id,      // 特別勘定
            ZREVAMT__c = 22200,                 // 積立金
            SVFPFCode__c = 'hfund0002',         // 変額保険ファンドコード
            ZHLDPERC__c = 22.2,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[2].Id,      // 特別勘定
            ZREVAMT__c = 33300,                 // 積立金
            SVFPFCode__c = 'hfund0003',         // 変額保険ファンドコード
            ZHLDPERC__c = 33.3,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[3].Id,      // 特別勘定
            ZREVAMT__c = 44400,                 // 積立金
            SVFPFCode__c = 'hfund0004',         // 変額保険ファンドコード
            ZHLDPERC__c = 60.6,                 // 保有割合
            ZINVPERC__c = 40
        ));
        insert esvfpfList;  
		
		//結果画面
		PageReference pref;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        pref = Page.E_InwardTransferAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_InwardTransferController controller = new E_InwardTransferController(standardcontroller);
	        E_InwardTransferExtender extender = controller.getExtender();

	        extender.init();
	        for(E_SVFPF__c svfpf : esvfpfList){
	        	controller.E_SVFPF_c_E_Policy_c.add(svfpf);
	        }
	        //controller.E_SVFPF_c_E_Policy_c.addAll(esvfpfList);
	        controller.E_SVFPF_c_E_Policy_c.items[0].record.InputZINVPERC__c = '11';
	        controller.E_SVFPF_c_E_Policy_c.items[1].record.InputZINVPERC__c = '29';
	        controller.E_SVFPF_c_E_Policy_c.items[2].record.InputZINVPERC__c = '60';
	        //controller.E_SVFPF_c_E_Policy_c.items[3].record.InputZINVPERC__c = '0';

			extender.PageAction();
	        extender.isValidate();
	        extender.doReturn();
	        extender.doNextEntry();
	        extender.isAgree = true;
	        extender.doNextEntry();
	        extender.doNextConfirm();
	        extender.doCancel();
	        // パスワードの設定
	      	extender.inputPassWord = '8520412101';//'2345678901';
	      	extender.newPassWord = 'terrask1';
	      	extender.verPassWord = 'terrask1';

	      	E_Email__c eMail = new E_Email__c(NAME = E_Const.EMAIL_CHARGE_TRANSFER,Email__c = 'xxxx@xx.xx');
            insert eMail;
			pref = extender.doConfirmSave();
			//extender.doCancel();

	        //テスト終了
			Test.stopTest();
			// 正常終了:完了画面
			system.assertEquals(null, pref);
		}
    }

	/**
	 * 正常系のレコードで実施(初期パスワード以外の場合)
	 */
	private static testMethod void testInwardTransfer5() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.FLAG05__c = '1';
		idcpf.FLAG06__c = '1';
		idcpf.TRCDE01__c = 'TD04';
		idcpf.TRCDE02__c = 'TD05';
		idcpf.TRCDE03__c = 'TD06';
		idcpf.TRCDE04__c = 'TD07';
		idcpf.TRCDE05__c = 'TDB0';
    	idcpf.ZSTATUS02__c = '1';
    	idcpf.ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207'; // terrasky
    	// 手続きパスワード変更日で仮パスワードかどうか判定
    	idcpf.ZPASSWD02DATE__c = '20150220';

		insert idcpf;

		// 保険契約ヘッダの登録
        E_Policy__c policy = new E_Policy__c();
       	policy.COMM_ZINVDCF__c = true;
        policy.COMM_ZTFRDCF__c = true;
        insert policy;

		// 特別勘定の登録
        List<E_SVCPF__c> esvcpfList = new List<E_SVCPF__c>();
        for(Integer i = 0; i < 4; i++){
	        esvcpfList.add(new E_SVCPF__c(
	            FLAG01__c = true, 						// FUND表示フラグ
	            ZEFUNDCD__c = 'V00' + String.valueOf(i),// ファンドコード
	            ZCATFLAG__c = E_Const.FUND_KIND_COLI,   // ファンド区分
	            ZFUNDNAM01__c = 'ファンド名101',     		// ファンド名０１
	            ZFUNDNAM02__c = 'ファンド名102',     		// ファンド名０２
	            ZFUNDNAM03__c = 'ファンド名103',     		// ファンド名０３
	            ZFUNDNAM04__c = 'ファンド名104',     		// ファンド名０４
	            ZEQTYRTO__c = 80,                 		// 株式割合
	            CURRFROM__c = '20141201'            	// 設定日
	        ));
        }
        insert esvcpfList;

        // 変額保険ファンドの登録
        List<E_SVFPF__c> esvfpfList = new List<E_SVFPF__c>();
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[0].Id,      // 特別勘定
            ZREVAMT__c = 11100,                 // 積立金
            SVFPFCode__c = 'hfund0001',         // 変額保険ファンドコード
            ZHLDPERC__c = 11.1,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[1].Id,      // 特別勘定
            ZREVAMT__c = 22200,                 // 積立金
            SVFPFCode__c = 'hfund0002',         // 変額保険ファンドコード
            ZHLDPERC__c = 22.2,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[2].Id,      // 特別勘定
            ZREVAMT__c = 33300,                 // 積立金
            SVFPFCode__c = 'hfund0003',         // 変額保険ファンドコード
            ZHLDPERC__c = 33.3,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[3].Id,      // 特別勘定
            ZREVAMT__c = 44400,                 // 積立金
            SVFPFCode__c = 'hfund0004',         // 変額保険ファンドコード
            ZHLDPERC__c = 60.6,                 // 保有割合
            ZINVPERC__c = 40
        ));
        insert esvfpfList;  
		
		//結果画面
		PageReference pref;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        pref = Page.E_InwardTransferAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_InwardTransferController controller = new E_InwardTransferController(standardcontroller);
	        E_InwardTransferExtender extender = controller.getExtender();

	        extender.init();
	        for(E_SVFPF__c svfpf : esvfpfList){
	        	controller.E_SVFPF_c_E_Policy_c.add(svfpf);
	        }
	        //controller.E_SVFPF_c_E_Policy_c.addAll(esvfpfList);
	        controller.E_SVFPF_c_E_Policy_c.items[0].record.InputZINVPERC__c = '11';
	        controller.E_SVFPF_c_E_Policy_c.items[1].record.InputZINVPERC__c = '29';
	        controller.E_SVFPF_c_E_Policy_c.items[2].record.InputZINVPERC__c = '60';
	        //controller.E_SVFPF_c_E_Policy_c.items[3].record.InputZINVPERC__c = '0';

			extender.PageAction();
	        extender.isValidate();
	        extender.doReturn();
	        extender.doNextEntry();
	        extender.isAgree = true;
	        extender.doNextEntry();
	        extender.doNextConfirm();
	        extender.doCancel();
	        // パスワードの設定
	      	extender.inputPassWord = 'terrasky';
	      	extender.newPassWord = 'terrask1';
	      	extender.verPassWord = 'terrask1';

	      	E_Email__c eMail = new E_Email__c(NAME = E_Const.EMAIL_CHARGE_TRANSFER,Email__c = 'xxxx@xx.xx');
            insert eMail;
			pref = extender.doConfirmSave();

	        //テスト終了
			Test.stopTest();
		}
		// 正常終了:完了画面
		system.assertEquals(null, pref);
    }

	/**
	 * 異常系のレコードで実施(初期パスワード以外、半角数字エラーの場合)
	 */
	private static testMethod void testInwardTransfer6() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		idcpf.FLAG04__c = '1';
		idcpf.FLAG05__c = '1';
		idcpf.FLAG06__c = '1';
		idcpf.TRCDE01__c = 'TD04';
		idcpf.TRCDE02__c = 'TD05';
		idcpf.TRCDE03__c = 'TD06';
		idcpf.TRCDE04__c = 'TD07';
		idcpf.TRCDE05__c = 'TDB0';
    	idcpf.ZSTATUS02__c = '1';
    	idcpf.ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207'; // terrasky
    	// 手続きパスワード変更日で仮パスワードかどうか判定
    	idcpf.ZPASSWD02DATE__c = '20150220';

		insert idcpf;

		// 保険契約ヘッダの登録
        E_Policy__c policy = new E_Policy__c();
       	policy.COMM_ZINVDCF__c = true;
        policy.COMM_ZTFRDCF__c = true;
        insert policy;

		// 特別勘定の登録
        List<E_SVCPF__c> esvcpfList = new List<E_SVCPF__c>();
        for(Integer i = 0; i < 4; i++){
	        esvcpfList.add(new E_SVCPF__c(
	            FLAG01__c = true, 						// FUND表示フラグ
	            ZEFUNDCD__c = 'V00' + String.valueOf(i),// ファンドコード
	            ZCATFLAG__c = E_Const.FUND_KIND_COLI,   // ファンド区分
	            ZFUNDNAM01__c = 'ファンド名101',     		// ファンド名０１
	            ZFUNDNAM02__c = 'ファンド名102',     		// ファンド名０２
	            ZFUNDNAM03__c = 'ファンド名103',     		// ファンド名０３
	            ZFUNDNAM04__c = 'ファンド名104',     		// ファンド名０４
	            ZEQTYRTO__c = 80,                 		// 株式割合
	            CURRFROM__c = '20141201'            	// 設定日
	        ));
        }
        insert esvcpfList;

        // 変額保険ファンドの登録
        List<E_SVFPF__c> esvfpfList = new List<E_SVFPF__c>();
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[0].Id,      // 特別勘定
            ZREVAMT__c = 11100,                 // 積立金
            SVFPFCode__c = 'hfund0001',         // 変額保険ファンドコード
            ZHLDPERC__c = 11.1,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[1].Id,      // 特別勘定
            ZREVAMT__c = 22200,                 // 積立金
            SVFPFCode__c = 'hfund0002',         // 変額保険ファンドコード
            ZHLDPERC__c = 22.2,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[2].Id,      // 特別勘定
            ZREVAMT__c = 33300,                 // 積立金
            SVFPFCode__c = 'hfund0003',         // 変額保険ファンドコード
            ZHLDPERC__c = 33.3,                 // 保有割合
            ZINVPERC__c = 20
        ));
        esvfpfList.add(new E_SVFPF__c(
            E_Policy__c = policy.Id,            // 保険契約ヘッダー
            E_SVCPF__c = esvcpfList[3].Id,      // 特別勘定
            ZREVAMT__c = 44400,                 // 積立金
            SVFPFCode__c = 'hfund0004',         // 変額保険ファンドコード
            ZHLDPERC__c = 60.6,                 // 保有割合
            ZINVPERC__c = 40
        ));
        insert esvfpfList;  
		
		//結果画面
		PageReference pref;
		
		//テストユーザで機能実行開始
		System.runAs(u){

	        pref = Page.E_InwardTransferAgree;

	        //テスト開始
			Test.startTest();
	        Test.setCurrentPage(pref);
	        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
	        E_InwardTransferController controller = new E_InwardTransferController(standardcontroller);
	        E_InwardTransferExtender extender = controller.getExtender();

	        extender.init();
	        for(E_SVFPF__c svfpf : esvfpfList){
	        	controller.E_SVFPF_c_E_Policy_c.add(svfpf);
	        }
			//controller.E_SVFPF_c_E_Policy_c.addAll(esvfpfList);
	        controller.E_SVFPF_c_E_Policy_c.items[0].record.InputZINVPERC__c = '11';
	        controller.E_SVFPF_c_E_Policy_c.items[1].record.InputZINVPERC__c = '29';
	        controller.E_SVFPF_c_E_Policy_c.items[2].record.InputZINVPERC__c = '60';
	        //controller.E_SVFPF_c_E_Policy_c.items[3].record.InputZINVPERC__c = '0';

			extender.PageAction();
	        extender.isValidate();
	        extender.doReturn();
	        extender.doNextEntry();
	        extender.isAgree = true;
	        extender.doNextEntry();
	        extender.doNextConfirm();
	        extender.doCancel();
	        // パスワードの設定
	      	extender.inputPassWord = 'てらすかい';
	      	extender.newPassWord = 'terrask1';
	      	extender.verPassWord = 'terrask1';

	      	E_Email__c eMail = new E_Email__c(NAME = E_Const.EMAIL_CHARGE_TRANSFER,Email__c = 'xxxx@xx.xx');
            insert eMail;
			pref = extender.doConfirmSave();

	        //テスト終了
			Test.stopTest();
		}
		// 正常終了:エラー画面
		system.assertEquals(null, pref);
    }

	/*メッセージマスタを登録
        String errMsgRequiredOld = E_Message.getMsgMap().get('PPC|001');    // 現在の手続サービス用パスワードが未入力
        String errMsgNotSameOld = E_Message.getMsgMap().get('PPC|002');     // 現在の手続サービス用パスワードが正しくない
        String errMsgRequiredNew = E_Message.getMsgMap().get('PPC|003');    // 新しい手続サービス用パスワードが未入力
        String errMsgValidateNew = E_Message.getMsgMap().get('PPC|004');    // 新しい手続サービス用パスワードの入力規則
        String errMsgRequiredVer = E_Message.getMsgMap().get('PPC|005');    // 新規パスワード（確認）が未入力
        String errMsgNotSameVer = E_Message.getMsgMap().get('PPC|006');     // 新規パスワード（確認）が正しくない	
	*/
	private static void createMsgs(){
		List<E_MessageMaster__c> msgs = new List<E_MessageMaster__c>();
		for(Integer i=1;i<=6;i++){
			msgs.add(new E_MessageMaster__c(Type__c = 'メッセージ'
											, Key__c = 'PPC|00' + String.valueOf(i)
											, Name = 'PPC|00' + String.valueOf(i)
											, value__c = 'PPC|00' + String.valueOf(i)
											)
					);
		}
		insert msgs;
	}
}