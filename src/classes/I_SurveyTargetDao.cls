public with sharing class I_SurveyTargetDao {

	/**
	 * ユーザーIdとアンケートIdから対象者取得
	 */
	public static List<I_SurveyTarget__c> getRecByUserIdAndSurveyId(Id userId, Id surveyId){
		List<I_SurveyTarget__c> targetList = new List<I_SurveyTarget__c>();
		targetList = [SELECT id FROM I_SurveyTarget__c WHERE Ownerid =: userId AND Survey__c =: surveyId];
		return targetList;
	}
}