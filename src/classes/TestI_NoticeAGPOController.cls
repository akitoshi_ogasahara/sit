@isTest
private class TestI_NoticeAGPOController {
	
	@isTest static void pageAction_test01() {
		// Data
		createData(5, I_NoticeConst.AGPO_TITLE01);
		// User　ユーザ
		User user = new User();
		user = TestE_TestUtil.createUser(true, 'test', 'システム管理者');
		user = [select usertype from user where Id =: user.Id];
		// E_IDCPF__c ID管理
		E_IDCPF__c idcpf = new E_IDCPF__c(User__c = user.Id, OwnerId = user.Id, ZINQUIRR__c = 'BR**');
		insert idcpf;
		// test
		System.runAs(user){
			Test.startTest();  
			I_NoticeAGPOController controller = new I_NoticeAGPOController();
			controller.filteringType = I_NoticeConst.AGPO_TITLE01;
			controller.pageAction();
			Test.stopTest();
			// assertion
			System.assertEquals(I_NoticeConst.AGPO_TITLE01, controller.filteringType);
			System.assertEquals(false, controller.sortIsAsc);
			System.assertEquals('response', controller.sortType);
			System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO, controller.dlFormNo);
		}
	}

	@isTest static void changeFilter_test01() {
		// Data
		createData(5, I_NoticeConst.AGPO_TITLE02);
		// User　ユーザ
		User user = new User();
		user = TestE_TestUtil.createUser(true, 'test', 'E_EmployeeStandard');
		user = [select usertype from user where Id =: user.Id];
		// E_IDCPF__c ID管理
		E_IDCPF__c idcpf = new E_IDCPF__c(User__c = user.Id, OwnerId = user.Id, ZINQUIRR__c = 'BR**');
		insert idcpf;
		// test
		System.runAs(user){
			Test.startTest();
			I_NoticeAGPOController controller = new I_NoticeAGPOController();
			controller.filteringType = I_NoticeConst.AGPO_TITLE02;
			controller.changeFilteringType();
			Test.stopTest();
			// assertion
			System.assertEquals(I_NoticeConst.AGPO_TITLE02, controller.filteringType);
		}
	}

	@isTest static void getIsViewDl_test01() {
		// User　ユーザ
		User user = new User();
		user = TestE_TestUtil.createUser(true, 'test', 'E_EmployeeStandard');
		user = [select usertype from user where Id =: user.Id];
		// E_IDCPF__c ID管理
		E_IDCPF__c idcpf = new E_IDCPF__c(User__c = user.Id, OwnerId = user.Id, ZINQUIRR__c = 'BR**');
		insert idcpf;
		// test
		System.runAs(user){
			Test.startTest();
			I_NoticeAGPOController controller = new I_NoticeAGPOController();
			controller.pageAction();
			Test.stopTest();
			// assertion
			System.assertEquals(0, controller.dtos.size());
			System.assertEquals(false, controller.getIsViewDl());
		}
	}

	/*山田　修正
     *フィルターモード
     */
    static testMethod void filterMode() {
    	//代理店格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//募集人コードが5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		// 個人ヘッダ
		E_Policy__c policyColi = new E_Policy__c();
		policyColi = TestE_TestUtil.createPolicy(false, agentA.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');
		policyColi.MainAgent__c = agentA.Id;
		insert policyColi;
		
		// SPVAヘッダ
		E_Policy__c policySpva = new E_Policy__c();
		policySpva = TestE_TestUtil.createPolicy(false, agentA.Id, E_Const.POLICY_RECORDTYPE_SPVA, '87654321');
		policySpva.MainAgent__c = agentA.Id;
		insert policySpva;

		//販売取扱者(個人)
		E_CHTPF__c chtpfColi = new E_CHTPF__c(E_Policy__c = policyColi.Id, AGNTNUM__c = agentA.E_CL3PF_AGNTNUM__c);
		insert chtpfColi;
		//販売取扱者（SPVA）
		E_CHTPF__c chtpfSpva = new E_CHTPF__c(E_Policy__c = policySpva.Id, AGNTNUM__c = agentA.E_CL3PF_AGNTNUM__c);
		insert chtpfSpva;
		
		//異動通知
		Date dColi = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
		E_AGPO__c agpoColi = new E_AGPO__c(
			ParentAccount__c = distribute.Id
			, Account__c = office.Id
			, Contact__c = agentA.Id
			, E_Policy__c = policyColi.Id
			, E_CHTPF__c = chtpfColi.Id
			, YYYY__c = dColi.year()
			, MM__c = dColi.month()
			, ZJINTSEQ__c = '1'
			, TITLE__c = I_NoticeConst.AGPO_TITLE01
			);
		insert agpoColi;

		Date dSpva = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
		E_AGPO__c agpoSpva = new E_AGPO__c(
			ParentAccount__c = distribute.Id
			, Account__c = office.Id
			, Contact__c = agentA.Id
			, E_Policy__c = policySpva.Id
			, E_CHTPF__c = chtpfSpva.Id
			, YYYY__c = dSpva.year()
			, MM__c = dSpva.month()
			, ZJINTSEQ__c = '1'
			, TITLE__c = I_NoticeConst.AGPO_TITLE01
			);
		insert agpoSpva;

		//実行ユーザ作成
		User runUs = TestE_TestUtil.createUser(true,'testUser','システム管理者');
		runUs = [select usertype from user where Id =: runUs.Id];
    	//ID管理を付与
		E_IDCPF__c runUsIdMng = TestI_TestUtil.createIDCPF(false, runUs.Id, 'EP');
		runUsIdMng.ZINQUIRR__c = 'BR**';
		runUsIdMng.OwnerId = runUs.Id;
		insert runUsIdMng;

		System.runAs(runUs){
			//=====================テスト開始=====================
	        Test.startTest();

	        I_NoticeAGPOController controller = new I_NoticeAGPOController();
	        E_IRISHandler iris = E_IRISHandler.getInstance();
			//募集人Bで参照
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
			controller.filteringType = I_NoticeConst.AGPO_TITLE01;
	        controller.pageAction();
	        Integer totalRows = controller.rowCnt;
	        system.assertEquals(2,totalRows);
	        //=====================テスト終了=====================
	        Test.stopTest();
	    }
	}

	/** data */
	private static void createData(Integer dataCnt, String title){
		// Account 代理店格
		Account ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
		insert ahAcc;

		// Account 事務所
		Account ayAcc = new Account(
				Name = 'office1'
				,ParentId = ahAcc.Id
				,E_CL2PF_ZAGCYNUM__c = 'ay001'
				,E_COMM_VALIDFLAG__c = '1'
		);
		insert ayAcc;

		// Contact 募集人
		Contact atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
		atCon.E_CL3PF_AGNTNUM__c = 'at001';
		atCon.email = 'fstest@terrasky.ingtesting';
		insert atCon;

		// 個人ヘッダ
		E_Policy__c policyColi = new E_Policy__c();
		policyColi = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');
		// SPVAヘッダ
		E_Policy__c policySpva = new E_Policy__c();
		policySpva = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_SPVA, '87654321');

		//販売取扱者(個人)
		E_CHTPF__c chtpfColi = new E_CHTPF__c(E_Policy__c = policyColi.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
		insert chtpfColi;
		//販売取扱者（SPVA）
		E_CHTPF__c chtpfSpva = new E_CHTPF__c(E_Policy__c = policySpva.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
		insert chtpfSpva;
		
		//異動通知
		List<E_AGPO__c> agpoList = new List<E_AGPO__c>();
		date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
		for(Integer i = 0; i < dataCnt; i++){
			Date d = targetDate.addMonths(i*-1);
			agpoList.add(createAGPO(ahAcc.Id, ayAcc.Id, atCon.Id, policyColi.Id, chtpfColi.Id, d.year(), d.month(), title));
			agpoList.add(createAGPO(ahAcc.Id, ayAcc.Id, atCon.Id, policySpva.Id, chtpfSpva.Id, d.year(), d.month(), title));
		}
		insert agpoList;
	}
	private static void createDataAccessObj(Id userId, String idType){
		createDataAccessObj(userId, idType, 'BR**');
	}
	
	private static void createDataAccessObj(Id userId, String idType, String zinquirr){
		
		// ID管理
		E_IDCPF__c idcpf = new E_IDCPF__c(
			User__c = userId
			,ZIDTYPE__c = idType
			,FLAG01__c = '1'
			,FLAG06__c = '1'
			,ZSTATUS01__c = '1'
			,OwnerId = userId
			,ZINQUIRR__c = zinquirr
		);
		insert idcpf;
	}
	/**
	* 異動通知
	* @param isInsert: whether to insert
	* @param ParentAccountId: 代理店格
	* @param AccountId: 代理店事務所
	* @param ContactId: 募集人
	* @param PolicyId: 保険契約ヘッダ
	* @param chtpfId: 販売取扱者
	* @return E_AGPO__c
	*/  
	public static E_AGPO__c createAGPO(Id ParentAccountId, Id AccountId, Id ContactId, Id PolicyId, Id chtpfId, Decimal YYYY, Decimal MM, String title){
	E_AGPO__c agpo = new E_AGPO__c(
	  ParentAccount__c = ParentAccountId
	  , Account__c = AccountId
	  , Contact__c = ContactId
	  , E_Policy__c = PolicyId
	  , E_CHTPF__c = chtpfId
	  , YYYY__c = YYYY
	  , MM__c = MM
	  , TITLE__c = title
	);
	return agpo;
	}
}