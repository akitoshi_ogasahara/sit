/**
 * MNT_YakkanEditExtenderのテストクラス.
 */
@isTest
private class TestMNT_YakkanEditExtender{
	
	/**
	 * initのテスト.
	 *
	 * 確認事項：子レコードの初期設定が行われないこと
	 */
	@isTest
	private static void myUnitTest01(){
		I_ContentMaster__c content = new I_ContentMaster__c(Name = 'テスト長期障害保険', UpsertKey__c = 'test001');
		insert content;
		
		PageReference pageRef = Page.MNT_YakkanEdit;
		Test.setCurrentPage(pageRef); 
		ApexPages.currentPage().getParameters().put('id', content.Id);
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_YakkanEditController controller = new MNT_YakkanEditController(stdController);
		MNT_YakkanEditExtender extender = new MNT_YakkanEditExtender(controller);
		
		Test.startTest();
		
		extender.init();
		
		Test.stopTest();
		
		Integer size = extender.extension.yakkanFileList.items.size();
		System.assertEquals(0, size);
	}
	
	/**
	 * initのテスト.
	 *
	 * 確認事項：子レコードの初期設定が行われること
	 */
	@isTest
	private static void myUnitTest02(){
		I_ContentMaster__c content = new I_ContentMaster__c();
		
		PageReference pageRef = Page.MNT_YakkanEdit;
		Test.setCurrentPage(pageRef); 
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_YakkanEditController controller = new MNT_YakkanEditController(stdController);
		MNT_YakkanEditExtender extender = new MNT_YakkanEditExtender(controller);
		
		Test.startTest();
		
		extender.init();
		
		Test.stopTest();
		
		Integer size = extender.extension.yakkanFileList.items.size();
		System.assertEquals(3, size);
		
		Set<String> yakkanTypeSet = new Set<String>{'本編', '別添1', '別添2'};
		
		for(MNT_YakkanEditController.yakkanFileListItem item : extender.extension.yakkanFileList.items){
			
			System.assertEquals(true, yakkanTypeSet.contains(item.record.YakkanType__c));
		}
	}
	
	/**
	 * doSaveのテスト.
	 *
	 * 確認事項：保存が成功すること（子レコードなし）
	 */
	@isTest
	private static void myUnitTest03(){
		I_ContentMaster__c content = new I_ContentMaster__c();
		
		PageReference pageRef = Page.MNT_YakkanEdit;
		Test.setCurrentPage(pageRef); 
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_YakkanEditController controller = new MNT_YakkanEditController(stdController);
		MNT_YakkanEditExtender extender = new MNT_YakkanEditExtender(controller);
		
		extender.init();
		
		Test.startTest();
		
		PageReference resPr = extender.doSave();
		
		Test.stopTest();
		
		Integer size = extender.extension.yakkanFileList.items.size();
		System.assertEquals(0, size);
		
		System.assertNotEquals(null, resPr);
	}
	
	/**
	 * doSaveのテスト.
	 *
	 * 確認事項：保存が成功すること（子レコード3件）
	 */
	@isTest
	private static void myUnitTest04(){
		I_ContentMaster__c content = new I_ContentMaster__c();
		
		PageReference pageRef = Page.MNT_YakkanEdit;
		Test.setCurrentPage(pageRef); 
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_YakkanEditController controller = new MNT_YakkanEditController(stdController);
		MNT_YakkanEditExtender extender = new MNT_YakkanEditExtender(controller);
		
		extender.init();
		
		// 約款種別設定
		for(MNT_YakkanEditController.yakkanFileListItem item : extender.extension.yakkanFileList.items) {
			
			item.record.filePath__c = 'PathTest';
		}
		
		Test.startTest();
		
		PageReference resPr = extender.doSave();
		
		Test.stopTest();
		
		Integer size = extender.extension.yakkanFileList.items.size();
		System.assertEquals(3, size);
		
		System.assertNotEquals(null, resPr);
	}
	
	/**
	 * doSaveのテスト.
	 *
	 * 確認事項：約款種別のエラーチェックが走ること
	 */
	@isTest
	private static void myUnitTest05(){
		I_ContentMaster__c content = new I_ContentMaster__c();
		
		PageReference pageRef = Page.MNT_YakkanEdit;
		Test.setCurrentPage(pageRef); 
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_YakkanEditController controller = new MNT_YakkanEditController(stdController);
		MNT_YakkanEditExtender extender = new MNT_YakkanEditExtender(controller);
		
		extender.init();
		
		// 約款種別設定
		for(MNT_YakkanEditController.yakkanFileListItem item : extender.extension.yakkanFileList.items) {
			
			item.record.filePath__c = 'PathTest';
			item.record.YakkanType__c = '本編';
		}
		
		Test.startTest();
		
		PageReference resPr = extender.doSave();
		
		Test.stopTest();
		
		Integer size = extender.extension.yakkanFileList.items.size();
		System.assertEquals(3, size);
		
		System.assertEquals(null, resPr);
	}
	
	/**
	 * doSaveのテスト.
	 *
	 * 確認事項：約款種別のエラーチェックが走ること
	 */
	@isTest
	private static void myUnitTest06(){
		I_ContentMaster__c content = new I_ContentMaster__c();
		
		PageReference pageRef = Page.MNT_YakkanEdit;
		Test.setCurrentPage(pageRef); 
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(content);
		MNT_YakkanEditController controller = new MNT_YakkanEditController(stdController);
		MNT_YakkanEditExtender extender = new MNT_YakkanEditExtender(controller);
		
		extender.init();
		
		for(Integer i = 0; i < 2; i++) {
			
			MNT_YakkanEditController.yakkanFileListItem item = extender.extension.yakkanFileList.items.get(i);
			
			item.remove();
		}
		
		// 約款種別設定
		for(MNT_YakkanEditController.yakkanFileListItem item : extender.extension.yakkanFileList.items) {
			
			item.record.filePath__c = 'PathTest';
			item.record.YakkanType__c = '別添1';
		}
		
		Test.startTest();
		
		PageReference resPr = extender.doSave();
		
		Test.stopTest();
		
		System.assertEquals(null, resPr);
	}
}