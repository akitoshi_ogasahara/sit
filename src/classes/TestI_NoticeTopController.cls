@isTest
private class TestI_NoticeTopController {
	
	/**
	 * レコードカウント取得テスト
	 */
	private static testMethod void getRecordSizeTest_001(){

		Test.startTest();
		//一応通す
		I_NoticeTopController.recordCounterNotice controller = new I_NoticeTopController.recordCounterNotice();
		system.assertEquals(controller.getObjectName(),'');

		I_NoticeTopController.recordCounterNotice ncolController = new I_NoticeTopController.recordCounterNoticeNCOL();
		Integer recCount = 0;
		for(String key :ncolController.getQueryIndicators()){
			recCount += Integer.valueOf(ncolController.getRecordSize(key));		
		}
		system.assertEquals(recCount,0);

		I_NoticeTopController.recordCounterNotice bilsController = new I_NoticeTopController.recordCounterNoticeBILS();
		recCount = 0;
		for(String key :bilsController.getQueryIndicators()){
			recCount += Integer.valueOf(bilsController.getRecordSize(key));		
		}
		system.assertEquals(recCount,0);

		I_NoticeTopController.recordCounterNotice bilaController = new I_NoticeTopController.recordCounterNoticeBILA();
		recCount = 0;
		for(String key :bilaController.getQueryIndicators()){
			recCount += Integer.valueOf(bilaController.getRecordSize(key));		
		}
		system.assertEquals(recCount,0);

		I_NoticeTopController.recordCounterNotice agpoController = new I_NoticeTopController.recordCounterNoticeAGPO();
		recCount = 0;
		for(String key :agpoController.getQueryIndicators()){
			recCount += Integer.valueOf(agpoController.getRecordSize(key));		
		}
		system.assertEquals(recCount,0);

		Test.stopTest();

	}
}