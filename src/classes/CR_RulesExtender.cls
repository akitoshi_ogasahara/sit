public with sharing class CR_RulesExtender extends CR_AbstractExtender{
    
    @TestVisible
    private CR_RulesController controller;
    
    //承認ボタン表示判定用
    private boolean confirming = false;
    //承認取消ボタン表示判定用
    private boolean approved = false;
        
    public CR_RulesExtender(CR_RulesController extension){
        super();
        this.controller = extension;
        this.helpMenukey = CR_Const.MENU_HELP_Rule;
        this.helpPageJumpTo = CR_Const.MENU_HELP_RULE_View;
    }

    public override void init(){
        if(this.controller.record == null 
                || String.isBlank(this.controller.record.AgencyCode__c)){
            isSuccessOfInitValidate = false;
            pageMessages.addErrorMessage(CR_Const.ERR_MSG03_INVALID_URL);       //'URLパラメータが不正です。'
        }else{
            this.viewingAgencyCode = this.controller.record.AgencyCode__c;
            isSuccessOfInitValidate = true;
        }       
    }
    
    public List<CR_Rule__c> getRules(){
        return CR_RuleDao.getRulesWithAttachmentsById(this.controller.record.Id);
    }
        
    /*
     * 承認/承認取消ボタン表示判定
     */
    public boolean getApprovalConfirmation(){
        // 規定ステータスが「02.確認中」且つ、承認ユーザの場合
        if(this.controller.record.Status__c == CR_Const.OUTSRC_STATUS_02a && getHasApprovalPermission()){
            confirming = true;
            return confirming;
        // 規定ステータスが「03.承認済」且つ、承認ユーザの場合
        }else if(this.controller.record.Status__c == CR_Const.OUTSRC_STATUS_03 && getHasApprovalPermission()){
            approved = true;
            return approved;
        }
        return false;
    }
    
    /*
     * 承認ボタンの状況から、表示するボタンの文言を変更する
     */
    public boolean getApprovedStatus(){
        return confirming;
    }   

    /*
     * 承認 or 承認取消ボタン押下時の処理
     */
    public PageReference doApprovalOrCancel(){
        pageMessages.clearMessages();
        CR_Agency__c agny = CR_AgencyDao.getAgencyModStampById(controller.record.Id);
        if(agny.SystemModStamp!=controller.record.SystemModStamp){
            pageMessages.addErrorMessage('当ページ照会時からレコードが更新されています。');
            return refreshPage(false);
        }

        Boolean saveResult = false;
        // 承認ボタン押下時の場合
        if(confirming == true){
            controller.record.ApprovalDate__c = system.now();
            //update controller.record;
            saveResult = E_GenericDao.saveViaPage(controller.record);
        // 承認取消押下時の場合
        }else if(approved == true){
            controller.record.ApprovalDate__c = null;
            //update controller.record;             
            saveResult = E_GenericDao.saveViaPage(controller.record);
        }

        //ページリフレッシュ（WFR結果を反映させる）
        if(saveResult){
            return refreshPage(true);
        }
        return null;
    }
        
    /*
     * ページのリフレッシュ
     */ 
    private PageReference refreshPage(Boolean doRedirect){
        PageReference pr = Page.E_CRRules;
        pr.getParameters().put('id', controller.record.Id);
        pr.setRedirect(doRedirect);
        return pr;
    }
        
        
    /*
     * 規定アップロードボタン押下時
     */ 
    public PageReference toUploadPage(){
        PageReference pr = Page.E_CRRuleSubmit;
        pr.getParameters().put('parentId', controller.record.Id);
        return pr;
    }
}