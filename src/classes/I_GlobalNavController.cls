public with sharing class I_GlobalNavController {
/*
	private Map<String, String> displayNamesByMainCategory = new Map<String, String>{
		I_Const.MENU_MAIN_CATEGORY_NEW_PROPOSAL => I_Const.MENU_DISPLAY_NAME_NEW_PROPOSAL,
		I_Const.MENU_MAIN_CATEGORY_POSSESSION_CONTACT => I_Const.MENU_DISPLAY_NAME_POSSESSION_CONTACT,
		I_Const.MENU_MAIN_CATEGORY_LIBLARY => I_Const.MENU_DISPLAY_NAME_LIBLARY,
		I_Const.MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT => I_Const.MENU_DISPLAY_NAME_AGENCY_MANAGEMENT,
		I_Const.MENU_MAIN_CATEGORY_FAQ => I_Const.MENU_DISPLAY_NAME_CATEGORY_FAQ
	};
*/
	private Map<String, String> iconsByMainCategory = new Map<String, String>{
		I_Const.MENU_MAIN_CATEGORY_NEW_PROPOSAL => I_Const.MENU_ICON_NAME_NEW_PROPOSAL,
		I_Const.MENU_MAIN_CATEGORY_POSSESSION_CONTACT => I_Const.MENU_ICON_NAME_POSSESSION_CONTACT,
		I_Const.MENU_MAIN_CATEGORY_LIBLARY => I_Const.MENU_ICON_NAME_LIBLARY,
		I_Const.MENU_MAIN_CATEGORY_AGENCY_MANAGEMENT => I_Const.MENU_ICON_NAME_AGENCY_MANAGEMENT,
		I_Const.MENU_MAIN_CATEGORY_FAQ => I_Const.MENU_ICON_NAME_CATEGORY_FAQ,
		I_Const.MENU_MAIN_CATEGORY_LOG => I_Const.MENU_ICON_NAME_CATEGORY_LOG
	};

	/**
	 * コンストラクタ
	 */
	public I_GlobalNavController(){}

	//IRIS_Handler
	public E_IRISHandler getIrisHandler(){
		return E_IRISHandler.getInstance();
	}

	//NNLinkへの切り替えURL
	public String getURL_AppSwitch2NNLink(){
		//E_AppSwitch?app=NNLink
		return 'E_AppSwitch?'+I_Const.APPSWITCH_URL_PARAM_APP + '=' + I_Const.APP_MODE_NNLINK;
	}

	/**
	 *		社員判定　Trueの場合SFDCに戻るリンクを表示
	 */
	public Boolean getIsEmployee(){
		return E_AccessController.getInstance().isEmployee();
	}

	public String getPageTitle(){
		return getIrisHandler().irisMenus.activeMenu.Name;
	}

	/**
	 *		IRIS 共同GW判定
	 */
	public Boolean getIsCGW(){
		return E_AccessController.getInstance().isCommonGateway();
	}

	/**
	 *		IRIS ゲストユーザ判定
	 */
	public Boolean getIsGuestUser(){
		return E_AccessController.getInstance().isGuest();
	}

	/**	Filter機能
	 *		IRIS サポート中のAgency/Agent名
	 */
	public String getNameSupported(){
		E_IRISHandler iris = getIrisHandler();

		//ゲストユーザの場合はFilter解除　（Filterのままブラウザ閉じて、ログイン画面に戻るケースなど）
		if(getIsGuestUser()){
			iris.clearPolicyCondition();
			return null;
		}

		//Filter機能可能メニュー以外の場合は解除
		if(iris.irisMenus.activeMenu != null
				//&& iris.irisMenus.activeMenu.MainCategory__c != '2.保有契約'		//保有契約以外でもFilter機能を利用可能とするため
				&& iris.irisMenus.activeMenu.CanFiltered__c == false			//	CanFilteredフラグにて判定
				&& iris.isSetPolicyCondition()){
			iris.clearPolicyCondition();
			return null;
		}

		//IRISHandlerから｜区切りの文字列を取得
		String breadCrumb = getIrisHandler().getPolicyConditionValue(I_Const.JSON_KEY_BREADCRUMB);
		if(String.isNotBlank(breadCrumb)){
			return breadCrumb.replace('|', ' > ');
		}
		return null;
	}

	// Filter機能をOFFとする
/*
	public PageReference FilterOff(){
		getIrisHandler().clearPolicyCondition();
		return null;
	}

	//	 Formタグが必要なときTrue
	public Boolean getRequiredFormTag(){
		String url = ApexPages.currentPage().getUrl();
		return url.containsIgnoreCase('/IRIS');		//IRIS作成ページの場合True
	}
*/
	/**
	 * 画面表示に必要なメニューの情報を返却する。			ライブラリサブナビ情報も合わせて作成
	 * @return List<MenuItem> メニュー情報のリスト
	 */
	private List<gNavItem> globalNavigatorItems;
	public List<gNavItem> getgNavItems() {
		if(globalNavigatorItems==null){
			E_IRISHandler iris = getIrisHandler();
			system.assert(iris.irisMenus != null, 'IRISメニューが取得できませんでした（I_GlobalNavController）');

			Map<String, Map<String,List<I_MenuMaster__c>>> categorys = iris.irisMenus.menusByCategory;
			globalNavigatorItems = new List<gNavItem>();

			// メニューの一覧を作成取得
			for (String categoryName: categorys.keyset()) {
				gNavItem item = new gNavItem();
				//item.name = displayNamesByMainCategory.get(categoryName);
				item.icon = iconsByMainCategory.get(categoryName);

				//ひとつめのサブカテゴリのデフォルトメニューレコードのセット
				for(String subCate:categorys.get(categoryName).keyset()){
					item.record = iris.irisMenus.extractDefaultMenuRecord(categoryName,subCate);
					globalNavigatorItems.add(item);
					if(item.record!=null &&
							item.record.MainCategory__c == I_Const.MENU_MAIN_CATEGORY_FAQ){
						faqNavigatorItem = item;
					}

					//ひとつめのサブカテゴリのみを対象
					break;
				}
			}

		}
		return globalNavigatorItems;
	}

	/**
	 *	よくあるご質問　メニュー
	 */
	private gNavItem faqNavigatorItem; 		//よくあるご質問　グロナビメニュー
	public gNavItem getFAQNavItem(){
		if(faqNavigatorItem==null){
			getgNavItems();
		}
		return faqNavigatorItem;
	}


	//サブカテゴリを持っているか？
	public Boolean getHasSubNav(){
		E_IRISHandler iris = getIrisHandler();
		Map<String,List<I_MenuMaster__c>> subCategorys = iris.irisMenus.menusByCategory.get(E_IrisHandler.getInstance().irisMenus.activeMenu.MainCategory__c);

		//サブカテゴリが2つ以上の時にTrue  Phase1ではライブラリのみサブカテゴリあり
		return subCategorys!=null&&subCategorys.size()>1;
	}


	/*	ライブラリのサブナビ	*/
	public I_MenuProvider.LibSubNavItemsHolder libSubNavItems{get{
																	if(libSubNavItems==null){
																		libSubNavItems = new I_MenuProvider.LibSubNavItemsHolder();
																	}
																	return libSubNavItems;
																}
																private set;
															}

	/**
	 * 画面表示に用いるメニュー情報を保持するクラス
	 *		recordにはカテゴリのTopMenuを設定する。
	 */
	public class gNavItem extends I_MenuItem{
		public String icon {get; set;}
		public Boolean isActive {get{
										return record != null
												&& record.MainCategory__c == E_IrisHandler.getInstance().irisMenus.activeMenu.MainCategory__c;
									}
								set;}

		//Topメニューの取得
		public I_MenuMaster__c getTopMenu(){
			return record;
		}

		public String getCategoryLabel(){
			return record.MainCategoryAlias__c;
		}

		public String getCategoryTopUrl(){
			if(record == null){
				//Topメニューが取得できない場合、エラーとする
				E_PageMessagesHolder.getInstance().addErrorMessage(getCategoryLabel() + 'のカテゴリートップメニューが設定されていません。');
				return null;
			}

			return getDestUrl();
		}

		public Boolean getShowForMainCategory(){
			if(record.MainCategory__c == I_Const.MENU_MAIN_CATEGORY_FAQ){
				return false;		//よくあるご質問は非表示
			}
			if(record.MainCategory__c == '5.よくあるご質問(住友生命)'){
				return false;		//5.よくあるご質問(住友生命)は非表示
			}
			if(!E_AccessController.getInstance().isEmployee() && record.MainCategory__c == I_Const.MENU_MAIN_CATEGORY_LOG){
				return false;		//社員以外はログ管理は非表示
			}

			return true;
		}
	}


	//============	2017.12 たもつくん対応 START	=============//
	public Boolean getViaMobileSDK(){
		//return ApexPages.currentPage().getParameters().get('tmtstyle')=='1';			//ブラウザでたもつくんUI確認する場合は、?tmtstyle=1を追加
		return E_AccessController.getInstance().isViaMobileSDK();
	}
	//============	2017.12 たもつくん対応  END	=============//

	/**
	 *	ページ共有のためのGenerator
			http://irisdev3-nnlife-jp.cs31.force.com/nnlinkage
	 */
	public UrlConverter urlConverter{get{
													if(urlConverter==null){
														urlConverter = new UrlConverter();
													}
													return urlConverter;
												}
												private set;}
	public class UrlConverter{

		public Boolean canShow(){
			return E_Accesscontroller.getInstance().isEmployee();
		}

		//インターネット用　代理店ユーザ　ページURL
		public String getUrl_via_Internet(){
			return E_Util.getUrlForNNLinkage();
		}

		//共同GW用　代理店ユーザ　ページURL
		public String getUrl_via_GW(){
			return E_Util.getURLForGWTB(getUrl_via_Internet());
		}

	}

}