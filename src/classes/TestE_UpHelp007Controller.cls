@isTest
private class TestE_UpHelp007Controller {
	
	/**
	 * メニューを作成して実施
	 */
	private static testMethod void test_MenuExist() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', E_Const.SYSTEM_ADMINISTRATOR);
		
		//想定画面
		PageReference expectedPage;
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			PageReference pref = Page.up007;
			// メニュー情報の登録
			E_MenuMaster__c menu = createMenuMaster(true, 'テストご利用方法メニューマスタ', 'Help_E_UnitPrice','E_UnitPrice', true);
			E_MessageMaster__c message = createMessageMaster(true, 'テストご利用方法メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
			pref.getParameters().put('menuid', menu.Id);
			expectedPage = pref;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			E_UpHelp007Controller controller = new E_UpHelp007Controller();
			controller.init();
			resultPage = ApexPages.currentPage();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(expectedPage, resultPage);
	}

	/**
	 * E_AbstractUPInfoControllerのテスト
	 */
	private static testMethod void test_E_AbstractUPInfoController() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = createUser(true, 'keiyakusha', E_Const.SYSTEM_ADMINISTRATOR);
		
		//想定画面
		PageReference expectedPage;
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			PageReference pref = Page.up007;
			// メニュー情報の登録
			E_MenuMaster__c menu = createMenuMaster(true, 'テストご利用方法メニューマスタ', 'Help_E_UnitPrice','E_UnitPrice', true);
			E_MessageMaster__c message = createMessageMaster(true, 'テストご利用方法メッセージマスタ', menu.Id, 'CMS', 'CMS', '001');
			pref.getParameters().put('menuid', menu.Id);
			expectedPage = pref;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			E_UpHelp007Controller controller = new E_UpHelp007Controller();
			controller.init();
			controller.getCmsContent();
			// menuidに誤ったIDを設定した状態でテスト
			pref = Page.up007;
			pref.getParameters().put('menuid', message.Id);
			Test.setCurrentPage(pref);
			controller.init();
			// menuidを設定しない状態でテスト
			pref = Page.up007;
			Test.setCurrentPage(pref);
			controller.init();
			resultPage = ApexPages.currentPage();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertNotEquals(expectedPage, resultPage);
	}


	
/*********************************************************************************************************************
 * テストデータ作成用
 *********************************************************************************************************************/
    
    /**
	 * Profileマップ取得
	 * @return Map<String, Id>: Map of ProfileName & ProfileID
	 */
	private static Map<String, Id> pMap;
	private static Map<String, Id> getProfileIdMap(){
		if(pMap != null){
			return pMap;
		}
		pMap = new Map<String, Id>();
		for(Profile pr: [select Id, Name From Profile]){
			pMap.put(pr.Name, pr.Id);
		}
		return pMap;
	}
    
    /**
	 * ユーザ作成
	 * @param isInsert: whether to insert
	 * @param LastName: 姓
	 * @param profileDevName: プロファイル名
	 * @return User: ユーザ
	 */
	private static User createUser(Boolean isInsert, String LastName, String profileDevName){
		String userName = LastName + '@terrasky.ingtesting';
		Id profileId = getProfileIdMap().get(profileDevName);
		User src = new User(
				  Lastname = LastName
				, Username = userName
				, Email = userName
				, ProfileId = profileId
				, Alias = LastName.left(8)
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = UserInfo.getLanguage()
		);
		if (isInsert) {
			insert src;
		}
		return src;
	}
	
	/**
	 * RecordTypeマップ取得
	 * @return Map<String, Map<String, Id>>: Map(sObjectType, Map(RecTypeDevName,RecTypeID)
	 */
	private static Map<String, Map<String, Id>> rMap;
	private static Map<String, Map<String, Id>> getRecTypeIdMap(){
		if(rMap != null) return rMap;
		rMap = new Map<String, Map<String, Id>>();
		for(RecordType rt: [select Id, DeveloperName, sObjectType From RecordType]){
			if(rMap.containsKey(rt.sObjectType)){
				rMap.get(rt.sObjectType).put(rt.DeveloperName, rt.Id);
			}else{
				rMap.put(rt.sObjectType, new Map<String, Id>{rt.DeveloperName => rt.Id});
			}
		}
		return rMap;
	}
    
	/**
	 * メニューマスタ作成
	 * @param isInsert: whether to insert
	 * @param name: メニューマスタ名
	 * @param Key: メニューマスタキー
	 * @param recTypeDevName: レコードタイプ名
	 * @param showCloseBtn: 閉じるボタンを表示
	 * @return E_MenuMaster__c: メニューマスタレコード
	 */
	public static E_MenuMaster__c createMenuMaster(Boolean isInsert, String name, String Key, String recTypeDevName, Boolean showCloseBtn) {
		Id recTypeId = getRecTypeIdMap().get('E_MenuMaster__c').get(recTypeDevName);
		E_MenuMaster__c src = new E_MenuMaster__c(
					Name = name,
					RecordTypeId = recTypeId,
					MenuMasterKey__c = Key,
					IsShowCloseBtn__c = showCloseBtn
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
	
	/**
	 * メッセージマスタ作成
	 * @param isInsert: whether to insert
	 * @param name: メッセージマスタ名
	 * @param mId: メニューマスタのレコードID
	 * @param recTypeDevName: レコードタイプ名
	 * @param type: 種別
	 * @param key: Key/並び順
	 * @return E_MessageMaster__c: メッセージマスタレコード
	 */
	public static E_MessageMaster__c createMessageMaster(Boolean isInsert, String name, Id mId, String recTypeDevName, String type, String key) {
		Id recTypeId = getRecTypeIdMap().get('E_MessageMaster__c').get(recTypeDevName);
		E_MessageMaster__c src = new E_MessageMaster__c(
					Name = name,
					RecordTypeId = recTypeId,
					Menu__c = mId,
					Type__c = type,
					Key__c = key
		);
		if(isInsert){
			insert src;
		}
		return src;
	}
	
}