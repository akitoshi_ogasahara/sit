@isTest
public with sharing class TestE_SVFPFDao {

	/**
     * E_SVFPFDao 分岐網羅
     */
    private static testMethod void testE_SVFPFDao() {
        //テストユーザ作成(共通クラスのメソッド呼び出し)
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        
        //結果画面
        PageReference resultPage;
        
        //テストユーザで機能実行開始
        System.runAs(u){
            //テスト開始
            Test.startTest();
            Id dummyId = null;
            set<id> Ids = new set<id>{dummyId};
            list<E_SVFPF__c> srcList1 = E_SVFPFDao.getRecsByPolicyIdForCOLI(dummyId);
            list<E_SVFPF__c> srcList2 = E_SVFPFDao.getRecsByPolicyId(dummyId);
            list<E_SVFPF__c> srcList3 = E_SVFPFDao.getRecsBySVCPFIds(dummyId,Ids);
            list<E_SVFPF__c> srcList4 = E_SVFPFDao.getRecsByPolicyIdForCOLIFrInwardTransfer(dummyId);
            
            System.assertEquals( 0,srcList1.size());
            System.assertEquals( 0,srcList2.size());
            System.assertEquals( 0,srcList3.size());
            System.assertEquals( 0,srcList4.size());

            //テスト終了
            Test.stopTest();
        }
        //※正常処理
        system.assertEquals(null, resultPage);
    }

    /**
     * ユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
}