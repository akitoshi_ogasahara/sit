public with sharing class ASS_MRPageController {

	//代理店事務所一覧
	public List<OfficeListRow> officeRows {get{
		if(officeRows != null){
			return officeRows;
		}
			return getOfficeList();
		} set;
	}

	//編集モード
	public Boolean editMode {get; private set;}
	//MRID
	public String mrId{get; private set;}
	//営業部ID
	public String unitId{get; private set;}
	//年
	public String year {get; private set;}
	//月
	public String month {get; private set;}
	//MRユーザ
	public User mr {get; private set;}

	//事務所IDと見込案件のリストのマップ
	private Map<Id,List<Opportunity>> oppMap;
	//事務所IDと行動のリストのマップ
	private Map<Id,List<Event>> eventMap;
	//事務所IDと営業目標のリストのマップ
	private Map<Id,SalesTarget__c> stMap;

	//FS目標値
	public Decimal target_FS {get; private set;}
	//PT目標値
	public Decimal target_PT {get; private set;}
	//BAFS目標値
	public Decimal target_BAFS {get; private set;}
	//BAPT目標値
	public Decimal target_BAPT {get; private set;}

	//デフォルトのリストの行数
	public Integer listDefCount {get; private set;}
	//表示リスト行数
	public Integer rowCount{get; set;}
	//全件表示ボタン表示フラグ
	public Boolean isShowALLBtn {get; set;}
	//元に戻るボタン表示フラグ
	public Boolean isShowRetBtn {get; set;}
	//事務所リストのサイズ
	public Integer officeListSize {get; private set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}
	//ソートアイコン
	public String sortIcon {get; private set;}
	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	//営業予算項目「BAFS」
	private static final String BUDGET_BAFS 				= 'BA Financial Solutions';
	//営業予算項目「BAPT」
	private static final String BUDGET_BAPT 				= 'BA Protection';
	//営業予算項目「FS」
	private static final String BUDGET_FS 					= 'CategoryⅢ';
	//営業予算項目「PT」
	private static final String BUDGET_PT 					= 'CategoryⅡ';

	//見込案件レコード「予算」
	private static final String RECTYPE_BUDGET 				= 'RecordType2';
	//見込案件レコード「実績」
	private static final String RECTYPE_PERFORM 			= 'RecordType1';
	// URLパラメータ名 - ソート項目
	private static final String URL_PARAM_SORT_TYPE 		= 'st';
	//ソートアイコン昇順
	private static final String ASCCON 						= '↑';
	//ソートアイコン降順
	private static final String DESCICON 					= '↓';

	//ソートキー
	public static final String SORT_TYPE_OFFICENAME 		= 'officename';
	public static final String SORT_TYPE_OFFICEKANANAME 	= 'officekananame';
	public static final String SORT_TYPE_ESTTARGET 			= 'esttarget';
	public static final String SORT_TYPE_ESTPERFORM 		= 'estperform';
	public static final String SORT_TYPE_DESIGNDOCTARGET 	= 'designdoctarget';
	public static final String SORT_TYPE_DESIGNDOCPERFORM 	= 'designdocperform';
	public static final String SORT_TYPE_APETARGET 			= 'apetarget';
	public static final String SORT_TYPE_APEPERFORM 		= 'apeperform';
	public static final String SORT_TYPE_CONTACTTARGET 		= 'contacttarget';
	public static final String SORT_TYPE_CONTACTPERFORM 	= 'contactperform';

	//レコードタイプのマップ
	private Map<String,String> recTypeMap;

	//コンストラクタ
	public ASS_MRPageController() {
		mrId 			= ApexPages.currentPage().getParameters().get('mrId');
		unitId 			= ApexPages.currentPage().getParameters().get('unitId');
		year 			= String.valueOf(System.today().Year());
		month 			= String.valueOf(System.today().Month());
		mr 				= ASS_UserDao.getUserById(mrId);

		target_FS 		= 0;
		target_PT 		= 0;
		target_BAFS 	= 0;
		target_BAPT 	= 0;

		listDefCount 	= 10;
		rowCount	 	= listDefCount;
		isShowALLBtn 	= true;
		isShowRetBtn 	= false;

		//初期ソートを事務所カナ名の昇順に設定
		sortType 		= SORT_TYPE_OFFICEKANANAME;
		sortIsAsc 		= true;
		sortIcon 		= ASCCON;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		recTypeMap = new Map<String,String>();
		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}
	}

	//予算目標取得
	public void getTarget() {
		//不具合２対応
		target_FS 		= 0;
		target_PT 		= 0;
		target_BAFS 	= 0;
		target_BAPT 	= 0;

		//見込案件リスト作成
		String[] level = new String[]{'S:予算'};
		List<Opportunity> oppList = ASS_OppDaoWithout.getThisMonthOppsByOwnerIdAndLevel(mrId,level);
		//見込案件レコードタイプ「予算」のId
		Id budgetId = recTypeMap.get(RECTYPE_BUDGET);

		if(oppList != null) {
			for(Opportunity op :oppList) {
				//レコードタイプが予算の場合
				if(op.RecordTypeId == budgetId){
					//「FS」の場合
					if(op.ProductName_TEXT__c == BUDGET_FS) {
						target_FS += null2Zero(op.WAPE__c);
					//「PT」の場合
					} else if(op.ProductName_TEXT__c == BUDGET_PT) {
						target_PT += null2Zero(op.WAPE__c);
					//「BAFS」の場合
					} else if(op.ProductName_TEXT__c == BUDGET_BAFS) {
						target_BAFS += null2Zero(op.WAPE__c);
					//「BAPT」の場合
					} else if(op.ProductName_TEXT__c == BUDGET_BAPT) {
						target_BAPT += null2Zero(op.WAPE__c);
					}
				}
			}
		}
	}

	public List<OfficeListRow> getOfficeList(){

		//表示するリスト
		officeRows = new List<OfficeListRow>();

		//事務所のマップを作成
		Map<Id,Account> officeMap = new Map<Id,Account>(ASS_AccountDao.getOfficesById(mrId,''));

		//表示するリストのサイズを取得
		officeListSize = officeMap.keySet().size();

		//見込案件対象レベル
		String[] level = new String[]{'S','A','B','C','D'};

		//事務所IDと見込案件のマップを作成
		oppMap = getOppMap(officeMap.keySet());

		//事務所IDと行動のマップを作成
		eventMap = getEventMap(officeMap.keySet());

		//事務所IDと営業目標のマップを作成
		stMap = getStMap(officeMap.keySet());

		//見込案件レコードタイプ「実績」のId
		Id performId = recTypeMap.get(RECTYPE_PERFORM);

		//リスト作成
		for(Account off :officeMap.values()){
			//表示用リストレコード
			OfficeListRow row = new OfficeListRow();

			//事務所ID
			row.officeId = off.Id;
			//事務所名
			row.officeName = off.Name;
			//事務所カナ名
			row.officeKanaName = off.E_CL2PF_ZEAYKNAM__c;

			//見込案件用リスト
			List<Opportunity> opps = new List<Opportunity>();
			opps = oppMap.get(off.Id);

			if(opps != null){
				//事務所ごとのAPE実績を作成
				for(Opportunity op :opps){
					//レコードタイプが実績の場合
					if(op.RecordTypeId == performId){
						//レベルがS~Dの場合WAPEの値を実績として足し加える
						if(level.contains(op.StageName)){
							row.apePerform += null2Zero(op.WAPE__c);
						}
					}
				}
			}

			//行動用リスト
			List<Event> events = new List<Event>();
			events = eventMap.get(off.Id);

			if(events != null){
				//接触回数を作成
				row.contactPerform = events.size();

				for(Event ev :events){
					if(ev.IsOpportunity__c){
						row.estPerform += 1;
					}
					if(ev.DesignDocReq__c){
						row.designDocPerform += 1;
					}
				}
			}

			//営業目標レコード
			SalesTarget__c st = new SalesTarget__c();
			st = stMap.get(off.Id);

			if(st != null){
				row.sTargetId 			= st.Id;
				row.estTarget 			= null2Zero(st.ESTOppTarget__c);
				row.designDocTarget 	= null2Zero(st.DesignDocReqTarget__c);
				row.apeTarget 			= null2Zero(st.APETarget__c);
				row.contactTarget 		= null2Zero(st.NumOfContactsTarget__c);
			}
			//目標の値を作成
			officeRows.add(row);

		}
		//作成したリストのサイズがデフォルトの表示件数より少ない場合は「全件表示」ボタンを非表示にする
		if(officeRows.size() >= rowCount
			&& isShowRetBtn == false){
			isShowALLBtn = true;
		}

		ASS_MRPageController.list_sortType  = SORT_TYPE_OFFICEKANANAME;
		ASS_MRPageController.list_sortIsAsc = true;
		sortType 		= SORT_TYPE_OFFICEKANANAME;
		sortIsAsc 		= true;
		sortIcon = ASCCON;

		officeRows.sort();

		return officeRows;
	}

	//null→0変換
	private Decimal null2Zero(Decimal dec){
		return dec == null ? 0 : dec;
	}

	//ソート
	public PageReference sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
			if(sortIsAsc) {
				sortIcon = ASCCON;
			} else {
				sortIcon = DESCICON;
			}
		}else{
			sortIsAsc = true;
			sortType  = sType;
			sortIcon  = ASCCON;
		}
		if(officeRows == null) {
			getOfficeList();
		}

		ASS_MRPageController.list_sortType  = sortType;
		ASS_MRPageController.list_sortIsAsc = sortIsAsc;

		officeRows.sort();

		return null;
	}

	//全件表示
	public void addRows(){
		isShowALLBtn = false;
		isShowRetBtn = true;
		rowCount 	 = officeRows.size();
	}

	//デフォルト表示
	public void returnRows(){
		isShowALLBtn = true;
		isShowRetBtn = false;
		rowCount 	 = listDefCount;
	}

	//代理店事務所ページへの遷移処理
	public PageReference moveToOfficeSummary() {
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('accountId',ApexPages.currentPage().getParameters().get('accountId'));
		pr.getParameters().put('unitId',unitId);
		pr.getParameters().put('mrId',mrId);
		pr.setRedirect(true);
		return pr;
	}

	//編集
	public PageReference edit() {
		editMode = true;
		return null;
	}

	public PageReference save() {

		List<SalesTarget__c> upsertList = new List<SalesTarget__c>();
		for(OfficeListRow row : officeRows){
			SalesTarget__c newSt = new SalesTarget__c();
			//新規の場合
			if(String.isEmpty(row.sTargetId)) {
				//すべてが値が0の場合はcontinue
				if(row.apeTarget 			== 0
					&& row.contactTarget 	== 0
					&& row.designDocTarget 	== 0
					&& row.estTarget 		== 0) {
					continue;
				} 
				//0以外の値が1つでもあれば新規作成
				//SalesTarget__c newSt = new SalesTarget__c();
				
			//既存の場合
			}else {
				//変更がなければcontinue
				SalesTarget__c currentSt = stMap.get(row.officeId);
				if(row.apeTarget 			== currentSt.APETarget__c 
					&& row.contactTarget 	== currentSt.NumOfContactsTarget__c 
					&& row.designDocTarget 	== currentSt.DesignDocReqTarget__c 
					&& row.estTarget 		== currentSt.ESTOppTarget__c){
					continue;
				}
				//SalesTarget__c newSt = new SalesTarget__c();
				newSt.Id 						= row.sTargetId;
			}
			newSt.APETarget__c 				= row.apeTarget;
			newSt.Account__c 				= row.officeId;
			newSt.DesignDocReqTarget__c 	= row.designDocTarget;
			newSt.ESTOppTarget__c 			= row.estTarget;
			newSt.NumOfContactsTarget__c 	= row.contactTarget;
			newSt.Day__c					= Date.today();

			upsertList.add(newSt);
		}

		try {
			upsert upsertList;
			//不具合3対応
			getOfficeList();
			
		} catch (DMLException e) {

			for (Integer i = 0; i < e.getNumDml(); i++) {
				ApexPages.addMessages(e);
			}
		}

		editMode = false;

		return null;
	}

	//キャンセル
	public void cancel() {
		getOfficeList();
		editMode = false;
	}

	//新規活動登録画面への遷移
	public PageReference newEvent() {
		PageReference pr = Page.ASS_EventInputPage;
		pr.getParameters().put('retPgName','/apex/ASS_MRPage');
		pr.getParameters().put('mrId',mrId);
		pr.getParameters().put('unitId',unitId);
		pr.setRedirect(true);
		return pr;
	}

	//戻る
	public PageReference back(){
		PageReference pr = Page.ASS_MRSummary;
		pr.getParameters().put('unitId',unitId);
		pr.setRedirect(true);
		return pr;
	}

	//事務所IDと案件のリストのマップを作成
	private Map<Id,List<Opportunity>> getOppMap(Set<Id> officeIdSet){

		Map<Id,List<Opportunity>> oppListMap = new Map<Id,List<Opportunity>>();
		//事務所リストに該当する見込案件のリストを取得
		List<Opportunity> oppThisMonthList = ASS_OppDaoWithout.getThisMonthOppsByOffIdSet(officeIdSet);
		if(oppThisMonthList != null){
		//if(!oppThisMonthList.isEmpty()){

			List<Opportunity> oppList = new List<Opportunity>();
			//事務所ごとに分けた見込案件のリストを事務所IDをキーとしてマップにPutする
			for(Opportunity opp :oppThisMonthList){
				if(oppListMap.containsKey(opp.AccountId)){
					oppList = oppListMap.get(opp.AccountId);
				}else{
					oppList = new List<Opportunity>();
				}
				oppList.add(opp);
				oppListMap.put(opp.AccountId,oppList);
			}
		}
		return oppListMap;
	}


	//事務所IDと行動のリストを作成
	private Map<Id,List<Event>> getEventMap(Set<Id> officeIdSet){
		Id recTypeId = ASS_RecTypeDao.getRecTypeByDevName('Event', 'DailyReport').Id;

		Map<Id,List<Event>> eventListMap = new Map<Id,List<Event>>();
		//事務所リストに該当する行動のリストを取得
		List<Event> evnetThisMonthList = ASS_EventDao.getThisMonthEventsByOffIdSet(recTypeId,officeIdSet);
		if(evnetThisMonthList != null){
		//if(!evnetThisMonthList.isEmpty()){

			List<Event> evList = new List<Event>();
			//事務所ごとに分けた行動のリストを事務所IDをキーとしてマップにPutする
			for(Event ev :evnetThisMonthList){
				if(eventListMap.containsKey(ev.WhatId)){
					evList = eventListMap.get(ev.WhatId);
				}else{
					evList = new List<Event>();
				}
				evList.add(ev);
				eventListMap.put(ev.WhatId,evList);
			}
		}
		return eventListMap;
	}

	//事務所IDと営業目標のリストのマップを作成
	private Map<Id,SalesTarget__c> getStMap(Set<Id> officeIdSet){

		Map<Id,SalesTarget__c> sMap = new Map<Id,SalesTarget__c>();
		//事務所リストに該当する営業目標のリストを取得
		List<SalesTarget__c> salesTargetList = ASS_SalesTargetDao.getThisMonthSalesTargetByOffIdSet(officeIdSet);
		if(salesTargetList != null){
		//if(!salesTargetList.isEmpty()){
			for(SalesTarget__c st : salesTargetList){
				sMap.put(st.Account__c, st);
			}
		}
		return sMap;
	}

	//内部クラス定義
	public class OfficeListRow implements Comparable {
		public String officeId {get; set;}          //事務所ID
		public String sTargetId {get; set;}         //営業目標ID

		public String officeName {get; set;}        //事務所名
		public String officeKanaName {get; set;} 	//代理店事務所カナ名
		public Decimal estTarget {get; set;}        //Est案件目標
		public Decimal estPerform {get; set;}       //Est案件実績
		public Decimal designDocTarget {get; set;}  //設計書依頼目標
		public Decimal designDocPerform {get; set;} //設計書依頼実績
		public Decimal apeTarget {get; set;}        //APE目標
		public Decimal apePerform {get; set;}       //APE実績
		public Decimal contactTarget {get; set;}    //接触回数目標
		public Decimal contactPerform {get; set;}   //接触回数実績


		//コンストラクタ
		public OfficeListRow(){
			officeId 			= '';
			sTargetId 			= '';
			officeName 			= '';
			officeKanaName 		= '';
			estTarget 			= 0;
			estPerform 			= 0;
			designDocTarget 	= 0;
			designDocPerform 	= 0;
			apeTarget 			= 0;
			apePerform 			= 0;
			contactTarget 		= 0;
			contactPerform 		= 0;
		}

		// ソート
		public Integer compareTo(Object compareTo) {
			OfficeListRow compareToRow = (OfficeListRow)compareTo;
			Integer result = 0;

			//事務所名
			if (list_sortType == SORT_TYPE_OFFICEKANANAME) {
				result = compareToString(officeKanaName, compareToRow.officeKanaName);
			//Est案件目標
			} else if (list_sortType == SORT_TYPE_ESTTARGET) {
				result = compareToDecimal(estTarget, compareToRow.estTarget);
			//Est案件実績
			} else if (list_sortType == SORT_TYPE_ESTPERFORM) {
				result = compareToDecimal(estPerform, compareToRow.estPerform);
			//設計書依頼目標
			} else if (list_sortType == SORT_TYPE_DESIGNDOCTARGET) {
				result = compareToDecimal(designDocTarget, compareToRow.designDocTarget);
			//設計書依頼実績
			} else if (list_sortType == SORT_TYPE_DESIGNDOCPERFORM) {
				result = compareToDecimal(designDocPerform, compareToRow.designDocPerform);
			//APE目標
			} else if (list_sortType == SORT_TYPE_APETARGET) {
				result = compareToDecimal(apeTarget, compareToRow.apeTarget);
			//APE実績
			} else if (list_sortType == SORT_TYPE_APEPERFORM) {
				result = compareToDecimal(apePerform, compareToRow.apePerform);
			//接触回数目標
			} else if (list_sortType == SORT_TYPE_CONTACTTARGET) {
				result = compareToDecimal(contactTarget, compareToRow.contactTarget);
			//接触回数実績
			} else if (list_sortType == SORT_TYPE_CONTACTPERFORM) {
				result = compareToDecimal(contactPerform, compareToRow.contactPerform);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
		private final Decimal MINUS = -1;
		private Decimal null2Minus(Decimal dec){
			return (dec == null)? MINUS : dec;
		}
		/**
		 * ソート判定（Decimal）
		 * @paarm  Decimal：比較元
		 * @paarm  Decimal：比較先
		 * @return Decimal:比較結果
		 */
		public Integer compareToDecimal(Decimal compare, Decimal compareTo){
			Integer result = 0;
			if(compare==null) return 1;
			if(compareTo==null) return -1;
			if(compare == compareTo){
				result = 0;
			} else if(compare > compareTo){
				result = 1;
			} else {
				result = -1;
			}
			return result;
		}

		/**
		 * ソート判定（文字列）
		 * @paarm  String：比較元
		 * @paarm  String：比較先
		 * @return Integer:比較結果
		 */
		public Integer compareToString(String compare, String compareTo){
			Integer result = 0;
			// null は昇順で一番下
			if(String.isBlank(compare)){
				result = 1;
			} else if(String.isBlank(compareTo)){
				result = -1;
			} else {
				result = compare.compareTo(compareTo);
			}
			return result;
		}
	}
}