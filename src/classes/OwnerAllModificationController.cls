public with sharing class OwnerAllModificationController {

    public List<Opportunity> opps {get; set;}               // 見込案件情報リスト
    public Boolean isOpp {get; set;}                        //見込案件情報存在有無フラグ
    public set<Id> setSelectedItems = new set<Id> ();       //選択IDセット
    public List<OppSelectedInfo> selectedOpps {get; set;}   //選択見込案件リスト
    public Boolean isAllSelected {get; set;}                //全選択のチェックボックスの選択値
    public OwnerModifyBean bean {get; set;}                 //画面ビンクラス
    public String h_Owner_Id {get; set;}                    //所有者のユーザID
    public static final String LEBEL_NOT_SHOW = 'S:予算'; //ランクの非表示項目

    /* コンストラクタ */
    public OwnerAllModificationController() {
        isOpp = false;
        // 画面ビン
        bean = new OwnerModifyBean();
        bean.sch_StageName = new List<String>();
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：見込案件の検索
    * 戻り値              ：
    * 機能概要            ：見込案件の検索
    *------------------------------------------------------------------------
    */
    public Pagereference searchOppInfo() {
        opps = new List<Opportunity>();
        try {
            String query = 'Select o.Id, o.SalesUnit__c, o.Name, o.Account.Name, o.StageName, o.CloseDate From Opportunity o';
            query += ' Where o.IsDeleted = false and o.SalesUnit__c like \'%' + String.escapeSingleQuotes(bean.sch_AgencyName) + '%\' and o.Owner.Name like \'%' + String.escapeSingleQuotes(bean.sch_OwnerId) + '%\'';
            if (bean.sch_From_CloseDate != null && bean.sch_From_CloseDate != '')
                query += ' and o.CloseDate >= ' + String.escapeSingleQuotes(bean.sch_From_CloseDate.replace('/', '-'));
            if (bean.sch_To_CloseDate != null && bean.sch_To_CloseDate != '')
                query += ' and o.CloseDate <= ' + String.escapeSingleQuotes(bean.sch_To_CloseDate.replace('/', '-'));
            String value = '';
            if (bean.sch_StageName != null) {
                Integer i = 0;
                for (String str : bean.sch_StageName) {
                        value += '\'' + str + '\'';
                        if (i <> bean.sch_StageName.size() - 1) value += ',';
                        i++;
                }
            }
            /// November 21,2014: Start
//          if (value != '')
//              query += ' and o.StageName IN (' + String.escapeSingleQuotes(value) + ')';
            if (value != '') {
                value = value.replace('\'', '#');
                query += ' and o.StageName IN (' + String.escapeSingleQuotes(value) + ')';
                query = query.replace('#', '\'');
            }
            /// November 21,2014: Finish
            query += ' order by o.SalesUnit__c asc NULLS LAST, o.Account.Name asc NULLS LAST, o.StageName asc NULLS LAST, o.CloseDate desc NULLS LAST';
            
            for (Opportunity opp : database.query(query)) {
                if(opps.size() >= 200){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, '検索条件で得られる結果が200件を超えています。200件目以降は表示されません。'));
                    break;
                } else {
                    opps.add(opp);
                }
            }
            if (opps.isEmpty()) {
                isOpp = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '該当する見込案件は存在しません。'));
                return null;
            } else {
                isOpp = true;
                isAllSelected = false;
                setSelectedItems.clear();
                getSelectedOpps();
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
        return null;
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：見込案件の所有者変更処理
    * 戻り値              ：
    * 機能概要            ：見込案件の所有者変更処理
    *------------------------------------------------------------------------
    */
    public Pagereference modifyOwnerName() {
        // トランザクション開始
        Savepoint sp = Database.setSavepoint();
        try {
            if (h_Owner_Id == null || h_Owner_Id == '') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '変更する所有者IDが存在しません。もう一度、変更所有者名を指定してください。'));
                return null;
            }
            // 選択見込案件の所有者一括変更
            List<Opportunity> oppList = new List<Opportunity>();
            if (selectedOpps != null && !selectedOpps.isEmpty()) {
                for (OppSelectedInfo selOpp : selectedOpps) {
                    if(selOpp.selected) {
                        selOpp.oppInfo.OwnerId = h_Owner_Id;
                        oppList.add(selOpp.oppInfo);
                    }
                }
            }
            if (!oppList.isEmpty()) update oppList;
            // 更新完了メッセージ出力
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '見込案件を更新しました。'));
            // 初期化処理
            initInfo();
            return null;

        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }

    /*-------------------------------------------------------------------------------------
    * 処理ブロック名      ：初期化処理
    * 戻り値              ：
    * 機能概要            ：Hidden値初期化及び、全てチェックボックスのチェックを外す。
    *-------------------------------------------------------------------------------------
    */
    public void initInfo() {
        h_Owner_Id = '';
        isAllSelected = false;
        if (selectedOpps != null) {
            for (OppSelectedInfo info : selectedOpps) {
               info.selected = false;
            }
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：ランク情報取得
    * 戻り値              ：
    * 機能概要            ：ランク情報取得
    *------------------------------------------------------------------------
    */
    public List<SelectOption> getStageName() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult Fld = Schema.sObjectType.Opportunity.fields.StageName;
        List<Schema.PicklistEntry> LEnt = Fld.getPicklistValues();
        for (Schema.PicklistEntry e : LEnt) {
            if(e.getLabel() != LEBEL_NOT_SHOW) {
                options.add(new SelectOption(e.getLabel(),e.getLabel()));
            }
        }
        return options;
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：見込案件情報の全選択処理
    * 戻り値              ：
    * 機能概要            ：見込案件情報の全選択処理
    *------------------------------------------------------------------------
    */
    public void AllSelected() {
        if (opps != null && opps.size() > 0) {
            selectedOpps.clear();
            for (Opportunity opp : opps) {
                OppSelectedInfo selOpp = new OppSelectedInfo(opp);
                if (isAllSelected) {
                    selOpp.selected = true;
                } else {
                    selOpp.selected = false;
                }
                selectedOpps.add(selOpp);
            }
        }
        setSelectedItems();
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：見込案件の選択IDセット
    * 戻り値              ：
    * 機能概要            ：見込案件の選択IDセット
    *------------------------------------------------------------------------
    */
    public void setSelectedItems() {
        if (selectedOpps != null) {
            for (OppSelectedInfo info : selectedOpps) {
               if (info.selected) {
                    setSelectedItems.add(info.oppInfo.id);
                } else {
                    setSelectedItems.remove(info.oppInfo.id);
                }
            }
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：見込案件の選択情報リスト
    * 戻り値              ：
    * 機能概要            ：見込案件の選択情報リスト
    *------------------------------------------------------------------------
    */
    public List<OppSelectedInfo> getSelectedOpps() {
        selectedOpps = new List<OppSelectedInfo>();
        if (opps != null && opps.size() > 0) {
            for(Opportunity info : opps) {
                OppSelectedInfo selOpp = new OppSelectedInfo(info);
                if (setSelectedItems.contains(info.id)) {
                    selOpp.selected = true;
                }
                selectedOpps.add(selOpp);
            }
        }
        return selectedOpps;
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：選択見込案件クラス
    * 戻り値              ：
    * 機能概要            ：選択見込案件クラス
    *------------------------------------------------------------------------
    */
    public class OppSelectedInfo {
        public Boolean selected {get; set;}             //見込案件選択有無
        public Opportunity oppInfo {get; private set;}  //見込案件

        /* コンストラクタ */
        public OppSelectedInfo(Opportunity opp) {
            this.oppInfo = opp;
        }
    }

    /*------------------------------------------------------------------------
    * 処理ブロック名      ：画面のビンクラス
    * 戻り値              ：
    * 機能概要            ：画面のビンクラス
    *------------------------------------------------------------------------
    */
    public class OwnerModifyBean {
        public String sch_AgencyName {get; set;}        //営業所
        public String sch_OwnerId {get; set;}           //所有者名
        public String sch_From_CloseDate {get; set;}    //検索対象日付（From）
        public String sch_To_CloseDate {get; set;}      //検索対象日付（To）
        public List<String> sch_StageName {get; set;}   //ランク
        public String rlt_mod_OwnerId {get; set;}       //変更所有者名
    }
}