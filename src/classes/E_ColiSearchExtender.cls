global with sharing class E_ColiSearchExtender extends E_AbstractSearchExtender {
    
    private static final String PAGE_TITLE = '契約照会（契約一覧検索）';
    
    public E_ColiSearchController extension;

    public String contactId{get;set;}
    public String disId{get;set;}
    public String offId{get;set;}
    public String ageId{get;set;}

    /** コンストラクタ */
    public E_ColiSearchExtender(E_ColiSearchController extension) {
        super();
        this.extension = extension;
        this.pgTitle = PAGE_TITLE;
        contactId = ApexPages.CurrentPage().getParameters().get('cId');
        disId = ApexPages.currentPage().getParameters().get('disId');
        offId = ApexPages.currentPage().getParameters().get('offId');
        ageId = ApexPages.currentPage().getParameters().get('ageId');
        pageRef = doAuth(E_Const.ID_KIND.CONTACT, contactId);
        if(pageRef == null){
            SkyEditor2.Query tableQuery1 = extension.queryMap.get('dataTable');
            tableQuery1.addWhereIfNotFirst('AND');
            tableQuery1.addWhere('( E_Policy__r.RecordType.DeveloperName = \'ECHDPF\' AND E_Policy__r.Contractor__c = \'' + contactId + '\' )' );
            //tableQuery1.addSort('E_Policy__r.COMM_OCCDATE__c',False,True).addSort('COLI_ZPSTDESC__c',True,True).addSort('E_Policy__r.COMM_CHDRNUM__c',True,True);
            //2015/09/01：E_CustomerInfoExtenderのソートに合わせる形で修正
            if (getIsEmployee()) {
                if (!String.isBlank(disId)) {
                    tableQuery1.addWhereIfNotFirst('AND');
                    tableQuery1.addWhere('( E_Policy__r.MainAgentAccount__r.ParentId = \'' + disId + '\' OR E_Policy__r.SubAgentAccount__r.ParentId = \'' + disId + '\' )' );
                }
                if (!String.isBlank(offId)) {
                    tableQuery1.addWhereIfNotFirst('AND');
                    tableQuery1.addWhere('( E_Policy__r.MainAgentAccount__c = \'' + offId + '\' OR E_Policy__r.SubAgentAccount__c = \'' + offId + '\' )' );
                }
                if (!String.isBlank(ageId)) {
                    tableQuery1.addWhereIfNotFirst('AND');
                    tableQuery1.addWhere('( E_Policy__r.MainAgent__c = \'' + ageId + '\' OR E_Policy__r.SubAgent__c = \'' + ageId + '\' )' );
                }
            }
            tableQuery1.addSort('E_Policy__r.COMM_OCCDATE__c',True,True).addSort('COLI_ZPSTDESC__c',True,True).addSort('E_Policy__r.COMM_CHDRNUM__c',True,True);

            extension.queryMap.put('dataTable',tableQuery1);
        }
    }

    ///** ページアクション */
    public PageReference pageAction(){
        if(pageRef == null){
            extension.doSearch();

            // 証券番号のリンク先設定
            setChdrnumUrl();
        }
        return E_Util.toErrorPage(pageRef, null);
    }

    /** ①半角数字⇒全角数字、②半角記号⇒全角記号、③半角アルファベット⇒全角アルファベット、④半角カナ⇒全角カナ */
    global override void preSearch() {
        if (extension != null) {
            if (extension.NameKana_val.SkyEditor2__Text__c != null) {
                extension.NameKana_val.SkyEditor2__Text__c = E_Util.enKatakanaToEm(E_Util.enAlphabetToEm(E_Util.enSignToEm(E_Util.enNumberToEm(extension.NameKana_val.SkyEditor2__Text__c))));
            }
        }
    }

    /**
     * AfterSearch
     */
    global override void afterSearch(){
        // 証券番号のリンク先設定
        setChdrnumUrl();
    }

    /** 
     * 証券番号のリンク先設定 
     */
    private void setChdrnumUrl(){
        // 証券番号リンクの生成　NNLink使用：E_Coli、IRIS使用：IRIS_Coli 
        Integer recCnt = extension.dataTable.Items.size();
        if(recCnt <= 0){
            return;
        }

        for(Integer i = 0; i < recCnt; i++){
            if(iris.getIsIRIS()){
                extension.dataTable.Items[i].record.COMM_CHDRNUM__c = extension.dataTable.Items[i].record.E_Policy__r.COMM_CHDRNUM_LINK_I__c;
            }else{
                extension.dataTable.Items[i].record.COMM_CHDRNUM__c = extension.dataTable.Items[i].record.E_Policy__r.COMM_CHDRNUM_LINK__c;
            }
        }
    }
}