@isTest
private class TestI_ReissuePasswordController {

	//認証画面_コンストラクタ
	static testMethod void constructorTest01(){

		PageReference pageRef = Page.IRIS_ReissuePassword;
		pageRef.getParameters().put('isPassedDays', '1');
		Test.setCurrentPage(pageRef);

		Test.startTest();
		I_ReissuePasswordController controller = new I_ReissuePasswordController();
		Test.stopTest();

		System.assertEquals(true, controller.isPassedDays);
	}

	//認証画面_ページアクション
	static testMethod void pageActionTest01(){

		I_ReissuePasswordController controller = new I_ReissuePasswordController();

		Test.startTest();
		PageReference result = controller.pageAction();
		Test.stopTest();

		System.assertEquals(null, result);
	}

	//認証画面_[次へ]ボタン
	//正常系
	static testMethod void doReissuePasswordTest01(){

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		//User u = TestE_TestUtil.createUser(false, 'userId', 'E_PartnerCommunity');
		//u.username = 'userId'+System.Label.E_USERNAME_SUFFIX;
		//insert u;

		//テストデータ作成
		createIRISCMS();
		createMessageData('MAB|002','メール文言');
		Contact con = createContactData('1');
		User us = createUser(con.id);
		//User thisUser = runAsUser();
		I_ReissuePasswordController controller = new I_ReissuePasswordController();
		controller.userId = us.Email;

		createIDManage(us.id);
		User thisUser = runAsUser();

		PageReference result;

		System.runAs(thisUser){

			Test.startTest();
			result = controller.doReissuePassword();
			Test.stopTest();
		}

		System.assertEquals(Page.IRIS_ReissuePasswordComp.getUrl(), result.getUrl());
	}


	//認証画面_[次へ]ボタン
	//ユーザID未入力
	static testMethod void doReissuePasswordTest02(){

		//メッセージマスタ作成
		E_MessageMaster__c LGE202 = TestE_TestUtil.createMessageMaster(false, 'パスワードリセット認証画面_ユーザIDを入力していない場合', null, 'MSG_DISCLAIMER', E_Const.E_MEGTYPE_MEG, 'LRE|202');
		LGE202.Value__c = 'パスワードリセット認証画面_ユーザIDを入力していない場合';
		insert LGE202;

		I_ReissuePasswordController controller = new I_ReissuePasswordController();
		controller.userId = '';

		Test.startTest();
		PageReference result = controller.doReissuePassword();
		Test.stopTest();

		System.assertEquals(null, result);
		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(LGE202.Value__c, controller.pageMessages.getErrorMessages()[0].summary);
	}

	//認証画面_[次へ]ボタン
	//ユーザID不正
	static testMethod void doReissuePasswordTest03(){

		//メッセージマスタ作成
		E_MessageMaster__c LGE202 = TestE_TestUtil.createMessageMaster(false, 'パスワードリセット認証画面_入力したユーザIDが不正の場合', null, 'MSG_DISCLAIMER', E_Const.E_MEGTYPE_MEG, 'LGE|001');
		LGE202.Value__c = 'パスワードリセット認証画面_入力したユーザIDが不正の場合';
		insert LGE202;

		I_ReissuePasswordController controller = new I_ReissuePasswordController();
		controller.userId = 'userId00';

		Test.startTest();
		PageReference result = controller.doReissuePassword();
		Test.stopTest();

		System.assertEquals(null, result);
		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(LGE202.Value__c, controller.pageMessages.getErrorMessages()[0].summary);
	}

	//認証画面_[次へ]ボタン
	//catch
/*	static testMethod void doReissuePasswordTest04(){

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User us1 = TestE_TestUtil.createUser(false, 'userId01', 'システム管理者');
		us1.username = 'userId01'+System.Label.E_USERNAME_SUFFIX;
		insert us1;
		User us2 = TestE_TestUtil.createUser(false, 'userId02', 'システム管理者');
		us2.username = 'userId02'+System.Label.E_USERNAME_SUFFIX;
		insert us2;

		PageReference result;
		System.runAs(us1){
			I_ReissuePasswordController controller = new I_ReissuePasswordController();
			controller.userId = 'userId02';

			Test.startTest();
			result = controller.doReissuePassword();
			Test.stopTest();
		}

		System.assertEquals(null,result);
	}
*/
	//認証画面_[次へ]ボタン
	//仮パスフラグがTrue
	static testMethod void doReissuePasswordTest05(){
		//テストデータ作成
		createIRISCMS();
		createMessageData('MAB|002','メール文言');
		Contact con = createContactData('1');
		User us = createUser(con.id);
		us.E_UseTempBasicPW__c = true;
		update us;
		//User thisUser = runAsUser();
		I_ReissuePasswordController controller = new I_ReissuePasswordController();
		controller.userId = us.Email;

		User thisUser = runAsUser();

		PageReference result;

		System.runAs(thisUser){

			Test.startTest();
			result = controller.doReissuePassword();
			Test.stopTest();
		}

		System.assertEquals(Page.IRIS_ReissuePasswordComp.getUrl(), result.getUrl());
	}

	//[戻る]ボタン
	//遷移元画面URLがある場合
	static testMethod void doBackTest01(){

		I_ReissuePasswordController controller = new I_ReissuePasswordController();

		Test.startTest();
		PageReference result = controller.doBack();
		Test.stopTest();

		System.assertEquals(Page.E_Exception.getUrl(), result.getUrl());
	}

	//完了画面_ページアクション
	//認証画面を経由していない場合
	static testMethod void pageAction_CompTest01(){

		I_ReissuePasswordController controller = new I_ReissuePasswordController();

		Test.startTest();
		PageReference result = controller.pageAction_Comp();
		Test.stopTest();

		System.assertEquals(Page.E_Exception.getUrl(), result.getUrl());
	}

	//完了画面_ページアクション
	//認証画面を経由している場合
	static testMethod void pageAction_CompTest02(){

		I_ReissuePasswordController controller = new I_ReissuePasswordController();
		controller.pageAction();

		Test.startTest();
		PageReference result = controller.pageAction_Comp();
		Test.stopTest();

		System.assertEquals(null, result);
	}

	//ID管理
	static E_IDCPF__c createIDManage(Id userId){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.appMode__c = I_Const.APP_MODE_IRIS;
		ic.User__c = userId;
		insert ic;
		return ic;
	}

	//ユーザ作成
	static User createUser(Id id){
		User us = new User();
		us.Username = 'test@test.com' + System.Label.E_USERNAME_SUFFIX;
		us.Alias = 'テスト花子';
		us.Email = 'test@test.com';
		us.EmailEncodingKey = 'UTF-8';
		us.LanguageLocaleKey = 'ja';
		us.LastName = 'テスト';
		us.LocaleSidKey = 'ja_JP';
		Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
		us.ProfileId = pro.Id;
		us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
		us.ContactId = Id;
		us.E_TempBasicPWModifiedDate__c = E_Util.getSysDate(null);
		us.IsActive = true;
		insert us;
		return us;
	}

	//募集人作成
	static Contact createContactData(String flag){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.E_CL3PF_AGNTNUM__c = 'test01';
			con.E_CLTPF_DOB__c = '20161017';
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.E_CL3PF_VALIDFLAG__c = flag;
			con.AccountId = accid;
			con.E_CL3PF_KMOFCODE__c = 'test01';
			con.E_CLTPF_CLNTNUM__c = 'client01';
			con.E_CL3PF_CLNTNUM__c = 'client01';
			insert con;
		}

		return con = [select Name,Id,FirstName,LastName from Contact where E_CL3PF_AGNTNUM__c = 'test01'];
	}

	//親取引先作成
	static Account createParentAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.E_CL1PF_KCHANNEL__c = 'LDA';
		acc.Optout_SelfRegist__c = false;
		insert acc;

		return acc;
	}

	//取引先作成
	static Id createAccount(){
		Account pac = createParentAccount();
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.ZAUTOTRF__c = ' ';
		acc.ParentId = pac.Id;
		insert acc;

		return acc.Id;
	}

	//メッセージマスタ作成
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			createMenuData();
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//メッセージマスタ作成
	static void createIRISCMS(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			I_PageMaster__c pm = createIRISPageData();
			I_ContentMaster__c cms = new I_ContentMaster__c();
			cms.Page__c = pm.Id;
			cms.Content__c = '内容';
			insert cms;
		}
	}

	//メニューマスタ作成
	static void createMenuData(){
		E_MenuMaster__c mn = new E_MenuMaster__c();
		mn.MenuMasterKey__c = 'internet_service_policy_agency';
		insert mn;
	}

	//メニューマスタ作成
	static I_PageMaster__c createIRISPageData(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'SelfRegistConfirm';
		insert pm;
		return pm;
	}

	//System.runAs用ユーザ
	static User runAsUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		return thisUser;
	}
}