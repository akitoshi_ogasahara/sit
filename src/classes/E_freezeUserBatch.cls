/**
 * 最終ログインがN年+M日以上前のユーザを無効にするBatchApex
 * (現時点で1年+90日を想定)
 * IsActive（抽出したユーザの有効）を無効化している
 */
global class E_freezeUserBatch implements Database.Batchable<sObject>,Database.Stateful {

	//ユーザ凍結処理の開始判断
	@TestVisible
	private Boolean freezeUserBatchIsAvailable(){
		//起算日のN年+M日後を算出（凍結開始日）
		Date startDate_Of_Freeze = I_Const.IRIS_GOLIVE_DAY.addYears(Integer.valueOf(System.Label.E_FREEZEUSER_YEARS))
															.addDays(Integer.valueOf(System.Label.E_FREEZEUSER_DAYS));
		//システム日付<凍結開始日の場合、後続の処理をしない
		if(E_Util.System_Today()<startDate_Of_Freeze){
			return false;
		}
		return true;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		//ユーザ凍結バッチ処理実施判定
		if(freezeUserBatchIsAvailable()==false){
			return Database.getQueryLocator([select id from user where id = null]);		//処理不要の為0件のリストを返す
		}

		//現在日時のN年+M日前を算出
		Date userFreezeDate = E_Util.System_Today().addYears(-Integer.valueOf(System.Label.E_FREEZEUSER_YEARS))
													.addDays(-Integer.valueOf(System.Label.E_FREEZEUSER_DAYS));

		//一般代理店ユーザのプロファイルIDのマップを取得
		Map<String,Profile> profileMap = E_ProfileDaoWithout.getRecsByNames(E_Const.PROFILE_NAMES_IA);

		//クエリー用Set
		Set<Id> partnerProfileId = new Set<Id>();

		for(String profileName : E_Const.PROFILE_NAMES){
			if(profileMap.containskey(profileName)){
				partnerProfileId.add(profileMap.get(profileName).Id);
			}
		}

		String soql = 'Select id, LastLoginDate,(Select id,ZSTATUS01__c,ZDSPFLAG02__c FROM E_IDCPFs__r) From User ';
		soql += 'Where isActive = true And ProfileId In :partnerProfileId ';
		soql += 'And (LastLoginDate < :userFreezeDate OR (LastLoginDate = null And CreatedDate < :userFreezeDate)) ';

		// テスト実施時のexecuteBatch複数回呼び出しを回避する
		soql += Test.isRunningTest() ? 'Limit 10' : '';

		System.debug('$$$E_freezeUserBatch.start() soql:' + soql);

		return Database.getQueryLocator(soql);

		/*
		return Database.getQueryLocator([select id,LastLoginDate from user
											where isActive = true
											 and ProfileId in :partnerProfileId
											 //and (LastLoginDate < :userFreezeDate OR ( LastLoginDate = null and LastModifiedDate < :userFreezeDate  )) LastModifiedDate →　CreatedDate　20170221
											 and (LastLoginDate < :userFreezeDate OR ( LastLoginDate = null and CreatedDate < :userFreezeDate  )) //最終ログイン日がuserFreeseDateより前　またはログイン未実施かつ作成日がuserFreeseDateより前
										]);
		*/
	}

	//Execute　　scope.size()==0の場合は呼出しされない
	global void execute(Database.BatchableContext BC, List<User> scope) {
		//無効化対象User
		Set<Id> inActiveUserIds = new Set<Id>();

		//ID適正化 緊急停止などは対象外としたい
		List<User> inActiveUsers = new List<User>();

		for(User usr : scope){
			if(!usr.E_IDCPFs__r.isEmpty() && !(usr.E_IDCPFs__r[0].ZSTATUS01__c == E_Const.PW_STATUS_OK && usr.E_IDCPFs__r[0].ZDSPFLAG02__c == E_Const.ZDSPFLAG02_IRIS)){
				continue;
			}
			//条件に当てはまるユーザを無効にし、リストに追加
			usr.IsActive = false;			//IsActive（抽出したユーザーの有効）を無効化
			//usr.IsPortalEnabled = false;	//ポータルユーザ情報を無効化		ContactIdとのリレーションを切らないためFalseとしない。 20161202 yokoyama　復活の場合があるため
			inActiveUserIds.add(usr.Id);
			inActiveUsers.add(usr);
		}

		SavePoint sp = Database.setSavePoint();
		try{
			E_IDCPFTriggerHandler.isSkipTriggerActions = true;		//EIDCのトリガを実行しない
			UserTriggerHandler.isSkipTriggerSCActions = true;		//自主点検管理責任者項目を更新しない
			//update scope;
			update inActiveUsers;
			E_IDCPFTriggerHandler.isSkipTriggerActions = false;
			UserTriggerHandler.isSkipTriggerSCActions = false;

			//0315対応 ここから
			//EIDR作成処理呼出
			createEidrQueueable createJob = new createEidrQueueable(inActiveUserIds);

			ID jobID = System.enqueueJob(createJob);
			//0315対応　ここまで
		}catch(Exception e){
			Database.rollback(sp);
			E_IDCPFTriggerHandler.isSkipTriggerActions = false;

			//エラーログ
			writeErrorLog('長期未使用ユーザ無効化処理時にエラーが発生しました。', e.getMessage(),e.getStackTraceString());
		}
	}

	global void finish(Database.BatchableContext BC) {

	}

	//エラーログ出力処理
	@future
	private static void writeErrorLog(String summary, String message, String stackTrace){
		//アクセスログ
		E_Log__c errLog = E_LogUtil.createLog();
		errLog.Detail__c = E_LogUtil.getLogDetailString(ApexPages.Severity.ERROR
														, summary + '(' + message + ')'
														, stackTrace);
		insert errLog;
	}

	//内部クラス
	//0315対応 ここから
	@TestVisible
	private class createEidrQueueable implements Queueable {
		//無効化されたユーザのID
		private Set<Id> UserIds;

		@TestVisible
		public createEidrQueueable(Set<Id> Ids) {
			this.UserIds = Ids;
		}

		public void execute(QueueableContext context) {
			createEIDR_for_Delete(UserIds);
		}

		private void createEIDR_for_Delete(Set<Id> inActiveUserIds){
			//無効化するユーザに対して作成するIDリクエストレコードリスト
			List<E_IDRequest__c> idRequestRecs = new List<E_IDRequest__c>();

			//無効化対象ユーザとEIDCの取得
			List<User> targetUsers = E_UserDao.getFreezeUserListByIds(inActiveUserIds);

			//ユーザに対するID管理レコード
			for(User usr:targetUsers){
				for(E_IDCPF__c eidc : usr.E_IDCPFs__r){
					//作成するIDリクエストレコードリストの作成
					E_IDRequest__c idreq = new E_IDRequest__c();
					idreq.E_IDCPF__c = eidc.Id;				//ID管理 ID管理のSalesforceID
					idreq.ZUPDFLAG__c = E_Const.ZUPDFLAG_D;	//リクエスト種別 D固定
					idreq.ZWEBID__c = eidc.ZWEBID__c;		//ID番号 ID管理のID番号
					idreq.ZIDTYPE__c = eidc.ZIDTYPE__c;		//ID種別
					idreq.ZIDOWNER__c = eidc.ZIDOWNER__c;	//所有者コード
					idreq.ZINQUIRR__c = eidc.ZINQUIRR__c;	//照会者コード
					//idreq.ZEMAILAD__c = eidc.ZEMAILAD__c	//E-MAILアドレス
					idRequestRecs.add(idreq);
				}
			}

			//IDリクエストレコード登録
			if(!idRequestRecs.isEmpty()){
				try{
					System.debug('freeze IDRequest : '+ idRequestRecs);
					insert idRequestRecs;
				}catch(Exception e){
					//内部クラスでは名前を変える
					writeErrorLog('長期未使用ユーザ無効化EIDR作成時にエラーが発生しました。', e.getMessage(),e.getStackTraceString());
				}
			}
		}
	}
	//0315対応 ここまで
}