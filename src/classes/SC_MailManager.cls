public without sharing class SC_MailManager {
	/* 送信対象
	 * 		CMDはグループアドレスのため、ワークフローにてメールを送信する
	　*		メール種別を追加するときはCMD用のワークフローを作成することを忘れないようにする
	 */
	// 拠点長
	private static String SEND_MNGR = 'MRManager__c';
	// MR
	private static String SEND_MR = 'OwnerId';
	// 代理店
	private static String SEND_AG = 'AG';
	
    /* メールテンプレートDeveloperName */
    /* ［ステータス変更］ */
    // 未提出→本社確認中（NN）
    public static String MT_STATUS_AGCMD_NN = 'SC_AgCmdToCmd';
    // 未提出→本社確認中（代理店）
    public static String MT_STATUS_AGCMD_AG = 'SC_AgCmdToAg';
    // 本社確認中→未提出
    public static String MT_STATUS_RE_CMDAG = 'SC_Re_CmdAgToAg';
    // 不備対応中　未提出→本社確認中（NN）
    public static String MT_STATUS_RE_AGCMD_NN = 'SC_Re_AgCmdToCmd';
    // 不備対応中　未提出→本社確認中（代理店）
    public static String MT_STATUS_RE_AGCMD_AG = 'SC_Re_AgCmdToAg';
    // 本社確認中→担当MR確認中
    public static String MT_STATUS_RE_CMDMR = 'SC_Re_CmdMrToMr';
    // 担当MR確認中→未提出
    public static String MT_STATUS_RE_MRAG = 'SC_Re_MrAgToAg';
    // 不備対応中　未提出→担当MR確認中（NN）
    public static String MT_STATUS_RE_AGMR_NN = 'SC_Re_AgMrToMr';
    // 不備対応中　未提出→担当MR確認中（代理店）
    public static String MT_STATUS_RE_AGMR_AG = 'SC_Re_AgMrToAg';
    // 不備対応中　担当MR確認中→本社確認
    public static String MT_STATUS_RE_MRCMD_MR = 'SC_Re_MrCmdToCmd';
    // 本社確認中→完了
    public static String MT_STATUS_CMDAG = 'SC_CmdAgToAg';
    /* ［問い合わせ］ */
    // CMD→代理店
    public static String MT_INQUIRY_CMDAG = 'SC_INQUIRY_CMDAG';
    // MR（拠点長）→代理店
    public static String MT_INQUIRY_MRAG = 'SC_INQUIRY_MRAG';
    // 代理店→CMD
    public static String MT_INQUIRY_AGCMD = 'SC_INQUIRY_AGCMD';
    /* ［リマインド］ */
    // 代理店向け＿締切前
    public static String MT_REMIND_BEFORE_AG = 'SC_RemindBeforeLimitAg';
    // 代理店向け＿締切後
    public static String MT_REMIND_AFTER_AG = 'SC_RemindAfterLimitAg';
    // 代理店向け＿不備対応中
    public static String MT_REMIND_DEFECT_AG = 'SC_RemindDefectAg';
    // MR向け＿締切前
    public static String MT_REMIND_BEFORE_MR = 'SC_RemindBeforeLimitMr';
    // MR向け＿締切後
    public static String MT_REMIND_AFTER_MR = 'SC_RemindAfterLimitMr';
    // MR向け＿不備対応中
    public static String MT_REMIND_DEFECT_MR = 'SC_RemindDefectMr';
    
	/* 送信対象の定義 */    
	public static Map<String, List<String>> sendTargetMap = new Map<String, List<String>>{
										// ステータス更新
										MT_STATUS_AGCMD_NN			=> new List<String>{SEND_MNGR, SEND_MR}
										,MT_STATUS_AGCMD_AG			=> new List<String>{SEND_AG}
										,MT_STATUS_RE_CMDAG			=> new List<String>{SEND_MNGR, SEND_MR, SEND_AG}
										,MT_STATUS_RE_AGCMD_NN		=> new List<String>{SEND_MNGR, SEND_MR}
										,MT_STATUS_RE_AGCMD_AG		=> new List<String>{SEND_AG}
										,MT_STATUS_RE_CMDMR			=> new List<String>{SEND_MNGR, SEND_MR}
										,MT_STATUS_RE_MRAG			=> new List<String>{SEND_AG}
										,MT_STATUS_RE_AGMR_NN		=> new List<String>{SEND_MR}
										,MT_STATUS_RE_AGMR_AG		=> new List<String>{SEND_AG}
										,MT_STATUS_RE_MRCMD_MR		=> new List<String>{SEND_MNGR}
										,MT_STATUS_CMDAG			=> new List<String>{SEND_MR, SEND_AG}
										// 問い合わせ
										,MT_INQUIRY_CMDAG => new List<String>{SEND_MNGR, SEND_MR, SEND_AG}
										,MT_INQUIRY_MRAG => new List<String>{SEND_MNGR, SEND_MR, SEND_AG}
										,MT_INQUIRY_AGCMD => new List<String>{SEND_MNGR, SEND_MR}
	};
	
	/* 差込項目リプレイスの定義 */
	public static List<String> replaceList = new List<String>{
										'Id'
										,'FiscalYear__c'
										,'AccZAGCYNUM__c'
										,'AccZEAYNAM__c'
										,'OwnerMRUnitName__c'
										,'DomainInternet__c'
										,'DomainGw__c'
										,'DomainEmployee__c'
	};
	
	public static List<String> replaceListRemind = new List<String>{
										'Id'
										,'FiscalYear__c'
										,'AccZEAYNAM__c'
										,'AccZAGCYNUM__c'
										,'OwnerMRUnitName__c'
										,'DomainInternet__c'
										,'DomainGw__c'
										,'DomainEmployee__c'
	};
	
	/**
	 * ステータス変更、問い合わせメール
	 */
	public static void sendMail(String mailType, SC_Office__c scOffice, List<SC_AgencyManager__c> agManagerList){
		// 対象ユーザ
		Set<Id> targetUserIds = new Set<Id>();
		for(String str : sendTargetMap.get(mailType)){
			// 拠点長
			if(str == SEND_MNGR){
				targetUserIds.add((Id)scOffice.get(SEND_MNGR));
			/* MRはワークフローにてメール送信
			// MR
			}else if(str == SEND_MR){
				targetUserIds.add((Id)scOffice.get(SEND_MR));
			*/
			// 代理店
			}else if(str == SEND_AG){
				for(SC_AgencyManager__c agManager : agManagerList){
					targetUserIds.add(agManager.User__c);
				}
			}
		}
		targetUserIds.remove(null);

		if(!targetUserIds.isEmpty()){
			// SingleEmailMessageList
	    	List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

	    	//送信元アドレス設定
			OrgWideEmailAddress senderAdress = E_OrgWideEmailAddressDao.getOrgWideEmailAddress(SC_Const.EMAIL_NAME_JIKOTENKEN);
	    	
	    	// 対象のメールテンプレート取得
	    	EmailTemplate mailTemplate = E_EmailTemplateDao.getRecByDeveloper(mailType);
	    	
	    	// 差込項目のリプレイス
	    	String subject = replaceTemplate(scOffice, mailTemplate.Subject, replaceList);
	    	String body = replaceTemplate(scOffice, mailTemplate.Body, replaceList); 
	    	
	    	// SingleEmailMessage
	    	for(Id targetUserId : targetUserIds){
		    	Messaging.SingleEmailMessage sem = new Messaging.SingleEmailMessage();

		    	// 送信元アドレス設定
				sem.setOrgWideEmailAddressId(senderAdress.Id);
				// 送信先アドレス設定
				sem.setTargetObjectId(targetUserId);
				// 件名 
				sem.setSubject(subject);
				// 本文
				sem.setPlainTextBody(body);
				// 
				sem.setSaveAsActivity(false);
		    	// 送信メールリストに追加
		    	messages.add(sem);
	    	}
	    	
			// 送信
			Messaging.sendEmail(messages);
		}
	}
	
	/**
	 * 差込項目のリプレイス
	 * @param SC_Office__c
	 * @param String リプレイス対象文字列
	 * @param List<String> リプレイス対象項目リスト
	 * @return String リプレイス後文字列
	 */
	public static String replaceTemplate(SC_Office__c scOffice, String str, List<String> targetColumnList){
    	String replaceStr = str;

		// 自主点検事務所
    	for(String rs : targetColumnList){
    		String beforeStr = '{!SC_Office__c.' + rs + '}';
    		String afterStr = E_Util.null2Blank((String)scOffice.get(rs));
    		replaceStr = replaceStr.replace(beforeStr, afterStr);
    	}

    	return replaceStr;
	}
}