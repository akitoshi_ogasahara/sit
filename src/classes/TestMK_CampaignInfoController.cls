@isTest
private class TestMK_CampaignInfoController {
	private static testMethod void TestgetIncentiveImageUrls_001() {
		MK_CampaignInfoController cntrllr = new MK_CampaignInfoController();
		CampaignManagement__c cm1 = TestMK_TestUtil.createCampaignManagement(true,system.today(),system.today());
		TestMK_TestUtil.createAttachment(true,cm1.id,'01.ファイル1',Blob.valueof('ファイル内容1'));
		TestMK_TestUtil.createAttachment(true,cm1.id,'02.ファイル2',Blob.valueof('ファイル内容2'));
		TestMK_TestUtil.createAttachment(true,cm1.id,'03.ファイル3',Blob.valueof('ファイル内容3'));
		TestMK_TestUtil.createAttachment(true,cm1.id,'04C.ファイル4',Blob.valueof('ファイル内容4'));

		cntrllr.record = cm1;
		cntrllr.contractantFlag = true;

		List<MK_CampaignInfoController.IncentiveImage> results1 = cntrllr.getIncentiveImageUrls();
		system.assertEquals(results1.size(),4);

		cntrllr = new MK_CampaignInfoController();
		cntrllr.record = cm1;
		cntrllr.contractantFlag = false;

		List<MK_CampaignInfoController.IncentiveImage> results2 = cntrllr.getIncentiveImageUrls();
		system.assertEquals(results2.size(),3);

	}
	private static testMethod void TestgetCntstRgltnUrl_001() {
		MK_CampaignInfoController cntrllr = new MK_CampaignInfoController();
		CampaignManagement__c cm1 = TestMK_TestUtil.createCampaignManagement(false,system.today(),system.today());
		cm1.E_MessageMaster_UpsertKey__c = 'Test_MessageMaster_UpsertKey';
		insert cm1;

		cntrllr.record = cm1;
		String result = cntrllr.getCntstRgltnUrl();

		system.assert(String.isBlank(result));
	}
}