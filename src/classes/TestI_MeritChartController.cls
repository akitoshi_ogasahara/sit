@isTest
private class TestI_MeritChartController {
	/*
    * 観点:getMeritChartJson メソッド正常系
    * 条件:画面表示用DTOリストを3年分設定してgetMeritChartJsonを実行
    * 検証:正しいkeyを持ったJson形式が返ってくること
    */
	@isTest static void getMeritChartJsonTest01(){
		I_MeritChartController con = new I_MeritChartController();
		con.meritChartDtoList = TestI_PIPTestUtil.createMeritTableDTO();
		String jsonStr = con.getMeritChartJson();
		Map<String,Object> jsonMap = (Map<String,Object>)JSON.deserializeUntyped(jsonStr);
		System.assert(jsonMap.containsKey('keikanensuList'));
		System.assert(jsonMap.containsKey('ageList'));
		System.assert(jsonMap.containsKey('s'));
		System.assert(jsonMap.containsKey('pRuikei'));
		System.assert(jsonMap.containsKey('simpleHenreiRt'));
		System.assert(jsonMap.containsKey('cv'));
		System.assert(jsonMap.containsKey('jisshitsuFutanGaku'));
		System.assert(jsonMap.containsKey('jisshitsuHenreiRt'));
	}

	/*
    * 観点:getMeritChartJson メソッド異常系
    * 条件:画面表示用DTOリストを設定せずにgetMeritChartJsonを実行
    * 検証:nullが返ってくること
    */
	@isTest static void getMeritChartJsonTest02(){
		I_MeritChartController con = new I_MeritChartController();
		String jsonStr = con.getMeritChartJson();
		System.assert(jsonStr == null);
	}
}