@isTest
private class TestE_DSpvaExtender {
	static I_MenuMaster__c mn;

	/**
	 * 最低限のレコードで実施
	 */
	private static testMethod void testSpva1() {

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			E_Policy__c policy = new E_Policy__c();

			PageReference pref = Page.E_Spva;


			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();
			extender.init();
			resultPage = extender.PageAction();
			extender.getIsDispCancelProcessLink();
			extender.getContractorName();
			extender.getContractorSex();
			extender.getContractorOld();
			extender.getMainAgentName();
			extender.getMainAgentSex();
			extender.getMainAgentOld();
			extender.getIsESWDPF();
			extender.getZWDRBAMT();
			extender.getZWTHDRRN();
			extender.getZWTHDRFN();
			extender.getZWDRNEXT();
			extender.getZWDRAMT();
			extender.getIsFixedAmount();
			extender.getFromType();

			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertNotEquals(null, resultPage);

	}
	/**
	 * ステップアップ情報の登録
	 * 引出割合をハイフンで出力
	 */
	private static testMethod void testSpva2() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.COMM_CRTABLE__c = 'ST';
			policy.SPVA_ZDPTYDSP__c = true;
			insert policy;
			PageReference pref = Page.E_Spva;

			// 契約者情報の登録
			E_CRLPF__c contractor = new E_CRLPF__c();
			contractor.CLRRROLE__c = E_CRLPFDao.ROLE_OW;
			contractor.E_Policy__c = policy.Id;
			insert contractor;
			// 被保険者の登録
			E_CRLPF__c mainAgent = new E_CRLPF__c();
			mainAgent.CLRRROLE__c = E_CRLPFDao.ROLE_LA;
			mainAgent.E_Policy__c = policy.Id;
			insert mainAgent;
			// 引出者の登録
			E_SWDPF__c swdpf = new E_SWDPF__c();
			swdpf.Policy__c = policy.Id;
			swdpf.ZWTHDRRN__c = 0;
			insert swdpf; 


		  E_CRLPF__c crlpf01 = new E_CRLPF__c();
			crlpf01.E_Policy__c = policy.ID;
			crlpf01.ZCLNAME__c ='漢字名';
			crlpf01.CLRRROLE__c = 'BN';
			crlpf01.BNYTYP__c = '01';
			crlpf01.BNYPC__c = 100;
			insert crlpf01;

		   E_CRLPF__c crlpf02 = new E_CRLPF__c();
			crlpf02.E_Policy__c = policy.ID;
			crlpf02.ZCLNAME__c ='漢字名';
			crlpf02.CLRRROLE__c = 'BN';
			crlpf02.BNYTYP__c = '08';
			crlpf02.BNYPC__c = 100;
			insert crlpf02;

		   E_CRLPF__c crlpf03 = new E_CRLPF__c();
			crlpf03.E_Policy__c = policy.ID;
			crlpf03.ZCLNAME__c ='漢字名';
			crlpf03.CLRRROLE__c = 'BN';
			crlpf03.BNYTYP__c = '09';
			crlpf03.BNYPC__c = 100;
			insert crlpf03;

		   E_CRLPF__c crlpf04 = new E_CRLPF__c();
			crlpf04.E_Policy__c = policy.ID;
			crlpf04.ZCLNAME__c ='漢字名';
			crlpf04.CLRRROLE__c = 'BN';
			crlpf04.BNYTYP__c = '11';
			crlpf04.BNYPC__c = 100;
			insert crlpf04;

		   E_CRLPF__c crlpf05 = new E_CRLPF__c();
			crlpf05.E_Policy__c = policy.ID;
			crlpf05.ZCLNAME__c ='漢字名';
			crlpf05.CLRRROLE__c = 'BN';
			crlpf05.BNYTYP__c = '40';
			crlpf05.BNYPC__c = 100;
			insert crlpf05;



			// ステップアップの履歴を最新から最大で6履歴表示
			E_SUPPF__c suppf = new E_SUPPF__c();
			suppf.ZATTMRAT01__c = 10;
			suppf.ZATTMRAT02__c = 15;
			suppf.ZATTMRAT03__c = 20;
			suppf.ZATTMRAT04__c = 25;
			suppf.ZATTMRAT05__c = 30;
			suppf.ZATTMRAT06__c = 35;
			suppf.ZATTMRAT07__c = 40;
			suppf.ZATTMRAT08__c = 45;
			suppf.ZATTMRAT09__c = 50;
			suppf.ZATTMRAT10__c = 55;
			suppf.ZATTMRAT11__c = 60;
			suppf.ZATTMRAT12__c = 65;
			suppf.ZATTMRAT13__c = 70;
			suppf.ZATTMRAT14__c = 75;
			suppf.ZATTMRAT15__c = 80;
			suppf.ZATTMRAT16__c = 85;
			suppf.ZATTMRAT17__c = 90;
			suppf.ZATTMRAT18__c = 95;
			suppf.ZATTMRAT19__c = 100;
			suppf.ZATTMRAT20__c = 105;
			suppf.E_Policy__c = policy.Id;
			insert suppf;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();
			extender.init();
			resultPage = extender.PageAction();
			extender.getIsDispCancelProcessLink();
			extender.getContractorName();
			extender.getContractorSex();
			extender.getContractorOld();
			extender.getMainAgentName();
			extender.getMainAgentSex();
			extender.getMainAgentOld();
			extender.getZWDRBAMT();
			extender.getZWTHDRRN();
			extender.getZWTHDRFN();
			extender.getZWDRNEXT();
			extender.getZWDRAMT();
			extender.getIsFixedAmount();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(null, resultPage);
	}
	/**
	 * ステップアップ情報の登録
	 * 引出割合を出力
	 */
	private static testMethod void testSpva3() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG03__c = '0';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.COMM_CRTABLE__c = 'SE';
			policy.SPVA_ZFIXAF__c = true;
			insert policy;
			PageReference pref = Page.E_Spva;

			// 契約者情報の登録
			E_CRLPF__c contractor = new E_CRLPF__c();
			contractor.CLRRROLE__c = E_CRLPFDao.ROLE_OW;
			contractor.ANBCCD__c = 25;
			contractor.E_Policy__c = policy.Id;
			insert contractor;
			// 被保険者の登録
			E_CRLPF__c mainAgent = new E_CRLPF__c();
			mainAgent.CLRRROLE__c = E_CRLPFDao.ROLE_LA;
			mainAgent.E_Policy__c = policy.Id;
			insert mainAgent;
			// 引出者の登録
			E_SWDPF__c swdpf = new E_SWDPF__c();
			swdpf.Policy__c = policy.Id;
			swdpf.ZWTHDRRN__c = 5;
			insert swdpf; 

		  E_CRLPF__c crlpf01 = new E_CRLPF__c();
			crlpf01.E_Policy__c = policy.ID;
			crlpf01.ZCLNAME__c ='漢字名';
			crlpf01.CLRRROLE__c = 'BN';
			crlpf01.BNYTYP__c = '01';
			crlpf01.BNYPC__c = 100;
			insert crlpf01;

		   E_CRLPF__c crlpf02 = new E_CRLPF__c();
			crlpf02.E_Policy__c = policy.ID;
			crlpf02.ZCLNAME__c ='漢字名';
			crlpf02.CLRRROLE__c = 'BN';
			crlpf02.BNYTYP__c = '08';
			crlpf02.BNYPC__c = 100;
			insert crlpf02;

		   E_CRLPF__c crlpf03 = new E_CRLPF__c();
			crlpf03.E_Policy__c = policy.ID;
			crlpf03.ZCLNAME__c ='漢字名';
			crlpf03.CLRRROLE__c = 'BN';
			crlpf03.BNYTYP__c = '09';
			crlpf03.BNYPC__c = 100;
			insert crlpf03;

		   E_CRLPF__c crlpf04 = new E_CRLPF__c();
			crlpf04.E_Policy__c = policy.ID;
			crlpf04.ZCLNAME__c ='漢字名';
			crlpf04.CLRRROLE__c = 'BN';
			crlpf04.BNYTYP__c = '11';
			crlpf04.BNYPC__c = 100;
			insert crlpf04;

		   E_CRLPF__c crlpf05 = new E_CRLPF__c();
			crlpf05.E_Policy__c = policy.ID;
			crlpf05.ZCLNAME__c ='漢字名';
			crlpf05.CLRRROLE__c = 'BN';
			crlpf05.BNYTYP__c = '40';
			crlpf05.BNYPC__c = 100;
			insert crlpf05;



			// ステップアップの履歴を最新から最大で6履歴表示
			E_SUPPF__c suppf = new E_SUPPF__c();
			suppf.ZATTMRAT01__c = 100;
			suppf.ZATTMRAT02__c = 150;
			suppf.ZATTMRAT03__c = 200;
			suppf.ZATTMRAT04__c = 250;
			suppf.ZATTMRAT05__c = 300;
			suppf.ZATTMRAT06__c = 350;
			suppf.ZATTMRAT07__c = 350;
			suppf.E_Policy__c = policy.Id;
			insert suppf;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();
			extender.init();
			resultPage = extender.PageAction();
			extender.getIsDispCancelProcessLink();
			extender.getContractorName();
			extender.getContractorSex();
			extender.getContractorOld();
			extender.getMainAgentName();
			extender.getMainAgentSex();
			extender.getMainAgentOld();
			extender.getZWDRBAMT();
			extender.getZWTHDRRN();
			extender.getZWTHDRFN();
			extender.getZWDRNEXT();
			extender.getZWDRAMT();
			extender.getIsFixedAmount();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertNotEquals(null, resultPage);
	}
	 /**
	 * pageTitle  CRTABLE2==SR
	 */
	private static testMethod void testSpva4() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG03__c = '0';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.COMM_CRTABLE__c = 'SR';            
			policy.SPVA_ZFIXAF__c = true;
			insert policy;
			PageReference pref = Page.E_Spva;

			// 契約者情報の登録
			E_CRLPF__c contractor = new E_CRLPF__c();
			contractor.CLRRROLE__c = E_CRLPFDao.ROLE_OW;
			contractor.ANBCCD__c = 25;
			contractor.E_Policy__c = policy.Id;
			insert contractor;
			// 被保険者の登録
			E_CRLPF__c mainAgent = new E_CRLPF__c();
			mainAgent.CLRRROLE__c = E_CRLPFDao.ROLE_LA;
			mainAgent.ANBCCD__c = 25;
			mainAgent.E_Policy__c = policy.Id;
			insert mainAgent;

			E_CRLPF__c crlpf01 = new E_CRLPF__c();
			crlpf01.E_Policy__c = policy.ID;
			crlpf01.ZCLNAME__c ='漢字名';
			crlpf01.CLRRROLE__c = 'BN';
			crlpf01.BNYTYP__c = '01';
			crlpf01.BNYPC__c = 100;
			insert crlpf01;

		   E_CRLPF__c crlpf02 = new E_CRLPF__c();
			crlpf02.E_Policy__c = policy.ID;
			crlpf02.ZCLNAME__c ='漢字名';
			crlpf02.CLRRROLE__c = 'BN';
			crlpf02.BNYTYP__c = '08';
			crlpf02.BNYPC__c = 100;
			insert crlpf02;

		   E_CRLPF__c crlpf03 = new E_CRLPF__c();
			crlpf03.E_Policy__c = policy.ID;
			crlpf03.ZCLNAME__c ='漢字名';
			crlpf03.CLRRROLE__c = 'BN';
			crlpf03.BNYTYP__c = '09';
			crlpf03.BNYPC__c = 100;
			insert crlpf03;

		   E_CRLPF__c crlpf04 = new E_CRLPF__c();
			crlpf04.E_Policy__c = policy.ID;
			crlpf04.ZCLNAME__c ='漢字名';
			crlpf04.CLRRROLE__c = 'BN';
			crlpf04.BNYTYP__c = '11';
			crlpf04.BNYPC__c = 100;
			insert crlpf04;

		   E_CRLPF__c crlpf05 = new E_CRLPF__c();
			crlpf05.E_Policy__c = policy.ID;
			crlpf05.ZCLNAME__c ='漢字名';
			crlpf05.CLRRROLE__c = 'BN';
			crlpf05.BNYTYP__c = '40';
			crlpf05.BNYPC__c = 100;
			insert crlpf05;



			// 引出者の登録
			E_SWDPF__c swdpf = new E_SWDPF__c();
			swdpf.Policy__c = policy.Id;
			swdpf.ZWTHDRRN__c = 5;
			insert swdpf; 

			// ステップアップの履歴を最新から最大で6履歴表示
			E_SUPPF__c suppf = new E_SUPPF__c();
			suppf.ZATTMRAT01__c = 100;
			suppf.ZATTMRAT02__c = 150;
			suppf.ZATTMRAT03__c = 200;
			suppf.ZATTMRAT04__c = 250;
			suppf.ZATTMRAT05__c = 300;
			suppf.ZATTMRAT06__c = 350;
			suppf.ZATTMRAT07__c = 350;
			suppf.E_Policy__c = policy.Id;
			insert suppf;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();
			extender.init();
			resultPage = extender.PageAction();
			extender.getIsDispCancelProcessLink();
			extender.getContractorName();
			extender.getContractorSex();
			extender.getContractorOld();
			extender.getMainAgentName();
			extender.getMainAgentSex();
			extender.getMainAgentOld();
			extender.getZWDRBAMT();
			extender.getZWTHDRRN();
			extender.getZWTHDRFN();
			extender.getZWDRNEXT();
			extender.getZWDRAMT();
			extender.getIsFixedAmount();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertNotEquals(null, resultPage);
	}
	 /**
	 * pageTitle  CRTABLE2==SR
	 */
	private static testMethod void testSpva5() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG03__c = '0';
		insert idcpf;
		
		//結果画面
		PageReference resultPage;
		
		//テストユーザで機能実行開始
		System.runAs(u){
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.COMM_CRTABLE__c = 'SR';
			policy.SPVA_ZFIXAF__c = true;
			policy.SPVA_ZDPTYDSP__c = true;
			insert policy;

			E_CRLPF__c crlpf01 = new E_CRLPF__c();
			crlpf01.E_Policy__c = policy.ID;
			crlpf01.ZCLNAME__c ='漢字名';
			crlpf01.CLRRROLE__c = 'BN';
			crlpf01.BNYTYP__c = '01';
			crlpf01.BNYPC__c = 100;
			insert crlpf01;

		   E_CRLPF__c crlpf02 = new E_CRLPF__c();
			crlpf02.E_Policy__c = policy.ID;
			crlpf02.ZCLNAME__c ='漢字名';
			crlpf02.CLRRROLE__c = 'BN';
			crlpf02.BNYTYP__c = '08';
			crlpf02.BNYPC__c = 100;
			insert crlpf02;

		   E_CRLPF__c crlpf03 = new E_CRLPF__c();
			crlpf03.E_Policy__c = policy.ID;
			crlpf03.ZCLNAME__c ='漢字名';
			crlpf03.CLRRROLE__c = 'BN';
			crlpf03.BNYTYP__c = '09';
			crlpf03.BNYPC__c = 100;
			insert crlpf03;

		   E_CRLPF__c crlpf04 = new E_CRLPF__c();
			crlpf04.E_Policy__c = policy.ID;
			crlpf04.ZCLNAME__c ='漢字名';
			crlpf04.CLRRROLE__c = 'BN';
			crlpf04.BNYTYP__c = '11';
			crlpf04.BNYPC__c = 100;
			insert crlpf04;

		   E_CRLPF__c crlpf05 = new E_CRLPF__c();
			crlpf05.E_Policy__c = policy.ID;
			crlpf05.ZCLNAME__c ='漢字名';
			crlpf05.CLRRROLE__c = 'BN';
			crlpf05.BNYTYP__c = '40';
			crlpf05.BNYPC__c = 100;
			insert crlpf05;

			// ステップアップの履歴を最新から最大で6履歴表示
			E_SUPPF__c suppf = new E_SUPPF__c();
			suppf.ZATTMRAT01__c = 10;
			suppf.ZATTMRAT02__c = 15;
			suppf.ZATTMRAT03__c = 20;
			suppf.ZATTMRAT04__c = 25;
			suppf.ZATTMRAT05__c = 0; //フラグを倒したい
			suppf.ZATTMRAT06__c = 35;
			suppf.ZATTMRAT07__c = 40;
			suppf.ZATTMRAT08__c = 45;
			suppf.ZATTMRAT09__c = 50;
			suppf.ZATTMRAT10__c = 55;
			suppf.ZATTMRAT11__c = 60;
			suppf.ZATTMRAT12__c = 65;
			suppf.ZATTMRAT13__c = 70;
			suppf.ZATTMRAT14__c = 75;
			suppf.ZATTMRAT15__c = 80;
			suppf.ZATTMRAT16__c = 85;
			suppf.ZATTMRAT17__c = 90;
			suppf.ZATTMRAT18__c = 95;
			suppf.ZATTMRAT19__c = 100;
			suppf.ZATTMRAT20__c = 105;
			suppf.E_Policy__c = policy.Id;
			insert suppf;

			PageReference pref = Page.E_Spva;


			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();
			extender.init();
			resultPage = extender.PageAction();
			extender.getIsDispCancelProcessLink();
			extender.getContractorName();
			extender.getContractorSex();
			extender.getContractorOld();
			extender.getMainAgentName();
			extender.getMainAgentSex();
			extender.getMainAgentOld();
			extender.getZWDRBAMT();
			extender.getZWTHDRRN();
			extender.getZWTHDRFN();
			extender.getZWDRNEXT();
			extender.getZWDRAMT();
			extender.getIsFixedAmount();
			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertNotEquals(null, resultPage);
	}

	/**
	 * 戻るボタン
	 * 「消滅日：yyyy/MM/dd」テキスト
	 * 「yyyy/MM/dd　時点の契約内容」テキスト
	 */
	private static testMethod void testSpva6() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG03__c = '0';
		insert idcpf;

		//結果
		Boolean dspvaFlg;
		Boolean shiftFlg;
		PageReference resultPage;
		String vanishDate;
		String chdrNum;

		//テストユーザで機能実行開始
		System.runAs(u){
			RecordType rtype = [Select Id From RecordType Where DeveloperName = :E_Const.POLICY_RECORDTYPE_DSPVA Limit 1];
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.RecordTypeId = rtype.Id;
			policy.COMM_CRTABLE__c = 'SR';
			policy.SPVA_ZFIXAF__c = true;
			policy.SPVA_ZDPTYDSP__c = true;
			policy.COMM_ZRSTDESC__c = '一時払定額年金へ移行';
			policy.COMM_CHDRNUM__c = '01234567|8';
			insert policy;

			PageReference pref = Page.E_Spva;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();

			createIRISMenu();
			createIRISPage();
			createLog();
			I_menuItem mnitem = new I_MenuItem(mn);
			extender.iris.irisMenus.menuItemsByKey.put('disappearanceList', mnitem);

			//「SPVA消滅」判定
			dspvaFlg = extender.getIsRecordTypeDSpva();
			//'一時払定額年金へ移行'判定
			shiftFlg = extender.getZrstdesc();
			//戻るボタン
			resultPage = extender.doReturn();
			//消滅日取得
			vanishDate = extender.getDataSyncDateVanishCustomer();
			//証券番号取得
			chdrNum = extender.getCommChdrNum();
			//テスト終了
			Test.stopTest();
		}

		//※正常処理
		system.assertEquals(true, dspvaFlg);
		system.assertEquals(true, shiftFlg);
		system.assertEquals('/apex/iris_vanishpolicy?irismn='+ mn.Id, resultPage.getUrl());
		system.assertEquals(DateTime.now().format('yyyy/MM/dd') +'　時点の契約内容', vanishDate);
		system.assertEquals('01234567', chdrNum);
	}

	 /**
	 * 【解約申込書DL設定】にデータが存在する
	 * 引出情報の取得
	 * COMM_ZRSTDESC__cが"一時払定額年金へ移行"と一致しない
	 * 表示する受取人種別が01,
	 * ステップアップ金額用のディスクレーマー処理フラグを降ろす
	 */
	private static testMethod void testSpva7() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '1';
		idcpf.FLAG02__c = '1';
		idcpf.FLAG03__c = '1';
		insert idcpf;

		//結果画面
		PageReference resultPage;

		//テストユーザで機能実行開始
		System.runAs(u){
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.COMM_CRTABLE__c = 'SR';
			policy.SPVA_ZFIXAF__c = true;
			policy.SPVA_ZDPTYDSP__c = true;
			insert policy;

			// 引出者の登録
			E_SWDPF__c swdpf = new E_SWDPF__c();
			swdpf.Policy__c = policy.Id;
			swdpf.ZWTHDRRN__c = 0;
			insert swdpf;

			// ステップアップの履歴を最新から最大で6履歴表示
			E_SUPPF__c suppf = new E_SUPPF__c();
			suppf.ZATTMRAT01__c = 10;
			suppf.ZATTMRAT02__c = 15;
			suppf.ZATTMRAT03__c = 0;
			//suppf.ZATTMRAT04__c = 25;
			suppf.ZATTMRAT05__c = 30;
			suppf.ZATTMRAT06__c = 35;
			suppf.ZATTMRAT07__c = 0;
			//suppf.ZATTMRAT08__c = 45;
			suppf.ZATTMRAT09__c = 50;
			suppf.ZATTMRAT10__c = 55;
			suppf.ZATTMRAT11__c = 60;
			suppf.ZATTMRAT12__c = 65;
			suppf.ZATTMRAT13__c = 0;
			//suppf.ZATTMRAT14__c = 75;
			suppf.ZATTMRAT15__c = 80;
			suppf.ZATTMRAT16__c = 85;
			suppf.ZATTMRAT17__c = 90;
			suppf.ZATTMRAT18__c = 95;
			suppf.ZATTMRAT19__c = 100;
			suppf.ZATTMRAT20__c = 105;
			suppf.E_Policy__c = policy.Id;
			insert suppf;

			PageReference pref = Page.E_Spva;

			E_CancellFormDownloadControl__c cancelForm = new E_CancellFormDownloadControl__c();
			cancelForm.Account__c = null;
			cancelForm.ProductCode__c = 'SR';
			insert cancelForm;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();
			extender.init();
			resultPage = extender.PageAction();
			extender.getIsDispCancelProcessLink();
			extender.getIsESWDPF();
			extender.getZrstdesc();

			//テスト終了
			Test.stopTest();
		}
		//※正常処理
		system.assertEquals(null, resultPage);
	}
	
	/**
	 * ダウンロードボタン
	 * 「消滅日：yyyy/MM/dd」テキスト
	 * 「yyyy/MM/dd　時点の契約内容」テキスト
	 */
	private static testMethod void testSpva8() {
		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		//ID管理DBにユーザ作成(共通クラスのメソッド呼び出し・第一引数はfalse、第二引数はテストユーザId)
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
		/*■機能別にユーザへの参照項目以外の各項目へ値を設定しinsertを行う
		 *※下記は契約者照会の設定例。機能別の設定値については"ID管理DB_機能別設定値一覧"を参照
		 *※設定値がすべて正しく設定された場合はdoAuth()でtrueがかえります。
		 *※例外パターン(doAuth()がfalse)を行いたい場合は、設定値を不正にして登録するか、登録自体をしないでください。
		 */
		idcpf.FLAG01__c = '0';
		idcpf.FLAG02__c = '0';
		idcpf.FLAG03__c = '0';
		insert idcpf;

		//結果
		Boolean dspvaFlg;
		Boolean shiftFlg;
		PageReference resultPage;
		String vanishDate;
		String chdrNum;

		//テストユーザで機能実行開始
		System.runAs(u){
			RecordType rtype = [Select Id From RecordType Where DeveloperName = :E_Const.POLICY_RECORDTYPE_DSPVA Limit 1];
			// 保険契約ヘッダの登録
			E_Policy__c policy = new E_Policy__c();
			policy.RecordTypeId = rtype.Id;
			policy.COMM_CRTABLE__c = 'SR';
			policy.SPVA_ZFIXAF__c = true;
			policy.SPVA_ZDPTYDSP__c = true;
			policy.COMM_ZRSTDESC__c = '一時払定額年金へ移行';
			policy.COMM_CHDRNUM__c = '01234567|8';
			insert policy;

			PageReference pref = Page.E_Spva;

			//テスト開始
			Test.startTest();
			Test.setCurrentPage(pref);
			Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
			E_DSpvaController controller = new E_DSpvaController(standardcontroller);
			E_DSpvaExtender extender = controller.getExtender();

			createIRISMenu();
			createIRISPage();
			createLog();
			I_menuItem mnitem = new I_MenuItem(mn);
			extender.iris.irisMenus.menuItemsByKey.put('disappearanceList', mnitem);

			//「SPVA消滅」判定
			dspvaFlg = extender.getIsRecordTypeDSpva();
			//'一時払定額年金へ移行'判定
			shiftFlg = extender.getZrstdesc();
			//ダウンロードボタン
			resultPage = extender.getE_Download();
			//テスト終了
			Test.stopTest();
		}

		//※正常処理
		system.assertEquals(true, dspvaFlg);
		system.assertEquals(true, shiftFlg);
		system.assertEquals(Page.E_DownloadPrintSheetsSelect.getUrl(), resultPage.getUrl());
	}


	//irisメニュー
	static void createIRISMenu(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			mn = new I_MenuMaster__c();
			mn.DisplayOrder__c = '1';
			mn.MainCategory__c = '2.保有契約';
			mn.MenuKey__c = 'invalidationList';
			insert mn;
		}
	}

	//IRISページ
	static void createIRISPage(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			I_PageMaster__c pm = new I_PageMaster__c();
			pm.Name = 'テスト';
			pm.Menu__c = mn.Id;
			insert pm;
		}
	}

	//ログ
	static void createLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_BizDataSyncLog__c log = new E_BizDataSyncLog__c(
				InquiryDate__c = Datetime.now().date(),
				DataSyncStartDate__c = DateTime.now().addDays(-1),
				DataSyncEndDate__c = DateTime.now(),
				Kind__c = '1'
			);
			insert log;
		}
	}
}