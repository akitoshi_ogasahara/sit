/**
 * 保全4帳票ダウンロード　共通機能クラス
 */
public class E_DownloadNoticeUtil {
    
    /**
     * オブジェクト名から期間の開始年月の取得
     */
    public static Date getPeriodStartDateByObjApi(String objName){
        String kind = E_DownloadNoticeConst.KIND_MAP.get(objName);
        return E_DownloadNoticeUtil.getPeriodStartDate(kind);
    }
    /**
     * 期間の開始年月の取得
     * 期間の自動化対応
     * @param 
     * @return Date
     */
    public static Date getPeriodStartDate(String kind){
        Date d = E_Util.SYSTEM_TODAY();
        d = Date.newInstance(d.year(), d.month(), 1);
        List<E_BizDataSyncLog__c> syncLog = E_BizDataSyncLogDao.getRecsByKindAndCreatedDate(kind,d);
        // 対象月以降の連携ログが存在する場合は当月より－3ヶ月、存在しない場合は前月より-3ヶ月）
        Integer targetAgo = syncLog.isEmpty() ? -1 : 0;
        return E_Util.SYSTEM_TODAY().addMonths(targetAgo);
    }

    /**
     * 期間選択リスト生成
     * @param
     * @return List<SelectOption>
     */
    public static List<SelectOption> createPeriodOptions(String strPeriodDay){
        return createPeriodOptions(strPeriodDay, true);
    }
    public static List<SelectOption> createPeriodOptions(String strPeriodDay, Boolean isAsc){
        List<SelectOption> options = new List<SelectOption>();
        
        // 対象月判定（E_DLN_PERIOD_DAY_帳票に設定されている日以降の場合は当月より－3ヶ月、以前の場合は前月より-3ヶ月）
        Date d = E_DownloadNoticeUtil.getPeriodStartDate(strPeriodDay);
        
        String target = E_Util.getFormatDateStr(d, 'yyyy/MM');
        String beforeOne = E_Util.getFormatDateStr(d.addMonths(-1), 'yyyy/MM');
        String beforeTwo = E_Util.getFormatDateStr(d.addMonths(-2), 'yyyy/MM');
        if(isAsc == true){
            options.add(new SelectOption(target + ',' + beforeOne + ',' + beforeTwo, beforeTwo + ' ~ ' + target));
            options.add(new SelectOption(beforeTwo, beforeTwo));
            options.add(new SelectOption(beforeOne, beforeOne));
            options.add(new SelectOption(target, target));  
        }else{
            options.add(new SelectOption(target, target));
            options.add(new SelectOption(beforeOne, beforeOne));
            options.add(new SelectOption(beforeTwo, beforeTwo));
            options.add(new SelectOption(target + ',' + beforeOne + ',' + beforeTwo, beforeTwo + ' ~ ' + target));
        }
        return options;
    }
    /**
   * 1ヶ月取得
   * @param 
   * 
   */
  public static Set<String> getPeriodSet(String strPeriodDay) {
    Set<String> periodSet = new Set<String>();
    Date d = E_DownloadNoticeUtil.getPeriodStartDate(strPeriodDay);
    
    periodSet.add(E_Util.getFormatDateStr(d, 'yyyy/MM'));

    return periodSet;
  }

  /**
   * 3ヶ月取得
   * @param 
   * 
   */
  public static Set<String> getPeriod3monthSet(String strPeriodDay) {
    Set<String> periodSet = new Set<String>();
    Date d = E_DownloadNoticeUtil.getPeriodStartDate(strPeriodDay);
    
    periodSet.add(E_Util.getFormatDateStr(d, 'yyyy/MM'));
    periodSet.add(E_Util.getFormatDateStr(d.addMonths(-1), 'yyyy/MM'));
    periodSet.add(E_Util.getFormatDateStr(d.addMonths(-2), 'yyyy/MM'));

    return periodSet;
  }
  
  /**
   * 1ヶ月取得 異動通知
   */
  public static Set<String> getPeriodSetAGPO() {
    return E_DownloadNoticeUtil.getPeriodSet(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO);
  }  
  
  /**
   * 1ヶ月取得 請求予告通知
   */
  public static Set<String> getPeriodSetBILA() {
    return E_DownloadNoticeUtil.getPeriodSet(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA);
  }  
  
  /**
   * 1ヶ月取得 請求通知
   */
  public static Set<String> getPeriodSetBILS() {
    return E_DownloadNoticeUtil.getPeriodSet(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS);
  }  

  /**
   * 1ヶ月取得 未入金通知
   */
    public static Set<String> getPeriodSetNCOL() {
        return E_DownloadNoticeUtil.getPeriodSet(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
    }   
    
    /**
     * ダウンロード履歴の帳票種別から、対象通知の期間判定日取得
     */ 
    public static String getTargetPeriodDay(String formNo){
        String pd = '';
        
        // 異動通知
        if(formNo == E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO){
            pd = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_AGPO;
        // 請求予告通知
        }else if(formNo == E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILA){
            pd = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILA;
        // 請求通知
        }else if(formNo == E_DownloadNoticeConst.DH_FORMNO_NOTICE_BILS){
            pd = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_BILS;
        // 未入金通知
        }else if(formNo == E_DownloadNoticeConst.DH_FORMNO_NOTICE_NCOL){
            pd = E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL;
        }
        return pd;
    }
    
    /**
     * 対象データ存在チェック
     * @param String 取得対象オブジェクトのAPI参照名
     * @param String 期間（3ヶ月分）
     * @param Account 代理店格
     * @param Account 代理店事務所
     * @param Contact 募集人
     * @return Boolean 対象データ存在チェック結果                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
     */
    public static Boolean hasDownloadDatas(String objApi, String periodOptions, Account distributor, Account office, Contact agent){
        // 期間の分割
        List<String> periods = periodOptions.split(',');
        
        String soql = ' Select';
        soql += ' Id';
        soql += ' From';
        soql += ' ' + objApi;
        soql += ' Where';
        
        // 保険契約ヘッダ
        soql += ' E_Policy__r.recordTypeId = \'' + E_RecordTypeDao.getEpolicyRecordTypeID(E_Const.POLICY_RECORDTYPE_COLI) + '\' ';
        soql += ' And';
        
        // 期間
        soql += ' YYYYMM__c In :periods';
        soql += ' And';
        
        String recordId;
        // 代理店
        if(distributor != null){
            recordId = distributor.id;
            soql += ' ParentAccount__c = :recordId';
        // 代理店事務所
        }else if(office != null){
            recordId = office.id;
            soql += ' Account__c = :recordId';
        // 募集人
        }else if(agent != null){
            recordId = agent.id;
            soql += ' Contact__c = :recordId';
        }
        
        soql += ' Limit 1';

        List<SObject> result = Database.query(soql);

        return result.isEmpty() == false;
    }
    
    /**
     * ダウンロード履歴作成
     * @param controller : E_DownloadController
     * @param selOfficeList : 選択された事務所一覧
     * @param periodValue : 選択された期間
     * @param isCsv : csvの場合true pdfの場合false
     */
    public static E_DownloadController createDlHistory(E_DownloadController controller ,String[] selOfficeList ,String periodValue ,Boolean isCsv){
        return createDlHistory(controller ,selOfficeList ,periodValue ,isCsv , '');
    }
    public static E_DownloadController createDlHistory(E_DownloadController controller ,String[] selOfficeList ,String periodValue ,Boolean isCsv, String csvType){
        try{
            controller.pageMessages.clearMessages();
            E_DownloadCondition.NoticeDownloadCondi dlCondi= new E_DownloadCondition.NoticeDownloadCondi();
  
            // CSV種別
            dlCondi.csvType = csvType;

            if(controller.distributor <> null){
                //事務所未選択時はエラーを返す
                if(selOfficeList.isEmpty()){
                    controller.pageMessages.addErrorMessage(controller.getMSG().get('DND|003'));
                    return controller;
                }
                dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
                dlCondi.distributorCode = controller.distributor.Id;
                dlCondi.ZHEADAY = controller.distributor.E_CL1PF_ZHEADAY__c;
                dlCondi.ZHEADNAME = controller.distributor.Name;
                dlCondi.officeCodes = selOfficeList;
            }else if(controller.office <> null){
                dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AY;
                dlCondi.distributorCode = controller.office.ParentId;
                dlCondi.ZHEADAY = controller.office.E_ParentZHEADAY__c;
                dlCondi.ZHEADNAME = controller.office.E_ParentName__c;
                dlCondi.officeCodes = new List<String>{controller.office.Id};
            }else{
                dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AT;
                dlCondi.distributorCode = controller.agent.Account.ParentId;
                dlCondi.ZHEADAY = controller.agent.E_CL3PF_ZHEADAY__c;
                dlCondi.ZHEADNAME = controller.agent.E_AccParentName__c;
                dlCondi.officeCodes = new List<String>{controller.agent.AccountId};
                //メインアカウントの取得
                Contact tmp_con = E_ContactDaoWithout.getRecByAgentNumber(controller.agent.E_CL3PF_AGNTNUM__c.Left(5));
                //Contact tmp_con = E_ContactDao.getRecByAgentNumber(controller.agent.E_CL3PF_AGNTNUM__c.Left(5));
                if(tmp_con == null){
                    controller.pageMessages.addErrorMessage(controller.getMSG().get('DND|004'));
                    return controller;
                }
                dlcondi.agentCode = tmp_con.Id;
            }

            List<String> periodList = periodValue.split(',');
            //単月指定
            if(periodList.size() == 1){
                dlCondi.fromyyyyMM = periodValue;
                dlCondi.toyyyyMM = periodValue;
            }
            //複数月指定
            else{
                dlCondi.fromyyyyMM = periodList.get(2);
                dlCondi.toyyyyMM = periodList.get(0);
            }
            //レコードタイプMap作成
            Map<String, Id> recTypes = E_RecordTypeDao.getRecordTypeBySobjectType('E_Policy__c');

            dlCondi.policyTypes = new List<String>{(String)recTypes.get(E_Const.POLICY_RECORDTYPE_COLI)};
            //異動契約はSPVAと消滅契約も対象とする
            if(controller.formNo == E_DownloadNoticeConst.DH_FORMNO_NOTICE_AGPO){
                dlCondi.policyTypes.add((String)recTypes.get(E_Const.POLICY_RECORDTYPE_DCOLI));
                dlCondi.policyTypes.add((String)recTypes.get(E_Const.POLICY_RECORDTYPE_SPVA));
                dlCondi.policyTypes.add((String)recTypes.get(E_Const.POLICY_RECORDTYPE_DSPVA));
            }
                
            controller.dh = new E_DownloadHistorry__c();

            controller.dh.FormNo__c = controller.formNo;
            controller.dh.Conditions__c = dlCondi.toJSON();
            controller.dh.outputType__c = isCsv ? E_Const.DH_OUTPUTTYPE_CSV : E_Const.DH_OUTPUTTYPE_PDF;

            insert controller.dh;

        }catch(Exception e){
            controller.pageMessages.addErrorMessage(e.getMessage());
        }
        return controller;
    }

    /**
     *  単月指定チェック
     * @param E_DownloadController
     * @param String 選択された期間
     * @return E_DownloadController
     */
    public static void checkPeriodPDF(E_DownloadController controller ,String periodValue){
        // 期間の値取得
        List<String> periods = periodValue.split(',');
        
        // 複数期間の場合にエラー
        if(periods.size() > 1){
            controller.pageMessages.addErrorMessage(controller.getMSG().get('DND|005'));
        }
        
        //return controller;
     }
     
    public static void setRelateToE_CHTPF (List<SObject> newList) {
        E_DownloadNoticeUtil.setRelateToE_CHTPF (newList,new Map<Id,SObject>());
    }

    /**
     * 販売取扱者へのリレーション作成
     * 使用する際は、『契約ヘッダリレーション項目』、『募集人番号』、『証券番号』のApi名をそろえること
     */
    public static void setRelateToE_CHTPF (List<SObject> newList,Map<Id,SObject> oldMap) {
        //保険契約ヘッダID => 募集人コード => 販売取扱者ID
        Map<Id,Map<String,id>> chtpfMap = new Map<Id,Map<String,id>>();
        for(SObject newRec : newList) {
            //変更がない場合は対象外とする
            if(!E_DownloadNoticeUtil.isTarget(newRec,oldMap.get(newRec.id))) continue;
            chtpfMap.put((Id)newRec.get('E_Policy__c'),new Map<String,id>());
        }
        if(chtpfMap.isEmpty()) return;
        for(E_CHTPF__c rec: E_CHTPFDao.getRecsByPlcyIds(chtpfMap.keySet())) {
            if(chtpfMap.containsKey(rec.E_Policy__c)){
                chtpfMap.get(rec.E_Policy__c).put(getSEQStr(rec.ZATSEQNO__c),rec.id);
            }else{
                chtpfMap.put(rec.E_Policy__c,new Map<String,id>{getSEQStr(rec.ZATSEQNO__c) => rec.id});
            }
        }
        for(SObject newRec : newList) {
            if(!E_DownloadNoticeUtil.isTarget(newRec,oldMap.get(newRec.id))) continue;
            newRec.put('E_CHTPF__c',chtpfMap.get((id)newRec.get('E_Policy__c')).get((String)newRec.get('ZJINTSEQ__c')));
        }
    }
    
    /**
     * 販売取扱者へのリレーション作成対象チェック
     * リレーション作成済 かつ 契約ヘッダリレーション、募集人番号に変更のない更新は対象外とする
     */
    private static Boolean isTarget (SObject newRec,SObject oldRec){
        //insertは対象とする
        if(oldRec == null) return true;
        //リレーション作成済 かつ 契約ヘッダリレーション、募集人番号に変更のない更新は対象外とする
        if(newRec.get('E_CHTPF__c') <> null
        && newRec.get('E_Policy__c') == oldRec.get('E_Policy__c')
        && newRec.get('ZJINTSEQ__c') == oldRec.get('ZJINTSEQ__c')) return false;
        return true;
    }
    /**
     * SEQコードの置換
     */
    private static String getSEQStr (Decimal seq) {
        if(seq == null) return '';
        return String.valueOf(seq).substringBefore('.');
    }
}