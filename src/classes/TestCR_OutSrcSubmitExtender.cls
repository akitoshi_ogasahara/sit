@isTest
private class TestCR_OutSrcSubmitExtender {

	// コンストラクタテスト１
	private static testMethod void CROutSrcSubmitTest001(){
		CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc();
		
		Test.startTest();
			PageReference pref = Page.E_CROutsrcSubmit;
        	Test.setCurrentPage(pref);
        	
        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
			CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
        	
        	System.assertEquals(target.helpMenukey, CR_Const.MENU_HELP_OUTSRC);
        	System.assertEquals(target.helpPageJumpTo, CR_Const.MENU_HELP_OUTSRC_Submit);
        	System.assertEquals(target.uploadedfiles.size(), 0);
        Test.stopTest(); 
	}

  	// initテスト (外部委託参照権限なしエラー) 
	private static testMethod void CROutSrcSubmitTest002(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createPermissions(usr.Id);
        
        system.runAs(usr){
	        Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
	        	PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest();
        }
	}

  	// initテスト２ (レコードId、またはレコード自体が存在しない場合)
	private static testMethod void CROutSrcSubmitTest003(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
        
        system.runAs(usr){
        	Test.startTest();
				Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
				CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
	        	PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	target.controller.record.Id = null;
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, false);
	        Test.stopTest(); 	
        } 
	}

	// initテスト３
	private static testMethod void CROutSrcSubmitTest004(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	
	        	PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	target.init();
	        	
	        	System.assertEquals(target.isSuccessOfInitValidate, true);
	        Test.stopTest();
		}
	}

	// 添付ファイル有無判定
	private static testMethod void CROutSrcSubmitTest005(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	Attachment file = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	
	        	PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	
	        	System.assertEquals(target.getExistsUploadedFiles(), true);
	        Test.stopTest();
		}
	}

	// 添付ファイルの中身チェック
	private static testMethod void CROutSrcSubmitTest006(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){	
	        Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	Attachment file = New Attachment();
	        	
	        	PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	
	        	System.assertEquals(target.fileIsNotAttached(), true);
	        Test.stopTest();
		}
	}

	// 外部委託先参照ページ遷移
	private static testMethod void CROutSrcSubmitTest007(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);
		
		system.runAs(usr){			
			Test.startTest();
				CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true);
				CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
				PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	
	        	System.assertEquals(target.getUrlforView(), '/apex/e_croutsrcview?id=' + outsrc.Id);
	        Test.stopTest();
		} 
	}

	// 保存ボタン押下(ファイルがないためエラー)
	private static testMethod void CROutSrcSubmitTest008(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);		
		
		system.runAs(usr){
			Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
	        	Attachment uploadedFile = TestCR_TestUtil.createAttachment(true, outsrc.Id);
	        	Attachment newFile = New Attachment();
				
				PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	target.file = newFile;
	        	PageReference result = target.doSaveEx();
	        	
	        	System.assertEquals(result, null);
	        Test.stopTest();
		}
	}

	// 保存ボタン押下(添付不可理由表示時のエラー)
	private static testMethod void CROutSrcSubmitTest009(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);		
		
		system.runAs(usr){
			Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrc(true, agny.Id);
				
				PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	PageReference result = target.doSaveEx();
	        	
	        	System.assertEquals(result, null);
	        Test.stopTest();
		}
	}

	// 保存ボタン押下(添付不可理由をクリア兼、ファイル添付)
	private static testMethod void CROutSrcSubmitTest010(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);		
		
		system.runAs(usr){
			Test.startTest();
	        	Account acc = TestCR_TestUtil.createAccount(true);
	        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
	        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcReason(true, agny.Id);
				
				PageReference pref = Page.E_CROutsrcSubmit;
	        	Test.setCurrentPage(pref);
	        	
	        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
				CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
	        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
	        	Blob body = Blob.valueOf('test.jpg');
	        	Attachment src = new Attachment(Name = 'File001', Body = body);
	        	target.file = src;
	        	PageReference result = target.doSaveEx();
	        	
	        	System.assertEquals(result.getUrl(), '/apex/e_croutsrcview?id=' + outsrc.Id);
	        Test.stopTest();
		}
	}
		
	// 続けてアップロードボタン押下
	private static testMethod void CROutSrcSubmitTest011(){
        User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
        TestCR_TestUtil.createOutSrcPermissions(usr.Id);		
		
		system.runAs(usr){
			Test.startTest();
		        	Account acc = TestCR_TestUtil.createAccount(true);
		        	CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc.Id, CR_Const.RECTYPE_DEVNAME_OUTSRC);
		        	CR_Outsrc__c outsrc = TestCR_TestUtil.createCROutsrcReason(true, agny.Id);
					
					PageReference pref = Page.E_CROutsrcSubmit;
		        	Test.setCurrentPage(pref);
		        	
		        	Apexpages.StandardController standardcontroller = new Apexpages.StandardController(outsrc);
					CR_OutSrcSubmitController controller = new CR_OutSrcSubmitController(standardcontroller);
		        	CR_OutSrcSubmitExtender target = new CR_OutSrcSubmitExtender(controller);
		        	Blob body = Blob.valueOf('test.jpg');
		        	Attachment src = new Attachment(Name = 'File001', Body = body);
		        	target.file = src;
		        	PageReference result = target.doSaveAndNew();
	        	
	        	System.assertEquals(result.getUrl(), '/apex/e_croutsrcsubmit?id=' + outsrc.Id);
	        Test.stopTest();
		}
	}
}