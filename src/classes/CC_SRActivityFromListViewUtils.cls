/**
 * terrasky 2018/05/01
 * MR => SR => 募集人の順にサマリーされたMRID => SRListのMapを作成
 */
public with sharing class CC_SRActivityFromListViewUtils {

	public Map<ID,MRSection> mrMap;
	private Map<String,SRSection> srMap;
	private Map<String,AgentSection> agtMap;
	private Set<String> polSet;
	private Map<String, CC_BranchConfig__c> branchMap;	// 20181017 add


	public CC_SRActivityFromListViewUtils() {
		mrMap = new Map<ID,MRSection>();
		srMap = new Map<String,SRSection>();
		polSet = new Set<String>();
		agtMap = new Map<String,AgentSection>();
		branchMap = CC_BranchConfig__c.getAll();	// 20181017 add
	}

	/**
	 * SR契約連結レコードから行を作成
	 * 契約1～4の主従両方に対して実施
	 * @param srPolicy
	 */
	public void setRowBySRPolicy(CC_SRPolicy__c srPolicy,CC_SRActivity__c srActive,String branchNo,Boolean isHead){
		if(String.isNotBlank(srPolicy.CC_PolicyID1__r.MainAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID1__r.MainAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID1__r.MainAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID1__r,
							srPolicy.CC_PolicyID1__r.MainAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID1__r.SubAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID1__r.SubAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID1__r.SubAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID1__r,
							srPolicy.CC_PolicyID1__r.SubAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID2__r.MainAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID2__r.MainAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID2__r.MainAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID2__r,
							srPolicy.CC_PolicyID2__r.MainAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID2__r.SubAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID2__r.SubAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID2__r.SubAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID2__r,
							srPolicy.CC_PolicyID2__r.SubAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID3__r.MainAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID3__r.MainAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID3__r.MainAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID3__r,
							srPolicy.CC_PolicyID3__r.MainAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID3__r.SubAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID3__r.SubAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID3__r.SubAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID3__r,
							srPolicy.CC_PolicyID3__r.SubAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID4__r.MainAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID4__r.MainAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID4__r.MainAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID4__r,
							srPolicy.CC_PolicyID4__r.MainAgent__r);
		}
		if(String.isNotBlank(srPolicy.CC_PolicyID4__r.SubAgent__r.Account.OwnerId)
				&& isTarget(srPolicy.CC_PolicyID4__r.SubAgent__r.Account,branchNo,isHead)){
			setRow(srPolicy.CC_PolicyID4__r.SubAgent__r.Account.Owner,
							srPolicy.CC_CaseId__r,
							srActive,
							srPolicy.CC_PolicyID4__r,
							srPolicy.CC_PolicyID4__r.SubAgent__r);
		}
	}
	/**
	 * 対象かを判定
	 * @param office: 事務所（Account）
	 * @param　branchNo: 対象の支社コード
	 * @param　isHead: 本社営業部の場合はTrue
	 * @return　result
	 */
	private Boolean isTarget(Account office,String branchNo,Boolean isHead){
		Boolean result = false;
		if(isHead){
			//本社営業部の場合
			result = office.E_CL2PF_BRANCH__c == branchNo
						|| office.KSECTION__c == E_Const.AGENCY_KSECTION_HEAD_OFFICE;
		}else{
			result = office.E_CL2PF_BRANCH__c == branchNo
						&& office.KSECTION__c != E_Const.AGENCY_KSECTION_HEAD_OFFICE;
		}
		return result;
	}

	/**
	 * MR => SR => 募集人リスト作成
	 * @param mr: MR（User）
	 * @param　sr: 問い合わせ情報
	 * @param　policy: 契約情報
	 * @param　agent:　募集人
	 */
	private void setRow(User mr ,Case sr ,CC_SRActivity__c srActive , E_Policy__c policy ,Contact agent){
		if(policy.id == null){
			//契約にアクセス権がない場合は対象外とする
			return;
		}
		//MRセクション
		MRSection mrSec;
		//SRセクション
		SRSection srSec;
		//募集人セクション
		AgentSection agtSec;
		//支社コード
		String branchNo = agent.Account.E_CL2PF_BRANCH__c;

		//MR単位で追加
		if(!mrMap.containskey(mr.Id)){
			//初回は値を追加
			mrSec = new MRSection();
			mrSec.mrId = mr.Id;
			mrSec.mrName = mr.Name;
			mrSec.branchName = getBranchName(branchNo);	// 20181017 add
			mrMap.put(mr.Id,mrSec);
			mrSec.srList = new List<SRSection>();
		}else{
			mrSec = mrMap.get(mr.Id);
		}
		//SR単位で追加
		String srKey = mr.Id + '-' + srActive.Id;
		if(!srMap.containskey(srKey)){
			//初回は値を追加
			srSec = new SRSection();
			srSec.srKey = srKey;
			srSec.record = sr;
			srSec.active = srActive;
			srSec.isCheck = srSec.record.CC_MRCheckBox__c;
			srSec.officeName = agent.Account.Name;
			srSec.agentName = agent.Name;
			srSec.policy = policy;
			srSec.polCount = 0;
			srSec.agtList = new List<AgentSection>();
			srMap.put(srKey,srSec);
			mrSec.srList.add(srSec);
		}else{
			srSec = srMap.get(srKey);
			//証券番号が若い場合には表示内容を差し替える
			if(I_Util.compareToString(srSec.policy.COMM_CHDRNUM__c,policy.COMM_CHDRNUM__c) > 0){
				srSec.officeName = agent.Account.Name;
				srSec.agentName = agent.Name;
				srSec.policy = policy;
			}
		}
		//募集人単位で追加
		String agtKey = srKey + '-' + agent.Id;
		String polKey = agtKey + '-' + policy.Id;
		if(!polSet.contains(polKey)){
			srSec.polCount ++;
			polSet.add(polKey);
			if(!agtMap.containskey(agtKey)){
				//初回は値を追加
				agtSec = new AgentSection();
				agtSec.agtKey = agtKey;
				agtSec.officeName = agent.Account.Name;
				agtSec.agentName = agent.Name;
				agtSec.policyList = new List<E_Policy__c>();
				agtMap.put(agtKey,agtSec);
				srSec.agtList.add(agtSec);
			}else{
				agtSec = agtMap.get(agtKey);
			}
			agtSec.policyList.add(policy);
		}
	}

	private String getBranchName(String branchNo) {
		String ret = '';

		for (CC_BranchConfig__c branch : branchMap.values()) {
			if (branch.CC_BranchCode__c == branchNo) {
				ret = branch.CC_BranchName__c;

				break;
			}
		}

		return ret;
	}

//内部クラス
	public class MRSection implements Comparable {
		public String mrName {get; set;}
		public String mrId {get; set;}

		public String branchName {get; set;}	// 20181017 add

		public List<SRSection> srList {get; set;}

		public Integer compareTo(Object compareTo){
			srList.sort();
			MRSection compareToMRSection = (MRSection)compareTo;
			Integer result = 0;
			result = I_Util.compareToString(mrName,compareToMRSection.mrName);
			return result;
		}
	}
	public class SRSection implements Comparable {
		public Boolean isCheck;
		public case record {get; set;}
		public String getReceptiDateTimeStr(){
			if(record.CMN_ReceptionDateTime__c == null){
				return '';
			}
			return record.CMN_ReceptionDateTime__c.format('yyyy/MM/dd HH:mm:ss','JST');
		}
		public String srKey {get; set;}
		public String officeName {get; set;}
		public String agentName {get; set;}
		public E_Policy__c policy {get; set;}
		public CC_SRActivity__c active;
		public Integer polCount {get; set;}

		public transient List<AgentSection> agtList {get; set;}

		public Integer compareTo(Object compareTo){
			agtList.sort();
			SRSection compareToSRSection = (SRSection)compareTo;
			Integer result = 0;
			result = I_Util.compareToString(active.CC_MRBatchKey__c,compareToSRSection.active.CC_MRBatchKey__c)*-1;
			if(result == 0){
				result = I_Util.compareToString(record.CaseNumber,compareToSRSection.record.CaseNumber);
			}
			return result;
		}
	}
	public class AgentSection implements Comparable {
		public String agtKey {get; set;}
		public String officeName {get; set;}
		public String agentName {get; set;}

		public List<E_Policy__c> policyList {get; set;}

		public Integer compareTo(Object compareTo){
			AgentSection compareToSRSection = (AgentSection)compareTo;
			Integer result = 0;
			result = I_Util.compareToString(officeName,compareToSRSection.officeName);
			if(result == 0){
				result = I_Util.compareToString(agentName,compareToSRSection.agentName);
			}
			return result;
		}
	}
}