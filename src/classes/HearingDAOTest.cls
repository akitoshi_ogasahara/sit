/**
* クラス名		:  HearingDAOTest
* クラス概要	:  クラウドシステム定数クラス(テスト)
* @created	:  2014/01/24 Hung Nguyen The
* @modified	:   
*/
@isTest
private class HearingDAOTest {
	/**
	* getFinanceById
	* ファイナンス情報を取得(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodGetFinanceById(){
		// ファイナンス情報を作成
		Finance__c finance = new Finance__c();
		finance = createFinance(1);
		Finance__c newFinance = new Finance__c();
		// テスト実行
		Test.startTest();
		// ファイナンス情報を取得
		newFinance = HearingDAO.getFinanceById(finance.Id);
		Test.stopTest();
		System.assertEquals(newFinance.H_RD__c, finance.H_RD__c);
		System.assertEquals(newFinance.H_JASGD__c, finance.H_JASGD__c);
		System.assertEquals(newFinance.H_LO__c, finance.H_LO__c);
		System.assertEquals(newFinance.H_DBAC__c, finance.H_DBAC__c);
		System.assertEquals(newFinance.H_UG__c, finance.H_UG__c);
		System.assertEquals(newFinance.H_DBAI__c, finance.H_DBAI__c);
		System.assertEquals(newFinance.H_BRM__c, finance.H_BRM__c);
		System.assertEquals(newFinance.H_BRP__c, finance.H_BRP__c);
		System.assertEquals(newFinance.H_BFLEM__c, finance.H_BFLEM__c);
		System.assertEquals(newFinance.H_BFLET__c, finance.H_BFLET__c);
		System.assertEquals(newFinance.H_RE__c, finance.H_RE__c);
		System.assertEquals(newFinance.H_CRAA__c, finance.H_CRAA__c);
		System.assertEquals(newFinance.H_ERB__c, finance.H_ERB__c);
		System.assertEquals(newFinance.H_ORB__c, finance.H_ORB__c);
	}
	/**
	* getFinanceByIdNull
	* ファイナンス情報を取得(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodGetFinanceByIdNull(){
		// ファイナンス情報を作成
		Finance__c newFinance = new Finance__c();
		// テスト実行
		Test.startTest();
		// ファイナンス情報を取得
		newFinance = HearingDAO.getFinanceById('a0N00000001');
		Test.stopTest();
		System.assertEquals(null, newFinance);
	}
	/**
	* testMethodGetCustomerById
	* 顧客を取得(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodGetCustomerById(){
		// ファイナンス情報を作成
		Customer__c customer = new Customer__c();
		customer = createCustomer('テスト');
		insert customer;
		Customer__c newCustomer = new Customer__c();
		// テスト実行
		Test.startTest();
		// ファイナンス情報を取得
		newCustomer = HearingDAO.getCustomerById(customer.Id);
		Test.stopTest();
		System.assertEquals(newCustomer.Name, customer.Name);
		System.assertEquals(newCustomer.Industry__c, customer.Industry__c);
		System.assertEquals(newCustomer.Id, customer.Id);
	}
	/**
	* testMethodGetCustomerByIdNull
	* 顧客を取得(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodGetCustomerByIdNull(){
		// 顧客を作成
		Customer__c newCustomer = new Customer__c();
		// テスト実行
		Test.startTest();
		// 顧客を取得
		newCustomer = HearingDAO.getCustomerById('a00000000001');
		Test.stopTest();
		System.assertEquals(null, newCustomer);
	}
	/**
	* testMethodgetIndustryAverage
	* 業界平均マスタを取得(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodgetIndustryAverage(){
		// 業界平均マスタを作成
		IndustryAverage__c industry = new IndustryAverage__c();
		industry = createIndustryAverage();
		IndustryAverage__c newIndustry = new IndustryAverage__c();
		// テスト実行
		Test.startTest();
		// 業界平均マスタを取得
		newIndustry = HearingDAO.getIndustryAverage(industry.Name__c);
		Test.stopTest();
		System.assertEquals(newIndustry.ROA__c,industry.ROA__c);
		System.assertEquals(newIndustry.ROE__c,industry.ROE__c);
		System.assertEquals(newIndustry.ROS__c,industry.ROS__c);
		System.assertEquals(newIndustry.ICR__c,industry.ICR__c);
		System.assertEquals(newIndustry.DR__c,industry.DR__c);
		System.assertEquals(newIndustry.CAR__c,industry.CAR__c);
		System.assertEquals(newIndustry.LR__c,industry.LR__c);
		System.assertEquals(newIndustry.ITO__c,industry.ITO__c);
		System.assertEquals(newIndustry.QD__c,industry.QD__c);
		System.assertEquals(newIndustry.APTO__c,industry.APTO__c);
		System.assertEquals(newIndustry.RTO__c,industry.RTO__c);
	}
	/**
	* testMethodgetIndustryAverageNull
	* 業界平均マスタを取得(テスト)
	* @param 	: なし
	* @return	: なし
	* @created  : 2014/01/24 Hung Nguyen The
	* @modified : 
	*/
	static testMethod void testMethodgetIndustryAverageNull(){
		// 業界平均マスタを作成
		IndustryAverage__c newIndustry = new IndustryAverage__c();
		// テスト実行
		Test.startTest();
		// 業界平均マスタを取得
		newIndustry = HearingDAO.getIndustryAverage('Name1');
		Test.stopTest();
		System.assertEquals(newIndustry, null);
	}
	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 2013-05-15 Khanh Vo Quoc
	*/
	static Customer__c createCustomer(String name){
		// 顧客
		Customer__c customs = new Customer__c();
		// 顧客名
		customs.Name = name;
		// 業界平均マスタ
		customs.Industry__c = 'Khanh-test';
		// 代理店
		//  代理店【INGAgency】を作成
		AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
		agcINGA.Name = 'BSPLInput-test-test';
		insert agcINGA;
		customs.AgencyForINGAgency__c = agcINGA.Id;
		return customs;
	}
	/**
	* createCustomer()
	* ファイナンス情報を作成
	* @return: ファイナンス情報
	* @created: 2014/01/24 Hung Nguyen The
	*/
	static Finance__c createFinance(Integer intOption){
		// ファイナンス情報を作成
		Customer__c customs = createCustomer('khanh-test');
		insert customs;
		Finance__c finance = new Finance__c();
		// 決算期(前期)
		finance.Term__c = 1;
		if(intOption != null){
			finance.Term__c = 1 * intOption;
			finance.H_RD__c = 2 * intOption;	// 予想売上下落率
			finance.H_JASGD__c = 2 * intOption;// 連帯保証債務額
			finance.H_LO__c = 2 * intOption;	// 役員借入金
			finance.H_DBAC__c = 2 * intOption;	// 死亡保障額(法人契約)
			finance.H_UG__c = 2 * intOption;	// 含み益
			finance.H_DBAI__c = 2 * intOption;	// 死亡保障額(個人契約)
			finance.H_BRM__c = 2 * intOption;	// 銀行返済額(月)
			finance.H_BRP__c = 2 * intOption;	// 銀行返済額　準備額
			finance.H_BFLEM__c = 2 * intOption;	// 遺族生活資金(月)
			finance.H_BFLET__c = 2 * intOption;	// 遺族生活資金(必要期間)
			finance.H_RE__c = 2 * intOption;	// 現在の社長の報酬/年
			finance.H_CRAA__c = 2 * intOption;	// 社長の報酬減額可能額/年
			finance.H_ERB__c = 2 * intOption;	// 従業員退職金(予定額)
			finance.H_ORB__c = 2 * intOption;	// 役員退職金予定額
		}
		finance.Customer__c = customs.id; // 顧客名
		insert finance;
		return finance;
	}
	/**
	* createIndustryAverage()
	* 業界平均マスタを作成
	* @return: 業界平均マスタ
	* @created: 2014/01/24 Hung Nguyen The
	*/
	static IndustryAverage__c createIndustryAverage(){
		IndustryAverage__c industry = new IndustryAverage__c();
		industry.Name__c = 'Khanh-test';
		industry.ROA__c = 10;	// 業界平均マスタ.ROA（総資産経常利益率）
		industry.ROE__c = 10;	// 業界平均マスタ.ROE(自己資本当期純利益率）
		industry.ROS__c = 10;	// 業界平均マスタ.ROS（売上高当期純利益率）
		industry.ICR__c = 10;	// 業界平均マスタ.インタレスト・カバレッジ・レシオ
		industry.DR__c = 10;	// 業界平均マスタ.債務償還年数
		industry.CAR__c = 10;	// 業界平均マスタ.自己資本比率
		industry.LR__c = 10;	// 業界平均マスタ.手元流動性比率
		industry.ITO__c = 10;	// 業界平均マスタ.棚卸資産回転率
		industry.QD__c = 10;	// 業界平均マスタ.当座比率
		industry.APTO__c = 10;	// 業界平均マスタ.買入債務回転率
		industry.RTO__c = 10;	// 業界平均マスタ.売上債権回転率
		insert industry;
		return industry;
	}
}