@isTest
private class TestASS_ProgressSummaryController {
	//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}
	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（MRページから）
	 * MRサマリー表示
	 */
	private static testMethod void mrPageTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc1 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc1;
		Account acc2 = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1');
		insert acc2;

		//事務所
		Account baOffice = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc1.Id, OwnerId = mr.Id);
		insert baOffice;
		//事務所
		Account iaOffice = new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc2.Id, OwnerId = mr.Id);
		insert iaOffice;

		//商品マスタ作成
		ProductMaster__c fsprod = new ProductMaster__c(Name = 'Sample_FS', LargeClassification__c = 'CategoryⅢ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert fsprod;

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();

		//FS予算
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'CategoryⅢ', OwnerId = mr.Id));
		//PT予算
		oppList.add(new Opportunity(Name = 'PT予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 2000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'CategoryⅡ', OwnerId = mr.Id));
		//BAFS予算
		oppList.add(new Opportunity(Name = 'BAFS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 3000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'BA Financial Solutions', OwnerId = mr.Id));
		//BAPT予算
		oppList.add(new Opportunity(Name = 'BAPT予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 4000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'BA Protection', OwnerId = mr.Id));

		//IA
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = iaOffice.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払', OwnerId = mr.Id));

		oppList.add(new Opportunity(Name = 'PT実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 2000000, InsuranceType__c = ptprod.Id, AccountId = iaOffice.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払', OwnerId = mr.Id));

		
		insert oppList;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		createCustomField(eventRecType.Id);
		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = baOffice.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;

		//営業目標
		List<SalesTarget__c> stList = new List<SalesTarget__c>();
		//IA
		stList.add(new SalesTarget__c(Account__c = iaOffice.Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 1, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 2));
		insert stList;

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('accountId',baOffice.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_ProgressSummaryController con = new ASS_ProgressSummaryController();
			con.paramMrId = mr.Id;

			Map<String,ASS_ProgressSummaryController.SummaryRow> resultMap = new Map<String,ASS_ProgressSummaryController.SummaryRow>();
			resultMap = con.getMap();

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.isMRpage,true);
			//fs
			System.assertEquals(resultMap.get('fs').budget,1000000);
			System.assertEquals(resultMap.get('fs').perform,1000000);
			System.assertEquals(resultMap.get('fs').difference,0);
			//pt
			System.assertEquals(resultMap.get('pt').budget,2000000);
			System.assertEquals(resultMap.get('pt').perform,2000000);
			System.assertEquals(resultMap.get('pt').difference,0);
			//bafs
			System.assertEquals(resultMap.get('bafs').budget,3000000);
			System.assertEquals(resultMap.get('bafs').perform,0);
			System.assertEquals(resultMap.get('bafs').difference,-3000000);
			//bapt
			System.assertEquals(resultMap.get('bapt').budget,4000000);
			System.assertEquals(resultMap.get('bapt').perform,0);
			System.assertEquals(resultMap.get('bapt').difference,-4000000);
			//est
			System.assertEquals(resultMap.get('est').budget,1);
			System.assertEquals(resultMap.get('est').perform,2);
			System.assertEquals(resultMap.get('est').difference,1);
			//docreq
			System.assertEquals(resultMap.get('docreq').budget,2);
			System.assertEquals(resultMap.get('docreq').perform,2);
			System.assertEquals(resultMap.get('docreq').difference,0);
			//activity
			System.assertEquals(resultMap.get('activity').budget,3);
			System.assertEquals(resultMap.get('activity').perform,3);
			System.assertEquals(resultMap.get('activity').difference,0);

		}
	}

		/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（代理店事務所ページから）
	 * 代理店事務所サマリー表示
	 */
	private static testMethod void officeSummaryTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'IP');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		//商品マスタ作成
		ProductMaster__c fsprod = new ProductMaster__c(Name = 'Sample_FS', LargeClassification__c = 'CategoryⅢ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert fsprod;

		//商品マスタ作成
		ProductMaster__c ptprod = new ProductMaster__c(Name = 'Sample_PT', LargeClassification__c = 'CategoryⅡ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert ptprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = office.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));

		oppList.add(new Opportunity(Name = 'PT実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1500000, InsuranceType__c = ptprod.Id, AccountId = office.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;

		//行動
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		createCustomField(eventRecType.Id);
		List<Event> eventList = new List<Event>();
		eventList.add(new Event(Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(Subject = 'test3', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;

		//営業目標
		SalesTarget__c st = new SalesTarget__c(Account__c = office.Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3);
		insert st;

		//ページ表示
		PageReference pr = Page.ASS_OfficeSummary;
		pr.getParameters().put('mrId',mr.Id);
		pr.getParameters().put('accountId',office.Id);
		pr.getParameters().put('unitId',unit.Id);
		Test.setCurrentPage(pr);


		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_ProgressSummaryController con = new ASS_ProgressSummaryController();
			con.paramMrId = mr.Id;
			con.paramOfficeId = office.Id;
			Map<String,ASS_ProgressSummaryController.SummaryRow> resultMap = new Map<String,ASS_ProgressSummaryController.SummaryRow>();
			resultMap = con.getMap();

//===============================テスト終了===============================
			Test.stopTest();

			System.assertEquals(con.isMRpage,false);
			//fs
			System.assertEquals(resultMap.get('fs').perform,1000000);
			//pt
			System.assertEquals(resultMap.get('pt').perform,1500000);
			//bafs
			System.assertEquals(resultMap.get('bafs').perform,0);
			//bapt
			System.assertEquals(resultMap.get('bapt').perform,0);

		}
	}
}