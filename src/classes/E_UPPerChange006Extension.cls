public with sharing class E_UPPerChange006Extension {
	
	private static String STR_HYPHEN = '-';

	// Controller
	private E_UPTerm003Controller controller;
	// インデックス存在判定
	private Boolean hasIndex = false;
	// SPVA判定
	public Boolean isSpva{get; set;}
	// コンポジット判定
	public Boolean isComposite{get; set;}
	// 騰落率リスト
	public List<PerChange> perChangeList{get; Set;}	
	// 騰落率
	public class PerChange{
		public String fundName{get; set;}
		public String fromPer{get; set;}
		public String toPer{get; set;}
		public String totalPer{get; set;}
	}
	// ダウンロード履歴の検索条件
	private E_DownloadCondition.UnitPriceDownloadCondi downloadCondi;

	/** コンストラクタ */
	public E_UPPerChange006Extension(E_UPTerm003Controller con){
		controller = con;
	}
	
	/** init */
	
	public Pagereference init006(){
		//controller.pgTitle = E_UPTerm003Controller.UNITPRICE_PAGENAMES.get('006');		//ページタイトル
		PageReference pr = controller.init();
		if(pr!=null){return pr;}
		
		controller.pgTitle = '騰落率';
		Pagereference pref;
		
		// SPVA判定
		isSpva = '0'.equals(controller.dlCondi.NameKBN);

		// 騰落率リスト取得
		perChangeList = getPerChangeList();
		
		// コンポジット判定
		isComposite = (isSpva == true && isComposite == false) ? false : true;
		
		//　エラーチェック
		pref = errorCheck();
		
		return pref;
	}

	/** ファンド基本情報 */
	public E_UPBPF__c getUpbpf(){
		return controller.eupbpf;
	}
	
	/** バリアブルアニュイティ判定 */
	public Boolean getIsVariableAnnuity(){
		return controller.isVariableAnnuity;
	}
	
	/** 開始日の日付 */
	public String getFromDate(){
		//return formatDate2String(controller.dlCondi.fromTerm);
		return formatDate2String(downloadCondi.fromTerm);
	}
	
	/** 終了の日付 */
	public String getToDate(){
		//return formatDate2String(controller.dlCondi.toTerm);
		return formatDate2String(downloadCondi.toTerm);
	}
	
	/** 騰落率リスト取得 */
	private List<PerChange> getPerChangeList(){
		List<PerChange> perChangeList = new List<PerChange>(); 
        downloadCondi = controller.dlCondi;
        
        // 開始日のユニットプライス
        Map<Id, E_UPFPF__c> fromFundsMap = E_UPFPFDao.getIndexBySvcpfIds(downloadCondi.ZSFUNDCD, downloadCondi.fromTerm, true);
        // 終了日のユニットプライス
		Map<Id, E_UPFPF__c> toFundsMap = E_UPFPFDao.getIndexBySvcpfIds(downloadCondi.ZSFUNDCD, downloadCondi.toTerm, false); 

		isComposite = false;
		for(String upfpfId : downloadCondi.ZSFUNDCD){
			// ファンド毎の開始日、終了日取得
			E_UPFPF__c fromUpfpf = fromFundsMap.get(upfpfId);
			E_UPFPF__c toUpfpf = toFundsMap.get(upfpfId);
				
			// ファンド毎の騰落率取得
			PerChange pc = getFundPerChange(fromUpfpf, toUpfpf);
			perChangeList.add(pc);
			
			// コンポジット判定
			if(fromUpfpf != null && isComposite == false && fromUpfpf.ZFUNDFLG__c == true){
				isComposite = true;
			}
		}

		return perChangeList;
	}
	
	/** ファンド毎の騰落率 */
	private PerChange getFundPerChange(E_UPFPF__c fromUpfpf, E_UPFPF__c toUpfpf){
		PerChange pc = new PerChange();
		
		pc.fundName = getFundName(fromUpfpf);						// 投資信託名、特別勘定名
		pc.fromPer = getIndex(fromUpfpf, downloadCondi.fromTerm);	// 開始日のインデックス
		pc.toPer = getIndex(toUpfpf, downloadCondi.toTerm);			// 終了日のインデックス
		pc.totalPer = getPercentChange(pc.fromPer, pc.toPer);			// 騰落率

		return pc;
	} 
	
	/** 表示名の取得  */
	private String getFundName(E_UPFPF__c upfpf){
		String fundName;
		
		if(upfpf == null) return null;
		
		// 名称取得区分『0』 かつ コンポジットファンドフラグ『false』 ：ファンド名＋投資信託名
		if(isSpva && !upfpf.ZFUNDFLG__c){
			fundName = upfpf.ZNAMEUnion__c;
			
		// 名称取得区分『0』 かつ コンポジットファンドフラグ『true』 ：投資信託名
		}else if(isSpva && upfpf.ZFUNDFLG__c){
			fundName = upfpf.ZIVTNAMEUnion__c;
			
		// 名称取得区分『1』  ：ファンド名
		}else{
			fundName = upfpf.ZFNDNAMEUnion__c;
		}
		
		return fundName;
	}
	
	/** インデックスの取得 */
	private String getIndex(E_UPFPF__c upfpf, String targetDay){
		String zero = E_Util.getRoundingDownString(0, 2);
		String index = zero;

		if(!upfpf.E_UPXPFs__r.isEmpty()){
			if(upfpf.E_UPXPFs__r.get(0).ZFUNDDTE__c == targetDay){
				index = E_Util.getRoundingDownString(upfpf.E_UPXPFs__r.get(0).ZVINDEX__c, 2);
				hasIndex = true;
			}
		}
		
		if(index == zero){
			index = STR_HYPHEN;
		}
		
		return index;
	}
	
	/** 騰落率の取得 */
	private String getPercentChange(String fromPer, String toPer){
		Decimal fromIndex;
		Decimal toIndex;
		Decimal decPercentChange;
		String zero = E_Util.getRoundingDownString(0, 2);
		String percentChange = zero;
		
		if(!STR_HYPHEN.equals(fromPer) && !STR_HYPHEN.equals(toPer)){
			fromIndex = Decimal.valueOf(fromPer);
			toIndex = Decimal.valueOf(toPer);
			if(fromIndex != null && toIndex != null){
				decPercentChange = (toIndex - fromIndex) / fromIndex * 100;
				percentChange = E_Util.getRoundingHalfupString(decPercentChange, 2);
			}
		}
			
		if(percentChange == zero){
			percentChange = '-';
		}else{
			percentChange = decPercentChange > 0 ? '+' + percentChange : percentChange;
		}
		
		return percentChange;
	}	
	
	/** 日付フォーマット */
	private String formatDate2String(String item){
		if(String.isNotBlank(item) && item.length() == 8){
			item = item.substring(0,4) + '/' + item.substring(4,6) + '/' + item.substring(6,8);
		}
		return item;
	}
	
	/** エラーチェック */
	private Pagereference errorCheck(){
		Pagereference pref;
		
		//　インデックスが存在しない場合エラー
		if(!hasIndex){
			pref = E_Util.toUpErrorPage('UP6|E01');
		}
		
		return pref;
	}
	
}