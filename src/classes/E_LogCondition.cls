/**
 * ログ管理
 */
public class E_LogCondition {

	public virtual class LogCondi{
		// エラー判定
		public Boolean isError {get; set;}
		// アクセスユーザ
		public User accessUser {get; set;}
		// アクセス日時
		public Datetime accessDateTime {get; set;}
    	// アクセスページ
    	public String accessPage {get; set;}
    	// 詳細情報
    	public String detail {get; set;}	
		// ヘッダー情報
		public String headAccept {get; set;}
		public String headAcceptEncoding {get; set;}
		public String headAcceptLanguage {get; set;}
		public String headCipherSuite {get; set;}
		public String headConnection {get; set;}
		public String headHost {get; set;}
		public String headUserAgent {get; set;}
		public String headXSalesforceForwardedTo {get; set;}
		public String headXSalesforceSIP {get; set;}
		public String headXSalesforceVIP {get; set;}
		
		/* Constructor */
		public LogCondi(){
			// エラー判定
			isError = false;
			// アクセスユーザ
			accessUser = new User();
			accessUser.Id = UserInfo.getUserId();
			// アクセス日時
			accessDateTime = Datetime.now();
			// アクセスページ
			accessPage = ApexPages.currentPage().getUrl().split('/')[2].split('\\?')[0];
			
			// ヘッダー情報セット
			Map<String, String> headerMap = Apexpages.currentPage().getHeaders();
	    	headAccept = E_Util.null2Blank(headerMap.get('Accept'));
	    	headAcceptEncoding = E_Util.null2Blank(headerMap.get('Accept-Encoding'));
	    	headAcceptLanguage = E_Util.null2Blank(headerMap.get('Accept-Language'));
	    	headCipherSuite = E_Util.null2Blank(headerMap.get('CipherSuite'));
	    	headConnection = E_Util.null2Blank(headerMap.get('Connection'));
	    	headHost = E_Util.null2Blank(headerMap.get('Host'));
	    	headUserAgent = E_Util.null2Blank(headerMap.get('User-Agent'));
	    	headXSalesforceForwardedTo = E_Util.null2Blank(headerMap.get('X-Salesforce-Forwarded-To'));
	    	headXSalesforceSIP = E_Util.null2Blank(headerMap.get('X-Salesforce-SIP'));
	    	headXSalesforceVIP = E_Util.null2Blank(headerMap.get('X-Salesforce-VIP'));
		}
	}

	/**
	 * ドクター
	 */
	public class DoctorCondi extends LogCondi{
		public String recId {get; set;}
	}
}