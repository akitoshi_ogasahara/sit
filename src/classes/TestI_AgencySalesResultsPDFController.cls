@isTest
private class TestI_AgencySalesResultsPDFController {
    private static Account distribute;
    private static Account office;
    private static Account suboffice;
    private static Contact agent;
    private static E_BizDataSyncLog__c ebizLog;
    private static E_AgencySalesResults__c asr;
    private static E_LicenseAssessment__c la;

    private static testMethod void pageAction_test001(){
        //実行ユーザ作成
        User actUser = TestI_TestUtil.createUser(true, 'fstest', 'システム管理者');
        E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR**',AppMode__c = 'IRIS');
        insert src;

        //テストデータ作成
        createData('AY');

        // ページ表示
        PageReference pr = Page.IRIS_AgencySalesResultsPrint;
        Test.setCurrentPage(pr);

        System.runAs(actUser){
//==================================テスト開始==================================
        Test.startTest();
        I_AgencySalesResultsController ascon = new I_AgencySalesResultsController();
        ApexPages.currentPage().getParameters().put('agency',distribute.Id);
        ascon.pageAction();

        //Logレコード作成
        E_Log__c elog = E_LogUtil.createLog();
        //必要な情報をjsonにする
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartobject();

        String memo = 'Test';
        String viewType = '1';
        Boolean isEmployee = true;

        gen.writeStringField('sResultsId', asr.Id );                    //代理店挙積情報レコード
        gen.writeStringField('qualificationType', asr.QualifSim__c);    //資格情報
        gen.writeStringField('memo', memo );                            //メモ
        gen.writeStringField('viewType', viewType);                  //ビュータイプ
        gen.writeBooleanField('isEmployee',isEmployee);                 //管理者ログインフラグ
        

        gen.writeEndObject();

        elog.detail__c = gen.getAsString();
        //ログ情報を設定してinsertする
        elog.AccessPage__c =  String.valueOf ( Page.IRIS_NNLife_AgencyInfo );//遷移先のページを設定
        elog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_DOWNLOAD;
        elog.Name = 'pdf出力';

        insert elog;

        PageReference pdfpr = Page.IRIS_NNLife_AgencyInfo;
        pdfpr.getParameters().put('id', elog.Id);
        Test.setCurrentPage(pdfpr);

        I_AgencySalesResultsPDFController apcon = new I_AgencySalesResultsPDFController();
        apcon.getPDFParam();
        apcon.getMemoList();
        apcon.getSalesResult();
        apcon.getLicenseAssessment();
//==================================テスト終了==================================
        Test.stopTest();
//==================================テスト判定==================================
        system.assertEquals(apcon.sResultsId,asr.Id);
        system.assertEquals(apcon.qualificationType,asr.QualifSim__c);
        system.assertEquals(apcon.memo,memo);
        system.assertEquals(apcon.viewType,viewType);
        system.assertEquals(apcon.isEmployee,isEmployee);

        }

    }
    /*
     *テストデータ作成
     */
    private static void createData(String hierarchy){
        /* Test Data */
        String CODE_DISTRIBUTE = '10001';       // 代理店格コード
        String CODE_OFFICE = '10002';           // 事務所コード
        String CODE_BRANCH = '88';              // 支社コード
        String CODE_DISTRIBUTE_SUB = '20001';   // 代理店格コード
        String CODE_OFFICE_SUB = '20002';       // 事務所コード
        String CODE_BRANCH_SUB = '99';          // 支社コード
        String BUSINESS_DATE = '20000101';      // 営業日

        //連携ログ作成
        Date d = E_Util.string2DateyyyyMMdd(BUSINESS_DATE);
        E_BizDataSyncLog__c ebizLog = new E_BizDataSyncLog__c(kind__c = 'F',InquiryDate__c = d,BatchEndDate__c = d);
        insert ebizLog;

        // Account 格
        distribute = new Account(Name = 'テスト代理店格',E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE);
        insert distribute;

        // Account 事務所
        office = new Account(Name = 'テスト事務所',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,E_CL2PF_BRANCH__c = CODE_BRANCH,E_COMM_VALIDFLAG__c = '1');
        insert office;
        suboffice = new Account(Name = 'テスト事務所SUB',ParentId = distribute.Id,E_CL2PF_ZAGCYNUM__c = CODE_OFFICE_SUB,E_CL2PF_BRANCH__c = CODE_BRANCH_SUB,E_COMM_VALIDFLAG__c = '1');
        insert suboffice;

        // Contact 募集人作成
        agent = new Contact(AccountId = office.id, LastName = 'テスト募集人');
        insert agent;

        // 代理店挙績情報
        asr = new E_AgencySalesResults__c(
            ParentAccount__c = distribute.Id,
            BusinessDate__c = BUSINESS_DATE,
            Hierarchy__c = hierarchy,
            QualifSim__c = '優績S',
            YYYYMM__c = '201801',
            XHAH_AGCLS__c = '優績A'
        );
        if(hierarchy == 'AY'){
            asr.Account__c = office.id;
        }else if(hierarchy == 'AT'){
            asr.Account__c = office.id;
            asr.Agent__c = agent.id;
        }
        insert asr;

        //資格査定詳細情報
        la = new E_LicenseAssessment__c(
            E_AgencySalesResults__c = asr.Id,
            BusinessDate__c = BUSINESS_DATE,
            Hierarchy__c = '優績S1',
            Standard_NBCANP__c = 5000000,
            State_NBCANP__c = -20000,
            Standard_IQA__c = 90,
            State_IQA__c = 8.0,
            Standard_ActiveMonth__c = 9,
            State_ActiveMonth__c = -8,
            Standard_NBCANP_SpInsType__c = 1000000,
            State_NBCANP_SpInsType__c = -1000000,
            Standard_IANP__c = 0,
            State_IANP__c = 0
        );
        insert la;
    }
}