public with sharing class E_IDCPFUpdatebyUserTrigger {

	/**
	 *		Constructor
	 */
	public E_IDCPFUpdatebyUserTrigger(){
	
	}
	
	/**
	 *		doUpdateEIDCPF
	 *			Userオブジェクトレコードの更新に伴ってEIDCPFレコードを空更新（権限セット付与のため）
	 */
	public void doUpdateEIDCPF(List<User> users, Map<Id,User> oldMap){
		Set<Id> userIds = new Set<Id>();
		for(User u:users){
			User oldRec = oldMap==null?null:oldMap.get(u.Id);
			if(u.isActive
				&& oldRec != null							//新規の場合は、割り当てはEIDCの設定で行われる
				&& (oldRec.isActive == false				//無効⇒有効へ変更されたとき
						||oldRec.profileId != u.profileId)	//プロファイルIDが変更されたとき
															){
				userIds.add(u.Id);
			}
		}
		
		if(userIds.size()>0){
//			doUpdateByUserIds(userIds);
			List<E_IDCPF__c> idcpfs = E_IDCPFDao.getRecsByUserIds(userIds);
			E_IDCPFTriggerBizLogic bizLogic = new E_IDCPFTriggerBizLogic();
			bizLogic.addTargetUsers(idcpfs);
			bizLogic.PermissionSetAssign();
		}
	}

	private static void doUpdateByUserIds(Set<Id> uids){
		List<E_IDCPF__c> idcpfs = E_IDCPFDao.getRecsByUserIds(uids);
		if(idcpfs.size()>0){
			update idcpfs;
		}
	}
}