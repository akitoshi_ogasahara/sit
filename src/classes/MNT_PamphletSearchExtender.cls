global with sharing class MNT_PamphletSearchExtender extends SkyEditor2.Extender{
    
	MNT_PamphletSearchController extension;
	I_ContentMaster__c record;

    public MNT_PamphletSearchExtender(MNT_PamphletSearchController extension){
        this.extension = extension;
    }

    public PageReference doExportCSV(){
		PageReference pr = Page.MNT_PamphletCSV;
		String soql = extension.queryMap.get('PamphletResultTable').toSoql();
		String sWhere = E_SoqlUtil.getAfterWhereClause(soql, extension.mainSObjectType.getDescribe().getName());
		sWhere = E_SoqlUtil.trimLimitClause(sWhere);
		pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
								E_EncryptUtil.getEncryptedString(sWhere));
		return pr;
	}

}