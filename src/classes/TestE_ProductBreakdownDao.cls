@isTest
public class TestE_ProductBreakdownDao {

	/**
	 * getRecsByAsrIdのテスト
	 */
	@isTest static void getRecsByAsrIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 商品内訳の作成
		List<E_ProductBreakdown__c> productBreakdowns = createProductBreakdowns(agencySalesResults.Id);
		insert productBreakdowns;

		// テスト
		Test.startTest();
		List<E_ProductBreakdown__c> recs = E_productBreakdownDao.getRecsByAsrId(agencySalesResults.Id);
		System.assertEquals(agencySalesResults.Id, recs[0].E_AgencySalesResults__c);
		Test.stopTest();

	}


	/**
	 * 商品内訳の作成
	 */
	public static List<E_ProductBreakdown__c> createProductBreakdowns(Id asrId){

		List<E_ProductBreakdown__c> productBreakdowns = new List<E_ProductBreakdown__c>();

		E_ProductBreakdown__c rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '生活障害';			// 商品名　当年
		rec.NBCANP__c					= 122482;				// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 314608;				// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 108055;				// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '重大疾患';			// 商品名　当年
		rec.NBCANP__c					= 143207;				// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 216225;				// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 35322;				// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '遅増';				// 商品名　当年
		rec.NBCANP__c					= 103699;				// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 167703;				// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 68305;				// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '無解約定期';			// 商品名　当年
		rec.NBCANP__c					= 588;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 4663;					// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 5145;					// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '無解約収保';			// 商品名　当年
		rec.NBCANP__c					= 0;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 0;					// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 100;					// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '定期(100歳満了)';	// 商品名　当年
		rec.NBCANP__c					= 1942;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 2110;					// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 128;					// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= 'その他定期';			// 商品名　当年
		rec.NBCANP__c					= 1475;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 23623;				// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 5065;					// 新契約係数ANP　2年前
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= '養老';				// 商品名　当年
		rec.NBCANP__c					= 0;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 3316;					// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 1207;					// 新契約係数ANP　2年前
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= 'ガン';				// 商品名　当年
		rec.NBCANP__c					= 2481;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 31806;				// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 18032;				// 新契約係数ANP　2年前
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		productBreakdowns.add(rec);

		rec = new E_ProductBreakdown__c();
		rec.E_AgencySalesResults__c		= asrId;				// 代理店挙積情報
		rec.ProductName__c				= 'その他';				// 商品名　当年
		rec.NBCANP__c					= 3;					// 新契約係数ANP　当年
		rec.NBCANP_LY__c				= 5;					// 新契約係数ANP　前年
		rec.NBCANP_B2Y__c				= 10;					// 新契約係数ANP　2年前
		productBreakdowns.add(rec);

		return productBreakdowns;
	}

}