/*
 * CC_CaseEditControllerTest
 * Test class of CC_CaseEditController
 * created  : Accenture 2018/4/5
 * modified :
 */

@isTest
private class CC_CaseEditControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Test invokeMethod()
	* methodName is initCase
	*/
	static testMethod void invokeMethodTest01() {
		CC_CaseEditController caseEditController = new CC_CaseEditController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'initCase';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			caseEditController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is extractSRPolicy
	*/
	static testMethod void invokeMethodTest02() {
		CC_CaseEditController caseEditController = new CC_CaseEditController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'extractSRPolicy';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			caseEditController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is saveCase
	*/
	static testMethod void invokeMethodTest03() {
		CC_CaseEditController caseEditController = new CC_CaseEditController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveCase';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			caseEditController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is SRFormPrint
	*/
	static testMethod void invokeMethodTest04() {
		CC_CaseEditController caseEditController = new CC_CaseEditController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'SRFormPrint';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			caseEditController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is saveCaseConfirmed
	*/
	static testMethod void invokeMethodTest05() {
		CC_CaseEditController caseEditController = new CC_CaseEditController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveCaseConfirmed';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			caseEditController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is SRFormPrintConfirmed
	*/
	static testMethod void invokeMethodTest06() {
		CC_CaseEditController caseEditController = new CC_CaseEditController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'SRFormPrintConfirmed';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			caseEditController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}