public class I_SalesPlanPDFController extends I_AbstractController {
	public String isClear;
	/*	Constructor	*/
	public I_SalesPlanPDFController(){
		this.isClear = ApexPages.currentPage().getParameters().get(I_SalesPlanExtender.URL_PARAM_ISCLEAR);
	}
	
	public PageReference createPDFAndShow(){
		String planId = ApexPages.currentPage().getParameters().get(I_SalesPlanExtender.URL_PARAM_PLANID);
		//system.assert(planId!=null, '営業成績Idが取得できません。');
	
		PageReference pr = Page.IRIS_SalesPlan;
		pr.getParameters().put('id', planId);
		pr.getParameters().put('isPdf', '1');
		pr.getParameters().put('isClear', this.isClear);
		if(this.isClear == 'true'){
			return pr;
		}else{
			Attachment att = new Attachment();
			att.parentId = planId;
			if(Test.isRunningTest() == false){
				att.Body = pr.getContentASPDF();
			}else{
				att.Body = Blob.valueOf('テスト');
			}

			String pdfName = ApexPages.currentPage().getParameters().get(I_SalesPlanExtender.URL_PARAM_PDFTitle);
			att.Name = pdfName + '.pdf';

			insert att;

			//営業計画書の利用はMR（社内ユーザ）のみ
			pr = new PageReference('/servlet/servlet.FileDownload');
			pr.getParameters().put('file', att.id);
			return pr;

		}
	}
}