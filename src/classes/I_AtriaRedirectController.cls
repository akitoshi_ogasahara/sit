// P16-0003 Atria対応開発
public with sharing class I_AtriaRedirectController {

	// Atriaのドメイン情報リスト
	public Map<String, String> aDomainMap{get;set;}

	// AtriaリンクをクリックしたユーザID
	private String userId;
	
	// URLパラメータキー：ユーザID
	private static final String PARAM_USER_ID = 'userid';
	
	// URLパラメータキー：Atriaドメイン名
	private static final String PARAM_ATRIA_EP_KEY = 'atriaEpKey';

	//URLパラメータキー：秘密の入り口フラグ
	private static final String PARAM_ISSECRET = 'isSecret';

	/**
	 * コンストラクタ.
	 */
	public I_AtriaRedirectController() {
		this.aDomainMap = new Map<String, String>();
	}

	/**
	 * 初期処理.
	 *
	 * @return PageReference PageReferenceインスタンス
	 */
	public PageReference init() {

		// URパラメータからユーザID取得
		this.userId = ApexPages.currentPage().getParameters().get(PARAM_USER_ID);

		//秘密の入口判定
		String isSecret = ApexPages.currentPage().getParameters().get(PARAM_ISSECRET);
		
		// カスタム表示ラベルからAtriaドメイン情報を取得する
		String domainInfos = (isSecret <> '1')? Label.E_ATRIA_DOMAINS : Label.E_ATRIA_DOMAINS_SECRET;
		
		try {
			this.aDomainMap = (Map<String, String>)JSON.deserialize(domainInfos, Map<String, String>.class);
			
			// 設定されているドメイン情報が1つである場合、リダイレクト
			if(this.aDomainMap.size() == 1) {
				return this.redirect(this.aDomainMap.values().get(0));
			}
		} catch(Exception e) {
			
		}
		
		return null;
	}
	
	/**
	 * 指定したAtriaドメインにリダイレクトする.
	 *
	 * @return PageReference PageReferenceインスタンス
	 */
	public PageReference doRedirect() {
		
		// Atriaドメイン名取得
		String endpointKey = ApexPages.currentPage().getParameters().get(PARAM_ATRIA_EP_KEY);
		
		if(String.isBlank(endpointKey)) {
			return null;
		}
		
		// Atriaドメインにリダイレクト
		if(this.aDomainMap.containsKey(endpointKey)) {
			return this.redirect(this.aDomainMap.get(endpointKey));
		}
		
		return null;
	}
	
	/**
	 * 指定したURLにリダイレクトする.
	 *
	 * @param String リダイレクト先のURL
	 * @return PageReference PageReferenceインスタンス
	 */
	private PageReference redirect(String url) {
		
		this.logging();
		
		return new PageReference(url);
	}
	
	/**
	 * ログ作成.
	 */
	private void logging() {
		E_Log__c log = E_LogUtil.getLogRec(null, null);
		log.Name = 'Atriaリダイレクト画面';
		log.AppName__c = E_AccessController.getInstance().idcpf.appMode__c;
		log.ActionType__c = I_Const.ACTION_TYPE_ATRIA_SSO;
		log.TargetUser__c = this.userId;
		
		insert log;
	}
	
	/**
	 * 設定されたAtriaドメイン数を返す.
	 *
	 * @reutrn Integer Atriaのドメイン数
	 */
	public Integer getAtriaDomainSize() {
		return this.aDomainMap.size();
	}
}