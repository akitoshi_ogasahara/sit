public with sharing class E_BarGraphPolicyController extends E_AbstractBarGraph {
	
	//ボタンラベル
	private static final String BUTTON_LABEL_HALF_YEAR = '半年';
	private static final String BUTTON_LABEL_2_YEAR = '2年';
	private static final String BUTTON_LABEL_ALL = '契約時以来';
	
	//URLパラメータ
	//ラチェット死亡保障金額の推移表示フラグ
	public Boolean ratchetFlag {get;set;}
	//ステップアップ情報表示フラグ
	public Boolean stepupFlag {get;set;}
	//最低死亡保障額の推移ラベル
	public String sinsdescLabel {get;set;}
	//ラチェット死亡保障額の推移ラベル
	public String ratchetLabel {get;set;}
	//保険種類コード
	public String typeCode {get;set;}
	
	//グラフステータス
	//2番目のグラフのラベル
	public String secondGraphLabel {get;set;}
	//3番目のグラフのラベル
	public String thirdGraphLabel {get;set;}
	//4番目のグラフのラベル
	public String fourthGraphLabel {get;set;}
	//3番目のグラフの色
	public String thirdGraphColor {get;set;}
	//3番目のグラフを表示するか？
	public Boolean isDislpayThirdGraph {get;set;}
	
	//どのボタンが押されたか？
	private String period;
	
	//グラフ生成
	public void createGraph() {
		
		//グラフ表示データ作成
		graphRecs = new List<GraphData>();
		
		String stepupLabel = 'ステップアップ金額';
		String actsumLabel = '死亡給付金額';
		
		//SPVAヘッダの保険種類コードの上2桁がSRの場合、死亡給付金額とステップアップ金額のラベルを変更
		if(String.isNotBlank(typeCode) && typeCode.startsWith('SR')){
			stepupLabel = 'ステップアップ死亡保障額';
			actsumLabel = '死亡保険金額';
		}
		
		secondGraphLabel = sinsdescLabel;
		fourthGraphLabel = actsumLabel;
		isDislpayThirdGraph = true;
		
		if(ratchetFlag){
			//ラチェット死亡保障金額の推移表示フラグがtrueの場合、ラチェット死亡保障額を表示
			thirdGraphLabel = ratchetLabel;
			thirdGraphColor = 'red';
		} else if(stepupFlag){
			//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
			thirdGraphLabel = stepupLabel;
			thirdGraphColor = 'green';
		} else {
			//「ラチェット死亡保障額」「ステップアップ金額」が非表示の場合
			isDislpayThirdGraph = false;
		}
		if(String.isBlank(period)) return;
		
		//グラフ表示データ作成
		if (period.equals(BUTTON_LABEL_HALF_YEAR)) {
			List<E_SPRPF_Weekly__c> weeklyListMax = E_SPRPF_WeeklyDao.getRecByPolicyIdMax(parentId);
			if(weeklyListMax.isEmpty()){
				return;
			}
			Date enddate = E_Util.stringToDate(weeklyListMax.get(0).ZPRMSDTE__c);
			if (enddate == null) {
				return;
			}
 			String startDate = E_Util.date2YYYYMMDD(enddate.addMonths(-6));
			//半年のときはweeklyレコードを参照
			List<E_SPRPF_Weekly__c> weeklyList = E_SPRPF_WeeklyDao.getRecByPolicyId(parentId, startDate);
			for(E_SPRPF_Weekly__c weekly : weeklyList){
				String year = weekly.ZPRMSDTE__c.substring(0, 4);
				String month = weekly.ZPRMSDTE__c.substring(4, 6);
				String day = weekly.ZPRMSDTE__c.substring(6, 8);
				String promiseDate = year + '/' + month + '/' + day;
				if(ratchetFlag){
					//ラチェット死亡保障金額の推移表示フラグがtrueの場合、ラチェット死亡保障額を表示
					GraphData graphData = new GraphData(promiseDate, weekly.ZREVAMT__c, weekly.SUMINS__c, weekly.ZRCHTSUM__c, weekly.ZACTSUM__c);
					graphRecs.add(graphData);
				} else if(stepupFlag){
					//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
					GraphData graphData = new GraphData(promiseDate, weekly.ZREVAMT__c, weekly.SUMINS__c, weekly.ZSTUPSUM__c, weekly.ZACTSUM__c);
					graphRecs.add(graphData);
				} else {
					//「ラチェット死亡保障額」「ステップアップ金額」が非表示の場合
					GraphData graphData = new GraphData(promiseDate, weekly.ZREVAMT__c, weekly.SUMINS__c, 0, weekly.ZACTSUM__c);
					graphRecs.add(graphData);
				}
			}
			
		} else if (period.equals(BUTTON_LABEL_2_YEAR)) {
			//2年のときはmonthlyレコードを参照
			List<E_SPRPF_Monthly__c> monthlyListMax = E_SPRPF_MonthlyDao.getRecByPolicyIdMax(parentId);
			if (monthlyListMax.isEmpty()) {
				return;
			}
			Date endtwoYearsAgo = null;
			for(Integer i = 12; i > 0; i--){
				String apiMonth = E_Util.leftPad(i, 2, '0');
				//基準日がブランクならスキップ
				String zprmsdte = String.valueOf(monthlyListMax.get(0).get('ZPRMSDTE_'+apiMonth+'__c'));
				if(!String.isBlank(zprmsdte)) {
					endtwoYearsAgo = Date.newInstance(Integer.valueOf(monthlyListMax.get(0).TargetYear__c), i, 1);
					break;
				}
			}
			
			/*
			Datetime twoYearsAgo = System.now().addYears(-2);
			String targetYear = String.valueOf(twoYearsAgo.year());
			*/
			
			Date twoYearsAgo = endtwoYearsAgo.addYears(-2).addMonths(+1);
			String targetYear = String.valueOf(twoYearsAgo.year());
			List<E_SPRPF_Monthly__c> monthlyList = E_SPRPF_MonthlyDao.getRecByPolicyIdAndTargetYear(parentId, targetYear);
			for(E_SPRPF_Monthly__c monthly : monthlyList){
				//開始年の場合、ループの開始は現在月から
				Integer startMonth = 1;
				if(monthly.TargetYear__c.equals(targetYear)){
					startMonth = twoYearsAgo.month();
				}
				createGraphDataFromMonthly(monthly, startMonth);
			}
			
		} else if (period.equals(BUTTON_LABEL_ALL)) {
			//契約日以来のときはmonthlyレコードを参照
			List<E_SPRPF_Monthly__c> monthlyList = E_SPRPF_MonthlyDao.getRecByPolicyId(parentId);
			for(E_SPRPF_Monthly__c monthly : monthlyList){
				createGraphDataFromMonthly(monthly, 1);
			}
		}
	}
	
	//SPVA積立金Monthlyからグラフデータ作成
	private void createGraphDataFromMonthly(E_SPRPF_Monthly__c monthly, Integer startMonth){
		for(Integer i = startMonth; i <= 12; i++){
			/*
			String apiMonth = i.format();
			if(apiMonth.length() < 2){
				apiMonth = '0'+apiMonth;
			}
			*/
			String apiMonth = E_Util.leftPad(i, 2, '0');
			//基準日がブランクならスキップ
			String zprmsdte = String.valueOf(monthly.get('ZPRMSDTE_'+apiMonth+'__c'));
			if(String.isBlank(zprmsdte)) continue;
			
			String year = zprmsdte.substring(0, 4);
			String month = zprmsdte.substring(4, 6);
			String day = zprmsdte.substring(6, 8);
			String promiseDate = year + '/' + month + '/' + day;
			//String promiseDate = E_Util.getFormatDate(zprmsdte, 'yyyy/MM/dd');
			if(ratchetFlag){
				//ラチェット死亡保障金額の推移表示フラグがtrueの場合、ラチェット死亡保障額を表示
				GraphData graphData = new GraphData(promiseDate, (Decimal)monthly.get('ZREVAMT_'+month+'__c'), (Decimal)monthly.get('SUMINS_'+month+'__c'), (Decimal)monthly.get('ZRCHTSUM_'+month+'__c'), (Decimal)monthly.get('ZACTSUM_'+month+'__c'));
				graphRecs.add(graphData);
			} else if(stepupFlag){
				//ステップアップ情報表示フラグがtrueの場合、ステップアップ金額を表示
				GraphData graphData = new GraphData(promiseDate, (Decimal)monthly.get('ZREVAMT_'+month+'__c'), (Decimal)monthly.get('SUMINS_'+month+'__c'), (Decimal)monthly.get('ZSTUPSUM_'+month+'__c'), (Decimal)monthly.get('ZACTSUM_'+month+'__c'));
				graphRecs.add(graphData);
			} else {
				//「ラチェット死亡保障額」「ステップアップ金額」が非表示の場合
				GraphData graphData = new GraphData(promiseDate, (Decimal)monthly.get('ZREVAMT_'+month+'__c'), (Decimal)monthly.get('SUMINS_'+month+'__c'), 0, (Decimal)monthly.get('ZACTSUM_'+month+'__c'));
				graphRecs.add(graphData);
			}
		}
	}
	
	//半年ボタンを押下
	public PageReference doHalfYearButton(){
		period = BUTTON_LABEL_HALF_YEAR;
		createGraph();
        isSelectedPeriod = true;
		return null;
	}
	
	//2年ボタンを押下
	public PageReference do2YearButton(){
		period = BUTTON_LABEL_2_YEAR;
        isSelectedPeriod = true;
		createGraph();
		return null;
	}
	
	//以来ボタンを押下
	public PageReference doAllButton(){
		period = BUTTON_LABEL_ALL;
        isSelectedPeriod = true;
		createGraph();
		return null;
	}
}