@isTest(SeeAllData=false)
public with sharing class TestE_OfficeLkupExtender {

	private static testMethod void testOfficeLkup() {
		//テストデータ作成
		Account acc = new Account();

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(true, 'test', 'システム管理者');

		system.runAs(u){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(acc);
			E_OfficeLkupController controller = new E_OfficeLkupController(standardcontroller);
			E_OfficeLkupExtender extender = new E_OfficeLkupExtender(controller);
			controller.ipVal_dairitenjimushokana.SkyEditor2__Text__c = 'ﾃｽﾄ';
			extender.presearch();

			//確認
			System.assertEquals(controller.ipVal_dairitenjimushokana.SkyEditor2__Text__c, 'テスト');
		}


	}

}