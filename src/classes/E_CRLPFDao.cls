public with sharing class E_CRLPFDao {
    /** 顧客ロール:契約者 */
    public static final String ROLE_OW = 'OW';
    /** 顧客ロール:被保険者 */
    public static final String ROLE_LA = 'LA';
    /** 顧客ロール:被保険者の配偶者 */
    public static final String ROLE_AL = 'AL';
    /** 顧客ロール:被保険者の子供 */
    public static final String ROLE_CD = 'CD';
    /** 顧客ロール:受取人 */
    public static final String ROLE_BN = 'BN';
    /** 顧客ロール:質権者 */
    public static final String ROLE_NE = 'NE';
    /** 顧客ロール:団体 */
    public static final String ROLE_GR = 'GR';

    /**
     * 保険契約IDをキーに、指定した保険顧客情報をリスト形式で取得する
     * @param policyId: 保険契約ID
     * @param clrrrole: 顧客ロール
     * @return List<E_CRLPF__c>: 保険顧客情報リスト
     */
    public static List<E_CRLPF__c> getRecsByType (Id policyId,String clrrrole) {
        List<E_CRLPF__c> recs = [
            Select Id,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,Name,Contractor__c,
            E_Policy__c,CHDRNUM__c,CLNTNUM__c,CLRRROLE__c,BNYTYP__c,ZSEQUE__c,BNYPC__c,LONGDESC__c,
            ZCLNAME__c,ZCLKNAME__c,ZKNJSEX__c,DOB__c,CLTPCODE__c,ZCLADDR__c,ZCLKADDR__c,CLTPHONE01__c,
            CLTPHONE02__c,FAXNO__c,ADDRCD__c,ZMAINBNF__c,ANBCCD__c,ZWEBCNTT__c,VALIDFLAG__c,ZDATATYP__c,BNYTYPName__c,
            CRLPFCode__c From E_CRLPF__c
            Where E_Policy__r.id =: policyId AND CLRRROLE__c = :clrrrole AND VALIDFLAG__c != '3'
        ];
        return recs;
    }



    /**
     * 保険契約IDをキーに、指定した保険顧客情報を取得する。
     * 複数件存在する場合は、１件のみ取得する。
     * @param policyId: 保険契約ID
     * @param clrrrole: 顧客ロール
     * @return E_CRLPF__c: 保険顧客情報
     */
    public static E_CRLPF__c getRecByType (Id policyId,String clrrrole) {
        E_CRLPF__c rec = null;
        List<E_CRLPF__c> recs = [
            Select Id,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,Name,Contractor__c,
            E_Policy__c,CHDRNUM__c,CLNTNUM__c,CLRRROLE__c,BNYTYP__c,ZSEQUE__c,BNYPC__c,LONGDESC__c,
            ZCLNAME__c,ZCLKNAME__c,ZKNJSEX__c,DOB__c,CLTPCODE__c,ZCLADDR__c,ZCLKADDR__c,CLTPHONE01__c,
            CLTPHONE02__c,FAXNO__c,ADDRCD__c,ZMAINBNF__c,ANBCCD__c,ZWEBCNTT__c,VALIDFLAG__c,ZDATATYP__c,
            CRLPFCode__c From E_CRLPF__c
            Where E_Policy__r.id =: policyId AND CLRRROLE__c = :clrrrole AND VALIDFLAG__c != '3'
        ];
        if (recs.size() > 0) {
            rec = recs.get(0);
        }
        return rec;
    }
    
    /**
     * 受取人情報取得
     */
	public static List<E_CRLPF__c> getRecsForColi(Id policyId){
		return [Select 
					Id, ANBCCD__c, BNYTYPName__c, CLNTNUM__c, ZCLNAME__c, ZKNJSEX__c, BNYPC__c, Age__c, DOB__c, BNYTYP__c
				From
					E_CRLPF__c
				Where
					E_Policy__r.Id = :policyId
				And
					BNYTYP__c In :I_Const.COLI_BNYTYPS
				And
					CLRRROLE__c = :E_Const.CLRRROLE_BN
				And
					VALIDFLAG__c <> :E_Const.VALIDFLAG_3
				Order by 
					BNYTYP__c asc, BNYPC__c desc, CLNTNUM__c asc
		];
	}
	
	/**
	 * 質権情報取得
	 */
	public static List<E_CRLPF__c> getRecsByRoleNE(Id policyId){
		return [Select 
					Id, ZCLNAME__c 
				From 
					E_CRLPF__c 
				Where 
					CLRRROLE__c = :E_Const.CLRRROLE_NE 
				And 
					E_Policy__r.Id = :policyId
		];
	}
}