public with sharing class SC_Search_agWrapController extends AMS_AbstractExtender{
       
    // 表示フラグ
    public Boolean isViewSearch {get; set;}

    /**
     * Constractor
     */
    public SC_Search_agWrapController() {
        pgTitle = '代理店事務所検索（代理店自己点検）';
        isViewSearch = false;
    }
    
    /**
     * PageAction
     */
    public pageReference pageAction(){
    	/* 2018年度対応　リダイレクトしないように変更
        // AY権限　リダイレクト
        if(access.isEmployee() == false && access.isAuthAY()){
            if(String.isNotBlank(viewingOfficeCode)){
				List<SC_Office__c> office = [SELECT Id FROM SC_Office__c WHERE AccZAGCYNUM__c = :viewingOfficeCode AND FiscalYear__c = :System.Label.SC_FiscalYear];
                
                // 提出一覧へリダイレクト
                if(office != null && !office.isEmpty()){
                    PageReference pr = Page.E_SCSelfCompliances;
                    pr.getParameters().put('id', office.get(0).Id);
                    return pr;
                    
                // ログインユーザの事務所コードをキーにして、自主点検事務所レコードが取得できなかった場合エラー
                }else{
                	System.debug('■[Error pageAction : 対象自主点検事務所レコードが存在しない]');
                    pageMessages.addErrorMessage(SC_Const.ERR_MSG05_SCOFFICE_NONE);
                    return null;
                }
			}else{
				System.debug('■[Error pageAction : ID管理の照会者コードが不正]');
				pageMessages.addErrorMessage(SC_Const.ERR_MSG05_SCOFFICE_NONE);
				return null;
			}
		}
		*/

        isViewSearch = true;
        return null;
    }
}