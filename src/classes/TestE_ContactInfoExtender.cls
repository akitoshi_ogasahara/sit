@isTest(SeeAllData=false)
public with sharing class TestE_ContactInfoExtender {
	
	private static User testuser;
	private static User ChatterFreeUser;
	private static E_IDCPF__c idcpf;
	private static Account account;
	private static Contact contact;
	private static RecordType recordType; 
	private static datetime nowDateTime = system.now();
	private static contact insCon;
	static void init(){
		insertMessage();
		insertE_bizDataSyncLog();
		insertContact();
	}
	
	/* レコード1件の正常系  */ 
	private static testMethod void testStandard() {
		init();
		//インスタンス
		Pagereference  testpage = Page.E_ContactInfo;
		testpage.getParameters().put('id', inscon.ID);
		Test.setCurrentPage(testpage);
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(inscon);
		E_ContactInfoController controller = new E_ContactInfoController(standardcontroller);
		E_ContactInfoExtender extender = controller.getExtender();
		PageReference resultPage = extender.pageAction();
		system.assertEquals(null, resultPage);        
	}
	
	
	/*chatter free ユーザーでid管理を作成できないため、メソッドをコメントアウト
	/* レコードの権限チェック。  
	private static testMethod void testChatterFreeAuth() {
		init();
		createChatterFreeDataCommon();
		//インスタンス
		Pagereference  testpage = Page.E_ContactInfo;
		//system.runas(ChatterFreeUser){
			testpage.getParameters().put('id', ChatterFreeUser.ID);
			Test.setCurrentPage(testpage);
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(inscon);
			E_ContactInfoController controller = new E_ContactInfoController(standardcontroller);
			E_ContactInfoExtender extender = controller.getExtender();        
			extender.pageaction();
			//system.runas(ChatterFreeUser){
			//system.assertEquals(false, extender.doAuthContact(ChatterFreeUser.ID));//レコードへのアクセス権限がないことを確認する
			//}
		//}
		
	}
	**/
	/* doChangeAddress */ 
	private static testMethod void testDoChangeAddress() {
		init();
		//インスタンス
		Pagereference  testpage = Page.E_ContactInfo;
		testpage.getParameters().put('id', inscon.ID);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(inscon);
		E_ContactInfoController controller = new E_ContactInfoController(standardcontroller);
		E_ContactInfoExtender extender = controller.getExtender();
		Pagereference resultPref = extender.doChangeAddress();
		Pagereference pref = Page.E_ADRPFChange;
		pref.getparameters().put('conid', inscon.Id);
		system.assertEquals(resultPref.getUrl(), pref.getUrl());
	}    
	
	/* doChangeEmail */ 
	private static testMethod void testDoChangeEmail() {
		init();
		//インスタンス
		Pagereference  testpage = Page.E_ContactInfo;
		testpage.getParameters().put('id', inscon.ID);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(inscon);
		E_ContactInfoController controller = new E_ContactInfoController(standardcontroller);
		E_ContactInfoExtender extender = controller.getExtender();
		Pagereference resultPref = extender.doChangeEmail();
		pagereference expectedPref = Page.E_EMailAddressChange;
		//expectedPref.getParameters().put('conid', inscon.Id);
		system.assertEquals(resultPref.getUrl(), expectedPref.getUrl());
	}    
	
	/* doChangeStandardPw */ 
	private static testMethod void testDoChangeStandardPw() {
		init();
		//インスタンス
		Pagereference  testpage = Page.E_ContactInfo;
		testpage.getParameters().put('id', inscon.ID);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(inscon);
		E_ContactInfoController controller = new E_ContactInfoController(standardcontroller);
		E_ContactInfoExtender extender = controller.getExtender();
		Pagereference resultPref = extender.doChangeStandardPw();
		pagereference expectedPref = Page.E_StandardPasswordChange;
		//expectedPref.getParameters().put('conid', inscon.Id);
		system.assertEquals(resultPref.getUrl(), expectedPref.getUrl());
	}  
	
	/* doChangeProceduralPw */ 
	private static testMethod void testDoChangeProceduralPw() {
		init();
		//インスタンス
		Pagereference  testpage = Page.E_ContactInfo;
		testpage.getParameters().put('id', inscon.ID);
		Test.setCurrentPage(testpage);
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(inscon);
		E_ContactInfoController controller = new E_ContactInfoController(standardcontroller);
		E_ContactInfoExtender extender = controller.getExtender();
		Pagereference resultPref = extender.doChangeProceduralPw();
		pagereference expectedPref = Page.E_ProceduralPasswordChange;
		expectedPref.getParameters().put('conid', inscon.Id);
		system.assertEquals(resultPref.getUrl(), expectedPref.getUrl());
	}  
	
	private static void insertMessage(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
			String type = 'ディスクレーマー';
			String param = 'CIN|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'CIN|002';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			insert insMessage;
		}
	}
	
	/* test data */
	static void createDataCommon(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			
			// User
			profile	profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
			);
			insert testuser;
			// Account
			account = new Account(Name = 'testAccount');
			insert account;
			
			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id,E_dataSyncDate__c = nowDateTime);
			insert contact;
			
			//
			idcpf = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = testuser.Id,
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
			);
			insert idcpf;
		}        
	}
	/* test data */
	static void createChatterFreeDataCommon(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			
			// User
			profile	profile = [Select Id, Name,usertype From Profile Where Name = 'Chatter Free User' Limit 1];
			ChatterFreeUser = new User(
				Lastname = 'chatterFree'
				, Username = 'chatterFree@terrasky.ingtesting'
				, Email = 'chatterFree@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'chaFree'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='chaFree1'
			);
			insert ChatterFreeUser;
			// Account
			account = new Account(Name = 'testAccount');
			insert account;
			
			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id,E_dataSyncDate__c = nowDateTime);
			insert contact;
			
			//
			idcpf = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = ChatterFreeUser.Id,
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
			);
			insert idcpf;
		}        
	}
	
	private static void insertE_bizDataSyncLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
			insert log;
		}    
	}
	
	private static void insertContact(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			inscon = new contact();
			insCon.E_dataSyncDate__c = system.now();
			insCon.FirstName = 'テスト';
			insCon.LastName = '太郎';
			insert inscon;
		}
	}
	
	
}