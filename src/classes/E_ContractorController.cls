global with sharing class E_ContractorController extends SkyEditor2.SkyEditorPageBaseWithSharing {
	
	public E_Policy__c record {get{return (E_Policy__c)mainRecord;}}
	public with sharing class CanvasException extends Exception {}

	public Map<String,Map<String,Object>> appComponentProperty {get; set;}
	public E_ContractorExtender getExtender() {return (E_ContractorExtender)extender;}
	
	
	public koHihokenshaTab koHihokenshaTab {get; private set;}
	public shitukenshaTab shitukenshaTab {get; private set;}
	{
	setApiVersion(31.0);
	}
	public E_ContractorController(ApexPages.StandardController controller) {
		super(controller);

		appComponentProperty = new Map<String, Map<String, Object>>();
		Map<String, Object> tmpPropMap = null;

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_welcome','{!Extender.Welcome}');
        tmpPropMap.put('p_showClose','false');
        tmpPropMap.put('p_showBack','false');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
        tmpPropMap.put('p_isHideMenu','');
		tmpPropMap.put('Component__Width','200');
		tmpPropMap.put('Component__Height','90');
		tmpPropMap.put('Component__id','Component1348');
		tmpPropMap.put('Component__Name','ELogoHeader');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1348',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('menuNo','search_customer,inquiry_contract');
        tmpPropMap.put('isHideMenu','');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','119');
		tmpPropMap.put('Component__Height','600');
		tmpPropMap.put('Component__id','Component812');
		tmpPropMap.put('Component__Name','EMenu');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component812',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_title','表示内容の説明');
        tmpPropMap.put('p_escapeTitle','false');
        tmpPropMap.put('p_rendered','True');
        tmpPropMap.put('p_value','E_Info?menu=Help_E_Contractor');
        tmpPropMap.put('p_target','_nnlinkhelp');
        tmpPropMap.put('p_style','');
        tmpPropMap.put('p_styleClass','btn btn-standard ');
		tmpPropMap.put('Component__Width','80');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1349');
		tmpPropMap.put('Component__Name','EOutputLink');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1349',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.Contractor__r.E_CLTPF_DOB__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1591');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1591',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_postalCode','{!record.Contractor__r.E_CLTPF_CLTPCODE__c}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1342');
		tmpPropMap.put('Component__Name','EPostalCodeLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1342',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!record.Insured__r.E_CLTPF_DOB__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1021');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1021',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('dateString','{!koHihokenshaTab_item.record.DOB__c}');
        tmpPropMap.put('dateFmt','yyyy/MM/dd');
        tmpPropMap.put('all9','*');
        tmpPropMap.put('all0','*');
        tmpPropMap.put('p_isRendered','true');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','18');
		tmpPropMap.put('Component__id','Component1022');
		tmpPropMap.put('Component__Name','EDateLabel');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1022',tmpPropMap);

		tmpPropMap = new Map<String,Object>();
        tmpPropMap.put('id','');
        tmpPropMap.put('top','');
        tmpPropMap.put('left','');
        tmpPropMap.put('width','');
        tmpPropMap.put('height','');
        tmpPropMap.put('SIncludeOrder','System.Object');
        tmpPropMap.put('p_isIRIS','{!Extender.iris.isIRIS}');
		tmpPropMap.put('Component__Width','100');
		tmpPropMap.put('Component__Height','15');
		tmpPropMap.put('Component__id','Component1347');
		tmpPropMap.put('Component__Name','ECopyRight');
		tmpPropMap.put('Component__NameSpace','');
		tmpPropMap.put('Component__Top','0');
		tmpPropMap.put('Component__Left','0');
		appComponentProperty.put('Component1347',tmpPropMap);


		SObjectField f;

		f = Contact.fields.Name;
		f = Contact.fields.E_CLTPF_ZCLKNAME__c;
		f = Contact.fields.E_COMM_ZCLADDR__c;
		f = E_CRLPF__c.fields.CLRRROLEname__c;
		f = E_CRLPF__c.fields.ZCLNAME__c;
		f = E_CRLPF__c.fields.ZCLKNAME__c;
		f = E_CRLPF__c.fields.ZKNJSEX__c;
		f = E_CRLPF__c.fields.ANBCCD__c;
		f = E_CRLPF__c.fields.CLRRROLE__c;
		f = E_CRLPF__c.fields.VALIDFLAG__c;
		f = E_CRLPF__c.fields.ZSEQUE__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = E_Policy__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			
			mainQuery = new SkyEditor2.Query('E_Policy__c');
			mainQuery.addFieldAsOutput('RecordTypeId');
			mainQuery.addFieldAsOutput('Contractor__r.Name');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_ZCLKNAME__c');
			mainQuery.addFieldAsOutput('Contractor__r.E_COMM_ZCLADDR__c');
			mainQuery.addFieldAsOutput('Insured__r.E_CLTPF_ZCLKNAME__c');
			mainQuery.addFieldAsOutput('SpClassification__c');
			mainQuery.addFieldAsOutput('RecordType.DeveloperName');
			mainQuery.addFieldAsOutput('RecordType.Name');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLNTNUM__c');
			mainQuery.addFieldAsOutput('Contractor__r.LastName');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_ZKNJSEX__c');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_DOB__c');
			mainQuery.addFieldAsOutput('Contractor__r.Phone');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLTPHONE02__c');
			mainQuery.addFieldAsOutput('Contractor__r.FirstName');
			mainQuery.addFieldAsOutput('Contractor__r.E_CLTPF_CLTPCODE__c');
			mainQuery.addFieldAsOutput('COMM_CLNTNUM__c');
			mainQuery.addFieldAsOutput('COMM_CHDRNUM__c');
			mainQuery.addFieldAsOutput('COMM_CHDRNUM_LINK__c');
			mainQuery.addFieldAsOutput('SPVA_ZANNSTFLG__c');
			mainQuery.addFieldAsOutput('Insured__r.E_CLTPF_ZKNJSEX__c');
			mainQuery.addFieldAsOutput('Insured__r.E_CLTPF_DOB__c');
			mainQuery.addFieldAsOutput('COLI_ZVARFLAG__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			
			
			
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			
			koHihokenshaTab = new koHihokenshaTab(new List<E_CRLPF__c>(), new List<koHihokenshaTabItem>(), new List<E_CRLPF__c>(), null);
			listItemHolders.put('koHihokenshaTab', koHihokenshaTab);
			query = new SkyEditor2.Query('E_CRLPF__c');
			query.addFieldAsOutput('CLRRROLEname__c');
			query.addFieldAsOutput('ZCLNAME__c');
			query.addFieldAsOutput('ZCLKNAME__c');
			query.addFieldAsOutput('ZKNJSEX__c');
			query.addFieldAsOutput('ANBCCD__c');
			query.addFieldAsOutput('DOB__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('koHihokenshaTab', 'E_Policy__c');
			koHihokenshaTab.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('koHihokenshaTab', query);
			
			shitukenshaTab = new shitukenshaTab(new List<E_CRLPF__c>(), new List<shitukenshaTabItem>(), new List<E_CRLPF__c>(), null);
			listItemHolders.put('shitukenshaTab', shitukenshaTab);
			query = new SkyEditor2.Query('E_CRLPF__c');
			query.addFieldAsOutput('ZCLNAME__c');
			query.addWhere('E_Policy__c', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
			relationFields.put('shitukenshaTab', 'E_Policy__c');
			shitukenshaTab.queryRelatedEvent = False;
			query.limitRecords(500);
			queryMap.put('shitukenshaTab', query);
			
			registRelatedList('E_CRLPFs__r', 'koHihokenshaTab');
			registRelatedList('E_CRLPFs__r', 'shitukenshaTab');
			
			SkyEditor2.Query koHihokenshaTabQuery = queryMap.get('koHihokenshaTab');
			koHihokenshaTabQuery.addWhereIfNotFirst('AND');
			koHihokenshaTabQuery.addWhere('(  ( CLRRROLE__c = \'AL\' OR CLRRROLE__c = \'CD\' )  AND VALIDFLAG__c != \'3\')');
			koHihokenshaTabQuery.addSort('CLRRROLE__c',True,True).addSort('ZSEQUE__c',True,True);
			SkyEditor2.Query shitukenshaTabQuery = queryMap.get('shitukenshaTab');
			shitukenshaTabQuery.addWhereIfNotFirst('AND');
			shitukenshaTabQuery.addWhere('( CLRRROLE__c = \'NE\' AND VALIDFLAG__c != \'3\')');
			p_showHeader = false;
			p_sidebar = false;
			addInheritParameter('RecordTypeId', 'RecordType');
			extender = new E_ContractorExtender(this);
			init();
			
			koHihokenshaTab.extender = this.extender;
			shitukenshaTab.extender = this.extender;
			if (record.Id == null) {
				
				saveOldValues();
				
			}

			
			extender.init();
			
		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}
	

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	global with sharing class koHihokenshaTabItem extends SkyEditor2.ListItem {
		public E_CRLPF__c record{get; private set;}
		@TestVisible
		koHihokenshaTabItem(koHihokenshaTab holder, E_CRLPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class koHihokenshaTab extends SkyEditor2.ListItemHolder {
		public List<koHihokenshaTabItem> items{get; private set;}
		@TestVisible
			koHihokenshaTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<koHihokenshaTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new koHihokenshaTabItem(this, (E_CRLPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	global with sharing class shitukenshaTabItem extends SkyEditor2.ListItem {
		public E_CRLPF__c record{get; private set;}
		@TestVisible
		shitukenshaTabItem(shitukenshaTab holder, E_CRLPF__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
				
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class shitukenshaTab extends SkyEditor2.ListItemHolder {
		public List<shitukenshaTabItem> items{get; private set;}
		@TestVisible
			shitukenshaTab(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<shitukenshaTabItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new shitukenshaTabItem(this, (E_CRLPF__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}
	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}