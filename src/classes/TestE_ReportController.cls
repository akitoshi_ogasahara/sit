@isTest(seealldata = false)
public with sharing class TestE_ReportController {
    
    //reportオブジェクトはreadonlyのため、テストメソッド内でデータ作成できない。
    
    private static testmethod void  errorTest(){
        PageReference resultPref;
        PageReference pref = Page.E_Report;
        pref.getParameters().put('rep','error');
        test.setCurrentPage(pref);
        E_ReportController controller  = new E_ReportController();        
        resultPref = controller.pageAction();
        system.debug(resultpref);
        system.assert(resultPref.getURL().contains(Page.E_ErrorPage.getURL()));
    }
	
}