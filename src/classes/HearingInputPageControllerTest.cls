/**
* クラス名		:  HearingInputPageControllerTest
* クラス概要	:  ヒアリング情報詳細画面(テスト)
* @created	:  2014/01/21 Khanh Vo Quoc
* @modified	:   
*/
@isTest
private class HearingInputPageControllerTest {
	/**
	* testMethodHearingInputPageController
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodHearingInputPageController() {
		// ファイナンス情報を作成
		Finance__c fi = createFinance(1);
		// 業界平均マスタを作成
		createIndustryAverage();
		// 法人税定義を作成
		createTaxDefinition();
		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// テスト実行
		Test.startTest();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		Test.stopTest();
		System.assertEquals(1, ct.finance.Term__c);
	}
	/**
	* testMethodHearingInputPageController1
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodHearingInputPageController1() {
		// ファイナンス情報を作成
		Finance__c fi = createFinance(1);
		// 業界平均マスタを作成
		createIndustryAverage();
		// 法人税定義を作成
		createTaxDefinition();
		/* カレントページにパラメータをセットする*/
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, '123456');
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, '123456');
		// テスト実行
		Test.startTest();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		Test.stopTest();
		System.assertEquals(null, ct.finance.Term__c);
		System.assertEquals(null, ct.finance.H_RD__c);
		System.assertEquals(null, ct.finance.H_JASGD__c);
		System.assertEquals(null, ct.finance.H_LO__c);
		System.assertEquals(null, ct.finance.H_DBAC__c);
		System.assertEquals(null, ct.finance.H_UG__c);
		System.assertEquals(null, ct.finance.H_DBAI__c);
		System.assertEquals(null, ct.finance.H_BRM__c);
		System.assertEquals(null, ct.finance.H_BRP__c);
		System.assertEquals(null, ct.finance.H_BFLEM__c);
		System.assertEquals(null, ct.finance.H_BFLET__c);
		System.assertEquals(null, ct.finance.H_RE__c);
		System.assertEquals(null, ct.finance.H_CRAA__c);
		System.assertEquals(null, ct.finance.H_ERB__c);
		System.assertEquals(null, ct.finance.H_ORB__c);
	}
	/**
	* testMethodHearingInputPageController2
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodHearingInputPageController2() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(null);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, null);
		// テスト実行
		Test.startTest();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_CustomerCheck));
	}
	/**
	* testMethodHearingInputPageController3
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodHearingInputPageController3() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(null);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, null);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// テスト実行
		Test.startTest();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		Test.stopTest();
		System.assertEquals(null, ct.finance.Term__c);
		System.assertEquals(0, ct.finance.H_RD__c);
		System.assertEquals(0, ct.finance.H_JASGD__c);
		System.assertEquals(0, ct.finance.H_LO__c);
		System.assertEquals(0, ct.finance.H_DBAC__c);
		System.assertEquals(0, ct.finance.H_UG__c);
		System.assertEquals(0, ct.finance.H_DBAI__c);
		System.assertEquals(0, ct.finance.H_BRM__c);
		System.assertEquals(0, ct.finance.H_BRP__c);
		System.assertEquals(0, ct.finance.H_BFLEM__c);
		System.assertEquals(0, ct.finance.H_BFLET__c);
		System.assertEquals(0, ct.finance.H_RE__c);
		System.assertEquals(0, ct.finance.H_CRAA__c);
		System.assertEquals(0, ct.finance.H_ERB__c);
		System.assertEquals(0, ct.finance.H_ORB__c);
	}
	/**
	* testMethodCheckInput1
	* 入力チェック(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCheckInput1() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(null);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_H_NotInput));
	}
	/**
	* testMethodCheckInput2
	* 入力チェック(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCheckInput2() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(-2);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		ct.finance = fi;
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_H_Minus));
	}
	/**
	* testMethodCheckExist
	* 入力チェック(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCheckExist() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		fi.Hearing__c = true;
		update fi;
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		ct.finance = fi.clone(false, false, false);
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_H_Overlap));
	}
	/**
	* testMethodCheckCustomSetting
	* カスタム設定取得チェック(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCheckCustomSetting() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_SetupCheck));
	}
	/**
	* testMethodCheckCustomer
	* 顧客チェック(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCheckCustomer() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, 'a0N00000001');
		// 法人税定義を作成
		createTaxDefinition();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_Error_CustomerCheck));
	}
	/**
	* testMethodCheckIndustry
	* 業界平均マスタチェック(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCheckIndustry() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// 法人税定義を作成
		createTaxDefinition();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		String strMsgError = String.valueOf(Apexpages.getMessages().get(0));
		System.assertEquals(true, strMsgError.contains(Label.Msg_ErrorIndustryCheck));
	}
	/**
	* testMethodUpdateData
	* 「登録」ボタン処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodUpdateData() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		// 業界平均マスタを作成
		createIndustryAverage();
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// 法人税定義を作成
		createTaxDefinition();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		fi.Term__c = 1;
		fi.H_RD__c = 2;		// 予想売上下落率
		fi.H_JASGD__c = 2	;// 連帯保証債務額
		fi.H_LO__c = 2;		// 役員借入金
		fi.H_DBAC__c = 2;	// 死亡保障額(法人契約)
		fi.H_UG__c = 2;		// 含み益
		fi.H_DBAI__c = 2;	// 死亡保障額(個人契約)
		fi.H_BRM__c = 2;	// 銀行返済額(月)
		fi.H_BRP__c = 2;	// 銀行返済額　準備額
		fi.H_BFLEM__c = 2;	// 遺族生活資金(月)
		fi.H_BFLET__c = 2;	// 遺族生活資金(必要期間)
		fi.H_RE__c = 2;		// 現在の社長の報酬/年
		fi.H_CRAA__c = 2;	// 社長の報酬減額可能額/年
		fi.H_ERB__c = 2;	// 従業員退職金(予定額)
		fi.H_ORB__c = 2;	// 役員退職金予定額
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		Finance__c finance = HearingDAO.getFinanceById(fi.Id);
		System.assertEquals(1, finance.Term__c);
		System.assertEquals(2, finance.H_RD__c);
		System.assertEquals(2, finance.H_JASGD__c);
		System.assertEquals(2, finance.H_LO__c);
		System.assertEquals(2, finance.H_DBAC__c);
		System.assertEquals(2, finance.H_UG__c);
		System.assertEquals(2, finance.H_DBAI__c);
		System.assertEquals(2, finance.H_BRM__c);
		System.assertEquals(2, finance.H_BRP__c);
		System.assertEquals(2, finance.H_BFLEM__c);
		System.assertEquals(2, finance.H_BFLET__c);
		System.assertEquals(2, finance.H_RE__c);
		System.assertEquals(2, finance.H_CRAA__c);
		System.assertEquals(2, finance.H_ERB__c);
		System.assertEquals(2, finance.H_ORB__c);
	}
	/**
	* testMethodInsertData1
	* 「登録」ボタン処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodInsertData1() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		// 業界平均マスタを作成
		createIndustryAverage();
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, null);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// 法人税定義を作成
		createTaxDefinition();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		fi.Term__c = 5;
		fi.H_RD__c = 2;		// 予想売上下落率
		fi.H_JASGD__c = 2	;// 連帯保証債務額
		fi.H_LO__c = 2;		// 役員借入金
		fi.H_DBAC__c = 2;	// 死亡保障額(法人契約)
		fi.H_UG__c = 2;		// 含み益
		fi.H_DBAI__c = 2;	// 死亡保障額(個人契約)
		fi.H_BRM__c = 2;	// 銀行返済額(月)
		fi.H_BRP__c = 2;	// 銀行返済額　準備額
		fi.H_BFLEM__c = 2;	// 遺族生活資金(月)
		fi.H_BFLET__c = 2;	// 遺族生活資金(必要期間)
		fi.H_RE__c = 2;		// 現在の社長の報酬/年
		fi.H_CRAA__c = 2;	// 社長の報酬減額可能額/年
		fi.H_ERB__c = 2;	// 従業員退職金(予定額)
		fi.H_ORB__c = 2;	// 役員退職金予定額
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		Finance__c fi1 = [Select Id From Finance__c Where Term__c = 5];
		Finance__c finance = HearingDAO.getFinanceById(fi1.Id);
		System.assertEquals(5, finance.Term__c);
		System.assertEquals(2, finance.H_RD__c);
		System.assertEquals(2, finance.H_JASGD__c);
		System.assertEquals(2, finance.H_LO__c);
		System.assertEquals(2, finance.H_DBAC__c);
		System.assertEquals(2, finance.H_UG__c);
		System.assertEquals(2, finance.H_DBAI__c);
		System.assertEquals(2, finance.H_BRM__c);
		System.assertEquals(2, finance.H_BRP__c);
		System.assertEquals(2, finance.H_BFLEM__c);
		System.assertEquals(2, finance.H_BFLET__c);
		System.assertEquals(2, finance.H_RE__c);
		System.assertEquals(2, finance.H_CRAA__c);
		System.assertEquals(2, finance.H_ERB__c);
		System.assertEquals(2, finance.H_ORB__c);
	}
	/**
	* testMethodInsertData2
	* 「登録」ボタン処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodInsertData2() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		// 業界平均マスタを作成
		createIndustryAverage();
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_ID, fi.Id);
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// 法人税定義を作成
		createTaxDefinition();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		delete [Select Id From Finance__c Where Id =: fi.Id];
		fi.Term__c = 5;
		fi.H_RD__c = 2;		// 予想売上下落率
		fi.H_JASGD__c = 2	;// 連帯保証債務額
		fi.H_LO__c = 2;		// 役員借入金
		fi.H_DBAC__c = 2;	// 死亡保障額(法人契約)
		fi.H_UG__c = 2;		// 含み益
		fi.H_DBAI__c = 2;	// 死亡保障額(個人契約)
		fi.H_BRM__c = 2;	// 銀行返済額(月)
		fi.H_BRP__c = 2;	// 銀行返済額　準備額
		fi.H_BFLEM__c = 2;	// 遺族生活資金(月)
		fi.H_BFLET__c = 2;	// 遺族生活資金(必要期間)
		fi.H_RE__c = 2;		// 現在の社長の報酬/年
		fi.H_CRAA__c = 2;	// 社長の報酬減額可能額/年
		fi.H_ERB__c = 2;	// 従業員退職金(予定額)
		fi.H_ORB__c = 2;	// 役員退職金予定額
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		Finance__c fi1 = [Select Id From Finance__c Where Term__c = 5];
		Finance__c finance = HearingDAO.getFinanceById(fi1.Id);
		System.assertEquals(5, finance.Term__c);
		System.assertEquals(2, finance.H_RD__c);
		System.assertEquals(2, finance.H_JASGD__c);
		System.assertEquals(2, finance.H_LO__c);
		System.assertEquals(2, finance.H_DBAC__c);
		System.assertEquals(2, finance.H_UG__c);
		System.assertEquals(2, finance.H_DBAI__c);
		System.assertEquals(2, finance.H_BRM__c);
		System.assertEquals(2, finance.H_BRP__c);
		System.assertEquals(2, finance.H_BFLEM__c);
		System.assertEquals(2, finance.H_BFLET__c);
		System.assertEquals(2, finance.H_RE__c);
		System.assertEquals(2, finance.H_CRAA__c);
		System.assertEquals(2, finance.H_ERB__c);
		System.assertEquals(2, finance.H_ORB__c);
	}
	/**
	* testMethodInsertData3
	* 「登録」ボタン処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodInsertData3() {
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		fi = createFinance(2);
		// 業界平均マスタを作成
		createIndustryAverage();
		ApexPages.currentPage().getParameters().put(CommonConst.STR_PARAMETER_CUID, fi.Customer__c);
		// 法人税定義を作成
		createTaxDefinition();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		fi.Term__c = 5;
		fi.H_RD__c = 2;		// 予想売上下落率
		fi.H_JASGD__c = 2	;// 連帯保証債務額
		fi.H_LO__c = 2;		// 役員借入金
		fi.H_DBAC__c = 2;	// 死亡保障額(法人契約)
		fi.H_UG__c = 2;		// 含み益
		fi.H_DBAI__c = 2;	// 死亡保障額(個人契約)
		fi.H_BRM__c = 2;	// 銀行返済額(月)
		fi.H_BRP__c = 2;	// 銀行返済額　準備額
		fi.H_BFLEM__c = 2;	// 遺族生活資金(月)
		fi.H_BFLET__c = 2;	// 遺族生活資金(必要期間)
		fi.H_RE__c = 2;		// 現在の社長の報酬/年
		fi.H_CRAA__c = 2;	// 社長の報酬減額可能額/年
		fi.H_ERB__c = 2;	// 従業員退職金(予定額)
		fi.H_ORB__c = 2;	// 役員退職金予定額
		ct.finance = fi;
		// テスト実行
		Test.startTest();
		ct.upsertData();
		Test.stopTest();
		Finance__c fi1 = [Select Id From Finance__c Where Term__c = 5];
		Finance__c finance = HearingDAO.getFinanceById(fi1.Id);
		System.assertEquals(5, finance.Term__c);
		System.assertEquals(2, finance.H_RD__c);
		System.assertEquals(2, finance.H_JASGD__c);
		System.assertEquals(2, finance.H_LO__c);
		System.assertEquals(2, finance.H_DBAC__c);
		System.assertEquals(2, finance.H_UG__c);
		System.assertEquals(2, finance.H_DBAI__c);
		System.assertEquals(2, finance.H_BRM__c);
		System.assertEquals(2, finance.H_BRP__c);
		System.assertEquals(2, finance.H_BFLEM__c);
		System.assertEquals(2, finance.H_BFLET__c);
		System.assertEquals(2, finance.H_RE__c);
		System.assertEquals(2, finance.H_CRAA__c);
		System.assertEquals(2, finance.H_ERB__c);
		System.assertEquals(2, finance.H_ORB__c);
	}
	/**
	* testMethodClearData
	* べてクリア」リンク処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodClearData(){
		// ファイナンス情報を作成
		Finance__c fi = new Finance__c();
		// コントローラを作成
		HearingInputPageController ct = new HearingInputPageController();
		// テスト実行
		Test.startTest();
		ct.clearData();
		Test.stopTest();
		System.assertEquals(null, ct.finance.H_RD__c);
		System.assertEquals(null, ct.finance.H_JASGD__c);
		System.assertEquals(null, ct.finance.H_LO__c);
		System.assertEquals(null, ct.finance.H_DBAC__c);
		System.assertEquals(null, ct.finance.H_UG__c);
		System.assertEquals(null, ct.finance.H_DBAI__c);
		System.assertEquals(null, ct.finance.H_BRM__c);
		System.assertEquals(null, ct.finance.H_BRP__c);
		System.assertEquals(null, ct.finance.H_BFLEM__c);
		System.assertEquals(null, ct.finance.H_BFLET__c);
		System.assertEquals(null, ct.finance.H_RE__c);
		System.assertEquals(null, ct.finance.H_CRAA__c);
		System.assertEquals(null, ct.finance.H_ERB__c);
		System.assertEquals(null, ct.finance.H_ORB__c);
	}
	/**
	* testMethodCancel
	* ページ初期化処理(テスト)
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static testMethod void testMethodCancel() {
		// ファイナンス情報を作成
		Finance__c fi = createFinance(1);
		// 業界平均マスタを作成
		createIndustryAverage();
		// 法人税定義を作成
		createTaxDefinition();
		HearingInputPageController ct = new HearingInputPageController();
		// テスト実行
		Test.startTest();
		// コントローラを作成
		Pagereference pageHome = ct.cancel();
		Test.stopTest();
		// URLパラメータマップを取得
		String urlHome = URL.getSalesforceBaseUrl().toExternalForm();
		System.assert(true, pageHome.getUrl().contains(urlHome));
	}
	/**
	* createCustomer()
	* 顧客を作成
	* @return: なし
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static Customer__c createCustomer(String name){
		// 顧客
		Customer__c customs = new Customer__c();
		// 顧客名
		customs.Name = name;
		// 業界平均マスタ
		customs.Industry__c = 'Khanh-test';
		// 代理店
		// 代理店【INGAgency】を作成
		AgencyForINGAgency__c agcINGA = new AgencyForINGAgency__c();
		agcINGA.Name = 'BSPLInput-test-test';
		insert agcINGA;
		customs.AgencyForINGAgency__c = agcINGA.Id;
		return customs;
	}
	/**
	* createCustomer()
	* ファイナンス情報を作成
	* @return: ファイナンス情報
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static Finance__c createFinance(Integer intOption){
		// ファイナンス情報を作成
		Customer__c customs = createCustomer('khanh-test');
		insert customs;
		Finance__c finance = new Finance__c();
		// 決算期(前期)
		finance.Term__c = 1;
		if(intOption != null){
			finance.Term__c = 1 * intOption;
			finance.H_RD__c = 2 * intOption;	// 予想売上下落率
			finance.H_JASGD__c = 2 * intOption;// 連帯保証債務額
			finance.H_LO__c = 2 * intOption;	// 役員借入金
			finance.H_DBAC__c = 2 * intOption;	// 死亡保障額(法人契約)
			finance.H_UG__c = 2 * intOption;	// 含み益
			finance.H_DBAI__c = 2 * intOption;	// 死亡保障額(個人契約)
			finance.H_BRM__c = 2 * intOption;	// 銀行返済額(月)
			finance.H_BRP__c = 2 * intOption;	// 銀行返済額　準備額
			finance.H_BFLEM__c = 2 * intOption;	// 遺族生活資金(月)
			finance.H_BFLET__c = 2 * intOption;	// 遺族生活資金(必要期間)
			finance.H_RE__c = 2 * intOption;	// 現在の社長の報酬/年
			finance.H_CRAA__c = 2 * intOption;	// 社長の報酬減額可能額/年
			finance.H_ERB__c = 2 * intOption;	// 従業員退職金(予定額)
			finance.H_ORB__c = 2 * intOption;	// 役員退職金予定額
		}
		finance.Customer__c = customs.id; // 顧客名
		insert finance;
		return finance;
	}
	/**
	* createIndustryAverage()
	* 業界平均マスタを作成
	* @return: 業界平均マスタ
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static IndustryAverage__c createIndustryAverage(){
		IndustryAverage__c industry = new IndustryAverage__c();
		industry.Name__c = 'Khanh-test';
		industry.ROA__c = 10;	// 業界平均マスタ.ROA（総資産経常利益率）
		industry.ROE__c = 10;	// 業界平均マスタ.ROE(自己資本当期純利益率）
		industry.ROS__c = 10;	// 業界平均マスタ.ROS（売上高当期純利益率）
		industry.ICR__c = 10;	// 業界平均マスタ.インタレスト・カバレッジ・レシオ
		industry.DR__c = 10;	// 業界平均マスタ.債務償還年数
		industry.CAR__c = 10;	// 業界平均マスタ.自己資本比率
		industry.LR__c = 10;	// 業界平均マスタ.手元流動性比率
		industry.ITO__c = 10;	// 業界平均マスタ.棚卸資産回転率
		industry.QD__c = 10;	// 業界平均マスタ.当座比率
		industry.APTO__c = 10;	// 業界平均マスタ.買入債務回転率
		industry.RTO__c = 10;	// 業界平均マスタ.売上債権回転率
		insert industry;
		return industry;
	}
	/**
	* createCustomSetting()
	* カスタム設定。法人税定義を作成
	* @return: カスタム設定。法人税定義
	* @created: 2014/01/21 Khanh Vo Quoc
	*/
	static TaxDefinition__c createTaxDefinition(){
		TaxDefinition__c taxDf = new TaxDefinition__c();
		taxDf.From__c = Date.valueOf('2014-01-20');	// 法人税定義.施行日
		taxDf.NRate__c = 10;	// 法人税定義.必要倍率	
		taxDf.TaxRate__c = 10;	// 法人税定義.法人税率
		insert taxDf;
		return taxDf;
	}
}