/**
 * 
 */
@isTest
private class TestE_CHTPFDao {

	/**
	 * List<E_CHTPF__c> getRecsByCHDRAndAGNT (Set<String> keys)
	 */
    static testMethod void getRecsByCHDRAndAGNT_test01() {
    	Set<String> keys = new Set<String>{'12345'};
    	
        Test.startTest();
		List<E_CHTPF__c> recs = E_CHTPFDao.getRecsByCHDRAndAGNT(keys);
        Test.stopTest();
    }
    
	/**
	 * List<E_CHTPF__c> getRecsByPlcyIds (Set<Id> keys)
	 */
    static testMethod void getRecsByPlcyIds_test01() {
    	E_Policy__c policy = createPolicy();
    	Set<Id> pids = new Set<Id>{policy.Id};
    	
        Test.startTest();
		List<E_CHTPF__c> recs = E_CHTPFDao.getRecsByPlcyIds(pids);
        Test.stopTest();
    }
    
    /** Data **********************************************************/
    private static E_Policy__c createPolicy(){
		// Account 代理店格
		Account ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
		insert ahAcc;

		// Account 事務所
		Account ayAcc = new Account(
				Name = 'office1'
				,ParentId = ahAcc.Id
				,E_CL2PF_ZAGCYNUM__c = 'ay001'
				,E_COMM_VALIDFLAG__c = '1'
		);
		insert ayAcc;

		// Contact 募集人
		Contact atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
		atCon.E_CL3PF_AGNTNUM__c = 'at001';
		atCon.email = 'fstest@terrasky.ingtesting';
		insert atCon;
		
		//保険契約ヘッダ（個人タイプ）
		E_Policy__c policy = new E_Policy__c();
		policy = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');
		
		return policy;
    }
}