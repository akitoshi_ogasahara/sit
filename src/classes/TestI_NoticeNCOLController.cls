/**
 *
 */
@isTest
private class TestI_NoticeNCOLController {

	/**
	 * PageReference PageAction()
	 */
    static testMethod void pageAction_001() {
    	// Data
    	createData(5);

        Test.startTest();

        I_NoticencolController controller = new I_NoticencolController();
        controller.pageAction();

        Test.stopTest();

        // assertion
        System.assertEquals(false, controller.sortIsAsc);
        System.assertEquals('response', controller.sortType);
        System.assertEquals(E_DownloadNoticeConst.DH_FORMNO_NOTICE_NCOL, controller.dlFormNo);
    }

    /*
     *フィルターモード
     */
    static testMethod void filterMode() {
    	//代理店格作成
		Account distribute = TestI_TestUtil.createAccount(true,null);
		//事務所作成
		Account office = TestI_TestUtil.createAccount(true,distribute);

		//募集人コードが5桁の募集人作成
		Contact agentA = TestI_TestUtil.createContact(false,office.id,'募集人A');
		agentA.E_CL3PF_AGNTNUM__c = '12345';
		agentA.E_CL3PF_VALIDFLAG__c = '2';
		insert agentA;
		
		//ユーザを作成し無効化
		User userA = TestI_TestUtil.createAgentUser(false, 'agentA', 'E_PartnerCommunity', agentA.Id);
		userA.IsActive = false;
		insert userA;

		//有効で5桁+|付きの募集人作成
		Contact agentB = TestI_TestUtil.createContact(false,office.id,'募集人B');
		agentB.E_CL3PF_AGNTNUM__c = '12345|67890';
		agentB.E_CL3PF_VALIDFLAG__c = '1';
		insert agentB;
		
		//ユーザを作成し有効化
		User userB = TestI_TestUtil.createAgentUser(false, 'agentB', 'E_PartnerCommunity', agentB.Id);
		userB.IsActive = true;
		insert userB;

		//ID管理を付与
		E_IDCPF__c idMng = TestI_TestUtil.createIDCPF(true, userB.Id, 'AT');

		//保険契約ヘッダ（個人タイプ）
		E_Policy__c policy = new E_Policy__c();
		policy = TestE_TestUtil.createPolicy(false, agentA.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');
		policy.MainAgent__c = agentA.Id;
		insert policy;

		//販売取扱者
		E_CHTPF__c chtpf = new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = agentA.E_CL3PF_AGNTNUM__c);
		insert chtpf;

		//対象月
		date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
			Date d = targetDate;
			E_NCOL__c ncol = new E_NCOL__c(
				ParentAccount__c = distribute.Id
				, Account__c = office.Id
				, AGNT__c = agentA.E_CL3PF_AGNTNUM__c
				, Contact__c = agentA.Id
				, E_Policy__c = policy.Id
				, E_CHTPF__c = chtpf.Id
				, YYYY__c = d.year()
				, MM__c = d.month()
				, ZJINTSEQ__c = '1'
				);

		insert ncol;

		//実行ユーザ作成
		User runUs = TestE_TestUtil.createUser(true,'testUser','システム管理者');
    	//ID管理を付与
		E_IDCPF__c runUsIdMng = TestI_TestUtil.createIDCPF(false, runUs.Id, 'EP');
		runUsIdMng.ZINQUIRR__c = 'BR**';
		insert runUsIdMng;
		System.runAs(runUs){
			//=====================テスト開始=====================
	        Test.startTest();

	        I_NoticencolController controller = new I_NoticencolController();
	        E_IRISHandler iris = E_IRISHandler.getInstance();
			//募集人Bで参照
			iris.setPolicyCondition('agent', '募集人B', agentB.id, 'breadCrumb');
	        controller.pageAction();
	        //0件
	        Integer totalRows = controller.rowCnt;
	        system.assertEquals(1,totalRows);
	        //=====================テスト終了=====================
	        Test.stopTest();
	    }
	}

	/** data */
	private static void createData(Integer dataCnt){
		// Account 代理店格
		Account ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
		insert ahAcc;

		// Account 事務所
		Account ayAcc = new Account(
				Name = 'office1'
				,ParentId = ahAcc.Id
				,E_CL2PF_ZAGCYNUM__c = 'ay001'
				,E_COMM_VALIDFLAG__c = '1'
		);
		insert ayAcc;

		// Contact 募集人
		Contact atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
		atCon.E_CL3PF_AGNTNUM__c = 'at001';
		atCon.email = 'fstest@terrasky.ingtesting';
		insert atCon;

		//保険契約ヘッダ（個人タイプ）
		E_Policy__c policy = new E_Policy__c();
		policy = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');

		//販売取扱者
		E_CHTPF__c chtpf = new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
		insert chtpf;

		//保険料請求予告通知
		List<E_NCOL__c> ncols = new List<E_NCOL__c>();
		//対象月
		date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(E_DownloadNoticeConst.BIZSYNC_KIND_NOTICE_NCOL);
		for(Integer i = 0; i < dataCnt; i++){
			Date d = targetDate.addMonths(i*-1);
			E_NCOL__c ncol = new E_NCOL__c(
				ParentAccount__c = ahAcc.Id
				, Account__c = ayAcc.Id
				, AGNT__c = atCon.E_CL3PF_AGNTNUM__c
				, Contact__c = atCon.Id
				, E_Policy__c = policy.Id
				, E_CHTPF__c = chtpf.Id
				, YYYY__c = d.year()
				, MM__c = d.month()
			);
			ncols.add(ncol);
		}
		insert ncols;
	}
}