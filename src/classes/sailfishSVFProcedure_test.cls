@isTest
private class sailfishSVFProcedure_test{
    @isTest static void testCallout() {
      
        User test1 = sailfish.fetchUser()  ;
        List<Sailfish_Onboarding__c> test2 = sailfish.getOnboardingRecordAllFromName(''); 
    	List<sObject> test3 = sailfish.fetchLookUpValues('','User') ; 
        List<sObject> test12 = sailfish.fetchLookUpValuesAccessId('','Sailfish_Onboarding__c') ; 
        
        Sailfish_Onboarding__c testRecord = new Sailfish_Onboarding__c(Form_Id__c='Test') ; 
        sailfish.insertOnboardingRecordApex(testRecord) ; 
        sailfish.updateOnboardingRecordApex(testRecord) ; 
        Sailfish_Results__c resultsRecord = new Sailfish_Results__c(Sailfish_Onboarding__c = testRecord.Id ) ; 
        sailfish.saveResultsRecord(resultsRecord) ;
        String idTest1 = testRecord.id ; 
        List<Sailfish_Results__c> test70 = sailfish.getResultsByOnboarding(); 
        List<Sailfish_Results__c> test71 = sailfish.getSearchRulesetResults(idTest1) ;
        List<Sailfish_Results__c> test72 = sailfish.getSearchRuleResults(idTest1)  ;

        List<Sailfish_Onboarding__c> test4 = sailfish.getOnboardingRecordFromId('') ; 
        List<Sailfish_Onboarding__c> test5 = sailfish.getOnboardingRecordFromName('') ; 
        List<Sailfish_Results__c> test6 = sailfish.getResultsByOnboarding() ; 
        List <Sailfish_Results__c> test9 = sailfish.getSearchDoctorResults('') ; 
        List<Sailfish_Results__c> test10 = sailfish.getLastResults() ; 
        List<Sailfish_Onboarding__c> test11 = sailfish.getOnboardingRecordFromAccessId('') ; 
 
        System.debug('idTest1='+idTest1);
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new SailfishMockHttpResponseGenerator_test());

        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
       // HttpResponse res = sailfishSVFProcedure().execute(idTest1);
       String aa = new sailfishSVFProcedure().execute(idTest1);

        // Verify response received contains fake values
        /*
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        String expectedValue = '{"example":"test"}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, res.getStatusCode());
        */
    }

}