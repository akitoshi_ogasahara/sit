public with sharing class I_UsersController extends I_AbstractController {
	//ページメッセージ
	public E_PageMessagesHolder pageMessages {get; set;}

	// 表示行数
	public Integer rowCount {get; set;}
	// MAX表示件数
	public Integer getListMaxRows() {
		return I_Const.LIST_MAX_ROWS;
	}

	public Boolean hasViewedByManager {get {
		return E_AccessController.getInstance().isAuthManager();
	}}

	// 表示データリスト
	public List<UserListRow> userList {get; set;}

	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}

	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	public string searchType {get; set;}

// 内部定数
	// URLパラメータ名 - ソート項目
	public static final String URL_PARAM_SORT_TYPE			= 'st';
	// ソートキー
	public static final String SORT_TYPE_AGENT_NAME			= 'agent';
	public static final String SORT_TYPE_OFFICE_NAME		= 'office';
	public static final String SORT_TYPE_LAST_LONGIN_DATE	= 'lastLoginDate';
	public static final String SORT_TYPE_LOGIN_COUNT		= 'loginCount';

	/**
	 * コンストラクタ
	 */
	public I_UsersController() {
		super();
		pageMessages = getPageMessages();

		// デフォルトソート
		sortType  = SORT_TYPE_LAST_LONGIN_DATE;
		sortIsAsc = false;

		// URLパラメータから繰り返し行数を取得
		rowCount = I_Const.LIST_DEFAULT_ROWS;
		try {
			Integer pRows = Integer.valueOf(ApexPages.currentPage().getParameters().get(I_Const.LIST_URL_PARAM_DISPLAY_ROWS));
			if(pRows > I_Const.LIST_MAX_ROWS){
				pRows = I_Const.LIST_MAX_ROWS;
			}
			rowCount = pRows;
		} catch (Exception e) {
			//数値変換エラーの場合　I_Const.LIST_DEFAULT_ROWSが設定されます
		}
	}

	/**
	 * 初期処理
	 */
	protected override pageReference init() {
		// アクセスログのName項目の設定
		this.pageAccessLog.Name = this.activePage.Name;
		if (pageAccessLog.Name==null) {
			pageAccessLog.Name = this.menu.Name;
		}

		// Superクラスでエラーなしの場合に処理を実施
		PageReference pr = super.init();
		if (pr!=null) {
			return pr;
		}

		this.searchType = 'all';

		// リストを作成する処理
		String condition = createPolicyCondition();
		createUserList(condition);

		return null;
	}

	public List<SelectOption> getSearchTypeOptions() {
		return new List<SelectOption>{
			new SelectOption('all', '自身の担当分'),
			new SelectOption('head_office', '本社営業部分')
		};
	}

	/**
	 * フィルタ条件を元にWHERE句を生成する
	 */
	public String createPolicyCondition(){
		String result ='';
		// フィルタの有りの場合
		if(iris.isSetPolicyCondition()){
			// フィルタ項目と値を取得
			String conditionField = iris.getPolicyConditionValue(I_Const.JSON_KEY_FIELD);
			String conditionValue = iris.getPolicyConditionValue(I_Const.JSON_KEY_SFID);

			if(String.isNotBlank(conditionField) && String.isNotBlank(conditionValue)){
				if(conditionField == I_Const.CONDITION_FIELD_UNIT){
					// 営業部フィルタ
					result = ' AND ( Contact.Account.E_CL2PF_BRANCH__c = \'' + conditionValue + '\' ) ';
				} else if(conditionField == I_Const.CONDITION_FIELD_MR){
					// MRフィルタ
					result = ' AND ( Contact.Account.Ownerid = \'' + conditionValue + '\' ) ';
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENCY){
					// 代理店フィルタ
					result = ' AND ( Contact.Account.ParentId = \'' + conditionValue + '\' ) ';
				} else if(conditionField == I_Const.CONDITION_FIELD_OFFICE){
					// 事務所フィルタ
					result = ' AND ( Contact.AccountId = \'' + conditionValue + '\' ) ';
				} else if(conditionField == I_Const.CONDITION_FIELD_AGENT){
					// 募集人フィルタ
					result = ' AND ( ContactId = \'' + conditionValue + '\' ) ';
				}
			}
		}
		return result;
	}

	private static List<User> retrieveUserList(String searchType, String condition) {
		E_AccessController access = E_AccessController.getInstance();
		List<User> users;
		//String condition = '';
		if (condition == ''){
			if (access.isAuthManager()) {
				// 本社営業
				if (searchType == 'head_office') {
					condition += ' AND (Contact.Account.KSECTION__c = \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' '
								+ ' OR (Contact.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\' '
								+ ' AND Contact.Account.E_CL2PF_BRANCH__c != \'' + access.idcpf.ZINQUIRR__c.right(2) + '\'))';
					users = E_UserDaoWithout.getIRISAgentUserRecord(access.idcpf.ZINQUIRR__c.right(2), condition);
				}
				// 自拠点
				else {
					condition += ' AND Contact.Account.KSECTION__c != \'' + E_Const.AGENCY_KSECTION_HEAD_OFFICE + '\''
								+ ' AND Contact.Account.E_CL2PF_BRANCH__c = \'' + access.idcpf.ZINQUIRR__c.right(2) + '\'';
					users = E_UserDaoWithout.getIRISAgentUserRecord(access.idcpf.ZINQUIRR__c.right(2), condition);
				}
			} else {
				users = E_UserDaoWithout.getIRISAgentUserRecord(access.idcpf.ZINQUIRR__c.right(2));
			}
		} else {
			// 別条件
			users = E_UserDaoWithout.getIRISAgentUserRecord('**', condition);
		}
		return users;
	}

	/**
	 * 過去1ヶ月以内のログがある募集人リストを作成
	 */
	public void createUserList() {
		// リストを作成する処理
		String condition = createPolicyCondition();
		createUserList(condition);
	}
	public void createUserList(String condition) {
		Set<Id> userIds = new Set<Id>();
		List<User> users = new List<User>();
		for (User usr : retrieveUserList(this.searchType, condition)) {
			// ログ出力対象のE_Log__cが存在しない場合は何もしない
			if(usr.NNLinkLogs__r == null || usr.NNLinkLogs__r.isEmpty()){
				continue;
			}

			userids.add(usr.Id);
			users.add(usr);
		}

		// ログイン履歴を取得
		AggregateResult[] historys = E_LoginHistoryDaoWithout.countLastMonthLoginHistory(userids);

		Map<Id, Integer> hisMap = new Map<Id, Integer>();
		for(AggregateResult ar : historys){
			hisMap.put(ID.valueOf(String.valueOf(ar.get('UserId'))), Integer.valueOf(ar.get('loginCount')));
		}

		// 表示リストを作成
		userList = new List<UserListRow>();
		for(User u : users){

			UserListRow row = new UserListRow();

			row.agentName = u.Contact.Name;
			row.agentNameKana = u.Contact.E_CL3PF_ZEATKNAM__c;
			row.officeName = u.Contact.Account.Name;
			row.officeNameKana = u.Contact.Account.E_CL2PF_ZEAYKNAM__c;
			row.lastLoginDate = u.LastLoginDate;
			row.loginCount = hisMap.get(u.Id);
			row.agentId = u.Id;

			userList.add(row);
			if(userList.size() == I_Const.LIST_MAX_ROWS){
				break;
			}
		}

		if(userList != null && !userList.isEmpty()){
			list_sortType  = sortType;
			list_sortIsAsc = sortIsAsc;
			userList.sort();
		}

		return;
	}

	/**
	 * ソート
	 */
	public void sortRows() {
		String sType = ApexPages.currentPage().getParameters().get(URL_PARAM_SORT_TYPE);

		if (sortType != sType) {
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}

		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;

		userList.sort();

		return;
	}

	/**
	 * 表示行数追加
	 */
	public PageReference addRows() {
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if (rowCount > I_Const.LIST_MAX_ROWS) {
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows() {
		return userList == null ? 0 : userList.size();
	}

	public class UserListRow implements Comparable {
		// 募集人名
		public String agentName {get; set;}
		// 募集人名カナ
		public String agentNameKana {get; set;}
		// 事務所名
		public String officeName {get; set;}
		// 事務所名カナ
		public String officeNameKana {get; set;}
		// 最終ログイン日時
		public DateTime lastLoginDate {get; set;}
		// 最終ログイン日時（表示用）
		public String lastLoginDateJST {
			get {
				return lastLoginDate.format('yyyy/MM/dd HH:mm', 'JST');
			}
			set;
		}
		// 過去1ヶ月のログイン回数
		public Integer loginCount {get; set;}
		// 募集人ID
		public String agentId {get; set;}

		public UserListRow(){
			agentName = '';
			agentNameKana = '';
			officeName = '';
			officeNameKana = '';
			lastLoginDate = DateTime.now();
			loginCount = 0;
			agentId = '';
		}

		// ソート
		public Integer compareTo(Object compareTo){
			UserListRow compareToRow = (UserListRow)compareTo;
			Integer result = 0;

			// 募集人名
			if(list_sortType == SORT_TYPE_AGENT_NAME){
				result = I_Util.compareToString(agentNameKana, compareToRow.agentNameKana);
			// 事務所名
			} else if(list_sortType == SORT_TYPE_OFFICE_NAME){
				result = I_Util.compareToString(officeNameKana, compareToRow.officeNameKana);
			// 最終ログイン日時
			} else if(list_sortType == SORT_TYPE_LAST_LONGIN_DATE){
				result = I_Util.compareToDateTime(lastLoginDate, compareToRow.lastLoginDate);
			// 過去1ヶ月のログイン回数
			} else if(list_sortType == SORT_TYPE_LOGIN_COUNT){
				result = I_Util.compareToInteger(loginCount, compareToRow.loginCount);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}
}