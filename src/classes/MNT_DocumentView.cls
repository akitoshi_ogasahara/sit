global with sharing class MNT_DocumentView extends SkyEditor2.SkyEditorPageBaseWithSharing {
	public I_ContentMaster__c record {get{return (I_ContentMaster__c)mainRecord;}}
	{
	setApiVersion(42.0);
	}
	public MNT_DocumentView(ApexPages.StandardController controller) {
		super(controller);
		deleteTransitionControl='/apex/MNT_DocumentSearch';

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.FormNo__c;
		f = I_ContentMaster__c.fields.DocumentCategory__c;
		f = I_ContentMaster__c.fields.DOM_Id__c;
		f = I_ContentMaster__c.fields.MaxCopies__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.CannotBeOrder__c;
		f = I_ContentMaster__c.fields.NewValidTo__c;
		f = I_ContentMaster__c.fields.ClickAction__c;
		f = I_ContentMaster__c.fields.filePath__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainSObjectType = I_ContentMaster__c.SObjectType;
			setPageReferenceFactory(new PageReferenceFactory());
			sve_ClassName = 'MNT_DocumentView';
			mainQuery = new SkyEditor2.Query('I_ContentMaster__c');
			mainQuery.addFieldAsOutput('Name');
			mainQuery.addFieldAsOutput('FormNo__c');
			mainQuery.addFieldAsOutput('DocumentCategory__c');
			mainQuery.addFieldAsOutput('DOM_Id__c');
			mainQuery.addFieldAsOutput('MaxCopies__c');
			mainQuery.addFieldAsOutput('DisplayFrom__c');
			mainQuery.addFieldAsOutput('ValidTo__c');
			mainQuery.addFieldAsOutput('CannotBeOrder__c');
			mainQuery.addFieldAsOutput('NewValidTo__c');
			mainQuery.addFieldAsOutput('ClickAction__c');
			mainQuery.addFieldAsOutput('filePath__c');
			mainQuery.addFieldAsOutput('ParentContent__c');
			mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
				.limitRecords(1);
			mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
			queryMap = new Map<String, SkyEditor2.Query>();
			SkyEditor2.Query query;
			p_showHeader = true;
			p_sidebar = false;
			init();
			if (record.Id == null) {
				saveOldValues();
			}

		}  catch (SkyEditor2.Errors.FieldNotFoundException e) {
			fieldNotFound(e);
		} catch (SkyEditor2.Errors.RecordNotFoundException e) {
			recordNotFound(e);
		} catch (SkyEditor2.ExtenderException e) {
			e.setMessagesToPage();
		}
	}

	@TestVisible
		private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}
	@TestVisible
		private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
		SkyEditor2.Messages.addErrorMessage(e.getMessage());
		hidePageBody = true;
	}

	with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
		public PageReference newPageReference(String url) {
			return new PageReference(url);
		}
	}
}