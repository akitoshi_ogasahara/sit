public with sharing class I_GroupPolicyController extends I_AbstractPolicyController{
	//保険契約ヘッダのリスト(データ保持用)
	public List<PolicyListRow> policyList {get; set;}
	//メッセージ表示用契約者リスト
	private List<E_Policy__c> policyRecs;
	//表示用のリスト
	public List<PolicyListRow> displayList {get; set;}

	//総行数
	public String isTotalRows{get; set;}
	//取得リスト行数
	public Integer listSize {get; set;}

	//団体番号
	public String getGrupNum{get; set;}
	//契約応当日期間の判別
	public Integer getFrom{get; set;}


	// ソート実行用：項目
	private static String list_sortType;
	// ソート実行用：昇順 OR 降順
	private static Boolean list_sortIsAsc;

	//データ保持用マップ
	private Map<String,PolicyListRow> policyMap;
	//ソート後、StandardSecControllerに設定する用のマップ
	private Map<String,E_Policy__c> policyRecMap;

	//ページング機能
	public ApexPages.StandardSetController ssc { get; set; }

	//ページ総数
	public Integer pageSize {get; set;}

	//団体顧客名
	public String groupName {get; set;}

	public String syncDate {get; set;}


	public I_GroupPolicyController() {
		super();
		pageMessages = getPageMessages();

		syncDate = E_Util.getFormatDateStr(Date.today(),'yyyy/MM/dd  ');

		// デフォルトソート - 契約日 : 昇順
		sortType  = 'contractDay';
		sortIsAsc = true;

		//パラメータの団体番号を取得
		getGrupNum = ApexPages.CurrentPage().getParameters().get('group');
		getFrom = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('from'));
	}

	/**
	 * 初期処理
	 */
	protected override PageReference init(){
		PageReference pr = super.init();
		pageAccessLog.Name = this.menu.Name;
		if(pr==null){
			//SOQLで該当データを取得、画面表示用リストを作成、Map対応
			makePolicyList();
			//リストソート実行
			doListSort();
		}

		return pr;
	}

	public void makePolicyList(){
		//保険契約ヘッダ画面表示用リスト
		policyList = new List<PolicyListRow>();

		//保険契約ヘッダ値取得用リスト
		policyRecs = E_PolicyDao.getRecsIRISGrupPolicys(getGrupNum,createPolicyCondition());
		System.debug('policyRecs='+policyRecs);

		//0件の時エラー
		if(policyRecs.isEmpty()){
			pageMessages.addWarningMessage(getMSG().get('LIS|002'));
			return;
		}

		//最大件数以上の場合は警告を表示する
		if(policyRecs.size() >= I_Const.LIST_MAX_ROWS_GRUPAGGREGATE){
			String errmessage = String.format(getMSG().get('LIS|001'),new String[]{Decimal.valueOf(I_Const.LIST_MAX_ROWS_GRUPAGGREGATE).format()});
			pageMessages.addWarningMessage(errmessage);
		}
		// アクティブメニューキー（URLに付与することでアクティブメニューを保持）
		String irisMenu = '&' + getIRISMenuParameter();

		//policyListデータ保持用マップをインスタンス生成
		policyMap = new Map<String, PolicyListRow>();
		//ソート後用(policyRec)のマップをインスタンス生成
		policyRecMap = new Map<String,E_Policy__c>();

		for(E_Policy__c pol : policyRecs){
			PolicyListRow plr = new PolicyListRow();

			//契約応答日
			plr.polResDate  = E_Util.formatMMdd(pol.PolicyResponseDate__c);
			//契約応答日ソート用
			plr.polResSort  = E_Util.formatBirthday(pol.PolicyResponseDate__c);
			//被保険者
			plr.insured = pol.InsuredName__c;
			//被保険者カナ名
			plr.insuredSort = pol.InsuredE_CLTPF_ZCLKNAME__c;		//スプリントバックログ9(2017/1/18) カナソート用追記
			//証券番号
			plr.policyNum = pol.COMM_CHDRNUM__c;
			//契約日
			plr.contractDay = E_Util.formatBirthday(pol.COMM_OCCDATE__c);
			//契約者名（個人）
			plr.contractorName = pol.ContractorName__c;
			//契約者カナ名
			plr.contractorSort = pol.ContractorE_CLTPF_ZCLKNAME__c;		//スプリントバックログ9(2017/1/18) カナソート用追記
			//契約者名
			groupName = pol.CorporateName__c;

			//遷移先URL
			//plr.pageURL = 'E_Coli?id=' + pol.Id + irisMenu;
			plr.pageURL = 'IRIS_Coli?id=' + pol.Id + irisMenu;

			//個人保険
			if(pol.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI){
				//保険契約ヘッダ
				if(pol.COMM_SINSTAMT__c != null){
					plr.policyFeeDec = pol.COMM_SINSTAMT__c;							//保険料:1回あたりの保険料
				}
				plr.cancellRefDec = getCancellationRefund(pol);							//解約返戻金

				//個人保険特約
				for(E_COVPF__c cov : pol.E_COVPFs__r){
					if(cov.COMM_SUMINS__c != null){
						plr.policyAmountDec += cov.COMM_SUMINS__c;						//保険金額:主契約現在保険金額
					}
					if(cov.COLI_ZCRIND__c == I_Const.COVPF_COLI_ZCRIND_C){
						//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類設定項目を変更
						plr.policyType = cov.COLI_ZCOVRNAMES__c;						//保険種類（主契約フラグがCのとき）
					}
				}
			//SPVA
			}else{
				if(pol.SPVA_ZNOWSUM__c != null){
					plr.policyAmountDec = pol.SPVA_ZNOWSUM__c;							//保険金額:最低死亡保障額/基本給付金(処理日時点）
				}
				if(pol.COMM_ZTOTPREM__c != null){
					plr.policyFeeDec = pol.COMM_ZTOTPREM__c;							//保険料:払込保険料
				}
				plr.cancellRefDec = pol.SPVA_ZREFAMT__c;								//解約返戻金：解約返戻金
				//SITP(2017/12/1R) 保健種類(結合)表示対応 保険種類設定項目を変更
				plr.policyType = pol.COLI_ZCOVRNAMES__c;								//保険種類
			}

			//画面出力用に格納
			plr.policyAmount = getFormat(plr.policyAmountDec);
			plr.policyFee = getFormat(plr.policyFeeDec);
			plr.cancellRef = getFormat(plr.cancellRefDec);

			//KeySetを作成(証券番号)
			String policyKey = plr.policyNum;

			//保険契約ヘッダを出力用のListにまとめたものをマップに対応付ける
			policyMap.put(policyKey,plr);

			//policyRecs(sObject型)をマップに対応付ける
			policyRecMap.put(policyKey,pol);

			//リストに加える
			policyList.add(plr);
		}
		
		//総行数の取得
		listSize = policyList.size();
		isTotalRows = policyList.size().format();
		//数字の部分はあとで定数かラベルに差し替え
		pageSize = listSize / I_Const.LIST_DISPLAY_ROWS;
		if(math.mod(policyList.size(),I_Const.LIST_DISPLAY_ROWS) > 0){
			pageSize += 1;
		}

	}

	// 画面表示行ラッパークラス
	public class PolicyListRow implements Comparable{
		//契約応答月
		public String polResDate {get; set;}
		//契約応答月 ソート用
		public String polResSort {get; set;}
		//契約日
		public String contractDay {get; set;}
		//被保険者
		public String insured {get; set;}
		//証券番号
		public String policyNum {get; set;}
		//保険種類
		public String policyType {get; set;}
		//保険料
		public Decimal policyFeeDec {get; set;}
		//画面用保険料
		public String policyFee {get; set;}
		//保険金額
		public Decimal policyAmountDec {get; set;}
		//画面用保険金額
		public String policyAmount {get; set;}
		//解約返戻金
		public Decimal cancellRefDec{get; set;}
		//画面用解約返戻金
		public String cancellRef{get; set;}
		//契約者名
		public String contractorName{get; set;}

		//スプリントバックログ9(2017/1/18) カナソート用追記(契約者名・被保険者名)
		//契約者名ソート用
		public String contractorSort{get; set;}
		//被保険者名ソート用
		public String insuredSort{get; set;}

		//URL
		public String pageURL{get; set;}

		// コンストラクタ
		public PolicyListRow(){
			polResDate	= '';
			polResSort	= '';
			policyType	= '';
			policyNum	= '';
			contractDay = '';
			insured 	= '';
			pageURL 	= '';
			policyFee	= '';
			policyAmount = '';
			cancellRef	= '';
			contractorName = '';

			//スプリントバックログ9(2017/1/18) カナソート用追記(contractorSort,insuredSort)
			contractorSort = '';
			insuredSort	   = '';

			policyFeeDec 	= 0;
			policyAmountDec = 0;
			cancellRefDec 	= 0;
		}

		/**
		 * ソート
		 * Mapに設定されているものは変数名
		 * 設定されていないものはAPI名
		 */
		public Integer compareTo(Object compareTo){
			PolicyListRow compareToRow = (PolicyListRow)compareTo;
			Integer result = 0;
			//保険料
			if(list_sortType == 'policyFee'){
				result = I_Util.compareToDecimal(policyFeeDec, compareToRow.policyFeeDec);
			//契約応答月
			}else if(list_sortType == 'polResSort'){
				result = I_Util.compareToString(polResSort, compareToRow.polResSort);
			//保険種類
			}else if(list_sortType == 'policyType'){
				result = I_Util.compareToString(policyType, compareToRow.policyType);
			//証券番号
			}else if(list_sortType == 'policyNum'){
				result = I_Util.compareToString(policyNum, compareToRow.policyNum);
			//解約返戻金
			}else if(list_sortType == 'cancellRef'){
				result = I_Util.compareToDecimal(cancellRefDec, compareToRow.cancellRefDec);
			//契約日
			}else if(list_sortType == 'contractDay'){
				result = I_Util.compareToString(contractDay, compareToRow.contractDay);
			//契約者
			}else if(list_sortType == 'contractorName'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(contractorSort, compareToRow.contractorSort);
				//result = I_Util.compareToString(contractorName, compareToRow.contractorName);
			//被保険者
			}else if(list_sortType == 'insured'){
				//スプリントバックログ9(2017/1/18) カナソート用修正
				result = I_Util.compareToString(insuredSort, compareToRow.insuredSort);
				//result = I_Util.compareToString(insured, compareToRow.insured);
			//保険金額
			}else if(list_sortType == 'policyAmount'){
				result = I_Util.compareToDecimal(policyAmountDec, compareToRow.policyAmountDec);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * ソート実行
	 */
	public PageReference sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		//押されたのが同じ項目だった場合逆に、違う項目だった場合昇順に
		if(sType == sortType){
			sortIsAsc = !sortIsAsc;
		}else{
			sortIsAsc = true;
			sortType = sType;

		}
		//リストソート実行
		doListSort();

		return null;
	}

	//リストソートのための処理
	private void doListSort(){

		I_GroupPolicyController.list_sortType  = sortType;
		I_GroupPolicyController.list_sortIsAsc = sortIsAsc;
		//データ保持用のリストをソート
		policyList.sort();

		//ページに対応付けるため、sObject型のリストを作成
		policyRecs = new List<E_Policy__c>();
		String key;
		//ソート後のリストで回す
		for(PolicyListRow plcList : policyList){
			E_Policy__c plr = new E_Policy__c();
			//Keyに証券番号
			key = plcList.policyNum;
			//DBから取得時に設定したMapから、証券番号をキーにしてデータを取得
			plr = policyRecMap.get(key);
			//リストへ加える
			policyRecs.add(plr);
		}
		//standardControllerにソート後のリストを設定
		makeStandardSetController(policyRecs);

		//画面表示用のリストを作成
		GroupListSet();

		//ページを始めのページへ
		first();
	}

	//format用
	public String getFormat(Decimal policyfee){
		if(policyfee <= 0 || policyfee == null){
			return '0';
		}
		return policyfee.format();
	}

	//メニューキー
	protected override String getLinkMenuKey(){
		return ApexPages.CurrentPage().getParameters().get('from')=='4'? 'PolicyList4To6' :'PolicyList1To3';
	}

	//StandardSetControllerインスタンス生成
	private void makeStandardSetController(List<E_Policy__c> plcLsit){
		ssc = new ApexPages.StandardSetController(plcLsit);
		ssc.setPageSize(I_Const.LIST_DISPLAY_ROWS);
		System.debug('前ページ='+ssc.getHasPrevious());
		System.debug('後ページ='+ssc.getHasNext());
	}

	// 最初のページへ
	public void first() {
		ssc.first();
		GroupListSet();
	}
	// 前のページへ
	public void previous() {
		ssc.previous();
		GroupListSet();
	}//次のページへ
	public void next() {
		ssc.next();
		GroupListSet();
	}// 最後のページへ
//	public void last() {
	public PageReference last() {
		ssc.last();
		GroupListSet();
		return null;
	}

	//団体一覧ページの表示用リストを作成
	public void GroupListSet(){
		//現在のページのレコードのリストを取得
		List<E_Policy__c> tempList = ssc.getRecords();
		//画面表示用のリストをインスタンス生成
		displayList = new List<PolicyListRow>();
		String key;
		//現ページのレコードを調べる
		for(E_Policy__c pol : tempList){
			PolicyListRow plr = new PolicyListRow();
			//Keyに証券番号をセット
			key = pol.COMM_CHDRNUM__c;
			//証券番号で保持用のリストから照らし合わせる
			plr = policyMap.get(key);
			//表示用リストに追加
			displayList.add(plr);
		}

	}
}