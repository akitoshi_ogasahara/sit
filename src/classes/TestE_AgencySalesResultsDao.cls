@isTest
public class TestE_AgencySalesResultsDao {

	/**
	 * getRecTypeAByIdのテスト
	 */
	@isTest static void getRecTypeAByIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecTypeAById(agencySalesResults.Id);
		System.assertEquals('201706', rec.YYYYMM__c);
		Test.stopTest();

	}

	/**
	 * getRecTypeBByIdのテスト
	 */
	@isTest static void getRecTypeBByIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecTypeBById(agencySalesResults.Id);
		System.assertEquals('優績A', rec.XHAH_AGCLS__c);
		Test.stopTest();

	}

	/**
	 * getRecTypeC1ByIdのテスト
	 */
	@isTest static void getRecTypeC1ByIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecTypeC1ById(agencySalesResults.Id);
		System.assertEquals('201706', rec.YYYYMM__c);
		Test.stopTest();

	}

	/**
	 * getRecTypeC2ByIdのテスト
	 */
	@isTest static void getRecTypeC2ByIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecTypeC2ById(agencySalesResults.Id);
		System.assertEquals('201706', rec.YYYYMM__c);
		Test.stopTest();

	}

	/**
	 * getRecTypeDByIdのテスト
	 */
	@isTest static void getRecTypeDByIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecTypeDById(agencySalesResults.Id);
		System.assertEquals(agencySalesResults.Id, rec.Id);
		Test.stopTest();

	}

	/**
	 * getRecTypeEByIdのテスト
	 */
	@isTest static void getRecTypeEByIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecTypeEById(agencySalesResults.Id);
		System.assertEquals(agencySalesResults.Id, rec.Id);
		Test.stopTest();

	}

	/**
	 * getRecBysWhereのテスト
	 */
	@isTest static void getRecBysWhereTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;
		String soqlWhere = 'QualifSim__c =  \'優績S\' ';

		// テスト
		Test.startTest();
		E_AgencySalesResults__c rec = E_AgencySalesResultsDao.getRecBysWhere(soqlWhere);
		System.assertEquals(agencySalesResults.Id, rec.Id);
		Test.stopTest();
	}

	/**
	 * getSRAgentRecsのテスト
	 */
	@isTest static void getSRAgentRecsTest(){
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = createAgencySalesResults();
		insert agencySalesResults;
		String soqlWhere = 'QualifSim__c =  \'優績S\' ';

		// テスト
		Test.startTest();
		List<E_AgencySalesResults__c> rec = E_AgencySalesResultsDao.getSRAgentRecs(soqlWhere);
		System.assertEquals(agencySalesResults.Id, rec.get(0).Id);
		Test.stopTest();
	}

	/**
	 * 代理店挙積情報の作成
	 */
	public static E_AgencySalesResults__c createAgencySalesResults(){
		E_AgencySalesResults__c agencySalesResults = new E_AgencySalesResults__c();
		agencySalesResults.Hierarchy__c							= 'AH';				// 階層
		agencySalesResults.Account__c							= null;				// 事務所コード
		agencySalesResults.ActiveAgency__c						= 0;				// 稼動事務所数
		agencySalesResults.ActiveAgent__c						= 0;				// 稼動募集人数
		agencySalesResults.ActivePolicyNumber__c				= 0;				// 稼働件数
		agencySalesResults.AddParentAccountCode__c				= '';				// 合算先代理店コード
		agencySalesResults.Address__c							= '東京都本店所在地';	// 本店所在地
		agencySalesResults.AgencyName__c						= '代理店テスト';	// 代理店名
		agencySalesResults.AgentName__c							= '';				// 募集人名
		agencySalesResults.AgentType__c							= '法代';				// 登録区分
		agencySalesResults.BelongOfficeName__c					= '所属事務所';			// 所属事務所名
		agencySalesResults.ClawBack__c							= 0;				// 戻入未収金残高
		agencySalesResults.CnvsLicenseReason__c					= '';				// 当社商品の募集資格免除事由
		agencySalesResults.CnvsLicenseStatus__c					= '';				// 当社商品の募集資格ステータスコード
		agencySalesResults.CnvsLicenseTerminate__c				= '';				// 当社商品の募集資格仮業廃
		agencySalesResults.CnvsLicenseVariableIns__c			= 'アリ';				// 当社での変額資格
		agencySalesResults.CnvsLicense__c						= '有';				// 当社商品の募集資格
		agencySalesResults.CnvsStartDate__c						= '20170131';		// 当社募集開始日
		agencySalesResults.DefactRate_LY__c						= 55;				// 単純不備率（前年YTD）
		agencySalesResults.DefactRate__c						= 66;				// 単純不備率（当年YTD）
		agencySalesResults.DefactRate_NumeratorLY__c			= 8;				// 単純不備率 単純不備数（前年YTD）
		agencySalesResults.DefactRate_Numerator__c				= 6;				// 単純不備率 単純不備数（当年YTD）
		agencySalesResults.DefactRate_DenominatorLY__c			= 20;				// 単純不備率 総数（前年YTD）
		agencySalesResults.DefactRate_Denominator__c			= 15;				// 単純不備率 総数（当年YTD）
		agencySalesResults.BusinessDate__c						= '20170131';		// 営業日
		agencySalesResults.E_Unit__c							= null;				// 営業部コード
		agencySalesResults.EvaCancellConsigContract__c			= '';				// 委託解約基準判定
		agencySalesResults.Payment_Jan__c						= 1232565;			// 振込手数料額　当年1月
		agencySalesResults.Payment_Feb__c						= 2213;				// 振込手数料額　当年2月
		agencySalesResults.Payment_Mar__c						= 1278307;			// 振込手数料額　当年3月
		agencySalesResults.Payment_Apr__c						= 120810;			// 振込手数料額　当年4月
		agencySalesResults.Payment_May__c						= 192132;			// 振込手数料額　当年5月
		agencySalesResults.Payment_Jun__c						= 66969;			// 振込手数料額　当年6月
		agencySalesResults.Payment_Jul__c						= 1353562;			// 振込手数料額　当年7月
		agencySalesResults.Payment_Aug__c						= 65345;			// 振込手数料額　当年8月
		agencySalesResults.Payment_Sep__c						= 4543;				// 振込手数料額　当年9月
		agencySalesResults.Payment_Oct__c						= 4355;				// 振込手数料額　当年10月
		agencySalesResults.Payment_Nov__c						= 41826;			// 振込手数料額　当年11月
		agencySalesResults.Payment_Dec__c						= 107494;			// 振込手数料額　当年12月
		agencySalesResults.FY_Jan__c							= 205200;			// 初年度手数料額　当年1月
		agencySalesResults.FY_Feb__c							= 0;				// 初年度手数料額　当年2月
		agencySalesResults.FY_Mar__c							= 216160;			// 初年度手数料額　当年3月
		agencySalesResults.FY_Apr__c							= 41520;			// 初年度手数料額　当年4月
		agencySalesResults.FY_May__c							= 64800;			// 初年度手数料額　当年5月
		agencySalesResults.FY_Jun__c							= 10800;			// 初年度手数料額　当年6月
		agencySalesResults.FY_Jul__c							= 241800;			// 初年度手数料額　当年7月
		agencySalesResults.FY_Aug__c							= 10800;			// 初年度手数料額　当年8月
		agencySalesResults.FY_Sep__c							= 0;				// 初年度手数料額　当年9月
		agencySalesResults.FY_Oct__c							= 512;				// 初年度手数料額　当年10月
		agencySalesResults.FY_Nov__c							= 6480;				// 初年度手数料額　当年11月
		agencySalesResults.FY_Dec__c							= 17280;			// 初年度手数料額　当年12月
		agencySalesResults.ASY_Jan__c							= 820800;			// 次年度以降手数料額　当年1月
		agencySalesResults.ASY_Feb__c							= 2136;				// 次年度以降手数料額　当年2月
		agencySalesResults.ASY_Mar__c							= 864640;			// 次年度以降手数料額　当年3月
		agencySalesResults.ASY_Apr__c							= 86080;			// 次年度以降手数料額　当年4月
		agencySalesResults.ASY_May__c							= 259200;			// 次年度以降手数料額　当年5月
		agencySalesResults.ASY_Jun__c							= 43200;			// 次年度以降手数料額　当年6月
		agencySalesResults.ASY_Jul__c							= 967200;			// 次年度以降手数料額　当年7月
		agencySalesResults.ASY_Aug__c							= 43200;			// 次年度以降手数料額　当年8月
		agencySalesResults.ASY_Sep__c							= 423;				// 次年度以降手数料額　当年9月
		agencySalesResults.ASY_Oct__c							= 0;				// 次年度以降手数料額　当年10月
		agencySalesResults.ASY_Nov__c							= 25920;			// 次年度以降手数料額　当年11月
		agencySalesResults.ASY_Dec__c							= 69120;			// 次年度以降手数料額　当年12月
		agencySalesResults.IANP_LY__c							= 500000;			// 保有ANP（前年）
		agencySalesResults.IANP__c								= 600000;			// 保有ANP（当年）
		agencySalesResults.IQA_DenominatorLY__c					= 400000;			// IQA継続率 分母（前年）
		agencySalesResults.IQA_Denominator__c					= 450000;			// IQA継続率 分母（当年）
		agencySalesResults.IQA_NumeratorLY__c					= 380000;			// IQA継続率 分子（前年）
		agencySalesResults.IQA_Numerator__c						= 420000;			// IQA継続率 分子（当年）
		agencySalesResults.IQA_LY__c							= 95.0;				// IQA継続率（前年）
		agencySalesResults.IQA__c								= 93.0;				// IQA継続率（当年）
		agencySalesResults.MOF13_DenominatorLY__c				= 300000;			// MOF13継続率 分母（前年）
		agencySalesResults.MOF13_Denominator__c					= 340000;			// MOF13継続率 分母（当年）
		agencySalesResults.MOF13_NumeratorLY__c					= 280000;			// MOF13継続率 分子（前年）
		agencySalesResults.MOF13_Numerator__c					= 260000;			// MOF13継続率 分子（当年）
		agencySalesResults.MOF13_LY__c							= 83.0;				// MOF13継続率（前年）
		agencySalesResults.MOF13__c								= 85.29;			// MOF13継続率（当年）
		agencySalesResults.MOF25_DenominatorLY__c				= 250000;			// MOF25継続率 分母（前年）
		agencySalesResults.MOF25_Denominator__c					= 290000;			// MOF25継続率 分母（当年）
		agencySalesResults.MOF25_NumeratorLY__c					= 210000;			// MOF25継続率 分子（前年）
		agencySalesResults.MOF25_Numerator__c					= 260000;			// MOF25継続率 分子（当年）
		agencySalesResults.MOF25_LY__c							= 84.0;				// MOF25継続率（前年）
		agencySalesResults.MOF25__c								= 89.67;			// MOF25継続率（当年）
		agencySalesResults.MR__c								= null;				// MRコード
		agencySalesResults.MotherAdress__c						= '東京都母店所在地';	// 母店　所在地
		agencySalesResults.MotherBranchName__c					= '母店担当営業部';		// 母店　担当営業部
		agencySalesResults.MotherMRCode__c						= 'mrcd';				// 母店　担当MRコード
		agencySalesResults.MotherMR__c							= '';					// 母店　担当MR
		agencySalesResults.XHAH_ZBUSAGCY__c						= '0123';				// 母店　事務所コード
		agencySalesResults.MotherOfficeName__c					= '母店事務所';			// 母店　事務所名
		agencySalesResults.NBCANP_Jan__c						= 106267;			// 新契約係数ANP　当年1月
		agencySalesResults.NBCANP_Feb__c						= 269607;			// 新契約係数ANP　当年2月
		agencySalesResults.NBCANP_Mar__c						= 150276;			// 新契約係数ANP　当年3月
		agencySalesResults.NBCANP_Apr__c						= 9804;				// 新契約係数ANP　当年4月
		agencySalesResults.NBCANP_May__c						= 240123;			// 新契約係数ANP　当年5月
		agencySalesResults.NBCANP_Jun__c						= 632471;			// 新契約係数ANP　当年6月
		agencySalesResults.NBCANP_Jul__c						= 82567;			// 新契約係数ANP　当年7月
		agencySalesResults.NBCANP_Aug__c						= 185678;			// 新契約係数ANP　当年8月
		agencySalesResults.NBCANP_Sep__c						= 235765;			// 新契約係数ANP　当年9月
		agencySalesResults.NBCANP_Oct__c						= 416387;			// 新契約係数ANP　当年10月
		agencySalesResults.NBCANP_Nov__c						= 153159;			// 新契約係数ANP　当年11月
		agencySalesResults.NBCANP_Dec__c						= 199784;			// 新契約係数ANP　当年12月
		agencySalesResults.NBCANP_LYJan__c						= 82323;			// 新契約係数ANP　前年1月
		agencySalesResults.NBCANP_LYFeb__c						= 59441;			// 新契約係数ANP　前年2月
		agencySalesResults.NBCANP_LYMar__c						= 571699;			// 新契約係数ANP　前年3月
		agencySalesResults.NBCANP_LYApr__c						= 33277;			// 新契約係数ANP　前年4月
		agencySalesResults.NBCANP_LYMay__c						= 24156;			// 新契約係数ANP　前年5月
		agencySalesResults.NBCANP_LYJun__c						= 77808;			// 新契約係数ANP　前年6月
		agencySalesResults.NBCANP_LYJul__c						= 76802;			// 新契約係数ANP　前年7月
		agencySalesResults.NBCANP_LYAug__c						= 139441;			// 新契約係数ANP　前年8月
		agencySalesResults.NBCANP_LYSep__c						= 159142;			// 新契約係数ANP　前年9月
		agencySalesResults.NBCANP_LYOct__c						= 12061;			// 新契約係数ANP　前年10月
		agencySalesResults.NBCANP_LYNov__c						= 75052;			// 新契約係数ANP　前年11月
		agencySalesResults.NBCANP_LYDec__c						= 162531;			// 新契約係数ANP　前年12月
		agencySalesResults.NBCANP_B2YJan__c						= 45226;			// 新契約係数ANP　2年前1月
		agencySalesResults.NBCANP_B2YFeb__c						= 23524;			// 新契約係数ANP　2年前2月
		agencySalesResults.NBCANP_B2YMar__c						= 212751;			// 新契約係数ANP　2年前3月
		agencySalesResults.NBCANP_B2YApr__c						= 6221;				// 新契約係数ANP　2年前4月
		agencySalesResults.NBCANP_B2YJun__c						= 10478;			// 新契約係数ANP　2年前6月
		agencySalesResults.NBCANP_B2YMay__c						= 30592;			// 新契約係数ANP　2年前5月
		agencySalesResults.NBCANP_B2YJul__c						= 24627;			// 新契約係数ANP　2年前7月
		agencySalesResults.NBCANP_B2YAug__c						= 36371;			// 新契約係数ANP　2年前8月
		agencySalesResults.NBCANP_B2YSep__c						= 72543;			// 新契約係数ANP　2年前9月
		agencySalesResults.NBCANP_B2YOct__c						= 4328;				// 新契約係数ANP　2年前10月
		agencySalesResults.NBCANP_B2YNov__c						= 25133;			// 新契約係数ANP　2年前11月
		agencySalesResults.NBCANP_B2YDec__c						= 11035;			// 新契約係数ANP　2年前12月
		agencySalesResults.NBCANP_1Q__c							= 526150;			// 新契約係数ANP　当年1Q
		agencySalesResults.NBCANP_2Q__c							= 882398;			// 新契約係数ANP　当年2Q
		agencySalesResults.NBCANP_3Q__c							= 504010;			// 新契約係数ANP　当年3Q
		agencySalesResults.NBCANP_4Q__c							= 769330;			// 新契約係数ANP　当年4Q
		agencySalesResults.NBCANP_LY1Q__c						= 713463;			// 新契約係数ANP　前年1Q
		agencySalesResults.NBCANP_LY2Q__c						= 135241;			// 新契約係数ANP　前年2Q
		agencySalesResults.NBCANP_LY3Q__c						= 375385;			// 新契約係数ANP　前年3Q
		agencySalesResults.NBCANP_LY4Q__c						= 249644;			// 新契約係数ANP　前年4Q
		agencySalesResults.NBCANP_B2Y1Q__c						= 281501;			// 新契約係数ANP　2年前1Q
		agencySalesResults.NBCANP_B2Y2Q__c						= 47291;			// 新契約係数ANP　2年前2Q
		agencySalesResults.NBCANP_B2Y3Q__c						= 133541;			// 新契約係数ANP　2年前3Q
		agencySalesResults.NBCANP_B2Y4Q__c						= 40496;			// 新契約係数ANP　2年前4Q
		agencySalesResults.NBCANP_Progress__c					= 181.98;			// 新契約係数ANP　当年対前年進捗率
		agencySalesResults.NBCANP_ProgressLY__c					= 293.09;			// 新契約係数ANP　前年対前年進捗率
		agencySalesResults.NBCANP_YTD__c						= 1000000;			// 新契約係数ANP（当年YTD）
		agencySalesResults.NBCANP_LYYTD__c						= 900000;			// 新契約係数ANP（前年YTD）
		agencySalesResults.NBCANP_SpInsTypeJan__c				= 106267;			// 新契約係数ANP　特定保険種類　当年1月
		agencySalesResults.NBCANP_SpInsTypeFeb__c				= 269607;			// 新契約係数ANP　特定保険種類　当年2月
		agencySalesResults.NBCANP_SpInsTypeMar__c				= 221354;			// 新契約係数ANP　特定保険種類　当年3月
		agencySalesResults.NBCANP_SpInsTypeApr__c				= 100125;			// 新契約係数ANP　特定保険種類　当年4月
		agencySalesResults.NBCANP_SpInsTypeMay__c				= 89014;			// 新契約係数ANP　特定保険種類　当年5月
		agencySalesResults.NBCANP_SpInsTypeJun__c				= 193870;			// 新契約係数ANP　特定保険種類　当年6月
		agencySalesResults.NBCANP_SpInsTypeJul__c				= 552137;			// 新契約係数ANP　特定保険種類　当年7月
		agencySalesResults.NBCANP_SpInsTypeAug__c				= 456721;			// 新契約係数ANP　特定保険種類　当年8月
		agencySalesResults.NBCANP_SpInsTypeSep__c				= 245678;			// 新契約係数ANP　特定保険種類　当年9月
		agencySalesResults.NBCANP_SpInsTypeOct__c				= 187671;			// 新契約係数ANP　特定保険種類　当年10月
		agencySalesResults.NBCANP_SpInsTypeNov__c				= 87164;			// 新契約係数ANP　特定保険種類　当年11月
		agencySalesResults.NBCANP_SpInsTypeDec__c				= 2130;				// 新契約係数ANP　特定保険種類　当年12月
		agencySalesResults.NBCANP_SpInsTypeLYJan__c				= 82323;			// 新契約係数ANP　特定保険種類　前年1月
		agencySalesResults.NBCANP_SpInsTypeLYFeb__c				= 59441;			// 新契約係数ANP　特定保険種類　前年2月
		agencySalesResults.NBCANP_SpInsTypeLYMar__c				= 571699;			// 新契約係数ANP　特定保険種類　前年3月
		agencySalesResults.NBCANP_SpInsTypeLYApr__c				= 33277;			// 新契約係数ANP　特定保険種類　前年4月
		agencySalesResults.NBCANP_SpInsTypeLYMay__c				= 24156;			// 新契約係数ANP　特定保険種類　前年5月
		agencySalesResults.NBCANP_SpInsTypeLYJun__c				= 77808;			// 新契約係数ANP　特定保険種類　前年6月
		agencySalesResults.NBCANP_SpInsTypeLYJul__c				= 76802;			// 新契約係数ANP　特定保険種類　前年7月
		agencySalesResults.NBCANP_SpInsTypeLYAug__c				= 139441;			// 新契約係数ANP　特定保険種類　前年8月
		agencySalesResults.NBCANP_SpInsTypeLYSep__c				= 159142;			// 新契約係数ANP　特定保険種類　前年9月
		agencySalesResults.NBCANP_SpInsTypeLYOct__c				= 12061;			// 新契約係数ANP　特定保険種類　前年10月
		agencySalesResults.NBCANP_SpInsTypeLYNov__c				= 75052;			// 新契約係数ANP　特定保険種類　前年11月
		agencySalesResults.NBCANP_SpInsTypeLYDec__c				= 162531;			// 新契約係数ANP　特定保険種類　前年12月
		agencySalesResults.NBCANP_SpInsTypeB2YJan__c			= 45226;			// 新契約係数ANP　特定保険種類　2年前1月
		agencySalesResults.NBCANP_SpInsTypeB2YFeb__c			= 23524;			// 新契約係数ANP　特定保険種類　2年前2月
		agencySalesResults.NBCANP_SpInsTypeB2YMar__c			= 212751;			// 新契約係数ANP　特定保険種類　2年前3月
		agencySalesResults.NBCANP_SpInsTypeB2YApr__c			= 6221;				// 新契約係数ANP　特定保険種類　2年前4月
		agencySalesResults.NBCANP_SpInsTypeB2YMay__c			= 10478;			// 新契約係数ANP　特定保険種類　2年前5月
		agencySalesResults.NBCANP_SpInsTypeB2YJun__c			= 30592;			// 新契約係数ANP　特定保険種類　2年前6月
		agencySalesResults.NBCANP_SpInsTypeB2YJul__c			= 24627;			// 新契約係数ANP　特定保険種類　2年前7月
		agencySalesResults.NBCANP_SpInsTypeB2YAug__c			= 36371;			// 新契約係数ANP　特定保険種類　2年前8月
		agencySalesResults.NBCANP_SpInsTypeB2YSep__c			= 72543;			// 新契約係数ANP　特定保険種類　2年前9月
		agencySalesResults.NBCANP_SpInsTypeB2YOct__c			= 4328;				// 新契約係数ANP　特定保険種類　2年前10月
		agencySalesResults.NBCANP_SpInsTypeB2YNov__c			= 25133;			// 新契約係数ANP　特定保険種類　2年前11月
		agencySalesResults.NBCANP_SpInsTypeB2YDec__c			= 11035;			// 新契約係数ANP　特定保険種類　2年前12月
		agencySalesResults.NBCANP_SpInsType1Q__c				= 597228;			// 新契約係数ANP　特定保険種類　当年1Q
		agencySalesResults.NBCANP_SpInsType2Q__c				= 383009;			// 新契約係数ANP　特定保険種類　当年2Q
		agencySalesResults.NBCANP_SpInsType3Q__c				= 1254536;			// 新契約係数ANP　特定保険種類　当年3Q
		agencySalesResults.NBCANP_SpInsType4Q__c				= 276965;			// 新契約係数ANP　特定保険種類　当年4Q
		agencySalesResults.NBCANP_SpInsTypeLY1Q__c				= 713463;			// 新契約係数ANP　特定保険種類　前年1Q
		agencySalesResults.NBCANP_SpInsTypeLY2Q__c				= 135241;			// 新契約係数ANP　特定保険種類　前年2Q
		agencySalesResults.NBCANP_SpInsTypeLY3Q__c				= 375385;			// 新契約係数ANP　特定保険種類　前年3Q
		agencySalesResults.NBCANP_SpInsTypeLY4Q__c				= 249644;			// 新契約係数ANP　特定保険種類　前年4Q
		agencySalesResults.NBCANP_SpInsTypeB2Y1Q__c				= 281501;			// 新契約係数ANP　特定保険種類　2年前1Q
		agencySalesResults.NBCANP_SpInsTypeB2Y2Q__c				= 47291;			// 新契約係数ANP　特定保険種類　2年前2Q
		agencySalesResults.NBCANP_SpInsTypeB2Y3Q__c				= 133541;			// 新契約係数ANP　特定保険種類　2年前3Q
		agencySalesResults.NBCANP_SpInsTypeB2Y4Q__c				= 40496;			// 新契約係数ANP　特定保険種類　2年前4Q
		agencySalesResults.NBCANP_SpInsTypeProgress__c			= 170.43;			// 新契約係数ANP　特定保険種類　当年対前年進捗率
		agencySalesResults.NBCANP_SpInsTypeLY__c				= 293.09;			// 新契約係数ANP　特定保険種類　前年対前年進捗率
		agencySalesResults.NBCANP_SpInsTypeLYYTD__c				= 800000;			// 新契約係数ANP 特定保険種類（前年YTD）
		agencySalesResults.NBCANP_SpInsTypeYTD__c				= 700000;			// 新契約係数ANP 特定保険種類（当年YTD）
		agencySalesResults.NBWANP_FSYTD__c						= 0;				// 新契約換算ANP（YTD）　FS
		agencySalesResults.NBWANP_ProtectionYTD__c				= 0;				// 新契約換算ANP（YTD）　Protection
		agencySalesResults.NBWANP_TotalYTD__c					= 0;				// 新契約換算ANP（YTD）　Total
		agencySalesResults.OfficeAddress__c						= '';				// 事務所　所在地
		agencySalesResults.OfficeName__c						= '';				// 事務所名
		agencySalesResults.Proxy__c								= '代申';			// 代申／非代申
		agencySalesResults.QualifDemoSuspension__c				= 'あり';			// 降格猶予（現在資格）
		agencySalesResults.QualifSimDemoSuspension__c			= 'なし';			// 降格猶予（次回予測資格）
		agencySalesResults.QualifElapsedmonthARegist__c			= 0;				// 登録経過月数
		agencySalesResults.QualifNextYYYYMM__c					= '201710';			// 次回資格査定年月
		agencySalesResults.QualifPrePartnerOrganization__c		= '';				// 協力団体優遇状況
		agencySalesResults.QualifSimEvaPerform__c				= '';				// 成績該当資格
		agencySalesResults.QualifSimNBCANP__c					= 109999999;		// 資格予測　新契約係数ANP
		agencySalesResults.QualifSimNBCANPSpInsType__c			= 999999999;		// 資格予測　新契約係数ANP 特定保険種類
		agencySalesResults.QualifSimIANP__c						= 899999999;		// 資格予測　保有ANP
		agencySalesResults.QualifSimActiveMonth__c				= 11;				// 資格予測　稼動月数
		agencySalesResults.QualifSimIQA__c						= 91.11;			// 資格予測　IQA継続率
		agencySalesResults.QualifSimPeriodNBCANP__c				= '2016/02～2017/07';	// 査定対象期間　新契約係数ANP
		agencySalesResults.QualifSimPeriodNBCANP_SpInsType__c	= '2016/03～2017/08';	// 査定対象期間　新契約係数ANP　特定保険種類
		agencySalesResults.QualifSimMonthEndIANP__c				= '2017/04末';			// 査定対象期間　保有ANP
		agencySalesResults.QualifSimPeriodActiveMonth__c		= '2016/04～2017/09';	// 査定対象期間　稼働月数
		agencySalesResults.QualifSimMonthEndIQA__c				= '2017/07末';			// 査定対象期間　IQA継続率
		agencySalesResults.XHAH_AGCLS__c						= '優績A';			// 現在資格
		agencySalesResults.QualifSim__c							= '優績S';			// 次回予測資格
		agencySalesResults.QualifSpProvision__c					= '';				// 資格特認優遇状況
		agencySalesResults.SectionCode__c						= '';				// 課コード
		agencySalesResults.Segment__c							= 'segment';		// セグメント
		agencySalesResults.YYYYMM__c							= '201706';			// 年月
		agencySalesResults.AGED_KPCDT__c						= '201601';			// 専門資格　合格年月
		agencySalesResults.AGED_KPCLC__c						= '234567';			// 専門資格　合格番号
		agencySalesResults.AGSG_ZSEGMCD__c						= '';				// セグメントコード
		agencySalesResults.XHAH_KCHANNEL__c						= 'cn';				// チャネル
		agencySalesResults.XHAH_KMOFCODE__c						= '123-456-789';	// 代理店登録番号
		agencySalesResults.XHAH_ZSHRMAIN__c						= '';				// 代申／非代申コード
		agencySalesResults.XHAH_ZSOURCE__c						= '';				// ソース
		agencySalesResults.XHAT_AGTYPE__c						= '';				// 登録区分コード
		agencySalesResults.XHAT_KEXEMRSN__c						= '';				// 当社商品の募集資格免除事由コード
		agencySalesResults.XHAT_KMOFCODE__c						= '4567';			// 募集人登録番号
		agencySalesResults.XHAT_KQUALFLG__c						= '';				// 当社商品の募集資格コード
		agencySalesResults.XHAT_ZAGSTAT__c						= '';				// 当社商品の募集資格ステータスコード
		agencySalesResults.XHAT_ZBLGAGCY__c						= '';				// 所属事務所コード
		agencySalesResults.XHAT_ZSUBTRM__c						= '';				// 当社商品の募集資格仮業廃フラグ
		agencySalesResults.XHAT_ZVLDTLC__c						= '';				// 当社での変額登録日
		agencySalesResults.XHAT_ZVLFLG__c						= '';				// 当社での変額資格コード
		agencySalesResults.XHAY_AGNTBR__c						= '';				// 事務所　担当営業部
		agencySalesResults.XHAY_ZMRCODE__c						= '';				// 事務所　担当MR
		agencySalesResults.XHAY_RCMTAB__c						= '';				// 手数料支払いタイプコード
		agencySalesResults.ComPayment__c						= '';				// 手数料支払いタイプ
		agencySalesResults.Agent__c								= null;				// 募集人
		agencySalesResults.FY_LYJan__c							= 0;				// 初年度手数料額　前年1月
		agencySalesResults.FY_LYFeb__c							= 0;				// 初年度手数料額　前年2月
		agencySalesResults.FY_LYMar__c							= 0;				// 初年度手数料額　前年3月
		agencySalesResults.FY_LYApr__c							= 0;				// 初年度手数料額　前年4月
		agencySalesResults.FY_LYMay__c							= 0;				// 初年度手数料額　前年5月
		agencySalesResults.FY_LYJun__c							= 0;				// 初年度手数料額　前年6月
		agencySalesResults.FY_LYJul__c							= 0;				// 初年度手数料額　前年7月
		agencySalesResults.FY_LYAug__c							= 0;				// 初年度手数料額　前年8月
		agencySalesResults.FY_LYSep__c							= 0;				// 初年度手数料額　前年9月
		agencySalesResults.FY_LYOct__c							= 0;				// 初年度手数料額　前年10月
		agencySalesResults.FY_LYNov__c							= 0;				// 初年度手数料額　前年11月
		agencySalesResults.FY_LYDec__c							= 0;				// 初年度手数料額　前年12月
		agencySalesResults.ASY_LYJan__c							= 0;				// 次年度以降手数料額　前年1月
		agencySalesResults.ASY_LYFeb__c							= 0;				// 次年度以降手数料額　前年2月
		agencySalesResults.ASY_LYMar__c							= 0;				// 次年度以降手数料額　前年3月
		agencySalesResults.ASY_LYApr__c							= 0;				// 次年度以降手数料額　前年4月
		agencySalesResults.ASY_LYMay__c							= 0;				// 次年度以降手数料額　前年5月
		agencySalesResults.ASY_LYJun__c							= 0;				// 次年度以降手数料額　前年6月
		agencySalesResults.ASY_LYJul__c							= 0;				// 次年度以降手数料額　前年7月
		agencySalesResults.ASY_LYAug__c							= 0;				// 次年度以降手数料額　前年8月
		agencySalesResults.ASY_LYSep__c							= 0;				// 次年度以降手数料額　前年9月
		agencySalesResults.ASY_LYOct__c							= 0;				// 次年度以降手数料額　前年10月
		agencySalesResults.ASY_LYNov__c							= 0;				// 次年度以降手数料額　前年11月
		agencySalesResults.ASY_LYDec__c							= 0;				// 次年度以降手数料額　前年12月
		agencySalesResults.CB_Jan__c							= -88022;			// 戻入金額　当年1月
		agencySalesResults.CB_Feb__c							= 0;				// 戻入金額　当年2月
		agencySalesResults.CB_Mar__c							= -104707;			// 戻入金額　当年3月
		agencySalesResults.CB_Apr__c							= -10010;			// 戻入金額　当年4月
		agencySalesResults.CB_May__c							= -28108;			// 戻入金額　当年5月
		agencySalesResults.CB_Jun__c							= -2873;			// 戻入金額　当年6月
		agencySalesResults.CB_Jul__c							= -110822;			// 戻入金額　当年7月
		agencySalesResults.CB_Aug__c							= -1218;			// 戻入金額　当年8月
		agencySalesResults.CB_Sep__c							= 0;				// 戻入金額　当年9月
		agencySalesResults.CB_Oct__c							= 0;				// 戻入金額　当年10月
		agencySalesResults.CB_Nov__c							= -273;				// 戻入金額　当年11月
		agencySalesResults.CB_Dec__c							= -6750;			// 戻入金額　当年12月
		agencySalesResults.CB_LYJan__c							= 0;				// 戻入金額　前年1月
		agencySalesResults.CB_LYFeb__c							= 0;				// 戻入金額　前年2月
		agencySalesResults.CB_LYMar__c							= 0;				// 戻入金額　前年3月
		agencySalesResults.CB_LYApr__c							= 0;				// 戻入金額　前年4月
		agencySalesResults.CB_LYMay__c							= 0;				// 戻入金額　前年5月
		agencySalesResults.CB_LYJun__c							= 0;				// 戻入金額　前年6月
		agencySalesResults.CB_LYJul__c							= 0;				// 戻入金額　前年7月
		agencySalesResults.CB_LYAug__c							= 0;				// 戻入金額　前年8月
		agencySalesResults.CB_LYSep__c							= 0;				// 戻入金額　前年9月
		agencySalesResults.CB_LYOct__c							= 0;				// 戻入金額　前年10月
		agencySalesResults.CB_LYNov__c							= 0;				// 戻入金額　前年11月
		agencySalesResults.CB_LYDec__c							= 0;				// 戻入金額　前年12月
		agencySalesResults.CnvsLicense_IRIS__c					= 'あり';				// 当社商品の募集資格
		agencySalesResults.Payment_LYJan__c						= 0;				// 振込手数料額　前年1月
		agencySalesResults.Payment_LYFeb__c						= 0;				// 振込手数料額　前年2月
		agencySalesResults.Payment_LYMar__c						= 0;				// 振込手数料額　前年3月
		agencySalesResults.Payment_LYApr__c						= 0;				// 振込手数料額　前年4月
		agencySalesResults.Payment_LYMay__c						= 0;				// 振込手数料額　前年5月
		agencySalesResults.Payment_LYJun__c						= 0;				// 振込手数料額　前年6月
		agencySalesResults.Payment_LYJul__c						= 0;				// 振込手数料額　前年7月
		agencySalesResults.Payment_LYAug__c						= 0;				// 振込手数料額　前年8月
		agencySalesResults.Payment_LYSep__c						= 0;				// 振込手数料額　前年9月
		agencySalesResults.Payment_LYOct__c						= 0;				// 振込手数料額　前年10月
		agencySalesResults.Payment_LYNov__c						= 0;				// 振込手数料額　前年11月
		agencySalesResults.Payment_LYDec__c						= 0;				// 振込手数料額　前年12月
		agencySalesResults.OT_Jan__c							= 88022;			// その他手数料額　当年1月
		agencySalesResults.OT_Feb__c							= 0;				// その他手数料額　当年2月
		agencySalesResults.OT_Mar__c							= 104707;			// その他手数料額　当年3月
		agencySalesResults.OT_Apr__c							= 10010;			// その他手数料額　当年4月
		agencySalesResults.OT_May__c							= 28108;			// その他手数料額　当年5月
		agencySalesResults.OT_Jun__c							= 2873;				// その他手数料額　当年6月
		agencySalesResults.OT_Jul__c							= 110822;			// その他手数料額　当年7月
		agencySalesResults.OT_Aug__c							= 1218;				// その他手数料額　当年8月
		agencySalesResults.OT_Sep__c							= 0;				// その他手数料額　当年9月
		agencySalesResults.OT_Oct__c							= 0;				// その他手数料額　当年10月
		agencySalesResults.OT_Nov__c							= 273;				// その他手数料額　当年11月
		agencySalesResults.OT_Dec__c							= 6750;				// その他手数料額　当年12月
		agencySalesResults.OT_LYJan__c							= 0;				// その他手数料額　前年1月
		agencySalesResults.OT_LYFeb__c							= 0;				// その他手数料額　前年2月
		agencySalesResults.OT_LYMar__c							= 0;				// その他手数料額　前年3月
		agencySalesResults.OT_LYApr__c							= 0;				// その他手数料額　前年4月
		agencySalesResults.OT_LYMay__c							= 0;				// その他手数料額　前年5月
		agencySalesResults.OT_LYJun__c							= 0;				// その他手数料額　前年6月
		agencySalesResults.OT_LYJul__c							= 0;				// その他手数料額　前年7月
		agencySalesResults.OT_LYAug__c							= 0;				// その他手数料額　前年8月
		agencySalesResults.OT_LYSep__c							= 0;				// その他手数料額　前年9月
		agencySalesResults.OT_LYOct__c							= 0;				// その他手数料額　前年10月
		agencySalesResults.OT_LYNov__c							= 0;				// その他手数料額　前年11月
		agencySalesResults.OT_LYDec__c							= 0;				// その他手数料額　前年12月
		agencySalesResults.XHAH_ZBUSBR__c						= '';				// 母店　担当営業部コード
		agencySalesResults.MotherBranchName__c					= '';				// 母店　担当営業部
		agencySalesResults.MotherMR__c							= '';				// 母店　担当MR

		return agencySalesResults;
	}
}