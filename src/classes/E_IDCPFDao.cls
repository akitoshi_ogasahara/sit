/**
 * ID管理データアクセスオブジェクト
 * CreatedDate 2014/12/11
 * @Author Terrasky
 */
public class E_IDCPFDao {

	/**
	 * ID管理データリストをユーザIdから取得する
	 * @param userId: ユーザID
	 * @return E_IDCPF__c: ID管理情報
	 */
	public static E_IDCPF__c getRecsById(Id userId){
		E_IDCPF__c rec = null;
		List<E_IDCPF__c> recs = [
				SELECT
					  Id 					//カスタムオブジェクト ID
					, OwnerId 				//所有者 ID
					, Name 					//ID番号No
					, User__c 				//ユーザ
					, User__r.ProfileId
					, CLNTNUM__c 			//所有者顧客コード
					, FLAG01__c 			//個人保険照会フラグ
					, FLAG02__c 			//SPVA照会フラグ
					, FLAG03__c 			//投信照会フラグ
					, FLAG04__c 			//顧客検索フラグ
					, FLAG05__c 			//販売状況データフラグ
					, FLAG06__c 			//手続の履歴フラグ
					, ZEBZCLNT__c 			//表示用顧客コード
					, ZEMAILAD__c 			//E-MAILアドレス
					, ZIDOWNER__c 			//所有者コード
					, ZIDTYPE__c 			//ID種別
					, ZINQUIRR__c 			//照会者コード
					, ZWEBID__c 			//ID番号
					, TRCDE01__c 			//トランザクションコード1
					, TRCDE02__c 			//トランザクションコード2
					, TRCDE03__c 			//トランザクションコード3
					, TRCDE04__c 			//トランザクションコード4
					, TRCDE05__c 			//トランザクションコード5
					, TRCDE06__c 			//トランザクションコード6
					, TRCDE07__c 			//トランザクションコード7
					, TRCDE08__c 			//トランザクションコード8
					, TRCDE09__c 			//トランザクションコード9
					, TRCDE10__c 			//トランザクションコード10
					, ZDSPFLAG01__c 		//手数料明細書照会フラグ
					, ZDSPFLAG02__c 		//予備
					, ZDSPFLAG03__c 		//予備
					, ZDSPFLAG04__c 		//予備
					, ZDSPFLAG05__c 		//予備
					, ZDSPFLAG06__c 		//予備
					, ZDSPFLAG07__c 		//予備
					, ZDSPFLAG08__c 		//予備
					, ZDSPFLAG09__c 		//予備
					, ZDSPFLAG10__c 		//予備
					, ZPASSWD02__c 			//手続パスワード
					, ZSTATUS01__c 			//パスワードステータス
					, ZSTATUS02__c 			//手続パスワードステータス
					, ZPASSWD02DATE__c		//手続パスワード変更日
					, UseTempProcedurePW__c	//仮手続PW使用
					, RegularProcedurePW__c	//本手続パスワード
					, AppMode__c			//利用アプリ
				FROM E_IDCPF__c
				WHERE User__c = :userId
				Order by SystemModstamp desc
		];
		if (recs.size() > 0) {
			rec = recs.get(0);
		}
		return rec;
	}

	/**
	 * ID管理データリストをユーザIdのSETから取得する
	 * @param userId: ユーザID
	 * @return List<E_IDCPF__c>: ID管理情報
	 */
	public static List<E_IDCPF__c> getRecsByUserIds(Set<Id> userIds){
		if(userIds==null || userIds.size()==0){
			return new List<E_IDCPF__c>();
		}
		List<E_IDCPF__c> recs = [
				SELECT
					  Id 					//カスタムオブジェクト ID
					, OwnerId 				//所有者 ID
					, Name 					//ID番号No
					, User__c					//ユーザ
					, User__r.Contact.E_CL3PF_ZAGMANGR__c			//管理責任者
					, User__r.Contact.Account.E_CL1PF_ZHEADAY__c	//代理店格コード
					, User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c	//代理店格コード
					, User__r.Contact.E_AccParentCord__c	// 代理店格コード
					, User__r.Contact.E_CL3PF_AGTYPE__c		// 募集人種別
					, User__r.UserType
					, User__r.isActive
					, User__r.AccountId
					, User__r.ProfileId
					, ZWEBID__c					//ID番号
					, ZIDTYPE__c				//ID種別
					, ZIDOWNER__c				//所有者コード
					, ZINQUIRR__c				//照会者コード
					, ZPASSWD02__c				//手続パスワード
					, ZPASSWD02DATE__c			//手続パスワード変更日
					, CLNTNUM__c				//所有者顧客コード
					, ZEBZCLNT__c				//表示用顧客コード
					, ZEMAILAD__c				//E-MAILアドレス
					, ZSTATUS01__c				//パスワードステータス
					, ZSTATUS02__c				//手続パスワードステータス
					, FLAG01__c					//個人保険照会フラグ
					, FLAG02__c					//SPVA照会フラグ
					, FLAG03__c					//投信照会フラグ
					, FLAG04__c					//顧客検索フラグ
					, FLAG05__c					//販売状況データフラグ
					, FLAG06__c					//手続の履歴フラグ
					, TRCDE01__c				//トランザクションコード01
					, TRCDE02__c				//トランザクションコード02
					, TRCDE03__c				//トランザクションコード03
					, TRCDE04__c				//トランザクションコード04
					, TRCDE05__c				//トランザクションコード05
					, TRCDE06__c				//トランザクションコード06
					, TRCDE07__c				//トランザクションコード07
					, TRCDE08__c				//トランザクションコード08
					, TRCDE09__c				//トランザクションコード09
					, TRCDE10__c				//トランザクションコード10
					, ZDSPFLAG01__c				//手数料明細書照会フラグ
					, ZDSPFLAG02__c				//予備
					, ZDSPFLAG03__c				//予備
					, ZDSPFLAG04__c				//予備
					, ZDSPFLAG05__c				//予備
					, ZDSPFLAG06__c				//予備
					, ZDSPFLAG07__c				//予備
					, ZDSPFLAG08__c				//予備
					, ZDSPFLAG09__c				//予備
					, ZDSPFLAG10__c				//予備
					, IsMaintenanceCharge__c	//メンテナンス担当
					, IsScAdmin__c				//自主点検（コンプラ）
					, IsScDirector__c			//自主点検（参照のみ）
					, IsCrApprove__c			//代理店規定管理（承認権限セット）
					, IsCrDpa__c				//代理店規程管理（DPA権限セット)
					, IsShowSREmployee__c		//社内挙績情報参照可能フラグ
					, IsDocMaint__c				//資料メンテナンス 20180214 資料発送申込対応
				FROM E_IDCPF__c
				WHERE User__c in :userIds
		];
		return recs;
	}


	/**
	 * ID管理データリストをIdリストから取得する
	 *			有効なユーザに紐づけられたID管理のみを取得
	 * @param eidcIDs: EIDCレコードIDのSet
	 * @return List<E_IDCPF__c>: ID管理情報
	 */
	public static List<E_IDCPF__c> getActiveUsersRecsByIDs(Set<Id> eidcIDs){
		//デフォルトは削除前のみを取得
		return getActiveUsersRecsByIDs(eidcIDs, false);
	}
	public static List<E_IDCPF__c> getActiveUsersRecsByIDs(Set<Id> eidcIDs, Boolean isDeleted){
		return 	[Select Id
						, isDeleted
						, User__c					//ユーザ
						, User__r.Contact.E_CL3PF_ZAGMANGR__c				//管理責任者
						, User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c	//代理店格コード
						, User__r.Contact.E_AccParentCord__c	// 代理店格コード
						, User__r.Contact.E_CL3PF_AGTYPE__c		// 募集人種別
						, User__r.UserType
						, User__r.isActive
						, User__r.AccountId
						, User__r.ProfileId
						, ZWEBID__c					//ID番号
						, ZIDTYPE__c				//ID種別
						, ZIDOWNER__c				//所有者コード
						, ZINQUIRR__c				//照会者コード
						, ZPASSWD02__c				//手続パスワード
						, ZPASSWD02DATE__c			//手続パスワード変更日
						, CLNTNUM__c				//所有者顧客コード
						, ZEBZCLNT__c				//表示用顧客コード
						, ZEMAILAD__c				//E-MAILアドレス
						, ZSTATUS01__c				//パスワードステータス
						, ZSTATUS02__c				//手続パスワードステータス
						, FLAG01__c					//個人保険照会フラグ
						, FLAG02__c					//SPVA照会フラグ
						, FLAG03__c					//投信照会フラグ
						, FLAG04__c					//顧客検索フラグ
						, FLAG05__c					//販売状況データフラグ
						, FLAG06__c					//手続の履歴フラグ
						, TRCDE01__c				//トランザクションコード01
						, TRCDE02__c				//トランザクションコード02
						, TRCDE03__c				//トランザクションコード03
						, TRCDE04__c				//トランザクションコード04
						, TRCDE05__c				//トランザクションコード05
						, TRCDE06__c				//トランザクションコード06
						, TRCDE07__c				//トランザクションコード07
						, TRCDE08__c				//トランザクションコード08
						, TRCDE09__c				//トランザクションコード09
						, TRCDE10__c				//トランザクションコード10
						, ZDSPFLAG01__c				//手数料明細書照会フラグ
						, ZDSPFLAG02__c				//予備
						, ZDSPFLAG03__c				//予備
						, ZDSPFLAG04__c				//予備
						, ZDSPFLAG05__c				//予備
						, ZDSPFLAG06__c				//予備
						, ZDSPFLAG07__c				//予備
						, ZDSPFLAG08__c				//予備
						, ZDSPFLAG09__c				//予備
						, ZDSPFLAG10__c				//予備
						, IsMaintenanceCharge__c	//メンテナンス担当
						, IsScAdmin__c				//自主点検（コンプラ）
						, IsScDirector__c			//自主点検（参照のみ）
						, IsCrApprove__c			//代理店規定管理（承認権限セット）
						, IsCrDpa__c				//代理店規程管理（DPA権限セット)
						, IsShowSREmployee__c		//社内挙績情報参照可能フラグ
						, IsDocMaint__c				//資料メンテナンス 20180214 資料発送申込対応
				FROM E_IDCPF__c e
				WHERE Id in: eidcIDs
				  AND User__r.isActive = true
				  AND isDeleted = :isDeleted
				ALL ROWS];
	}



	/**
	 * ID管理データリストを代理店格コードから取得する
	 *			有効なユーザに紐づけられたID管理のみを取得
	 * @param zheadays: 代理店格コードのSet
	 * @return List<E_IDCPF__c>: ID管理情報
	 */
	public static List<E_IDCPF__c> getActiveUsersRecsByZHEADAY(String zheaday){
		return 	[Select Id
						, isDeleted
						, User__c					//ユーザ
						, User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c	//代理店格コード
						, User__r.Contact.E_AccParentCord__c	// 代理店格コード
						, User__r.Contact.E_CL3PF_AGTYPE__c		// 募集人種別
						, User__r.UserType
						, User__r.isActive
						, User__r.AccountId
						, User__r.ProfileId
						, User__r.Contact.Account.ParentId
						, ZWEBID__c					//ID番号
						, ZIDTYPE__c				//ID種別
						, ZIDOWNER__c				//所有者コード
						, ZINQUIRR__c				//照会者コード
						, ZPASSWD02__c				//手続パスワード
						, ZPASSWD02DATE__c			//手続パスワード変更日
						, CLNTNUM__c				//所有者顧客コード
						, ZEBZCLNT__c				//表示用顧客コード
						, ZEMAILAD__c				//E-MAILアドレス
						, ZSTATUS01__c				//パスワードステータス
						, ZSTATUS02__c				//手続パスワードステータス
						, FLAG01__c					//個人保険照会フラグ
						, FLAG02__c					//SPVA照会フラグ
						, FLAG03__c					//投信照会フラグ
						, FLAG04__c					//顧客検索フラグ
						, FLAG05__c					//販売状況データフラグ
						, FLAG06__c					//手続の履歴フラグ
						, TRCDE01__c				//トランザクションコード01
						, TRCDE02__c				//トランザクションコード02
						, TRCDE03__c				//トランザクションコード03
						, TRCDE04__c				//トランザクションコード04
						, TRCDE05__c				//トランザクションコード05
						, TRCDE06__c				//トランザクションコード06
						, TRCDE07__c				//トランザクションコード07
						, TRCDE08__c				//トランザクションコード08
						, TRCDE09__c				//トランザクションコード09
						, TRCDE10__c				//トランザクションコード10
						, ZDSPFLAG01__c				//手数料明細書照会フラグ
						, ZDSPFLAG02__c				//予備
						, ZDSPFLAG03__c				//予備
						, ZDSPFLAG04__c				//予備
						, ZDSPFLAG05__c				//予備
						, ZDSPFLAG06__c				//予備
						, ZDSPFLAG07__c				//予備
						, ZDSPFLAG08__c				//予備
						, ZDSPFLAG09__c				//予備
						, ZDSPFLAG10__c				//予備
						, IsMaintenanceCharge__c	//メンテナンス担当
						, IsScAdmin__c				//自主点検（コンプラ）
						, IsScDirector__c			//自主点検（参照のみ）
						, IsCrApprove__c			//代理店規定管理（承認権限セット）
						, IsCrDpa__c				//代理店規程管理（DPA権限セット)
						, IsShowSREmployee__c		//社内挙績情報参照可能フラグ
						, IsDocMaint__c				//資料メンテナンス 20180214 資料発送申込対応
				FROM E_IDCPF__c e
				WHERE (User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c = :zheaday
				   OR User__r.Contact.Account.E_CL1PF_ZHEADAY__c = :zheaday)
				  AND User__r.isActive = true];
	}

	/**
	 * PushOut通知　募集人レコードIDよりID管理情報を取得する
	 * @param Set<String> ContactIds
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getNotificationRecsByUserContactId(Set<String> conIds){
		return [Select Id
					, User__r.Id					// User.ID
					, USer__r.Email
					, ContactCL3PF_AGNTNUM__c 		// 募集人コード（Contact数式）
				From E_IDCPF__c
				Where IsNotNotification__c = false 	// 通知対象外
				And FLAG01__c = :E_Const.IS_INQUIRY_1			// 個人保険照会フラグ
				And ZSTATUS01__c = :E_Const.ZSTATUS01_ENABLE	// パスワードステータス
				And ZDSPFLAG02__c = :E_Const.ZDSPFLAG02_IRIS	// 利用システムフラグ TODO ID適正化
				And User__r.IsActive = true			// User.有効
				And User__r.ContactId In :conIds 	// User.募集人ID
		];
	}

	/**
	 * PushOut通知　募集人レコードIDよりID管理情報を取得する
	 * @param Set<String> ContactIds
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getNotificationRecsByUserContactIdQuery(Set<Id> conIds, Boolean isInquiry){
		String soql = '';

		soql += ' Select';
		soql += ' Id';
		soql += ' ,User__r.Id';
		soql += ' ,USer__r.Email';
		soql += ' ,USer__r.UserType';
		soql += ' ,User__r.ContactId';
		soql += ' ,ContactCL3PF_AGNTNUM__c';
		soql += ' From';
		soql += ' E_IDCPF__c';
		soql += ' Where';
		soql += ' IsNotNotification__c = false';		// 通知対象外
		soql += ' And ZSTATUS01__c = \'' + E_Const.ZSTATUS01_ENABLE + '\'';	// パスワードステータス
		soql += ' And ZDSPFLAG02__c = \'' + E_Const.ZDSPFLAG02_IRIS + '\'';	// 利用システムフラグ
		soql += ' And User__r.IsActive = true';			// User.有効
		soql += ' And User__r.ContactId In :conIds';	// User.募集人ID
		// 個人保険照会フラグ
		if(isInquiry){
			soql += ' And FLAG01__c = \'' + E_Const.IS_INQUIRY_1 + '\'';
		}

		return Database.query(soql);
	}

	/**
	 * PushOut通知　募集人コード（5桁）よりID管理情報を取得する
	 * @param Set<String> 募集人コード（5桁）
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getNotificationRecsByContactAgentNum5Query(Set<String> agentNums, Boolean isInquiry){
		String soql = '';

		soql += ' Select';
		soql += ' Id';
		soql += ' ,User__r.Id';
		soql += ' ,USer__r.Email';
		soql += ' ,USer__r.UserType';
		soql += ' ,User__r.ContactId';
		soql += ' ,User__r.Contact.AgentNum5__c';
		soql += ' ,ContactCL3PF_AGNTNUM__c';
		soql += ' From';
		soql += ' E_IDCPF__c';
		soql += ' Where';
		soql += ' IsNotNotification__c = false';		// 通知対象外
		soql += ' And ZSTATUS01__c = \'' + E_Const.ZSTATUS01_ENABLE + '\'';	// パスワードステータス
		soql += ' And ZDSPFLAG02__c = \'' + E_Const.ZDSPFLAG02_IRIS + '\'';	// 利用システムフラグ　TODO ID適正化
		soql += ' And User__r.IsActive = true';			// User.有効
		soql += ' And User__r.Contact.AgentNum5__c In :agentNums';	// User.募集人.募集人コード（5桁）
		//20171109 銀行代理店開放 START
		soql += ' And User__r.Contact.Account.E_NoEMail__c = false';	// User.募集人.代理店事務所.代理店格.メール停止対象
		//20171109 銀行代理店開放 END
		// 個人保険照会フラグ
		if(isInquiry){
			soql += ' And FLAG01__c = \'' + E_Const.IS_INQUIRY_1 + '\'';
		}

		return Database.query(soql);
	}

	/**
	 * PushOut通知　照会者コードよりID管理情報を取得する
	 * @param Set<String> 照会者コード
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getNotificationRecsByZINQUIRR(Set<String> grpCode){
		return [Select Id
					, User__r.Id					// User.ID
					, USer__r.Email
					, ContactCL3PF_AGNTNUM__c 		// 募集人コード（Contact数式）
				From E_IDCPF__c
				Where IsNotNotification__c = false 	// 通知対象外
				And FLAG01__c = :E_Const.IS_INQUIRY_1			// 個人保険照会フラグ
				And ZSTATUS01__c = :E_Const.ZSTATUS01_ENABLE	// パスワードステータス
				And ZDSPFLAG02__c = :E_Const.ZDSPFLAG02_IRIS	// 利用システムフラグ TODO ID適正化
				And User__r.IsActive = true			// User.有効
				And ZINQUIRR__c In :grpCode 		// 照会者コード
		];
	}

	/**
	 * プロファイル名よりID管理情報を取得する
	 * @param Set<String> プロファイル名
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getRecsByProfileNames(Set<String> profileNames){
		return [Select Id
				, User__r.Id					// User.ID
				, USer__r.Email
				From E_IDCPF__c
				Where User__r.IsActive = true	// User.有効
				And User__r.Profile.Name In :profileNames 	// プロファイル名
		];
	}

	/**
	 * ユーザよりID管理情報を取得する
	 * @param Set<String> userIds
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getNotificationRecsByUserIds(Set<Id> userIds){
		return [Select Id
				, User__r.Id					// User.ID
				, USer__r.Email
				, User__r.UserType
				From E_IDCPF__c
				Where User__r.IsActive = true	// User.有効
				And User__r.Id In :userIds 		// User.ID
		];
	}

	/**
	 * ID管理とそれに紐づくEIDRを取得する
	 * @param  Id　recId
	 * @return E_IDCPF__c
	 */
	public static E_IDCPF__c getEIDRById(Id recId){
		return [SELECT Id
				, ZSTATUS01__c
				, ZEMAILAD__c
				, ZIDTYPE__c
				, ZWEBID__c
				, ZIDOWNER__c
				, ZINQUIRR__c
				, dataSyncDate__c
				, (SELECT Id
					, dataSyncDate__c
					, ZUPDFLAG__c
				   FROM IR__r)
				FROM E_IDCPF__c
				WHERE Id = :recId
		];
	}

	/* 代理店格コードのセットからID管理のリストを取得
	 * @param Set<String> parentCodes
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getRecsByParentCodes(Set<String> parentCodes){
		return [Select Id
				, Name
				, OwnerId
				, User__c
				, Contact__c
				, User__r.AccountId
				, User__r.Contact.E_AccParentCord__c
				FROM E_IDCPF__c
				WHERE User__r.Contact.E_AccParentCord__c IN :parentCodes
				AND User__r.Contact.E_CL3PF_ZAGMANGR__c =:'Y'
				AND User__r.IsActive = true
				And ZSTATUS01__c = :E_Const.ZSTATUS01_ENABLE	// パスワードステータス
				Order by User__r.Contact.E_AccParentCord__c
				];
	}

	/**
	 * ユーザ.ContactIdをキーにID管理のリストを取得
	 * @param Set<Id> ContactのIdセット
	 * @return List<E_IDCPF__c>
	 */
	public static List<E_IDCPF__c> getRecsByContactIds(Set<Id> conIds){
			return [Select
						Id
						, User__r.Id
						, OwnerId 				//所有者 ID
						, Name 					//ID番号No
						, User__c					//ユーザ
						, User__r.Contact.E_CL3PF_ZAGMANGR__c			//管理責任者
						, User__r.Contact.Account.E_CL1PF_ZHEADAY__c	//代理店格コード
						, User__r.Contact.Account.Parent.E_CL1PF_ZHEADAY__c	//代理店格コード
						, User__r.Contact.E_AccParentCord__c	// 代理店格コード
						, User__r.Contact.E_CL3PF_AGTYPE__c		// 募集人種別
						, User__r.UserType
						, User__r.isActive
						, User__r.AccountId
						, User__r.ProfileId
						, ZWEBID__c					//ID番号
						, ZIDTYPE__c				//ID種別
						, ZIDOWNER__c				//所有者コード
						, ZINQUIRR__c				//照会者コード
						, ZPASSWD02__c				//手続パスワード
						, ZPASSWD02DATE__c			//手続パスワード変更日
						, CLNTNUM__c				//所有者顧客コード
						, ZEBZCLNT__c				//表示用顧客コード
						, ZEMAILAD__c				//E-MAILアドレス
						, ZSTATUS01__c				//パスワードステータス
						, ZSTATUS02__c				//手続パスワードステータス
						, FLAG01__c					//個人保険照会フラグ
						, FLAG02__c					//SPVA照会フラグ
						, FLAG03__c					//投信照会フラグ
						, FLAG04__c					//顧客検索フラグ
						, FLAG05__c					//販売状況データフラグ
						, FLAG06__c					//手続の履歴フラグ
						, TRCDE01__c				//トランザクションコード01
						, TRCDE02__c				//トランザクションコード02
						, TRCDE03__c				//トランザクションコード03
						, TRCDE04__c				//トランザクションコード04
						, TRCDE05__c				//トランザクションコード05
						, TRCDE06__c				//トランザクションコード06
						, TRCDE07__c				//トランザクションコード07
						, TRCDE08__c				//トランザクションコード08
						, TRCDE09__c				//トランザクションコード09
						, TRCDE10__c				//トランザクションコード10
						, ZDSPFLAG01__c				//手数料明細書照会フラグ
						, ZDSPFLAG02__c				//予備
						, ZDSPFLAG03__c				//予備
						, ZDSPFLAG04__c				//予備
						, ZDSPFLAG05__c				//予備
						, ZDSPFLAG06__c				//予備
						, ZDSPFLAG07__c				//予備
						, ZDSPFLAG08__c				//予備
						, ZDSPFLAG09__c				//予備
						, ZDSPFLAG10__c				//予備
						, IsMaintenanceCharge__c	//メンテナンス担当
						, IsScAdmin__c				//自主点検（コンプラ）
						, IsScDirector__c			//自主点検（参照のみ）
						, IsCrApprove__c			//代理店規定管理（承認権限セット）
						, IsCrDpa__c				//代理店規程管理（DPA権限セット)
						, IsShowSREmployee__c		//社内挙績情報参照可能フラグ
						, IsDocMaint__c				//資料メンテナンス 20180214 資料発送申込対応
					From
						E_IDCPF__c
					Where
						User__r.ContactId In :conIds];
	}
}