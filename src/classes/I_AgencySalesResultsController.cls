public with sharing class I_AgencySalesResultsController extends I_AbstractSalesResultsController {

    //メインアカウント
    public String mainAgentId {get;private set;}
    //ページメッセージ
    public E_PageMessagesHolder pageMessages {get;set;}
    //表示タイプ
    public String viewType {get;private set;}
    //挙積情報
    public E_AgencySalesResults__c sResults {get; private set;}
    //ユーザID
    public String userid {get; private set;}
    //ID管理フラグ
    public Boolean haveEIDC {get;private set;}
    //有効なユーザが紐づく募集人ID
    public String activeAgentId {get;private set;}
    //資格情報
    public String qualificationType {get;set;}
    //メモ欄テキスト
    public String memo {get;set;}
    //print表示フラグ
    public Boolean isPrint {get;private set;}
    //表示する資格情報
    public String selectedQulif {get; private set;}

    //ディスクレイマー
    public List<String> getDisclaimers(){
        List<String> disclaimers = new List<String>();
        disclaimers.add(getDisclaimer().get('AMS|SR|001'));
        if(access.isEmployee()){
            disclaimers.add(getDisclaimer().get('AMS|SR|002'));
        }else{
            disclaimers.add('');
        }
        // 販売実績値
        disclaimers.add(getDisclaimer().get('AMS|SR|006'));
        disclaimers.add(getDisclaimer().get('AMS|SR|014'));
        disclaimers.add(getDisclaimer().get('AMS|SR|015'));
        disclaimers.add(getDisclaimer().get('AMS|SR|016'));
        return disclaimers;
    }
    //手数料ディスクレイマー
    public List<String> getChartDisclaimers(){
        List<String> disclaimers = new List<String>();
        disclaimers.add(getDisclaimer().get('AMS|SR|003'));
        disclaimers.add(getDisclaimer().get('AMS|SR|013'));
        return disclaimers;
    }
    //資格査定詳細情報ディスクレイマー
    public List<String> getLADisclaimers(){
        List<String> disclaimers = new List<String>();
        disclaimers.add(getDisclaimer().get('AMS|SR|004'));
        return disclaimers;
    }
    public List<String> getMsgSRAgent(){
        return new List<String>{getMSG().get('AMS|SR|007'),getDisclaimer().get('AMS|SR|008')};
    }
    //商品内訳ディスクレイマー
    public List<String> getProductDisclaimers(){
        List<String> disclaimers = new List<String>();
        disclaimers.add(getDisclaimer().get('AMS|SR|012'));
        return disclaimers;
    }
    /**
     * プレビューURL
     */
    public String getPreviewURL (){
        if(viewType == '1'){
            return 'IRIS_AgencySalesResultsPrint?agency=' + agencyid;
        }else if(viewType == '2'){
            return 'IRIS_AgencySalesResultsPrint?office=' + officeid;
        }
        return '';
    }
    /**
     * 拠点MRは支社コードを返す
     */
    public String getBrNo(){
        //BR** または 社員以外は空白を返す
        if(!access.isEmployee() || access.ZINQUIRR_is_BRAll()){
            return '';
        }
        return access.getAccessKindFromThree();
    }

    /*
     * 手数料ボタン表示フラグ
     */
    public Boolean getCanFeeDetail(){
        return access.canFeeDetail();
    }
    /**
     * 商品内訳表示制御
     */
    public Boolean getHasProduct(){
        return !sResults.E_ProductBreakdowns__r.isEmpty();
    }

    private final String ACHIEVEMENT_LABEL_ACHSS = '優績SS';              //優績SS
    private final String ACHIEVEMENT_LABEL_ACHS = '優績S';
    private final String ACHIEVEMENT_LABEL_ACHA = '優績A';
    private final String ACHIEVEMENT_LABEL_ACHB = '優績B';
    private final String ACHIEVEMENT_LABEL_ADV  = '上級';
    private final String ACHIEVEMENT_LABEL_NORM = '普通';
    private final String ACHIEVEMENT_LABEL_PROT = 'プロテクション優績特則';    //プロテクション優績特則
    private final Set<String> ACHIEVEMENT_LABELS = new Set<String>{     //優績SS、プロテクション優績特則
        ACHIEVEMENT_LABEL_ACHSS,
        ACHIEVEMENT_LABEL_ACHS,
        ACHIEVEMENT_LABEL_ACHA,
        ACHIEVEMENT_LABEL_ACHB,
        ACHIEVEMENT_LABEL_ADV,
        ACHIEVEMENT_LABEL_NORM,
        ACHIEVEMENT_LABEL_PROT
    };

    public Boolean getIsShowAchievement(){
        return ACHIEVEMENT_LABELS.contains(E_Util.emAlphabetToEn(sResults.QualifSim_Formula__c));
    }

    //コンストラクタ
    public I_AgencySalesResultsController() {
        pageMessages = getPageMessages();
        //デフォルト0としてすべて非表示
        viewType = '0';
        userid = '';
        memo = '';
        isPrint = true;
    }

    /**
     * メニューキー
     */
    protected override String getLinkMenuKey() {
        return 'agentList';
    }

    /**
     * リダイレクト
     */
    protected override PageReference doRedirect() {

        //URLパラメータなしの場合はログインユーザで判定する
        if(String.isBlank(agencyId) && String.isBlank(officeId) && String.isBlank(agentId)){
            // ユーザ判定
            if (access.isEmployee()) {
                // 本社営業部(BR** リダイレクト処理)
                if(access.ZINQUIRR_is_BRAll()){
                    return redirectUnitPage();
                // 拠点MR(リダイレクト処理)
                }else{
                    return redirectEmployeeSRPage();
                }
            }else {
                User user = [Select Contact.Account.ParentId,AccountId,ContactId,Contact.E_CL3PF_AGNTNUM__c From User Where Id  =: UserInfo.getUserId()];
                // AH(代理店格)
                if (access.isAuthAH()){
                    agencyId = String.valueOf(user.Contact.Account.ParentId);
                // AY(事務所)
                } else if(access.isAuthAY()){
                    //事務所のID
                    if(access.getOfficeCode() == access.getAccessKindFromThree()){
                        officeId = String.valueOf(user.AccountId);
                    }else{
                        Account office = E_AccountDaoWithout.getRecByAccCode(access.getAccessKindFromThree());
                        officeId = (office == null) ? '' : String.valueOf(office.id);
                    }
                // AT(募集人)
                } else if(access.isAuthAT()){
                    //募集人のID
                    agentId = String.valueOf(user.ContactId);
                }
            }
        }
        //条件+作成日が最新のもの一件
        //階層がAH,AY,ATと一致している、かつ、Idも一致しているレコードを取得する
        String soqlWhere = 'BusinessDate__c = \'' + getBizDate() + '\' ';
        if(String.isNotBlank(agencyId)){
            viewType = '1';
            soqlWhere += 'AND Hierarchy__c = \'' + E_Const.ZIDTYPE_AH + '\' '
                      + 'AND ParentAccountId__c = \''  + agencyId + '\' ';
        }else if(String.isNotBlank(officeId)){
            viewType = '2';
            soqlWhere += 'AND Hierarchy__c = \'' + E_Const.ZIDTYPE_AY + '\' '
                      + 'AND AccountId__c = \''  + officeId + '\' ';
        }else if(String.isNotBlank(agentId)){
            activeAgentId = agentId;
            // メインアカウント（募集人コードの5桁）を取得
            Contact con = E_ContactDaoWithout.getRecById(agentId);

            haveEIDC = E_IDCPFDaoWithout.getRecsByContactId(agentId) == null ? false : true ;       //1229追加

            if(access.isEmployee()){
                userid = E_UserDao.getRecByContactId(con.id);
            }
            if(con.E_CL3PF_AGNTNUM__c.length() <> 5){
                // メインアカウント（募集人コードの5桁）を取得
                List<Contact> cons = E_ContactDaoWithout.getRecByAgntNum(con.E_CL3PF_AGNTNUM__c.Left(5));
                if(cons.size() != 1){
                    pageMessages.addErrorMessage(getMSG().get('DNI|004'));
                    return null;
                }
                mainAgentId = String.valueOf(cons[0].id);
            }else{
                mainAgentId = String.valueOf(con.id);
            }
            viewType = '3';
            soqlWhere += 'AND Hierarchy__c = \'' + E_Const.ZIDTYPE_AT + '\' '
                      + 'AND AgentId__c = \''  + mainAgentId + '\' ';
        }else{
            //エラー
            pageMessages.addErrorMessage(getMSG().get('ERR|999'));
        }
        sResults = E_AgencySalesResultsDao.getRecBysWhere(soqlWhere);
        if(sResults != null){
            isShowSR = true;

            String displayQulif = '';
            //getのメソッドにしてreturnさせてもいいかもその代わり、処理順序がsResultsより先に来ないことが前提
            try {
                displayQulif = ApexPages.currentPage().getParameters().get('qualiftype');
            }catch(Exception e){

            }
            if(String.isNotBlank(displayQulif)){
                selectedQulif = displayQulif;
            }else{
                selectedQulif = sResults.XHAH_AGCLS__c;
            }
        }
        

        return null;
    }

    /**
     * 契約者一覧へ遷移
     */
    private String URL_PARAM_FILTER_ID = 'id';
    public PageReference moveToOwnerList() {
        setPolicyCondition();

        PageReference pr = Page.IRIS_Owner;
        pr.getParameters().put(I_Const.URL_PARAM_MENUID, iris.irisMenus.menuItemsByKey.get('ownerList').record.Id);
        pr.setRedirect(true);

        return pr;
    }

    /**
     * 新契約一覧へ遷移
     */
    public PageReference moveToNewPolicyList() {
        //ToDo共通化
        setPolicyCondition();
        PageReference pr = Page.IRIS_NewPolicy;
        pr.setRedirect(true);

        return pr;
    }
    private void setPolicyCondition (){
        String id = ApexPages.currentPage().getParameters().get(URL_PARAM_FILTER_ID);
        String field = '';
        if(viewType == '1'){
            field = I_Const.CONDITION_FIELD_AGENCY;
        }else if (viewType == '2') {
            field = I_Const.CONDITION_FIELD_OFFICE;
        }else if (viewType == '3') {
            field = I_Const.CONDITION_FIELD_AGENT;
        }
        E_IRISHandler.setPolicyCondition(field, id);
    }
    /*
     *ログレコード作成
     */
    public PageReference createLog(){
        //String qualificationType = System.currentPageReference().getParameters().get('qualificationType');
        E_Log__c elog = E_LogUtil.createLog();
        //必要な情報をjsonにする
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartobject();

        //nullの場合にはBlankを代入
        memo = E_Util.null2Blank(memo);
        qualificationType = E_Util.null2Blank(qualificationType);


        gen.writeStringField('sResultsId', sResults.Id );               //代理店挙積情報レコード
        gen.writeStringField('qualificationType', qualificationType);   //資格情報
        gen.writeStringField('memo', memo );                            //メモ
        gen.writeStringField('viewType', viewType);                     //ビュータイプ
        gen.writeBooleanField('isEmployee',access.isEmployee());        //管理者ログインフラグ
        
        gen.writeEndObject();

        elog.detail__c = gen.getAsString();
        //ログ情報を設定してinsertする
        PageReference pr = Page.IRIS_NNLife_AgencyInfo;
        elog.AccessPage__c =  pr.getUrl();//遷移先のページを設定
        elog.ActionType__c = E_LogUtil.NN_LOG_ACTIONTYPE_DOWNLOAD;
        elog.Name = 'pdf出力';

        insert elog;

        //遷移先を指定し、作成したelogのIdを渡してページ遷移
        pr.getParameters().put('id', elog.Id);
        return pr;
    }
}