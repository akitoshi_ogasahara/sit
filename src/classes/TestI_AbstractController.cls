@isTest
private class TestI_AbstractController {
	static testMethod void initTest01() {
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgencyController iabs = new I_AgencyController();
				iabs.pageAction();

				system.assertEquals(iabs.activePage, null);
				system.assertEquals(iabs.menu, null);
				system.assertEquals(E_IRISHandler.getInstance().getIsIRIS(), false);
				system.assertEquals(E_AccessController.getInstance().isGuest(), false);
			Test.stopTest();
		}
	}

	static testMethod void initTest02() {
		// 実行ユーザ作成（Guest）
		String userName = 'testUser@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = 'testUser'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		insert actUser;

		E_IDCPF__c userIdcpf = new E_IDCPF__c(
					 User__c = actUser.Id
					,OwnerId = actUser.Id
					,ZSTATUS01__c = '1'
					,ZINQUIRR__c = 'BR**'
					,ZDSPFLAG01__c = '1'
					,AppMode__c = I_Const.APP_MODE_IRIS
				);
		insert userIdcpf;

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgencyController iabs = new I_AgencyController();
				iabs.pageAction();

				/** E_IRISHandler L.80~86 12/26リリース時の制限 **/
				//system.assertEquals(E_IRISHandler.getInstance().getIsIRIS(), true);
				system.assertEquals(E_AccessController.getInstance().isGuest(), true);
			Test.stopTest();
		}
	}

	static testMethod void ConstructorTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c m = new I_MenuMaster__c(Name = 'testMenu', Required_ZIDTYPE__c = 'AY', UrlParameters__c = 'testParameter');
				insert m;
		
				I_PageMaster__c pm = new I_PageMaster__c(Name = 'testPage', Menu__c = m.Id);
				insert pm;
				pr.getParameters().put(I_Const.URL_PARAM_PAGE, pm.Id);

				I_CMSController iabs = new I_CMSController();
				iabs.pageAction();

				system.assertEquals(iabs.activePage.Menu__c, pm.Menu__c);
			Test.stopTest();
		}
	}

	static testMethod void ConstructorTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgencyController iabs = new I_AgencyController();
				iabs.pageAction();

				system.assertEquals(I_MenuProvider.getInstance().activeMenu, null);
			Test.stopTest();
		}
	}

	static testMethod void ConstructorTest03() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);
		pr.getParameters().put(I_Const.URL_PARAM_PAGE, 'testParamPage');

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_MenuMaster__c imenu = new I_MenuMaster__c(MenuKey__c = 'testKey');
				E_IRISHandler.getInstance().setLinkMenuKey('testKey02');

				I_CMSController iabs = new I_CMSController();
				iabs.pageAction();
			Test.stopTest();
		}
	}

	static testMethod void accessLogTest01() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				E_PageMessagesHolder.getInstance().addErrorMessage('testSummary');
				I_AgencyController iabs = new I_AgencyController();
				iabs.pageAction();

				system.assertEquals(E_PageMessagesHolder.getInstance().getHasErrorMessages(), true);
			Test.stopTest();
		}
	}

	static testMethod void accessLogTest02() {
		// 実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'MR01', 'ＭＲ');
		TestI_TestUtil.createIDCPF(true, actUser.Id, null);

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgencyController iabs = new I_AgencyController();
				iabs.pageAction();

				system.assertEquals(E_PageMessagesHolder.getInstance().getHasErrorMessages(), false);
			Test.stopTest();
		}
	}

	static testMethod void accessLogTest03() {
		// 実行ユーザ作成
		String userName = 'testUser@terrasky.ingtesting';
		Profile p = [SELECT Id FROM Profile WHERE UserLicense.Name = 'Guest User License' LIMIT 1];
		User actUser = new User(
			  Lastname = 'testUser'
			, Username = userName
			, Email = userName
			, ProfileId = p.Id
			, Alias = 'tUser'
			, TimeZoneSidKey = UserInfo.getTimeZone().getID()
			, LocaleSidKey = UserInfo.getLocale()
			, EmailEncodingKey = 'UTF-8'
			, LanguageLocaleKey = UserInfo.getLanguage()
		);
		insert actUser;

		// ページ表示
		PageReference pr = Page.IRIS;
		Test.setCurrentPage(pr);

		system.runAs(actUser) {
			// 権限セットを付与
			TestI_TestUtil.createBasePermissions(actUser.Id);

			Test.startTest();
				I_AgencyController iabs = new I_AgencyController();
				iabs.pageAction();

				system.assertEquals(E_LogUtil.requiredLogging(), false);
			Test.stopTest();
		}
	}
}