/**
* Listenerの基底クラス
*
*/
public abstract class API_Listener {
	//独自の例外クラス
	public virtual class RestAPIException extends Exception{}

	public class RestAPIAuthException extends RestAPIException{}

	//ユーザ情報
	protected User usr{get{
							if(usr == null){
								usr = [SELECT id,isActive,Contact.Id FROM User WHERE id =: UserInfo.getUserId()];
							}
							return usr;
						} 
						private set;}
	//ID管理
	protected E_IDCPF__c eidc{get{
								if(eidc == null){
									eidc = [SELECT id,ZSTATUS01__c,FLAG01__c FROM E_IDCPF__c WHERE User__c =: UserInfo.getUserId()];
								}
								return eidc;
							}
							private set;}


	//ユーザー有効チェック  サブクラスにて追加実装
	protected virtual Boolean hasAuth(){
		return true;
	}

	//レスポンスの共通部分定義
	public class responseErrorMessage{
		public String msgId {get;set;}
		public String message {get;set;}

		public responseErrorMessage(){
			this.msgId = '';
			this.message = '';
		}
	}

	//レスポンスのエラーメッセージ格納クラス
	public virtual class responseBody{

		public  responseBody(){
			errors = new List<responseErrorMessage>();
		}

		public List<responseErrorMessage> errors;
		public List<responseErrorMessage> addErrorMessage(String mId){
			//引数をkeyにメッセージを取得
			API_Const.API_ErrorDefined errDefined = API_Const.errorMessagesMap.get(mId);
			responseErrorMessage resMessage = new responseErrorMessage();
			resMessage.msgId = errDefined.code;
			resMessage.message = errDefined.msg;

			errors.add(resMessage);
			return errors;
		}
	}

	public void executeGet(){
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		//エラーメッセージ用
		responseBody rb = new responseBody();

		try{
			//ユーザー権限チェック
			if(!(usr.isActive && eidc.ZSTATUS01__c == E_Const.PW_STATUS_OK)){
				//エラーメッセージををセット
				res.statusCode = 403;
				rb.addErrorMessage('AUTH_NO_ACCESS');
				res.responseBody = Blob.valueOf(JSON.serialize(rb));
				return;
			}
			//権限チェックメソッド呼び出し
			if(hasAuth()){	//例外があればthrow
				//正常系をデフォルトとしてセット
				res.statusCode = 200;	
				//レスポンス生成作成メソッド呼び出し
				createRestResponse(req,res);
			}else{
				throw new RestAPIAuthException('AUTH_NO_REASON');	//原因不明エラー
			}
		}catch(RestAPIAuthException e){
			res.statusCode = 403;
			rb.addErrorMessage(e.getMessage());
			res.responseBody = Blob.valueOf(JSON.serialize(rb));
		}catch(RestAPIException e){
			//異常処理（カスタマイズ）
			res.statusCode = 400;
			rb.addErrorMessage(e.getMessage());
			res.responseBody = Blob.valueOf(JSON.serialize(rb));
		}catch(Exception e){
			//異常処理（システム）
			res.statusCode = 500;
			res.responseBody = Blob.valueOf(e.getMessage());
		}
	}

	//レスポンス生成の抽象メソッド
	protected abstract void createRestResponse(RestRequest req,RestResponse res);

}