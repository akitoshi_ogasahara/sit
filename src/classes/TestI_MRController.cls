@isTest
private class TestI_MRController {

	private static testMethod void pageAction_test001(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(false, 'fstest', 'ＭＲ');
		actUser.Unit__c = 'テスト営業部';
		insert actUser;
		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = Date.newInstance(2017, 1, 10));

		System.runAs(actUser){
			Test.startTest();
			I_MRController controller = new I_MRController();
			controller.brNo = '88';

			Test.stopTest();

			System.assert(!controller.MRRows.isEmpty());
			System.assertEquals(controller.getListMaxRows(),I_Const.LIST_MAX_ROWS);
			System.assertEquals(controller.bizDate, '2017/01/10');
		}
	}
	private static testMethod void sortRows_test001(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(false, 'fstest', 'ＭＲ');
		actUser.Unit__c = 'テスト営業部';
		insert actUser;
		User user = TestI_TestUtil.createUser(false, 'fstest2', 'ＭＲ');
		user.Unit__c = 'テスト営業部';
		insert user;

		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = System.today());

		PageReference pr = Page.IRIS_SalesResults;
		Test.setCurrentPage(pr);

		System.runAs(actUser){
			Test.startTest();
			I_MRController controller = new I_MRController();
			controller.brNo = '88';
			controller.getMRRecList();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_MRNAME);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_MRCODE);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_FS);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_FSYTD);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_PRO);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_PROYTD);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_COLI);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_COLIYTD);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_TOTAL);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_TOTALYTD);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_AGENCY);
			controller.sortIsAsc = true;
			controller.sortRows();

			pr.getParameters().put('st', I_MRController.SORT_TYPE_AGENT);
			controller.sortIsAsc = true;
			controller.sortRows();
			controller.sortRows();

			Test.stopTest();

		}
	}

	private static testMethod void getSR_test001(){
		// 実行ユーザ作成（MR）
		User actUser = TestI_TestUtil.createUser(false, 'fstest', 'ＭＲ');
		actUser.Unit__c = 'テスト営業部';
		insert actUser;
		User user = TestI_TestUtil.createUser(false, 'fstest2', 'ＭＲ');
		user.Unit__c = 'テスト営業部';
		insert user;

		E_IDCPF__c src = new E_IDCPF__c(User__c = actUser.id,OwnerId = actUser.id,ZIDTYPE__c = 'EP',ZINQUIRR__c = 'BR88',AppMode__c = 'IRIS');
		insert src;

		// テストデータ作成
		E_Area__c area = new E_Area__c();
		insert area;
		E_Unit__c unit = new E_Unit__c(Area__c = area.id,BRANCH__c = '88',Name = 'テスト営業部');
		insert unit;

		//営業日
		insert new E_BizDataSyncLog__c(kind__c = 'F', BatchEndDate__c = System.now(), InquiryDate__c = Date.today());

		System.runAs(actUser){
			// test用代理店挙績情報作成
			List<E_EmployeeSalesResults__c> srList = new List<E_EmployeeSalesResults__c>();
			srList.add(new E_EmployeeSalesResults__c(BusinessDate__c=System.now().format('yyyyMMdd'), MR__c=actUser.Id));
			srList.add(new E_EmployeeSalesResults__c(BusinessDate__c=System.now().format('yyyyMMdd'), MR__c=user.Id));
			insert srList;

			Test.startTest();
				I_MRController controller = new I_MRController();
				controller.brNo = '88';
				controller.isDispSR = true;

				controller.readyAction();

				System.assertEquals(controller.rowCount, 20);
				controller.addRows();
				System.assertEquals(controller.getTotalRows(), 2);
			Test.stopTest();
		}
	}

}