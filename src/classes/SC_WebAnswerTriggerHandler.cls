public with sharing class SC_WebAnswerTriggerHandler {
    private final String RESOURCES_NAME = 'ExhibitRule';    // 静的リソース名
    private List<ExhibitJson> jsonList;

    public SC_WebAnswerTriggerHandler () {

    }

    /*
     * 変更履歴取得
     *
     * @param oldMap    更新前Map
     * @param newMap    更新後Map
     */
    public void historyAcquisition(Map<Id, SC_WebAnswer__c> oldMap, Map<Id, SC_WebAnswer__c> newMap) {
        SC_WebAnswer__c webAnswer = null;

        Map<Id, SC_WebAnswer__c> targets = new Map<Id, SC_WebAnswer__c>();
        for (SC_WebAnswer__c record : newMap.values()) {
            // 差戻日があるレコードを対象とする
            if (record.CMDRemandDate__c != null && oldMap.get(record.id).CMDRemandDate__c != null) {
                webAnswer = record;
                targets.put(record.id, record);
            }
        }

        if (webAnswer != null) {
            I_CompareBeforeAfter cba = new I_CompareBeforeAfter(String.valueOf(webAnswer.getSObjectType()));
            cba.compare(oldMap, targets);
        }
    }

    /*
     * 別紙レコード作成
     *
     * @param newMap    更新後Map
     */
    public void createExhibit(Map<Id, SC_WebAnswer__c> newMap) {
        // 履歴取得対象オブジェクトの情報取得
        Map<String, Schema.SObjectType> smap = Schema.getGlobalDescribe();
        Schema.SObjectType sobj = smap.get(String.valueOf(newMap.values()[0].getSObjectType()));
        Schema.DescribeSObjectResult sDescribe = sobj.getDescribe();
        Map<String, Schema.SObjectField> fmap = sDescribe.fields.getMap();

        StaticResource sr = [SELECT Id,Body FROM StaticResource WHERE Name = :RESOURCES_NAME LIMIT 1];
        jsonList = (List<ExhibitJson>) JSON.deserialize(sr.Body.toString(), List<ExhibitJson>.class);
        system.debug('debug:body=' + sr.Body.toString());

        List<SC_WebAnswerExhibit__c> insRecords = new List<SC_WebAnswerExhibit__c>();

        // 既存の別紙レコードを削除する
        List<SC_WebAnswerExhibit__c> delRecords = [select id from SC_WebAnswerExhibit__c where SC_WebAnswer__c in :newMap.keySet()];
        delete delRecords;

        // レコード作成
        for (SC_WebAnswer__c record : newMap.values()) {
            List<String> fieldNames = getExhibitRule(record.year__c, record.kind__c);
            for (String f : fieldNames) {
                String fieldName = f + '__c';

                if (record.get(fieldName) != null) {
                    SC_WebAnswerExhibit__c rec = new SC_WebAnswerExhibit__c();

                    // 対象項目のラベル名
                    Schema.DescribeFieldResult fr = fmap.get(fieldName).getDescribe();
                    String filedLabel = fr.getLabel();

                    rec.SC_WebAnswer__c = record.id;
                    rec.FieldName__c = fieldName;
                    rec.FieldLabel__c = filedLabel;
                    rec.Content__c = String.valueOf(record.get(fieldName));
                    insRecords.add(rec);
                }
            }
        }
        insert insRecords;
    }


    /*
     * 静的リソース取得
     *
     * @param sObjectName   対象オブジェクトのAPI参照名
     */
    private List<String> getExhibitRule(String year, String kind) {
        system.debug('debug:jsonList=' + jsonList);
        List<String> filedNames = new List<String>();

        for (ExhibitJson json : jsonList) {
            if (json.year == year && json.kind == kind) {
                filedNames = json.fields;
                break;
            }
        }
        return filedNames;
    }

    // jsonに定義されている構成
    public class ExhibitJson {
        public String year;             // 年度
        public String kind;             // 種別
        public List<String> fields;     // 項目
    }
}