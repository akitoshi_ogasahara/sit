/**
 *
 */
@isTest
private class TestMD_DetailExtension {

	/**
	 * void init()
	 * 正常
	 */
	static testMethod void initTest01() {
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		Test.setCurrentPage(pr);

		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailExtension ext = new MD_DetailExtension(stdController);
		ext.init();

		Test.stopTest();

		// [IRIS] ログをコントローラーで取得しないように変更したため削除
		// assert
		//List<E_Log__c> logList = [Select Id, Detail__c From E_Log__c];
		//System.assertEquals(1, logList.size());
		//System.assertEquals('[嘱託医番号: ' + doctor.DoctorNumber__c + ']', logList[0].Detail__c);
	}
	
	/**
	 * void doSendMail()
	 */
	static testMethod void doSendMailTest01(){
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		Test.setCurrentPage(pr);

		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailExtension ext = new MD_DetailExtension(stdController);
		ext.init();
		ext.doSendMail();

		Test.stopTest();

		// assert
		List<E_Log__c> logList = [Select Id, RecordId__c, Detail__c, AccessPage__c From E_Log__c Where AccessPage__c = 'MD_Detail SendMail'];
		System.assertEquals(1, logList.size());
		System.assertEquals(doctor.Id, logList[0].RecordId__c);
		System.assertEquals('[嘱託医番号: ' + doctor.DoctorNumber__c + ']', logList[0].Detail__c);
	}
	
	/**
	 * void wgsLatlon()

	static testMethod void wgsLatlonTest01(){
		String resultParam1 = 'param1';
		String resultParam2 = 'param2';
		
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		pr.getParameters().put('mailLat', resultParam1);
		pr.getParameters().put('mailLon', resultParam2);
		Test.setCurrentPage(pr);
		
		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailExtension ext = new MD_DetailExtension(stdController);
		ext.wgsLatlon();

		Test.stopTest();

		// assert
		System.assertEquals(resultParam1, ext.mail_lat);
		System.assertEquals(resultParam2, ext.mail_lon);
	}
	 */	
	/**
	 * void getMailtoLink()
	 */
	static testMethod void getMailtoLinkTest01(){
		String resultParam1 = 'param1';
		String resultParam2 = 'param2';
		
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		pr.getParameters().put('mailLat', resultParam1);
		pr.getParameters().put('mailLon', resultParam2);
		Test.setCurrentPage(pr);
		
		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailExtension ext = new MD_DetailExtension(stdController);
//		ext.wgsLatlon();
		String result = ext.getMailtoLink();

		Test.stopTest();

		// assert
		System.assert(String.isNotBlank(result));
	}
	
	/**
	 * void getMailtoLink()
	 */
	static testMethod void getMailtoLinkTest02(){
		// 詳細画面
		PageReference pr = Page.E_MDDetail;
		Test.setCurrentPage(pr);
		
		// TestData
		MD_Doctor__c doctor = createDoctor('嘱託医　テスト1', '病院名　ビョウイン1');
		insert doctor;

		Test.startTest();

		ApexPages.StandardController stdController = new ApexPages.StandardController(doctor);
		MD_DetailExtension ext = new MD_DetailExtension(stdController);
		String result = ext.getMailtoLink();

		Test.stopTest();

		// assert
		System.assert(String.isNotBlank(result));
	}

	/* Test Data　位置情報固定 */
	private static MD_Doctor__c createDoctor(String name, String hosName) {
		MD_Doctor__c doc = new MD_Doctor__c();
		doc.Name = name;
		doc.HospitalName__c = hosName;
		doc.ParticularDoctor__c = 'Y';
		doc.Blood__c = '有';
		doc.Electrocardiogram__c = '有';
		doc.State__c = '東京都';
		doc.Address__c = '中央区日本橋1-1-1';
		doc.Phone__c = '03-1234-5678';
		doc.ConsultationTime__c = '10:00 ~ 18:00';
		doc.Note__c = 'test memo';
		doc.DoctorNumber__c = '99999';
		return doc;
	}
}