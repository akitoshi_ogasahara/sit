/**
 * IDリクエストデータアクセスオブジェクト(Without版)テストクラス
 * CreatedDate 2016/10/27
 * @Author Terrasky
 */
@isTest
private class TestE_IDRequestDaoWithout {


	//IDリクエストデータリストをIDリクエストレコードIDから取得するメソッドのテスト
	@isTest static void getRecByIdTest() {
		//データ作成
		E_IDCPF__c idcp = createIDManage();
		E_IDRequest__c idr = createIDRequest(idcp);

		Test.startTest();
		//IDで検索
		idr = E_IDRequestDaoWithout.getRecById(idr.Id);
		System.assertEquals(idr.ZEMAILAD__c,'test@test.com');
		Test.stopTest();
	}

	//getRecsByIDCPFIdのテスト
	@isTest static void getRecsByIDCPFIdTest() {
		//データ作成
		E_IDCPF__c idcp = createIDManage();
		E_IDRequest__c idr = createIDRequest(idcp);

		Test.startTest();
		//IDで検索
		List<E_IDRequest__c> idrr = E_IDRequestDaoWithout.getRecsByIDCPFId(idcp.Id);
		System.assertEquals(idrr[0].Id,idr.Id);
		Test.stopTest();
	}

	static Datetime nowTime = Datetime.newInstance(2000, 2, 22, 2, 22, 22);
	//updateEidrのテスト
	@isTest static void updateEidrTest() {
		//データ作成
		E_IDCPF__c idcp = createIDManage();
		E_IDRequest__c idr = createIDRequest(idcp);

		Test.startTest();
		//IDで検索
		E_IDRequestDaoWithout.updateEidr(idr.Id,'テスト');
		Test.stopTest();
		E_IDRequest__c idrr = [ select dataSyncDate__c from E_IDRequest__c where Id = :idr.Id]; 
		//updateされていれば値が異なるためassertNotEquals
		System.assertNotEquals(idrr.dataSyncDate__c,nowTime);
	}

	//IDリクエストデータを作成(リクエスト種別:E)するメソッドのテスト
	@isTest static void insEIdrRecWithMailTest() {
		Test.startTest();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;
			//メソッド呼び出し
			E_IDRequest__c idr = E_IDRequestDaoWithout.insEIdrRecWithMail(idcp,u.Email);
			System.assertEquals(idr.ZUPDFLAG__c,'E');
		}
		Test.stopTest();
	}

	//insertRecのテスト
	@isTest static void insertRecTest() {
		//データ作成
		E_IDCPF__c idcp = createIDManage();
		E_IDRequest__c idr = new E_IDRequest__c(
			ZEMAILAD__c = 'testtest@test.com',
			E_IDCPF__c = idcp.Id,
			ZIDOWNER__c = 'AGtest01',
			ZIDTYPE__c = 'AT',
			ZINQUIRR__c = 'L3test01',
			ZWEBID__c = '50test01',
			ZUPDFLAG__c = 'I',
			dataSyncDate__c = Datetime.newInstance(2000, 2, 22, 2, 22, 22)
		);

		Test.startTest();
		//IDで検索
		E_IDRequestDaoWithout.insertRec(idr);
		E_IDRequest__c idrr = [select ZEMAILAD__c from E_IDRequest__c where Id = :idr.Id];
		System.assertEquals(idr.ZEMAILAD__c,'testtest@test.com');
		Test.stopTest();
	}

	//getRecsByAgentUserIdのテスト
	@isTest static void getRecsByAgentUserIdTest() {
		Test.startTest();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;
			E_IDRequest__c idr = createIDRequest(idcp);
			Set<Id> setid = new Set<Id>();
			setid.add(u.Id);
			//メソッド呼び出し
			List<E_IDRequest__c> idrr = E_IDRequestDaoWithout.getRecsByAgentUserId(setid);
			System.assertEquals(idrr[0].Id,idr.Id);
		}
		Test.stopTest();
	}

	//データ作成
	//ID管理作成
	static E_IDCPF__c createIDManage(){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		insert ic;
		return ic;
	}

	//IDリクエスト作成
	static E_IDRequest__c createIDRequest(E_IDCPF__c idmn){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		E_IDRequest__c idr = new E_IDRequest__c();
		system.runAs(thisUser){
			idr.ZEMAILAD__c = 'test@test.com';
			idr.E_IDCPF__c = idmn.Id;
			idr.ZIDOWNER__c = 'AG' + 'test01';
			idr.ZIDTYPE__c = 'AT';
			idr.ZINQUIRR__c = 'L3' + 'test01';
			idr.ZWEBID__c = '50' + 'test01';
			idr.ZUPDFLAG__c = 'I';
			idr.dataSyncDate__c = nowTime;
			insert idr;
		}
		return idr;
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	//ユーザ作成
	static User createUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = con.Id;
			insert us;
		}
		return us;
	}
}