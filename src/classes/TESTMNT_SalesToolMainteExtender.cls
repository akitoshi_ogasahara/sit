@isTest
private class TESTMNT_SalesToolMainteExtender {

	@isTest static void testAfterSearch() {
		MNT_SalesToolMainteSVEController page = new MNT_SalesToolMainteSVEController(new ApexPages.StandardController(new I_ContentMaster__c()));
		MNT_SalesToolMainteExtender extender = new MNT_SalesToolMainteExtender(page);
		Test.startTest();
			extender.afterSearch();
		Test.stopTest();
	}

	@isTest static void testDoExportCSV() {
		MNT_SalesToolMainteSVEController page = new MNT_SalesToolMainteSVEController(new ApexPages.StandardController(new I_ContentMaster__c()));
		MNT_SalesToolMainteExtender extender = new MNT_SalesToolMainteExtender(page);

		Test.startTest();
			PageReference result = extender.doExportCSV();

			PageReference pr = new PageReference('/');
			System.assertNotEquals(result, pr);
		Test.stopTest();
	}

}