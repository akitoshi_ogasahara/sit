/**
 * Contactデータアクセスオブジェクト
 * CreatedDate 2016/08/02
 * @Author Terrasky
 */
public with sharing class E_MK_CampaignTargetDao {

	/*
	 * @param CLTPF_CLNTNUM 顧客管理番号
	 * @return レコード
	 */
	public static MK_CampaignTarget__c getActiveRecByCLTPF_CLNTNUM (String CLTPF_CLNTNUM) {

		if(String.isBlank(CLTPF_CLNTNUM)) return null;

		List<MK_CampaignTarget__c> recs = [Select id
									, name
									, CLTPF_CLNTNUM__c
									,ContractantKana__c
									,ContractantMailAddress__c
									,Campaign__c
									,Campaign__r.id
									,Campaign__r.Name
									,Campaign__r.Incentive__c
									,Campaign__r.ContractantIncentive__c
									,Campaign__r.Information__c
									,Campaign__r.Overview__c
									,Campaign__r.Introduction__c
									,Campaign__r.E_MessageMaster_UpsertKey__c
									,Campaign__r.IncentiveImportant__c
								From MK_CampaignTarget__c
								where CLTPF_CLNTNUM__c = :CLTPF_CLNTNUM
								AND Campaign__r.FromDate__c <= toDay
								AND Campaign__r.ToDate__c >= toDay
								Order by Campaign__r.FromDate__c
								limit 1];
		return (recs.isEmpty())? null: recs[0];
	}
	/*
	 * @param id レコードID
	 * @return レコード
	 */
	public static MK_CampaignTarget__c getActiveRecById(ID id){
		if(id == null) return null;
		List<MK_CampaignTarget__c> recs = [Select id
								,Name
								,Agent__c
								,AgentNameKanji__c
								,AgencyNameKanji__c
								,AgentMailAddress__c
								,ContractantMailAddress__c
								,Campaign__r.id
								,Campaign__r.Name
								,Campaign__r.title__c
								,Campaign__r.Incentive__c
								,Campaign__r.Information__c
								,Campaign__r.Overview__c
								,Campaign__r.Introduction__c
								,Campaign__r.E_MessageMaster_UpsertKey__c
								,Campaign__r.IncentiveImportant__c
								,MR__r.Email
							From MK_CampaignTarget__c
							Where id =: id
							AND Campaign__r.FromDate__c <= toDay
							AND Campaign__r.ToDate__c >= toDay
							Order by Campaign__r.FromDate__c
							limit 1];
		return (recs.isEmpty())? null: recs[0];
	}
}