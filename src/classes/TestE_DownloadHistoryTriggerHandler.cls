@isTest
private class TestE_DownloadHistoryTriggerHandler {

	//after insert ログが作られない場合
	@isTest static void afterInsertCreateNoLogTest() {

		Test.startTest();
		E_DownloadHistorry__c dh = createDownloadHistory('9');
		Test.stopTest();

		List<E_Log__c> logList = [select Name from E_Log__c where DownloadHistory__c = :dh.Id];
		System.assertEquals(logList.size(),0);
	}

	//after insert ログが作られる場合
	@isTest static void afterInsertCreateLogTest() {

		Test.startTest();
		E_DownloadHistorry__c dh = createDownloadHistory('1');
		Test.stopTest();

		List<E_Log__c> logList = [select Name from E_Log__c where DownloadHistory__c = :dh.Id];
		System.assertEquals(logList.size(),1);
	}
	

	//テストデータ作成
	//ダウンロード履歴
	static E_DownloadHistorry__c createDownloadHistory(String formNo){
		E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
		dh.FormNo__c = formNo;
		insert dh;

		return dh;
	}

}