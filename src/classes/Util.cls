public class Util {

	/*------------------------------------------------------------------------
	* 処理ブロック名      ：対象項目がNullの場合空白で返却
	* 戻り値              ：String(空白又はパラメータ値)
	* 機能概要            ：対象項目がNullの場合空白で返却
	*------------------------------------------------------------------------
	*/
	public static String cnvNullStr(String s) {
		return (s == null) ? '' : s;
	}

	/*------------------------------------------------------------------------
	* 処理ブロック名      ： Date型への変換処理
	* 戻り値              ： Date(yyyy-mm-dd)
	* 機能概要            ： String型をDate型へ変換する。
	*------------------------------------------------------------------------
	*/
	public static Date cnvStrDate(String s) {
		return Date.newInstance(Integer.valueOf(s.substring(0, 4)),
								Integer.valueOf(s.substring(5, 7)),
								Integer.valueOf(s.substring(8, 10)));
	}

	/*------------------------------------------------------------------------
	* 処理ブロック名      ： 月の最初日を取得
	* 戻り値              ： String型(yyyy-mm-dd)
	* 機能概要            ： 指定月の最初日を返却。
	*------------------------------------------------------------------------
	*/
	public static String getFirstDate(String s) {
		Integer y = Integer.valueOf(s.substring(0, 4));
		Integer m = Integer.valueOf(s.substring(5, 7));
		return String.valueOf(Date.newInstance(y,m,1));
	}

	/*------------------------------------------------------------------------
	* 処理ブロック名      ： 月の最後日を取得
	* 戻り値              ： String型(yyyy-mm-dd)
	* 機能概要            ： 指定月の最後日を返却。
	*------------------------------------------------------------------------
	*/
	public static String getLastDate(String s) {
		Integer y = Integer.valueOf(s.substring(0, 4));
		Integer m = Integer.valueOf(s.substring(5, 7));
		return String.valueOf(Date.newInstance(y,m+1,1).addDays(-1));
	}

	/*------------------------------------------------------------------------
	* 処理ブロック名      ： PickListを取得
	* 戻り値              ： List<Schema.PicklistEntry>
	* 機能概要            ： 指定Objectの指定FieldのPickListを返却。
	*------------------------------------------------------------------------
	*/
	public static List<Schema.PicklistEntry> getPicklist(Sobject obj, String fieldName) {
		Schema.DescribeSObjectResult objDesc = obj.getSObjectType().getDescribe();
		Map<String, Schema.SObjectField> fieldsMap = objDesc.fields.getMap();
		List<Schema.PicklistEntry> rt = fieldsMap.get(fieldName).getDescribe().getPickListValues();
		return rt;
	}

}