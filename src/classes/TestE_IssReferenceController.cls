@isTest
private class TestE_IssReferenceController {
	
	//urlパラメータ不正
	@isTest static void URLParaError() {
		String encoded = EncodingUtil.urlEncode('|', 'UTF-8');

		Test.startTest();
		E_IssReferenceController isr = new E_IssReferenceController();
		PageReference pageref = isr.init();
		System.assertEquals(pageref.getUrl(),Page.E_ErrorPage.getUrl() + '?code=ISS' + encoded + '001' + '&iss=1');
		Test.stopTest();

	}

	//証券番号から保険契約ヘッダレコードが取得できないとき
	@isTest static void noPolicyRecordTest() {
		PageReference setPage = Page.E_IssReference;
		setPage.getParameters().put('RENKEI_SHOUBAN', '20161220');
		Test.setCurrentPage(setPage);
		String encoded = EncodingUtil.urlEncode('|', 'UTF-8');

		Test.startTest();
		E_IssReferenceController isr = new E_IssReferenceController();
		PageReference pageref = isr.init();
		System.assertEquals(pageref.getUrl(),Page.E_ErrorPage.getUrl() + '?code=ISS' + encoded + '003' + '&iss=1');
		Test.stopTest();
	}

	//取得した保険契約ヘッダのレコードタイプがSPVAで、年金支払開始日到来フラグがtrue、抽出対象判定フラグ(契約者)と抽出対象判定フラグ(被保険者)がfalse
	@isTest static void policyRecordTypeSPVATest() {
		String code = '20161220';
		String msg = '該当する情報はございません。';
		ApexPages.Message apexmsg = new ApexPages.Message(ApexPages.Severity.ERROR,msg);
		PageReference setPage = Page.E_IssReference;
		setPage.getParameters().put('RENKEI_SHOUBAN', code);
		Test.setCurrentPage(setPage);
		createPolicy(code,E_Const.POLICY_RECORDTYPE_SPVA,0);
		createMessageData('ISS|005',msg);

		Test.startTest();
		E_IssReferenceController isr = new E_IssReferenceController();
		PageReference pageref = isr.init();
		System.assertEquals(ApexPages.getMessages()[0],apexmsg);
		Test.stopTest();
	}

	//e_coliへ遷移
	@isTest static void pageRefEColiTest() {
		String code = '20161220';
		PageReference setPage = Page.E_IssReference;
		setPage.getParameters().put('RENKEI_SHOUBAN', code);
		Test.setCurrentPage(setPage);
		E_Policy__c pol = createPolicy(code,E_Const.POLICY_RECORDTYPE_COLI,0);

		Test.startTest();
		E_IssReferenceController isr = new E_IssReferenceController();
		PageReference pageref = isr.init();
		System.assertEquals(pageref.getUrl(),Page.E_Coli.getUrl() + '?id=' + pol.Id + '&iss=1');
		Test.stopTest();
	}


	//e_spvaへ遷移
	@isTest static void pageRefESpvaTest() {
		String code = '20161220';
		PageReference setPage = Page.E_IssReference;
		setPage.getParameters().put('RENKEI_SHOUBAN', code);
		Test.setCurrentPage(setPage);
		E_Policy__c pol = createPolicy(code,E_Const.POLICY_RECORDTYPE_SPVA,1);

		Test.startTest();
		E_IssReferenceController isr = new E_IssReferenceController();
		PageReference pageref = isr.init();
		System.assertEquals(pageref.getUrl(),Page.E_Spva.getUrl() + '?id=' + pol.Id + '&iss=1');
		Test.stopTest();
	}

	//E_Annuityへ遷移
	@isTest static void pageRefEAnnuityTest() {
		String code = '20161220';
		PageReference setPage = Page.E_IssReference;
		setPage.getParameters().put('RENKEI_SHOUBAN', code);
		Test.setCurrentPage(setPage);
		E_Policy__c pol = createPolicy(code,E_Const.POLICY_RECORDTYPE_SPVA,2);

		Test.startTest();
		E_IssReferenceController isr = new E_IssReferenceController();
		PageReference pageref = isr.init();
		System.assertEquals(pageref.getUrl(),Page.E_Annuity.getUrl() + '?id=' + pol.Id + '&iss=1');
		Test.stopTest();
	}

	//テストデータ作成
	//メッセージマスタ
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//保険契約ヘッダ
	static E_Policy__c createPolicy(String code,String recType,Integer num){
		Account acc = TestE_TestUtil.createAccount(true);
		Contact con = TestE_TestUtil.createContact(true, acc.Id, '契約者', 'Test000', 'Test000');
		E_Policy__c pol = new E_Policy__c();
		pol.COMM_CHDRNUM__c = code;
		if(recType == E_Const.POLICY_RECORDTYPE_SPVA && num == 0){
			pol.SPVA_ZANNSTFLG__c = true;
			pol.SPVA_ZEXTFLG01__c = false;
			pol.SPVA_ZEXTFLG02__c = false;
		}else if(recType == E_Const.POLICY_RECORDTYPE_SPVA && num == 1){
			pol.COMM_CRTABLE__c = 'SR00';
		}else if(recType == E_Const.POLICY_RECORDTYPE_SPVA && num == 2){
			pol.SPVA_ZANNSTFLG__c = true;
			pol.SPVA_ZVWDCF__c = '0';
			pol.SPVA_ZEXTFLG01__c = true;
		}
		RecordType rec = [select SobjectType, Id, DeveloperName from RecordType 
						  where SobjectType = 'E_Policy__c' AND DeveloperName =: recType];
		pol.RecordTypeId = rec.Id;
		insert pol;
		return pol;
	}
	
}