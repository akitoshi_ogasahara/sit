/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EventUpdateTriggerTest {

   static testMethod void myUnitTest() {
        // TO DO: implement unit test
        System.debug('-------------------------------------------------------OpportunityUpdateTriggerTest：START');
        try{

            User userData = [SELECT Id, Unit__c, UnitName__c FROM User where isActive=true limit 1];
            userData.UnitName__c = 'テスト拠点名';
            userData.Unit__c = 'テスト営業部';
            update userData;

            System.runAs(userData){

                Event eventData = new Event();
                eventData.Area__c = 'テスト';
                eventData.SalesUnit__c = 'テスト';
                eventData.DurationInMinutes = 1;
                eventData.ActivityDateTime = System.now();
                insert eventData;

                Event InputDataList = [SELECT Id, Area__c, SalesUnit__c FROM Event WHERE ID =: eventData.Id];
                // 所有者の拠点名と一致すること
                System.assertEquals(userData.UnitName__c, InputDataList.Area__c);
                // 所有者の営業部と一致すること
                System.assertEquals(userData.Unit__c, InputDataList.SalesUnit__c);

            }

        }catch(QueryException qe){
            System.debug('-------------------------------------------------------QueryException例外内容'+qe);
        }catch(Exception e){
            System.debug('-------------------------------------------------------Exception例外内容'+e);
        }
        System.debug('-------------------------------------------------------OpportunityUpdateTriggerTest：END');
    } // END myUnitTest()
}