/**
 * ID管理データアクセスオブジェクト(Without版)のテストクラス
 * CreatedDate 2016/10/27
 * @Author Terrasky
 */
@isTest
private class TestE_IDCPFDaoWithout {
	//ID管理データをContactIdから取得するメソッドのテスト
	@isTest static void getRecsByContactIdTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;
			//テスト
			Test.startTest();
			idcp = E_IDCPFDaoWithout.getRecsByContactId(u.ContactId);
			System.assertEquals(idcp.User__c,u.Id);
			Test.stopTest();
		}
	}
	//ID管理データリストを代理店格コードから取得するメソッドのテスト
	@isTest static void getActiveUsersRecsByZHEADAYTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;
			//テスト
			Test.startTest();
			List<E_IDCPF__c> idcps = E_IDCPFDaoWithout.getActiveUsersRecsByZHEADAY('12345');
			System.assertEquals(idcps[0].User__c,u.Id);
			Test.stopTest();
		}
	}
	//ID管理データリストをID管理のSFIDから取得するメソッドのテスト
	@isTest static void getRecsByZWEBIDTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;
			//テスト
			Test.startTest();
			idcp = E_IDCPFDaoWithout.getRecsByZWEBID(idcp.Id);
			System.assertEquals(idcp.User__c,u.Id);
			Test.stopTest();
		}
	}

	//insertRecのテスト
	@isTest static void insertRecTest() {
		User thisUser = runAsUser();
		E_IDCPF__c idcp = new E_IDCPF__c();
		idcp.ZWEBID__c = '20161220';
		idcp.User__c = thisUser.Id;
		System.runAs(thisUser){
			Test.startTest();
			E_IDCPFDaoWithout.insertRec(idcp);
			E_IDCPF__c idcpRec = [select User__r.Name from E_IDCPF__c where Id = :idcp.Id];
			System.assertEquals(idcpRec.User__r.Name,thisUser.Name);
			Test.stopTest();
		}
	}

	//updateRecのテスト
	@isTest static void updateRecTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			String email = 'test@test.com';

			//データ作成
			E_IDCPF__c idcp = createIDManage();
			idcp.ZEMAILAD__c = email;

			//テスト
			Test.startTest();
			E_IDCPFDaoWithout.updateRec(idcp);
			Test.stopTest();
			E_IDCPF__c idcpRec = [select ZEMAILAD__c from E_IDCPF__c where Id = :idcp.Id];
			System.assertEquals(email, idcpRec.ZEMAILAD__c);
		}
	}

	//updateEidcのテスト
	@isTest static void updateEidcTest() {
		User thisUser = runAsUser();
		String email = 'test@test.com';
		System.runAs(thisUser){
			//データ作成
			E_IDCPF__c idcp = createIDManage();

			//テスト
			Test.startTest();
			E_IDCPFDaoWithout.updateEidc(idcp.Id,email);
			Test.stopTest();
			E_IDCPF__c idcpRec = [select ZEMAILAD__c from E_IDCPF__c where Id = :idcp.Id];
			System.assertEquals(idcpRec.ZEMAILAD__c,email);
		}
	}

	//getRecsByUserIDのテスト
	@isTest static void getRecsByUserIDTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;

			//テスト
			Test.startTest();
			E_IDCPF__c idcpRec = E_IDCPFDaoWithout.getRecsByUserID(u.Id);
			System.assertEquals(idcpRec.User__c,u.Id);
			Test.stopTest();
		}
	}

	//getIdRecsByContactIdのテスト
	@isTest static void getIdRecsByContactIdTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			//データ作成
			User u = createUser();
			Contact c = createContactData();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			idcp.Contact__c = c.Id;
			update idcp;

			//テスト
			Test.startTest();
			List<E_IDCPF__c> idcpRecs = E_IDCPFDaoWithout.getIdRecsByContactId(c.Id);
			System.assertEquals(1, idcpRecs.size());
			System.assertEquals(c.Id, idcpRecs[0].Contact__c);
			Test.stopTest();
		}
	}

	//getEidcCodeByMrIdのテスト
	@isTest static void getEidcCodeByMrIdTest() {
		User thisUser = runAsUser();
		System.runAs(thisUser){
			String zinquirr = '12345';

			//データ作成
			User u = createUser();
			E_IDCPF__c idcp = createIDManage();
			idcp.User__c = u.Id;
			idcp.ZINQUIRR__c = zinquirr;
			update idcp;

			//テスト
			Test.startTest();
			E_IDCPF__c resultIdcp = E_IDCPFDaoWithout.getEidcCodeByMrId(u.Id);
			System.assertEquals(zinquirr, resultIdcp.ZINQUIRR__c);
			Test.stopTest();
		}
	}

	//cutOutRelationWithUserのテスト
	@isTest static void cutOutRelationWithUserTest() {
		User thisUser = runAsUser();

		User u = new User();
		E_IDCPF__c idcp = new E_IDCPF__c();

		System.runAs(thisUser){
			//データ作成
			u = createUser();
			idcp = createIDManage();
			idcp.User__c = u.Id;
			update idcp;

			//テスト
			Test.startTest();
			E_IDCPFDaoWithout.cutOutRelationWithUser(u.Id);
			Test.stopTest();
		}

		E_IDCPF__c resultIdcp = [Select Id, User__c, OldUser__c From E_IDCPF__c Where Id = :idcp.Id];
		System.assertEquals(u.Id, resultIdcp.OldUser__c);
		System.assertEquals(null, resultIdcp.User__c);
	}

	//データ作成
	static User runAsUser(){
		User thisUser = [ select Id,Name from User where Id = :UserInfo.getUserId() ];
		return thisUser;
	}

	//ID管理作成
	static E_IDCPF__c createIDManage(){
		E_IDCPF__c ic = new E_IDCPF__c();
		ic.ZWEBID__c = '1234567890';
		ic.ZIDOWNER__c = 'AG' + 'test01';
		ic.ZEMAILAD__c = 'manage@manage.com';
		insert ic;
		return ic;
	}

	//取引先作成
	static Id createAccount(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		acc.E_CL1PF_ZHEADAY__c = '12345';
		insert acc;
		return acc.Id;
	}

	//募集人作成
	static Contact createContactData(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	//ユーザ作成
	static User createUser(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.ContactId = con.Id;
			insert us;
		}
		return us;
	}
}