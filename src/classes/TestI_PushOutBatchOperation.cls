/**
 * 
 */
@isTest
private class TestI_PushOutBatchOperation {

	//ProfileName
	private final String PF_SYSTEM = 'システム管理者';
	private static String PF_EMPLOYEE = 'E_EmployeeStandard';
	private static String PF_PARTNER = 'E_PartnerCommunity';
	
	//User情報
	private static User user;
	private static User thisUser = [SELECT id FROM user WHERE id = :system.userInfo.getUserId()];	//Id取得
	private static User communityUser;
	private static Account ahAcc;
	private static Account ayAcc;
	private static Contact atCon;
	private static E_BizDataSyncLog__c log;

	/**
	 * Database.QueryLocator getQueryLocator()
	 */
    static testMethod void getQueryLocator_test01() {
    	Database.QueryLocator result;
    	
        // Data
        createDataEBizLog(I_Const.EBIZDATASYNC_KBN_PUSHOUT, I_Const.EBIZDATASYNC_KBN_BILA);
        log = [Select Id, CreatedDate, Kind__c, NotificationKind__c From E_BizDataSyncLog__c Where Id = :log.Id];
        
        Test.startTest();
        I_PushOutBatchOperation operation = new I_PushOutBatchOperation(log);
        result = operation.getQueryLocator();
        Test.stopTest();
        
        // 通知タイプのクエリ作成
    	String soql = '';
    	String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(I_Const.EBIZDATASYNC_KBN_BILA);
    	Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(I_Const.EBIZDATASYNC_KBN_BILA);
    	if(columns.isEmpty() == false){
	    	soql = ' Select Id';
	    	for(String col :columns){
	    		soql += ', ' + col;
	    	}
	    	soql += ' From ' + objApi;
	    	
	    	// Where句
	    	soql += ' Where ';
	    	soql += ' CreatedDate > ' + log.CreatedDate.addDays(-1).format('yyyy-MM-dd\'T\'hh:mm:ss.SSS\'Z\'');
    	}
        
        System.assertEquals(String.valueOf(Database.getQueryLocator(soql)), String.valueOf(result));
    }
    
	/**
	 * void execute(List<SObject> records)
	 */
    static testMethod void execute_test01() {
	   	// Data
        createDataEBizLog(I_Const.EBIZDATASYNC_KBN_PUSHOUT, I_Const.EBIZDATASYNC_KBN_BILA);
        createUser(PF_PARTNER);
        createDataAccessObj(user.Id, 'AT');
        createData(2, user.Id);
        
        log = [Select Id, CreatedDate, Kind__c, NotificationKind__c From E_BizDataSyncLog__c Where Id = :log.Id];
        
        // 通知タイプのクエリ作成
    	String soql = '';
    	String objApi = I_Const.PUSHOUT_OBJ_API_MAP.get(I_Const.EBIZDATASYNC_KBN_BILA);
    	Set<String> columns = I_Const.QUERY_SELECTS_MAP.get(I_Const.EBIZDATASYNC_KBN_BILA);
    	if(columns.isEmpty() == false){
	    	soql = ' Select Id';
	    	for(String col :columns){
	    		soql += ', ' + col;
	    	}
	    	soql += ' From ' + objApi;
	    	
	    	// Where句
	    	soql += ' Where ';
	    	soql += ' CreatedDate > ' + log.CreatedDate.addDays(-1).format('yyyy-MM-dd\'T\'hh:mm:ss.SSS\'Z\'');
    	}
        List<SObject> sobjs = Database.query(soql);

        Test.startTest();
        I_PushOutBatchOperation operation = new I_PushOutBatchOperation(log);
        operation.getQueryLocator();
        operation.execute(sobjs);
        Test.stopTest();

    }
    
    /*  */
    private static void createUser(String profileName){
		String userName = 'test@terrasky.ingtesting';
        Profile p = [Select Id From Profile Where Name = :profileName];
    	
		// Base Info
        user = new User(
            Lastname = 'test'
            , Username = userName
            , Email = userName
            , ProfileId = p.Id
            , Alias = 'test'
            , TimeZoneSidKey = UserInfo.getTimeZone().getID()
            , LocaleSidKey = UserInfo.getLocale()
            , EmailEncodingKey = 'UTF-8'
            , LanguageLocaleKey = UserInfo.getLanguage()
        );
    	
    	// User
    	if(profileName != PF_PARTNER){
    		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
    		user.UserRoleId = portalRole.Id;
    		insert user;
    		
    	// Partner User
    	}else{
			system.runAs(thisuser){
				// Account 代理店格
				ahAcc = new Account(Name = 'ahAccount',E_CL1PF_ZHEADAY__c = 'ah001');
				insert ahAcc;
		
				// Account 事務所
				ayAcc = new Account(
						Name = 'office1'
						,ParentId = ahAcc.Id
						,E_CL2PF_ZAGCYNUM__c = 'ay001'
						,E_COMM_VALIDFLAG__c = '1'
				);
				insert ayAcc;
		
				// Contact 募集人
				atCon = new Contact(LastName = 'test',AccountId = ayAcc.Id, E_CL3PF_ZHEADAY__c = ahAcc.E_CL1PF_ZHEADAY__c, E_CL3PF_ZAGCYNUM__c = ayAcc.E_CL2PF_ZAGCYNUM__c);
				atCon.E_CL3PF_AGNTNUM__c = 'at001';
				atCon.email = 'fstest@terrasky.ingtesting';
				insert atCon;
			}
			
    		user.ContactId = atCon.Id;
    		insert user;
				
			ContactShare cs = new ContactShare(
							ContactId = atCon.Id,
							ContactAccessLevel = 'read',
							UserOrGroupId = user.Id);
			insert cs;
    	}
    }
    
    private static void createDataAccessObj(Id userId, String idType){
        system.runAs(thisuser){
            // 権限割り当て
            TestE_TestUtil.createPermissionSetAssignmentBase(true, userId);
            
            // ID管理
            E_IDCPF__c idcpf = new E_IDCPF__c(
                User__c = userId
                ,ZIDTYPE__c = idType
                ,FLAG01__c = '1'
                ,FLAG06__c = '1'
                ,ZSTATUS01__c = '1'
                ,OwnerId = userId
            );
            insert idcpf;
        }
    }
    
	private static void createData(Integer dataCnt, String shareUserId){
		system.runAs(thisuser){
			//保険契約ヘッダ（個人タイプ）
			E_Policy__c policy = new E_Policy__c();
			policy = TestE_TestUtil.createPolicy(true, atCon.Id, E_Const.POLICY_RECORDTYPE_COLI, '12345678');
			policy = [Select Id, MainAgent__c From E_Policy__c Where Id = :policy.Id];
			policy.MainAgent__c = atCon.Id;
			update policy;
	
			// Share
			if(String.isNotBlank(shareUserId)){
				E_Policy__Share ps = new E_Policy__Share(
								ParentId = policy.Id,
								AccessLevel = 'read',
								UserOrGroupId = shareUserId);
				insert ps;
			}
	
			//販売取扱者
			E_CHTPF__c chtpf = new E_CHTPF__c(E_Policy__c = policy.Id, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c);
			insert chtpf;
	
			//保険料請求予告通知
			List<E_BILA__c> bilas = new List<E_BILA__c>();
			//対象月
			date targetDate = E_DownloadNoticeUtil.getPeriodStartDate(System.Label.E_DLN_PERIOD_DAY_BILA);
			for(Integer i = 0; i < dataCnt; i++){
				Date d = targetDate.addMonths(-2);
				E_BILA__c bila = new E_BILA__c(
					ParentAccount__c = ahAcc.Id
					, Account__c = ayAcc.Id
					, AGNTNUM__c = atCon.E_CL3PF_AGNTNUM__c
					, Contact__c = atCon.Id
					, E_Policy__c = policy.Id
					, E_CHTPF__c = chtpf.Id
					, YYYY__c = d.year()
					, MM__c = d.month()
					, OWNAME__c = 'Owner' + i
					, ZJINTSEQ__c = '1'
				);
				bilas.add(bila);
			}
			insert bilas;
		}
	}

    private static void createDataEBizLog(String kind, String noticekind){
        system.runAs(thisuser){
            log = new E_BizDataSyncLog__c();
            log.Kind__c = kind;
            log.NotificationKind__c = noticekind;
            insert log;
        }
    }
}