@RestResource(urlMapping='/SCApp/*')
global without sharing class SC_RestResource {
    

    global class appStatusResponse {
        public String access{get;set;}      // アクセス権
        public String usertype{get;set;}    // ユーザタイプ
        public String officetype{get;set;}  // 代理店種別
        public String officename{get;set;}  // 事業所名
        public String officecode{get;set;}  // 事業所コード
        public String mrunitname{get;set;}  // 営業部名
        public String mrname{get;set;}      // MR氏名
        public String status{get;set;}      // ステータス
        public String dispdefect{get;set;}  // 不備ステータス
        public String message{get;set;}     // メッセージ
        public String statuscode{get;set;}  // ステータスコード
        public String token{get;set;}       // LimeSurveyトークン
        public String surveyid{get;set;}    // LimeSurveyアンケートid

        global appStatusResponse(){
            //Constructor   
            this.access = '';
            this.usertype = '';
            this.officetype = '';
            this.officename = '';
            this.officecode = '';
            this.mrunitname = '';
            this.mrname = '';
            this.status = '';
            this.dispdefect = '';
            this.message = '';
            this.statuscode = '';
            this.token = '';
            this.surveyid = '';
        }
    }

    /* API Request
     *    APIリクエストを定義する
     */
    global class appAnswerRequest{
        public String token{get;set;}                   // LimeSurvyトークン
        public Integer surveyid{get;set;}               // LimeSurvyアンケートID
        public String holdstatus{get;set;}              // 保留有無
        public String responseid{get;set;}              // LimeSurvyアンケート回答ID
        public String submitdate{get;set;}              // 送信日時
        public Map<String,String> responses{get;set;}   //　回答
    }

    /* API Response
     *    APIレスポンスを定義する
     */
    global class appAnswerResponse{
        public String status{get;set;}
        public String statuscode{get;set;}
        public String message{get;set;}

        public appAnswerResponse() {
            this.status = '';
            this.statuscode = '';
            this.message = '';
        }
    }

    @HttpGet
    global static appStatusResponse appStatus() {
        appStatusResponse response = new appStatusResponse();
        SC_AppManager appManager;
        try {
            // LimeSurveyトークン取得
            RestRequest req = RestContext.request;
            String token = req.params.get('token');
            String surveyid = req.params.get('surveyid');

            appManager = new SC_AppManager(token);

            // レスポンスセット
            response.access = appManager.getAccess();
            response.usertype = appManager.getUserType();
            response.officetype = appManager.record.OfficeType__c;
            response.officename = appManager.record.AccZEAYNAM__c;
            response.officecode = appManager.record.AccZAGCYNUM__c;
            response.mrunitname = appManager.record.OwnerMRUnitName__c;
            response.mrname = appManager.record.OwnerMRName__c;
            response.status = appManager.record.Status__c;
            response.dispdefect = appManager.record.DispDefect__c;
            response.token = token;
            response.surveyid = surveyid;
            response.statuscode = 'SCS001';

            system.debug('debug:response=' + response);

            E_log__c log = new E_Log__c();
            log.Name = 'surveyLog';
            log.Detail__c = String.valueOf(response);
            log.AccessUser__c = UserInfo.getUserId();
            log.AccessDatetime__c = Datetime.now();

            Database.Saveresult saveResult = Database.insert(log);

            if(!saveResult.isSuccess()){
                System.debug(saveResult.getErrors());
            }

            return response;

        }catch(Exception e) {
            List<String> errMsg = e.getMessage().split('::');
            if (errMsg.size() == 2) {
                response.statuscode = errMsg[0];
                response.message = errMsg[1];

            } else {
                response.statuscode = 'SCE000';
                response.message = e.getMessage() + ' / ' + e.getStackTraceString();
                
            }
            system.debug('debug:response=' + response);

            return response;
        }
    }

    @HttpPost
    global static appAnswerResponse answerRegistered(appAnswerRequest request) {
        appAnswerResponse response = new appAnswerResponse();
        SC_AppManager appManager;
        String prefix = '';

        // String selectSelectFileds = '';
        try {
            system.debug('debug:request=' + request);
            /*
            for (String key : request.responses.keySet()) {
                system.debug('debug:' + key + '=' + request.responses.get(key));
                selectSelectFileds += ',' + key + '__c';
            }
            String query = 'select id' + selectSelectFileds + ' from SC_WebAnswer__c where LS_Token__c=\'' + request.token + '\'';
            system.debug('debug: query=' + query);
            */
            appManager = new SC_AppManager(request.token, request.surveyid);
            appManager.validate();

            // 追加の場合はPrefix付与
            if (appManager.parent.Kind__c == 'NN追加') prefix = 'NN';

            SC_WebAnswer__c record = new SC_WebAnswer__c();
            for (String key : request.responses.keySet()) {
                system.debug('debug:key=' + key + ' / value=' + request.responses.get(key));
                // 回答をセット
                record.put(prefix + key + '__c', request.responses.get(key));
            }
            record.SC_SelfCompliance__c = appManager.parent.id;
            record.submitDate__c = request.submitdate;
            record.holdStatus__c = request.holdStatus;
            record.LimeSurveyToken__c = request.token;
            record.LimeSurveyId__c = String.valueOf(request.surveyid);

            // Upsert Key
            // record.UpsertKey__c = request.responseid;
            record.UpsertKey__c = request.token + '|' + String.valueOf(request.surveyid);

            // requestの値をそのまま保持
            record.requestBody__c = String.valueOf(request);

            upsert record UpsertKey__c;

            response.status = 'OK';
            response.statuscode = 'SCS001';

            system.debug('debug:response=' + response);

            return response;

        } catch (Exception e) {
            system.debug('err:' + e);
            response.status = 'NG';

            List<String> errMsg = e.getMessage().split('::');
            if (errMsg.size() == 2) {
                response.statuscode = errMsg[0];
                response.message = errMsg[1];

            } else {
                
                response.statuscode = 'SCE000';
                response.message = e.getMessage();
                
            }
            system.debug('debug:response=' + response);

            return response;
        }
    }
}