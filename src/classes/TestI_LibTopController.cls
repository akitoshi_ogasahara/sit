@isTest
private class TestI_LibTopController {

	//コンストラクタのテスト
	@isTest static void constructorTest() {
		I_ContentMaster__c icm = createData();

		Test.startTest();
			I_LibTopController ilt = new I_LibTopController();
			System.assertEquals(ilt.sections[0].Id,icm.Id);
		Test.stopTest();
	}

	//getTubeConnectHelperのテスト
	@isTest static void getTubeConnectHelperTest() {
		I_ContentMaster__c icm = createData();

		Test.startTest();
			I_LibTopController ilt = new I_LibTopController();
			I_TubeConnectHelper conect =  ilt.getTubeConnectHelper();
			System.assertEquals(conect.getSessionId(),E_Util.getSessionId());
		Test.stopTest();
	}


	//getTubeConnectHelperのテスト
	@isTest static void getBannerContentsTest() {
		//I_ContentMaster__c icm = createData();
		I_MenuMaster__c mm = createIRISMenuMaster2();
		I_PageMaster__c ipm = createIRISPage2(mm.Id);
		I_ContentMaster__c cm = createIRISCMS2(ipm);

		Test.startTest();
			I_LibTopController ilt = new I_LibTopController();
			List<I_CMSComponentController> compList = ilt.getBannerContents();
			System.assertEquals(compList.size(),1);
		Test.stopTest();
	}

	//データ作成
	static I_ContentMaster__c createData(){
		I_MenuMaster__c mm = createIRISMenuMaster();
		I_PageMaster__c ipm = createIRISPage(mm.Id);
		I_ContentMaster__c icm = createIRISCMS(ipm);
		//ID管理
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());

		return icm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.Menu__c = mmId;
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS(icm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS(I_ContentMaster__c paricm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		insert icm;
		return icm;
	}

	static I_MenuMaster__c createIRISMenuMaster(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = I_CONST.IRIS_MENU_NAME_LIB_TOP;
		insert mm;
		return mm;
	}

	static I_MenuMaster__c createIRISMenuMaster2(){
		I_MenuMaster__c mm = new I_MenuMaster__c();
		mm.Name = '特集';
		mm.SubCategory__c = I_Const.MENU_SUB_CATEGORY_LIBLARY_FEATURE;
		insert mm;
		return mm;
	}

	//irisPage
	static I_PageMaster__c createIRISPage2(Id mmId){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'uniquekey';
		pm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		pm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		pm.Menu__c = mmId;
		insert pm;
		return pm;
	}

	//irisCMS
	static I_ContentMaster__c createIRISCMS2(I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.Page__c = ipm.Id;
		insert icm;
		createChildIRISCMS2(icm,ipm);
		return icm;
	}

	static I_ContentMaster__c createChildIRISCMS2(I_ContentMaster__c paricm,I_PageMaster__c ipm){
		I_ContentMaster__c icm = new I_ContentMaster__c();
		icm.ParentContent__c = paricm.Id;
		icm.IRIS_Page__c = ipm.Id;
		//icm.FeatureDisplayTo__c = E_Util.SYSTEM_TODAY();
		//icm.FeatureDisplayFrom__c = E_Util.SYSTEM_TODAY();
		//icm.FeatureFlag__c = true;
		insert icm;
		return icm;
	}
}