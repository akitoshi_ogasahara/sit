/**
 * 新契約申込進捗結果(JP)コントローラクラス
 * CreatedDate 2014/12/15
 * @Author Terrasky
 */
public with sharing class E_ProgressSearchController extends E_AbstractController{

	public String iPage {
		get {
			return iPage == null ? 'E_ProgressSearchAgentSVE' : iPage;
		}
		set;}

	public E_ProgressSearchController() {
		pgTitle = '新契約申込進捗結果';
	}
}