/**
 *		EBiz連携ログから呼び出されるBatch処理の実行ロジック用Superクラス
 */
public abstract with sharing class E_BizDataSyncBatchOperation {
	public List<String> errMsgs = new List<String>();
	public List<String> getErrMsgs(){
		return errMsgs;
	}

	public abstract Database.QueryLocator getQueryLocator();
	public abstract void execute(List<Sobject> records);

}