@isTest
private class TestASS_MRPageController {

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（MR一覧から）
	 * エリア部長の場合の初期表示と空更新処理
	 */
	private static testMethod void areaManagerTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//MRユーザ作成
		User mr = createMRUser();
		//事務所作成
		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> officeList = new List<Account>();
		officeList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'ア'));
		officeList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'イ'));
		officeList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'ウ'));
		officeList.add(new Account(Name = 'off4', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'エ'));
		officeList.add(new Account(Name = 'off5', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'オ'));
		insert officeList;

		//商品マスタ作成
		ProductMaster__c fsprod = new ProductMaster__c(Name = 'Sample_FS', LargeClassification__c = 'CategoryⅢ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert fsprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//FS見込案件作成
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = officeList[0].Id, OwnerId = mr.Id, ProductName_TEXT__c = 'CategoryⅢ'));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = officeList[0].Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;

		//行動のレコードタイプ取得
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		//行動
		List<Event> eventList = new List<Event>();

		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test1', WhatId = officeList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test2', WhatId = officeList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = eventRecType.Id));
		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test3', WhatId = officeList[0].Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = eventRecType.Id));
		insert eventList;
		

		//営業目標
		SalesTarget__c st = new SalesTarget__c(Account__c = officeList[0].Id, Day__c = System.today(), APETarget__c = 100, ESTOppTarget__c = 200, NumOfContactsTarget__c = 300, DesignDocReqTarget__c = 400);
		insert st;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',mr.Id);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();

			List<ASS_MRPageController.OfficeListRow> ofrow = new List<ASS_MRPageController.OfficeListRow>();
			ofrow = con.getOfficeList();

			//空更新
			con.edit();
			con.save();
			//キャンセル
			con.edit();
			con.cancel();
//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.getOfficeList().size(),officeList.size());
			List<SalesTarget__c> resultSTList = [SELECT ESTOppTarget__c, DesignDocReqTarget__c, APETarget__c, NumOfContactsTarget__c FROM SalesTarget__c];
			Integer estEventListSize = [SELECT COUNT() FROM Event WHERE IsOpportunity__c = true];
			Integer docRecEventListSize = [SELECT COUNT() FROM Event WHERE DesignDocReq__c = true];

			System.assertEquals(ofrow[0].estTarget,resultSTList[0].ESTOppTarget__c);
			System.assertEquals(ofrow[0].estPerform,estEventListSize);
			System.assertEquals(ofrow[0].designDocTarget,resultSTList[0].DesignDocReqTarget__c);
			System.assertEquals(ofrow[0].designDocPerform,docRecEventListSize);
			System.assertEquals(ofrow[0].apeTarget,resultSTList[0].APETarget__c);
			System.assertEquals(ofrow[0].apePerform,oppList[0].WAPE__c);
			System.assertEquals(ofrow[0].contactTarget,resultSTList[0].NumOfContactsTarget__c);
			System.assertEquals(ofrow[0].contactPerform,eventList.size());

		}
	
	}

	/**
	 * 実行ユーザ: エリア部長
	 * パラメータ: あり（MR一覧から）
	 * エリア部長の場合のソート処理
	 */
	private static testMethod void areaManagerSortTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(true, 'satest', 'エリア部長');

		//MRユーザ作成
		User mr = createMRUser();
		//事務所作成
		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		List<Account> officeList = new List<Account>();
		officeList.add(new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'ア'));
		officeList.add(new Account(Name = 'off2', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'イ'));
		officeList.add(new Account(Name = 'off3', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'ウ'));
		officeList.add(new Account(Name = 'off4', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'エ'));
		officeList.add(new Account(Name = 'off5', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id, E_CL2PF_ZEAYKNAM__c = 'オ'));
		insert officeList;

		//FS見込案件作成
		createOpp(officeList[4],mr);
		//行動のレコードタイプ取得
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		//行動
		createEvent(officeList[4],mr,eventRecType.Id);
		

		//営業目標
		SalesTarget__c st = new SalesTarget__c(Account__c = officeList[4].Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3);
		insert st;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',mr.Id);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			//初期表示時のソートタイプ
			String firstSortType = con.sortType;
			//初期表示時のソート順
			Boolean firstSortIsAsc = con.sortIsAsc;

			//APE目標でソート
			pr.getParameters().put('st','apetarget');
			con.sortRows();

			//APE目標のソート時のソートタイプ
			String resultSortType = con.sortType;
			//APE目標のソート時のソート順
			Boolean resultSortIsAsc = con.sortIsAsc;

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(firstSortType,'officekananame');
			System.assertEquals(firstSortIsAsc,true);
			System.assertEquals(resultSortType,'apetarget');
			System.assertEquals(resultSortIsAsc,true);
		}
	
	}
	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MR一覧から）
	 * MR場合の画面と更新処理
	 */
	private static testMethod void mrTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);

		//見込案件作成
		createOpp(office,actUser);
		//行動のレコードタイプ取得
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		//行動
		createEvent(office,actUser,eventRecType.Id);

		//営業目標
		SalesTarget__c st = new SalesTarget__c(Account__c = office.Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3);
		insert st;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',actUser.Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			ApexPages.currentPage().getParameters().put('st','officekananame');
			Test.setCurrentPage(pr);
			con.sortRows();
			//更新
			con.edit();
			con.officeRows[0].estTarget = 100;
			con.save();
//===============================テスト終了===============================
			Test.stopTest();
			SalesTarget__c resultST = [SELECT ESTOppTarget__c FROM SalesTarget__c WHERE Id = :st.Id];
			System.assertEquals(con.getOfficeList().size(),1);
			System.assertEquals(resultST.ESTOppTarget__c,100);
		}
	
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MR一覧から）
	 * MR場合の画面と表示件数切替処理及び戻る処理
	 */
	private static testMethod void mrBackTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = actUser.Id);
		insert office;

		//全件表示用事務所リスト作成
		List<Account> offList = new List<Account>();
		for(Integer i=0; i<15; i++) {
			Account off = new Account(Name = 'off'+ i , E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = actUser.Id);
			offList.add(off);
		}
		insert offList;
		
		//見込案件
		createOpp(office, actUser);
		//行動のレコードタイプ取得
		RecordType eventRecType = [select id from RecordType where SobjectType = 'Event' and DeveloperName = 'DailyReport'];
		//カスタム設定作成
		createCustomField(eventRecType.Id);
		//行動
		createEvent(office,actUser,eventRecType.Id);

		//営業目標
		SalesTarget__c st = new SalesTarget__c(Account__c = office.Id, Day__c = System.today(), APETarget__c = 1000000, ESTOppTarget__c = 3, NumOfContactsTarget__c = 3, DesignDocReqTarget__c = 3);
		insert st;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',actUser.Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			con.addRows();
			con.returnRows();
			con.back();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_MRSummary;
			System.assertEquals(con.back().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MR一覧から）
	 * MR場合の画面と代理店事務所ページへの遷移処理
	 */
	private static testMethod void mrMoveToOfficeSummaryTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',actUser.Id);

		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			ApexPages.currentPage().getParameters().put('accountId',office.Id);
			con.moveToOfficeSummary();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_OfficeSummary;
			newPage.getParameters().put('mrId',actUser.Id);
			newPage.getParameters().put('accountId',office.Id);
			newPage.setRedirect(true);
			System.assertEquals(con.moveToOfficeSummary().getUrl(),newPage.getUrl());
		}
	
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MR一覧から）
	 * MR場合の新規活動作成画面遷移処理
	 */
	private static testMethod void mrNewEventTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',actUser.Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			con.newEvent();
//===============================テスト終了===============================
			Test.stopTest();
			PageReference newPage = Page.ASS_EventInputPage;
			newPage.getParameters().put('mrId',actUser.Id);
			newPage.getParameters().put('retPgName','/apex/ASS_MRPage');

			System.assertEquals(con.newEvent().getUrl(),newPage.getUrl());
		}
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MR一覧から）
	 * 担当事務所一覧を複数回保存した時に常にインサートされる不具合対応
	 */
	private static testMethod void mrMultiSaveTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);


		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',actUser.Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			//1回目保存
			con.edit();
			con.officeRows[0].estTarget = 100;
			con.officeRows[0].designdoctarget = 100;
			con.officeRows[0].apetarget = 100;
			con.officeRows[0].contacttarget = 100;
			con.save();

			//2回目保存
			con.edit();
			con.officeRows[0].estTarget = 200;
			con.officeRows[0].designdoctarget = 200;
			con.officeRows[0].apetarget = 200;
			con.officeRows[0].contacttarget = 200;
			con.save();
//===============================テスト終了===============================
			Test.stopTest();
			List<SalesTarget__c> resultSTList = [SELECT ESTOppTarget__c, DesignDocReqTarget__c, APETarget__c, NumOfContactsTarget__c FROM SalesTarget__c WHERE Account__c = :office.Id];
			System.assertEquals(resultSTList.size(),1);
			System.assertEquals(resultSTList[0].ESTOppTarget__c,200);
			System.assertEquals(resultSTList[0].DesignDocReqTarget__c,200);
			System.assertEquals(resultSTList[0].APETarget__c,200);
			System.assertEquals(resultSTList[0].NumOfContactsTarget__c,200);
		}
	
	}

	/**
	 * 実行ユーザ: MR
	 * パラメータ: あり（MR一覧から）
	 * 予算目標を再読み込み時に初期化せずに合計値が変わる不具合対応
	 */
	private static testMethod void mrTargetReloadTest() {
		//実行ユーザ作成
		User actUser = TestI_TestUtil.createUser(false, 'satest', 'ＭＲ');
		actUser.Unit__c = 'テスト1営業部';
		actUser.IsActive = true;
		insert actUser;

		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//事務所作成
		Account office = createOffice(actUser);

		//商品マスタ作成
		ProductMaster__c fsprod = new ProductMaster__c(Name = 'Sample_FS', LargeClassification__c = 'CategoryⅢ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert fsprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		//FS予算
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'CategoryⅢ', OwnerId = actUser.Id));
		//PT予算
		oppList.add(new Opportunity(Name = 'PT予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 2000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'CategoryⅡ', OwnerId = actUser.Id));
		//BAFS予算
		oppList.add(new Opportunity(Name = 'BAFS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 3000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'BA Financial Solutions', OwnerId = actUser.Id));
		//BAPT予算
		oppList.add(new Opportunity(Name = 'BAPT予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 4000000, InsuranceType__c = fsprod.Id, ProductName_TEXT__c = 'BA Protection', OwnerId = actUser.Id));
		//FS実績
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = office.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払', OwnerId = actUser.Id));
		insert oppList;

		//ページ表示
		PageReference pr = Page.ASS_MRPage;
		Test.setCurrentPage(pr);
		pr.getParameters().put('mrId',actUser.Id);
		System.runAs(actUser) {
//===============================テスト開始===============================
			Test.startTest();
			ASS_MRPageController con = new ASS_MRPageController();
			con.getOfficeList();
			con.getTarget();
			con.getTarget();
			con.getTarget();

//===============================テスト終了===============================
			Test.stopTest();
			System.assertEquals(con.target_FS,1000000);
			System.assertEquals(con.target_PT,2000000);
			System.assertEquals(con.target_BAFS,3000000);
			System.assertEquals(con.target_BAPT,4000000);
		}
	
	}

		//カスタム設定作成
	private static void createCustomField(String recTypeId) {
		RecordType recordType = [SELECT Id, DeveloperName FROM RecordType where SobjectType = 'Event' and Id = :recTypeId limit 1];
		
        EventCheck__c eventCheck = new EventCheck__c();
        eventCheck.Name = 'システム定義';
        eventCheck.Recordtypename__c = recordType.DeveloperName;
        insert eventCheck;
	}

	//MRユーザ作成
	private static User createMRUser() {
		//エリア
		E_Area__c area = new E_Area__c();
		area.Name = 'テスト';
		area.AreaCode__c = 'A1';
		area.SortNo__c = 1;
		area.IsActive__c = '1';
		insert area;

		//営業部
		E_Unit__c unit = new E_Unit__c(Name = 'テスト1営業部', BRANCH__c = '00', IsActive__c = '1', Area__c = area.Id);
		insert unit;

		//MRユーザ作成
		User mr = TestI_TestUtil.createUser(false, 'mr1', 'ＭＲ');
		mr.Unit__c = 'テスト1営業部';
		mr.IsActive = true;
		insert mr;

		return mr;
	}

	//事務所作成
	private static Account createOffice(User mr) {
		//代理店格
		Account acc = new Account(Name = 'ac1', E_COMM_VALIDFLAG__c = '1', E_CL1PF_KCHANNEL__c = 'BK');
		insert acc;

		//事務所
		Account office = new Account(Name = 'off1', E_COMM_VALIDFLAG__c = '1',ParentId = acc.Id, OwnerId = mr.Id);
		insert office;

		return office;
	}

	//FS見込案件作成
	private static void createOpp(Account office, User mr) {
		//商品マスタ作成
		ProductMaster__c fsprod = new ProductMaster__c(Name = 'Sample_FS', LargeClassification__c = 'CategoryⅢ', MiddleClassification__c = 'LTA', IsActive__c = true);
		insert fsprod;

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();

		//見込案件のレコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//見込案件
		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(new Opportunity(Name = 'FS予算', RecordTypeId = recTypeMap.get('RecordType2'), StageName = 'S:予算', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = office.Id, OwnerId = mr.Id));
		oppList.add(new Opportunity(Name = 'FS実績', RecordTypeId = recTypeMap.get('RecordType1'), StageName = 'A', CloseDate = System.today()
										, Amount = 100, WAPE__c = 1000000, InsuranceType__c = fsprod.Id, AccountId = office.Id, ContractTyoe__c = '法人', Count__c = 1, PaymentType__c = '年払'));
		insert oppList;
	}

	//行動作成
	private static void createEvent(Account office, User mr, String recTypeId) {
		List<Event> eventList = new List<Event>();

		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test1', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = true, RecordTypeId = recTypeId));
		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test2', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = false, DesignDocReq__c = true, RecordTypeId = recTypeId));
		eventList.add(new Event(OwnerId = mr.Id, Subject = 'test3', WhatId = office.Id, startDatetime = System.now(), endDatetime = System.now(), EventType__c = '記録'
								,Action__c = 'TEL', IsOpportunity__c = true, DesignDocReq__c = false, RecordTypeId = recTypeId));
		insert eventList;
	}
}