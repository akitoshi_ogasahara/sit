/*
 * CC_VILAgentSearchControllerTest
 * Test class of CC_VILAgentSearchController
 * created  : Accenture 2018/7/6
 * modified :
 */

@isTest
private class CC_VILAgentSearchControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			vlocity_ins__VlocitySearchWidgetSetup__c newVlocitySearchWidgetSetup = CC_TestDataFactory.getVlocitySearchWidgetSetupSkel('CC_VILAgentSearchController');
			insert newVlocitySearchWidgetSetup;
			Contact newContact = CC_TestDataFactory.getContactSkel();
			newContact.E_CL3PF_VALIDFLAG__c = '1';
			newContact.E_CL3PF_AGNTNUM__c = 'test';
			newContact.E_CL3PF_ZEATKNAM__c= 'test';
			insert newContact;

		}
	}

	/**
	 * Test getSearchRequest
	 */
	static testMethod void getSearchRequestTest() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchRequest';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting lookupRequest
			inputMap.put('lookupRequest', (Object)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0]);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 */
	static testMethod void getSearchResultsTest01() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('CL3PFAGNTNUM', '*');
			searchValueMap.put('CL3PFZEATKNAM', '*');
			searchValueMap.put('CLTPFDOB', '*');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * 生年月日の検索：和暦 明治
	 */
	static testMethod void getSearchResultsTest02() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('CLTPFDOB', 'M010/01/01');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * 生年月日の検索：和暦 大正
	 */
	static testMethod void getSearchResultsTest03() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('CLTPFDOB', 'T0100101');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * 生年月日の検索：和暦 昭和
	 */
	static testMethod void getSearchResultsTest04() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('CLTPFDOB', 'S010/1/1');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * 生年月日の検索：和暦 平成
	 */
	static testMethod void getSearchResultsTest05() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('CLTPFDOB', 'h10/1/1');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test getSearchResults
	 * 生年月日の検索：西暦
	 */
	static testMethod void getSearchResultsTest06() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'getSearchResults';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting resultInfo
			Map<String, Object> resultInfo = new Map<String, Object>();
			resultInfo.put('uniqueRequestName', null);
			resultInfo.put('drBundleName', null);
			resultInfo.put('interactionObjName', null);
			inputMap.put('resultInfo', resultInfo);

			// Setting lookupRequest & searchValueMap
			vlocity_ins.LookupRequest request = (vlocity_ins.LookupRequest)vlocity_ins.LookupRequestResultController.getLookupRequests(null)[0];
			Map<String, Object> searchValueMap = request.searchValueMap;
			searchValueMap.put('CLTPFDOB', '1/1');
			request.searchValueMap = searchValueMap;
			inputMap.put('lookupRequest', request);

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	 * Test launchConsole
	 */
	static testMethod void launchConsoleTest() {
		CC_VILAgentSearchController ctrl = new CC_VILAgentSearchController();
		String methodName = 'launchConsole';
		Map<String, Object> inputMap = new Map<String, Object>();
		Map<String, Object> outputMap = new Map<String, Object>();
		Map<String, Object> optionMap = new Map<String, Object>();

		System.runAs ( testUser ) {

			// Setting Launch data
			inputMap.put('interactionObjName', 'test');
			inputMap.put('recordId', 'test');
			inputMap.put('name', 'test');

			Test.startTest();
			ctrl.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}