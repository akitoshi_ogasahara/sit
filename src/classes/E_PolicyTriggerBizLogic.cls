public with sharing class E_PolicyTriggerBizLogic {

	private static Map<String,RecordType> policyRecTypes;
	private static Map<Id,RecordType> policyRecIdTypes;
	private static Map<String,RecordType> getPolicyRecordTypes(){
		if(policyRecTypes==null){
			policyRecTypes = new Map<String,RecordType>();
			policyRecIdTypes = new Map<Id,RecordType>();
			for(RecordType rt:[SELECT Id,Name,DeveloperName FROM RecordType 
					WHERE SObjectType = :E_Policy__c..sObjectType.getDescribe().getName()]){
				policyRecTypes.put(rt.DeveloperName, rt);
				policyRecIdTypes.put(rt.Id, rt);
			}
		}
		
		return policyRecTypes;
	} 


	/**
	 *		参照可能な顧客情報を設定する
	 *			年金支払開始前：契約者　　開始後：年金受取人
	 */
	public void setAccesibleCustomer(List<E_Policy__c> records){
		//SPVA_ZANNSTFLG__c
		Id spvaRecordTypeId = getPolicyRecordTypes().get('ESPHPF').Id;
		Id coliRecordTypeId = getPolicyRecordTypes().get('ECHDPF').Id;
		
		Set<Id> contactIds = new Set<Id>();
		List<E_Policy__c> corporateList = new List<E_Policy__c>();
		
		for(E_Policy__c rec:records){
			//SPVA(消滅以外)
			if(rec.recordTypeId == spvaRecordTypeId){
				if(rec.SPVA_ZANNSTFLG__c){
					//年金支払開始後は年金受取人
					rec.AccessibleCustomer__c = rec.Annuitant__c;
				}else{
					//契約者
					rec.AccessibleCustomer__c = rec.Contractor__c;
				}

			//COLI（消滅以外）
			}else if(rec.recordTypeId == coliRecordTypeId){
				//契約者を設定
				rec.AccessibleCustomer__c = rec.Contractor__c;

			//消滅など上記以外	
			}else{
				rec.AccessibleCustomer__c = null;
			}
			//対象の団体顧客
			if (rec.Corporate__c != null) {
				contactIds.add(rec.Corporate__c);
				corporateList.add(rec);
			} else {
				rec.CorporateName__c = null;
			}
			if (policyRecIdTypes.get(rec.recordTypeId) != null) {
				rec.RecordTypeDeveloperName__c = policyRecIdTypes.get(rec.recordTypeId).DeveloperName;
			}
		}
		
		if (!contactIds.isEmpty()) {
			//団体顧客名をセット
			Map<id, Contact> contactMap = new Map<id, Contact>([Select LastName, FirstName From Contact Where id in :contactIds]);
			for (E_Policy__c rec : corporateList) {
				Contact con = contactMap.get(rec.Corporate__c);
				if (con != null) {
					String lastName = con.LastName != null ? con.LastName : '';
					String firstName = con.FirstName != null ? con.FirstName : '';
					rec.CorporateName__c = lastName + ' ' + firstName;
				} 
			}
		}
	}


}