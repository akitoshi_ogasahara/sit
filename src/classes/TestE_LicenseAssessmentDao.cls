@isTest
public class TestE_LicenseAssessmentDao {

	/**
	 * getRecsByASRIdのテスト
	 */
	@isTest static void getRecsByASRIdTest() {
		// 代理店挙積情報の作成
		E_AgencySalesResults__c agencySalesResults = TestE_AgencySalesResultsDao.createAgencySalesResults();
		insert agencySalesResults;

		// 資格査定詳細情報の作成
		E_LicenseAssessment__c licenseAssessment = createLicenseAssessment(agencySalesResults.Id);
		insert licenseAssessment;

		Test.startTest();
		List<E_LicenseAssessment__c> recs = E_LicenseAssessmentDao.getRecsByASRId(agencySalesResults.Id);
		System.assertEquals('優績A 1', recs[0].Hierarchy__c);
		Test.stopTest();

	}

	/**
	 * 資格査定詳細情報の作成
	 */
	public static E_LicenseAssessment__c createLicenseAssessment(Id ASRId){
		E_LicenseAssessment__c licenseAssessment = new E_LicenseAssessment__c();
		//licenseAssessment.AddParentAccountCode__c		= '';				// 合算先代理店コード
		licenseAssessment.E_AgencySalesResults__c		= ASRId;			// 代理店挙積情報
		//licenseAssessment.E_Unit__c					= null;				// 営業部コード
		licenseAssessment.Hierarchy__c					= '優績A 1';		// 階層
		//licenseAssessment.MR__c						= '';				// MRコード
		//licenseAssessment.ParentAccount__c			= '';				// 代理店コード
		//licenseAssessment.SectionCode__c				= '';				// 課コード
		licenseAssessment.Standard_ActiveMonth__c		= 0;				// 基準　稼動月数
		licenseAssessment.Standard_IANP__c				= 0;				// 基準　保有ANP
		licenseAssessment.Standard_IQA__c				= 0;				// 基準　IQA継続率
		licenseAssessment.Standard_NBCANP_SpInsType__c	= 0;				// 基準　新契約係数ANP 特定保険種類
		licenseAssessment.Standard_NBCANP__c			= 0;				// 基準　新契約係数ANP
		licenseAssessment.State_ActiveMonth__c			= 0;				// 状況　稼動月数
		licenseAssessment.State_IANP__c					= 0;				// 状況　保有ANP
		licenseAssessment.State_IQA__c					= 0;				// 状況　IQA継続率
		licenseAssessment.State_NBCANP_SpInsType__c		= 0;				// 状況　新契約係数ANP 特定保険種類
		licenseAssessment.State_NBCANP__c				= 0;				// 状況　新契約係数ANP
		licenseAssessment.BusinessDate__c				= '20170131';		// 営業日
		return licenseAssessment;
	}

}