public with sharing class E_ErrorPage extends E_AbstractController {

	private String errorMsgCode;
	public Boolean pipflag {get;set;}
	public Boolean isSumisei {get; private set;}
	// IRISメニュー
	public I_MenuMaster__c menu {get{
										if(menu==null){
											menu = iris.irisMenus.activeMenu;
										}
										return menu;
									}
									set;}

   	//ページタイトル
	private static final String PAGE_TITLE = 'エラーページ';
	private static final String DEFAULT_ERROR_MSG = 'エラーが発生しました。';


	public E_ErrorPage () {
		errorMsgCode = ApexPages.currentPage().getParameters().get('code');
		//pip画面からの遷移か
		pipflag = false;
		String param = ApexPages.currentPage().getParameters().get(I_Const.ERR_URLPARAM_KEY);
		if(param == I_Const.ERR_URLPARAM_PIP){
			pipflag = true;
		}
        this.pgTitle = PAGE_TITLE;

        this.isSumisei = access.isSumiseiIRISUser();
	}

	public PageReference init() {
		//抽象クラスのクッキー設定メソッド
		//setIssCookie();

		E_MessageMaster__c msgMaster = E_MessageMasterDao.getRecByCode(errorMsgCode);
		if (msgMaster != null) {
			//errorMsg = message.Value__c;
			List<String> params = new List<String>();

			//String.formatに埋め込む値を最大3つ受取可能とする。
			for(Integer i=1;i<=3;i++){
				String pNM = 'p' + String.valueOf(i);
				if(ApexPages.currentPage().getParameters().containsKey(pNM)){
					params.add(ApexPages.currentPage().getParameters().get(pNM));
				}
			}

			getPageMessages().addErrorMessage(String.format(msgMaster.Value__c, params));

		}else{
			getPageMessages().addErrorMessage(DEFAULT_ERROR_MSG);
		}
		return null;
	}

	public boolean getIsClosebtn() {
		return E_Util.getIsIss(E_CookieHandler.getCookieIssSessionId());
	}
}