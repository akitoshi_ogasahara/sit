/**
 * 
 */
@isTest
private class TestCR_BizLogicBatchOperation {

    static testMethod void operationTest() {
    	// 社員ユーザ作成
		User usr = TestCR_TestUtil.createUser(true, 'TestUser001', 'E_EmployeeStandard');
		usr.MRCD__c = '11111';
		update usr;
		
		// Account作成(母店)
		List<Account> botenList = new List<Account>();
		Account acc1 = TestCR_TestUtil.createAccount(true);
		acc1.E_CL1PF_ZHEADAY__c = '11111';
		acc1.E_CL1PF_ZBUSBR__c = '10';
		acc1.E_CL1PF_ZBUSAGCY__c = '11111';
		acc1.ZMRCODE__c = '11111';
		acc1.KSection__c = '11';
		botenList.add(acc1);
		
		Account acc2 = TestCR_TestUtil.createAccount(true);
		acc2.E_CL1PF_ZHEADAY__c = '22222';
		acc2.E_CL1PF_ZBUSBR__c = '20';
		acc2.E_CL1PF_ZBUSAGCY__c = '22222';
		acc2.ZMRCODE__c = '22222';
		acc2.KSection__c = '22';
		botenList.add(acc2);
		update botenList;
		
		// Account作成(事務所)
		List<Account> branchList = new List<Account>();
		Account branchAcc1 = TestCR_TestUtil.createAccount(true);
		branchAcc1.ParentId = acc1.Id;
		branchAcc1.E_CL2PF_BRANCH__c = '10';
		branchAcc1.E_COMM_VALIDFLAG__c = '1';
		branchAcc1.E_CL2PF_ZAGCYNUM__c = '11111';
		branchList.add(branchAcc1);
		
		Account branchAcc2 = TestCR_TestUtil.createAccount(true);
		branchAcc2.ParentId = acc2.Id;
		branchAcc2.E_CL2PF_BRANCH__c = '20';
		branchAcc2.E_COMM_VALIDFLAG__c = '1';
		branchAcc2.E_CL2PF_ZAGCYNUM__c = '22222';
		branchList.add(branchAcc2);		
		update branchList;

		//Group
		List<Group> gpList = new List<Group>();
		gpList.add(new Group(name='BR10'));
		gpList.add(new Group(name='BR20'));
		insert gpList;
		
		Map<String, Group> gmap = new Map<String, Group>();
		gmap.put('10', gpList[0]);
		gmap.put('20', gpList[1]);
		
		// 代理店提出
		CR_Agency__c agny = TestCR_TestUtil.createCRAgency(true, acc2.Id, CR_Const.RECTYPE_DEVNAME_BIZ);

		Test.startTest();
		
		CR_BizLogicBatchOperation bbo = new CR_BizLogicBatchOperation();
		bbo.setZbusbrCdMap(gmap);
		bbo.init([Select Id, E_CL1PF_ZHEADAY__c, E_CL1PF_ZBUSBR__c, ZMRCODE__c From Account]);
		bbo.createData();
		bbo.executeData();
		
		Test.stopTest();
		
		Set<Id> ids = new Set<Id>{botenList[0].Id, botenList[1].Id};
		List<CR_Agency__c> resultAgncy = [Select Id From CR_Agency__c Where Account__c In :ids];
		system.assert(resultAgncy.size() > 0);
        
    }
}