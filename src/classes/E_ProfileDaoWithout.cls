/**
 * プロファイルを取得するためのDao(Without)
 */
public without sharing class E_ProfileDaoWithout {

    /**
     * プロファイルＩＤをNameから取得する
     * @param  proName:プロファイルの名前
     * @return rec:取得したプロファイルレコード
     */
    public static Profile getRecByName(String proName){

        Profile rec = null;
        List<Profile> recs = [select Id,Name from Profile where Name = :proName];
        if(recs.size() > 0){
            rec = recs[0];
        }
        return rec;
    }

    /**
     * プロファイルＩＤをNameリストから取得する
     * @param  proName:プロファイルの名前リスト
     * @return rec:取得したプロファイルレコードMap
     */
    public static Map<String,Profile> getRecsByNames(Set<String> proNames){

		Map<String,Profile> ret = new Map<String,Profile>();
		for(Profile p:[select Id,Name from Profile where Name in :proNames]){
			ret.put(p.Name, p);
		}
		return ret;
    }
	
}