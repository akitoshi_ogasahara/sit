@isTest
private class TestE_LogUtil {

	@isTest static void getPageName_Test001() {
		PageReference pageRef = Page.IRIS_Top;
		Test.setCurrentPageReference(pageRef);

		Test.startTest();
		String result = E_LogUtil.getPageName();
		Test.stopTest();

		System.assertEquals('iris_top', result);
	}

	@isTest static void getLogRec_Test001() {
		Test.startTest();
		E_Log__c log = E_LogUtil.getLogRec('hoge', 'hogehoge');
		Test.stopTest();

		System.assertNotEquals(null, log);
	}

	@isTest static void getLogRec_Test002() {
		Test.startTest();
		E_Log__c log = E_LogUtil.getLogRec('hoge', 'hogehoge', false);
		Test.stopTest();

		System.assertNotEquals(null, log);
	}

	@isTest static void getLogRec_Test003() {
		Test.startTest();
		E_Log__c log = E_LogUtil.getLogRec('hoge', 'hogehoge', null, false);
		Test.stopTest();

		System.assertNotEquals(null, log);
	}

	@isTest static void getLogRec_Test004() {
		Test.startTest();
		E_Log__c log = E_LogUtil.getLogRec('hoge', 'hogehoge', '');
		Test.stopTest();

		System.assertNotEquals(null, log);
	}

	@isTest static void createLog_Test001() {
		PageReference pageRef = Page.IRIS_Top;
		Test.setCurrentPageReference(pageRef);

		Test.startTest();
		E_Log__c log = E_LogUtil.createLog();
		Test.stopTest();

		System.assertNotEquals(null, log);
	}

	@isTest static void getLogDetailString_Test001() {
		PageReference pageRef = Page.IRIS_Top;
		Test.setCurrentPageReference(pageRef);

		Test.startTest();
		try{
			List<String> s = null;
			s.size();
		}catch(Exception e){
			String result = E_LogUtil.getLogDetailString(ApexPages.Severity.ERROR, e.getMessage(), e.getStackTraceString());
			System.assertNotEquals(null, result);
		}
		Test.stopTest();

	}

	@isTest static void getLogDetailString_Test002() {
		PageReference pageRef = Page.IRIS_Top;
		Test.setCurrentPageReference(pageRef);

		Test.startTest();
		try{
			List<String> s = null;
			s.size();
		}catch(Exception e){
			E_PageMessagesHolder msgHolder= E_PageMessagesHolder.getInstance();
			msgHolder.addMessage(ApexPages.Severity.ERROR, e.getMessage());
			String result = E_LogUtil.getLogDetailString(msgHolder.getMessages());
			System.assertNotEquals(null, result);
		}
		Test.stopTest();
	}

	@isTest static void requiredLogging_Test001() {
		E_LogUtil.requiredLogging();
	}

}