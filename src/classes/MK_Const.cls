public class MK_Const {
    /* メッセージ一覧 */

    /* 権限セット名*/
    public static final String PERMISSIONSET_MK_Agent = 'MK_PermissionSet_AG';  //キャンペーン権限セット　一般代理店ユーザ
    /* レコードタイプdev名*/
    public static final String RECTYPE_DEV_NAME_BATON = 'BatonRecType';
    /* 新規リード時の選択リスト値 */
    public static final String PICKLIST_VALUE_NEW_LEAD = '新規リード';

    /* エラーメッセージ*/
    //認証エラー
    public static final String ERR_MSG01_IS_NOT_CONFIRM = '登録されている情報と一致しませんでした。入力内容をお確かめください。';
    // 必須未入力時のメッセージ
    public static final String ERR_MSG02_IS_NOT_ENTRY = '未入力の項目が存在します。全ての情報を入力してください。';
    // カナ項目への不整地入力時のメッセージ
    public static final String ERR_MSG03_IS_NOT_KATAKANA = 'カナ項目には、カタカナのみを入力してください。';
    //郵便番号への不整地入力
    public static final String ERR_MSG04_IS_NOT_POSTALCODE = '郵便番号が不正な値です。';
    //電話番号への不整地入力
    public static final String ERR_MSG05_IS_NOT_PHONE = '電話番号が不正な値です。';
    //Email再確認入力が別の値
    public static final String ERR_MSG05_IS_NOT_RECON_EMAIL = 'Emailが再入力と異なります。Emailを確認ください。';
    //Emailが重複
    public static final String ERR_MSG06_IS_NOT_SAME_EMAIL = '同一メールアドレスのご友人は一度しか登録できません。';
    //個人情報同意にチェックなし
    public static final String ERR_MSG07_IS_NOT_AGREE = '内容をご確認のうえ、「はい。上記内容に同意します。」にチェックをお願いします。';
    //紹介者数の上限に達している際のメッセージ
    public static final String ERR_MSG08_MAX_BATON = 'ご友人は{0}人までしか登録できません。';
    // IDパラメータ不正時のメッセージ
    public static final String ERR_MSG09_URLPARAM_ID = 'ページの取得に失敗しました。URLをお確かめ下さい。';
    //禁則文字が含まれる時のメッセージ
    public static final String ERR_MSG10_IS_NO_SEND = '禁則文字が含まれています';
    //リードTMPが更新された場合
    public static final String ERR_MSG11_IS_UPDATE = '更新されています。';
    //リードが作成済み
    public static final String ERR_MSG12_IS_CREATE_LEAD = '登録済みです。';
    //既契約者メール項目エラー
    public static final String ERR_MSG13_IS_NOT_CNTRCTNTMAILADDRESS = 'メールアドレスを入力してください。新規、変更の際は、再確認を入力してください。';
    //開催中のキャンペーン管理がない場合
    public static final String ERR_MSG13_IS_NOT_ACTIVE_CAMPAIGN = '開催期間外です';

}