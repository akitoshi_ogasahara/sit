public without sharing class ASS_E_UnitDaoWithout {
	
	/**
	 * 営業部リスト取得
	 * @param 
	 * @return List<E_Unit__c>: 営業部リスト
	 */
	public static List<E_Unit__c> getUnits() {
		return[SELECT
					Name
				FROM
					E_Unit__c
				WHERE 
					Area__r.AreaCode__c != 'A9' 
				ORDER BY 
					Area__r.SortNo__c,BRANCH__c
		];
	}

	/**
	 * 営業部取得
	 * @param unitId: 営業部ID
	 * @return E_Unit__c: 営業部
	 */
	public static E_Unit__c getUnitById(String unitId) {
		return [SELECT 
					 Name 
				FROM 
					 E_Unit__c
				WHERE 
					 Id = :unitId
				LIMIT 1
				];
	}

	/**
	 * 営業部取得
	 * @param unitName: 営業部名
	 * @return E_Unit__c: 営業部
	 */
	public static E_Unit__c getUnitByName(String unitName) {
		return [SELECT 
					 Name 
				FROM 
					 E_Unit__c 
				WHERE 
					 Name = :unitName 
				LIMIT 1
				];
	}
}