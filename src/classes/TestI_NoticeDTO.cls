@isTest
private class TestI_NoticeDTO {
	/*
	**未入金通知
	*/
	@isTest static void I_NoticeDTO_test01(){
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		E_Policy__c policy = createDataPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
			E_NCOL__c ncol = new E_NCOL__c(
							E_Policy__c = policy.id
							, ParentAccount__c = acc.Parent.Id
							, Account__c = acc.Id			//事務所Id
							, Contact__c = con.Id 			//募集人Id
							, Owname__c = 'テスト契約者'		//契約者名
							, CHDRNUM__c = '12345678'		//証券番号
							, ZCOVRNAM__c = 'テスト保険種類'	//保険種類
							, INSTAMT12__c = 70000			//保険料
							, ZSUMINS12N__c = 1000			//保険金額
							, KBILLDESC__c = '請求区分'		//請求区分
							);
		insert ncol;
		Test.setCreatedDate(ncol.Id, Datetime.now());
			ncol = [SELECT e_policy__r.RecordTypeId
						,e_policy__r.COMM_CHDRNUM__c
						,e_policy__r.MainAgent__c
						,e_policy__r.SubAgent__c
						,e_policy__r.COLI_ZCSHVAL__c
						,e_policy__r.COLI_ZEAPLTOT__c
						,e_policy__r.COLI_ZEPLTOT__c
						,e_policy__r.COLI_ZEADVPRM__c
						,e_policy__r.COLI_ZUNPREM__c
						,e_policy__r.COLI_ZCVDCF__c
						,e_policy__r.COLI_ZUNPCF__c
						,e_policy__r.COLI_ZUNPDCF__c
						,e_policy__r.COMM_OCCDATE_yyyyMMdd__c
						,e_policy__r.PolicyElapsedYears__c
						,IsZGRUP__c
						,ZGRUPNUM__c
						,ParentAccountId__c
						,AccountId__c
						,ParentAccountName__c
						,AccountOwnerName__c
						,AccountName__c
						,ContactName__c
						,Account__c
						,Contact__c
						,Owname__c
						,CHDRNUM__c
						,ZCOVRNAM__c
						,INSTAMT12__c
						,ZSUMINS12N__c
						,KBILLDESC__c
						,createdDate
						,ContractorName__c
						,InsuranceType__c
							FROM E_NCOL__c WHERE Id = :ncol.Id];

		I_NoticeDTO dto= new I_NoticeDTO(ncol);

		system.assertEquals('保険料未入金通知',dto.formType);
		system.assertEquals('テスト契約者',dto.owname);									//契約者名
		system.assertEquals('12345678',dto.no);											//証券番号
		system.assertEquals('テスト保険種類',dto.cntType);									//保険種類
		system.assertEquals(70000,dto.instamt12);										//保険料
		system.assertEquals(1000,dto.zsumins12);										//保険金額
		system.assertEquals(acc.Id,dto.officeId);										//事務所ID
		system.assertEquals(500,dto.zcsvn);

	}

	/*
	**請求通知
	*/

	@isTest static void I_NoticeDTO_test02(){
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		E_Policy__c policy = createDataPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
			E_BILS__c bils = new E_BILS__c(
							E_Policy__c = policy.id
							, ParentAccount__c = acc.Parent.Id
							, Account__c = acc.Id			//事務所Id
							, Contact__c = con.Id 			//募集人Id
							, Owname__c = 'テスト契約者'		//契約者名
							, CHDRNUM__c = '12345678'		//証券番号
							, ZCOVRNAM__c = 'テスト保険種類'	//保険種類
							, INSTAMT12__c = 70000			//保険料
							, ZSUMINS12N__c = 1000			//保険金額
							);
		insert bils;
		Test.setCreatedDate(bils.Id, Datetime.now());
			bils = [SELECT e_policy__r.RecordTypeId
						,e_policy__r.COMM_CHDRNUM__c
						,e_policy__r.MainAgent__c
						,e_policy__r.SubAgent__c
						,e_policy__r.COLI_ZCSHVAL__c
						,e_policy__r.COLI_ZEAPLTOT__c
						,e_policy__r.COLI_ZEPLTOT__c
						,e_policy__r.COLI_ZEADVPRM__c
						,e_policy__r.COLI_ZUNPREM__c
						,e_policy__r.COLI_ZCVDCF__c
						,e_policy__r.COLI_ZUNPCF__c
						,e_policy__r.COLI_ZUNPDCF__c
						,e_policy__r.COMM_OCCDATE_yyyyMMdd__c
						,e_policy__r.PolicyElapsedYears__c
						,IsZGRUP__c
						,ZGRUPNUM__c
						,ParentAccountId__c
						,AccountId__c
						,ParentAccountName__c
						,AccountOwnerName__c
						,AccountName__c
						,ContactName__c
						,Account__c
						,Contact__c
						,Owname__c
						,CHDRNUM__c
						,ZCOVRNAM__c
						,INSTAMT12__c
						,ZSUMINS12N__c
						,REASON03__c
						,REASON04__c
						,createdDate
						,ContractorName__c
						,InsuranceType__c
							FROM E_BILS__c WHERE Id = :bils.Id];

		I_NoticeDTO dto= new I_NoticeDTO(bils);

		system.assertEquals('保険料請求通知',dto.formType);
		system.assertEquals('テスト契約者',dto.owname);									//契約者名
		system.assertEquals('12345678',dto.no);											//証券番号
		system.assertEquals('テスト保険種類',dto.cntType);									//保険種類
		system.assertEquals(70000,dto.instamt12);										//保険料
		system.assertEquals(1000,dto.zsumins12);										//保険金額
		system.assertEquals(acc.Id,dto.officeId);										//事務所ID
		system.assertEquals(500,dto.zcsv12n);

	}

	@isTest static void I_NoticeDTO_test03(){
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人');
		E_Policy__c policy = createDataPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
			E_BILA__c bila = new E_BILa__c(
							E_Policy__c = policy.id
							, ParentAccount__c = acc.Parent.Id
							, Account__c = acc.Id			//事務所Id
							, Contact__c = con.Id 			//募集人Id
							, Owname__c = 'テスト契約者'		//契約者名
							, CHDRNUM__c = '12345678'		//証券番号
							, ZCOVRNAM__c = 'テスト保険種類'	//保険種類
							, INSTAMT12__c = 70000			//保険料
							, ZSUMINS12N__c = 1000			//保険金額
							, ZJINTSEQ__c = '1'
							);
		insert bila;
		Test.setCreatedDate(bila.Id, Datetime.now());
			bila = [SELECT e_policy__r.RecordTypeId
						,e_policy__r.COMM_CHDRNUM__c
						,e_policy__r.MainAgent__c
						,e_policy__r.SubAgent__c
						,e_policy__r.COLI_ZCSHVAL__c
						,e_policy__r.COLI_ZEAPLTOT__c
						,e_policy__r.COLI_ZEPLTOT__c
						,e_policy__r.COLI_ZEADVPRM__c
						,e_policy__r.COLI_ZUNPREM__c
						,e_policy__r.COLI_ZCVDCF__c
						,e_policy__r.COLI_ZUNPCF__c
						,e_policy__r.COLI_ZUNPDCF__c
						,e_policy__r.COMM_OCCDATE_yyyyMMdd__c
						,e_policy__r.PolicyElapsedYears__c
						,IsZGRUP__c
						,ParentAccountId__c
						,AccountId__c
						,ParentAccountName__c
						,AccountOwnerName__c
						,AccountName__c
						,ContactName__c
						,Account__c
						,Contact__c
						,Owname__c
						,CHDRNUM__c
						,ZCOVRNAM__c
						,INSTAMT12__c
						,ZSUMINS12N__c
						,ZJINTSEQ__c
						,createdDate
						,ContractorName__c
						,InsuranceType__c
							FROM E_BILA__c WHERE Id = :bila.Id];
System.debug(bila.AccountName__c);
		I_NoticeDTO dto= new I_NoticeDTO(bila);

		system.assertEquals('保険料請求予告通知',dto.formType);
		system.assertEquals('テスト契約者',dto.owname);									//契約者名
		system.assertEquals('12345678',dto.no);											//証券番号
		system.assertEquals('テスト保険種類',dto.cntType);									//保険種類
		system.assertEquals(70000,dto.instamt12);										//保険料
		system.assertEquals(1000,dto.zsumins12);										//保険金額
		system.assertEquals(acc.Id,dto.officeId);										//事務所ID
		system.assertEquals(500,dto.zcsv12n);
//		system.assertEquals(I_NoticeConst.VIEW_MAIN_SUB_AGENT,dto.officeName);
//		system.assertEquals(I_NoticeConst.VIEW_MAIN_SUB_AGENT,dto.agentName);
		system.assertEquals('テスト募集人',dto.agentName);

	}

	@isTest static void I_NoticeDTO_test04(){
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人');
		List<E_AGPO__c> agpoList = new List<E_AGPO__c>();
		E_Policy__c policyColi = createDataPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_COLI, '33333',500);
		E_Policy__c policySpva = createDataPolicy(true, con.Id, con2.Id, E_Const.POLICY_RECORDTYPE_SPVA, '55555',500);
			E_AGPO__c agpoColi = new E_AGPO__c(
							E_Policy__c = policyColi.id
							, ParentAccount__c = acc.Parent.Id
							, Account__c = acc.Id			//事務所Id
							, Contact__c = con.Id 			//募集人Id
							, Owname__c = 'テスト契約者'		//契約者名
							, CHDRNUM__c = '12345678'		//証券番号
							, ZCOVRNAM__c = 'テスト保険種類'	//保険種類
							, INSTAMT12__c = 70000			//保険料
							, ZSUMINS12N__c = 1000			//保険金額
							, ZJINTSEQ__c = '1'
							, LIMITDT__c = '20170101'
							);
			agpoList.add(agpoColi);
			E_AGPO__c agpoSpva = new E_AGPO__c(
							E_Policy__c = policySpva.id
							, ParentAccount__c = acc.Parent.Id
							, Account__c = acc.Id			//事務所Id
							, Contact__c = con2.Id 			//募集人Id
							, Owname__c = 'テスト契約者'		//契約者名
							, CHDRNUM__c = '87654321'		//証券番号
							, ZCOVRNAM__c = 'テスト保険種類'	//保険種類
							, INSTAMT12__c = 70000			//保険料
							, ZSUMINS12N__c = 1000			//保険金額
							, ZJINTSEQ__c = '2'
							);
			agpoList.add(agpoSpva);
		insert agpoList;
			agpoList = [SELECT e_policy__r.RecordTypeId
						,e_policy__r.RecordType.DeveloperName
						,e_policy__r.COMM_CHDRNUM__c
						,e_policy__r.MainAgent__c
						,e_policy__r.SubAgent__c
						,e_policy__r.COLI_ZCSHVAL__c
						,e_policy__r.COLI_ZEAPLTOT__c
						,e_policy__r.COLI_ZEPLTOT__c
						,e_policy__r.COLI_ZEADVPRM__c
						,e_policy__r.COLI_ZUNPREM__c
						,e_policy__r.COLI_ZCVDCF__c
						,e_policy__r.COLI_ZUNPCF__c
						,e_policy__r.COLI_ZUNPDCF__c
						,e_policy__r.SpClassification__c
						,e_policy__r.COMM_OCCDATE_yyyyMMdd__c
						,e_policy__r.PolicyElapsedYears__c
						,ParentAccountId__c
						,AccountId__c
//						,ParentAccountName__c
						,AccountOwnerName__c
						,AccountName__c
						,ContactName__c
						,Account__c
						,Contact__c
						,Owname__c
						,CHDRNUM__c
						,ZCOVRNAM__c
						,INSTAMT12__c
						,ZSUMINS12N__c
						,CONTENT__c
						,ZJINTSEQ__c
						,LIMITDT__c
						,createdDate
						,ContractorName__c
						,InsuranceType__c
							FROM E_AGPO__c WHERE Id = :agpoColi.Id OR Id = :agpoSpva.Id];
		List<I_NoticeDTO> dtos = new List<I_NoticeDTO>();
		for(E_AGPO__c agpo: agpoList){
System.debug(agpo.AccountName__c);
			Test.setCreatedDate(agpo.Id, Datetime.now());
			I_NoticeDTO dto = new I_NoticeDTO(agpo);
			system.assertEquals('保有契約異動通知',dto.formType);
			system.assertEquals('テスト契約者',dto.owname);										//契約者名
			system.assertEquals('テスト保険種類',dto.cntType);									//保険種類
//			system.assertEquals(I_NoticeConst.VIEW_MAIN_SUB_AGENT,dto.officeName);
			if(agpo.e_policy__r.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI){
				system.assertEquals('12345678', dto.no);
				system.assertEquals(agpo.ZJINTSEQ__c, '1');
//				system.assertEquals(agpo.e_policy__r.MainAgent__c,dto.officeName);
			}else{
				system.assertEquals('87654321', dto.no);
				system.assertEquals(agpo.ZJINTSEQ__c, '2');
//				system.assertEquals(agpo.e_policy__r.SubAgent__c,dto.officeName);
			}
			dtos.add(dto);

		}
		I_NoticeDTO.sortIsAsc = true;
		/* 共通 */
		I_NoticeDTO.sortType = 'transdate';
		dtos.sort();
		I_NoticeDTO.sortType = 'owname';
		dtos.sort();
		I_NoticeDTO.sortType = 'grpno';
		dtos.sort();
		I_NoticeDTO.sortType = 'no';
		dtos.sort();
		I_NoticeDTO.sortType = 'cnttype';
		dtos.sort();
		I_NoticeDTO.sortType = 'instamt12';
		dtos.sort();
		I_NoticeDTO.sortType = 'office';
		dtos.sort();
		I_NoticeDTO.sortType = 'agent';
		dtos.sort();
		I_NoticeDTO.sortType = 'mr';
		dtos.sort();
		I_NoticeDTO.sortType = 'response';
		dtos.sort();
		I_NoticeDTO.sortType = 'elapsed';
		dtos.sort();
		I_NoticeDTO.sortType = 'limitDt';
		dtos.sort();

		/* 未入金 */
		I_NoticeDTO.sortType = 'zcsvn';
		dtos.sort();

		/*請求通知 */
		I_NoticeDTO.sortType = 'zcsv12n';
		dtos.sort();

		/* 異動通知 */
		I_NoticeDTO.sortType = 'content';
		dtos.sort();
	}

	@isTest static void I_NoticeDTO_test05(){
		Account acc = createDataAccount(true);
		Contact con = createDataContct(true, acc.Id,'テスト募集人');
		Contact con2 = createDataContct(true, acc.Id,'テスト募集人');
		Set<Id> poliIdList = new Set<Id>();

		E_Policy__c policyDcoli = createDataPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_DCOLI, '11111',500);
		poliIdList.add(policyDcoli.Id);

		E_Policy__c policySpva = createDataPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_SPVA, '22222',500);
		policySpva.SPVA_ZANNSTFLG__c = TRUE;
		policySpva.SPVA_ZVWDCF__c = '0';
		update policySpva;
		poliIdList.add(policySpva.Id);

		E_Policy__c policydSpva = createDataPolicy(true, con.Id, E_Const.POLICY_RECORDTYPE_DSPVA, '33333',500);
		poliIdList.add(policydSpva.Id);

		List<E_AGPO__c> agpoList = new List<E_AGPO__c>();
		for(Id policyId : poliIdList){
			E_AGPO__c agpoColi = new E_AGPO__c(
				E_Policy__c = policyId
				, ParentAccount__c = acc.Parent.Id
				, Account__c = acc.Id			//事務所Id
				, Contact__c = con.Id 			//募集人Id
				, Owname__c = 'テスト契約者'		//契約者名
				, CHDRNUM__c = '12345678'		//証券番号
				, ZCOVRNAM__c = 'テスト保険種類'	//保険種類
				, INSTAMT12__c = 70000			//保険料
				, ZSUMINS12N__c = 1000			//保険金額
				, ZJINTSEQ__c = '1'
				, LIMITDT__c = '20170101'
			);
			agpoList.add(agpoColi);
		}

		insert agpoList;

		agpoList = [
			SELECT e_policy__r.RecordTypeId
				,e_policy__r.RecordType.DeveloperName
				,e_policy__r.COMM_CHDRNUM__c
				,e_policy__r.MainAgent__c
				,e_policy__r.SubAgent__c
				,e_policy__r.COLI_ZCSHVAL__c
				,e_policy__r.COLI_ZEAPLTOT__c
				,e_policy__r.COLI_ZEPLTOT__c
				,e_policy__r.COLI_ZEADVPRM__c
				,e_policy__r.COLI_ZUNPREM__c
				,e_policy__r.COLI_ZCVDCF__c
				,e_policy__r.COLI_ZUNPCF__c
				,e_policy__r.COLI_ZUNPDCF__c
				,e_policy__r.SpClassification__c
				,e_policy__r.COMM_OCCDATE_yyyyMMdd__c
				,e_policy__r.PolicyElapsedYears__c
				,ParentAccountId__c
				,AccountId__c
//				,ParentAccountName__c
				,AccountOwnerName__c
				,AccountName__c
				,ContactName__c
				,Account__c
				,Contact__c
				,Owname__c
				,CHDRNUM__c
				,ZCOVRNAM__c
				,INSTAMT12__c
				,ZSUMINS12N__c
				,CONTENT__c
				,ZJINTSEQ__c
				,LIMITDT__c
				,createdDate
				,ContractorName__c
				,InsuranceType__c
			FROM E_AGPO__c WHERE Id IN :agpoList];

		for(E_AGPO__c agpo :agpoList){
			I_NoticeDTO dto = new I_NoticeDTO(agpo);
		}
	}

/*******テストデータ**********************************************************/

	/*
	**AccountData作成
	*/
	private static Account createDataAccount(Boolean isInsert){
		/*代理店格*/
		Account pacc= new Account(
			Name = 'テスト代理店格'
			, E_CL1PF_ZHEADAY__c = 'AMS01'
			);
		if(isInsert){
			insert pacc;
		}

		/*代理店事務所*/
		Account acc = new Account(
			Name = 'テスト代理店事務所'
			, ParentId = pacc.Id
			, E_CL2PF_ZAGCYNUM__c = 'AMS11'
			,Owner = TestE_TestUtil.createUser(true,'testMr','E_EmployeeStandard'));
		if(isInsert){
			insert acc;
		}

		return acc;

	}

	/*
	**Contact作成
	*/
	private static Contact createDataContct(Boolean isInsert,Id accId, String lastName){
		/*募集人*/
		Contact con = new Contact(
			LastName = lastName
			, AccountId = accId);

		if(isInsert){
			insert con;
		}
		return con;
	}
	/*
	**保険契約ヘッダ
	*/
	private static E_Policy__c createDataPolicy(Boolean isInsert, Id conId, String recTypeDevName, String cno, Decimal zc) {
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(recTypeDevName);
		E_Policy__c src = new E_Policy__c(
			RecordTypeId = recTypeId
			, COMM_CHDRNUM__c = cno
			, MainAgent__c = conId
			, COLI_ZCSHVAL__c = zc
			, COMM_OCCDATE__c = '20160701'
		);
		if(isInsert){
			insert src;
		}
		return src;
	}

	/*
	**保険契約ヘッダ(共同募集人判定)
	*/
	private static E_Policy__c createDataPolicy(Boolean isInsert, Id conId, Id conId2, String recTypeDevName, String cno, Decimal zc) {
		Id recTypeId = TestE_TestUtil.getRecTypeIdMap().get('E_Policy__c').get(recTypeDevName);
		E_Policy__c src = new E_Policy__c(
			RecordTypeId = recTypeId
			, COMM_CHDRNUM__c = cno
			, MainAgent__c = conId
			, SubAgent__c  = conId2
			, COLI_ZCSHVAL__c = zc
			, COMM_OCCDATE__c = '20160701'
		);
		if(isInsert){
			insert src;
		}
		return src;
	}


}