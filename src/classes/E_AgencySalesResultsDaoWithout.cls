public without sharing class E_AgencySalesResultsDaoWithout {

	//　特定関係法人取得用
	public static List<E_AgencySalesResults__c> getSpRelationCorp(String agency, String bizDate){
		return [
			Select
				BusDate__c
				,AgencyName__c
				, (
					Select
						SpRelationCorpName__c
						, Relation__c
						, AGCO_AGNTREL__c
					From
						E_SpRelationCorps__r
					ORDER BY
						AGCO_AGNTREL__c asc
						, AGCO_CLNTNUM__c asc
				)
			From
				E_AgencySalesResults__c
			Where
				ParentAccountId__c = :agency
				AND Hierarchy__c = 'AH'
				AND BusinessDate__c = :bizDate
			limit 1
		];
	}
	public static E_AgencySalesResults__c getAHRecByAccId(String aid, String bizDate){
		return  [Select AgencyName__c,BusDate__c From E_AgencySalesResults__c Where ParentAccountId__c =: aid AND Hierarchy__c = 'AH' AND BusinessDate__c =: bizDate limit 1];
	}
	/**
	 * 代理店格のSFIDに紐づく代理店挙績情報の募集人を取得する。
	 * 使用箇所
	 *  I_SRAgentController
	 * @param agencyId: 代理店挙積情報ID
	 * @return E_AgencySalesResults__c: 代理店挙積情報
	 */
	public static List<E_AgencySalesResults__c> getSRAgentRecs(String soqlWhere) {
		return getSRAgentRecs(soqlWhere,false);
	}
	public static List<E_AgencySalesResults__c> getSRAgentRecs(String soqlWhere,Boolean isCsv) {
		String soql = ' SELECT '
					+ 'AgentCode__c '
					+ ',XHAT_KMOFCODE__c '
					+ ',AgentNm__c '
					+ ',AgentType__c '
					+ ',BelongOfficeName__c '
					+ ',CnvsLicense_IRIS__c '
					+ ',CnvsStartDate__c '
					+ ',ComStartDate__c '
					+ ',CnvsLicenseVariableIns__c '
					+ ',AGED_KPCDT__c '
					+ ',AGED_KPCDTNum__c '
					+ ',AGED_KPCLC__c '
					+ ',AGED_CAP__c '
					+ ',BusDate__c '
					+ ',ParentAccountId__c '
					+ ',Agentid__c '
					+ ',brCode__c '
					+ 'FROM E_AgencySalesResults__c '
					+ 'WHERE '
					+ soqlWhere
					+ ' ORDER BY '
					+ ' XHAT_AGTYPE__c '
					+ ' ,AccountCode__c '
					+ ' ,ComStartDate__c NULLS LAST ';

		if(!isCsv){
			soql += ' LIMIT 1001 ';
		}else {
			soql += ' LIMIT 10001 ';
		}
		return Database.query(soql);
	}

	/**
	 * 代理店一覧-事務所一覧の挙績情報取得用
	 * @param  accIds [代理店事務所Id]
	 * @return        [代理店挙績情報]
	 */
	public static List<E_AgencySalesResults__c> getSRbyaccId(Set<Id> accIds) {
		String businessDate = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
		String soql = ''
			+' SELECT'
			+'  Id'
			+', ParentAccount__c'
			+', NBCANP_YTD__c'
			+', NBCANP_SpInsTypeYTD__c'
			+', IANP__c'
			+', IQA__c'
			+', NBWANP_TotalYTD__c'
			+', NBWANP_FSYTD__c'
			+', NBWANP_ProtectionYTD__c'
			+', DefactRate__c'
			+', ActiveAgent__c '
			+' FROM E_AgencySalesResults__c '
			+' WHERE ParentAccount__c IN :accIds '
			+'	AND BusinessDate__c =: businessDate '
			+'	AND Hierarchy__c = \'' + E_Const.ZIDTYPE_AH + '\'';

		return Database.query(soql);
	}
	/**
	 * 募集人一覧の挙績情報取得用
	 * @param  conIds [募集人Id]
	 * @return        [代理店挙績情報]
	 */
	public static List<E_AgencySalesResults__c> getSRbyConId(Set<Id> conIds) {
		String businessDate = E_Util.date2YYYYMMDD(E_BizDataSyncLogDao.getRecKindSRNotBlankBatchEndDateByLast1().InquiryDate__c);
		String soql = ''
			+' SELECT'
			+'  Id'
			+', Agent__c'
			+', NBCANP_YTD__c'
			+', NBCANP_SpInsTypeYTD__c'
			+', IANP__c'
			+', IQA__c'
			+', NBWANP_TotalYTD__c'
			+', NBWANP_FSYTD__c'
			+', NBWANP_ProtectionYTD__c'
			+', ActivePolicyNumber__c '
			+' FROM E_AgencySalesResults__c '
			+' WHERE Agent__c IN :conIds '
			+'	AND BusinessDate__c =: businessDate '
			+'	AND Hierarchy__c = \'' + E_Const.ZIDTYPE_AT + '\'';

		return Database.query(soql);
	}
	/**
	 * 指定された代理店格と階層・営業日の代理店挙積情報リストを取得する。
	 * 使用箇所
	 *  I_SRAgencyInfoController
	 * @param Id parentId: 代理店格
	 * @param String hrchy: 階層
	 * @param String bizDate: 営業日
	 * @return List<E_AgencySalesResults__c>: 代理店挙積情報リスト
	 */
	public static List<E_AgencySalesResults__c> getRecordsByParentAccountId(Id parentId, String hrchy, String bizDate) {
		return [SELECT
					Id
					,ParentAccount__c	// 代理店格
					,BusinessDate__c	// 営業日
					,Hierarchy__c		// 階層
					,XHAH_KCHANNEL__c	// チャネル
				 From
					E_AgencySalesResults__c
				 Where
					ParentAccount__c	= :parentId	// 代理店格
				 And
					BusinessDate__c		= :bizDate	// 営業日
				 And
					Hierarchy__c		= :hrchy 	// 階層
				 ORDER BY UpsertKey__c];
	}}