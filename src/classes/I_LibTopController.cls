/**
 * IRISライブラリトップのコントローラクラス.
 */
public with sharing class I_LibTopController extends I_CMSController {
	
	// ページ：ライブラリ配下のIRIS CMSコンテンツ群
	public List<I_ContentMaster__c> sections{get;set;}

	// I_TubeConnectHelper
	private I_TubeConnectHelper connector;

	//特集バナー用コンテンツ群
	public List<I_ContentMaster__c> bannerContentList{get;set;}

	public List<I_CMSComponentController> cmsComps{get;set;}
	
	// コンストラクタ
	public I_LibTopController() {
		try{
			this.sections = new List<I_ContentMaster__c>();
			
			for(I_CMSSection s : CMSSections) {
				this.sections.add(s.record);
			}
			
			this.connector = I_TubeConnectHelper.getInstance();
		}catch(Exception e){
			getPageMessages().addErrorMessage(e.getMessage(), e.getStackTraceString());
		}
	}
	
	/**
	 * I_TubeConnectHelperインスタンスを返す.
	 *
	 * @return I_TubeConnectHelper
	 */
	public I_TubeConnectHelper getTubeConnectHelper() {
		return this.connector;
	}
	
	protected virtual override Id getIrisMenuId(){
		
		// 「ライブラリトップ」のIRISメニュー取得
		I_MenuMaster__c menu = I_MenuMasterDao.getRecByName(I_CONST.IRIS_MENU_NAME_LIB_TOP);

		if(menu != null) {
			String pageId = menu.Pages__r.size() > 0 ? menu.Pages__r.get(0).Id : '';
			ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_PAGE, pageId);
			ApexPages.currentPage().getParameters().put(I_Const.URL_PARAM_MENUID, menu.Id);
		}
		
		return super.getIrisMenuId();
	}

	public List<I_CMSComponentController> getBannerContents(){

		this.bannerContentList = I_ContentMasterDao.getBannerContents();
		Set<Id> pageCompIds = new Set<Id>();

		for(I_ContentMaster__c secRec: this.bannerContentList){
			pageCompIds.add(secRec.Id);
		}

		// コンポーネントの添付ファイルを取得
		Map<ID, List<Attachment>> files = E_AttachmentDao.getRecsByParentIds(pageCompIds);

		//System.debug(files);

		// ChatterFileのIdを取得
		Map<Id,Id> chatterFileIds = I_ContentMasterDao.getChatterFileIdsByRecId(pageCompIds);

		this.cmsComps = new List<I_CMSComponentController>();

		for (I_ContentMaster__c cmpRec: this.bannerContentList) {
			this.cmsComps.add(new I_CMSComponentController(cmpRec, files.get(cmpRec.Id), chatterFileIds.get(cmpRec.Id)));
		}

		return this.cmsComps;
	}

}