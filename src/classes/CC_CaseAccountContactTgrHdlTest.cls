/*
 * CC_CaseAccountContactTgrHdlTest
 * Test class of CC_CaseAccountContactTgrHdl
 * created  : Accenture 2018/5/29
 * modified :
 */

@isTest
private class CC_CaseAccountContactTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();
	private static User testAdminUser = CC_TestDataFactory.createTestAdminUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {

			//Create SRTypeMaster list
			List<CC_SRTypeMaster__c> srTypeMasterList = CC_TestDataFactory.getSRTypeMasterSkelList();
			insert srTypeMasterList;

			//Create Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account newAccount = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			insert newAccount;

			//Create Contact list
			List<Contact> newContactList = new List<Contact>();
			RecordType recordTypeOfContact = [SELECT Id FROM RecordType where SobjectType = 'Contact' and Name = '顧客' limit 1];
			Contact newContact1 = CC_TestDataFactory.getContactSkel();
			newContact1.RecordTypeId = recordTypeOfContact.Id;
			newContact1.AccountId = newAccount.Id;
			newContact1.E_CLTPF_CLTTYPE__c = 'P';
			newContactList.add(newContact1);

			Contact newContact2 = CC_TestDataFactory.getContactSkel();
			newContact2.RecordTypeId = recordTypeOfContact.Id;
			newContact2.AccountId = newAccount.Id;
			newContact2.E_CLTPF_CLTTYPE__c = 'C';
			newContactList.add(newContact2);
			Insert newContactList;

			//Create Case list
			List<Case> newCaseList = new List<Case>();
			Case newCase1 = CC_TestDataFactory.getCaseSkel(srTypeMasterList[0].Id);
			newCase1.CMN_IsClaimed__c = true;
			newCase1.CMN_ContactName__c = newContact1.Id;
			newCaseList.add(newCase1);

			Case newCase2 = CC_TestDataFactory.getCaseSkel(srTypeMasterList[0].Id);
			newCase2.CMN_IsClaimed__c = true;
			newCase2.CMN_CorporateName__c = newAccount.Id;
			newCaseList.add(newCase2);

			Insert newCaseList;

		}
	}

	/**
	* Test Insert
	* 顧客は個人顧客
	* 苦情フラグはTrue
	*/
	static testMethod void caseAccountContactTgrHdlTestInsert01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Contact
			Contact newContact = [select Id from Contact where FirstName = 'test' and E_CLTPF_CLTTYPE__c = 'P'];

			//Get SRTypeMaster list
			List<CC_SRTypeMaster__c> srTypeMasterList = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c like 'testSRTypeMaster%'];

			//Create Caselist
			Case newCase = CC_TestDataFactory.getCaseSkel(srTypeMasterList[0].Id);
			newCase.CMN_FreeSpace1__c = 'testCase02';
			newCase.CMN_ContactName__c = newContact.Id;
			newCase.CMN_IsClaimed__c = true;

			Test.startTest();
			Insert newCase;
			Test.stopTest();

		}
	}

	/**
	* Test Insert
	* 顧客は個人顧客
	* 苦情フラグはFlase
	*/
	static testMethod void caseAccountContactTgrHdlTestInsert02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Contact
			Contact newContact = [select Id from Contact where FirstName = 'test' and E_CLTPF_CLTTYPE__c = 'P'];

			//Get SRTypeMaster list
			List<CC_SRTypeMaster__c> srTypeMasterList = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c like 'testSRTypeMaster%'];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(srTypeMasterList[0].Id);
			newCase.CMN_ContactName__c = newContact.Id;
			newCase.CMN_IsClaimed__c = false;

			Test.startTest();
			Insert newCase;
			Test.stopTest();

		}
	}

	/**
	* Test Update
	* 顧客は法人顧客
	* 苦情フラグはTrue
	*/
	static testMethod void caseAccountContactTgrHdlTestUpdate01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Account
			Account newAccount = [select Id from Account where Name = 'testAccount'];

			//Get Contact
			Contact newContact = [select Id from Contact where FirstName = 'test' and E_CLTPF_CLTTYPE__c = 'C'];

			//Get SRTypeMaster list
			List<CC_SRTypeMaster__c> srTypeMasterList = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c like 'testSRTypeMaster%'];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(srTypeMasterList[0].Id);
			newCase.CMN_CorporateName__c = newAccount.Id;
			newCase.CMN_IsClaimed__c = true;
			Insert newCase;

			Test.startTest();
			newCase.CC_SRTypeId__c = srTypeMasterList[1].Id;
			Update newCase;
			Test.stopTest();

		}
	}

	/**
	* Test Update
	* 顧客は法人顧客
	* 苦情フラグはFalse
	*/
	static testMethod void caseAccountContactTgrHdlTestUpdate02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Get Account
			Account newAccount = [select Id from Account where Name = 'testAccount'];

			//Get Contact
			Contact newContact = [select Id from Contact where FirstName = 'test' and E_CLTPF_CLTTYPE__c = 'C'];

			//Get SRTypeMaster list
			List<CC_SRTypeMaster__c> srTypeMasterList = [select Id from CC_SRTypeMaster__c where CC_SRTypeName__c like 'testSRTypeMaster%'];

			//Create Case
			Case newCase = CC_TestDataFactory.getCaseSkel(srTypeMasterList[0].Id);
			newCase.CMN_CorporateName__c = newAccount.Id;
			newCase.CMN_IsClaimed__c = false;
			Insert newCase;

			Test.startTest();
			newCase.CC_SRTypeId__c = srTypeMasterList[1].Id;
			Update newCase;
			Test.stopTest();

		}
	}

	/**
	* Test Delete
	* 顧客は個人顧客(顧客の苦情フラグはTrue)
	*/
	static testMethod void caseAccountContactTgrHdlTestDelete01() {
		System.runAs ( testAdminUser ) {

			//Prepare test data
			//Get Contact
			Contact newContact = [select Id from Contact where FirstName = 'test' and E_CLTPF_CLTTYPE__c = 'P'];
			//Get Case
			Case newCase = [select Id from Case where CMN_ContactName__c =: newContact.Id];

			Test.startTest();
			Delete newCase;
			Test.stopTest();

		}
	}

	/**
	* Test Delete
	* 顧客は法人顧客(顧客の苦情フラグはTrue)
	*/
	static testMethod void caseAccountContactTgrHdlTestDelete02() {
		System.runAs ( testAdminUser ) {

			//Prepare test data
			//Get Account
			Account newAccount = [select Id from Account where Name = 'testAccount'];

			//Get Case
			Case newCase = [select Id from Case where CMN_CorporateName__c =: newAccount.Id];

			Test.startTest();
			Delete newCase;
			Test.stopTest();

		}
	}

}