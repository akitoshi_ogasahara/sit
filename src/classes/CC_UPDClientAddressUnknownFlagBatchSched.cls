global class CC_UPDClientAddressUnknownFlagBatchSched implements Schedulable {
	global void execute(SchedulableContext sc){
		Database.executeBatch(new CC_UPDClientAddressUnknownFlagBatch(), 200);
	}

}