@isTest
private class TestE_LoginController {

	private final static String username = 'shoyusya';
	private final static String tempBasicPWModifiedDate = '20150407';
	
	//ページアクション
	static testMethod void pageActionTest01(){
		E_LoginController controller = new E_LoginController();

		Test.startTest();
		PageReference result = controller.pageAction();
		Test.stopTest();

		//System.assertEquals(null, result);
		System.assert(result.getUrl().contains('/e_errorpage?code'));			//Menuマスタが取得できないエラーとなる
	}

	//[ログイン]ボタン
	//暗号化なしでログイン成功
	static testMethod void doLoginTest01(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			E_LoginController controller = new E_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(null, result);
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//初回ログインでログイン成功
	static testMethod void doLoginTest02(){

		//ログインユーザ作成
		String password = '1234567801';
		User user01 = createUser(true, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encryptInitPass(password, user01.E_TempBasicPWModifiedDate__c));

			E_LoginController controller = new E_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(null, result);
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//既存ユーザでログイン成功
	static testMethod void doLoginTest03(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, true);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encrypt(password));

			E_LoginController controller = new E_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(null, result);
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//存在しないユーザID
	static testMethod void doLoginTest04(){

		//メッセージマスタ作成
		E_MessageMaster__c LGE001 = TestE_TestUtil.createMessageMaster(false, 'ログイン画面_ユーザID不正', null, 'MSG_DISCLAIMER', E_Const.E_MEGTYPE_MEG, 'LGE|001');
		LGE001.Value__c = 'ログイン画面_ユーザID不正';
		insert LGE001;

		//実行
		E_LoginController controller = new E_LoginController();
		controller.userId = '';

		Test.startTest();
		PageReference result = controller.doLogin();
		Test.stopTest();

		System.assertEquals(null, result);
		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(LGE001.Value__c, controller.pageMessages.getErrorMessages()[0].summary);
	}

	//[ログイン]ボタン
	//パスワード誤り
	static testMethod void doLoginTest05(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, false);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			E_LoginController controller = new E_LoginController();
			controller.userId = username;
			controller.password = 'hogehoge2';
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'summary'));
			
			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(null, result);
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}
	
	//[ログイン]ボタン
	//エラー発生
	static testMethod void doLoginTest06(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(true, false);
		user01.E_TempBasicPWModifiedDate__c = null;
		update user01;

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, password);

			E_LoginController controller = new E_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(null, result);
			System.assert(controller.pageMessages.getHasErrorMessages());
		}
	}

	//[ログイン]ボタン
	//既存ユーザでログイン成功 startUul有り
	static testMethod void doLoginTest07(){

		//ログインユーザ作成
		String password = 'Hogehoge1';
		User user01 = createUser(false, true);

		//実行
		System.runAs(user01){
			System.setPassword(user01.Id, E_Const.PREFIX_TEMP_PASSWORD + E_EncryptUtil.encrypt(password));

			PageReference pageRef = Page.E_LoginCustomer;
			pageRef.getParameters().put('startUrl','hogehoge');
			Test.setCurrentPage(pageRef);

			E_LoginController controller = new E_LoginController();
			controller.userId = username;
			controller.password = password;

			Test.startTest();
			PageReference result = controller.doLogin();
			Test.stopTest();

			//Site.loginはtestmethod内では何もしない
			System.assertEquals(null, result);
			System.assert(!controller.pageMessages.getHasErrorMessages());
		}
	}

	//[パスワードをお忘れの方はこちら]ボタン
	static testMethod void doReissueTest01(){

		E_LoginController controller = new E_LoginController();

		Test.startTest();
		PageReference result = controller.doReissue();
		Test.stopTest();

		System.assertEquals(Page.E_ReissuePassword.getUrl(), result.getUrl());
	}

	//ログインユーザ作成
	static User createUser(Boolean useTempBasicPW, Boolean useExistingBasicPW){
		UserRole uRole = [select Id from UserRole where Name = '管理者' limit 1];
		User ownerUser = TestE_TestUtil.createUser(false, username, E_Const.SYSTEM_ADMINISTRATOR);
		ownerUser.userroleid = uRole.Id;
		ownerUser.username =  username +  System.Label.E_USERNAME_SUFFIX;
		ownerUser.E_UseTempBasicPW__c = useTempBasicPW;
		ownerUser.E_UseExistingBasicPW__c = useExistingBasicPW;
		ownerUser.E_TempBasicPWModifiedDate__c = tempBasicPWModifiedDate;
		insert ownerUser;
		return ownerUser;
	}

}