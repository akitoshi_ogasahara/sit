/**
@version 1.0
@author PwC
LMT_LeadUpdateTriggerHandlerテストクラス
*/

@isTest
private class TestLMT_EmailAddressManagement {
     @isTest
    private static void testMethodFor() {

        Test.startTest();

        List<User> partnerUserList = [SELECT Id
                                        FROM User
                                        WHERE UserType = 'PowerPartner'
                                        AND IsActive = true
                                        LIMIT 10];

        List<User> NNuserList = [SELECT Id
                                        FROM User
                                        WHERE UserType = 'Standard'
                                        AND IsActive = true
                                        LIMIT 10];

        List<LMT_EmailAddressManagement__c> testDataList = new List<LMT_EmailAddressManagement__c>();
        LMT_EmailAddressManagement__c emailAMold01 = new LMT_EmailAddressManagement__c();
        emailAMold01.User1__c = partnerUserList[0].Id;
        emailAMold01.User2__c = partnerUserList[1].Id;
        emailAMold01.User3__c = partnerUserList[2].Id;
        emailAMold01.User4__c = NNuserList[0].Id;
        emailAMold01.User5__c = NNuserList[1].Id;
        emailAMold01.User6__c = NNuserList[2].Id;
        testDataList.add(emailAMold01);

        LMT_EmailAddressManagement__c emailAMold02 = new LMT_EmailAddressManagement__c();
        emailAMold02.User1__c = partnerUserList[3].Id;
        emailAMold02.User2__c = partnerUserList[4].Id;
        emailAMold02.User3__c = partnerUserList[5].Id;
        emailAMold02.User4__c = NNuserList[3].Id;
        emailAMold02.User5__c = NNuserList[4].Id;
        emailAMold02.User6__c = NNuserList[5].Id;
        testDataList.add(emailAMold02);

        Insert testDataList;

        try{
            LMT_EmailAddressManagement__c emailAMnew = new LMT_EmailAddressManagement__c();
            emailAMnew.User1__c = partnerUserList[0].Id;
            Insert emailAMnew;
        }catch(Exception e){

            System.assert(e.getMessage().contains(Label.Msg_Error_LMT_DuplicateUser1));
        }

        try{
            emailAMold02.User1__c = partnerUserList[0].Id;
            update emailAMold02;
        }catch(Exception e){
            System.assert(e.getMessage().contains(Label.Msg_Error_LMT_DuplicateUser1));
        }

        Test.stopTest();

    }
}