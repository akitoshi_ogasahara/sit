@isTest(seealldata = false)
public with sharing class TestE_ProgressSearchAgentExtender {

	private static testMethod void Testagent1(){
		//テストユーザ作成
        User u = createUser(true, 'testUser', 'システム管理者');
        system.debug('@@@u=' + u.id);
        
        //Agentユーザ作成
        User portalOwner = createportalAccountOwner(true, 'agentUser', 'システム管理者');
		
		PageReference pref = Page.E_ProgressSearchAgentSVE;
        pref.getHeaders().put('X-Salesforce-SIP', '122.208.71.147');
        Test.setCurrentPage(pref);
        
        
        //テストユーザでテスト開始
        Test.startTest();
        Account acc;
        Contact con;
        User AgUser;
        E_CPCPF__c cpcpf;
        System.runAs(u){
	        acc = createAcc(true, 'dummyAccount',portalOwner.id);
	        con = CreateContact(true, acc.Id);
	        AgUser = createAgentUser(true, 'TestContact', 'Partner Community User',con.id);
			system.debug('@@@Agent=' + AgUser.id);    
        }
        TestE_TestUtil.createPermissionSetAssignmentBase(true, AgUser.id);
        SET<String> psName = new SET<String>{'E_PermissionSet_Content','E_PermissionSet_Commision','E_PermissionSet_ProceduralHistory','E_PermissionSet_JPLife'};
        LIST<PermissionSet> ps = [Select Id From PermissionSet Where Name in :psName];
        LIST<PermissionSetAssignment> insList = new  LIST<PermissionSetAssignment>();
        for(permissionset item:ps){
            PermissionSetAssignment src = new PermissionSetAssignment(
                PermissionSetId = item.Id
                ,AssigneeId =  AgUser.id
            );          
            insList.add(src);
        }
        insert insList;        

        system.runas(AgUser){
            cpcpf = new E_CPCPF__c();
            cpcpf.Agent__c = con.id;
            insert cpcpf;
        	E_ProgressSearchAgentController controller = new E_ProgressSearchAgentController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAgentExtender extender = new E_ProgressSearchAgentExtender(controller);
            extender.init();
            extender.pageAction();
            extender.myRecordSearch();
            
            system.debug('@@@=' + E_UserDao.getUserRecByUserId(AgUser.id).Contact.Name);
            
			system.debug('@@@募集人0= ' + AgUser.Contact.Name);
			system.debug('@@@募集人1= ' + controller.iAgent_val.SkyEditor2__Text__c);
			
			//system.assertEquals(AgUser.Contact.Name,controller.iAgent_val.SkyEditor2__Text__c);
        	
        //テスト終了
        Test.stopTest();
        }
		
	}
	
	private static testMethod void Testagent2(){
		//テストユーザ作成
        User u = createUser(true, 'keiyakusha', 'システム管理者');
        system.debug('@@@u=' + u.id);
		//募集人ユーザを作成
		Account acc = createAccount(true, 'テスト代理店事務所');
		Contact Agent = createAgent(true, 'testAgent', acc.id );
		User AgUser =  createAgentUser(true, 'testAgent' , '契約部', null);
		
		PageReference pref = Page.E_ProgressSearchAgentSVE;
        
        Test.setCurrentPage(pref);
        
        E_CPCPF__c cpcpf = new E_CPCPF__c();
        
        //テストユーザでテスト開始
        Test.startTest();
        System.runAs(u){
        	
        	E_ProgressSearchAgentController controller = new E_ProgressSearchAgentController(new ApexPages.StandardController(cpcpf));
            E_ProgressSearchAgentExtender extender = new E_ProgressSearchAgentExtender(controller);
            
            extender.init();
            extender.pageAction();
            extender.myRecordSearch();
            
			system.debug('@@@募集人2= ' + controller.iAgent_val.SkyEditor2__Text__c);
			system.debug('@@@募集人3= ' + AgUser.Contact.Name);
			
			system.assertEquals(null,controller.iAgent_val.SkyEditor2__Text__c);
        	
        //テスト終了
        Test.stopTest();
		}
		
	}


/*********************************************************************************************************************
 * テストデータ作成用
 *********************************************************************************************************************/
    /**
     * テストユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createUser(Boolean isInsert, String LastName, String profileDevName){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    private static Map<String, Id> pMap;
    private static Map<String, Id> getProfileIdMap(){
        if(pMap != null){
            return pMap;
        }
        pMap = new Map<String, Id>();
        for(Profile pr: [select Id, Name From Profile]){
            pMap.put(pr.Name, pr.Id);
        }
        return pMap;
    }
    
    /**
     * 代理店事務所作成
     * @param isInsert: whether to insert
     * @param AccName: 取引先名
     * @return account: account
     */
    private static Account createAccount(Boolean isInsert, String AccName ){
        Account src = new Account(
                  name = AccName
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    
    /**
     * 募集人作成
     * @param isInsert: whether to insert
     * @param LastName: 募集人姓
     * @param AccId: 取引先Id
     * @return Contact: Contact
     */
    private static Contact createAgent(Boolean isInsert, String LastName, Id AccId ){
        Contact src = new Contact(
                  Lastname = LastName
                 ,AccountId = AccId
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    
     /**
     * パートナーユーザ作成
     * @param isInsert: whether to insert
     * @param LastName: 姓
     * @param profileDevName: プロファイル名
     * @return User: ユーザ
     */
    private static User createportalAccountOwner(Boolean isInsert, String LastName, String profileDevName){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('@@@USERTYPE= ' + portalRole.id);
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User portalAccountOwner = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , UserRoleId = portalRole.Id
        );
        if (isInsert) {
            insert portalAccountOwner;
        }
        return portalAccountOwner;
    }
    
     /**
     * パートナー取引先作成
     * @param isInsert: whether to insert
     * @param accName: 取引先名
     * @return Account: 取引先
     */
    private static Account createAcc(Boolean isInsert, String accName, Id portalAccountOwnerid){
        Account src = new Account(
                  name = accName
                 ,OwnerId = portalAccountOwnerid
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
   
    /**
     * Contact作成
     * @param isInsert: whether to insert
     * @param accId: 取引先Id 
     * @return Contact: 取引先責任者
     */
    private static Contact CreateContact(Boolean isInsert, Id accId){
        Contact src = new Contact(
                  LastName = 'TestContact'
                 ,AccountId = accId
                  
        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    
    /**
     * Agentユーザ作成
     * @param isInsert: whether to insert
     * @param conId: 取引先責任者Id 
     * @return User: User
     */
    private static User createAgentUser(Boolean isInsert, String LastName, String profileDevName, Id conId){
        String userName = LastName + '@terrasky.ingtesting';
        Id profileId = getProfileIdMap().get(profileDevName);
        User src = new User(
                  Lastname = LastName
                , Username = userName
                , Email = userName
                , ProfileId = profileId
                , Alias = LastName.left(8)
                , TimeZoneSidKey = UserInfo.getTimeZone().getID()
                , LocaleSidKey = UserInfo.getLocale()
                , EmailEncodingKey = 'UTF-8'
                , LanguageLocaleKey = UserInfo.getLanguage()
                , ContactId = conId

        );
        if (isInsert) {
            insert src;
        }
        return src;
    }
    
}