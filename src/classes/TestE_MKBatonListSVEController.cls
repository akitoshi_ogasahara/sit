@isTest
private with sharing class TestE_MKBatonListSVEController{
	private static testMethod void testPageMethods() {		E_MKBatonListSVEController extension = new E_MKBatonListSVEController(new ApexPages.StandardController(new Contact()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testfriendListTab() {
		E_MKBatonListSVEController.friendListTab friendListTab = new E_MKBatonListSVEController.friendListTab(new List<Lead>(), new List<E_MKBatonListSVEController.friendListTabItem>(), new List<Lead>(), null);
		friendListTab.create(new Lead());
		System.assert(true);
	}
	
	private static testMethod void testtargetListTab() {
		E_MKBatonListSVEController.targetListTab targetListTab = new E_MKBatonListSVEController.targetListTab(new List<MK_CampaignTarget__c>(), new List<E_MKBatonListSVEController.targetListTabItem>(), new List<MK_CampaignTarget__c>(), null);
		targetListTab.create(new MK_CampaignTarget__c());
		System.assert(true);
	}
	
}