/**
 * I_KeywordMarker
 *
 * 複数のキーワードを保持し、保持したキーワードから<mark/>付けを行うのに用いる
 */
public with sharing class I_KeywordMarker {

	/** 生成したパターンインスタンスを保持する */
	private static Map<String, Pattern> patternRegistory = new Map<String, Pattern>();

	/** キーワードのリスト */
	private List<String> keywords;

	/**
	 * コンストラクタ
	 */
	public I_KeywordMarker() {
		this(null);
	}

	/**
	 * コンストラクタ
	 * @param List<String> keywords
	 */
	public I_KeywordMarker(List<String> keywords) {
		if (keywords == null) {
			keywords = new List<String>();
		}
		this.keywords = keywords.clone();
	}

	/**
	 * キーワードを追加する
	 * @param String keyword
	 */
	public void addKeyword(String keyword) {
		this.keywords.add(keyword);
	}

	/**
	 * textsを用いて保持しているkeywordsがすべて一致した場合のみTRUE
	 * @param List<String> texts
	 * @return Boolean
	 */
	public Boolean wholeMatches(List<String> texts) {
		Boolean isWholeMatched;
		for (String keyword: this.keywords) {
			Pattern p = I_KeywordMarker.getPattern('(' + keyword + ')');
			if (isWholeMatched == null) {
				isWholeMatched = true;
			}
			Boolean isMatched = false;
			for (String text: texts) {
				if (String.isNotBlank(text) && p.matcher(text).find()) {
					isMatched = true;
					break;
				}
			}
			isWholeMatched = isWholeMatched && isMatched;
		}
		if (isWholeMatched == null) {
			isWholeMatched = false;
		}
		return isWholeMatched;
	}

	/**
	 * keywordsと一致した部分に<mark/>をつける
	 * @param String text
	 */
 	public String mark(String text) {
		return mark(text, -1);
	}

	/**
	 * widthで切り取ってkeywordsと一致した部分に<mark/>をつける
	 * @see String#abbreviate
	 * @param String text
	 * @param width Integer - マイナスを指定すると切り取りを行わない。
	 */
	public static final String ELLIPSIS = '...';
	public String mark(String text, Integer width) {
		Pattern p = getPattern('(' + this.toString() + ')');
		String result;
		if (text != null) {
			Matcher m = p.matcher(text);
			if (m.find()) {
				Integer startIndex = m.start();
				Integer endIndex = m.end();
				Integer matchedSize = endIndex - startIndex;
				Integer extractSize = width;
				if (extractSize >= 0) {
					Integer offset = extractSize < matchedSize ? startIndex : startIndex - Integer.valueOf(Math.ceil((extractSize - matchedSize)/2));
					if (offset < 0) {
						offset = 0;
						extractSize += ELLIPSIS.length();
					} else if (extractSize >= matchedSize && text.length() - offset < extractSize) {
						extractSize += ELLIPSIS.length();
					} else {
						extractSize += ELLIPSIS.length() * 2;
					}
					text = text.abbreviate(extractSize, offset);
				}
				result = width >= 0 && width < matchedSize ? '<mark>' + text + '</mark>' : p.matcher(text).replaceAll('<mark>$1</mark>');
			} else {
				result = width >= 0 ? text.abbreviate(width + ELLIPSIS.length()) : text;
			}
		}
		return result;
	}

	/**
	 * 現在保持しているキーワードを連結して出力する
	 * @return String
	 */
	public override String toString() {
		return String.join(this.keywords, '|');
	}

	/**
	 * Patternインスタンスを取得する。
	 * @param String keyword
	 * @return Pattern
	 */
	public static Pattern getPattern(String keyword) {
		keyword = String.isNotBlank(keyword) ? keyword : '';
		if (!patternRegistory.containsKey(keyword)) {
			patternRegistory.put(keyword, Pattern.compile(keyword));
		}
		return patternRegistory.get(keyword);
	}

	/**
	 * 受け取ったキーワードから、あいまい検索用の正規表現リテラルを作成する。
	 * @param String keyword
	 * @return String
	 */
	private static String REGEX_SPECIAL_KEYWORDS = '\\*+.?{}()[]^$-|';
	private static String SIGN_CHAR_EN = '!#$%&()*+,-./:;<=>?@[]^_{|}~｡｢｣､` "･\'\\';
	private static String SIGN_CHAR_EM = '！＃＄％＆（）＊＋，－．／：；＜＝＞？＠［］＾＿｛｜｝～。「」、｀　”・’￥';
	public static String createFuzzyPattern(String keyword) {
		String fuzzyPatternLiteral = '';
		Pattern mAtoZ = getPattern('[ａ-ｚＡ-Ｚ]');
		Pattern nAtoZ = getPattern('[a-zA-Z]');
		Pattern m0to9 = getPattern('[０-９]');
		Pattern n0to9 = getPattern('[0-9]');
		// 半角カナの濁点半濁点がつらいので全角に変えておく
		keyword = String.isNotBlank(keyword) ? E_Util.enKatakanaToEm(keyword) : '';
		for (Integer i = 0; i < keyword.length(); i++) {
			String character = keyword.substring(i, i + 1);
			Set<String> candidatesSet = new Set<String>();
			candidatesSet.add(character);
			//全角カナ
			if (E_Util.isEmKatakana(character) || character == 'ー') {
				candidatesSet.add(E_Util.emKatakanaToEn(character));
			}
			//半角カナ
			/*if (E_Util.isEnKatakana(character)) candidatesSet.add(E_Util.enKatakanaToEm(character));*/
			//全角英
			if (mAtoZ.matcher(character).matches()) {
				String en = E_Util.emAlphabetToEn(character);
				candidatesSet.add(en.toUpperCase());
				candidatesSet.add(en.toLowerCase());
				// 全角大文字
				candidatesSet.add(E_Util.enAlphabetToEm(en.toUpperCase()));
				// 全角小文字
				candidatesSet.add(E_Util.enAlphabetToEm(en.toLowerCase()));
			}
			//半角英
			if (nAtoZ.matcher(character).matches()) {
				candidatesSet.add(character.toUpperCase());
				candidatesSet.add(character.toLowerCase());
				// 全角大文字
				candidatesSet.add(E_Util.enAlphabetToEm(character.toUpperCase()));
				// 全角小文字
				candidatesSet.add(E_Util.enAlphabetToEm(character.toLowerCase()));
			}
			//全角数
			if (m0to9.matcher(character).matches()) candidatesSet.add(E_Util.emNumberToEn(character));
			//半角数
			if (n0to9.matcher(character).matches()) candidatesSet.add(E_Util.enNumberToEm(character));
			// 全角記号
			if (SIGN_CHAR_EM.indexOf(character) >= 0) candidatesSet.add(E_Util.emSignToEn(character));
			// 半角記号
			if (SIGN_CHAR_EN.indexOf(character) >= 0) candidatesSet.add(E_Util.enSignToEm(character));

			List<String> candidates = new List<String>(candidatesSet);
			candidates.sort();
			escapeSpecialChar(candidates);
			if (candidates.size() > 0) {
				fuzzyPatternLiteral += candidates.size() > 1 ? '(?:'+ String.join(candidates, '|') + ')' : candidates.get(0);
			}
		}
		return fuzzyPatternLiteral;
	}

	private static void escapeSpecialChar(List<String> characters) {
		for (Integer i = 0; i < characters.size(); i++) {
			String character = characters.get(i);
			if (REGEX_SPECIAL_KEYWORDS.indexOf(character) >= 0) {
				characters.set(i, '\\' + character);
			}
		}
	}
}