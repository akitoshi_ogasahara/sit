@isTest
private class TestE_SoqlUtil {

	static testMethod void test_getAfterWhereClause(){
		String soql = 'select id,name from Account';
		//WHERE句がない場合、空文字が返る
		System.assertEquals(E_SoqlUtil.getAfterWhereClause(soql, 'Account'), '');
		
		//WHERE句を取得
		string swhere = ' where name = \'Terrasky\'';
		soql += sWhere;
		System.assertEquals(E_SoqlUtil.getAfterWhereClause(soql, 'Account'), sWhere.trim());

		//Order By Limit句付
		String sOrderby = ' order by name limit 100';
		soql += sOrderBy;
		System.assertEquals(E_SoqlUtil.getAfterWhereClause(soql, 'Account'), (sWhere+sOrderBy).trim());

		//Order Byのみ　Whereなし
		soql = 'select id,name From Account';
		sOrderby = ' order by name limit 100';
		soql += sOrderBy;
		System.assertEquals(E_SoqlUtil.getAfterWhereClause(soql, 'Account'), sOrderBy.trim());

		
		//サブクエリがある場合
		soql = 'select id,name,(select id,name FROM contacts) FROM account';
		System.assertEquals(E_SoqlUtil.getAfterWhereClause(soql, 'Account'), '');
		
		swhere = ' where name = \'Terrasky\'';
		soql += sWhere;
		System.assertEquals(E_SoqlUtil.getAfterWhereClause(soql, 'Account'), swhere.trim());
	}

	static testMethod void test_trimLimitClause(){
		String base = 'name = \'Terrasky\' order by name';
		
		//Limit句がないのでそのまま
		System.assertEquals(E_SoqlUtil.trimLimitClause(base), base);
		
		//Limit句を追加
		//name = \'Terrasky\' order by name  limit 200;
		System.assertEquals(E_SoqlUtil.trimLimitClause(base + ' limit 200'), base);
		
		//Limitが２つ含まれているときは後ろのLimit句のみ除去
		base = 'select id,limit__c, (select id from limitmax__r) from sobject';
		System.assertEquals(E_SoqlUtil.trimLimitClause(base + ' limit 500'), base);
	}


}