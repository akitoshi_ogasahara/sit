public with sharing class CR_CommentController{

	public CR_CommentController(){
		canModifyComment = false;
	}

	public CR_Agency__c record{get;set;}

	public PageReference modifyComment(){
		canModifyComment = true;
		return null;
	}
	
	private Boolean canModifyComment;
	public Boolean getCanModifyComment(){
		return canModifyComment;
	}
	
	public String getMsgForMRNotExists(){
		E_MessageMaster__c msg = E_MessageMasterDao.getRecByCode('CR|002');
		return msg==null?'#担当MR未設定':msg.value__c;
	}

	public PageReference doSaveEx(){
		String s = '';
		if(!canModifyComment && String.isNotBlank(record.addComment__c)){
			//  複数人の同時更新を考慮してここでComment項目を再取得してUpdateする
			//		  コメント編集の場合は、そのまま編集可能とする。
			CR_Agency__c agny = CR_AgencyDao.getRefreshedCommentById(record.Id);
			if(agny.Comment__c != record.Comment__c){
				record.Comment__c = agny.Comment__c;
			}

			if(String.isBlank(record.Comment__c)){
				record.Comment__c = '';
			}else{
			}

			s += '-----';
			s += System.now().format();
			s += '  ';
			s += UserInfo.getName();
			s += '-----';
			s += '\n'; 
			s += record.addComment__c;
			s += '\n\n';
			
			record.Comment__c = s + record.Comment__c;
		}
		
		record.addComment__c = null;
		update record;
		canModifyComment = false;
		return null;
	}

}