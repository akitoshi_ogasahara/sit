global with sharing class E_InsuredInfoExtender extends E_AbstractViewExtender {

    public E_InsuredInfoController extension;
    public String contractorName {get; set;}
    public String address {get; set;}
    public String birthDate {get; set;}
    public String sex  {get; set;}
    public String cno {get; set;}
    public String cname {get; set;}
    //public Contact contarct;
    public E_Policy__c policy;

    public String cId{get;set;}
    public String disId{get;set;}
    public String offId{get;set;}
    public String ageId{get;set;}

    //ページタイトル
    private static final String PAGE_TITLE = '被保険者照会';


    /** 初期処理  */
    public E_InsuredInfoExtender(E_InsuredInfoController extension){
        super();
        this.extension  = extension;
        this.pgTitle = PAGE_TITLE;
        
        this.cid = ApexPages.currentPage().getParameters().get('cId');
        disId = ApexPages.currentPage().getParameters().get('disId');
        offId = ApexPages.currentPage().getParameters().get('offId');
        ageId = ApexPages.currentPage().getParameters().get('ageId');
        if(String.isNotBlank(this.cId)){        //cidが取得できない場合はdoAuthでエラーとなる。
            SkyEditor2.Query insuredListQuery = extension.queryMap.get('InsuredList');
            insuredListQuery.addWhereIfNotFirst('AND');
            insuredListQuery.addWhere('Insured__c = \'' + cId + '\'' );
            if (getIsEmployee()) {
                if(String.isNotBlank(this.disId)){
                    insuredListQuery.addWhereIfNotFirst('AND');
                    insuredListQuery.addWhere('( MainAgentAccount__r.ParentId = \'' + disId + '\' OR SubAgentAccount__r.ParentId = \'' + disId + '\' )' ); 
                }
                if(String.isNotBlank(this.offId)){
                    insuredListQuery.addWhereIfNotFirst('AND');
                    insuredListQuery.addWhere('( MainAgentAccount__c = \'' + offId + '\' OR SubAgentAccount__c = \'' + offId + '\' )' ); 
                }
                if(String.isNotBlank(this.ageId)){
                    insuredListQuery.addWhereIfNotFirst('AND');
                    insuredListQuery.addWhere('( MainAgent__c = \'' + ageId + '\' OR SubAgent__c = \'' + ageId + '\' )' ); 
                }
            }
        }

    }

    /** 初期処理  */
    public override void init(){

        //this.id     = ApexPages.CurrentPage().getParameters().get('cid');
        pageRef      = doAuth(E_Const.ID_KIND.CONTACT, this.cid);
        this.cName  = ApexPages.CurrentPage().getParameters().get('cName');
        this.policy     = this.extension.InsuredList.Items.isEmpty() ? null : getPolicyWithInsuredInfo();
        // 照会処理を行う
        if(pageRef == null){
            this.contractorName = this.policy.Insured__r.Name;
            this.address        = this.policy.Insured__r.E_COMM_ZCLADDR__c;
            this.sex            = this.policy.Insured__r.E_CLTPF_ZKNJSEX__c;
            this.birthDate      = this.policy.Insured__r.E_CLTPF_DOB__c;
            Set<Id> ids         = new Set<Id>();
            for(E_InsuredInfoController.InsuredListItem item : extension.InsuredList.Items) {
                if (item.record.RecordType.DeveloperName == E_Const.POLICY_RECORDTYPE_COLI) {
                    ids.add(item.record.id);
                }
            }
            Map<id, E_COVPF__c> recMap = E_COVPFDao.getRecById(ids);
            for(E_InsuredInfoController.InsuredListItem itemUpd : extension.InsuredList.Items) {
                E_COVPF__c cov = recMap.get(itemUpd.record.id);
                if (cov != null) {
                    itemUpd.record.COMM_ZCOVRNAM__c = cov.COLI_ZCOVRNAMES__c;
                    itemUpd.record.COMM_ZRSTDESC__c = cov.COLI_ZPSTDESC__c;
                }
                
				// 証券番号リンクの生成　NNLink使用：E_Coli、IRIS使用：IRIS_Coli 
				if(iris.getIsIRIS()){
					itemUpd.record.COMM_CHDRNUM__c = itemUpd.record.COMM_CHDRNUM_LINK_I__c;
				}else{
					itemUpd.record.COMM_CHDRNUM__c = itemUpd.record.COMM_CHDRNUM_LINK__c;
				}

            }
        }
    }

    //E_Policyに被保険者情報を設定する（権限のない取引先責任者情報も取得可能とする）
    private E_Policy__c getPolicyWithInsuredInfo(){
        return E_ContactDaoWithout.fillContactsToPolicy(this.extension.InsuredList.Items[0].record,false,true,false,false,false);
    }

    /** ページアクション */
    public PageReference pageAction(){
        return E_Util.toErrorPage(pageRef, null);
    }

    public override Boolean isValidate() {
        // URLパラメータ整合性チェック
        boolean isParam = checkConsistency();
        errorMsgCode = E_Const.ERROR_MSG_PARAMETER;
        return isParam;
    }

    /**
     * 各子テーブルの１件目とURLパラメータの整合性チェックする
     * @param ---
     * @return boolean
     */
     public boolean checkConsistency(){
        system.debug('---policy---' + policy);
        system.debug('---policy.Insured__c---' + policy.Insured__c);
        system.debug('---this.policy.Insured__r.Name---' + this.policy.Insured__r.Name);
        system.debug('---this.cid---' + this.cid);
        system.debug('---this.cName---' + this.cName);
        system.debug('---policy.Insured__c == this.cid---' + policy.Insured__c.equals(this.cid));
        system.debug('---this.policy.Insured__r.Name == this.cName---' + this.policy.Insured__r.Name.equals(this.cName));
        system.debug('---this.policy.Insured__r.Name == this.cName---' + this.policy.Insured__r.Name == this.cName);
        if (policy.Insured__c == this.cid && 
                this.policy.Insured__r.Name == this.cName) {
            return true;
        }
        return false;
    }
    
    /**
     * 戻るボタン
     */
    public override PageReference doReturn(){
System.debug('***********E_InsuredInfoExtender doReturn getCookieReferer:'+E_CookieHandler.getCookieReferer());
System.debug('***********E_InsuredInfoExtender doReturn getCookieRefererPolicy:'+E_CookieHandler.getCookieRefererPolicy());
System.debug('***********E_InsuredInfoExtender doReturn getCookieRefererContact:'+E_CookieHandler.getCookieRefererContact());
		/* スプリントバックログ 19対応　start  2017/1/30 */
		String pr = E_CookieHandler.getCookieRefererContact();
		E_CookieHandler.setCookieContactReferer(null);

		return new PageReference(pr);
		/* スプリントバックログ 19対応　end */
//        return Page.E_CustomerSearch;
    }
}