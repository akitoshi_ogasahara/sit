public with sharing class I_MeritTableBizLogic {

	//契約者種別
	private static final String CONTRACTOR_TYPE_CORPORATE = '3';

	//被保険者ID
	private static final String INSURED_ID_DUMMY = 'H0';

	//被保険者性別
	private static final String INSURED_SEX_MALE = '1';
	private static final String INSURED_SEX_FEMALE = '2';


	//払込頻度（年払い、半年払い、月払い）
	private static final String BILL_FREQUENCY_ANNUAL = '01';
	private static final String BILL_FREQUENCY_HALFANNUAL = '02';
	private static final String BILL_FREQUENCY_MONTHLY = '12';

	//払込方法
	private static final String PAYMENT_METHOD_ANNUAL = '1';
	private static final String PAYMENT_METHOD_HALFANNUAL = '2';
	private static final String PAYMENT_METHOD_MONTHLY = '3';
	private static final String PAYMENT_METHOD_PREPAYMENT_ALL = '4';
	private static final String PAYMENT_METHOD_PREPAYMENT_PARTIAL = '5';
	private static final String PAYMENT_METHOD_NORMALGROUP_ANNUAL = '6';
	private static final String PAYMENT_METHOD_NORMALGROUP_HALFANNUAL_A = '7';
	private static final String PAYMENT_METHOD_NORMALGROUP_HALFANNUAL_B = '8';
	private static final String PAYMENT_METHOD_NORMALGROUP_MONTHLY_A = '9';
	private static final String PAYMENT_METHOD_NORMALGROUP_MONTHLY_B = '10';
	private static final String PAYMENT_METHOD_SPECIALGROUP_ANNUAL = '11';
	private static final String PAYMENT_METHOD_SPECIALGROUP_HALFANNUAL = '12';
	private static final String PAYMENT_METHOD_SPECIALGROUP_MONTHLY = '13';
	private static final String PAYMENT_METHOD_DANTAIGROUP_MONTHLY_A = '14';


	//選択方法
	private static final String SELECTION_METHOD_ASSIGNED_NO = '0';
	private static final String SELECTION_METHOD_TYPE_NONE = '01';
	private static final String SELECTION_METHOD_CANDIDATE_NONE = null;

	//年・歳・終身
	private static final String INPUT_TYPE_YEAR = '1';
	private static final String INPUT_TYPE_AGE = '2';
	private static final String INPUT_TYPE_WHOLELIFE = '3';

	//終身時の保険期間
	private static final String PERIOD_WHOLELIFE = '999';

	//損金タイプ（養老保険でのみ使用）
	private static final String EXPENSE_TYPE_DUMMY = '1';

	//前納年数
	private static final Integer PREPAYMENT_YEAR_ZERO = 0;

	//低CV期間（低解約定期用）
	private static final String LCVTERM_INPUT_TYPE_NONE = '';
	private static final String LCVTERM_TERM_NONE = '';

	//収入保証期間（収入保証用）
	private static final String INCOME_ASSURANCE_TERM_NONE = '';

	//P免払込特則有無
	private static final String PREMIUM_EXEMPTION_NONE = '';

	//始期指定
	private static final String EFFECTIVE_DATE_NONE = '0';

	//払込方法（団体）
	private static final String BILLING_CHANNEL_GROUP = 'G';

	//団体種別
	private static final String ORGANIZATION_TYPE_NORMAL = '1';
	private static final String ORGANIZATION_TYPE_SPECIAL = '2';
	private static final String ORGANIZATION_TYPE_GROUP = '3';

	//団体料率
	private static final String PREMIUM_DISCOUNT_METHOD_B = '1';
	private static final String PREMIUM_DISCOUNT_METHOD_A = '2';

	//通常体・健康体・優良体
	private static final String MORTALITY_NORMAL = '1';
	private static final String MORTALITY_KENKOTAI = '2';
	private static final String MORTALITY_YURYOTAI = '3';
	private static final String MORTALITY_KENKOTAI_FOR_TERM_RIDER= '4';
	private static final String MORTALITY_YURYOTAI_FOR_TERM_RIDER = '5';

	//割増
	private static final String MORTALITY_NOT_WARIMASHI = '0';

	//個別申込・一括申込区分
	private static final String APPLICATION_TYPE_SINGLE = '1';

	private List<String> errorMessages = new List<String>();

	public final Map<String,String> warimashiMap = new Map<String,String>{
		//LifeJのMortalcls -> Atriaの割増コード（ALD、DD以外の場合に使用）
		'A' => '1',	'B' => '2','C' => '3','D' => '4','E' => '5','F' => '6','G' => '7','H' => '8','I' => '9',
		'J' => '10','K' => '11','L' => '12','M' => '13','N' => '14','O' => '15','P' => '16','Q' => '17','R' => '18',
		'T' => '40','U' => '41',
		'1' => '31','2' => '32','3' => '33','4' => '34','5' => '35','6' => '36','7' => '37','8' => '38','9' => '39'
    };

	public final Map<String,String> warimashiMapPQR = new Map<String,String>{
		//LifeJのMortalcls -> Atriaの割増コード（ALD、DDの場合に使用）
		'P' => '1','Q' => '2','R' => '3'
    };

	public final Map<String,String> sakugenMap = new Map<String,String>{
		//LifeJのLienCD -> Atriaの削減コード
		''   => '0','01' => '1','02' => '2','03' => '3','04' => '4','05' => '5'
    };

    public List<I_MeritTableDTO> convertEntityToDTO(List<I_MeritTableEntity> entityList, String crCode){

        List<I_MeritTableDTO> dtoList = new List<I_MeritTableDTO>();


        for(I_MeritTableEntity entity: entityList){
            I_MeritTableDTO dto = new I_MeritTableDTO();

            dto.keikaNensu = entity.keikaNensu;
            dto.age = entity.age;
            dto.pRuikei = Decimal.valueOf(entity.pRuikei);
            dto.cv = Decimal.valueOf(entity.cv);
            dto.simpleHenreiRt = Decimal.valueOf(entity.simpleHenreiRt);
            dto.sonkinP = Decimal.valueOf(entity.sonkinP);
            dto.koukaGaku = Decimal.valueOf(entity.koukaGaku);
            dto.koukaGakuRuikei =Decimal.valueOf(entity.koukaGakuRuikei);
            dto.jisshitsuFutanGaku = Decimal.valueOf(entity.jisshitsuFutanGaku);
            dto.jisshitsuHenreiRt = Decimal.valueOf(entity.jisshitsuHenreiRt);
            dto.shisanKeijoGaku = Decimal.valueOf(entity.shisanKeijoGaku);

			switch on crCode{
				when 'TT'{
					dto.S = Decimal.valueOf(entity.judaishippeiSManyen);
				}
				when 'TS'{
					dto.S = Decimal.valueOf(entity.shibouSeikatsuShogaiSManyen);
				}
				when 'OF', 'OG', 'OH', 'OI'{
					dto.S = null;
				}
				when else {
					dto.S = Decimal.valueOf(entity.shibouKoudoShogaiSManyen);
				}
			}
            dtoList.add(dto);

        }


        return dtoList;
    }

	//policy,cover,input情報から画面表示DTOListを返す
    public List<I_MeritTableDTO> createTableData(E_Policy__c policy, List<E_COVPF__c> covpfs, I_MeritTableChartController.I_TableInputData input){

    	I_CalcServiceInput calcInput = createCalcInput(policy,covpfs,input);

		if( errorMessages.size() > 0 ){
			String detailMessage = String.join(errorMessages,'\r\n');
			System.debug('detailMessage:'+detailMessage);
			throw new I_PIPException( '項目の値が想定外です。' , detailMessage );
		}

    	//Atria試算呼び出し
		I_CalcServiceAdapter calcAdapter = new I_CalcServiceAdapter();
		calcAdapter.nnlinkid = input.nnlinkid;

		//テストクラス内でコールアウト + データのInsertを行うとエラーになる為、分岐。
		if(!Test.isRunningTest()){

			List<I_MeritTableEntity> entityList = calcAdapter.calcMeritTable(calcInput);
			//推移表表示用DTOList作成
			List<I_MeritTableDTO> meritTableDTOList = convertEntityToDTO(entityList, calcInput.mainCoverInfo.crCode);

			return meritTableDTOList;
		}

        return null;
    }

    public I_CalcServiceInput createCalcInput(E_Policy__c policy, List<E_COVPF__c> covpfs, I_MeritTableChartController.I_TableInputData input){
        //主契約
        E_COVPF__c mainCovpf;

        //特約リスト
        List<E_COVPF__c> subCovpfs = new List<E_COVPF__c>();

        //個人保険特約データを主契約、特約に分ける
        for(E_COVPF__c covpf : covpfs){
            // 保障内容　C：主契約、C以外：特約
            if(covpf.COLI_ZCRIND__c.equals(I_Const.COVPF_COLI_ZCRIND_C) ){
                mainCovpf = covpf;
            }else{
                subCovpfs.add(covpf);
            }
        }

        I_CalcServiceInput calcInput = new I_CalcServiceInput();
        calcInput.mainCoverInfo = new I_CalcServiceInput.MainCoverInfo();


        /*プラン設計データの設定*/

        //計算基準日
        if(policy.coli_billfreq__c == BILL_FREQUENCY_MONTHLY){
            if(policy.comm_occdate__c == policy.comm_ccdate__c){
                calcInput.keisanKijunDate = policy.comm_ccdate__c;
            }else{
                calcInput.keisanKijunDate = policy.comm_occdate__c;
            }
        }else{
            calcInput.keisanKijunDate = policy.comm_ccdate__c;
        }

		//画面から取得する項目に変更
        //実行税率
        calcInput.taxRt = input.taxRate;

        //責任開始日
        calcInput.plcyDate = policy.comm_ccdate__c;

        //契約者種別
        calcInput.keiyakushaSb = CONTRACTOR_TYPE_CORPORATE;

        //申込形態
        calcInput.kobetsuIkkatsuKb = APPLICATION_TYPE_SINGLE;

        //損金タイプ
        calcInput.sonkinType = EXPENSE_TYPE_DUMMY;

        //前納年数
        calcInput.zennoNensu = PREPAYMENT_YEAR_ZERO;


        /*被保険者データの設定*/
        //被保険者ID
        calcInput.planHihokensyaInfo.hihokenshaId = INSURED_ID_DUMMY;

        //被保険者性別
        if(policy.insuredcltpf_zknjsex__c == '男性' ){
            calcInput.planHihokensyaInfo.hihokenshaSex = INSURED_SEX_MALE;
        }else if(policy.insuredcltpf_zknjsex__c == '女性'){
            calcInput.planHihokensyaInfo.hihokenshaSex = INSURED_SEX_FEMALE;
        }else{
   			//errorMessages.add('insuredcltpf_zknjsex__c:'+policy.insuredcltpf_zknjsex__c);
   			if(!Test.isRunningTest()) errorMessages.add('insuredcltpf_zknjsex__c:'+policy.insuredcltpf_zknjsex__c);
        }

        //被保険者年齢
		if(I_PolicyUtil.getInsuredANBCCD(policy.Id) != null){
			calcInput.planHihokensyaInfo.hihokenshaAge = I_PolicyUtil.getInsuredANBCCD(policy.Id).intValue();
		} else {
			errorMessages.add('E_CRLPF__c.ANBCCD__c:null');
		}


        //選択方法有無
        calcInput.planHihokensyaInfo.sentakuShiteiUmu = SELECTION_METHOD_ASSIGNED_NO;

        //選択方法
        calcInput.planHihokensyaInfo.sentakuHoho = SELECTION_METHOD_TYPE_NONE;

        //選択方法候補
        calcInput.planHihokensyaInfo.sentakuHohoOthKoho = SELECTION_METHOD_CANDIDATE_NONE ;


        /*主契約データの設定*/
        //CRコード
		if(mainCovpf.comm_crtable2__c != null){
			calcInput.mainCoverInfo.crCode = mainCovpf.comm_crtable2__c;
		}else{
			errorMessages.add('mainCovpf.comm_crtable2__c:'+mainCovpf.comm_crtable2__c);
		}


		//保険金額
		if(mainCovpf.COLI_BASICINSURANCE__c != null){
			calcInput.mainCoverInfo.sPlcy = Integer.valueOf(mainCovpf.COLI_BASICINSURANCE__c);
		}else{
			errorMessages.add('mainCovpf.COLI_BASICINSURANCE__c:'+mainCovpf.COLI_BASICINSURANCE__c);
		}


		 //保険期間種別
		if(mainCovpf.COMM_ZRCESDSC__c == null){

			errorMessages.add('mainCovpf.COMM_ZRCESDSC__c:null');

		}else if(mainCovpf.COMM_ZRCESDSC__c == '終身'){
			calcInput.mainCoverInfo.hokenKikanNenSaiSyushin = INPUT_TYPE_WHOLELIFE;
			calcInput.mainCoverInfo.hokenKikanPlcy = PERIOD_WHOLELIFE;

		}else if(mainCovpf.COMM_ZRCESDSC__c.right(1) == '歳'){

			calcInput.mainCoverInfo.hokenKikanNenSaiSyushin = INPUT_TYPE_AGE;
			calcInput.mainCoverInfo.hokenKikanPlcy = mainCovpf.COMM_ZRCESDSC__c.substring(0,mainCovpf.COMM_ZRCESDSC__c.length() -1);

		}else if(mainCovpf.COMM_ZRCESDSC__c.right(1) == '年'){
			calcInput.mainCoverInfo.hokenKikanNenSaiSyushin = INPUT_TYPE_YEAR;
			calcInput.mainCoverInfo.hokenKikanPlcy = mainCovpf.COMM_ZRCESDSC__c.substring(0,mainCovpf.COMM_ZRCESDSC__c.length() -1);

		}else{
			//エラーハンドリングロジック
			errorMessages.add('mainCovpf.COMM_ZRCESDSC__c:'+mainCovpf.COMM_ZRCESDSC__c);
		}

//      //払込期間種別
		if(mainCovpf.COMM_ZPCESDSC__c == null){

			errorMessages.add('mainCovpf.COMM_ZPCESDSC__c:null');

		}else if(mainCovpf.COMM_ZPCESDSC__c == '終身'){

			calcInput.mainCoverInfo.haraikomiKikanNenSaiSyushin = INPUT_TYPE_WHOLELIFE;
			calcInput.mainCoverInfo.haraikomiKikan = PERIOD_WHOLELIFE;

		}else if(mainCovpf.COMM_ZPCESDSC__c.right(1) == '歳'){

			calcInput.mainCoverInfo.haraikomiKikanNenSaiSyushin = INPUT_TYPE_AGE;
			calcInput.mainCoverInfo.haraikomiKikan = mainCovpf.COMM_ZPCESDSC__c.substring(0,mainCovpf.COMM_ZPCESDSC__c.length() -1);

		}else if(mainCovpf.COMM_ZPCESDSC__c.right(1) == '年'){
			calcInput.mainCoverInfo.haraikomiKikanNenSaiSyushin = INPUT_TYPE_YEAR;
			calcInput.mainCoverInfo.haraikomiKikan = mainCovpf.COMM_ZPCESDSC__c.substring(0,mainCovpf.COMM_ZPCESDSC__c.length() -1);

		}else{
			//エラーハンドリングロジック
			errorMessages.add('mainCovpf.COMM_ZPCESDSC__c:'+mainCovpf.COMM_ZPCESDSC__c);
		}

	    //低CV期間種別
		calcInput.mainCoverInfo.lowCvKikanNenSai = LCVTERM_INPUT_TYPE_NONE;

	    //低CV期間
		calcInput.mainCoverInfo.lowCvKikan = LCVTERM_TERM_NONE;

      	//保証期間
		calcInput.mainCoverInfo.inPHoshoKikan = INCOME_ASSURANCE_TERM_NONE;

		//健康体・優良体特約付加状態、割増（新特別条件特約）、削減年数
		I_Colidatas.condition condition = I_HealthConditionCheck.getKenkotaiWarimashiSakugenCode(covpfs);

		//エラーが発生していた場合は、エラーの内容を追加する
		if( condition.errorMsg.size() > 0 ){
			system.debug('エラー ：' + condition.errorMsg);
			errorMessages.addAll( condition.errorMsg );
		}

		//通常体・健康体・優良体
		switch on condition.MortalityClassKenkotaiYuryotai {
			when 'S'{
				calcInput.mainCoverInfo.kenkotai = '1';
			}
			when 'Y'{
				calcInput.mainCoverInfo.kenkotai = condition.TeikicoverFlg == true ? '4' : '2';
			}
			when 'Z'{
				calcInput.mainCoverInfo.kenkotai = condition.TeikicoverFlg == true ? '5' : '3';
			}
		}

		//割増
		if( condition.MortalityClassWarimashi != null && condition.MortalityClassWarimashi != '' ){
			if(condition.DDALDFlg == true){
				calcInput.mainCoverInfo.warimashi = warimashiMapPQR.get(condition.MortalityClassWarimashi);
			}else{
				calcInput.mainCoverInfo.warimashi = warimashiMap.get(condition.MortalityClassWarimashi);
			}
		}else{
			calcInput.mainCoverInfo.warimashi = '0';
		}

		//削減
		calcInput.mainCoverInfo.sakugen = sakugenMap.get(condition.LienCondition);


	    //特定疾病保険料払込免除特則 有無
   		calcInput.mainCoverInfo.tokuteiShippeiPMenTokusoku = PREMIUM_EXEMPTION_NONE;

		//払い込み方法
      	calcInput.mainCoverInfo.haraikomiHoho = getHaraikomiHoho(policy.COLI_BILLCHNL__c, policy.COLI_KGRPTYPE__c, policy.COLI_PREMDISC__c,  policy.COLI_BILLFREQ__c);

		//始期指定
		calcInput.mainCoverInfo.shikiShitei = EFFECTIVE_DATE_NONE;

		List<I_CalcServiceInput.SubCoverInfo> subCoverList = new List<I_CalcServiceInput.SubCoverInfo>();

		/*特約データの設定*/
		for(E_COVPF__c subCovpf : subCovpfs){

			I_CalcServiceInput.SubCoverInfo subCover = new I_CalcServiceInput.SubCoverInfo();

			//CRコード
			if(subCovpf.comm_crtable2__c != null){
				subCover.tokuyakuCrCode = subCovpf.comm_crtable2__c;
			}else{
				errorMessages.add('subCovpf.comm_crtable2__c:null');
			}


			//特約保険金額
			if(subCovpf.COLI_BASICINSURANCE__c != null){
				subCover.sTokuyaku = Integer.valueOf(subCovpf.COLI_BASICINSURANCE__c);
			}else{
				errorMessages.add('subCovpf.COLI_BASICINSURANCE__c:null');
			}

			 //保険期間種別//
			if(subCovpf.COMM_ZRCESDSC__c == null){

				//保険期間の判定は別の箇所で対応しているので不要

			}else if(subCovpf.COMM_ZRCESDSC__c == '終身'){

				subCover.hokenKikanNenSaiSyushin = '3';
				subCover.hokenKikanTokuyaku = '999';

			}else if(subCovpf.COMM_ZRCESDSC__c.right(1) == '歳'){

				subCover.hokenKikanNenSaiSyushin = '2';
				subCover.hokenKikanTokuyaku = subCovpf.COMM_ZRCESDSC__c.substring(0,subCovpf.COMM_ZRCESDSC__c.length() -1);

			}else if(subCovpf.COMM_ZRCESDSC__c.right(1) == '年'){
				subCover.hokenKikanNenSaiSyushin = '1';
				subCover.hokenKikanTokuyaku = subCovpf.COMM_ZRCESDSC__c.substring(0,subCovpf.COMM_ZRCESDSC__c.length() -1);
			}

			subCoverList.add(subCover);
		}

		calcInput.mainCoverInfo.subCoverList = subCoverList;
        return calcInput;
    }

	@Testvisible  private String getHaraikomiHoho(String billingChannel, String groupType, String premiumDiscountMethod, String billFreq){

		String haraikomiHoho = null;

		//団体の場合
		if(billingChannel == BILLING_CHANNEL_GROUP){
			//団体種類が通常の場合
			if(groupType == ORGANIZATION_TYPE_NORMAL){
				//団体保険料率がBタイプの場合、以下の払方に応じて払い込み方法をセット
				if(premiumDiscountMethod == PREMIUM_DISCOUNT_METHOD_B){
					if(billFreq == BILL_FREQUENCY_ANNUAL){
						haraikomiHoho = PAYMENT_METHOD_NORMALGROUP_ANNUAL;
					}else if(billFreq == BILL_FREQUENCY_HALFANNUAL){
						haraikomiHoho = PAYMENT_METHOD_NORMALGROUP_HALFANNUAL_B;
					}else if(billFreq == BILL_FREQUENCY_MONTHLY){
						haraikomiHoho = PAYMENT_METHOD_NORMALGROUP_MONTHLY_B;
					}
				//団体保険料率がAタイプの場合、以下の払方に応じて払い込み方法をセット
				}else if (premiumDiscountMethod == PREMIUM_DISCOUNT_METHOD_A){
					if(billFreq == BILL_FREQUENCY_HALFANNUAL){
						haraikomiHoho = PAYMENT_METHOD_NORMALGROUP_HALFANNUAL_A;
					}else if(billFreq == BILL_FREQUENCY_MONTHLY){
						haraikomiHoho = PAYMENT_METHOD_NORMALGROUP_MONTHLY_A;
					}
				}

			}else if(groupType == ORGANIZATION_TYPE_SPECIAL){

				if(billFreq == BILL_FREQUENCY_ANNUAL){
					haraikomiHoho = PAYMENT_METHOD_SPECIALGROUP_ANNUAL;
				}else if(billFreq == BILL_FREQUENCY_HALFANNUAL){
					haraikomiHoho = PAYMENT_METHOD_SPECIALGROUP_HALFANNUAL;
				}else if(billFreq == BILL_FREQUENCY_MONTHLY){
					haraikomiHoho = PAYMENT_METHOD_SPECIALGROUP_MONTHLY;
				}

			}else if(groupType == ORGANIZATION_TYPE_GROUP){
				if(billFreq == BILL_FREQUENCY_MONTHLY){
					haraikomiHoho = PAYMENT_METHOD_DANTAIGROUP_MONTHLY_A;
				}
			}

		//団体でない場合
		}else{
			if(billFreq == BILL_FREQUENCY_ANNUAL){
				haraikomiHoho = PAYMENT_METHOD_ANNUAL;
			}
			else if(billFreq == BILL_FREQUENCY_HALFANNUAL){
				haraikomiHoho = PAYMENT_METHOD_HALFANNUAL;
			}else if(billFreq == BILL_FREQUENCY_MONTHLY){
				haraikomiHoho = PAYMENT_METHOD_MONTHLY;
			}
		}
		if( haraikomiHoho == null ){
			// エラーハンドリング
			errorMessages.add('払込方法のInputが異常です。');
			errorMessages.add('policy.COLI_BILLCHNL__c:'+billingChannel);
			errorMessages.add('policy.COLI_KGRPTYPE__c:'+groupType);
			errorMessages.add('policy.COLI_PREMDISC__c:'+premiumDiscountMethod);
			errorMessages.add('policy.COLI_BILLFREQ__c:'+billFreq);
		}

		return haraikomiHoho;
	}
/*
	@TestVisible private String[] getKenkotaiWarimashiSakugenCode(List<E_COVPF__c> covpfs){
		String[] kenkotaiWarimashiSakugen = new String[3] ;

		String mainMortalCls = '';
		String mainCRCode = '';
		String mainLienCode = '';
		String subMortalCls = 'S';

		for(E_COVPF__c cover: covpfs){
			system.debug('cover.COMM_CRTABLE2__c =' + cover.COMM_CRTABLE2__c);
			//主契約判定
			if(cover.COLI_ZCRIND__c == 'C'){
				mainMortalCls = cover.COLI_MORTCLS__c;
				system.debug(' mainMortalCls = ' + mainMortalCls );
				mainCRCode = cover.COMM_CRTABLE2__c;
				if(cover.COLI_LIENCD__c == null) mainLienCode = '' ; else  mainLienCode = cover.COLI_LIENCD__c;
			}else{
				if(cover.COLI_MORTCLS__c != '' && cover.COLI_MORTCLS__c != null && cover.COLI_MORTCLS__c != 'S' && cover.COMM_CRTABLE2__c =='TR'){
					if(subMortalCls != 'S'){
						// エラーハンドリング
						errorMessages.add(I_PIPConst.ERROR_MESSAGE_BIZLOGIC_TOO_MANY_NOT_STANDART);
					}
					subMortalCls = cover.COLI_MORTCLS__c;
				}
			}

		}
		system.debug( 'mainMortalCls = ' + mainMortalCls + ' / subMortalCls = ' + subMortalCls );

		switch on mainMortalcls {



			when '', 'S',null {

				switch on subMortalCls{
					when 'S',null {
						kenkotaiWarimashiSakugen[0] = MORTALITY_NORMAL;
						kenkotaiWarimashiSakugen[1] = MORTALITY_NOT_WARIMASHI;
					}

					when 'Y' {
						kenkotaiWarimashiSakugen[0] = MORTALITY_KENKOTAI_FOR_TERM_RIDER;
						kenkotaiWarimashiSakugen[1] = MORTALITY_NOT_WARIMASHI;
					}

					when 'Z' {
						kenkotaiWarimashiSakugen[0] = MORTALITY_YURYOTAI_FOR_TERM_RIDER;
						kenkotaiWarimashiSakugen[1] = MORTALITY_NOT_WARIMASHI;
					}

					when else {
						// エラーハンドリング
						errorMessages.add('subMortalCls:'+subMortalCls);
					}
				}
			}

			when 'Y' {
				kenkotaiWarimashiSakugen[0] = MORTALITY_KENKOTAI;
				kenkotaiWarimashiSakugen[1] = MORTALITY_NOT_WARIMASHI;
			}

			when 'Z' {
				kenkotaiWarimashiSakugen[0] = MORTALITY_YURYOTAI;
				kenkotaiWarimashiSakugen[1] = MORTALITY_NOT_WARIMASHI;
			}

			when else {
				String warimashi = '';
				if(mainCRCode == I_PIPConst.CR_CODE_TS || mainCRCode == 'TL'){
					warimashi = warimashiMapPQR.get(mainMortalcls);
				}else{
					warimashi = warimashiMap.get(mainMortalcls);
				}

				if(String.isEmpty(warimashi)){
					// エラーハンドリング
					ErrorMessages.add('mainCRCode:'+mainCRCode);
					ErrorMessages.add('mainMortalcls:'+mainMortalcls);
				}

				kenkotaiWarimashiSakugen[0] = '1';
				kenkotaiWarimashiSakugen[1] = warimashi;
			}

		}

		String sakugen = sakugenMap.get(mainLienCode);

		if(sakugen == null){
			// エラーハンドリング
			errorMessages.add('mainLienCode:'+mainLienCode);
		}

		kenkotaiWarimashiSakugen[2] = sakugen;
		System.debug(mainLienCode);
		System.debug(sakugen);



		return kenkotaiWarimashiSakugen;

	}

*/
}