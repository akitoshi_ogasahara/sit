public with sharing class E_DownloadPolicyHistorySearchExtension {
    
    E_DownloadController extention;
    
    //全期間チェックボックス
    public Boolean isAllPeriod {get;set;}
    
    //選択可能年月の最大値・最小値
    public string startDate {get;set;}
    public string endDate {get;set;}
    
    //選択された年月
    public String selectedPeriodFrom {get;set;}
    public String selectedPeriodTo {get;set;}
    
    public String getTitle(){
        return '手続履歴出力条件設定';
    }
    
    //コンストラクタ
    public E_DownloadPolicyHistorySearchExtension(E_DownloadController extention) {
        this.extention = extention;
        extention.setPgTitle(getTitle());
        //パラメータから取得したレコード存在チェック
        extention.isValidate = extention.getActor() && extention.getCanDisplayPolicyHisotry();
        extention.pageRef = extention.init();
        
        if(extention.pageRef == null){
            isAllPeriod = false;
            startDate = datetime.newInstance(E_Util.getInquiryDate().addYears(-5).year(), 1, 1, 0, 0, 0).format('yyyy/MM/dd');
            endDate = E_Util.formatDataSyncDate();
            extention.formNo = E_Const.DH_FORMNO_POLIHISTORY;
        }
    }
    
    //初期化
    public PageReference pageAction(){
        return E_Util.toErrorPage(extention.pageRef, null);
    }
    
    //実行ボタン
    public PageReference doDownloadCsv(){
        
        PageReference ret = null;
        extention.pageMessages.clearMessages();
        
        try {
            E_DownloadCondition.HistoryDownloadCondi dlCondi = new E_DownloadCondition.HistoryDownloadCondi();
            extention.dh = new E_DownloadHistorry__c();
            
            //入力値検証
            boolean errFlg = false;
            if(!isAllPeriod){
                if(String.isNotBlank(selectedPeriodFrom)){
                    if(!extention.validateExistsDate(selectedPeriodFrom, '期間', true, '開始月')){
                        errFlg = true;
                    }
                    dlCondi.fromTRANDATE = selectedPeriodFrom;
                }
                if(String.isNotBlank(selectedPeriodTo)){
                    if(!extention.validateExistsDate(selectedPeriodTo, '期間', true, '終了月')){
                        errFlg = true;
                    }
                    dlCondi.toTRANDATE = selectedPeriodTo;
                }
                if(errFlg){
                    return null;
                }
            }
            
            //OPROにリクエストを投げる
            dlCondi.officeCodes = new List<String>();
            if(extention.distributor <> null){
                dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AH;
                dlCondi.distributorCode = extention.distributor.Id;
                dlCondi.ZHEADAY = extention.distributor.E_CL1PF_ZHEADAY__c;
                dlCondi.ZHEADNAME = extention.distributor.Name;
            } else if(extention.office <> null){
                dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AY;
                dlCondi.distributorCode = extention.office.ParentId;
                dlCondi.ZHEADAY = extention.office.E_ParentZHEADAY__c;
                dlCondi.ZHEADNAME = extention.office.E_ParentName__c;
                dlCondi.officeCodes.add(extention.office.Id);
            } else if(extention.agent <> null){
                dlCondi.ZIDTYPE = E_Const.ZIDTYPE_AT;
                dlCondi.distributorCode = extention.agent.Account.ParentId;
                dlCondi.ZHEADAY = extention.agent.E_AccParentCord__c;
                dlCondi.ZHEADNAME = extention.agent.E_AccParentName__c;
                dlCondi.officeCodes.add(extention.agent.AccountId);
                Contact tmp_con = E_ContactDaoWithout.getRecByAgentNumber(extention.agent.E_CL3PF_AGNTNUM__c.Left(5));
                //Contact tmp_con = E_ContactDao.getRecByAgentNumber(extention.agent.E_CL3PF_AGNTNUM__c.Left(5));
                if(tmp_con == null){
                    extention.pageMessages.addErrorMessage('メインアカウントが存在しません。');
                    return null;
                }
                dlCondi.agentCode = tmp_con.Id;
            }
            //ダウンロードログレコード生成
            extention.dh.outputType__c = E_Const.DH_OUTPUTTYPE_CSV;
            extention.dh.FormNo__c = extention.formNo;
            extention.dh.Conditions__c = dlCondi.toJSON();
            insert extention.dh;
            
        } catch(Exception e) {
            extention.pageMessages.addErrorMessage(e.getMessage());
            ret = null;
        }
        
        return ret;
    }
    
}