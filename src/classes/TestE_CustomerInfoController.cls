@isTest
private with sharing class TestE_CustomerInfoController{
	private static testMethod void testPageMethods() {		E_CustomerInfoController extension = new E_CustomerInfoController(new ApexPages.StandardController(new Contact()));
		SkyEditor2.Messages.clear();
		extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
		SkyEditor2.Messages.clear();
		extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
		SkyEditor2.Messages.clear();
		extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
		System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

		Integer defaultSize;
	}
	private static testMethod void testcoliTab() {
		E_CustomerInfoController.coliTab coliTab = new E_CustomerInfoController.coliTab(new List<E_COVPF__c>(), new List<E_CustomerInfoController.coliTabItem>(), new List<E_COVPF__c>(), null);
		coliTab.create(new E_COVPF__c());
		System.assert(true);
	}
	
	private static testMethod void testspvaTab() {
		E_CustomerInfoController.spvaTab spvaTab = new E_CustomerInfoController.spvaTab(new List<E_Policy__c>(), new List<E_CustomerInfoController.spvaTabItem>(), new List<E_Policy__c>(), null);
		spvaTab.create(new E_Policy__c());
		System.assert(true);
	}
	
	private static testMethod void testanTab() {
		E_CustomerInfoController.anTab anTab = new E_CustomerInfoController.anTab(new List<E_Policy__c>(), new List<E_CustomerInfoController.anTabItem>(), new List<E_Policy__c>(), null);
		anTab.create(new E_Policy__c());
		System.assert(true);
	}
	
	private static testMethod void testtiTab() {
		E_CustomerInfoController.tiTab tiTab = new E_CustomerInfoController.tiTab(new List<E_ITHPF__c>(), new List<E_CustomerInfoController.tiTabItem>(), new List<E_ITHPF__c>(), null);
		tiTab.create(new E_ITHPF__c());
		System.assert(true);
	}
	
}