public with sharing class I_ContentMasterDao {

	/**
	 * 指定したIRISページのコンポーネントとコンテンツを取得する
	 * @param String SalesforceID(IRISページ)
	 * @return List<I_ContentMaster__c> ページに紐づくコンテンツレコード一覧
	 */
	public static List<I_ContentMaster__c> getRecordsByPageId(String pid) {
		return [
				SELECT
					Id,
					Name,
					//Type__c,
					Style__c,
					Content__c,
					isHTML__c,
					DisplayBookmark__c,
					HiddenContentSubject__c,
					createIndex__c,
					DOM_Id__c,
					SelectFrom__c,
					SelectCondition__c,
					SelectLimit__c,
					LinkURL__c,
					WindowTarget__c,
					FeatureFlag__c,
					FeatureContents__c,
					FeaturePageUrl__c,
					FeatureSearch__c,
					ForCustomers__c,
					ForAgency__c,
					CompanyLimited__c,
					DisplayFrom__c,
					ValidTo__c,
					Category__c,
					Management__c,
					DisplayOrderKbn__c,
					DisplayOrderNo__c
					 ,(
						SELECT
						  Id,
						  Name,
						  //parentContent__r.Type__c,
						  parentContent__r.Style__c,
						  ClickAction__c,
						  filePath__c,
						  WindowTarget__c,
						  Content__c,
						  LinkTitle__c,
						  LinkURL__c,
						  isHTML__c,
						  useSP__c,
						  isRecommend__c,
						  HiddenContentSubject__c,
						  DateStr__c,
						  LabelforDate__c,
						  FormNo__c,
						  inquiryTo__c,
						  inquirySubject__c,
						  DocumentNo__c,
						  ApprovalNo__c,
						  DOM_Id__c,
						  CategoryJoin__c,
						  InfoLabel__c,
						  Style__c,
						  DisplayFrom__c,
						  ValidTo__c,
						  CanDisplayVia__c,
						  PostParam__c,
						  SelectCondition__c,
						  SelectLimit__c,
						  FeatureContents__c,
						  FeatureSearch__c,
						  FeaturePageUrl__c,
						  FeatureFlag__c,
						  ForCustomers__c,
						  ForAgency__c,
						  CompanyLimited__c,
						  FeatureDisplayFrom__c,
						  FeatureDisplayTo__c,
						  NewValidTo__c,
						  Category__c,
						  Management__c,
						  DisplayOrderKbn__c,
						  DisplayOrderNo__c,
						  ContentEmployee__c
						FROM
						  ChildContents__r
						ORDER BY
						  DisplayOrder__c
					  )
				FROM
					I_ContentMaster__c
				WHERE
					Page__c = :pid
				ORDER BY
					DisplayOrder__c
		];
	}


	/**
	 *		Contentsマスタ―に紐づけられたChatterFileのContentVersionId(068)の取得
	 */
	public static Map<Id,Id> getChatterFileIdsByRecId(Set<Id> recId){
		Map<Id, Id> parentCvMap = new Map<Id,Id>();
		for(I_ContentMaster__Feed cmf:[Select Type, Title, ParentId, Id
										, (Select Id, FeedEntityId, Type, RecordId, Title//, Value
											From FeedAttachments
											where type='Content')
										From I_ContentMaster__Feed
										Where parentId in :recId
										order by lastmodifieddate desc]){
			if(cmf.FeedAttachments.size()>0 && parentCvMap.containsKey(cmf.parentId)==false){		//最新のContentVersionを設定しておく
				parentCvMap.put(cmf.parentId, cmf.FeedAttachments[0].recordId);
			}
		}

		Map<Id, Id> cdCvMap = new Map<Id,Id>();

		for(ContentVersion cv : [select id,contentDocumentId From ContentVersion Where id in : parentCvMap.values()]){

			if(!cdCvMap.containsKey(cv.contentDocumentId)) {
				cdCvMap.put(cv.contentDocumentId,cv.id);
			}
		}

		Map<Id,Id> cvOldNewMap = new Map<Id,Id>();

		for(ContentDocument cd : [select id, (select id from ContentVersions order by VersionNumber desc) from ContentDocument where id in :cdCvMap.keySet()]){

			if(!cvOldNewMap.containsKey(cdCvMap.get(cd.Id))) {
				cvOldNewMap.put(cdCvMap.get(cd.Id), cd.contentVersions[0].Id);
			}
		}

		Map<Id, Id> ret = new Map<Id,Id>();

		for(Id rId : parentCvMap.keyset()){
			Id oldCvId = parentCvMap.get(rId);
			Id newCvId = cvOldNewMap.get(oldCvId);
			ret.put(rId, newCvId);
		}

		return ret;
	}

	/**
	 *		メニューメインカテゴリ単位のContentsマスターレコードの取得
	 */
	public static List<I_ContentMaster__c> getRecordsByMenuCategory(String cate) {
		return [
				SELECT
					Id,
					Name,
//					Type__c,
					Style__c,
					Content__c,
					isHTML__c,
					DisplayBookmark__c,
					HiddenContentSubject__c,
					createIndex__c,
					DOM_Id__c,
//					Page__r.Name,
					Page__c,
					Page__r.menu__c,
					Page__r.MainCategory__c,
					Page__r.menu__r.Id,
					Page__r.menu__r.DestUrl__c,
					Page__r.menu__r.requiredDefaultPageId__c,
					Page__r.menu__r.urlParameters__c,
					Page__r.menu__r.RequiredPermissionSet__c,
					ForCustomers__c,
					ForAgency__c,
					Category__c,
					Management__c,
					DisplayOrderKbn__c,
					DisplayOrderNo__c
					,(
						SELECT
							Id,
							Name,
//							parentContent__r.Type__c,
							parentContent__r.Style__c,
							ClickAction__c,
							filePath__c,
							WindowTarget__c,
							Content__c,
							LinkTitle__c,
							LinkURL__c,
							isHTML__c,
							useSP__c,
							isRecommend__c,
							HiddenContentSubject__c,
							DateStr__c,
							LabelforDate__c,
							FormNo__c,
							inquiryTo__c,
							inquirySubject__c,
							DOM_Id__c,
							Style__c,
							DisplayFrom__c,
							ValidTo__c,
							CanDisplayVia__c,
							PostParam__c,
							ForCustomers__c,
							ForAgency__c,
							NewValidTo__c,
							Category__c,
							Management__c,
							DisplayOrderKbn__c,
							DisplayOrderNo__c
						FROM
							ChildContents__r
						ORDER BY
							DisplayOrder__c
					)
				FROM
					I_ContentMaster__c
				WHERE
					Page__r.MainCategory__c = :cate
				ORDER BY
					Page__r.Menu__r.DisplayOrder__c, DisplayOrder__c
		];
	}

	/**
	 * 資料管理番号+枝番からIRIS CMSコンテンツを1件取得する.
	 *
	 * @param String 資料管理番号+枝番
	 * @return I_ContentMaster__c IRIS CMSコンテンツレコード
	 */
	public static I_ContentMaster__c getRecByDocNumber(String docNum) {
		I_ContentMaster__c res = null;

		List<I_ContentMaster__c> recs =
			[
				Select
					Id,
					Name,
					DocumentNo__c,
					DOM_Id__c,
					CategoryJoin__c,
					DocumentAndBranchNo__c,
					SelectFrom__c,
					CanDisplayVia__c,
					PostParam__c,
					Category__c,
					isEmployeeCategory__c
				From
					I_ContentMaster__c
				Where
					// Debug
					//DocumentAndBranchNo__c =: docNum
					DocumentNo__c =: docNum
				Order by
					LastModifiedDate Desc
			];

		if(recs.size() > 0) {
			res = recs.get(0);
		}

		return res;
	}

	/**
	 * formNoからIRIS CMSコンテンツを1件取得する.
	 *
	 * @param String FormNo
	 * @return I_ContentMaster__c IRIS CMSコンテンツレコード
	 */
	public static I_ContentMaster__c getRecByFormNo(String formNo) {
		I_ContentMaster__c res = null;

		List<I_ContentMaster__c> recs =
			[
				Select
					Id,
					Name,
					DOM_Id__c,
					FormNo__c,
					PamphletCategory__c,
					isEmployeeCategory__c
				From
					I_ContentMaster__c
				Where
					FormNo__c =: formNo
				Order by
					LastModifiedDate Desc
			];

		if(recs.size() > 0) {
			res = recs.get(0);
		}

		return res;
	}

	/**
	 * formNoとNameからIRIS CMSコンテンツを取得する.
	 *
	 * @param String FormNo
	 * @return I_ContentMaster__c IRIS CMSコンテンツレコード
	 */
	public static I_ContentMaster__c getRecByFormNoAndName(String formNo, String contentName) {
		I_ContentMaster__c res = null;

		List<I_ContentMaster__c> recs =
			[
				Select
					Id,
					Name,
					DOM_Id__c,
					FormNo__c,
					PamphletCategory__c,
					isEmployeeCategory__c
				From
					I_ContentMaster__c
				Where
					FormNo__c =: formNo
				And
					Name =:contentName
				Order by
					LastModifiedDate Desc
			];

		if(recs.size() > 0) {
			res = recs.get(0);
		}

		return res;
	}

	/**
	 * documentNoとNameからIRIS CMSコンテンツを取得する.
	 *
	 * @param String DcumentNo
	 * @return I_ContentMaster__c IRIS CMSコンテンツレコード
	 */
	public static I_ContentMaster__c getRecByDocumentNoAndName(String documentNo, String contentName) {
		I_ContentMaster__c res = null;

		List<I_ContentMaster__c> recs =
			[
				Select
					Id
				From
					I_ContentMaster__c
				Where
					DocumentNo__c =: documentNo
				And
					Name =:contentName
				Order by
					LastModifiedDate Desc
			];

		if(recs.size() > 0) {
			res = recs.get(0);
		}

		return res;
	}

	/**
	 * メニュー名からIRIS CMSコンテンツを取得する.
	 *
	 * @param String メニュー名
	 * @return List<I_ContentMaster__c> IRIS CMSコンテンツのリスト
	 */
	public static List<I_ContentMaster__c> getSalesToolRecords(String cateLayer2) {

		//SalesToolのセクションレコードを取得
		I_ContentMaster__c salesToolSection;
		for(I_ContentMaster__c sec:[SELECT Id FROM I_ContentMaster__c
									WHERE Name = :I_Const.IRIS_CONTENTS_NAME_SALESTOOL Limit 1]){
			salesToolSection = sec;
		}
		if(salesToolSection == null){
			return new List<I_ContentMaster__c>();
		}

		List<I_ContentMaster__c> childlen =
			[
				Select
					Id,
					Name,
					CategoryJoin__c,
					DisplayFrom__c,
					DisplayTo__c,
					ValidTo__c,
					Category__c
				From
					I_ContentMaster__c
				Where
					ParentContent__c =: salesToolSection.Id
			];

		Set<Id> childIds = new Set<Id>();

		Integer loopLimit = childlen.size();

		for(Integer i = 0; i < loopLimit; i++) {

			I_ContentMaster__c child = childlen.get(i);

			if(child.DisplayFrom__c != null && child.DisplayFrom__c > E_Util.SYSTEM_TODAY()) {
				continue;
			}

			/*
			if(child.DisplayTo__c != null && child.DisplayTo__c < E_Util.SYSTEM_TODAY()) {
				continue;
			}
			*/

			if(child.ValidTo__c != null && child.ValidTo__c < E_Util.SYSTEM_TODAY()) {
				continue;
			}

			/* UX2 セールスツールマスタメンテ対応 20170629
			if(String.isBlank(child.CategoryJoin__c)) {
				continue;
			}

			for(String cate : child.CategoryJoin__c.split(',')) {
				if(cate == cateLayer2) {
					childIds.add(child.Id);

					break;
				}
			}
			*/
			if(String.isBlank(child.Category__c)) {
				continue;
			}

			for(String cate : child.Category__c.split(';')) {
				if(cate == cateLayer2) {
					childIds.add(child.Id);

					break;
				}
			}
		}

		if(childIds.size() == 0) {
			return new List<I_ContentMaster__c>();
		}

		System.debug(childIds.size());
		return
			[
				SELECT
					Id,
					Name,
					Style__c,
					Content__c,
					ContentEmployee__c,
					isHTML__c,
					DisplayBookmark__c,
					HiddenContentSubject__c,
					createIndex__c,
					SelectFrom__c,
					DOM_Id__c,
					FeatureFlag__c,
					FeatureContents__c,
					ForCustomers__c,
					ForAgency__c,
					CompanyLimited__c,
					Category__c,
					Management__c,
					DisplayOrderKbn__c,
					DisplayOrderNo__c,
					(
						SELECT
							Id,
							Name,
							parentContent__r.Style__c,
							ClickAction__c,
							filePath__c,
							WindowTarget__c,
							Content__c,
							ContentEmployee__c,
							LinkTitle__c,
							LinkURL__c,
							isHTML__c,
							useSP__c,
							isRecommend__c,
							HiddenContentSubject__c,
							DateStr__c,
							LabelforDate__c,
							FormNo__c,
							inquiryTo__c,
							inquirySubject__c,
							DOM_Id__c,
							DocumentNo__c,
							ApprovalNo__c,
							ForCustomers__c,
							ForAgency__c,
							CompanyLimited__c,
							DocumentAndBranchNo__c,
							NewValidTo__c,
							InfoLabel__c,
							Style__c,
							DisplayFrom__c,
							ValidTo__c,
							CanDisplayVia__c,
							PostParam__c,
							FeatureFlag__c,
							FeatureContents__c,
							Category__c,
							Management__c,
							DisplayOrderKbn__c,
							DisplayOrderNo__c
						FROM
							ChildContents__r
						WHERE
							Id IN: childIds
						ORDER BY
							/*  UX2 セールスツールマスタメンテ対応 20170629
								DisplayOrder__c
							*/
							DisplayOrderKbn__c asc, DisplayOrderNo__c asc, DisplayOrder__c asc
					)
				FROM
					I_ContentMaster__c
				WHERE
					Id = :salesToolSection.Id
				ORDER BY
					/*  UX2 セールスツールマスタメンテ対応 20170629
						DisplayOrder__c
					*/
					DisplayOrderKbn__c asc, DisplayOrderNo__c asc, DisplayOrder__c asc
		];
	}

	/**
	 * お知らせメニューの子コンテンツを取得する
	 * @return List<I_ContentMaster__c> IRIS CMSコンテンツのリスト
	 */
	public static List<I_ContentMaster__c> getRecordsInfoContent() {
		return getRecordsInfoContent(null);
	}
	public static List<I_ContentMaster__c> getRecordsInfoContent(String recId) {
		String OshiraseName = I_Const.IRIS_CONTENTS_NAME_INFO;
		Date sysday = E_Util.SYSTEM_TODAY();
		String conday = String.valueOf(sysday);
		conday = conday.replaceAll('-', '/');

		String soql = 'SELECT ';
				soql += 'Id,';
				soql += 'Name,';
				soql += 'CreatedDate,';
				soql += 'Style__c,';
				soql += 'Content__c,';
				soql += 'isHTML__c,';
				soql += 'DisplayBookmark__c,';
				soql += 'InfoLabel__c,';
				soql += 'HiddenContentSubject__c,';
				soql += 'createIndex__c,';
				soql += 'SelectFrom__c,';
				soql += 'DOM_Id__c,';
				soql += 'DateStr__c,';
				soql += 'FeatureFlag__c,';
				soql += 'FeatureContents__c,';
				soql += 'FeaturePageUrl__c,';
				soql += 'FeatureSearch__c,';
				soql += 'ForCustomers__c,';
				soql += 'ForAgency__c,';
				soql += 'CompanyLimited__c,';
				soql += 'Page__c,';
				soql += 'Page__r.menu__c,';
				soql += 'Page__r.MainCategory__c,';
				soql += 'Page__r.menu__r.Id,';
				soql += 'Page__r.menu__r.DestUrl__c,';
				soql += 'Page__r.menu__r.requiredDefaultPageId__c,';
				soql += 'Page__r.menu__r.urlParameters__c,';
				soql += '(SELECT Id,';
				soql += 		'Name,';
				soql += 		'CreatedDate,';
				soql += 		'Page__c,';
				soql += 		'parentContent__r.Style__c,';
				soql += 		'ClickAction__c,';
				soql += 		'filePath__c,';
				soql += 		'WindowTarget__c,';
				soql += 		'Content__c,';
				soql += 		'LinkTitle__c,';
				soql += 		'LinkURL__c,';
				soql += 		'isHTML__c,';
				soql += 		'useSP__c,';
				soql += 		'isRecommend__c,';
				soql += 		'HiddenContentSubject__c,';
				soql += 		'InfoLabel__c ,';
				soql += 		'DateStr__c,';
				soql += 		'LabelforDate__c,';
				soql += 		'FormNo__c,';
				soql += 		'inquiryTo__c,';
				soql += 		'inquirySubject__c,';
				soql += 		'DOM_Id__c,';
				soql += 		'Page__r.menu__c,';
				soql += 		'Style__c,';
				soql += 		'CanDisplayVia__c,';
				soql += 		'PostParam__c,';
				soql += 		'FeatureFlag__c,';
				soql += 		'FeatureContents__c,';
				soql += 		'FeaturePageUrl__c,';
				soql += 		'FeatureSearch__c,';
				soql += 		'ForCustomers__c,';
				soql += 		'ForAgency__c,';
				soql += 		'CompanyLimited__c,';
				soql += 		'tubeIds__c ';	//20170705 NN_IRIS-495対応
				soql +=		' FROM ChildContents__r ';
				soql +=		' Where DateStr__c <=: conday';
				if(String.isNotBlank(recId)){
					soql +=		' AND Id = :recId ';
				}
				soql +=		' ORDER BY DateStr__c DESC, LastModifiedDate DESC) ';
				soql += ' FROM I_ContentMaster__c ';
				soql += ' Where Name =:OshiraseName ';		//お知らせ
				soql += ' ORDER BY DateStr__c ';
				soql += ' Limit 1 ';								//基本1レコードのみ存在するはず

		return Database.query(soql);
	}

	public static List<I_ContentMaster__c> getPickupContentsForLibTop(String condition) {
		return getPickupContentsForLibTop(condition, 0);
	}
	public static List<I_ContentMaster__c> getPickupContentsForLibTop(String condition, Decimal retrieveRecordCnt) {
		String soql = 'Select Id, Name, ClickAction__c, Style__c, HiddenContentSubject__c, isHTML__c, DateStr__c, InfoLabel__c, ValidTo__c, ';
		soql += 'inquirySubject__c, NewValidTo__c, isRecommend__c, DocumentNo__c, ApprovalNo__c, CategoryJoin__c,DisplayFrom__c, LinkURL__c, ';
		soql += 'LinkTitle__c, LabelforDate__c, DOM_Id__c, Content__c, WindowTarget__c, ParentContent__r.Id, ParentContent__r.Style__c, CanDisplayVia__c, PostParam__c,ForCustomers__c,ForAgency__c,CompanyLimited__c,DocumentAndBranchNo__c, ';
		soql += 'Category__c, Management__c, DisplayOrderKbn__c, DisplayOrderNo__c, ContentEmployee__c,FilePath__c ';
		soql += 'From I_ContentMaster__c';

		if(String.isNotBlank(condition)) {
			soql += ' Where ' + condition;
		}

		if(retrieveRecordCnt > 0){
			soql += ' Limit ' + String.valueOf(retrieveRecordCnt.intValue());
		}

		return Database.query(soql);
	}

	public static List<I_ContentMaster__c> getBannerContents() {
		return [
				SELECT
					Id,
					Name,
					Style__c,
					Content__c,
					isHTML__c,
					DisplayBookmark__c,
					HiddenContentSubject__c,
					createIndex__c,
					DOM_Id__c,
					SelectFrom__c,
					SelectCondition__c,
					SelectLimit__c,
					LinkURL__c,
					WindowTarget__c,
					FeatureFlag__c,
					FeatureContents__c,
					FeaturePageUrl__c,
					FeatureSearch__c,
					ForCustomers__c,
					ForAgency__c,
					CompanyLimited__c,
					FilePath__c
				FROM
					I_ContentMaster__c
				WHERE
					FeatureDisplayTo__c >= Today
				AND FeatureDisplayFrom__c <= today
				AND FeatureFlag__c = true
				ORDER BY
					FeatureDisplayTo__c DESC
				];
	}


	/**
	*
	* Upsertkey__c(外部ID)からIRIS_CMSコンテンツを取得する
	*
	* @param String upsertKey（外部ID）
	* @return I_ContentMaster__c IRIS_CMSコンテンツレコード
	*/
	public static I_ContentMaster__c getRecordByUpsertKey(String upsertKey){
		I_ContentMaster__c rec = null;
		List<I_ContentMaster__c> recs = [SELECT Id, ClickAction__c
											, FilePath__c
											, LinkURL__c    //20180411 クリックアクション追加対応
											, (SELECT Id, Name FROM Attachments ORDER BY LastModifiedDate Desc)
									FROM I_ContentMaster__c
									WHERE UpsertKey__c =: upsertKey
									];
		if(!recs.isEmpty() ) rec = recs[0];
		return rec;
	}

	/**
	*
	* Upsertkey__c(外部ID)からIRIS_CMSコンテンツを取得する
	*
	* @param String upsertKey（外部ID）
	* @return I_ContentMaster__c IRIS_CMSコンテンツレコード
	*/
	public static I_ContentMaster__c getTextByUpsertKey(String upsertKey){
		List<I_ContentMaster__c> recs = [SELECT Id
											,Content__c
											,ValidTo__c
									FROM I_ContentMaster__c
									WHERE UpsertKey__c =: upsertKey
									];
		return !recs.isEmpty() ? recs[0] : null;
	}

	/**
	*
	* Upsertkey__c(外部ID)からIRIS_CMSコンテンツをまとめて取得する
	*
	* @param List<String> upsertKey（外部ID）
	* @return I_ContentMaster__c IRIS_CMSコンテンツレコード
	*/
	public static Map<String, I_ContentMaster__c> getCMSMapByUpsertKey(List<String> upsertKeys){
		Map<String, I_ContentMaster__c> cmsmap = new Map<String, I_ContentMaster__c>();
		List<I_ContentMaster__c> recs = [SELECT ClickAction__c
											, FilePath__c
											, LinkURL__c
											, UpsertKey__c
											, (SELECT Id, Name FROM Attachments ORDER BY LastModifiedDate Desc)
									FROM I_ContentMaster__c
									WHERE UpsertKey__c =: upsertKeys
									];
		if(!recs.isEmpty()){
			for(I_ContentMaster__c cms : recs){
				cmsmap.put(cms.UpsertKey__c, cms);
			}
		}
		return cmsmap;
	}

	/**
	 * 指定したIRISページのコンポーネントとコンテンツを取得する
	 * @param Set<Id> parentIds 親CMSコンテンツレコードIDセット
	 * @return List<I_ContentMaster__c> コンテンツレコード一覧
	 */
	public static List<I_ContentMaster__c> getChildRecordsByPageId(Set<Id> parentIds) {
		return [SELECT
					Id,
					Name,
					//parentContent__r.Type__c,
					parentContent__r.Style__c,
					ClickAction__c,
					filePath__c,
					WindowTarget__c,
					Content__c,
					LinkTitle__c,
					LinkURL__c,
					isHTML__c,
					useSP__c,
					isRecommend__c,
					HiddenContentSubject__c,
					DateStr__c,
					LabelforDate__c,
					FormNo__c,
					inquiryTo__c,
					inquirySubject__c,
					DocumentNo__c,
					ApprovalNo__c,
					DOM_Id__c,
					CategoryJoin__c,
					InfoLabel__c,
					Style__c,
					DisplayFrom__c,
					ValidTo__c,
					CanDisplayVia__c,
					PostParam__c,
					SelectCondition__c,
					SelectLimit__c,
					FeatureContents__c,
					FeatureSearch__c,
					FeaturePageUrl__c,
					FeatureFlag__c,
					ForCustomers__c,
					ForAgency__c,
					CompanyLimited__c,
					FeatureDisplayFrom__c,
					FeatureDisplayTo__c,
					NewValidTo__c,
					Category__c,
					Management__c,
					DisplayOrderKbn__c,
					DisplayOrderNo__c
				FROM
					I_ContentMaster__c
				WHERE
					parentContent__r.Id IN :parentIds
				ORDER BY
					DisplayOrder__c
		];
	}

	/**
	 * メニュー名からIRIS CMSコンテンツを取得する.
	 *
	 * @param String メニュー名
	 * @param Set<Id> parentIds 親CMSコンテンツレコードIDセット
	 * @return List<I_ContentMaster__c> IRIS CMSコンテンツのリスト
	 */
	public static List<I_ContentMaster__c> getSalesToolChildRecords(String cateLayer2, Set<Id> parentIds) {

		//SalesToolのセクションレコードを取得
		I_ContentMaster__c salesToolSection;
		for(I_ContentMaster__c sec:[SELECT Id FROM I_ContentMaster__c
									WHERE Name = :I_Const.IRIS_CONTENTS_NAME_SALESTOOL Limit 1]){
			salesToolSection = sec;
		}
		if(salesToolSection == null){
			return new List<I_ContentMaster__c>();
		}

		List<I_ContentMaster__c> childlen =
			[
				Select
					Id,
					Name,
					CategoryJoin__c,
					DisplayFrom__c,
					DisplayTo__c,
					ValidTo__c,
					Category__c
				From
					I_ContentMaster__c
				Where
					ParentContent__c =: salesToolSection.Id
			];

		Set<Id> childIds = new Set<Id>();

		Integer loopLimit = childlen.size();

		for(Integer i = 0; i < loopLimit; i++) {

			I_ContentMaster__c child = childlen.get(i);

			if(child.DisplayFrom__c != null && child.DisplayFrom__c > E_Util.SYSTEM_TODAY()) {
				continue;
			}

			/*
			if(child.DisplayTo__c != null && child.DisplayTo__c < E_Util.SYSTEM_TODAY()) {
				continue;
			}
			*/

			if(child.ValidTo__c != null && child.ValidTo__c < E_Util.SYSTEM_TODAY()) {
				continue;
			}

			/* UX2 セールスツールマスタメンテ対応 20170629
			if(String.isBlank(child.CategoryJoin__c)) {
				continue;
			}

			for(String cate : child.CategoryJoin__c.split(',')) {
				if(cate == cateLayer2) {
					childIds.add(child.Id);

					break;
				}
			}
			*/
			if(String.isBlank(child.Category__c)) {
				continue;
			}

			for(String cate : child.Category__c.split(';')) {
				if(cate == cateLayer2) {
					childIds.add(child.Id);

					break;
				}
			}
		}

		if(childIds.size() == 0) {
			return new List<I_ContentMaster__c>();
		}

		return
			[SELECT
				Id,
				Name,
				parentContent__r.Style__c,
				ClickAction__c,
				filePath__c,
				WindowTarget__c,
				Content__c,
				LinkTitle__c,
				LinkURL__c,
				isHTML__c,
				useSP__c,
				isRecommend__c,
				HiddenContentSubject__c,
				DateStr__c,
				LabelforDate__c,
				FormNo__c,
				inquiryTo__c,
				inquirySubject__c,
				DOM_Id__c,
				DocumentNo__c,
				ApprovalNo__c,
				ForCustomers__c,
				ForAgency__c,
				CompanyLimited__c,
				DocumentAndBranchNo__c,
				NewValidTo__c,
				Style__c,
				DisplayFrom__c,
				ValidTo__c,
				CanDisplayVia__c,
				PostParam__c,
				FeatureFlag__c,
				FeatureContents__c,
				Category__c,
				Management__c,
				DisplayOrderKbn__c,
				DisplayOrderNo__c
			FROM
				I_ContentMaster__c
			WHERE
				Id IN: childIds
			AND
				parentContent__r.Id IN :parentIds
			ORDER BY
				/*  UX2 セールスツールマスタメンテ対応 20170629
					DisplayOrder__c
				*/
				DisplayOrderKbn__c asc, DisplayOrderNo__c asc, DisplayOrder__c asc
		];
	}

	/**
	 * お知らせラベルからIRIS_CMSコンテンツを取得する
	 * @param  Set<String> names お知らせラベル
	 * @param  Integer limitSize 取得上限値
	 * @return I_ContentMaster__c IRIS_CMSコンテンツレコード
	 */
	public static List<I_ContentMaster__c> getRecordByInfoName(Set<String> names, Integer limitSize){
		Date sysday = E_Util.SYSTEM_TODAY();
		String conday = String.valueOf(sysday);
		conday = conday.replaceAll('-', '/');
		return [
			SELECT
				Id, Name, InfoLabel__c, DateStr__c
			FROM
				I_ContentMaster__c
			WHERE
				InfoLabel__c IN :names
			  AND DateStr__c <= :conday
			ORDER BY DateStr__c
			DESC LIMIT :limitSize
		];
	}

	/**
	 * 資料カテゴリからIRIS_CMSコンテンツを取得する
	 * @param  Set<String> categories 資料カテゴリ
	 * @return I_ContentMaster__c IRIS_CMSコンテンツレコード
	 */
	public static List<I_ContentMaster__c> retrieveDocumentalContents(Set<String> categories) {
		return [
			SELECT
				Id,
				UpsertKey__c,
				Name,
				FormNo__c,
				DisplayFrom__c,
				NewValidTo__c,
				DocumentCategory__c,
				CannotBeOrder__c,
				MaxCopies__c
			FROM
				I_ContentMaster__c
			WHERE
				DocumentCategory__c IN :categories
			  AND
				DisplayFrom__c <= :Date.today()
			  AND
				(
					ValidTo__c >= :Date.today()
				  OR
					ValidTo__c = NULL
				)
			ORDER BY PamphletOrderBy__c ASC
		];
	}

	public static I_ContentMaster__c getOshiraseInfo(){
		E_AccessController access = E_AccessController.getInstance();

		String OshiraseName = I_Const.IRIS_CONTENTS_NAME_INFO;
		Date sysday = E_Util.SYSTEM_TODAY();
		String conday = String.valueOf(sysday);
		conday = conday.replaceAll('-', '/');

		String soql = 'SELECT ';
			soql += 'Id,';
			soql += 'Page__r.menu__r.Id,';
			soql += 'Page__r.menu__r.DestUrl__c,';
			soql += '(SELECT Id,';
			soql += 		'Name,';
			soql += 		'InfoLabel__c ,';
			soql += 		'DateStr__c ';
			soql += 	' FROM ChildContents__r ';
			soql += 	' Where DateStr__c <=: conday';
			if(access.user.Contact.Account.CannotBeOrder__c){
				for(String doc : I_Const.DR_INFO_LABEL){
					soql += ' AND InfoLabel__c != \'' + doc + '\' ';
				}
			}
			soql += 	' ORDER BY DateStr__c DESC, LastModifiedDate DESC) ';
			soql += ' FROM I_ContentMaster__c ';
			soql += ' Where Name =:OshiraseName ';		//お知らせ
			soql += ' ORDER BY DateStr__c ';
			soql += ' Limit 1 ';

		return Database.query(soql);
	}

	/**
	 * パンフレット等
	 * メニュー名からIRIS CMSコンテンツを取得する.
	 *
	 * @param String メニュー名
	 * @return List<I_ContentMaster__c> IRIS CMSコンテンツのリスト
	 */
	public static List<I_ContentMaster__c> getPamphletRecords(String menuName) {

		//Pamphletのセクションレコードを取得
		List<I_ContentMaster__c> pamphletSections = new List<I_ContentMaster__c>();
		for(I_ContentMaster__c sec:[SELECT Id FROM I_ContentMaster__c WHERE Name IN :I_Const.PAMPHLET_CONTENTS]){
			pamphletSections.add(sec);
		}
		if(pamphletSections == null){
			return new List<I_ContentMaster__c>();
		}

		List<I_ContentMaster__c> childlen =
		[
			Select
				Id,
				Name,
				CategoryJoin__c,
				DisplayFrom__c,
				DisplayTo__c,
				ValidTo__c,
				PamphletCategory__c
			From
				I_ContentMaster__c
			Where
				ParentContent__c IN :pamphletSections
		];

		Set<Id> childIds = new Set<Id>();

		Integer loopLimit = childlen.size();

		for(Integer i = 0; i < loopLimit; i++) {

			I_ContentMaster__c child = childlen.get(i);

			if(String.isBlank(child.PamphletCategory__c)) {
				continue;
			}

			for(String category : child.PamphletCategory__c.split(';')) {
				if(category == menuName) {
					childIds.add(child.Id);
					break;
				}
			}
		}

		if(childIds.size() == 0) {
			return new List<I_ContentMaster__c>();
		}
		return
			[
				SELECT
					Id,
					Name,
					Style__c,
					Content__c,
					isHTML__c,
					HiddenContentSubject__c,
					createIndex__c,
					SelectFrom__c,
					DOM_Id__c,
					FeatureFlag__c,
					PamphletCategory__c,
					(
						SELECT
							Id,
							Name,
							parentContent__r.Style__c,
							ClickAction__c,
							filePath__c,
							WindowTarget__c,
							Content__c,
							LinkTitle__c,
							LinkURL__c,
							isHTML__c,
							HiddenContentSubject__c,
							FormNo__c,
							DOM_Id__c,
							DocumentNo__c,
							ApprovalNo__c,
							InfoLabel__c,
							DisplayFrom__c,
							ValidTo__c,
							CanDisplayVia__c,
							FeatureFlag__c,
							PamphletCategory__c,
							PamphletOrderBy__c
						FROM
							ChildContents__r
						WHERE
							Id IN: childIds
						AND
							DisplayFrom__c <=: Date.today()
						ORDER BY
							// 商品順 掲示開始日の降順
							PamphletOrderBy__c asc, DisplayOrder__c asc, DisplayFrom__c desc
					)
				FROM
					I_ContentMaster__c
				WHERE
					Id IN :pamphletSections
				ORDER BY
					DisplayOrder__c asc
		];
	}

	/**
	 * Web約款検索
	 */
	public static List<I_ContentMaster__c> getYakkanContents(String condition, Date tday){

		String soql = '';
		soql += 'SELECT Id, isSitePublishFlg__c, IsYakkanParentFlg__c, FormNo__c,';
		soql += ' toLabel(InsType__c), ContractDateFrom__c, ContractDateTo__c, AgencyCode__c, GuideAgreeDiv__c,';
		soql += ' (SELECT Id FROM ChildContents__r)';
		soql += ' FROM I_ContentMaster__c';
		soql += ' WHERE IsYakkanParentFlg__c = true AND isSitePublishFlg__c = true AND GuideAgreeDiv__c != \'\' AND AgencyCode__c != \'\' AND InsType__c != \'\'';
		soql += ' AND ContractDateFrom__c != null';
		if(!String.isBlank(condition)){
			soql += condition;
		}
		soql += ' ORDER BY FormNo__c ASC';
		return Database.query(soql);
	}

	/**
	 * Web約款検索
	 * 子コンテンツ、添付ファイル取得
	 */
	public static List<I_ContentMaster__c> getContentAndAttachment(Set<Id> fileParentIdSet){
		List<I_ContentMaster__c> children = new List<I_ContentMaster__c>();
		children = [SELECT Id, YakkanType__c, ParentContent__c,
					(SELECT Id, Name, ParentId, BodyLength FROM Attachments WHERE ParentId IN :fileParentIdSet ORDER BY LastModifiedDate DESC) 
					FROM I_ContentMaster__c WHERE Id IN :fileParentIdSet AND YakkanType__c != null AND IsYakkanParentFlg__c = false AND isSitePublishFlg__c = true];
		return children;
	}
}