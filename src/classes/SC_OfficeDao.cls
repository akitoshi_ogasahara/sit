public with sharing class SC_OfficeDao {

	/**
	 * 事務所、年度を元に直前の自主点検事務所を取得する
	 * @param Integer 年度（数値）
	 * @param String 事務所コード
	 * @return List<SC_Office__c>
	 */
	public static List<SC_Office__c> getPrevSCOffice(Decimal fiscalYearNum, String officeId){
		return [SELECT
					Id
				FROM
					SC_Office__c
				WHERE
					AccZAGCYNUM__c = :officeId
				AND FiscalYearNum__c < :fiscalYearNum
				ORDER BY
					FiscalYearNum__c DESC
				LIMIT 1];
	}

	/**
	 * 事務所、年度を元に次の自主点検事務所を取得する
	 * @param Decimal 年度（数値）
	 * @param String 事務所コード
	 * @return List<SC_Office__c>
	 */
	public static List<SC_Office__c> getNextSCOffice(Decimal fiscalYearNum, String officeId){
		return [SELECT
					Id
				FROM
					SC_Office__c
				WHERE
					AccZAGCYNUM__c = :officeId
				AND FiscalYearNum__c > :fiscalYearNum
				ORDER BY
					FiscalYearNum__c
				LIMIT 1];
	}

	/**
	 * update前に最新のレコード値を取得
	 * @param Id SalesforceID
	 * @return List<SC_Office__c>
	 */
	public static List<SC_Office__c> getRefreshSCOffice(ID id){
		return [SELECT
					Id, Comment__c, StatusHistory__c, Inquiry__c
				FROM
					SC_Office__c
				WHERE
					ID = :id
				LIMIT 1];
	}

	/**
	 * 自主点検事務所IDをキーに、自主点検に紐づく添付ファイル情報を取得
	 * @param String 自主点検事務所ID
	 * @return List<SC_SelfCompliance__c>
	 */
	public static List<SC_SelfCompliance__c> getAttachmentsByOfficeId(String officeId){
		return [SELECT
					Id, Name, Type__c, LastModifiedDate, SC_Office__c, TypeShort__c
					,(SELECT Id, Name, LastmodifiedDate, LastmodifiedById, LastmodifiedBy.FirstName, LastmodifiedBy.LastName FROM Attachments)
				FROM
					SC_SelfCompliance__c
				WHERE
					SC_Office__c = :officeId
				ORDER BY
					TypeShort__c,
					LastModifiedDate DESC];
	}

	/**
	 * 自主点検事務所IDをキーに、レコードを取得する。
	 * @param  String 自主点検事務所ID
	 * @return SC_Office__c
	 */
	public static SC_Office__c getRecById(String recid){
		return [SELECT
					Id,
					Name,
					FiscalYear__c,
					Status__c,
					ZAGCYNUM__c,
					ZEAYNAM__c,
					OwnerMRUnitName__c,
					OwnerMRCode__c,
					OwnerMRName__c,
					LimitPlan__c ,
//					CMDReturnDate__c,
					CMDRemandDate__c,
					Defect__c,
					Inquiry__c,
					Comment__c,
					LimitFixed__c,
					LimitDefault__c,
					SendMailMR__c,
					SendMailManager__c,
					SendMailCMD__c,
					FiscalYearNum__c,
					StatusHistory__c,
					AccZAGCYNUM__c,
					RemandSource__c,
					AccZEAYNAM__c,  
					MRManager__c, 
					OwnerId, 
					DomainInternet__c, 
					DomainGw__c, 
					DomainEmployee__c,
					SendInquiryMailCMD__c,
					InquiryAgCmd__c,
					InquiryCmdAg__c,
					InquiryMrAg__c,
					// 業務管理責任者
					(Select
						Id, Contact__r.Name, User__r.Email, HasUserEmail__c, UserName__c, UserEmail__c, ContactName__c
					 From
					 	SC_AgencyManagers__r)
				FROM
					SC_Office__c
				WHERE
					ID = :recid];
	}
	
	/**
	 * リマインドメール送信対象の自主点検事務所を取得
	 * @param Set<Id> 自主点検事務所Idセット
	 * @return List<SC_Office__c>
	 */
	public static List<SC_Office__c> getRecsForRemindMail(Set<Id> ids){
		return [SELECT
					Id, MRManager__r.Id, Account__r.OwnerId, FiscalYear__c, AccZEAYNAM__c, AccZAGCYNUM__c, OwnerMRUnitName__c
					, DomainInternet__c, DomainGw__c, DomainEmployee__c, Account__r.Owner.Email, IsHeadSection__c
					,(Select Id, User__r.Id From SC_AgencyManagers__r Where HasUserEmail__c = true Limit 5) 
				FROM
					SC_Office__c
				WHERE
					Id IN :ids];
	}

	/**
	 * 代理店格コードのセットをキーにレコードリスト取得
	 * 	業務管理責任者作成用
	 */
	public static List<SC_Office__c> getRecsByParentZHEADAYs(Set<String> parentCodes){
		return [Select Id, Account__r.E_ParentZHEADAY__c, AccZHEADAY__c From SC_Office__c Where Account__r.E_ParentZHEADAY__c In :parentCodes And (FiscalYear__c Not In ('2016', '2017'))];
	}

	/**
	 * IDのセットをキーにレコードリスト取得
	 * 	業務管理責任者作成用
	 */
	public static List<SC_Office__c> getRecsByIds(Set<Id> ids){
		return [Select 
					Id, Account__r.E_ParentZHEADAY__c, AccZHEADAY__c, AccParentId__c,
					(Select Id From SC_AgencyManagers__r)
				From 
					SC_Office__c 
				Where 
					Id In :ids 
				And 
					(FiscalYear__c Not In ('2016', '2017'))];
	}
}