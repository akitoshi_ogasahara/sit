/**
*  クラス名	:	CommonConst
*  クラス概要	:	クラウドシステム定数クラス
*  @created	:	2014/01/21 Hung Nguyen The
*  @modified:
*/
public class CommonConst {
	// ファイナンス情報obj.カスタムオブジェクトID
	public static final String STR_PARAMETER_FIID = 'fiid';
	// 現在のページURL
	public static final String STR_PARAMETER_RETURL = 'returl';
	// ファイナンス情報obj.顧客obj.カスタムオブジェクトID
	public static final String STR_PARAMETER_CUID = 'cuid';
	// カスタムオブジェクトID
	public static final String STR_PARAMETER_ID = 'id';
	// なし
	public static final String STR_EMPTY = '';
	// ＋
	public static final String STR_PARAMETER_PLUS_SIGN = '＋';
	public static String getSTR_PARAMETER_PLUS_SIGN(){
		return STR_PARAMETER_PLUS_SIGN;
	}
	// -
	public static final String STR_PARAMETER_MINUS_SIGN = '－';
	public static String getSTR_PARAMETER_MINUS_SIGN(){
		return STR_PARAMETER_MINUS_SIGN;
	}
	// -
	public static final String STR_PARAMETER_MULTIPLY_SIGN = 'ｘ';
	public static String getSTR_PARAMETER_MULTIPLY_SIGN(){
		return STR_PARAMETER_MULTIPLY_SIGN;
	}
	// = 
	public static final String STR_PARAMETER_EQUAL_SIGN = '＝';
	public static String getSTR_PARAMETER_EQUAL_SIGN(){
		return STR_PARAMETER_EQUAL_SIGN;
	}
	// ｜｜
	public static final String STR_PARAMETER_LINE_SIGN = '||';
	public static String getSTR_PARAMETER_LINE_SIGN(){
		return STR_PARAMETER_LINE_SIGN;
	}
}