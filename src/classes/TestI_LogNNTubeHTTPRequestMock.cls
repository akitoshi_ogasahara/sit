@isTest
public class TestI_LogNNTubeHTTPRequestMock implements HttpCalloutMock {

	protected Integer code;
	protected String status;
	protected String body;
	protected Map<String, String> responseHeaders;
	public Boolean errorFlg {get;set;}

	public TestI_LogNNTubeHTTPRequestMock(Boolean errFlg) {
		this.errorFlg = errFlg;
	}

	public HTTPResponse respond(HTTPRequest req) {
		System.assertEquals('GET',req.getMethod());

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String body = '';
		if(this.errorFlg){
			body = '{"resultcode":"2601", "data":[ {"ActionType":"動画", "AccessPage":"https://www.nntube.jp/ad/player.html?mid=f100", ' +
			 '"AccessUser":"", "AccessDatetime":"2017/8/31 10:00:00", "DisplayURL":"https://www.nntube.jp/auth.aspx?type=3&mid=100", ' +
			 '"SummaryName":"【動画視聴】財務分析ノート：1 全体の構成と概要", "AppName":"NNTube", ' +
			 '"TubeLogSeq":"playerlog_344"}, {"ActionType":"動画", '+
			 '"AccessPage":"https://www.nntube.jp/ad/pdf/taisyokuhayamihyou.pdf", ' +
			 '"AccessUser":"", "AccessDatetime":"2017/8/31 10:10:00", ' +
			 '"DisplayURL":"https://www.nntube.jp/auth.aspx?type=6&mid=200&title=退職金提案のためのツール解 説&link_title=資料はこちら&link_url=https://www.nntube.jp/ad/pdf/taisyokuhayamihyou.pdf",' +
			 '"SummaryName":"【関連リンク】退職金提案のためのツール解説/資料はこちら", "AppName":"NNTube", "TubeLogSeq":"linklog_123"} ] }';

			res.setBody(body);
			res.setStatusCode(200);
			return res;
		}
		body = '{"resultcode":"0000", "data":[ {"ActionType":"動画", "AccessPage":"https://www.nntube.jp/ad/player.html?mid=f100", ' +
			 '"AccessUser":"", "AccessDatetime":"2017/8/31 10:00:00", "DisplayURL":"https://www.nntube.jp/auth.aspx?type=3&mid=100", ' +
			 '"SummaryName":"【動画視聴】財務分析ノート：1 全体の構成と概要", "AppName":"NNTube", ' +
			 '"TubeLogSeq":"playerlog_344"}, {"ActionType":"動画", '+
			 '"AccessPage":"https://www.nntube.jp/ad/pdf/taisyokuhayamihyou.pdf", ' +
			 '"AccessUser":"", "AccessDatetime":"2017/8/31 10:10:00", ' +
			 '"DisplayURL":"https://www.nntube.jp/auth.aspx?type=6&mid=200&title=退職金提案のためのツール解 説&link_title=資料はこちら&link_url=https://www.nntube.jp/ad/pdf/taisyokuhayamihyou.pdf",' +
			 '"SummaryName":"【関連リンク】退職金提案のためのツール解説/資料はこちら", "AppName":"NNTube", "TubeLogSeq":"linklog_123"} ] }';

		res.setBody(body);
		res.setStatusCode(200);
		return res;
	}

}