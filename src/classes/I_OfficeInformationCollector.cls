public with sharing class I_OfficeInformationCollector {

	public I_AddresseeInformationDTO dto {get; set;}
	public String keyword {get; set;}
	private E_AccessController access;
	public List<SearchResultInfo> results {get; set;}

	// メッセージ
	public List<String> searchMessages {get; set;}
	// メッセージあり
	public Boolean getHasMessages(){
		return searchMessages != null && searchMessages.size() > 0;
	}
	//メッセージMap
	public Map<String,String> getMSG(){
		return E_Message.getMsgMap();
	}
	//ディスクレイマーMap
	public Map<String,String> getDISCLAIMER(){
		return E_Message.getDisclaimerMap();
	}

	public I_OfficeInformationCollector() {
		access = E_AccessController.getInstance();
		if(access.isAuthManager() || access.isAuthMR() || access.isEmployee()) return;
		search();
	}

	public PageReference search() {
		searchMessages = new List<String>();

		Id parentId = access.user.Contact.Account.ParentId;
		if(parentId == null) return null;
		String soqlWhere = 'AND ParentId = \'' + parentId + '\' ';
		List<Account> offices = E_AccountDaoWithout.getRecsIRISAgencySearchBoxOffice(soqlWhere, this.keyword);

		// 検索結果がゼロ件の場合は処理終了
		if(offices == null || offices.isEmpty()){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_NOT_FOUND));
			return null;
		}

		//最大件数以上の場合は警告を表示
		if(offices.size() >= I_Const.LIST_MAX_ROWS){
			searchMessages.add(getMSG().get(I_Const.MESSAGE_KEY_RECORD_OVER_LIMIT));
			return null;
		}

		results = new List<SearchResultInfo>();
		for(Account acc : offices){
			SearchResultInfo result = new SearchResultInfo();
			result.officeName = acc.E_CL2PF_ZEAYNAM__c;
			result.officeNameKana = acc.E_CL2PF_ZEAYKNAM__c;
			result.officeCode = acc.E_CL2PF_ZAGCYNUM__c;
			result.postalCode = acc.CLTPCODE__c;
			result.address = acc.E_COMM_ZCLADDR__c;
			result.phone = acc.CLTPHONE01__c;
			results.add(result);
		}
		this.sortType = SORT_TYPE_OFFICE_NAME;
		this.sortIsAsc = TRUE;
		list_sortType = sortType;
		list_sortIsAsc = sortIsAsc;
		results.sort();
		this.rowCount = I_Const.LIST_DEFAULT_ROWS;
		return null;
	}

// ソート用
	// ソートキー
	public String sortType {get; set;}
	// ソート順
	public Boolean sortIsAsc {get; set;}
// ソート用
	// ソートキー
	public static String list_sortType {get; set;}
	// ソート順
	public static Boolean list_sortIsAsc {get; set;}
// ローカル定数
	private static final String SORT_TYPE_OFFICE_NAME = 'officeName';
	private static final String SORT_TYPE_OFFICE_CODE = 'officeCode';
	private static final String SORT_TYPE_ADDRESS     = 'address';

	/**
	 * ソート
	 */
	public void sortRows(){
		String sType = ApexPages.currentPage().getParameters().get('st');

		if(sortType != sType){
			sortType  = sType;
			sortIsAsc = true;
		} else {
			sortIsAsc = !sortIsAsc;
		}
		list_sortType  = sortType;
		list_sortIsAsc = sortIsAsc;

		results.sort();

		return;
	}

	public class SearchResultInfo implements Comparable{
		// 事務所名
		public String officeName {get; set;}
		// 事務所名カナ
		public String officeNameKana {get; set;}
		// 事務所コード
		public String officeCode {get; set;}
		// 郵便番号
		public String postalCode {get; set;}
		// 住所
		public String address {get; set;}
		// 電話番号
		public String phone {get; set;}

		public SearchResultInfo(){
			officeName = '';
			officeNameKana = '';
			officeCode = '';
			postalCode = '';
			address = '';
			phone = '';
		}

		// ソート
		public Integer compareTo(Object compareTo){
			SearchResultInfo compareToRow = (SearchResultInfo)compareTo;
			Integer result = 0;

			// 事務所名（カナ）
			if(list_sortType == SORT_TYPE_OFFICE_NAME){
				result = I_Util.compareToString(officeNameKana, compareToRow.officeNameKana);
			// 事務所コード
			} else if(list_sortType == SORT_TYPE_OFFICE_CODE){
				result = I_Util.compareToString(officeCode, compareToRow.officeCode);
			// 住所
			} else if(list_sortType == SORT_TYPE_ADDRESS){
				result = I_Util.compareToString(address, compareToRow.address);
			}

			return list_sortIsAsc ? result : result * (-1);
		}
	}

	/**
	 * 総行数取得
	 */
	public Integer getTotalRows(){
		return results == null ? 0 : results.size();
	}

	// 表示件数
	public Integer rowCount {get; set;}
	/**
	 * 表示行数追加
	 */
	public PageReference addRows(){
		rowCount += I_Const.LIST_ADD_MORE_ADD_ROWS;
		if(rowCount > I_Const.LIST_MAX_ROWS){
			rowCount = I_Const.LIST_MAX_ROWS;
		}
		return null;
	}
}