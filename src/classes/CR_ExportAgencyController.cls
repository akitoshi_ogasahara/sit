public with sharing class CR_ExportAgencyController extends E_CSVExportController{

	public override String mainSObjectAPIName(){
		return Schema.SObjectType.CR_Agency__c.Name;
	}

	/**
	 *	Constructor
	 */
	public CR_ExportAgencyController(){
		super();
		
		//項目の追加
		addColumn(new E_CSVColumnDescribe.BaseField('Status__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('MRUnit__c'));
		addColumn(new E_CSVColumnDescribe.ParentStringField('MR__r.Name','User','MR名'));	
		addColumn(new E_CSVColumnDescribe.BaseField('AgencyCode__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('AgencyName__c'));
		addColumn(new E_CSVColumnDescribe.DatetimeField('LastModifiedDate','最終更新日時'));
		//addColumn(new E_CSVColumnDescribe.BooleanField('OutOfBusiness__c'));
		addColumn(new E_CSVColumnDescribe.BaseField('BusinessStatus__c'));
	}

}