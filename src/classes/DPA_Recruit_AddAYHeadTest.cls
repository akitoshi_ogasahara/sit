@isTest
private class DPA_Recruit_AddAYHeadTest {
    
	@isTest
    static void DPA_Recruit_AH_AddTest() {
        // プロファイルを取得する
        Profile p = [select Id from Profile where Name = 'MR'];

        // ユーザを作成する
        User u = new User();
        u.LastName = 'test';
        u.Alias = 'test';
        u.Email = 'test' + Date.Today().format() + '@test.com';
        u.UserName = u.Email;
        u.EmailEncodingKey = 'ISO-2022-JP';
        u.TimeZoneSidKey = 'Asia/Tokyo';
        u.LocaleSidKey = 'ja_JP';
        u.LanguageLocaleKey = 'ja';
        u.profileId = p.Id;
        insert u;
 
		Test.startTest();
 
        // 作成したユーザで処理を実行
        System.runAs(u){ 
            // テスト
            // １レコード新規作成
            DPA_Recruit_AH__c ah = new DPA_Recruit_AH__c(Name = 'Test',
                                                         RecruitStatus__c = 'TA名簿アップロード',
                                                         PostalCode__c = '1112222',
                                                         BotenAddress__c = '東京都',
                                                         BotenAddressLocStatus__c = '未設定',
                                                         RecruitClassification__c = '一般代理店'
                                                        );
            insert ah;
        
            List<DPA_Recruit_AH__c> updateList = [select OwnerToUpdate__c, OwnerId from DPA_Recruit_AH__c where Name = 'Test'];
            for(DPA_Recruit_AH__c ah2 : updateList) {
                ah2.OwnerToUpdate__c = ah2.OwnerId; // これでも問題ないはず！
                update ah2;            
            }
        }
 
		Test.stopTest();
        
    }
}