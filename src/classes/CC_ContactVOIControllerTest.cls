/*
 * CC_ContactVOIControllerTest
 * Test class of CC_ContactVOIController
 * created  : Accenture 2018/7/9
 * modified :
 */

@isTest
private class CC_ContactVOIControllerTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Test invokeMethod()
	* methodName is saveAgent
	*/
	static testMethod void invokeMethodTest01() {
		CC_ContactVOIController contactVOIController = new CC_ContactVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveAgent';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			contactVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* methodName is saveClient
	*/
	static testMethod void invokeMethodTest02() {
		CC_ContactVOIController contactVOIController = new CC_ContactVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveClient';
			Map<String, Object> inputMap = new Map<String, Object>();
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			contactVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

	/**
	* Test invokeMethod()
	* Exception
	*/
	static testMethod void invokeMethodTest03() {
		CC_ContactVOIController contactVOIController = new CC_ContactVOIController();
		System.runAs ( testUser ) {

			//Prepare test data
			String methodName = 'saveClient';
			Map<String, Object> inputMap = null;
			Map<String, Object> outputMap = new Map<String, Object>();
			Map<String, Object> optionMap = new Map<String, Object>();

			Test.startTest();
			contactVOIController.invokeMethod(methodName, inputMap, outputMap, optionMap);
			Test.stopTest();

		}
	}

}