public with sharing class I_ColiController extends I_AbstractPIPController {
	private static final String PAGE_TITLE = '保険契約概要';
	
	// 保険契約ヘッダId
	//private String policyId;

	// 主契約（個人保険特約）
	public E_COVPF__c mainCovpf {get; set;}
	// 個人契約ヘッダ
	public E_Policy__c policy {get; private set;}
	// ダウンロード履歴
	public E_DownloadHistorry__c dh {get; set;}
	// 個人保険特約リスト
	public List<E_COVPF__c> covpfs {get; set;}
	// 団体フラグ
	public Boolean isGroup {get; set;}
	// 団体名
	public String groupName {get; set;}
	// リスト行数_手続履歴
	public Integer rowCountHistory {get; set;}
	// PDFコメント
	public String pdfComment {get; set;}
	//推移表遷移時エラーメッセージ
	public String modalMessage {get; private set;}
	//推移表表示可能フラグ
	public Boolean isDisplayMeritChart{get; private set;}

	/**
	 * 表示募集人取得
	 */
	public Boolean getIsViewMainAgent(){
		Boolean isViewMainAgent = true;
		
		// 従募集人が存在しない場合、Mainを表示する
		if(this.policy.SubAgent__c == null){
			return true;
		}

		// AH
		if(access.isAuthAH()){
			// 従代理店格コードと一致するか
			isViewMainAgent = I_ColiUtil.isViewMain(access.getAccountCode()
										, this.policy.MainParentAccountE_CL1PF_ZHEADAY__c
										, this.policy.SubParentAccountE_CL1PF_ZHEADAY__c);
		// AY
		}else if(access.isAuthAY()){
			// 従事務所コードと一致するか
			isViewMainAgent = I_ColiUtil.isViewMain(access.getOfficeCode()
										, this.policy.MainAgentAccountCL2PF_ZAGCYNUM__c
										, this.policy.SubAgentAccountCL2PF_ZAGCYNUM__c);
		// AT
		}else if(access.isAuthAT()){
			// 募集人コードと一致するか
			isViewMainAgent = I_ColiUtil.isViewMain(access.user.Contact.AgentNum5__c
										, this.policy.MainAgentNum5__c
										, this.policy.SubAgentNum5__c);
		}
		
		return isViewMainAgent;
	}

	/**
	 * 失効フラグ
	 */
	public Boolean getIsDisablePolicy(){
		return I_PolicyUtil.getIsDisablePolicy(this.policy);
	}
	
	/**
	 * 口座情報表示フラグ
	 */
	public Boolean getIsViewBank(){
		return I_PolicyUtil.getIsViewBank(this.policy);
	}
	
	/**
	 * 担当者一覧
	 */
	public I_COLIDatas.Charge getCharge(){
		I_COLIDatas.Charge charge = I_ColiUtil.getCharge(this.policy);
		return charge;
	}

	/**
	 * 保障内容
	 */
	public List<I_COLIDatas.Cover> getCovers(){
		return I_ColiUtil.getCovers(this.covpfs, this.mainCovpf);
	}
	
	/** 
	 * 契約者
	 */
	public List<I_COLIDatas.Contractor> getContractors(){
		List<I_COLIDatas.Contractor> contractors = new List<I_COLIDatas.Contractor>();
		contractors.add(I_ColiUtil.getContractor(this.policy));
		return contractors;
	}
	
	/** 
	 * 被保険者（主たる被保険者） 
	 */
	public List<I_COLIDatas.Insured> getInsureds(){
		List<I_COLIDatas.Insured> insureds = new List<I_COLIDatas.Insured>();
		insureds.add(I_ColiUtil.getInsured(this.policy));
		return insureds;
	}

	/** 
	 * 受取人
	 */
	public List<I_COLIDatas.Beneficiary> getBeneficiarys(){
		List<I_COLIDatas.Beneficiary> beneficiarys = new List<I_COLIDatas.Beneficiary>();
		
		// 主契約の保険種類コードが『JA』まはた『FA』の場合、固定行を生成 
		if(mainCovpf.COMM_CRTABLE2__c == E_Const.COVPF_CRTABLE2_JA || mainCovpf.COMM_CRTABLE2__c == E_Const.COVPF_CRTABLE2_FA){
			beneficiarys.add(I_ColiUtil.getBeneficiary(null, true));
		}else{
			for(E_CRLPF__c crlpf : E_CRLPFDao.getRecsForColi(this.policyId)){
				beneficiarys.add(I_ColiUtil.getBeneficiary(crlpf, false));
			}
		}

		return beneficiarys;
	}
	
	/**
	 * 払込・口座情報
	 */
	public I_COLIDatas.Bank getBank(){
		return I_ColiUtil.getBank(this.policy, access.isAgent());
	}

	/**
	 * 解約時受取金額
	 */
	public I_COLIDatas.Surrender getSurrender(){
		return I_ColiUtil.getSurrender(this.policy);
	}
	
	/**
	 * 前納
	 */
	public I_COLIDatas.Advance getAdvance(){
		return I_ColiUtil.getAdvance(this.policy);
	}
	
	/**
	 * 質権設定
	 */
	public List<I_ColiDatas.Pledge> getPledges(){
		List<I_ColiDatas.Pledge> pledges = new List<I_ColiDatas.Pledge>();
		for(E_CRLPF__c crlpf : E_CRLPFDao.getRecsByRoleNE(this.policyId)){
			pledges.add(I_ColiUtil.getPledge(crlpf));
		}
		return pledges;
	}
	
	/**
	 * 契約者貸付
	 */
	public I_COLIDatas.Lone getLone(){
		return I_ColiUtil.getLone(this.policy);
	}

	/**
	 * 保険料自動振替貸付
	 */
	public I_COLIDatas.AutoLone getAutoLone(){
		return I_ColiUtil.getAutoLone(this.policy);
	}

	/** 
	 * 契約に関する通知
	 */
	private I_COLIDatas.Notification notification;
	public I_COLIDatas.Notification getNotification(){
		if(notification == null){
			notification = I_ColiUtil.getNotification(this.policyId);
		}
		return notification;
	}
	//public I_COLIDatas.Notification getNotification(){
	//	return I_ColiUtil.getNotification(this.policyId);
	//}

	/** 
	 * 手続履歴
	 */
	public List<I_COLIDatas.ProcessHistory> getProcessHistorys(){
		List<I_COLIDatas.ProcessHistory> processHistorys = new List<I_COLIDatas.ProcessHistory>();
		for(E_PTNPF__c ptnpf : E_PTNPFDao.getRecsByPolicyid(this.policyId)){
			processHistorys.add(I_ColiUtil.getProcessHistory(ptnpf));
		}
		return processHistorys;
	}
	
	/**
	 * Constructor
	 */
	public I_ColiController(){
		super();
		pageMessages = getPageMessages();
		pgTitle = PAGE_TITLE;
		
		this.policyId = ApexPages.CurrentPage().getParameters().get('id');
		this.mainCovpf = new E_COVPF__c();
		this.covpfs = new List<E_COVPF__c>();
		
		// デフォルト行数10行
		rowCountHistory = 10;
	}
	
	/**
	 * init
	 */
	protected override PageReference init(){
		// LogName
		pageAccessLog.Name = PAGE_TITLE;
		
		// アクセス判定
		kind = E_Const.ID_KIND.POLICY;
		authTargetId = this.policyId;

		//共同GW用エラーメッセージ
		modalMessage = '共同ゲートウェイをご利用の方は現在推移表機能をご利用いただけません。<br/>今後対応予定です。ご不便をおかけして申し訳ございませんが、対応完了までお待ちください。';
		
		PageReference pr = super.init();
		if(pr != null){
			// PIPからの遷移判定のパラメータ付与
			pr.getParameters().put(I_Const.ERR_URLPARAM_KEY, I_Const.ERR_URLPARAM_PIP);
			return pr;
			
		}else{
			// 保険契約ヘッダ取得
			this.policy = E_PolicyDao.getRecByIdForIRISColi(this.policyId);
			
			// 個人保険特約取得
			this.covpfs = E_COVPFDao.getRecsByPolicyIdAndRtdevname(this.policyId, E_Const.COVPF_RECORDTYPE_ECOVPF);
			
			// 主契約セット
			setMainCovpf(this.covpfs);
			
			// 団体情報セット
			setGroupInfo();

			I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(this.policy,this.covpfs);
			isDisplayMeritChart = checker.isAvailableProduct();
            
			return null;
		}
	}

	/**
	 * PDFダウンロードボタン押下
	 */
	public PageReference doDownloadPDF(){
		try{
			pageMessages.clearMessages();

			// 事務所と募集人の設定
			String officeName = '';
			String agentName = '';
			if(getIsViewMainAgent()){
				officeName = policy.MainAgentAccountName__c;
				agentName = policy.MainAgentName__c;
			}else{
				officeName = policy.SubAgentAccountName__c;
				agentName = policy.SubAgentName__c;
			}

			// JSON作成
			//String json = I_ColiUtil.createDlJson(this.policyId, officeName, agentName, getDataSyncDateHead(), getIsIss(), E_Const.POLICY_RECORDTYPE_COLI, getNotification().dto.noticeList);
			String json = I_ColiUtil.createDlJson(this.policyId, officeName, agentName, groupName, getDataSyncDateHead(), getIsIss(), E_Const.POLICY_RECORDTYPE_COLI, notification.dto.noticeList);
	
			// ダウンロード履歴Insert
			this.dh = I_ColiUtil.createDownloadHistorry(json, this.pdfComment);
			// 文字切り捨て
			Database.DMLOptions dml = new Database.DMLOptions();
			dml.allowFieldTruncation = true;
			this.dh.setOptions(dml);
            insert this.dh;

		}catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
		}

		// コメント初期化
		this.pdfComment = '';

		return null;
	}	

	/**
	 * 表示行数追加
	 */
	public PageReference addRowsHistory(){
		try{
			// 現在の表示行数+10
			rowCountHistory += 10;
			
			// 現在の表示行数が1000より大きくなった場合、表示行数は1000
			if(rowCountHistory > I_Const.LIST_MAX_ROWS){
				rowCountHistory = I_Const.LIST_MAX_ROWS;
			}
		}catch(Exception e){
			pageMessages.addErrorMessage(e.getMessage());
		}
		return null;
	}

	/**
	 * 主契約
	 */
	private void setMainCovpf(List<E_COVPF__c> covpfs){
		// 個人保険特約
		for(E_COVPF__c covpf : covpfs){
			// 保障内容　C：主契約、C以外：特約
			if(covpf.COLI_ZCRIND__c.equals(I_Const.COVPF_COLI_ZCRIND_C) ){
				this.mainCovpf = covpf;
				break;
			}
		}
	} 
	
	/**
	 * 団体情報
	 */
	private void setGroupInfo(){
		// 団体判定  団体表示フラグ==true かつ 次回保険料払込期月フラグ == true
		if(this.policy.COLI_ZGRUPDCF__c && this.policy.COLI_ZNPTDDCF__c){
			this.isGroup = true;
			this.groupName = I_PolicyUtil.getCorporationName(this.policy.COLI_ZGCLTNM__c);
		}else{
			this.isGroup = false;
			this.groupName = '';
		}
	} 

    /**
     *  解約請求書ボタン表示制御
     */
    public Boolean getShowCancelDLBtn(){

    	// 失効判定
        if (getIsDisablePolicy()) {
            return false;
        }

        // 質権設定
        if (getPledges().size() > 0) {
            return false;
        }

		// 保険種類コード（数式）
        String crtable = mainCovpf.COMM_CRTABLE2__c;

        // ・新医療保険とSPWH
        if (I_Const.CRTABLE2_NEW_SPWH.contains(crtable)) {
            return false;
        }

		// ・家族特約（子型）
        for(E_COVPF__c covpf : covpfs){
            String crtable2 = covpf.COMM_CRTABLE2__c;
            if (I_Const.CRTABLE2_FAML.contains(crtable2)) {
                return false;
            }
        }

        // 日付の比較は文字列の比較で代用できる
        if (I_Const.CRTABLE2_OLDR.contains(crtable) && (policy.COMM_OCCDATE__c <= '19901031')) {
            return false;
        }

        // ・共同募集契約　かつ　主と従が違う
        // 　数式項目。代理店格コードで判断。※仕様変更がありそうのため
        if (policy.IsCommonCoverage__c){
            return false;
        }

        // ・解約返戻金計算不能フラグ＝True
        if (policy.COLI_ZCVDCF__c) {
            return false;
        }

        // ・契約者法人／個人区分（数式）が(P:個人, C:法人)以外
        if (!I_Const.COLI_ZCNTRDSC.contains(policy.ContractorCLTPF_CLTTYPE__c)) {
			return false;
        }

		return true;
        
    }


    /**
     * 解約請求書DLボタン処理
     */
    public void doDownloadCancelForm(){
    	try{
    		String fileName = '解約請求書' + '(' + I_Const.COLI_ZCNTRDSC_LABEL.get(policy.ContractorCLTPF_CLTTYPE__c) + ')';

	    	// 解約請求書ダウンロード履歴作成
	    	E_CancellFormDownloadHistory__c cancelDlHistory = insertHistoryCancelDl(filename);
            
	    	// アクセスログ登録
	    	insertLogCancelDl(policy, cancelDlHistory.Id, fileName);

    	}catch(Exception e){
    		pageMessages.addErrorMessage('解約請求書ダウンロード処理でエラーが発生しました。' + e.getMessage());
    	}
    }

    /**
     * 解約請求書ダウンロード履歴作成
     */
    private E_CancellFormDownloadHistory__c insertHistoryCancelDl(String filename){
		// 数式ではなくロジックで実装　(実行順序を考慮するため)
		boolean toNotifyFlag = false;
		//※ダウンロード募集人は主募集人と別代理店の場合、Account.Owner.ProfileIdを見れないため、WithOutDaoで取得
		Account mainAgentAccount_WithOut = E_AccountDaoWithOut.getOwner(policy.MainAgentAccount__c);
        Id ownerProfileId = mainAgentAccount_WithOut.Owner.ProfileId;
        /** 主募集人の担当MRか代理店拠点長宛てに通知メールを送信する*/
		if ((E_ProfileDao.isMR(ownerProfileId) 
            || E_ProfileDao.isMRManager(ownerProfileId))
            //本社営業以外
            && policy.MainAgentAccount__r.KSECTION__c != E_Const.AGENCY_KSECTION_HEAD_OFFICE) {
                //募集人がダウンロードした場合のみ通知メールを送信する
                toNotifyFlag = access.isAgent();
        }
        
        User DLUser = new User();
        id DLUserId = Userinfo.getUserId();
        if (access.isAgent()) {
            DLUser = [ Select Id, Contact.Name, Contact.E_AccParentName__c, Contact.E_AccParentCord__c, Contact.AgentNum5__c 
                       From User Where Id = :DLUserId ];
        }
        
        //解約DL契約レコードの作成
        Id OwnerId = mainAgentAccount_WithOut.OwnerId;
		E_CancellFormDownloadHistoryCreator dlHistory = new E_CancellFormDownloadHistoryCreator();
		E_CancellFormDownloadHistory__c cancelDlHistory = dlHistory.createCancellationDLLog(policy, mainCovpf, filename, toNotifyFlag, DLUser, OwnerId);

		return cancelDlHistory;
    }

	/**
	 * 	アクセスログ登録
	 */
	private void insertLogCancelDl(E_Policy__c policy, ID historyId, String fileName){
		// ログレコード生成
		E_Log__c log = E_LogUtil.createLog();

		// ログ名 ： 保険契約概要_解約請求書（個人/法人）
		log.Name = '保険契約概要_' + policy.COMM_CHDRNUM__c + '_' + policy.ContractorName__c + '_' + fileName;
		// 一覧表示名
		log.SummaryName__c = log.Name;
		// アクションタイプ ： PDF出力
		log.ActionType__c = I_Const.ACTION_TYPE_PDF;
		// レコードID　 ： 解約請求書契約一覧（E_CancellFormDownloadHistory__c）
		log.RecordId__c = historyId;
		// 一覧表示フラグ ： false
		log.isDisplay__c = false;
		// アクセスページ  ： 文字数オーバーになるため空文字更新
		log.AccessPage__c = '';

		insert log;
	}

	/**
	 * 	推移表ボタン押下時の挙動
	 */	
	public PageReference moveToMeritTableChart(){
		I_PIPMeritTableAvailabilityChecker checker = new I_PIPMeritTableAvailabilityChecker(policy,covpfs);
		if(checker.isAvailableContract()){
			if(checker.isServiceTime()){
				PageReference pr = Page.IRIS_MeritTableChart;
				pr.getParameters().put('policyId',policy.Id);
				return pr;
			}else{
				//サービス時間外の場合
				//modalMessage = I_PIPConst.ERROR_MESSAGE_OUT_OF_SERVICE;
				modalMessage = getMSG().get(I_PIPConst.MESSAGE_KEY_COLI_OUT_OF_SERVICE_TIME);
			}
		}else {
			//対象外契約の場合
			//modalMessage = I_PIPConst.ERROR_MESSAGE_UNAVEIRABLE_CONTRACT;
			modalMessage = getMSG().get(I_PIPConst.MESSAGE_KEY_COLI_NOTAVAILABLE_PRODUCT);
		}
		return null;
	}


}