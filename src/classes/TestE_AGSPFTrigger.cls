@isTest
private class TestE_AGSPFTrigger {
    
    private static testMethod void testBeforeInsertAndUpdate_001() {
        Account acc = TestE_TestUtil.createAccount(true);
        Contact con = TestE_TestUtil.createContact(true ,acc.id ,'テスト契約者' ,'11111' ,'22222' );
        E_Policy__c plcy = TestE_TestUtil.createPolicy(true , con.id , 'ECHDPF', '33333');

        E_CHTPF__c chtpf1 = createE_CHTPF(true ,plcy.Id ,'33333' , 1);
        E_CHTPF__c chtpf2 = createE_CHTPF(true ,plcy.Id ,'44444' , 1);

        Test.StartTest();
        List<E_AGSPF__c> agspfs = new List<E_AGSPF__c>();
        E_AGSPF__c agspf1 = createE_AGSPF(false , '2', '33333', 1);
        agspfs.add(agspf1);
        E_AGSPF__c agspf2 = createE_AGSPF(false , '1', '44444', 1);
        agspfs.add(agspf2);

        insert agspfs;

        E_AGSPF__c result1 = [Select E_CHTPF__c From E_AGSPF__c Where id = :agspf1.id];
        E_AGSPF__c result2 = [Select E_CHTPF__c From E_AGSPF__c Where id = :agspf2.id];

        system.assertEquals(result1.E_CHTPF__c,chtpf1.Id);
        system.assert(result2.E_CHTPF__c == null);

        agspf2.KDATATYP__c = '2';

        update agspfs;

        result1 = [Select E_CHTPF__c From E_AGSPF__c Where id = :agspf1.id];
        result2 = [Select E_CHTPF__c From E_AGSPF__c Where id = :agspf2.id];

        system.assertEquals(result1.E_CHTPF__c,chtpf1.Id);
        system.assertEquals(result2.E_CHTPF__c,chtpf2.Id);

        Test.stopTest();
    }
    private static E_CHTPF__c createE_CHTPF(Boolean isInsert , ID pId ,String CHDRNUM ,Decimal ZATSEQNO) {
        E_CHTPF__c src = new E_CHTPF__c(
                            E_Policy__c = pId
                            , CHDRNUM__c = CHDRNUM
                            , ZATSEQNO__c = ZATSEQNO
                            );
        if(isInsert) insert src;

        return src;
    }
    private static E_AGSPF__c createE_AGSPF(Boolean isInsert , String KDATATYP ,String CHDRNUM ,Decimal ZJINTSEQ2) {
        E_AGSPF__c src = new E_AGSPF__c(
                            KDATATYP__c = KDATATYP
                            , CHDRNUM__c = CHDRNUM
                            , ZJINTSEQ2__c = ZJINTSEQ2
                            );
        if(isInsert) insert src;

        return src;
    }
}