@isTest(SeeAllData=false)
public with sharing class TestE_EMailAddressChangeExtender {
	// test data
	private static User testuser;
	private static E_IDCPF__c idcpf;
	private static Account account;
	private static Contact contact;
	private static RecordType recordType;

	static void init(){
		insertE_bizDataSyncLog();
		system.debug('this is init');
		insertMessage();
		insertEmail();

	}

	//レコード1件の正常系
	private static testMethod void testStandard() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_EmailAddressChange;
		//pref.getParameters().put('Id', epolicy.Id);
		Test.setCurrentPage(pref);
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			//extender.getButtonMessage();
			System.debug('************************EXTpageRef11 = ' + extender.pageRef);
			extender.getInfoMessage();
			extender.mailAddress = 'aaa@aaa.aa';
			extender.mailAddressConfirm = 'aaa@aaa.aa';
			extender.saveMailAddress();
			System.debug('************************EXTpageRef12 = ' + extender.pageRef);
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
		//System.assertEquals(testuser.Username,'aaa@aaa.aa'+System.Label.E_USERNAME_SUFFIX);
	}

	//レコード1件の正常系 代理店ユーザ
	//Site.domain が動作しないためコメントアウト
	private static testMethod void testPartnerStandard() {
		Test.startTest();
		//createPartnerDataCommon('E_PartnerCommunity2');
		createPartnerDataCommon(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);
		init();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_EmailAddressChange;
		//pref.getParameters().put('Id', epolicy.Id);        Test.setCurrentPage(pref);

		Test.stopTest();
		system.runAs(testuser){
			E_CookieHandler.setCookieCompliance();
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			//extender.getButtonMessage();
			System.debug('************************EXTpageRef1 = ' + extender.pageRef);
			extender.getInfoMessage();
			System.debug('************************EXTpageRef2 = ' + extender.pageRef);
			extender.mailAddress = 'aaa@aaa.aa';
			extender.mailAddressConfirm = 'aaa@aaa.aa';
			System.debug('************************EXTpageRef3 = ' + extender.pageRef);
			extender.saveMailAddress();
			System.debug('************************EXTpageRef4 = ' + extender.pageRef);
			System.debug('************************pref4 = ' + pref);
			pref = extender.pageAction();
			System.debug('************************pref5 = ' + pref);
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		System.assertEquals(null, pref);
	}

	//レコード1件の正常系 非代理店ユーザ
	private static testMethod void testAgentStandard() {
		init();
		createDataCommon();
		//インスタンス
		User user = new user();
		PageReference pref = Page.E_EmailAddressChange;
		//pref.getParameters().put('Id', epolicy.Id);
		Test.setCurrentPage(pref);
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			//extender.getButtonMessage();
			System.debug('************************EXTpageRef11 = ' + extender.pageRef);
			extender.getInfoMessage();
			extender.mailAddress = 'aaa@aaa.aa';
			extender.mailAddressConfirm = 'aaa@aaa.aa';
			extender.saveMailAddress();
			System.debug('************************EXTpageRef12 = ' + extender.pageRef);
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
		//System.assertEquals(testuser.Username,'aaa@aaa.aa'+System.Label.E_USERNAME_SUFFIX);
	}

	//メールアドレス空欄の異常系
	private static testMethod void testErrorNoAddress() {
		init();
		createDataCommon();
		PageReference pref = null;
		//インスタンス
		User user = new user();
		Test.startTest();
		system.runAs(testuser){
			pagereference testPage = page.E_EMailAddressChange;
			test.setCurrentPage(testPage);
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			//extender.getButtonMessage();
			extender.getInfoMessage();
			extender.mailAddress = '';
			extender.mailAddressConfirm = '';
			pref = extender.saveMailAddress();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//メールアドレスの異常(形式異常)
	private static testMethod void testErrorAddress1() {
		init();
		createDataCommon();
		PageReference pref = null;
		//インスタンス
		User user = new user();
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
		E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
		E_EMailAddressChangeExtender extender = controller.getExtender();
		Test.startTest();
		system.runAs(testuser){
			extender.mailAddress = 'aaa@aaa';
			extender.mailAddressConfirm = 'aaa@aaa';
			pref = extender.saveMailAddress();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//メールアドレスの異常(アドレスの不一致)
	private static testMethod void testErrorAddress2() {
		init();
		createDataCommon();
		PageReference pref = null;
		//インスタンス
		User user = new user();
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(user);
		E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
		E_EMailAddressChangeExtender extender = controller.getExtender();
		Test.startTest();
		system.runAs(testuser){
			extender.mailAddress = 'aaa@aaa.aa.aa';
			extender.mailAddressConfirm = 'aaa@aaa.aa.ab';
			pref = extender.saveMailAddress();

			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
	}

	//メールアドレスの異常(同じアドレスへの変更)
	private static testMethod void testErrorAddress3() {
		init();
		createDataCommon();
		PageReference pref = null;
		//インスタンス
		User user = new user();
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(user);
		E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
		E_EMailAddressChangeExtender extender = controller.getExtender();
		Test.startTest();
		system.runAs(testuser){
			extender.mailAddress = 'fstest@terrasky.ingtesting';
			extender.mailAddressConfirm = 'fstest@terrasky.ingtesting';
			pref = extender.saveMailAddress();

			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, pref);
		System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, getMessage('MAC|004'));
	}

	//メールアドレスの異常(他のユーザが使っているアドレスへの変更)
	private static testMethod void testErrorAddress4() {
		//テストデータ作成
		createIRISCMS();
		createMessageData('CID|008','ご指定のメールアドレスはIDとして利用できません。');

		createPartnerDataCommon(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);
		init();
		createDataCommon2();
		PageReference pref = null;
		Test.startTest();
		system.runAs(testuser){
			E_CookieHandler.setCookieCompliance();
		//インスタンス
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			extender.mailAddress = 'aaa@aaa.aa';
			extender.mailAddressConfirm = 'aaa@aaa.aa';
			extender.saveMailAddress();
			pref = extender.pageAction();
			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
			System.assertEquals(extender.pageMessages.getErrorMessages()[0].summary, 'ご指定のメールアドレスはIDとして利用できません。');
		}
		Test.stopTest();
//		System.assertEquals(null, pref);
	}

	//クリアボタン押下
	private static testMethod void testClear() {
		init();
		createDataCommon();
		PageReference pref = null;
		//インスタンス
		User user = new user();
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(user);
		E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
		E_EMailAddressChangeExtender extender = controller.getExtender();
		Test.startTest();
		system.runAs(testuser){
			extender.mailAddress = 'fstest@terrasky.ingtesting';
			extender.mailAddressConfirm = 'fstest@terrasky.ingtesting';
			extender.clearMailAddress();

			//確認
			//System.assertEquals(extender.getAcceptanceMessage(),type );
		}
		Test.stopTest();
		System.assertEquals(null, extender.mailAddress);
		System.assertEquals(null, extender.mailAddressConfirm);
	}

	//戻るボタン押下 契約者・代理店ユーザ以外
	private static testMethod void testBack1() {
		init();
		createDataCommon();
		PageReference pref = null;
		//インスタンス
		User user = new user();
		Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(user);
		E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
		E_EMailAddressChangeExtender extender = controller.getExtender();
		Test.startTest();
		system.runAs(testuser){
			pref = extender.doReturn();
		}
		Test.stopTest();
		System.assertEquals(page.E_home.getURL(), pref.getUrl());
	}

	//戻るボタン押下 契約者ユーザ
	private static testMethod void testBack2(){
		init();
		createPartnerDataCommon('E_CustomerCommunity');
		PageReference pref = null;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			extender.getInfoMessage();
			pref = extender.doReturn();
		}
		Test.stopTest();
		System.assertEquals(Page.E_ContactInfo.getUrl() +'?id='+ testuser.ContactId, pref.getUrl());
	}

	//戻るボタン押下 代理店ユーザ
	private static testMethod void testBack3(){
		init();
		//createPartnerDataCommon('E_PartnerCommunity2');
		createPartnerDataCommon(E_Const.PROFILE_E_PARTNERCOMMUNITY_IA);
		PageReference pref = null;
		Test.startTest();
		system.runAs(testuser){
			Apexpages.Standardcontroller standardcontroller = new  Apexpages.Standardcontroller(testuser);
			E_EMailAddressChangeController controller = new E_EMailAddressChangeController(standardcontroller);
			E_EMailAddressChangeExtender extender = controller.getExtender();
			extender.getInfoMessage();
			pref = extender.doReturn();
		}
		Test.stopTest();
		System.assertEquals(Page.E_Home.getUrl(), pref.getUrl());
		System.assertEquals(1, [SELECT Id FROM E_IDRequest__c WHERE E_IDCPF__c = :idcpf.Id].size());
	}



	private static void insertMessage(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			LIST<E_MessageMaster__c> insMessage = new LIST<E_MessageMaster__c>();
			String type = 'メッセージ';
			String param = 'MAC|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|002';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|003';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|004';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|005';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAB|001';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));

			type = 'ディスクレーマー';
			param = 'MAC|006';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|007';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|008';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			param = 'MAC|009';
			insMessage.add(new E_MessageMaster__c(Value__C = param,Key__c = param,Name = param,Type__C = type));
			insert insMessage;
		}
	}

	private static String getMessage(String key){
		E_MessageMaster__c message = [SELECT Id, Name, Value__c FROM E_MessageMaster__c WHERE Key__c = :key];
		return message.Value__c;
	}

	/* test data */
	static void createDataCommon(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){

			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			testuser = new User(
				Lastname = 'fstest'
				, Username = 'fstest@terrasky.ingtesting'+ System.Label.E_USERNAME_SUFFIX
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
			);
			insert testuser;
			// Account
			account = new Account(Name = 'testAccount');
			insert account;

			// Contact
			contact = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account.Id);
			insert contact;


			//
			idcpf = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = testuser.Id,
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207', // terrasky
				ZDSPFLAG02__c = '0'
			);
			insert idcpf;
		}
	}

	/* test data */
	static void createDataCommon2(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){

			// User
			profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			User testuser2 = new User(
				Lastname = 'fstest2'
//				, Username = 'fstest2@terrasky.ingtesting'+ System.Label.E_USERNAME_SUFFIX
//				, Email = 'fstest2@terrasky.ingtesting'
				, Username = 'aaa@aaa.aa'+ System.Label.E_USERNAME_SUFFIX
				, Email = 'aaa@aaa.aa'
				, ProfileId = profile.Id
				, Alias = 'fstest2'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser2'
			);
			insert testuser2;
			// Account
			Account account2 = new Account(Name = 'testAccount');
			insert account2;

			// Contact
			Contact contact2 = new Contact(LastName = 'lastName', FirstName = 'firstName', AccountId = account2.Id);
			insert contact2;

			// EIDC
			E_IDCPF__c idcpf2 = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				User__c = testuser2.Id,
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207', // terrasky
				ZDSPFLAG02__c = '0'
			);
			insert idcpf2;
		}
	}

	/* partner test data */
	static void createPartnerDataCommon(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Profile profile = [Select Id, Name,usertype From Profile Where Name = 'システム管理者' Limit 1];
			userrole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
			//having role user
			User roleUser = new User(
				Lastname = 'roleuser'
				, Username = 'roleuser@terrasky.ingtesting'+ System.Label.E_USERNAME_SUFFIX
				, Email = 'roleuser@terrasky.ingtesting'
				, ProfileId = profile.Id
				, UserRoleId = portalRole.id
				, Alias = 'roleuser'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='roleuser'
			);
			insert roleuser;

			// Account
			account = new Account(Name = 'testAccount',ownerid = roleuser.id);
			insert account;

			// Contact
			contact = new Contact(LastName = 'fstest', FirstName = 'firstName', AccountId = account.Id,email = 'fstest@terrasky.ingtesting',ownerid = roleuser.id);
			insert contact;

			// User
			if(profilename != null){
				profile = [Select Id, Name,usertype From Profile Where Name = :profileName Limit 1];
			}
			testuser = new User(
				Lastname = 'fstest'
				, FirstName = 'firstName'
				, Username = 'fstest@terrasky.ingtesting'+ System.Label.E_USERNAME_SUFFIX
				, Email = 'fstest@terrasky.ingtesting'
				, ProfileId = profile.Id
				, Alias = 'fstest'
				, TimeZoneSidKey = UserInfo.getTimeZone().getID()
				, LocaleSidKey = UserInfo.getLocale()
				, EmailEncodingKey = 'UTF-8'
				, LanguageLocaleKey = 'ja'
				, CommunityNickName='tuser1'
				, contactID = contact.id
			);
			insert testuser;

			idcpf = new E_IDCPF__c(
				FLAG01__c = '1',
				FLAG02__c = '1',
				//TRCDE01__c = 'TD06',
				//TRCDE02__c = 'TD06',
				ZSTATUS01__c = '1',
				User__c = testuser.Id,
				ZSTATUS02__c = '1',
				ZPASSWD02__c = '69faa927f801c5bacc84970bd66b5231290271bd0a369dc246724a4e0859f7ea686fe7dee47d2d5ab165a217f70dfa0e20a6d895fa51d6032c72afc7c2533207' // terrasky
				,ownerid = testuser.id,
				ZDSPFLAG02__c = '0'
			);
			insert idcpf;
		}
	}

	private static void insertEmail(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_Email__c eMail = new E_Email__c(NAME = 'E_101',Email__c = 'xxxx@xx.xx');
			insert eMail;
		}
	}

	private static void insertE_bizDataSyncLog(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			E_bizDataSyncLog__c log = new E_bizDataSyncLog__c(kind__c = '1');
			insert log;
		}
	}

	/*
	* テスト用ページの取得
	* @param    : なし
	* @return   : テスト用ページ
	*/
	static PageReference getPageReference() {
		return new PageReference('/nnlinkage/E_EMailAddressChange');
//		return new PageReference('https://irisdev1-nnlife-jp.cs31.force.com/nnlinkage/e_emailaddresschange');
	}

	//メッセージマスタ作成
	static void createMessageData(String key,String value){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			createMenuData();
			E_MessageMaster__c msg = new E_MessageMaster__c();
			msg.Name = 'テストメッセージ';
			msg.Key__c = key;
			msg.Value__c = value;
			msg.Type__c = 'メッセージ';
			insert msg;
		}
	}

	//メニューマスタ作成
	static void createMenuData(){
		E_MenuMaster__c mn = new E_MenuMaster__c();
		mn.MenuMasterKey__c = 'internet_service_policy_agency';
		insert mn;
	}

	//メッセージマスタ作成
	static void createIRISCMS(){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			I_PageMaster__c pm = createIRISPageData();
			I_ContentMaster__c cms = new I_ContentMaster__c();
			cms.Page__c = pm.Id;
			cms.Content__c = '内容';
			insert cms;
		}
	}

	//メニューマスタ作成
	static I_PageMaster__c createIRISPageData(){
		I_PageMaster__c pm = new I_PageMaster__c();
		pm.page_unique_key__c = 'SelfRegistConfirm';
		insert pm;
		return pm;
	}
}