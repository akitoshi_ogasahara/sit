@isTest(SeeAllData=false)
private class TestE_UPMR002Controller {
	
	private static testMethod void unitTestByPrdCd() {
		// メニューマスタ
		E_MenuMaster__c menuMaster = new E_MenuMaster__c(Name = 'テストメニュー', MenuMasterKey__c = 'testMenu');
		insert menuMaster;
		// メッセージマスタ
		E_MessageMaster__c messageMaster = new E_MessageMaster__c(Name = 'テストCMS', 	Menu__c = menuMaster.Id, Key__c = '001');
		insert messageMaster;
		// 添付ファイル
		E_CMSFile__c cmsFile = new E_CMSFile__c(Name = '添付ファイル', MessageMaster__c = messageMaster.Id);
		insert cmsFile;
		
		// パラメータ
		String productCd = E_EncryptUtil.prdCodeConversion('testMenu', true);
		PageReference pageRef = Page.upmr002;
		pageRef.getParameters().put('Zprdcd', productCd);
		
		Test.startTest();
		Test.setCurrentPage(pageRef);
		E_UPMR002Controller controller = new E_UPMR002Controller();
		PageReference result = controller.init();
		
		Test.stopTest();
		
		System.assertEquals(null, result);
	}
	
	private static testMethod void unitTestByMenuId() {
		// メニューマスタ
		E_MenuMaster__c menuMaster = new E_MenuMaster__c(Name = 'テストメニュー', MenuMasterKey__c = 'testMenu');
		insert menuMaster;
		// メッセージマスタ
		E_MessageMaster__c messageMaster = new E_MessageMaster__c(Name = 'テストCMS', 	Menu__c = menuMaster.Id, Key__c = '001');
		insert messageMaster;
		// 添付ファイル
		E_CMSFile__c cmsFile = new E_CMSFile__c(Name = '添付ファイル', MessageMaster__c = messageMaster.Id);
		insert cmsFile;
		
		//　パラメータ
		PageReference pageRef = Page.upmr002;
		pageRef.getParameters().put('menuid', menuMaster.Id);
		
		Test.startTest();
		Test.setCurrentPage(pageRef);
		E_UPMR002Controller controller = new E_UPMR002Controller();
		PageReference result = controller.init();
		
		Test.stopTest();
		
		System.assertEquals(null, result);
	}
}