public without sharing class E_PermissionSetDaoWithout {
	public E_PermissionSetDaoWithout() {}

	/**
	 *　NameをキーにPermissionSetをリストで取得
	 *　@param names Nameのリスト
	 *　@return PermissionSetリスト
	 */
	public static List<PermissionSet> getRecordsByNames(Set<String> names){
		return [Select 
					id, Name
				From 
					PermissionSet
				Where 
					Name in :names
				];
	}
	//@future		同期処理に変更する。
	public static void insertRecsToUserByUId(String userId, Set<String> permissionNames){
        
        //権限セット取得
        List<PermissionSet> permissionSetList= E_PermissionSetDaoWithout.getRecordsByNames(permissionNames);
        //User と PermissionSet の中間OBJリスト
        List<PermissionSetAssignment> assignmentList = new List<PermissionSetAssignment>();
                
        for(PermissionSet pSet : permissionSetList){
            PermissionSetAssignment assignment = new PermissionSetAssignment();
            assignment.PermissionSetId = pSet.Id;
            assignment.AssigneeId = userId;
            assignmentList.add(assignment);
        }
        if(!assignmentList.isEmpty()){
            insert assignmentList;
            System.debug(assignmentList);
       }
    }
}