@isTest
private class TestE_NewPolicyTrigger {
	
	/**
	 * ステータス発信日テスト
	 * 個人契約
	 */
	private static testMethod void statusDateTest_001() {
		Datetime d1 = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		Test.startTest();
		E_NewPolicy__c newPlcy = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'','',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		insert newPlcy;
		E_NewPolicy__c result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d1.addHours(-9),result.StatusDate__c);

		//30分後　ステータスの更新はなし
		Datetime d2 = d1.addMinutes(30);
		newPlcy.SyncDate__c = d2;
		update newPlcy;
		result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d1.addHours(-9),result.StatusDate__c);

		//30分後　ステータスの更新あり
		Datetime d3 = d2.addMinutes(30);
		newPlcy.SyncDate__c = d3;
		newPlcy.STATCODE__c = 'I';
		update newPlcy;
		Test.stopTest();
		result = [Select id ,StatusDate__c From E_NewPolicy__c Where id =: newPlcy.id];
		//JST変換
		system.assertEquals(d3.addHours(-9),result.StatusDate__c);
	}

	/**
	 * ステータス発信日テスト
	 * 団体契約
	 */

	private static testMethod void statusDateTest_002() {
		Datetime d1 = system.now();
		Account distribute = TestI_TestUtil.createAccount(true,null);
		Account office = TestI_TestUtil.createAccount(true,distribute);
		Contact agent = TestI_TestUtil.createContact(true,office.id,'募集人1');
		Test.startTest();
		E_NewPolicy__c newPlcy1 = TestUtil_AMS.createNewPolicy(false,'P','TEST001',d1,'Y','D001',200001,'契約者名1','ケイヤクシャメイ1','被保険者名1','ヒホケンシャメイ1',distribute.id,office.id,agent.id);
		E_NewPolicy__c newPlcy2 = TestUtil_AMS.createNewPolicy(false,'P','TEST002',d1,'Y','D001',200001,'契約者名2','ケイヤクシャメイ2','被保険者名2','ヒホケンシャメイ2',distribute.id,office.id,agent.id);
		List<E_NewPolicy__c> dList = new List<E_NewPolicy__c>{newPlcy1,newPlcy2};
		insert dList;
		List<E_NewPolicy__c> results = [Select id ,StatusDate__c From E_NewPolicy__c Where GroupParentFlag__c = true Order By StatusDate__c desc];
		system.assertEquals(results.size(),1);
		//JST変換
		system.assertEquals(d1.addHours(-9),results[0].StatusDate__c);

		//30分後　ステータスの更新はなし
		Datetime d2 = d1.addMinutes(30);
		newPlcy1.SyncDate__c = d2;
		newPlcy2.SyncDate__c = d2;
		E_NewPolicy__c newPlcy3 = TestUtil_AMS.createNewPolicy(false,'P','TEST003',d2,'Y','D001',200001,'契約者名3','ケイヤクシャメイ3','被保険者名3','ヒホケンシャメイ3',distribute.id,office.id,agent.id);
		E_NewPolicy__c newPlcy4 = TestUtil_AMS.createNewPolicy(false,'P','TEST004',d2,'Y','D001',200002,'契約者名4','ケイヤクシャメイ4','被保険者名4','ヒホケンシャメイ4',distribute.id,office.id,agent.id);
		dList = new List<E_NewPolicy__c>{newPlcy1,newPlcy2,newPlcy3,newPlcy4};

		upsert dList;
		results = [Select id ,StatusDate__c From E_NewPolicy__c Where GroupParentFlag__c = true Order By StatusDate__c desc];
		system.assertEquals(results.size(),2);
		//JST変換
		system.assertEquals(d2.addHours(-9),results[0].StatusDate__c);
		system.assertEquals(d1.addHours(-9),results[1].StatusDate__c);

		//30分後　ステータスの更新あり
		Datetime d3 = d2.addMinutes(30);
		newPlcy1.SyncDate__c = d3;
		newPlcy1.STATCODE__c = 'I';
		newPlcy2.SyncDate__c = d3;
		newPlcy3.SyncDate__c = d3;
		newPlcy4.SyncDate__c = d3;
		dList = new List<E_NewPolicy__c>{newPlcy1,newPlcy2,newPlcy3,newPlcy4};

		upsert dList;
		Test.stopTest();
		results = [Select id ,StatusDate__c From E_NewPolicy__c Where GroupParentFlag__c = true Order By StatusDate__c desc];

		system.assertEquals(results.size(),2);
		//JST変換
		system.assertEquals(d3.addHours(-9),results[0].StatusDate__c);
		system.assertEquals(d2.addHours(-9),results[1].StatusDate__c);
	}
}