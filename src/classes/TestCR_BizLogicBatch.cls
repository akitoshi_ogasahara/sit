/**
 * CR_BizLogicBatchテストクラス
 */
@isTest
private class TestCR_BizLogicBatch {
	
	private static Account acc1 = null;
	private static Account acc2 = null;
	private static Account acc3 = null;
	private static Account acc4 = null;
	private static User u1 = null;
	private static User u2 = null;
	private static User u3 = null;
	private static User u4 = null;
	private static Group g1 = null;
	private static Group g2 = null;
	
	/**
	 * 代理店提出が存在しない場合
	 */
	private static testMethod void testCR_BizLogicBatchSet01() {
		System.debug('■■■■テスト準備■■■');
		testData();
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(true,u.Id);
		
		System.runAs(u){
			Test.startTest();
			System.debug('■■■■テスト開始■■■');
			E_BizDataSyncLog__c ebizlog = new E_BizDataSyncLog__c(kind__c = '5');
			insert ebizlog;
			Test.stopTest();
			System.debug('■■■■結果検証■■■');
		}
	}
	
	/**
	 * 代理店提出が存在する場合(MRが変わった場合)
	 */
	private static testMethod void testCR_BizLogicBatchSet02() {
		System.debug('■■■■テスト準備■■■');
		User u = TestE_TestUtil.createUser(true, 'keiyakusha', 'システム管理者');
		E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(true,u.Id);
		
		testData();	
		
		//CR_Agency
		List<CR_Agency__c> craList = new List<CR_Agency__c>();
		CR_Agency__c cr1 = new CR_Agency__c(Account__c = acc1.id, mr__c = u2.id);
		CR_Agency__c cr2 = new CR_Agency__c(Account__c = acc2.id, mr__c = u3.id);
		CR_Agency__c cr3 = new CR_Agency__c(Account__c = acc3.id, mr__c = u3.id);
		CR_Agency__c cr4 = new CR_Agency__c(Account__c = acc4.id, mr__c = u3.id);
		craList.add(cr1);
		craList.add(cr2);
		craList.add(cr3);
		craList.add(cr4);
		insert craList;
		List<CR_Agency__Share> craShare = new List<CR_Agency__Share>();
		CR_Agency__Share share1 = new CR_Agency__Share(ParentID = cr1.id, UserOrGroupId = g2.id, AccessLevel = 'Edit', RowCause = Schema.CR_Agency__Share.RowCause.Manual);
		CR_Agency__Share share2 = new CR_Agency__Share(ParentID = cr2.id, UserOrGroupId = g1.id, AccessLevel = 'Edit', RowCause = Schema.CR_Agency__Share.RowCause.Manual);
		CR_Agency__Share share3 = new CR_Agency__Share(ParentID = cr3.id, UserOrGroupId = g1.id, AccessLevel = 'Edit', RowCause = Schema.CR_Agency__Share.RowCause.Manual);
		CR_Agency__Share share4 = new CR_Agency__Share(ParentID = cr4.id, UserOrGroupId = g2.id, AccessLevel = 'Edit', RowCause = Schema.CR_Agency__Share.RowCause.Manual);
		craShare.add(share1);
		craShare.add(share2);
		craShare.add(share3);
		craShare.add(share4);
		insert craShare;
		
		System.runAs(u){
			Test.startTest();
			System.debug('■■■■テスト開始■■■');
			E_BizDataSyncLog__c ebizlog = new E_BizDataSyncLog__c(kind__c = '5');
			insert ebizlog;
			Test.stopTest();
			System.debug('■■■■結果検証■■■');
		}
	}
	
	private static void testData() {
		//Account
		List<Account> accList = new List<Account>();
		acc1 = new Account(Name = 'TestAccountName', E_CL1PF_ZHEADAY__c='XXXX5', ZMRCODE__c = 'X12345', E_CL1PF_ZBUSBR__c='XX1');
		acc2 = new Account(Name = 'TestAccountName', E_CL1PF_ZHEADAY__c='XXXX6', ZMRCODE__c = 'X12346', E_CL1PF_ZBUSBR__c='XX2');
		acc3 = new Account(Name = 'TestAccountName', E_CL1PF_ZHEADAY__c='XXXX7', ZMRCODE__c = 'X12347', E_CL1PF_ZBUSBR__c='XX1');
		acc4 = new Account(Name = 'TestAccountName', E_CL1PF_ZHEADAY__c='XXXX8', ZMRCODE__c = 'X12348', E_CL1PF_ZBUSBR__c='XX3');
		accList.add(acc1);
		accList.add(acc2);
		accList.add(acc3);
		accList.add(acc4);
		insert accList;
		
		//Contact
		Contact con1 = new Contact(AccountId = acc1.id, firstname='Con', lastname='1');
		insert con1;
		
		//User
		List<User> userList = new List<User>();
		u1 = TestE_TestUtil.createUser(false, 'xxxx1', 'システム管理者');
		u1.mrcd__c = 'X12345';
		u2 = TestE_TestUtil.createUser(false, 'xxxx2', 'システム管理者');
		u2.mrcd__c = 'X12346';
		u3 = TestE_TestUtil.createUser(false, 'xxxx3', 'システム管理者');
		u3.mrcd__c = 'X12347';
		u4 = TestE_TestUtil.createUser(false, 'xxxx4', 'E_PartnerCommunity');
		u4.contactid = con1.id;
		userList.add(u1);
		userList.add(u2);
		userList.add(u3);
		userList.add(u4);
		insert userList;
		
		//Group
		List<Group> gpList = new List<Group>();
		g1 = new Group(name='BRXX1');
		g2 = new Group(name='BRXX2');
		gpList.add(g1);
		gpList.add(g2);
		insert gpList;
	}
}