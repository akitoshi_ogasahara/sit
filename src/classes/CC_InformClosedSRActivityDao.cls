public with sharing class CC_InformClosedSRActivityDao {

    /**
     * Caseに紐づくSRPolicyを取得
     * @param cIds CaseIDのSet
     * @return List<CC_SRPolicy__c>
     */
    public static List<CC_SRPolicy__c> getRecsByCaseids(Set<ID> cIds){
        //条件不備の場合には空を返す
        if(cIds == null || cIds.isEmpty()){
            return new List<CC_SRPolicy__c>();
        }
        return [SELECT
                    id,
                    //契約1
                    CC_PolicyID1__r.MainAgent__c,
                    CC_PolicyID1__r.SubAgent__c,
                    CC_PolicyID1__r.MainAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID1__r.SubAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID1__r.MainAgent__r.Account.KSECTION__c,
                    CC_PolicyID1__r.SubAgent__r.Account.KSECTION__c,
                    //契約2
                    CC_PolicyID2__r.MainAgent__c,
                    CC_PolicyID2__r.SubAgent__c,
                    CC_PolicyID2__r.MainAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID2__r.SubAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID2__r.MainAgent__r.Account.KSECTION__c,
                    CC_PolicyID2__r.SubAgent__r.Account.KSECTION__c,
                    //契約3
                    CC_PolicyID3__r.MainAgent__c,
                    CC_PolicyID3__r.SubAgent__c,
                    CC_PolicyID3__r.MainAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID3__r.SubAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID3__r.MainAgent__r.Account.KSECTION__c,
                    CC_PolicyID3__r.SubAgent__r.Account.KSECTION__c,
                    //契約4
                    CC_PolicyID4__r.MainAgent__c,
                    CC_PolicyID4__r.SubAgent__c,
                    CC_PolicyID4__r.MainAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID4__r.SubAgent__r.Account.E_CL2PF_BRANCH__c,
                    CC_PolicyID4__r.MainAgent__r.Account.KSECTION__c,
                    CC_PolicyID4__r.SubAgent__r.Account.KSECTION__c
                FROM
                    CC_SRPolicy__c
                WHERE
                    CC_CaseId__c in: cIds];
    }

    public static List<CC_BranchConfig__c> getRecsByBRCodes(Set<String> brSet){
        if(brSet == null || brSet.isEmpty()){
            return new List<CC_BranchConfig__c>();
        }
        return [SELECT CC_SharedMailAdd__c FROM CC_BranchConfig__c WHERE CC_BranchCode__c in: brSet];
    }

}