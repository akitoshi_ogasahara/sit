@isTest private class TestI_SurveyTargetDaoWithout {

	/**
	 * 代理店IdとアンケートIdから対象者取得
	 */
	@isTest static void getRecByAccountIdAndSurveyIdTest01() {
		//データ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, true);
		List<I_SurveyTarget__c> targetList = new List<I_SurveyTarget__c>();

		System.runAs(us){
			Test.startTest();
			targetList = I_SurveyTargetDaoWithout.getRecByAccountIdAndSurveyId(us.Contact.Account.ParentId, survey.Id);
			Test.stopTest();
		}

		System.assertEquals(1,targetList.size());
	}

	/*
	 * 対象者がいない場合
	 * 
	 */
	@isTest static void getRecByAccountIdAndSurveyIdTest02() {
		//データ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, false);
		List<I_SurveyTarget__c> targetList = new List<I_SurveyTarget__c>();

		System.runAs(us){
			Test.startTest();
			targetList = I_SurveyTargetDaoWithout.getRecByAccountIdAndSurveyId(us.Contact.Account.ParentId, survey.Id);
			Test.stopTest();
		}

		System.assertEquals(0,targetList.size());
	}
	/**
	 * 募集人で対象者と回答をinsert
	 */
	@isTest static void insertRecTest() {
		//データ作成
		User us = createUser();
		I_Survey__c survey = createSurveyRec(us.Id, false);
		I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
		sTarget.Ownerid = us.id;
		sTarget.Survey__c = survey.id;
		sTarget.Contact__c = us.contactId;
		System.runAs(us){
			Test.startTest();
			I_SurveyTargetDaoWithout.insertRec(sTarget);
			Test.stopTest();
		}

		I_SurveyTarget__c targetRec = [SELECT Id, Contact__c FROM I_SurveyTarget__c WHERE Id =: sTarget.Id];
		System.assertEquals(sTarget.Id, targetRec.Id);
		System.assertEquals(us.ContactId, targetRec.Contact__c);

	}

	/*
	 * アンケート登録
	 * @param userId: userId
	 * @param isAnswered: whether agent has answered
	 * @return I_Survey__c： アンケート
	 */
	private static I_Survey__c createSurveyRec(Id userId, boolean isAnswered){
		//アンケート作成
		I_Survey__c survey = new I_Survey__c();
		survey.Name = 'テストアンケート';
		survey.OverView__c = 'テスト概要';
		survey.UpsertKey__c = 'survey01';
		survey.PeriodFrom__c = Date.today();
		survey.PeriodTo__c = Date.today().addDays(5);
		survey.SurveyTarget__c = '代理店';
		insert survey;
		//アンケートに紐づく対象者作成
		if(isAnswered){
			I_SurveyTarget__c sTarget = new I_SurveyTarget__c();
			sTarget.Ownerid = userId;
			sTarget.Survey__c = survey.id;
			insert sTarget;
		}
		//アンケートに紐づく設問作成
		I_SurveyQuestion__c question = new I_SurveyQuestion__c();
		question.Name = 'テスト設問';
		question.Selection__c = '参加';
		question.Survey__c = survey.Id;
		insert question;

		return survey;
	}

	/*
	 * 代理店ユーザー作成
	 * @return User: ユーザー
	 */
	private static User createUser(){
		User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
		User us = new User();
		System.runAs(thisUser){
			Account parentAcc = TestI_TestUtil.createAccount(true, null);
			Account acc = TestI_TestUtil.createAccount(true, parentAcc);
			Contact con = TestI_TestUtil.createContact(true, acc.Id, 'testCon');
			us = TestI_TestUtil.createAgentUser(true, 'testUser', E_Const.PROFILE_E_PARTNERCOMMUNITY, con.Id);
		}
		return us;
	}
}