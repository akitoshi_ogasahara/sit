/**
 * 
 */
@isTest
private class TestE_ShareBase {
	// NNLinkの共有ルール名（各オブジェクトのApex共有の理由に設定しておくこと）　　NNLink共有：NNLinkSharingRule
	private static final String SHARE_ROWCAUSE_NNLink = 'NNLinkSharingRule__c';
	
	// テスト用のE_ShareBase継承クラス
	public class Test_ShareBase extends E_ShareBase{}

    /**
     * void deleteNNLinkShares(Set<SObject> recs)
     * 正常
     */
	static testMethod void test_deleteNNLinkShares01(){
		/* Test Data */
		// 新契約ステータス
		List<E_NewPolicy__c> policys = new List<E_NewPolicy__c>();
		for(Integer i = 0; i < 3; i++){
			E_NewPolicy__c np = new E_NewPolicy__c();
			np.STATCODE__c = 'P';			// 新契約ステータス
			np.CHDRNUM__c = '0000000' + i;		// 証券番号
			np.SyncDate__c = System.now();		// データ発信日
			policys.add(np);
		}
		insert policys;
		
		// ShareUser
		User user = [Select Id From User Where IsActive = true Limit 1];
		
		// Share
		List<E_NewPolicy__Share> shares = new List<E_NewPolicy__Share>();
		for(E_NewPolicy__c policy : policys){
			// RowCause : NNLinkSharingRule__c
			shares.add(new E_NewPolicy__Share(
				ParentId = policy.Id,
				AccessLevel = 'Edit',
				RowCause = SHARE_ROWCAUSE_NNLink,
				UserOrGroupId = user.Id)
			); 
			// RowCause : 
			shares.add(new E_NewPolicy__Share(
				ParentId = policy.Id,
				AccessLevel = 'Edit',
				UserOrGroupId = user.Id)
			); 
		}
		insert shares;
		
		// SObject
		Set<SObject> sobjs = new Set<SObject>();
		for(E_NewPolicy__c policy : policys){
			sobjs.add(policy);
		}
		
		// Test
		Test.startTest();
		
		// 実施前 assertion
		List<E_NewPolicy__Share> beforeShares = [Select Id From E_NewPolicy__Share Where ROWCause = :SHARE_ROWCAUSE_NNLink];
		system.assertEquals(sobjs.size(), beforeShares.size());
		
		Test_ShareBase base = new Test_ShareBase();
		base.deleteNNLinkShares(sobjs);

		// 実施後 assertion
		List<E_NewPolicy__Share> afterShares = [Select Id From E_NewPolicy__Share Where ROWCause = :SHARE_ROWCAUSE_NNLink];
		system.assertEquals(0, afterShares.size());
		
		Test.stopTest();
	}


    /**
     * List<SObject> createSharesNNLink(Map<SObject, Account> recs, String level)
     * 正常
	 */
	static testMethod void test_createSharesNNLink01(){
		/* Test Data */
		String CODE_DISTRIBUTE = '10001';	// 代理店格コード
		String CODE_OFFICE = '10002';		// 事務所コード
		String CODE_BRANCH = '88';			// 支社コード
		String GRP_DISTRIBUTE = 'L1' + CODE_DISTRIBUTE;
		String GRP_OFFICE = 'L2' + CODE_OFFICE;
		String GRP_BRANCH = 'BR' + CODE_BRANCH;
		
		// 既存の公開グループ
		Map<String, Group> grpMap = new Map<String, Group>();
		for(Group grp : [Select Id, DeveloperName From Group Where DeveloperName In (:GRP_DISTRIBUTE, :GRP_OFFICE, :GRP_BRANCH)]){
			grpMap.put(grp.DeveloperName, grp);
		}
		
		// 既存に存在しない公開グループ作成
		List<Group> grps = new List<Group>();
		if(grpMap.get('L1' + CODE_DISTRIBUTE) == null){
			grps.add(new Group(Name='L1'+CODE_DISTRIBUTE, DeveloperName=GRP_DISTRIBUTE, Type='Regular'));
		}
		if(grpMap.get('L2' + CODE_OFFICE) == null){
			grps.add(new Group(Name='L2'+CODE_OFFICE, DeveloperName=GRP_OFFICE, Type='Regular'));
		}
		if(grpMap.get('BR' + CODE_BRANCH) == null){
			grps.add(new Group(Name='BR'+CODE_BRANCH, DeveloperName=GRP_BRANCH, Type='Regular'));
		}
		if(!grps.isEmpty()){
			insert grps;
		}

		// Account 格
		Account distribute = new Account(
			Name = 'テスト代理店格',
			E_CL1PF_ZHEADAY__c = CODE_DISTRIBUTE
		);
		insert distribute;
		
		// Account 事務所
		Account office = new Account(
			Name = 'テスト事務所',
			ParentId = distribute.Id,
			E_CL2PF_ZAGCYNUM__c = CODE_OFFICE,
			E_CL2PF_BRANCH__c = CODE_BRANCH
		);
		insert office;
		
		// 新契約ステータス
		E_NewPolicy__c policy = new E_NewPolicy__c(
			ParentAccount__c = distribute.Id,
			Account__c = office.Id,
			CHDRNUM__c = '00000001',
			STATCODE__c = 'P',
			SyncDate__c = System.now()
		);
		insert policy;
		
		// SObject Map
		office = [Select Id, ParentId, E_ParentZHEADAY__c, E_CL2PF_ZAGCYNUM__c, E_CL2PF_BRANCH__c From Account Where Id = :office.Id Limit 1];
		Map<SObject, Account> sobjMap = new Map<SObject, Account>();
		sobjMap.put(policy, office);
		
		// Test
		Test.startTest();
		
		Test_ShareBase base = new Test_ShareBase();
		base.createSharesNNLink(sobjMap, 'Edit');
		
		Map<String, E_NewPolicy__Share> resultShareMap = new Map<String, E_NewPolicy__Share>();
		List<E_NewPolicy__Share> shares = [Select Id, ParentId, AccessLevel, RowCause, UserOrGroupId, UserOrGroup.Name From E_NewPolicy__Share Where ROWCause = :SHARE_ROWCAUSE_NNLink];
		for(E_NewPolicy__Share share : shares){
			resultShareMap.put(share.UserOrGroup.Name, share);
		}
		
		// assertion
		system.assertEquals(3, shares.size());
		system.assert(resultShareMap.get(GRP_DISTRIBUTE) != null);
		system.assert(resultShareMap.get(GRP_OFFICE) != null);
		system.assert(resultShareMap.get(GRP_BRANCH) != null);
		
		Test.stopTest();
	}
}