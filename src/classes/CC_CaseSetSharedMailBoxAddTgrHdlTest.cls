/*
 * CC_CaseSetSharedMailBoxAddTgrHdlTest
 * Test class of CC_CaseSetSharedMailBoxAddTgrHdl
 * created  : Accenture 2018/7/11
 * modified :
 */

@isTest
private class CC_CaseSetSharedMailBoxAddTgrHdlTest{

	private static User testUser = CC_TestDataFactory.createTestUser();

	/**
	* Prepare test data
	*/
	@testSetup static void prepareTestData(){
		System.runAs ( testUser ) {
			//Create and update Account
			RecordType recordTypeOfAccount = [SELECT Id FROM RecordType where SobjectType = 'Account' and Name = '法人顧客' limit 1];
			Account account = CC_TestDataFactory.getAccountSkel(recordTypeOfAccount.Id);
			account.E_CL2PF_BRANCH__c = '11';
			account.KSECTION__c = '88';
			Insert account;

			//Create Contact
			RecordType recordTypeOfContact = [SELECT Id FROM RecordType where SobjectType = 'Contact' and Name = '募集人' limit 1];
			Contact contactObj = CC_TestDataFactory.getContactSkel();
			contactObj.RecordTypeId = recordTypeOfContact.Id;
			contactObj.AccountId = account.Id;
			contactObj.E_CL3PF_AGNTNUM__c = '25GLK';
			Insert contactObj;

			//Create and update SRTypeMaster
			CC_SRTypeMaster__c srTypeMaster = CC_TestDataFactory.getSRTypeMasterSkel();
			srTypeMaster.CC_IsInformedClosedSRFlag__c = true;
			Insert srTypeMaster;

			//Create BranchConfig
			CC_BranchConfig__c branchConfig = new CC_BranchConfig__c(
				Name = 'testBranchConfig03',
				CC_SharedMailBoxAdd__c = 'testbranchConfig03@test.com',
				CC_BranchCode__c = '03'
			);
			Insert branchConfig;
		}
	}

	/**
	* Test before update
	* KSECTION__c.equals('88')
	*/
	static testMethod void onBeforeUpdateTest01() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Case
			CC_SRTypeMaster__c srTypeMaster = [Select Id, CC_IsInformedClosedSRFlag__c From CC_SRTypeMaster__c Where Name = '10001' limit 1];
			Contact contact = [Select Id, RecordType.Name From Contact Where FirstName = 'test' And LastName = 'Contact' limit 1];
			Case caseObj = CC_TestDataFactory.getCaseSkel(srTypeMaster.Id);
			Account account = [Select Id, Name From Account Where Name = 'testAccount' limit 1];
			caseObj.CMN_AgencyName__c = account.Id;
			caseObj.CMN_AgentName__c = contact.Id;
			caseObj.RecruiterCD__c = '25GLK';
			Insert caseObj;

			Test.startTest();
			//Update Case
			caseObj.Status = 'クローズ';
			Update caseObj;
			Test.stopTest();

		}
	}

	/**
	* Test before update
	* !KSECTION__c.equals('88') && !E_CL2PF_BRANCH__c.equals('03')
	*/
	static testMethod void onBeforeUpdateTest02() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create BranchConfig
			CC_BranchConfig__c branchConfig = new CC_BranchConfig__c(
				Name = 'testBranchConfig11',
				CC_SharedMailBoxAdd__c = 'testbranchConfig11@test.com',
				CC_BranchCode__c = '11'
			);
			Insert branchConfig;
			//Create Case
			CC_SRTypeMaster__c srTypeMaster = [Select Id, CC_IsInformedClosedSRFlag__c From CC_SRTypeMaster__c Where Name = '10001' limit 1];
			Contact contact = [Select Id, RecordType.Name From Contact Where FirstName = 'test' And LastName = 'Contact' limit 1];
			Case caseObj = CC_TestDataFactory.getCaseSkel(srTypeMaster.Id);
			Account account = [Select Id, Name From Account Where Name = 'testAccount' limit 1];
			account.KSECTION__c = '44';
			Update account;
			caseObj.CMN_AgencyName__c = account.Id;
			caseObj.CMN_AgentName__c = contact.Id;
			caseObj.RecruiterCD__c = '25GLK';
			Insert caseObj;

			Test.startTest();
			//Update Case
			caseObj.Status = 'クローズ';
			Update caseObj;
			Test.stopTest();

		}
	}

	/**
	* Test before update
	* String.isEmpty(E_CL2PF_BRANCH__c)
	*/
	static testMethod void onBeforeUpdateTest03() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Case
			CC_SRTypeMaster__c srTypeMaster = [Select Id, CC_IsInformedClosedSRFlag__c From CC_SRTypeMaster__c Where Name = '10001' limit 1];
			Contact contact = [Select Id, RecordType.Name From Contact Where FirstName = 'test' And LastName = 'Contact' limit 1];
			Case caseObj = CC_TestDataFactory.getCaseSkel(srTypeMaster.Id);
			Account account = [Select Id, Name From Account Where Name = 'testAccount' limit 1];
			account.E_CL2PF_BRANCH__c = '';
			account.KSECTION__c = '44';
			Update account;
			caseObj.CMN_AgencyName__c = account.Id;
			caseObj.CMN_AgentName__c = contact.Id;
			caseObj.RecruiterCD__c = '25GLK';
			Insert caseObj;

			Test.startTest();
			//Update Case
			caseObj.Status = 'クローズ';
			Update caseObj;
			Test.stopTest();

		}
	}
	/**
	* Test before update
	* String.isEmpty(KSECTION__c)
	*/
	static testMethod void onBeforeUpdateTest04() {
		System.runAs ( testUser ) {

			//Prepare test data
			//Create Case
			CC_SRTypeMaster__c srTypeMaster = [Select Id, CC_IsInformedClosedSRFlag__c From CC_SRTypeMaster__c Where Name = '10001' limit 1];
			Contact contact = [Select Id, RecordType.Name From Contact Where FirstName = 'test' And LastName = 'Contact' limit 1];
			Case caseObj = CC_TestDataFactory.getCaseSkel(srTypeMaster.Id);
			Account account = [Select Id, Name From Account Where Name = 'testAccount' limit 1];
			account.E_CL2PF_BRANCH__c = '11';
			account.KSECTION__c = '';
			Update account;
			caseObj.CMN_AgencyName__c = account.Id;
			caseObj.CMN_AgentName__c = contact.Id;
			caseObj.RecruiterCD__c = '25GLK';
			Insert caseObj;

			Test.startTest();
			//Update Case
			caseObj.Status = 'クローズ';
			Update caseObj;
			Test.stopTest();

		}
	}

}