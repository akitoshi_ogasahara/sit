public with sharing class CC_CaseSetSharedMailBoxAddTgrHdl{

	// Trigger Handler実行確認用変数
	private static Set<String> runTgrSet = new Set<String>();

	/**
	 * Before Insert呼び出しメソッド
	 */
	public static void onBeforeInsert(List<Case> newList){}

	/**
	 * After Insert呼び出しメソッド
	 */
	public static void onAfterInsert(List<Case> newList, Map<Id,Case> newMap){}

	/**
	 * Before Update呼び出しメソッド
	 */
	public static void onBeforeUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){
		//共有メールボックスアドレスの更新 Begin. //

		//支社メールアドレス設定情報取得
		List<CC_BranchConfig__c> branchConfigList = getBranchConfigList();
		Map<String, String> branchCodeAndMailMap = new Map<String, String>();
		if(branchConfigList.size() > 0){
			for(CC_BranchConfig__c branchConfigObj : branchConfigList){
				if(branchConfigObj.CC_BranchCode__c != null && !branchConfigObj.CC_BranchCode__c.equals('') ){
					branchCodeAndMailMap.put(branchConfigObj.CC_BranchCode__c, branchConfigObj.CC_SharedMailBoxAdd__c);
				}
			}
		}

		//関連データを一括取得
		List<Case> newCaseListFiltered = new List<Case>();
		Set<Id> agencyIdSet = new Set<Id>();
		Map<Id, Account> accountMap = new Map<Id, Account>();
		Set<Id> srTypeIdSet = new Set<Id>();
		Map<Id, CC_SRTypeMaster__c > srTypeMasterMap = new Map<Id, CC_SRTypeMaster__c >();
		for(Case caseObj : newList){
			if(caseObj.Status.equals('クローズ') && caseObj.CC_SRTypeId__c != null && caseObj.CMN_AgencyName__c != null){
				agencyIdSet.add(caseObj.CMN_AgencyName__c);
				srTypeIdSet.add(caseObj.CC_SRTypeId__c);
				newCaseListFiltered.add(caseObj);
			}
		}

		if(agencyIdSet.size() > 0){
			accountMap = getAccountMapByIdList(agencyIdSet);
		}

		if(srTypeIdSet.size() > 0){
			srTypeMasterMap = getSRTypeMasterMapByIdList(srTypeIdSet);
		}

		//SR更新
		if(newCaseListFiltered.size() > 0 && branchCodeAndMailMap.keyset().size() > 0
		   && srTypeMasterMap.keyset().size() > 0 && accountMap.keyset().size() > 0){
			for(Case caseObj : newCaseListFiltered){
				String branchCode = accountMap.get(caseObj.CMN_AgencyName__c).E_CL2PF_BRANCH__c;
				String kSectionCode = accountMap.get(caseObj.CMN_AgencyName__c).KSECTION__c;
				Boolean isInformedClosedSRFlag = srTypeMasterMap.get(caseObj.CC_SRTypeId__c).CC_IsInformedClosedSRFlag__c;
				if(isInformedClosedSRFlag && !String.isEmpty(branchCode)){
					if((!String.isEmpty(kSectionCode) && kSectionCode.equals('88')) || branchCode.equals('03')){
						caseObj.CC_SharedMailBoxAdd__c = branchCodeAndMailMap.get('03');
						caseObj.CC_SRCloseNoticeMailAdd__c = '';
					}else{
						caseObj.CC_SharedMailBoxAdd__c = branchCodeAndMailMap.get(branchCode);
						//MRのメールアドレスをメール型のフィールドに移行する
						caseObj.CC_SRCloseNoticeMailAdd__c = caseObj.CC_MRMailAdd__c;
					}
				}
			}
		}

		//共有メールボックスアドレスの更新 End. //
	}

	/**
	 * After Update呼び出しメソッド
	 */
	public static void onAfterUpdate(List<Case> newList, Map<Id,Case> newMap, Map<Id,Case> oldMap){}

	/**
	 * Object: CC_BranchConfig__c
	 * Parameter: none
	 * return: List<CC_BranchConfig__c>
	 */
	public static List<CC_BranchConfig__c> getBranchConfigList(){
		List<CC_BranchConfig__c> branchConfigList = [
						SELECT Id, CC_SharedMailBoxAdd__c, CC_BranchCode__c
						  FROM CC_BranchConfig__c
					  ORDER BY CC_BranchCode__c];

		return branchConfigList;
	}

	/**
	 * Object: Account
	 * Parameter: Id set
	 * return: Map<Id, Account>
	 */
	public static Map<Id, Account> getAccountMapByIdList(Set<Id> idSet){
		Map<Id, Account> accountMap = new Map<Id, Account>([
						SELECT Id, Name, E_CL2PF_BRANCH__c, KSECTION__c, E_CL2PF_ZAGCYNUM__c, CMN_ClientNumber__c,
								CMN_isAddressUnknown__c, CC_LastUpdateDateForAddress__c
						  FROM Account
						 WHERE Id in: idSet]);

		return accountMap;
	}

	/**
	 * Object: CC_SRTypeMaster__c
	 * Parameter: Id set
	 * return: Map<Id, CC_SRTypeMaster__c>
	 */
	public static Map<Id, CC_SRTypeMaster__c> getSRTypeMasterMapByIdList(Set<Id> srTypeIdSet){
		Map<Id, CC_SRTypeMaster__c> srTypeMasterMap = new Map<Id, CC_SRTypeMaster__c>([
							SELECT Id, Name, CC_SRTypeNameandSubject__c, CC_IsInformedClosedSRFlag__c
							  FROM CC_SRTypeMaster__c
							 WHERE Id IN: srTypeIdSet]);

		return srTypeMasterMap;
	}

}