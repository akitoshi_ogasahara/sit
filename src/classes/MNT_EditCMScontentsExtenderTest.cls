@isTest
private class MNT_EditCMScontentsExtenderTest{
	// init case1 クエリパラメータあり
    private static testMethod void testInit_QueryParam1(){
		I_ContentMaster__c iCts1 = new I_ContentMaster__c(Name = 'test1');
		insert iCts1;
		I_ContentMaster__c iCts2 = new I_ContentMaster__c(Name = 'test2', ParentContent__c = iCts1.Id);
		insert iCts2;

        Pagereference pref = Page.MNT_EditCMScontents;
        pref.getParameters().put('ParentContent__c', iCts1.Id);

        //テスト開始
        Test.startTest();
        Test.setCurrentPage(pref);
        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(iCts1);
        MNT_EditCMScontents controller = new MNT_EditCMScontents(standardcontroller);
        MNT_EditCMScontentsExtender extender = controller.getExtender();

        extender.init();

        //テスト終了
        Test.stopTest();
    }
	// init case2 クエリパラメータなし
	private static testMethod void testInit_QueryParam2(){
		I_ContentMaster__c iCts1 = new I_ContentMaster__c(Name = 'test1');
		insert iCts1;
		I_ContentMaster__c iCts2 = new I_ContentMaster__c(Name = 'test2', ParentContent__c = iCts1.Id);
		insert iCts2;

        Pagereference pref = Page.MNT_EditCMScontents;

        //テスト開始
        Test.startTest();
        Test.setCurrentPage(pref);
        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(iCts1);
        MNT_EditCMScontents controller = new MNT_EditCMScontents(standardcontroller);
        MNT_EditCMScontentsExtender extender = controller.getExtender();

        extender.init();

        //テスト終了
        Test.stopTest();
    }
	// clone case1 値あり
	private static testMethod void testClone_valueToNull(){
		I_ContentMaster__c iCts1 = new I_ContentMaster__c(Name = 'test1', UpsertKey__c = 'testUpsert');
		insert iCts1;

        Pagereference pref = Page.MNT_EditCMScontents;
        //コピー時パラメータあり
        pref.getParameters().put('clone', '1');

        //テスト開始
        Test.startTest();
        Test.setCurrentPage(pref);
        Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(iCts1);
        MNT_EditCMScontents controller = new MNT_EditCMScontents(standardcontroller);
        MNT_EditCMScontentsExtender extender = controller.getExtender();

        extender.init();

        //テスト終了
        Test.stopTest();
    }

}