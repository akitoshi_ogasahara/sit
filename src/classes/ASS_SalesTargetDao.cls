/**
 * 営業日報データアクセスオブジェクト
 * CreatedDate 2018/04/27
 * @Author Terrasky
 */

public class ASS_SalesTargetDao {
	/**
	 * 営業目標リスト取得
	 * @param offIdSet: 事務所IDセット 
	 * @return List<SalesTarget__c>: 行動リスト
	 */
	public static List<SalesTarget__c> getThisMonthSalesTargetByOffIdSet(Set<Id> offIdSet) {
		return [SELECT 
					Day__c                      		//日付
					, APETarget__c              		//APE目標
					, ESTOppTarget__c           		//EST案件目標
					, NumOfContactsTarget__c    		//接触回数目標
					, DesignDocReqTarget__c     		//設計書依頼目標
					, Account__c                		//代理店事務所名
				FROM 
					SalesTarget__c
				WHERE
					Account__c IN : offIdSet         	//代理店事務所名が事務所リストに該当する
				AND 
					Day__c = THIS_MONTH                 //日付が当月
				Order By 
					Day__c
				];
	}
}