public with sharing class E_LogoHeaderController {

	private E_IRISHandler iris;
	
	private E_AccessController access;

	private final Set<String> ATRIA_LOGO_MENU = new Set<String>{E_Const.MK_EXID_AGENCY,E_Const.MK_EXID_BANK_AGENCY};
	
	public E_LogoHeaderController(){
		iris = E_IRISHandler.getInstance();
		
		this.access = E_AccessController.getInstance();
	}

	public Boolean getShowIRISSwitcher(){
		return iris.getCanUseIRIS();
	}
	
	// P16-0003 Atria対応開発
	public Boolean getCanAccessAtria() {
		return this.access.canAccessAtria();
	}
}