public with sharing class ASS_ProgressSummaryController {

	public Map<String,SummaryRow> summaryMap {get; set;}

	//MRのId
	public String paramMrId {get; set;}
	//事務所のID
	public String paramOfficeId {get; set;}

	//MRページフラグ
	public Boolean isMRpage {get; private set;}

	//チャネル「IA」
	public final String CHANEL_IA 				= 'IA';
	//チャネル「BA」
	public final String CHANEL_BA 				= 'BA';
	//カテゴリー「FS」
	public final String CATEGORY_FS 			= 'FS';
	//カテゴリー「PT」
	public final String CATEGORY_PT 			= 'PT';


	//営業予算項目「BAFS」
	public final String BUDGET_BAFS 			= 'BA Financial Solutions';
	//営業予算項目「BAPT」
	public final String BUDGET_BAPT 			= 'BA Protection';
	//営業予算項目「FS」
	public final String BUDGET_FS 				= 'CategoryⅢ';
	//営業予算項目「PT」
	public final String BUDGET_PT 				= 'CategoryⅡ';


	//見込案件レコード「予算」
	public final String RECTYPE_BUDGET 			= 'RecordType2';
	//見込案件レコード「実績」
	public final String RECTYPE_PERFORM 		= 'RecordType1';

	//WAPE(IAFS)
	public static final String TYPE_FS 			= 'fs';
	//WAPE(IAPT)
	public static final String TYPE_PT 			= 'pt';
	//WAPE(BAFS)
	public static final String TYPE_BAFS 		= 'bafs';
	//WAPE(BAPT)
	public static final String TYPE_BAPT 		= 'bapt';
	//活動回数
	public static final String TYPE_ACTIVITY 	= 'activity';
	//設計書依頼数
	public static final String TYPE_REQDOC 		= 'docreq';
	//EST案件数
	public static final String TYPE_EST 		= 'est';
	
	//typeList
	public List<String> typeList = new List<String>{};
	{
		typeList.add(TYPE_FS);
		typeList.add(TYPE_PT);
		typeList.add(TYPE_BAFS);
		typeList.add(TYPE_BAPT);
		typeList.add(TYPE_ACTIVITY);
		typeList.add(TYPE_EST);
		typeList.add(TYPE_REQDOC);
	}

	//Innerクラス
	public SummaryRow row;


	//コンストラクタ
	public ASS_ProgressSummaryController() {
		//初期化
		this.paramMrId = '';
		this.paramOfficeId = '';
		isMRpage = false;
	}

	public Map<String,SummaryRow> getMap(){
		//MRページ判定
		if(String.isEmpty(paramOfficeId)) {
			isMRpage = true;
		}

		//Mapの初期化
		summaryMap = new Map<String,SummaryRow>();
		for(String typ :typeList) {
			row = new SummaryRow();
			summaryMap.put(typ,row);
		}

		//見込案件レコードタイプ名とレコードタイプIdのマップを作成
		Map<String,String> recTypeMap = new Map<String,String>();
		//レコードタイプを取得
		List<RecordType> recTypeList = ASS_RecTypeDao.getRecTypesBySubType('Opportunity');
		for(RecordType rec :recTypeList){
			recTypeMap.put(rec.DeveloperName,rec.Id);
		}

		//事務所のマップを作成
		Map<Id,Account> officeMap = new Map<Id,Account>(ASS_AccountDao.getOfficesById(paramMrId,paramOfficeId));


		//見込案件のリストを取得
		String[] level = new String[]{'S:予算','S','A','B','C','D'};

		List<Opportunity> oppList = new List<Opportunity>();
		//MR進捗サマリーの場合は、所有者がMRのレコードのみを取得
		if(isMRpage){
			oppList = ASS_OppDaoWithout.getThisMonthOppsByOwnerIdAndLevel(paramMrId,level);
		//事務所進捗サマリーの場合は、事務所リストに紐づくレコードのみを取得
		} else {
			oppList = ASS_OppDaoWithout.getThisMonthOppsByOffIdSetAndLevel(officeMap.keySet(),level);
		}
		

		//見込案件を集計
		if(!oppList.isEmpty()) {
			for(Opportunity op :oppList) {
				//レコードタイプが「実績」の場合
				if(op.RecordTypeId == recTypeMap.get(RECTYPE_PERFORM)) {
					//PT/FSが「FS」の場合
					if(op.PtFsFormula__c == CATEGORY_FS) {
						if(summaryMap.containsKey(TYPE_FS)) {
							row = summaryMap.get(TYPE_FS);
						} else {
							row = new SummaryRow();
						}
						row.perform += op.WAPE__c;
						//Mapにput
						summaryMap.put(TYPE_FS,row);
						//チャネルが「BA」の場合
						if(op.ChannelMiddle__c == CHANEL_BA) {
							if(summaryMap.containsKey(TYPE_BAFS)) {
								row = summaryMap.get(TYPE_BAFS);
							} else {
								row = new SummaryRow();
							}
							row.perform += op.WAPE__c;
							//Mapにput
							summaryMap.put(TYPE_BAFS,row);
						}
					//PT/FSが「PT」の場合
					} else if(op.PtFsFormula__c == CATEGORY_PT) {
						if(summaryMap.containsKey(TYPE_PT)) {
							row = summaryMap.get(TYPE_PT);
						} else {
							row = new SummaryRow();
						}
						row.perform += op.WAPE__c;
						//Mapにput
						summaryMap.put(TYPE_PT,row);
						//チャネルが「BA」の場合
						if(op.ChannelMiddle__c == CHANEL_BA) {
							if(summaryMap.containsKey(TYPE_BAPT)) {
								row = summaryMap.get(TYPE_BAPT);
							} else {
								row = new SummaryRow();
							}
							row.perform += op.WAPE__c;
							//Mapにput
							summaryMap.put(TYPE_BAPT,row);
						}
					}
				//レコードタイプが「予算」の場合
				} else if(op.RecordTypeId == recTypeMap.get(RECTYPE_BUDGET)) {
					//「IAFS」の場合
					if(op.ProductName_TEXT__c == BUDGET_FS) {
						if(summaryMap.containsKey(TYPE_FS)) {
							row = summaryMap.get(TYPE_FS);
						} else {
							row = new SummaryRow();
						}
						row.budget += op.WAPE__c;
						//Mapにput
						summaryMap.put(TYPE_FS,row);
					//「IAPT」の場合
					} else if(op.ProductName_TEXT__c == BUDGET_PT) {
						if(summaryMap.containsKey(TYPE_PT)) {
							row = summaryMap.get(TYPE_PT);
						} else {
							row = new SummaryRow();
						}
						row.budget += op.WAPE__c;
						//Mapにput
						summaryMap.put(TYPE_PT,row);
					//「BAFS」の場合
					} else if(op.ProductName_TEXT__c == BUDGET_BAFS) {
						if(summaryMap.containsKey(TYPE_BAFS)) {
							row = summaryMap.get(TYPE_BAFS);
						} else {
							row = new SummaryRow();
						}
						row.budget += op.WAPE__c;
						//Mapにput
						summaryMap.put(TYPE_BAFS,row);
					//「BAPT」の場合
					} else if(op.ProductName_TEXT__c == BUDGET_BAPT) {
						if(summaryMap.containsKey(TYPE_BAPT)) {
							row = summaryMap.get(TYPE_BAPT);
						} else {
							row = new SummaryRow();
						}
						row.budget += op.WAPE__c;
						//Mapにput
						summaryMap.put(TYPE_BAPT,row);
					}
				}
			}
		}


		//事務所に紐づく行動のリストを作成
		Id recTypeId = ASS_RecTypeDao.getRecTypeByDevName('Event', 'DailyReport').Id;
		List<Event> eveList = ASS_EventDao.getThisMonthEventsByOffIdSet(recTypeId,officeMap.keySet());

		//行動を集計
		if(!eveList.isEmpty()){
			row = new SummaryRow();
			//活動回数実績
			row.perform =  eveList.size();
			//Mapにput
			summaryMap.put(TYPE_ACTIVITY,row);

			for(Event ev : eveList){
				//行動の「案件」にチェックがある場合
				if(ev.IsOpportunity__c){
					if(summaryMap.containsKey(TYPE_EST)) {
						row = summaryMap.get(TYPE_EST);
					} else {
						row = new SummaryRow();
					}
					row.perform += 1;
					summaryMap.put(TYPE_EST,row);
				}
				//行動の「設計書依頼」にチェックがある場合
				if(ev.DesignDocReq__c){
					if(summaryMap.containsKey(TYPE_REQDOC)) {
						row = summaryMap.get(TYPE_REQDOC);
					} else {
						row = new SummaryRow();
					}
					row.perform += 1;
					summaryMap.put(TYPE_REQDOC,row);
				}
			}
		}

		//事務所に紐づく営業目標のリストを取得
		List<SalesTarget__c> stList = ASS_SalesTargetDao.getThisMonthSalesTargetByOffIdSet(officeMap.keySet());

		//営業目標を集計
		if(!stList.isEmpty()){
			for(SalesTarget__c st : stList){
				//活動回数の目標
				if(summaryMap.containsKey(TYPE_ACTIVITY)) {
					row = summaryMap.get(TYPE_ACTIVITY);
				} else {
					row = new SummaryRow();
				}
				row.budget += st.NumOfContactsTarget__c;
				summaryMap.put(TYPE_ACTIVITY,row);

				//EST案件数の目標
				if(summaryMap.containsKey(TYPE_EST)) {
					row = summaryMap.get(TYPE_EST);
				} else {
					row = new SummaryRow();
				}
				row.budget += st.ESTOppTarget__c;
				summaryMap.put(TYPE_EST,row);
				
				//設計書依頼数の目標
				if(summaryMap.containsKey(TYPE_REQDOC)) {
					row = summaryMap.get(TYPE_REQDOC);
				} else {
					row = new SummaryRow();
				}
				row.budget += st.DesignDocReqTarget__c;
				summaryMap.put(TYPE_REQDOC,row);
			}
		}
		for(String typ :typeList) {
			row = summaryMap.get(typ);
			row.difference = row.perform - row.budget;
			summaryMap.put(typ,row);
		}
		return summaryMap;
	}
	
	/*
	 * 表示用の内部クラス作成
	 */

	public class SummaryRow {

		//予算
		public Decimal budget {get; set;}
		//実績
		public Decimal perform {get; set;}
		//差分
		public Decimal difference {get; set;}


		public SummaryRow(){
			budget = 		0;
			perform = 		0;
			difference = 	0;
		}
	}
}