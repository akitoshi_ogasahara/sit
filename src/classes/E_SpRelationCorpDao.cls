/**
 * E_SpRelationCorp(特定関係法人)データアクセスオブジェクト
 * CreatedDate 2017/07/20
 * @Author Terrasky
 */
public with sharing class E_SpRelationCorpDao {

	public static List<E_SpRelationCorp__c> getCorpRecById(String agencyId){
		return [
			SELECT 
				SpRelationCorpName__c,
				Relation__c,
				AGCO_AGNTREL__c
			FROM 
				E_SpRelationCorp__c
			WHERE
				E_AgencySalesResults__r.ParentAccountId__c =:agencyId
			ORDER BY
				AGCO_AGNTREL__c asc,
				AGCO_CLNTNUM__c asc
		];
	}
}