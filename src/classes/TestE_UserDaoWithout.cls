@isTest
private class TestE_UserDaoWithout {

	static String zinquirr = 'L3ZT000';

	/**
	 * ユーザデータを統合IDから取得する
	 */
	@isTest static void getUserRecByFederationIdentifierTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDaoWithout.getUserRecByFederationIdentifier('TestUser01');
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ユーザデータをユーザ名から取得する
	 */
	@isTest static void getUserRecByUserNameTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDaoWithout.getUserRecByUserName(us.Username);
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ユーザデータをユーザIDから取得する
	 */
	@isTest static void getUserRecByUserIdTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDaoWithout.getUserRecByUserId(us.Id);
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ログインせずにUser情報をupdate
	 */
	@isTest static void updateRecTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		String webId = '09876';

		Test.startTest();
		System.assertEquals(null, us.E_ZWEBID__c);
		us.E_ZWEBID__c = webId;
		E_UserDaoWithout.updateRec(us);
		Test.stopTest();

		User result = [SELECT Id, E_ZWEBID__c FROM User WHERE Id = :us.Id];
		System.assertEquals(webId, result.E_ZWEBID__c);
	}

	/**
	 * ユーザデータを作成する
	 */
	@isTest static void createUserTest() {
		//データ作成
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData01();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity');
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.MRCD__c = 'testUs';
			us.ContactId = con.Id;
		}

		Test.startTest();
		Profile prof = E_ProfileDaoWithout.getRecByName('E_PartnerCommunity_IA');
		User result = E_UserDaoWithout.createUser(us, prof.Id);
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
		System.assertEquals(prof.Id, result.ProfileId);
	}

	/**
	 * ユーザデータを更新する
	 */
	@isTest static void updateUserRecTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		String webId = '09876';

		Test.startTest();
		us.E_ZWEBID__c = webId;
		E_UserDaoWithout.updateUserRec(us);
		Test.stopTest();

		User result = [Select Id, E_ZWEBID__c From User Where Id = :us.Id];
		System.assertEquals(webId, result.E_ZWEBID__c);
	}

	/**
	 * ユーザレコードとＩＤ管理レコードをContactIdから取得
	 */
	@isTest static void getUserRecsByContactIdTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		List<User> result = E_UserDaoWithout.getUserRecsByContactId(us.ContactId);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * ユーザレコードを(username or email) and isActiveで取得する
	 */
	@isTest static void getUserRecsByEmailOrUsernameAndIsActiveTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		List<User> result = E_UserDaoWithout.getUserRecsByEmailOrUsernameAndIsActive(us.Email,null);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * ユーザデータをユーザ名から取得する(ID管理取得)
	 */
	@isTest static void getUserRecByUserNameWithEIDCTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		User result = E_UserDaoWithout.getUserRecByUserNameWithEIDC(us.Username);
		Test.stopTest();

		System.assertEquals(us.Id, result.Id);
	}

	/**
	 * ユーザデータをContactId,UserName,isActiveから取得
	 */
	@isTest static void getUserRecByUserRecTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');

		Test.startTest();
		List<User> result = E_UserDaoWithout.getUserRecByUserRec(us);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	/**
	 * ユーザデータをユーザIdから取得する	ContactやAccountの情報を付加して再取得
	 */
	@isTest static void getUserByUserTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		List<User> userList = new List<User>();
		userList.add(us);

		Test.startTest();
		Map<Id, User> result = E_UserDaoWithout.getUserByUser(userList);
		Test.stopTest();

		System.assertEquals(us.Id, result.get(us.Id).Id);
	}

	/**
	 * 利用募集人一覧を取得
	 */
	@isTest static void getIRISAgentUserRecordTest() {
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		createIDCPF(us.Id);

		Test.startTest();
		List<User> result = E_UserDaoWithout.getIRISAgentUserRecord(zinquirr.right(2));
		Test.stopTest();

		System.assertEquals(0, result.size());		//最終ログイン日時を条件に含む
	}

	/**
	 * ContactIdセットからID管理とともにUserリスト取得
	 */
	@isTest static void getRecsAgentIdWithEIDCTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		createIDCPF(us.Id);
		Set<Id> contactIdSet = new Set<Id>();
		contactIdset.add(us.ContactId);

		Test.startTest();
		List<User> result = E_UserDaoWithout.getRecsAgentIdWithEIDC(contactIdset);
		Test.stopTest();

		System.assertEquals(us.Id, result[0].Id);
	}

	@isTest static void getUserRecordByUserIdTest(){
		//データ作成
		User us = createUser01('E_PartnerCommunity');
		createIDCPF(us.Id);

		Test.startTest();
		User userRec = E_UserDaoWithout.getUserRecordByUserId(us.Id);
		Test.stopTest();

		System.assertEquals(us.Id, userRec.Id);
	}


	//取引先作成
	static Id createAccount01(){
		Account acc = new Account();
		acc.Name = 'テスト株式会社';
		insert acc;
		return acc.Id;
	}

	/**
	 * 募集人作成
	 * Contact.Account.Parentなし
	 */
	static Contact createContactData01(){
		Contact con = new Contact();
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		system.runAs(thisUser){
			Id accid = createAccount01();
			con.FirstName = '花子';
			con.LastName = 'テストテスト';
			con.AccountId = accid;
			insert con;
		}
		return con;
	}

	/**
	 * ユーザ作成
	 * Contactあり
	 * Contact.Account.Parentなし
	 */
	static User createUser01(String profileName){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		User us = new User();
		system.runAs(thisUser){
			Contact con = createContactData01();
			us.Username = 'test@test.com@sfdc.com';
			us.Alias = 'テスト花子';
			us.Email = 'test@test.com';
			us.EmailEncodingKey = 'UTF-8';
			us.LanguageLocaleKey = 'ja';
			us.LastName = 'テスト';
			us.LocaleSidKey = 'ja_JP';
			Profile pro = E_ProfileDaoWithout.getRecByName(profileName);
			us.ProfileId = pro.Id;
			us.TimeZoneSidKey = E_Const.USER_TIMEZONESIDKEY_ASIATOKYO;
			us.MRCD__c = 'testUs';
			us.ContactId = con.Id;
			us.FederationIdentifier = 'TestUser01';
			insert us;
		}
		return us;
	}

	/**
	 * ID管理作成
	 */
	static void createIDCPF(Id userId){
		User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		E_IDCPF__c idcpf = new E_IDCPF__c();
		system.runAs(thisUser){
			idcpf.ZSTATUS01__c = '1';
			idcpf.User__c = userId;
			idcpf.OwnerId = userId;
			idcpf.ZINQUIRR__c = zinquirr;
			insert idcpf;
		}
	}
}