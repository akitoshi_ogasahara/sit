@RestResource(urlMapping='/policies/*')
global without sharing class API_PoliciesListener extends API_Listener{
	@HttpGet
	global static void doGet(){
		new API_PoliciesListener().executeGet();
	}

	//レスポンス生成メソッド
	public override void createRestResponse(RestRequest req,RestResponse res){
		//ユーザーに紐づくContactから、保険契約ヘッダと個人保険特約を参照し、レスポンスを作成
		API_PoliciesBizLogic policiesBiz = new API_PoliciesBizLogic();
		API_PoliciesBizLogic.policies rb = policiesBiz.getPolicies(usr.Contact.Id);

		//保険契約を持っていない場合
		if(String.isBlank(rb.clientno)){
			rb.addErrorMessage('NO_COLI_RECORDS');
			res.statusCode = 404;
		}
		res.responseBody = Blob.valueOf(JSON.serialize(rb));
	}

	/**
	* ID管理を見て、個人保険契約にアクセス権限を持っているかの確認
	* アクセス可能 return true
	* アクセス不可 return 例外処理
	*/
	protected override Boolean hasAuth(){
		//個人保険照会フラグ!='1'のとき、アクセス不可
		if(eidc.FLAG01__c != E_Const.IS_INQUIRY_1){
			throw new API_Listener.RestAPIAuthException('AUTH_NO_COLI');
		}
		return true;
	}
}