/**
 * MNT_YakkanViewExtenderのテストクラス.
 */
@isTest
private class TestMNT_YakkanViewExtender {
	
	/**
	 * doDeleteのテスト.
	 *
	 * 確認事項：削除が成功すること.
	 */
	static testMethod void myUnitTest01() {
		I_ContentMaster__c parent = new I_ContentMaster__c(Name = '親', UpsertKey__c = 'test001');
		insert parent;
		
		I_ContentMaster__c child = new I_ContentMaster__c(Name = '子', UpsertKey__c = 'test002', ParentContent__c = parent.Id);
		insert child;
		
		Set<Id> ids = new Set<Id>{parent.Id, child.Id};
		
		PageReference pageRef = Page.MNT_YakkanView;
		Test.setCurrentPage(pageRef); 
		ApexPages.currentPage().getParameters().put('id', parent.Id);
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(parent);
		MNT_YakkanViewController controller = new MNT_YakkanViewController(stdController);
		MNT_YakkanViewExtender extender = new MNT_YakkanViewExtender(controller);
		
		extender.init();
		
		Test.startTest();
		
		extender.doDelete();
		
		Test.stopTest();
		
		List<I_ContentMaster__c> result = [Select Id From I_ContentMaster__c Where Id In: ids];
		
		System.assertEquals(true, result.isEmpty());
	}
	
	/**
	 * doDeleteのテスト.
	 *
	 * 確認事項：削除が失敗すること.
	 */
	static testMethod void myUnitTest02() {
		I_ContentMaster__c parent = new I_ContentMaster__c(Name = '親', UpsertKey__c = 'test001');
		insert parent;
		
		I_ContentMaster__c child = new I_ContentMaster__c(Name = '子', UpsertKey__c = 'test002', ParentContent__c = parent.Id);
		insert child;
		
		PageReference pageRef = Page.MNT_YakkanView;
		Test.setCurrentPage(pageRef); 
		ApexPages.currentPage().getParameters().put('id', parent.Id);
		
		ApexPages.StandardController stdController = new ApexPages.StandardController(parent);
		MNT_YakkanViewController controller = new MNT_YakkanViewController(stdController);
		MNT_YakkanViewExtender extender = new MNT_YakkanViewExtender(controller);
		
		Test.startTest();
		
		try {
			extender.doDelete();
		} catch(Exception e) {
			System.assert(true);
		}
	}
}