@isTest
private class TestE_ReissuePasswordController {

	//認証画面_コンストラクタ
	//static testMethod void constructorTest01(){

	//	PageReference pageRef = Page.E_ReissuePassword;
	//	//pageRef.getParameters().put('isPassedDays', '1');
	//	//Test.setCurrentPage(pageRef);

	//	Test.startTest();
	//	E_ReissuePasswordController controller = new E_ReissuePasswordController();
	//	Test.stopTest();

	//	System.assertEquals(false, controller.isReissue);
	//}

	//認証画面_ページアクション
	static testMethod void pageActionTest01(){

		E_ReissuePasswordController controller = new E_ReissuePasswordController();

		Test.startTest();
		PageReference result = controller.pageAction();
		Test.stopTest();

		System.assertEquals(null, result);
	}

	//認証画面_[次へ]ボタン
	//正常系
	static testMethod void doReissuePasswordTest01(){

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User u = TestE_TestUtil.createUser(false, 'userId', 'システム管理者');
		u.username = 'userId'+System.Label.E_USERNAME_SUFFIX;
		insert u;
		E_ReissuePasswordController controller = new E_ReissuePasswordController();
		controller.userId = 'userId';

		Test.startTest();
		PageReference result = controller.doReissuePassword();
		Test.stopTest();

		System.assertEquals(Page.E_ReissuePasswordComp.getUrl(), result.getUrl());
	}

	//認証画面_[次へ]ボタン
	//ユーザID未入力
	static testMethod void doReissuePasswordTest02(){

		//メッセージマスタ作成
		E_MessageMaster__c LGE202 = TestE_TestUtil.createMessageMaster(false, 'パスワードリセット認証画面_ユーザIDを入力していない場合', null, 'MSG_DISCLAIMER', E_Const.E_MEGTYPE_MEG, 'LRE|202');
		LGE202.Value__c = 'パスワードリセット認証画面_ユーザIDを入力していない場合';
		insert LGE202;

		E_ReissuePasswordController controller = new E_ReissuePasswordController();
		controller.userId = '';

		Test.startTest();
		PageReference result = controller.doReissuePassword();
		Test.stopTest();

		System.assertEquals(null, result);
		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(LGE202.Value__c, controller.pageMessages.getErrorMessages()[0].summary);
	}

	//認証画面_[次へ]ボタン
	//ユーザID不正
	static testMethod void doReissuePasswordTest03(){

		//メッセージマスタ作成
		E_MessageMaster__c LGE202 = TestE_TestUtil.createMessageMaster(false, 'パスワードリセット認証画面_入力したユーザIDが不正の場合', null, 'MSG_DISCLAIMER', E_Const.E_MEGTYPE_MEG, 'LGE|001');
		LGE202.Value__c = 'パスワードリセット認証画面_入力したユーザIDが不正の場合';
		insert LGE202;

		E_ReissuePasswordController controller = new E_ReissuePasswordController();
		controller.userId = 'userId00';

		Test.startTest();
		PageReference result = controller.doReissuePassword();
		Test.stopTest();

		System.assertEquals(null, result);
		System.assert(controller.pageMessages.getHasErrorMessages());
		System.assertEquals(LGE202.Value__c, controller.pageMessages.getErrorMessages()[0].summary);
	}

	//認証画面_[次へ]ボタン
	//
	static testMethod void doReissuePasswordTest04(){

		//テストユーザ作成(共通クラスのメソッド呼び出し)
		User us1 = TestE_TestUtil.createUser(false, 'userId01', 'システム管理者');
		us1.username = 'userId01'+System.Label.E_USERNAME_SUFFIX;
		insert us1;
		User us2 = TestE_TestUtil.createUser(false, 'userId02', 'システム管理者');
		us2.username = 'userId02'+System.Label.E_USERNAME_SUFFIX;
		insert us2;

		PageReference result;
		System.runAs(us1){
			E_ReissuePasswordController controller = new E_ReissuePasswordController();
			controller.userId = 'userId02';

			Test.startTest();
			result = controller.doReissuePassword();
			Test.stopTest();
		}

		System.assertEquals(Page.E_ReissuePasswordComp.getUrl(), result.getUrl());
	}

	//[戻る]ボタン
	//遷移元画面URLがある場合
	static testMethod void doBackTest01(){

		E_ReissuePasswordController controller = new E_ReissuePasswordController();

		Test.startTest();
		PageReference result = controller.doBack();
		Test.stopTest();

		System.assertEquals(Page.E_Exception.getUrl(), result.getUrl());
	}

	//完了画面_ページアクション
	//認証画面を経由していない場合
	static testMethod void pageAction_CompTest01(){

		E_ReissuePasswordController controller = new E_ReissuePasswordController();

		Test.startTest();
		PageReference result = controller.pageAction_Comp();
		Test.stopTest();

		System.assertEquals(Page.E_Exception.getUrl(), result.getUrl());
	}

	//完了画面_ページアクション
	//認証画面を経由している場合
	static testMethod void pageAction_CompTest02(){

		E_ReissuePasswordController controller = new E_ReissuePasswordController();
		controller.pageAction();

		Test.startTest();
		PageReference result = controller.pageAction_Comp();
		Test.stopTest();

		System.assertEquals(null, result);
	}
}