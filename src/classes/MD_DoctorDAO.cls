public with sharing class MD_DoctorDAO {
	/**
	 * ドクター情報取得
	 * @param String Where
	 * @param String Orderby
	 * @return List<MD_Doctor__c>
	 */
	public static List<MD_Doctor__c> getRecsByWhere(String strWhere, String strOrder, Integer limitCnt) {
		String soql = '';

		soql += 'SELECT';
		soql += ' Id';
		soql += ',Name';
		soql += ',Location__Latitude__s';
		soql += ',Location__Longitude__s';
		soql += ',HospitalName__c';
		soql += ',Age__c';
		soql += ',Gender__c';
		soql += ',GenderView__c';
		soql += ',State__c';
		soql += ',Address__c';
		soql += ',Phone__c';
		soql += ',ParticularDoctor__c';
		soql += ',ParticularDoctorView__c';
		soql += ',Blood__c';
		soql += ',ExaminationDate__c';
		soql += ',Electrocardiogram__c';
		soql += ',HouseCall__c';
		soql += ',ConsultationTime__c';
		soql += ' FROM';
		soql += ' MD_Doctor__c';

		// Where句
		if (String.isNotBlank(strWhere)) {
			soql += ' WHERE ';
			soql += strWhere;
		}

		// Orderby句
		if (String.isNotBlank(strOrder)) {
			soql += ' ORDER BY ';
			soql += strOrder;
		}

		// Limit
		if (limitCnt > 0) {
			soql += ' LIMIT ';
			soql += limitCnt;
		}
		system.debug('■[soql]' + soql);

		return Database.query(soql);
	}

	/**
	 * ドクター情報取得(件数カウント)
	 * @param String strWhere
	 * @return Integer
	 */	
	public static Integer getRecsByWhereCount(String strWhere, Integer limitCnt) {
		String soql = '';

		soql += 'SELECT';
		soql += ' count()';
		soql += ' FROM';
		soql += ' MD_Doctor__c';

		// Where句生成
		if (String.isNotBlank(strWhere)) {
			soql += ' WHERE ';
			soql += strWhere;
		}

		// 上限を超えているかどうかだけ分かれば良いので Limit + 1
		if (limitCnt > 0) {
			soql += ' LIMIT ';
			soql += (limitCnt + 1);
		}

		system.debug('■[soql]' + soql);

		return Database.countQuery(soql);
	}

	/** 
	 * 条件からドクター情報のリストを取得
	 * @param condition
	 * @param limitCnt
	 * @return List<MD_Doctor__c>
	 */
	public static List<MD_Doctor__c> getRecsByCondition(String condition, Integer limitCnt) {
		String soql = 'Select ';
		soql += 'Id, Location__Latitude__s, Location__Longitude__s, HospitalName__c, Name, Age__c, Gender__c, GenderView__c, State__c, Address__c, Phone__c, ParticularDoctor__c, ParticularDoctorView__c, Blood__c, ExaminationDate__c, Electrocardiogram__c, HouseCall__c ';
		soql += 'From MD_Doctor__c ';

		if (String.isNotBlank(condition)) {
			soql += 'Where ';
			soql += condition;
		}

		if (limitCnt > 0) {
			soql += ' LIMIT ';
			soql += limitCnt;
		}

		return Database.query(soql);
	}
}