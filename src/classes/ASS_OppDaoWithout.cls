public without sharing class ASS_OppDaoWithout {

	/**
	 * 見込案件リスト取得
	 * @param offIdSet: 事務所IDセット 
	 * @return List<Opportunity>: 見込案件リスト
	 */
	public static List<Opportunity> getThisMonthOppsByOffIdSet(Set<Id> offIdSet) {
		return [SELECT 
					ChannelMiddle__c                //チャネル　中分類
					, PtFsFormula__c                //PT／FS
					, WAPE__c                       //WAPE
					, StageName                     //レベル
					, CloseDate                     //レベル更新日
					, AccountId                     //代理店事務所ID
					, RecordTypeId
				FROM 
					Opportunity
				WHERE
					AccountId IN : offIdSet         //取引先IDが事務所リストに
				AND 
					CloseDate = THIS_MONTH          //レベル更新日が当月
				Order By 
					AccountId
				];
	}

	/**
	 * 見込案件リスト取得
	 * @param offIdSet: 事務所IDセット
	 * @param level: 集計対象のレベルリスト
	 * @return List<Opportunity>: 見込案件リスト
	 */
	public static List<Opportunity> getThisMonthOppsByOffIdSetAndLevel(Set<Id> offIdSet, List<String> levels) {
		return  [SELECT 
					WAPE__c                         //WAPE
					, PtFsFormula__c                //PT／FS
					, ChannelMiddle__c              //チャネル　中分類
					, RecordTypeId                  //レコードタイプId
					, ProductName_TEXT__c           //営業予算項目
				FROM 
					Opportunity
				WHERE 
					CloseDate = THIS_MONTH
				AND 
					AccountId IN : offIdSet
				AND 
					StageName IN : levels
				];
	}

	/**
	 * 見込案件リスト取得
	 * @param officeId: 事務所ID 
	 * @param levels: 集計対象のレベルリスト 
	 * @return List<Opportunity>: 見込案件リスト
	 */
	public static List<Opportunity> getOppsByOfficeId(String officeId, List<String> levels) {
		return [SELECT 
					Name                //契約者名
					, StageName         //レベル
					, CloseDate         //レベル更新日
					, WAPE__c           //WAPE
				FROM 
					Opportunity 
				WHERE 
					CloseDate = THIS_MONTH 
				AND 
					AccountId = :officeId 
				AND 
					StageName IN : levels
				];
	}

	/**
	 * 見込案件リスト取得
	 * @param mrId: MRId
	 * @param level: 集計対象のレベルリスト
	 * @return List<Opportunity>: 見込案件リスト
	 */
	public static List<Opportunity> getThisMonthOppsByOwnerIdAndLevel(String mrId, List<String> levels) {
		return  [SELECT 
					WAPE__c							//WAPE
					, PtFsFormula__c 				//PT／FS
					, ChannelMiddle__c 				//チャネル　中分類
					, RecordTypeId 					//レコードタイプId
					, ProductName_TEXT__c 			//営業予算項目
				FROM 
					Opportunity
				WHERE 
					CloseDate = THIS_MONTH
				AND 
					OwnerId = :mrId
				AND 
					StageName IN :levels
				];
	}
}