@isTest
private class TestE_VerticalLabelsController {
	private static testMethod void getStringList_test01() {
		E_VerticalLabelsController con = new E_VerticalLabelsController();
		con.rowSize = 0;
		con.dataStrings = new List<String>();
		con.dataStrings.add('test1');
		con.dataStrings.add('test2');
		con.dataStrings.add('test3');
		con.dataStrings.add('test4');
		con.dataStrings.add('test5');
		con.dataStrings.add('test6');
		con.dataStrings.add('test7');
		Test.startTest();
		con.getStringList();
		Test.stopTest();
		System.assertEquals(con.getStringList().size(), 2);
		System.assertEquals(con.getStringList().get(0).get(0), 'test1');
		System.assertEquals(con.getStringList().get(1).get(0), 'test6');
		System.assertEquals(con.getStringList().get(1).get(4), '');
	}
}