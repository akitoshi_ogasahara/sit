@isTest (SeeAllData=false)
private class TestE_ProcessHistoryExtender{

		//パラメータがpidの場合
        private static testMethod void testProcessHistoryExtender1() {
			//テストデータ作成
        	E_Policy__c policy = new E_Policy__c();
        	policy.RecordTypeId = E_RecordTypeDao.getEpolicyRecordTypeID('ECHDPF');
        	insert policy;
        	E_PTNPF__c ptnpf = new E_PTNPF__c( E_Policy__c = policy.Id );
        	insert ptnpf;

        	PageReference resultPage;

        	//テストユーザ作成
        	User u = TestE_TestUtil.createUser(true, 'testUser', 'システム管理者');
        	//ID管理DBへレコード追加:手続きの履歴機能の設定値
        	E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        	idcpf.FLAG01__c = '1';
        	idcpf.FLAG02__c = '1';
        	idcpf.FLAG06__c = '1';
        	insert idcpf;

			System.runAs(u){
				Test.startTest();
        		PageReference pref = Page.E_ProcessHistory;
        		pref.getParameters().put('pid',policy.Id);
        		Test.setCurrentPage(pref);
        		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
        		E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
        		E_ProcessHistoryExtender extender = controller.getExtender();
        		resultPage =  extender.pageAction();
        		extender.getProcessHistoryCloseBackLabel();
        		Test.stopTest();
			}
        	// 確認
        	System.assertEquals(resultPage,null);
        }


		//パラメータがcid,cno,cnameの場合
		private static testMethod void testProcessHistoryExtender2() {
			//テストデータ作成
			Contact con = new Contact();
			con.LastName = '苗字';
			con.FirstName = '名前';
			con.E_CLTPF_CLNTNUM__c = '98765432';
			insert con;
        	E_Policy__c policy = new E_Policy__c();
        	policy.RecordTypeId = E_RecordTypeDao.getEpolicyRecordTypeID('ECHDPF');
        	policy.Contractor__c = con.Id;
        	insert policy;
        	E_PTNPF__c ptnpf = new E_PTNPF__c( E_Policy__c = policy.Id );
        	insert ptnpf;

        	PageReference resultPage;

        	//テストユーザ作成
        	User u = TestE_TestUtil.createUser(true, 'testUser', 'システム管理者');
        	//ID管理DBへレコード追加:手続きの履歴機能の設定値
        	E_IDCPF__c idcpf = TestE_TestUtil.createIDCPF(false,u.Id);
        	idcpf.FLAG01__c = '1';
        	idcpf.FLAG02__c = '1';
        	idcpf.FLAG06__c = '1';
        	insert idcpf;

			System.runAs(u){
				Test.startTest();
        		PageReference pref = Page.E_ProcessHistory;
        		pref.getParameters().put('cid',con.Id);
        		pref.getParameters().put('cno','98765432');
        		pref.getParameters().put('cname','苗字 名前');
        		Test.setCurrentPage(pref);
        		Apexpages.Standardcontroller standardcontroller = new Apexpages.Standardcontroller(policy);
        		E_ProcessHistoryController controller = new E_ProcessHistoryController(standardcontroller);
        		E_ProcessHistoryExtender extender = controller.getExtender();
        		resultPage =  extender.pageAction();
        		extender.getProcessHistoryCloseBackLabel();
        		Test.stopTest();
			}
        	// 確認
        	System.assertEquals(resultPage,null);
        }

}