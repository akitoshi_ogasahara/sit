@isTest(SeeAllData=false)
private class TestE_LogoDownloadController {

	private static testMethod void standardTest(){
		E_MenuMaster__c insEMenu = new E_MenuMaster__c(name = 'test',MenuMasterKey__c = 'logo_mark',SelectedMenuKey__c = 'logo_mark',IsHideMenu__c =false,IsShowCloseBtn__c = true );
		insert insEMenu;
		TestE_TestUtil.createIDCPF(true,UserInfo.getUserId());
		PageReference pref = Page.E_LogoDownload;
		test.setCurrentPage(pref);
		E_LogoDownloadController controller = new E_LogoDownloadController();
		controller.init();
		string key = controller.menu.SelectedMenuKey__c;
		system.assertEquals('logo_mark', key);
	}
	

}