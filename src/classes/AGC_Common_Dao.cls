public with sharing class AGC_Common_Dao{
    
    //ステートレスで処理させるため何もしない。
    public AGC_Common_Dao(){
    }
    
    //代理店補足情報を取得する
    public AGC_DairitenJimushoHosokujoho__c getDairitenHosokujoho(Id id){
        return [select 
            Name,
            AgencyInfoRef__c,
            AgencyInfo__c,
            BranchID__c,
            Competitor__c,
            JoinStatus__c,
            MailAddr1__c,
            MailAddr2__c,
            MailAddr3__c,
            MailOwner1__c,
            MailOwner2__c,
            MailOwner3__c,
            MobileNumber1__c,
            MobileNumber2__c,
            MobileOwner1__c,
            MobileOwner2__c,
            Segment__c,
            SendMethod__c,
            Status__c,
            TransferDate__c,
            Id
        from AGC_DairitenJimushoHosokuJoho__c
        where Id = :id];
    }
    
    //代理店補足情報を取得する
    public AGC_DairitenJimushoHosokujoho__c getDairitenHosokujohoBykihonId(Id id){
        return [select 
            Name,
            AgencyInfoRef__c,
            AgencyInfo__c,
            BranchID__c,
            Competitor__c,
            JoinStatus__c,
            MailAddr1__c,
            MailAddr2__c,
            MailAddr3__c,
            MailOwner1__c,
            MailOwner2__c,
            MailOwner3__c,
            MobileNumber1__c,
            MobileNumber2__c,
            MobileOwner1__c,
            MobileOwner2__c,
            Segment__c,
            SendMethod__c,
            Status__c,
            TransferDate__c,
            Id
        from AGC_DairitenJimushoHosokuJoho__c
        where AgencyInfoRef__c = :id];
    }
    
    //代理店基本情報を取得する。
    public AGC_DairitenKihonjoho__c getKihonJoho(Id id){

        return [select Name,
            AGCA_ChannelFlag__c,
            AGCA_Kname__c,
            AGCA_TransferCnt__c,
            AGCA_TransferDate__c,
            AYSI_AgencyCnt__c,
            AYSI_ZHOIANP__c,
            AYSI_ZHOISUM__c,
            AddAgency__c,
            AddReason__c,
            AddRelation__c,
            Address1__c,
            Address2__c,
            Address3__c,
            Address4__c,
            AgencyFax__c,
            AgencyID__c,
            AgencyKJName1__c,
            AgencyKJName2__c,
            AgencyKanaName1__c,
            AgencyKanaName2__c,
            AgencyShare__c,
            AgencyTel1__c,
            AgencyTel2__c,
            AgentClassificaion__c,
            AgentKJGName__c,
            AgentKJSName__c,
            AgentKNGName__c,
            AgentKNSName__c,
            AgentNumber__c,
            AgentStatus__c,
            AgentType__c,
            BotenBRID__c,
            BotenID__c,
            BranchCodeRef__c,
            BranchID__c,
            BroadAreaAgencyFlag__c,
            Classification__c,
            ClientNumber__c,
            GroupTerm__c,
            InvestmentTrustFlag__c,
            InvstTstAccCnt__c,
            Kchannel__c,
            MRCodeRef__c,
            ManagerCode__c,
            ManagerKJGName__c,
            ManagerKJSName__c,
            ManagerKNGName__c,
            ManagerKNSName__c,
            PostalCode__c,
            ProxyApp__c,
            RetainCnt__c,
            RetainGroupCnt__c,
            SPVAPRetainCnt__c,
            SPVAPRetainDth__c,
            SPVAP__c,
            Source__c,
            Status__c,
            TounenKansanAnp__c,
            TounenShinKeiyakuAnp__c,
            TounenShinKeiyakuCnt__c,
            TounenSpva__c,
            VACert__c,
            XHAHCalculateFromDate__c,
            XHAHClaimApproveDate__c,
            XHAH_AGNTBR__c,
            AgencyName__c,
            AgencyKanaName__c,
            AgentName__c,
            Address__c,
            ManagerName__c,
            ManagerKanaName__c,
            AgentKanaName__c,
            Searchkey__c,
            Id 
        from AGC_DairitenKihonjoho__c
        where id = :Id];        
    }
    
    //業廃フラグをたてるべきレコードを取得する
    public List<AGC_DairitenKihonjoho__c> getGyouhaiRecords(Date gyohaidate){
    return [select DelRecord__c,
                AGCA_TransferDate__c,
                Id
            from
                AGC_DairitenKihonJoho__c
            where
                DataInputDate__c < :gyohaidate AND DelRecord__c = false];
    }
    
    
    
    //支社MRマスタを取得する。
    public AGC_ShishaMr_Mst__c getShishaMrMstById(Id id){
        return [select Name,MRCode__c,DisplayName__c from AGC_ShishaMr_Mst__c where Id = :id];
    }
    
    //支社マスタを取得する。
    public AGC_Shisha_Mst__c getShishaMstById(Id id){
        return [select Id,Name,ShishaCd__c from AGC_Shisha_Mst__c where Id = :id];
    }
    
    
    //コンタクト履歴を取得する。
    public AGC_ContactRireki__c getContactRirekiListById(Id id){
        return [
            select Id,
                Name,
                AGC_DairitenKihonJoho_Ref__c,
                ContactPerson__c,
                InquiryCnt__c,
                InquiryDetails__c,
                InquiryTitle__c,
                InquiryTypeCode__c,
                ReceptionData__c,
                ReceptionStaff__c,
                ReceptionTypeID__c,
                CollectFlg__c
            from AGC_ContactRireki__c
            where Id = :id
        ];
    }   
    
    //代理店補足情報のIDをキーにコンタクト履歴を取得する。
    public List<AGC_ContactRireki__c> getContactRirekiListByKihonId(Id id){
    
        return [
            select Id,
                Name,
                AGC_DairitenKihonJoho_Ref__c,
                ContactPerson__c,
                InquiryCnt__c,
                InquiryDetails__c,
                InquiryTitle__c,
                InquiryTypeCode__c,
                ReceptionData__c,
                ReceptionStaff__c,
                ReceptionTypeID__c,
                CollectFlg__c
            from AGC_ContactRireki__c
            where AGC_DairitenKihonJoho_Ref__c = :id
            order by ReceptionData__c desc
        ];
    }
    
}