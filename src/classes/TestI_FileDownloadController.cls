@isTest
private class TestI_FileDownloadController {

	//DLファイル格納先とAttachment共通のファイル名
	private static String fileName = 'fileName';

/*
* 観点:DL(Attachment)正常系 添付ファイル1件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:取得したいIRIS_CMSコンテンツのDLファイル格納先と同名のAttachmentIdが取得されること
*/
	static testMethod voId DL_Attachment_fileId_getter_test01(){
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT,fileName);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals( cpnt.Attachments[0].Id, fileId);
		System.assertEquals( cpnt.ClickAction__c, clickAction);
		System.assertEquals( cpnt.Attachments[0].Name , cpnt.FilePath__c);
	}

/*
* 観点:DL(Attachment)正常系 添付ファイル3件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:同名の添付ファイルが3件以上登録されていたら、最終更新日が最近のAttachmentのIdが取得される
*/
	static testMethod voId DL_Attachment_fileId_getter_test02(){
		Integer attNum = 3;
		I_ContentMaster__c cpnt = setTestcpnt(attNum,I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT,fileName);

		//添付ファイルのリスト2つ目を更新する
		Attachment updatedAtt = cpnt.Attachments[attNum-2];
		updatedAtt.Description = 'リスト二つ目のAttachmentの説明を更新';
		update updatedAtt;

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		List<Attachment> checkList = [SELECT Id, Description, Name, LastModifiedDate FROM Attachment ORDER BY LastModifiedDate Desc];

		System.assertEquals( updatedAtt.Name , cpnt.FilePath__c);
		//最終更新日に差異が出なかった場合を考慮
		if(checkList[0].LastModifiedDate != checkList[1].LastModifiedDate){
			System.assertEquals( updatedAtt.Id, fileId);
		}
	}

/*
* 観点:DL(Attachment)正常系 添付ファイル3件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:添付ファイルが3件以上登録されていたら、DLファイル格納先と同名で最終更新日が最新のAttachmentのIdが取得される
*/
	static testMethod voId DL_Attachment_fileId_getter_test03(){
		Integer attNum = 3;
		I_ContentMaster__c cpnt = setTestcpnt(attNum,I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT,fileName);

		List<Attachment> updateAttList = new List<Attachment>();
		//Attachmentsリスト一つ目のファイル名を更新
		cpnt.Attachments[0].Name = '異なるファイル名';
		updateAttList.add(cpnt.Attachments[0]);
		//Attachmentsリスト二つ目の説明を更新
		Attachment updatedAtt = cpnt.Attachments[1];
		updatedAtt.Description = '取得したいAttachmentの説明です。';
		updateAttList.add(updatedAtt);

		update updateAttList;

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		List<Attachment> checkList = [SELECT Id, Description, Name, LastModifiedDate FROM Attachment ORDER BY LastModifiedDate Desc];

		System.assertEquals( updatedAtt.Name , cpnt.FilePath__c);
		//最終更新日に差異が出なかった場合を考慮
		if(checkList[0].LastModifiedDate != checkList[1].LastModifiedDate){
			System.assertEquals( updatedAtt.Id, fileId);
		}
	}


/*
* 観点:DL(Attachment)異常系　添付ファイルなし
* 条件:コントローラのfileUpsertKeyに、添付ファイルが未登録のIRIS_CMSコンテンツのUpsertKeyがsetされている
* 検証:添付ファイルが登録されていなければ、nullが代入される
*/
	static testMethod voId DL_Attachment_fileId_getter_test04(){
		I_ContentMaster__c cpnt = setTestcpnt(0, I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT,fileName);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:DL(Attachment)異常系　外部ID誤り
* 条件:コントローラのfileUpsertKeyに、存在しないIRIS_CMSコンテンツの外部IDがsetされている
* 検証:setしたUpsertKeyと合致するIRIS_CMSコンテンツがない場合、FileIdにnullが設定される
*/
	static testMethod voId DL_Attachment_fileId_getter_test05(){
		I_ContentMaster__c cpnt = setTestcpnt(3, I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT,fileName);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = '存在しない外部ID';
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:DL(Attachment)異常系　DLファイル格納先誤り
* 条件:コントローラのfileUpsertKeyに、添付ファイルが未登録のIRIS_CMSコンテンツのUpsertKeyがsetされている
* 検証:DLファイル格納先と一致する添付ファイルが見つからなければ、nullが代入される
*/
	static testMethod voId DL_Attachment_fileId_getter_test06(){
		I_ContentMaster__c cpnt = setTestcpnt(3, I_Const.CMSCONTENTS_CLICKACTION_DL_ATTACHMENT,fileName);

		cpnt.FilePath__c = '存在しないファイル';
		update cpnt;

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:DL(Attachment)正常系 2件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:取得したいIRIS_CMSコンテンツのDLファイル格納先と同名のAttachmentIdが取得されること
*/
	static testMethod voId DL_Attachment_fileId_getter_test07(){
		I_ContentMaster__c cpnt1 = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE, 'fileName1', 'cmsName1');
		I_ContentMaster__c cpnt2 = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE, 'fileName2', 'cmsName2');
		I_FileDownloadController.cmsMap = new Map<String, I_ContentMaster__c>{cpnt1.UpsertKey__c=>cpnt1, cpnt2.UpsertKey__c=>cpnt2};

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKeys = new List<String>{cpnt1.UpsertKey__c, cpnt2.UpsertKey__c};
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();
	}

/*
* 観点:DL(Attachment)正常系 2件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:取得したいIRIS_CMSコンテンツのDLファイル格納先と同名のAttachmentIdが取得されること
*/
	static testMethod voId DL_Attachment_fileId_getter_test08(){
		I_ContentMaster__c cpnt1 = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE, 'fileName1', 'cmsName1');
		I_ContentMaster__c cpnt2 = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE, 'fileName2', 'cmsName2');

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKeys = new List<String>{cpnt1.UpsertKey__c, cpnt2.UpsertKey__c};
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();
	}

//				これよりDL(NNLink)のテスト

/* 観点:DL(NNLink)正常系:　ファイル情報-添付ファイル1件
* 条件:コントローラのfileUpsertKeyに、IRIS_CMSコンテンツの外部IDがsetされている
* 検証:ファイル情報の添付ファイルIDがセットされること
*/
	static testMethod voId DL_NNLink_fileId_getter_test01(){
		E_CMSFile__c cmsFile = setTestcmsFile(1,fileName);
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK,cmsFile.UpsertKey__c);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(cmsFile.Attachments[0].Id ,fileId);
	}

/*
* 観点:DL(NNLink)正常系:　ファイル情報-添付ファイル3件
* 条件:コントローラのfileUpsertKeyに、IRIS_CMSコンテンツの外部IDがsetされている
* 検証:ファイル情報の添付ファイルIDがセットされること
*/
	static testMethod voId DL_NNLink_fileId_getter_test02(){
		Integer attNum = 3;
		E_CMSFile__c cmsFile = setTestcmsFile(attNum,fileName);
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK,cmsFile.UpsertKey__c);

		//添付ファイルのリスト2つ目を更新する
		Attachment updatedAtt = cmsFile.Attachments[attNum-2];
		updatedAtt.Description = 'リスト二つ目のAttachmentの説明を更新';
		update updatedAtt;

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		List<Attachment> checkList = [SELECT Id, Description, Name, LastModifiedDate FROM Attachment ORDER BY LastModifiedDate Desc];

		//最終更新日に差異が出なかった場合を考慮
		if(checkList[0].LastModifiedDate != checkList[1].LastModifiedDate){
			System.assertEquals(updatedAtt.Id ,fileId);
		}
	}


/*
* 観点:DL(NNLink)異常系　ファイル情報-添付ファイルなし
* 条件:コントローラのfileUpsertKeyに、IRIS_CMSコンテンツの外部IDがsetされている
* 検証:添付ファイルが登録されていなければ、nullが代入される
*/
	static testMethod voId DL_NNLink_fileId_getter_test03(){
		E_CMSFile__c cmsFile = setTestcmsFile(0,fileName);
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK,cmsFile.UpsertKey__c);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:DL(NNLink)異常系　外部ID誤り
* 条件:コントローラのfileUpsertKeyに、存在しないIRIS_CMSコンテンツの外部IDがsetされている
* 検証:setしたUpsertKeyと合致するIRIS_CMSコンテンツがない場合、FileIdにnullが設定される
*/
	static testMethod voId DL_NNLink_fileId_getter_test04(){
		E_CMSFile__c cmsFile = setTestcmsFile(0,fileName);
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK,cmsFile.UpsertKey__c);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = '存在しない外部ID';
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:DL(NNLink)異常系　DLファイル格納先誤り
* 条件:コントローラのfileUpsertKeyに、添付ファイルが未登録のIRIS_CMSコンテンツのUpsertKeyがsetされている
* 検証:DLファイル格納先と一致する添付ファイルが見つからなければ、nullが代入される
*/
	static testMethod voId DL_NNLink_fileId_getter_test05(){
		E_CMSFile__c cmsFile = setTestcmsFile(0,fileName);
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_NNLINK,cmsFile.UpsertKey__c);

		cpnt.FilePath__c = '存在しないファイル情報.UpsertKey';
		update cpnt;

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:DL(Chatterfile)正常系 添付ファイル1件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:取得したいIRIS_CMSコンテンツのDLファイル格納先と同名のAttachmentIdが取得されること
*/
	static testMethod voId DL_Chatterfile_fileId_getter_test01(){
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_DL_CHATTERFILE,fileName);

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(null, fileId);
	}

/*
* 観点:URL正常系 添付ファイル1件
* 条件:コントローラのfileUpsertKeyに、取得したいIRIS_CMSコンテンツのUpsertKeyとクリックアクションがsetされている
* 検証:取得したいIRIS_CMSコンテンツのDLファイル格納先と同名のAttachmentIdが取得されること
*/
	static testMethod voId URL_fileId_getter_test01(){
		I_ContentMaster__c cpnt = setTestcpnt(1, I_Const.CMSCONTENTS_CLICKACTION_URL,fileName);
		cpnt.LinkURL__c = 'url';
		update cpnt;

		Test.startTest();
		I_FileDownloadController ctrl = new I_FileDownloadController();
		ctrl.cmsUpsertKey = cpnt.UpsertKey__c;
		String clickAction = ctrl.clickAction;
		String fileId = ctrl.fileId;
		Test.stopTest();

		System.assertEquals(cpnt.LinkURL__c, fileId);
	}

/*
* テストデータを作成する（IRIS_CMSコンテンツ）
* @param attNum 作成する添付ファイル数
* @param clickAction 作成するIRIS_CMSコンテンツのクリックアクション
* @return cpnt I_ContentMaster__c 登録したIRIS_CMSコンテンツ
*/
	public static I_ContentMaster__c setTestcpnt(Integer attNum,String clickAction,String filePath){
		return setTestcpnt(attNum,clickAction,filePath,'テストCMSコンテンツ');
	}
	public static I_ContentMaster__c setTestcpnt(Integer attNum,String clickAction,String filePath,String fileName){
		//IRIS_CMSコンテンツ - 最後のファイルをDLファイル格納先に設定
		I_ContentMaster__c cpnt = TestI_TestUtil.createContentMaster(true, fileName, clickAction, filePath);

		//IRIS_CMSコンテンツに紐づく添付ファイル　すべて同名*attNumに指定した数
		List<Attachment> attList = new List<Attachment>();
		while(attList.size() < attNum){
			Attachment att = TestI_TestUtil.createAttachment(false, cpnt.Id);
			att.Name = filePath;
			attList.add(att);
		}
		insert attList;

		//Upsertkeyが更新されるため、リフレッシュして返す
		return [SELECT UpsertKey__c
						,FilePath__c
						,ClickAction__c
						,(SELECT Id, Name FROM Attachments)
				FROM I_ContentMaster__c
				WHERE Id =: cpnt.Id];
	}


/*
* テストデータを作成する（ファイル情報）
* @param attNum 作成する添付ファイル数
* @param cmsFileName 作成するIRIS_CMSコンテンツのクリックアクション
* @return cpnt I_ContentMaster__c 登録したIRIS_CMSコンテンツ
*/
	public static E_CMSFile__c setTestcmsFile(Integer attNum,String cmsFileName){
		//親メッセージマスタ
		E_MessageMaster__c msg = TestE_TestUtil.createMessageMaster(true,'メッセージマスタ',null,'CMS','CMS','1');
		//ファイル情報
		E_CMSFile__c cmsFile = TestI_TestUtil.createCMSFile(true,cmsFileName,msg.Id,'pattern02');

		//ファイル情報に紐づく添付ファイル　*attNumに指定した数
		List<Attachment> attList = new List<Attachment>();
		while(attList.size() < attNum){
			Attachment att = TestI_TestUtil.createAttachment(false, cmsFile.Id);
			att.Name = cmsFileName;
			attList.add(att);
		}
		insert attList;

		//Upsertkeyが更新されるため、リフレッシュして返す
		return [SELECT Id
						,Name
						,Note__c
						,UpsertKey__c
						,(SELECT Id, Name FROM Attachments)
				FROM E_CMSFile__c
				WHERE Id =: cmsFile.Id];
	}

}