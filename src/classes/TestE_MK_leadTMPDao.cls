@isTest
private class TestE_MK_leadTMPDao{

		private static testMethod void TestgetRecCountByCmpgnTrgt_001(){
			//キャンペーンターゲットを作成
			MK_CampaignTarget__c MK_CampaignTarget1 = TestMK_TestUtil.createMK_CampaignTarget(true);
			//キャンペーンターゲットID呼び出し=cid
			ID cid = MK_CampaignTarget1.id;
			//キャンペーンターゲットID(=cid)をパラメータとして、リードTMPレコード(=MK_leadTMP1)を作成
			MK_leadTMP__c MK_leadTMP1 =TestMK_TestUtil.createMK_leadTMP(true,cid);
			//テストされる側のメソッドを呼び出す
			Integer ldtmp =E_MK_leadTMPDao.getRecCountByCmpgnTrgt(cid);
            //キャンペーンターゲットに紐づくリードTMPが1名
			System.assertEquals(1,ldtmp);
        }

        private static testMethod void TestgetRecById_001(){
        	//キャンペーンターゲットを作成
			MK_CampaignTarget__c MK_CampaignTarget = TestMK_TestUtil.createMK_CampaignTarget(true);
			//キャンペーンターゲットID呼び出し=ctid
			ID ctid = MK_CampaignTarget.id;
			//パラメータとしてダミーメールアドレス作成
			String email = 'a@example.com';
			//キャンペーンターゲットID(=ctid)、メールアドレス(=email)をパラメータとして、リードTMPレコード(=MK_leadTMP)を作成
			//リードTMPのキャンペーンターゲットID、姓、名、セイ、メイ、会社名、リードフラグ、メールアドレス取得
			MK_leadTMP__c MK_leadTMP = TestMK_TestUtil.createMK_leadTMP(true,ctid,email);
			//テストされるメソッドにリードTMPIDを渡す。(他の項目に値が入る)
			MK_leadTMP__c recs = E_MK_leadTMPDao.getRecById(MK_leadTMP.id);
            System.assertEquals(MK_leadTMP.id,recs.id);
			System.assertEquals('company',recs.Company__c);
			System.assertEquals('姓',recs.LastName__c);
			System.assertEquals('名',recs.FirstName__c);
			System.assertEquals('セイ',recs.LastNameKana__c);
			System.assertEquals('メイ',recs.FirstNameKana__c);
			System.assertEquals('a@example.com',recs.Email__c);
			System.assertEquals(ctId,recs.MK_CampaignTarget__c);
			System.assertEquals(true,recs.CreateLeadFlag__c);
			}
    
    private static testMethod void TestgetEmailListByCmpgnTrgt_001(){
			//キャンペーンターゲットを作成
			MK_CampaignTarget__c MK_CampaignTarget = TestMK_TestUtil.createMK_CampaignTarget(true);
			//キャンペーンターゲットID呼び出し=ctid
			  ID ctid = MK_CampaignTarget.id;
			//メールアドレス作成
			String email= 'a@example.com';
			//キャンペーンターゲット①IDに紐づくリードTMP作成
			MK_leadTMP__c mk_leadTMP = TestMK_TestUtil.createMK_leadTMP(true,ctid,email);
			//テストされるメソッドにキャンペーンターゲット①IDを渡す
			Set<String> results = E_MK_leadTMPDao.getEmailListByCmpgnTrgt(ctid);
			//テストされるメソッドに含まれるメールアドレスがリードTMPのメールアドレスと一致でtrue
			System.assertEquals(true,results.contains(mk_leadTMP.email__c));
		}
}