public class AGC_TestCommon{
	
	public AGC_Shisha_Mst__c createShishaMst(){
	
		AGC_Shisha_Mst__c mst = new AGC_Shisha_Mst__c();
		mst.Name = 'テスト支社名';
		mst.ShishaCd__c = 'A1';
		return mst;
	}

	public AGC_ShishaMr_Mst__c createTantoMst(){
		AGC_ShishaMr_Mst__c mst = new AGC_ShishaMr_Mst__c();
		mst.MRCode__c = '01';
		mst.SectionName__c = 'テスト部署名';
		mst.Asterisk__c = '*';
		mst.Name = '担当者';
		return mst;
	
	
}   
	public AGC_DairitenKihonJoho__c createKihonInfo(Id sishaId,Id tantoId){
		AGC_DairitenKihonJoho__c kihon = new AGC_DairitenKihonJoho__c();
		kihon.BranchID__c = '10000';
		kihon.NewRecord__c = true;
		kihon.BranchCodeRef__c = sishaId;
		kihon.MRCodeRef__c = tantoId;
		
		return kihon;
	}
	
	public User createEigyoUser(){
		User user = new User();
		user.Username = 'agc_testuser@agc_test.ing-life.co.jp';
		user.ProfileId = [select Id from Profile where Name = '本社営業部MR'][0].Id;
		user.LastName = 'test';
		user.Email = 'aaa@ing.com';
		user.Alias = 'testuser';
		user.TimeZoneSidKey = 'Asia/Tokyo';
		user.LocaleSidKey = 'ja_JP';
		user.EmailEncodingKey = 'ISO-2022-JP';
		user.LanguageLocaleKey = 'ja';
		
		return user;
	}
	
	public User createApiUser(){
		User user = new User();
		user.Username = 'agc_testapiuser@agc_test.ing-life.co.jp';
		user.ProfileId = [select Id from Profile where Name = 'インフォマティカ連携用'][0].Id;
		user.LastName = 'test';
		user.Email = 'aaa@ing.com';
		user.Alias = 'testuser';
		user.TimeZoneSidKey = 'Asia/Tokyo';
		user.LocaleSidKey = 'ja_JP';
		user.EmailEncodingKey = 'ISO-2022-JP';
		user.LanguageLocaleKey = 'ja';
		
		return user;
	}
	
	public AGC_Config__c getCustomSetting(){
		AGC_Config__c conf = new AGC_Config__c();
		conf.GyohaiHanteiDays__c = 2;
		conf.APIUserName__c = 'agc_testapiuser@agc_test.ing-life.co.jp';
		//conf.APIUserName__c = 'takasu@sfdc.inglj.dev3';
		conf.Name = 'Default';
		insert conf;
		return conf;

	}
}