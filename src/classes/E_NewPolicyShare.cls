public with sharing class E_NewPolicyShare extends E_ShareBase {
	// 主募集人の事務所マップ
	private Map<SObject, Account> accountMap;
	// 従募集人の事務所マップ
	private Map<SObject, Account> subAccountMap;
	// 募集人のUserIdマップ
	private Map<String, Id> agentUserMap;
    
    /**
     * Constructor
     */
    public E_NewPolicyShare(Map<SObject, Account> aMap, Map<SObject, Account> sMap){
    	accountMap = aMap;
    	subAccountMap = sMap;
    	agentUserMap = new Map<String, Id>();
    }
    
    /**
     * execute
     */
    public void execute(){
		// Share削除
		deleteNNLinkShares(accountMap.keySet());
		
		// Share登録
		createSharesNNLink(accountMap, E_Const.SHARE_ACCESS_LEVEL_READ);
    }

	/**
	 * override
	 * Shareロジック
	 */
	protected override void createSharesBizLogic(Map<SObject, Account> recs){
		/* 募集人コードの先頭5桁より、有効なユーザを取得するように変更
		// 募集人（主/従）のContactIdセット作成
		Set<Id> agentContactIds = new Set<Id>();
		for(SObject parentRec : recs.keyset()){
			agentContactIds.add((ID)parentRec.get('Agent__c'));
			agentContactIds.add((ID)parentRec.get('SubAgent__c'));
		}

		// 募集人（主/従）のUserMap作成
		List<User> users = [Select Id, Contact.E_CL3PF_AGNTNUM__c From User Where Contact.Id In :agentContactIds And IsActive = true];
		for(User u : users){
			agentUserMap.put(u.Contact.E_CL3PF_AGNTNUM__c, u.Id);
		}
		*/

		// 募集人コード（5桁）
		Set<String> agentNums = new Set<String>();
		for(SObject parentRec : recs.keyset()){
			// 主募集人コード（5桁）
			String mainCode = (String)parentRec.get(I_Const.API_MAINAGENTNUM5);
			if(String.isNotBlank(mainCode)){
				agentNums.add(mainCode);
			}
			// 従募集人コード（5桁）
			String subCode = (String)parentRec.get(I_Const.API_SUBAGENTNUM5);
			if(String.isNotBlank(subCode)){
				agentNums.add(subCode);
			}
		}
		
		// 募集人（主/従）のUserMap作成	※募集人コード（5桁）から有効なユーザを取得
		List<User> users = [Select Id, Contact.AgentNum5__c From User Where Contact.AgentNum5__c In :agentNums And IsActive = true Order by Contact.E_CL3PF_AGNTNUM__c asc];
		for(User u : users){
			agentUserMap.put(u.Contact.AgentNum5__c, u.Id);
		}	
		
		// 事務所毎の公開グループより、GroupIdを取得
		Map<String, Group> grMap = getGroupIds(recs); 
		
		// 事務所の件数分、処理を行う
		for(SObject parentRec : recs.keyset()){
			// 事務所レコード取得
			Account acc = recs.get(parentRec);
		
			// 事務所レコードが取得できない場合は、Skipする。
			if(acc == null) continue;
			
			//ShareオブジェクトのAPI名取得		prefixが変更された場合のみDescribeを呼び出す。
			if(lastRecPrefix == null || lastRecPrefix != String.valueOf(parentRec.Id).subString(0,3)){
				sObjName = parentRec.Id.getSObjectType().getDescribe().getName();
				shareObjName = sObjName.replace('__c','__share');
				shareObjType = Schema.getGlobalDescribe().get(shareObjName);
				lastRecPrefix = String.valueOf(parentRec.Id).subString(0,3);
			}
			
			// Shareの作成
			createShares(acc, parentRec, grMap);
		}
	}
	
	/**
	 * override
	 * 事務所毎の公開グループ取得
	 */
	protected override Set<String> getGroupNames(Map<SObject, Account> recs){
		Set<String> groupNames = new Set<String>();
		
		// 事務所毎の公開グループを設定（L1、L2、BR）
		for(SObject parentRec : recs.keySet()){
			/* 主募集人 */
			// 事務所レコード取得
			Account acc = recs.get(parentRec);
			if(acc == null) continue;
			
			// L1（代理店格コード）
			if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
				groupNames.add('L1' + acc.E_ParentZHEADAY__c);
			}
			// L2（代理店事務所コード）
			if(String.isNotBlank(acc.E_CL2PF_ZAGCYNUM__c)){
				groupNames.add('L2' + acc.E_CL2PF_ZAGCYNUM__c);
			}
			// BR（支社コード）
			if(String.isNotBlank(acc.E_CL2PF_BRANCH__c)){
				groupNames.add('BR' + acc.E_CL2PF_BRANCH__c);
			}
			
			/* 従募集人 */
			// 事務所レコード取得
			Account subAcc = subAccountMap.get(parentRec);
			if(subAcc == null) continue;

			// BR（支社コード）
			if(String.isNotBlank(subAcc.E_CL2PF_BRANCH__c)){
				groupNames.add('BR' + subAcc.E_CL2PF_BRANCH__c);
			}

			// L1（代理店格コード）
			if(String.isNotBlank(subAcc.E_ParentZHEADAY__c)){
				groupNames.add('L1' + subAcc.E_ParentZHEADAY__c);
			}
			// L2（代理店事務所コード）
			if(String.isNotBlank(subAcc.E_CL2PF_ZAGCYNUM__c)){
				groupNames.add('L2' + subAcc.E_CL2PF_ZAGCYNUM__c);
			}

		}
		
		return groupNames;
	}

	/**
	 * override
	 * Shareの作成
	 */
	protected override void createShares(Account acc, SObject parentRec, Map<String, Group> grMap){
		/* 主募集人 */
		//L1　公開グループ設定
		if(String.isNotBlank(acc.E_ParentZHEADAY__c)){
			Group grL1 = grMap.get('L1'+acc.E_ParentZHEADAY__c);
			if(grL1 != null){
				shares.add(createShare(shareObjType, parentRec.Id, grL1.Id));
			}
		}
		//L2　公開グループ設定
		if(String.isNotBlank(acc.E_CL2PF_ZAGCYNUM__c)){
			Group grL2 = grMap.get('L2'+acc.E_CL2PF_ZAGCYNUM__c);
			if(grL2 != null){
				shares.add(createShare(shareObjType, parentRec.Id, grL2.Id));
			}
		}
		//BR　公開グループ設定
		if(String.isNotBlank(acc.E_CL2PF_BRANCH__c)){
			Group grBR = grMap.get('BR'+acc.E_CL2PF_BRANCH__c);
			if(grBR != null){
				shares.add(createShare(shareObjType, parentRec.Id, grBR.Id));
			}	
		}
		// 募集人（主）ユーザ設定
		String agentCode = (String)parentRec.get(I_Const.API_MAINAGENTNUM5);
		if(String.isNotBlank(agentCode)){
			Id agentUserId = agentUserMap.get(agentCode);
			if(agentUserId != null){
				shares.add(createShare(shareObjType, parentRec.Id, agentUserId));
			}
		}
		
		/* 従募集人 */
		Account subAcc = subAccountMap.get(parentRec);
		if(subAcc != null){
			//BR　公開グループ設定
			if(String.isNotBlank(subAcc.E_CL2PF_BRANCH__c)){
				Group grBR = grMap.get('BR' + subAcc.E_CL2PF_BRANCH__c);
				if(grBR != null){
					shares.add(createShare(shareObjType, parentRec.Id, grBR.Id));
				}	
			}

			//L1　公開グループ設定
			if(String.isNotBlank(subAcc.E_ParentZHEADAY__c)){
				Group grL1 = grMap.get('L1' + subAcc.E_ParentZHEADAY__c);
				if(grL1 != null){
					shares.add(createShare(shareObjType, parentRec.Id, grL1.Id));
				}
			}
			//L2　公開グループ設定
			if(String.isNotBlank(subAcc.E_CL2PF_ZAGCYNUM__c)){
				Group grL2 = grMap.get('L2' + subAcc.E_CL2PF_ZAGCYNUM__c);
				if(grL2 != null){
					shares.add(createShare(shareObjType, parentRec.Id, grL2.Id));
				}
			}
			// 募集人（従）ユーザ設定
			String subCode = (String)parentRec.get(I_Const.API_SUBAGENTNUM5);
			if(String.isNotBlank(subCode)){
				Id subUserId = agentUserMap.get(subCode);
				if(subUserId != null){
					shares.add(createShare(shareObjType, parentRec.Id, subUserId));
				}
			}
		}
	}
}