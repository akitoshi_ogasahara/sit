@isTest
private with sharing class MNT_DocumentSearchTest{
		private static testMethod void testPageMethods() {	
			MNT_DocumentSearch page = new MNT_DocumentSearch(new ApexPages.StandardController(new I_ContentMaster__c()));	
			page.getOperatorOptions_I_ContentMaster_c_Name();	
			page.getOperatorOptions_I_ContentMaster_c_DocumentCategory_c_multi();	
			page.getOperatorOptions_I_ContentMaster_c_FormNo_c();	
			page.getOperatorOptions_I_ContentMaster_c_CannotBeOrder_c();	
			page.getOperatorOptions_I_ContentMaster_c_DisplayFrom_c();	
			System.assert(true);
		}	
			
	private static testMethod void testResultList() {
		MNT_DocumentSearch.ResultList ResultList = new MNT_DocumentSearch.ResultList(new List<I_ContentMaster__c>(), new List<MNT_DocumentSearch.ResultListItem>(), new List<I_ContentMaster__c>(), null);
		ResultList.setPageItems(new List<MNT_DocumentSearch.ResultListItem>());
		ResultList.create(new I_ContentMaster__c());
		ResultList.doDeleteSelectedItems();
		ResultList.setPagesize(10);		ResultList.doFirst();
		ResultList.doPrevious();
		ResultList.doNext();
		ResultList.doLast();
		ResultList.doSort();
		System.assert(true);
	}
	
}