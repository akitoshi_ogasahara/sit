public with sharing class MNT_SalesToolMainteExtender extends SkyEditor2.Extender{
	
	private MNT_SalesToolMainteSVEController controller;
	
	/**
	 * Constructor
	 */
	public MNT_SalesToolMainteExtender(MNT_SalesToolMainteSVEController extension){
		this.controller = extension;
	}
	
	/**
	 * afterSearch
	 */
	public override void afterSearch(){
		// 検索結果
		SkyEditor2.ListItemHolder lih = controller.listItemHolders.get('resultTable');
		
		// ヘッダーの選択チェックのON
		lih.selected = true;
		
		// レコードの選択チェックON
		List<SObject> recs = new List<SObject>();
		for(SkyEditor2.ListItem item : lih.items){
			item.selected = true;
		}
	}

	/**
	 * CSVExport
	 */
	public PageReference doExportCSV(){
		PageReference pr = Page.MNT_ExportSalesToolMainte;
		String soql = controller.queryMap.get('resultTable').toSoql();
		String sWhere = E_SoqlUtil.getAfterWhereClause(soql, controller.mainSObjectType.getDescribe().getName());
		sWhere = E_SoqlUtil.trimLimitClause(sWhere);
		pr.getParameters().put(E_CSVExportController.SOQL_WHERE, 
								E_EncryptUtil.getEncryptedString(sWhere));  
		return pr;
	}

	/**
	 * 掲示取下

	public void doWithdrawal(){
		// 検索結果
		SkyEditor2.ListItemHolder lih = controller.listItemHolders.get('resultTable');
		
		// 選択されたレコードの操作
		List<SObject> recs = new List<SObject>();
		for(SkyEditor2.ListItem item : lih.items){
			// 選択されているレコード
			if(item.selected == true){
				// レコードをSObjectにセット
				SObject sobj = item.getRecord();
				
				// 有効期限に本日日付をセット
				sobj.put('ValidTo__c', System.today());
				recs.add(sobj);
				
				// 選択チェックのOFF
				item.selected = false;
			}
		}
		// Update
		update recs;
		
		// ヘッダーの選択チェックのOFF
		lih.selected = false;
	}
	 */
}