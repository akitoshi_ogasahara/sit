public with sharing class E_UPTable005Extension{

	// CSVDownloadURL@OPRO
	public String downloadUrl{get; set;}
	
    private E_UPTable005Service tblsv;
    private E_UPTerm003Controller controller;
    private E_DownloadCondition.UnitPriceDownloadCondi downloadCondi;

    /**
     *      Constructor
     */
    public E_UPTable005Extension(E_UPTerm003Controller con){
        controller = con;
        tblsv = null;
    }
    
    public PageReference init005(){
        try{
            controller.pgTitle = E_UPTerm003Controller.UNITPRICE_PAGENAMES.get('005');      //ユニットプライス
            PageReference pr = controller.init();
            if(pr!=null){return pr;}
            
            //　DLConditionがNULLの場合、
            if(controller.dlCondi==null){
                return E_Util.toUpErrorPage('ERR|002');
            }
            
            //Indexレコードが1件以上存在しているかの検証のため、Serviceクラスを取得 ⇒存在しない場合　UPIndexNotExistsException
            getTblService();
            
            return null;
        }catch(E_UPTable005Service.UPIndexNotExistsException upe){  
            return E_Util.toUpErrorPage('UP5|E01');
        }catch(Exception e){
            return E_Util.toUpErrorPage('ユニットプライス表示　初期処理中にエラーが発生しました。 ' + e.getMessage());//エラーを発生させられないため、テストメソッドでは通らない
        }
    }
    
    public E_UPTable005Service getTblService(){
        if(tblsv==null){
            downloadCondi = controller.dlCondi;

			//終了日-開始日>10年の場合に開始日を10年前の日付にする
			Date fromDt = E_Util.string2DateyyyyMMdd(downloadCondi.fromTerm);
			Date toDt = E_Util.string2DateyyyyMMdd(downloadCondi.toTerm);
			Date toDt_10yrsBefore = toDt.addYears(-10);
			if(fromDt < toDt_10yrsBefore){
				downloadCondi.fromTerm = E_Util.date2YYYYMMDD(toDt_10yrsBefore);
			}

            //表示用ファンドリストの取得
//            List<E_UPFPF__c> funds = E_UPFPFDao.getIndexesBySvcpfIds(downloadCondi.ZSFUNDCD
            List<E_UPFPF__c> funds = E_UPFPFDao.getIndexLimit1BySvcpfIds(downloadCondi.ZSFUNDCD
                                                                            , downloadCondi.fromTerm
                                                                            , downloadCondi.toTerm);
            
            // 表示サービスクラス    インデックスレコードが0件の場合には例外が生成される
            tblsv = new E_UPTable005Service(funds, downloadCondi.NAMEKBN);
        }
        return tblsv;
    }

    public Integer selectedPageNo{get{
                                        if(selectedPageNo==null) selectedPageNo=1;
                                        return selectedPageNo;
                                    }
                                    set;}


    /**
     *  ChangePage
     *      ページ変更アクション
     */
    public PageReference changePage(){
        return null;
    }

    /**
     *  getIndexTableHTML
     *      テーブル情報取得
     */
    public String getIndexTableHTML(){
    
        return getTblService().getIndexTableHTML(selectedPageNo
                                                , downloadCondi.fromTerm
                                                , downloadCondi.toTerm);
    
    }
	
    /**
     *  DownloadCSV
     *      CSV出力（OPRO呼出し）
     */
    public void downloadCSV(){
        //up003.pageからも呼び出されることあり
        controller.showPageLink = false;
        
        //ダウンロードログレコード生成
        E_DownloadHistorry__c dh = new E_DownloadHistorry__c();
        dh.outputType__c = E_Const.DH_OUTPUTTYPE_CSV;
        dh.Conditions__c = controller.dlCondi.toJSON();
        insert dh;
        
        //OPRO　URLの組み立て
        downloadUrl = Label.E_UP_OPRO_DOMAIN;
        downloadUrl += Label.E_UP_OPRO_CSV;
        downloadUrl += dh.Id;
        downloadUrl += Label.E_UP_OPRO_SUFFIX;
    }
}