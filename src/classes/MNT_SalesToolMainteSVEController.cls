global with sharing class MNT_SalesToolMainteSVEController extends SkyEditor2.SkyEditorPageBaseWithSharing{
	public I_ContentMaster__c record{get;set;}
	public MNT_SalesToolMainteExtender getExtender() {return (MNT_SalesToolMainteExtender)extender;}
	public resultTable resultTable {get; private set;}
	public SkyEditor2.RecordTypeSelector recordTypeSelector {get; private set;}
	public SkyEditor2__SkyEditorDummy__c Component25_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component25_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component27_from{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component27_to{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component24_val {get;set;}
	public SkyEditor2.TextHolder Component24_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component36_val {get;set;}
	public SkyEditor2.TextHolder Component36_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component38_val {get;set;}
	public SkyEditor2.TextHolder Component38_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component8_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component8_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component8_op{get;set;}
	public List<SelectOption> valueOptions_I_ContentMaster_c_Category_c {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component94_val {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component94_val_dummy {get;set;}
	public SkyEditor2.TextHolder Component94_op{get;set;}
	public List<SelectOption> valueOptions_I_ContentMaster_c_Management_c {get;set;}
	public SkyEditor2__SkyEditorDummy__c Component84_val {get;set;}
	public SkyEditor2.TextHolder Component84_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component86_val {get;set;}
	public SkyEditor2.TextHolder Component86_op{get;set;}
	public SkyEditor2__SkyEditorDummy__c Component88_val {get;set;}
	public SkyEditor2.TextHolder Component88_op{get;set;}
	public String recordTypeRecordsJSON_I_ContentMaster_c {get; private set;}
	public String defaultRecordTypeId_I_ContentMaster_c {get; private set;}
	public String metadataJSON_I_ContentMaster_c {get; private set;}
	{
	setApiVersion(42.0);
	}
	public MNT_SalesToolMainteSVEController(ApexPages.StandardController controller){
		super(controller);

		SObjectField f;

		f = I_ContentMaster__c.fields.Name;
		f = I_ContentMaster__c.fields.DocumentNo__c;
		f = I_ContentMaster__c.fields.ApprovalNo__c;
		f = I_ContentMaster__c.fields.Category__c;
		f = I_ContentMaster__c.fields.Management__c;
		f = I_ContentMaster__c.fields.ForCustomers__c;
		f = I_ContentMaster__c.fields.ForAgency__c;
		f = I_ContentMaster__c.fields.CompanyLimited__c;
		f = I_ContentMaster__c.fields.Content__c;
		f = I_ContentMaster__c.fields.ContentEmployee__c;
		f = I_ContentMaster__c.fields.DisplayFrom__c;
		f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.FileTarget__c;
 f = I_ContentMaster__c.fields.DisplayFrom__c;
 f = I_ContentMaster__c.fields.ValidTo__c;
		f = I_ContentMaster__c.fields.ParentContent__c;
		f = I_ContentMaster__c.fields.DisplayOrderKbn__c;
		f = I_ContentMaster__c.fields.DisplayOrderNo__c;

		List<RecordTypeInfo> recordTypes;
		try {
			mainRecord = null;
			mainSObjectType = I_ContentMaster__c.SObjectType;
			mode = SkyEditor2.LayoutMode.TempSearch_01; 
			Component25_from = new SkyEditor2__SkyEditorDummy__c();
			Component25_to = new SkyEditor2__SkyEditorDummy__c();
			Component27_from = new SkyEditor2__SkyEditorDummy__c();
			Component27_to = new SkyEditor2__SkyEditorDummy__c();
			Component24_val = new SkyEditor2__SkyEditorDummy__c();
			Component24_op = new SkyEditor2.TextHolder('co');
			Component36_val = new SkyEditor2__SkyEditorDummy__c();
			Component36_op = new SkyEditor2.TextHolder('co');
			Component38_val = new SkyEditor2__SkyEditorDummy__c();
			Component38_op = new SkyEditor2.TextHolder('co');
			Component8_val = new SkyEditor2__SkyEditorDummy__c();
			Component8_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component8_op = new SkyEditor2.TextHolder('inc');
			valueOptions_I_ContentMaster_c_Category_c = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : I_ContentMaster__c.Category__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_I_ContentMaster_c_Category_c.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component94_val = new SkyEditor2__SkyEditorDummy__c();
			Component94_val_dummy = new SkyEditor2__SkyEditorDummy__c();
			Component94_op = new SkyEditor2.TextHolder('inc');
			valueOptions_I_ContentMaster_c_Management_c = new List<SelectOption>{new SelectOption('', Label.none)};
			for (PicklistEntry e : I_ContentMaster__c.Management__c.getDescribe().getPicklistValues()) {
				if (e.isActive()) {valueOptions_I_ContentMaster_c_Management_c.add(new SelectOption(e.getValue(), e.getLabel()));}
			}
			Component84_val = new SkyEditor2__SkyEditorDummy__c();
			Component84_op = new SkyEditor2.TextHolder('eq');
			Component86_val = new SkyEditor2__SkyEditorDummy__c();
			Component86_op = new SkyEditor2.TextHolder('eq');
			Component88_val = new SkyEditor2__SkyEditorDummy__c();
			Component88_op = new SkyEditor2.TextHolder('eq');
			queryMap.put(
				'resultTable',
				new SkyEditor2.Query('I_ContentMaster__c')
					.addFieldAsOutput('Name')
					.addField('DocumentNo__c')
					.addField('Content__c')
					.addField('ContentEmployee__c')
					.addField('ApprovalNo__c')
					.addField('DisplayFrom__c')
					.addField('ValidTo__c')
					.addFieldAsOutput('FileTarget__c')
					.limitRecords(545)
					.addListener(new SkyEditor2.QueryWhereRegister(Component25_from, 'SkyEditor2__Date__c', 'DisplayFrom__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component25_to, 'SkyEditor2__Date__c', 'DisplayFrom__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component27_from, 'SkyEditor2__Date__c', 'ValidTo__c', new SkyEditor2.TextHolder('ge'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component27_to, 'SkyEditor2__Date__c', 'ValidTo__c', new SkyEditor2.TextHolder('le'), false, 0 )) 
					.addListener(new SkyEditor2.QueryWhereRegister(Component24_val, 'SkyEditor2__Text__c', 'Name', Component24_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component36_val, 'SkyEditor2__Text__c', 'DocumentNo__c', Component36_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component38_val, 'SkyEditor2__Text__c', 'ApprovalNo__c', Component38_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component8_val_dummy, 'SkyEditor2__Text__c','Category__c', Component8_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component94_val_dummy, 'SkyEditor2__Text__c','Management__c', Component94_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component84_val, 'SkyEditor2__Checkbox__c', 'ForCustomers__c', Component84_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component86_val, 'SkyEditor2__Checkbox__c', 'ForAgency__c', Component86_op, true, 0, true ))
					.addListener(new SkyEditor2.QueryWhereRegister(Component88_val, 'SkyEditor2__Checkbox__c', 'CompanyLimited__c', Component88_op, true, 0, true ))
					.addWhere(' ( ParentContent__r.Name = \'販売促進資料\')')
.addSort('DisplayOrderKbn__c',True,False).addSort('DisplayOrderNo__c',True,False)
				);
			resultTable = new resultTable(new List<I_ContentMaster__c>(), new List<resultTableItem>(), new List<I_ContentMaster__c>(), null);
			listItemHolders.put('resultTable', resultTable);
			recordTypeSelector = new SkyEditor2.RecordTypeSelector(I_ContentMaster__c.SObjectType, true);
			p_showHeader = true;
			p_sidebar = false;
			extender = new MNT_SalesToolMainteExtender(this);
			execInitialSearch = false;
			presetSystemParams();
			extender.init();
			resultTable.extender = this.extender;
			initSearch();
		} catch (SkyEditor2.Errors.SObjectNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.Errors.FieldNotFoundException e) {
			SkyEditor2.Messages.addErrorMessage(e.getMessage());
		} catch (SkyEditor2.ExtenderException e) {
			 e.setMessagesToPage();
		} catch (Exception e) {
			System.Debug(LoggingLevel.Error, e);
			SkyEditor2.Messages.addErrorMessage(SkyEditor2.Messages.UNKNOWN_ERROR);
		}
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Name() { 
		return getOperatorOptions('I_ContentMaster__c', 'Name');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_DocumentNo_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'DocumentNo__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_ApprovalNo_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'ApprovalNo__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Category_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'Category__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_Management_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'Management__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_ForCustomers_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'ForCustomers__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_ForAgency_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'ForAgency__c');
	}
	public List<SelectOption> getOperatorOptions_I_ContentMaster_c_CompanyLimited_c() { 
		return getOperatorOptions('I_ContentMaster__c', 'CompanyLimited__c');
	}
	global with sharing class resultTableItem extends SkyEditor2.ListItem {
		public I_ContentMaster__c record{get; private set;}
		@TestVisible
		resultTableItem(resultTable holder, I_ContentMaster__c record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(holder);
			if (record.Id == null ){
				if (recordTypeSelector != null) {
					recordTypeSelector.applyDefault(record);
				}
			}
			this.record = record;
		}
		global override SObject getRecord() {return record;}
		public void doDeleteItem(){deleteItem();}
	}
	global with sharing  class resultTable extends SkyEditor2.ListItemHolder {
		public List<resultTableItem> items{get; private set;}
		@TestVisible
			resultTable(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
			super(records, items, deleteRecords, recordTypeSelector);
			this.items = (List<resultTableItem>)items;
		}
		global override SkyEditor2.ListItem create(SObject data) {
			return new resultTableItem(this, (I_ContentMaster__c)data, recordTypeSelector);
		}
		public void doDeleteSelectedItems(){deleteSelectedItems();}
	}

	public I_ContentMaster__c resultTable_table_Conversion { get { return new I_ContentMaster__c();}}
	
	public String resultTable_table_selectval { get; set; }
	
	
}