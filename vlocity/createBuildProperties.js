const fs = require('fs');
const output_name = "build.properties";
  
var output_text = "";
var sfdc = {
    USERNAME : 2,
    PASSWORD : 3,
    URL : 4
};

for(var i = 0;i < process.argv.length; i++){

  switch(i)    {
    case sfdc.USERNAME:
        output_text = "sf.username = " + process.argv[i] + "\n";
        break;
        
    case sfdc.PASSWORD:
        output_text += "sf.password = " + process.argv[i] + "\n";
        break;
        
    case sfdc.URL:
        output_text += "vlocity.namespace = vlocity_ins" + "\n";
        output_text += "vlocity.dataPacksJobFolder = dataPackJobs" + "\n";
        output_text += "sf.serverurl = " + process.argv[i] + "\n";
        output_text += "sf.loginUrl = " + process.argv[i] + "\n";
        break;
        
  }

}

fs.writeFileSync(output_name,output_text,"utf-8");
